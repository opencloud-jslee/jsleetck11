#Signature file v2
#Version 1.1
CLSS public static final javax.slee.resource.ConfigProperties$Property
cons public javax.slee.resource.ConfigProperties$Property.Property(java.lang.String,java.lang.String,java.lang.Object)
intf java.io.Serializable
meth protected java.lang.Object java.lang.Object.clone() throws java.lang.CloneNotSupportedException
meth protected void java.lang.Object.finalize() throws java.lang.Throwable
meth public boolean javax.slee.resource.ConfigProperties$Property.equals(java.lang.Object)
meth public final java.lang.Class java.lang.Object.getClass()
meth public final void java.lang.Object.notify()
meth public final void java.lang.Object.notifyAll()
meth public final void java.lang.Object.wait() throws java.lang.InterruptedException
meth public final void java.lang.Object.wait(long) throws java.lang.InterruptedException
meth public final void java.lang.Object.wait(long,int) throws java.lang.InterruptedException
meth public int javax.slee.resource.ConfigProperties$Property.hashCode()
meth public java.lang.Object javax.slee.resource.ConfigProperties$Property.getValue()
meth public java.lang.String javax.slee.resource.ConfigProperties$Property.getName()
meth public java.lang.String javax.slee.resource.ConfigProperties$Property.getType()
meth public java.lang.String javax.slee.resource.ConfigProperties$Property.toString()
meth public static java.lang.Object javax.slee.resource.ConfigProperties$Property.toObject(java.lang.String,java.lang.String)
meth public void javax.slee.resource.ConfigProperties$Property.setValue(java.lang.Object)
supr java.lang.Object
CLSS public static abstract interface javax.slee.resource.ReceivableService$ReceivableEvent
meth public abstract boolean javax.slee.resource.ReceivableService$ReceivableEvent.isInitialEvent()
meth public abstract java.lang.String javax.slee.resource.ReceivableService$ReceivableEvent.getResourceOption()
meth public abstract javax.slee.EventTypeID javax.slee.resource.ReceivableService$ReceivableEvent.getEventType()
supr null
CLSS public abstract interface javax.slee.ActivityContextInterface
meth public abstract boolean javax.slee.ActivityContextInterface.equals(java.lang.Object)
meth public abstract boolean javax.slee.ActivityContextInterface.isAttached(javax.slee.SbbLocalObject)
meth public abstract boolean javax.slee.ActivityContextInterface.isEnding()
meth public abstract int javax.slee.ActivityContextInterface.hashCode()
meth public abstract java.lang.Object javax.slee.ActivityContextInterface.getActivity()
meth public abstract void javax.slee.ActivityContextInterface.attach(javax.slee.SbbLocalObject)
meth public abstract void javax.slee.ActivityContextInterface.detach(javax.slee.SbbLocalObject)
supr null
CLSS public abstract interface javax.slee.ActivityEndEvent
supr null
CLSS public javax.slee.Address
cons public javax.slee.Address.Address(javax.slee.AddressPlan,java.lang.String)
cons public javax.slee.Address.Address(javax.slee.AddressPlan,java.lang.String,javax.slee.AddressPresentation,javax.slee.AddressScreening,java.lang.String,java.lang.String)
intf java.io.Serializable
meth protected java.lang.Object java.lang.Object.clone() throws java.lang.CloneNotSupportedException
meth protected void java.lang.Object.finalize() throws java.lang.Throwable
meth public boolean javax.slee.Address.equals(java.lang.Object)
meth public final java.lang.Class java.lang.Object.getClass()
meth public final void java.lang.Object.notify()
meth public final void java.lang.Object.notifyAll()
meth public final void java.lang.Object.wait() throws java.lang.InterruptedException
meth public final void java.lang.Object.wait(long) throws java.lang.InterruptedException
meth public final void java.lang.Object.wait(long,int) throws java.lang.InterruptedException
meth public int javax.slee.Address.hashCode()
meth public java.lang.String javax.slee.Address.getAddressName()
meth public java.lang.String javax.slee.Address.getAddressString()
meth public java.lang.String javax.slee.Address.getSubAddressString()
meth public java.lang.String javax.slee.Address.toString()
meth public javax.slee.AddressPlan javax.slee.Address.getAddressPlan()
meth public javax.slee.AddressPresentation javax.slee.Address.getAddressPresentation()
meth public javax.slee.AddressScreening javax.slee.Address.getAddressScreening()
supr java.lang.Object
CLSS public final javax.slee.AddressPlan
fld  public static final int javax.slee.AddressPlan.ADDRESS_PLAN_AESA
fld  public static final int javax.slee.AddressPlan.ADDRESS_PLAN_E164
fld  public static final int javax.slee.AddressPlan.ADDRESS_PLAN_E164_MOBILE
fld  public static final int javax.slee.AddressPlan.ADDRESS_PLAN_GT
fld  public static final int javax.slee.AddressPlan.ADDRESS_PLAN_H323
fld  public static final int javax.slee.AddressPlan.ADDRESS_PLAN_IP
fld  public static final int javax.slee.AddressPlan.ADDRESS_PLAN_MULTICAST
fld  public static final int javax.slee.AddressPlan.ADDRESS_PLAN_NOT_PRESENT
fld  public static final int javax.slee.AddressPlan.ADDRESS_PLAN_NSAP
fld  public static final int javax.slee.AddressPlan.ADDRESS_PLAN_SIP
fld  public static final int javax.slee.AddressPlan.ADDRESS_PLAN_SLEE_PROFILE
fld  public static final int javax.slee.AddressPlan.ADDRESS_PLAN_SLEE_PROFILE_TABLE
fld  public static final int javax.slee.AddressPlan.ADDRESS_PLAN_SMTP
fld  public static final int javax.slee.AddressPlan.ADDRESS_PLAN_SSN
fld  public static final int javax.slee.AddressPlan.ADDRESS_PLAN_UNDEFINED
fld  public static final int javax.slee.AddressPlan.ADDRESS_PLAN_UNICAST
fld  public static final int javax.slee.AddressPlan.ADDRESS_PLAN_URI
fld  public static final int javax.slee.AddressPlan.ADDRESS_PLAN_X400
fld  public static final java.lang.String javax.slee.AddressPlan.AESA_STRING
fld  public static final java.lang.String javax.slee.AddressPlan.E164_MOBILE_STRING
fld  public static final java.lang.String javax.slee.AddressPlan.E164_STRING
fld  public static final java.lang.String javax.slee.AddressPlan.GT_STRING
fld  public static final java.lang.String javax.slee.AddressPlan.H323_STRING
fld  public static final java.lang.String javax.slee.AddressPlan.IP_STRING
fld  public static final java.lang.String javax.slee.AddressPlan.MULTICAST_STRING
fld  public static final java.lang.String javax.slee.AddressPlan.NOT_PRESENT_STRING
fld  public static final java.lang.String javax.slee.AddressPlan.NSAP_STRING
fld  public static final java.lang.String javax.slee.AddressPlan.SIP_STRING
fld  public static final java.lang.String javax.slee.AddressPlan.SLEE_PROFILE_STRING
fld  public static final java.lang.String javax.slee.AddressPlan.SLEE_PROFILE_TABLE_STRING
fld  public static final java.lang.String javax.slee.AddressPlan.SMTP_STRING
fld  public static final java.lang.String javax.slee.AddressPlan.SSN_STRING
fld  public static final java.lang.String javax.slee.AddressPlan.UNDEFINED_STRING
fld  public static final java.lang.String javax.slee.AddressPlan.UNICAST_STRING
fld  public static final java.lang.String javax.slee.AddressPlan.URI_STRING
fld  public static final java.lang.String javax.slee.AddressPlan.X400_STRING
fld  public static final javax.slee.AddressPlan javax.slee.AddressPlan.AESA
fld  public static final javax.slee.AddressPlan javax.slee.AddressPlan.E164
fld  public static final javax.slee.AddressPlan javax.slee.AddressPlan.E164_MOBILE
fld  public static final javax.slee.AddressPlan javax.slee.AddressPlan.GT
fld  public static final javax.slee.AddressPlan javax.slee.AddressPlan.H323
fld  public static final javax.slee.AddressPlan javax.slee.AddressPlan.IP
fld  public static final javax.slee.AddressPlan javax.slee.AddressPlan.MULTICAST
fld  public static final javax.slee.AddressPlan javax.slee.AddressPlan.NOT_PRESENT
fld  public static final javax.slee.AddressPlan javax.slee.AddressPlan.NSAP
fld  public static final javax.slee.AddressPlan javax.slee.AddressPlan.SIP
fld  public static final javax.slee.AddressPlan javax.slee.AddressPlan.SLEE_PROFILE
fld  public static final javax.slee.AddressPlan javax.slee.AddressPlan.SLEE_PROFILE_TABLE
fld  public static final javax.slee.AddressPlan javax.slee.AddressPlan.SMTP
fld  public static final javax.slee.AddressPlan javax.slee.AddressPlan.SSN
fld  public static final javax.slee.AddressPlan javax.slee.AddressPlan.UNDEFINED
fld  public static final javax.slee.AddressPlan javax.slee.AddressPlan.UNICAST
fld  public static final javax.slee.AddressPlan javax.slee.AddressPlan.URI
fld  public static final javax.slee.AddressPlan javax.slee.AddressPlan.X400
intf java.io.Serializable
meth protected java.lang.Object java.lang.Object.clone() throws java.lang.CloneNotSupportedException
meth protected void java.lang.Object.finalize() throws java.lang.Throwable
meth public boolean javax.slee.AddressPlan.equals(java.lang.Object)
meth public boolean javax.slee.AddressPlan.isAESA()
meth public boolean javax.slee.AddressPlan.isE164()
meth public boolean javax.slee.AddressPlan.isE164Mobile()
meth public boolean javax.slee.AddressPlan.isGT()
meth public boolean javax.slee.AddressPlan.isH323()
meth public boolean javax.slee.AddressPlan.isIP()
meth public boolean javax.slee.AddressPlan.isMulticast()
meth public boolean javax.slee.AddressPlan.isNSAP()
meth public boolean javax.slee.AddressPlan.isNotPresent()
meth public boolean javax.slee.AddressPlan.isSIP()
meth public boolean javax.slee.AddressPlan.isSMTP()
meth public boolean javax.slee.AddressPlan.isSSN()
meth public boolean javax.slee.AddressPlan.isSleeProfile()
meth public boolean javax.slee.AddressPlan.isSleeProfileTable()
meth public boolean javax.slee.AddressPlan.isURI()
meth public boolean javax.slee.AddressPlan.isUndefined()
meth public boolean javax.slee.AddressPlan.isUnicast()
meth public boolean javax.slee.AddressPlan.isX400()
meth public final java.lang.Class java.lang.Object.getClass()
meth public final void java.lang.Object.notify()
meth public final void java.lang.Object.notifyAll()
meth public final void java.lang.Object.wait() throws java.lang.InterruptedException
meth public final void java.lang.Object.wait(long) throws java.lang.InterruptedException
meth public final void java.lang.Object.wait(long,int) throws java.lang.InterruptedException
meth public int javax.slee.AddressPlan.hashCode()
meth public int javax.slee.AddressPlan.toInt()
meth public java.lang.String javax.slee.AddressPlan.toString()
meth public static javax.slee.AddressPlan javax.slee.AddressPlan.fromInt(int)
meth public static javax.slee.AddressPlan javax.slee.AddressPlan.fromString(java.lang.String)
supr java.lang.Object
CLSS public final javax.slee.AddressPresentation
fld  public static final int javax.slee.AddressPresentation.ADDRESS_PRESENTATION_ADDRESS_NOT_AVAILABLE
fld  public static final int javax.slee.AddressPresentation.ADDRESS_PRESENTATION_ALLOWED
fld  public static final int javax.slee.AddressPresentation.ADDRESS_PRESENTATION_RESTRICTED
fld  public static final int javax.slee.AddressPresentation.ADDRESS_PRESENTATION_UNDEFINED
fld  public static final java.lang.String javax.slee.AddressPresentation.ADDRESS_NOT_AVAILABLE_STRING
fld  public static final java.lang.String javax.slee.AddressPresentation.ALLOWED_STRING
fld  public static final java.lang.String javax.slee.AddressPresentation.RESTRICTED_STRING
fld  public static final java.lang.String javax.slee.AddressPresentation.UNDEFINED_STRING
fld  public static final javax.slee.AddressPresentation javax.slee.AddressPresentation.ADDRESS_NOT_AVAILABLE
fld  public static final javax.slee.AddressPresentation javax.slee.AddressPresentation.ALLOWED
fld  public static final javax.slee.AddressPresentation javax.slee.AddressPresentation.RESTRICTED
fld  public static final javax.slee.AddressPresentation javax.slee.AddressPresentation.UNDEFINED
intf java.io.Serializable
meth protected java.lang.Object java.lang.Object.clone() throws java.lang.CloneNotSupportedException
meth protected void java.lang.Object.finalize() throws java.lang.Throwable
meth public boolean javax.slee.AddressPresentation.equals(java.lang.Object)
meth public boolean javax.slee.AddressPresentation.isAddressNotAvailable()
meth public boolean javax.slee.AddressPresentation.isAllowed()
meth public boolean javax.slee.AddressPresentation.isRestricted()
meth public boolean javax.slee.AddressPresentation.isUndefined()
meth public final java.lang.Class java.lang.Object.getClass()
meth public final void java.lang.Object.notify()
meth public final void java.lang.Object.notifyAll()
meth public final void java.lang.Object.wait() throws java.lang.InterruptedException
meth public final void java.lang.Object.wait(long) throws java.lang.InterruptedException
meth public final void java.lang.Object.wait(long,int) throws java.lang.InterruptedException
meth public int javax.slee.AddressPresentation.hashCode()
meth public int javax.slee.AddressPresentation.toInt()
meth public java.lang.String javax.slee.AddressPresentation.toString()
meth public static javax.slee.AddressPresentation javax.slee.AddressPresentation.fromInt(int)
meth public static javax.slee.AddressPresentation javax.slee.AddressPresentation.fromString(java.lang.String)
supr java.lang.Object
CLSS public final javax.slee.AddressScreening
fld  public static final int javax.slee.AddressScreening.ADDRESS_SCREENING_NETWORK
fld  public static final int javax.slee.AddressScreening.ADDRESS_SCREENING_UNDEFINED
fld  public static final int javax.slee.AddressScreening.ADDRESS_SCREENING_USER_NOT_VERIFIED
fld  public static final int javax.slee.AddressScreening.ADDRESS_SCREENING_USER_VERIFIED_FAILED
fld  public static final int javax.slee.AddressScreening.ADDRESS_SCREENING_USER_VERIFIED_PASSED
fld  public static final java.lang.String javax.slee.AddressScreening.NETWORK_STRING
fld  public static final java.lang.String javax.slee.AddressScreening.UNDEFINED_STRING
fld  public static final java.lang.String javax.slee.AddressScreening.USER_NOT_VERIFIED_STRING
fld  public static final java.lang.String javax.slee.AddressScreening.USER_VERIFIED_FAILED_STRING
fld  public static final java.lang.String javax.slee.AddressScreening.USER_VERIFIED_PASSED_STRING
fld  public static final javax.slee.AddressScreening javax.slee.AddressScreening.NETWORK
fld  public static final javax.slee.AddressScreening javax.slee.AddressScreening.UNDEFINED
fld  public static final javax.slee.AddressScreening javax.slee.AddressScreening.USER_NOT_VERIFIED
fld  public static final javax.slee.AddressScreening javax.slee.AddressScreening.USER_VERIFIED_FAILED
fld  public static final javax.slee.AddressScreening javax.slee.AddressScreening.USER_VERIFIED_PASSED
intf java.io.Serializable
meth protected java.lang.Object java.lang.Object.clone() throws java.lang.CloneNotSupportedException
meth protected void java.lang.Object.finalize() throws java.lang.Throwable
meth public boolean javax.slee.AddressScreening.equals(java.lang.Object)
meth public boolean javax.slee.AddressScreening.isNetwork()
meth public boolean javax.slee.AddressScreening.isUndefined()
meth public boolean javax.slee.AddressScreening.isUserNotVerified()
meth public boolean javax.slee.AddressScreening.isUserVerifiedFailed()
meth public boolean javax.slee.AddressScreening.isUserVerifiedPassed()
meth public final java.lang.Class java.lang.Object.getClass()
meth public final void java.lang.Object.notify()
meth public final void java.lang.Object.notifyAll()
meth public final void java.lang.Object.wait() throws java.lang.InterruptedException
meth public final void java.lang.Object.wait(long) throws java.lang.InterruptedException
meth public final void java.lang.Object.wait(long,int) throws java.lang.InterruptedException
meth public int javax.slee.AddressScreening.hashCode()
meth public int javax.slee.AddressScreening.toInt()
meth public java.lang.String javax.slee.AddressScreening.toString()
meth public javax.slee.AddressScreening javax.slee.AddressScreening.fromString(java.lang.String)
meth public static javax.slee.AddressScreening javax.slee.AddressScreening.fromInt(int)
supr java.lang.Object
CLSS public abstract interface javax.slee.ChildRelation
intf java.lang.Iterable
intf java.util.Collection
meth public abstract [Ljava.lang.Object; java.util.Collection.toArray()
meth public abstract [Ljava.lang.Object; java.util.Collection.toArray([Ljava.lang.Object;)
meth public abstract boolean java.util.Collection.add(java.lang.Object)
meth public abstract boolean java.util.Collection.addAll(java.util.Collection)
meth public abstract boolean java.util.Collection.contains(java.lang.Object)
meth public abstract boolean java.util.Collection.containsAll(java.util.Collection)
meth public abstract boolean java.util.Collection.equals(java.lang.Object)
meth public abstract boolean java.util.Collection.isEmpty()
meth public abstract boolean java.util.Collection.remove(java.lang.Object)
meth public abstract boolean java.util.Collection.removeAll(java.util.Collection)
meth public abstract boolean java.util.Collection.retainAll(java.util.Collection)
meth public abstract int java.util.Collection.hashCode()
meth public abstract int java.util.Collection.size()
meth public abstract java.util.Iterator java.util.Collection.iterator()
meth public abstract javax.slee.SbbLocalObject javax.slee.ChildRelation.create() throws javax.slee.CreateException
meth public abstract void java.util.Collection.clear()
supr null
CLSS public abstract javax.slee.ComponentID
cons protected javax.slee.ComponentID.ComponentID(java.lang.String,java.lang.String,java.lang.String)
intf java.io.Serializable
intf java.lang.Cloneable
intf java.lang.Comparable
meth protected abstract java.lang.String javax.slee.ComponentID.getClassName()
meth protected final int javax.slee.ComponentID.compareTo(java.lang.String,javax.slee.ComponentID)
meth protected java.lang.Object java.lang.Object.clone() throws java.lang.CloneNotSupportedException
meth protected void java.lang.Object.finalize() throws java.lang.Throwable
meth public abstract int javax.slee.ComponentID.compareTo(java.lang.Object)
meth public final boolean javax.slee.ComponentID.equals(java.lang.Object)
meth public final int javax.slee.ComponentID.hashCode()
meth public final java.lang.Class java.lang.Object.getClass()
meth public final java.lang.String javax.slee.ComponentID.getName()
meth public final java.lang.String javax.slee.ComponentID.getVendor()
meth public final java.lang.String javax.slee.ComponentID.getVersion()
meth public final java.lang.String javax.slee.ComponentID.toString()
meth public final void java.lang.Object.notify()
meth public final void java.lang.Object.notifyAll()
meth public final void java.lang.Object.wait() throws java.lang.InterruptedException
meth public final void java.lang.Object.wait(long) throws java.lang.InterruptedException
meth public final void java.lang.Object.wait(long,int) throws java.lang.InterruptedException
supr java.lang.Object
CLSS public javax.slee.CreateException
cons public javax.slee.CreateException.CreateException(java.lang.String)
cons public javax.slee.CreateException.CreateException(java.lang.String,java.lang.Throwable)
intf java.io.Serializable
meth protected java.lang.Object java.lang.Object.clone() throws java.lang.CloneNotSupportedException
meth protected void java.lang.Object.finalize() throws java.lang.Throwable
meth public [Ljava.lang.StackTraceElement; java.lang.Throwable.getStackTrace()
meth public boolean java.lang.Object.equals(java.lang.Object)
meth public final java.lang.Class java.lang.Object.getClass()
meth public final void java.lang.Object.notify()
meth public final void java.lang.Object.notifyAll()
meth public final void java.lang.Object.wait() throws java.lang.InterruptedException
meth public final void java.lang.Object.wait(long) throws java.lang.InterruptedException
meth public final void java.lang.Object.wait(long,int) throws java.lang.InterruptedException
meth public int java.lang.Object.hashCode()
meth public java.lang.String java.lang.Throwable.getLocalizedMessage()
meth public java.lang.String java.lang.Throwable.getMessage()
meth public java.lang.String java.lang.Throwable.toString()
meth public java.lang.Throwable java.lang.Throwable.fillInStackTrace()
meth public java.lang.Throwable java.lang.Throwable.initCause(java.lang.Throwable)
meth public java.lang.Throwable javax.slee.CreateException.getCause()
meth public void java.lang.Throwable.printStackTrace()
meth public void java.lang.Throwable.printStackTrace(java.io.PrintStream)
meth public void java.lang.Throwable.printStackTrace(java.io.PrintWriter)
meth public void java.lang.Throwable.setStackTrace([Ljava.lang.StackTraceElement;)
supr java.lang.Object java.lang.Throwable java.lang.Exception
CLSS public abstract interface javax.slee.EventContext
meth public abstract boolean javax.slee.EventContext.isSuspended()
meth public abstract java.lang.Object javax.slee.EventContext.getEvent()
meth public abstract javax.slee.ActivityContextInterface javax.slee.EventContext.getActivityContextInterface()
meth public abstract javax.slee.Address javax.slee.EventContext.getAddress()
meth public abstract javax.slee.ServiceID javax.slee.EventContext.getService()
meth public abstract void javax.slee.EventContext.resumeDelivery()
meth public abstract void javax.slee.EventContext.suspendDelivery()
meth public abstract void javax.slee.EventContext.suspendDelivery(int)
supr null
CLSS public final javax.slee.EventTypeID
cons public javax.slee.EventTypeID.EventTypeID(java.lang.String,java.lang.String,java.lang.String)
intf java.io.Serializable
intf java.lang.Cloneable
intf java.lang.Comparable
meth protected final int javax.slee.ComponentID.compareTo(java.lang.String,javax.slee.ComponentID)
meth protected java.lang.String javax.slee.EventTypeID.getClassName()
meth protected void java.lang.Object.finalize() throws java.lang.Throwable
meth public final boolean javax.slee.ComponentID.equals(java.lang.Object)
meth public final int javax.slee.ComponentID.hashCode()
meth public final int javax.slee.EventTypeID.compareTo(java.lang.Object)
meth public final java.lang.Class java.lang.Object.getClass()
meth public final java.lang.String javax.slee.ComponentID.getName()
meth public final java.lang.String javax.slee.ComponentID.getVendor()
meth public final java.lang.String javax.slee.ComponentID.getVersion()
meth public final java.lang.String javax.slee.ComponentID.toString()
meth public final void java.lang.Object.notify()
meth public final void java.lang.Object.notifyAll()
meth public final void java.lang.Object.wait() throws java.lang.InterruptedException
meth public final void java.lang.Object.wait(long) throws java.lang.InterruptedException
meth public final void java.lang.Object.wait(long,int) throws java.lang.InterruptedException
meth public java.lang.Object javax.slee.EventTypeID.clone()
supr java.lang.Object javax.slee.ComponentID
CLSS public javax.slee.FactoryException
cons public javax.slee.FactoryException.FactoryException(java.lang.String)
cons public javax.slee.FactoryException.FactoryException(java.lang.String,java.lang.Throwable)
intf java.io.Serializable
meth protected java.lang.Object java.lang.Object.clone() throws java.lang.CloneNotSupportedException
meth protected void java.lang.Object.finalize() throws java.lang.Throwable
meth public [Ljava.lang.StackTraceElement; java.lang.Throwable.getStackTrace()
meth public boolean java.lang.Object.equals(java.lang.Object)
meth public final java.lang.Class java.lang.Object.getClass()
meth public final void java.lang.Object.notify()
meth public final void java.lang.Object.notifyAll()
meth public final void java.lang.Object.wait() throws java.lang.InterruptedException
meth public final void java.lang.Object.wait(long) throws java.lang.InterruptedException
meth public final void java.lang.Object.wait(long,int) throws java.lang.InterruptedException
meth public int java.lang.Object.hashCode()
meth public java.lang.String java.lang.Throwable.getLocalizedMessage()
meth public java.lang.String java.lang.Throwable.getMessage()
meth public java.lang.String java.lang.Throwable.toString()
meth public java.lang.Throwable java.lang.Throwable.fillInStackTrace()
meth public java.lang.Throwable java.lang.Throwable.initCause(java.lang.Throwable)
meth public java.lang.Throwable javax.slee.SLEEException.getCause()
meth public void java.lang.Throwable.printStackTrace()
meth public void java.lang.Throwable.printStackTrace(java.io.PrintStream)
meth public void java.lang.Throwable.printStackTrace(java.io.PrintWriter)
meth public void java.lang.Throwable.setStackTrace([Ljava.lang.StackTraceElement;)
supr java.lang.Object java.lang.Throwable java.lang.Exception java.lang.RuntimeException javax.slee.SLEEException
CLSS public abstract interface javax.slee.InitialEventSelector
meth public abstract boolean javax.slee.InitialEventSelector.isActivityContextSelected()
meth public abstract boolean javax.slee.InitialEventSelector.isAddressProfileSelected()
meth public abstract boolean javax.slee.InitialEventSelector.isAddressSelected()
meth public abstract boolean javax.slee.InitialEventSelector.isEventSelected()
meth public abstract boolean javax.slee.InitialEventSelector.isEventTypeSelected()
meth public abstract boolean javax.slee.InitialEventSelector.isInitialEvent()
meth public abstract java.lang.Object javax.slee.InitialEventSelector.getActivity()
meth public abstract java.lang.Object javax.slee.InitialEventSelector.getEvent()
meth public abstract java.lang.String javax.slee.InitialEventSelector.getCustomName()
meth public abstract java.lang.String javax.slee.InitialEventSelector.getEventName()
meth public abstract javax.slee.Address javax.slee.InitialEventSelector.getAddress()
meth public abstract void javax.slee.InitialEventSelector.setActivityContextSelected(boolean)
meth public abstract void javax.slee.InitialEventSelector.setAddress(javax.slee.Address)
meth public abstract void javax.slee.InitialEventSelector.setAddressProfileSelected(boolean)
meth public abstract void javax.slee.InitialEventSelector.setAddressSelected(boolean)
meth public abstract void javax.slee.InitialEventSelector.setCustomName(java.lang.String)
meth public abstract void javax.slee.InitialEventSelector.setEventSelected(boolean)
meth public abstract void javax.slee.InitialEventSelector.setEventTypeSelected(boolean)
meth public abstract void javax.slee.InitialEventSelector.setInitialEvent(boolean)
supr null
CLSS public javax.slee.InvalidArgumentException
cons public javax.slee.InvalidArgumentException.InvalidArgumentException()
cons public javax.slee.InvalidArgumentException.InvalidArgumentException(java.lang.String)
intf java.io.Serializable
meth protected java.lang.Object java.lang.Object.clone() throws java.lang.CloneNotSupportedException
meth protected void java.lang.Object.finalize() throws java.lang.Throwable
meth public [Ljava.lang.StackTraceElement; java.lang.Throwable.getStackTrace()
meth public boolean java.lang.Object.equals(java.lang.Object)
meth public final java.lang.Class java.lang.Object.getClass()
meth public final void java.lang.Object.notify()
meth public final void java.lang.Object.notifyAll()
meth public final void java.lang.Object.wait() throws java.lang.InterruptedException
meth public final void java.lang.Object.wait(long) throws java.lang.InterruptedException
meth public final void java.lang.Object.wait(long,int) throws java.lang.InterruptedException
meth public int java.lang.Object.hashCode()
meth public java.lang.String java.lang.Throwable.getLocalizedMessage()
meth public java.lang.String java.lang.Throwable.getMessage()
meth public java.lang.String java.lang.Throwable.toString()
meth public java.lang.Throwable java.lang.Throwable.fillInStackTrace()
meth public java.lang.Throwable java.lang.Throwable.getCause()
meth public java.lang.Throwable java.lang.Throwable.initCause(java.lang.Throwable)
meth public void java.lang.Throwable.printStackTrace()
meth public void java.lang.Throwable.printStackTrace(java.io.PrintStream)
meth public void java.lang.Throwable.printStackTrace(java.io.PrintWriter)
meth public void java.lang.Throwable.setStackTrace([Ljava.lang.StackTraceElement;)
supr java.lang.Object java.lang.Throwable java.lang.Exception
CLSS public javax.slee.InvalidStateException
cons public javax.slee.InvalidStateException.InvalidStateException()
cons public javax.slee.InvalidStateException.InvalidStateException(java.lang.String)
intf java.io.Serializable
meth protected java.lang.Object java.lang.Object.clone() throws java.lang.CloneNotSupportedException
meth protected void java.lang.Object.finalize() throws java.lang.Throwable
meth public [Ljava.lang.StackTraceElement; java.lang.Throwable.getStackTrace()
meth public boolean java.lang.Object.equals(java.lang.Object)
meth public final java.lang.Class java.lang.Object.getClass()
meth public final void java.lang.Object.notify()
meth public final void java.lang.Object.notifyAll()
meth public final void java.lang.Object.wait() throws java.lang.InterruptedException
meth public final void java.lang.Object.wait(long) throws java.lang.InterruptedException
meth public final void java.lang.Object.wait(long,int) throws java.lang.InterruptedException
meth public int java.lang.Object.hashCode()
meth public java.lang.String java.lang.Throwable.getLocalizedMessage()
meth public java.lang.String java.lang.Throwable.getMessage()
meth public java.lang.String java.lang.Throwable.toString()
meth public java.lang.Throwable java.lang.Throwable.fillInStackTrace()
meth public java.lang.Throwable java.lang.Throwable.getCause()
meth public java.lang.Throwable java.lang.Throwable.initCause(java.lang.Throwable)
meth public void java.lang.Throwable.printStackTrace()
meth public void java.lang.Throwable.printStackTrace(java.io.PrintStream)
meth public void java.lang.Throwable.printStackTrace(java.io.PrintWriter)
meth public void java.lang.Throwable.setStackTrace([Ljava.lang.StackTraceElement;)
supr java.lang.Object java.lang.Throwable java.lang.Exception
CLSS public javax.slee.NoSuchObjectLocalException
cons public javax.slee.NoSuchObjectLocalException.NoSuchObjectLocalException(java.lang.String)
cons public javax.slee.NoSuchObjectLocalException.NoSuchObjectLocalException(java.lang.String,java.lang.Throwable)
intf java.io.Serializable
meth protected java.lang.Object java.lang.Object.clone() throws java.lang.CloneNotSupportedException
meth protected void java.lang.Object.finalize() throws java.lang.Throwable
meth public [Ljava.lang.StackTraceElement; java.lang.Throwable.getStackTrace()
meth public boolean java.lang.Object.equals(java.lang.Object)
meth public final java.lang.Class java.lang.Object.getClass()
meth public final void java.lang.Object.notify()
meth public final void java.lang.Object.notifyAll()
meth public final void java.lang.Object.wait() throws java.lang.InterruptedException
meth public final void java.lang.Object.wait(long) throws java.lang.InterruptedException
meth public final void java.lang.Object.wait(long,int) throws java.lang.InterruptedException
meth public int java.lang.Object.hashCode()
meth public java.lang.String java.lang.Throwable.getLocalizedMessage()
meth public java.lang.String java.lang.Throwable.getMessage()
meth public java.lang.String java.lang.Throwable.toString()
meth public java.lang.Throwable java.lang.Throwable.fillInStackTrace()
meth public java.lang.Throwable java.lang.Throwable.initCause(java.lang.Throwable)
meth public java.lang.Throwable javax.slee.SLEEException.getCause()
meth public void java.lang.Throwable.printStackTrace()
meth public void java.lang.Throwable.printStackTrace(java.io.PrintStream)
meth public void java.lang.Throwable.printStackTrace(java.io.PrintWriter)
meth public void java.lang.Throwable.setStackTrace([Ljava.lang.StackTraceElement;)
supr java.lang.Object java.lang.Throwable java.lang.Exception java.lang.RuntimeException javax.slee.SLEEException
CLSS public javax.slee.NotAttachedException
cons public javax.slee.NotAttachedException.NotAttachedException()
cons public javax.slee.NotAttachedException.NotAttachedException(java.lang.String)
intf java.io.Serializable
meth protected java.lang.Object java.lang.Object.clone() throws java.lang.CloneNotSupportedException
meth protected void java.lang.Object.finalize() throws java.lang.Throwable
meth public [Ljava.lang.StackTraceElement; java.lang.Throwable.getStackTrace()
meth public boolean java.lang.Object.equals(java.lang.Object)
meth public final java.lang.Class java.lang.Object.getClass()
meth public final void java.lang.Object.notify()
meth public final void java.lang.Object.notifyAll()
meth public final void java.lang.Object.wait() throws java.lang.InterruptedException
meth public final void java.lang.Object.wait(long) throws java.lang.InterruptedException
meth public final void java.lang.Object.wait(long,int) throws java.lang.InterruptedException
meth public int java.lang.Object.hashCode()
meth public java.lang.String java.lang.Throwable.getLocalizedMessage()
meth public java.lang.String java.lang.Throwable.getMessage()
meth public java.lang.String java.lang.Throwable.toString()
meth public java.lang.Throwable java.lang.Throwable.fillInStackTrace()
meth public java.lang.Throwable java.lang.Throwable.initCause(java.lang.Throwable)
meth public java.lang.Throwable javax.slee.SLEEException.getCause()
meth public void java.lang.Throwable.printStackTrace()
meth public void java.lang.Throwable.printStackTrace(java.io.PrintStream)
meth public void java.lang.Throwable.printStackTrace(java.io.PrintWriter)
meth public void java.lang.Throwable.setStackTrace([Ljava.lang.StackTraceElement;)
supr java.lang.Object java.lang.Throwable java.lang.Exception java.lang.RuntimeException javax.slee.SLEEException
CLSS public abstract interface javax.slee.RolledBackContext
meth public abstract boolean javax.slee.RolledBackContext.isRemoveRolledBack()
meth public abstract java.lang.Object javax.slee.RolledBackContext.getEvent()
meth public abstract javax.slee.ActivityContextInterface javax.slee.RolledBackContext.getActivityContextInterface()
supr null
CLSS public javax.slee.SLEEException
cons public javax.slee.SLEEException.SLEEException(java.lang.String)
cons public javax.slee.SLEEException.SLEEException(java.lang.String,java.lang.Throwable)
intf java.io.Serializable
meth protected java.lang.Object java.lang.Object.clone() throws java.lang.CloneNotSupportedException
meth protected void java.lang.Object.finalize() throws java.lang.Throwable
meth public [Ljava.lang.StackTraceElement; java.lang.Throwable.getStackTrace()
meth public boolean java.lang.Object.equals(java.lang.Object)
meth public final java.lang.Class java.lang.Object.getClass()
meth public final void java.lang.Object.notify()
meth public final void java.lang.Object.notifyAll()
meth public final void java.lang.Object.wait() throws java.lang.InterruptedException
meth public final void java.lang.Object.wait(long) throws java.lang.InterruptedException
meth public final void java.lang.Object.wait(long,int) throws java.lang.InterruptedException
meth public int java.lang.Object.hashCode()
meth public java.lang.String java.lang.Throwable.getLocalizedMessage()
meth public java.lang.String java.lang.Throwable.getMessage()
meth public java.lang.String java.lang.Throwable.toString()
meth public java.lang.Throwable java.lang.Throwable.fillInStackTrace()
meth public java.lang.Throwable java.lang.Throwable.initCause(java.lang.Throwable)
meth public java.lang.Throwable javax.slee.SLEEException.getCause()
meth public void java.lang.Throwable.printStackTrace()
meth public void java.lang.Throwable.printStackTrace(java.io.PrintStream)
meth public void java.lang.Throwable.printStackTrace(java.io.PrintWriter)
meth public void java.lang.Throwable.setStackTrace([Ljava.lang.StackTraceElement;)
supr java.lang.Object java.lang.Throwable java.lang.Exception java.lang.RuntimeException
CLSS public abstract interface javax.slee.Sbb
meth public abstract void javax.slee.Sbb.sbbActivate()
meth public abstract void javax.slee.Sbb.sbbCreate() throws javax.slee.CreateException
meth public abstract void javax.slee.Sbb.sbbExceptionThrown(java.lang.Exception,java.lang.Object,javax.slee.ActivityContextInterface)
meth public abstract void javax.slee.Sbb.sbbLoad()
meth public abstract void javax.slee.Sbb.sbbPassivate()
meth public abstract void javax.slee.Sbb.sbbPostCreate() throws javax.slee.CreateException
meth public abstract void javax.slee.Sbb.sbbRemove()
meth public abstract void javax.slee.Sbb.sbbRolledBack(javax.slee.RolledBackContext)
meth public abstract void javax.slee.Sbb.sbbStore()
meth public abstract void javax.slee.Sbb.setSbbContext(javax.slee.SbbContext)
meth public abstract void javax.slee.Sbb.unsetSbbContext()
supr null
CLSS public abstract interface javax.slee.SbbContext
meth public abstract [Ljava.lang.String; javax.slee.SbbContext.getEventMask(javax.slee.ActivityContextInterface)
meth public abstract [Ljavax.slee.ActivityContextInterface; javax.slee.SbbContext.getActivities()
meth public abstract boolean javax.slee.SbbContext.getRollbackOnly()
meth public abstract javax.slee.SbbID javax.slee.SbbContext.getSbb()
meth public abstract javax.slee.SbbLocalObject javax.slee.SbbContext.getSbbLocalObject()
meth public abstract javax.slee.ServiceID javax.slee.SbbContext.getService()
meth public abstract javax.slee.facilities.Tracer javax.slee.SbbContext.getTracer(java.lang.String)
meth public abstract void javax.slee.SbbContext.maskEvent([Ljava.lang.String;,javax.slee.ActivityContextInterface) throws javax.slee.UnrecognizedEventException
meth public abstract void javax.slee.SbbContext.setRollbackOnly()
supr null
CLSS public final javax.slee.SbbID
cons public javax.slee.SbbID.SbbID(java.lang.String,java.lang.String,java.lang.String)
intf java.io.Serializable
intf java.lang.Cloneable
intf java.lang.Comparable
meth protected final int javax.slee.ComponentID.compareTo(java.lang.String,javax.slee.ComponentID)
meth protected java.lang.String javax.slee.SbbID.getClassName()
meth protected void java.lang.Object.finalize() throws java.lang.Throwable
meth public final boolean javax.slee.ComponentID.equals(java.lang.Object)
meth public final int javax.slee.ComponentID.hashCode()
meth public final int javax.slee.SbbID.compareTo(java.lang.Object)
meth public final java.lang.Class java.lang.Object.getClass()
meth public final java.lang.String javax.slee.ComponentID.getName()
meth public final java.lang.String javax.slee.ComponentID.getVendor()
meth public final java.lang.String javax.slee.ComponentID.getVersion()
meth public final java.lang.String javax.slee.ComponentID.toString()
meth public final void java.lang.Object.notify()
meth public final void java.lang.Object.notifyAll()
meth public final void java.lang.Object.wait() throws java.lang.InterruptedException
meth public final void java.lang.Object.wait(long) throws java.lang.InterruptedException
meth public final void java.lang.Object.wait(long,int) throws java.lang.InterruptedException
meth public java.lang.Object javax.slee.SbbID.clone()
supr java.lang.Object javax.slee.ComponentID
CLSS public abstract interface javax.slee.SbbLocalObject
meth public abstract boolean javax.slee.SbbLocalObject.isIdentical(javax.slee.SbbLocalObject)
meth public abstract byte javax.slee.SbbLocalObject.getSbbPriority()
meth public abstract void javax.slee.SbbLocalObject.remove()
meth public abstract void javax.slee.SbbLocalObject.setSbbPriority(byte)
supr null
CLSS public final javax.slee.ServiceID
cons public javax.slee.ServiceID.ServiceID(java.lang.String,java.lang.String,java.lang.String)
intf java.io.Serializable
intf java.lang.Cloneable
intf java.lang.Comparable
meth protected final int javax.slee.ComponentID.compareTo(java.lang.String,javax.slee.ComponentID)
meth protected java.lang.String javax.slee.ServiceID.getClassName()
meth protected void java.lang.Object.finalize() throws java.lang.Throwable
meth public final boolean javax.slee.ComponentID.equals(java.lang.Object)
meth public final int javax.slee.ComponentID.hashCode()
meth public final int javax.slee.ServiceID.compareTo(java.lang.Object)
meth public final java.lang.Class java.lang.Object.getClass()
meth public final java.lang.String javax.slee.ComponentID.getName()
meth public final java.lang.String javax.slee.ComponentID.getVendor()
meth public final java.lang.String javax.slee.ComponentID.getVersion()
meth public final java.lang.String javax.slee.ComponentID.toString()
meth public final void java.lang.Object.notify()
meth public final void java.lang.Object.notifyAll()
meth public final void java.lang.Object.wait() throws java.lang.InterruptedException
meth public final void java.lang.Object.wait(long) throws java.lang.InterruptedException
meth public final void java.lang.Object.wait(long,int) throws java.lang.InterruptedException
meth public java.lang.Object javax.slee.ServiceID.clone()
supr java.lang.Object javax.slee.ComponentID
CLSS public javax.slee.TransactionRequiredLocalException
cons public javax.slee.TransactionRequiredLocalException.TransactionRequiredLocalException(java.lang.String)
cons public javax.slee.TransactionRequiredLocalException.TransactionRequiredLocalException(java.lang.String,java.lang.Throwable)
intf java.io.Serializable
meth protected java.lang.Object java.lang.Object.clone() throws java.lang.CloneNotSupportedException
meth protected void java.lang.Object.finalize() throws java.lang.Throwable
meth public [Ljava.lang.StackTraceElement; java.lang.Throwable.getStackTrace()
meth public boolean java.lang.Object.equals(java.lang.Object)
meth public final java.lang.Class java.lang.Object.getClass()
meth public final void java.lang.Object.notify()
meth public final void java.lang.Object.notifyAll()
meth public final void java.lang.Object.wait() throws java.lang.InterruptedException
meth public final void java.lang.Object.wait(long) throws java.lang.InterruptedException
meth public final void java.lang.Object.wait(long,int) throws java.lang.InterruptedException
meth public int java.lang.Object.hashCode()
meth public java.lang.String java.lang.Throwable.getLocalizedMessage()
meth public java.lang.String java.lang.Throwable.getMessage()
meth public java.lang.String java.lang.Throwable.toString()
meth public java.lang.Throwable java.lang.Throwable.fillInStackTrace()
meth public java.lang.Throwable java.lang.Throwable.initCause(java.lang.Throwable)
meth public java.lang.Throwable javax.slee.SLEEException.getCause()
meth public void java.lang.Throwable.printStackTrace()
meth public void java.lang.Throwable.printStackTrace(java.io.PrintStream)
meth public void java.lang.Throwable.printStackTrace(java.io.PrintWriter)
meth public void java.lang.Throwable.setStackTrace([Ljava.lang.StackTraceElement;)
supr java.lang.Object java.lang.Throwable java.lang.Exception java.lang.RuntimeException javax.slee.SLEEException
CLSS public javax.slee.TransactionRolledbackLocalException
cons public javax.slee.TransactionRolledbackLocalException.TransactionRolledbackLocalException(java.lang.String)
cons public javax.slee.TransactionRolledbackLocalException.TransactionRolledbackLocalException(java.lang.String,java.lang.Throwable)
intf java.io.Serializable
meth protected java.lang.Object java.lang.Object.clone() throws java.lang.CloneNotSupportedException
meth protected void java.lang.Object.finalize() throws java.lang.Throwable
meth public [Ljava.lang.StackTraceElement; java.lang.Throwable.getStackTrace()
meth public boolean java.lang.Object.equals(java.lang.Object)
meth public final java.lang.Class java.lang.Object.getClass()
meth public final void java.lang.Object.notify()
meth public final void java.lang.Object.notifyAll()
meth public final void java.lang.Object.wait() throws java.lang.InterruptedException
meth public final void java.lang.Object.wait(long) throws java.lang.InterruptedException
meth public final void java.lang.Object.wait(long,int) throws java.lang.InterruptedException
meth public int java.lang.Object.hashCode()
meth public java.lang.String java.lang.Throwable.getLocalizedMessage()
meth public java.lang.String java.lang.Throwable.getMessage()
meth public java.lang.String java.lang.Throwable.toString()
meth public java.lang.Throwable java.lang.Throwable.fillInStackTrace()
meth public java.lang.Throwable java.lang.Throwable.initCause(java.lang.Throwable)
meth public java.lang.Throwable javax.slee.SLEEException.getCause()
meth public void java.lang.Throwable.printStackTrace()
meth public void java.lang.Throwable.printStackTrace(java.io.PrintStream)
meth public void java.lang.Throwable.printStackTrace(java.io.PrintWriter)
meth public void java.lang.Throwable.setStackTrace([Ljava.lang.StackTraceElement;)
supr java.lang.Object java.lang.Throwable java.lang.Exception java.lang.RuntimeException javax.slee.SLEEException
CLSS public javax.slee.UnrecognizedActivityException
cons public javax.slee.UnrecognizedActivityException.UnrecognizedActivityException(java.lang.Object)
cons public javax.slee.UnrecognizedActivityException.UnrecognizedActivityException(java.lang.String,java.lang.Object)
intf java.io.Serializable
meth protected java.lang.Object java.lang.Object.clone() throws java.lang.CloneNotSupportedException
meth protected void java.lang.Object.finalize() throws java.lang.Throwable
meth public [Ljava.lang.StackTraceElement; java.lang.Throwable.getStackTrace()
meth public boolean java.lang.Object.equals(java.lang.Object)
meth public final java.lang.Class java.lang.Object.getClass()
meth public final java.lang.Object javax.slee.UnrecognizedActivityException.getActivity()
meth public final void java.lang.Object.notify()
meth public final void java.lang.Object.notifyAll()
meth public final void java.lang.Object.wait() throws java.lang.InterruptedException
meth public final void java.lang.Object.wait(long) throws java.lang.InterruptedException
meth public final void java.lang.Object.wait(long,int) throws java.lang.InterruptedException
meth public int java.lang.Object.hashCode()
meth public java.lang.String java.lang.Throwable.getLocalizedMessage()
meth public java.lang.String java.lang.Throwable.getMessage()
meth public java.lang.String java.lang.Throwable.toString()
meth public java.lang.Throwable java.lang.Throwable.fillInStackTrace()
meth public java.lang.Throwable java.lang.Throwable.initCause(java.lang.Throwable)
meth public java.lang.Throwable javax.slee.SLEEException.getCause()
meth public void java.lang.Throwable.printStackTrace()
meth public void java.lang.Throwable.printStackTrace(java.io.PrintStream)
meth public void java.lang.Throwable.printStackTrace(java.io.PrintWriter)
meth public void java.lang.Throwable.setStackTrace([Ljava.lang.StackTraceElement;)
supr java.lang.Object java.lang.Throwable java.lang.Exception java.lang.RuntimeException javax.slee.SLEEException
CLSS public javax.slee.UnrecognizedAlarmException
cons public javax.slee.UnrecognizedAlarmException.UnrecognizedAlarmException()
cons public javax.slee.UnrecognizedAlarmException.UnrecognizedAlarmException(java.lang.String)
intf java.io.Serializable
meth protected java.lang.Object java.lang.Object.clone() throws java.lang.CloneNotSupportedException
meth protected void java.lang.Object.finalize() throws java.lang.Throwable
meth public [Ljava.lang.StackTraceElement; java.lang.Throwable.getStackTrace()
meth public boolean java.lang.Object.equals(java.lang.Object)
meth public final java.lang.Class java.lang.Object.getClass()
meth public final void java.lang.Object.notify()
meth public final void java.lang.Object.notifyAll()
meth public final void java.lang.Object.wait() throws java.lang.InterruptedException
meth public final void java.lang.Object.wait(long) throws java.lang.InterruptedException
meth public final void java.lang.Object.wait(long,int) throws java.lang.InterruptedException
meth public int java.lang.Object.hashCode()
meth public java.lang.String java.lang.Throwable.getLocalizedMessage()
meth public java.lang.String java.lang.Throwable.getMessage()
meth public java.lang.String java.lang.Throwable.toString()
meth public java.lang.Throwable java.lang.Throwable.fillInStackTrace()
meth public java.lang.Throwable java.lang.Throwable.getCause()
meth public java.lang.Throwable java.lang.Throwable.initCause(java.lang.Throwable)
meth public void java.lang.Throwable.printStackTrace()
meth public void java.lang.Throwable.printStackTrace(java.io.PrintStream)
meth public void java.lang.Throwable.printStackTrace(java.io.PrintWriter)
meth public void java.lang.Throwable.setStackTrace([Ljava.lang.StackTraceElement;)
supr java.lang.Object java.lang.Throwable java.lang.Exception
CLSS public javax.slee.UnrecognizedComponentException
cons public javax.slee.UnrecognizedComponentException.UnrecognizedComponentException()
cons public javax.slee.UnrecognizedComponentException.UnrecognizedComponentException(java.lang.String)
intf java.io.Serializable
meth protected java.lang.Object java.lang.Object.clone() throws java.lang.CloneNotSupportedException
meth protected void java.lang.Object.finalize() throws java.lang.Throwable
meth public [Ljava.lang.StackTraceElement; java.lang.Throwable.getStackTrace()
meth public boolean java.lang.Object.equals(java.lang.Object)
meth public final java.lang.Class java.lang.Object.getClass()
meth public final void java.lang.Object.notify()
meth public final void java.lang.Object.notifyAll()
meth public final void java.lang.Object.wait() throws java.lang.InterruptedException
meth public final void java.lang.Object.wait(long) throws java.lang.InterruptedException
meth public final void java.lang.Object.wait(long,int) throws java.lang.InterruptedException
meth public int java.lang.Object.hashCode()
meth public java.lang.String java.lang.Throwable.getLocalizedMessage()
meth public java.lang.String java.lang.Throwable.getMessage()
meth public java.lang.String java.lang.Throwable.toString()
meth public java.lang.Throwable java.lang.Throwable.fillInStackTrace()
meth public java.lang.Throwable java.lang.Throwable.getCause()
meth public java.lang.Throwable java.lang.Throwable.initCause(java.lang.Throwable)
meth public void java.lang.Throwable.printStackTrace()
meth public void java.lang.Throwable.printStackTrace(java.io.PrintStream)
meth public void java.lang.Throwable.printStackTrace(java.io.PrintWriter)
meth public void java.lang.Throwable.setStackTrace([Ljava.lang.StackTraceElement;)
supr java.lang.Object java.lang.Throwable java.lang.Exception
CLSS public javax.slee.UnrecognizedEventException
cons public javax.slee.UnrecognizedEventException.UnrecognizedEventException()
cons public javax.slee.UnrecognizedEventException.UnrecognizedEventException(java.lang.String)
intf java.io.Serializable
meth protected java.lang.Object java.lang.Object.clone() throws java.lang.CloneNotSupportedException
meth protected void java.lang.Object.finalize() throws java.lang.Throwable
meth public [Ljava.lang.StackTraceElement; java.lang.Throwable.getStackTrace()
meth public boolean java.lang.Object.equals(java.lang.Object)
meth public final java.lang.Class java.lang.Object.getClass()
meth public final void java.lang.Object.notify()
meth public final void java.lang.Object.notifyAll()
meth public final void java.lang.Object.wait() throws java.lang.InterruptedException
meth public final void java.lang.Object.wait(long) throws java.lang.InterruptedException
meth public final void java.lang.Object.wait(long,int) throws java.lang.InterruptedException
meth public int java.lang.Object.hashCode()
meth public java.lang.String java.lang.Throwable.getLocalizedMessage()
meth public java.lang.String java.lang.Throwable.getMessage()
meth public java.lang.String java.lang.Throwable.toString()
meth public java.lang.Throwable java.lang.Throwable.fillInStackTrace()
meth public java.lang.Throwable java.lang.Throwable.getCause()
meth public java.lang.Throwable java.lang.Throwable.initCause(java.lang.Throwable)
meth public void java.lang.Throwable.printStackTrace()
meth public void java.lang.Throwable.printStackTrace(java.io.PrintStream)
meth public void java.lang.Throwable.printStackTrace(java.io.PrintWriter)
meth public void java.lang.Throwable.setStackTrace([Ljava.lang.StackTraceElement;)
supr java.lang.Object java.lang.Throwable java.lang.Exception javax.slee.UnrecognizedComponentException
CLSS public javax.slee.UnrecognizedSbbException
cons public javax.slee.UnrecognizedSbbException.UnrecognizedSbbException()
cons public javax.slee.UnrecognizedSbbException.UnrecognizedSbbException(java.lang.String)
intf java.io.Serializable
meth protected java.lang.Object java.lang.Object.clone() throws java.lang.CloneNotSupportedException
meth protected void java.lang.Object.finalize() throws java.lang.Throwable
meth public [Ljava.lang.StackTraceElement; java.lang.Throwable.getStackTrace()
meth public boolean java.lang.Object.equals(java.lang.Object)
meth public final java.lang.Class java.lang.Object.getClass()
meth public final void java.lang.Object.notify()
meth public final void java.lang.Object.notifyAll()
meth public final void java.lang.Object.wait() throws java.lang.InterruptedException
meth public final void java.lang.Object.wait(long) throws java.lang.InterruptedException
meth public final void java.lang.Object.wait(long,int) throws java.lang.InterruptedException
meth public int java.lang.Object.hashCode()
meth public java.lang.String java.lang.Throwable.getLocalizedMessage()
meth public java.lang.String java.lang.Throwable.getMessage()
meth public java.lang.String java.lang.Throwable.toString()
meth public java.lang.Throwable java.lang.Throwable.fillInStackTrace()
meth public java.lang.Throwable java.lang.Throwable.getCause()
meth public java.lang.Throwable java.lang.Throwable.initCause(java.lang.Throwable)
meth public void java.lang.Throwable.printStackTrace()
meth public void java.lang.Throwable.printStackTrace(java.io.PrintStream)
meth public void java.lang.Throwable.printStackTrace(java.io.PrintWriter)
meth public void java.lang.Throwable.setStackTrace([Ljava.lang.StackTraceElement;)
supr java.lang.Object java.lang.Throwable java.lang.Exception javax.slee.UnrecognizedComponentException
CLSS public javax.slee.UnrecognizedServiceException
cons public javax.slee.UnrecognizedServiceException.UnrecognizedServiceException()
cons public javax.slee.UnrecognizedServiceException.UnrecognizedServiceException(java.lang.String)
intf java.io.Serializable
meth protected java.lang.Object java.lang.Object.clone() throws java.lang.CloneNotSupportedException
meth protected void java.lang.Object.finalize() throws java.lang.Throwable
meth public [Ljava.lang.StackTraceElement; java.lang.Throwable.getStackTrace()
meth public boolean java.lang.Object.equals(java.lang.Object)
meth public final java.lang.Class java.lang.Object.getClass()
meth public final void java.lang.Object.notify()
meth public final void java.lang.Object.notifyAll()
meth public final void java.lang.Object.wait() throws java.lang.InterruptedException
meth public final void java.lang.Object.wait(long) throws java.lang.InterruptedException
meth public final void java.lang.Object.wait(long,int) throws java.lang.InterruptedException
meth public int java.lang.Object.hashCode()
meth public java.lang.String java.lang.Throwable.getLocalizedMessage()
meth public java.lang.String java.lang.Throwable.getMessage()
meth public java.lang.String java.lang.Throwable.toString()
meth public java.lang.Throwable java.lang.Throwable.fillInStackTrace()
meth public java.lang.Throwable java.lang.Throwable.getCause()
meth public java.lang.Throwable java.lang.Throwable.initCause(java.lang.Throwable)
meth public void java.lang.Throwable.printStackTrace()
meth public void java.lang.Throwable.printStackTrace(java.io.PrintStream)
meth public void java.lang.Throwable.printStackTrace(java.io.PrintWriter)
meth public void java.lang.Throwable.setStackTrace([Ljava.lang.StackTraceElement;)
supr java.lang.Object java.lang.Throwable java.lang.Exception javax.slee.UnrecognizedComponentException
CLSS public abstract interface javax.slee.facilities.ActivityContextNamingFacility
fld  public static final java.lang.String javax.slee.facilities.ActivityContextNamingFacility.JNDI_NAME
meth public abstract javax.slee.ActivityContextInterface javax.slee.facilities.ActivityContextNamingFacility.lookup(java.lang.String)
meth public abstract void javax.slee.facilities.ActivityContextNamingFacility.bind(javax.slee.ActivityContextInterface,java.lang.String) throws javax.slee.facilities.NameAlreadyBoundException
meth public abstract void javax.slee.facilities.ActivityContextNamingFacility.unbind(java.lang.String) throws javax.slee.facilities.NameNotBoundException
supr null
CLSS public abstract interface javax.slee.facilities.AlarmFacility
fld  public static final java.lang.String javax.slee.facilities.AlarmFacility.JNDI_NAME
meth public abstract boolean javax.slee.facilities.AlarmFacility.clearAlarm(java.lang.String)
meth public abstract int javax.slee.facilities.AlarmFacility.clearAlarms()
meth public abstract int javax.slee.facilities.AlarmFacility.clearAlarms(java.lang.String)
meth public abstract java.lang.String javax.slee.facilities.AlarmFacility.raiseAlarm(java.lang.String,java.lang.String,javax.slee.facilities.AlarmLevel,java.lang.String)
meth public abstract java.lang.String javax.slee.facilities.AlarmFacility.raiseAlarm(java.lang.String,java.lang.String,javax.slee.facilities.AlarmLevel,java.lang.String,java.lang.Throwable)
meth public abstract void javax.slee.facilities.AlarmFacility.createAlarm(javax.slee.ComponentID,javax.slee.facilities.Level,java.lang.String,java.lang.String,java.lang.Throwable,long) throws javax.slee.UnrecognizedComponentException
meth public abstract void javax.slee.facilities.AlarmFacility.createAlarm(javax.slee.ComponentID,javax.slee.facilities.Level,java.lang.String,java.lang.String,long) throws javax.slee.UnrecognizedComponentException
supr null
CLSS public final javax.slee.facilities.AlarmLevel
fld  public static final int javax.slee.facilities.AlarmLevel.LEVEL_CLEAR
fld  public static final int javax.slee.facilities.AlarmLevel.LEVEL_CRITICAL
fld  public static final int javax.slee.facilities.AlarmLevel.LEVEL_INDETERMINATE
fld  public static final int javax.slee.facilities.AlarmLevel.LEVEL_MAJOR
fld  public static final int javax.slee.facilities.AlarmLevel.LEVEL_MINOR
fld  public static final int javax.slee.facilities.AlarmLevel.LEVEL_WARNING
fld  public static final java.lang.String javax.slee.facilities.AlarmLevel.CLEAR_STRING
fld  public static final java.lang.String javax.slee.facilities.AlarmLevel.CRITICAL_STRING
fld  public static final java.lang.String javax.slee.facilities.AlarmLevel.INDETERMINATE_STRING
fld  public static final java.lang.String javax.slee.facilities.AlarmLevel.MAJOR_STRING
fld  public static final java.lang.String javax.slee.facilities.AlarmLevel.MINOR_STRING
fld  public static final java.lang.String javax.slee.facilities.AlarmLevel.WARNING_STRING
fld  public static final javax.slee.facilities.AlarmLevel javax.slee.facilities.AlarmLevel.CLEAR
fld  public static final javax.slee.facilities.AlarmLevel javax.slee.facilities.AlarmLevel.CRITICAL
fld  public static final javax.slee.facilities.AlarmLevel javax.slee.facilities.AlarmLevel.INDETERMINATE
fld  public static final javax.slee.facilities.AlarmLevel javax.slee.facilities.AlarmLevel.MAJOR
fld  public static final javax.slee.facilities.AlarmLevel javax.slee.facilities.AlarmLevel.MINOR
fld  public static final javax.slee.facilities.AlarmLevel javax.slee.facilities.AlarmLevel.WARNING
intf java.io.Serializable
meth protected java.lang.Object java.lang.Object.clone() throws java.lang.CloneNotSupportedException
meth protected void java.lang.Object.finalize() throws java.lang.Throwable
meth public boolean javax.slee.facilities.AlarmLevel.equals(java.lang.Object)
meth public boolean javax.slee.facilities.AlarmLevel.isClear()
meth public boolean javax.slee.facilities.AlarmLevel.isCritical()
meth public boolean javax.slee.facilities.AlarmLevel.isHigherLevel(javax.slee.facilities.AlarmLevel)
meth public boolean javax.slee.facilities.AlarmLevel.isIndeterminate()
meth public boolean javax.slee.facilities.AlarmLevel.isMajor()
meth public boolean javax.slee.facilities.AlarmLevel.isMinor()
meth public boolean javax.slee.facilities.AlarmLevel.isWarning()
meth public final java.lang.Class java.lang.Object.getClass()
meth public final void java.lang.Object.notify()
meth public final void java.lang.Object.notifyAll()
meth public final void java.lang.Object.wait() throws java.lang.InterruptedException
meth public final void java.lang.Object.wait(long) throws java.lang.InterruptedException
meth public final void java.lang.Object.wait(long,int) throws java.lang.InterruptedException
meth public int javax.slee.facilities.AlarmLevel.hashCode()
meth public int javax.slee.facilities.AlarmLevel.toInt()
meth public java.lang.String javax.slee.facilities.AlarmLevel.toString()
meth public static javax.slee.facilities.AlarmLevel javax.slee.facilities.AlarmLevel.fromInt(int)
meth public static javax.slee.facilities.AlarmLevel javax.slee.facilities.AlarmLevel.fromString(java.lang.String)
supr java.lang.Object
CLSS public abstract interface javax.slee.facilities.EventLookupFacility
meth public abstract javax.slee.resource.FireableEventType javax.slee.facilities.EventLookupFacility.getFireableEventType(javax.slee.EventTypeID) throws javax.slee.UnrecognizedEventException
supr null
CLSS public javax.slee.facilities.FacilityException
cons public javax.slee.facilities.FacilityException.FacilityException(java.lang.String)
cons public javax.slee.facilities.FacilityException.FacilityException(java.lang.String,java.lang.Throwable)
intf java.io.Serializable
meth protected java.lang.Object java.lang.Object.clone() throws java.lang.CloneNotSupportedException
meth protected void java.lang.Object.finalize() throws java.lang.Throwable
meth public [Ljava.lang.StackTraceElement; java.lang.Throwable.getStackTrace()
meth public boolean java.lang.Object.equals(java.lang.Object)
meth public final java.lang.Class java.lang.Object.getClass()
meth public final void java.lang.Object.notify()
meth public final void java.lang.Object.notifyAll()
meth public final void java.lang.Object.wait() throws java.lang.InterruptedException
meth public final void java.lang.Object.wait(long) throws java.lang.InterruptedException
meth public final void java.lang.Object.wait(long,int) throws java.lang.InterruptedException
meth public int java.lang.Object.hashCode()
meth public java.lang.String java.lang.Throwable.getLocalizedMessage()
meth public java.lang.String java.lang.Throwable.getMessage()
meth public java.lang.String java.lang.Throwable.toString()
meth public java.lang.Throwable java.lang.Throwable.fillInStackTrace()
meth public java.lang.Throwable java.lang.Throwable.initCause(java.lang.Throwable)
meth public java.lang.Throwable javax.slee.SLEEException.getCause()
meth public void java.lang.Throwable.printStackTrace()
meth public void java.lang.Throwable.printStackTrace(java.io.PrintStream)
meth public void java.lang.Throwable.printStackTrace(java.io.PrintWriter)
meth public void java.lang.Throwable.setStackTrace([Ljava.lang.StackTraceElement;)
supr java.lang.Object java.lang.Throwable java.lang.Exception java.lang.RuntimeException javax.slee.SLEEException
CLSS public final javax.slee.facilities.Level
fld  public static final int javax.slee.facilities.Level.LEVEL_CONFIG
fld  public static final int javax.slee.facilities.Level.LEVEL_FINE
fld  public static final int javax.slee.facilities.Level.LEVEL_FINER
fld  public static final int javax.slee.facilities.Level.LEVEL_FINEST
fld  public static final int javax.slee.facilities.Level.LEVEL_INFO
fld  public static final int javax.slee.facilities.Level.LEVEL_OFF
fld  public static final int javax.slee.facilities.Level.LEVEL_SEVERE
fld  public static final int javax.slee.facilities.Level.LEVEL_WARNING
fld  public static final javax.slee.facilities.Level javax.slee.facilities.Level.CONFIG
fld  public static final javax.slee.facilities.Level javax.slee.facilities.Level.FINE
fld  public static final javax.slee.facilities.Level javax.slee.facilities.Level.FINER
fld  public static final javax.slee.facilities.Level javax.slee.facilities.Level.FINEST
fld  public static final javax.slee.facilities.Level javax.slee.facilities.Level.INFO
fld  public static final javax.slee.facilities.Level javax.slee.facilities.Level.OFF
fld  public static final javax.slee.facilities.Level javax.slee.facilities.Level.SEVERE
fld  public static final javax.slee.facilities.Level javax.slee.facilities.Level.WARNING
intf java.io.Serializable
meth protected java.lang.Object java.lang.Object.clone() throws java.lang.CloneNotSupportedException
meth protected void java.lang.Object.finalize() throws java.lang.Throwable
meth public boolean javax.slee.facilities.Level.equals(java.lang.Object)
meth public boolean javax.slee.facilities.Level.isConfig()
meth public boolean javax.slee.facilities.Level.isFine()
meth public boolean javax.slee.facilities.Level.isFiner()
meth public boolean javax.slee.facilities.Level.isFinest()
meth public boolean javax.slee.facilities.Level.isHigherLevel(javax.slee.facilities.Level)
meth public boolean javax.slee.facilities.Level.isMinor()
meth public boolean javax.slee.facilities.Level.isOff()
meth public boolean javax.slee.facilities.Level.isSevere()
meth public boolean javax.slee.facilities.Level.isWarning()
meth public final java.lang.Class java.lang.Object.getClass()
meth public final void java.lang.Object.notify()
meth public final void java.lang.Object.notifyAll()
meth public final void java.lang.Object.wait() throws java.lang.InterruptedException
meth public final void java.lang.Object.wait(long) throws java.lang.InterruptedException
meth public final void java.lang.Object.wait(long,int) throws java.lang.InterruptedException
meth public int javax.slee.facilities.Level.hashCode()
meth public int javax.slee.facilities.Level.toInt()
meth public java.lang.String javax.slee.facilities.Level.toString()
meth public static javax.slee.facilities.Level javax.slee.facilities.Level.fromInt(int)
supr java.lang.Object
CLSS public javax.slee.facilities.NameAlreadyBoundException
cons public javax.slee.facilities.NameAlreadyBoundException.NameAlreadyBoundException(java.lang.String)
intf java.io.Serializable
meth protected java.lang.Object java.lang.Object.clone() throws java.lang.CloneNotSupportedException
meth protected void java.lang.Object.finalize() throws java.lang.Throwable
meth public [Ljava.lang.StackTraceElement; java.lang.Throwable.getStackTrace()
meth public boolean java.lang.Object.equals(java.lang.Object)
meth public final java.lang.Class java.lang.Object.getClass()
meth public final void java.lang.Object.notify()
meth public final void java.lang.Object.notifyAll()
meth public final void java.lang.Object.wait() throws java.lang.InterruptedException
meth public final void java.lang.Object.wait(long) throws java.lang.InterruptedException
meth public final void java.lang.Object.wait(long,int) throws java.lang.InterruptedException
meth public int java.lang.Object.hashCode()
meth public java.lang.String java.lang.Throwable.getLocalizedMessage()
meth public java.lang.String java.lang.Throwable.getMessage()
meth public java.lang.String java.lang.Throwable.toString()
meth public java.lang.Throwable java.lang.Throwable.fillInStackTrace()
meth public java.lang.Throwable java.lang.Throwable.getCause()
meth public java.lang.Throwable java.lang.Throwable.initCause(java.lang.Throwable)
meth public void java.lang.Throwable.printStackTrace()
meth public void java.lang.Throwable.printStackTrace(java.io.PrintStream)
meth public void java.lang.Throwable.printStackTrace(java.io.PrintWriter)
meth public void java.lang.Throwable.setStackTrace([Ljava.lang.StackTraceElement;)
supr java.lang.Object java.lang.Throwable java.lang.Exception
CLSS public javax.slee.facilities.NameNotBoundException
cons public javax.slee.facilities.NameNotBoundException.NameNotBoundException(java.lang.String)
intf java.io.Serializable
meth protected java.lang.Object java.lang.Object.clone() throws java.lang.CloneNotSupportedException
meth protected void java.lang.Object.finalize() throws java.lang.Throwable
meth public [Ljava.lang.StackTraceElement; java.lang.Throwable.getStackTrace()
meth public boolean java.lang.Object.equals(java.lang.Object)
meth public final java.lang.Class java.lang.Object.getClass()
meth public final void java.lang.Object.notify()
meth public final void java.lang.Object.notifyAll()
meth public final void java.lang.Object.wait() throws java.lang.InterruptedException
meth public final void java.lang.Object.wait(long) throws java.lang.InterruptedException
meth public final void java.lang.Object.wait(long,int) throws java.lang.InterruptedException
meth public int java.lang.Object.hashCode()
meth public java.lang.String java.lang.Throwable.getLocalizedMessage()
meth public java.lang.String java.lang.Throwable.getMessage()
meth public java.lang.String java.lang.Throwable.toString()
meth public java.lang.Throwable java.lang.Throwable.fillInStackTrace()
meth public java.lang.Throwable java.lang.Throwable.getCause()
meth public java.lang.Throwable java.lang.Throwable.initCause(java.lang.Throwable)
meth public void java.lang.Throwable.printStackTrace()
meth public void java.lang.Throwable.printStackTrace(java.io.PrintStream)
meth public void java.lang.Throwable.printStackTrace(java.io.PrintWriter)
meth public void java.lang.Throwable.setStackTrace([Ljava.lang.StackTraceElement;)
supr java.lang.Object java.lang.Throwable java.lang.Exception
CLSS public abstract interface javax.slee.facilities.ServiceLookupFacility
meth public abstract javax.slee.resource.ReceivableService javax.slee.facilities.ServiceLookupFacility.getReceivableService(javax.slee.ServiceID) throws javax.slee.UnrecognizedServiceException
supr null
CLSS public abstract interface javax.slee.facilities.TimerEvent
meth public abstract int javax.slee.facilities.TimerEvent.getMissedRepetitions()
meth public abstract int javax.slee.facilities.TimerEvent.getNumRepetitions()
meth public abstract int javax.slee.facilities.TimerEvent.getRemainingRepetitions()
meth public abstract javax.slee.facilities.TimerID javax.slee.facilities.TimerEvent.getTimerID()
meth public abstract long javax.slee.facilities.TimerEvent.getExpiryTime()
meth public abstract long javax.slee.facilities.TimerEvent.getPeriod()
meth public abstract long javax.slee.facilities.TimerEvent.getScheduledTime()
supr null
CLSS public abstract interface javax.slee.facilities.TimerFacility
fld  public static final java.lang.String javax.slee.facilities.TimerFacility.JNDI_NAME
meth public abstract javax.slee.ActivityContextInterface javax.slee.facilities.TimerFacility.getActivityContextInterface(javax.slee.facilities.TimerID)
meth public abstract javax.slee.facilities.TimerID javax.slee.facilities.TimerFacility.setTimer(javax.slee.ActivityContextInterface,javax.slee.Address,long,javax.slee.facilities.TimerOptions)
meth public abstract javax.slee.facilities.TimerID javax.slee.facilities.TimerFacility.setTimer(javax.slee.ActivityContextInterface,javax.slee.Address,long,long,int,javax.slee.facilities.TimerOptions)
meth public abstract long javax.slee.facilities.TimerFacility.getDefaultTimeout()
meth public abstract long javax.slee.facilities.TimerFacility.getResolution()
meth public abstract void javax.slee.facilities.TimerFacility.cancelTimer(javax.slee.facilities.TimerID)
supr null
CLSS public abstract interface javax.slee.facilities.TimerID
intf java.io.Serializable
meth public abstract boolean javax.slee.facilities.TimerID.equals(java.lang.Object)
meth public abstract int javax.slee.facilities.TimerID.hashCode()
meth public abstract java.lang.String javax.slee.facilities.TimerID.toString()
supr null
CLSS public javax.slee.facilities.TimerOptions
cons public javax.slee.facilities.TimerOptions.TimerOptions()
cons public javax.slee.facilities.TimerOptions.TimerOptions(boolean,long,javax.slee.facilities.TimerPreserveMissed)
cons public javax.slee.facilities.TimerOptions.TimerOptions(javax.slee.facilities.TimerOptions)
cons public javax.slee.facilities.TimerOptions.TimerOptions(long,javax.slee.facilities.TimerPreserveMissed)
intf java.io.Serializable
meth protected java.lang.Object java.lang.Object.clone() throws java.lang.CloneNotSupportedException
meth protected void java.lang.Object.finalize() throws java.lang.Throwable
meth public boolean javax.slee.facilities.TimerOptions.equals(java.lang.Object)
meth public final boolean javax.slee.facilities.TimerOptions.isPersistent()
meth public final java.lang.Class java.lang.Object.getClass()
meth public final javax.slee.facilities.TimerPreserveMissed javax.slee.facilities.TimerOptions.getPreserveMissed()
meth public final long javax.slee.facilities.TimerOptions.getTimeout()
meth public final void java.lang.Object.notify()
meth public final void java.lang.Object.notifyAll()
meth public final void java.lang.Object.wait() throws java.lang.InterruptedException
meth public final void java.lang.Object.wait(long) throws java.lang.InterruptedException
meth public final void java.lang.Object.wait(long,int) throws java.lang.InterruptedException
meth public final void javax.slee.facilities.TimerOptions.setPersistent(boolean)
meth public final void javax.slee.facilities.TimerOptions.setPreserveMissed(javax.slee.facilities.TimerPreserveMissed)
meth public final void javax.slee.facilities.TimerOptions.setTimeout(long)
meth public int javax.slee.facilities.TimerOptions.hashCode()
meth public java.lang.String javax.slee.facilities.TimerOptions.toString()
supr java.lang.Object
CLSS public final javax.slee.facilities.TimerPreserveMissed
fld  public static final int javax.slee.facilities.TimerPreserveMissed.PRESERVE_ALL
fld  public static final int javax.slee.facilities.TimerPreserveMissed.PRESERVE_LAST
fld  public static final int javax.slee.facilities.TimerPreserveMissed.PRESERVE_NONE
fld  public static final java.lang.String javax.slee.facilities.TimerPreserveMissed.ALL_STRING
fld  public static final java.lang.String javax.slee.facilities.TimerPreserveMissed.LAST_STRING
fld  public static final java.lang.String javax.slee.facilities.TimerPreserveMissed.NONE_STRING
fld  public static final javax.slee.facilities.TimerPreserveMissed javax.slee.facilities.TimerPreserveMissed.ALL
fld  public static final javax.slee.facilities.TimerPreserveMissed javax.slee.facilities.TimerPreserveMissed.LAST
fld  public static final javax.slee.facilities.TimerPreserveMissed javax.slee.facilities.TimerPreserveMissed.NONE
intf java.io.Serializable
meth protected java.lang.Object java.lang.Object.clone() throws java.lang.CloneNotSupportedException
meth protected void java.lang.Object.finalize() throws java.lang.Throwable
meth public boolean javax.slee.facilities.TimerPreserveMissed.equals(java.lang.Object)
meth public boolean javax.slee.facilities.TimerPreserveMissed.isAll()
meth public boolean javax.slee.facilities.TimerPreserveMissed.isLast()
meth public boolean javax.slee.facilities.TimerPreserveMissed.isNone()
meth public final java.lang.Class java.lang.Object.getClass()
meth public final void java.lang.Object.notify()
meth public final void java.lang.Object.notifyAll()
meth public final void java.lang.Object.wait() throws java.lang.InterruptedException
meth public final void java.lang.Object.wait(long) throws java.lang.InterruptedException
meth public final void java.lang.Object.wait(long,int) throws java.lang.InterruptedException
meth public int javax.slee.facilities.TimerPreserveMissed.hashCode()
meth public int javax.slee.facilities.TimerPreserveMissed.toInt()
meth public java.lang.String javax.slee.facilities.TimerPreserveMissed.toString()
meth public static javax.slee.facilities.TimerPreserveMissed javax.slee.facilities.TimerPreserveMissed.fromInt(int)
meth public static javax.slee.facilities.TimerPreserveMissed javax.slee.facilities.TimerPreserveMissed.fromString(java.lang.String)
supr java.lang.Object
CLSS public abstract interface javax.slee.facilities.TraceFacility
meth public abstract javax.slee.facilities.Level javax.slee.facilities.TraceFacility.getTraceLevel(javax.slee.ComponentID) throws javax.slee.UnrecognizedComponentException
meth public abstract void javax.slee.facilities.TraceFacility.createTrace(javax.slee.ComponentID,javax.slee.facilities.Level,java.lang.String,java.lang.String,java.lang.Throwable,long) throws javax.slee.UnrecognizedComponentException
meth public abstract void javax.slee.facilities.TraceFacility.createTrace(javax.slee.ComponentID,javax.slee.facilities.Level,java.lang.String,java.lang.String,long) throws javax.slee.UnrecognizedComponentException
supr null
CLSS public final javax.slee.facilities.TraceLevel
fld  public static final int javax.slee.facilities.TraceLevel.LEVEL_CONFIG
fld  public static final int javax.slee.facilities.TraceLevel.LEVEL_FINE
fld  public static final int javax.slee.facilities.TraceLevel.LEVEL_FINER
fld  public static final int javax.slee.facilities.TraceLevel.LEVEL_FINEST
fld  public static final int javax.slee.facilities.TraceLevel.LEVEL_INFO
fld  public static final int javax.slee.facilities.TraceLevel.LEVEL_OFF
fld  public static final int javax.slee.facilities.TraceLevel.LEVEL_SEVERE
fld  public static final int javax.slee.facilities.TraceLevel.LEVEL_WARNING
fld  public static final java.lang.String javax.slee.facilities.TraceLevel.CONFIG_STRING
fld  public static final java.lang.String javax.slee.facilities.TraceLevel.FINER_STRING
fld  public static final java.lang.String javax.slee.facilities.TraceLevel.FINEST_STRING
fld  public static final java.lang.String javax.slee.facilities.TraceLevel.FINE_STRING
fld  public static final java.lang.String javax.slee.facilities.TraceLevel.INFO_STRING
fld  public static final java.lang.String javax.slee.facilities.TraceLevel.OFF_STRING
fld  public static final java.lang.String javax.slee.facilities.TraceLevel.SEVERE_STRING
fld  public static final java.lang.String javax.slee.facilities.TraceLevel.WARNING_STRING
fld  public static final javax.slee.facilities.TraceLevel javax.slee.facilities.TraceLevel.CONFIG
fld  public static final javax.slee.facilities.TraceLevel javax.slee.facilities.TraceLevel.FINE
fld  public static final javax.slee.facilities.TraceLevel javax.slee.facilities.TraceLevel.FINER
fld  public static final javax.slee.facilities.TraceLevel javax.slee.facilities.TraceLevel.FINEST
fld  public static final javax.slee.facilities.TraceLevel javax.slee.facilities.TraceLevel.INFO
fld  public static final javax.slee.facilities.TraceLevel javax.slee.facilities.TraceLevel.OFF
fld  public static final javax.slee.facilities.TraceLevel javax.slee.facilities.TraceLevel.SEVERE
fld  public static final javax.slee.facilities.TraceLevel javax.slee.facilities.TraceLevel.WARNING
intf java.io.Serializable
meth protected java.lang.Object java.lang.Object.clone() throws java.lang.CloneNotSupportedException
meth protected void java.lang.Object.finalize() throws java.lang.Throwable
meth public boolean javax.slee.facilities.TraceLevel.equals(java.lang.Object)
meth public boolean javax.slee.facilities.TraceLevel.isConfig()
meth public boolean javax.slee.facilities.TraceLevel.isFine()
meth public boolean javax.slee.facilities.TraceLevel.isFiner()
meth public boolean javax.slee.facilities.TraceLevel.isFinest()
meth public boolean javax.slee.facilities.TraceLevel.isHigherLevel(javax.slee.facilities.TraceLevel)
meth public boolean javax.slee.facilities.TraceLevel.isInfo()
meth public boolean javax.slee.facilities.TraceLevel.isOff()
meth public boolean javax.slee.facilities.TraceLevel.isSevere()
meth public boolean javax.slee.facilities.TraceLevel.isWarning()
meth public final java.lang.Class java.lang.Object.getClass()
meth public final void java.lang.Object.notify()
meth public final void java.lang.Object.notifyAll()
meth public final void java.lang.Object.wait() throws java.lang.InterruptedException
meth public final void java.lang.Object.wait(long) throws java.lang.InterruptedException
meth public final void java.lang.Object.wait(long,int) throws java.lang.InterruptedException
meth public int javax.slee.facilities.TraceLevel.hashCode()
meth public int javax.slee.facilities.TraceLevel.toInt()
meth public java.lang.String javax.slee.facilities.TraceLevel.toString()
meth public static javax.slee.facilities.TraceLevel javax.slee.facilities.TraceLevel.fromInt(int)
meth public static javax.slee.facilities.TraceLevel javax.slee.facilities.TraceLevel.fromString(java.lang.String)
supr java.lang.Object
CLSS public abstract interface javax.slee.facilities.Tracer
meth public abstract boolean javax.slee.facilities.Tracer.isConfigEnabled()
meth public abstract boolean javax.slee.facilities.Tracer.isFineEnabled()
meth public abstract boolean javax.slee.facilities.Tracer.isFinerEnabled()
meth public abstract boolean javax.slee.facilities.Tracer.isFinestEnabled()
meth public abstract boolean javax.slee.facilities.Tracer.isInfoEnabled()
meth public abstract boolean javax.slee.facilities.Tracer.isSevereEnabled()
meth public abstract boolean javax.slee.facilities.Tracer.isTraceable(javax.slee.facilities.TraceLevel)
meth public abstract boolean javax.slee.facilities.Tracer.isWarningEnabled()
meth public abstract java.lang.String javax.slee.facilities.Tracer.getParentTracerName()
meth public abstract java.lang.String javax.slee.facilities.Tracer.getTracerName()
meth public abstract javax.slee.facilities.TraceLevel javax.slee.facilities.Tracer.getTraceLevel()
meth public abstract void javax.slee.facilities.Tracer.config(java.lang.String)
meth public abstract void javax.slee.facilities.Tracer.config(java.lang.String,java.lang.Throwable)
meth public abstract void javax.slee.facilities.Tracer.fine(java.lang.String)
meth public abstract void javax.slee.facilities.Tracer.fine(java.lang.String,java.lang.Throwable)
meth public abstract void javax.slee.facilities.Tracer.finer(java.lang.String)
meth public abstract void javax.slee.facilities.Tracer.finer(java.lang.String,java.lang.Throwable)
meth public abstract void javax.slee.facilities.Tracer.finest(java.lang.String)
meth public abstract void javax.slee.facilities.Tracer.finest(java.lang.String,java.lang.Throwable)
meth public abstract void javax.slee.facilities.Tracer.info(java.lang.String)
meth public abstract void javax.slee.facilities.Tracer.info(java.lang.String,java.lang.Throwable)
meth public abstract void javax.slee.facilities.Tracer.severe(java.lang.String)
meth public abstract void javax.slee.facilities.Tracer.severe(java.lang.String,java.lang.Throwable)
meth public abstract void javax.slee.facilities.Tracer.trace(javax.slee.facilities.TraceLevel,java.lang.String)
meth public abstract void javax.slee.facilities.Tracer.trace(javax.slee.facilities.TraceLevel,java.lang.String,java.lang.Throwable)
meth public abstract void javax.slee.facilities.Tracer.warning(java.lang.String)
meth public abstract void javax.slee.facilities.Tracer.warning(java.lang.String,java.lang.Throwable)
supr null
CLSS public final javax.slee.management.Alarm
cons public javax.slee.management.Alarm.Alarm(java.lang.String,javax.slee.management.NotificationSource,java.lang.String,java.lang.String,javax.slee.facilities.AlarmLevel,java.lang.String,java.lang.Throwable,long)
intf java.io.Serializable
intf java.lang.Comparable
intf javax.slee.management.VendorExtensions
meth protected java.lang.Object java.lang.Object.clone() throws java.lang.CloneNotSupportedException
meth protected void java.lang.Object.finalize() throws java.lang.Throwable
meth public boolean javax.slee.management.Alarm.equals(java.lang.Object)
meth public final java.lang.Class java.lang.Object.getClass()
meth public final void java.lang.Object.notify()
meth public final void java.lang.Object.notifyAll()
meth public final void java.lang.Object.wait() throws java.lang.InterruptedException
meth public final void java.lang.Object.wait(long) throws java.lang.InterruptedException
meth public final void java.lang.Object.wait(long,int) throws java.lang.InterruptedException
meth public int javax.slee.management.Alarm.compareTo(java.lang.Object)
meth public int javax.slee.management.Alarm.hashCode()
meth public java.lang.Object javax.slee.management.Alarm.getVendorData()
meth public java.lang.String javax.slee.management.Alarm.getAlarmID()
meth public java.lang.String javax.slee.management.Alarm.getAlarmType()
meth public java.lang.String javax.slee.management.Alarm.getInstanceID()
meth public java.lang.String javax.slee.management.Alarm.getMessage()
meth public java.lang.String javax.slee.management.Alarm.toString()
meth public java.lang.Throwable javax.slee.management.Alarm.getCause()
meth public javax.slee.facilities.AlarmLevel javax.slee.management.Alarm.getAlarmLevel()
meth public javax.slee.management.NotificationSource javax.slee.management.Alarm.getNotificationSource()
meth public long javax.slee.management.Alarm.getTimestamp()
meth public static void javax.slee.management.Alarm.disableVendorDataDeserialization()
meth public static void javax.slee.management.Alarm.disableVendorDataSerialization()
meth public static void javax.slee.management.Alarm.enableVendorDataDeserialization()
meth public static void javax.slee.management.Alarm.enableVendorDataSerialization()
meth public void javax.slee.management.Alarm.setVendorData(java.lang.Object)
supr java.lang.Object
CLSS public javax.slee.management.AlarmDuplicateFilter
cons public javax.slee.management.AlarmDuplicateFilter.AlarmDuplicateFilter(long)
intf java.io.Serializable
intf javax.management.NotificationFilter
meth protected java.lang.Object java.lang.Object.clone() throws java.lang.CloneNotSupportedException
meth protected void java.lang.Object.finalize() throws java.lang.Throwable
meth public boolean java.lang.Object.equals(java.lang.Object)
meth public boolean javax.slee.management.AlarmDuplicateFilter.isNotificationEnabled(javax.management.Notification)
meth public final java.lang.Class java.lang.Object.getClass()
meth public final void java.lang.Object.notify()
meth public final void java.lang.Object.notifyAll()
meth public final void java.lang.Object.wait() throws java.lang.InterruptedException
meth public final void java.lang.Object.wait(long) throws java.lang.InterruptedException
meth public final void java.lang.Object.wait(long,int) throws java.lang.InterruptedException
meth public int java.lang.Object.hashCode()
meth public java.lang.String java.lang.Object.toString()
supr java.lang.Object
CLSS public javax.slee.management.AlarmLevelFilter
cons public javax.slee.management.AlarmLevelFilter.AlarmLevelFilter(javax.slee.facilities.AlarmLevel)
cons public javax.slee.management.AlarmLevelFilter.AlarmLevelFilter(javax.slee.facilities.Level)
intf java.io.Serializable
intf javax.management.NotificationFilter
meth protected java.lang.Object java.lang.Object.clone() throws java.lang.CloneNotSupportedException
meth protected void java.lang.Object.finalize() throws java.lang.Throwable
meth public boolean java.lang.Object.equals(java.lang.Object)
meth public boolean javax.slee.management.AlarmLevelFilter.isNotificationEnabled(javax.management.Notification)
meth public final java.lang.Class java.lang.Object.getClass()
meth public final void java.lang.Object.notify()
meth public final void java.lang.Object.notifyAll()
meth public final void java.lang.Object.wait() throws java.lang.InterruptedException
meth public final void java.lang.Object.wait(long) throws java.lang.InterruptedException
meth public final void java.lang.Object.wait(long,int) throws java.lang.InterruptedException
meth public int java.lang.Object.hashCode()
meth public java.lang.String java.lang.Object.toString()
supr java.lang.Object
CLSS public abstract interface javax.slee.management.AlarmMBean
fld  public static final java.lang.String javax.slee.management.AlarmMBean.ALARM_NOTIFICATION_TYPE
fld  public static final java.lang.String javax.slee.management.AlarmMBean.OBJECT_NAME
meth public abstract [Ljava.lang.String; javax.slee.management.AlarmMBean.getAlarms() throws javax.slee.management.ManagementException
meth public abstract [Ljava.lang.String; javax.slee.management.AlarmMBean.getAlarms(javax.slee.management.NotificationSource) throws javax.slee.management.ManagementException,javax.slee.management.UnrecognizedNotificationSourceException
meth public abstract [Ljavax.slee.management.Alarm; javax.slee.management.AlarmMBean.getDescriptors([Ljava.lang.String;) throws javax.slee.management.ManagementException
meth public abstract boolean javax.slee.management.AlarmMBean.clearAlarm(java.lang.String) throws javax.slee.management.ManagementException
meth public abstract boolean javax.slee.management.AlarmMBean.isActive(java.lang.String) throws javax.slee.management.ManagementException
meth public abstract int javax.slee.management.AlarmMBean.clearAlarms(javax.slee.management.NotificationSource) throws javax.slee.management.ManagementException,javax.slee.management.UnrecognizedNotificationSourceException
meth public abstract int javax.slee.management.AlarmMBean.clearAlarms(javax.slee.management.NotificationSource,java.lang.String) throws javax.slee.management.ManagementException,javax.slee.management.UnrecognizedNotificationSourceException
meth public abstract javax.slee.management.Alarm javax.slee.management.AlarmMBean.getDescriptor(java.lang.String) throws javax.slee.management.ManagementException
supr null
CLSS public final javax.slee.management.AlarmNotification
cons public javax.slee.management.AlarmNotification.AlarmNotification(java.lang.String,javax.slee.management.AlarmMBean,java.lang.String,javax.slee.management.NotificationSource,java.lang.String,java.lang.String,javax.slee.facilities.AlarmLevel,java.lang.String,java.lang.Throwable,long,long)
cons public javax.slee.management.AlarmNotification.AlarmNotification(javax.slee.management.AlarmMBean,java.lang.String,java.lang.Object,javax.slee.facilities.Level,java.lang.String,java.lang.Throwable,long,long)
fld  protected java.lang.Object javax.management.Notification.source
intf java.io.Serializable
intf javax.slee.management.VendorExtensions
meth protected java.lang.Object java.lang.Object.clone() throws java.lang.CloneNotSupportedException
meth protected void java.lang.Object.finalize() throws java.lang.Throwable
meth public boolean javax.slee.management.AlarmNotification.equals(java.lang.Object)
meth public final java.lang.Class java.lang.Object.getClass()
meth public final void java.lang.Object.notify()
meth public final void java.lang.Object.notifyAll()
meth public final void java.lang.Object.wait() throws java.lang.InterruptedException
meth public final void java.lang.Object.wait(long) throws java.lang.InterruptedException
meth public final void java.lang.Object.wait(long,int) throws java.lang.InterruptedException
meth public int javax.slee.management.AlarmNotification.hashCode()
meth public java.lang.Object java.util.EventObject.getSource()
meth public java.lang.Object javax.management.Notification.getUserData()
meth public java.lang.Object javax.slee.management.AlarmNotification.getAlarmSource()
meth public java.lang.Object javax.slee.management.AlarmNotification.getVendorData()
meth public java.lang.String javax.management.Notification.getMessage()
meth public java.lang.String javax.management.Notification.getType()
meth public java.lang.String javax.slee.management.AlarmNotification.getAlarmID()
meth public java.lang.String javax.slee.management.AlarmNotification.getAlarmType()
meth public java.lang.String javax.slee.management.AlarmNotification.getInstanceID()
meth public java.lang.String javax.slee.management.AlarmNotification.toString()
meth public java.lang.Throwable javax.slee.management.AlarmNotification.getCause()
meth public javax.slee.facilities.AlarmLevel javax.slee.management.AlarmNotification.getAlarmLevel()
meth public javax.slee.facilities.Level javax.slee.management.AlarmNotification.getLevel()
meth public javax.slee.management.NotificationSource javax.slee.management.AlarmNotification.getNotificationSource()
meth public long javax.management.Notification.getSequenceNumber()
meth public long javax.management.Notification.getTimeStamp()
meth public static void javax.slee.management.AlarmNotification.disableVendorDataDeserialization()
meth public static void javax.slee.management.AlarmNotification.disableVendorDataSerialization()
meth public static void javax.slee.management.AlarmNotification.enableVendorDataDeserialization()
meth public static void javax.slee.management.AlarmNotification.enableVendorDataSerialization()
meth public void javax.management.Notification.setSequenceNumber(long)
meth public void javax.management.Notification.setSource(java.lang.Object)
meth public void javax.management.Notification.setTimeStamp(long)
meth public void javax.management.Notification.setUserData(java.lang.Object)
meth public void javax.slee.management.AlarmNotification.setVendorData(java.lang.Object)
supr java.lang.Object java.util.EventObject javax.management.Notification
CLSS public javax.slee.management.AlarmThresholdFilter
cons public javax.slee.management.AlarmThresholdFilter.AlarmThresholdFilter(int,long)
intf java.io.Serializable
intf javax.management.NotificationFilter
meth protected java.lang.Object java.lang.Object.clone() throws java.lang.CloneNotSupportedException
meth protected void java.lang.Object.finalize() throws java.lang.Throwable
meth public boolean java.lang.Object.equals(java.lang.Object)
meth public boolean javax.slee.management.AlarmThresholdFilter.isNotificationEnabled(javax.management.Notification)
meth public final java.lang.Class java.lang.Object.getClass()
meth public final void java.lang.Object.notify()
meth public final void java.lang.Object.notifyAll()
meth public final void java.lang.Object.wait() throws java.lang.InterruptedException
meth public final void java.lang.Object.wait(long) throws java.lang.InterruptedException
meth public final void java.lang.Object.wait(long,int) throws java.lang.InterruptedException
meth public int java.lang.Object.hashCode()
meth public java.lang.String java.lang.Object.toString()
supr java.lang.Object
CLSS public javax.slee.management.AlreadyDeployedException
cons public javax.slee.management.AlreadyDeployedException.AlreadyDeployedException(java.lang.String)
intf java.io.Serializable
meth protected java.lang.Object java.lang.Object.clone() throws java.lang.CloneNotSupportedException
meth protected void java.lang.Object.finalize() throws java.lang.Throwable
meth public [Ljava.lang.StackTraceElement; java.lang.Throwable.getStackTrace()
meth public boolean java.lang.Object.equals(java.lang.Object)
meth public final java.lang.Class java.lang.Object.getClass()
meth public final void java.lang.Object.notify()
meth public final void java.lang.Object.notifyAll()
meth public final void java.lang.Object.wait() throws java.lang.InterruptedException
meth public final void java.lang.Object.wait(long) throws java.lang.InterruptedException
meth public final void java.lang.Object.wait(long,int) throws java.lang.InterruptedException
meth public int java.lang.Object.hashCode()
meth public java.lang.String java.lang.Throwable.getLocalizedMessage()
meth public java.lang.String java.lang.Throwable.getMessage()
meth public java.lang.String java.lang.Throwable.toString()
meth public java.lang.Throwable java.lang.Throwable.fillInStackTrace()
meth public java.lang.Throwable java.lang.Throwable.initCause(java.lang.Throwable)
meth public java.lang.Throwable javax.slee.management.DeploymentException.getCause()
meth public void java.lang.Throwable.printStackTrace()
meth public void java.lang.Throwable.printStackTrace(java.io.PrintStream)
meth public void java.lang.Throwable.printStackTrace(java.io.PrintWriter)
meth public void java.lang.Throwable.setStackTrace([Ljava.lang.StackTraceElement;)
supr java.lang.Object java.lang.Throwable java.lang.Exception javax.slee.management.DeploymentException
CLSS public abstract javax.slee.management.ComponentDescriptor
cons protected javax.slee.management.ComponentDescriptor.ComponentDescriptor(javax.slee.ComponentID,javax.slee.management.DeployableUnitID,java.lang.String,[Ljavax.slee.management.LibraryID;)
intf java.io.Serializable
intf javax.slee.management.VendorExtensions
meth protected final void javax.slee.management.ComponentDescriptor.toString(java.lang.StringBuffer)
meth protected java.lang.Object java.lang.Object.clone() throws java.lang.CloneNotSupportedException
meth protected void java.lang.Object.finalize() throws java.lang.Throwable
meth public final [Ljavax.slee.management.LibraryID; javax.slee.management.ComponentDescriptor.getLibraries()
meth public final boolean javax.slee.management.ComponentDescriptor.equals(java.lang.Object)
meth public final int javax.slee.management.ComponentDescriptor.hashCode()
meth public final java.lang.Class java.lang.Object.getClass()
meth public final java.lang.String javax.slee.management.ComponentDescriptor.getName()
meth public final java.lang.String javax.slee.management.ComponentDescriptor.getSource()
meth public final java.lang.String javax.slee.management.ComponentDescriptor.getVendor()
meth public final java.lang.String javax.slee.management.ComponentDescriptor.getVersion()
meth public final javax.slee.ComponentID javax.slee.management.ComponentDescriptor.getID()
meth public final javax.slee.management.DeployableUnitID javax.slee.management.ComponentDescriptor.getDeployableUnit()
meth public final void java.lang.Object.notify()
meth public final void java.lang.Object.notifyAll()
meth public final void java.lang.Object.wait() throws java.lang.InterruptedException
meth public final void java.lang.Object.wait(long) throws java.lang.InterruptedException
meth public final void java.lang.Object.wait(long,int) throws java.lang.InterruptedException
meth public java.lang.Object javax.slee.management.ComponentDescriptor.getVendorData()
meth public java.lang.String java.lang.Object.toString()
meth public static void javax.slee.management.ComponentDescriptor.disableVendorDataDeserialization()
meth public static void javax.slee.management.ComponentDescriptor.disableVendorDataSerialization()
meth public static void javax.slee.management.ComponentDescriptor.enableVendorDataDeserialization()
meth public static void javax.slee.management.ComponentDescriptor.enableVendorDataSerialization()
meth public void javax.slee.management.ComponentDescriptor.setVendorData(java.lang.Object)
supr java.lang.Object
CLSS public javax.slee.management.DependencyException
cons public javax.slee.management.DependencyException.DependencyException(java.lang.String)
intf java.io.Serializable
meth protected java.lang.Object java.lang.Object.clone() throws java.lang.CloneNotSupportedException
meth protected void java.lang.Object.finalize() throws java.lang.Throwable
meth public [Ljava.lang.StackTraceElement; java.lang.Throwable.getStackTrace()
meth public boolean java.lang.Object.equals(java.lang.Object)
meth public final java.lang.Class java.lang.Object.getClass()
meth public final void java.lang.Object.notify()
meth public final void java.lang.Object.notifyAll()
meth public final void java.lang.Object.wait() throws java.lang.InterruptedException
meth public final void java.lang.Object.wait(long) throws java.lang.InterruptedException
meth public final void java.lang.Object.wait(long,int) throws java.lang.InterruptedException
meth public int java.lang.Object.hashCode()
meth public java.lang.String java.lang.Throwable.getLocalizedMessage()
meth public java.lang.String java.lang.Throwable.getMessage()
meth public java.lang.String java.lang.Throwable.toString()
meth public java.lang.Throwable java.lang.Throwable.fillInStackTrace()
meth public java.lang.Throwable java.lang.Throwable.initCause(java.lang.Throwable)
meth public java.lang.Throwable javax.slee.management.DeploymentException.getCause()
meth public void java.lang.Throwable.printStackTrace()
meth public void java.lang.Throwable.printStackTrace(java.io.PrintStream)
meth public void java.lang.Throwable.printStackTrace(java.io.PrintWriter)
meth public void java.lang.Throwable.setStackTrace([Ljava.lang.StackTraceElement;)
supr java.lang.Object java.lang.Throwable java.lang.Exception javax.slee.management.DeploymentException
CLSS public javax.slee.management.DeployableUnitDescriptor
cons public javax.slee.management.DeployableUnitDescriptor.DeployableUnitDescriptor(javax.slee.management.DeployableUnitID,java.util.Date,[Ljavax.slee.ComponentID;)
intf java.io.Serializable
intf javax.slee.management.VendorExtensions
meth protected java.lang.Object java.lang.Object.clone() throws java.lang.CloneNotSupportedException
meth protected void java.lang.Object.finalize() throws java.lang.Throwable
meth public [Ljavax.slee.ComponentID; javax.slee.management.DeployableUnitDescriptor.getComponents()
meth public boolean javax.slee.management.DeployableUnitDescriptor.equals(java.lang.Object)
meth public final java.lang.Class java.lang.Object.getClass()
meth public final void java.lang.Object.notify()
meth public final void java.lang.Object.notifyAll()
meth public final void java.lang.Object.wait() throws java.lang.InterruptedException
meth public final void java.lang.Object.wait(long) throws java.lang.InterruptedException
meth public final void java.lang.Object.wait(long,int) throws java.lang.InterruptedException
meth public int javax.slee.management.DeployableUnitDescriptor.hashCode()
meth public java.lang.Object javax.slee.management.DeployableUnitDescriptor.getVendorData()
meth public java.lang.String javax.slee.management.DeployableUnitDescriptor.getURL()
meth public java.lang.String javax.slee.management.DeployableUnitDescriptor.toString()
meth public java.util.Date javax.slee.management.DeployableUnitDescriptor.getDeploymentDate()
meth public javax.slee.management.DeployableUnitID javax.slee.management.DeployableUnitDescriptor.getID()
meth public static void javax.slee.management.DeployableUnitDescriptor.disableVendorDataDeserialization()
meth public static void javax.slee.management.DeployableUnitDescriptor.disableVendorDataSerialization()
meth public static void javax.slee.management.DeployableUnitDescriptor.enableVendorDataDeserialization()
meth public static void javax.slee.management.DeployableUnitDescriptor.enableVendorDataSerialization()
meth public void javax.slee.management.DeployableUnitDescriptor.setVendorData(java.lang.Object)
supr java.lang.Object
CLSS public final javax.slee.management.DeployableUnitID
cons public javax.slee.management.DeployableUnitID.DeployableUnitID(java.lang.String)
intf java.io.Serializable
intf java.lang.Comparable
meth protected java.lang.Object java.lang.Object.clone() throws java.lang.CloneNotSupportedException
meth protected void java.lang.Object.finalize() throws java.lang.Throwable
meth public final boolean javax.slee.management.DeployableUnitID.equals(java.lang.Object)
meth public final int javax.slee.management.DeployableUnitID.hashCode()
meth public final java.lang.Class java.lang.Object.getClass()
meth public final java.lang.String javax.slee.management.DeployableUnitID.getURL()
meth public final void java.lang.Object.notify()
meth public final void java.lang.Object.notifyAll()
meth public final void java.lang.Object.wait() throws java.lang.InterruptedException
meth public final void java.lang.Object.wait(long) throws java.lang.InterruptedException
meth public final void java.lang.Object.wait(long,int) throws java.lang.InterruptedException
meth public int javax.slee.management.DeployableUnitID.compareTo(java.lang.Object)
meth public java.lang.String javax.slee.management.DeployableUnitID.toString()
supr java.lang.Object
CLSS public javax.slee.management.DeploymentException
cons public javax.slee.management.DeploymentException.DeploymentException(java.lang.String)
cons public javax.slee.management.DeploymentException.DeploymentException(java.lang.String,java.lang.Throwable)
intf java.io.Serializable
meth protected java.lang.Object java.lang.Object.clone() throws java.lang.CloneNotSupportedException
meth protected void java.lang.Object.finalize() throws java.lang.Throwable
meth public [Ljava.lang.StackTraceElement; java.lang.Throwable.getStackTrace()
meth public boolean java.lang.Object.equals(java.lang.Object)
meth public final java.lang.Class java.lang.Object.getClass()
meth public final void java.lang.Object.notify()
meth public final void java.lang.Object.notifyAll()
meth public final void java.lang.Object.wait() throws java.lang.InterruptedException
meth public final void java.lang.Object.wait(long) throws java.lang.InterruptedException
meth public final void java.lang.Object.wait(long,int) throws java.lang.InterruptedException
meth public int java.lang.Object.hashCode()
meth public java.lang.String java.lang.Throwable.getLocalizedMessage()
meth public java.lang.String java.lang.Throwable.getMessage()
meth public java.lang.String java.lang.Throwable.toString()
meth public java.lang.Throwable java.lang.Throwable.fillInStackTrace()
meth public java.lang.Throwable java.lang.Throwable.initCause(java.lang.Throwable)
meth public java.lang.Throwable javax.slee.management.DeploymentException.getCause()
meth public void java.lang.Throwable.printStackTrace()
meth public void java.lang.Throwable.printStackTrace(java.io.PrintStream)
meth public void java.lang.Throwable.printStackTrace(java.io.PrintWriter)
meth public void java.lang.Throwable.setStackTrace([Ljava.lang.StackTraceElement;)
supr java.lang.Object java.lang.Throwable java.lang.Exception
CLSS public abstract interface javax.slee.management.DeploymentMBean
fld  public static final java.lang.String javax.slee.management.DeploymentMBean.OBJECT_NAME
meth public abstract [Ljavax.slee.ComponentID; javax.slee.management.DeploymentMBean.getReferringComponents(javax.slee.ComponentID) throws javax.slee.UnrecognizedComponentException,javax.slee.management.ManagementException
meth public abstract [Ljavax.slee.EventTypeID; javax.slee.management.DeploymentMBean.getEventTypes() throws javax.slee.management.ManagementException
meth public abstract [Ljavax.slee.SbbID; javax.slee.management.DeploymentMBean.getSbbs() throws javax.slee.management.ManagementException
meth public abstract [Ljavax.slee.SbbID; javax.slee.management.DeploymentMBean.getSbbs(javax.slee.ServiceID) throws javax.slee.UnrecognizedServiceException,javax.slee.management.ManagementException
meth public abstract [Ljavax.slee.ServiceID; javax.slee.management.DeploymentMBean.getServices() throws javax.slee.management.ManagementException
meth public abstract [Ljavax.slee.management.ComponentDescriptor; javax.slee.management.DeploymentMBean.getDescriptors([Ljavax.slee.ComponentID;) throws javax.slee.management.ManagementException
meth public abstract [Ljavax.slee.management.DeployableUnitDescriptor; javax.slee.management.DeploymentMBean.getDescriptors([Ljavax.slee.management.DeployableUnitID;) throws javax.slee.management.ManagementException
meth public abstract [Ljavax.slee.management.DeployableUnitID; javax.slee.management.DeploymentMBean.getDeployableUnits() throws javax.slee.management.ManagementException
meth public abstract [Ljavax.slee.management.LibraryID; javax.slee.management.DeploymentMBean.getLibraries() throws javax.slee.management.ManagementException
meth public abstract [Ljavax.slee.profile.ProfileSpecificationID; javax.slee.management.DeploymentMBean.getProfileSpecifications() throws javax.slee.management.ManagementException
meth public abstract [Ljavax.slee.resource.ResourceAdaptorID; javax.slee.management.DeploymentMBean.getResourceAdaptors() throws javax.slee.management.ManagementException
meth public abstract [Ljavax.slee.resource.ResourceAdaptorTypeID; javax.slee.management.DeploymentMBean.getResourceAdaptorTypes() throws javax.slee.management.ManagementException
meth public abstract boolean javax.slee.management.DeploymentMBean.isInstalled(javax.slee.ComponentID) throws javax.slee.management.ManagementException
meth public abstract boolean javax.slee.management.DeploymentMBean.isInstalled(javax.slee.management.DeployableUnitID) throws javax.slee.management.ManagementException
meth public abstract javax.slee.management.ComponentDescriptor javax.slee.management.DeploymentMBean.getDescriptor(javax.slee.ComponentID) throws javax.slee.UnrecognizedComponentException,javax.slee.management.ManagementException
meth public abstract javax.slee.management.DeployableUnitDescriptor javax.slee.management.DeploymentMBean.getDescriptor(javax.slee.management.DeployableUnitID) throws javax.slee.management.ManagementException,javax.slee.management.UnrecognizedDeployableUnitException
meth public abstract javax.slee.management.DeployableUnitID javax.slee.management.DeploymentMBean.getDeployableUnit(java.lang.String) throws javax.slee.management.ManagementException,javax.slee.management.UnrecognizedDeployableUnitException
meth public abstract javax.slee.management.DeployableUnitID javax.slee.management.DeploymentMBean.install(java.lang.String) throws java.net.MalformedURLException,javax.slee.management.DeploymentException,javax.slee.management.ManagementException
meth public abstract void javax.slee.management.DeploymentMBean.uninstall(javax.slee.management.DeployableUnitID) throws javax.slee.InvalidStateException,javax.slee.management.DependencyException,javax.slee.management.ManagementException,javax.slee.management.UnrecognizedDeployableUnitException
supr null
CLSS public javax.slee.management.EventTypeDescriptor
cons public javax.slee.management.EventTypeDescriptor.EventTypeDescriptor(javax.slee.EventTypeID,javax.slee.management.DeployableUnitID,java.lang.String,[Ljavax.slee.management.LibraryID;,java.lang.String)
intf java.io.Serializable
intf javax.slee.management.VendorExtensions
meth protected final void javax.slee.management.ComponentDescriptor.toString(java.lang.StringBuffer)
meth protected java.lang.Object java.lang.Object.clone() throws java.lang.CloneNotSupportedException
meth protected void java.lang.Object.finalize() throws java.lang.Throwable
meth public final [Ljavax.slee.management.LibraryID; javax.slee.management.ComponentDescriptor.getLibraries()
meth public final boolean javax.slee.management.ComponentDescriptor.equals(java.lang.Object)
meth public final int javax.slee.management.ComponentDescriptor.hashCode()
meth public final java.lang.Class java.lang.Object.getClass()
meth public final java.lang.String javax.slee.management.ComponentDescriptor.getName()
meth public final java.lang.String javax.slee.management.ComponentDescriptor.getSource()
meth public final java.lang.String javax.slee.management.ComponentDescriptor.getVendor()
meth public final java.lang.String javax.slee.management.ComponentDescriptor.getVersion()
meth public final java.lang.String javax.slee.management.EventTypeDescriptor.getEventClassName()
meth public final javax.slee.ComponentID javax.slee.management.ComponentDescriptor.getID()
meth public final javax.slee.management.DeployableUnitID javax.slee.management.ComponentDescriptor.getDeployableUnit()
meth public final void java.lang.Object.notify()
meth public final void java.lang.Object.notifyAll()
meth public final void java.lang.Object.wait() throws java.lang.InterruptedException
meth public final void java.lang.Object.wait(long) throws java.lang.InterruptedException
meth public final void java.lang.Object.wait(long,int) throws java.lang.InterruptedException
meth public java.lang.Object javax.slee.management.ComponentDescriptor.getVendorData()
meth public java.lang.String javax.slee.management.EventTypeDescriptor.toString()
meth public static void javax.slee.management.ComponentDescriptor.disableVendorDataDeserialization()
meth public static void javax.slee.management.ComponentDescriptor.disableVendorDataSerialization()
meth public static void javax.slee.management.ComponentDescriptor.enableVendorDataDeserialization()
meth public static void javax.slee.management.ComponentDescriptor.enableVendorDataSerialization()
meth public void javax.slee.management.ComponentDescriptor.setVendorData(java.lang.Object)
supr java.lang.Object javax.slee.management.ComponentDescriptor
CLSS public javax.slee.management.InvalidLinkNameBindingStateException
cons public javax.slee.management.InvalidLinkNameBindingStateException.InvalidLinkNameBindingStateException()
cons public javax.slee.management.InvalidLinkNameBindingStateException.InvalidLinkNameBindingStateException(java.lang.String)
intf java.io.Serializable
meth protected java.lang.Object java.lang.Object.clone() throws java.lang.CloneNotSupportedException
meth protected void java.lang.Object.finalize() throws java.lang.Throwable
meth public [Ljava.lang.StackTraceElement; java.lang.Throwable.getStackTrace()
meth public boolean java.lang.Object.equals(java.lang.Object)
meth public final java.lang.Class java.lang.Object.getClass()
meth public final void java.lang.Object.notify()
meth public final void java.lang.Object.notifyAll()
meth public final void java.lang.Object.wait() throws java.lang.InterruptedException
meth public final void java.lang.Object.wait(long) throws java.lang.InterruptedException
meth public final void java.lang.Object.wait(long,int) throws java.lang.InterruptedException
meth public int java.lang.Object.hashCode()
meth public java.lang.String java.lang.Throwable.getLocalizedMessage()
meth public java.lang.String java.lang.Throwable.getMessage()
meth public java.lang.String java.lang.Throwable.toString()
meth public java.lang.Throwable java.lang.Throwable.fillInStackTrace()
meth public java.lang.Throwable java.lang.Throwable.getCause()
meth public java.lang.Throwable java.lang.Throwable.initCause(java.lang.Throwable)
meth public void java.lang.Throwable.printStackTrace()
meth public void java.lang.Throwable.printStackTrace(java.io.PrintStream)
meth public void java.lang.Throwable.printStackTrace(java.io.PrintWriter)
meth public void java.lang.Throwable.setStackTrace([Ljava.lang.StackTraceElement;)
supr java.lang.Object java.lang.Throwable java.lang.Exception javax.slee.InvalidStateException
CLSS public javax.slee.management.LibraryDescriptor
cons public javax.slee.management.LibraryDescriptor.LibraryDescriptor(javax.slee.management.LibraryID,javax.slee.management.DeployableUnitID,java.lang.String,[Ljavax.slee.management.LibraryID;,[Ljava.lang.String;)
intf java.io.Serializable
intf javax.slee.management.VendorExtensions
meth protected final void javax.slee.management.ComponentDescriptor.toString(java.lang.StringBuffer)
meth protected java.lang.Object java.lang.Object.clone() throws java.lang.CloneNotSupportedException
meth protected void java.lang.Object.finalize() throws java.lang.Throwable
meth public final [Ljava.lang.String; javax.slee.management.LibraryDescriptor.getLibraryJars()
meth public final [Ljavax.slee.management.LibraryID; javax.slee.management.ComponentDescriptor.getLibraries()
meth public final boolean javax.slee.management.ComponentDescriptor.equals(java.lang.Object)
meth public final int javax.slee.management.ComponentDescriptor.hashCode()
meth public final java.lang.Class java.lang.Object.getClass()
meth public final java.lang.String javax.slee.management.ComponentDescriptor.getName()
meth public final java.lang.String javax.slee.management.ComponentDescriptor.getSource()
meth public final java.lang.String javax.slee.management.ComponentDescriptor.getVendor()
meth public final java.lang.String javax.slee.management.ComponentDescriptor.getVersion()
meth public final javax.slee.ComponentID javax.slee.management.ComponentDescriptor.getID()
meth public final javax.slee.management.DeployableUnitID javax.slee.management.ComponentDescriptor.getDeployableUnit()
meth public final void java.lang.Object.notify()
meth public final void java.lang.Object.notifyAll()
meth public final void java.lang.Object.wait() throws java.lang.InterruptedException
meth public final void java.lang.Object.wait(long) throws java.lang.InterruptedException
meth public final void java.lang.Object.wait(long,int) throws java.lang.InterruptedException
meth public java.lang.Object javax.slee.management.ComponentDescriptor.getVendorData()
meth public java.lang.String javax.slee.management.LibraryDescriptor.toString()
meth public static void javax.slee.management.ComponentDescriptor.disableVendorDataDeserialization()
meth public static void javax.slee.management.ComponentDescriptor.disableVendorDataSerialization()
meth public static void javax.slee.management.ComponentDescriptor.enableVendorDataDeserialization()
meth public static void javax.slee.management.ComponentDescriptor.enableVendorDataSerialization()
meth public void javax.slee.management.ComponentDescriptor.setVendorData(java.lang.Object)
supr java.lang.Object javax.slee.management.ComponentDescriptor
CLSS public final javax.slee.management.LibraryID
cons public javax.slee.management.LibraryID.LibraryID(java.lang.String,java.lang.String,java.lang.String)
intf java.io.Serializable
intf java.lang.Cloneable
intf java.lang.Comparable
meth protected final int javax.slee.ComponentID.compareTo(java.lang.String,javax.slee.ComponentID)
meth protected java.lang.String javax.slee.management.LibraryID.getClassName()
meth protected void java.lang.Object.finalize() throws java.lang.Throwable
meth public final boolean javax.slee.ComponentID.equals(java.lang.Object)
meth public final int javax.slee.ComponentID.hashCode()
meth public final int javax.slee.management.LibraryID.compareTo(java.lang.Object)
meth public final java.lang.Class java.lang.Object.getClass()
meth public final java.lang.String javax.slee.ComponentID.getName()
meth public final java.lang.String javax.slee.ComponentID.getVendor()
meth public final java.lang.String javax.slee.ComponentID.getVersion()
meth public final java.lang.String javax.slee.ComponentID.toString()
meth public final void java.lang.Object.notify()
meth public final void java.lang.Object.notifyAll()
meth public final void java.lang.Object.wait() throws java.lang.InterruptedException
meth public final void java.lang.Object.wait(long) throws java.lang.InterruptedException
meth public final void java.lang.Object.wait(long,int) throws java.lang.InterruptedException
meth public java.lang.Object javax.slee.management.LibraryID.clone()
supr java.lang.Object javax.slee.ComponentID
CLSS public javax.slee.management.LinkNameAlreadyBoundException
cons public javax.slee.management.LinkNameAlreadyBoundException.LinkNameAlreadyBoundException()
cons public javax.slee.management.LinkNameAlreadyBoundException.LinkNameAlreadyBoundException(java.lang.String)
intf java.io.Serializable
meth protected java.lang.Object java.lang.Object.clone() throws java.lang.CloneNotSupportedException
meth protected void java.lang.Object.finalize() throws java.lang.Throwable
meth public [Ljava.lang.StackTraceElement; java.lang.Throwable.getStackTrace()
meth public boolean java.lang.Object.equals(java.lang.Object)
meth public final java.lang.Class java.lang.Object.getClass()
meth public final void java.lang.Object.notify()
meth public final void java.lang.Object.notifyAll()
meth public final void java.lang.Object.wait() throws java.lang.InterruptedException
meth public final void java.lang.Object.wait(long) throws java.lang.InterruptedException
meth public final void java.lang.Object.wait(long,int) throws java.lang.InterruptedException
meth public int java.lang.Object.hashCode()
meth public java.lang.String java.lang.Throwable.getLocalizedMessage()
meth public java.lang.String java.lang.Throwable.getMessage()
meth public java.lang.String java.lang.Throwable.toString()
meth public java.lang.Throwable java.lang.Throwable.fillInStackTrace()
meth public java.lang.Throwable java.lang.Throwable.getCause()
meth public java.lang.Throwable java.lang.Throwable.initCause(java.lang.Throwable)
meth public void java.lang.Throwable.printStackTrace()
meth public void java.lang.Throwable.printStackTrace(java.io.PrintStream)
meth public void java.lang.Throwable.printStackTrace(java.io.PrintWriter)
meth public void java.lang.Throwable.setStackTrace([Ljava.lang.StackTraceElement;)
supr java.lang.Object java.lang.Throwable java.lang.Exception
CLSS public javax.slee.management.ManagementException
cons public javax.slee.management.ManagementException.ManagementException(java.lang.String)
cons public javax.slee.management.ManagementException.ManagementException(java.lang.String,java.lang.Throwable)
intf java.io.Serializable
meth protected java.lang.Object java.lang.Object.clone() throws java.lang.CloneNotSupportedException
meth protected void java.lang.Object.finalize() throws java.lang.Throwable
meth public [Ljava.lang.StackTraceElement; java.lang.Throwable.getStackTrace()
meth public boolean java.lang.Object.equals(java.lang.Object)
meth public final java.lang.Class java.lang.Object.getClass()
meth public final void java.lang.Object.notify()
meth public final void java.lang.Object.notifyAll()
meth public final void java.lang.Object.wait() throws java.lang.InterruptedException
meth public final void java.lang.Object.wait(long) throws java.lang.InterruptedException
meth public final void java.lang.Object.wait(long,int) throws java.lang.InterruptedException
meth public int java.lang.Object.hashCode()
meth public java.lang.String java.lang.Throwable.getLocalizedMessage()
meth public java.lang.String java.lang.Throwable.getMessage()
meth public java.lang.String java.lang.Throwable.toString()
meth public java.lang.Throwable java.lang.Throwable.fillInStackTrace()
meth public java.lang.Throwable java.lang.Throwable.initCause(java.lang.Throwable)
meth public java.lang.Throwable javax.slee.management.ManagementException.getCause()
meth public void java.lang.Throwable.printStackTrace()
meth public void java.lang.Throwable.printStackTrace(java.io.PrintStream)
meth public void java.lang.Throwable.printStackTrace(java.io.PrintWriter)
meth public void java.lang.Throwable.setStackTrace([Ljava.lang.StackTraceElement;)
supr java.lang.Object java.lang.Throwable java.lang.Exception
CLSS public abstract interface javax.slee.management.NotificationSource
intf java.io.Serializable
intf java.lang.Comparable
meth public abstract boolean javax.slee.management.NotificationSource.equals(java.lang.Object)
meth public abstract int javax.slee.management.NotificationSource.compareTo(java.lang.Object)
meth public abstract int javax.slee.management.NotificationSource.hashCode()
meth public abstract java.lang.String javax.slee.management.NotificationSource.getAlarmNotificationType()
meth public abstract java.lang.String javax.slee.management.NotificationSource.getTraceNotificationType()
meth public abstract java.lang.String javax.slee.management.NotificationSource.getUsageNotificationType()
meth public abstract java.lang.String javax.slee.management.NotificationSource.toString()
supr null
CLSS public javax.slee.management.PeerUnavailableException
cons public javax.slee.management.PeerUnavailableException.PeerUnavailableException(java.lang.String)
cons public javax.slee.management.PeerUnavailableException.PeerUnavailableException(java.lang.String,java.lang.Throwable)
intf java.io.Serializable
meth protected java.lang.Object java.lang.Object.clone() throws java.lang.CloneNotSupportedException
meth protected void java.lang.Object.finalize() throws java.lang.Throwable
meth public [Ljava.lang.StackTraceElement; java.lang.Throwable.getStackTrace()
meth public boolean java.lang.Object.equals(java.lang.Object)
meth public final java.lang.Class java.lang.Object.getClass()
meth public final void java.lang.Object.notify()
meth public final void java.lang.Object.notifyAll()
meth public final void java.lang.Object.wait() throws java.lang.InterruptedException
meth public final void java.lang.Object.wait(long) throws java.lang.InterruptedException
meth public final void java.lang.Object.wait(long,int) throws java.lang.InterruptedException
meth public int java.lang.Object.hashCode()
meth public java.lang.String java.lang.Throwable.getLocalizedMessage()
meth public java.lang.String java.lang.Throwable.getMessage()
meth public java.lang.String java.lang.Throwable.toString()
meth public java.lang.Throwable java.lang.Throwable.fillInStackTrace()
meth public java.lang.Throwable java.lang.Throwable.initCause(java.lang.Throwable)
meth public java.lang.Throwable javax.slee.management.PeerUnavailableException.getCause()
meth public void java.lang.Throwable.printStackTrace()
meth public void java.lang.Throwable.printStackTrace(java.io.PrintStream)
meth public void java.lang.Throwable.printStackTrace(java.io.PrintWriter)
meth public void java.lang.Throwable.setStackTrace([Ljava.lang.StackTraceElement;)
supr java.lang.Object java.lang.Throwable java.lang.Exception
CLSS public abstract interface javax.slee.management.ProfileProvisioningMBean
fld  public static final java.lang.String javax.slee.management.ProfileProvisioningMBean.OBJECT_NAME
meth public abstract java.util.Collection javax.slee.management.ProfileProvisioningMBean.getProfileTables() throws javax.slee.management.ManagementException
meth public abstract java.util.Collection javax.slee.management.ProfileProvisioningMBean.getProfileTables(javax.slee.profile.ProfileSpecificationID) throws javax.slee.management.ManagementException,javax.slee.profile.UnrecognizedProfileSpecificationException
meth public abstract java.util.Collection javax.slee.management.ProfileProvisioningMBean.getProfiles(java.lang.String) throws javax.slee.management.ManagementException,javax.slee.profile.UnrecognizedProfileTableNameException
meth public abstract java.util.Collection javax.slee.management.ProfileProvisioningMBean.getProfilesByAttribute(java.lang.String,java.lang.String,java.lang.Object) throws javax.slee.InvalidArgumentException,javax.slee.management.ManagementException,javax.slee.profile.AttributeTypeMismatchException,javax.slee.profile.UnrecognizedAttributeException,javax.slee.profile.UnrecognizedProfileTableNameException
meth public abstract java.util.Collection javax.slee.management.ProfileProvisioningMBean.getProfilesByDynamicQuery(java.lang.String,javax.slee.profile.query.QueryExpression) throws javax.slee.management.ManagementException,javax.slee.profile.AttributeTypeMismatchException,javax.slee.profile.UnrecognizedAttributeException,javax.slee.profile.UnrecognizedProfileTableNameException
meth public abstract java.util.Collection javax.slee.management.ProfileProvisioningMBean.getProfilesByIndexedAttribute(java.lang.String,java.lang.String,java.lang.Object) throws javax.slee.management.ManagementException,javax.slee.profile.AttributeNotIndexedException,javax.slee.profile.AttributeTypeMismatchException,javax.slee.profile.UnrecognizedAttributeException,javax.slee.profile.UnrecognizedProfileTableNameException
meth public abstract java.util.Collection javax.slee.management.ProfileProvisioningMBean.getProfilesByStaticQuery(java.lang.String,java.lang.String,[Ljava.lang.Object;) throws javax.slee.InvalidArgumentException,javax.slee.management.ManagementException,javax.slee.profile.AttributeTypeMismatchException,javax.slee.profile.UnrecognizedProfileTableNameException,javax.slee.profile.UnrecognizedQueryNameException
meth public abstract javax.management.ObjectName javax.slee.management.ProfileProvisioningMBean.createProfile(java.lang.String,java.lang.String) throws javax.slee.InvalidArgumentException,javax.slee.management.ManagementException,javax.slee.profile.ProfileAlreadyExistsException,javax.slee.profile.UnrecognizedProfileTableNameException
meth public abstract javax.management.ObjectName javax.slee.management.ProfileProvisioningMBean.getDefaultProfile(java.lang.String) throws javax.slee.management.ManagementException,javax.slee.profile.UnrecognizedProfileTableNameException
meth public abstract javax.management.ObjectName javax.slee.management.ProfileProvisioningMBean.getProfile(java.lang.String,java.lang.String) throws javax.slee.management.ManagementException,javax.slee.profile.UnrecognizedProfileNameException,javax.slee.profile.UnrecognizedProfileTableNameException
meth public abstract javax.management.ObjectName javax.slee.management.ProfileProvisioningMBean.getProfileTableUsageMBean(java.lang.String) throws javax.slee.InvalidArgumentException,javax.slee.management.ManagementException,javax.slee.profile.UnrecognizedProfileTableNameException
meth public abstract javax.slee.profile.ProfileSpecificationID javax.slee.management.ProfileProvisioningMBean.getProfileSpecification(java.lang.String) throws javax.slee.management.ManagementException,javax.slee.profile.UnrecognizedProfileTableNameException
meth public abstract void javax.slee.management.ProfileProvisioningMBean.createProfileTable(javax.slee.profile.ProfileSpecificationID,java.lang.String) throws javax.slee.InvalidArgumentException,javax.slee.management.ManagementException,javax.slee.profile.ProfileTableAlreadyExistsException,javax.slee.profile.UnrecognizedProfileSpecificationException
meth public abstract void javax.slee.management.ProfileProvisioningMBean.removeProfile(java.lang.String,java.lang.String) throws javax.slee.management.ManagementException,javax.slee.profile.UnrecognizedProfileNameException,javax.slee.profile.UnrecognizedProfileTableNameException
meth public abstract void javax.slee.management.ProfileProvisioningMBean.removeProfileTable(java.lang.String) throws javax.slee.management.ManagementException,javax.slee.profile.UnrecognizedProfileTableNameException
meth public abstract void javax.slee.management.ProfileProvisioningMBean.renameProfileTable(java.lang.String,java.lang.String) throws javax.slee.InvalidArgumentException,javax.slee.management.ManagementException,javax.slee.profile.ProfileTableAlreadyExistsException,javax.slee.profile.UnrecognizedProfileTableNameException
supr null
CLSS public final javax.slee.management.ProfileTableNotification
cons public javax.slee.management.ProfileTableNotification.ProfileTableNotification(java.lang.String)
fld  public static final java.lang.String javax.slee.management.ProfileTableNotification.ALARM_NOTIFICATION_TYPE
fld  public static final java.lang.String javax.slee.management.ProfileTableNotification.PROFILE_TABLE_NAME_KEY
fld  public static final java.lang.String javax.slee.management.ProfileTableNotification.TRACE_NOTIFICATION_TYPE
fld  public static final java.lang.String javax.slee.management.ProfileTableNotification.USAGE_NOTIFICATION_TYPE
intf java.io.Serializable
intf java.lang.Comparable
intf javax.slee.management.NotificationSource
meth protected final int javax.slee.management.AbstractNotificationSource.compareTo(java.lang.String,java.lang.Object)
meth protected java.lang.Object java.lang.Object.clone() throws java.lang.CloneNotSupportedException
meth protected java.lang.String javax.slee.management.ProfileTableNotification.getClassName()
meth protected void java.lang.Object.finalize() throws java.lang.Throwable
meth public boolean javax.slee.management.ProfileTableNotification.equals(java.lang.Object)
meth public final java.lang.Class java.lang.Object.getClass()
meth public final void java.lang.Object.notify()
meth public final void java.lang.Object.notifyAll()
meth public final void java.lang.Object.wait() throws java.lang.InterruptedException
meth public final void java.lang.Object.wait(long) throws java.lang.InterruptedException
meth public final void java.lang.Object.wait(long,int) throws java.lang.InterruptedException
meth public int javax.slee.management.ProfileTableNotification.compareTo(java.lang.Object)
meth public int javax.slee.management.ProfileTableNotification.hashCode()
meth public java.lang.String javax.slee.management.ProfileTableNotification.getAlarmNotificationType()
meth public java.lang.String javax.slee.management.ProfileTableNotification.getProfileTableName()
meth public java.lang.String javax.slee.management.ProfileTableNotification.getTraceNotificationType()
meth public java.lang.String javax.slee.management.ProfileTableNotification.getUsageNotificationType()
meth public java.lang.String javax.slee.management.ProfileTableNotification.toString()
supr java.lang.Object javax.slee.management.AbstractNotificationSource
CLSS public abstract interface javax.slee.management.ProfileTableUsageMBean
fld  public static final java.lang.String javax.slee.management.ProfileTableUsageMBean.BASE_OBJECT_NAME
fld  public static final java.lang.String javax.slee.management.ProfileTableUsageMBean.PROFILE_TABLE_NAME_KEY
meth public abstract [Ljava.lang.String; javax.slee.management.ProfileTableUsageMBean.getUsageParameterSets() throws javax.slee.management.ManagementException
meth public abstract java.lang.String javax.slee.management.ProfileTableUsageMBean.getProfileTableName() throws javax.slee.management.ManagementException
meth public abstract javax.management.ObjectName javax.slee.management.ProfileTableUsageMBean.getUsageMBean() throws javax.slee.management.ManagementException
meth public abstract javax.management.ObjectName javax.slee.management.ProfileTableUsageMBean.getUsageMBean(java.lang.String) throws javax.slee.management.ManagementException,javax.slee.usage.UnrecognizedUsageParameterSetNameException
meth public abstract javax.management.ObjectName javax.slee.management.ProfileTableUsageMBean.getUsageNotificationManagerMBean() throws javax.slee.management.ManagementException
meth public abstract void javax.slee.management.ProfileTableUsageMBean.close() throws javax.slee.management.ManagementException
meth public abstract void javax.slee.management.ProfileTableUsageMBean.createUsageParameterSet(java.lang.String) throws javax.slee.InvalidArgumentException,javax.slee.management.ManagementException,javax.slee.management.UsageParameterSetNameAlreadyExistsException
meth public abstract void javax.slee.management.ProfileTableUsageMBean.removeUsageParameterSet(java.lang.String) throws javax.slee.management.ManagementException,javax.slee.usage.UnrecognizedUsageParameterSetNameException
meth public abstract void javax.slee.management.ProfileTableUsageMBean.resetAllUsageParameters() throws javax.slee.management.ManagementException
supr null
CLSS public javax.slee.management.ResourceAdaptorEntityAlreadyExistsException
cons public javax.slee.management.ResourceAdaptorEntityAlreadyExistsException.ResourceAdaptorEntityAlreadyExistsException()
cons public javax.slee.management.ResourceAdaptorEntityAlreadyExistsException.ResourceAdaptorEntityAlreadyExistsException(java.lang.String)
intf java.io.Serializable
meth protected java.lang.Object java.lang.Object.clone() throws java.lang.CloneNotSupportedException
meth protected void java.lang.Object.finalize() throws java.lang.Throwable
meth public [Ljava.lang.StackTraceElement; java.lang.Throwable.getStackTrace()
meth public boolean java.lang.Object.equals(java.lang.Object)
meth public final java.lang.Class java.lang.Object.getClass()
meth public final void java.lang.Object.notify()
meth public final void java.lang.Object.notifyAll()
meth public final void java.lang.Object.wait() throws java.lang.InterruptedException
meth public final void java.lang.Object.wait(long) throws java.lang.InterruptedException
meth public final void java.lang.Object.wait(long,int) throws java.lang.InterruptedException
meth public int java.lang.Object.hashCode()
meth public java.lang.String java.lang.Throwable.getLocalizedMessage()
meth public java.lang.String java.lang.Throwable.getMessage()
meth public java.lang.String java.lang.Throwable.toString()
meth public java.lang.Throwable java.lang.Throwable.fillInStackTrace()
meth public java.lang.Throwable java.lang.Throwable.getCause()
meth public java.lang.Throwable java.lang.Throwable.initCause(java.lang.Throwable)
meth public void java.lang.Throwable.printStackTrace()
meth public void java.lang.Throwable.printStackTrace(java.io.PrintStream)
meth public void java.lang.Throwable.printStackTrace(java.io.PrintWriter)
meth public void java.lang.Throwable.setStackTrace([Ljava.lang.StackTraceElement;)
supr java.lang.Object java.lang.Throwable java.lang.Exception
CLSS public final javax.slee.management.ResourceAdaptorEntityNotification
cons public javax.slee.management.ResourceAdaptorEntityNotification.ResourceAdaptorEntityNotification(java.lang.String)
fld  public static final java.lang.String javax.slee.management.ResourceAdaptorEntityNotification.ALARM_NOTIFICATION_TYPE
fld  public static final java.lang.String javax.slee.management.ResourceAdaptorEntityNotification.RESOURCE_ADAPTOR_ENTITY_NAME_KEY
fld  public static final java.lang.String javax.slee.management.ResourceAdaptorEntityNotification.TRACE_NOTIFICATION_TYPE
fld  public static final java.lang.String javax.slee.management.ResourceAdaptorEntityNotification.USAGE_NOTIFICATION_TYPE
intf java.io.Serializable
intf java.lang.Comparable
intf javax.slee.management.NotificationSource
meth protected final int javax.slee.management.AbstractNotificationSource.compareTo(java.lang.String,java.lang.Object)
meth protected java.lang.Object java.lang.Object.clone() throws java.lang.CloneNotSupportedException
meth protected java.lang.String javax.slee.management.ResourceAdaptorEntityNotification.getClassName()
meth protected void java.lang.Object.finalize() throws java.lang.Throwable
meth public boolean javax.slee.management.ResourceAdaptorEntityNotification.equals(java.lang.Object)
meth public final java.lang.Class java.lang.Object.getClass()
meth public final void java.lang.Object.notify()
meth public final void java.lang.Object.notifyAll()
meth public final void java.lang.Object.wait() throws java.lang.InterruptedException
meth public final void java.lang.Object.wait(long) throws java.lang.InterruptedException
meth public final void java.lang.Object.wait(long,int) throws java.lang.InterruptedException
meth public int javax.slee.management.ResourceAdaptorEntityNotification.compareTo(java.lang.Object)
meth public int javax.slee.management.ResourceAdaptorEntityNotification.hashCode()
meth public java.lang.String javax.slee.management.ResourceAdaptorEntityNotification.getAlarmNotificationType()
meth public java.lang.String javax.slee.management.ResourceAdaptorEntityNotification.getEntityName()
meth public java.lang.String javax.slee.management.ResourceAdaptorEntityNotification.getTraceNotificationType()
meth public java.lang.String javax.slee.management.ResourceAdaptorEntityNotification.getUsageMBeanProperties()
meth public java.lang.String javax.slee.management.ResourceAdaptorEntityNotification.getUsageNotificationType()
meth public java.lang.String javax.slee.management.ResourceAdaptorEntityNotification.toString()
meth public static java.lang.String javax.slee.management.ResourceAdaptorEntityNotification.getUsageMBeanProperties(java.lang.String)
supr java.lang.Object javax.slee.management.AbstractNotificationSource
CLSS public final javax.slee.management.ResourceAdaptorEntityState
fld  public static final int javax.slee.management.ResourceAdaptorEntityState.ENTITY_ACTIVE
fld  public static final int javax.slee.management.ResourceAdaptorEntityState.ENTITY_INACTIVE
fld  public static final int javax.slee.management.ResourceAdaptorEntityState.ENTITY_STOPPING
fld  public static final java.lang.String javax.slee.management.ResourceAdaptorEntityState.ACTIVE_STRING
fld  public static final java.lang.String javax.slee.management.ResourceAdaptorEntityState.INACTIVE_STRING
fld  public static final java.lang.String javax.slee.management.ResourceAdaptorEntityState.STOPPING_STRING
fld  public static final javax.slee.management.ResourceAdaptorEntityState javax.slee.management.ResourceAdaptorEntityState.ACTIVE
fld  public static final javax.slee.management.ResourceAdaptorEntityState javax.slee.management.ResourceAdaptorEntityState.INACTIVE
fld  public static final javax.slee.management.ResourceAdaptorEntityState javax.slee.management.ResourceAdaptorEntityState.STOPPING
intf java.io.Serializable
meth protected java.lang.Object java.lang.Object.clone() throws java.lang.CloneNotSupportedException
meth protected void java.lang.Object.finalize() throws java.lang.Throwable
meth public boolean javax.slee.management.ResourceAdaptorEntityState.equals(java.lang.Object)
meth public boolean javax.slee.management.ResourceAdaptorEntityState.isActive()
meth public boolean javax.slee.management.ResourceAdaptorEntityState.isInactive()
meth public boolean javax.slee.management.ResourceAdaptorEntityState.isStopping()
meth public final java.lang.Class java.lang.Object.getClass()
meth public final void java.lang.Object.notify()
meth public final void java.lang.Object.notifyAll()
meth public final void java.lang.Object.wait() throws java.lang.InterruptedException
meth public final void java.lang.Object.wait(long) throws java.lang.InterruptedException
meth public final void java.lang.Object.wait(long,int) throws java.lang.InterruptedException
meth public int javax.slee.management.ResourceAdaptorEntityState.hashCode()
meth public int javax.slee.management.ResourceAdaptorEntityState.toInt()
meth public java.lang.String javax.slee.management.ResourceAdaptorEntityState.toString()
meth public static javax.slee.management.ResourceAdaptorEntityState javax.slee.management.ResourceAdaptorEntityState.fromInt(int)
meth public static javax.slee.management.ResourceAdaptorEntityState javax.slee.management.ResourceAdaptorEntityState.fromString(java.lang.String)
supr java.lang.Object
CLSS public final javax.slee.management.ResourceAdaptorEntityStateChangeNotification
cons public javax.slee.management.ResourceAdaptorEntityStateChangeNotification.ResourceAdaptorEntityStateChangeNotification(javax.slee.management.ResourceManagementMBean,java.lang.String,javax.slee.management.ResourceAdaptorEntityState,javax.slee.management.ResourceAdaptorEntityState,long)
fld  protected java.lang.Object javax.management.Notification.source
intf java.io.Serializable
intf javax.slee.management.VendorExtensions
meth protected java.lang.Object java.lang.Object.clone() throws java.lang.CloneNotSupportedException
meth protected void java.lang.Object.finalize() throws java.lang.Throwable
meth public boolean java.lang.Object.equals(java.lang.Object)
meth public final java.lang.Class java.lang.Object.getClass()
meth public final void java.lang.Object.notify()
meth public final void java.lang.Object.notifyAll()
meth public final void java.lang.Object.wait() throws java.lang.InterruptedException
meth public final void java.lang.Object.wait(long) throws java.lang.InterruptedException
meth public final void java.lang.Object.wait(long,int) throws java.lang.InterruptedException
meth public int java.lang.Object.hashCode()
meth public java.lang.Object java.util.EventObject.getSource()
meth public java.lang.Object javax.management.Notification.getUserData()
meth public java.lang.Object javax.slee.management.ResourceAdaptorEntityStateChangeNotification.getVendorData()
meth public java.lang.String javax.management.Notification.getMessage()
meth public java.lang.String javax.management.Notification.getType()
meth public java.lang.String javax.slee.management.ResourceAdaptorEntityStateChangeNotification.getEntityName()
meth public java.lang.String javax.slee.management.ResourceAdaptorEntityStateChangeNotification.toString()
meth public javax.slee.management.ResourceAdaptorEntityState javax.slee.management.ResourceAdaptorEntityStateChangeNotification.getNewState()
meth public javax.slee.management.ResourceAdaptorEntityState javax.slee.management.ResourceAdaptorEntityStateChangeNotification.getOldState()
meth public long javax.management.Notification.getSequenceNumber()
meth public long javax.management.Notification.getTimeStamp()
meth public static void javax.slee.management.ResourceAdaptorEntityStateChangeNotification.disableVendorDataDeserialization()
meth public static void javax.slee.management.ResourceAdaptorEntityStateChangeNotification.disableVendorDataSerialization()
meth public static void javax.slee.management.ResourceAdaptorEntityStateChangeNotification.enableVendorDataDeserialization()
meth public static void javax.slee.management.ResourceAdaptorEntityStateChangeNotification.enableVendorDataSerialization()
meth public void javax.management.Notification.setSequenceNumber(long)
meth public void javax.management.Notification.setSource(java.lang.Object)
meth public void javax.management.Notification.setTimeStamp(long)
meth public void javax.management.Notification.setUserData(java.lang.Object)
meth public void javax.slee.management.ResourceAdaptorEntityStateChangeNotification.setVendorData(java.lang.Object)
supr java.lang.Object java.util.EventObject javax.management.Notification
CLSS public abstract interface javax.slee.management.ResourceManagementMBean
fld  public static final java.lang.String javax.slee.management.ResourceManagementMBean.OBJECT_NAME
fld  public static final java.lang.String javax.slee.management.ResourceManagementMBean.RESOURCE_ADAPTOR_ENTITY_STATE_CHANGE_NOTIFICATION_TYPE
meth public abstract [Ljava.lang.String; javax.slee.management.ResourceManagementMBean.getLinkNames() throws javax.slee.management.ManagementException
meth public abstract [Ljava.lang.String; javax.slee.management.ResourceManagementMBean.getLinkNames(java.lang.String) throws javax.slee.management.ManagementException,javax.slee.management.UnrecognizedResourceAdaptorEntityException
meth public abstract [Ljava.lang.String; javax.slee.management.ResourceManagementMBean.getResourceAdaptorEntities() throws javax.slee.management.ManagementException
meth public abstract [Ljava.lang.String; javax.slee.management.ResourceManagementMBean.getResourceAdaptorEntities([Ljava.lang.String;) throws javax.slee.management.ManagementException
meth public abstract [Ljava.lang.String; javax.slee.management.ResourceManagementMBean.getResourceAdaptorEntities(javax.slee.management.ResourceAdaptorEntityState) throws javax.slee.management.ManagementException
meth public abstract [Ljava.lang.String; javax.slee.management.ResourceManagementMBean.getResourceAdaptorEntities(javax.slee.resource.ResourceAdaptorID) throws javax.slee.management.ManagementException,javax.slee.management.UnrecognizedResourceAdaptorException
meth public abstract [Ljavax.slee.SbbID; javax.slee.management.ResourceManagementMBean.getBoundSbbs(java.lang.String) throws javax.slee.management.ManagementException,javax.slee.management.UnrecognizedLinkNameException
meth public abstract java.lang.String javax.slee.management.ResourceManagementMBean.getResourceAdaptorEntity(java.lang.String) throws javax.slee.management.ManagementException,javax.slee.management.UnrecognizedLinkNameException
meth public abstract javax.management.ObjectName javax.slee.management.ResourceManagementMBean.getResourceUsageMBean(java.lang.String) throws javax.slee.InvalidArgumentException,javax.slee.management.ManagementException,javax.slee.management.UnrecognizedResourceAdaptorEntityException
meth public abstract javax.slee.management.ResourceAdaptorEntityState javax.slee.management.ResourceManagementMBean.getState(java.lang.String) throws javax.slee.management.ManagementException,javax.slee.management.UnrecognizedResourceAdaptorEntityException
meth public abstract javax.slee.resource.ConfigProperties javax.slee.management.ResourceManagementMBean.getConfigurationProperties(java.lang.String) throws javax.slee.management.ManagementException,javax.slee.management.UnrecognizedResourceAdaptorEntityException
meth public abstract javax.slee.resource.ConfigProperties javax.slee.management.ResourceManagementMBean.getConfigurationProperties(javax.slee.resource.ResourceAdaptorID) throws javax.slee.management.ManagementException,javax.slee.management.UnrecognizedResourceAdaptorException
meth public abstract javax.slee.resource.ResourceAdaptorID javax.slee.management.ResourceManagementMBean.getResourceAdaptor(java.lang.String) throws javax.slee.management.ManagementException,javax.slee.management.UnrecognizedResourceAdaptorEntityException
meth public abstract void javax.slee.management.ResourceManagementMBean.activateResourceAdaptorEntity(java.lang.String) throws javax.slee.InvalidStateException,javax.slee.management.ManagementException,javax.slee.management.UnrecognizedResourceAdaptorEntityException
meth public abstract void javax.slee.management.ResourceManagementMBean.bindLinkName(java.lang.String,java.lang.String) throws javax.slee.InvalidArgumentException,javax.slee.management.LinkNameAlreadyBoundException,javax.slee.management.ManagementException,javax.slee.management.UnrecognizedResourceAdaptorEntityException
meth public abstract void javax.slee.management.ResourceManagementMBean.createResourceAdaptorEntity(javax.slee.resource.ResourceAdaptorID,java.lang.String,javax.slee.resource.ConfigProperties) throws javax.slee.InvalidArgumentException,javax.slee.management.ManagementException,javax.slee.management.ResourceAdaptorEntityAlreadyExistsException,javax.slee.management.UnrecognizedResourceAdaptorException,javax.slee.resource.InvalidConfigurationException
meth public abstract void javax.slee.management.ResourceManagementMBean.deactivateResourceAdaptorEntity(java.lang.String) throws javax.slee.InvalidStateException,javax.slee.management.ManagementException,javax.slee.management.UnrecognizedResourceAdaptorEntityException
meth public abstract void javax.slee.management.ResourceManagementMBean.removeResourceAdaptorEntity(java.lang.String) throws javax.slee.InvalidStateException,javax.slee.management.DependencyException,javax.slee.management.ManagementException,javax.slee.management.UnrecognizedResourceAdaptorEntityException
meth public abstract void javax.slee.management.ResourceManagementMBean.unbindLinkName(java.lang.String) throws javax.slee.management.DependencyException,javax.slee.management.ManagementException,javax.slee.management.UnrecognizedLinkNameException
meth public abstract void javax.slee.management.ResourceManagementMBean.updateConfigurationProperties(java.lang.String,javax.slee.resource.ConfigProperties) throws javax.slee.InvalidStateException,javax.slee.management.ManagementException,javax.slee.management.UnrecognizedResourceAdaptorEntityException,javax.slee.resource.InvalidConfigurationException
supr null
CLSS public abstract interface javax.slee.management.ResourceUsageMBean
fld  public static final java.lang.String javax.slee.management.ResourceUsageMBean.BASE_OBJECT_NAME
fld  public static final java.lang.String javax.slee.management.ResourceUsageMBean.RESOURCE_ADAPTOR_ENTITY_NAME_KEY
meth public abstract [Ljava.lang.String; javax.slee.management.ResourceUsageMBean.getUsageParameterSets() throws javax.slee.management.ManagementException
meth public abstract java.lang.String javax.slee.management.ResourceUsageMBean.getEntityName() throws javax.slee.management.ManagementException
meth public abstract javax.management.ObjectName javax.slee.management.ResourceUsageMBean.getUsageMBean() throws javax.slee.management.ManagementException
meth public abstract javax.management.ObjectName javax.slee.management.ResourceUsageMBean.getUsageMBean(java.lang.String) throws javax.slee.management.ManagementException,javax.slee.usage.UnrecognizedUsageParameterSetNameException
meth public abstract javax.management.ObjectName javax.slee.management.ResourceUsageMBean.getUsageNotificationManagerMBean() throws javax.slee.management.ManagementException
meth public abstract void javax.slee.management.ResourceUsageMBean.close() throws javax.slee.management.ManagementException
meth public abstract void javax.slee.management.ResourceUsageMBean.createUsageParameterSet(java.lang.String) throws javax.slee.InvalidArgumentException,javax.slee.management.ManagementException,javax.slee.management.UsageParameterSetNameAlreadyExistsException
meth public abstract void javax.slee.management.ResourceUsageMBean.removeUsageParameterSet(java.lang.String) throws javax.slee.management.ManagementException,javax.slee.usage.UnrecognizedUsageParameterSetNameException
meth public abstract void javax.slee.management.ResourceUsageMBean.resetAllUsageParameters() throws javax.slee.management.ManagementException
supr null
CLSS public javax.slee.management.SbbDescriptor
cons public javax.slee.management.SbbDescriptor.SbbDescriptor(javax.slee.SbbID,javax.slee.management.DeployableUnitID,java.lang.String,[Ljavax.slee.management.LibraryID;,[Ljavax.slee.SbbID;,[Ljavax.slee.EventTypeID;,[Ljavax.slee.profile.ProfileSpecificationID;,javax.slee.profile.ProfileSpecificationID,[Ljavax.slee.resource.ResourceAdaptorTypeID;,[Ljava.lang.String;)
intf java.io.Serializable
intf javax.slee.management.VendorExtensions
meth protected final void javax.slee.management.ComponentDescriptor.toString(java.lang.StringBuffer)
meth protected java.lang.Object java.lang.Object.clone() throws java.lang.CloneNotSupportedException
meth protected void java.lang.Object.finalize() throws java.lang.Throwable
meth public final [Ljava.lang.String; javax.slee.management.SbbDescriptor.getResourceAdaptorEntityLinks()
meth public final [Ljavax.slee.EventTypeID; javax.slee.management.SbbDescriptor.getEventTypes()
meth public final [Ljavax.slee.SbbID; javax.slee.management.SbbDescriptor.getSbbs()
meth public final [Ljavax.slee.management.LibraryID; javax.slee.management.ComponentDescriptor.getLibraries()
meth public final [Ljavax.slee.profile.ProfileSpecificationID; javax.slee.management.SbbDescriptor.getProfileSpecifications()
meth public final [Ljavax.slee.resource.ResourceAdaptorTypeID; javax.slee.management.SbbDescriptor.getResourceAdaptorTypes()
meth public final boolean javax.slee.management.ComponentDescriptor.equals(java.lang.Object)
meth public final int javax.slee.management.ComponentDescriptor.hashCode()
meth public final java.lang.Class java.lang.Object.getClass()
meth public final java.lang.String javax.slee.management.ComponentDescriptor.getName()
meth public final java.lang.String javax.slee.management.ComponentDescriptor.getSource()
meth public final java.lang.String javax.slee.management.ComponentDescriptor.getVendor()
meth public final java.lang.String javax.slee.management.ComponentDescriptor.getVersion()
meth public final javax.slee.ComponentID javax.slee.management.ComponentDescriptor.getID()
meth public final javax.slee.management.DeployableUnitID javax.slee.management.ComponentDescriptor.getDeployableUnit()
meth public final javax.slee.profile.ProfileSpecificationID javax.slee.management.SbbDescriptor.getAddressProfileSpecification()
meth public final void java.lang.Object.notify()
meth public final void java.lang.Object.notifyAll()
meth public final void java.lang.Object.wait() throws java.lang.InterruptedException
meth public final void java.lang.Object.wait(long) throws java.lang.InterruptedException
meth public final void java.lang.Object.wait(long,int) throws java.lang.InterruptedException
meth public java.lang.Object javax.slee.management.ComponentDescriptor.getVendorData()
meth public java.lang.String javax.slee.management.SbbDescriptor.toString()
meth public static void javax.slee.management.ComponentDescriptor.disableVendorDataDeserialization()
meth public static void javax.slee.management.ComponentDescriptor.disableVendorDataSerialization()
meth public static void javax.slee.management.ComponentDescriptor.enableVendorDataDeserialization()
meth public static void javax.slee.management.ComponentDescriptor.enableVendorDataSerialization()
meth public void javax.slee.management.ComponentDescriptor.setVendorData(java.lang.Object)
supr java.lang.Object javax.slee.management.ComponentDescriptor
CLSS public final javax.slee.management.SbbNotification
cons public javax.slee.management.SbbNotification.SbbNotification(javax.slee.ServiceID,javax.slee.SbbID)
fld  public static final java.lang.String javax.slee.management.SbbNotification.ALARM_NOTIFICATION_TYPE
fld  public static final java.lang.String javax.slee.management.SbbNotification.SBB_NAME_KEY
fld  public static final java.lang.String javax.slee.management.SbbNotification.SBB_VENDOR_KEY
fld  public static final java.lang.String javax.slee.management.SbbNotification.SBB_VERSION_KEY
fld  public static final java.lang.String javax.slee.management.SbbNotification.SERVICE_NAME_KEY
fld  public static final java.lang.String javax.slee.management.SbbNotification.SERVICE_VENDOR_KEY
fld  public static final java.lang.String javax.slee.management.SbbNotification.SERVICE_VERSION_KEY
fld  public static final java.lang.String javax.slee.management.SbbNotification.TRACE_NOTIFICATION_TYPE
fld  public static final java.lang.String javax.slee.management.SbbNotification.USAGE_NOTIFICATION_TYPE
intf java.io.Serializable
intf java.lang.Comparable
intf javax.slee.management.NotificationSource
meth protected final int javax.slee.management.AbstractNotificationSource.compareTo(java.lang.String,java.lang.Object)
meth protected java.lang.Object java.lang.Object.clone() throws java.lang.CloneNotSupportedException
meth protected java.lang.String javax.slee.management.SbbNotification.getClassName()
meth protected void java.lang.Object.finalize() throws java.lang.Throwable
meth public boolean javax.slee.management.SbbNotification.equals(java.lang.Object)
meth public final java.lang.Class java.lang.Object.getClass()
meth public final void java.lang.Object.notify()
meth public final void java.lang.Object.notifyAll()
meth public final void java.lang.Object.wait() throws java.lang.InterruptedException
meth public final void java.lang.Object.wait(long) throws java.lang.InterruptedException
meth public final void java.lang.Object.wait(long,int) throws java.lang.InterruptedException
meth public int javax.slee.management.SbbNotification.compareTo(java.lang.Object)
meth public int javax.slee.management.SbbNotification.hashCode()
meth public java.lang.String javax.slee.management.SbbNotification.getAlarmNotificationType()
meth public java.lang.String javax.slee.management.SbbNotification.getTraceNotificationType()
meth public java.lang.String javax.slee.management.SbbNotification.getUsageMBeanProperties()
meth public java.lang.String javax.slee.management.SbbNotification.getUsageNotificationType()
meth public java.lang.String javax.slee.management.SbbNotification.toString()
meth public javax.slee.SbbID javax.slee.management.SbbNotification.getSbb()
meth public javax.slee.ServiceID javax.slee.management.SbbNotification.getService()
meth public static java.lang.String javax.slee.management.SbbNotification.getUsageMBeanProperties(javax.slee.ServiceID,javax.slee.SbbID)
supr java.lang.Object javax.slee.management.AbstractNotificationSource
CLSS public javax.slee.management.ServiceDescriptor
cons public javax.slee.management.ServiceDescriptor.ServiceDescriptor(javax.slee.ServiceID,javax.slee.management.DeployableUnitID,java.lang.String,javax.slee.SbbID,java.lang.String,java.lang.String)
intf java.io.Serializable
intf javax.slee.management.VendorExtensions
meth protected final void javax.slee.management.ComponentDescriptor.toString(java.lang.StringBuffer)
meth protected java.lang.Object java.lang.Object.clone() throws java.lang.CloneNotSupportedException
meth protected void java.lang.Object.finalize() throws java.lang.Throwable
meth public final [Ljavax.slee.management.LibraryID; javax.slee.management.ComponentDescriptor.getLibraries()
meth public final boolean javax.slee.management.ComponentDescriptor.equals(java.lang.Object)
meth public final int javax.slee.management.ComponentDescriptor.hashCode()
meth public final java.lang.Class java.lang.Object.getClass()
meth public final java.lang.String javax.slee.management.ComponentDescriptor.getName()
meth public final java.lang.String javax.slee.management.ComponentDescriptor.getSource()
meth public final java.lang.String javax.slee.management.ComponentDescriptor.getVendor()
meth public final java.lang.String javax.slee.management.ComponentDescriptor.getVersion()
meth public final java.lang.String javax.slee.management.ServiceDescriptor.getAddressProfileTable()
meth public final java.lang.String javax.slee.management.ServiceDescriptor.getResourceInfoProfileTable()
meth public final javax.slee.ComponentID javax.slee.management.ComponentDescriptor.getID()
meth public final javax.slee.SbbID javax.slee.management.ServiceDescriptor.getRootSbb()
meth public final javax.slee.management.DeployableUnitID javax.slee.management.ComponentDescriptor.getDeployableUnit()
meth public final void java.lang.Object.notify()
meth public final void java.lang.Object.notifyAll()
meth public final void java.lang.Object.wait() throws java.lang.InterruptedException
meth public final void java.lang.Object.wait(long) throws java.lang.InterruptedException
meth public final void java.lang.Object.wait(long,int) throws java.lang.InterruptedException
meth public java.lang.Object javax.slee.management.ComponentDescriptor.getVendorData()
meth public java.lang.String javax.slee.management.ServiceDescriptor.toString()
meth public static void javax.slee.management.ComponentDescriptor.disableVendorDataDeserialization()
meth public static void javax.slee.management.ComponentDescriptor.disableVendorDataSerialization()
meth public static void javax.slee.management.ComponentDescriptor.enableVendorDataDeserialization()
meth public static void javax.slee.management.ComponentDescriptor.enableVendorDataSerialization()
meth public void javax.slee.management.ComponentDescriptor.setVendorData(java.lang.Object)
supr java.lang.Object javax.slee.management.ComponentDescriptor
CLSS public abstract interface javax.slee.management.ServiceManagementMBean
fld  public static final java.lang.String javax.slee.management.ServiceManagementMBean.OBJECT_NAME
fld  public static final java.lang.String javax.slee.management.ServiceManagementMBean.SERVICE_STATE_CHANGE_NOTIFICATION_TYPE
meth public abstract [Ljavax.slee.ServiceID; javax.slee.management.ServiceManagementMBean.getServices(javax.slee.management.ServiceState) throws javax.slee.management.ManagementException
meth public abstract javax.management.ObjectName javax.slee.management.ServiceManagementMBean.getServiceUsageMBean(javax.slee.ServiceID) throws javax.slee.UnrecognizedServiceException,javax.slee.management.ManagementException
meth public abstract javax.slee.management.ServiceState javax.slee.management.ServiceManagementMBean.getState(javax.slee.ServiceID) throws javax.slee.UnrecognizedServiceException,javax.slee.management.ManagementException
meth public abstract void javax.slee.management.ServiceManagementMBean.activate([Ljavax.slee.ServiceID;) throws javax.slee.InvalidArgumentException,javax.slee.InvalidStateException,javax.slee.UnrecognizedServiceException,javax.slee.management.ManagementException
meth public abstract void javax.slee.management.ServiceManagementMBean.activate(javax.slee.ServiceID) throws javax.slee.InvalidStateException,javax.slee.UnrecognizedServiceException,javax.slee.management.ManagementException
meth public abstract void javax.slee.management.ServiceManagementMBean.deactivate([Ljavax.slee.ServiceID;) throws javax.slee.InvalidArgumentException,javax.slee.InvalidStateException,javax.slee.UnrecognizedServiceException,javax.slee.management.ManagementException
meth public abstract void javax.slee.management.ServiceManagementMBean.deactivate(javax.slee.ServiceID) throws javax.slee.InvalidStateException,javax.slee.UnrecognizedServiceException,javax.slee.management.ManagementException
meth public abstract void javax.slee.management.ServiceManagementMBean.deactivateAndActivate([Ljavax.slee.ServiceID;,[Ljavax.slee.ServiceID;) throws javax.slee.InvalidArgumentException,javax.slee.InvalidStateException,javax.slee.UnrecognizedServiceException,javax.slee.management.ManagementException
meth public abstract void javax.slee.management.ServiceManagementMBean.deactivateAndActivate(javax.slee.ServiceID,javax.slee.ServiceID) throws javax.slee.InvalidArgumentException,javax.slee.InvalidStateException,javax.slee.UnrecognizedServiceException,javax.slee.management.ManagementException
supr null
CLSS public final javax.slee.management.ServiceState
fld  public static final int javax.slee.management.ServiceState.SERVICE_ACTIVE
fld  public static final int javax.slee.management.ServiceState.SERVICE_INACTIVE
fld  public static final int javax.slee.management.ServiceState.SERVICE_STOPPING
fld  public static final java.lang.String javax.slee.management.ServiceState.ACTIVE_STRING
fld  public static final java.lang.String javax.slee.management.ServiceState.INACTIVE_STRING
fld  public static final java.lang.String javax.slee.management.ServiceState.STOPPING_STRING
fld  public static final javax.slee.management.ServiceState javax.slee.management.ServiceState.ACTIVE
fld  public static final javax.slee.management.ServiceState javax.slee.management.ServiceState.INACTIVE
fld  public static final javax.slee.management.ServiceState javax.slee.management.ServiceState.STOPPING
intf java.io.Serializable
meth protected java.lang.Object java.lang.Object.clone() throws java.lang.CloneNotSupportedException
meth protected void java.lang.Object.finalize() throws java.lang.Throwable
meth public boolean javax.slee.management.ServiceState.equals(java.lang.Object)
meth public boolean javax.slee.management.ServiceState.isActive()
meth public boolean javax.slee.management.ServiceState.isInactive()
meth public boolean javax.slee.management.ServiceState.isStopping()
meth public final java.lang.Class java.lang.Object.getClass()
meth public final void java.lang.Object.notify()
meth public final void java.lang.Object.notifyAll()
meth public final void java.lang.Object.wait() throws java.lang.InterruptedException
meth public final void java.lang.Object.wait(long) throws java.lang.InterruptedException
meth public final void java.lang.Object.wait(long,int) throws java.lang.InterruptedException
meth public int javax.slee.management.ServiceState.hashCode()
meth public int javax.slee.management.ServiceState.toInt()
meth public java.lang.String javax.slee.management.ServiceState.toString()
meth public static javax.slee.management.ServiceState javax.slee.management.ServiceState.fromInt(int)
meth public static javax.slee.management.ServiceState javax.slee.management.ServiceState.fromString(java.lang.String)
supr java.lang.Object
CLSS public final javax.slee.management.ServiceStateChangeNotification
cons public javax.slee.management.ServiceStateChangeNotification.ServiceStateChangeNotification(javax.slee.management.ServiceManagementMBean,javax.slee.ServiceID,javax.slee.management.ServiceState,javax.slee.management.ServiceState,long)
fld  protected java.lang.Object javax.management.Notification.source
intf java.io.Serializable
intf javax.slee.management.VendorExtensions
meth protected java.lang.Object java.lang.Object.clone() throws java.lang.CloneNotSupportedException
meth protected void java.lang.Object.finalize() throws java.lang.Throwable
meth public boolean java.lang.Object.equals(java.lang.Object)
meth public final java.lang.Class java.lang.Object.getClass()
meth public final void java.lang.Object.notify()
meth public final void java.lang.Object.notifyAll()
meth public final void java.lang.Object.wait() throws java.lang.InterruptedException
meth public final void java.lang.Object.wait(long) throws java.lang.InterruptedException
meth public final void java.lang.Object.wait(long,int) throws java.lang.InterruptedException
meth public int java.lang.Object.hashCode()
meth public java.lang.Object java.util.EventObject.getSource()
meth public java.lang.Object javax.management.Notification.getUserData()
meth public java.lang.Object javax.slee.management.ServiceStateChangeNotification.getVendorData()
meth public java.lang.String javax.management.Notification.getMessage()
meth public java.lang.String javax.management.Notification.getType()
meth public java.lang.String javax.slee.management.ServiceStateChangeNotification.toString()
meth public javax.slee.ServiceID javax.slee.management.ServiceStateChangeNotification.getService()
meth public javax.slee.management.ServiceState javax.slee.management.ServiceStateChangeNotification.getNewState()
meth public javax.slee.management.ServiceState javax.slee.management.ServiceStateChangeNotification.getOldState()
meth public long javax.management.Notification.getSequenceNumber()
meth public long javax.management.Notification.getTimeStamp()
meth public static void javax.slee.management.ServiceStateChangeNotification.disableVendorDataDeserialization()
meth public static void javax.slee.management.ServiceStateChangeNotification.disableVendorDataSerialization()
meth public static void javax.slee.management.ServiceStateChangeNotification.enableVendorDataDeserialization()
meth public static void javax.slee.management.ServiceStateChangeNotification.enableVendorDataSerialization()
meth public void javax.management.Notification.setSequenceNumber(long)
meth public void javax.management.Notification.setSource(java.lang.Object)
meth public void javax.management.Notification.setTimeStamp(long)
meth public void javax.management.Notification.setUserData(java.lang.Object)
meth public void javax.slee.management.ServiceStateChangeNotification.setVendorData(java.lang.Object)
supr java.lang.Object java.util.EventObject javax.management.Notification
CLSS public abstract interface javax.slee.management.ServiceUsageMBean
fld  public static final java.lang.String javax.slee.management.ServiceUsageMBean.BASE_OBJECT_NAME
fld  public static final java.lang.String javax.slee.management.ServiceUsageMBean.SERVICE_NAME_KEY
fld  public static final java.lang.String javax.slee.management.ServiceUsageMBean.SERVICE_VENDOR_KEY
fld  public static final java.lang.String javax.slee.management.ServiceUsageMBean.SERVICE_VERSION_KEY
meth public abstract [Ljava.lang.String; javax.slee.management.ServiceUsageMBean.getUsageParameterSets(javax.slee.SbbID) throws javax.slee.InvalidArgumentException,javax.slee.UnrecognizedSbbException,javax.slee.management.ManagementException
meth public abstract javax.management.ObjectName javax.slee.management.ServiceUsageMBean.getSbbUsageMBean(javax.slee.SbbID) throws javax.slee.InvalidArgumentException,javax.slee.UnrecognizedSbbException,javax.slee.management.ManagementException
meth public abstract javax.management.ObjectName javax.slee.management.ServiceUsageMBean.getSbbUsageMBean(javax.slee.SbbID,java.lang.String) throws javax.slee.InvalidArgumentException,javax.slee.UnrecognizedSbbException,javax.slee.management.ManagementException,javax.slee.usage.UnrecognizedUsageParameterSetNameException
meth public abstract javax.management.ObjectName javax.slee.management.ServiceUsageMBean.getSbbUsageNotificationManagerMBean(javax.slee.SbbID) throws javax.slee.InvalidArgumentException,javax.slee.UnrecognizedSbbException,javax.slee.management.ManagementException
meth public abstract javax.slee.ServiceID javax.slee.management.ServiceUsageMBean.getService() throws javax.slee.management.ManagementException
meth public abstract void javax.slee.management.ServiceUsageMBean.close() throws javax.slee.management.ManagementException
meth public abstract void javax.slee.management.ServiceUsageMBean.createUsageParameterSet(javax.slee.SbbID,java.lang.String) throws javax.slee.InvalidArgumentException,javax.slee.UnrecognizedSbbException,javax.slee.management.ManagementException,javax.slee.management.UsageParameterSetNameAlreadyExistsException
meth public abstract void javax.slee.management.ServiceUsageMBean.removeUsageParameterSet(javax.slee.SbbID,java.lang.String) throws javax.slee.InvalidArgumentException,javax.slee.UnrecognizedSbbException,javax.slee.management.ManagementException,javax.slee.usage.UnrecognizedUsageParameterSetNameException
meth public abstract void javax.slee.management.ServiceUsageMBean.resetAllUsageParameters() throws javax.slee.management.ManagementException
meth public abstract void javax.slee.management.ServiceUsageMBean.resetAllUsageParameters(javax.slee.SbbID) throws javax.slee.InvalidArgumentException,javax.slee.UnrecognizedSbbException,javax.slee.management.ManagementException
supr null
CLSS public abstract interface javax.slee.management.SleeManagementMBean
fld  public static final java.lang.String javax.slee.management.SleeManagementMBean.OBJECT_NAME
fld  public static final java.lang.String javax.slee.management.SleeManagementMBean.SLEE_STATE_CHANGE_NOTIFICATION_TYPE
meth public abstract [Ljava.lang.String; javax.slee.management.SleeManagementMBean.getSubsystems() throws javax.slee.management.ManagementException
meth public abstract [Ljava.lang.String; javax.slee.management.SleeManagementMBean.getUsageParameterSets(java.lang.String) throws javax.slee.InvalidArgumentException,javax.slee.management.ManagementException,javax.slee.management.UnrecognizedSubsystemException
meth public abstract boolean javax.slee.management.SleeManagementMBean.hasUsage(java.lang.String) throws javax.slee.management.ManagementException,javax.slee.management.UnrecognizedSubsystemException
meth public abstract java.lang.String javax.slee.management.SleeManagementMBean.getSleeName()
meth public abstract java.lang.String javax.slee.management.SleeManagementMBean.getSleeVendor()
meth public abstract java.lang.String javax.slee.management.SleeManagementMBean.getSleeVersion()
meth public abstract javax.management.ObjectName javax.slee.management.SleeManagementMBean.getAlarmMBean()
meth public abstract javax.management.ObjectName javax.slee.management.SleeManagementMBean.getDeploymentMBean()
meth public abstract javax.management.ObjectName javax.slee.management.SleeManagementMBean.getProfileProvisioningMBean()
meth public abstract javax.management.ObjectName javax.slee.management.SleeManagementMBean.getResourceManagementMBean()
meth public abstract javax.management.ObjectName javax.slee.management.SleeManagementMBean.getServiceManagementMBean()
meth public abstract javax.management.ObjectName javax.slee.management.SleeManagementMBean.getTraceMBean()
meth public abstract javax.management.ObjectName javax.slee.management.SleeManagementMBean.getUsageMBean(java.lang.String) throws javax.slee.InvalidArgumentException,javax.slee.management.ManagementException,javax.slee.management.UnrecognizedSubsystemException
meth public abstract javax.management.ObjectName javax.slee.management.SleeManagementMBean.getUsageMBean(java.lang.String,java.lang.String) throws javax.slee.InvalidArgumentException,javax.slee.management.ManagementException,javax.slee.management.UnrecognizedSubsystemException,javax.slee.usage.UnrecognizedUsageParameterSetNameException
meth public abstract javax.management.ObjectName javax.slee.management.SleeManagementMBean.getUsageNotificationManagerMBean(java.lang.String) throws javax.slee.InvalidArgumentException,javax.slee.management.ManagementException,javax.slee.management.UnrecognizedSubsystemException
meth public abstract javax.slee.management.SleeState javax.slee.management.SleeManagementMBean.getState() throws javax.slee.management.ManagementException
meth public abstract void javax.slee.management.SleeManagementMBean.shutdown() throws javax.slee.InvalidStateException,javax.slee.management.ManagementException
meth public abstract void javax.slee.management.SleeManagementMBean.start() throws javax.slee.InvalidStateException,javax.slee.management.ManagementException
meth public abstract void javax.slee.management.SleeManagementMBean.stop() throws javax.slee.InvalidStateException,javax.slee.management.ManagementException
supr null
CLSS public abstract interface javax.slee.management.SleeProvider
meth public abstract javax.management.ObjectName javax.slee.management.SleeProvider.getSleeManagementMBean()
supr null
CLSS public final javax.slee.management.SleeProviderFactory
cons public javax.slee.management.SleeProviderFactory.SleeProviderFactory()
meth protected java.lang.Object java.lang.Object.clone() throws java.lang.CloneNotSupportedException
meth protected void java.lang.Object.finalize() throws java.lang.Throwable
meth public boolean java.lang.Object.equals(java.lang.Object)
meth public final java.lang.Class java.lang.Object.getClass()
meth public final void java.lang.Object.notify()
meth public final void java.lang.Object.notifyAll()
meth public final void java.lang.Object.wait() throws java.lang.InterruptedException
meth public final void java.lang.Object.wait(long) throws java.lang.InterruptedException
meth public final void java.lang.Object.wait(long,int) throws java.lang.InterruptedException
meth public int java.lang.Object.hashCode()
meth public java.lang.String java.lang.Object.toString()
meth public static javax.slee.management.SleeProvider javax.slee.management.SleeProviderFactory.getSleeProvider(java.lang.String) throws javax.slee.management.PeerUnavailableException
meth public static javax.slee.management.SleeProvider javax.slee.management.SleeProviderFactory.getSleeProvider(java.lang.String,java.lang.ClassLoader) throws javax.slee.management.PeerUnavailableException
supr java.lang.Object
CLSS public final javax.slee.management.SleeState
fld  public static final int javax.slee.management.SleeState.SLEE_RUNNING
fld  public static final int javax.slee.management.SleeState.SLEE_STARTING
fld  public static final int javax.slee.management.SleeState.SLEE_STOPPED
fld  public static final int javax.slee.management.SleeState.SLEE_STOPPING
fld  public static final java.lang.String javax.slee.management.SleeState.RUNNING_STRING
fld  public static final java.lang.String javax.slee.management.SleeState.STARTING_STRING
fld  public static final java.lang.String javax.slee.management.SleeState.STOPPED_STRING
fld  public static final java.lang.String javax.slee.management.SleeState.STOPPING_STRING
fld  public static final javax.slee.management.SleeState javax.slee.management.SleeState.RUNNING
fld  public static final javax.slee.management.SleeState javax.slee.management.SleeState.STARTING
fld  public static final javax.slee.management.SleeState javax.slee.management.SleeState.STOPPED
fld  public static final javax.slee.management.SleeState javax.slee.management.SleeState.STOPPING
intf java.io.Serializable
meth protected java.lang.Object java.lang.Object.clone() throws java.lang.CloneNotSupportedException
meth protected void java.lang.Object.finalize() throws java.lang.Throwable
meth public boolean javax.slee.management.SleeState.equals(java.lang.Object)
meth public boolean javax.slee.management.SleeState.isRunning()
meth public boolean javax.slee.management.SleeState.isStarting()
meth public boolean javax.slee.management.SleeState.isStopped()
meth public boolean javax.slee.management.SleeState.isStopping()
meth public final java.lang.Class java.lang.Object.getClass()
meth public final void java.lang.Object.notify()
meth public final void java.lang.Object.notifyAll()
meth public final void java.lang.Object.wait() throws java.lang.InterruptedException
meth public final void java.lang.Object.wait(long) throws java.lang.InterruptedException
meth public final void java.lang.Object.wait(long,int) throws java.lang.InterruptedException
meth public int javax.slee.management.SleeState.hashCode()
meth public int javax.slee.management.SleeState.toInt()
meth public java.lang.String javax.slee.management.SleeState.toString()
meth public static javax.slee.management.SleeState javax.slee.management.SleeState.fromInt(int)
meth public static javax.slee.management.SleeState javax.slee.management.SleeState.fromString(java.lang.String)
supr java.lang.Object
CLSS public final javax.slee.management.SleeStateChangeNotification
cons public javax.slee.management.SleeStateChangeNotification.SleeStateChangeNotification(javax.slee.management.SleeManagementMBean,javax.slee.management.SleeState,javax.slee.management.SleeState,long)
fld  protected java.lang.Object javax.management.Notification.source
intf java.io.Serializable
intf javax.slee.management.VendorExtensions
meth protected java.lang.Object java.lang.Object.clone() throws java.lang.CloneNotSupportedException
meth protected void java.lang.Object.finalize() throws java.lang.Throwable
meth public boolean java.lang.Object.equals(java.lang.Object)
meth public final java.lang.Class java.lang.Object.getClass()
meth public final void java.lang.Object.notify()
meth public final void java.lang.Object.notifyAll()
meth public final void java.lang.Object.wait() throws java.lang.InterruptedException
meth public final void java.lang.Object.wait(long) throws java.lang.InterruptedException
meth public final void java.lang.Object.wait(long,int) throws java.lang.InterruptedException
meth public int java.lang.Object.hashCode()
meth public java.lang.Object java.util.EventObject.getSource()
meth public java.lang.Object javax.management.Notification.getUserData()
meth public java.lang.Object javax.slee.management.SleeStateChangeNotification.getVendorData()
meth public java.lang.String javax.management.Notification.getMessage()
meth public java.lang.String javax.management.Notification.getType()
meth public java.lang.String javax.slee.management.SleeStateChangeNotification.toString()
meth public javax.slee.management.SleeState javax.slee.management.SleeStateChangeNotification.getNewState()
meth public javax.slee.management.SleeState javax.slee.management.SleeStateChangeNotification.getOldState()
meth public long javax.management.Notification.getSequenceNumber()
meth public long javax.management.Notification.getTimeStamp()
meth public static void javax.slee.management.SleeStateChangeNotification.disableVendorDataDeserialization()
meth public static void javax.slee.management.SleeStateChangeNotification.disableVendorDataSerialization()
meth public static void javax.slee.management.SleeStateChangeNotification.enableVendorDataDeserialization()
meth public static void javax.slee.management.SleeStateChangeNotification.enableVendorDataSerialization()
meth public void javax.management.Notification.setSequenceNumber(long)
meth public void javax.management.Notification.setSource(java.lang.Object)
meth public void javax.management.Notification.setTimeStamp(long)
meth public void javax.management.Notification.setUserData(java.lang.Object)
meth public void javax.slee.management.SleeStateChangeNotification.setVendorData(java.lang.Object)
supr java.lang.Object java.util.EventObject javax.management.Notification
CLSS public final javax.slee.management.SubsystemNotification
cons public javax.slee.management.SubsystemNotification.SubsystemNotification(java.lang.String)
fld  public static final java.lang.String javax.slee.management.SubsystemNotification.ALARM_NOTIFICATION_TYPE
fld  public static final java.lang.String javax.slee.management.SubsystemNotification.SUBSYSTEM_NAME_KEY
fld  public static final java.lang.String javax.slee.management.SubsystemNotification.TRACE_NOTIFICATION_TYPE
fld  public static final java.lang.String javax.slee.management.SubsystemNotification.USAGE_NOTIFICATION_TYPE
intf java.io.Serializable
intf java.lang.Comparable
intf javax.slee.management.NotificationSource
meth protected final int javax.slee.management.AbstractNotificationSource.compareTo(java.lang.String,java.lang.Object)
meth protected java.lang.Object java.lang.Object.clone() throws java.lang.CloneNotSupportedException
meth protected java.lang.String javax.slee.management.SubsystemNotification.getClassName()
meth protected void java.lang.Object.finalize() throws java.lang.Throwable
meth public boolean javax.slee.management.SubsystemNotification.equals(java.lang.Object)
meth public final java.lang.Class java.lang.Object.getClass()
meth public final void java.lang.Object.notify()
meth public final void java.lang.Object.notifyAll()
meth public final void java.lang.Object.wait() throws java.lang.InterruptedException
meth public final void java.lang.Object.wait(long) throws java.lang.InterruptedException
meth public final void java.lang.Object.wait(long,int) throws java.lang.InterruptedException
meth public int javax.slee.management.SubsystemNotification.compareTo(java.lang.Object)
meth public int javax.slee.management.SubsystemNotification.hashCode()
meth public java.lang.String javax.slee.management.SubsystemNotification.getAlarmNotificationType()
meth public java.lang.String javax.slee.management.SubsystemNotification.getSubsystemName()
meth public java.lang.String javax.slee.management.SubsystemNotification.getTraceNotificationType()
meth public java.lang.String javax.slee.management.SubsystemNotification.getUsageMBeanProperties()
meth public java.lang.String javax.slee.management.SubsystemNotification.getUsageNotificationType()
meth public java.lang.String javax.slee.management.SubsystemNotification.toString()
meth public static java.lang.String javax.slee.management.SubsystemNotification.getUsageMBeanProperties(java.lang.String)
supr java.lang.Object javax.slee.management.AbstractNotificationSource
CLSS public javax.slee.management.TraceLevelFilter
cons public javax.slee.management.TraceLevelFilter.TraceLevelFilter(javax.slee.facilities.Level)
cons public javax.slee.management.TraceLevelFilter.TraceLevelFilter(javax.slee.facilities.TraceLevel)
intf java.io.Serializable
intf javax.management.NotificationFilter
meth protected java.lang.Object java.lang.Object.clone() throws java.lang.CloneNotSupportedException
meth protected void java.lang.Object.finalize() throws java.lang.Throwable
meth public boolean java.lang.Object.equals(java.lang.Object)
meth public boolean javax.slee.management.TraceLevelFilter.isNotificationEnabled(javax.management.Notification)
meth public final java.lang.Class java.lang.Object.getClass()
meth public final void java.lang.Object.notify()
meth public final void java.lang.Object.notifyAll()
meth public final void java.lang.Object.wait() throws java.lang.InterruptedException
meth public final void java.lang.Object.wait(long) throws java.lang.InterruptedException
meth public final void java.lang.Object.wait(long,int) throws java.lang.InterruptedException
meth public int java.lang.Object.hashCode()
meth public java.lang.String java.lang.Object.toString()
supr java.lang.Object
CLSS public abstract interface javax.slee.management.TraceMBean
fld  public static final java.lang.String javax.slee.management.TraceMBean.OBJECT_NAME
fld  public static final java.lang.String javax.slee.management.TraceMBean.TRACE_NOTIFICATION_TYPE
meth public abstract [Ljava.lang.String; javax.slee.management.TraceMBean.getTracersSet(javax.slee.management.NotificationSource) throws javax.slee.management.ManagementException,javax.slee.management.UnrecognizedNotificationSourceException
meth public abstract [Ljava.lang.String; javax.slee.management.TraceMBean.getTracersUsed(javax.slee.management.NotificationSource) throws javax.slee.management.ManagementException,javax.slee.management.UnrecognizedNotificationSourceException
meth public abstract javax.slee.facilities.Level javax.slee.management.TraceMBean.getTraceLevel(javax.slee.ComponentID) throws javax.slee.UnrecognizedComponentException,javax.slee.management.ManagementException
meth public abstract javax.slee.facilities.TraceLevel javax.slee.management.TraceMBean.getTraceLevel(javax.slee.management.NotificationSource,java.lang.String) throws javax.slee.InvalidArgumentException,javax.slee.management.ManagementException,javax.slee.management.UnrecognizedNotificationSourceException
meth public abstract void javax.slee.management.TraceMBean.setTraceLevel(javax.slee.ComponentID,javax.slee.facilities.Level) throws javax.slee.UnrecognizedComponentException,javax.slee.management.ManagementException
meth public abstract void javax.slee.management.TraceMBean.setTraceLevel(javax.slee.management.NotificationSource,java.lang.String,javax.slee.facilities.TraceLevel) throws javax.slee.InvalidArgumentException,javax.slee.management.ManagementException,javax.slee.management.UnrecognizedNotificationSourceException
meth public abstract void javax.slee.management.TraceMBean.unsetTraceLevel(javax.slee.management.NotificationSource,java.lang.String) throws javax.slee.InvalidArgumentException,javax.slee.management.ManagementException,javax.slee.management.UnrecognizedNotificationSourceException
supr null
CLSS public final javax.slee.management.TraceNotification
cons public javax.slee.management.TraceNotification.TraceNotification(java.lang.String,javax.slee.management.TraceMBean,javax.slee.management.NotificationSource,java.lang.String,javax.slee.facilities.TraceLevel,java.lang.String,java.lang.Throwable,long,long)
cons public javax.slee.management.TraceNotification.TraceNotification(javax.slee.management.TraceMBean,java.lang.String,java.lang.Object,javax.slee.facilities.Level,java.lang.String,java.lang.Throwable,long,long)
fld  protected java.lang.Object javax.management.Notification.source
intf java.io.Serializable
intf javax.slee.management.VendorExtensions
meth protected java.lang.Object java.lang.Object.clone() throws java.lang.CloneNotSupportedException
meth protected void java.lang.Object.finalize() throws java.lang.Throwable
meth public boolean javax.slee.management.TraceNotification.equals(java.lang.Object)
meth public final java.lang.Class java.lang.Object.getClass()
meth public final void java.lang.Object.notify()
meth public final void java.lang.Object.notifyAll()
meth public final void java.lang.Object.wait() throws java.lang.InterruptedException
meth public final void java.lang.Object.wait(long) throws java.lang.InterruptedException
meth public final void java.lang.Object.wait(long,int) throws java.lang.InterruptedException
meth public int javax.slee.management.TraceNotification.hashCode()
meth public java.lang.Object java.util.EventObject.getSource()
meth public java.lang.Object javax.management.Notification.getUserData()
meth public java.lang.Object javax.slee.management.TraceNotification.getMessageSource()
meth public java.lang.Object javax.slee.management.TraceNotification.getVendorData()
meth public java.lang.String javax.management.Notification.getMessage()
meth public java.lang.String javax.management.Notification.getType()
meth public java.lang.String javax.slee.management.TraceNotification.getMessageType()
meth public java.lang.String javax.slee.management.TraceNotification.getTracerName()
meth public java.lang.String javax.slee.management.TraceNotification.toString()
meth public java.lang.Throwable javax.slee.management.TraceNotification.getCause()
meth public javax.slee.facilities.Level javax.slee.management.TraceNotification.getLevel()
meth public javax.slee.facilities.TraceLevel javax.slee.management.TraceNotification.getTraceLevel()
meth public javax.slee.management.NotificationSource javax.slee.management.TraceNotification.getNotificationSource()
meth public long javax.management.Notification.getSequenceNumber()
meth public long javax.management.Notification.getTimeStamp()
meth public static void javax.slee.management.TraceNotification.disableVendorDataDeserialization()
meth public static void javax.slee.management.TraceNotification.disableVendorDataSerialization()
meth public static void javax.slee.management.TraceNotification.enableVendorDataDeserialization()
meth public static void javax.slee.management.TraceNotification.enableVendorDataSerialization()
meth public void javax.management.Notification.setSequenceNumber(long)
meth public void javax.management.Notification.setSource(java.lang.Object)
meth public void javax.management.Notification.setTimeStamp(long)
meth public void javax.management.Notification.setUserData(java.lang.Object)
meth public void javax.slee.management.TraceNotification.setVendorData(java.lang.Object)
supr java.lang.Object java.util.EventObject javax.management.Notification
CLSS public javax.slee.management.UnrecognizedDeployableUnitException
cons public javax.slee.management.UnrecognizedDeployableUnitException.UnrecognizedDeployableUnitException()
cons public javax.slee.management.UnrecognizedDeployableUnitException.UnrecognizedDeployableUnitException(java.lang.String)
intf java.io.Serializable
meth protected java.lang.Object java.lang.Object.clone() throws java.lang.CloneNotSupportedException
meth protected void java.lang.Object.finalize() throws java.lang.Throwable
meth public [Ljava.lang.StackTraceElement; java.lang.Throwable.getStackTrace()
meth public boolean java.lang.Object.equals(java.lang.Object)
meth public final java.lang.Class java.lang.Object.getClass()
meth public final void java.lang.Object.notify()
meth public final void java.lang.Object.notifyAll()
meth public final void java.lang.Object.wait() throws java.lang.InterruptedException
meth public final void java.lang.Object.wait(long) throws java.lang.InterruptedException
meth public final void java.lang.Object.wait(long,int) throws java.lang.InterruptedException
meth public int java.lang.Object.hashCode()
meth public java.lang.String java.lang.Throwable.getLocalizedMessage()
meth public java.lang.String java.lang.Throwable.getMessage()
meth public java.lang.String java.lang.Throwable.toString()
meth public java.lang.Throwable java.lang.Throwable.fillInStackTrace()
meth public java.lang.Throwable java.lang.Throwable.getCause()
meth public java.lang.Throwable java.lang.Throwable.initCause(java.lang.Throwable)
meth public void java.lang.Throwable.printStackTrace()
meth public void java.lang.Throwable.printStackTrace(java.io.PrintStream)
meth public void java.lang.Throwable.printStackTrace(java.io.PrintWriter)
meth public void java.lang.Throwable.setStackTrace([Ljava.lang.StackTraceElement;)
supr java.lang.Object java.lang.Throwable java.lang.Exception
CLSS public javax.slee.management.UnrecognizedLinkNameException
cons public javax.slee.management.UnrecognizedLinkNameException.UnrecognizedLinkNameException()
cons public javax.slee.management.UnrecognizedLinkNameException.UnrecognizedLinkNameException(java.lang.String)
intf java.io.Serializable
meth protected java.lang.Object java.lang.Object.clone() throws java.lang.CloneNotSupportedException
meth protected void java.lang.Object.finalize() throws java.lang.Throwable
meth public [Ljava.lang.StackTraceElement; java.lang.Throwable.getStackTrace()
meth public boolean java.lang.Object.equals(java.lang.Object)
meth public final java.lang.Class java.lang.Object.getClass()
meth public final void java.lang.Object.notify()
meth public final void java.lang.Object.notifyAll()
meth public final void java.lang.Object.wait() throws java.lang.InterruptedException
meth public final void java.lang.Object.wait(long) throws java.lang.InterruptedException
meth public final void java.lang.Object.wait(long,int) throws java.lang.InterruptedException
meth public int java.lang.Object.hashCode()
meth public java.lang.String java.lang.Throwable.getLocalizedMessage()
meth public java.lang.String java.lang.Throwable.getMessage()
meth public java.lang.String java.lang.Throwable.toString()
meth public java.lang.Throwable java.lang.Throwable.fillInStackTrace()
meth public java.lang.Throwable java.lang.Throwable.getCause()
meth public java.lang.Throwable java.lang.Throwable.initCause(java.lang.Throwable)
meth public void java.lang.Throwable.printStackTrace()
meth public void java.lang.Throwable.printStackTrace(java.io.PrintStream)
meth public void java.lang.Throwable.printStackTrace(java.io.PrintWriter)
meth public void java.lang.Throwable.setStackTrace([Ljava.lang.StackTraceElement;)
supr java.lang.Object java.lang.Throwable java.lang.Exception
CLSS public javax.slee.management.UnrecognizedNotificationSourceException
cons public javax.slee.management.UnrecognizedNotificationSourceException.UnrecognizedNotificationSourceException()
cons public javax.slee.management.UnrecognizedNotificationSourceException.UnrecognizedNotificationSourceException(java.lang.String)
intf java.io.Serializable
meth protected java.lang.Object java.lang.Object.clone() throws java.lang.CloneNotSupportedException
meth protected void java.lang.Object.finalize() throws java.lang.Throwable
meth public [Ljava.lang.StackTraceElement; java.lang.Throwable.getStackTrace()
meth public boolean java.lang.Object.equals(java.lang.Object)
meth public final java.lang.Class java.lang.Object.getClass()
meth public final void java.lang.Object.notify()
meth public final void java.lang.Object.notifyAll()
meth public final void java.lang.Object.wait() throws java.lang.InterruptedException
meth public final void java.lang.Object.wait(long) throws java.lang.InterruptedException
meth public final void java.lang.Object.wait(long,int) throws java.lang.InterruptedException
meth public int java.lang.Object.hashCode()
meth public java.lang.String java.lang.Throwable.getLocalizedMessage()
meth public java.lang.String java.lang.Throwable.getMessage()
meth public java.lang.String java.lang.Throwable.toString()
meth public java.lang.Throwable java.lang.Throwable.fillInStackTrace()
meth public java.lang.Throwable java.lang.Throwable.getCause()
meth public java.lang.Throwable java.lang.Throwable.initCause(java.lang.Throwable)
meth public void java.lang.Throwable.printStackTrace()
meth public void java.lang.Throwable.printStackTrace(java.io.PrintStream)
meth public void java.lang.Throwable.printStackTrace(java.io.PrintWriter)
meth public void java.lang.Throwable.setStackTrace([Ljava.lang.StackTraceElement;)
supr java.lang.Object java.lang.Throwable java.lang.Exception
CLSS public javax.slee.management.UnrecognizedResourceAdaptorEntityException
cons public javax.slee.management.UnrecognizedResourceAdaptorEntityException.UnrecognizedResourceAdaptorEntityException()
cons public javax.slee.management.UnrecognizedResourceAdaptorEntityException.UnrecognizedResourceAdaptorEntityException(java.lang.String)
intf java.io.Serializable
meth protected java.lang.Object java.lang.Object.clone() throws java.lang.CloneNotSupportedException
meth protected void java.lang.Object.finalize() throws java.lang.Throwable
meth public [Ljava.lang.StackTraceElement; java.lang.Throwable.getStackTrace()
meth public boolean java.lang.Object.equals(java.lang.Object)
meth public final java.lang.Class java.lang.Object.getClass()
meth public final void java.lang.Object.notify()
meth public final void java.lang.Object.notifyAll()
meth public final void java.lang.Object.wait() throws java.lang.InterruptedException
meth public final void java.lang.Object.wait(long) throws java.lang.InterruptedException
meth public final void java.lang.Object.wait(long,int) throws java.lang.InterruptedException
meth public int java.lang.Object.hashCode()
meth public java.lang.String java.lang.Throwable.getLocalizedMessage()
meth public java.lang.String java.lang.Throwable.getMessage()
meth public java.lang.String java.lang.Throwable.toString()
meth public java.lang.Throwable java.lang.Throwable.fillInStackTrace()
meth public java.lang.Throwable java.lang.Throwable.getCause()
meth public java.lang.Throwable java.lang.Throwable.initCause(java.lang.Throwable)
meth public void java.lang.Throwable.printStackTrace()
meth public void java.lang.Throwable.printStackTrace(java.io.PrintStream)
meth public void java.lang.Throwable.printStackTrace(java.io.PrintWriter)
meth public void java.lang.Throwable.setStackTrace([Ljava.lang.StackTraceElement;)
supr java.lang.Object java.lang.Throwable java.lang.Exception
CLSS public javax.slee.management.UnrecognizedResourceAdaptorException
cons public javax.slee.management.UnrecognizedResourceAdaptorException.UnrecognizedResourceAdaptorException()
cons public javax.slee.management.UnrecognizedResourceAdaptorException.UnrecognizedResourceAdaptorException(java.lang.String)
intf java.io.Serializable
meth protected java.lang.Object java.lang.Object.clone() throws java.lang.CloneNotSupportedException
meth protected void java.lang.Object.finalize() throws java.lang.Throwable
meth public [Ljava.lang.StackTraceElement; java.lang.Throwable.getStackTrace()
meth public boolean java.lang.Object.equals(java.lang.Object)
meth public final java.lang.Class java.lang.Object.getClass()
meth public final void java.lang.Object.notify()
meth public final void java.lang.Object.notifyAll()
meth public final void java.lang.Object.wait() throws java.lang.InterruptedException
meth public final void java.lang.Object.wait(long) throws java.lang.InterruptedException
meth public final void java.lang.Object.wait(long,int) throws java.lang.InterruptedException
meth public int java.lang.Object.hashCode()
meth public java.lang.String java.lang.Throwable.getLocalizedMessage()
meth public java.lang.String java.lang.Throwable.getMessage()
meth public java.lang.String java.lang.Throwable.toString()
meth public java.lang.Throwable java.lang.Throwable.fillInStackTrace()
meth public java.lang.Throwable java.lang.Throwable.getCause()
meth public java.lang.Throwable java.lang.Throwable.initCause(java.lang.Throwable)
meth public void java.lang.Throwable.printStackTrace()
meth public void java.lang.Throwable.printStackTrace(java.io.PrintStream)
meth public void java.lang.Throwable.printStackTrace(java.io.PrintWriter)
meth public void java.lang.Throwable.setStackTrace([Ljava.lang.StackTraceElement;)
supr java.lang.Object java.lang.Throwable java.lang.Exception javax.slee.UnrecognizedComponentException
CLSS public javax.slee.management.UnrecognizedSubsystemException
cons public javax.slee.management.UnrecognizedSubsystemException.UnrecognizedSubsystemException()
cons public javax.slee.management.UnrecognizedSubsystemException.UnrecognizedSubsystemException(java.lang.String)
intf java.io.Serializable
meth protected java.lang.Object java.lang.Object.clone() throws java.lang.CloneNotSupportedException
meth protected void java.lang.Object.finalize() throws java.lang.Throwable
meth public [Ljava.lang.StackTraceElement; java.lang.Throwable.getStackTrace()
meth public boolean java.lang.Object.equals(java.lang.Object)
meth public final java.lang.Class java.lang.Object.getClass()
meth public final void java.lang.Object.notify()
meth public final void java.lang.Object.notifyAll()
meth public final void java.lang.Object.wait() throws java.lang.InterruptedException
meth public final void java.lang.Object.wait(long) throws java.lang.InterruptedException
meth public final void java.lang.Object.wait(long,int) throws java.lang.InterruptedException
meth public int java.lang.Object.hashCode()
meth public java.lang.String java.lang.Throwable.getLocalizedMessage()
meth public java.lang.String java.lang.Throwable.getMessage()
meth public java.lang.String java.lang.Throwable.toString()
meth public java.lang.Throwable java.lang.Throwable.fillInStackTrace()
meth public java.lang.Throwable java.lang.Throwable.getCause()
meth public java.lang.Throwable java.lang.Throwable.initCause(java.lang.Throwable)
meth public void java.lang.Throwable.printStackTrace()
meth public void java.lang.Throwable.printStackTrace(java.io.PrintStream)
meth public void java.lang.Throwable.printStackTrace(java.io.PrintWriter)
meth public void java.lang.Throwable.setStackTrace([Ljava.lang.StackTraceElement;)
supr java.lang.Object java.lang.Throwable java.lang.Exception
CLSS public javax.slee.management.UsageParameterSetNameAlreadyExistsException
cons public javax.slee.management.UsageParameterSetNameAlreadyExistsException.UsageParameterSetNameAlreadyExistsException()
cons public javax.slee.management.UsageParameterSetNameAlreadyExistsException.UsageParameterSetNameAlreadyExistsException(java.lang.String)
intf java.io.Serializable
meth protected java.lang.Object java.lang.Object.clone() throws java.lang.CloneNotSupportedException
meth protected void java.lang.Object.finalize() throws java.lang.Throwable
meth public [Ljava.lang.StackTraceElement; java.lang.Throwable.getStackTrace()
meth public boolean java.lang.Object.equals(java.lang.Object)
meth public final java.lang.Class java.lang.Object.getClass()
meth public final void java.lang.Object.notify()
meth public final void java.lang.Object.notifyAll()
meth public final void java.lang.Object.wait() throws java.lang.InterruptedException
meth public final void java.lang.Object.wait(long) throws java.lang.InterruptedException
meth public final void java.lang.Object.wait(long,int) throws java.lang.InterruptedException
meth public int java.lang.Object.hashCode()
meth public java.lang.String java.lang.Throwable.getLocalizedMessage()
meth public java.lang.String java.lang.Throwable.getMessage()
meth public java.lang.String java.lang.Throwable.toString()
meth public java.lang.Throwable java.lang.Throwable.fillInStackTrace()
meth public java.lang.Throwable java.lang.Throwable.getCause()
meth public java.lang.Throwable java.lang.Throwable.initCause(java.lang.Throwable)
meth public void java.lang.Throwable.printStackTrace()
meth public void java.lang.Throwable.printStackTrace(java.io.PrintStream)
meth public void java.lang.Throwable.printStackTrace(java.io.PrintWriter)
meth public void java.lang.Throwable.setStackTrace([Ljava.lang.StackTraceElement;)
supr java.lang.Object java.lang.Throwable java.lang.Exception
CLSS public javax.slee.management.VendorExtensionUtils
cons public javax.slee.management.VendorExtensionUtils.VendorExtensionUtils()
meth protected java.lang.Object java.lang.Object.clone() throws java.lang.CloneNotSupportedException
meth protected void java.lang.Object.finalize() throws java.lang.Throwable
meth public boolean java.lang.Object.equals(java.lang.Object)
meth public final java.lang.Class java.lang.Object.getClass()
meth public final void java.lang.Object.notify()
meth public final void java.lang.Object.notifyAll()
meth public final void java.lang.Object.wait() throws java.lang.InterruptedException
meth public final void java.lang.Object.wait(long) throws java.lang.InterruptedException
meth public final void java.lang.Object.wait(long,int) throws java.lang.InterruptedException
meth public int java.lang.Object.hashCode()
meth public java.lang.String java.lang.Object.toString()
meth public static java.lang.Object javax.slee.management.VendorExtensionUtils.readObject(java.io.ObjectInputStream,boolean) throws java.io.IOException,java.lang.ClassNotFoundException
meth public static void javax.slee.management.VendorExtensionUtils.writeObject(java.io.ObjectOutputStream,java.lang.Object) throws java.io.IOException
supr java.lang.Object
CLSS public abstract interface javax.slee.management.VendorExtensions
meth public abstract java.lang.Object javax.slee.management.VendorExtensions.getVendorData()
meth public abstract void javax.slee.management.VendorExtensions.setVendorData(java.lang.Object)
supr null
CLSS public abstract interface javax.slee.nullactivity.NullActivity
meth public abstract void javax.slee.nullactivity.NullActivity.endActivity()
supr null
CLSS public abstract interface javax.slee.nullactivity.NullActivityContextInterfaceFactory
fld  public static final java.lang.String javax.slee.nullactivity.NullActivityContextInterfaceFactory.JNDI_NAME
meth public abstract javax.slee.ActivityContextInterface javax.slee.nullactivity.NullActivityContextInterfaceFactory.getActivityContextInterface(javax.slee.nullactivity.NullActivity)
supr null
CLSS public abstract interface javax.slee.nullactivity.NullActivityFactory
fld  public static final java.lang.String javax.slee.nullactivity.NullActivityFactory.JNDI_NAME
meth public abstract javax.slee.nullactivity.NullActivity javax.slee.nullactivity.NullActivityFactory.createNullActivity()
supr null
CLSS public abstract interface javax.slee.profile.AddressProfile11CMP
meth public abstract javax.slee.Address javax.slee.profile.AddressProfile11CMP.getAddress()
meth public abstract void javax.slee.profile.AddressProfile11CMP.setAddress(javax.slee.Address)
supr null
CLSS public abstract interface javax.slee.profile.AddressProfileCMP
meth public abstract [Ljavax.slee.Address; javax.slee.profile.AddressProfileCMP.getAddresses()
meth public abstract void javax.slee.profile.AddressProfileCMP.setAddresses([Ljavax.slee.Address;)
supr null
CLSS public abstract interface javax.slee.profile.AddressProfileLocal
intf javax.slee.profile.AddressProfile11CMP
intf javax.slee.profile.ProfileLocalObject
meth public abstract boolean javax.slee.profile.ProfileLocalObject.isIdentical(javax.slee.profile.ProfileLocalObject)
meth public abstract java.lang.String javax.slee.profile.ProfileLocalObject.getProfileName()
meth public abstract java.lang.String javax.slee.profile.ProfileLocalObject.getProfileTableName()
meth public abstract javax.slee.Address javax.slee.profile.AddressProfile11CMP.getAddress()
meth public abstract javax.slee.profile.ProfileTable javax.slee.profile.ProfileLocalObject.getProfileTable()
meth public abstract void javax.slee.profile.AddressProfile11CMP.setAddress(javax.slee.Address)
meth public abstract void javax.slee.profile.ProfileLocalObject.remove()
supr null
CLSS public abstract interface javax.slee.profile.AddressProfileManagement
intf javax.slee.profile.AddressProfile11CMP
meth public abstract javax.slee.Address javax.slee.profile.AddressProfile11CMP.getAddress()
meth public abstract void javax.slee.profile.AddressProfile11CMP.setAddress(javax.slee.Address)
supr null
CLSS public javax.slee.profile.AttributeNotIndexedException
cons public javax.slee.profile.AttributeNotIndexedException.AttributeNotIndexedException()
cons public javax.slee.profile.AttributeNotIndexedException.AttributeNotIndexedException(java.lang.String)
intf java.io.Serializable
meth protected java.lang.Object java.lang.Object.clone() throws java.lang.CloneNotSupportedException
meth protected void java.lang.Object.finalize() throws java.lang.Throwable
meth public [Ljava.lang.StackTraceElement; java.lang.Throwable.getStackTrace()
meth public boolean java.lang.Object.equals(java.lang.Object)
meth public final java.lang.Class java.lang.Object.getClass()
meth public final void java.lang.Object.notify()
meth public final void java.lang.Object.notifyAll()
meth public final void java.lang.Object.wait() throws java.lang.InterruptedException
meth public final void java.lang.Object.wait(long) throws java.lang.InterruptedException
meth public final void java.lang.Object.wait(long,int) throws java.lang.InterruptedException
meth public int java.lang.Object.hashCode()
meth public java.lang.String java.lang.Throwable.getLocalizedMessage()
meth public java.lang.String java.lang.Throwable.getMessage()
meth public java.lang.String java.lang.Throwable.toString()
meth public java.lang.Throwable java.lang.Throwable.fillInStackTrace()
meth public java.lang.Throwable java.lang.Throwable.getCause()
meth public java.lang.Throwable java.lang.Throwable.initCause(java.lang.Throwable)
meth public void java.lang.Throwable.printStackTrace()
meth public void java.lang.Throwable.printStackTrace(java.io.PrintStream)
meth public void java.lang.Throwable.printStackTrace(java.io.PrintWriter)
meth public void java.lang.Throwable.setStackTrace([Ljava.lang.StackTraceElement;)
supr java.lang.Object java.lang.Throwable java.lang.Exception
CLSS public javax.slee.profile.AttributeTypeMismatchException
cons public javax.slee.profile.AttributeTypeMismatchException.AttributeTypeMismatchException(java.lang.String)
intf java.io.Serializable
meth protected java.lang.Object java.lang.Object.clone() throws java.lang.CloneNotSupportedException
meth protected void java.lang.Object.finalize() throws java.lang.Throwable
meth public [Ljava.lang.StackTraceElement; java.lang.Throwable.getStackTrace()
meth public boolean java.lang.Object.equals(java.lang.Object)
meth public final java.lang.Class java.lang.Object.getClass()
meth public final void java.lang.Object.notify()
meth public final void java.lang.Object.notifyAll()
meth public final void java.lang.Object.wait() throws java.lang.InterruptedException
meth public final void java.lang.Object.wait(long) throws java.lang.InterruptedException
meth public final void java.lang.Object.wait(long,int) throws java.lang.InterruptedException
meth public int java.lang.Object.hashCode()
meth public java.lang.String java.lang.Throwable.getLocalizedMessage()
meth public java.lang.String java.lang.Throwable.getMessage()
meth public java.lang.String java.lang.Throwable.toString()
meth public java.lang.Throwable java.lang.Throwable.fillInStackTrace()
meth public java.lang.Throwable java.lang.Throwable.getCause()
meth public java.lang.Throwable java.lang.Throwable.initCause(java.lang.Throwable)
meth public void java.lang.Throwable.printStackTrace()
meth public void java.lang.Throwable.printStackTrace(java.io.PrintStream)
meth public void java.lang.Throwable.printStackTrace(java.io.PrintWriter)
meth public void java.lang.Throwable.setStackTrace([Ljava.lang.StackTraceElement;)
supr java.lang.Object java.lang.Throwable java.lang.Exception
CLSS public abstract interface javax.slee.profile.Profile
meth public abstract void javax.slee.profile.Profile.profileActivate()
meth public abstract void javax.slee.profile.Profile.profileInitialize()
meth public abstract void javax.slee.profile.Profile.profileLoad()
meth public abstract void javax.slee.profile.Profile.profilePassivate()
meth public abstract void javax.slee.profile.Profile.profilePostCreate() throws javax.slee.CreateException
meth public abstract void javax.slee.profile.Profile.profileRemove()
meth public abstract void javax.slee.profile.Profile.profileStore()
meth public abstract void javax.slee.profile.Profile.profileVerify() throws javax.slee.profile.ProfileVerificationException
meth public abstract void javax.slee.profile.Profile.setProfileContext(javax.slee.profile.ProfileContext)
meth public abstract void javax.slee.profile.Profile.unsetProfileContext()
supr null
CLSS public abstract interface javax.slee.profile.ProfileAddedEvent
meth public abstract java.lang.Object javax.slee.profile.ProfileAddedEvent.getAddedProfile()
meth public abstract javax.slee.Address javax.slee.profile.ProfileAddedEvent.getProfileAddress()
meth public abstract javax.slee.profile.ProfileID javax.slee.profile.ProfileAddedEvent.getProfile()
meth public abstract javax.slee.profile.ProfileLocalObject javax.slee.profile.ProfileAddedEvent.getAddedProfileLocal()
supr null
CLSS public javax.slee.profile.ProfileAlreadyExistsException
cons public javax.slee.profile.ProfileAlreadyExistsException.ProfileAlreadyExistsException()
cons public javax.slee.profile.ProfileAlreadyExistsException.ProfileAlreadyExistsException(java.lang.String)
intf java.io.Serializable
meth protected java.lang.Object java.lang.Object.clone() throws java.lang.CloneNotSupportedException
meth protected void java.lang.Object.finalize() throws java.lang.Throwable
meth public [Ljava.lang.StackTraceElement; java.lang.Throwable.getStackTrace()
meth public boolean java.lang.Object.equals(java.lang.Object)
meth public final java.lang.Class java.lang.Object.getClass()
meth public final void java.lang.Object.notify()
meth public final void java.lang.Object.notifyAll()
meth public final void java.lang.Object.wait() throws java.lang.InterruptedException
meth public final void java.lang.Object.wait(long) throws java.lang.InterruptedException
meth public final void java.lang.Object.wait(long,int) throws java.lang.InterruptedException
meth public int java.lang.Object.hashCode()
meth public java.lang.String java.lang.Throwable.getLocalizedMessage()
meth public java.lang.String java.lang.Throwable.getMessage()
meth public java.lang.String java.lang.Throwable.toString()
meth public java.lang.Throwable java.lang.Throwable.fillInStackTrace()
meth public java.lang.Throwable java.lang.Throwable.getCause()
meth public java.lang.Throwable java.lang.Throwable.initCause(java.lang.Throwable)
meth public void java.lang.Throwable.printStackTrace()
meth public void java.lang.Throwable.printStackTrace(java.io.PrintStream)
meth public void java.lang.Throwable.printStackTrace(java.io.PrintWriter)
meth public void java.lang.Throwable.setStackTrace([Ljava.lang.StackTraceElement;)
supr java.lang.Object java.lang.Throwable java.lang.Exception
CLSS public abstract interface javax.slee.profile.ProfileContext
meth public abstract boolean javax.slee.profile.ProfileContext.getRollbackOnly()
meth public abstract java.lang.String javax.slee.profile.ProfileContext.getProfileName()
meth public abstract java.lang.String javax.slee.profile.ProfileContext.getProfileTableName()
meth public abstract javax.slee.facilities.Tracer javax.slee.profile.ProfileContext.getTracer(java.lang.String)
meth public abstract javax.slee.profile.ProfileLocalObject javax.slee.profile.ProfileContext.getProfileLocalObject()
meth public abstract javax.slee.profile.ProfileTable javax.slee.profile.ProfileContext.getProfileTable()
meth public abstract javax.slee.profile.ProfileTable javax.slee.profile.ProfileContext.getProfileTable(java.lang.String) throws javax.slee.profile.UnrecognizedProfileTableNameException
meth public abstract void javax.slee.profile.ProfileContext.setRollbackOnly()
supr null
CLSS public abstract interface javax.slee.profile.ProfileFacility
fld  public static final java.lang.String javax.slee.profile.ProfileFacility.JNDI_NAME
meth public abstract java.util.Collection javax.slee.profile.ProfileFacility.getProfiles(java.lang.String) throws javax.slee.profile.UnrecognizedProfileTableNameException
meth public abstract java.util.Collection javax.slee.profile.ProfileFacility.getProfilesByIndexedAttribute(java.lang.String,java.lang.String,java.lang.Object) throws javax.slee.profile.AttributeNotIndexedException,javax.slee.profile.AttributeTypeMismatchException,javax.slee.profile.UnrecognizedAttributeException,javax.slee.profile.UnrecognizedProfileTableNameException
meth public abstract javax.slee.profile.ProfileID javax.slee.profile.ProfileFacility.getProfileByIndexedAttribute(java.lang.String,java.lang.String,java.lang.Object) throws javax.slee.profile.AttributeNotIndexedException,javax.slee.profile.AttributeTypeMismatchException,javax.slee.profile.UnrecognizedAttributeException,javax.slee.profile.UnrecognizedProfileTableNameException
meth public abstract javax.slee.profile.ProfileTable javax.slee.profile.ProfileFacility.getProfileTable(java.lang.String) throws javax.slee.profile.UnrecognizedProfileTableNameException
meth public abstract javax.slee.profile.ProfileTableActivity javax.slee.profile.ProfileFacility.getProfileTableActivity(java.lang.String) throws javax.slee.profile.UnrecognizedProfileTableNameException
supr null
CLSS public javax.slee.profile.ProfileID
cons public javax.slee.profile.ProfileID.ProfileID(java.lang.String,java.lang.String)
cons public javax.slee.profile.ProfileID.ProfileID(javax.slee.Address)
intf java.io.Serializable
intf java.lang.Cloneable
meth protected void java.lang.Object.finalize() throws java.lang.Throwable
meth public boolean javax.slee.profile.ProfileID.equals(java.lang.Object)
meth public final java.lang.Class java.lang.Object.getClass()
meth public final java.lang.String javax.slee.profile.ProfileID.getProfileName()
meth public final java.lang.String javax.slee.profile.ProfileID.getProfileTableName()
meth public final void java.lang.Object.notify()
meth public final void java.lang.Object.notifyAll()
meth public final void java.lang.Object.wait() throws java.lang.InterruptedException
meth public final void java.lang.Object.wait(long) throws java.lang.InterruptedException
meth public final void java.lang.Object.wait(long,int) throws java.lang.InterruptedException
meth public final void javax.slee.profile.ProfileID.setProfileID(java.lang.String,java.lang.String)
meth public int javax.slee.profile.ProfileID.hashCode()
meth public java.lang.Object javax.slee.profile.ProfileID.clone()
meth public java.lang.String javax.slee.profile.ProfileID.toString()
meth public javax.slee.Address javax.slee.profile.ProfileID.toAddress()
supr java.lang.Object
CLSS public javax.slee.profile.ProfileImplementationException
cons public javax.slee.profile.ProfileImplementationException.ProfileImplementationException(java.lang.Throwable)
intf java.io.Serializable
meth protected java.lang.Object java.lang.Object.clone() throws java.lang.CloneNotSupportedException
meth protected void java.lang.Object.finalize() throws java.lang.Throwable
meth public [Ljava.lang.StackTraceElement; java.lang.Throwable.getStackTrace()
meth public boolean java.lang.Object.equals(java.lang.Object)
meth public final java.lang.Class java.lang.Object.getClass()
meth public final void java.lang.Object.notify()
meth public final void java.lang.Object.notifyAll()
meth public final void java.lang.Object.wait() throws java.lang.InterruptedException
meth public final void java.lang.Object.wait(long) throws java.lang.InterruptedException
meth public final void java.lang.Object.wait(long,int) throws java.lang.InterruptedException
meth public int java.lang.Object.hashCode()
meth public java.lang.String java.lang.Throwable.getLocalizedMessage()
meth public java.lang.String java.lang.Throwable.getMessage()
meth public java.lang.String java.lang.Throwable.toString()
meth public java.lang.Throwable java.lang.Throwable.fillInStackTrace()
meth public java.lang.Throwable java.lang.Throwable.initCause(java.lang.Throwable)
meth public java.lang.Throwable javax.slee.profile.ProfileImplementationException.getCause()
meth public void java.lang.Throwable.printStackTrace()
meth public void java.lang.Throwable.printStackTrace(java.io.PrintStream)
meth public void java.lang.Throwable.printStackTrace(java.io.PrintWriter)
meth public void java.lang.Throwable.setStackTrace([Ljava.lang.StackTraceElement;)
supr java.lang.Object java.lang.Throwable java.lang.Exception
CLSS public abstract interface javax.slee.profile.ProfileLocalObject
meth public abstract boolean javax.slee.profile.ProfileLocalObject.isIdentical(javax.slee.profile.ProfileLocalObject)
meth public abstract java.lang.String javax.slee.profile.ProfileLocalObject.getProfileName()
meth public abstract java.lang.String javax.slee.profile.ProfileLocalObject.getProfileTableName()
meth public abstract javax.slee.profile.ProfileTable javax.slee.profile.ProfileLocalObject.getProfileTable()
meth public abstract void javax.slee.profile.ProfileLocalObject.remove()
supr null
CLSS public abstract interface javax.slee.profile.ProfileMBean
fld  public static final java.lang.String javax.slee.profile.ProfileMBean.BASE_OBJECT_NAME
fld  public static final java.lang.String javax.slee.profile.ProfileMBean.PROFILE_NAME_KEY
fld  public static final java.lang.String javax.slee.profile.ProfileMBean.PROFILE_TABLE_NAME_KEY
meth public abstract boolean javax.slee.profile.ProfileMBean.isProfileDirty() throws javax.slee.management.ManagementException
meth public abstract boolean javax.slee.profile.ProfileMBean.isProfileWriteable() throws javax.slee.management.ManagementException
meth public abstract void javax.slee.profile.ProfileMBean.closeProfile() throws javax.slee.InvalidStateException,javax.slee.management.ManagementException
meth public abstract void javax.slee.profile.ProfileMBean.commitProfile() throws javax.slee.InvalidStateException,javax.slee.management.ManagementException,javax.slee.profile.ProfileVerificationException
meth public abstract void javax.slee.profile.ProfileMBean.editProfile() throws javax.slee.management.ManagementException
meth public abstract void javax.slee.profile.ProfileMBean.restoreProfile() throws javax.slee.InvalidStateException,javax.slee.management.ManagementException
supr null
CLSS public abstract interface javax.slee.profile.ProfileManagement
meth public abstract boolean javax.slee.profile.ProfileManagement.isProfileDirty()
meth public abstract boolean javax.slee.profile.ProfileManagement.isProfileValid(javax.slee.profile.ProfileID)
meth public abstract void javax.slee.profile.ProfileManagement.markProfileDirty()
meth public abstract void javax.slee.profile.ProfileManagement.profileInitialize()
meth public abstract void javax.slee.profile.ProfileManagement.profileLoad()
meth public abstract void javax.slee.profile.ProfileManagement.profileStore()
meth public abstract void javax.slee.profile.ProfileManagement.profileVerify() throws javax.slee.profile.ProfileVerificationException
supr null
CLSS public abstract interface javax.slee.profile.ProfileRemovedEvent
meth public abstract java.lang.Object javax.slee.profile.ProfileRemovedEvent.getRemovedProfile()
meth public abstract javax.slee.Address javax.slee.profile.ProfileRemovedEvent.getProfileAddress()
meth public abstract javax.slee.profile.ProfileID javax.slee.profile.ProfileRemovedEvent.getProfile()
meth public abstract javax.slee.profile.ProfileLocalObject javax.slee.profile.ProfileRemovedEvent.getRemovedProfileLocal()
supr null
CLSS public javax.slee.profile.ProfileSpecificationDescriptor
cons public javax.slee.profile.ProfileSpecificationDescriptor.ProfileSpecificationDescriptor(javax.slee.profile.ProfileSpecificationID,javax.slee.management.DeployableUnitID,java.lang.String,[Ljavax.slee.management.LibraryID;,[Ljavax.slee.profile.ProfileSpecificationID;,java.lang.String)
intf java.io.Serializable
intf javax.slee.management.VendorExtensions
meth protected final void javax.slee.management.ComponentDescriptor.toString(java.lang.StringBuffer)
meth protected java.lang.Object java.lang.Object.clone() throws java.lang.CloneNotSupportedException
meth protected void java.lang.Object.finalize() throws java.lang.Throwable
meth public final [Ljavax.slee.management.LibraryID; javax.slee.management.ComponentDescriptor.getLibraries()
meth public final [Ljavax.slee.profile.ProfileSpecificationID; javax.slee.profile.ProfileSpecificationDescriptor.getProfileSpecifications()
meth public final boolean javax.slee.management.ComponentDescriptor.equals(java.lang.Object)
meth public final int javax.slee.management.ComponentDescriptor.hashCode()
meth public final java.lang.Class java.lang.Object.getClass()
meth public final java.lang.String javax.slee.management.ComponentDescriptor.getName()
meth public final java.lang.String javax.slee.management.ComponentDescriptor.getSource()
meth public final java.lang.String javax.slee.management.ComponentDescriptor.getVendor()
meth public final java.lang.String javax.slee.management.ComponentDescriptor.getVersion()
meth public final java.lang.String javax.slee.profile.ProfileSpecificationDescriptor.getCMPInterfaceName()
meth public final javax.slee.ComponentID javax.slee.management.ComponentDescriptor.getID()
meth public final javax.slee.management.DeployableUnitID javax.slee.management.ComponentDescriptor.getDeployableUnit()
meth public final void java.lang.Object.notify()
meth public final void java.lang.Object.notifyAll()
meth public final void java.lang.Object.wait() throws java.lang.InterruptedException
meth public final void java.lang.Object.wait(long) throws java.lang.InterruptedException
meth public final void java.lang.Object.wait(long,int) throws java.lang.InterruptedException
meth public java.lang.Object javax.slee.management.ComponentDescriptor.getVendorData()
meth public java.lang.String javax.slee.profile.ProfileSpecificationDescriptor.toString()
meth public static void javax.slee.management.ComponentDescriptor.disableVendorDataDeserialization()
meth public static void javax.slee.management.ComponentDescriptor.disableVendorDataSerialization()
meth public static void javax.slee.management.ComponentDescriptor.enableVendorDataDeserialization()
meth public static void javax.slee.management.ComponentDescriptor.enableVendorDataSerialization()
meth public void javax.slee.management.ComponentDescriptor.setVendorData(java.lang.Object)
supr java.lang.Object javax.slee.management.ComponentDescriptor
CLSS public final javax.slee.profile.ProfileSpecificationID
cons public javax.slee.profile.ProfileSpecificationID.ProfileSpecificationID(java.lang.String,java.lang.String,java.lang.String)
intf java.io.Serializable
intf java.lang.Cloneable
intf java.lang.Comparable
meth protected final int javax.slee.ComponentID.compareTo(java.lang.String,javax.slee.ComponentID)
meth protected java.lang.String javax.slee.profile.ProfileSpecificationID.getClassName()
meth protected void java.lang.Object.finalize() throws java.lang.Throwable
meth public final boolean javax.slee.ComponentID.equals(java.lang.Object)
meth public final int javax.slee.ComponentID.hashCode()
meth public final int javax.slee.profile.ProfileSpecificationID.compareTo(java.lang.Object)
meth public final java.lang.Class java.lang.Object.getClass()
meth public final java.lang.String javax.slee.ComponentID.getName()
meth public final java.lang.String javax.slee.ComponentID.getVendor()
meth public final java.lang.String javax.slee.ComponentID.getVersion()
meth public final java.lang.String javax.slee.ComponentID.toString()
meth public final void java.lang.Object.notify()
meth public final void java.lang.Object.notifyAll()
meth public final void java.lang.Object.wait() throws java.lang.InterruptedException
meth public final void java.lang.Object.wait(long) throws java.lang.InterruptedException
meth public final void java.lang.Object.wait(long,int) throws java.lang.InterruptedException
meth public java.lang.Object javax.slee.profile.ProfileSpecificationID.clone()
supr java.lang.Object javax.slee.ComponentID
CLSS public abstract interface javax.slee.profile.ProfileTable
meth public abstract boolean javax.slee.profile.ProfileTable.remove(java.lang.String)
meth public abstract java.util.Collection javax.slee.profile.ProfileTable.findAll()
meth public abstract java.util.Collection javax.slee.profile.ProfileTable.findProfilesByAttribute(java.lang.String,java.lang.Object)
meth public abstract javax.slee.profile.ProfileLocalObject javax.slee.profile.ProfileTable.create(java.lang.String) throws javax.slee.CreateException,javax.slee.profile.ProfileAlreadyExistsException
meth public abstract javax.slee.profile.ProfileLocalObject javax.slee.profile.ProfileTable.find(java.lang.String)
meth public abstract javax.slee.profile.ProfileLocalObject javax.slee.profile.ProfileTable.findProfileByAttribute(java.lang.String,java.lang.Object)
supr null
CLSS public abstract interface javax.slee.profile.ProfileTableActivity
meth public abstract java.lang.String javax.slee.profile.ProfileTableActivity.getProfileTableName()
supr null
CLSS public abstract interface javax.slee.profile.ProfileTableActivityContextInterfaceFactory
fld  public static final java.lang.String javax.slee.profile.ProfileTableActivityContextInterfaceFactory.JNDI_NAME
meth public abstract javax.slee.ActivityContextInterface javax.slee.profile.ProfileTableActivityContextInterfaceFactory.getActivityContextInterface(javax.slee.profile.ProfileTableActivity)
supr null
CLSS public javax.slee.profile.ProfileTableAlreadyExistsException
cons public javax.slee.profile.ProfileTableAlreadyExistsException.ProfileTableAlreadyExistsException()
cons public javax.slee.profile.ProfileTableAlreadyExistsException.ProfileTableAlreadyExistsException(java.lang.String)
intf java.io.Serializable
meth protected java.lang.Object java.lang.Object.clone() throws java.lang.CloneNotSupportedException
meth protected void java.lang.Object.finalize() throws java.lang.Throwable
meth public [Ljava.lang.StackTraceElement; java.lang.Throwable.getStackTrace()
meth public boolean java.lang.Object.equals(java.lang.Object)
meth public final java.lang.Class java.lang.Object.getClass()
meth public final void java.lang.Object.notify()
meth public final void java.lang.Object.notifyAll()
meth public final void java.lang.Object.wait() throws java.lang.InterruptedException
meth public final void java.lang.Object.wait(long) throws java.lang.InterruptedException
meth public final void java.lang.Object.wait(long,int) throws java.lang.InterruptedException
meth public int java.lang.Object.hashCode()
meth public java.lang.String java.lang.Throwable.getLocalizedMessage()
meth public java.lang.String java.lang.Throwable.getMessage()
meth public java.lang.String java.lang.Throwable.toString()
meth public java.lang.Throwable java.lang.Throwable.fillInStackTrace()
meth public java.lang.Throwable java.lang.Throwable.getCause()
meth public java.lang.Throwable java.lang.Throwable.initCause(java.lang.Throwable)
meth public void java.lang.Throwable.printStackTrace()
meth public void java.lang.Throwable.printStackTrace(java.io.PrintStream)
meth public void java.lang.Throwable.printStackTrace(java.io.PrintWriter)
meth public void java.lang.Throwable.setStackTrace([Ljava.lang.StackTraceElement;)
supr java.lang.Object java.lang.Throwable java.lang.Exception
CLSS public abstract interface javax.slee.profile.ProfileUpdatedEvent
meth public abstract java.lang.Object javax.slee.profile.ProfileUpdatedEvent.getAfterUpdateProfile()
meth public abstract java.lang.Object javax.slee.profile.ProfileUpdatedEvent.getBeforeUpdateProfile()
meth public abstract javax.slee.Address javax.slee.profile.ProfileUpdatedEvent.getProfileAddress()
meth public abstract javax.slee.profile.ProfileID javax.slee.profile.ProfileUpdatedEvent.getProfile()
meth public abstract javax.slee.profile.ProfileLocalObject javax.slee.profile.ProfileUpdatedEvent.getAfterUpdateProfileLocal()
meth public abstract javax.slee.profile.ProfileLocalObject javax.slee.profile.ProfileUpdatedEvent.getBeforeUpdateProfileLocal()
supr null
CLSS public javax.slee.profile.ProfileVerificationException
cons public javax.slee.profile.ProfileVerificationException.ProfileVerificationException(java.lang.String)
cons public javax.slee.profile.ProfileVerificationException.ProfileVerificationException(java.lang.String,java.lang.Throwable)
intf java.io.Serializable
meth protected java.lang.Object java.lang.Object.clone() throws java.lang.CloneNotSupportedException
meth protected void java.lang.Object.finalize() throws java.lang.Throwable
meth public [Ljava.lang.StackTraceElement; java.lang.Throwable.getStackTrace()
meth public boolean java.lang.Object.equals(java.lang.Object)
meth public final java.lang.Class java.lang.Object.getClass()
meth public final void java.lang.Object.notify()
meth public final void java.lang.Object.notifyAll()
meth public final void java.lang.Object.wait() throws java.lang.InterruptedException
meth public final void java.lang.Object.wait(long) throws java.lang.InterruptedException
meth public final void java.lang.Object.wait(long,int) throws java.lang.InterruptedException
meth public int java.lang.Object.hashCode()
meth public java.lang.String java.lang.Throwable.getLocalizedMessage()
meth public java.lang.String java.lang.Throwable.getMessage()
meth public java.lang.String java.lang.Throwable.toString()
meth public java.lang.Throwable java.lang.Throwable.fillInStackTrace()
meth public java.lang.Throwable java.lang.Throwable.initCause(java.lang.Throwable)
meth public java.lang.Throwable javax.slee.profile.ProfileVerificationException.getCause()
meth public void java.lang.Throwable.printStackTrace()
meth public void java.lang.Throwable.printStackTrace(java.io.PrintStream)
meth public void java.lang.Throwable.printStackTrace(java.io.PrintWriter)
meth public void java.lang.Throwable.setStackTrace([Ljava.lang.StackTraceElement;)
supr java.lang.Object java.lang.Throwable java.lang.Exception
CLSS public javax.slee.profile.ReadOnlyProfileException
cons public javax.slee.profile.ReadOnlyProfileException.ReadOnlyProfileException()
cons public javax.slee.profile.ReadOnlyProfileException.ReadOnlyProfileException(java.lang.String)
intf java.io.Serializable
meth protected java.lang.Object java.lang.Object.clone() throws java.lang.CloneNotSupportedException
meth protected void java.lang.Object.finalize() throws java.lang.Throwable
meth public [Ljava.lang.StackTraceElement; java.lang.Throwable.getStackTrace()
meth public boolean java.lang.Object.equals(java.lang.Object)
meth public final java.lang.Class java.lang.Object.getClass()
meth public final void java.lang.Object.notify()
meth public final void java.lang.Object.notifyAll()
meth public final void java.lang.Object.wait() throws java.lang.InterruptedException
meth public final void java.lang.Object.wait(long) throws java.lang.InterruptedException
meth public final void java.lang.Object.wait(long,int) throws java.lang.InterruptedException
meth public int java.lang.Object.hashCode()
meth public java.lang.String java.lang.Throwable.getLocalizedMessage()
meth public java.lang.String java.lang.Throwable.getMessage()
meth public java.lang.String java.lang.Throwable.toString()
meth public java.lang.Throwable java.lang.Throwable.fillInStackTrace()
meth public java.lang.Throwable java.lang.Throwable.getCause()
meth public java.lang.Throwable java.lang.Throwable.initCause(java.lang.Throwable)
meth public void java.lang.Throwable.printStackTrace()
meth public void java.lang.Throwable.printStackTrace(java.io.PrintStream)
meth public void java.lang.Throwable.printStackTrace(java.io.PrintWriter)
meth public void java.lang.Throwable.setStackTrace([Ljava.lang.StackTraceElement;)
supr java.lang.Object java.lang.Throwable java.lang.Exception java.lang.RuntimeException
CLSS public abstract interface javax.slee.profile.ResourceInfoProfileCMP
meth public abstract java.lang.String javax.slee.profile.ResourceInfoProfileCMP.getInfo()
meth public abstract void javax.slee.profile.ResourceInfoProfileCMP.setInfo(java.lang.String)
supr null
CLSS public javax.slee.profile.UnrecognizedAttributeException
cons public javax.slee.profile.UnrecognizedAttributeException.UnrecognizedAttributeException()
cons public javax.slee.profile.UnrecognizedAttributeException.UnrecognizedAttributeException(java.lang.String)
intf java.io.Serializable
meth protected java.lang.Object java.lang.Object.clone() throws java.lang.CloneNotSupportedException
meth protected void java.lang.Object.finalize() throws java.lang.Throwable
meth public [Ljava.lang.StackTraceElement; java.lang.Throwable.getStackTrace()
meth public boolean java.lang.Object.equals(java.lang.Object)
meth public final java.lang.Class java.lang.Object.getClass()
meth public final void java.lang.Object.notify()
meth public final void java.lang.Object.notifyAll()
meth public final void java.lang.Object.wait() throws java.lang.InterruptedException
meth public final void java.lang.Object.wait(long) throws java.lang.InterruptedException
meth public final void java.lang.Object.wait(long,int) throws java.lang.InterruptedException
meth public int java.lang.Object.hashCode()
meth public java.lang.String java.lang.Throwable.getLocalizedMessage()
meth public java.lang.String java.lang.Throwable.getMessage()
meth public java.lang.String java.lang.Throwable.toString()
meth public java.lang.Throwable java.lang.Throwable.fillInStackTrace()
meth public java.lang.Throwable java.lang.Throwable.getCause()
meth public java.lang.Throwable java.lang.Throwable.initCause(java.lang.Throwable)
meth public void java.lang.Throwable.printStackTrace()
meth public void java.lang.Throwable.printStackTrace(java.io.PrintStream)
meth public void java.lang.Throwable.printStackTrace(java.io.PrintWriter)
meth public void java.lang.Throwable.setStackTrace([Ljava.lang.StackTraceElement;)
supr java.lang.Object java.lang.Throwable java.lang.Exception
CLSS public javax.slee.profile.UnrecognizedProfileNameException
cons public javax.slee.profile.UnrecognizedProfileNameException.UnrecognizedProfileNameException()
cons public javax.slee.profile.UnrecognizedProfileNameException.UnrecognizedProfileNameException(java.lang.String)
intf java.io.Serializable
meth protected java.lang.Object java.lang.Object.clone() throws java.lang.CloneNotSupportedException
meth protected void java.lang.Object.finalize() throws java.lang.Throwable
meth public [Ljava.lang.StackTraceElement; java.lang.Throwable.getStackTrace()
meth public boolean java.lang.Object.equals(java.lang.Object)
meth public final java.lang.Class java.lang.Object.getClass()
meth public final void java.lang.Object.notify()
meth public final void java.lang.Object.notifyAll()
meth public final void java.lang.Object.wait() throws java.lang.InterruptedException
meth public final void java.lang.Object.wait(long) throws java.lang.InterruptedException
meth public final void java.lang.Object.wait(long,int) throws java.lang.InterruptedException
meth public int java.lang.Object.hashCode()
meth public java.lang.String java.lang.Throwable.getLocalizedMessage()
meth public java.lang.String java.lang.Throwable.getMessage()
meth public java.lang.String java.lang.Throwable.toString()
meth public java.lang.Throwable java.lang.Throwable.fillInStackTrace()
meth public java.lang.Throwable java.lang.Throwable.getCause()
meth public java.lang.Throwable java.lang.Throwable.initCause(java.lang.Throwable)
meth public void java.lang.Throwable.printStackTrace()
meth public void java.lang.Throwable.printStackTrace(java.io.PrintStream)
meth public void java.lang.Throwable.printStackTrace(java.io.PrintWriter)
meth public void java.lang.Throwable.setStackTrace([Ljava.lang.StackTraceElement;)
supr java.lang.Object java.lang.Throwable java.lang.Exception
CLSS public javax.slee.profile.UnrecognizedProfileSpecificationException
cons public javax.slee.profile.UnrecognizedProfileSpecificationException.UnrecognizedProfileSpecificationException()
cons public javax.slee.profile.UnrecognizedProfileSpecificationException.UnrecognizedProfileSpecificationException(java.lang.String)
intf java.io.Serializable
meth protected java.lang.Object java.lang.Object.clone() throws java.lang.CloneNotSupportedException
meth protected void java.lang.Object.finalize() throws java.lang.Throwable
meth public [Ljava.lang.StackTraceElement; java.lang.Throwable.getStackTrace()
meth public boolean java.lang.Object.equals(java.lang.Object)
meth public final java.lang.Class java.lang.Object.getClass()
meth public final void java.lang.Object.notify()
meth public final void java.lang.Object.notifyAll()
meth public final void java.lang.Object.wait() throws java.lang.InterruptedException
meth public final void java.lang.Object.wait(long) throws java.lang.InterruptedException
meth public final void java.lang.Object.wait(long,int) throws java.lang.InterruptedException
meth public int java.lang.Object.hashCode()
meth public java.lang.String java.lang.Throwable.getLocalizedMessage()
meth public java.lang.String java.lang.Throwable.getMessage()
meth public java.lang.String java.lang.Throwable.toString()
meth public java.lang.Throwable java.lang.Throwable.fillInStackTrace()
meth public java.lang.Throwable java.lang.Throwable.getCause()
meth public java.lang.Throwable java.lang.Throwable.initCause(java.lang.Throwable)
meth public void java.lang.Throwable.printStackTrace()
meth public void java.lang.Throwable.printStackTrace(java.io.PrintStream)
meth public void java.lang.Throwable.printStackTrace(java.io.PrintWriter)
meth public void java.lang.Throwable.setStackTrace([Ljava.lang.StackTraceElement;)
supr java.lang.Object java.lang.Throwable java.lang.Exception javax.slee.UnrecognizedComponentException
CLSS public javax.slee.profile.UnrecognizedProfileTableNameException
cons public javax.slee.profile.UnrecognizedProfileTableNameException.UnrecognizedProfileTableNameException()
cons public javax.slee.profile.UnrecognizedProfileTableNameException.UnrecognizedProfileTableNameException(java.lang.String)
intf java.io.Serializable
meth protected java.lang.Object java.lang.Object.clone() throws java.lang.CloneNotSupportedException
meth protected void java.lang.Object.finalize() throws java.lang.Throwable
meth public [Ljava.lang.StackTraceElement; java.lang.Throwable.getStackTrace()
meth public boolean java.lang.Object.equals(java.lang.Object)
meth public final java.lang.Class java.lang.Object.getClass()
meth public final void java.lang.Object.notify()
meth public final void java.lang.Object.notifyAll()
meth public final void java.lang.Object.wait() throws java.lang.InterruptedException
meth public final void java.lang.Object.wait(long) throws java.lang.InterruptedException
meth public final void java.lang.Object.wait(long,int) throws java.lang.InterruptedException
meth public int java.lang.Object.hashCode()
meth public java.lang.String java.lang.Throwable.getLocalizedMessage()
meth public java.lang.String java.lang.Throwable.getMessage()
meth public java.lang.String java.lang.Throwable.toString()
meth public java.lang.Throwable java.lang.Throwable.fillInStackTrace()
meth public java.lang.Throwable java.lang.Throwable.getCause()
meth public java.lang.Throwable java.lang.Throwable.initCause(java.lang.Throwable)
meth public void java.lang.Throwable.printStackTrace()
meth public void java.lang.Throwable.printStackTrace(java.io.PrintStream)
meth public void java.lang.Throwable.printStackTrace(java.io.PrintWriter)
meth public void java.lang.Throwable.setStackTrace([Ljava.lang.StackTraceElement;)
supr java.lang.Object java.lang.Throwable java.lang.Exception
CLSS public javax.slee.profile.UnrecognizedQueryNameException
cons public javax.slee.profile.UnrecognizedQueryNameException.UnrecognizedQueryNameException()
cons public javax.slee.profile.UnrecognizedQueryNameException.UnrecognizedQueryNameException(java.lang.String)
intf java.io.Serializable
meth protected java.lang.Object java.lang.Object.clone() throws java.lang.CloneNotSupportedException
meth protected void java.lang.Object.finalize() throws java.lang.Throwable
meth public [Ljava.lang.StackTraceElement; java.lang.Throwable.getStackTrace()
meth public boolean java.lang.Object.equals(java.lang.Object)
meth public final java.lang.Class java.lang.Object.getClass()
meth public final void java.lang.Object.notify()
meth public final void java.lang.Object.notifyAll()
meth public final void java.lang.Object.wait() throws java.lang.InterruptedException
meth public final void java.lang.Object.wait(long) throws java.lang.InterruptedException
meth public final void java.lang.Object.wait(long,int) throws java.lang.InterruptedException
meth public int java.lang.Object.hashCode()
meth public java.lang.String java.lang.Throwable.getLocalizedMessage()
meth public java.lang.String java.lang.Throwable.getMessage()
meth public java.lang.String java.lang.Throwable.toString()
meth public java.lang.Throwable java.lang.Throwable.fillInStackTrace()
meth public java.lang.Throwable java.lang.Throwable.getCause()
meth public java.lang.Throwable java.lang.Throwable.initCause(java.lang.Throwable)
meth public void java.lang.Throwable.printStackTrace()
meth public void java.lang.Throwable.printStackTrace(java.io.PrintStream)
meth public void java.lang.Throwable.printStackTrace(java.io.PrintWriter)
meth public void java.lang.Throwable.setStackTrace([Ljava.lang.StackTraceElement;)
supr java.lang.Object java.lang.Throwable java.lang.Exception
CLSS public final javax.slee.profile.query.And
cons public javax.slee.profile.query.And.And([Ljavax.slee.profile.query.QueryExpression;)
cons public javax.slee.profile.query.And.And(javax.slee.profile.query.QueryExpression,javax.slee.profile.query.QueryExpression)
intf java.io.Serializable
meth protected final void javax.slee.profile.query.CompositeQueryExpression.add(javax.slee.profile.query.QueryExpression)
meth protected java.lang.Object java.lang.Object.clone() throws java.lang.CloneNotSupportedException
meth protected void java.lang.Object.finalize() throws java.lang.Throwable
meth protected void javax.slee.profile.query.And.toString(java.lang.StringBuffer)
meth public boolean java.lang.Object.equals(java.lang.Object)
meth public final [Ljavax.slee.profile.query.QueryExpression; javax.slee.profile.query.CompositeQueryExpression.getExpressions()
meth public final java.lang.Class java.lang.Object.getClass()
meth public final java.lang.String javax.slee.profile.query.QueryExpression.toString()
meth public final void java.lang.Object.notify()
meth public final void java.lang.Object.notifyAll()
meth public final void java.lang.Object.wait() throws java.lang.InterruptedException
meth public final void java.lang.Object.wait(long) throws java.lang.InterruptedException
meth public final void java.lang.Object.wait(long,int) throws java.lang.InterruptedException
meth public int java.lang.Object.hashCode()
meth public javax.slee.profile.query.And javax.slee.profile.query.And.and(javax.slee.profile.query.QueryExpression)
supr java.lang.Object javax.slee.profile.query.QueryExpression javax.slee.profile.query.CompositeQueryExpression
CLSS public abstract javax.slee.profile.query.CompositeQueryExpression
cons public javax.slee.profile.query.CompositeQueryExpression.CompositeQueryExpression()
intf java.io.Serializable
meth protected abstract void javax.slee.profile.query.QueryExpression.toString(java.lang.StringBuffer)
meth protected final void javax.slee.profile.query.CompositeQueryExpression.add(javax.slee.profile.query.QueryExpression)
meth protected java.lang.Object java.lang.Object.clone() throws java.lang.CloneNotSupportedException
meth protected void java.lang.Object.finalize() throws java.lang.Throwable
meth public boolean java.lang.Object.equals(java.lang.Object)
meth public final [Ljavax.slee.profile.query.QueryExpression; javax.slee.profile.query.CompositeQueryExpression.getExpressions()
meth public final java.lang.Class java.lang.Object.getClass()
meth public final java.lang.String javax.slee.profile.query.QueryExpression.toString()
meth public final void java.lang.Object.notify()
meth public final void java.lang.Object.notifyAll()
meth public final void java.lang.Object.wait() throws java.lang.InterruptedException
meth public final void java.lang.Object.wait(long) throws java.lang.InterruptedException
meth public final void java.lang.Object.wait(long,int) throws java.lang.InterruptedException
meth public int java.lang.Object.hashCode()
supr java.lang.Object javax.slee.profile.query.QueryExpression
CLSS public final javax.slee.profile.query.Equals
cons public javax.slee.profile.query.Equals.Equals(java.lang.String,java.lang.Object)
cons public javax.slee.profile.query.Equals.Equals(java.lang.String,java.lang.String,javax.slee.profile.query.QueryCollator)
intf java.io.Serializable
meth protected final void javax.slee.profile.query.SimpleQueryExpression.toString(java.lang.StringBuffer)
meth protected java.lang.Object java.lang.Object.clone() throws java.lang.CloneNotSupportedException
meth protected java.lang.String javax.slee.profile.query.Equals.getRelation()
meth protected void java.lang.Object.finalize() throws java.lang.Throwable
meth public boolean java.lang.Object.equals(java.lang.Object)
meth public final java.lang.Class java.lang.Object.getClass()
meth public final java.lang.Object javax.slee.profile.query.SimpleQueryExpression.getAttributeValue()
meth public final java.lang.String javax.slee.profile.query.QueryExpression.toString()
meth public final java.lang.String javax.slee.profile.query.SimpleQueryExpression.getAttributeName()
meth public final javax.slee.profile.query.QueryCollator javax.slee.profile.query.SimpleQueryExpression.getCollator()
meth public final void java.lang.Object.notify()
meth public final void java.lang.Object.notifyAll()
meth public final void java.lang.Object.wait() throws java.lang.InterruptedException
meth public final void java.lang.Object.wait(long) throws java.lang.InterruptedException
meth public final void java.lang.Object.wait(long,int) throws java.lang.InterruptedException
meth public int java.lang.Object.hashCode()
supr java.lang.Object javax.slee.profile.query.QueryExpression javax.slee.profile.query.SimpleQueryExpression
CLSS public final javax.slee.profile.query.GreaterThan
cons public javax.slee.profile.query.GreaterThan.GreaterThan(java.lang.String,java.lang.Object)
cons public javax.slee.profile.query.GreaterThan.GreaterThan(java.lang.String,java.lang.String,javax.slee.profile.query.QueryCollator)
intf java.io.Serializable
meth protected final void javax.slee.profile.query.SimpleQueryExpression.toString(java.lang.StringBuffer)
meth protected java.lang.Object java.lang.Object.clone() throws java.lang.CloneNotSupportedException
meth protected java.lang.String javax.slee.profile.query.GreaterThan.getRelation()
meth protected void java.lang.Object.finalize() throws java.lang.Throwable
meth public boolean java.lang.Object.equals(java.lang.Object)
meth public final java.lang.Class java.lang.Object.getClass()
meth public final java.lang.Object javax.slee.profile.query.SimpleQueryExpression.getAttributeValue()
meth public final java.lang.String javax.slee.profile.query.QueryExpression.toString()
meth public final java.lang.String javax.slee.profile.query.SimpleQueryExpression.getAttributeName()
meth public final javax.slee.profile.query.QueryCollator javax.slee.profile.query.SimpleQueryExpression.getCollator()
meth public final void java.lang.Object.notify()
meth public final void java.lang.Object.notifyAll()
meth public final void java.lang.Object.wait() throws java.lang.InterruptedException
meth public final void java.lang.Object.wait(long) throws java.lang.InterruptedException
meth public final void java.lang.Object.wait(long,int) throws java.lang.InterruptedException
meth public int java.lang.Object.hashCode()
supr java.lang.Object javax.slee.profile.query.QueryExpression javax.slee.profile.query.SimpleQueryExpression javax.slee.profile.query.OrderedQueryExpression
CLSS public final javax.slee.profile.query.GreaterThanOrEquals
cons public javax.slee.profile.query.GreaterThanOrEquals.GreaterThanOrEquals(java.lang.String,java.lang.Object)
cons public javax.slee.profile.query.GreaterThanOrEquals.GreaterThanOrEquals(java.lang.String,java.lang.String,javax.slee.profile.query.QueryCollator)
intf java.io.Serializable
meth protected final void javax.slee.profile.query.SimpleQueryExpression.toString(java.lang.StringBuffer)
meth protected java.lang.Object java.lang.Object.clone() throws java.lang.CloneNotSupportedException
meth protected java.lang.String javax.slee.profile.query.GreaterThanOrEquals.getRelation()
meth protected void java.lang.Object.finalize() throws java.lang.Throwable
meth public boolean java.lang.Object.equals(java.lang.Object)
meth public final java.lang.Class java.lang.Object.getClass()
meth public final java.lang.Object javax.slee.profile.query.SimpleQueryExpression.getAttributeValue()
meth public final java.lang.String javax.slee.profile.query.QueryExpression.toString()
meth public final java.lang.String javax.slee.profile.query.SimpleQueryExpression.getAttributeName()
meth public final javax.slee.profile.query.QueryCollator javax.slee.profile.query.SimpleQueryExpression.getCollator()
meth public final void java.lang.Object.notify()
meth public final void java.lang.Object.notifyAll()
meth public final void java.lang.Object.wait() throws java.lang.InterruptedException
meth public final void java.lang.Object.wait(long) throws java.lang.InterruptedException
meth public final void java.lang.Object.wait(long,int) throws java.lang.InterruptedException
meth public int java.lang.Object.hashCode()
supr java.lang.Object javax.slee.profile.query.QueryExpression javax.slee.profile.query.SimpleQueryExpression javax.slee.profile.query.OrderedQueryExpression
CLSS public final javax.slee.profile.query.HasPrefix
cons public javax.slee.profile.query.HasPrefix.HasPrefix(java.lang.String,java.lang.String)
cons public javax.slee.profile.query.HasPrefix.HasPrefix(java.lang.String,java.lang.String,javax.slee.profile.query.QueryCollator)
intf java.io.Serializable
meth protected final void javax.slee.profile.query.SimpleQueryExpression.toString(java.lang.StringBuffer)
meth protected java.lang.Object java.lang.Object.clone() throws java.lang.CloneNotSupportedException
meth protected java.lang.String javax.slee.profile.query.HasPrefix.getRelation()
meth protected void java.lang.Object.finalize() throws java.lang.Throwable
meth public boolean java.lang.Object.equals(java.lang.Object)
meth public final java.lang.Class java.lang.Object.getClass()
meth public final java.lang.Object javax.slee.profile.query.SimpleQueryExpression.getAttributeValue()
meth public final java.lang.String javax.slee.profile.query.QueryExpression.toString()
meth public final java.lang.String javax.slee.profile.query.SimpleQueryExpression.getAttributeName()
meth public final javax.slee.profile.query.QueryCollator javax.slee.profile.query.SimpleQueryExpression.getCollator()
meth public final void java.lang.Object.notify()
meth public final void java.lang.Object.notifyAll()
meth public final void java.lang.Object.wait() throws java.lang.InterruptedException
meth public final void java.lang.Object.wait(long) throws java.lang.InterruptedException
meth public final void java.lang.Object.wait(long,int) throws java.lang.InterruptedException
meth public int java.lang.Object.hashCode()
supr java.lang.Object javax.slee.profile.query.QueryExpression javax.slee.profile.query.SimpleQueryExpression
CLSS public final javax.slee.profile.query.LessThan
cons public javax.slee.profile.query.LessThan.LessThan(java.lang.String,java.lang.Object)
cons public javax.slee.profile.query.LessThan.LessThan(java.lang.String,java.lang.String,javax.slee.profile.query.QueryCollator)
intf java.io.Serializable
meth protected final void javax.slee.profile.query.SimpleQueryExpression.toString(java.lang.StringBuffer)
meth protected java.lang.Object java.lang.Object.clone() throws java.lang.CloneNotSupportedException
meth protected java.lang.String javax.slee.profile.query.LessThan.getRelation()
meth protected void java.lang.Object.finalize() throws java.lang.Throwable
meth public boolean java.lang.Object.equals(java.lang.Object)
meth public final java.lang.Class java.lang.Object.getClass()
meth public final java.lang.Object javax.slee.profile.query.SimpleQueryExpression.getAttributeValue()
meth public final java.lang.String javax.slee.profile.query.QueryExpression.toString()
meth public final java.lang.String javax.slee.profile.query.SimpleQueryExpression.getAttributeName()
meth public final javax.slee.profile.query.QueryCollator javax.slee.profile.query.SimpleQueryExpression.getCollator()
meth public final void java.lang.Object.notify()
meth public final void java.lang.Object.notifyAll()
meth public final void java.lang.Object.wait() throws java.lang.InterruptedException
meth public final void java.lang.Object.wait(long) throws java.lang.InterruptedException
meth public final void java.lang.Object.wait(long,int) throws java.lang.InterruptedException
meth public int java.lang.Object.hashCode()
supr java.lang.Object javax.slee.profile.query.QueryExpression javax.slee.profile.query.SimpleQueryExpression javax.slee.profile.query.OrderedQueryExpression
CLSS public final javax.slee.profile.query.LessThanOrEquals
cons public javax.slee.profile.query.LessThanOrEquals.LessThanOrEquals(java.lang.String,java.lang.Object)
cons public javax.slee.profile.query.LessThanOrEquals.LessThanOrEquals(java.lang.String,java.lang.String,javax.slee.profile.query.QueryCollator)
intf java.io.Serializable
meth protected final void javax.slee.profile.query.SimpleQueryExpression.toString(java.lang.StringBuffer)
meth protected java.lang.Object java.lang.Object.clone() throws java.lang.CloneNotSupportedException
meth protected java.lang.String javax.slee.profile.query.LessThanOrEquals.getRelation()
meth protected void java.lang.Object.finalize() throws java.lang.Throwable
meth public boolean java.lang.Object.equals(java.lang.Object)
meth public final java.lang.Class java.lang.Object.getClass()
meth public final java.lang.Object javax.slee.profile.query.SimpleQueryExpression.getAttributeValue()
meth public final java.lang.String javax.slee.profile.query.QueryExpression.toString()
meth public final java.lang.String javax.slee.profile.query.SimpleQueryExpression.getAttributeName()
meth public final javax.slee.profile.query.QueryCollator javax.slee.profile.query.SimpleQueryExpression.getCollator()
meth public final void java.lang.Object.notify()
meth public final void java.lang.Object.notifyAll()
meth public final void java.lang.Object.wait() throws java.lang.InterruptedException
meth public final void java.lang.Object.wait(long) throws java.lang.InterruptedException
meth public final void java.lang.Object.wait(long,int) throws java.lang.InterruptedException
meth public int java.lang.Object.hashCode()
supr java.lang.Object javax.slee.profile.query.QueryExpression javax.slee.profile.query.SimpleQueryExpression javax.slee.profile.query.OrderedQueryExpression
CLSS public final javax.slee.profile.query.LongestPrefixMatch
cons public javax.slee.profile.query.LongestPrefixMatch.LongestPrefixMatch(java.lang.String,java.lang.String)
cons public javax.slee.profile.query.LongestPrefixMatch.LongestPrefixMatch(java.lang.String,java.lang.String,javax.slee.profile.query.QueryCollator)
intf java.io.Serializable
meth protected final void javax.slee.profile.query.SimpleQueryExpression.toString(java.lang.StringBuffer)
meth protected java.lang.Object java.lang.Object.clone() throws java.lang.CloneNotSupportedException
meth protected java.lang.String javax.slee.profile.query.LongestPrefixMatch.getRelation()
meth protected void java.lang.Object.finalize() throws java.lang.Throwable
meth public boolean java.lang.Object.equals(java.lang.Object)
meth public final java.lang.Class java.lang.Object.getClass()
meth public final java.lang.Object javax.slee.profile.query.SimpleQueryExpression.getAttributeValue()
meth public final java.lang.String javax.slee.profile.query.QueryExpression.toString()
meth public final java.lang.String javax.slee.profile.query.SimpleQueryExpression.getAttributeName()
meth public final javax.slee.profile.query.QueryCollator javax.slee.profile.query.SimpleQueryExpression.getCollator()
meth public final void java.lang.Object.notify()
meth public final void java.lang.Object.notifyAll()
meth public final void java.lang.Object.wait() throws java.lang.InterruptedException
meth public final void java.lang.Object.wait(long) throws java.lang.InterruptedException
meth public final void java.lang.Object.wait(long,int) throws java.lang.InterruptedException
meth public int java.lang.Object.hashCode()
supr java.lang.Object javax.slee.profile.query.QueryExpression javax.slee.profile.query.SimpleQueryExpression
CLSS public final javax.slee.profile.query.Not
cons public javax.slee.profile.query.Not.Not(javax.slee.profile.query.QueryExpression)
intf java.io.Serializable
meth protected java.lang.Object java.lang.Object.clone() throws java.lang.CloneNotSupportedException
meth protected void java.lang.Object.finalize() throws java.lang.Throwable
meth protected void javax.slee.profile.query.Not.toString(java.lang.StringBuffer)
meth public boolean java.lang.Object.equals(java.lang.Object)
meth public final java.lang.Class java.lang.Object.getClass()
meth public final java.lang.String javax.slee.profile.query.QueryExpression.toString()
meth public final void java.lang.Object.notify()
meth public final void java.lang.Object.notifyAll()
meth public final void java.lang.Object.wait() throws java.lang.InterruptedException
meth public final void java.lang.Object.wait(long) throws java.lang.InterruptedException
meth public final void java.lang.Object.wait(long,int) throws java.lang.InterruptedException
meth public int java.lang.Object.hashCode()
meth public javax.slee.profile.query.QueryExpression javax.slee.profile.query.Not.getExpression()
supr java.lang.Object javax.slee.profile.query.QueryExpression
CLSS public final javax.slee.profile.query.NotEquals
cons public javax.slee.profile.query.NotEquals.NotEquals(java.lang.String,java.lang.Object)
cons public javax.slee.profile.query.NotEquals.NotEquals(java.lang.String,java.lang.String,javax.slee.profile.query.QueryCollator)
intf java.io.Serializable
meth protected final void javax.slee.profile.query.SimpleQueryExpression.toString(java.lang.StringBuffer)
meth protected java.lang.Object java.lang.Object.clone() throws java.lang.CloneNotSupportedException
meth protected java.lang.String javax.slee.profile.query.NotEquals.getRelation()
meth protected void java.lang.Object.finalize() throws java.lang.Throwable
meth public boolean java.lang.Object.equals(java.lang.Object)
meth public final java.lang.Class java.lang.Object.getClass()
meth public final java.lang.Object javax.slee.profile.query.SimpleQueryExpression.getAttributeValue()
meth public final java.lang.String javax.slee.profile.query.QueryExpression.toString()
meth public final java.lang.String javax.slee.profile.query.SimpleQueryExpression.getAttributeName()
meth public final javax.slee.profile.query.QueryCollator javax.slee.profile.query.SimpleQueryExpression.getCollator()
meth public final void java.lang.Object.notify()
meth public final void java.lang.Object.notifyAll()
meth public final void java.lang.Object.wait() throws java.lang.InterruptedException
meth public final void java.lang.Object.wait(long) throws java.lang.InterruptedException
meth public final void java.lang.Object.wait(long,int) throws java.lang.InterruptedException
meth public int java.lang.Object.hashCode()
supr java.lang.Object javax.slee.profile.query.QueryExpression javax.slee.profile.query.SimpleQueryExpression
CLSS public final javax.slee.profile.query.Or
cons public javax.slee.profile.query.Or.Or([Ljavax.slee.profile.query.QueryExpression;)
cons public javax.slee.profile.query.Or.Or(javax.slee.profile.query.QueryExpression,javax.slee.profile.query.QueryExpression)
intf java.io.Serializable
meth protected final void javax.slee.profile.query.CompositeQueryExpression.add(javax.slee.profile.query.QueryExpression)
meth protected java.lang.Object java.lang.Object.clone() throws java.lang.CloneNotSupportedException
meth protected void java.lang.Object.finalize() throws java.lang.Throwable
meth protected void javax.slee.profile.query.Or.toString(java.lang.StringBuffer)
meth public boolean java.lang.Object.equals(java.lang.Object)
meth public final [Ljavax.slee.profile.query.QueryExpression; javax.slee.profile.query.CompositeQueryExpression.getExpressions()
meth public final java.lang.Class java.lang.Object.getClass()
meth public final java.lang.String javax.slee.profile.query.QueryExpression.toString()
meth public final void java.lang.Object.notify()
meth public final void java.lang.Object.notifyAll()
meth public final void java.lang.Object.wait() throws java.lang.InterruptedException
meth public final void java.lang.Object.wait(long) throws java.lang.InterruptedException
meth public final void java.lang.Object.wait(long,int) throws java.lang.InterruptedException
meth public int java.lang.Object.hashCode()
meth public javax.slee.profile.query.Or javax.slee.profile.query.Or.or(javax.slee.profile.query.QueryExpression)
supr java.lang.Object javax.slee.profile.query.QueryExpression javax.slee.profile.query.CompositeQueryExpression
CLSS public abstract javax.slee.profile.query.OrderedQueryExpression
cons protected javax.slee.profile.query.OrderedQueryExpression.OrderedQueryExpression(java.lang.String,java.lang.Object,javax.slee.profile.query.QueryCollator)
intf java.io.Serializable
meth protected abstract java.lang.String javax.slee.profile.query.SimpleQueryExpression.getRelation()
meth protected final void javax.slee.profile.query.SimpleQueryExpression.toString(java.lang.StringBuffer)
meth protected java.lang.Object java.lang.Object.clone() throws java.lang.CloneNotSupportedException
meth protected void java.lang.Object.finalize() throws java.lang.Throwable
meth public boolean java.lang.Object.equals(java.lang.Object)
meth public final java.lang.Class java.lang.Object.getClass()
meth public final java.lang.Object javax.slee.profile.query.SimpleQueryExpression.getAttributeValue()
meth public final java.lang.String javax.slee.profile.query.QueryExpression.toString()
meth public final java.lang.String javax.slee.profile.query.SimpleQueryExpression.getAttributeName()
meth public final javax.slee.profile.query.QueryCollator javax.slee.profile.query.SimpleQueryExpression.getCollator()
meth public final void java.lang.Object.notify()
meth public final void java.lang.Object.notifyAll()
meth public final void java.lang.Object.wait() throws java.lang.InterruptedException
meth public final void java.lang.Object.wait(long) throws java.lang.InterruptedException
meth public final void java.lang.Object.wait(long,int) throws java.lang.InterruptedException
meth public int java.lang.Object.hashCode()
supr java.lang.Object javax.slee.profile.query.QueryExpression javax.slee.profile.query.SimpleQueryExpression
CLSS public final javax.slee.profile.query.QueryCollator
cons public javax.slee.profile.query.QueryCollator.QueryCollator(java.util.Locale)
cons public javax.slee.profile.query.QueryCollator.QueryCollator(java.util.Locale,int)
cons public javax.slee.profile.query.QueryCollator.QueryCollator(java.util.Locale,int,int)
intf java.io.Serializable
meth protected java.lang.Object java.lang.Object.clone() throws java.lang.CloneNotSupportedException
meth protected void java.lang.Object.finalize() throws java.lang.Throwable
meth public boolean javax.slee.profile.query.QueryCollator.equals(java.lang.Object)
meth public boolean javax.slee.profile.query.QueryCollator.hasDecomposition()
meth public boolean javax.slee.profile.query.QueryCollator.hasStrength()
meth public final java.lang.Class java.lang.Object.getClass()
meth public final void java.lang.Object.notify()
meth public final void java.lang.Object.notifyAll()
meth public final void java.lang.Object.wait() throws java.lang.InterruptedException
meth public final void java.lang.Object.wait(long) throws java.lang.InterruptedException
meth public final void java.lang.Object.wait(long,int) throws java.lang.InterruptedException
meth public int javax.slee.profile.query.QueryCollator.getDecomposition()
meth public int javax.slee.profile.query.QueryCollator.getStrength()
meth public int javax.slee.profile.query.QueryCollator.hashCode()
meth public java.lang.String javax.slee.profile.query.QueryCollator.toString()
meth public java.text.Collator javax.slee.profile.query.QueryCollator.getCollator()
meth public java.util.Locale javax.slee.profile.query.QueryCollator.getLocale()
supr java.lang.Object
CLSS public abstract javax.slee.profile.query.QueryExpression
cons public javax.slee.profile.query.QueryExpression.QueryExpression()
intf java.io.Serializable
meth protected abstract void javax.slee.profile.query.QueryExpression.toString(java.lang.StringBuffer)
meth protected java.lang.Object java.lang.Object.clone() throws java.lang.CloneNotSupportedException
meth protected void java.lang.Object.finalize() throws java.lang.Throwable
meth public boolean java.lang.Object.equals(java.lang.Object)
meth public final java.lang.Class java.lang.Object.getClass()
meth public final java.lang.String javax.slee.profile.query.QueryExpression.toString()
meth public final void java.lang.Object.notify()
meth public final void java.lang.Object.notifyAll()
meth public final void java.lang.Object.wait() throws java.lang.InterruptedException
meth public final void java.lang.Object.wait(long) throws java.lang.InterruptedException
meth public final void java.lang.Object.wait(long,int) throws java.lang.InterruptedException
meth public int java.lang.Object.hashCode()
supr java.lang.Object
CLSS public final javax.slee.profile.query.RangeMatch
cons public javax.slee.profile.query.RangeMatch.RangeMatch(java.lang.String,java.lang.Object,java.lang.Object)
cons public javax.slee.profile.query.RangeMatch.RangeMatch(java.lang.String,java.lang.String,java.lang.String,javax.slee.profile.query.QueryCollator)
intf java.io.Serializable
meth protected java.lang.Object java.lang.Object.clone() throws java.lang.CloneNotSupportedException
meth protected void java.lang.Object.finalize() throws java.lang.Throwable
meth protected void javax.slee.profile.query.RangeMatch.toString(java.lang.StringBuffer)
meth public boolean java.lang.Object.equals(java.lang.Object)
meth public final java.lang.Class java.lang.Object.getClass()
meth public final java.lang.String javax.slee.profile.query.QueryExpression.toString()
meth public final void java.lang.Object.notify()
meth public final void java.lang.Object.notifyAll()
meth public final void java.lang.Object.wait() throws java.lang.InterruptedException
meth public final void java.lang.Object.wait(long) throws java.lang.InterruptedException
meth public final void java.lang.Object.wait(long,int) throws java.lang.InterruptedException
meth public int java.lang.Object.hashCode()
meth public java.lang.Object javax.slee.profile.query.RangeMatch.getFromValue()
meth public java.lang.Object javax.slee.profile.query.RangeMatch.getToValue()
meth public java.lang.String javax.slee.profile.query.RangeMatch.getAttributeName()
meth public javax.slee.profile.query.QueryCollator javax.slee.profile.query.RangeMatch.getCollator()
supr java.lang.Object javax.slee.profile.query.QueryExpression
CLSS public abstract javax.slee.profile.query.SimpleQueryExpression
cons protected javax.slee.profile.query.SimpleQueryExpression.SimpleQueryExpression(java.lang.String,java.lang.Object,javax.slee.profile.query.QueryCollator)
intf java.io.Serializable
meth protected abstract java.lang.String javax.slee.profile.query.SimpleQueryExpression.getRelation()
meth protected final void javax.slee.profile.query.SimpleQueryExpression.toString(java.lang.StringBuffer)
meth protected java.lang.Object java.lang.Object.clone() throws java.lang.CloneNotSupportedException
meth protected void java.lang.Object.finalize() throws java.lang.Throwable
meth public boolean java.lang.Object.equals(java.lang.Object)
meth public final java.lang.Class java.lang.Object.getClass()
meth public final java.lang.Object javax.slee.profile.query.SimpleQueryExpression.getAttributeValue()
meth public final java.lang.String javax.slee.profile.query.QueryExpression.toString()
meth public final java.lang.String javax.slee.profile.query.SimpleQueryExpression.getAttributeName()
meth public final javax.slee.profile.query.QueryCollator javax.slee.profile.query.SimpleQueryExpression.getCollator()
meth public final void java.lang.Object.notify()
meth public final void java.lang.Object.notifyAll()
meth public final void java.lang.Object.wait() throws java.lang.InterruptedException
meth public final void java.lang.Object.wait(long) throws java.lang.InterruptedException
meth public final void java.lang.Object.wait(long,int) throws java.lang.InterruptedException
meth public int java.lang.Object.hashCode()
supr java.lang.Object javax.slee.profile.query.QueryExpression
CLSS public javax.slee.resource.ActivityAlreadyExistsException
cons public javax.slee.resource.ActivityAlreadyExistsException.ActivityAlreadyExistsException(java.lang.String)
intf java.io.Serializable
meth protected java.lang.Object java.lang.Object.clone() throws java.lang.CloneNotSupportedException
meth protected void java.lang.Object.finalize() throws java.lang.Throwable
meth public [Ljava.lang.StackTraceElement; java.lang.Throwable.getStackTrace()
meth public boolean java.lang.Object.equals(java.lang.Object)
meth public final java.lang.Class java.lang.Object.getClass()
meth public final void java.lang.Object.notify()
meth public final void java.lang.Object.notifyAll()
meth public final void java.lang.Object.wait() throws java.lang.InterruptedException
meth public final void java.lang.Object.wait(long) throws java.lang.InterruptedException
meth public final void java.lang.Object.wait(long,int) throws java.lang.InterruptedException
meth public int java.lang.Object.hashCode()
meth public java.lang.String java.lang.Throwable.getLocalizedMessage()
meth public java.lang.String java.lang.Throwable.getMessage()
meth public java.lang.String java.lang.Throwable.toString()
meth public java.lang.Throwable java.lang.Throwable.fillInStackTrace()
meth public java.lang.Throwable java.lang.Throwable.initCause(java.lang.Throwable)
meth public java.lang.Throwable javax.slee.SLEEException.getCause()
meth public void java.lang.Throwable.printStackTrace()
meth public void java.lang.Throwable.printStackTrace(java.io.PrintStream)
meth public void java.lang.Throwable.printStackTrace(java.io.PrintWriter)
meth public void java.lang.Throwable.setStackTrace([Ljava.lang.StackTraceElement;)
supr java.lang.Object java.lang.Throwable java.lang.Exception java.lang.RuntimeException javax.slee.SLEEException
CLSS public final javax.slee.resource.ActivityFlags
fld  public static final int javax.slee.resource.ActivityFlags.NO_FLAGS
fld  public static final int javax.slee.resource.ActivityFlags.REQUEST_ACTIVITY_UNREFERENCED_CALLBACK
fld  public static final int javax.slee.resource.ActivityFlags.REQUEST_ENDED_CALLBACK
fld  public static final int javax.slee.resource.ActivityFlags.SLEE_MAY_MARSHAL
fld  public static final int javax.slee.resource.ActivityFlags.STANDARD_FLAGS_MASK
fld  public static final int javax.slee.resource.ActivityFlags.VENDOR_FLAGS_MASK
meth protected java.lang.Object java.lang.Object.clone() throws java.lang.CloneNotSupportedException
meth protected void java.lang.Object.finalize() throws java.lang.Throwable
meth public boolean java.lang.Object.equals(java.lang.Object)
meth public final java.lang.Class java.lang.Object.getClass()
meth public final void java.lang.Object.notify()
meth public final void java.lang.Object.notifyAll()
meth public final void java.lang.Object.wait() throws java.lang.InterruptedException
meth public final void java.lang.Object.wait(long) throws java.lang.InterruptedException
meth public final void java.lang.Object.wait(long,int) throws java.lang.InterruptedException
meth public int java.lang.Object.hashCode()
meth public java.lang.String java.lang.Object.toString()
meth public static boolean javax.slee.resource.ActivityFlags.hasFlags(int,int)
meth public static boolean javax.slee.resource.ActivityFlags.hasNoFlags(int)
meth public static boolean javax.slee.resource.ActivityFlags.hasRequestEndedCallback(int)
meth public static boolean javax.slee.resource.ActivityFlags.hasRequestSleeActivityGCCallback(int)
meth public static boolean javax.slee.resource.ActivityFlags.hasSleeMayMarshal(int)
meth public static boolean javax.slee.resource.ActivityFlags.hasStandardFlags(int)
meth public static boolean javax.slee.resource.ActivityFlags.hasVendorFlags(int)
meth public static int javax.slee.resource.ActivityFlags.setRequestEndedCallback(int)
meth public static int javax.slee.resource.ActivityFlags.setRequestSleeActivityGCCallback(int)
meth public static int javax.slee.resource.ActivityFlags.setSleeMayMarshal(int)
meth public static java.lang.String javax.slee.resource.ActivityFlags.toString(int)
supr java.lang.Object
CLSS public abstract interface javax.slee.resource.ActivityHandle
meth public abstract boolean javax.slee.resource.ActivityHandle.equals(java.lang.Object)
meth public abstract int javax.slee.resource.ActivityHandle.hashCode()
supr null
CLSS public javax.slee.resource.ActivityIsEndingException
cons public javax.slee.resource.ActivityIsEndingException.ActivityIsEndingException()
cons public javax.slee.resource.ActivityIsEndingException.ActivityIsEndingException(java.lang.String)
intf java.io.Serializable
meth protected java.lang.Object java.lang.Object.clone() throws java.lang.CloneNotSupportedException
meth protected void java.lang.Object.finalize() throws java.lang.Throwable
meth public [Ljava.lang.StackTraceElement; java.lang.Throwable.getStackTrace()
meth public boolean java.lang.Object.equals(java.lang.Object)
meth public final java.lang.Class java.lang.Object.getClass()
meth public final void java.lang.Object.notify()
meth public final void java.lang.Object.notifyAll()
meth public final void java.lang.Object.wait() throws java.lang.InterruptedException
meth public final void java.lang.Object.wait(long) throws java.lang.InterruptedException
meth public final void java.lang.Object.wait(long,int) throws java.lang.InterruptedException
meth public int java.lang.Object.hashCode()
meth public java.lang.String java.lang.Throwable.getLocalizedMessage()
meth public java.lang.String java.lang.Throwable.getMessage()
meth public java.lang.String java.lang.Throwable.toString()
meth public java.lang.Throwable java.lang.Throwable.fillInStackTrace()
meth public java.lang.Throwable java.lang.Throwable.initCause(java.lang.Throwable)
meth public java.lang.Throwable javax.slee.SLEEException.getCause()
meth public void java.lang.Throwable.printStackTrace()
meth public void java.lang.Throwable.printStackTrace(java.io.PrintStream)
meth public void java.lang.Throwable.printStackTrace(java.io.PrintWriter)
meth public void java.lang.Throwable.setStackTrace([Ljava.lang.StackTraceElement;)
supr java.lang.Object java.lang.Throwable java.lang.Exception java.lang.RuntimeException javax.slee.SLEEException
CLSS public final javax.slee.resource.ConfigProperties
cons public javax.slee.resource.ConfigProperties.ConfigProperties()
cons public javax.slee.resource.ConfigProperties.ConfigProperties([Ljavax.slee.resource.ConfigProperties$Property;)
innr public static final javax.slee.resource.ConfigProperties$Property
intf java.io.Serializable
intf java.lang.Cloneable
meth protected void java.lang.Object.finalize() throws java.lang.Throwable
meth public [Ljavax.slee.resource.ConfigProperties$Property; javax.slee.resource.ConfigProperties.getProperties()
meth public boolean java.lang.Object.equals(java.lang.Object)
meth public final java.lang.Class java.lang.Object.getClass()
meth public final void java.lang.Object.notify()
meth public final void java.lang.Object.notifyAll()
meth public final void java.lang.Object.wait() throws java.lang.InterruptedException
meth public final void java.lang.Object.wait(long) throws java.lang.InterruptedException
meth public final void java.lang.Object.wait(long,int) throws java.lang.InterruptedException
meth public int java.lang.Object.hashCode()
meth public java.lang.Object javax.slee.resource.ConfigProperties.clone()
meth public java.lang.String javax.slee.resource.ConfigProperties.toString()
meth public javax.slee.resource.ConfigProperties$Property javax.slee.resource.ConfigProperties.getProperty(java.lang.String)
meth public void javax.slee.resource.ConfigProperties.addProperty(javax.slee.resource.ConfigProperties$Property)
supr java.lang.Object
CLSS public final javax.slee.resource.EventFlags
fld  public static final int javax.slee.resource.EventFlags.NO_FLAGS
fld  public static final int javax.slee.resource.EventFlags.REQUEST_EVENT_UNREFERENCED_CALLBACK
fld  public static final int javax.slee.resource.EventFlags.REQUEST_PROCESSING_FAILED_CALLBACK
fld  public static final int javax.slee.resource.EventFlags.REQUEST_PROCESSING_SUCCESSFUL_CALLBACK
fld  public static final int javax.slee.resource.EventFlags.SBB_PROCESSED_EVENT
fld  public static final int javax.slee.resource.EventFlags.SLEE_MAY_MARSHAL
fld  public static final int javax.slee.resource.EventFlags.STANDARD_FLAGS_MASK
fld  public static final int javax.slee.resource.EventFlags.VENDOR_FLAGS_MASK
meth protected java.lang.Object java.lang.Object.clone() throws java.lang.CloneNotSupportedException
meth protected void java.lang.Object.finalize() throws java.lang.Throwable
meth public boolean java.lang.Object.equals(java.lang.Object)
meth public final java.lang.Class java.lang.Object.getClass()
meth public final void java.lang.Object.notify()
meth public final void java.lang.Object.notifyAll()
meth public final void java.lang.Object.wait() throws java.lang.InterruptedException
meth public final void java.lang.Object.wait(long) throws java.lang.InterruptedException
meth public final void java.lang.Object.wait(long,int) throws java.lang.InterruptedException
meth public int java.lang.Object.hashCode()
meth public java.lang.String java.lang.Object.toString()
meth public static boolean javax.slee.resource.EventFlags.hasFlags(int,int)
meth public static boolean javax.slee.resource.EventFlags.hasNoFlags(int)
meth public static boolean javax.slee.resource.EventFlags.hasRequestEventReferenceReleasedCallback(int)
meth public static boolean javax.slee.resource.EventFlags.hasRequestProcessingFailedCallback(int)
meth public static boolean javax.slee.resource.EventFlags.hasRequestProcessingSuccessfulCallback(int)
meth public static boolean javax.slee.resource.EventFlags.hasSbbProcessedEvent(int)
meth public static boolean javax.slee.resource.EventFlags.hasSleeMayMarshal(int)
meth public static boolean javax.slee.resource.EventFlags.hasStandardFlags(int)
meth public static boolean javax.slee.resource.EventFlags.hasVendorFlags(int)
meth public static int javax.slee.resource.EventFlags.setRequestEventReferenceReleasedCallback(int)
meth public static int javax.slee.resource.EventFlags.setRequestProcessingFailedCallback(int)
meth public static int javax.slee.resource.EventFlags.setRequestProcessingSuccessfulCallback(int)
meth public static int javax.slee.resource.EventFlags.setSbbProcessedEvent(int)
meth public static int javax.slee.resource.EventFlags.setSleeMayMarshal(int)
meth public static java.lang.String javax.slee.resource.EventFlags.toString(int)
supr java.lang.Object
CLSS public javax.slee.resource.FailureReason
cons protected javax.slee.resource.FailureReason.FailureReason(int)
fld  public static final int javax.slee.resource.FailureReason.REASON_EVENT_MARSHALING_ERROR
fld  public static final int javax.slee.resource.FailureReason.REASON_EVENT_QUEUE_FULL
fld  public static final int javax.slee.resource.FailureReason.REASON_EVENT_QUEUE_TIMEOUT
fld  public static final int javax.slee.resource.FailureReason.REASON_FIRING_TRANSACTION_ROLLED_BACK
fld  public static final int javax.slee.resource.FailureReason.REASON_OTHER_REASON
fld  public static final int javax.slee.resource.FailureReason.REASON_SYSTEM_OVERLOAD
fld  public static final javax.slee.resource.FailureReason javax.slee.resource.FailureReason.EVENT_MARSHALING_ERROR
fld  public static final javax.slee.resource.FailureReason javax.slee.resource.FailureReason.EVENT_QUEUE_FULL
fld  public static final javax.slee.resource.FailureReason javax.slee.resource.FailureReason.EVENT_QUEUE_TIMEOUT
fld  public static final javax.slee.resource.FailureReason javax.slee.resource.FailureReason.FIRING_TRANSACTION_ROLLED_BACK
fld  public static final javax.slee.resource.FailureReason javax.slee.resource.FailureReason.OTHER_REASON
fld  public static final javax.slee.resource.FailureReason javax.slee.resource.FailureReason.SYSTEM_OVERLOAD
meth protected final int javax.slee.resource.FailureReason.getReason()
meth protected java.lang.Object java.lang.Object.clone() throws java.lang.CloneNotSupportedException
meth protected void java.lang.Object.finalize() throws java.lang.Throwable
meth public boolean javax.slee.resource.FailureReason.equals(java.lang.Object)
meth public final boolean javax.slee.resource.FailureReason.isEventMarshalingError()
meth public final boolean javax.slee.resource.FailureReason.isEventQueueFull()
meth public final boolean javax.slee.resource.FailureReason.isEventQueueTimeout()
meth public final boolean javax.slee.resource.FailureReason.isFiringTransactionRolledBack()
meth public final boolean javax.slee.resource.FailureReason.isOtherReason()
meth public final boolean javax.slee.resource.FailureReason.isSystemOverLoad()
meth public final int javax.slee.resource.FailureReason.hashCode()
meth public final int javax.slee.resource.FailureReason.toInt()
meth public final java.lang.Class java.lang.Object.getClass()
meth public final void java.lang.Object.notify()
meth public final void java.lang.Object.notifyAll()
meth public final void java.lang.Object.wait() throws java.lang.InterruptedException
meth public final void java.lang.Object.wait(long) throws java.lang.InterruptedException
meth public final void java.lang.Object.wait(long,int) throws java.lang.InterruptedException
meth public java.lang.String javax.slee.resource.FailureReason.toString()
meth public static javax.slee.resource.FailureReason javax.slee.resource.FailureReason.fromInt(int)
supr java.lang.Object
CLSS public javax.slee.resource.FireEventException
cons public javax.slee.resource.FireEventException.FireEventException()
cons public javax.slee.resource.FireEventException.FireEventException(java.lang.String)
intf java.io.Serializable
meth protected java.lang.Object java.lang.Object.clone() throws java.lang.CloneNotSupportedException
meth protected void java.lang.Object.finalize() throws java.lang.Throwable
meth public [Ljava.lang.StackTraceElement; java.lang.Throwable.getStackTrace()
meth public boolean java.lang.Object.equals(java.lang.Object)
meth public final java.lang.Class java.lang.Object.getClass()
meth public final void java.lang.Object.notify()
meth public final void java.lang.Object.notifyAll()
meth public final void java.lang.Object.wait() throws java.lang.InterruptedException
meth public final void java.lang.Object.wait(long) throws java.lang.InterruptedException
meth public final void java.lang.Object.wait(long,int) throws java.lang.InterruptedException
meth public int java.lang.Object.hashCode()
meth public java.lang.String java.lang.Throwable.getLocalizedMessage()
meth public java.lang.String java.lang.Throwable.getMessage()
meth public java.lang.String java.lang.Throwable.toString()
meth public java.lang.Throwable java.lang.Throwable.fillInStackTrace()
meth public java.lang.Throwable java.lang.Throwable.getCause()
meth public java.lang.Throwable java.lang.Throwable.initCause(java.lang.Throwable)
meth public void java.lang.Throwable.printStackTrace()
meth public void java.lang.Throwable.printStackTrace(java.io.PrintStream)
meth public void java.lang.Throwable.printStackTrace(java.io.PrintWriter)
meth public void java.lang.Throwable.setStackTrace([Ljava.lang.StackTraceElement;)
supr java.lang.Object java.lang.Throwable java.lang.Exception
CLSS public abstract interface javax.slee.resource.FireableEventType
meth public abstract java.lang.ClassLoader javax.slee.resource.FireableEventType.getEventClassLoader()
meth public abstract java.lang.String javax.slee.resource.FireableEventType.getEventClassName()
meth public abstract javax.slee.EventTypeID javax.slee.resource.FireableEventType.getEventType()
supr null
CLSS public javax.slee.resource.IllegalEventException
cons public javax.slee.resource.IllegalEventException.IllegalEventException(java.lang.String)
cons public javax.slee.resource.IllegalEventException.IllegalEventException(java.lang.String,java.lang.Throwable)
intf java.io.Serializable
meth protected java.lang.Object java.lang.Object.clone() throws java.lang.CloneNotSupportedException
meth protected void java.lang.Object.finalize() throws java.lang.Throwable
meth public [Ljava.lang.StackTraceElement; java.lang.Throwable.getStackTrace()
meth public boolean java.lang.Object.equals(java.lang.Object)
meth public final java.lang.Class java.lang.Object.getClass()
meth public final void java.lang.Object.notify()
meth public final void java.lang.Object.notifyAll()
meth public final void java.lang.Object.wait() throws java.lang.InterruptedException
meth public final void java.lang.Object.wait(long) throws java.lang.InterruptedException
meth public final void java.lang.Object.wait(long,int) throws java.lang.InterruptedException
meth public int java.lang.Object.hashCode()
meth public java.lang.String java.lang.Throwable.getLocalizedMessage()
meth public java.lang.String java.lang.Throwable.getMessage()
meth public java.lang.String java.lang.Throwable.toString()
meth public java.lang.Throwable java.lang.Throwable.fillInStackTrace()
meth public java.lang.Throwable java.lang.Throwable.initCause(java.lang.Throwable)
meth public java.lang.Throwable javax.slee.SLEEException.getCause()
meth public void java.lang.Throwable.printStackTrace()
meth public void java.lang.Throwable.printStackTrace(java.io.PrintStream)
meth public void java.lang.Throwable.printStackTrace(java.io.PrintWriter)
meth public void java.lang.Throwable.setStackTrace([Ljava.lang.StackTraceElement;)
supr java.lang.Object java.lang.Throwable java.lang.Exception java.lang.RuntimeException javax.slee.SLEEException
CLSS public javax.slee.resource.InvalidConfigurationException
cons public javax.slee.resource.InvalidConfigurationException.InvalidConfigurationException(java.lang.String)
cons public javax.slee.resource.InvalidConfigurationException.InvalidConfigurationException(java.lang.String,java.lang.Throwable)
intf java.io.Serializable
meth protected java.lang.Object java.lang.Object.clone() throws java.lang.CloneNotSupportedException
meth protected void java.lang.Object.finalize() throws java.lang.Throwable
meth public [Ljava.lang.StackTraceElement; java.lang.Throwable.getStackTrace()
meth public boolean java.lang.Object.equals(java.lang.Object)
meth public final java.lang.Class java.lang.Object.getClass()
meth public final void java.lang.Object.notify()
meth public final void java.lang.Object.notifyAll()
meth public final void java.lang.Object.wait() throws java.lang.InterruptedException
meth public final void java.lang.Object.wait(long) throws java.lang.InterruptedException
meth public final void java.lang.Object.wait(long,int) throws java.lang.InterruptedException
meth public int java.lang.Object.hashCode()
meth public java.lang.String java.lang.Throwable.getLocalizedMessage()
meth public java.lang.String java.lang.Throwable.getMessage()
meth public java.lang.String java.lang.Throwable.toString()
meth public java.lang.Throwable java.lang.Throwable.fillInStackTrace()
meth public java.lang.Throwable java.lang.Throwable.getCause()
meth public java.lang.Throwable java.lang.Throwable.initCause(java.lang.Throwable)
meth public void java.lang.Throwable.printStackTrace()
meth public void java.lang.Throwable.printStackTrace(java.io.PrintStream)
meth public void java.lang.Throwable.printStackTrace(java.io.PrintWriter)
meth public void java.lang.Throwable.setStackTrace([Ljava.lang.StackTraceElement;)
supr java.lang.Object java.lang.Throwable java.lang.Exception
CLSS public abstract interface javax.slee.resource.Marshaler
meth public abstract int javax.slee.resource.Marshaler.getEstimatedEventSize(javax.slee.resource.FireableEventType,java.lang.Object)
meth public abstract int javax.slee.resource.Marshaler.getEstimatedHandleSize(javax.slee.resource.ActivityHandle)
meth public abstract java.lang.Object javax.slee.resource.Marshaler.unmarshalEvent(javax.slee.resource.FireableEventType,java.io.DataInput) throws java.io.IOException
meth public abstract java.nio.ByteBuffer javax.slee.resource.Marshaler.getEventBuffer(javax.slee.resource.FireableEventType,java.lang.Object)
meth public abstract javax.slee.resource.ActivityHandle javax.slee.resource.Marshaler.unmarshalHandle(java.io.DataInput) throws java.io.IOException
meth public abstract void javax.slee.resource.Marshaler.marshalEvent(javax.slee.resource.FireableEventType,java.lang.Object,java.io.DataOutput) throws java.io.IOException
meth public abstract void javax.slee.resource.Marshaler.marshalHandle(javax.slee.resource.ActivityHandle,java.io.DataOutput) throws java.io.IOException
meth public abstract void javax.slee.resource.Marshaler.releaseEventBuffer(javax.slee.resource.FireableEventType,java.lang.Object,java.nio.ByteBuffer)
supr null
CLSS public abstract interface javax.slee.resource.ReceivableService
innr public static abstract interface javax.slee.resource.ReceivableService$ReceivableEvent
meth public abstract [Ljavax.slee.resource.ReceivableService$ReceivableEvent; javax.slee.resource.ReceivableService.getReceivableEvents()
meth public abstract boolean javax.slee.resource.ReceivableService.equals(java.lang.Object)
meth public abstract javax.slee.ServiceID javax.slee.resource.ReceivableService.getService()
supr null
CLSS public abstract interface javax.slee.resource.ResourceAdaptor
meth public abstract java.lang.Object javax.slee.resource.ResourceAdaptor.getActivity(javax.slee.resource.ActivityHandle)
meth public abstract java.lang.Object javax.slee.resource.ResourceAdaptor.getResourceAdaptorInterface(java.lang.String)
meth public abstract javax.slee.resource.ActivityHandle javax.slee.resource.ResourceAdaptor.getActivityHandle(java.lang.Object)
meth public abstract javax.slee.resource.Marshaler javax.slee.resource.ResourceAdaptor.getMarshaler()
meth public abstract void javax.slee.resource.ResourceAdaptor.activityEnded(javax.slee.resource.ActivityHandle)
meth public abstract void javax.slee.resource.ResourceAdaptor.activityUnreferenced(javax.slee.resource.ActivityHandle)
meth public abstract void javax.slee.resource.ResourceAdaptor.administrativeRemove(javax.slee.resource.ActivityHandle)
meth public abstract void javax.slee.resource.ResourceAdaptor.eventProcessingFailed(javax.slee.resource.ActivityHandle,javax.slee.resource.FireableEventType,java.lang.Object,javax.slee.Address,javax.slee.resource.ReceivableService,int,javax.slee.resource.FailureReason)
meth public abstract void javax.slee.resource.ResourceAdaptor.eventProcessingSuccessful(javax.slee.resource.ActivityHandle,javax.slee.resource.FireableEventType,java.lang.Object,javax.slee.Address,javax.slee.resource.ReceivableService,int)
meth public abstract void javax.slee.resource.ResourceAdaptor.eventUnreferenced(javax.slee.resource.ActivityHandle,javax.slee.resource.FireableEventType,java.lang.Object,javax.slee.Address,javax.slee.resource.ReceivableService,int)
meth public abstract void javax.slee.resource.ResourceAdaptor.queryLiveness(javax.slee.resource.ActivityHandle)
meth public abstract void javax.slee.resource.ResourceAdaptor.raActive()
meth public abstract void javax.slee.resource.ResourceAdaptor.raConfigurationUpdate(javax.slee.resource.ConfigProperties)
meth public abstract void javax.slee.resource.ResourceAdaptor.raConfigure(javax.slee.resource.ConfigProperties)
meth public abstract void javax.slee.resource.ResourceAdaptor.raInactive()
meth public abstract void javax.slee.resource.ResourceAdaptor.raStopping()
meth public abstract void javax.slee.resource.ResourceAdaptor.raUnconfigure()
meth public abstract void javax.slee.resource.ResourceAdaptor.raVerifyConfiguration(javax.slee.resource.ConfigProperties) throws javax.slee.resource.InvalidConfigurationException
meth public abstract void javax.slee.resource.ResourceAdaptor.serviceActive(javax.slee.resource.ReceivableService)
meth public abstract void javax.slee.resource.ResourceAdaptor.serviceInactive(javax.slee.resource.ReceivableService)
meth public abstract void javax.slee.resource.ResourceAdaptor.serviceStopping(javax.slee.resource.ReceivableService)
meth public abstract void javax.slee.resource.ResourceAdaptor.setResourceAdaptorContext(javax.slee.resource.ResourceAdaptorContext)
meth public abstract void javax.slee.resource.ResourceAdaptor.unsetResourceAdaptorContext()
supr null
CLSS public abstract interface javax.slee.resource.ResourceAdaptorContext
meth public abstract [Ljavax.slee.resource.ResourceAdaptorTypeID; javax.slee.resource.ResourceAdaptorContext.getResourceAdaptorTypes()
meth public abstract java.lang.Object javax.slee.resource.ResourceAdaptorContext.getDefaultUsageParameterSet()
meth public abstract java.lang.Object javax.slee.resource.ResourceAdaptorContext.getUsageParameterSet(java.lang.String) throws javax.slee.usage.UnrecognizedUsageParameterSetNameException
meth public abstract java.lang.String javax.slee.resource.ResourceAdaptorContext.getEntityName()
meth public abstract java.util.Timer javax.slee.resource.ResourceAdaptorContext.getTimer()
meth public abstract javax.slee.ServiceID javax.slee.resource.ResourceAdaptorContext.getInvokingService()
meth public abstract javax.slee.facilities.AlarmFacility javax.slee.resource.ResourceAdaptorContext.getAlarmFacility()
meth public abstract javax.slee.facilities.EventLookupFacility javax.slee.resource.ResourceAdaptorContext.getEventLookupFacility()
meth public abstract javax.slee.facilities.ServiceLookupFacility javax.slee.resource.ResourceAdaptorContext.getServiceLookupFacility()
meth public abstract javax.slee.facilities.Tracer javax.slee.resource.ResourceAdaptorContext.getTracer(java.lang.String)
meth public abstract javax.slee.profile.ProfileTable javax.slee.resource.ResourceAdaptorContext.getProfileTable(java.lang.String) throws javax.slee.profile.UnrecognizedProfileTableNameException
meth public abstract javax.slee.resource.ResourceAdaptorID javax.slee.resource.ResourceAdaptorContext.getResourceAdaptor()
meth public abstract javax.slee.resource.SleeEndpoint javax.slee.resource.ResourceAdaptorContext.getSleeEndpoint()
meth public abstract javax.slee.transaction.SleeTransactionManager javax.slee.resource.ResourceAdaptorContext.getSleeTransactionManager()
supr null
CLSS public javax.slee.resource.ResourceAdaptorDescriptor
cons public javax.slee.resource.ResourceAdaptorDescriptor.ResourceAdaptorDescriptor(javax.slee.resource.ResourceAdaptorID,javax.slee.management.DeployableUnitID,java.lang.String,[Ljavax.slee.management.LibraryID;,[Ljavax.slee.resource.ResourceAdaptorTypeID;,[Ljavax.slee.profile.ProfileSpecificationID;,boolean)
intf java.io.Serializable
intf javax.slee.management.VendorExtensions
meth protected final void javax.slee.management.ComponentDescriptor.toString(java.lang.StringBuffer)
meth protected java.lang.Object java.lang.Object.clone() throws java.lang.CloneNotSupportedException
meth protected void java.lang.Object.finalize() throws java.lang.Throwable
meth public final [Ljavax.slee.management.LibraryID; javax.slee.management.ComponentDescriptor.getLibraries()
meth public final [Ljavax.slee.profile.ProfileSpecificationID; javax.slee.resource.ResourceAdaptorDescriptor.getProfileSpecifications()
meth public final [Ljavax.slee.resource.ResourceAdaptorTypeID; javax.slee.resource.ResourceAdaptorDescriptor.getResourceAdaptorTypes()
meth public final boolean javax.slee.management.ComponentDescriptor.equals(java.lang.Object)
meth public final boolean javax.slee.resource.ResourceAdaptorDescriptor.supportsActiveReconfiguration()
meth public final int javax.slee.management.ComponentDescriptor.hashCode()
meth public final java.lang.Class java.lang.Object.getClass()
meth public final java.lang.String javax.slee.management.ComponentDescriptor.getName()
meth public final java.lang.String javax.slee.management.ComponentDescriptor.getSource()
meth public final java.lang.String javax.slee.management.ComponentDescriptor.getVendor()
meth public final java.lang.String javax.slee.management.ComponentDescriptor.getVersion()
meth public final javax.slee.ComponentID javax.slee.management.ComponentDescriptor.getID()
meth public final javax.slee.management.DeployableUnitID javax.slee.management.ComponentDescriptor.getDeployableUnit()
meth public final javax.slee.resource.ResourceAdaptorTypeID javax.slee.resource.ResourceAdaptorDescriptor.getResourceAdaptorType()
meth public final void java.lang.Object.notify()
meth public final void java.lang.Object.notifyAll()
meth public final void java.lang.Object.wait() throws java.lang.InterruptedException
meth public final void java.lang.Object.wait(long) throws java.lang.InterruptedException
meth public final void java.lang.Object.wait(long,int) throws java.lang.InterruptedException
meth public java.lang.Object javax.slee.management.ComponentDescriptor.getVendorData()
meth public java.lang.String javax.slee.resource.ResourceAdaptorDescriptor.toString()
meth public static void javax.slee.management.ComponentDescriptor.disableVendorDataDeserialization()
meth public static void javax.slee.management.ComponentDescriptor.disableVendorDataSerialization()
meth public static void javax.slee.management.ComponentDescriptor.enableVendorDataDeserialization()
meth public static void javax.slee.management.ComponentDescriptor.enableVendorDataSerialization()
meth public void javax.slee.management.ComponentDescriptor.setVendorData(java.lang.Object)
supr java.lang.Object javax.slee.management.ComponentDescriptor
CLSS public final javax.slee.resource.ResourceAdaptorID
cons public javax.slee.resource.ResourceAdaptorID.ResourceAdaptorID(java.lang.String,java.lang.String,java.lang.String)
intf java.io.Serializable
intf java.lang.Cloneable
intf java.lang.Comparable
meth protected final int javax.slee.ComponentID.compareTo(java.lang.String,javax.slee.ComponentID)
meth protected java.lang.String javax.slee.resource.ResourceAdaptorID.getClassName()
meth protected void java.lang.Object.finalize() throws java.lang.Throwable
meth public final boolean javax.slee.ComponentID.equals(java.lang.Object)
meth public final int javax.slee.ComponentID.hashCode()
meth public final int javax.slee.resource.ResourceAdaptorID.compareTo(java.lang.Object)
meth public final java.lang.Class java.lang.Object.getClass()
meth public final java.lang.String javax.slee.ComponentID.getName()
meth public final java.lang.String javax.slee.ComponentID.getVendor()
meth public final java.lang.String javax.slee.ComponentID.getVersion()
meth public final java.lang.String javax.slee.ComponentID.toString()
meth public final void java.lang.Object.notify()
meth public final void java.lang.Object.notifyAll()
meth public final void java.lang.Object.wait() throws java.lang.InterruptedException
meth public final void java.lang.Object.wait(long) throws java.lang.InterruptedException
meth public final void java.lang.Object.wait(long,int) throws java.lang.InterruptedException
meth public java.lang.Object javax.slee.resource.ResourceAdaptorID.clone()
supr java.lang.Object javax.slee.ComponentID
CLSS public javax.slee.resource.ResourceAdaptorTypeDescriptor
cons public javax.slee.resource.ResourceAdaptorTypeDescriptor.ResourceAdaptorTypeDescriptor(javax.slee.resource.ResourceAdaptorTypeID,javax.slee.management.DeployableUnitID,java.lang.String,[Ljavax.slee.management.LibraryID;,[Ljava.lang.String;,java.lang.String,[Ljavax.slee.EventTypeID;)
intf java.io.Serializable
intf javax.slee.management.VendorExtensions
meth protected final void javax.slee.management.ComponentDescriptor.toString(java.lang.StringBuffer)
meth protected java.lang.Object java.lang.Object.clone() throws java.lang.CloneNotSupportedException
meth protected void java.lang.Object.finalize() throws java.lang.Throwable
meth public final [Ljava.lang.String; javax.slee.resource.ResourceAdaptorTypeDescriptor.getActivityTypes()
meth public final [Ljavax.slee.EventTypeID; javax.slee.resource.ResourceAdaptorTypeDescriptor.getEventTypes()
meth public final [Ljavax.slee.management.LibraryID; javax.slee.management.ComponentDescriptor.getLibraries()
meth public final boolean javax.slee.management.ComponentDescriptor.equals(java.lang.Object)
meth public final int javax.slee.management.ComponentDescriptor.hashCode()
meth public final java.lang.Class java.lang.Object.getClass()
meth public final java.lang.String javax.slee.management.ComponentDescriptor.getName()
meth public final java.lang.String javax.slee.management.ComponentDescriptor.getSource()
meth public final java.lang.String javax.slee.management.ComponentDescriptor.getVendor()
meth public final java.lang.String javax.slee.management.ComponentDescriptor.getVersion()
meth public final java.lang.String javax.slee.resource.ResourceAdaptorTypeDescriptor.getResourceAdaptorInterface()
meth public final javax.slee.ComponentID javax.slee.management.ComponentDescriptor.getID()
meth public final javax.slee.management.DeployableUnitID javax.slee.management.ComponentDescriptor.getDeployableUnit()
meth public final void java.lang.Object.notify()
meth public final void java.lang.Object.notifyAll()
meth public final void java.lang.Object.wait() throws java.lang.InterruptedException
meth public final void java.lang.Object.wait(long) throws java.lang.InterruptedException
meth public final void java.lang.Object.wait(long,int) throws java.lang.InterruptedException
meth public java.lang.Object javax.slee.management.ComponentDescriptor.getVendorData()
meth public java.lang.String javax.slee.resource.ResourceAdaptorTypeDescriptor.toString()
meth public static void javax.slee.management.ComponentDescriptor.disableVendorDataDeserialization()
meth public static void javax.slee.management.ComponentDescriptor.disableVendorDataSerialization()
meth public static void javax.slee.management.ComponentDescriptor.enableVendorDataDeserialization()
meth public static void javax.slee.management.ComponentDescriptor.enableVendorDataSerialization()
meth public void javax.slee.management.ComponentDescriptor.setVendorData(java.lang.Object)
supr java.lang.Object javax.slee.management.ComponentDescriptor
CLSS public final javax.slee.resource.ResourceAdaptorTypeID
cons public javax.slee.resource.ResourceAdaptorTypeID.ResourceAdaptorTypeID(java.lang.String,java.lang.String,java.lang.String)
intf java.io.Serializable
intf java.lang.Cloneable
intf java.lang.Comparable
meth protected final int javax.slee.ComponentID.compareTo(java.lang.String,javax.slee.ComponentID)
meth protected java.lang.String javax.slee.resource.ResourceAdaptorTypeID.getClassName()
meth protected void java.lang.Object.finalize() throws java.lang.Throwable
meth public final boolean javax.slee.ComponentID.equals(java.lang.Object)
meth public final int javax.slee.ComponentID.hashCode()
meth public final int javax.slee.resource.ResourceAdaptorTypeID.compareTo(java.lang.Object)
meth public final java.lang.Class java.lang.Object.getClass()
meth public final java.lang.String javax.slee.ComponentID.getName()
meth public final java.lang.String javax.slee.ComponentID.getVendor()
meth public final java.lang.String javax.slee.ComponentID.getVersion()
meth public final java.lang.String javax.slee.ComponentID.toString()
meth public final void java.lang.Object.notify()
meth public final void java.lang.Object.notifyAll()
meth public final void java.lang.Object.wait() throws java.lang.InterruptedException
meth public final void java.lang.Object.wait(long) throws java.lang.InterruptedException
meth public final void java.lang.Object.wait(long,int) throws java.lang.InterruptedException
meth public java.lang.Object javax.slee.resource.ResourceAdaptorTypeID.clone()
supr java.lang.Object javax.slee.ComponentID
CLSS public abstract interface javax.slee.resource.SleeEndpoint
meth public abstract void javax.slee.resource.SleeEndpoint.endActivity(javax.slee.resource.ActivityHandle)
meth public abstract void javax.slee.resource.SleeEndpoint.endActivityTransacted(javax.slee.resource.ActivityHandle)
meth public abstract void javax.slee.resource.SleeEndpoint.fireEvent(javax.slee.resource.ActivityHandle,javax.slee.resource.FireableEventType,java.lang.Object,javax.slee.Address,javax.slee.resource.ReceivableService) throws javax.slee.resource.FireEventException
meth public abstract void javax.slee.resource.SleeEndpoint.fireEvent(javax.slee.resource.ActivityHandle,javax.slee.resource.FireableEventType,java.lang.Object,javax.slee.Address,javax.slee.resource.ReceivableService,int) throws javax.slee.resource.FireEventException
meth public abstract void javax.slee.resource.SleeEndpoint.fireEventTransacted(javax.slee.resource.ActivityHandle,javax.slee.resource.FireableEventType,java.lang.Object,javax.slee.Address,javax.slee.resource.ReceivableService) throws javax.slee.resource.FireEventException
meth public abstract void javax.slee.resource.SleeEndpoint.fireEventTransacted(javax.slee.resource.ActivityHandle,javax.slee.resource.FireableEventType,java.lang.Object,javax.slee.Address,javax.slee.resource.ReceivableService,int) throws javax.slee.resource.FireEventException
meth public abstract void javax.slee.resource.SleeEndpoint.startActivity(javax.slee.resource.ActivityHandle,java.lang.Object) throws javax.slee.resource.StartActivityException
meth public abstract void javax.slee.resource.SleeEndpoint.startActivity(javax.slee.resource.ActivityHandle,java.lang.Object,int) throws javax.slee.resource.StartActivityException
meth public abstract void javax.slee.resource.SleeEndpoint.startActivitySuspended(javax.slee.resource.ActivityHandle,java.lang.Object) throws javax.slee.resource.StartActivityException
meth public abstract void javax.slee.resource.SleeEndpoint.startActivitySuspended(javax.slee.resource.ActivityHandle,java.lang.Object,int) throws javax.slee.resource.StartActivityException
meth public abstract void javax.slee.resource.SleeEndpoint.startActivityTransacted(javax.slee.resource.ActivityHandle,java.lang.Object) throws javax.slee.resource.StartActivityException
meth public abstract void javax.slee.resource.SleeEndpoint.startActivityTransacted(javax.slee.resource.ActivityHandle,java.lang.Object,int) throws javax.slee.resource.StartActivityException
meth public abstract void javax.slee.resource.SleeEndpoint.suspendActivity(javax.slee.resource.ActivityHandle)
supr null
CLSS public javax.slee.resource.StartActivityException
cons public javax.slee.resource.StartActivityException.StartActivityException()
cons public javax.slee.resource.StartActivityException.StartActivityException(java.lang.String)
intf java.io.Serializable
meth protected java.lang.Object java.lang.Object.clone() throws java.lang.CloneNotSupportedException
meth protected void java.lang.Object.finalize() throws java.lang.Throwable
meth public [Ljava.lang.StackTraceElement; java.lang.Throwable.getStackTrace()
meth public boolean java.lang.Object.equals(java.lang.Object)
meth public final java.lang.Class java.lang.Object.getClass()
meth public final void java.lang.Object.notify()
meth public final void java.lang.Object.notifyAll()
meth public final void java.lang.Object.wait() throws java.lang.InterruptedException
meth public final void java.lang.Object.wait(long) throws java.lang.InterruptedException
meth public final void java.lang.Object.wait(long,int) throws java.lang.InterruptedException
meth public int java.lang.Object.hashCode()
meth public java.lang.String java.lang.Throwable.getLocalizedMessage()
meth public java.lang.String java.lang.Throwable.getMessage()
meth public java.lang.String java.lang.Throwable.toString()
meth public java.lang.Throwable java.lang.Throwable.fillInStackTrace()
meth public java.lang.Throwable java.lang.Throwable.getCause()
meth public java.lang.Throwable java.lang.Throwable.initCause(java.lang.Throwable)
meth public void java.lang.Throwable.printStackTrace()
meth public void java.lang.Throwable.printStackTrace(java.io.PrintStream)
meth public void java.lang.Throwable.printStackTrace(java.io.PrintWriter)
meth public void java.lang.Throwable.setStackTrace([Ljava.lang.StackTraceElement;)
supr java.lang.Object java.lang.Throwable java.lang.Exception
CLSS public javax.slee.resource.UnrecognizedActivityHandleException
cons public javax.slee.resource.UnrecognizedActivityHandleException.UnrecognizedActivityHandleException(java.lang.String)
cons public javax.slee.resource.UnrecognizedActivityHandleException.UnrecognizedActivityHandleException(java.lang.String,java.lang.Throwable)
intf java.io.Serializable
meth protected java.lang.Object java.lang.Object.clone() throws java.lang.CloneNotSupportedException
meth protected void java.lang.Object.finalize() throws java.lang.Throwable
meth public [Ljava.lang.StackTraceElement; java.lang.Throwable.getStackTrace()
meth public boolean java.lang.Object.equals(java.lang.Object)
meth public final java.lang.Class java.lang.Object.getClass()
meth public final void java.lang.Object.notify()
meth public final void java.lang.Object.notifyAll()
meth public final void java.lang.Object.wait() throws java.lang.InterruptedException
meth public final void java.lang.Object.wait(long) throws java.lang.InterruptedException
meth public final void java.lang.Object.wait(long,int) throws java.lang.InterruptedException
meth public int java.lang.Object.hashCode()
meth public java.lang.String java.lang.Throwable.getLocalizedMessage()
meth public java.lang.String java.lang.Throwable.getMessage()
meth public java.lang.String java.lang.Throwable.toString()
meth public java.lang.Throwable java.lang.Throwable.fillInStackTrace()
meth public java.lang.Throwable java.lang.Throwable.initCause(java.lang.Throwable)
meth public java.lang.Throwable javax.slee.SLEEException.getCause()
meth public void java.lang.Throwable.printStackTrace()
meth public void java.lang.Throwable.printStackTrace(java.io.PrintStream)
meth public void java.lang.Throwable.printStackTrace(java.io.PrintWriter)
meth public void java.lang.Throwable.setStackTrace([Ljava.lang.StackTraceElement;)
supr java.lang.Object java.lang.Throwable java.lang.Exception java.lang.RuntimeException javax.slee.SLEEException
CLSS public abstract interface javax.slee.serviceactivity.ServiceActivity
meth public abstract javax.slee.ServiceID javax.slee.serviceactivity.ServiceActivity.getService()
supr null
CLSS public abstract interface javax.slee.serviceactivity.ServiceActivityContextInterfaceFactory
fld  public static final java.lang.String javax.slee.serviceactivity.ServiceActivityContextInterfaceFactory.JNDI_NAME
meth public abstract javax.slee.ActivityContextInterface javax.slee.serviceactivity.ServiceActivityContextInterfaceFactory.getActivityContextInterface(javax.slee.serviceactivity.ServiceActivity)
supr null
CLSS public abstract interface javax.slee.serviceactivity.ServiceActivityFactory
fld  public static final java.lang.String javax.slee.serviceactivity.ServiceActivityFactory.JNDI_NAME
meth public abstract javax.slee.serviceactivity.ServiceActivity javax.slee.serviceactivity.ServiceActivityFactory.getActivity()
supr null
CLSS public abstract interface javax.slee.serviceactivity.ServiceStartedEvent
meth public abstract javax.slee.ServiceID javax.slee.serviceactivity.ServiceStartedEvent.getService()
supr null
CLSS public abstract interface javax.slee.transaction.CommitListener
meth public abstract void javax.slee.transaction.CommitListener.committed()
meth public abstract void javax.slee.transaction.CommitListener.heuristicMixed(javax.transaction.HeuristicMixedException)
meth public abstract void javax.slee.transaction.CommitListener.heuristicRollback(javax.transaction.HeuristicRollbackException)
meth public abstract void javax.slee.transaction.CommitListener.rolledBack(javax.transaction.RollbackException)
meth public abstract void javax.slee.transaction.CommitListener.systemException(javax.transaction.SystemException)
supr null
CLSS public abstract interface javax.slee.transaction.RollbackListener
meth public abstract void javax.slee.transaction.RollbackListener.rolledBack()
meth public abstract void javax.slee.transaction.RollbackListener.systemException(javax.transaction.SystemException)
supr null
CLSS public abstract interface javax.slee.transaction.SleeTransaction
intf javax.transaction.Transaction
meth public abstract boolean javax.slee.transaction.SleeTransaction.delistResource(javax.transaction.xa.XAResource,int) throws javax.transaction.SystemException
meth public abstract boolean javax.slee.transaction.SleeTransaction.enlistResource(javax.transaction.xa.XAResource) throws javax.transaction.RollbackException
meth public abstract boolean javax.slee.transaction.SleeTransaction.equals(java.lang.Object)
meth public abstract int javax.slee.transaction.SleeTransaction.hashCode()
meth public abstract int javax.transaction.Transaction.getStatus() throws javax.transaction.SystemException
meth public abstract java.lang.String javax.slee.transaction.SleeTransaction.toString()
meth public abstract void javax.slee.transaction.SleeTransaction.asyncCommit(javax.slee.transaction.CommitListener)
meth public abstract void javax.slee.transaction.SleeTransaction.asyncRollback(javax.slee.transaction.RollbackListener)
meth public abstract void javax.transaction.Transaction.commit() throws javax.transaction.HeuristicMixedException,javax.transaction.HeuristicRollbackException,javax.transaction.RollbackException,javax.transaction.SystemException
meth public abstract void javax.transaction.Transaction.registerSynchronization(javax.transaction.Synchronization) throws javax.transaction.RollbackException,javax.transaction.SystemException
meth public abstract void javax.transaction.Transaction.rollback() throws javax.transaction.SystemException
meth public abstract void javax.transaction.Transaction.setRollbackOnly() throws javax.transaction.SystemException
supr null
CLSS public abstract interface javax.slee.transaction.SleeTransactionManager
intf javax.transaction.TransactionManager
meth public abstract int javax.transaction.TransactionManager.getStatus() throws javax.transaction.SystemException
meth public abstract javax.slee.transaction.SleeTransaction javax.slee.transaction.SleeTransactionManager.asSleeTransaction(javax.transaction.Transaction) throws javax.transaction.SystemException
meth public abstract javax.slee.transaction.SleeTransaction javax.slee.transaction.SleeTransactionManager.beginSleeTransaction() throws javax.transaction.NotSupportedException,javax.transaction.SystemException
meth public abstract javax.slee.transaction.SleeTransaction javax.slee.transaction.SleeTransactionManager.getSleeTransaction() throws javax.transaction.SystemException
meth public abstract javax.transaction.Transaction javax.transaction.TransactionManager.getTransaction() throws javax.transaction.SystemException
meth public abstract javax.transaction.Transaction javax.transaction.TransactionManager.suspend() throws javax.transaction.SystemException
meth public abstract void javax.slee.transaction.SleeTransactionManager.asyncCommit(javax.slee.transaction.CommitListener)
meth public abstract void javax.slee.transaction.SleeTransactionManager.asyncRollback(javax.slee.transaction.RollbackListener)
meth public abstract void javax.transaction.TransactionManager.begin() throws javax.transaction.NotSupportedException,javax.transaction.SystemException
meth public abstract void javax.transaction.TransactionManager.commit() throws javax.transaction.HeuristicMixedException,javax.transaction.HeuristicRollbackException,javax.transaction.RollbackException,javax.transaction.SystemException
meth public abstract void javax.transaction.TransactionManager.resume(javax.transaction.Transaction) throws javax.transaction.InvalidTransactionException,javax.transaction.SystemException
meth public abstract void javax.transaction.TransactionManager.rollback() throws javax.transaction.SystemException
meth public abstract void javax.transaction.TransactionManager.setRollbackOnly() throws javax.transaction.SystemException
meth public abstract void javax.transaction.TransactionManager.setTransactionTimeout(int) throws javax.transaction.SystemException
supr null
CLSS public javax.slee.usage.NoUsageParametersInterfaceDefinedException
cons public javax.slee.usage.NoUsageParametersInterfaceDefinedException.NoUsageParametersInterfaceDefinedException(java.lang.String)
intf java.io.Serializable
meth protected java.lang.Object java.lang.Object.clone() throws java.lang.CloneNotSupportedException
meth protected void java.lang.Object.finalize() throws java.lang.Throwable
meth public [Ljava.lang.StackTraceElement; java.lang.Throwable.getStackTrace()
meth public boolean java.lang.Object.equals(java.lang.Object)
meth public final java.lang.Class java.lang.Object.getClass()
meth public final void java.lang.Object.notify()
meth public final void java.lang.Object.notifyAll()
meth public final void java.lang.Object.wait() throws java.lang.InterruptedException
meth public final void java.lang.Object.wait(long) throws java.lang.InterruptedException
meth public final void java.lang.Object.wait(long,int) throws java.lang.InterruptedException
meth public int java.lang.Object.hashCode()
meth public java.lang.String java.lang.Throwable.getLocalizedMessage()
meth public java.lang.String java.lang.Throwable.getMessage()
meth public java.lang.String java.lang.Throwable.toString()
meth public java.lang.Throwable java.lang.Throwable.fillInStackTrace()
meth public java.lang.Throwable java.lang.Throwable.initCause(java.lang.Throwable)
meth public java.lang.Throwable javax.slee.SLEEException.getCause()
meth public void java.lang.Throwable.printStackTrace()
meth public void java.lang.Throwable.printStackTrace(java.io.PrintStream)
meth public void java.lang.Throwable.printStackTrace(java.io.PrintWriter)
meth public void java.lang.Throwable.setStackTrace([Ljava.lang.StackTraceElement;)
supr java.lang.Object java.lang.Throwable java.lang.Exception java.lang.RuntimeException javax.slee.SLEEException
CLSS public final javax.slee.usage.SampleStatistics
cons public javax.slee.usage.SampleStatistics.SampleStatistics()
cons public javax.slee.usage.SampleStatistics.SampleStatistics(long,long,long,double)
intf java.io.Serializable
intf javax.slee.management.VendorExtensions
meth protected java.lang.Object java.lang.Object.clone() throws java.lang.CloneNotSupportedException
meth protected void java.lang.Object.finalize() throws java.lang.Throwable
meth public boolean java.lang.Object.equals(java.lang.Object)
meth public double javax.slee.usage.SampleStatistics.getMean()
meth public final java.lang.Class java.lang.Object.getClass()
meth public final void java.lang.Object.notify()
meth public final void java.lang.Object.notifyAll()
meth public final void java.lang.Object.wait() throws java.lang.InterruptedException
meth public final void java.lang.Object.wait(long) throws java.lang.InterruptedException
meth public final void java.lang.Object.wait(long,int) throws java.lang.InterruptedException
meth public int java.lang.Object.hashCode()
meth public java.lang.Object javax.slee.usage.SampleStatistics.getVendorData()
meth public java.lang.String javax.slee.usage.SampleStatistics.toString()
meth public long javax.slee.usage.SampleStatistics.getMaximum()
meth public long javax.slee.usage.SampleStatistics.getMinimum()
meth public long javax.slee.usage.SampleStatistics.getSampleCount()
meth public static void javax.slee.usage.SampleStatistics.disableVendorDataDeserialization()
meth public static void javax.slee.usage.SampleStatistics.disableVendorDataSerialization()
meth public static void javax.slee.usage.SampleStatistics.enableVendorDataDeserialization()
meth public static void javax.slee.usage.SampleStatistics.enableVendorDataSerialization()
meth public void javax.slee.usage.SampleStatistics.setVendorData(java.lang.Object)
supr java.lang.Object
CLSS public abstract interface javax.slee.usage.SbbUsageMBean
fld  public static final java.lang.String javax.slee.usage.SbbUsageMBean.USAGE_NOTIFICATION_TYPE
meth public abstract java.lang.String javax.slee.usage.SbbUsageMBean.getUsageParameterSet() throws javax.slee.management.ManagementException
meth public abstract javax.slee.SbbID javax.slee.usage.SbbUsageMBean.getSbb() throws javax.slee.management.ManagementException
meth public abstract javax.slee.ServiceID javax.slee.usage.SbbUsageMBean.getService() throws javax.slee.management.ManagementException
meth public abstract void javax.slee.usage.SbbUsageMBean.close() throws javax.slee.InvalidStateException,javax.slee.management.ManagementException
meth public abstract void javax.slee.usage.SbbUsageMBean.resetAllUsageParameters() throws javax.slee.management.ManagementException
supr null
CLSS public javax.slee.usage.UnrecognizedUsageParameterSetNameException
cons public javax.slee.usage.UnrecognizedUsageParameterSetNameException.UnrecognizedUsageParameterSetNameException()
cons public javax.slee.usage.UnrecognizedUsageParameterSetNameException.UnrecognizedUsageParameterSetNameException(java.lang.String)
intf java.io.Serializable
meth protected java.lang.Object java.lang.Object.clone() throws java.lang.CloneNotSupportedException
meth protected void java.lang.Object.finalize() throws java.lang.Throwable
meth public [Ljava.lang.StackTraceElement; java.lang.Throwable.getStackTrace()
meth public boolean java.lang.Object.equals(java.lang.Object)
meth public final java.lang.Class java.lang.Object.getClass()
meth public final void java.lang.Object.notify()
meth public final void java.lang.Object.notifyAll()
meth public final void java.lang.Object.wait() throws java.lang.InterruptedException
meth public final void java.lang.Object.wait(long) throws java.lang.InterruptedException
meth public final void java.lang.Object.wait(long,int) throws java.lang.InterruptedException
meth public int java.lang.Object.hashCode()
meth public java.lang.String java.lang.Throwable.getLocalizedMessage()
meth public java.lang.String java.lang.Throwable.getMessage()
meth public java.lang.String java.lang.Throwable.toString()
meth public java.lang.Throwable java.lang.Throwable.fillInStackTrace()
meth public java.lang.Throwable java.lang.Throwable.getCause()
meth public java.lang.Throwable java.lang.Throwable.initCause(java.lang.Throwable)
meth public void java.lang.Throwable.printStackTrace()
meth public void java.lang.Throwable.printStackTrace(java.io.PrintStream)
meth public void java.lang.Throwable.printStackTrace(java.io.PrintWriter)
meth public void java.lang.Throwable.setStackTrace([Ljava.lang.StackTraceElement;)
supr java.lang.Object java.lang.Throwable java.lang.Exception
CLSS public abstract interface javax.slee.usage.UsageMBean
fld  public static final java.lang.String javax.slee.usage.UsageMBean.BASE_OBJECT_NAME
fld  public static final java.lang.String javax.slee.usage.UsageMBean.NOTIFICATION_SOURCE_KEY
fld  public static final java.lang.String javax.slee.usage.UsageMBean.USAGE_PARAMETER_SET_NAME_KEY
meth public abstract java.lang.String javax.slee.usage.UsageMBean.getUsageParameterSet() throws javax.slee.management.ManagementException
meth public abstract javax.management.ObjectName javax.slee.usage.UsageMBean.getUsageNotificationManagerMBean() throws javax.slee.management.ManagementException
meth public abstract javax.slee.management.NotificationSource javax.slee.usage.UsageMBean.getNotificationSource() throws javax.slee.management.ManagementException
meth public abstract void javax.slee.usage.UsageMBean.close() throws javax.slee.InvalidStateException,javax.slee.management.ManagementException
meth public abstract void javax.slee.usage.UsageMBean.resetAllUsageParameters() throws javax.slee.management.ManagementException
supr null
CLSS public final javax.slee.usage.UsageNotification
cons public javax.slee.usage.UsageNotification.UsageNotification(java.lang.String,javax.slee.usage.UsageMBean,javax.slee.management.NotificationSource,java.lang.String,java.lang.String,boolean,long,long,long)
cons public javax.slee.usage.UsageNotification.UsageNotification(javax.slee.usage.SbbUsageMBean,javax.slee.ServiceID,javax.slee.SbbID,java.lang.String,java.lang.String,boolean,long,long,long)
fld  protected java.lang.Object javax.management.Notification.source
intf java.io.Serializable
intf javax.slee.management.VendorExtensions
meth protected java.lang.Object java.lang.Object.clone() throws java.lang.CloneNotSupportedException
meth protected void java.lang.Object.finalize() throws java.lang.Throwable
meth public boolean javax.slee.usage.UsageNotification.equals(java.lang.Object)
meth public boolean javax.slee.usage.UsageNotification.isCounter()
meth public final java.lang.Class java.lang.Object.getClass()
meth public final void java.lang.Object.notify()
meth public final void java.lang.Object.notifyAll()
meth public final void java.lang.Object.wait() throws java.lang.InterruptedException
meth public final void java.lang.Object.wait(long) throws java.lang.InterruptedException
meth public final void java.lang.Object.wait(long,int) throws java.lang.InterruptedException
meth public int javax.slee.usage.UsageNotification.hashCode()
meth public java.lang.Object java.util.EventObject.getSource()
meth public java.lang.Object javax.management.Notification.getUserData()
meth public java.lang.Object javax.slee.usage.UsageNotification.getVendorData()
meth public java.lang.String javax.management.Notification.getMessage()
meth public java.lang.String javax.management.Notification.getType()
meth public java.lang.String javax.slee.usage.UsageNotification.getUsageParameterName()
meth public java.lang.String javax.slee.usage.UsageNotification.getUsageParameterSetName()
meth public java.lang.String javax.slee.usage.UsageNotification.toString()
meth public javax.slee.SbbID javax.slee.usage.UsageNotification.getSbb()
meth public javax.slee.ServiceID javax.slee.usage.UsageNotification.getService()
meth public javax.slee.management.NotificationSource javax.slee.usage.UsageNotification.getNotificationSource()
meth public long javax.management.Notification.getSequenceNumber()
meth public long javax.management.Notification.getTimeStamp()
meth public long javax.slee.usage.UsageNotification.getValue()
meth public static void javax.slee.usage.UsageNotification.disableVendorDataDeserialization()
meth public static void javax.slee.usage.UsageNotification.disableVendorDataSerialization()
meth public static void javax.slee.usage.UsageNotification.enableVendorDataDeserialization()
meth public static void javax.slee.usage.UsageNotification.enableVendorDataSerialization()
meth public void javax.management.Notification.setSequenceNumber(long)
meth public void javax.management.Notification.setSource(java.lang.Object)
meth public void javax.management.Notification.setTimeStamp(long)
meth public void javax.management.Notification.setUserData(java.lang.Object)
meth public void javax.slee.usage.UsageNotification.setVendorData(java.lang.Object)
supr java.lang.Object java.util.EventObject javax.management.Notification
CLSS public abstract interface javax.slee.usage.UsageNotificationManagerMBean
fld  public static final java.lang.String javax.slee.usage.UsageNotificationManagerMBean.BASE_OBJECT_NAME
fld  public static final java.lang.String javax.slee.usage.UsageNotificationManagerMBean.NOTIFICATION_SOURCE_KEY
meth public abstract javax.slee.management.NotificationSource javax.slee.usage.UsageNotificationManagerMBean.getNotificationSource() throws javax.slee.management.ManagementException
meth public abstract void javax.slee.usage.UsageNotificationManagerMBean.close() throws javax.slee.management.ManagementException
supr null
CLSS public javax.slee.usage.UsageOutOfRangeFilter
cons public javax.slee.usage.UsageOutOfRangeFilter.UsageOutOfRangeFilter(javax.slee.ServiceID,javax.slee.SbbID,java.lang.String,long,long) throws javax.slee.InvalidArgumentException
cons public javax.slee.usage.UsageOutOfRangeFilter.UsageOutOfRangeFilter(javax.slee.management.NotificationSource,java.lang.String,long,long) throws javax.slee.InvalidArgumentException
intf java.io.Serializable
intf javax.management.NotificationFilter
meth protected java.lang.Object java.lang.Object.clone() throws java.lang.CloneNotSupportedException
meth protected void java.lang.Object.finalize() throws java.lang.Throwable
meth public boolean java.lang.Object.equals(java.lang.Object)
meth public boolean javax.slee.usage.UsageOutOfRangeFilter.isNotificationEnabled(javax.management.Notification)
meth public final java.lang.Class java.lang.Object.getClass()
meth public final void java.lang.Object.notify()
meth public final void java.lang.Object.notifyAll()
meth public final void java.lang.Object.wait() throws java.lang.InterruptedException
meth public final void java.lang.Object.wait(long) throws java.lang.InterruptedException
meth public final void java.lang.Object.wait(long,int) throws java.lang.InterruptedException
meth public int java.lang.Object.hashCode()
meth public java.lang.String java.lang.Object.toString()
supr java.lang.Object
CLSS public javax.slee.usage.UsageThresholdFilter
cons public javax.slee.usage.UsageThresholdFilter.UsageThresholdFilter(javax.slee.ServiceID,javax.slee.SbbID,java.lang.String,long)
cons public javax.slee.usage.UsageThresholdFilter.UsageThresholdFilter(javax.slee.management.NotificationSource,java.lang.String,long)
intf java.io.Serializable
intf javax.management.NotificationFilter
meth protected java.lang.Object java.lang.Object.clone() throws java.lang.CloneNotSupportedException
meth protected void java.lang.Object.finalize() throws java.lang.Throwable
meth public boolean java.lang.Object.equals(java.lang.Object)
meth public boolean javax.slee.usage.UsageThresholdFilter.isNotificationEnabled(javax.management.Notification)
meth public final java.lang.Class java.lang.Object.getClass()
meth public final void java.lang.Object.notify()
meth public final void java.lang.Object.notifyAll()
meth public final void java.lang.Object.wait() throws java.lang.InterruptedException
meth public final void java.lang.Object.wait(long) throws java.lang.InterruptedException
meth public final void java.lang.Object.wait(long,int) throws java.lang.InterruptedException
meth public int java.lang.Object.hashCode()
meth public java.lang.String java.lang.Object.toString()
supr java.lang.Object
CLSS public javax.slee.usage.UsageUpdatedFilter
cons public javax.slee.usage.UsageUpdatedFilter.UsageUpdatedFilter(javax.slee.ServiceID,javax.slee.SbbID,java.lang.String)
cons public javax.slee.usage.UsageUpdatedFilter.UsageUpdatedFilter(javax.slee.management.NotificationSource,java.lang.String)
intf java.io.Serializable
intf javax.management.NotificationFilter
meth protected java.lang.Object java.lang.Object.clone() throws java.lang.CloneNotSupportedException
meth protected void java.lang.Object.finalize() throws java.lang.Throwable
meth public boolean java.lang.Object.equals(java.lang.Object)
meth public boolean javax.slee.usage.UsageUpdatedFilter.isNotificationEnabled(javax.management.Notification)
meth public final java.lang.Class java.lang.Object.getClass()
meth public final void java.lang.Object.notify()
meth public final void java.lang.Object.notifyAll()
meth public final void java.lang.Object.wait() throws java.lang.InterruptedException
meth public final void java.lang.Object.wait(long) throws java.lang.InterruptedException
meth public final void java.lang.Object.wait(long,int) throws java.lang.InterruptedException
meth public int java.lang.Object.hashCode()
meth public java.lang.String java.lang.Object.toString()
supr java.lang.Object
