/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.ant;

import java.io.IOException;
import java.util.Iterator;
import java.util.List;
import com.opencloud.util.XMLParser;
import org.apache.tools.ant.BuildException;
import org.apache.tools.ant.types.Path;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

/**
 The LibraryJar task automates the construction of a Rhino library jar file
 that may be included in a deployable unit jar.
 <pre>
 &lt;library destfile="pathname.jar" libraryjarxml="foo/bar/oc-library-jar.xml" autoinclude="yes" classpath="..."&gt;
 &lt;classpath ...&gt;
 &lt;fileset ... &gt;
 &lt;/library&gt;
 </pre>
 libraryjarxml is assumed to have a default value of oc-library-jar.xml, so this property may be ommited if
 the metatinfbase property is used or inherited from an enclosing deployableunit task.
 */
public class LibraryJar extends SleeJar implements Component {
    public LibraryJar() {
        super("library-jar", "create");
        // set a default classpath so it doesn't need to be included in the task xml
        // it's not necessary as the library xml doesn't define any classes to autoinclude
        setClasspath(new Path(getProject(), ""));
    }

    protected final void includeTypeSpecificClasses() throws BuildException {
        // Parse the library-jar.xml and look for library jars to include
        try {
            Document libraryjarDoc = XMLParser.getDocument(getJarXml().toURL(), entityResolver);
            setSleeVersion(VERSION_1_1);
            Element libraryRoot = libraryjarDoc.getDocumentElement(); // <library-jar>
            List libraryNodes = XMLParser.getElementsByTagName(libraryRoot, "library"); // <library>
            for (Iterator i = libraryNodes.iterator(); i.hasNext(); ) {
                Element libraryNode = (Element)i.next();
                List jarNodes = XMLParser.getElementsByTagName(libraryNode, "jar");
                for (Iterator j = jarNodes.iterator(); j.hasNext(); ) {
                    Element jarNode = (Element)j.next();
                    includeJar(XMLParser.getTextElement(jarNode, "jar-name"));
                }
            }
        }
        catch (IOException e) {
            throw new BuildException(e);
        }
    }

    //   libraryjarxml="..."
    public void setLibraryjarxml(String libraryjarxml) {
        setJarXml(libraryjarxml);
    }

    protected final String getComponentType() { return "libraryjar"; }
    protected final String getJarXmlName() { return "libraryjarxml"; }
}
