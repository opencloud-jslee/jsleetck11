/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.ant;

import com.opencloud.util.XMLParser;
import org.apache.tools.ant.BuildException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import java.io.IOException;
import java.util.Iterator;
import java.util.List;

/**
 * The ProfileSpecJar task automates the construction of a profile specification
 * jar file that may be included in a deployable unit jar.
 * <pre>
 * &lt;profilespec destfile="pathname.jar" profilespecjarxml="foo/bar/profile-spec-jar.xml" autoinclude="yes" classpath="..."&gt;
 * &lt;classpath ...&gt;
 * &lt;fileset ... &gt;
 * &lt;/profilespec&gt;
 * </pre>
 * profilespecjarxml is assumed to have a default value of profile-spec-jar.xml, so this property may be ommited if
 * the metatinfbase property is used or inherited from an enclosing deployableunit task.
 */

public class ProfileSpecJar extends SleeJar {
    public ProfileSpecJar() {
        super("profile-spec-jar", "create");
    }

    protected final void includeTypeSpecificClasses() throws BuildException {
        try {
            Document profileSpecJarDoc = XMLParser.getDocument(getJarXml().toURL(), entityResolver);
            int sleeVersion = getSleeVersion(profileSpecJarDoc);
            setSleeVersion(sleeVersion);

            Element profileSpecRoot = profileSpecJarDoc.getDocumentElement(); // <profile-spec-jar>
            List profileSpecNodes = XMLParser.getElementsByTagName(profileSpecRoot, "profile-spec"); // <profile-spec>
            for (Iterator i = profileSpecNodes.iterator(); i.hasNext(); ) {
                Element profileSpecNode = (Element)i.next();
                Element profileSpecClassesNode = XMLParser.getUniqueElement(profileSpecNode, "profile-classes");
                if (sleeVersion == VERSION_1_0) {
                    for (int j = 0; j < SLEE_10_PROFILE_CLASSES.length; ++j) {
                        String className = XMLParser.getOptionalTextElement(profileSpecClassesNode, SLEE_10_PROFILE_CLASSES[j] );
                        if(className != null) includeClass(className);
                    }
                }
                else if (sleeVersion == VERSION_1_1) {
                    for (int j=0; j<SLEE_11_PROFILE_CLASSES.length; j++) {
                        Element node = XMLParser.getOptionalElement(profileSpecClassesNode, SLEE_11_PROFILE_CLASSES[j]);
                        if (node != null) includeClass(XMLParser.getTextElement(node, SLEE_11_PROFILE_CLASSES[j] + "-name"));
                    }
                }
            }
        } catch (IOException e) {
            throw new BuildException(e);
        }
    }

    protected final String getComponentType() { return "profilespecjar"; }
    protected final String getJarXmlName() { return "profilespecjarxml"; }

    //   profilespecjarxml="..."
    public void setProfilespecjarxml(String profilespecjarxml) {
        setJarXml(profilespecjarxml);
    }

    private static final SleeDTDResolver entityResolver = new SleeDTDResolver();

    // Parse the profile-spec-jar.xml and look for profile classes to include.
    private static final String[] SLEE_10_PROFILE_CLASSES = {
        "profile-cmp-interface-name", "profile-management-interface-name", "profile-management-abstract-class-name"
    };
    private static final String[] SLEE_11_PROFILE_CLASSES = {
        "profile-cmp-interface", "profile-local-interface", "profile-management-interface", "profile-abstract-class",
        "profile-table-interface", "profile-usage-parameters-interface"
    };
}
