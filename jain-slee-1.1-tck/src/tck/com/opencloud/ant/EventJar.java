/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.ant;

import com.opencloud.util.XMLParser;
import org.apache.tools.ant.BuildException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import java.io.IOException;
import java.util.Iterator;
import java.util.List;

/**
 The EventJar task automates the construction of an event jar file that may
 be included in a deployable unit jar.
 <pre>
 &lt;event destfile="pathname.jar" eventjarxml="foo/bar/event-jar.xml" autoinclude="yes" classpath="..."&gt;
 &lt;classpath ...&gt;
 &lt;fileset ... &gt;
 &lt;/event&gt;
 </pre>
 eventjarxml is assumed to have a default value of event-jar.xml, so this property may be ommited if
 the metatinfbase property is used or inherited from an enclosing deployableunit task.
 */

public class EventJar extends SleeJar {
    public EventJar() {
        super("event-jar", "create");
    }

    protected final void includeTypeSpecificClasses() throws BuildException {
        try {
            Document eventjarDoc = XMLParser.getDocument(getJarXml().toURL(), entityResolver);
            setSleeVersion(getSleeVersion(eventjarDoc));
            Element eventRoot = eventjarDoc.getDocumentElement(); // <event-jar>
            List eventNodes = XMLParser.getElementsByTagName(eventRoot, "event-definition");

            Iterator i = eventNodes.iterator();
            while (i.hasNext()) {
                Element eventNode = (Element) i.next();

                String className = XMLParser.getTextElement(eventNode, "event-class-name");
                includeClass(className);
            }
        } catch (IOException e) {
            throw new BuildException(e);
        }
    }

    //   eventjarxml="..."
    public void setEventjarxml(String eventjarxml) {
        setJarXml(eventjarxml);
    }

    protected final String getComponentType() { return "eventjar"; }
    protected final String getJarXmlName() { return "eventjarxml"; }

    private static final SleeDTDResolver entityResolver = new SleeDTDResolver();
}
