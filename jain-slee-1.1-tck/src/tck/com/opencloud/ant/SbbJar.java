/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.ant;

import com.opencloud.util.XMLParser;
import org.apache.tools.ant.BuildException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import java.io.IOException;
import java.util.Iterator;
import java.util.List;

/**
 * The SbbJar task automates the construction of an SBB jar file
 * that may be included in a deployable unit jar.
 * <pre>
 * &lt;sbb destfile="pathname.jar" sbbjarxml="foo/bar/sbb-jar.xml" extxml="foo/bar/oc-sbb.xml" autoinclude="yes" classpath="..."&gt;
 * &lt;classpath ...&gt;
 * &lt;fileset ... &gt;
 * &lt;/sbb&gt;
 * </pre>
 * sbbjarxml is assumed to have a default value of sbb-jar.xml, so this property may be ommited if
 * the metatinfbase property is used or inherited from an enclosing deployableunit task.
 */

public class SbbJar extends SleeJar implements Component{
    public SbbJar() {
        super("sbb-jar", "create");
    }

    protected final void includeTypeSpecificClasses() throws BuildException {

        // Parse the sbb-jar.xml and look for SBB classes to include.
        String[] classTypes = { "sbb-abstract-class",
                                "sbb-local-interface",
                                "sbb-activity-context-interface",
                                "sbb-usage-parameters-interface" };

        try {
            Document sbbjarDoc = XMLParser.getDocument(getJarXml().toURL(), entityResolver);
            setSleeVersion(getSleeVersion(sbbjarDoc));
            Element sbbRoot = sbbjarDoc.getDocumentElement(); // <sbb-jar>
            List sbbNodes = XMLParser.getElementsByTagName(sbbRoot, "sbb"); // <sbb>
            for (Iterator i = sbbNodes.iterator(); i.hasNext(); ) {
                Element sbbNode = (Element)i.next();
                Element sbbClassesNode = XMLParser.getUniqueElement(sbbNode, "sbb-classes");
                for (int j = 0; j < classTypes.length; ++j) {
                    Element classNode = XMLParser.getOptionalElement(sbbClassesNode, classTypes[j]);
                    if (classNode != null) {
                        String className = XMLParser.getTextElement(classNode, classTypes[j] + "-name");
                        includeClass(className);
                    }
                }
            }
        } catch (IOException e) {
            throw new BuildException(e);
        }
    }

    //   sbbjarxml="..."
    public void setSbbjarxml(String sbbjarxml) {
        setJarXml(sbbjarxml);
    }

    protected final String getComponentType() { return "sbbjar"; }
    protected final String getJarXmlName() { return "sbbjarxml"; }

    private static final SleeDTDResolver entityResolver = new SleeDTDResolver();
}
