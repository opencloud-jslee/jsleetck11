/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.ant;

/**
 * ManifestJar will add a library to a sbb or ra runtime classpath by adding a Class-Path
 * entry to the manfiest.
 */

public class ManifestJar extends org.apache.tools.ant.types.FileSet {
}
