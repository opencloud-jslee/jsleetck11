/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.ant;

import org.apache.tools.ant.Project;
import java.io.File;

public interface Component {
    public static final int VERSION_1_0 = 0;
    public static final int VERSION_1_1 = 1;

    public int getSleeVersion();
    File getComponentFile(Project project);
}
