/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.ant;

import org.apache.tools.ant.BuildException;
import org.apache.tools.ant.DirectoryScanner;
import org.apache.tools.ant.Project;
import org.apache.tools.ant.types.FileSet;
import org.apache.tools.ant.types.Path;
import org.apache.tools.ant.types.Reference;
import org.apache.tools.ant.util.FileUtils;
import org.apache.tools.ant.util.TaskLogger;

import java.io.File;
import java.lang.reflect.Method;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.ArrayList;
import java.util.List;
import java.util.zip.ZipFile;

public class VerifierTask extends org.apache.tools.ant.taskdefs.Jar {

    public VerifierTask(Project project){
        setProject(project);
        taskLogger=new TaskLogger(this);
    }
    public String getTaskName(){
        return "verifier";
    }

    public void execute(){
        ClassLoader classLoader=null;

        if (classpath !=null && classpathFileList == null) {
            // Initialize the classpath once.


            String[] classpathList = classpath.list();
            classpathURLList = new URL[classpathList.length];
            for (int i = 0; i < classpathList.length; ++i)
                try{
                    classpathURLList[i] = fileUtils.normalize(classpathList[i]).toURL();

                    taskLogger.info(classpathURLList[i].toExternalForm());
                }catch(MalformedURLException xMURL){
                    throw new BuildException("classpath entry not found.",xMURL);
                }

            classLoader=new URLClassLoader(classpathURLList,this.getClass().getClassLoader());
        }
        if(classLoader==null)
            classLoader=this.getClass().getClassLoader();



        List urlList=new ArrayList();
        for(java.util.Iterator i=libraryList.iterator();i.hasNext();){

            FileSet fs=(FileSet)i.next();
            DirectoryScanner ds = fs.getDirectoryScanner(getProject());         // 3
            String[] includedFiles = ds.getIncludedFiles();
            String base=ds.getBasedir().getPath();
            for(int j=0; j<includedFiles.length; j++) {
                try{
                    taskLogger.info("adding library path " +
                            base+File.separatorChar+ includedFiles[j]);
                    urlList.add(new URL(new URL("file:."),base+File.separatorChar
                            + includedFiles[j]));
                }catch(java.net.MalformedURLException xMURL){
                    throw new BuildException("invalid library path",xMURL);
                }
            }
        }

        URL[] urls=(URL[])urlList.toArray(new URL[urlList.size()]);

        String tmpDirStr = System.getProperty("java.io.tmpdir");
        if (tmpDirStr == null) {
            System.err.println("The system property java.io.tmpdir is not set");
            System.err.println("Please set it to the name of a temporary directory that can be used");
            System.exit(1);
        }
        File tmpDir = new File(new File(tmpDirStr), "slee-verifier");

        boolean error=false;
        Throwable t=null;
        try{

            Thread.currentThread().setContextClassLoader(classLoader);

            Class clazz=Class.forName("com.opencloud.rhino.management.deployment.verifier.Verifier",false,classLoader);
            Method m=clazz.getMethod("verify",new
                    Class[]{urls.getClass(),tmpDir.getClass(),org.apache.log4j.Level.class});
            m.invoke( clazz.newInstance(), new Object[]{urls,tmpDir,org.apache.log4j.Level.WARN});

        }catch(java.lang.ClassNotFoundException xCNF){ error=true; t=xCNF;
        }catch(java.lang.NoSuchMethodException xNSM){ error=true; t=xNSM;
        }catch(java.lang.reflect.InvocationTargetException xIT){ error=true; t=xIT;
        }catch(java.lang.IllegalArgumentException xIA){ error=true; t=xIA;
        }catch(java.lang.IllegalAccessException xIAC){ error=true; t=xIAC;
        }catch(java.lang.InstantiationException xI){ error=true; t=xI;
        }finally{
            if (error) throw new BuildException("verifier not installed correctly" , t);
        }
    }
    public void addDeployableUnit(FileSet library){
        libraryList.add(library);
    }
    // Setters:

    //   classpath="..."
    public void setClasspath(Path newClasspath) {
        if (this.classpath == null)
            this.classpath = newClasspath;
        else
            this.classpath.append(newClasspath);
    }

    //   nested <classpath>
    public Path createClasspath() {
        if (classpath == null)
            classpath = new Path(getProject());

        return classpath.createPath();
    }

    //   classpathref="..."
    public void setClasspathRef(Reference r) {
        createClasspath().setRefid(r);
    }

    private URL[] classpathURLList;
    private File[] classpathFileList;
    private ZipFile[] classpathZipList;
    private Path classpath;
    private java.util.List libraryList=new java.util.ArrayList();
    protected static final FileUtils fileUtils = FileUtils.newFileUtils();
    private TaskLogger taskLogger;
}
