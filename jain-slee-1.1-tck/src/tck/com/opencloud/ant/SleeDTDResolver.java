/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.ant;

import com.opencloud.util.DTDResolver;

public class SleeDTDResolver extends DTDResolver {
    public SleeDTDResolver() {
        super();
        // SLEE 1.0
        registerDTDResource("-//Sun Microsystems, Inc.//DTD JAIN SLEE Deployable Unit 1.0//EN", "slee/dtd/slee-deployable-unit_1_0.dtd");
        registerDTDResource("-//Sun Microsystems, Inc.//DTD JAIN SLEE SBB 1.0//EN", "slee/dtd/slee-sbb-jar_1_0.dtd");
        registerDTDResource("-//Sun Microsystems, Inc.//DTD JAIN SLEE Profile Specification 1.0//EN", "slee/dtd/slee-profile-spec-jar_1_0.dtd");
        registerDTDResource("-//Sun Microsystems, Inc.//DTD JAIN SLEE Event 1.0//EN", "slee/dtd/slee-event-jar_1_0.dtd");
        registerDTDResource("-//Sun Microsystems, Inc.//DTD JAIN SLEE Resource Adaptor Type 1.0//EN", "slee/dtd/slee-resource-adaptor-type-jar_1_0.dtd");
        registerDTDResource("-//Sun Microsystems, Inc.//DTD JAIN SLEE Resource Adaptor 1.0//EN", "slee/dtd/slee-resource-adaptor-jar_1_0.dtd");
        registerDTDResource("-//Sun Microsystems, Inc.//DTD JAIN SLEE Service 1.0//EN", "slee/dtd/slee-service-xml_1_0.dtd");

        // SLEE 1.1
        registerDTDResource("-//Sun Microsystems, Inc.//DTD JAIN SLEE Deployable Unit 1.1//EN", "slee/dtd/slee-deployable-unit_1_1.dtd");
        registerDTDResource("-//Sun Microsystems, Inc.//DTD JAIN SLEE Library 1.1//EN", "slee/dtd/slee-library-jar_1_1.dtd");
        registerDTDResource("-//Sun Microsystems, Inc.//DTD JAIN SLEE SBB 1.1//EN", "slee/dtd/slee-sbb-jar_1_1.dtd");
        registerDTDResource("-//Sun Microsystems, Inc.//DTD JAIN SLEE Profile Specification 1.1//EN", "slee/dtd/slee-profile-spec-jar_1_1.dtd");
        registerDTDResource("-//Sun Microsystems, Inc.//DTD JAIN SLEE Event 1.1//EN", "slee/dtd/slee-event-jar_1_1.dtd");
        registerDTDResource("-//Sun Microsystems, Inc.//DTD JAIN SLEE Resource Adaptor Type 1.1//EN", "slee/dtd/slee-resource-adaptor-type-jar_1_1.dtd");
        registerDTDResource("-//Sun Microsystems, Inc.//DTD JAIN SLEE Resource Adaptor 1.1//EN", "slee/dtd/slee-resource-adaptor-jar_1_1.dtd");
        registerDTDResource("-//Sun Microsystems, Inc.//DTD JAIN SLEE Service 1.1//EN", "slee/dtd/slee-service-xml_1_1.dtd");
    }
}
