/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.ant;

import java.io.IOException;
import java.util.Iterator;
import java.util.List;

import org.apache.tools.ant.BuildException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import com.opencloud.util.XMLParser;

/**
 * The ResourceAdaptorJar task automates the construction of a resource adaptor
 * jar file that may be included in a deployable unit jar.
 * <pre>
 * &lt;resourceadaptorjar destfile="pathname.jar" resourceadaptorjarxml="foo/bar/resource-adaptor-jar.xml" autoinclude="yes" classpath="..."&gt;
 * &lt;classpath ...&gt;
 * &lt;fileset ... &gt;
 * &lt;/resourceadaptortypejar&gt;
 * </pre>
 * resourceadaptorjarxml is assumed to have a default value of resource-adaptor-jar.xml, so this property may be omitted if
 * the metatinfbase property is used or inherited from an enclosing deployableunit task.
 */
public class ResourceAdaptorJar extends SleeJar {
    public ResourceAdaptorJar() {
        super("resource-adaptor-jar", "create");
    }

    protected final void includeTypeSpecificClasses() throws BuildException {
        try {
            Document rajarDoc = XMLParser.getDocument(getJarXml().toURL(), entityResolver);
            Element raRoot = rajarDoc.getDocumentElement(); // <resource-adaptor>

            List raNodes = XMLParser.getElementsByTagName(raRoot, "resource-adaptor");
            Iterator raNodesIter = raNodes.iterator();
            while (raNodesIter.hasNext()) {
                Element raNode = (Element) raNodesIter.next();

                // Resource Adaptor classes are now defined in the SLEE 1.1
                // specification. -Alex
                List raClassesList = XMLParser.getElementsByTagName(raNode, "resource-adaptor-classes");
                Iterator raClassesListIter = raClassesList.iterator();
                while (raClassesListIter.hasNext()) {
                    Element raClasses = (Element) raClassesListIter.next();
                    List raClassList = XMLParser.getElementsByTagName(raClasses, "resource-adaptor-class");
                    Iterator raClassListIter = raClassList.iterator();
                    while (raClassListIter.hasNext()) {
                        Element raClass = (Element) raClassListIter.next();
                        String className = XMLParser.getTextElement(raClass, "resource-adaptor-class-name");
                        System.out.println("Including class: " + className);
                        includeClass(className);
                    }
                }
            }
        } catch (IOException e) {
            throw new BuildException(e);
        }
    }

    // resourceadaptortypejarxml="..."
    public void setResourceAdaptorjarxml(String resourceadaptortypejarxml) {
        setJarXml(resourceadaptortypejarxml);
    }

    protected final String getComponentType() {
        return "resourceadaptorjar";
    }

    protected final String getJarXmlName() {
        return "resourceadaptorjarxml";
    }

    //private static final FileUtils fileUtils = FileUtils.newFileUtils();
    private static final SleeDTDResolver entityResolver = new SleeDTDResolver();
}
