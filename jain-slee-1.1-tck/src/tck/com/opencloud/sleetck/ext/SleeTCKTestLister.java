/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.ext;

import com.opencloud.sleetck.lib.infra.testfinder.SleeTCKTestFinder;
import com.sun.javatest.TestDescription;
import com.sun.javatest.TestFinder;
import java.io.File;
import java.io.PrintStream;
import java.io.FileOutputStream;

/**
 * A tool which lists the paths of all the test descriptions in a set of directories/files.
 */
public class SleeTCKTestLister {

    /**
     * Lists the paths of all the test descriptions in the given directory/file.
     * @param testSuiteRoot the root directory of the test suite
     * @param testsFileOrDir the tests directory or file to search in
     * @param out the PrintStream to write the list of test description paths to
     */
    public synchronized void listTestDescriptions(File testSuiteRoot, File testsFileOrDir, PrintStream out) throws Exception {
        try {
            this.out = out;
            finder = new SleeTCKTestFinder(testSuiteRoot);
            processFileOrDirectory(testsFileOrDir); // recursively process all the test directories
        } finally {
            this.out = null;
            finder = null;
        }
    }

    /**
     * Recursively processes all the test directories and test descriptions
     */
    private void processFileOrDirectory(File file) throws Exception {
        finder.read(file);
        if(file.isDirectory()) {
            File[] filesRead = finder.getFiles();
            for (int i = 0; i < filesRead.length; i++) {
                processFileOrDirectory(filesRead[i]); // recurse
            }
        } else { // file
            TestDescription[] descriptions = finder.getTests();
            for (int i = 0; i < descriptions.length; i++) {
                listFile(descriptions[i].getRootRelativePath());
            }
        }
    }

    /**
     * Prints the given path to the output stream
     */
    private void listFile(String path) {
        out.println(path);
    }

    /**
     * Syntax: java SleeTCKTestLister testSuiteRoot [-o outputPath] testFile1 [testFile2, ... ]
     */
    public static void main(String[] args) {
        int exitStatus = 0;
        PrintStream createdSteam = null;
        PrintStream out = null;
        try {
            if(args.length < 2) throw new IllegalArgumentException("Insufficient arguments");
            int index = 0;
            File testSuiteRoot = new File(args[index++]);
            if(!testSuiteRoot.isDirectory()) throw new IllegalArgumentException("testSuiteRoot "+testSuiteRoot+" is not a valid directory");
            if(!testSuiteRoot.isAbsolute()) testSuiteRoot = testSuiteRoot.getAbsoluteFile();
            if(args[index].equals("-o")) {
                out = createdSteam = new PrintStream(new FileOutputStream(args[++index]));
                index++;
            } else out = System.out;

            if(index >= args.length) throw new IllegalArgumentException("Insufficient arguments");
            SleeTCKTestLister lister = new SleeTCKTestLister();
            while (index < args.length) {
                File testsFileOrDir = new File(testSuiteRoot,args[index++]);
                if(!testsFileOrDir.exists()) throw new IllegalArgumentException("testsFileOrDir "+testsFileOrDir+
                    " cannot be found. note that this file is resolved against the testsuite directory");
                lister.listTestDescriptions(testSuiteRoot,testsFileOrDir,out);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            System.err.println("Syntax: java SleeTCKTestLister testSuiteRoot [-o outputPath] testFile1 [testFile2, ... ]");
            exitStatus = 1;
        } finally {
            if(createdSteam != null) {
                try {
                    createdSteam.flush();
                    createdSteam.close();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
            System.exit(exitStatus);
        }
    }

    private SleeTCKTestFinder finder;
    private PrintStream out;

}
