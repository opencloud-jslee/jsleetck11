/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/package com.opencloud.sleetck.ext;

import java.io.*;
import java.net.InetAddress;
import java.net.UnknownHostException;

public class SetupTCKConfig {

    public static String vendor_lib;
    public static String tck_home;
    public static String localIP;

    /**
    * Sets up the JAIN SLEE TCK:
    * - generates configuration files from templates for TCK1.1 Release
    *
    * Syntax: <script> [<VENDOR_LIB>]
    *
    *  VENDOR_LIB - the optional path of a jar file containing vendor classes.
    *     If not specified, the variable VENDOR_LIB will have to be set
    *     manually in config/config_variables.unix
     */
    public static void main(String[] args) {

        vendor_lib = "";
        tck_home = System.getProperty("user.dir").replaceAll("\\\\", "/");

        File f = new File(tck_home+"/bin/setup-tck-config.sh");
        if (!f.exists())
            fail("This script must be run from the base TCK directory");

        ensureAbsoluteDir( tck_home, "TCK_HOME");

        // take first command line arg as vendor_lib, if there's more they are thrown away.
        if (args.length ==1)
            vendor_lib = args[0].replaceAll("\\\\", "/");
        else if (args.length>1) {
            printUsage();
            fail("Script was invoked with unallowed command line parameters");
        }

        // Get the local IP address before we generate the configuration files
        String loopback="127.0.0.1";
        try {
            localIP= InetAddress.getLocalHost().getHostAddress();
            if (localIP != null && !loopback.equals(localIP))
                System.out.println("Detected local IP address: "+localIP);
            else {
                System.out.println("Unable to detect local IP address. Using loopback address "+loopback+" instead");
                localIP = loopback;
            }
        } catch (UnknownHostException e) {
            System.out.println("Exception occured detecting local IP address. Using loopback address "+loopback+" instead: ");
            e.printStackTrace();
            localIP = loopback;
        }

        // Building configuration files from templates
        System.out.println( "Building configuration files from templates...");
        File tck_config_dir = new File(tck_home+"/config");
        if (!tck_config_dir.exists())
            fail(tck_home+"/config directory could not be found!");

        File[] config_files = tck_config_dir.listFiles();
        for (int i=0; i<config_files.length; i++) {
            File config_file = config_files[i];
            String name = config_file.getName();
            String path = tck_home+"/config/";
            String file_ext = getFileExt(name);

            if ("template".equals(file_ext)) {
                File dest_config_file = new File(path+getConfFileName(name));
                buildFromTemplate(config_file, dest_config_file);
            }
        }

        //exit with status 0
        System.exit(0);
    }

    private static void printUsage() {
        System.out.println("Sets up the JAIN SLEE TCK: \n" +
                "- generates configuration files from templates for TCK1.1 Release\n\n" +
                "Syntax: <script> [<VENDOR_LIB>] \n\n" +
                "   VENDOR_LIB - the optional path of a jar file containing vendor classes.\n" +
                "                If not specified, the variable VENDOR_LIB will have to be set \n" +
                "                manually in config/config_variables.unix");
    }

    private static String getConfFileName(String template_filename) {
        int pos = template_filename.lastIndexOf(".template");

         if (pos<0)
             return null;
         else
             return  template_filename.substring(0, pos);
    }

    private static String getFileExt(String filename) {
        int pos = filename.lastIndexOf('.');

        if (pos<0)
            return "";
        else
            return filename.substring(pos+1);
    }

    private static void fail(String reason) {
        System.err.println("Error: "+reason);
        System.exit(1);
    }

    // ensureAbsoluteDir <full-path> <display-name>
    private static void ensureAbsoluteDir(String to_check, String display_name) {
        File f = new File(to_check);
        if (!f.exists() || !f.isDirectory())
            fail(display_name+" directory not found at "+to_check);

        if (!f.isAbsolute())
            fail (to_check+" is not an absolute pathname");
    }

    // buildFromTemplate <template> <destination-file>
    private static void buildFromTemplate(File template, File destination) {
        System.out.println("Generating "+destination.getName()+" from template "+template.getName()+"...");

        // Maybe an ugly hack but works: file://@TCK_HOME@/... from the templates is under linux correctly replaced
        //by file:///home/user... as an absolute path name starts with '/', however under
        //windows it will be replaced by file://c:/... which is incorrect, so make sure that
        //the tck_home replacer always has a preceeding '/', which implies however the knowledge that it always
        //appears in a file://@TCK_HOME@/ context in the template files!
        String tck_home_mod = tck_home;
        if (!tck_home_mod.startsWith("/"))
            tck_home_mod = "/"+tck_home_mod;

        BufferedReader in = null;
        try {
            in = new BufferedReader(new FileReader(template));
        } catch (FileNotFoundException e) {
            System.out.println("Building template from "+template.getName() +" not successful because file could not be found: ");
            e.printStackTrace();
            return;
        }

        FileWriter out = null;
        try {
            out = new FileWriter(destination);
        } catch (IOException e) {
            System.out.println("Exception occured when creating destination file "+destination.getName()+" for template "+template.getName());
            e.printStackTrace();
            return;
        }

        char[] cbuf = new char[1024];
        int count = -1;
        try {
            while ((count=in.read(cbuf))!=-1) {
                String S = new String(cbuf, 0, count);
                S = S.replaceAll("@VENDOR_LIB@", vendor_lib);
                S = S.replaceAll("@TCK_HOME@", tck_home_mod);
                S = S.replaceAll("@LOCAL_IP@", localIP);

                out.write(S);
            }
        } catch (IOException e) {
            System.out.println("Exception occured while instantiating template file "+template.getName()+": ");
            e.printStackTrace();
            return;
        } finally {
            try {
                in.close();
                out.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

    }

}
