/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.ext.sleemanagement;

import com.opencloud.logging.LogLevelUtil;
import com.opencloud.logging.Logable;
import com.opencloud.logging.StdErrLog;
import com.opencloud.sleetck.lib.EnvironmentKeys;
import com.opencloud.sleetck.lib.OperationTimedOutException;
import com.opencloud.sleetck.lib.TCKTestErrorException;
import com.opencloud.sleetck.lib.infra.eventbridge.ClientSetup;
import com.opencloud.sleetck.lib.infra.eventbridge.TCKEventListenerRegistry;
import com.opencloud.sleetck.lib.infra.eventbridge.TCKEventSenderRemote;
import com.opencloud.sleetck.lib.infra.jmx.MBeanFacadeAgentRemote;
import com.opencloud.sleetck.lib.infra.jmx.MBeanFacadeImpl;
import com.opencloud.sleetck.lib.infra.jmx.MBeanFacadeRMIConstants;
import com.opencloud.sleetck.lib.infra.sleeplugin.SleeTCKPluginMBean;
import com.opencloud.sleetck.lib.testutils.jmx.SleeManagementMBeanProxy;
import com.opencloud.sleetck.lib.testutils.jmx.impl.SleeManagementMBeanProxyImpl;
import com.opencloud.util.SleepUtil;

import javax.management.ObjectName;
import java.io.File;
import java.io.FileInputStream;
import java.rmi.Naming;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.util.Properties;
import java.util.Vector;

/**
 * Utility class for waiting for the SLEE to initialise.
 */
public class WaitForSlee {

    // Constants

    // ms to wait between connection attempts and queries
    private static final long WAIT_PAUSE = 500;
    // ms to wait after first successful lookup of a TCK plugin component
    private static final long DEFAULT_WAIT_AFTER_CONNECT = 15000;

    private static final String CONFIG_PATH = "config/slee-management.config";
    private String[] MANDATORY_PROPERTIES = {
     EnvironmentKeys.JMX_AGENT_HOST_IP,EnvironmentKeys.JMX_AGENT_PORT,
     EnvironmentKeys.DEFAULT_TIMEOUT,EnvironmentKeys.DEBUG_LEVEL};

    /**
     * main() waits until
     * (1) the SLEE is initialised (exit status 0)
     * (2) the timeout is reached (exit status 1), or
     * (3) an unexpected Error or Exception is thrown (exit status 2).
     *
     * The SLEE in considered initialised after the TCK plugin and SleeManagementMBean
     * are initalised and available.
     *
     * <pre>
     * Syntax: java WaitForSlee <timeout-seconds> [<wait-after-connect>]
     *   timeout-seconds - the number of seconds to wait for the SLEE to initialise before timing out
     *   wait-after-connect - the number of seconds to wait after finding the TCK plugin components before
     *     checking the SleeManagementMBean (default is 15 seconds)
     * </pre>
     */
    public static void main(String[] args) {
        WaitForSlee waiter = null;
        try {
            if(args.length == 0) throw new IllegalArgumentException("Syntax: java WaitForSlee <timeout-seconds>");
            long timeout = Integer.parseInt(args[0]) * 1000;
            long waitAfterConnect = args.length >= 2 ? Integer.parseInt(args[1]) * 1000 : DEFAULT_WAIT_AFTER_CONNECT;
            waiter = new WaitForSlee();
            waiter.waitForSlee(timeout,waitAfterConnect);
            System.out.println("Confirmed that the SLEE is initialised");
            System.exit(0);
        } catch (OperationTimedOutException toex) {
            System.err.println("Timed out while waiting for the SLEE to initialise:"+toex);
            System.exit(1);
        } catch (Exception ex) {
            ex.printStackTrace();
            try {
                if(waiter != null) waiter.cleanUp();
            } catch(Exception e) {
                ex.printStackTrace();
            }
            System.exit(2);
        }
    }

    public WaitForSlee() throws Exception {
        log = new StdErrLog();
        readConfig();
        log.setIgnoreLevel(LogLevelUtil.toLogLevel(config.getProperty(EnvironmentKeys.DEBUG_LEVEL)));
    }

    public void waitForSlee(long timeout, long waitAfterConnect) throws Exception {
        long timeoutAt = System.currentTimeMillis() + timeout;
        connectToMBeanFacade(timeoutAt, waitAfterConnect);
        checkManagementMBean();
    }

    private void connectToMBeanFacade(long timeoutAt, long waitAfterConnect) throws Exception {
        try {
            // lookup the remote sender
            String jmxAgentHostIP = config.getProperty(EnvironmentKeys.JMX_AGENT_HOST_IP);
            String jmxAgentHostPort = config.getProperty(EnvironmentKeys.JMX_AGENT_PORT);
            String remoteSenderURL = "//"+jmxAgentHostIP+":"+jmxAgentHostPort+"/"+MBeanFacadeRMIConstants.TCK_EVENT_SENDER_REMOTE;

            // expect that the remote sender may not yet be bound
            log.info("Looking up tck plugin components...");
            long now = System.currentTimeMillis();
            while(remoteSender == null && now < timeoutAt) {
                try {
                    remoteSender = (TCKEventSenderRemote)Naming.lookup(remoteSenderURL);
                    break;
                } catch (RemoteException rex) {
                    log.fine("Caught expected exception while waiting for TCK plugin: "+rex);
                } catch (NotBoundException nbex) {
                    log.fine("Caught expected exception while waiting for TCK plugin: "+nbex);
                }
                SleepUtil.sleepFor(Math.min(WAIT_PAUSE,timeoutAt - now));
                now = System.currentTimeMillis();
            }
            if(remoteSender == null) throw new OperationTimedOutException("Timed out while waiting "+
             "for the TCK components to be bound in the rmi registry.");

            // we successfully connected to our first tck component. allow the SLEE and
            // TCK plugin some time to initialise
            log.info("Sleeping for "+waitAfterConnect+" ms to allow the tck plugin and SLEE "+
             "to initialise...");
            SleepUtil.sleepFor(waitAfterConnect);

            // continue setting up connection to the SLEE
            log.info("Awoke. Continuing connection process...");
            TCKEventListenerRegistry listenerRegistry = ClientSetup.setupClient(remoteSender);
            mBeanFacade = new MBeanFacadeImpl(log);

            // lookup and connect to the remote MBeanFacade agent
            String facadeURL = "//"+jmxAgentHostIP+":"+jmxAgentHostPort+"/"+MBeanFacadeRMIConstants.MBEAN_FACADE_AGENT_REMOTE;
            MBeanFacadeAgentRemote facadeAgent = (MBeanFacadeAgentRemote)Naming.lookup(facadeURL);
            mBeanFacade.setMBeanFacadeAgentRemote(facadeAgent);

            // the MBeanFacade listens to the event listener registry
            mBeanFacade.setListenerRegistry(listenerRegistry);
        } catch(java.rmi.ConnectException ce) {
            throw new TCKTestErrorException(
                "Received ConnectException while trying to connect to TCK plugin. "+
                "Check that the TCK Plugin MBean was successfully registered in the MBeanServer, "+
                "and that the jmx agent properties are properly configured",ce);
        }
    }

    /**
     * Looks up the SleeManagementMBean and queries for the current state.
     * If this method returns successfully, the SLEE is considered initialised.
     */
    private void checkManagementMBean() throws Exception {
        String jmxDomain = ""; // currently not specified
        ObjectName pluginName = new ObjectName(jmxDomain + SleeTCKPluginMBean.JMX_ID);
        log.fine("Asking TCK plugin for SleeManagementMBean name...");
        ObjectName mgmtMBeanName = (ObjectName)mBeanFacade.invoke(pluginName, "getSleeManagementMBean", new Object[]{}, new String[]{});
        log.fine("SleeManagementMBean name:"+mgmtMBeanName);
        SleeManagementMBeanProxy mgmtProxy = new SleeManagementMBeanProxyImpl(mgmtMBeanName,mBeanFacade);
        log.fine("Invoking getState() on the SleeManagementMBean to test the availability of the MBean...");
        mgmtProxy.getState(); // will fail if the SleeManagementMBean is not available
        log.fine("getState() call was successful");
    }

    public void cleanUp() throws Exception {
        log.fine("Cleaning up resources...");
        Vector caught = new Vector();

        // event bridge
        if(remoteSender != null) {
            try {
                remoteSender.disconnectReceiver();
            } catch(Exception e) { e.printStackTrace(); caught.addElement(e); }
            remoteSender = null;
        }

        // jmx
        if(mBeanFacade != null) {
            try {
                mBeanFacade.removeAllNotificationListeners();
            } catch(Exception e) { e.printStackTrace(); caught.addElement(e); }
            mBeanFacade = null;
        }

        if(caught.size() == 1) throw (Exception)caught.firstElement();
        if(caught.size() > 1) throw new TCKTestErrorException("Multiple Exceptions caught during "+
            "test cleanup. (Stack traces were written to the log)");
    }

    private void readConfig() throws Exception {
        log.finer("Reading configuration...");
        config = new Properties();
        File configFile = new File(CONFIG_PATH);
        if(!configFile.exists()) throw new Exception("Configuration file not found at path: "+configFile.getAbsolutePath());
        FileInputStream fis = new FileInputStream(configFile);
        config.load(fis);
        fis.close();
        for (int i = 0; i < MANDATORY_PROPERTIES.length; i++) {
            if(config.getProperty(MANDATORY_PROPERTIES[i]) == null)
                throw new Exception("No value set for mandatory property \'"+MANDATORY_PROPERTIES[i]+"\'");
        }
    }

    // Private state

    private MBeanFacadeImpl mBeanFacade;
    private TCKEventSenderRemote remoteSender;
    private Logable log;
    private Properties config;

}
