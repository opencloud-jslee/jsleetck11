/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.ext.sleemanagement;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.FileInputStream;
import java.net.InetAddress;
import java.net.MalformedURLException;
import java.util.Properties;
import java.util.Iterator;
import java.util.Vector;
import java.rmi.Naming;
import java.rmi.RemoteException;
import java.rmi.NotBoundException;

import javax.management.ObjectName;
import javax.management.MBeanException;

import com.opencloud.logging.Logable;
import com.opencloud.logging.LogLevel;
import com.opencloud.logging.LogLevelUtil;
import com.opencloud.logging.StdErrLog;
import com.opencloud.sleetck.lib.SleeTCKTest;
import com.opencloud.sleetck.lib.SleeTCKTestUtils;
import com.opencloud.sleetck.lib.EnvironmentKeys;
import com.opencloud.sleetck.lib.TCKTestErrorException;
import com.opencloud.sleetck.lib.testutils.ExceptionsUtil;
import com.opencloud.sleetck.lib.testutils.SleeStarter;
import com.opencloud.sleetck.lib.infra.SleeTCKTestUtilsImpl;
import com.opencloud.sleetck.lib.testutils.jmx.MBeanFacade;
import com.opencloud.sleetck.lib.infra.jmx.MBeanFacadeImpl;
import com.opencloud.sleetck.lib.infra.jmx.MBeanFacadeRMIConstants;
import com.opencloud.sleetck.lib.infra.jmx.MBeanFacadeAgentRemote;
import com.opencloud.sleetck.lib.infra.eventbridge.TCKEventReceiverRemoteImpl;
import com.opencloud.sleetck.lib.infra.eventbridge.TCKEventSenderRemote;
import com.opencloud.sleetck.lib.infra.eventbridge.TCKEventListenerRegistry;
import com.opencloud.sleetck.lib.infra.eventbridge.ClientSetup;
import com.opencloud.sleetck.lib.infra.sleeplugin.SleeTCKPluginMBean;
import com.opencloud.sleetck.lib.testutils.jmx.MBeanProxyFactory;
import com.opencloud.sleetck.lib.testutils.jmx.impl.MBeanProxyFactoryImpl;
import com.opencloud.sleetck.lib.testutils.jmx.SleeManagementMBeanProxy;

/**
 * Provides connectivity to a SLEE container via the TCK plugin.
 * Most of the initialisation code in this class is copied from SleeTCKScript.
 */
public class SleeConnectivity {

    // Constants

    private static final String CONFIG_PATH = "config/slee-management.config";

    private String[] MANDATORY_PROPERTIES = {
     EnvironmentKeys.JMX_AGENT_HOST_IP,EnvironmentKeys.JMX_AGENT_PORT,
     EnvironmentKeys.DEFAULT_TIMEOUT,EnvironmentKeys.DEBUG_LEVEL};

    // Public accessor methods

    public ObjectName getMgmtMBeanName() {
        return mgmtMBeanName;
    }
    public MBeanFacadeImpl getMBeanFacade() {
        return mBeanFacade;
    }
    public MBeanProxyFactory getMBeanProxyFactory() {
        return mBeanProxyFactory;
    }
    public Logable getLog() {
        return log;
    }
    public String getProperty(String key) {
        return config.getProperty(key);
    }
    public int getIntProperty(String key) {
        return Integer.parseInt(getProperty(key));
    }

    // Public lifecycle methods

    public SleeConnectivity() throws Exception {
        try {
            readConfig();

            // -- Prepare resources -- //

            log = new StdErrLog();
            log.setIgnoreLevel(LogLevelUtil.toLogLevel(getProperty(EnvironmentKeys.DEBUG_LEVEL)));

            initMBeanFacade();
            initMgmtMBeanName();
            mBeanProxyFactory = new MBeanProxyFactoryImpl(mBeanFacade);

            SleeManagementMBeanProxy mgmtProxy = mBeanProxyFactory.createSleeManagementMBeanProxy(mgmtMBeanName);
            SleeStarter.startSlee(mgmtProxy,getIntProperty(EnvironmentKeys.DEFAULT_TIMEOUT));
        } catch (Throwable t) {
            try {
                cleanUp();
            } catch(Throwable t2) {
                t2.printStackTrace();
            }
            if(t instanceof Exception) throw (Exception)t;
            else throw (Error)t;
        }
    }

    public void cleanUp() throws Exception {
        Vector caught = new Vector();

        // event bridge
        if(remoteSender != null) {
            try {
                remoteSender.disconnectReceiver();
            } catch(Exception e) { e.printStackTrace(); caught.addElement(e); }
            remoteSender = null;
        }

        // jmx
        if(mBeanFacade != null) {
            try {
                mBeanFacade.removeAllNotificationListeners();
            } catch(Exception e) { e.printStackTrace(); caught.addElement(e); }
            mBeanFacade = null;
            mBeanProxyFactory = null;
        }

        if(caught.size() == 1) throw (Exception)caught.firstElement();
        if(caught.size() > 1) throw new Exception("Multiple Exceptions caught during "+
            "test cleanup. (Stack traces were written to the log)");
    }

    // Private instance methods

    private void readConfig() throws Exception {
        config = new Properties();
        File configFile = new File(CONFIG_PATH);
        if(!configFile.exists()) throw new Exception("Configuration file not found at path: "+configFile.getAbsolutePath());
        FileInputStream fis = new FileInputStream(configFile);
        config.load(fis);
        fis.close();
        for (int i = 0; i < MANDATORY_PROPERTIES.length; i++) {
            if(config.getProperty(MANDATORY_PROPERTIES[i]) == null)
                throw new Exception("No value set for mandatory property \'"+MANDATORY_PROPERTIES[i]+"\'");
        }
    }

    private void initMBeanFacade() throws Exception {
        try {
            // lookup the remote sender
            String jmxAgentHostIP = getProperty(EnvironmentKeys.JMX_AGENT_HOST_IP);
            String jmxAgentHostPort = getProperty(EnvironmentKeys.JMX_AGENT_PORT);
            String remoteSenderURL = "//"+jmxAgentHostIP+":"+jmxAgentHostPort+"/"+MBeanFacadeRMIConstants.TCK_EVENT_SENDER_REMOTE;
            remoteSender = (TCKEventSenderRemote)Naming.lookup(remoteSenderURL);

            TCKEventListenerRegistry listenerRegistry = ClientSetup.setupClient(remoteSender);

            // create the MBeanFacade
            mBeanFacade = new MBeanFacadeImpl(log);

            // lookup and connect to the remote MBeanFacade agent
            String facadeURL = "//"+jmxAgentHostIP+":"+jmxAgentHostPort+"/"+MBeanFacadeRMIConstants.MBEAN_FACADE_AGENT_REMOTE;
            MBeanFacadeAgentRemote facadeAgent = (MBeanFacadeAgentRemote)Naming.lookup(facadeURL);
            mBeanFacade.setMBeanFacadeAgentRemote(facadeAgent);

            // the MBeanFacade listens to the event listener registry
            mBeanFacade.setListenerRegistry(listenerRegistry);
        } catch(java.rmi.ConnectException ce) {
            throw new Exception(
                "Received ConnectException while trying to connect to TCK plugin. "+
                "Check that the TCK Plugin MBean was successfully registered in the MBeanServer, "+
                "and that the jmx agent properties are properly configured. ConnectException:"+ce);
        }
    }

    private void initMgmtMBeanName() throws Exception {
        String jmxDomain = "";
        ObjectName pluginName = new ObjectName(jmxDomain + SleeTCKPluginMBean.JMX_ID);
        mgmtMBeanName = (ObjectName)mBeanFacade.invoke(pluginName, "getSleeManagementMBean", new Object[]{}, new String[]{});
    }

    // Private state

    private ObjectName mgmtMBeanName;
    private MBeanFacadeImpl mBeanFacade;
    private MBeanProxyFactory mBeanProxyFactory;
    private TCKEventSenderRemote remoteSender;
    private Logable log;
    private Properties config;

}