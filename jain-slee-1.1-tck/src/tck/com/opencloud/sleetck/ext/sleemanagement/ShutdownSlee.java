/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.ext.sleemanagement;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.FileInputStream;
import java.net.InetAddress;
import java.net.MalformedURLException;
import java.util.Properties;
import java.util.Iterator;
import java.util.Vector;
import java.rmi.Naming;
import java.rmi.RemoteException;
import java.rmi.NotBoundException;

import javax.management.ObjectName;
import javax.management.MBeanException;
import javax.slee.management.*;
import javax.slee.*;

import com.opencloud.logging.Logable;
import com.opencloud.logging.LogLevel;
import com.opencloud.logging.StdErrLog;
import com.opencloud.util.SleepUtil;
import com.opencloud.sleetck.lib.EnvironmentKeys;
import com.opencloud.sleetck.lib.TCKTestErrorException;
import com.opencloud.sleetck.lib.OperationTimedOutException;
import com.opencloud.sleetck.lib.testutils.ExceptionsUtil;
import com.opencloud.sleetck.lib.testutils.jmx.MBeanFacade;
import com.opencloud.sleetck.lib.infra.jmx.MBeanFacadeImpl;
import com.opencloud.sleetck.lib.infra.jmx.MBeanFacadeRMIConstants;
import com.opencloud.sleetck.lib.infra.jmx.MBeanFacadeAgentRemote;
import com.opencloud.sleetck.lib.infra.sleeplugin.SleeTCKPluginMBean;
import com.opencloud.sleetck.lib.testutils.jmx.MBeanProxyFactory;
import com.opencloud.sleetck.lib.testutils.jmx.impl.MBeanProxyFactoryImpl;
import com.opencloud.sleetck.lib.testutils.jmx.DeploymentMBeanProxy;
import com.opencloud.sleetck.lib.testutils.jmx.SleeManagementMBeanProxy;
import com.opencloud.sleetck.lib.testutils.jmx.impl.SleeManagementMBeanProxyImpl;

/**
 * Utility class for shutting down the SLEE.
 */
public class ShutdownSlee {

    // Constants

    private static final long WAIT_FOR_STOP_PAUSE = 500;

    /**
     * Syntax: java ShutdownSlee
     */
    public static void main(String[] args) {
        SleeConnectivity cnx = null;
        boolean success = false;
        try {
            cnx = new SleeConnectivity();
            SleeManagementMBeanProxy sleeMgmt = new SleeManagementMBeanProxyImpl(
                cnx.getMgmtMBeanName(),cnx.getMBeanFacade());
            ensureSleeIsStopped(sleeMgmt,cnx.getLog(),cnx.getIntProperty(EnvironmentKeys.DEFAULT_TIMEOUT));
            try {
                sleeMgmt.shutdown();
            } catch(TCKTestErrorException te) {
                Exception enclosed = te.getEnclosedException();
                if(enclosed != null && enclosed instanceof RemoteException) {
                    cnx.getLog().fine("(Caught expected RemoteException on shutdown call)");
                } else throw te;
            }
            cnx.getLog().info("Shutdown successful");
            success = true;
        } catch(Exception e) {
            e.printStackTrace();
            // we only attempt to clean up our slee connections if the shutdown failed
            try {
                if(cnx != null) {
                    cnx.getLog().fine("Attempting to clean up connections to the SLEE");
                    cnx.cleanUp();
                    cnx.getLog().fine("Cleanup complete");
                }
            } catch (Throwable t2) {
                t2.printStackTrace();
            }
        } finally {
            System.err.println("Exiting the JVM...");
            System.exit(success ? 0 : 1);
        }
    }

    private static void ensureSleeIsStopped(SleeManagementMBeanProxy sleeMgmt, Logable log, int timeout) throws Exception {
        SleeState sleeState = sleeMgmt.getState();
        if(!sleeState.isStopped()) {
            log.info("Waiting for SLEE to reach STOPPED state...");
            long now = System.currentTimeMillis();
            long timeoutAt = now + timeout;
            while(now < timeoutAt && !sleeState.isStopped()) {
                switch (sleeState.toInt()) {
                    case SleeState.SLEE_RUNNING:
                        sleeMgmt.stop();
                        break;
                    case SleeState.SLEE_STOPPING:
                        log.fine("SLEE is stopping - will wait...");
                        SleepUtil.sleepFor(Math.min(timeoutAt - now, WAIT_FOR_STOP_PAUSE));
                        break;
                    case SleeState.SLEE_STARTING:
                        log.fine("SLEE is starting - will wait...");
                        SleepUtil.sleepFor(Math.min(timeoutAt - now, WAIT_FOR_STOP_PAUSE));
                        break;
                    default: throw new IllegalStateException("Illegal SleeState: "+sleeState);
                }
                sleeState = sleeMgmt.getState();
                now = System.currentTimeMillis();
            }
            if(!sleeState.isStopped()) {
                // the timeout was reached
                throw new OperationTimedOutException("The timeout of "+timeout+
                    " ms was reached while waiting for the SLEE to stop");
            }
            log.info("SLEE stopped successfully");
        }
    }

}
