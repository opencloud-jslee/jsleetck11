/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.ext.sleemanagement;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.FileInputStream;
import java.net.InetAddress;
import java.net.MalformedURLException;
import java.util.Properties;
import java.util.Iterator;
import java.util.Vector;
import java.rmi.Naming;
import java.rmi.RemoteException;
import java.rmi.NotBoundException;

import javax.management.ObjectName;
import javax.management.MBeanException;
import javax.slee.resource.*;
import javax.slee.management.*;
import javax.slee.facilities.Level;
import javax.slee.*;
import com.opencloud.sleetck.ext.sleemanagement.SleeConnectivity;

import com.opencloud.logging.Logable;
import com.opencloud.logging.LogLevel;
import com.opencloud.logging.StdErrLog;
import com.opencloud.util.SleepUtil;
import com.opencloud.sleetck.lib.EnvironmentKeys;
import com.opencloud.sleetck.lib.OperationTimedOutException;
import com.opencloud.sleetck.lib.SleeTCKTestUtils;
import com.opencloud.sleetck.lib.testutils.ExceptionsUtil;
import com.opencloud.sleetck.lib.testutils.jmx.MBeanFacade;
import com.opencloud.sleetck.lib.infra.SleeTCKComponentConstants;
import com.opencloud.sleetck.lib.infra.SleeTCKTestUtilsImpl;
import com.opencloud.sleetck.lib.infra.jmx.MBeanFacadeImpl;
import com.opencloud.sleetck.lib.infra.jmx.MBeanFacadeRMIConstants;
import com.opencloud.sleetck.lib.infra.jmx.MBeanFacadeAgentRemote;
import com.opencloud.sleetck.lib.infra.sleeplugin.SleeTCKPluginMBean;
import com.opencloud.sleetck.lib.testutils.jmx.MBeanProxyFactory;
import com.opencloud.sleetck.lib.testutils.jmx.impl.MBeanProxyFactoryImpl;
import com.opencloud.sleetck.lib.testutils.jmx.ServiceManagementMBeanProxy;
import com.opencloud.sleetck.lib.testutils.jmx.impl.ServiceManagementMBeanProxyImpl;
import com.opencloud.sleetck.lib.testutils.jmx.SleeManagementMBeanProxy;
import com.opencloud.sleetck.lib.testutils.jmx.impl.SleeManagementMBeanProxyImpl;
import com.opencloud.sleetck.lib.testutils.jmx.DeploymentMBeanProxy;

/**
 * Utility class for automating the installation, trace level setting
 * and activation of a service.
 */
public class SetupService {

    public SetupService(SleeConnectivity connectivity, String traceLevel) throws Exception {
        this.connectivity=connectivity;
        sleeMgmt = new SleeManagementMBeanProxyImpl(
            connectivity.getMgmtMBeanName(),connectivity.getMBeanFacade());
        deploymentMgmt = connectivity.getMBeanProxyFactory().createDeploymentMBeanProxy(sleeMgmt.getDeploymentMBean());
        serviceMgmt = connectivity.getMBeanProxyFactory().createServiceManagementMBeanProxy(sleeMgmt.getServiceManagementMBean());
        // note: the use of the as utils object is very ad hoc - we're creating it with a subset of the
        // usual contructor arguments, and can only expect a subset of its functionality
        Properties envParams = new Properties();
        if(traceLevel != null) envParams.setProperty(EnvironmentKeys.SBB_TRACE_LEVEL,getLevelInt(traceLevel));
        utils = new SleeTCKTestUtilsImpl(null, envParams,
                connectivity.getMBeanFacade(), connectivity.getMBeanProxyFactory(),
                null, null, null, connectivity.getLog(),connectivity.getMgmtMBeanName(), null, -1, -1);
    }

    // Public instance methods

    public void installService(String duURL) throws Exception {
        getLog().info("-- Starting install and setup service procedure --");

        getLog().info("Installing the deployable unit");
        DeployableUnitID duID = deploymentMgmt.install(duURL);

        getLog().info("Activating the service(s) (and setting trace level if specified)");
        ServiceID[] serviceIDs = utils.activateServices(duID, true);

        getLog().info("Confirming that the service(s) is/are active");
        for (int i = 0; i < serviceIDs.length; i++) {
            if(!serviceMgmt.getState(serviceIDs[i]).isActive()) throw new Exception(
                "Service "+serviceIDs[i]+" is not active");
        }
        getLog().info("-- Finished install and setup service procedure --");
    }

    // Private instance methods

    /**
     * Returns the given level string (as returned by Level.toString()), and
     * returns the corresponding int value as a String.
     * eg getLevelInt("Info") returns "3".
     * This method assumes a non-null argument.
     */
    private String getLevelInt(String levelString) {
        Level[] validLevels = {Level.OFF,Level.SEVERE,Level.WARNING,
            Level.INFO,Level.CONFIG,Level.FINE,Level.FINER,Level.FINEST};
        Level level = null;
        for (int i = 0; i < validLevels.length; i++) {
            // note: this assumes that the return values of Level.toString() are unique and fixed for each Level
            if(validLevels[i].toString().equalsIgnoreCase(levelString)) level = validLevels[i];
        }
        if(level == null) throw new IllegalArgumentException("Unrecognized Level string: "+levelString);
        return Integer.toString(level.toInt());
    }

    private Logable getLog() {
        return connectivity.getLog();
    }

    // Static methods

    /**
     * Syntax: java SetupService [-t <trace-level>] <service-DU-URL1> [<service-DU-URL2>,...]
     */
    public static void main(String[] args) {
        SleeConnectivity connectivity = null;
        boolean success = false;
        try {
            int argIndex = 0;
            String traceLevel = null;
            if(args.length > 0 && args[argIndex].equals("-t")) {
                traceLevel = args[argIndex+1];
                argIndex+=2;
            }
            if(args.length < argIndex+1) {
                throw new IllegalArgumentException("At least one Service-DU-URL must be specified. "+
                    "Syntax: java SetupService [-t <trace-level>] <service-DU-URL1> [<service-DU-URL2>,...]");
            }
            String[] serviceDUURLs = new String[args.length - argIndex];
            for (int i = 0; i < serviceDUURLs.length; i++) {
                serviceDUURLs[i] = args[argIndex + i];
            }
            connectivity = new SleeConnectivity();
            SetupService instance = new SetupService(connectivity,traceLevel);
            for (int i = 0; i < serviceDUURLs.length; i++) {
                instance.installService(serviceDUURLs[i]);
            }
            success = true;
        } catch(Throwable t) {
            t.printStackTrace();
        } finally {
            try {
                if(connectivity != null) {
                    connectivity.getLog().fine("Cleaning up connections to the SLEE");
                    connectivity.cleanUp();
                    connectivity.getLog().fine("Cleanup complete");
                }
            } catch (Throwable t2) {
                t2.printStackTrace();
            }
            System.err.println("Exiting the JVM...");
            System.exit(success ? 0 : 1);
        }
    }

    // Private state

    private SleeConnectivity connectivity;
    private SleeManagementMBeanProxy sleeMgmt;
    private DeploymentMBeanProxy deploymentMgmt;
    private ServiceManagementMBeanProxy serviceMgmt;
    private SleeTCKTestUtils utils;

}
