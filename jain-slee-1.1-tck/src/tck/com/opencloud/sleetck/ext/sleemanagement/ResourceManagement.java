/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.ext.sleemanagement;

import javax.management.ObjectName;
import javax.slee.ComponentID;
import javax.slee.management.ComponentDescriptor;
import javax.slee.management.DeployableUnitDescriptor;
import javax.slee.management.DeployableUnitID;
import javax.slee.management.ResourceAdaptorEntityState;
import javax.slee.management.UnrecognizedResourceAdaptorEntityException;
import javax.slee.resource.ConfigProperties;
import javax.slee.resource.ResourceAdaptorID;
import javax.slee.resource.ResourceAdaptorTypeID;

import com.opencloud.logging.Logable;
import com.opencloud.logging.StdErrLog;
import com.opencloud.sleetck.ext.sleemanagement.SleeConnectivity;

import com.opencloud.sleetck.lib.EnvironmentKeys;
import com.opencloud.sleetck.lib.OperationTimedOutException;
import com.opencloud.sleetck.lib.infra.SleeTCKComponentConstants;
import com.opencloud.sleetck.lib.testutils.jmx.DeploymentMBeanProxy;
import com.opencloud.sleetck.lib.testutils.jmx.ResourceManagementMBeanProxy;
import com.opencloud.sleetck.lib.testutils.jmx.SleeManagementMBeanProxy;
import com.opencloud.sleetck.lib.testutils.jmx.impl.ResourceManagementMBeanProxyImpl;
import com.opencloud.sleetck.lib.testutils.jmx.impl.SleeManagementMBeanProxyImpl;
import com.opencloud.util.SleepUtil;

/**
 * Utility class for automating the TCK resource removal and installation and
 * setup procedures.
 */
public class ResourceManagement {
    // Constants

    private static final long WAIT_FOR_RA_PAUSE = 500;
    private static final String ENTITY_NAME = "TCK-RA";
    private static final String LINK_NAME = "slee/resources/tck";

    // Configuration parameters
    private static final String RESOURCE_TYPE_DU_PARAM = "resourceTypeDUPath";
    private static final String RESOURCE_ADAPTOR_DU_PARAM = "resourceAdaptorDUPath";

    private String[] MANDATORY_PROPERTIES = { RESOURCE_TYPE_DU_PARAM, RESOURCE_ADAPTOR_DU_PARAM };

    public ResourceManagement(SleeConnectivity connectivity) throws Exception {
        this.connectivity = connectivity;
        checkConfig();
        sleeMgmt = new SleeManagementMBeanProxyImpl(connectivity.getMgmtMBeanName(), connectivity.getMBeanFacade());
        ObjectName resourceManagementMBeanName = sleeMgmt.getResourceManagementMBean();
        resourceMgmt = new ResourceManagementMBeanProxyImpl(resourceManagementMBeanName, connectivity.getMBeanFacade());
        deploymentMgmt = connectivity.getMBeanProxyFactory().createDeploymentMBeanProxy(sleeMgmt.getDeploymentMBean());
    }

    // Public instance methods

    public void removeResource() throws Exception {
        getLog().info("-- Starting resource removal procedure --");

        getLog().info("Finding the resource adaptor's ID");
        ResourceAdaptorID tckRAID = (ResourceAdaptorID) getTCKComponent(deploymentMgmt.getResourceAdaptors());
        if (tckRAID == null)
            throw new Exception("Couldn't find TCK resource adaptor");

        getLog().info("Confirming presence of TCK resource adaptor entity");
        try {
            if (!resourceMgmt.getResourceAdaptor(ENTITY_NAME).equals(tckRAID))
                throw new Exception("Resource adaptor entity " + ENTITY_NAME + " is not a TCK resource adaptor");
        } catch (UnrecognizedResourceAdaptorEntityException uraee) {
            throw new Exception("TCK resource adaptor entity has not been created");
        }

        getLog().info("Confirming that the entity is active");
        ResourceAdaptorEntityState tckRAState = resourceMgmt.getState(ENTITY_NAME);
        if (!tckRAState.isActive())
            throw new Exception("TCK resource adaptor entity is not active");

        getLog().info("Unbinding the entity's link names");
        try {
            resourceMgmt.unbindLinkName(LINK_NAME);
        } catch (Exception ex) {
            getLog().warning("Caught Exception while trying to unbind the link names for the TCK resource adaptor entity:");
            getLog().warning(ex);
        }

        getLog().info("Deactivating the entity");
        resourceMgmt.deactivateResourceAdaptorEntity(ENTITY_NAME);
        waitForRAEntity(ENTITY_NAME);

        getLog().info("Removing the entity");
        resourceMgmt.removeResourceAdaptorEntity(ENTITY_NAME);

        getLog().info("Uninstalling the resource adaptor");
        deploymentMgmt.uninstall(getDUForComponent(tckRAID));

        getLog().info("Uninstalling the resource type");
        ResourceAdaptorTypeID tckResourceTypeID = (ResourceAdaptorTypeID) getTCKComponent(deploymentMgmt.getResourceAdaptorTypes());
        if (tckResourceTypeID == null)
            throw new Exception("Couldn't find TCK resource adaptor type");
        deploymentMgmt.uninstall(getDUForComponent(tckResourceTypeID));

        getLog().info("Confirming that no TCK resource types are installed");
        tckResourceTypeID = (ResourceAdaptorTypeID) getTCKComponent(deploymentMgmt.getResourceAdaptorTypes());
        if (tckResourceTypeID != null)
            throw new Exception("A TCK resource adaptor type is still installed: " + tckResourceTypeID);

        DeployableUnitID[] duIDs = deploymentMgmt.getDeployableUnits();
        if (duIDs != null && duIDs.length > 0) {
            getLog().warning("The following deployable units remain installed in the SLEE");
            for (int i = 0; i < duIDs.length; i++) {
                getLog().warning(duIDs[i].toString());
            }
        } else
            getLog().info("No deployable units remain installed in the SLEE");

        getLog().info("-- Finished resource removal procedure --");
    }

    public void installResource() throws Exception {
        getLog().info("-- Starting install and setup resource procedure --");

        getLog().info("Confirming that no TCK resource types are installed");
        ResourceAdaptorTypeID tckResourceTypeID = (ResourceAdaptorTypeID) getTCKComponent(deploymentMgmt.getResourceAdaptorTypes());
        if (tckResourceTypeID != null)
            throw new Exception("A TCK resource adaptor type is still installed: " + tckResourceTypeID);

        getLog().info("Installing the resource type");
        String duURL = getProperty(RESOURCE_TYPE_DU_PARAM);
        DeployableUnitID duID = deploymentMgmt.install(duURL);
        ComponentID[] componentIDs = deploymentMgmt.getDescriptor(duID).getComponents();
        for (int i = 0; i < componentIDs.length; i++) {
            if (componentIDs[i] instanceof ResourceAdaptorTypeID)
                tckResourceTypeID = (ResourceAdaptorTypeID) componentIDs[i];
        }
        if (tckResourceTypeID == null)
            throw new Exception("Deployable unit did not contain a resource type. Path=" + duURL);

        getLog().info("Installing the resource adaptor");
        duURL = getProperty(RESOURCE_ADAPTOR_DU_PARAM);
        duID = deploymentMgmt.install(duURL);
        componentIDs = deploymentMgmt.getDescriptor(duID).getComponents();
        ResourceAdaptorID tckRAID = null;
        for (int i = 0; i < componentIDs.length; i++) {
            if (componentIDs[i] instanceof ResourceAdaptorID)
                tckRAID = (ResourceAdaptorID) componentIDs[i];
        }
        if (tckRAID == null)
            throw new Exception("Deployable unit did not contain a resource adaptor. Path=" + duURL);

        getLog().info("Creating the resource entity");
        ConfigProperties configProperties = resourceMgmt.getConfigurationProperties(tckRAID);
        resourceMgmt.createResourceAdaptorEntity(tckRAID, ENTITY_NAME, configProperties);

        getLog().info("Activating the entity");
        resourceMgmt.activateResourceAdaptorEntity(ENTITY_NAME);

        getLog().info("Binding the entity to link name \'" + LINK_NAME + "\'");
        resourceMgmt.bindLinkName(ENTITY_NAME, LINK_NAME);

        getLog().info("Confirming the link");
        if (!resourceMgmt.getResourceAdaptorEntity(LINK_NAME).equals(ENTITY_NAME))
            throw new Exception("Resource adaptor entity found for link name doesn't match the tck resource adaptor entity " + ENTITY_NAME);

        getLog().info("Confirming that the entity is active");
        if (!resourceMgmt.getState(ENTITY_NAME).isActive())
            throw new Exception("TCK resource adaptor entity is not active");

        getLog().info("-- Finished install and setup resource procedure --");
    }

    // Private instance methods

    private String getProperty(String key) {
        return connectivity.getProperty(key);
    }

    private Logable getLog() {
        if (log == null)
            log = new StdErrLog();
        return log;
    }

    /**
     * Returns the first TCK component found in the given array of ComponentIDs
     */
    private ComponentID getTCKComponent(ComponentID[] componentIDs) throws Exception {
        ComponentID tckComponentID = null;
        ComponentDescriptor[] descriptors = deploymentMgmt.getDescriptors(componentIDs);
        for (int i = 0; i < componentIDs.length; i++) {
            if (SleeTCKComponentConstants.TCK_VENDOR.equals(descriptors[i].getVendor())) {
                if (tckComponentID == null)
                    tckComponentID = componentIDs[i];
                else
                    getLog().warning("More than one TCK component found for a particular component type. componentID=" + componentIDs[i]);
            }
        }
        return tckComponentID;
    }

    /**
     * Returns the ID of the deployable unit which contains the given component.
     */
    private DeployableUnitID getDUForComponent(ComponentID componentID) throws Exception {
        DeployableUnitID[] duIDs = deploymentMgmt.getDeployableUnits();
        for (int i = 0; i < duIDs.length; i++) {
            DeployableUnitDescriptor duDesc = deploymentMgmt.getDescriptor(duIDs[i]);
            ComponentID[] duComponentIDs = duDesc.getComponents();
            for (int j = 0; j < duComponentIDs.length; j++) {
                if (duComponentIDs[j].equals(componentID))
                    return duIDs[i];
            }
        }
        throw new Exception("Deployable unit not found for component: " + componentID);
    }

    private void waitForRAEntity(String tckRAEntityName) throws Exception {
        ResourceAdaptorEntityState entityState = resourceMgmt.getState(tckRAEntityName);
        if (!entityState.isInactive()) {
            getLog().info("Waiting for entity to reach inactive state...");
            long now = System.currentTimeMillis();
            int defaultTimeout = connectivity.getIntProperty(EnvironmentKeys.DEFAULT_TIMEOUT);
            long timeoutAt = now + defaultTimeout;
            while (now < timeoutAt && !entityState.isInactive()) {
                SleepUtil.sleepFor(Math.min(timeoutAt - now, WAIT_FOR_RA_PAUSE));
                entityState = resourceMgmt.getState(tckRAEntityName);
                now = System.currentTimeMillis();
            }
            if (!entityState.isInactive()) {
                // the timeout was reached
                throw new OperationTimedOutException("The timeout of " + defaultTimeout + " ms was reached while waiting for resource adaptor entity "
                        + tckRAEntityName + " to stop.");
            }
            getLog().debug("Resource adaptor entity stopped successfully");
        }
    }

    private void checkConfig() throws Exception {
        for (int i = 0; i < MANDATORY_PROPERTIES.length; i++) {
            if (getProperty(MANDATORY_PROPERTIES[i]) == null)
                throw new Exception("No value set for mandatory property \'" + MANDATORY_PROPERTIES[i] + "\'");
        }
    }

    // Static methods

    /**
     * Syntax: java ResourceManagement -r|-i|-ri
     * <p>
     * -r - remove the resource <br>
     * -i - install and setup the resource <br>
     * -ri - remove then install then setup the resource <br> ? - print help
     * info
     */
    public static void main(String[] args) {
        if (args.length != 1)
            printHelpAndExit();
        String arg = args[0];
        boolean removeResource = false;
        boolean installResource = false;
        if ("-r".equals(arg))
            removeResource = true;
        else if ("-i".equals(arg))
            installResource = true;
        else if ("-ri".equals(arg))
            removeResource = installResource = true;
        else
            printHelpAndExit();
        SleeConnectivity connectivity = null;
        boolean success = false;
        try {
            connectivity = new SleeConnectivity();
            ResourceManagement instance = new ResourceManagement(connectivity);
            if (removeResource)
                instance.removeResource();
            if (installResource)
                instance.installResource();
            success = true;
        } catch (Throwable t) {
            t.printStackTrace();
        } finally {
            try {
                if (connectivity != null) {
                    connectivity.getLog().debug("Cleaning up connections to the SLEE");
                    connectivity.cleanUp();
                    connectivity.getLog().debug("Cleanup complete");
                }
            } catch (Throwable t2) {
                t2.printStackTrace();
            }
            System.err.println("Exiting the JVM...");
            System.exit(success ? 0 : 1);
        }
    }

    private static void printHelpAndExit() {
        System.err.println("Usage: java ResourceManagement -r|-i|-ri");
        System.err.println(" -r   - remove the TCK Resource Adaptor");
        System.err.println(" -i   - install and setup the TCK Resource Adaptor");
        System.err.println(" -ri  - remove then install then setup the TCK Resource Adaptor");
        System.exit(1);
    }

    // Private state

    private SleeConnectivity connectivity;
    private SleeManagementMBeanProxy sleeMgmt;
    private ResourceManagementMBeanProxy resourceMgmt;
    private DeploymentMBeanProxy deploymentMgmt;
    private Logable log;
}
