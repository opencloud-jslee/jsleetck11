/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.ext;

import com.sun.javatest.InterviewParameters;
import com.opencloud.sleetck.lib.interview.SleeTCKInterview;
import java.util.Properties;
import java.io.File;
import java.io.FileInputStream;

/**
 * This utility adds a valid checksum to a sleetck
 * configuration interview file, thus allowing a script
 * or user to edit the interview file as plain text.
 */
public class InterviewChecksumTool {

    /**
     * Syntax: java InterviewChecksumTool <interview file>
     */
    public static void main(String[] args) {
        String path = null;
        try {
            if(args.length == 0) throw new IllegalArgumentException("interview file path must be specified");
            path = args[0];
            File file = new File(path);
            System.err.println("Processing "+path+"...");
            // load the configuration properties
            Properties properties = new Properties();
            properties.load(new FileInputStream(file));
            // load the interview with the loaded properties
            InterviewParameters interview = new SleeTCKInterview();
            interview.load(properties,false); // false means don't check the checksum
            // save the interview, to generate and insert a valid checksum
            interview.save(file);
            // all done
            System.err.println("Done");
            System.exit(0);
        } catch (Exception ex) {
            System.err.println("Caught Exception while trying process "+path+":");
            ex.printStackTrace();
            System.err.println("Syntax: java InterviewChecksumTool <interview file>");
            System.exit(1);
        }
    }

}