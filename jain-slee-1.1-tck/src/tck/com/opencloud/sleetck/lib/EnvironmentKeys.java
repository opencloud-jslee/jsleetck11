/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib;

/**
 * Defines parameter names used in the
 * configuration interview.
 */
public class EnvironmentKeys {

    public static final String ENV_DESCRIPTION ="description";
    public static final String JMX_AGENT_HOST_IP = "jmxAgentHostIP";
    public static final String JMX_AGENT_PORT =    "jmxAgentPort";
    public static final String JMX_AGENT_DOMAIN =  "jmxAgentDomain";
    public static final String TCK_RESOURCE_HOST_IP = "tckResourceHostIP";
    public static final String TCK_RESOURCE_PORT = "tckResourcePort";
    public static final String COMPONENT_URL_PREFIX = "componentUrlPrefix";
    public static final String DEBUG_LEVEL = "debugLevel";
    public static final String SBB_TRACE_LEVEL = "sbbTraceLevel";
    public static final String DEFAULT_TIMEOUT = "defaultTimeout";
    public static final String RESTART_BETWEEEN_TESTS = "restartBetweenTests";

}