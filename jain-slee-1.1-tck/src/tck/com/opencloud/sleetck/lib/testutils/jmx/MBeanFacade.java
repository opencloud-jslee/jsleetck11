/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testutils.jmx;

import java.util.Set;
import java.util.Map;
import java.util.HashMap;
import java.util.Iterator;
import javax.management.*;
import com.opencloud.sleetck.lib.TCKCommunicationException;
import com.opencloud.sleetck.lib.TCKTestErrorException;
import com.opencloud.sleetck.lib.infra.eventbridge.TCKEventListener;
import java.rmi.RemoteException;

/**
 * This interface represents a reduced and adapted view of a
 * remote MBeanServer.
 * The functionality defined in this interface is a subset of the
 * functionality offered in javax.management.MBeanServer.
 * Each method throws a TCKCommunicationException upon a
 * communication failure, and a TCKTestErrorException if the facade
 * has not been connected to a remote agent.
 */
public interface MBeanFacade extends TCKEventListener {

    public ObjectInstance createMBean(String className, ObjectName name)
            throws ReflectionException, InstanceAlreadyExistsException, MBeanRegistrationException, MBeanException, NotCompliantMBeanException, TCKCommunicationException, TCKTestErrorException;

    public ObjectInstance createMBean(String className, ObjectName name, ObjectName loaderName)
        throws ReflectionException, InstanceAlreadyExistsException, MBeanRegistrationException, MBeanException, NotCompliantMBeanException,
               InstanceNotFoundException, TCKCommunicationException, TCKTestErrorException;

    public ObjectInstance createMBean(String className, ObjectName name, Object params[], String signature[])
        throws ReflectionException, InstanceAlreadyExistsException, MBeanRegistrationException, MBeanException,
                NotCompliantMBeanException, TCKCommunicationException, TCKTestErrorException ;

    public ObjectInstance createMBean(String className, ObjectName name, ObjectName loaderName, Object params[], String signature[])
        throws ReflectionException, InstanceAlreadyExistsException, MBeanRegistrationException, MBeanException, NotCompliantMBeanException,
               InstanceNotFoundException, TCKCommunicationException, TCKTestErrorException;

    public void unregisterMBean(ObjectName name) throws InstanceNotFoundException,
        MBeanRegistrationException, TCKCommunicationException, TCKTestErrorException;

    public ObjectInstance getObjectInstance(ObjectName name)
        throws InstanceNotFoundException, TCKCommunicationException, TCKTestErrorException ;

    public Set queryMBeans(ObjectName name, QueryExp query) throws TCKCommunicationException, TCKTestErrorException ;

    public Set queryNames(ObjectName name, QueryExp query) throws TCKCommunicationException, TCKTestErrorException;

    public boolean isRegistered(ObjectName name) throws TCKCommunicationException, TCKTestErrorException;

    public Integer getMBeanCount() throws TCKCommunicationException, TCKTestErrorException;

    public Object getAttribute(ObjectName name, String attribute) throws
        MBeanException, AttributeNotFoundException, InstanceNotFoundException,
        ReflectionException, TCKCommunicationException, TCKTestErrorException ;

    public AttributeList getAttributes(ObjectName name, String[] attributes)
        throws InstanceNotFoundException, ReflectionException, TCKCommunicationException, TCKTestErrorException;

    public void setAttribute(ObjectName name, Attribute attribute) throws
            InstanceNotFoundException, AttributeNotFoundException, InvalidAttributeValueException, MBeanException,
            ReflectionException ,TCKCommunicationException, TCKTestErrorException ;

    public AttributeList setAttributes(ObjectName name, AttributeList attributes)
        throws InstanceNotFoundException, ReflectionException, TCKCommunicationException, TCKTestErrorException;

    /**
     * Invokes the given method on the given object.
     * @return the return value of the operation. If the return type
     *  of the called method is 'void', this method returns null.
     */
    public Object invoke(ObjectName name, String operationName, Object params[], String signature[])
        throws InstanceNotFoundException, MBeanException, ReflectionException,  TCKCommunicationException, TCKTestErrorException;

    public String getDefaultDomain() throws TCKCommunicationException, TCKTestErrorException;

    /**
     * Registers a listener to receive remote notifications.
     * Note: the listener and handback objects are not serialized - the
     *  filter is.
     */
    public void addNotificationListener(ObjectName name, NotificationListener listener, NotificationFilter filter, Object handback)
            throws InstanceNotFoundException, TCKCommunicationException, TCKTestErrorException;

    public void addNotificationListener(ObjectName name, ObjectName listener, NotificationFilter filter, Object handback)
        throws InstanceNotFoundException, TCKCommunicationException, TCKTestErrorException;

    /**
     * Deregisters the given listener.
     * After this method returns, the listener will no longer
     * receive notifications.
     */
    public void removeNotificationListener(ObjectName name, NotificationListener listener)
        throws InstanceNotFoundException, ListenerNotFoundException, TCKCommunicationException, TCKTestErrorException;

    public void removeNotificationListener(ObjectName name, ObjectName listener)
        throws InstanceNotFoundException, ListenerNotFoundException, TCKCommunicationException, TCKTestErrorException;

    public MBeanInfo getMBeanInfo(ObjectName name) throws
            InstanceNotFoundException, IntrospectionException, ReflectionException,TCKCommunicationException, TCKTestErrorException;

    public boolean isInstanceOf(ObjectName name, String className) throws InstanceNotFoundException, TCKCommunicationException, TCKTestErrorException;

    // -- Methods with no parallel in the MBeanServer interface -- //

    /**
     * Removes all currently registered notification listeners.
     */
    public void removeAllNotificationListeners() throws TCKCommunicationException, TCKTestErrorException;

    // -- Implementation of TCKEventListener -- //

    public void handleEvent(Object event, Object channelID);

}
