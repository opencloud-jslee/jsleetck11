/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testutils.jmx;

import javax.management.ObjectName;
import javax.slee.management.ResourceUsageMBean;

/**
 * Contains factory methods for building SLEE MBean proxies.
 */
public interface MBeanProxyFactory {
    public AlarmMBeanProxy createAlarmMBeanProxy(ObjectName objName);
    public DeploymentMBeanProxy createDeploymentMBeanProxy(ObjectName objName);
    public ProfileMBeanProxy createProfileMBeanProxy(ObjectName objName);
    public ProfileProvisioningMBeanProxy createProfileProvisioningMBeanProxy(ObjectName objName);
    public ServiceManagementMBeanProxy createServiceManagementMBeanProxy(ObjectName objName);
    public ServiceUsageMBeanProxy createServiceUsageMBeanProxy(ObjectName objName);
    public SleeManagementMBeanProxy createSleeManagementMBeanProxy(ObjectName objName);
    public TraceMBeanProxy createTraceMBeanProxy(ObjectName objName);
    public SbbUsageMBeanProxy createSbbUsageMBeanProxy(ObjectName objName);
    public ProfileTableUsageMBeanProxy createProfileTableUsageMBeanProxy(ObjectName profileTableUsageMBeanName);
    public UsageMBeanProxy createUsageMBeanProxy(ObjectName objName);
    public ResourceUsageMBeanProxy createResourceUsageMBeanProxy(ObjectName objName);
    public UsageNotificationManagerMBeanProxy createUsageNotificationManagerMBeanProxy(ObjectName objName);
    public ResourceManagementMBeanProxy createResourceManagementMBeanProxy(ObjectName objName);
}
