/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testutils;

import com.opencloud.sleetck.lib.TCKTestResult;
import com.opencloud.sleetck.lib.TCKTestFailureException;
import com.opencloud.util.Future;
import com.opencloud.logging.Logable;

/**
 * Utility class for setting and waiting for a test result across multiple threads.
 * Only the first result is used - subsequent attempts to set a result are logged then ignored.   
 */
public class FutureResult {

    public FutureResult(Logable log) {
        this.log=log;
        result = new Future();
    }

    // Public methods

    public void setPassed() {
        setIfEmpty(TCKTestResult.passed());
    }

    public void setFailed(int assertionID, String reason) {
        setIfEmpty(TCKTestResult.failed(assertionID, reason));
    }

    public void setFailed(TCKTestFailureException failure) {
        setIfEmpty(TCKTestResult.failed(failure));
    }

    public void setError(String reason, Throwable cause) {
        setIfEmpty(TCKTestResult.error(reason, cause));
    }

    public void setError(String reason) {
        setIfEmpty(TCKTestResult.error(reason));
    }

    public void setError(Throwable cause) {
        setIfEmpty(TCKTestResult.error(cause));
    }

    public void setResult(TCKTestResult result) {
        setIfEmpty(result);
    }

    /**
     * Returns whether or not the test result has been set.
     */
    public boolean isSet() {
        return result.isSet();
    }

    /**
     * Blocks the calling thread until the test result is set or
     * the timeout is reached, then returns a test result.
     * In the case of a time out, an error result is returned.
     * (If the timeout indicates a failure, waitForResultOrFail() should be used instead).
     * @param timeout The amount of time to wait before timing out.
     *  If zero, real time is not taken into consideration and
     *  the method blocks until a value is set.
     * @param condition a human readable description of the condition which can timeout
     */
    public TCKTestResult waitForResult(long timeout, String condition) {
        return waitForResultImpl(timeout,false,condition,TCKTestResult.NO_ASSERTION);
    }

    /**
     * Blocks the calling thread until the test result is set or
     * the timeout is reached, then returns a test result.
     * In the case of a time out, a failure result is returned.
     * (If the timeout indicates an error, waitForResult() should be used instead).
     * @param timeout The amount of time to wait before timing out.
     *  If zero, real time is not taken into consideration and
     *  the method blocks until a value is set.
     * @param condition a human readable description of the condition which can timeout
     * @param assertionID the ID of the assertion related to the potential failure
     */
    public TCKTestResult waitForResultOrFail(long timeout, String condition, int assertionID) {
        return waitForResultImpl(timeout,true,condition,assertionID);
    }

    /**
     * Blocks the calling thread until the value is set or
     * the timeout is reached, then returns the value
     * or throws a TimeoutException.
     * @param timeout The amount of time to wait before timing out.
     *  If zero, real time is not taken into consideration and
     *  the method blocks until a value is set.
     */
    public TCKTestResult waitForResult(long timeout) throws Future.TimeoutException {
        return (TCKTestResult)result.getValue(timeout);
    }

    // Private methods

    private TCKTestResult waitForResultImpl(long timeout, boolean timeoutIsFail, String condition, int assertionID) {
        TCKTestResult rResult = null;
        try {
            rResult = waitForResult(timeout);
        } catch (Future.TimeoutException toe) {
            String message = "Timed out waiting for condition: "+condition;
            rResult = timeoutIsFail ? TCKTestResult.failed(assertionID, message) :
                                      TCKTestResult.error(message);
            synchronized (result) {
                if(result.isSet()) log.warning("Dropping result set by another thread "+
                                                "immediately after the timeout: "+result.getValue());
                // we override any value set by another thread since our timeout
                result.setValue(rResult);
            }
        }
        return rResult;
    }

    private void setIfEmpty(TCKTestResult value) {
        synchronized (result) {
            if(!result.isSet()) result.setValue(value);
            else log.warning("Attempt to set result for FutureResult after a result has already been set. "+
                             "Ignoring suggested new result: "+value.toString());
        }
    }

    // Private state

    private Future result;
    private Logable log;

}