/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testutils;

import javax.management.Notification;
import javax.management.NotificationListener;
import javax.slee.management.SleeState;
import javax.slee.management.SleeStateChangeNotification;

import com.opencloud.sleetck.lib.OperationTimedOutException;
import com.opencloud.sleetck.lib.TCKTestErrorException;
import com.opencloud.sleetck.lib.testutils.jmx.SleeManagementMBeanProxy;
import com.opencloud.util.Future;

/**
 * Utility class for starting the Slee - ie putting it
 * in the RUNNING state.
 */
public class SleeStarter {

    /**
     * Starts the Slee, i.e. gets it into the RUNNING state.
     * In the normal case this method will eventually return silently
     * to indicate that the Slee is in the RUNNING state.
     * A TCKTestErrorException is thrown to indicate one of the following:
     * (1) The operation timed out (in which case an OperationTimedOutException is thrown)
     * (2) The Slee couldn't start -
     *  ie it shifted from the STARTING state directly to the STOPPING state
     * (3) An exception occured while trying to connect with and invoke the
     *  SleeManagementMBean
     *
     * Note: this method does not support concurrent control of the Slee state machine by other Threads.
     *
     * @param mgmtProxy the proxy to use to control the Slee state machine
     * @param timeout the timeout for the start/restart cycle
     */
    public static void startSlee(SleeManagementMBeanProxy mgmtProxy, long timeout) throws TCKTestErrorException {
        startSlee(mgmtProxy,timeout,false);
    }

    /**
     * Restarts the Slee.
     * If in the RUNNING state, this method will stop the Slee, then get it back to the RUNNING state.
     * Otherwise it will simply get it to the RUNNING state.
     * In the normal case this method will eventually return silently to indicate that the Slee is in the RUNNING state.
     * A TCKTestErrorException is thrown to indicate one of the following:
     * (1) The operation timed out (in which case an OperationTimedOutException is thrown)
     * (2) The Slee couldn't start -
     *  ie it shifted from the STARTING state directly to the STOPPING state
     * (3) An exception occured while trying to connect with and invoke the
     *  SleeManagementMBean
     *
     * Note: this method does not support concurrent control of the Slee state machine by other Threads.
     *
     * @param mgmtProxy the proxy to use to control the Slee state machine
     * @param timeout the timeout for the start/restart cycle
     */
    public static void restartSlee(SleeManagementMBeanProxy mgmtProxy, long timeout) throws TCKTestErrorException {
        startSlee(mgmtProxy,timeout,true);
    }

    /**
     * Implementation of startSlee and restartSlee.
     * 
     * @param restartIfRunning
     *            if true, this method will restart the Slee if currently in the RUNNING state.
     */
    private static void startSlee(SleeManagementMBeanProxy mgmtProxy, long timeout, boolean restartIfRunning) throws TCKTestErrorException {

        Future futureResult = new Future(); // will be set with SUCCESS, otherwise with the Exception thrown
        SleeStateMachine stateMachine = null;
        try {
            SleeState currentState = mgmtProxy.getState();

            if (currentState.isRunning() && !restartIfRunning)
                return; // we are at our desired state

            stateMachine = new SleeStateMachine(mgmtProxy, futureResult);
            mgmtProxy.addNotificationListener(stateMachine, null, null);

            // Stop if running here - SleeStateMachine will take care of the restart.
            if (currentState.isRunning())
                mgmtProxy.stop();
            stateMachine.handleSleeState();
            
            Object result = futureResult.getValue(timeout);
            if (result == SUCCESS)
                return;
            else
                throw (Exception) result;
        } catch (Future.TimeoutException fte) {
            throw new OperationTimedOutException("Timed out while trying to start the Slee: " + fte);
        } catch (TCKTestErrorException te) {
            throw te;
        } catch (Exception e) {
            throw new TCKTestErrorException("Caught exception while trying to start the Slee: ", e);
        } finally {
            try {
                if (stateMachine != null)
                    mgmtProxy.removeNotificationListener(stateMachine);
            } catch (Exception e) {
                // no-op
            }
        }
    }

    /**
     * Reworking of old 1.0 TCK SLEE state notification handler. The old version was prone to break if a notification
     * was received after the associated state change had already been checked by the TCK via a management command. With
     * this version, only management commands are used to determine the current state of the SLEE. This is slightly more
     * inefficient, but has the benefit of actually working.
     */
    private static class SleeStateMachine implements NotificationListener {
        public SleeStateMachine(SleeManagementMBeanProxy mgmtProxy, Future result) {
            this.mgmtProxy = mgmtProxy;
            this.result = result;
        }

        public void handleNotification(Notification notification, Object handback) {
            if (notification instanceof SleeStateChangeNotification) {
                // We don't actually care what the notification is, just that a SLEE state transition occurred.
                handleSleeState();
            }
        }

        public synchronized void handleSleeState() {
            if (result.isSet())
                return;

            try {
                SleeState currentState = mgmtProxy.getState();

                if (currentState.isRunning()) {
                    result.setValue(SUCCESS);
                    return;
                }

                if (sleeStarted)
                    if (currentState.isStopped() || currentState.isStopping())
                        result.setValue(new TCKTestErrorException("Entered STOPPING state after STARTING state - the Slee failed to start."));

                if (currentState.isStopped()) {
                    // Start the SLEE if it's currently stopped.
                    mgmtProxy.start();
                    sleeStarted = true;
                    return;
                }
            } catch (Exception e) {
                result.setValue(e);
            }
        }

        private final SleeManagementMBeanProxy mgmtProxy;
        private final Future result;
        private boolean sleeStarted = false;
    }

    private static final String SUCCESS = "SUCCESS";

}
