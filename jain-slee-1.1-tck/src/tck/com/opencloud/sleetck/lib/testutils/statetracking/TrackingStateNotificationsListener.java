/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testutils.statetracking;

import javax.management.Notification;
import javax.management.NotificationListener;
import javax.slee.management.SleeState;
import javax.slee.management.SleeStateChangeNotification;
import com.opencloud.logging.Logable;

public class TrackingStateNotificationsListener implements NotificationListener {

    public TrackingStateNotificationsListener(StateMachineTrackerUtil stateTracker, Logable log) {
        this.stateTracker = stateTracker;
        this.log = log;
    }

    // -- Implementation of NotificationListener -- //

    public void handleNotification(Notification notification, Object handback) {
        if(notification instanceof SleeStateChangeNotification) {
            SleeState newState = ((SleeStateChangeNotification)notification).getNewState();
            SleeState previousState = ((SleeStateChangeNotification)notification).getOldState();
            log.finer("StateNotificationsListener:received state change to "+newState+" from "+previousState);
            stateTracker.shiftState(newState.toInt());
        }
    }

    private StateMachineTrackerUtil stateTracker;
    private Logable log;

}