/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testutils;

import java.rmi.RemoteException;
import com.opencloud.sleetck.lib.resource.TCKSbbMessage;
import com.opencloud.sleetck.lib.resource.TCKActivityID;
import com.opencloud.sleetck.lib.resource.testapi.TCKResourceListener;
import com.opencloud.sleetck.lib.TCKTestErrorException;

/**
 * An abstract implementation of TCKResourceListener which provides an
 * empty implementation for each method except onException() and
 * onEventProcessingFailed().
 * onException() is not implemented: test classes are expected to use the onException() callback to
 * set the test result to error.
 * The default implementation of onEventProcessingFailed() simply delegates to onException() with a
 * TCKTestErrorException describing the event failure.
 */
public abstract class BaseTCKResourceListener implements TCKResourceListener {

    public void onSbbMessage(TCKSbbMessage message, TCKActivityID calledActivity) throws RemoteException {}

    public Object onSbbCall(Object argument) throws Exception {
        return null;
    }

    public void onActivityCreatedBySbb(TCKActivityID activityID) throws RemoteException {}

    public void onActivityEndedBySbb(TCKActivityID activityID) throws RemoteException {}

// Maintenance note: this method should be activated if and when the relevant callback
//  method is introduced in a standard SLEE Resource Adaptor API
//    public void onActivityContextNotAttached(TCKActivityID activityID) throws RemoteException {}

    public void onActivityContextInvalid(TCKActivityID activityID) throws RemoteException {}

    public void onEventProcessingSuccessful(long eventObjectID) throws RemoteException {}

    /**
     * Delegates to onException() with a TCKTestErrorException describing the event failure.
     */
    public void onEventProcessingFailed(long eventObjectID, String message, Exception exception) throws RemoteException {
        StringBuffer buf = new StringBuffer("Received onEventProcessingFailed() callback for TCKResourceEvent with object ID="+eventObjectID);
        if(message != null) buf.append(message);
        onException(new TCKTestErrorException(buf.toString(),exception));
    }

}