/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testutils.jmx.impl;

import com.opencloud.sleetck.lib.TCKTestErrorException;
import com.opencloud.sleetck.lib.infra.jmx.NotificationBroadcasterProxyImpl;
import com.opencloud.sleetck.lib.testutils.jmx.AlarmMBeanProxy;
import com.opencloud.sleetck.lib.testutils.jmx.MBeanFacade;
import com.opencloud.sleetck.lib.testutils.jmx.impl.EmptyArrays;
import javax.management.InstanceNotFoundException;
import javax.management.MBeanException;
import javax.management.ObjectName;
import javax.management.ReflectionException;
import javax.management.RuntimeMBeanException;
import javax.slee.management.Alarm;
import javax.slee.management.ManagementException;
import javax.slee.management.NotificationSource;
import javax.slee.management.UnrecognizedNotificationSourceException;

/**
 * Defines a proxy implementation for javax.slee.management.AlarmMBean
 *
 * In addition to the Exceptions declared by each proxied method, 
 * each proxy method can throw a TCKTestErrorException - for commication failures, 
 * and to wrap InstanceNotFoundExceptions and ReflectionExceptions generated by the 
 * MBeanServer.invoke() method, and unchecked Exceptions other than RuntimeExceptions. 
 * RuntimeExceptions generated by the MBean are rethrown by the proxy.
 * 
 * This class was generated by the ProxyGenerator tool - Mon Jul 02 14:43:54 NZST 2007
 * Command line args: -n -a javax.slee.management.AlarmMBean 
 */
public class AlarmMBeanProxyImpl extends NotificationBroadcasterProxyImpl implements AlarmMBeanProxy {

    public AlarmMBeanProxyImpl(ObjectName objName, MBeanFacade facade) {
        super(objName,facade);
        this.objName = objName;
        this.facade = facade;
    }

    public Alarm[] getDescriptors(String[] p0) throws NullPointerException, ManagementException, TCKTestErrorException { 
        try {
            return (Alarm[])facade.invoke(objName,"getDescriptors",new Object[]{p0},new String[]{"[Ljava.lang.String;"});
        } catch(InstanceNotFoundException ie) {
            throw new TCKTestErrorException("Caught Exception while calling MBeanServer.invoke()",ie);
        } catch(ReflectionException re) {
            throw new TCKTestErrorException("Caught Exception while calling MBeanServer.invoke()",re);
        } catch(RuntimeMBeanException rmbe) {
            throw rmbe.getTargetException();
        } catch(MBeanException e) {
            Exception enclosed = e.getTargetException();
            if(enclosed instanceof NullPointerException) throw (NullPointerException)enclosed;
            if(enclosed instanceof ManagementException) throw (ManagementException)enclosed;
            if(enclosed instanceof RuntimeException) throw (RuntimeException)enclosed;
            throw new TCKTestErrorException("Caught undeclared exception",enclosed);
        }
    }

    public String[] getAlarms() throws ManagementException, TCKTestErrorException { 
        try {
            return (String[])facade.invoke(objName,"getAlarms",EmptyArrays.EMPTY_OBJECT_ARRAY,EmptyArrays.EMPTY_STRING_ARRAY);
        } catch(InstanceNotFoundException ie) {
            throw new TCKTestErrorException("Caught Exception while calling MBeanServer.invoke()",ie);
        } catch(ReflectionException re) {
            throw new TCKTestErrorException("Caught Exception while calling MBeanServer.invoke()",re);
        } catch(RuntimeMBeanException rmbe) {
            throw rmbe.getTargetException();
        } catch(MBeanException e) {
            Exception enclosed = e.getTargetException();
            if(enclosed instanceof ManagementException) throw (ManagementException)enclosed;
            if(enclosed instanceof RuntimeException) throw (RuntimeException)enclosed;
            throw new TCKTestErrorException("Caught undeclared exception",enclosed);
        }
    }

    public int clearAlarms(NotificationSource p0) throws NullPointerException, UnrecognizedNotificationSourceException, ManagementException, TCKTestErrorException { 
        try {
            Integer rValue = (Integer)facade.invoke(objName,"clearAlarms",new Object[]{p0},new String[]{"javax.slee.management.NotificationSource"});
            return rValue.intValue();
        } catch(InstanceNotFoundException ie) {
            throw new TCKTestErrorException("Caught Exception while calling MBeanServer.invoke()",ie);
        } catch(ReflectionException re) {
            throw new TCKTestErrorException("Caught Exception while calling MBeanServer.invoke()",re);
        } catch(RuntimeMBeanException rmbe) {
            throw rmbe.getTargetException();
        } catch(MBeanException e) {
            Exception enclosed = e.getTargetException();
            if(enclosed instanceof NullPointerException) throw (NullPointerException)enclosed;
            if(enclosed instanceof UnrecognizedNotificationSourceException) throw (UnrecognizedNotificationSourceException)enclosed;
            if(enclosed instanceof ManagementException) throw (ManagementException)enclosed;
            if(enclosed instanceof RuntimeException) throw (RuntimeException)enclosed;
            throw new TCKTestErrorException("Caught undeclared exception",enclosed);
        }
    }

    public boolean clearAlarm(String p0) throws NullPointerException, ManagementException, TCKTestErrorException { 
        try {
            Boolean rValue = (Boolean)facade.invoke(objName,"clearAlarm",new Object[]{p0},new String[]{"java.lang.String"});
            return rValue.booleanValue();
        } catch(InstanceNotFoundException ie) {
            throw new TCKTestErrorException("Caught Exception while calling MBeanServer.invoke()",ie);
        } catch(ReflectionException re) {
            throw new TCKTestErrorException("Caught Exception while calling MBeanServer.invoke()",re);
        } catch(RuntimeMBeanException rmbe) {
            throw rmbe.getTargetException();
        } catch(MBeanException e) {
            Exception enclosed = e.getTargetException();
            if(enclosed instanceof NullPointerException) throw (NullPointerException)enclosed;
            if(enclosed instanceof ManagementException) throw (ManagementException)enclosed;
            if(enclosed instanceof RuntimeException) throw (RuntimeException)enclosed;
            throw new TCKTestErrorException("Caught undeclared exception",enclosed);
        }
    }

    public Alarm getDescriptor(String p0) throws NullPointerException, ManagementException, TCKTestErrorException { 
        try {
            return (Alarm)facade.invoke(objName,"getDescriptor",new Object[]{p0},new String[]{"java.lang.String"});
        } catch(InstanceNotFoundException ie) {
            throw new TCKTestErrorException("Caught Exception while calling MBeanServer.invoke()",ie);
        } catch(ReflectionException re) {
            throw new TCKTestErrorException("Caught Exception while calling MBeanServer.invoke()",re);
        } catch(RuntimeMBeanException rmbe) {
            throw rmbe.getTargetException();
        } catch(MBeanException e) {
            Exception enclosed = e.getTargetException();
            if(enclosed instanceof NullPointerException) throw (NullPointerException)enclosed;
            if(enclosed instanceof ManagementException) throw (ManagementException)enclosed;
            if(enclosed instanceof RuntimeException) throw (RuntimeException)enclosed;
            throw new TCKTestErrorException("Caught undeclared exception",enclosed);
        }
    }

    public String[] getAlarms(NotificationSource p0) throws NullPointerException, UnrecognizedNotificationSourceException, ManagementException, TCKTestErrorException { 
        try {
            return (String[])facade.invoke(objName,"getAlarms",new Object[]{p0},new String[]{"javax.slee.management.NotificationSource"});
        } catch(InstanceNotFoundException ie) {
            throw new TCKTestErrorException("Caught Exception while calling MBeanServer.invoke()",ie);
        } catch(ReflectionException re) {
            throw new TCKTestErrorException("Caught Exception while calling MBeanServer.invoke()",re);
        } catch(RuntimeMBeanException rmbe) {
            throw rmbe.getTargetException();
        } catch(MBeanException e) {
            Exception enclosed = e.getTargetException();
            if(enclosed instanceof NullPointerException) throw (NullPointerException)enclosed;
            if(enclosed instanceof UnrecognizedNotificationSourceException) throw (UnrecognizedNotificationSourceException)enclosed;
            if(enclosed instanceof ManagementException) throw (ManagementException)enclosed;
            if(enclosed instanceof RuntimeException) throw (RuntimeException)enclosed;
            throw new TCKTestErrorException("Caught undeclared exception",enclosed);
        }
    }

    public int clearAlarms(NotificationSource p0, String p1) throws NullPointerException, UnrecognizedNotificationSourceException, ManagementException, TCKTestErrorException { 
        try {
            Integer rValue = (Integer)facade.invoke(objName,"clearAlarms",new Object[]{p0,p1},new String[]{"javax.slee.management.NotificationSource","java.lang.String"});
            return rValue.intValue();
        } catch(InstanceNotFoundException ie) {
            throw new TCKTestErrorException("Caught Exception while calling MBeanServer.invoke()",ie);
        } catch(ReflectionException re) {
            throw new TCKTestErrorException("Caught Exception while calling MBeanServer.invoke()",re);
        } catch(RuntimeMBeanException rmbe) {
            throw rmbe.getTargetException();
        } catch(MBeanException e) {
            Exception enclosed = e.getTargetException();
            if(enclosed instanceof NullPointerException) throw (NullPointerException)enclosed;
            if(enclosed instanceof UnrecognizedNotificationSourceException) throw (UnrecognizedNotificationSourceException)enclosed;
            if(enclosed instanceof ManagementException) throw (ManagementException)enclosed;
            if(enclosed instanceof RuntimeException) throw (RuntimeException)enclosed;
            throw new TCKTestErrorException("Caught undeclared exception",enclosed);
        }
    }

    public boolean isActive(String p0) throws NullPointerException, ManagementException, TCKTestErrorException { 
        try {
            Boolean rValue = (Boolean)facade.invoke(objName,"isActive",new Object[]{p0},new String[]{"java.lang.String"});
            return rValue.booleanValue();
        } catch(InstanceNotFoundException ie) {
            throw new TCKTestErrorException("Caught Exception while calling MBeanServer.invoke()",ie);
        } catch(ReflectionException re) {
            throw new TCKTestErrorException("Caught Exception while calling MBeanServer.invoke()",re);
        } catch(RuntimeMBeanException rmbe) {
            throw rmbe.getTargetException();
        } catch(MBeanException e) {
            Exception enclosed = e.getTargetException();
            if(enclosed instanceof NullPointerException) throw (NullPointerException)enclosed;
            if(enclosed instanceof ManagementException) throw (ManagementException)enclosed;
            if(enclosed instanceof RuntimeException) throw (RuntimeException)enclosed;
            throw new TCKTestErrorException("Caught undeclared exception",enclosed);
        }
    }

    private ObjectName objName;
    private MBeanFacade facade;

}
