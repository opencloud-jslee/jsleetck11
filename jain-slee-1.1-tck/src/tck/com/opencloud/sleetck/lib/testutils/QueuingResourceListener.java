/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testutils;

import com.opencloud.sleetck.lib.SleeTCKTestUtils;
import com.opencloud.sleetck.lib.TCKTestResult;
import com.opencloud.sleetck.lib.TCKTestFailureException;
import com.opencloud.sleetck.lib.TCKTestErrorException;
import com.opencloud.sleetck.lib.OperationTimedOutException;
import com.opencloud.sleetck.lib.testutils.BaseTCKResourceListener;
import com.opencloud.sleetck.lib.resource.TCKSbbMessage;
import com.opencloud.sleetck.lib.resource.TCKActivityID;

import java.rmi.RemoteException;
import java.util.LinkedList;

/**
 * An implementation of the TCKResourceListener interface which
 * privides a queue, and a blocking interface for querying the queue.
 */
public class QueuingResourceListener extends BaseTCKResourceListener {

    public QueuingResourceListener(SleeTCKTestUtils utils) {
        this.utils = utils;
    }

    /**
     * Blocks until a message or Exception is found in the queue,
     * then returns the message or throws an Exception if an Exception
     * is found.
     * @return the next TCKSbbMessage in the queue
     * @throws TCKTestFailureException if the next item is a TCKTestFailureException
     * @throws TCKTestErrorException if the next item in the queue is an Exception other
     *  than a TCKTestFailureException
     * @throws OperationTimedOutException if the test timeout was reached while waiting
     *  for an acceptable message
     */
    public TCKSbbMessage nextMessage() throws TCKTestErrorException, TCKTestFailureException {
        return nextMessage(null);
    }

    /**
     * Blocks until a message or Exception is found in the queue,
     * then returns the message or throws an Exception if an Exception
     * is found. The optional filter is used to filter messages during dequeuing.
     * @param filter an optional message filter
     * @return the next TCKSbbMessage in the queue
     * @throws TCKTestFailureException if the next item is a TCKTestFailureException
     * @throws TCKTestErrorException if the next item in the queue is an Exception other
     *  than a TCKTestFailureException
     * @throws OperationTimedOutException if the test timeout was reached while waiting
     *  for an acceptable message
     */
    public synchronized TCKSbbMessage nextMessage(TCKSbbMessageFilter filter) throws TCKTestErrorException, TCKTestFailureException {

        long now = System.currentTimeMillis();
        long timeoutAt = now + utils.getTestTimeout();

        while (now < timeoutAt) {
            while (queue.isEmpty() && now < timeoutAt) {
                try {
                    wait(timeoutAt - now);
                } catch(InterruptedException ie) { /* no-op*/ }
                now = System.currentTimeMillis();
            }

            while (!queue.isEmpty()) {
                Object head = queue.removeFirst();
                if(head instanceof Exception) {
                    if(head instanceof TCKTestFailureException) {
                        throw (TCKTestFailureException)head;
                    }
                    if(head instanceof TCKTestErrorException) {
                        throw (TCKTestErrorException)head;
                    }
                    throw new TCKTestErrorException("Exception received from resource or Sbb",(Exception)head);
                }
                TCKSbbMessage message = (TCKSbbMessage)head;
                if(filter == null || filter.accept(message)) return message;
            }
        }
        throw new OperationTimedOutException("Timed out waiting for a message or Exception");
    }

    // TCKResourceListener methods

    /**
     * Adds the given message to the queue
     */
    public synchronized void onSbbMessage(TCKSbbMessage message, TCKActivityID calledActivity) throws RemoteException {
        utils.getLog().info("Received message from SBB");
        queue.addLast(message);
        notify();
    }

    /**
     * Adds the given Exception to the queue
     */
    public synchronized void onException(Exception exception) throws RemoteException {
        utils.getLog().info("Received Exception from SBB or resource.");
        queue.addLast(exception);
        notify();
    }

    private final SleeTCKTestUtils utils;
    private final LinkedList queue = new LinkedList();

}
