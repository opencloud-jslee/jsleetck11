/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testutils.statetracking;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.Iterator;
import java.util.Vector;
import java.io.PrintWriter;
import com.opencloud.sleetck.lib.OperationTimedOutException;
import com.opencloud.logging.Logable;

/**
 * Helper utility to help test state machines.
 *
 * Usage:
 * 1. Create a StateMachineTrackerUtil with an array of
 *    state IDs.
 * 2. Build up the state machine using addTransition().
 * 3. Transition the state using shiftState().
 * 4. Audit results using getIllegalTransistions() etc.
 */
public class StateMachineTrackerUtil {

    public StateMachineTrackerUtil(int[] allStates, Logable log) {
        this.log=log;
        // copy and sort the states array
        states = new int[allStates.length];
        System.arraycopy(allStates,0,states,0,allStates.length);
        Arrays.sort(states);
        graph = new DirectionalGraph(states);

        illegalMoves = new LinkedList();
        allMoves = new LinkedList();
        isWaitingForDestination = false;
        stateUnknown = true;
    }

    /**
     * Adds the given transition to the state machine.
     */
    public void addTransition(int fromState, int toState) {
        graph.addEdge(fromState,toState);
    }

    public void shiftState(int newState) {
        synchronized (allMoves) {
            if(isRecording) {
                Transition trans = new Transition(currentState,newState,stateUnknown);
                allMoves.addLast(trans);
                if(!stateUnknown) {
                    boolean isValid = graph.isEdge(currentState,newState);
                    if(!isValid)illegalMoves.addLast(trans);
                    log.finer("StateMachineTrackerUtil: transition valid="+isValid);
                }
            }
            currentState = newState;
            stateUnknown = false;
            if(isWaitingForDestination && (currentState == destinationState))
                isWaitingForDestination = false;
            allMoves.notifyAll();
            log.finer("StateMachineTrackerUtil: Shifted state to "+currentState+
                    ". isWaitingForDestination="+isWaitingForDestination);
        }
    }

    /**
     * Called to indicate that the state of the state machine is not
     * known. After this call, the next call to shiftState() will
     * not be considered an illegal move.
     */
    public void stateUnknown() {
        synchronized (allMoves) {
            stateUnknown = true;
        }
    }

    public void startRecording() {
        isRecording = true;
    }

    public void stopRecording() {
        isRecording = false;
    }

    public boolean isRecording() {
        return isRecording;
    }

    /**
     * Note: if the given state is the same as the current state,
     * the destination is reached - waitForDestination will return immediately.
     */
    public void setDestination(int destinationState) {
        synchronized (allMoves) {
            this.destinationState = destinationState;
            isWaitingForDestination = (currentState != destinationState);
            if(isWaitingForDestination) log.finer("started waiting for destination state: "+destinationState+". current state:"+currentState);
            else log.finer("set destination to current state: "+currentState+" (not waiting)");
        }
    }

    public void waitForDestination() throws OperationTimedOutException {
        waitForDestination(0);
    }

    /**
     * Waits for the destination state if one is set, and throws
     * an OperationTimedOutException if the state is not reached within
     * the sprecified timeout. A timeout argument of 0 is interpreted as no timeout.
     * If the tracker is not waiting for a destination state, this
     * method returns immediately and silently.
     */
    public void waitForDestination(int timeout) throws OperationTimedOutException {
        long target = 0L;
        if(timeout > 0) target = System.currentTimeMillis() + timeout;
        synchronized (allMoves) {
            while(isWaitingForDestination) {
                log.finest("StateMachineTrackerUtil: still waiting for destination");
                try {
                    if(timeout > 0) {
                        long now = System.currentTimeMillis();
                        if(now >= target) throw new OperationTimedOutException("Timed out waiting for destination state.");
                        allMoves.wait(target - now);
                    } else {
                        allMoves.wait();
                    }
                } catch(InterruptedException ie) {
                    // ignore
                }
            }
            log.finer("StateMachineTrackerUtil: destination reached (or not set)");
        }
    }

    /**
     * Returns an Iterator of Transitions
     */
    public Iterator getTransitions() {
        synchronized (allMoves) {
            Vector allMovesCopy = new Vector(allMoves);
            return allMovesCopy.iterator();
        }
    }

    /**
     * Returns an Iterator of illegal Transitions
     */
    public Iterator getIllegalTransitions() {
        synchronized (allMoves) {
            Vector illegalMovesCopy = new Vector(illegalMoves);
            return illegalMovesCopy.iterator();
        }
    }

    public void clearTransitions() {
        synchronized (allMoves) {
            allMoves.clear();
            illegalMoves.clear();
        }
    }

    // -- Private state -- //

    private DirectionalGraph graph;
    private int[] states;
    private volatile boolean isRecording;
    private LinkedList allMoves;
    private LinkedList illegalMoves;
    private int currentState;
    private int destinationState;
    private boolean isWaitingForDestination;
    private boolean stateUnknown;
    private Logable log;

    // -- Inner Classes -- //

    public static class Transition {

        public Transition(int from, int to, boolean previousStateUnknown) {
            this.from = from;
            this.to = to;
            this.previousStateUnknown = previousStateUnknown;
        }

        public int getFromState() {
            return from;
        }

        public int getToState() {
            return to;
        }

        public boolean isPreviousStateUnknown() {
            return previousStateUnknown;
        }

        private int from;
        private int to;
        private boolean previousStateUnknown;

    }

}
