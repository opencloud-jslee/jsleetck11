/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testutils;

import com.opencloud.sleetck.lib.OperationTimedOutException;
import com.opencloud.sleetck.lib.SleeTCKTestUtils;

import javax.management.Notification;
import javax.management.NotificationListener;
import java.util.LinkedList;

/**
 * An implementation of the NotificationListener interface which
 * privides a queue, and a blocking interface for querying the queue.
 */
public class QueuingNotificationListener implements NotificationListener {

    public QueuingNotificationListener(SleeTCKTestUtils utils) {
        this.utils = utils;
    }

    /**
     * Blocks until a Notification is found in the queue,
     * then returns the Notification or throws an Exception if the test timeout is reached
     * @return the next Notification in the queue
     * @throws OperationTimedOutException if the test timeout was reached while waiting
     *  for an acceptable Notification
     */
    public synchronized Notification nextNotification() throws OperationTimedOutException {

        long now = System.currentTimeMillis();
        long timeoutAt = now + utils.getTestTimeout();

        while (now < timeoutAt) {
            while (queue.isEmpty() && now < timeoutAt) {
                try {
                    wait(timeoutAt - now);
                } catch(InterruptedException ie) { /* no-op*/ }
                now = System.currentTimeMillis();
            }

            while (!queue.isEmpty()) {
                return (Notification)queue.removeFirst();
            }
        }
        throw new OperationTimedOutException("Timed out waiting for a Notification");
    }

    // NotificationListener method

    /**
     * Adds the given Notification to the queue
     */
    public synchronized final void handleNotification(Notification notification, Object o) {
        utils.getLog().info("Received Notification");
        queue.addLast(notification);
        notify();
    }

    // Private state

    private final SleeTCKTestUtils utils;
    private final LinkedList queue = new LinkedList();

}
