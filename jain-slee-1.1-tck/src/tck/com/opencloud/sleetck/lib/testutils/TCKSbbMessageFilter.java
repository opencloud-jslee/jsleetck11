/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testutils;

import com.opencloud.sleetck.lib.TCKTestErrorException;
import com.opencloud.sleetck.lib.TCKTestFailureException;
import com.opencloud.sleetck.lib.resource.TCKSbbMessage;

/**
 * Used by clients of the QueuingResourceListener to filter messages
 * during calls to nextMessage().
 */
public interface TCKSbbMessageFilter {

    /**
     * Returns whether or not to return the message to the QueuingResourceListener client.
     * If this method returns true, the message will be returned.
     * If this method returns false, the message will not be returned: the next acceptable
     * message will returned or nextMessage() will continue to block until an acceptable message
     * is received.
     * The implemetor may throw a TCKTestErrorException or TCKTestFailureException to
     * indicate a test error or failure - the Exception will be propagated to the caller
     * of nextMessage().
     */
    public boolean accept(TCKSbbMessage message) throws TCKTestErrorException, TCKTestFailureException;

}