/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testutils.jmx.impl;

/**
 * Contains empty arrays used as arguments to MBean methods
 * which take no arguments.
 */
public interface EmptyArrays {

    public static final Object[] EMPTY_OBJECT_ARRAY = {};
    public static final String[] EMPTY_STRING_ARRAY = {};

}