/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testutils.jmx.impl;

import com.opencloud.sleetck.lib.testutils.jmx.*;

import javax.management.ObjectName;

public class MBeanProxyFactoryImpl implements MBeanProxyFactory {

    public MBeanProxyFactoryImpl(MBeanFacade mBeanFacade) {
        this.mBeanFacade=mBeanFacade;
    }

    public AlarmMBeanProxy createAlarmMBeanProxy(ObjectName objName) {
        return new AlarmMBeanProxyImpl(objName,mBeanFacade);
    }

    public DeploymentMBeanProxy createDeploymentMBeanProxy(ObjectName objName) {
        return new DeploymentMBeanProxyImpl(objName,mBeanFacade);
    }

    public ProfileMBeanProxy createProfileMBeanProxy(ObjectName objName) {
        return new ProfileMBeanProxyImpl(objName, mBeanFacade);
    }

    public ProfileProvisioningMBeanProxy createProfileProvisioningMBeanProxy(ObjectName objName) {
        return new ProfileProvisioningMBeanProxyImpl(objName,mBeanFacade);
    }

    public ServiceManagementMBeanProxy createServiceManagementMBeanProxy(ObjectName objName) {
        return new ServiceManagementMBeanProxyImpl(objName,mBeanFacade);
    }

    public ServiceUsageMBeanProxy createServiceUsageMBeanProxy(ObjectName objName) {
        return new ServiceUsageMBeanProxyImpl(objName,mBeanFacade);
    }

    public SleeManagementMBeanProxy createSleeManagementMBeanProxy(ObjectName objName) {
        return new SleeManagementMBeanProxyImpl(objName,mBeanFacade);
    }

    public TraceMBeanProxy createTraceMBeanProxy(ObjectName objName) {
        return new TraceMBeanProxyImpl(objName,mBeanFacade);
    }

    public SbbUsageMBeanProxy createSbbUsageMBeanProxy(ObjectName objName) {
        return new SbbUsageMBeanProxyImpl(objName,mBeanFacade);
    }

    public ProfileTableUsageMBeanProxy createProfileTableUsageMBeanProxy(ObjectName profileTableUsageMBeanName) {
        return new ProfileTableUsageMBeanProxyImpl(profileTableUsageMBeanName, mBeanFacade);
    }

    public UsageMBeanProxy createUsageMBeanProxy(ObjectName objName) {
        return new UsageMBeanProxyImpl(objName, mBeanFacade);
    }

    public ResourceUsageMBeanProxy createResourceUsageMBeanProxy(ObjectName objName) {
        return new ResourceUsageMBeanProxyImpl(objName, mBeanFacade);
    }

    public UsageNotificationManagerMBeanProxy createUsageNotificationManagerMBeanProxy(ObjectName objName) {
        return new UsageNotificationManagerMBeanProxyImpl(objName, mBeanFacade);
    }

    public ResourceManagementMBeanProxy createResourceManagementMBeanProxy(ObjectName objName) {
        return new ResourceManagementMBeanProxyImpl(objName, mBeanFacade);
    }
    
    private MBeanFacade mBeanFacade;


}
