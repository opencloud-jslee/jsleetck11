/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testutils.statetracking;

import java.util.Arrays;

/**
 * Data structure to represent a directional graph.
 */
public class DirectionalGraph {

    /**
     * @param sortedNodes a sorted set of nodes
     */
    public DirectionalGraph(int[] sortedNodes) {
        nodes = sortedNodes;
        graph = new boolean[nodes.length][nodes.length];
    }

    /**
     * Returns whether an edge exists directly from
     * the first node to the second.
     * @throws ArrayIndexOutOfBoundsException if either argument
     *    was not in the original nodes set.
     */
    public boolean isEdge(int from, int to) {
        int fromIndex = indexOf(from);
        int toIndex = indexOf(to);
        if(fromIndex < 0 || toIndex < 0) return false;
        return graph[fromIndex][toIndex];
    }

    /**
     * Adds the given egde to the graph.
     * @throws ArrayIndexOutOfBoundsException if either argument
     *    was not in the original nodes set.
     */
    public void addEdge(int from, int to) {
        int fromIndex = indexOf(from);
        int toIndex = indexOf(to);
        graph[fromIndex][toIndex] = true;
    }

    /**
     * Removes the given egde from the graph.
     * @throws ArrayIndexOutOfBoundsException if either argument
     *    was not in the original nodes set.
     */
    public void removeEdge(int from, int to) {
        int fromIndex = indexOf(from);
        int toIndex = indexOf(to);
        graph[fromIndex][toIndex] = false;
    }


    /**
     * Returns the row/column index of the given node.
     */
    private int indexOf(int node) {
        return Arrays.binarySearch(nodes,node);
    }

    private int[] nodes;
    private boolean[][] graph;

}