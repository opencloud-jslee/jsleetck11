/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testutils;

import java.util.Arrays;
import java.util.Vector;

import com.opencloud.logging.Logable;

public class FutureAssertionsResult extends FutureResult {
    private Vector assertionIDs;

    public FutureAssertionsResult (int[] assertionIDs, Logable log) {
        super(log);

        Integer[] ids = new Integer[assertionIDs.length];
        for (int i=0; i<assertionIDs.length; i++)
            ids[i] = new Integer(assertionIDs[i]);

        this.assertionIDs = new Vector(Arrays.asList(ids));
    }

    public FutureAssertionsResult (Logable log) {
        this(new int[]{}, log);
    }

    public void setPassed(int assertionID) {
        assertionIDs.remove(new Integer(assertionID));

        if (assertionIDs.size()==0)
            this.setPassed();
    }
}
