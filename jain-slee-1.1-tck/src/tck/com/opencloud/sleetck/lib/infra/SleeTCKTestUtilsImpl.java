/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.infra;

import com.opencloud.sleetck.lib.SleeTCKTestUtils;
import com.opencloud.sleetck.lib.TCKTestErrorException;
import com.opencloud.sleetck.lib.OperationTimedOutException;
import com.opencloud.sleetck.lib.EnvironmentKeys;
import com.opencloud.sleetck.lib.testutils.jmx.MBeanFacade;
import com.opencloud.sleetck.lib.rautils.MessageHandlerRegistry;
import com.opencloud.sleetck.lib.rautils.RMIObjectChannel;
import com.opencloud.sleetck.lib.resource.testapi.TCKResourceTestInterface;
import com.opencloud.sleetck.lib.testutils.jmx.AlarmMBeanProxy;
import com.opencloud.sleetck.lib.testutils.jmx.MBeanProxyFactory;
import com.opencloud.sleetck.lib.testutils.jmx.ResourceManagementMBeanProxy;
import com.opencloud.sleetck.lib.testutils.jmx.ResourceUsageMBeanProxy;
import com.opencloud.sleetck.lib.testutils.jmx.SleeManagementMBeanProxy;
import com.opencloud.sleetck.lib.testutils.jmx.DeploymentMBeanProxy;
import com.opencloud.sleetck.lib.testutils.jmx.ServiceManagementMBeanProxy;
import com.opencloud.sleetck.lib.testutils.jmx.TraceMBeanProxy;

import java.util.Properties;
import java.util.Set;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.Iterator;
import com.opencloud.logging.Logable;
import com.opencloud.util.SleepUtil;
import javax.management.ObjectName;
import javax.slee.ServiceID;
import javax.slee.SbbID;
import javax.slee.ComponentID;
import javax.slee.UnrecognizedServiceException;
import javax.slee.InvalidStateException;
import javax.slee.management.DeployableUnitID;
import javax.slee.management.DeployableUnitDescriptor;
import javax.slee.management.ResourceAdaptorEntityState;
import javax.slee.management.ServiceDescriptor;
import javax.slee.management.SbbDescriptor;
import javax.slee.management.ServiceState;
import javax.slee.management.ManagementException;
import javax.slee.resource.ResourceAdaptorID;
import javax.slee.facilities.Level;

public class SleeTCKTestUtilsImpl implements SleeTCKTestUtils {

    public SleeTCKTestUtilsImpl(
                Properties testParams, Properties envParams,
                MBeanFacade facade, MBeanProxyFactory mBeanProxyFactory,
                TCKResourceTestInterface resourceInterface,
                RMIObjectChannel rmiObjectChannel,
                MessageHandlerRegistry messageHandlerRegistry,
                Logable log,ObjectName mgmtMBeanName,
                String baseDeployableUnitsURL, int defaultTimeout, int testTimeout) {
        this.testParams=testParams;
        this.envParams=envParams;
        this.facade=facade;
        this.mBeanProxyFactory=mBeanProxyFactory;
        this.resourceInterface=resourceInterface;
        this.rmiObjectChannel=rmiObjectChannel;
        this.messageHandlerRegistry=messageHandlerRegistry;
        this.log=log;
        this.mgmtMBeanName=mgmtMBeanName;
        this.baseDeployableUnitsURL=baseDeployableUnitsURL;
        this.defaultTimeout=defaultTimeout;
        this.testTimeout=testTimeout;
        deployableUnitIDs = new LinkedList();
        activatedServiceIDs = new HashSet();
    }

    // -- Implementation of SleeTCKTestUtils -- //

    public Properties getTestParams() {
        return testParams;
    }

    public Properties getEnvParams() {
        return envParams;
    }

    public int getDefaultTimeout() {
        return defaultTimeout;
    }

    public int getTestTimeout() {
        return testTimeout;
    }

    public Logable getLog() {
        return log;
    }

    public MBeanFacade getMBeanFacade() {
        return facade;
    }

    public MBeanProxyFactory getMBeanProxyFactory() {
        return mBeanProxyFactory;
    }

    public ObjectName getSleeManagementMBeanName() {
        return mgmtMBeanName;
    }

    public SleeManagementMBeanProxy getSleeManagementMBeanProxy() throws TCKTestErrorException {
        if(sleeManagementMBeanProxy == null) {
            sleeManagementMBeanProxy =
                mBeanProxyFactory.createSleeManagementMBeanProxy(mgmtMBeanName);
        }
        return sleeManagementMBeanProxy;
    }

    public String getDeploymentUnitURL(String relativeURL) {
        if(baseDeployableUnitsURL == null) throw new NullPointerException("baseDeployableUnitsURL must be set");
        if(relativeURL == null) throw new NullPointerException("relativeURL must be non-null");
        StringBuffer rBuf = new StringBuffer(baseDeployableUnitsURL);
        if(!baseDeployableUnitsURL.endsWith("/") && !relativeURL.startsWith("/"))rBuf.append("/");
        return rBuf.append(relativeURL).toString();
    }

    public DeploymentMBeanProxy getDeploymentMBeanProxy() throws TCKTestErrorException {
        if(deploymentMBeanProxy == null) {
            ObjectName deploymentMBeanName = getSleeManagementMBeanProxy().getDeploymentMBean();
            deploymentMBeanProxy = mBeanProxyFactory.createDeploymentMBeanProxy(deploymentMBeanName);
        }
        return deploymentMBeanProxy;
    }

    public DeployableUnitID install(String relativePath) throws TCKTestErrorException {
        try {
            synchronized (deployableUnitIDs) {
                String fullURL = getDeploymentUnitURL(relativePath);
                log.fine("Installing deployable unit "+fullURL);
                DeployableUnitID duID = getDeploymentMBeanProxy().install(fullURL);
                deployableUnitIDs.addFirst(duID); // add to the front of the list (most recent is first)
                return duID;
            }
        } catch(Exception e) { throw new TCKTestErrorException("Caught Exception while trying to install component",e); }
    }

    public boolean uninstallAll() throws TCKTestErrorException {
        boolean uninstalledAny = false;
        TCKTestErrorException caught = null;
        synchronized (deployableUnitIDs) {
            log.fine("Uninstalling all deployable units installed via SleeTCKTestUtils...");
            Iterator deployableUnitIDsIter = deployableUnitIDs.iterator();
            while(deployableUnitIDsIter.hasNext()) {
                DeployableUnitID duID = (DeployableUnitID)deployableUnitIDsIter.next();
                try {
                    if(deploymentMBeanProxy.isInstalled(duID)) {
                        log.finer("Uninstalling deployable unit "+duID);
                        try {
                            ensureServicesStopped(duID);
                        } catch(Exception e) {
                            log.warning(e);
                            caught = new TCKTestErrorException("Caught Exception while trying to ensure services are stopped",e);
                        }
                        deploymentMBeanProxy.uninstall(duID);
                        uninstalledAny = true;
                    }
                } catch(Exception e) {
                    log.warning(e);
                    caught = new TCKTestErrorException("Caught Exception while trying to uninstall component",e);
                }
                deployableUnitIDsIter.remove();
            }
        }
        if(caught != null) throw caught; // throw the last caught exception (the others were logged)
        return uninstalledAny;
    }

    public ServiceID[] activateServices(DeployableUnitID duID, boolean setTraceLevel) throws TCKTestErrorException {
        try {
            // set the trace level to set only if setTraceLevel is true
            // and the sbb trace level is set to a non-null value other than Level.LEVEL_OFF
            Level traceLevel = null;
            if(setTraceLevel) {
                String traceLevelString = envParams.getProperty(EnvironmentKeys.SBB_TRACE_LEVEL);
                if(traceLevelString != null) {
                    int traveLevelInt = Integer.parseInt(traceLevelString);
                    if(traveLevelInt != Level.LEVEL_OFF) traceLevel = Level.fromInt(traveLevelInt);
                }
            }
            // activate the services
            log.fine("Activating services for deployable unit "+duID);
            ServiceID[] serviceIDs = getServices(duID);
            synchronized (activatedServiceIDs) {
                for (int i = 0; i < serviceIDs.length; i++) {
                    log.finer("Activating service "+serviceIDs[i]);
                    // activate the service
                    getServiceManagementMBeanProxy().activate(serviceIDs[i]);
                    activatedServiceIDs.add(serviceIDs[i]);
                    // set the trace level if specified
                    if(traceLevel != null) {
                        ServiceDescriptor serviceDescriptor = (ServiceDescriptor)getDeploymentMBeanProxy().getDescriptor(serviceIDs[i]);
                        SbbID rootSbbID = serviceDescriptor.getRootSbb();
                        setTraceLevelForSbbTree(rootSbbID,traceLevel);
                    }
                }
            }
            return serviceIDs;
        } catch(Exception e) {
            if(e instanceof TCKTestErrorException) throw (TCKTestErrorException)e;
            throw new TCKTestErrorException("Caught Exception while trying to activate service",e);
        }
    }

    public boolean deactivateAllServices() throws TCKTestErrorException {
        boolean deactivatedAny = false;
        TCKTestErrorException caught = null;
        ServiceManagementMBeanProxy serviceMgmtMBeanProxy = getServiceManagementMBeanProxy();
        synchronized (activatedServiceIDs) {
            log.finer("Deactivating all services activated via SleeTCKTestUtils...");
            Iterator activatedServiceIDsIter = activatedServiceIDs.iterator();
            while(activatedServiceIDsIter.hasNext()) {
                ServiceID serviceID = (ServiceID)activatedServiceIDsIter.next();
                log.finer("deactivateAllServices: activatedServiceIDs.next = " +serviceID.toString());
                try {
                    javax.slee.management.ServiceState state = serviceMgmtMBeanProxy.getState(serviceID);
                    log.finer("deactivateAllServices: Current state for service is: "+state.toString());
                    if(serviceMgmtMBeanProxy.getState(serviceID).isActive()) {
                        log.finer("Deactivating service "+serviceID);
                        deactivateService(serviceID);
                        deactivatedAny = true;
                    }
                } catch(Exception e) {
                    log.warning(e);
                    caught = new TCKTestErrorException("Caught Exception while trying to deactivate service",e);
                }
                activatedServiceIDsIter.remove();
            }
            log.fine("deactivateAllServices: activatedServiceIDs.next complete");
        }
        if(caught != null) throw caught; // throw the last caught exception (the others were logged)
        return deactivatedAny;
    }

    // Removes all RA Entities created by TCK Tests.
    public void removeRAEntities() throws TCKTestErrorException {
        // If we errored before the MBean was initialised (e.g. during
        // deployment), then we can't use the MBean to undeploy.
        if (resourceManagementMBeanProxy == null)
            return;

        try {
            long now = System.currentTimeMillis();
            long timeoutAt = now + getTestTimeout();
            while (System.currentTimeMillis() < timeoutAt) {
                boolean done = true;
                String[] entities = resourceManagementMBeanProxy.getResourceAdaptorEntities();
                for (int i = 0; i < entities.length; i++) {
                    String raEntity = entities[i];
                    ResourceAdaptorID raID = resourceManagementMBeanProxy.getResourceAdaptor(raEntity);
                    ResourceAdaptorEntityState state = resourceManagementMBeanProxy.getState(raEntity);
                    // Don't remove the TCK RA
                    if (raID.getName().equals("TCK_Resource_Adaptor") && raID.getVendor().equals(SleeTCKComponentConstants.TCK_VENDOR))
                        continue;

                    // Ignore non-TCK RAs
                    if (!raID.getVendor().equals(SleeTCKComponentConstants.TCK_VENDOR))
                        continue;
                    if (state.isActive()) {
                        log.finer("Deactivating RA Entity: " + raEntity);
                        resourceManagementMBeanProxy.deactivateResourceAdaptorEntity(raEntity);
                        done = false;
                    } else if (state.isStopping()) {
                        log.finer("Still waiting for RA Entity to deactivate: " + raEntity);
                        done = false;
                    } else if (state.isInactive()) {
                        String[] linkNames = resourceManagementMBeanProxy.getLinkNames(raEntity);
                        for (int j = 0; j < linkNames.length; j++) {
                            String linkName = linkNames[j];
                            log.finer("Unbinding link name: " + linkName);
                            resourceManagementMBeanProxy.unbindLinkName(linkName);
                        }
                        log.finer("Removing RA Entity: " + raEntity);
                        resourceManagementMBeanProxy.removeResourceAdaptorEntity(raEntity);
                    }
                }
                if (done) {
                    log.finer("RA entities removed");
                    return;
                }
                Thread.sleep(WAIT_FOR_COMPONENT_PAUSE);
            }
        } catch (Exception e) {
            throw new TCKTestErrorException("Failed to deactivate and remove one or more RA Entities", e);
        }
        throw new TCKTestErrorException("Failed to deactivate and remove one or more RA Entities before test timeout");
    }

    public ResourceUsageMBeanProxy getResourceUsageMBeanProxy(String entityName) throws TCKTestErrorException {
        // Note: Don't bother to cache this as it's entity-specific anyway.
        try {
            return mBeanProxyFactory.createResourceUsageMBeanProxy(getResourceManagementMBeanProxy().getResourceUsageMBean(entityName));
        } catch (Exception e) {
            throw new TCKTestErrorException("An error occured attempting to create a UsageMBeanProxy for Resource Adaptor Entity: " + entityName, e);
        }
    }

    public ResourceManagementMBeanProxy getResourceManagementMBeanProxy() throws TCKTestErrorException {
        if(resourceManagementMBeanProxy == null) {
            resourceManagementMBeanProxy =
                mBeanProxyFactory.createResourceManagementMBeanProxy(getSleeManagementMBeanProxy().getResourceManagementMBean());
        }
        return resourceManagementMBeanProxy;
    }

    public ServiceManagementMBeanProxy getServiceManagementMBeanProxy() throws TCKTestErrorException {
        if(serviceManagementMBeanProxy == null) {
            serviceManagementMBeanProxy =
                mBeanProxyFactory.createServiceManagementMBeanProxy(getSleeManagementMBeanProxy().getServiceManagementMBean());
        }
        return serviceManagementMBeanProxy;
    }

    public TraceMBeanProxy getTraceMBeanProxy() throws TCKTestErrorException {
        if(traceMBeanProxy == null) {
            traceMBeanProxy = mBeanProxyFactory.createTraceMBeanProxy(getSleeManagementMBeanProxy().getTraceMBean());
        }
        return traceMBeanProxy;
    }

    public AlarmMBeanProxy getAlarmMBeanProxy() throws TCKTestErrorException {
        if(alarmMBeanProxy == null) {
            alarmMBeanProxy = mBeanProxyFactory.createAlarmMBeanProxy(getSleeManagementMBeanProxy().getAlarmMBean());
        }
        return alarmMBeanProxy;
    }

    public TCKResourceTestInterface getResourceInterface() {
        return resourceInterface;
    }

    public RMIObjectChannel getRMIObjectChannel() {
        return rmiObjectChannel;
    }

    public MessageHandlerRegistry getMessageHandlerRegistry() {
        return messageHandlerRegistry;
    }

    // -- Private methods -- //

    /**
     * Ensures that all services contained in the given deployable unit are in the INACTIVE state,
     * or gives up and throws a TCKTestErrorException
     * @throws TCKTestErrorException if a ManagementException is thrown by the ServiceManagementMBean
     * @throws OperationTimedOutException if a service doesn't stop within the timeout
     */
    private void ensureServicesStopped(DeployableUnitID duID) throws TCKTestErrorException {
        ServiceID[] serviceIDs = getServices(duID);
        for (int i = 0; i < serviceIDs.length; i++) {
            ServiceID serviceID = serviceIDs[i];
            if(serviceID != null) {
                try {
                    ServiceManagementMBeanProxy serviceManagementMBeanProxy = getServiceManagementMBeanProxy();
                    ServiceState serviceState = serviceManagementMBeanProxy.getState(serviceID);
                    if(serviceState.isActive()) {
                        log.warning("A service to be uninstalled was not deactivated by the test - "+
                                    "services should be explicitly deactivated before uninstallation. serviceID="+serviceID);
                        try {
                            serviceManagementMBeanProxy.deactivate(serviceID);
                        } catch(InvalidStateException ise) {
                            log.warning("A service was just deactivated by another thread (detected during ensureServicesStopped())"+
                                        "This indicates a race condition in the test. serviceID="+serviceID);
                        }
                        serviceState = serviceManagementMBeanProxy.getState(serviceID);
                    }
                    if(!serviceState.isInactive()) {
                        long now = System.currentTimeMillis();
                        // use the test timeout rather than the default timeout, as tests which require long timeouts
                        // may require long timeouts for cleanup
                        long timeoutAt = now + getTestTimeout();
                        while(now < timeoutAt && !serviceState.isInactive()) {
                            SleepUtil.sleepFor(Math.min(timeoutAt - now, WAIT_FOR_COMPONENT_PAUSE));
                            serviceState = serviceManagementMBeanProxy.getState(serviceID);
                            now = System.currentTimeMillis();
                        }
                        if(!serviceState.isInactive()) {
                            // the timeout was reached
                            throw new OperationTimedOutException("The timeout of "+getTestTimeout()+
                                " ms  was reached while waiting for service to stop. serviceID="+serviceID+" serviceState="+serviceState);
                        }
                        log.finest("Service stopped successfully (during preparation to uninstall). serviceID="+serviceID);
                    }
                } catch(UnrecognizedServiceException use) {
                    log.warning("unrecognized service ID found for deployable unit. serviceID="+serviceID+". deployable unit="+duID);
                    log.warning(use);
                } catch(ManagementException mge) {
                    throw new TCKTestErrorException("ManagementException caught while trying to ensure service is stopped. serviceID="+serviceID,mge);
                }
            } else log.warning("null service ID encountered for deployable unit. deployable unit ID: "+duID);
        }
    }

    /**
     * Returns the ServiceIDs of all services contained in the given deployable unit.
     */
    private ServiceID[] getServices(DeployableUnitID duID) throws TCKTestErrorException {
        try {
            DeployableUnitDescriptor duDes = getDeploymentMBeanProxy().getDescriptor(duID);
            ComponentID[] componentIDs = duDes.getComponents();
            ServiceID[] serviceIDs = new ServiceID[componentIDs.length];
            int serviceCount = 0;
            for (int i = 0; i < componentIDs.length; i++) {
                if(componentIDs[i] instanceof ServiceID) serviceIDs[serviceCount++] = (ServiceID)componentIDs[i];
            }
            if(serviceCount < serviceIDs.length) {
                ServiceID[] copyTo = new ServiceID[serviceCount];
                System.arraycopy(serviceIDs,0,copyTo,0,serviceCount);
                serviceIDs = copyTo;
            }
            return serviceIDs;
        } catch(Exception e) {
            throw new TCKTestErrorException("Caught Exception while trying to get ServiceIDs from DeployableUnitID "+duID,e);
        }
    }

    /**
     * Sets the trace level for the given SBB and all its descendants recursively.
     */
    private void setTraceLevelForSbbTree(SbbID rootSbbID, Level traceLevel) throws TCKTestErrorException {
        HashSet s = new HashSet();
        s.add(rootSbbID);
        setTraceLevelForSbbTree(rootSbbID, traceLevel, s);
    }

    private void setTraceLevelForSbbTree(SbbID rootSbbID, Level traceLevel, HashSet visited) throws TCKTestErrorException {
        try {
            log.finest("Setting trace level to "+traceLevel+" for Sbb and all referenced Sbbs. SbbID="+rootSbbID);
            SbbDescriptor sbbDescriptor = (SbbDescriptor)getDeploymentMBeanProxy().getDescriptor(rootSbbID);
            // set the trace level for the given SBB
            getTraceMBeanProxy().setTraceLevel(rootSbbID,traceLevel);
            // recursively set the trace level for all the SBB's descendants
            SbbID[] sbbIDs = sbbDescriptor.getSbbs();
            if(sbbIDs != null) {
                for (int i = 0; i < sbbIDs.length; i++) {
                    // only visit sbbs if not already visited
                    if (visited.add(sbbIDs[i])) setTraceLevelForSbbTree(sbbIDs[i],traceLevel,visited);
                }
            }
        } catch (Exception e) {
            if(e instanceof TCKTestErrorException) throw (TCKTestErrorException)e;
            throw new TCKTestErrorException("Caught Exception while trying to set trace level for SBB. sbbID="+rootSbbID,e);
        }
    }

    public void deactivateResourceAdaptorEntity(String raEntity) throws TCKTestErrorException {
        ResourceManagementMBeanProxy resourceMBean = getResourceManagementMBeanProxy();
        long now = System.currentTimeMillis();
        long timeout = now + getTestTimeout();
        boolean done = false;
        while (System.currentTimeMillis() < timeout) {
            try {
                ResourceAdaptorEntityState state = resourceMBean.getState(raEntity);

                if (state.isActive()) {
                    resourceMBean.deactivateResourceAdaptorEntity(raEntity);
                } else if (state.isInactive()) {
                    done = true;
                    break;
                }
                Thread.sleep(500);
            } catch (Exception e) {
                throw new TCKTestErrorException("An exception occured while deactivating " + raEntity + ": ", e);
            }
        }
        if (!done)
            throw new TCKTestErrorException(raEntity + " did not deactivate within test timeout period");
    }

    public void deactivateService(ServiceID serviceID) throws TCKTestErrorException {
        ServiceManagementMBeanProxy serviceMBean = getServiceManagementMBeanProxy();
        long now = System.currentTimeMillis();
        long timeout = now + getTestTimeout();
        boolean done = false;
        ServiceState state;

        try {
            state = serviceMBean.getState(serviceID);
            log.finer("deactivateService: Current state for service is: "+state.toString());
            if (state.isActive()) {
                serviceMBean.deactivate(serviceID);
                state = serviceMBean.getState(serviceID);
                log.finer("deactivateService: New state for service is: "+state.toString());
            }
        } catch (Exception e) {
            throw new TCKTestErrorException("An exception occured while deactivating " + serviceID + ": ", e);
        }

        // Now check that Service becomes Inactive
        while (System.currentTimeMillis() < timeout) {
            try {
                state = serviceMBean.getState(serviceID);
                if (state.isInactive()) {
                    done = true;
                    break;
                }
                Thread.sleep(500);
            } catch (Exception e) {
                throw new TCKTestErrorException("An exception occured while obtaining state for Service: " + serviceID + ": ", e);
            }
        }
        if (!done)
            throw new TCKTestErrorException(serviceID + " did not deactivate within test timeout period");
    }

    // -- Private state -- //

    private Properties testParams;
    private Properties envParams;
    private MBeanFacade facade;
    private MBeanProxyFactory mBeanProxyFactory;
    private TCKResourceTestInterface resourceInterface;
    private RMIObjectChannel rmiObjectChannel;
    private MessageHandlerRegistry messageHandlerRegistry;
    private Logable log;
    private ObjectName mgmtMBeanName;
    private String baseDeployableUnitsURL;
    private int defaultTimeout;
    private int testTimeout;
    private SleeManagementMBeanProxy sleeManagementMBeanProxy;
    private ServiceManagementMBeanProxy serviceManagementMBeanProxy;
    private ResourceManagementMBeanProxy resourceManagementMBeanProxy;
    private TraceMBeanProxy traceMBeanProxy;
    private AlarmMBeanProxy alarmMBeanProxy;
    private DeploymentMBeanProxy deploymentMBeanProxy;
    private LinkedList deployableUnitIDs; // in reverse chronological order (most recent is first)
    private Set activatedServiceIDs;

    // how long to wait between checks that a service has stopped
    // (not important enough to be an interview parameter - hardcode it)
    private static final long WAIT_FOR_COMPONENT_PAUSE = 500;

}
