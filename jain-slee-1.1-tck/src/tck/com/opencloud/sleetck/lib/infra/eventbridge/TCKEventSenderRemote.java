/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.infra.eventbridge;

import java.rmi.Remote;
import java.rmi.RemoteException;
import java.rmi.NotBoundException;
import java.net.MalformedURLException;

/**
 * Defines the remote interface used by a 'client'
 * to register an event listener.
 */
public interface TCKEventSenderRemote extends Remote {

    /**
     * Connects the given receiver: after this call the given 
     * receiver can receive events.
     * If any receiver is already connected, it is replaced by the given receiver.
     */
    public void connectReceiver(TCKEventReceiverRemote receiver) throws RemoteException;

    /**
     * Disconnects the current receiver.
     * If no receiver is connected, this method does nothing.
     */
    public void disconnectReceiver() throws RemoteException;    
    
}