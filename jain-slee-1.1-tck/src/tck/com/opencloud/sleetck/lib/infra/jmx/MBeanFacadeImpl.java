/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.infra.jmx;

import java.util.Set;
import java.util.Map;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import javax.management.*;
import com.opencloud.sleetck.lib.testutils.jmx.MBeanFacade;
import com.opencloud.sleetck.lib.TCKCommunicationException;
import com.opencloud.sleetck.lib.TCKTestErrorException;
import com.opencloud.sleetck.lib.infra.eventbridge.TCKEventListenerRegistry;
import com.opencloud.logging.Logable;
import java.rmi.RemoteException;

/**
 * This component represents a reduced and adapted view of a
 * remote MBeanServer.
 * The functionality defined in this interface is a subset of the
 * functionality offered in javax.management.MBeanServer.
 * The connect() method must be called with a valid handle to
 * a remote agent before any of the facade methods will succeed.
 * Each method throws a TCKCommunicationException upon a
 * communication failure, and a TCKTestErrorException if a
 * SleeTCK related error occurs.
 */
public class MBeanFacadeImpl implements MBeanFacade {

    public MBeanFacadeImpl(Logable log) {
        this.log=log;
        nextNotificationListenerID = 1;
        notificationListeners = new HashMap();
    }

    // -- Setup methods -- //

    /**
     * This method must be called with a valid handle to
     * the remote agent for any of the facade methods to work.
     */
    public void setMBeanFacadeAgentRemote(MBeanFacadeAgentRemote remoteServer) {
        this.remoteServer = remoteServer;
    }

    public void setListenerRegistry(TCKEventListenerRegistry eventListenerRegistry) {
        this.eventListenerRegistry = eventListenerRegistry;
    }

    // -- Implementation of MBeanFacade -- //

    public ObjectInstance createMBean(String className, ObjectName name)
            throws ReflectionException, InstanceAlreadyExistsException, MBeanRegistrationException, MBeanException, NotCompliantMBeanException, TCKCommunicationException, TCKTestErrorException {
        assertConnected();
        try {
            return remoteServer.createMBean(className, name);
        } catch(RemoteException re) {
            throw new TCKCommunicationException("RemoteException caught while executing remote MBeanServer method",re);
        }
    }

    public ObjectInstance createMBean(String className, ObjectName name, ObjectName loaderName)
        throws ReflectionException, InstanceAlreadyExistsException, MBeanRegistrationException, MBeanException, NotCompliantMBeanException,
               InstanceNotFoundException, TCKCommunicationException, TCKTestErrorException {
        assertConnected();
        try {
            return remoteServer.createMBean(className, name, loaderName);
        } catch(RemoteException re) {
            throw new TCKCommunicationException("RemoteException caught while executing remote MBeanServer method",re);
        }
    }


    public ObjectInstance createMBean(String className, ObjectName name, Object params[], String signature[])
        throws ReflectionException, InstanceAlreadyExistsException, MBeanRegistrationException, MBeanException,
                NotCompliantMBeanException, TCKCommunicationException, TCKTestErrorException  {
        assertConnected();
        try {
            return remoteServer.createMBean(className, name, params, signature);
        } catch(RemoteException re) {
            throw new TCKCommunicationException("RemoteException caught while executing remote MBeanServer method",re);
        }
    }

    public ObjectInstance createMBean(String className, ObjectName name, ObjectName loaderName, Object params[], String signature[])
        throws ReflectionException, InstanceAlreadyExistsException, MBeanRegistrationException, MBeanException, NotCompliantMBeanException,
               InstanceNotFoundException, TCKCommunicationException, TCKTestErrorException {
        assertConnected();
        try {
            return remoteServer.createMBean(className, name, loaderName, params, signature);
        } catch(RemoteException re) {
            throw new TCKCommunicationException("RemoteException caught while executing remote MBeanServer method",re);
        }
    }

    public void unregisterMBean(ObjectName name) throws InstanceNotFoundException,
        MBeanRegistrationException, TCKCommunicationException, TCKTestErrorException {
        assertConnected();
        try {
            remoteServer.unregisterMBean(name);
        } catch(RemoteException re) {
            throw new TCKCommunicationException("RemoteException caught while executing remote MBeanServer method",re);
        }
    }

    public ObjectInstance getObjectInstance(ObjectName name)
        throws InstanceNotFoundException, TCKCommunicationException, TCKTestErrorException  {
        assertConnected();
        try {
            return remoteServer.getObjectInstance(name);
        } catch(RemoteException re) {
            throw new TCKCommunicationException("RemoteException caught while executing remote MBeanServer method",re);
        }
    }

    public Set queryMBeans(ObjectName name, QueryExp query) throws TCKCommunicationException, TCKTestErrorException  {
        assertConnected();
        try {
            return remoteServer.queryMBeans(name, query);
        } catch(RemoteException re) {
            throw new TCKCommunicationException("RemoteException caught while executing remote MBeanServer method",re);
        }
    }

    public Set queryNames(ObjectName name, QueryExp query) throws TCKCommunicationException, TCKTestErrorException {
        assertConnected();
        try {
            return remoteServer.queryNames(name,  query);
        } catch(RemoteException re) {
            throw new TCKCommunicationException("RemoteException caught while executing remote MBeanServer method",re);
        }
    }

    public boolean isRegistered(ObjectName name) throws TCKCommunicationException, TCKTestErrorException{
        assertConnected();
        try {
            return remoteServer.isRegistered(name);
        } catch(RemoteException re) {
            throw new TCKCommunicationException("RemoteException caught while executing remote MBeanServer method",re);
        }
    }


    public Integer getMBeanCount() throws TCKCommunicationException, TCKTestErrorException{
        assertConnected();
        try {
            return remoteServer.getMBeanCount();
        } catch(RemoteException re) {
            throw new TCKCommunicationException("RemoteException caught while executing remote MBeanServer method",re);
        }
    }

    public Object getAttribute(ObjectName name, String attribute) throws
    MBeanException, AttributeNotFoundException, InstanceNotFoundException,
    ReflectionException, TCKCommunicationException, TCKTestErrorException  {
        assertConnected();
        try {
            return remoteServer.getAttribute(name, attribute);
        } catch(RemoteException re) {
            throw new TCKCommunicationException("RemoteException caught while executing remote MBeanServer method",re);
        }
    }


    public AttributeList getAttributes(ObjectName name, String[] attributes)
        throws InstanceNotFoundException, ReflectionException, TCKCommunicationException, TCKTestErrorException {
        assertConnected();
        try {
            return remoteServer.getAttributes(name, attributes);
        } catch(RemoteException re) {
            throw new TCKCommunicationException("RemoteException caught while executing remote MBeanServer method",re);
        }
    }

    public void setAttribute(ObjectName name, Attribute attribute) throws
            InstanceNotFoundException, AttributeNotFoundException, InvalidAttributeValueException, MBeanException,
            ReflectionException ,TCKCommunicationException, TCKTestErrorException  {
        assertConnected();
        try {
            remoteServer.setAttribute(name, attribute);
        } catch(RemoteException re) {
            throw new TCKCommunicationException("RemoteException caught while executing remote MBeanServer method",re);
        }
    }



    public AttributeList setAttributes(ObjectName name, AttributeList attributes)
        throws InstanceNotFoundException, ReflectionException, TCKCommunicationException, TCKTestErrorException{
        assertConnected();
        try {
            return remoteServer.setAttributes(name, attributes);
        } catch(RemoteException re) {
            throw new TCKCommunicationException("RemoteException caught while executing remote MBeanServer method",re);
        }
    }

    /**
     * Invokes the given method on the given object.
     * @return the return value of the operation. If the return type
     *  of the called method is 'void', this method returns null.
     */
    public Object invoke(ObjectName name, String operationName, Object params[], String signature[])
        throws InstanceNotFoundException, MBeanException, ReflectionException,  TCKCommunicationException, TCKTestErrorException{
        assertConnected();
        try {
            log.finest("Invoking the "+operationName+" operation on "+name.toString());
            return remoteServer.invoke(name, operationName, params, signature);
        } catch(RemoteException re) {
            throw new TCKCommunicationException("RemoteException caught while executing remote MBeanServer method",re);
        }
    }

    public String getDefaultDomain() throws TCKCommunicationException, TCKTestErrorException{
        assertConnected();
        try {
            return remoteServer.getDefaultDomain();
        } catch(RemoteException re) {
            throw new TCKCommunicationException("RemoteException caught while executing remote MBeanServer method",re);
        }
    }

    /**
     * Registers a listener to receive remote notifications.
     * Note: the listener and handback objects are not serialized - the
     *  filter is.
     */
    public void addNotificationListener(ObjectName name, NotificationListener listener, NotificationFilter filter, Object handback)
            throws InstanceNotFoundException, TCKCommunicationException, TCKTestErrorException {
        assertConnected();
        if(eventListenerRegistry == null) throw new TCKTestErrorException("The event listener registry is not set.");
        // create the listener ID
        NotificationListenerID listenerID = createNotificationListenerID();
        // register the listener, object name and handback object
        NotificationListenerWrapper wrapper = new NotificationListenerWrapper(name,listener,handback);
        synchronized (notificationListeners) {
            notificationListeners.put(listenerID,wrapper);
        }
        // register ourself with the event bridge for the event channel
        eventListenerRegistry.addListener(this,listenerID);
        // register a listener on the event source machine
        try {
            remoteServer.registerNotificationListener(listenerID, name, filter);
        } catch(RemoteException re) {
            // registration failed - roll back local state - ie deregister
            eventListenerRegistry.removeListener(this,listenerID);
            synchronized (notificationListeners) {
                notificationListeners.remove(listenerID);
            }
            throw new TCKCommunicationException("RemoteException caught while executing remote MBeanServer method",re);
        }
    }

    public void addNotificationListener(ObjectName name, ObjectName listener, NotificationFilter filter, Object handback)
        throws InstanceNotFoundException, TCKCommunicationException, TCKTestErrorException{
        assertConnected();
        try {
            remoteServer.addNotificationListener(name, listener, filter, handback);
        } catch(RemoteException re) {
            throw new TCKCommunicationException("RemoteException caught while executing remote MBeanServer method",re);
        }
    }

    /**
     * Deregisters the given listener.
     * After this method returns, the listener will no longer
     * receive notifications.
     */
    public void removeNotificationListener(ObjectName name, NotificationListener listener)
        throws InstanceNotFoundException, ListenerNotFoundException, TCKCommunicationException, TCKTestErrorException{
        assertConnected();
        if(eventListenerRegistry == null) throw new TCKTestErrorException("The event listener registry is not set.");
        // synchronize on eventListenerRegistry first to avoid deadlock with the event handling thread,
        // which locks on the eventListenerRegistry before locking on notificationListeners
        synchronized(eventListenerRegistry) {
            synchronized (notificationListeners) {
                // iterate over a copy of the key set, as we will be modifying the map as we iterate
                Iterator iter = new HashSet(notificationListeners.keySet()).iterator();
                while(iter.hasNext()) {
                    NotificationListenerID listenerID = (NotificationListenerID)iter.next();
                    NotificationListenerWrapper wrapper =
                        (NotificationListenerWrapper)notificationListeners.get(listenerID);
                    if(wrapper != null && wrapper.getName().equals(name) &&
                            wrapper.getListener().equals(listener))
                        deregisterNotificationListener(listenerID,wrapper);
                }
            }
        }
    }

    public void removeNotificationListener(ObjectName name, ObjectName listener)
        throws InstanceNotFoundException, ListenerNotFoundException, TCKCommunicationException, TCKTestErrorException {
        assertConnected();
        try {
            remoteServer.removeNotificationListener(name, listener);
        } catch(RemoteException re) {
            throw new TCKCommunicationException("RemoteException caught while executing remote MBeanServer method",re);
        }
    }

    public MBeanInfo getMBeanInfo(ObjectName name) throws
            InstanceNotFoundException, IntrospectionException, ReflectionException,TCKCommunicationException, TCKTestErrorException {
        assertConnected();
        try {
            return remoteServer.getMBeanInfo(name) ;
        } catch(RemoteException re) {
            throw new TCKCommunicationException("RemoteException caught while executing remote MBeanServer method",re);
        }
    }

    public boolean isInstanceOf(ObjectName name, String className) throws InstanceNotFoundException, TCKCommunicationException, TCKTestErrorException{
        assertConnected();
        try {
            return remoteServer.isInstanceOf(name, className);
        } catch(RemoteException re) {
            throw new TCKCommunicationException("RemoteException caught while executing remote MBeanServer method",re);
        }
    }

    public void removeAllNotificationListeners() throws TCKCommunicationException, TCKTestErrorException {
        RemoteException caught = null;
        try {
            remoteServer.deregisterAllNotificationListeners();
        } catch(RemoteException re) { caught = re; }
        eventListenerRegistry.removeAllListeners();
        synchronized (notificationListeners) { notificationListeners.clear(); }
        if(caught != null) throw new TCKTestErrorException("RemoteException caught while executing remote MBeanServer method",caught);
    }

    // -- Implementation of TCKEventListener -- //

    public void handleEvent(Object event, Object channelID) {
        // lookup the listener then pass it the event
        synchronized (notificationListeners) {
            NotificationListenerWrapper wrapper = (NotificationListenerWrapper)notificationListeners.get(channelID);
            if(wrapper != null) {
                wrapper.getListener().handleNotification(
                    (Notification)event,wrapper.getHandback());
            }
        }

    }

    // -- Private methods -- //

    /**
     * Convenience method to throw a TCKTestErrorException if the
     * facade is not connected to the remote agent.
     */
    private void assertConnected() throws TCKTestErrorException {
        if(remoteServer == null) throw new TCKTestErrorException("Not connected to mbean facade remote agent.");
    }

    private synchronized NotificationListenerID createNotificationListenerID() {
        return new NotificationListenerID(nextNotificationListenerID++);
    }

    private void deregisterNotificationListener(NotificationListenerID listenerID,
            NotificationListenerWrapper wrapper) throws TCKCommunicationException, TCKTestErrorException,
                InstanceNotFoundException, ListenerNotFoundException {
        if(eventListenerRegistry == null) throw new TCKTestErrorException("The event listener registry is not set.");
        // deregister the proxy listener on the remote host
        try {
            remoteServer.deregisterNotificationListener(listenerID, wrapper.getName());
        } catch(RemoteException re) {
            throw new TCKCommunicationException("RemoteException caught while executing remote MBeanServer method",re);
        }
        // remove the listener from the local registry and event bridge
        eventListenerRegistry.removeListener(this,listenerID);
        synchronized (notificationListeners) {
            notificationListeners.remove(listenerID);
        }
    }

    // -- Private classes -- //

    /**
     * Wraps a notification listener, the object name it's listening
     * to, and the handback object.
     */
    private class NotificationListenerWrapper {

        public NotificationListenerWrapper(ObjectName name, NotificationListener listener, Object handback) {
            this.name = name;
            this.listener = listener;
            this.handback = handback;
        }

        private ObjectName getName() {
            return name;
        }

        private NotificationListener getListener() {
            return listener;
        }

        private Object getHandback() {
            return handback;
        }

        private ObjectName name;
        private NotificationListener listener;
        private Object handback;
    }

    // -- Private state -- //

    private MBeanFacadeAgentRemote remoteServer;
    private TCKEventListenerRegistry eventListenerRegistry;
    private int nextNotificationListenerID;
    private Map notificationListeners;
    private Logable log;

}
