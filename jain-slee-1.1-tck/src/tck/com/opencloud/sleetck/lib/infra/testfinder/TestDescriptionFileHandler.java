/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.infra.testfinder;

import java.util.Map;
import java.util.HashMap;
import java.util.Vector;
import java.util.Iterator;

import org.xml.sax.helpers.DefaultHandler;
import org.xml.sax.SAXNotRecognizedException;
import org.xml.sax.SAXParseException;
import org.xml.sax.SAXException;
import org.xml.sax.Attributes;
import org.xml.sax.InputSource;

import com.opencloud.sleetck.lib.DescriptionKeys;
import com.opencloud.sleetck.lib.TCKTestErrorException;

/**
 * An XML file handler to assist in reading TCK test description files.
 * It will throw an SAXException at some stage during the parse if the
 * file is not a valid test description, or cannot be read.
 * If the file is not a test description file, the parser will
 * throw a SAXException containing a nested NotATestDescriptionException.
 */
public class TestDescriptionFileHandler extends DefaultHandler {

    // Constants

    // The URI of the test description DTD as expected to appear in each test description
    private static final String DTD_URI = "http://www.opencloud.com/dtd/slee-tck-test-description_1_0.dtd";
    // Used to look for test description DTDs of other versions
    private static final String DTD_NAME_PREFIX = "slee-tck-test-description";
    private static final String NO_URI = "";

    // Constructor

    public TestDescriptionFileHandler(String localDTDURI) {
        this.localDTDURI = localDTDURI;
        testParameters = new HashMap();
        assertions = new Vector();
        assumeAssertions = new Vector();
        keywords = new Vector();
        elementTextBuf = new StringBuffer();
    }

    // Public methods introduced by this class

    /**
     * Returns a copy of the Map containing the test description parameters.
     */
    public Map getTestParameters() {
        return new HashMap(testParameters);
    }

    // Handler methods

    public InputSource resolveEntity(java.lang.String publicId,
                                     java.lang.String systemId)
                              throws SAXException {
        if(systemId.equals(DTD_URI)) {
            // the file references the expected DTD. return the local dtd as the InputSource
            foundDTD = true;
            return new InputSource(localDTDURI);
        } else if(systemId.indexOf(DTD_NAME_PREFIX) != -1) {
            // the file references a different version of the test description DTD
            throw new SAXNotRecognizedException(
             "The referenced test description dtd reference is not specified with the expected systemID. "+
             "Expected="+DTD_URI+"; Found="+systemId);
        } else {
            // the file is not a test description
            throw new SAXException(new NotATestDescriptionException());
        }
    }

    public void startDocument() throws SAXException {
        // clear state
        foundDTD = false;
        testParameters.clear();
        assertions.clear();
        assumeAssertions.clear();
        keywords.clear();
    }

    public void endDocument() throws SAXException {
        // convert the vectors to comma separated lists
        setParameter(DescriptionKeys.KEYWORDS,getList(keywords));
        setParameter(DescriptionKeys.ASSERTIONS,getList(assertions));
        setParameter(DescriptionKeys.ASSUME_ASSERTIONS,getList(assumeAssertions));
    }

    public void startElement(java.lang.String uri,
                             java.lang.String localName,
                             java.lang.String qName,
                             Attributes attributes)
                      throws SAXException {

        // reject all documents that don't have the correct dtd
        if(!foundDTD) throw new SAXException(new NotATestDescriptionException());

        elementTextBuf.setLength(0);

        if(localName.equals("assertion")) {
            assertions.addElement(attributes.getValue(NO_URI,"id"));

        } else if(localName.equals("assume-assertion")) {
            assumeAssertions.addElement(attributes.getValue(NO_URI,"id"));

        } else if(localName.equals("property")) {
            String name  = attributes.getValue(NO_URI,"name");
            validatePropertyName(name);
            String value = attributes.getValue(NO_URI,"value");
            setParameter(name,value);
        }
    }

    public void endElement(java.lang.String uri,
                           java.lang.String localName,
                           java.lang.String qName)
                    throws SAXException {

        String elementText = elementTextBuf.toString();
        if(localName.equals("title")) {
            setParameter(DescriptionKeys.TITLE,elementText);
        } else if(localName.equals("description")) {
            setParameter(DescriptionKeys.DESCRIPTION,elementText);
        } else if(localName.equals("executeClass")) {
            setParameter(DescriptionKeys.TEST_CLASS,elementText);
        } else if(localName.equals("executeArgs")) {
            setParameter(DescriptionKeys.EXECUTE_ARGS,elementText);
        } else if(localName.equals("timeout-multiplier")) {
            setParameter(DescriptionKeys.TIMEOUT_MULTIPLIER,elementText);
        } else if(localName.equals("keyword")) {
            if(keywords.contains(elementText)) throw new SAXException(
                new TCKTestErrorException("keyword "+elementText+" included twice in test description: "+localDTDURI));
            keywords.addElement(elementText);
        }
    }

    public void characters(char[] ch, int start, int length) throws SAXException {
        elementTextBuf.append(ch,start,length);
    }

    public void warning(SAXParseException e) throws SAXException {
        throw e;
    }

    public void error(SAXParseException e) throws SAXException {
        throw e;
    }

    // Private methods

    /**
     * Sets the given parameter to value if value
     * is not null, or does nothing if value is null.
     */
    private void setParameter(String name, String value) {
        if(value != null) testParameters.put(name, value);
    }

    /**
     * Throws a SAXException if the property name is repeated, or is a reserved
     * key as defined in DescriptionKeys.RESERVED_KEYS
     */
    private void validatePropertyName(String name) throws SAXException {
        for (int i = 0; i < DescriptionKeys.RESERVED_KEYS.length; i++) {
            if(DescriptionKeys.RESERVED_KEYS[i].equals(name)) throw new SAXException(
                new TCKTestErrorException("The property name "+name+
                " is a reserved name and may not be used to define a custom test property"));
        }
        if(testParameters.containsKey(name)) throw new SAXException(
                new TCKTestErrorException("Attempt to define property "+name+
                " twice. Properties can only be defined once in each test description."));
    }

    /**
     * A utility method which returns a comma separated list of
     * the elements contained in the given Vector of Strings.
     */
    private String getList(Vector vector) {
        StringBuffer buf = null;
        Iterator iter = vector.iterator();
        if(iter.hasNext()) {
            buf = new StringBuffer();
            buf.append((String)iter.next());
        }
        while(iter.hasNext()) {
            buf.append(',').append((String)iter.next());
        }
        return buf == null ? null : buf.toString();
    }

    // Private state

    private String localDTDURI;
    private boolean foundDTD;
    private Map testParameters;
    private Vector assertions;
    private Vector assumeAssertions;
    private Vector keywords;
    private StringBuffer elementTextBuf;

}
