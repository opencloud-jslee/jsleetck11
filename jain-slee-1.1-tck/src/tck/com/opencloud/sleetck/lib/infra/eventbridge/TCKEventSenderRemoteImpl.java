/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.infra.eventbridge;

import java.rmi.server.UnicastRemoteObject;
import java.rmi.RemoteException;
import java.rmi.NotBoundException;
import java.net.MalformedURLException;
import java.rmi.Naming;

/**
 * Implements the remote interface used by a 'client'
 * to connect an event listener.
 */
public class TCKEventSenderRemoteImpl extends UnicastRemoteObject
            implements TCKEventSenderRemote, BulkEventHandler {

    public TCKEventSenderRemoteImpl() throws RemoteException {

    }

    // -- Implementation of TCKEventSenderRemote -- //

    /**
     * Connects the given receiver: after this call the given
     * receiver can receive events.
     * If any receiver is already connected, it is replaced by the given receiver.
     */
    public synchronized void connectReceiver(TCKEventReceiverRemote receiver) throws RemoteException {
        if(_DEBUG_)debug("TCKEventReceiverRemoteImpl:connectReceiver()");
        if(this.receiver != null) disconnectReceiver();
        this.receiver = receiver;
    }

    /**
     * Disconnects the current receiver.
     * If no receiver is connected, this method does nothing.
     */
    public synchronized void disconnectReceiver() throws RemoteException {
        if(_DEBUG_)debug("TCKEventReceiverRemoteImpl:disconnectReceiver();");
        receiver = null;
    }

    // -- Implementation of BulkEventHandler -- //

    public synchronized void handleEvent(Object event, Object channelID) {
        if(_DEBUG_)debug("TCKEventReceiverRemoteImpl:handleEvent()");
        if(receiver != null) {
            try {
                if(_DEBUG_)debug("TCKEventReceiverRemoteImpl:sending event");
                receiver.receiveEvent(event, channelID);
            } catch(Exception e) {
                debug("WARNING: TCKEventReceiverRemoteImpl caught an Exception while sending an event to the event receiver:");
                e.printStackTrace();
            }
        }
    }

    public synchronized void handleEvents(Object[] events, Object[] channelIDs) {
        if(_DEBUG_)debug("TCKEventReceiverRemoteImpl:handleEvents()");
        if(receiver != null) {
            try {
                if(_DEBUG_)debug("TCKEventReceiverRemoteImpl:sending event");
                receiver.receiveEvents(events, channelIDs);
            } catch(Exception e) {
                debug("WARNING: TCKEventReceiverRemoteImpl caught an Exception while sending an event to the event receiver:");
                e.printStackTrace();
            }
        }
    }

    private void debug(String text) {
        System.err.println(text);
    }

    private TCKEventReceiverRemote receiver;

    private static final boolean _DEBUG_ = false;

}