/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.infra.eventbridge;

import java.rmi.Remote;
import java.rmi.RemoteException;

/**
 * Defines the remote interface used by the event
 * broadcaster to send events to the receiver.
 */
public interface TCKEventReceiverRemote extends Remote {

    public void receiveEvent(Object event, Object channelID) throws RemoteException;

    public void receiveEvents(Object[] events, Object[] channelIDs) throws RemoteException;

}