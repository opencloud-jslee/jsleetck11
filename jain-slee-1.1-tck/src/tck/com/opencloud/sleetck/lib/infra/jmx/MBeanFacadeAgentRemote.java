/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.infra.jmx;

import java.util.Set;
import javax.management.*;
import java.rmi.Remote;
import java.rmi.RemoteException;
import com.opencloud.sleetck.lib.TCKTestErrorException;

public interface MBeanFacadeAgentRemote extends Remote {

    public ObjectInstance createMBean(String className, ObjectName name)
        throws ReflectionException, InstanceAlreadyExistsException, MBeanRegistrationException, MBeanException, NotCompliantMBeanException, RemoteException, TCKTestErrorException;

    public ObjectInstance createMBean(String className, ObjectName name, ObjectName loaderName) 
        throws ReflectionException, InstanceAlreadyExistsException, MBeanRegistrationException, MBeanException, NotCompliantMBeanException,
               InstanceNotFoundException, RemoteException, TCKTestErrorException;

    public ObjectInstance createMBean(String className, ObjectName name, Object params[], String signature[]) 
        throws ReflectionException, InstanceAlreadyExistsException, MBeanRegistrationException, MBeanException, 
                NotCompliantMBeanException, RemoteException, TCKTestErrorException;

    public ObjectInstance createMBean(String className, ObjectName name, ObjectName loaderName, Object params[], String signature[]) 
        throws ReflectionException, InstanceAlreadyExistsException, MBeanRegistrationException, MBeanException, NotCompliantMBeanException,
               InstanceNotFoundException, RemoteException, TCKTestErrorException;

    public void unregisterMBean(ObjectName name) throws InstanceNotFoundException, 
        MBeanRegistrationException, RemoteException, TCKTestErrorException;

    public ObjectInstance getObjectInstance(ObjectName name) 
        throws InstanceNotFoundException, RemoteException, TCKTestErrorException;

    public Set queryMBeans(ObjectName name, QueryExp query) throws RemoteException, TCKTestErrorException;

    public Set queryNames(ObjectName name, QueryExp query) throws RemoteException, TCKTestErrorException;

    public boolean isRegistered(ObjectName name) throws RemoteException, TCKTestErrorException;

    public Integer getMBeanCount() throws RemoteException, TCKTestErrorException;

    public Object getAttribute(ObjectName name, String attribute) throws
        MBeanException, AttributeNotFoundException, InstanceNotFoundException,
        ReflectionException, RemoteException, TCKTestErrorException;


    public AttributeList getAttributes(ObjectName name, String[] attributes)
        throws InstanceNotFoundException, ReflectionException, RemoteException, TCKTestErrorException;

    public void setAttribute(ObjectName name, Attribute attribute) throws
        InstanceNotFoundException, AttributeNotFoundException, InvalidAttributeValueException, MBeanException, 
        ReflectionException ,RemoteException, TCKTestErrorException;

    public AttributeList setAttributes(ObjectName name, AttributeList attributes)
        throws InstanceNotFoundException, ReflectionException, RemoteException, TCKTestErrorException;

    /**
     * Invokes the given method on the given object.
     * @return the return value of the operation. If the return type
     *  of the called method is 'void', this method returns null.
     */
    public Object invoke(ObjectName name, String operationName, Object params[], String signature[]) 
        throws InstanceNotFoundException, MBeanException, ReflectionException,  RemoteException, TCKTestErrorException;
  
    public String getDefaultDomain() throws RemoteException, TCKTestErrorException;

    public void addNotificationListener(ObjectName name, ObjectName listener, NotificationFilter filter, Object handback)
        throws InstanceNotFoundException, RemoteException, TCKTestErrorException;

    public void removeNotificationListener(ObjectName name, ObjectName listener) 
        throws InstanceNotFoundException, ListenerNotFoundException, RemoteException, TCKTestErrorException;

    public MBeanInfo getMBeanInfo(ObjectName name) throws InstanceNotFoundException, IntrospectionException, 
        ReflectionException,RemoteException, TCKTestErrorException;

    public boolean isInstanceOf(ObjectName name, String className) 
        throws InstanceNotFoundException, RemoteException, TCKTestErrorException;

    // -- Methods which differ from the standard MBeanServer interface -- //

    /**
     * Replaces addNotificationListener to handle the RMI invokation pattern.
     */
    public void registerNotificationListener(NotificationListenerID id, ObjectName name, NotificationFilter filter)
        throws InstanceNotFoundException, RemoteException, TCKTestErrorException;

    /**
     * Replaces removeNotificationListener to handle the RMI invokation pattern.
     */
    public void deregisterNotificationListener(NotificationListenerID id, ObjectName name)
        throws InstanceNotFoundException, ListenerNotFoundException, RemoteException, TCKTestErrorException;

    /**
     * Deregisters all notification listeners currently registered through this interface.
     */
    public void deregisterAllNotificationListeners() throws RemoteException, TCKTestErrorException;
    
}