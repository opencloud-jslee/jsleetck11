/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.infra.jmx;

import java.io.Serializable;

/**
 * A NotificationListenerID uniquely identifies a notification listener,
 * based on the creation time, and a unique number passed to the 
 * contructor.
 */
public class NotificationListenerID implements Serializable {

    public NotificationListenerID(int id) {        
        this.id = id;
        date = System.currentTimeMillis();
    }

    public boolean equals(Object o) {
        if((o != null) && (o instanceof NotificationListenerID)) {
            NotificationListenerID other = (NotificationListenerID)o;
            return other.id == id && other.date == date;
        } else return false;
    }

    public int hashCode() {
        return id;
    }
    
    private int id;
    private long date;
    
}
