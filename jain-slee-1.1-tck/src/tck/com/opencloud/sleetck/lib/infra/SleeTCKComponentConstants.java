/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.infra;

import javax.slee.resource.ResourceAdaptorTypeID;
import javax.slee.EventTypeID;

/**
 * Defines constants related to TCK installed components and profiles.
 */
public interface SleeTCKComponentConstants {

    /**
     * The expected value for the component vendor field in the component descriptors
     * of all tck installed components.
     */
    public static final String TCK_VENDOR = "jain.slee.tck";

    /**
     * The profile table name prefix for all tck created standard address
     * profile tables. The value if this constant is "tck."
     */
    public static final String TCK_ADDRESS_PROFILE_TABLE_PREFIX = "tck.";

    /**
     * The index of the component type field within the EXPECTED_TCK_COMPONENTS_INFO array
     */
    public static final int COMPONENT_TYPE_FIELD = 0;

    /**
     * The index of the component name field within the EXPECTED_TCK_COMPONENTS_INFO array
     */
    public static final int COMPONENT_NAME_FIELD = 1;

    /**
     * The index of the component version field within the EXPECTED_TCK_COMPONENTS_INFO array
     */
    public static final int COMPONENT_VERSION_FIELD = 2;

    /**
     * Component information for all the TCK specific event types,
     * profile specification types, resource adaptor types, sbbs, and services
     * which the TCK expects to remain installed across tests.
     * Each nested Object array is in the following form:
     * <code>
     * {component type ID class, component name, version}
     * </code>
     * e.g.:
     * <code>
     * {javax.slee.EventTypeID.class, "TCKEvent1", "1.0"}
     * </code>
     * Notes:
     * (1) The vendor for each component is TCK_VENDOR
     * (2) each component name must be unique within this array
     */
    public static final Object[][] EXPECTED_TCK_COMPONENTS_INFO = {
        {ResourceAdaptorTypeID.class,"TCK_Resource_Type","1.0"},
        {EventTypeID.class,"com.opencloud.sleetck.lib.resource.events.TCKResourceEventX.X1","1.0"},
        {EventTypeID.class,"com.opencloud.sleetck.lib.resource.events.TCKResourceEventX.X2","1.0"},
        {EventTypeID.class,"com.opencloud.sleetck.lib.resource.events.TCKResourceEventX.X3","1.0"},
        {EventTypeID.class,"com.opencloud.sleetck.lib.resource.events.TCKResourceEventY.Y1","1.0"},
        {EventTypeID.class,"com.opencloud.sleetck.lib.resource.events.TCKResourceEventY.Y2","1.0"},
        {EventTypeID.class,"com.opencloud.sleetck.lib.resource.events.TCKResourceEventY.Y3","1.0"}
    };

}
