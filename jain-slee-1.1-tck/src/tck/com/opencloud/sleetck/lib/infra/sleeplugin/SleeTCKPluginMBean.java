/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.infra.sleeplugin;

import javax.management.ObjectName;
import javax.slee.management.PeerUnavailableException;

public interface SleeTCKPluginMBean {

    /**
     * The identity of the MBean within the context of
     * the default domain.
     */
    public static final String JMX_ID = ":name=SleeTCKPlugin";

    /**
     * Returns the JMX ObjectName of the vendor's
     * SleeManagementMBean.
     *
     * This method does not throw any Exceptions.
     */
    public ObjectName getSleeManagementMBean();

}