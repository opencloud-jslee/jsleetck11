/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.infra.testfinder;

/**
 * An Exception for indicating that an xml file is not a test description file.
 * The test description reader expects to receive this exception if it
 * tries to parse an xml file which is not a test description (e.g. a build file).
 * As the exception is expected in the normal case, it is not reported, and
 * contains no message.
 */
public class NotATestDescriptionException extends Exception {
}
