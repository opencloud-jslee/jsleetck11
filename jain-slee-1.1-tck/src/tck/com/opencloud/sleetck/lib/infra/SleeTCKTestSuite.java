/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.infra;

import java.io.File;
import java.util.Map;
import com.sun.interview.Interview;
import com.sun.javatest.Script;
import com.sun.javatest.TestSuite;
import com.sun.javatest.Harness;
import com.sun.javatest.TestFinder;
import com.sun.javatest.TestDescription;
import com.sun.javatest.TestEnvironment;
import com.sun.javatest.InterviewParameters;
import com.sun.javatest.WorkDirectory;
import com.sun.javatest.util.BackupPolicy;
import com.sun.javatest.util.I18NResourceBundle;
import com.opencloud.sleetck.lib.interview.SleeTCKInterview;
import com.opencloud.sleetck.lib.infra.testfinder.SleeTCKTestFinder;

public class SleeTCKTestSuite extends TestSuite {

    /**
     * Constructor
     *
     * @param root the root file of the test suite
     * @param tsInfo a map of the properties set in the testsuite.jtt file
     */
    public SleeTCKTestSuite(File root, Map tsInfo, ClassLoader cl) throws TestSuite.Fault {
        super(root, tsInfo, cl);
        i18NResourceBundle = I18NResourceBundle.getBundleForClass(getClass());
    }

    public TestFinder createTestFinder() throws TestSuite.Fault {
        try {
            return new SleeTCKTestFinder(getRoot());
        } catch(TestFinder.Fault fault) {
            fault.printStackTrace();
            throw new TestSuite.Fault(i18NResourceBundle,TEST_FINDER_FAULT_MSG,fault.getMessage());
        }
    }

    public Script createScript(TestDescription td, String[] exclTestCases,
                TestEnvironment scriptEnv, WorkDirectory workDir, BackupPolicy backupPolicy) {

        Script rScript = new SleeTCKScript();
        rScript.initTestDescription(td);
        rScript.initExcludedTestCases(exclTestCases);
        rScript.initTestEnvironment(scriptEnv);
        rScript.initWorkDir(workDir);
        rScript.initBackupPolicy(backupPolicy);
        rScript.initClassLoader(getClassLoader());
        return rScript;
    }

    public InterviewParameters createInterview() throws TestSuite.Fault {
        try {
            return new SleeTCKInterview(this);
        } catch(Interview.Fault fault) {
            fault.printStackTrace();
            throw new TestSuite.Fault(i18NResourceBundle,INTERVIEW_FAULT_MSG,fault.getMessage());
        }
    }

    public void starting(Harness harness) {
        System.out.println("Starting test suite...");
    }

    private I18NResourceBundle i18NResourceBundle = null;

    // il8n resource bundle keys
    private static final String TEST_FINDER_FAULT_MSG = "SleeTCKTestSuite.createTestFinder.test-finder-fault";
    private static final String INTERVIEW_FAULT_MSG = "SleeTCKTestSuite.createInterview.interview-fault";

}
