/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.infra.eventbridge;

import java.util.Map;
import java.util.HashMap;
import java.util.Vector;

/**
 * Components can register TCKEventListeners via this interface.
 */
public interface TCKEventListenerRegistry {

    /**
     * Registers the given listener for the given event channel.
     */
    public void addListener(TCKEventListener listener, Object channelID);

    /**
     * Deregisters the given listener for the given event channel.
     */
    public void removeListener(TCKEventListener listener, Object channelID);

    /**
     * Removes all listeners from the registry.
     */
    public void removeAllListeners();

}