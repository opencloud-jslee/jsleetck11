/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.infra;

import java.net.MalformedURLException;
import java.util.Properties;
import java.util.Iterator;
import java.util.Vector;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.StringTokenizer;
import java.rmi.Naming;
import java.rmi.RemoteException;
import java.rmi.NotBoundException;
import java.rmi.server.UnicastRemoteObject;

import javax.management.ObjectName;

import javax.slee.ComponentID;
import javax.slee.management.ManagementException;
import javax.slee.UnrecognizedComponentException;
import javax.slee.management.ComponentDescriptor;
import javax.slee.management.SleeState;

import com.sun.javatest.Script;
import com.sun.javatest.Status;
import com.sun.javatest.Test;
import com.sun.javatest.TestDescription;
import com.sun.javatest.TestEnvironment;

import com.opencloud.logging.LogLevel;
import com.opencloud.logging.Logable;
import com.opencloud.logging.PrintWriterLog;
import com.opencloud.sleetck.lib.SleeTCKTest;
import com.opencloud.sleetck.lib.TCKTestResult;
import com.opencloud.sleetck.lib.SleeTCKTestUtils;
import com.opencloud.sleetck.lib.TCKTestErrorException;
import com.opencloud.sleetck.lib.TCKTestFailureException;
import com.opencloud.sleetck.lib.OperationTimedOutException;
import com.opencloud.sleetck.lib.EnvironmentKeys;
import com.opencloud.sleetck.lib.DescriptionKeys;
import com.opencloud.sleetck.lib.testutils.SleeStarter;
import com.opencloud.sleetck.lib.testutils.ProfileUtils;
import com.opencloud.sleetck.lib.infra.SleeTCKTestUtilsImpl;
import com.opencloud.sleetck.lib.infra.SleeTCKComponentConstants;
import com.opencloud.sleetck.lib.infra.jmx.MBeanFacadeImpl;
import com.opencloud.sleetck.lib.infra.jmx.MBeanFacadeRMIConstants;
import com.opencloud.sleetck.lib.infra.jmx.MBeanFacadeAgentRemote;
import com.opencloud.sleetck.lib.infra.eventbridge.TCKEventSenderRemote;
import com.opencloud.sleetck.lib.infra.eventbridge.TCKEventListenerRegistry;
import com.opencloud.sleetck.lib.infra.eventbridge.ClientSetup;
import com.opencloud.sleetck.lib.infra.sleeplugin.SleeTCKPluginMBean;
import com.opencloud.sleetck.lib.testutils.jmx.MBeanProxyFactory;
import com.opencloud.sleetck.lib.testutils.jmx.impl.MBeanProxyFactoryImpl;
import com.opencloud.sleetck.lib.testutils.jmx.DeploymentMBeanProxy;
import com.opencloud.sleetck.lib.rautils.MessageHandlerRegistry;
import com.opencloud.sleetck.lib.rautils.MessageHandlerRegistryImpl;
import com.opencloud.sleetck.lib.rautils.RMIObjectChannel;
import com.opencloud.sleetck.lib.rautils.RMIObjectChannelImpl;
import com.opencloud.sleetck.lib.resource.testapi.TCKResourceTestInterface;
import com.opencloud.sleetck.lib.resource.impl.TCKResourceTestInterfaceProxy;

/**
 * The SleeTCKScript is responsible for running an individual test
 * and returning a test result.
 *
 * A new instance of this class is created for each test.
 */
public class SleeTCKScript extends Script {

    // -- Public methods -- //

    /**
     * Run the given test using the given arguments.
     *
     * This method is responsible for achieving the appropriate
     * state prior to and following a test run, executing
     * the test, and returing the result of the test run.
     *
     * If an Exception or Error is thrown during test preparations,
     * the test result is of type ERROR.
     *
     * @param args  Ignored
     * @param td    The test description for the test to be performed
     * @param env   The test environment giving the details of how to run the test
     * @return        The result of running the script
     */
    public Status run(String[] args, TestDescription td, TestEnvironment env) {

        // support test types and result types for both the SleeTCKTest interface and the com.sun.javatest.Test interface
        TCKTestResult result = null;
        Status status = null;
        SleeTCKTest sleeTCKTest = null;
        Test classicTest = null;
        try {
            prepare();
            Object test = loadTest();
            if (test instanceof SleeTCKTest) {
                sleeTCKTest = (SleeTCKTest) loadTest();
                sleeTCKTest.init(testUtils);
            } else if (test instanceof Test) {
                classicTest = (Test) test;
            } else
                throw new ClassCastException("Test " + test + " was not a SleeTCKTest or a com.sun.javatest.Test");
            
            if (sleeTCKTest != null) {
                // Log title of test to TCK Resource - This is mainly to make reading the resulting log files easier.
                try {
                    String testTitle = td.getParameter(DescriptionKeys.TITLE);
                    String testClass = td.getParameter(DescriptionKeys.TEST_CLASS);
                    String assertions = td.getParameter(DescriptionKeys.ASSERTIONS);
                    testUtils.getResourceInterface().log(LogLevel.INFO,
"\n\n" +
"========================================================================================\n" +
" Test title: " + testTitle + "\n" + 
" Test class: " + testClass + "\n" +
" Assertions:  " + assertions + "\n" +
"========================================================================================\n");
                } catch (Exception e) {
                    log.error("An error occured while trying to log a test description to the TCK Resource");
                    log.error(e);
                }
                
                try {
                    try {
                        testUtils.getResourceInterface().log(LogLevel.INFO, "\n\n============\n" + 
                                                                              " Test setup \n" + 
                                                                              "============");
                    } catch (Exception e) {
                        log.error("An error occured while trying to log a message to the TCK Resource");
                        log.error(e);
                    }
                    sleeTCKTest.setUp();
                    try {
                        try {
                            testUtils.getResourceInterface().log(LogLevel.INFO, "\n\n===========\n" + 
                                                                                  " Test body \n" + 
                                                                                  "===========");
                        } catch (Exception e) {
                            log.error("An error occured while trying to log a message to the TCK Resource");
                            log.error(e);
                        }
                        result = sleeTCKTest.run();
                        if (result == null)
                            throw new TCKTestErrorException("Tests must return a non-null result from the run() method");
                    } catch (Throwable caughtDuringRun) {
                        if (caughtDuringRun instanceof TCKTestFailureException) {
                            result = TCKTestResult.failed((TCKTestFailureException) caughtDuringRun);
                        } else
                            throw caughtDuringRun;
                    }
                } finally {
                    try {
                        testUtils.getResourceInterface().log(LogLevel.INFO, "\n\n===============\n" + 
                                                                              " Test teardown \n" + 
                                                                              "===============");
                    } catch (Exception e) {
                        log.error("An error occured while trying to log a message to the TCK Resource");
                        log.error(e);
                    }
                    sleeTCKTest.tearDown();
                }
                
                try {
                    testUtils.getResourceInterface().log(LogLevel.INFO, 
                        "\n=============\n" + 
                          " End of test \n" + 
                          "=============\n");
                } catch (Exception e) {
                    log.error("An error occured while trying to log a message to the TCK Resource");
                    log.error(e);
                }
            } else { 
                String executeArgsString = td.getParameter(DescriptionKeys.EXECUTE_ARGS);
                StringTokenizer argTokens = new StringTokenizer(executeArgsString);
                String[] executeArgs = new String[argTokens.countTokens()];
                int i = 0;
                while(argTokens.hasMoreTokens()) {
                    executeArgs[i++] = argTokens.nextToken();
                }
                status = classicTest.run(executeArgs, trOut, trOut);
            }
        } catch(Throwable caughtBeforeCleanUp) {
            log.warning(caughtBeforeCleanUp);
            // note: we override a pass result with any error caught during tearDown()
            if(sleeTCKTest != null) {
              if(result == null || result.isPassed()) result = TCKTestResult.error(caughtBeforeCleanUp);
            } else { 
              if(status == null || status.isPassed()) status = Status.error(caughtBeforeCleanUp.toString());
            }
        } finally {
            try {
                cleanUp();
            } catch(Throwable caughtDuringCleanUp) {
                log.warning(caughtDuringCleanUp);
                // note: we override a pass result with any error caught during cleanUp()
                if(sleeTCKTest != null) {
                 if(result.isPassed()) result = TCKTestResult.error(caughtDuringCleanUp);
                } else {
                 if(status.isPassed()) status = Status.error(caughtDuringCleanUp.toString());
                }
            }
        }
        

        return status != null ? status : resultToStatus(result);
    }

    // -- Private methods -- //

    /**
     * This method is responsible for achieving the state promised by the
     * contract defined in com.opencloud.sleetck.lib.SleeTCKTest.run(), and
     * initialising required resources and parameters used to run the test.
     * These include:<ol>
     * <li>the parameter maps</li>
     * <li>the MBean facade</li>
     * <li>the resource interface</li>
     * <li>the logger</li>
     * </ol>
     *
     * @throws Exception If an exception or error is thrown,
     *  the test result is of type Status.ERROR.
     */
    private void prepare() throws Exception {
        // -- Prepare resources -- //

        log = new PrintWriterLog(trOut);
        initParameterMaps();
        int logLevel = Integer.parseInt(envParams.getProperty(EnvironmentKeys.DEBUG_LEVEL));
        log.setIgnoreLevel(logLevel);
        initTimeouts();

        initMBeanFacade();
        initMgmtMBeanName();
        mBeanProxyFactory = new MBeanProxyFactoryImpl(mBeanFacade);
        lookupResourceInterface();
        
        bindRMIObjectChannel();
        bindMessageHandlerRegistry();

        testUtils = new SleeTCKTestUtilsImpl(testParams,
            envParams, mBeanFacade, mBeanProxyFactory, resourceInterface, rmiObjectChannel, messageHandlerRegistry, log, mgmtMBeanName,
            envParams.getProperty(EnvironmentKeys.COMPONENT_URL_PREFIX), defaultTimeout, testTimeout);

        // -- Achieve pre-conditions defined in SleeTCKTest -- //

        // ensure that the SLEE is in the RUNNING state
        if(isSleeStuckInStoppingState && testUtils.getSleeManagementMBeanProxy().getState().isStopping()) {
            throw new TCKTestErrorException("The SLEE is stuck in the STOPPING state: a previous test timed "+
                "out while waiting for the SLEE to restart, and the SLEE is still in the STOPPING state.");
        } else isSleeStuckInStoppingState = false;
        try {
            SleeStarter.startSlee(testUtils.getSleeManagementMBeanProxy(),defaultTimeout);
        } catch (OperationTimedOutException otoe) {
            String message = null;
            SleeState currentState = testUtils.getSleeManagementMBeanProxy().getState();
            if(currentState.isStopping()) {
                isSleeStuckInStoppingState = true;
                message = "Couldn't run the test, because the SLEE is stuck in the STOPPING state";
            } else message = "Couldn't run the test, because the timeout was reached while waiting for the SLEE to "+
                "reach the RUNNING state. Current state: "+currentState;
            throw new TCKTestErrorException(message,otoe);
        }

        // ensure that only the correct TCK-owned services are installed in the SLEE
        ensureCorrectTCKComponents();

        // ensure that no TCK created address profile tables exist in the SLEE
        List tckStdAddressProfileTables = new ProfileUtils(testUtils).getTCKStdAddressProfileTables();
        if(!tckStdAddressProfileTables.isEmpty()) {
            String aTableName = (String)tckStdAddressProfileTables.get(0);
            throw new TCKTestErrorException(
             "A tck created standard address profile table still exists in the SLEE. TableName="+aTableName+
             "\nOne of the pre-conditions of each test is that no such tables may exist prior to a test run.");
        }
    }

    /**
     * This method is responsible for performing any post-test cleanup functions.
     *
     * This method is always run following a cleanup() call, whether or not the test
     * was actually run.
     *
     * If this method throws an Exception, and the test result was a pass,
     * the result will be changed to an error. Otherwise the test result
     * is not affected.
     *
     * @throws Exception
     */
    private void cleanUp() throws Exception {
        Vector caught = new Vector();

        // resource
        if(resourceInterface != null) {
            try {
                resourceInterface.clearActivities();
                resourceInterface.releaseResources();
            } catch(Exception e) { e.printStackTrace(trOut); caught.addElement(e); }
            resourceInterface = null;
        }
        
        // RMI Object channel
        unbindRMIObjectChannel();

        // Message handler registry
        unbindMessageHandlerRegistry();
        
        // uninstall components which were installed via the SleeTCKTestUtils interface
        if(testUtils != null) {
            try {
                if(testUtils.deactivateAllServices()) log.warning("Test did not deactivate all its own services.");
                if(testUtils.uninstallAll()) log.warning("Test did not uninstall all its own SLEE components.");
            } catch(Exception e) { e.printStackTrace(trOut); caught.addElement(e); }
        }

        // restart the Slee if this option is chosen, and the Slee is not stuck in the STOPPING state
        if(!isSleeStuckInStoppingState) {
            Boolean restartOption = Boolean.valueOf(envParams.getProperty(EnvironmentKeys.RESTART_BETWEEEN_TESTS));
            if(restartOption.booleanValue()) {
                try {
                    log.finer("Restart Slee option specified. About to restart Slee...");
                    SleeStarter.restartSlee(testUtils.getSleeManagementMBeanProxy(),defaultTimeout);
                    log.finer("Slee restarted successfully");
                } catch(OperationTimedOutException otoe) {
                    try {
                        SleeState currentState = testUtils.getSleeManagementMBeanProxy().getState();
                        if(currentState.isStopping()) isSleeStuckInStoppingState = true;
                        throw new TCKTestErrorException(
                            "The timeout was reached while trying to restart the SLEE. Current state:"+currentState);
                    } catch (Exception checkStateEx) {
                        checkStateEx.printStackTrace(trOut);
                        caught.addElement(checkStateEx);
                    }
                } catch (Exception e) {
                    e.printStackTrace(trOut);
                    caught.addElement(e);
                }
            }
        }

        // event bridge
        if(remoteSender != null) {
            try {
                remoteSender.disconnectReceiver();
            } catch(Exception e) { e.printStackTrace(trOut); caught.addElement(e); }
            remoteSender = null;
        }

        // jmx
        if(mBeanFacade != null) {
            try {
                mBeanFacade.removeAllNotificationListeners();
            } catch(Exception e) { e.printStackTrace(trOut); caught.addElement(e); }
            mBeanFacade = null;
            mBeanProxyFactory = null;
        }

        testUtils = null;
        if(caught.size() == 1) throw (Exception)caught.firstElement();
        if(caught.size() > 1) throw new TCKTestErrorException("Multiple Exceptions caught during "+
            "test cleanup. (Stack traces were written to the log)");
    }

    /**
     * Initialises the parameter maps - extracts name-value pairs
     * from the TestEnvironment and TestDescription into a Properties map.
     */
    private void initParameterMaps() throws Exception {
        // convert the TestDescription to a map of name-value pairs
        testParams = new Properties();
        Iterator tdIter = td.getParameterKeys();
        while(tdIter.hasNext()) {
            String tdKey = (String)tdIter.next();
            String tdValue = td.getParameter(tdKey);
            testParams.setProperty(tdKey,tdValue);
        }

        // convert the TestEnvironment to a map of name-value pairs
        envParams = new Properties();
        Iterator envIter = env.keys().iterator();
        while(envIter.hasNext()) {
            String envKey = (String)envIter.next();
            String[] envValues = env.lookup(envKey);
            if(envValues != null && envValues.length > 0)
                envParams.setProperty(envKey,envValues[0]);
        }
    }

    private void initMBeanFacade() throws Exception {
        try {
            // lookup the remote sender
            String agentHost = envParams.getProperty(EnvironmentKeys.JMX_AGENT_HOST_IP);
            String agentRMIPort = envParams.getProperty(EnvironmentKeys.JMX_AGENT_PORT);
            String remoteSenderURL = "//"+agentHost+":"+agentRMIPort+"/"+MBeanFacadeRMIConstants.TCK_EVENT_SENDER_REMOTE;
            remoteSender = (TCKEventSenderRemote)Naming.lookup(remoteSenderURL);

            TCKEventListenerRegistry listenerRegistry = ClientSetup.setupClient(remoteSender);

            // create the MBeanFacade
            mBeanFacade = new MBeanFacadeImpl(log);

            // lookup and connect to the remote MBeanFacade agent
            String facadeURL = "//"+agentHost+":"+agentRMIPort+"/"+MBeanFacadeRMIConstants.MBEAN_FACADE_AGENT_REMOTE;
            MBeanFacadeAgentRemote facadeAgent = (MBeanFacadeAgentRemote)Naming.lookup(facadeURL);
            mBeanFacade.setMBeanFacadeAgentRemote(facadeAgent);

            // the MBeanFacade listens to the event listener registry
            mBeanFacade.setListenerRegistry(listenerRegistry);
        } catch(java.rmi.ConnectException ce) {
            throw new TCKTestErrorException(
                "Received ConnectException while trying to connect to TCK plugin. "+
                "Check that the TCK Plugin MBean was successfully registered in the MBeanServer, "+
                "and that the jmx agent properties are properly configured",ce);
        }
    }

    private void initMgmtMBeanName() throws Exception {
        String jmxDomain = envParams.getProperty(EnvironmentKeys.JMX_AGENT_DOMAIN);
        if(jmxDomain == null) jmxDomain = "";
        ObjectName pluginName = new ObjectName(jmxDomain + SleeTCKPluginMBean.JMX_ID);
        mgmtMBeanName = (ObjectName)mBeanFacade.invoke(pluginName, "getSleeManagementMBean", new Object[]{}, new String[]{});
    }

    private void initTimeouts() throws Exception {
        defaultTimeout = Integer.parseInt(envParams.getProperty(EnvironmentKeys.DEFAULT_TIMEOUT));
        String timeoutMultiplerString = testParams.getProperty(DescriptionKeys.TIMEOUT_MULTIPLIER);
        if(timeoutMultiplerString != null) {
            try {
                double timeoutMultipler = Double.parseDouble(timeoutMultiplerString);
                if(timeoutMultipler <= 0) throw new TCKTestErrorException("Invalid timeout-multiplier specified: "+
                        timeoutMultipler+". Value must be a positive real number.");
                testTimeout = (int)(defaultTimeout * timeoutMultipler);
                log.fine("Test timeout value set to "+testTimeout+". (Default timeout "+defaultTimeout+
                        " multiplied by timeout-multiplier "+timeoutMultipler+")");
            } catch (NumberFormatException e) {
                throw new TCKTestErrorException("Invalid timeout-multiplier specified: "+
                        timeoutMultiplerString+". Value must be a positive real number.",e);
            }
        } else testTimeout = defaultTimeout;
    }

    private void lookupResourceInterface() throws TCKTestErrorException {
        String resourceHost = envParams.getProperty(EnvironmentKeys.TCK_RESOURCE_HOST_IP);
        String resourcePort = envParams.getProperty(EnvironmentKeys.TCK_RESOURCE_PORT);
        String rmiName = "rmi://"+resourceHost+":"+resourcePort+"/"+TCKResourceTestInterface.RMI_NAME;
        try {
            TCKResourceTestInterface remoteHandle = (TCKResourceTestInterface)Naming.lookup(rmiName);
            resourceInterface = new TCKResourceTestInterfaceProxy(remoteHandle,log);
        } catch(RemoteException re) {
            throw new TCKTestErrorException("Exception caught setting up resource interface: ",re);
        } catch(NotBoundException nbe) {
            throw new TCKTestErrorException("Exception caught setting up resource interface: ",nbe);
        } catch(MalformedURLException mue) {
            throw new TCKTestErrorException("Exception caught setting up resource interface: ",mue);
        }
    }

    private void bindRMIObjectChannel() throws TCKTestErrorException {
        try {
            rmiObjectChannel = new RMIObjectChannelImpl();
            Naming.bind(getRMIObjectChannelName(), rmiObjectChannel);
        } catch (Exception e) {
            throw new TCKTestErrorException("Exception caught setting up RMIObjectChannel: ", e);
        }
    }

    private void unbindRMIObjectChannel() throws TCKTestErrorException {
        try {
            UnicastRemoteObject.unexportObject(rmiObjectChannel, true);
            Naming.unbind(getRMIObjectChannelName());
        } catch (Exception e) {
            throw new TCKTestErrorException("Exception caught cleaning up RMIObjectChannel: ", e);
        }
    }

    private String getRMIObjectChannelName() {
        String resourceHost = envParams.getProperty(EnvironmentKeys.TCK_RESOURCE_HOST_IP);
        String resourcePort = envParams.getProperty(EnvironmentKeys.TCK_RESOURCE_PORT);
        String rmiName = "rmi://" + resourceHost + ":" + resourcePort + "/" + RMIObjectChannel.RMI_NAME;
        return rmiName;
    }

    private void bindMessageHandlerRegistry() throws TCKTestErrorException {
        try {
            messageHandlerRegistry = new MessageHandlerRegistryImpl();
            Naming.bind(getMessageHanglerRegistryName(), messageHandlerRegistry);
        } catch (Exception e) {
            throw new TCKTestErrorException("Exception caught setting up MessageHandlerRegistry: ", e);
        }
    }

    private void unbindMessageHandlerRegistry() throws TCKTestErrorException {
        try {
            UnicastRemoteObject.unexportObject(messageHandlerRegistry, true);
            Naming.unbind(getMessageHanglerRegistryName());
        } catch (Exception e) {
            throw new TCKTestErrorException("Exception caught cleaning up MessageHandlerRegistry: ", e);
        }
    }

    private String getMessageHanglerRegistryName() {
        String resourceHost = envParams.getProperty(EnvironmentKeys.TCK_RESOURCE_HOST_IP);
        String resourcePort = envParams.getProperty(EnvironmentKeys.TCK_RESOURCE_PORT);
        String rmiName = "rmi://" + resourceHost + ":" + resourcePort + "/" + MessageHandlerRegistry.RMI_NAME;
        return rmiName;
    }
    
    /**
     * Loads and returns the test class
     */
    private Object loadTest() throws ClassNotFoundException,
            InstantiationException, IllegalAccessException {
        String className = testParams.getProperty(DescriptionKeys.TEST_CLASS);
        Class testClass = loader.loadClass(className);
        return testClass.newInstance();
    }

    /**
     * Ensures that only the corrected TCK-owned services are installed in the SLEE
     */
    private void ensureCorrectTCKComponents() throws TCKTestErrorException,
                                ManagementException, UnrecognizedComponentException {
        DeploymentMBeanProxy deploymentProxy = testUtils.getDeploymentMBeanProxy();
        // get the component IDs for all component types we are interested in

        ComponentID[][] componentIDs = new ComponentID[][] {
            deploymentProxy.getEventTypes(),deploymentProxy.getProfileSpecifications(),
            deploymentProxy.getResourceAdaptorTypes(),deploymentProxy.getSbbs(),
            deploymentProxy.getServices()};
        // build a map of the expected tck components
        Object[][] componentsInfo = SleeTCKComponentConstants.EXPECTED_TCK_COMPONENTS_INFO;
        Map expectedComponents = new HashMap();
        for (int i = 0; i < componentsInfo.length; i++) {
            expectedComponents.put(componentsInfo[i][SleeTCKComponentConstants.COMPONENT_NAME_FIELD],componentsInfo[i]);
        }
        // check each component
        for (int typeIndex = 0; typeIndex < componentIDs.length; typeIndex++) {
            for (int compIndex = 0; compIndex < componentIDs[typeIndex].length; compIndex++) {
                ComponentID componentID = componentIDs[typeIndex][compIndex];
                ComponentDescriptor descriptor = deploymentProxy.getDescriptor(componentID);
                if(SleeTCKComponentConstants.TCK_VENDOR.equals(descriptor.getVendor())) {
                    // check that the tck component is expected
                    boolean wasExpected = false;
                    String componentName = descriptor.getName();
                    if(expectedComponents.containsKey(componentName)) {
                        Object[] matchingComponent = (Object[])expectedComponents.get(componentName);
                        Class matchingClass = (Class)matchingComponent[SleeTCKComponentConstants.COMPONENT_TYPE_FIELD];
                        if(matchingClass.isAssignableFrom(componentID.getClass()) &&
                           matchingComponent[SleeTCKComponentConstants.COMPONENT_VERSION_FIELD].equals(descriptor.getVersion())) {
                            wasExpected = true;
                            expectedComponents.remove(componentName); // tick it off the list of components to ensure
                        }
                    }
                    if(!wasExpected) throw new TCKTestErrorException("Found unexpected TCK component still "+
                     "installed in the SLEE (probably left by a previous test). ComponentID="+componentID);
                }
            }
        }
        // check that all the expected components were found
        if(!expectedComponents.isEmpty()) {
            Object[] info = (Object[])expectedComponents.values().iterator().next();
            StringBuffer formattedInfo = new StringBuffer("Name="+info[SleeTCKComponentConstants.COMPONENT_NAME_FIELD]);
            formattedInfo.append(",Type="+info[SleeTCKComponentConstants.COMPONENT_TYPE_FIELD]);
            formattedInfo.append(",Version="+info[SleeTCKComponentConstants.COMPONENT_VERSION_FIELD]);
            throw new TCKTestErrorException("Expected TCK component is not installed: "+formattedInfo.toString());
        }
    }

    /**
     * Converts the given TCKTestResult object into a Status object.
     */
    private Status resultToStatus(TCKTestResult result) {
        Status rStatus = null;
        switch (result.getType()) {
            case TCKTestResult.PASSED:
                rStatus = Status.passed("Test passed");
                break;
            case TCKTestResult.FAILED:
                String failureDescription = formatFailure(result.getAssertionID(),result.getReason());
                rStatus = Status.failed(failureDescription);
                break;
            case TCKTestResult.ERROR:
                rStatus = Status.error(result.getReason());
                break;
            default: throw new IllegalArgumentException("Illegal result type: "+result.getType());
        }
        return rStatus;
    }

    /**
     * Creates a formatted message based on the given assertionID and failure message
     */
    private String formatFailure(int assertionID, String message) {
        StringBuffer rBuf = new StringBuffer();
        rBuf.append("Failed assertion ID: ").append(assertionID);
        rBuf.append("\nFailure context/reason: ").append(message);
        return rBuf.toString();
    }

    // -- Private state -- //

    private ObjectName mgmtMBeanName;
    private MBeanFacadeImpl mBeanFacade;
    private MBeanProxyFactory mBeanProxyFactory;
    private TCKResourceTestInterfaceProxy resourceInterface;
    private Properties envParams;
    private Properties testParams;
    private Logable log;
    private int defaultTimeout;
    private int testTimeout;
    private TCKEventSenderRemote remoteSender;
    private SleeTCKTestUtils testUtils;
    private RMIObjectChannel rmiObjectChannel;
    private MessageHandlerRegistry messageHandlerRegistry;
    
    // -- Private static state -- //

    /**
     * This static flag is set when the script detects that the SLEE is stuck in the
     * stopping state, to allow detection of the condition during subsequent tests.
     */
    private static boolean isSleeStuckInStoppingState = false;

}

