/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.infra.jmx;

import java.util.Set;
import java.util.Map;
import java.util.HashMap;
import java.util.Iterator;
import javax.management.*;
import java.rmi.Remote;
import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import com.opencloud.sleetck.lib.TCKTestErrorException;
import com.opencloud.sleetck.lib.infra.eventbridge.TCKEventBroadcaster;

public class MBeanFacadeAgentRemoteImpl extends UnicastRemoteObject implements MBeanFacadeAgentRemote {

    public MBeanFacadeAgentRemoteImpl() throws RemoteException {
        notificationListeners = new HashMap();
        listenerSources = new HashMap();
    }

    // -- Public methods -- //

    public void setMBeanServer(MBeanServer server) {
        this.server = server;
    }

    public void setEventBroadcaster(TCKEventBroadcaster broadcaster) {
        this.broadcaster = broadcaster;
    }

    // -- Implementation of MBeanFacadeRemote -- //

    public ObjectInstance createMBean(String className, ObjectName name)
            throws ReflectionException, InstanceAlreadyExistsException, MBeanRegistrationException,
                MBeanException, NotCompliantMBeanException, TCKTestErrorException {
        assertServer();
        return server.createMBean(className, name);
    }

    public ObjectInstance createMBean(String className, ObjectName name, ObjectName loaderName)
            throws ReflectionException, InstanceAlreadyExistsException, MBeanRegistrationException, MBeanException, NotCompliantMBeanException,
               InstanceNotFoundException, TCKTestErrorException {
        assertServer();
        return server.createMBean(className, name, loaderName) ;
    }

    public ObjectInstance createMBean(String className, ObjectName name, Object params[], String signature[])
        throws ReflectionException, InstanceAlreadyExistsException, MBeanRegistrationException, MBeanException,
                NotCompliantMBeanException, TCKTestErrorException {
        assertServer();
        return server.createMBean(className, name, params, signature);
    }

    public ObjectInstance createMBean(String className, ObjectName name, ObjectName loaderName, Object params[], String signature[])
        throws ReflectionException, InstanceAlreadyExistsException, MBeanRegistrationException, MBeanException, NotCompliantMBeanException,
               InstanceNotFoundException, TCKTestErrorException {
        assertServer();
        return server.createMBean(className, name, loaderName, params, signature);
    }

    public void unregisterMBean(ObjectName name) throws InstanceNotFoundException,
        MBeanRegistrationException, TCKTestErrorException {
        assertServer();
        server.unregisterMBean(name);
    }

    public ObjectInstance getObjectInstance(ObjectName name)
        throws InstanceNotFoundException, TCKTestErrorException {
        assertServer();
        return server.getObjectInstance(name);
    }

    public Set queryMBeans(ObjectName name, QueryExp query) throws TCKTestErrorException {
        assertServer();
        return server.queryMBeans(name, query);
    }

    public Set queryNames(ObjectName name, QueryExp query) throws TCKTestErrorException {
        assertServer();
        return server.queryNames(name, query);
    }

    public boolean isRegistered(ObjectName name) throws TCKTestErrorException {
        assertServer();
        return server.isRegistered(name);
    }


    public Integer getMBeanCount() throws TCKTestErrorException {
        assertServer();
        return server.getMBeanCount();
    }

    public Object getAttribute(ObjectName name, String attribute) throws
            MBeanException, AttributeNotFoundException, InstanceNotFoundException,
            ReflectionException, TCKTestErrorException {
        assertServer();
        return server.getAttribute(name, attribute);
    }


    public AttributeList getAttributes(ObjectName name, String[] attributes)
        throws InstanceNotFoundException, ReflectionException, TCKTestErrorException {
        assertServer();
        return server.getAttributes(name, attributes);
    }

    public void setAttribute(ObjectName name, Attribute attribute) throws
            InstanceNotFoundException, AttributeNotFoundException, InvalidAttributeValueException, MBeanException,
            ReflectionException, TCKTestErrorException {
        assertServer();
        server.setAttribute(name, attribute);
    }

    public AttributeList setAttributes(ObjectName name, AttributeList attributes)
        throws InstanceNotFoundException, ReflectionException, TCKTestErrorException {
        assertServer();
        return server.setAttributes(name, attributes);
    }

    public Object invoke(ObjectName name, String operationName, Object params[], String signature[])
        throws InstanceNotFoundException, MBeanException, ReflectionException, TCKTestErrorException {

        assertServer();
        if(_DEBUG_)debug("MBeanFacadeAgentRemoteImpl:invoke():"+name+","+operationName);
        Object rVal = server.invoke(name, operationName, params, signature);
        if(_DEBUG_)debug("MBeanFacadeAgentRemoteImpl:invoke(),returning "+rVal);

        ////////////////////////////////////////////////////////////////////////
        // NOTE: convert Void.TYPE to null - Void.TYPE doesn't serialize properly.
        //  The JMX specification doesn't specify whether invoke() should
        //  return null or Void.TYPE (the Class 'void') for a method with
        //  'void' return type. This is a permanent workaround.
        if(rVal == Void.TYPE) rVal = null;
        ////////////////////////////////////////////////////////////////////////

        return rVal;
    }

    public String getDefaultDomain() throws TCKTestErrorException {
        assertServer();
        return server.getDefaultDomain();
    }

    public void registerNotificationListener(NotificationListenerID id, ObjectName name, NotificationFilter filter)
        throws InstanceNotFoundException, RemoteException, TCKTestErrorException {
        assertServer();
        // create a listener proxy
        if(broadcaster == null) throw new TCKTestErrorException(
            "The operation failed because the event broadcaster reference was not set in the JMX agent.");
        NotificationListenerProxy proxy = new NotificationListenerProxy(broadcaster);
        synchronized (notificationListeners) {
            // Register the listener proxy with the mbean. The proxy is the
            // NotificationListener, the event id is the handback
            server.addNotificationListener(name, proxy, filter, id);
            // maintain references to the listener and target for deregistration purposes
            notificationListeners.put(id,proxy);
            listenerSources.put(id,name);
        }
    }

    public void deregisterNotificationListener(NotificationListenerID id, ObjectName name)
            throws InstanceNotFoundException, ListenerNotFoundException, RemoteException, TCKTestErrorException {
        assertServer();
        NotificationListener removed = null;
        // remove the listener from our local regsistries
        synchronized (notificationListeners) {
            removed = (NotificationListener)notificationListeners.remove(id);
            listenerSources.remove(id);
        }
        // remove the listener from the mbean
        if(removed != null) {
            server.removeNotificationListener(name, removed);
        }
    }

    public void deregisterAllNotificationListeners() throws RemoteException, TCKTestErrorException {
        assertServer();
        synchronized (notificationListeners) {
            // take a snapshot of the listener registry, then iterate over that -
            // as our actions will modify the maps themselves as we iterate
            Map listenersSnapshot = copyMap(listenerSources);
            Iterator iter = listenersSnapshot.keySet().iterator();
            while(iter.hasNext()) {
                NotificationListenerID id = (NotificationListenerID)iter.next();
                ObjectName source = (ObjectName)listenersSnapshot.get(id);
                try {
                    deregisterNotificationListener(id,source);
                } catch(InstanceNotFoundException infe) {
                    debug("WARNING: MBeanFacadeAgentRemoteImpl.deregisterAllNotificationListeners(): caught exception while deregistering listener of id "+id+":"+infe);
                } catch(ListenerNotFoundException lnfe) {
                    debug("WARNING: MBeanFacadeAgentRemoteImpl.deregisterAllNotificationListeners(): caught exception while deregistering listener of id "+id+":"+lnfe);
                }
            }
        }
    }

    public void addNotificationListener(ObjectName name, ObjectName listener, NotificationFilter filter, Object handback)
            throws InstanceNotFoundException, TCKTestErrorException {
        assertServer();
        server.addNotificationListener(name, listener, filter, handback);
    }

    public void removeNotificationListener(ObjectName name, ObjectName listener)
            throws InstanceNotFoundException, ListenerNotFoundException, TCKTestErrorException {
        assertServer();
        server.removeNotificationListener(name, listener);
    }

    public MBeanInfo getMBeanInfo(ObjectName name) throws
            InstanceNotFoundException, IntrospectionException, ReflectionException, TCKTestErrorException {
        assertServer();
        return server.getMBeanInfo(name);
    }

    public boolean isInstanceOf(ObjectName name, String className) throws InstanceNotFoundException, TCKTestErrorException {
        assertServer();
        return server.isInstanceOf(name, className);
    }

    // -- Private methods -- //

    /**
     * Convenience method which checks that the server
     * attribute is set to a non-null value, and throws
     * a TCKTestErrorException if it is not.
     */
    private void assertServer() throws TCKTestErrorException {
        if(server == null) throw new TCKTestErrorException(
            "The operation failed because the MBeanServer reference was not set in the JMX agent.");
    }

    /**
     * Makes a copy of the given map.
     * The caller is responsible for synchronization - the
     * given Map must not be modified while this method executes.
     */
    private Map copyMap(Map toClone) {
        Map rMap = new HashMap();
        Iterator keysIter = toClone.keySet().iterator();
        while(keysIter.hasNext()) {
            Object key = keysIter.next();
            Object value = toClone.get(key);
            rMap.put(key,value);
        }
        return rMap;
    }

    private void debug(String message) {
        System.err.println(message);
    }

    // -- Private state -- //

    private MBeanServer server;
    private TCKEventBroadcaster broadcaster;
    // a map of listener ID's to listeners
    private Map notificationListeners;
    // a map of listener ID's to notification sources - maintained alongside notificationListeners
    private Map listenerSources;

    private static final boolean _DEBUG_ = false;

}