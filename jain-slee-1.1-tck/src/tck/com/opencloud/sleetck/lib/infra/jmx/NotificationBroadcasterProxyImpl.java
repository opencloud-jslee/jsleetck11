/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.infra.jmx;

import javax.management.NotificationListener;
import javax.management.NotificationFilter;
import javax.management.InstanceNotFoundException;
import javax.management.ListenerNotFoundException;
import com.opencloud.sleetck.lib.testutils.jmx.MBeanFacade;
import com.opencloud.sleetck.lib.TCKCommunicationException;
import com.opencloud.sleetck.lib.TCKTestErrorException;
import javax.management.ObjectName;

public class NotificationBroadcasterProxyImpl implements NotificationBroadcasterProxy {

    public NotificationBroadcasterProxyImpl(ObjectName objName, MBeanFacade facade) {
        this.objName = objName;
        this.facade = facade;
    }

    public void addNotificationListener(
            NotificationListener listener, NotificationFilter filter, Object handback)
                throws InstanceNotFoundException, TCKCommunicationException, TCKTestErrorException {
        facade.addNotificationListener(objName, listener, filter, handback);
    }

    public void removeNotificationListener(NotificationListener listener)
            throws InstanceNotFoundException, ListenerNotFoundException,
                TCKCommunicationException, TCKTestErrorException {
        facade.removeNotificationListener(objName, listener);
    }

    private ObjectName objName;
    private MBeanFacade facade;

}
