/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.infra.eventbridge;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;

/**
 * Implements the remote interface used by the event
 * broadcaster to send events to the receiver.
 */
public class TCKEventReceiverRemoteImpl extends UnicastRemoteObject implements TCKEventReceiverRemote {

    public TCKEventReceiverRemoteImpl() throws RemoteException {}

    // -- Implementation of TCKEventReceiverRemote -- //

    public synchronized void receiveEvent(Object event, Object channelID) throws RemoteException {
        if(_DEBUG_)debug("TCKEventReceiverRemoteImpl:receiveEvent():event:"+event+",channelID:"+channelID);
        if(listener != null) listener.handleEvent(event, channelID);
    }

    public synchronized void receiveEvents(Object[] events, Object[] channelIDs) throws RemoteException {
        if(_DEBUG_)debug("TCKEventReceiverRemoteImpl:receiveEvent():events:"+events+",channelIDs:"+channelIDs);
        if(listener != null) {
            for (int i = 0; i < events.length; i++) {
                listener.handleEvent(events[i],channelIDs[i]);
            }
        }
    }

    // -- Public methods -- //

    public synchronized void setListener(TCKEventListener listener) {
        this.listener = listener;
    }

    // -- Private methods -- //

    private void debug(String text) {
        System.err.println(text);
    }

    private TCKEventListener listener;

    private static final boolean _DEBUG_ = false;

}