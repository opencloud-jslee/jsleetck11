/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.infra.jmx;

import javax.management.Notification;
import javax.management.NotificationListener;
import com.opencloud.sleetck.lib.infra.eventbridge.TCKEventBroadcaster;

/**
 * Represents the client side listener in the
 * event source machine.
 */
public class NotificationListenerProxy implements NotificationListener {

    public NotificationListenerProxy(TCKEventBroadcaster broadcaster) {
        this.broadcaster = broadcaster;
    }

    // -- Implementation of NotificationListener -- //

    /**
     * Note: this method assumes that the handback object is the
     *  serializable event channel ID
     */
    public void handleNotification(Notification notification, Object handback) {
        broadcaster.fireEvent(notification, handback);
    }

    private TCKEventBroadcaster broadcaster;

}