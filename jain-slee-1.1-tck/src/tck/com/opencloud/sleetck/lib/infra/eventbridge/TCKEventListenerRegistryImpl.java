/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.infra.eventbridge;

import java.util.Map;
import java.util.HashMap;
import java.util.Vector;
import java.util.Iterator;

/**
 * Components wishing to receive TCK events register with
 * this class.
 */
public class TCKEventListenerRegistryImpl implements TCKEventListenerRegistry, TCKEventListener {

    public TCKEventListenerRegistryImpl() {
        listenersMap = new HashMap();
    }

    // -- Implementation of TCKEventListenerRegistry -- //

    /**
     * Registers the given listener for the given event channel.
     */
    public synchronized void addListener(TCKEventListener listener, Object channelID) {
        Vector v = (Vector)listenersMap.get(channelID);
        if(v == null) {
            v = new Vector();
            listenersMap.put(channelID,v);
        }
        v.add(listener);
    }

    /**
     * Deregisters the given listener for the given event channel.
     */
    public synchronized void removeListener(TCKEventListener listener, Object channelID) {
        Vector v = (Vector)listenersMap.get(channelID);
        if(v != null) {
            v.remove(listener);
        }
    }

    /**
     * Removes all listeners from the registry.
     */
    public synchronized void removeAllListeners() {
        Iterator channelsIter = listenersMap.keySet().iterator();
        while(channelsIter.hasNext()) {
            Vector v = (Vector)listenersMap.get(channelsIter.next());
            v.removeAllElements();
            channelsIter.remove();
        }
    }

    // -- Implementation of TCKEventListener -- //

    public synchronized void handleEvent(Object event, Object channelID) {
        Vector v = (Vector)listenersMap.get(channelID);
        if(v != null) {
            for (int i = 0; i < v.size(); i++) {
                TCKEventListener listener = (TCKEventListener)v.elementAt(i);
                listener.handleEvent(event, channelID);
            }
        }
    }

    // -- Private state -- //

    private Map listenersMap;


}