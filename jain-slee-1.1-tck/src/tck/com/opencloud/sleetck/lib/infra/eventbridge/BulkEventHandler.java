/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.infra.eventbridge;

/**
 * Implemented by event bridge components which handle
 * TCK events one by one or in event bundles.
 */
public interface BulkEventHandler extends TCKEventListener {

    /**
     * Handles the given event bundle.
     * @param events the array of events to be handled
     * @param channelIDs the array of channel IDs which corresponds to the
     *   events array
     */
    public void handleEvents(Object[] events, Object[] channelIDs);

}