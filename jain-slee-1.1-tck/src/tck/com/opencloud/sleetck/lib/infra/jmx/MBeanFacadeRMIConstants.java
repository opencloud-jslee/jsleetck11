/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.infra.jmx;

/**
 * This interface defines RMI related constants used 
 * by the MBeanFacade components.
 */
public interface MBeanFacadeRMIConstants {
    
    public static final String TCK_EVENT_SENDER_REMOTE   = "infra.jmx.TCKEventSender";
    public static final String MBEAN_FACADE_AGENT_REMOTE = "infra.jmx.MBeanFacadeAgent";

}