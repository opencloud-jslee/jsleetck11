/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.infra.testfinder;

import java.io.File;
import java.util.Vector;
import java.util.HashMap;
import java.util.Map;
import java.util.Iterator;
import java.net.URL;
import java.net.MalformedURLException;

import com.sun.javatest.TestFinder;
import com.sun.javatest.TestDescription;
import com.sun.javatest.util.I18NResourceBundle;

import com.opencloud.sleetck.lib.TCKTestErrorException;
import com.opencloud.sleetck.lib.testutils.ExceptionsUtil;

import org.xml.sax.SAXException;

/**
 * An implementation of TestFinder which searches for and reads
 * test descriptions in an xml format defined by the test description dtd.
 */
public class SleeTCKTestFinder extends TestFinder {

    // Note: the test description dtd must be placed in the root directory of the test suite
    private static final String DTD_RELATIVE_PATH = "slee-tck-test-description_1_0.dtd";

    /**
     * Constructs a new SleeTCKTestFinder
     * @param root the root file or directory of the test suite
     */
    public SleeTCKTestFinder(File root) throws TestFinder.Fault {
        // initialise the resource bundle before we generate any errors
        i18NResourceBundle = I18NResourceBundle.getBundleForClass(getClass());
        setRoot(root);

        // look for the DTD
        File localDTDFile = new File(getRootDir(),DTD_RELATIVE_PATH);
        String localDTDURI = null;
        if(localDTDFile.exists()) {
            try {
                localDTDURI = localDTDFile.toURL().toExternalForm();
            } catch(MalformedURLException e) {
                throw new TestFinder.Fault(i18NResourceBundle,DTD_LOCATION_ERROR,
                    "MalformedURLException which trying convert file "+
                    localDTDFile.getPath()+" to a URI: "+ExceptionsUtil.formatThrowable(e));
            }
        } else throw new TestFinder.Fault(i18NResourceBundle,DTD_LOCATION_ERROR,
                "DTD file not found at "+localDTDFile.getPath());

        // initialise the reader
        try {
            descriptionReader = new TestDescriptionReader(root,localDTDURI);
        } catch(SAXException e) {
            throw new TestFinder.Fault(i18NResourceBundle,XML_READER_ERROR,ExceptionsUtil.formatThrowable(e));
        }

        testsFound = new Vector();
        filesFound = new Vector();
    }

    // TestFinder methods

    /**
     * Implements the scan() method defined in TestFinder to scan directories and files
     */
    protected void scan(File file) {
        // clear state
        testsFound.clear();
        filesFound.clear();
        // scan the file or directory
        if(file.isDirectory()) scanDirectory(file);
        else scanFile(file);
    }

    /**
     * Over-writes getFiles() in TestFinder to return the files found
     * during the last call to scan()
     */
    public File[] getFiles() {
        File[] rFiles = new File[filesFound.size()];
        filesFound.copyInto(rFiles);
        return rFiles;
    }

    /**
     * Over-writes getTests() in TestFinder to return the test descriptions found
     * during the last call to scan()
     */
    public TestDescription[] getTests() {
        TestDescription[] rDescriptions = new TestDescription[testsFound.size()];
        testsFound.copyInto(rDescriptions);
        return rDescriptions;
    }

    // Private methods

    /**
     * If the given file is an xml file, this method tries to read
     * it as a test description. It will continue to try to read it
     * until it detects that the file is not a test description, or
     * detects an error.
     * If the file is a valid test description, and no errors are encountered while
     * reading it, a new TestDescription will be added to testsFound.
     */
    private void scanFile(File file) {
        try {
            TestDescription testDescription = descriptionReader.readTestDescription(file);
            if(testDescription != null) testsFound.addElement(testDescription);
        } catch(Exception e) {
            error(i18NResourceBundle,TEST_DESCRIPTION_ERROR,ExceptionsUtil.formatThrowable(e));
        }
    }

    /**
     * Scans the given directory.
     * Each file under the directory is added to filesFound if
     * (1) it is a directory, or
     * (2) ends with the suffix ".xml".
     */
    private void scanDirectory(File dir) {
        File[] children = dir.listFiles();
        for (int i = 0; i < children.length; i++) {
            if (children[i].isDirectory() ||
                children[i].getPath().endsWith(".xml")) filesFound.addElement(children[i]);
        }
    }

    // Private state

    private TestDescriptionReader descriptionReader;
    private Vector testsFound;
    private Vector filesFound;
    private I18NResourceBundle i18NResourceBundle;

    // Message keys

    private static final String TEST_DESCRIPTION_ERROR = "SleeTCKTestFinder.scanFile.test-description-error";
    private static final String DTD_LOCATION_ERROR = "SleeTCKTestFinder.init.dtd-location-error";
    private static final String XML_READER_ERROR = "SleeTCKTestFinder.init.xml-reader-error";

}
