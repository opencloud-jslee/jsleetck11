/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.infra.sleeplugin;

import javax.slee.management.SleeProviderFactory;
import javax.slee.management.SleeProvider;
import javax.slee.management.PeerUnavailableException;
import javax.management.MBeanServer;
import javax.management.MBeanRegistration;
import javax.management.ObjectName;
import javax.management.MalformedObjectNameException;
import javax.management.MBeanInfo;
import javax.management.loading.MLet;
import java.rmi.registry.Registry;
import java.rmi.registry.LocateRegistry;
import java.rmi.RemoteException;
import com.opencloud.sleetck.lib.infra.jmx.MBeanFacadeAgentRemote;
import com.opencloud.sleetck.lib.infra.jmx.MBeanFacadeAgentRemoteImpl;
import com.opencloud.sleetck.lib.infra.jmx.MBeanFacadeRMIConstants;
import com.opencloud.sleetck.lib.infra.eventbridge.TCKEventBroadcaster;
import com.opencloud.sleetck.lib.infra.eventbridge.TCKEventSenderRemote;
import com.opencloud.sleetck.lib.infra.eventbridge.TCKEventSenderRemoteImpl;

/**
 * The SleeTCKPlugin is the initial tck MBean loaded into
 * the MBeanServer of the SLEE.
 * It is responsible for setting up the MBean proxy
 * (a limited JMX RMI protocol adaptor).
 */
public class SleeTCKPlugin implements SleeTCKPluginMBean, MBeanRegistration {

    public SleeTCKPlugin(int rmiPort, String sleeProviderClass) {
        this.rmiPort=rmiPort;
        this.sleeProviderClass=sleeProviderClass;
    }

    // -- Implementation of SleeTCKPluginMBean -- //

    public ObjectName getSleeManagementMBean() {
        // guaranteed to be non-null, as we checked for null during preRegister()
        return mgmtMBeanName;
    }

    // -- Implementation of MBeanRegistration -- //

    /**
     * Creates an object name if not set, sets the SleeManagementMBean name,
     * and starts the rmi protocol adaptor.
     */
    public ObjectName preRegister (MBeanServer server, ObjectName name) throws Exception {
        // set our ObjectName
        if(name == null) {
            name = new ObjectName(server.getDefaultDomain()+SleeTCKPluginMBean.JMX_ID);
        }

        // get the ObjectName of the slee management mbean via the SleeProvider
        SleeProvider provider = SleeProviderFactory.getSleeProvider(sleeProviderClass);
        mgmtMBeanName = provider.getSleeManagementMBean();
        if(mgmtMBeanName == null) throw new NullPointerException("Provider returned a null ObjectName for the SleeManagementMBean");

        // -- Register the MBean facade agent and supporting components -- //

        // create an rmi registry on the given port
        if(_DEBUG_)debug("SleeTCKPlugin: Locating registry at port "+rmiPort+" on localhost...");
        Registry rmiRegistry = LocateRegistry.getRegistry(rmiPort);
        if(_DEBUG_)debug("SleeTCKPlugin: Registry located");

        // create and register the event sender
        TCKEventSenderRemoteImpl tckEventSenderRemote = new TCKEventSenderRemoteImpl();
        rmiRegistry.rebind(MBeanFacadeRMIConstants.TCK_EVENT_SENDER_REMOTE,tckEventSenderRemote);
        if(_DEBUG_)debug("SleeTCKPlugin: Bound event sender");

        // create and initialise the event broadcaster
        TCKEventBroadcaster broadcaster = new TCKEventBroadcaster();
        broadcaster.setBulkEventHandler(tckEventSenderRemote);

        // create, initialise and bind the mbean facade agent
        MBeanFacadeAgentRemoteImpl facadeAgent = new MBeanFacadeAgentRemoteImpl();
        facadeAgent.setMBeanServer(server);
        facadeAgent.setEventBroadcaster(broadcaster);
        rmiRegistry.rebind(MBeanFacadeRMIConstants.MBEAN_FACADE_AGENT_REMOTE,facadeAgent);
        if(_DEBUG_)debug("SleeTCKPlugin: Bound MBean facade agent");

        return name;
    }

    /**
     * Empty implementation
     */
    public void postRegister (Boolean registrationDone) {}

    /**
     * Empty implementation
     */
    public void preDeregister() throws Exception {}

    /**
     * Empty implementation
     */
    public void postDeregister() {}

    // -- Private methods -- //

    private void debug(String message) {
        System.err.println(message);
    }

    // -- Private state -- //

    private int rmiPort;
    private String sleeProviderClass;
    private ObjectName mgmtMBeanName;

    private static boolean _DEBUG_ = true;

}