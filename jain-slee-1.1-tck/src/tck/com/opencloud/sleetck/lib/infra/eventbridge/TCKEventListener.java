/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.infra.eventbridge;

/**
 * Components wishing to receive TCK events should implement this
 * interface and register with the TCKEventListenerRegistry.
 */
public interface TCKEventListener {

    /**
     * Handles the given event.
     */
    public void handleEvent(Object event, Object channelID);

}