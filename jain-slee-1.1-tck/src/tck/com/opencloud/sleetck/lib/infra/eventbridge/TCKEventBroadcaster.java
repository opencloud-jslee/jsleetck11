/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.infra.eventbridge;

import java.util.Vector;

/**
 * Singleton component which event generating components use
 * to send events via the event bridge.<br>
 * Uses a threading/queueing model which frees the event firing thread,
 * and delivers pending events in bulk as receiver resources become free.
 *
 * The event queue can overflow in the case where events are generated faster
 * than they can be processed. If this is scenario applies - the setMaxQueueLength()
 * method can be used to limit the size of the eventQ, and block event firing
 * threads while this limit is exceeded.
 */
public class TCKEventBroadcaster {

    public TCKEventBroadcaster() {
        maxQueueLength = -1;
        eventQ = new Vector();
        idQ = new Vector();
        // start the worker thread - which dequeues and sends events
        new Thread(new BroadcasterTask()).start();
    }

    public void setBulkEventHandler(BulkEventHandler handler) {
        this.handler = handler;
    }

    /**
     * From the caller's point of view - asynchronously fires the given event.
     * The implementation adds the given event to the pending event queue.
     * If a Q limit is imposed, the calling thread is blocked until
     * the Q size falls below the limit.
     */
    public synchronized void fireEvent(Object event, Object channelID) {
        if(_DEBUG_)debug("TCKEventBroadcaster:fireEvent():waiting for lock");
        synchronized (eventQ) {
            // if a Q limit is imposed, wait till the size falls below the limit
            if(maxQueueLength > 0) {
                while(eventQ.size() > maxQueueLength) {
                    if(_DEBUG_)debug("TCKEventBroadcaster:Q limit exceed: waiting for queue to be reduced");
                    try {
                        eventQ.wait();
                    } catch(InterruptedException ie) {
                        // no action required
                    }
                }
                if(_DEBUG_)debug("TCKEventBroadcaster:Q size is within limit");
            }
            if(_DEBUG_)debug("TCKEventBroadcaster:fireEvent():adding event to Q");
            eventQ.addElement(event);
            idQ.addElement(channelID);
            eventQ.notifyAll();
        }
    }

    /**
     * Sets the upper limit on the size of the event queue.
     * When this limit is reached, all event firing threads
     * are blocked until the queue length falls below
     * the limit again.
     * Any value below 1 is interpreted as 'no limit'.
     */
    public void setMaxQueueLength(int maxQueueLength) {
        this.maxQueueLength = maxQueueLength;
    }

    /**
     * Runnable task which removes events from the event queue and
     * forwards them to the broadcaster.
     */
    private class BroadcasterTask implements Runnable {
        public void run() {
            if(_DEBUG_)debug("TCKEventBroadcaster:worker thread started");
            Object soloEvent;
            Object soloID;
            Object[] eventBundle;
            Object[] idBundle;

            for(;;) {
                soloEvent = null;
                soloID = null;
                eventBundle = null;
                idBundle = null;

                synchronized (eventQ) {
                    if(_DEBUG_)debug("TCKEventBroadcaster:Waiting for event Q to become non-empty");
                    while(eventQ.isEmpty()) {
                        try {
                            eventQ.wait();
                        } catch(InterruptedException ie) {
                            // no action required
                        }
                    }
                    if(_DEBUG_)debug("TCKEventBroadcaster:Found "+eventQ.size()+" events in the Q");
                    // copy the events to method local storage
                    if(eventQ.size() == 1) {
                        soloEvent = eventQ.firstElement();
                        soloID = idQ.firstElement();
                    } else {
                        eventBundle = eventQ.toArray();
                        idBundle = idQ.toArray();
                    }
                    // remove the copied events from the Q
                    eventQ.clear();
                    idQ.clear();
                    eventQ.notifyAll();
                }
                if(_DEBUG_)debug("TCKEventBroadcaster:Sending events");
                if(handler == null) {
                    debug("WARNING: TCKEventBroadcaster could not send event because the event handler was not set");
                }
                if(soloEvent != null)handler.handleEvent(soloEvent,soloID);
                else handler.handleEvents(eventBundle,idBundle);
            }
        }

    }

    private void debug(String message) {
        System.err.println(message);
    }

    private BulkEventHandler handler;
    // queue for pending events. also used as the synchronization lock
    private Vector eventQ;
    // queue for ids - matching pending events
    private Vector idQ;

    private int maxQueueLength;

    private static final boolean _DEBUG_ = false;

}