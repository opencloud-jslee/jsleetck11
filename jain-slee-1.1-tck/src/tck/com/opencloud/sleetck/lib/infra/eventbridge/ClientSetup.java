/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.infra.eventbridge;

import java.rmi.RemoteException;
import com.opencloud.sleetck.lib.infra.eventbridge.TCKEventReceiverRemoteImpl;
import com.opencloud.sleetck.lib.infra.eventbridge.TCKEventSenderRemote;
import com.opencloud.sleetck.lib.infra.eventbridge.TCKEventListenerRegistry;

/**
 * Utility class to help setup the client (event receiver) side
 * of the event bridge.
 */
public class ClientSetup {

    /**
     * Static utility method to help setup the client components
     * of the event bridge. Takes a remote interface to the event 'server',
     * and returns a listener registry.
     * @param connectTo the server interface to connect to
     * @return a listener registry which event listeners can register with
     */
    public static TCKEventListenerRegistry setupClient(TCKEventSenderRemote connectTo) throws RemoteException {

        TCKEventReceiverRemoteImpl tckEventReceiverRemoteImpl = new TCKEventReceiverRemoteImpl();
        TCKEventListenerRegistryImpl listenerRegistry = new TCKEventListenerRegistryImpl();
        tckEventReceiverRemoteImpl.setListener(listenerRegistry);
        connectTo.connectReceiver(tckEventReceiverRemoteImpl);
        return listenerRegistry;

    }

}