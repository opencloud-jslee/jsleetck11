/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.infra.testfinder;

import com.sun.javatest.TestDescription;

import java.io.File;
import java.util.Map;

import org.xml.sax.SAXException;
import org.xml.sax.InputSource;
import org.xml.sax.XMLReader;
import org.xml.sax.helpers.XMLReaderFactory;

import com.opencloud.sleetck.lib.TCKTestErrorException;
import com.opencloud.sleetck.lib.testutils.ExceptionsUtil;

/**
 * The TestDescriptionReader can read test TCK test description files.
 * This class expects a valid xml parser to be specified in the
 * org.xml.sax.driver system property.
 */
public class TestDescriptionReader {

    /**
     * Constructs a new TestDescriptionReader
     * @throws SAXException if an error occurs while trying to initialise the XMLReader
     */
    public TestDescriptionReader(File testSuiteRoot, String localDTDURI) throws SAXException {
        this.testSuiteRoot=testSuiteRoot;
        reader = XMLReaderFactory.createXMLReader();
        fileHandler = new TestDescriptionFileHandler(localDTDURI);
        reader.setContentHandler(fileHandler);
        reader.setDTDHandler(fileHandler);
        reader.setEntityResolver(fileHandler);
        reader.setErrorHandler(fileHandler);
    }

    /**
     * Tries to read the given file as a test description.
     * If the file is a valid test description, a TestDescription will
     * be returned, or an Exception will be thrown to indicate an unexpected error.
     * If the file is not a test description, null will be returned
     * or an Exception will be thrown to indicate an unexpected error.
     * @param file the file to try to read
     * @return a TestDescription if the file is a test description or null if it is not
     * @throws TCKTestErrorException if an unexpected error occurs
     */
    public TestDescription readTestDescription(File file) throws TCKTestErrorException {
        TestDescription rDescription = null;
        if(file.getPath().endsWith(".xml")) {
            try {
                reader.parse(file.toURL().toExternalForm());
                // we didn't throw an Exception, therefore we just read a valid test description
                Map params = fileHandler.getTestParameters();
                rDescription = new TestDescription(testSuiteRoot,file,params);
            } catch(Exception e) {
                // we expect a NotATestDescriptionException enclosed in a SAXException for xml
                // files of other types (e.g. build files)
                if(!(e instanceof SAXException) ||
                   !(((SAXException)e).getException() instanceof NotATestDescriptionException)) {
                    throw new TCKTestErrorException("Caught unexpected error while trying to read a test description",e);
                }
            }
        }
        return rDescription;
    }

    private TestDescriptionFileHandler fileHandler;
    private XMLReader reader;
    private File testSuiteRoot;

}
