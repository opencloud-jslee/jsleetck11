/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib;

import com.opencloud.sleetck.lib.TCKTestErrorException;

/**
 * Exception class for communication errors within the TCK.
 * Use to wrap exceptions such as java.rmi.RemoteExceptions
 * and java.io.IOExceptions.
 */
public class TCKCommunicationException extends TCKTestErrorException {

    public TCKCommunicationException(String message) {
        super(message);
    }

    public TCKCommunicationException(String message, Exception nestedException) {
        super(message,nestedException);
    }

}