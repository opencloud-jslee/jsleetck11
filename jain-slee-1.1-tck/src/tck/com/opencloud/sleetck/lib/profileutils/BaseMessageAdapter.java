/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.profileutils;

import java.rmi.RemoteException;
import java.util.HashMap;
import java.util.Iterator;

import com.opencloud.logging.Logable;
import com.opencloud.sleetck.lib.rautils.MessageHandler;

public class BaseMessageAdapter implements MessageHandler {

    private Logable log;

    public BaseMessageAdapter(Logable log) {
        this.log = log;
    }

    private boolean checkMsgClass(HashMap map) {
        Integer msgClass = (Integer)map.get(BaseMessageConstants.MESSAGE_CLASS_KEY);
        if (msgClass==null || msgClass.intValue()!=BaseMessageConstants.MESSAGE_CLASS)
            return false;

        return true;
    }

    public void onException(Exception e) { onSetError("Exception occured: ", e); }

    public boolean handleMessage(Object message) throws RemoteException {

        log.finer("Received message: "+message);

        if (message instanceof Exception) {
            onException((Exception)message);
            return true;
        }
        else if (message instanceof HashMap) {
            HashMap map = (HashMap)message;
            if (!checkMsgClass(map))
                return false;

            try {
                int type = ((Integer)map.get(BaseMessageConstants.TYPE)).intValue();
                String msg;
                Exception e;
                int assertionID;

                switch (type) {
                case BaseMessageConstants.TYPE_FAILURE:
                    assertionID = ((Integer)map.get(BaseMessageConstants.ID)).intValue();
                    msg = (String)map.get(BaseMessageConstants.MSG);
                    e = (Exception)map.get(BaseMessageConstants.CAUSE);
                    onSetFailed(assertionID, msg, e);
                    return true;
                case BaseMessageConstants.TYPE_SUCCESS:
                    assertionID = ((Integer)map.get(BaseMessageConstants.ID)).intValue();
                    msg = (String)map.get(BaseMessageConstants.MSG);
                    onSetPassed(assertionID, msg);
                    return true;
                case BaseMessageConstants.TYPE_LOG_MSG:
                    msg = (String) map.get(BaseMessageConstants.MSG);
                    onLogCall(msg);
                    return true;
                case BaseMessageConstants.TYPE_ERROR:
                    msg = (String)map.get(BaseMessageConstants.MSG);
                    e = (Exception)map.get(BaseMessageConstants.CAUSE);
                    onSetError(msg, e);
                    return true;
                }
            }
            catch (NullPointerException e) {
                log.warning("NullPointerException occured while trying to read fields of message from RMIObjectChannel. The msg contents were:");
                Iterator iter = map.keySet().iterator();
                while (iter.hasNext()) {
                    Object key = iter.next();
                    Object value = map.get(key);
                    log.warning("Key: "+key+" Value:"+value);
                }
                log.warning("Thrown exception: ");
                log.warning(e);
                return true;
            }
        }

        return false;
    }

    public void onSetPassed(int assertionID, String msg) {}
    public void onSetFailed(int assertionID, String msg, Exception e) {}
    public void onLogCall(String msg) {}
    public void onSetError(String msg, Exception e) {}

}
