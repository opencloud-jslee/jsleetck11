/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.profileutils;

import java.rmi.RemoteException;
import java.security.AccessController;
import java.security.PrivilegedAction;
import java.util.HashMap;

import com.opencloud.logging.Logable;
import com.opencloud.sleetck.lib.rautils.RMIObjectChannel;

public class BaseMessageSender {

    private RMIObjectChannel out;
    private Logable log;

    public BaseMessageSender(RMIObjectChannel out, Logable log) {
        this.out = out;
        this.log = log;
    }

    public HashMap getBaseConfig() {
        HashMap map = new HashMap();
        map.put(BaseMessageConstants.MESSAGE_CLASS_KEY, new Integer(BaseMessageConstants.MESSAGE_CLASS));
        return map;
    }

    public void sendException(Exception e) {
        send(e);
    }

    public void sendFailure(int assertionID, String msg, Exception e) {
        HashMap map = getBaseConfig();
        map.put(BaseMessageConstants.TYPE, new Integer(BaseMessageConstants.TYPE_FAILURE));
        map.put(BaseMessageConstants.ID, new Integer(assertionID));
        if (msg!=null)
            map.put(BaseMessageConstants.MSG, msg);
        if (e!=null)
            map.put(BaseMessageConstants.CAUSE, e);
        send(map);
    }

    public void sendFailure(int assertionID, String msg) {
        sendFailure(assertionID, msg, null);
    }

    public void sendSuccess(int assertionID, String msg) {
        HashMap map = getBaseConfig();
        map.put(BaseMessageConstants.TYPE, new Integer(BaseMessageConstants.TYPE_SUCCESS));
        map.put(BaseMessageConstants.ID, new Integer(assertionID));
        if (msg!=null)
            map.put(BaseMessageConstants.MSG, msg);
        send(map);
    }

    public void sendError(String msg) {
        sendError(msg, null);
    }

    public void sendError(String msg, Exception e) {
        HashMap map = getBaseConfig();
        map.put(BaseMessageConstants.TYPE, new Integer(BaseMessageConstants.TYPE_ERROR));
        if (msg!=null)
            map.put(BaseMessageConstants.MSG, msg);
        if (e!=null)
            map.put(BaseMessageConstants.CAUSE, e);
        send(map);
    }

    private void send(final Object obj) {
        AccessController.doPrivileged(new PrivilegedAction() {
            public Object run() {
                if (out == null) {
                    log.warning("Attempted to write to the RMIObjectChannel before it was initialized");
                    return null;
                }
                try {
                    out.writeObject(obj);
                } catch (RemoteException e) {
                    log.warning("Exception occured when writing to RMIObjectChannel: ");
                    log.warning(e);
                }
                return null;
            }
        });
    }

    public void sendLogMsg(String msg) {
        HashMap map = getBaseConfig();
        map.put(BaseMessageConstants.TYPE, new Integer(BaseMessageConstants.TYPE_LOG_MSG));
        if (msg!=null)
            map.put(BaseMessageConstants.MSG, msg);
        send(map);
    }

    public void sendCustomMessage(int type, HashMap map) {
        map.put(BaseMessageConstants.TYPE, new Integer(type));
        send(map);
    }

}
