/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.profileutils;

public interface BaseMessageConstants {
    public static final int TYPE_FAILURE = 0;
    public static final int TYPE_SUCCESS = 1;
    public static final int TYPE_ERROR = 2;
    public static final int TYPE_LOG_MSG = 3;

    public static final String TYPE = "Type";
    public static final String MSG = "Msg";
    public static final String ID = "ID";
    public static final String CAUSE = "Cause";

    public static final int MESSAGE_CLASS = 1;
    public static final String MESSAGE_CLASS_KEY = "MsgClass";
}
