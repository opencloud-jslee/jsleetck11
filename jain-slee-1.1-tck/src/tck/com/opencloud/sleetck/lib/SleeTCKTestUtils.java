/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib;

import com.opencloud.sleetck.lib.testutils.jmx.*;
import com.opencloud.sleetck.lib.rautils.MessageHandlerRegistry;
import com.opencloud.sleetck.lib.rautils.RMIObjectChannel;
import com.opencloud.sleetck.lib.resource.testapi.TCKResourceTestInterface;
import com.opencloud.sleetck.lib.TCKTestErrorException;
import javax.slee.management.DeployableUnitID;
import javax.slee.management.ResourceUsageMBean;
import javax.slee.ServiceID;
import com.opencloud.logging.Logable;
import java.util.Properties;
import javax.management.ObjectName;

/**
 * Main utility interface for SleeTCKTests.
 */
public interface SleeTCKTestUtils {

    // -- Environment related methods -- //

    /**
     * Access method for the test parameters -
     * a map of the parameters specified in the TestDescription.
     * The test description values are defined in the html test description
     * file for each test.
     * The standard keys are defined in
     * com.opencloud.sleetck.lib.DescriptionKeys.
     */
    public Properties getTestParams();

    /**
     * Access method for the test environment parameters -
     * a map of the parameters specified in the TestEnvironment.
     * The test environment values are defined via the JavaTest interview.
     * The relevant keys are defined in
     * com.opencloud.sleetck.lib.EnvironmentKeys.
     */
    public Properties getEnvParams();

    /**
     * Convenience method which returns the default time out specified in the
     * configuration interview.
     */
    public int getDefaultTimeout();

    /**
     * Returns the timeout to use for the test.
     * If timeout-multiplier is specified in the test description, the timeout for the test
     * is the default timeout multiplied by timeout-multiplier.
     * Otherwise, the test timeout is the same as the default timeout.
     */
    public int getTestTimeout();

    /**
     * Access method for the logger - for logging messages
     * and errors.
     */
    public Logable getLog();

    // -- SLEE MBean related methods -- //

    /**
     * Access method for the MBean facade -
     * a facade to the MBeanServer which hosts
     * the SleeManagement interfaces.
     */
    public MBeanFacade getMBeanFacade();

    /**
     * Returns an MBeanProxyFactory which the test can use
     * to create proxies to the SLEE MBeans.
     */
    public MBeanProxyFactory getMBeanProxyFactory();

    /**
     * Access method for the slee management mbean object name.
     */
    public ObjectName getSleeManagementMBeanName();

    /**
     * Convenience method which returns a SleeManagementMBeanProxy
     */
    public SleeManagementMBeanProxy getSleeManagementMBeanProxy() throws TCKTestErrorException;

    /**
     * Returns the full URL of the deployable unit jar, as the concatenation of
     * the base URL of the deployement units tree and the given relative URL.
     */
    public String getDeploymentUnitURL(String relativeURL);

    /**
     * Convenience method which returns a DeploymentMBeanProxy
     */
    public DeploymentMBeanProxy getDeploymentMBeanProxy() throws TCKTestErrorException;

    /**
     * Convenience method for installing SLEE components into the container.
     * @param relativePath the path of the deployable unit jar, relative to the deployable units tree
     * @return the DeployableUnitID of the deployable unit
     * @throws TCKTestErrorException to wrap any exception generated during the call to install()
     */
    public DeployableUnitID install(String relativePath) throws TCKTestErrorException;

    /**
     * Convenience method which uninstalls all the components installed via this interface,
     * which are still installed in the container.
     * Notes: (1) this method will only uninstall components installed via this interface;
     * (2) this method will only try to uninstall components which are current installed;
     * (3) all services in the deployable unit must be deactivated before calling this method;
     * (4) this method will wait for services in the STOPPING state to reach the INACTIVE state
     *  before uninstalling them, or will throw an OperationTimedOutException if the timeout is
     *  reached first
     * (5) this method may catch a number of Exceptions; only the last one caught will be rethrown
     * @return true only if one or more components were uninstalled during the call
     * @throws OperationTimedOutException if a stopping service does not stop before the timeout
     * @throws TCKTestErrorException to wrap any exception generated during the call to uninstall(),
     *  or any non-recoverable Exception thrown while trying to wait for services to stop
     */
    public boolean uninstallAll() throws TCKTestErrorException;

    /**
     * Convenience method for activating all services in a deployable unit.
     * @param duID the id of the deployable unit containing the services to deploy
     * @param setTraceLevel whether to set the trace level for each SBB in each service.
     *  If true - sets the trace level for each Sbb to the trace level set in the configuration
     *  interview, unless the trace level in the interview is set to Level.OFF. If false, or
     *  if the level is set to Level.OFF, no trace level is set for the SBBs.
     *  Tests should specify 'true' unless the test is testing the tracing system.
     * @return the ServiceIDs of all activated services
     * @throws TCKTestErrorException to wrap any exception generated by the MBean calls
     */
    public ServiceID[] activateServices(DeployableUnitID duID, boolean setTraceLevel) throws TCKTestErrorException;

    /**
     * Convenience method which deactivates all services activated via this interface,
     * which are currently active.
     * Notes: (1) this method will only deactivate services activated via this interface;
     * (2) this method will only try to deactivate services which are currently active
     * (3) this method may catch a number of Exceptions; only the last one caught will be rethrown
     * @return true only if one or more services were deactivated during the call
     * @throws TCKTestErrorException to wrap any exception generated by the MBean calls
     */
    public boolean deactivateAllServices() throws TCKTestErrorException;

    /**
     * Convenience method which returns a ServiceManagementMBeanProxy
     */
    public ServiceManagementMBeanProxy getServiceManagementMBeanProxy() throws TCKTestErrorException;

    /**
     * Convenience method which returns a new ResourceUsageMBeanProxy for the
     * Resource Adaptor Entity specified.
     */
    public ResourceUsageMBeanProxy getResourceUsageMBeanProxy(String entityName) throws TCKTestErrorException;
    
    /**
     * Convenience method which returns a ResourceManagementMBeanProxy
     */
    public ResourceManagementMBeanProxy getResourceManagementMBeanProxy() throws TCKTestErrorException;
    
    /**
     * Convenience method which returns a TraceMBeanProxy
     */
    public TraceMBeanProxy getTraceMBeanProxy() throws TCKTestErrorException;

    /**
     * Convenience method which returns an AlarmMBeanProxy
     */
    public AlarmMBeanProxy getAlarmMBeanProxy() throws TCKTestErrorException;

    /**
     * Removes all RA Entities (except for the main TCK RA) which have a vendor
     * string indicating they belong to the TCK.
     */
    public void removeRAEntities() throws TCKTestErrorException;
   
    // -- Resource related methods -- //

    /**
     * Returns an interface to the TCK SLEE resource.
     */
    public TCKResourceTestInterface getResourceInterface();

    /**
     * Returns an interface to the RMIObjectChannel.
     */
    public RMIObjectChannel getRMIObjectChannel();

    /**
     * Returns an interface to the MessageHandlerRegistry.
     */
    public MessageHandlerRegistry getMessageHandlerRegistry();
    
    /**
     * Deactivates the specified Resource Adaptor Entity. 
     */
    public void deactivateResourceAdaptorEntity(String raEntity) throws TCKTestErrorException;
    
    /**
     * Deactivates the specified service. 
     */
    public void deactivateService(ServiceID serviceID) throws TCKTestErrorException;
    
}
