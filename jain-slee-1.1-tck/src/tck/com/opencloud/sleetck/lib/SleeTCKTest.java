/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib;

/**
 * Inidividual test cases implement this interface - which is the entry
 * point to a test.
 * <br>
 *
 * Configuration data and resources are made available by the SleeTCKTestUtils
 * object passed to the init() method. These resources are available
 * during any of the setUp(), run() and tearDown() calls.
 * <br>
 */
public interface SleeTCKTest {

    // -- Public methods -- //

    /**
     * This is the first method called on the test.
     * The test can use the given utils object to access
     * configuration data and other test related resources.
     */
    public void init(SleeTCKTestUtils utils);

    /**
     * The entry point into the test. Tests implement this method
     * to carry out the given test.
     *
     * The test can assume the following prior to this method call: <ol>
     * <li>The SLEE is in the RUNNING state</li>
     * <li>Only the correct TCK-owned services are installed in the SLEE</li>
     * <li>No TCK created standard address profile tables exist in the SLEE</li>
     * </ol>
     *
     * The test may throw a TCKTestFailureException to indicate a test failure;
     * all other Exceptions and Errors are interpreted as test errors.
     *
     * The returned result object must be non-null.
     * <br>
     */
    public TCKTestResult run() throws Exception;

    /**
     * Called to perform any pre-test preparation functions,
     * such as initialising resources and achieving any required state.<br>
     *
     * If this method throws an Exception, the run() method is not called,
     * and the test result will be of type ERROR.<br>
     *
     * The test can assume the following prior to this method call: <ol>
     * <li>The SLEE is in the RUNNING state</li>
     * <li>Only the correct TCK-owned services are installed in the SLEE</li>
     * <li>No TCK created standard address profile tables exist in the SLEE</li>
     * </ol>
     */
    public void setUp() throws Exception;

    /**
     * Called to perform any post-test clean up functions,
     * such as cleaning up resources.<br>
     *
     * Each test is obliged to do the following: <ol>
     * <li>Uninstall ALL components which it installed (directly or indirectly)
     * during setUp() run(), including event types, profile specification types,
     * resource adaptor types, sbbs, and services</li>
     * <li>Remove all profile tables which it created (directly or indirectly)</li>
     * </ol>
     *
     * Failure to do any of the above may cause the test suite to abort.<br>
     *
     * This method is always called following a setUp() call,
     * whether or not the test is actually run.<br>
     *
     * If this method throws an Exception, and the test result was a pass,
     * the result will be changed to an error. Otherwise the test result
     * is not affected.<br>
     */
    public void tearDown() throws Exception;

}