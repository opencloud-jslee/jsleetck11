/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib;

/**
 * Defines standard parameter names used in the
 * test description files.
 */
public interface DescriptionKeys {

    // Keys used by the TCK infrastructure

    // The title of the test
    public static final String TITLE = "title";

    // An optional description of the test
    public static final String DESCRIPTION = "description";

    // The test class to execute
    public static final String TEST_CLASS = "executeClass";

    // Optional arguments to pass to the test class
    public static final String EXECUTE_ARGS = "executeArgs";

    // A comma separated list of ids of assertions tested by this test
    public static final String ASSERTIONS = "assertions";

    // A comma separated list of ids of assertions assumed to have been tested
    // before running this test
    public static final String ASSUME_ASSERTIONS = "assume-assertions";

    // The factor to multiply the default timeout by to get the test timeout
    public static final String TIMEOUT_MULTIPLIER = "timeout-multiplier";

    // A comma separated list of keywords used to group and select tests (not currently used)
    public static final String KEYWORDS = "keywords";

    // Reserved keys

    public static final String[] RESERVED_KEYS =
        {
        // defined in the test description
        "title","description","executeClass","executeArgs","keywords","assertions","assume-assertions",
        // not defined in the test description
        "assertion","assume-assertion","timeout","id","classDir",
        "source","sources","keyword","property","name","value"
        };

    // Keys shared by many tests

    public static final String SERVICE_DU_PATH_PARAM = "serviceDUPath";
    public static final String RESOURCE_DU_PATH_PARAM = "resourceDUPath";

}
