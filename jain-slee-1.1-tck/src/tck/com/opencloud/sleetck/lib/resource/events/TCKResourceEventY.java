/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.resource.events;

/**
 * An event class used by the TCK resource.
 */
public interface TCKResourceEventY extends TCKResourceEvent {

    // Event type names
    public static final String Y1 =
        "com.opencloud.sleetck.lib.resource.events.TCKResourceEventY.Y1";
    public static final String Y2 =
        "com.opencloud.sleetck.lib.resource.events.TCKResourceEventY.Y2";
    public static final String Y3 =
        "com.opencloud.sleetck.lib.resource.events.TCKResourceEventY.Y3";

}
