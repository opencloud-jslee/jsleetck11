/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.resource.events;

/**
 * An event class used by the TCK resource.
 */
public interface TCKResourceEventX extends TCKResourceEvent {

    // Event type names
    public static final String X1 =
        "com.opencloud.sleetck.lib.resource.events.TCKResourceEventX.X1";
    public static final String X2 =
        "com.opencloud.sleetck.lib.resource.events.TCKResourceEventX.X2";
    public static final String X3 =
        "com.opencloud.sleetck.lib.resource.events.TCKResourceEventX.X3";

}
