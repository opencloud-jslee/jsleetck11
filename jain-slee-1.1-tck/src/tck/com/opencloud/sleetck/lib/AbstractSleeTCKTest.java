/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib;

import javax.slee.management.DeployableUnitID;
import com.opencloud.logging.Logable;
import com.opencloud.sleetck.lib.resource.testapi.TCKResourceListener;
import com.opencloud.sleetck.lib.testutils.FutureResult;

/**
 * Abstract base class that provides common functionality useful
 * for many tests.
 */
public abstract class AbstractSleeTCKTest implements SleeTCKTest {

    //
    // SleeTCKTest methods
    //

    /**
     * Default implementation which stores a reference to the SleeTCKTestUtils
     */
    public void init(SleeTCKTestUtils utils) { this.utils=utils; }

    /**
     * Tests must override either this method or run(FutureResult).
     * The default implementation calls run(FutureResult), then waits for a result
     * from the FutureResult. It returns with error status upon a timeout.
     * Tests which should fail on a timeout must override this method.
     */
    public TCKTestResult run() throws Exception {
        FutureResult result = new FutureResult(utils.getLog());
        run(result);
        getLog().fine("Test thread is waiting for test result");
        return result.waitForResult(utils.getTestTimeout(),"Test did not complete within timeout");
    }

    /**
     * Called by the default implementation of run(FutureResult).
     * Tests must override either this method or run().
     */
    protected void run(FutureResult result) throws Exception {}

    /**
     * Optionally overridden by subclasses.
     * Default implementation installs and activates a single service specified by
     * the DescriptionKeys.SERVICE_DU_PATH_PARAM parameter in the test description.
     * Subclasses may opt to not call this impl if they so desire.
     */
    public void setUp() throws Exception {
        setupService(DescriptionKeys.SERVICE_DU_PATH_PARAM, true);
    }

    /**
     * Optionally overridden by subclasses.
     * Subclasses should always call this tearDown() to get correct cleanup behaviour
     */
    public void tearDown() throws Exception {
        utils.getLog().fine("Ending and purging activities");
        utils.getResourceInterface().clearActivities();

        utils.getLog().fine("Disconnecting from resource");
        utils.getResourceInterface().removeResourceListener();

        utils.getLog().fine("Deactivating and uninstalling all services");
        utils.deactivateAllServices();
        utils.uninstallAll();

    }

    //
    // Util methods available to subclasses.
    //

    /**
     * Sets up the service whose deployment unit jar is at the path specified by
     * the given test parameter.
     * @param propertyKey the test parameter containing the deployment unit jar's path
     */
    protected DeployableUnitID setupService(String propertyKey) throws Exception {
        return setupService(propertyKey, true);
    }

    /**
     * Sets up the service whose deployment unit jar is at the path specified by
     * the given test parameter.
     * @param propertyKey the test parameter containing the deployment unit jar's path
     * @param sbbLogging whether to activate tracing for the sbbs in the service
     */
    protected DeployableUnitID setupService(String propertyKey, boolean sbbLogging) throws Exception {
        String duPath = utils.getTestParams().getProperty(propertyKey);
        utils.getLog().fine("Installing and activating service: " + duPath);
        DeployableUnitID duID = utils.install(duPath);
        utils.activateServices(duID, sbbLogging);
        return duID;
    }

    protected SleeTCKTestUtils utils() {
        return utils;
    }

    protected Logable getLog() {
        return utils.getLog();
    }

    protected void setResourceListener(TCKResourceListener resourceListener) throws Exception {
        utils.getLog().fine("Connecting to resource");
        utils.getResourceInterface().setResourceListener(resourceListener);
    }

    private SleeTCKTestUtils utils;
}
