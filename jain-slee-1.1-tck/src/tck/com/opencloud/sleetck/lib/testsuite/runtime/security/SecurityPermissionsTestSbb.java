/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.runtime.security;

import com.opencloud.sleetck.lib.TCKTestFailureException;
import com.opencloud.sleetck.lib.TCKTestErrorException;
import com.opencloud.sleetck.lib.sbbutils.BaseTCKSbb;
import com.opencloud.sleetck.lib.sbbutils.TCKSbbUtils;
import com.opencloud.sleetck.lib.resource.events.TCKResourceEventX;
import java.util.ArrayList;
import javax.slee.ActivityContextInterface;
import javax.slee.facilities.Level;
import java.security.AccessController;
import java.security.AllPermission;
import java.security.Permission;
import java.security.SecurityPermission;
import java.security.AccessControlException;
import java.awt.AWTPermission;
import java.io.FilePermission;
import java.io.SerializablePermission;
import java.net.NetPermission;
import java.net.SocketPermission;
import java.util.PropertyPermission;
import java.util.Iterator;
import java.lang.reflect.ReflectPermission;

/**
 * Tests an Sbb's security access using AccessController.checkPermission()
 */
public abstract class SecurityPermissionsTestSbb extends BaseTCKSbb {

    /**
     * Checks the Sbb security permissions.
     * If all permissions are granted and denied as expected, a positive ACK
     * is sent to the test. If an unexpected grant/deny is detected, or an unexpected
     * Exception is thrown, an Exception is sent as a negative ACK.
     */
    public void onTCKResourceEventX1(TCKResourceEventX event, ActivityContextInterface aci) {
        try {
            TCKSbbUtils.createTrace(getSbbID(),Level.FINE,"Received event",null);
            checkPermissionsTo1_3();
            checkPermissionsSince1_4();
            if (failedResults.isEmpty()) {
                // send a positive ACK to the test
                TCKSbbUtils.getResourceInterface().sendSbbMessage("All tested permissions granted/denied as appropriate");
            }
            else {
                StringBuffer buf = new StringBuffer();
                buf.append("Some assertions failed:");
                for (Iterator i=failedResults.iterator(); i.hasNext(); )
                    buf.append('\n').append(i.next());
                throw new TCKTestFailureException(firstFailedAssertion, buf.toString());
            }

        } catch(Exception e) { // catch failures and other Exceptions
            TCKSbbUtils.handleException(e); // send to the test
        }
    }

    /**
     * Checks the various permissions, with target names and actions introduced in Java 1.3 or lower
     */
    private void checkPermissionsTo1_3() throws TCKTestErrorException {

        checkDeny(new AllPermission(),1133);

        checkDeny(new AWTPermission("*"),1134);
        checkDeny(new AWTPermission("accessClipboard"),1134);
        checkDeny(new AWTPermission("accessEventQueue"),1134);
        checkDeny(new AWTPermission("listenToAllAWTEvents"),1134);
        checkDeny(new AWTPermission("showWindowWithoutWarningBanner"),1134);
        checkDeny(new AWTPermission("readDisplayPixels"),1134);
        checkDeny(new AWTPermission("createRobot"),1134);

        checkDeny(new FilePermission("*","read"),1135);
        checkDeny(new FilePermission("*","write"),1135);
        checkDeny(new FilePermission("*","execute"),1135);
        checkDeny(new FilePermission("*","delete"),1135);

        checkDeny(new NetPermission("setDefaultAuthenticator"),1136);
        checkDeny(new NetPermission("requestPasswordAuthentication"),1136);
        checkDeny(new NetPermission("specifyStreamHandler"),1136);

        checkGrant(new PropertyPermission("*","read"),1137);
        checkDeny(new PropertyPermission("*","write"),1137);

        checkDeny(new ReflectPermission("suppressAccessChecks"),1138);

        // queuePrintJob no longer allowed
        checkDeny(new RuntimePermission("queuePrintJob"), 1139);
        checkDeny(new RuntimePermission("createClassLoader"),1139);
        checkDeny(new RuntimePermission("getClassLoader"),1139);
        checkDeny(new RuntimePermission("setContextClassLoader"),1139);
        checkDeny(new RuntimePermission("setSecurityManager"),1139);
        checkDeny(new RuntimePermission("createSecurityManager"),1139);
        checkDeny(new RuntimePermission("exitVM"),1139);
        checkDeny(new RuntimePermission("shutdownHooks"),1139);
        checkDeny(new RuntimePermission("setFactory"),1139);
        checkDeny(new RuntimePermission("setIO"),1139);
        checkDeny(new RuntimePermission("modifyThread"),1139);
        checkDeny(new RuntimePermission("stopThread"),1139);
        checkDeny(new RuntimePermission("modifyThreadGroup"),1139);
        checkDeny(new RuntimePermission("getProtectionDomain"),1139);
        checkDeny(new RuntimePermission("readFileDescriptor"),1139);
        checkDeny(new RuntimePermission("writeFileDescriptor"),1139);
        checkDeny(new RuntimePermission("loadLibrary.*"),1139);
        checkDeny(new RuntimePermission("accessClassInPackage.*"),1139);
        checkDeny(new RuntimePermission("defineClassInPackage.*"),1139);
        checkDeny(new RuntimePermission("accessDeclaredMembers"),1139);
        checkDeny(new RuntimePermission("*"),1139);

        checkDeny(new SecurityPermission("*"),1140);
        checkDeny(new SecurityPermission("createAccessControlContext"),1140);
        checkDeny(new SecurityPermission("getDomainCombiner"),1140);
        checkDeny(new SecurityPermission("getPolicy"),1140);
        checkDeny(new SecurityPermission("setPolicy"),1140);
        checkDeny(new SecurityPermission("getProperty.*"),1140);
        checkDeny(new SecurityPermission("setProperty.*"),1140);
        checkDeny(new SecurityPermission("insertProvider.*"),1140);
        checkDeny(new SecurityPermission("removeProvider.*"),1140);
        checkDeny(new SecurityPermission("setSystemScope"),1140);
        checkDeny(new SecurityPermission("setIdentityPublicKey"),1140);
        checkDeny(new SecurityPermission("setIdentityInfo"),1140);
        checkDeny(new SecurityPermission("addIdentityCertificate"),1140);
        checkDeny(new SecurityPermission("removeIdentityCertificate"),1140);
        checkDeny(new SecurityPermission("printIdentity"),1140);
        checkDeny(new SecurityPermission("clearProviderProperties.*"),1140);
        checkDeny(new SecurityPermission("putProviderProperty.*"),1140);
        checkDeny(new SecurityPermission("removeProviderProperty.*"),1140);
        checkDeny(new SecurityPermission("getSignerPrivateKey"),1140);
        checkDeny(new SecurityPermission("setSignerKeyPair"),1140);

        checkDeny(new SerializablePermission("enableSubclassImplementation"),1141);
        checkDeny(new SerializablePermission("enableSubstitution"),1141);

        // SocketPermission.connect no longer allowed in PFD3
        checkDeny(new SocketPermission("*","accept"),1142);
        checkDeny(new SocketPermission("*","listen"),1142);

         // the following two checks have been removed, as they would prohibit a typical
        // ejb implementation of SLEE from working
        //checkDeny(new SocketPermission("*", "connect"), 1142);
        //checkDeny(new SocketPermission("*", "resolve"), 1142);
    }

    /**
     * Checks permissions with target names and actions introduced in Java 1.4, if running Java 1.4 or later.
     */
    private void checkPermissionsSince1_4() throws TCKTestErrorException {
        String version = System.getProperty("java.specification.version");
        if(version == null) throw new TCKTestErrorException("Couldn't detect Java specification version");
        if(!version.startsWith("1.0") && !version.startsWith("1.1") &&
             !version.startsWith("1.2") && !version.startsWith("1.3")) {
            checkDeny(new AWTPermission("fullScreenExclusive"),1134);
        }
    }

    /**
     * Checks that the SLEE will grant the Sbb the given permission
     */
    private void checkGrant(Permission permission, int assertionID) throws TCKTestErrorException {
        try {
            AccessController.checkPermission(permission);
            TCKSbbUtils.createTrace(getSbbID(),Level.INFO,"The SLEE granted the Sbb permission " + permission + " as expected",null);
        } catch (AccessControlException ace) {
            failedResults.add("The SLEE denied the Sbb a permission which it was expected to grant: " + permission + " (assertion " + assertionID + ")");
            if(firstFailedAssertion == -1) firstFailedAssertion = assertionID;
        } catch (Exception e) {
            throw new TCKTestErrorException("Caught unexpected Exception from AccessController.checkPermission(). "+
                "Permission="+permission,e);
        }
    }

    /**
     * Checks that the SLEE will deny the Sbb the given permission
     */
    private void checkDeny(Permission permission, int assertionID) throws TCKTestErrorException {
        try {
            AccessController.checkPermission(permission);
            failedResults.add("The SLEE granted the Sbb a permission which it was expected to deny: " + permission + " (assertion " + assertionID + ")");
            if(firstFailedAssertion == -1) firstFailedAssertion = assertionID;
        } catch (AccessControlException ace) {
            TCKSbbUtils.createTrace(getSbbID(),Level.INFO,"The SLEE denied the Sbb permission " + permission + " as expected",null);
        } catch (Exception e) {
            throw new TCKTestErrorException("Caught unexpected Exception from AccessController.checkPermission(). "+
                "Permission="+permission,e);
        }
    }


    private final ArrayList failedResults = new ArrayList();
    private int firstFailedAssertion = -1;
}
