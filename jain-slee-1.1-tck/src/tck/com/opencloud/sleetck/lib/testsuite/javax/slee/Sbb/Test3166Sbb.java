/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.javax.slee.Sbb;

import com.opencloud.sleetck.lib.resource.events.TCKResourceEventX;
import com.opencloud.sleetck.lib.sbbutils.BaseTCKSbb;
import com.opencloud.sleetck.lib.sbbutils.TCKSbbUtils;

import javax.slee.ActivityContextInterface;
import javax.slee.SbbContext;
import java.util.HashMap;

public abstract class Test3166Sbb extends BaseTCKSbb {

    public void setSbbContext(SbbContext sbbContext) {
        super.setSbbContext(sbbContext);
        wasSetSbbContextCalled = true;
        try {
            setValue(true);
            couldSetCMPStateInSetSbbContext = true;
        } catch (java.lang.Throwable t) {
            couldSetCMPStateInSetSbbContext = false;
        }
    }

    // Initial event
    public void onTCKResourceEventX1(TCKResourceEventX ev, ActivityContextInterface aci) {
        try {
            HashMap map = new HashMap();
            if (wasSetSbbContextCalled) {
                if (couldSetCMPStateInSetSbbContext) {
                    map.put("Result", new Boolean(false));
                    map.put("Message", "was allowed to access CMP state");
                    map.put("ID", new Integer(3166));
                } else {
                    map.put("Result", new Boolean(true));
                    map.put("Message", "Ok");
                }
                TCKSbbUtils.getResourceInterface().sendSbbMessage(map);
            } else {
                Exception toThrow = new IllegalStateException(
                    "Event handler was called on the SBB object before setSbbContext() was called");
                TCKSbbUtils.getResourceInterface().sendException(toThrow);
            }
        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }
    }

    // CMP field
    public abstract void setValue(boolean val);
    public abstract boolean getValue();

    // Instance variables

    // Flag to detect whether setSbbContext() is called on
    // the SBB object before the event handler
    private boolean wasSetSbbContextCalled;
    // Flag to store whether or the SBB object
    // could access CMP state from setSbbContext()
    private boolean couldSetCMPStateInSetSbbContext;

}
