/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.transactions;

import javax.naming.NamingException;
import javax.slee.Address;
import javax.slee.ServiceID;
import javax.slee.resource.ActivityHandle;
import javax.slee.resource.ConfigProperties;
import javax.slee.resource.FailureReason;
import javax.slee.resource.FireableEventType;
import javax.slee.resource.InvalidConfigurationException;
import javax.slee.resource.Marshaler;
import javax.slee.resource.ReceivableService;
import javax.slee.resource.ResourceAdaptorContext;
import javax.slee.transaction.SleeTransactionManager;

import com.opencloud.sleetck.lib.rautils.BaseTCKRA;
import com.opencloud.sleetck.lib.rautils.MessageHandler;
import com.opencloud.sleetck.lib.rautils.MessageHandlerRegistry;
import com.opencloud.sleetck.lib.rautils.TCKRAUtils;
import com.opencloud.sleetck.lib.rautils.TraceLogger;

/**
 * Resource adaptor which works as Slee-side receptor for TCK test messages as
 * only RA's have access to TXN manager.
 */
public class Test1109247ResourceAdaptor extends BaseTCKRA {

    public void setResourceAdaptorContext(ResourceAdaptorContext context) {
        super.setResourceAdaptorContext(context);
        setTracer(context.getTracer("Context RA"));
        try {
            MessageHandlerRegistry registry = TCKRAUtils.lookupMessageHandlerRegistry();
            messageHandler = new Test1109247MessageListener(this);
            registry.registerMessageHandler(messageHandler);
        } catch (Exception e) {
            getLog().severe("An error occured during setResourceAdaptorContext()", e);
        }
    }

    public void unsetResourceAdaptorContext() {
        try {
            MessageHandlerRegistry registry = TCKRAUtils.lookupMessageHandlerRegistry();
            if (messageHandler != null)
                registry.unregisterMessageHandler(messageHandler);
        } catch (Exception e) {
            getLog().severe("An error occured during unsetResourceAdaptorContext()", e);
        }
        setTracer(null);
        super.unsetResourceAdaptorContext();
    }

    public SleeTransactionManager getTransactionManager() throws NamingException {
        return getResourceAdaptorContext().getSleeTransactionManager();
    }

    public void activityEnded(ActivityHandle arg0) {
    }

    public void activityUnreferenced(ActivityHandle arg0) {
    }

    public void administrativeRemove(ActivityHandle arg0) {
    }

    public void raConfigurationUpdate(ConfigProperties arg0) {
    }

    public void raConfigure(ConfigProperties arg0) {
    }

    public void raUnconfigure() {
    }

    public void raActive() {
    }

    public void raInactive() {
    }

    public void raStopping() {
    }

    public void raVerifyConfiguration(ConfigProperties arg0) throws InvalidConfigurationException {
    }

    public void eventProcessingFailed(ActivityHandle handle, FireableEventType eventType, Object event, Address address, ReceivableService serviceInfo, int flags, FailureReason reason) {
    }

    public void eventProcessingSuccessful(ActivityHandle handle, FireableEventType eventType, Object event, Address address, ReceivableService serviceInfo, int flags) {
    }

    public void eventUnreferenced(ActivityHandle handle, FireableEventType eventType, Object event, Address address, ReceivableService serviceInfo, int flags) {
    }

    public Object getActivity(ActivityHandle arg0) {
        return null;
    }

    public ActivityHandle getActivityHandle(Object arg0) {
        return null;
    }

    public Marshaler getMarshaler() {
        return null;
    }

    public void queryLiveness(ActivityHandle arg0) {
    }

    public void serviceActive(ReceivableService serviceInfo) {
    }

    public void serviceInactive(ReceivableService serviceInfo) {
    }

    public void serviceStopping(ReceivableService serviceInfo) {
    }

    public void sendMessage(Object obj) {
        super.sendMessage(obj);
    }

    public TraceLogger getLog() {
        return super.getLog();
    }

    private MessageHandler messageHandler = null;
}
