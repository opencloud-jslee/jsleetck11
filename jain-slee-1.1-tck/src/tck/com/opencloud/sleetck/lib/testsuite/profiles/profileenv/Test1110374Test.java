/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.profiles.profileenv;

import java.util.Enumeration;
import java.util.Vector;

import javax.management.ObjectName;
import javax.slee.profile.ProfileSpecificationID;

import com.opencloud.sleetck.lib.AbstractSleeTCKTest;
import com.opencloud.sleetck.lib.TCKTestFailureException;
import com.opencloud.sleetck.lib.TCKTestResult;
import com.opencloud.sleetck.lib.infra.SleeTCKComponentConstants;
import com.opencloud.sleetck.lib.profileutils.BaseMessageAdapter;
import com.opencloud.sleetck.lib.rautils.RMIObjectChannel;
import com.opencloud.sleetck.lib.testutils.ProfileUtils;
import com.opencloud.sleetck.lib.testutils.jmx.ProfileProvisioningMBeanProxy;


public class Test1110374Test extends AbstractSleeTCKTest {

    private static final String DU_PATH_PARAM= "DUPath";

    private static final String SPEC_NAME = "Test1110374Profile";
    private static final String SPEC_NAME2 = "Test1110374_2Profile";
    private static final String SPEC_VERSION = "1.0";

    private static final String PROFILE_TABLE_NAME = "Test1110374ProfileTable";
    private static final String PROFILE_TABLE_NAME2 = "Test1110374_2ProfileTable";

    /**
     * This test runs some tests on the profile component environment.
     */
    public TCKTestResult run() throws Exception {

        //11101125: A Profile Deployment Descriptor fulfilling the requirements of the assertions
        //1110391, 1110392, 1110394, 1110396 should deploy.
        try {
            getLog().fine("Deploy profile spec.");
            setupService(DU_PATH_PARAM);
        } catch (Exception e) {
            return TCKTestResult.failed(11101125, "Deployment of profile specification failed with exception: "+e);
        }

        //create profile tables
        ProfileSpecificationID specID2 = new ProfileSpecificationID(SPEC_NAME2, SleeTCKComponentConstants.TCK_VENDOR, SPEC_VERSION);
        profileProvisioning.createProfileTable(specID2, PROFILE_TABLE_NAME2);
        profileTables.add(PROFILE_TABLE_NAME2);
        getLog().fine("Added profile table "+PROFILE_TABLE_NAME2+" based on profile spec "+SPEC_NAME2);

        ProfileSpecificationID specID = new ProfileSpecificationID(SPEC_NAME, SleeTCKComponentConstants.TCK_VENDOR, SPEC_VERSION);
        profileProvisioning.createProfileTable(specID, PROFILE_TABLE_NAME);
        profileTables.add(PROFILE_TABLE_NAME);
        getLog().fine("Added profile table "+PROFILE_TABLE_NAME+" based on profile spec "+SPEC_NAME);

        ObjectName defaultProfile = profileProvisioning.getDefaultProfile(PROFILE_TABLE_NAME);
        utils().getMBeanFacade().invoke(defaultProfile, "manage", new Object[0], new String[0]);

        if (exResult!=null)
            return exResult;
        return TCKTestResult.passed();
    }

    public void setPassed(int assertionID, String msg) {
        getLog().fine(assertionID+": "+msg);
    }

    public void setFailed(int assertionID, String msg, Exception e) {
        if (e==null) {
            getLog().fine("FAILURE: "+assertionID+":"+msg);
            //in case the currently active futureResult has already been set to passed() before the failure
            //comes in, we have to provide a mechanism to still detect this failure
            setIfEmpty(TCKTestResult.failed(assertionID, msg));
        }
        else {
            getLog().fine("FAILURE: "+assertionID+":"+msg);
            getLog().fine(e);
            //in case the currently active futureResult has already been set to passed() before the failure
            //comes in, we have to provide a mechanism to still detect this failure
            setIfEmpty(TCKTestResult.failed(new TCKTestFailureException(assertionID, msg, e)));
        }
    }

    public void setError(String msg, Exception e) {
        if (e==null) {
            getLog().warning(msg);
            //in case the currently active futureResult has already been set to passed() before the error
            //comes in, we have to provide a mechanism to still detect this error
            setIfEmpty(TCKTestResult.error(msg));
        }
        else {
            getLog().warning(msg);
            getLog().warning(e);
            //in case the currently active futureResult has already been set to passed() before the error
            //comes in, we have to provide a mechanism to still detect this failure
            setIfEmpty(TCKTestResult.error(msg, e));
        }
    }

    private void setIfEmpty(TCKTestResult exResult) {
        if (this.exResult==null)
            this.exResult = exResult;
    }

    public void setUp() throws Exception {

        in = utils().getRMIObjectChannel();
        in.setMessageHandler(new BaseMessageAdapter(getLog()) {
            public void onSetPassed(int assertionID, String msg) { setPassed(assertionID, msg); }
            public void onSetFailed(int assertionID, String msg, Exception e) { setFailed(assertionID, msg, e); }
            public void onLogCall(String msg) { getLog().fine(msg); }
            public void onSetError(String msg, Exception e) { setError(msg, e); }

        });

        profileTables = new Vector();

        profileUtils = new ProfileUtils(utils());
        profileProvisioning = profileUtils.getProfileProvisioningProxy();

    }

    public void tearDown() throws Exception {

        in.clearQueue();

        try {
            Enumeration en = profileTables.elements();
            while (en.hasMoreElements()) {
                profileUtils.removeProfileTable((String)en.nextElement());
            }
        }
        catch (Exception e) {
            getLog().warning("Caught exception while trying to remove profile table:");
            getLog().warning(e);
        }

        super.tearDown();
    }

    private ProfileUtils profileUtils;
    private Vector profileTables;
    private ProfileProvisioningMBeanProxy profileProvisioning;

    private TCKTestResult exResult;
    private RMIObjectChannel in;
}
