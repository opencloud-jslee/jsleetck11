/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.javax.slee.ActivityContextInterface.Attach;

import com.opencloud.sleetck.lib.sbbutils.BaseTCKSbb;
import com.opencloud.sleetck.lib.sbbutils.TCKSbbUtils;
import com.opencloud.sleetck.lib.resource.events.TCKResourceEventX;

import javax.slee.ActivityContextInterface;
import javax.slee.facilities.Level;

import java.util.HashMap;

/**
 * Throws null pointer exception if sbb passed to ACI.attach() is null.
 */

public abstract class AttachNullTestSbb extends BaseTCKSbb {


    public void onTCKResourceEventX1(TCKResourceEventX event, ActivityContextInterface aci) {
        HashMap map = new HashMap();

        try {
            try {
                aci.attach(null);
            } catch (NullPointerException e) {
                // Success
                map.put("Result", new Boolean(true));
                map.put("Message", "OK");
                TCKSbbUtils.getResourceInterface().sendSbbMessage(map);
                return;
            } catch (Exception f) {
                TCKSbbUtils.handleException(f);
                return;
            }

            // Failure.
            map.put("Result", new Boolean(false));
            map.put("Message", "NullPointerException should have been thrown, but no exception was thrown.");
            TCKSbbUtils.getResourceInterface().sendSbbMessage(map);

        } catch (Exception g) {
            TCKSbbUtils.handleException(g);
        }

    }

}
