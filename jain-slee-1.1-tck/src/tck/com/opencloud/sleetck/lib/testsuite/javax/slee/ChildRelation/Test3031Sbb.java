/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.javax.slee.ChildRelation;

import javax.slee.ActivityEndEvent;
import javax.slee.ActivityContextInterface;
import javax.slee.Address;
import javax.slee.SbbLocalObject;
import javax.slee.facilities.Level;
import javax.slee.ChildRelation;

import com.opencloud.sleetck.lib.sbbutils.BaseTCKSbb;
import com.opencloud.sleetck.lib.sbbutils.TCKSbbUtils;
import com.opencloud.sleetck.lib.sbbutils.TCKSbbConstants;
import com.opencloud.sleetck.lib.resource.events.TCKResourceEventX;
import com.opencloud.sleetck.lib.resource.TCKActivityID;
import com.opencloud.sleetck.lib.resource.sbbapi.TCKActivity;
import com.opencloud.sleetck.lib.resource.sbbapi.TCKResourceSbbInterface;
import com.opencloud.sleetck.lib.resource.sbbapi.TCKActivityContextInterfaceFactory;

import java.util.HashMap;

/**
 * SbbLocalObject.setSbbPriority(SbbLocalObject)
 */

public abstract class Test3031Sbb extends BaseTCKSbb {

    public void onTCKResourceEventX1(TCKResourceEventX event, ActivityContextInterface aci) {

        try {
            HashMap map = new HashMap();

            SbbLocalObject childLocalObject = getChildRelation().create();

            if (getChildRelation().contains(childLocalObject)) {
                map.put("Result", new Boolean(true));
                map.put("Message", "Ok");
            } else {
                map.put("Result", new Boolean(false));
                map.put("Message", "ChildRelation did not contain the created child.");
            }
            TCKSbbUtils.getResourceInterface().sendSbbMessage(map);
            return;

        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }

    }

    public abstract ChildRelation getChildRelation();

}
