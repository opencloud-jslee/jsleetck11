/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.profiles.profilelocal;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.SQLException;

/**
 * Business methods have same name (though different signature) as methods in javax.slee.profile.Profile
 * Business method defines throws clause (!= java.rmi.RemoteException)
 * Super interface adheres to ProfileLocal interface rules
 */
public interface Test1110098_10ProfileLocal extends Test1110098_10ProfileLocalSuper {
    public String getValue(int dummy);
    public void setValue(String value, int dummy);
    public void business(String exToThrow) throws FileNotFoundException, IOException, SQLException;
}
