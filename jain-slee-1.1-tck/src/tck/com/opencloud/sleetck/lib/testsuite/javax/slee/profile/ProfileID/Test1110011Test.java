/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.javax.slee.profile.ProfileID;

//import com.opencloud.sleetck.lib.AbstractSleeTCKTest;
import com.opencloud.logging.Logable;
import com.opencloud.sleetck.lib.AbstractSleeTCKTest;
import com.opencloud.sleetck.lib.SleeTCKTest;
import com.opencloud.sleetck.lib.SleeTCKTestUtils;
import com.opencloud.sleetck.lib.TCKTestFailureException;
import com.opencloud.sleetck.lib.TCKTestResult;
import com.opencloud.sleetck.lib.testutils.ProfileUtils;
import com.opencloud.sleetck.lib.testutils.jmx.DeploymentMBeanProxy;
import com.opencloud.sleetck.lib.testutils.jmx.ProfileMBeanProxy;
import com.opencloud.sleetck.lib.testutils.jmx.ProfileProvisioningMBeanProxy;

import javax.management.ObjectName;
import javax.slee.Address;
import javax.slee.AddressPlan;
import javax.slee.ComponentID;
import javax.slee.management.DeployableUnitDescriptor;
import javax.slee.management.DeployableUnitID;
import javax.slee.profile.ProfileID;
import javax.slee.profile.ProfileSpecificationID;
import com.opencloud.sleetck.lib.testutils.Assert;

public class Test1110011Test extends AbstractSleeTCKTest {

    private static final String DU_PATH_PARAM = "DUPath";

    public static final String PROFILE_TABLE_NAME = "Test1110011ProfileTable";
    public static final String PROFILE_TABLE_NAME2 = "Test1110011ProfileTable2";
    public static final String PROFILE_NAME = "Test1110011Profile";
    public static final String PROFILE_NAME2 = "Test1110011Profile2";

    /**
     * Perform the actual test.
     */
    public TCKTestResult run() throws Exception {

        Logable log = getLog();

        //4471: create profile using one-args constructor
        ProfileID profileID = null;
        try {
            profileID = new ProfileID(new Address(AddressPlan.SLEE_PROFILE,PROFILE_TABLE_NAME+"/"+PROFILE_NAME));
            log.fine("Created a ProfileID with one-args constructor and valid profile table name");
        } catch (Exception e) {
            return TCKTestResult.failed(4471, "Caught exception when creating ProfileID with one-args constructor and valid table name: "+e);
        }

        //4422: provide null as argument for one-args constructor
        try {
            new ProfileID(null);
            return TCKTestResult.failed(4422, "Did not get NullPointerException when constructor's 'address' parameter set to 'null'.");
        }
        catch (NullPointerException e) {
            log.fine("NullPointerException correctly thrown by one-args constructor when 'address' parameter set to 'null'.");
        }
        catch (Exception e) {
            return TCKTestResult.failed(4422, "Received incorrect exception creating a ProfileID with constructor's 'address' parameter set to 'null', expected NullPointerException. "+e);
        }

        //4610: IllegalArgumentException - if the address plan is not AddressPlan.SLEE_PROFILE, or the address string does not contain a profile identifier with the correct encoding: <profile-table-name>/<profile-name>.
        try {
            new ProfileID(new Address(AddressPlan.AESA,PROFILE_TABLE_NAME+"/"+PROFILE_NAME));
            return TCKTestResult.failed(4610, "IllegalArgumentException should have been thrown when using one-args constructor with address plan not 'AddressPlan.SLEE_PROFILE'");
        } catch (IllegalArgumentException e) {
            log.fine("IllegalArgumentException correctly thrown by one-args constructor if address plan is not 'AddressPlan.SLEE_PROFILE'");
        } catch (Exception e) {
            return TCKTestResult.failed(4610, "Wrong type of exception thrown, expected IllegalArgumentException but caught: "+e);
        }
        try {
            new ProfileID(new Address(AddressPlan.SLEE_PROFILE,PROFILE_TABLE_NAME+PROFILE_NAME));
            return TCKTestResult.failed(4610, "IllegalArgumentException should have been thrown when using one-args constructor without the correct <profile-table-name>/<profile-name> encoding.");
        } catch (IllegalArgumentException e) {
            log.fine("IllegalArgumentException correctly thrown by one-args constructor when not using the correct <profile-table-name>/<profile-name> encoding.");
        } catch (Exception e) {
            return TCKTestResult.failed(4610, "Wrong type of exception thrown, expected IllegalArgumentException but caught: "+e);
        }


        //4425: test getProfileTable name
        Assert.assertEquals(4425, "Profile table name returned by 'getProfileTableName' is incorrect.", PROFILE_TABLE_NAME, profileID.getProfileTableName()) ;
        log.fine("Profile table name returned by 'getProfileTableName' is correct.");

        //4427: test getProfileName
        Assert.assertEquals(4427, "Profile name returned by 'getProfileName' is incorrect.", PROFILE_NAME, profileID.getProfileName()) ;
        log.fine("Profile name returned by 'getProfileName' is correct.");

        //4470: test setProfileID, valid values
        try {
            profileID.setProfileID(PROFILE_TABLE_NAME2, PROFILE_NAME2);
            log.fine("Pointing profileID to different profile via setProfileID() worked fine.");
        }
        catch (Exception e) {
            return TCKTestResult.failed(4470, "Received exception when trying to use setProfileID() with valid parameters: "+e);
        }

        //4429: test setProfileID, null parameters -> NPE thrown
        try {
            profileID.setProfileID(null,"lala");
            return TCKTestResult.failed(4429, "No exception thrown when calling setProfileID with a 'null' parameter.");
        }
        catch (NullPointerException e) {
            log.fine("NullPointerException correctly thrown by setProfileID() when ProfileTableName parameter set to 'null'.");
        }
        catch (Exception e) {
            return TCKTestResult.failed(4429, "Received incorrect exception creating a ProfileID with parameter set to 'null', expected NullPointerException but received: "+ e);
        }
        try {
            profileID.setProfileID("lala",null);
            return TCKTestResult.failed(4429, "No exception thrown when calling setProfileID with a 'null' parameter.");
        }
        catch (NullPointerException e) {
            log.fine("NullPointerException correctly thrown by setProfileID() when ProfileName parameter set to 'null'.");
        }
        catch (Exception e) {
            return TCKTestResult.failed(4429, "Received incorrect exception creating a ProfileID with parameter set to 'null', expected NullPointerException but received: "+ e);
        }

        //4611: test setProfileID, illegal table name
        try {
            profileID.setProfileID("A/A", "A");
            return TCKTestResult.failed(4611, "Creation of ProfileID with invalid name should produce IllegalArgumentException");
        } catch (IllegalArgumentException e) {
            log.fine("Caught IllegalArgumentException creating ProfileID with '/' in table name");
        } catch (Exception e) {
            return TCKTestResult.failed(4611, "Received incorrect exception creating a ProfileID with invalid table name, expected IllegalArgumentException but received: "+ e);
        }

        return TCKTestResult.passed();
    }

    /**
     * Do all the pre-run configuration of the test.
     */

    public void setUp() throws Exception {

        // Install the Deployable Units
        DeployableUnitID duID = setupService(DU_PATH_PARAM);

        // Create a Profile Table, and some Profiles.
        profileUtils = new ProfileUtils(utils());
        profileProvisioning = profileUtils.getProfileProvisioningProxy();

        DeploymentMBeanProxy duProxy = utils().getDeploymentMBeanProxy();
        DeployableUnitDescriptor duDesc = duProxy.getDescriptor(duID);
        ComponentID[] components = duDesc.getComponents();

        ObjectName profile;
        ProfileMBeanProxy profileProxy;
        for (int i = 0; i < components.length; i++) {
            if (components[i] instanceof ProfileSpecificationID) {
                ProfileSpecificationID profileSpecID = (ProfileSpecificationID) components[i];

                profileProvisioning.createProfileTable(profileSpecID, PROFILE_TABLE_NAME);

                profile = profileProvisioning.createProfile(PROFILE_TABLE_NAME, PROFILE_NAME);
                profileProxy = utils().getMBeanProxyFactory().createProfileMBeanProxy(profile);
                profileProxy.commitProfile();

                profile = profileProvisioning.createProfile(PROFILE_TABLE_NAME, PROFILE_NAME2);
                profileProxy = utils().getMBeanProxyFactory().createProfileMBeanProxy(profile);
                profileProxy.commitProfile();

                profileProvisioning.createProfileTable(profileSpecID, PROFILE_TABLE_NAME2);

                profile = profileProvisioning.createProfile(PROFILE_TABLE_NAME2, PROFILE_NAME);
                profileProxy = utils().getMBeanProxyFactory().createProfileMBeanProxy(profile);
                profileProxy.commitProfile();

                profile = profileProvisioning.createProfile(PROFILE_TABLE_NAME2, PROFILE_NAME2);
                profileProxy = utils().getMBeanProxyFactory().createProfileMBeanProxy(profile);
                profileProxy.commitProfile();

            }
        }

    }

    /**
     * Clean up after the test.
     */

    public void tearDown() throws Exception {
        // remove profile tables
        try {
            profileUtils.removeProfileTable(PROFILE_TABLE_NAME);
        } catch(Exception e) {
            getLog().warning("Caught exception while trying to remove profile tables:");
            getLog().warning(e);
        }
        try {
            profileUtils.removeProfileTable(PROFILE_TABLE_NAME2);
        } catch(Exception e) {
            getLog().warning("Caught exception while trying to remove profile tables:");
            getLog().warning(e);
        }
        super.tearDown();
    }

    private ProfileUtils profileUtils;
    private ProfileProvisioningMBeanProxy profileProvisioning;
}
