/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.events.sbbabstractclass;

import java.util.HashMap;

import javax.naming.InitialContext;
import javax.slee.ActivityContextInterface;
import javax.slee.facilities.ActivityContextNamingFacility;
import javax.slee.facilities.Tracer;

import com.opencloud.sleetck.lib.sbbutils.BaseTCKSbb;
import com.opencloud.sleetck.lib.sbbutils.TCKSbbUtils;

/**
 * AssertionID(1108077): Test The SBB object of the SbbContext object must be in
 * the Ready state, i.e. it must have an assigned SBB entity, when it invokes
 * this method. Otherwise, this method throws a java.lang.IllegalStateException.
 */
public abstract class Test1108077ChildSbb extends BaseTCKSbb {

    public void sbbCreate() throws javax.slee.CreateException {

        try {
            tracer = getSbbContext().getTracer("com.test");
            tracer.info("Test1108077ChildSbb:sbbCreate()");
            ActivityContextNamingFacility acNaming = (ActivityContextNamingFacility) new InitialContext()
                    .lookup(ActivityContextNamingFacility.JNDI_NAME);
            ActivityContextInterface aci = acNaming.lookup("name");

            boolean passed = false;
            try {
                tracer
                        .info("Test1108077Sbb:calling getEventMask() from the sbbCreate() method. (Expecting an IllegalStateException)");
                getSbbContext().getEventMask(aci);
            } catch (IllegalStateException ise) {
                tracer
                        .info(
                                "Test1108077Sbb:calling getEventMask() from the sbbCreate() method did throw an IllegalStateException)",
                                null);
                passed = true;
            }

            if (!passed) {
                sendResultToTCK(1108077, "SLEE did not throw IllegalStateException when an SBB with no "
                        + "assigned entity invoked getEventMask on its own SbbContext" ,"Test1108077Test" , true);
                return;
            }

            passed = false;
            try {
                getSbbContext().getEventMask(null);
            } catch (java.lang.NullPointerException e) {
                tracer.info("got expected NullPointerException when the activity argument is null", null);
                passed = true;
            }

            if (!passed) {
                sendResultToTCK(1108184, "getSbbContext().maskEvent(eventNames, null) "
                        + "should have thrown java.lang.NullPointerException." ,"Test1108077Test" , false);
                return;
            }

            sendResultToTCK(1108184, "Test of calling getEventMask() from the sbbCreate() method passed!" ,"Test1108077Test" , true);

        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }

        return;

    }

    private void sendResultToTCK(int failedAssertionID, String message, String testName, boolean result) throws Exception {
        HashMap sbbData = new HashMap();
        sbbData.put("testname", testName);
        sbbData.put("result", result ? "pass" : "fail");
        sbbData.put("message", message);
        sbbData.put("id", new Integer(failedAssertionID));
        TCKSbbUtils.getResourceInterface().sendSbbMessage(sbbData);
    }

    private Tracer tracer;
}
