/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.resource.config;

import java.util.HashMap;
import java.util.Random;

import javax.slee.resource.ConfigProperties;
import javax.slee.resource.InvalidConfigurationException;
import javax.slee.resource.ResourceAdaptorID;
import javax.slee.resource.ConfigProperties.Property;

import com.opencloud.sleetck.lib.TCKTestErrorException;
import com.opencloud.sleetck.lib.TCKTestFailureException;
import com.opencloud.sleetck.lib.TCKTestResult;
import com.opencloud.sleetck.lib.infra.SleeTCKComponentConstants;
import com.opencloud.sleetck.lib.rautils.RMIObjectChannel;
import com.opencloud.sleetck.lib.testsuite.resource.BaseResourceTest;
import com.opencloud.sleetck.lib.testsuite.resource.RAMethods;
import com.opencloud.sleetck.lib.testsuite.resource.TCKMessage;
import com.opencloud.sleetck.lib.testutils.jmx.ResourceManagementMBeanProxy;

/**
 * Basic RA configuration callback methods test.
 * <p>
 * This test checks that: a) The config related callbacks are being called in
 * response to management operations, and b) That the callbacks are supplied
 * with correct ConfigProperties as arguments.
 * <p>
 * Covers assertions: 1115115, 1115118, 1115407, 1115408, 1115514, 1115540,
 * 1115546, 1115549
 */
public class Test1115540Test extends BaseResourceTest {

    private static final String RA_NAME = "TCK_Config_Test_RA";
    private static final String RA_ENTITY_NAME = RA_NAME + "_Entity";
    private static final String RA_VENDOR = SleeTCKComponentConstants.TCK_VENDOR;
    private static final String RA_VERSION = "1.1";

    public Test1115540Test() {
        super();
        generator = new Random();
    }

    public TCKTestResult run() throws Exception {
        RMIObjectChannel in = utils().getRMIObjectChannel();

        /*
         * String, Character, Integer, Boolean, Double, Byte, Short, Long, Float
         */

        ConfigProperties testProperties = new ConfigProperties();

        // Random name/value pairs.

        // Strings
        addProperty(testProperties, randomString() + "Test property !@#$%^&*(){}-=_+;\":\',./<>?`~", randomString() + " test property");
        addProperty(testProperties, "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890" + randomString(), "property test" + randomString());

        // Characters
        addProperty(testProperties, "Character A", new Character('A'));
        addProperty(testProperties, "Character Z", new Character('Z'));

        // Integers
        addProperty(testProperties, "Integer 1", new Integer(Integer.MAX_VALUE));
        addProperty(testProperties, "Integer 2", new Integer(Integer.MIN_VALUE));
        addProperty(testProperties, "Integer 3", new Integer(0));
        addProperty(testProperties, "Integer 4", new Integer(generator.nextInt()));

        // Booleans
        addProperty(testProperties, "Boolean (True)", Boolean.TRUE);
        addProperty(testProperties, "Boolean (False)", Boolean.FALSE);

        // Doubles
        addProperty(testProperties, "Double 1", new Double(Double.MAX_VALUE));
        addProperty(testProperties, "Double 2", new Double(Double.MIN_VALUE));
        addProperty(testProperties, "Double 3", new Double(0));
        addProperty(testProperties, "Double 4", new Double(generator.nextDouble()));

        // Byte
        addProperty(testProperties, "Byte 1", new Byte(Byte.MAX_VALUE));
        addProperty(testProperties, "Byte 2", new Byte(Byte.MIN_VALUE));
        addProperty(testProperties, "Byte 3", new Byte((byte) 0));
        byte bytes[] = new byte[1];
        generator.nextBytes(bytes);
        addProperty(testProperties, "Byte 4", new Byte(bytes[0]));

        // Shorts
        addProperty(testProperties, "Short 1", new Short(Short.MAX_VALUE));
        addProperty(testProperties, "Short 2", new Short(Short.MIN_VALUE));
        addProperty(testProperties, "Short 3", new Short((short) 0));
        addProperty(testProperties, "Short 4", new Short((short) generator.nextInt()));

        // Long
        addProperty(testProperties, "Long 1", new Long(Long.MAX_VALUE));
        addProperty(testProperties, "Long 2", new Long(Long.MIN_VALUE));
        addProperty(testProperties, "Long 3", new Long(0));
        addProperty(testProperties, "Long 4", new Long(generator.nextLong()));

        // Floats
        addProperty(testProperties, "Float 1", new Float(Float.MAX_VALUE));
        addProperty(testProperties, "Float 2", new Float(Float.MIN_VALUE));
        addProperty(testProperties, "Float 3", new Float(0));
        addProperty(testProperties, "Float 4", new Float(generator.nextFloat()));

        ResourceManagementMBeanProxy resourceMBean = utils().getResourceManagementMBeanProxy();

        // Create RA entity with specific properties
        getLog().info("Creating RA Entity");
        ResourceAdaptorID raID = new ResourceAdaptorID(RA_NAME, RA_VENDOR, RA_VERSION);
        resourceMBean.createResourceAdaptorEntity(raID, RA_ENTITY_NAME, testProperties);

        // We expect to see at least one call to raVerifyConfiguration(),
        // and at least one call to raConfigure()
        TCKMessage[] messages = castMessages(in.readQueue(5000));
        checkMessages(messages, testProperties, RAMethods.raConfigure);

        // Update RA entity with new properties
        getLog().info("Updating RA Entity with new properties");
        ConfigProperties testProperties2 = new ConfigProperties();
        addProperty(testProperties2, "key: " + randomString(), "value: " + randomString());
        resourceMBean.updateConfigurationProperties(RA_ENTITY_NAME, testProperties2);

        // We expect to see at least one call to raVerifyConfiguration(),
        // and at least one call to raConfigurationUpdate()
        messages = castMessages(in.readQueue(5000));
        checkMessages(messages, testProperties2, RAMethods.raConfigurationUpdate);

        ConfigProperties invalidProperties = new ConfigProperties();
        addProperty(invalidProperties, ResourceConfigConstants.INVALID_PROPERTY_KEY, "An invalid property");

        in.clearQueue();

        getLog().info("Updating RA Entity with invalid properties");
        boolean iceExceptionThrown = false;
        try {
            resourceMBean.updateConfigurationProperties(RA_ENTITY_NAME, invalidProperties);
        } catch (InvalidConfigurationException ice) {
            iceExceptionThrown = true;
        }

        messages = castMessages(in.readQueue(5000));
        checkForOnlyVerifyMessages(messages);

        if (!iceExceptionThrown)
            throw new TCKTestFailureException(1115118,
                    "InvalidConfigurationException was not thrown by ResourceManagmentMBean.updateConfigurationProperties() when passed invalid configuration properties.");

        getLog().info("Removing old RA Entity properties");
        resourceMBean.removeResourceAdaptorEntity(RA_ENTITY_NAME);

        getLog().info("Creating RA Entity with invalid properties");
        iceExceptionThrown = false;
        try {
            resourceMBean.createResourceAdaptorEntity(raID, RA_ENTITY_NAME, invalidProperties);
        } catch (InvalidConfigurationException ice) {
            iceExceptionThrown = true;
        }

        messages = castMessages(in.readQueue(5000));
        checkForOnlyVerifyMessages(messages);

        if (!iceExceptionThrown)
            throw new TCKTestFailureException(1115115,
                    "InvalidConfigurationException was not thrown by ResourceManagmentMBean.createResourceAdaptorEntity() when passed invalid configuration properties.");

        return TCKTestResult.passed();
    }

    //
    // Private
    //

    private String randomString() {
        return new Integer(generator.nextInt()).toString();
    }

    private void addProperty(ConfigProperties properties, String propertyName, Object propertyValue) {
        String propertyType;
        if (propertyValue.getClass() == String.class)
            propertyType = "java.lang.String";
        else if (propertyValue.getClass() == Character.class)
            propertyType = "java.lang.Character";
        else if (propertyValue.getClass() == Integer.class)
            propertyType = "java.lang.Integer";
        else if (propertyValue.getClass() == Boolean.class)
            propertyType = "java.lang.Boolean";
        else if (propertyValue.getClass() == Double.class)
            propertyType = "java.lang.Double";
        else if (propertyValue.getClass() == Byte.class)
            propertyType = "java.lang.Byte";
        else if (propertyValue.getClass() == Short.class)
            propertyType = "java.lang.Short";
        else if (propertyValue.getClass() == Long.class)
            propertyType = "java.lang.Long";
        else if (propertyValue.getClass() == Float.class)
            propertyType = "java.lang.Float";
        else
            throw new IllegalArgumentException("addProperty() called with unhandled propertyValue class: " + propertyValue.getClass());
        properties.addProperty(new Property(propertyName, propertyType, propertyValue));
    }

    /**
     * Returns true if ConfigProperties 'b' is a subset of (contained in)
     * ConfigProperties 'a'. A subset comparison is used to ensure that the test
     * passes if the SLEE is adding properties to the properties specified by an
     * administrator during deployment.
     */
    private boolean contains(ConfigProperties a, ConfigProperties b) {
        Property[] propertiesB = b.getProperties();
        for (int i = 0; i < propertiesB.length; i++)
            // Check all elements of 'b' exist in 'a'.
            if (a.getProperty(propertiesB[i].getName()) == null)
                return false;
        return true;
    }

    /**
     * Check to see that messages received were appropriately ordered (verify
     * first), contained the appropriate method callbacks, and were given the
     * same properties that were specified during create/update.
     *
     * @throws TCKTestFailureException
     */
    private void checkMessages(TCKMessage[] messages, ConfigProperties originalProperties, int expectedMethod) throws TCKTestErrorException,
            TCKTestFailureException {
        boolean verifyCalled = false;
        boolean methodSeen = false;

        for (int i = 0; i < messages.length; i++) {
            TCKMessage message = messages[i];

            HashMap results = (HashMap) message.getArgument();
            if (results == null)
                throw new TCKTestErrorException("Null result returned from test component.");
            ConfigProperties properties = (ConfigProperties) results.get("properties");
            Boolean configuredBoolean = (Boolean) results.get("configured");
            if (configuredBoolean == null)
                throw new TCKTestErrorException("Null 'configured' argument returned from test component.");
            boolean configured = configuredBoolean.booleanValue();

            if (message.getMethod() == expectedMethod)
                methodSeen = true;

            switch (message.getMethod()) {
            case RAMethods.raVerifyConfiguration:
                verifyCalled = true;
                break;
            case RAMethods.raConfigure:
                if (!verifyCalled)
                    throw new TCKTestFailureException(1115540, RAMethods.getMethodName(message.getMethod()) + "() invoked before raVerifyConfiguration()");
                if (properties == null || !contains(properties, originalProperties))
                    throw new TCKTestFailureException(1115408, RAMethods.getMethodName(message.getMethod()) + "() invoked with wrong ConfigProperties: "
                            + properties);
                break;
            case RAMethods.raConfigurationUpdate:
                if (!verifyCalled)
                    throw new TCKTestFailureException(1115540, RAMethods.getMethodName(message.getMethod()) + "() invoked before raVerifyConfiguration()");
                if (!configured)
                    throw new TCKTestFailureException(1115549, RAMethods.getMethodName(message.getMethod())
                            + "() invoked on a RA Object in an unconfigured state.");
                if (properties == null || !contains(properties, originalProperties))
                    throw new TCKTestFailureException(1115407, RAMethods.getMethodName(message.getMethod()) + "() invoked with wrong ConfigProperties: "
                            + properties);
                break;
            default:
                throw new TCKTestErrorException("Unexpected method type returned in a ConfigMessage: " + message.toString());
            }
        }

        if (!methodSeen) {
            switch (expectedMethod) {
            case RAMethods.raConfigure:
                throw new TCKTestFailureException(1115514, "Expected method call was not received by the Resource Adaptor: "
                        + RAMethods.getMethodName(expectedMethod) + "()");
            case RAMethods.raConfigurationUpdate:
                throw new TCKTestFailureException(1115546, "Expected method call was not received by the Resource Adaptor: "
                        + RAMethods.getMethodName(expectedMethod) + "()");
            default:
                throw new TCKTestErrorException("Expected method call was not received by the Resource Adaptor: " + RAMethods.getMethodName(expectedMethod));

            }
        }
    }

    /**
     * Check to ensure raConfigurationUpdate() and raConfigure()
     * callbacks aren't invoked when the RA is updated or created with invalid
     * properties.
     *
     * @throws TCKTestFailureException
     * @throws TCKTestErrorException
     */
    private void checkForOnlyVerifyMessages(TCKMessage[] messages) throws TCKTestFailureException, TCKTestErrorException {
        for (int i = 0; i < messages.length; i++) {
            TCKMessage message = messages[i];
            if (message.getMethod() != RAMethods.raVerifyConfiguration)
                switch (message.getMethod()) {
                case RAMethods.raConfigure:
                    throw new TCKTestFailureException(1115115, "Unexpected call to " + RAMethods.getMethodName(message.getMethod())
                            + "() - only raVerifyConfiguration() is expected when invalid configuration properties are supplied.");
                case RAMethods.raConfigurationUpdate:
                    throw new TCKTestFailureException(1115118, "Unexpected call to " + RAMethods.getMethodName(message.getMethod())
                            + "() - only raVerifyConfiguration() is expected when invalid configuration properties are supplied.");
                default:
                    throw new TCKTestErrorException("Unhandled method received while checking that only raVerifyConfiguration() has been called.");
                }
        }
    }

    private final Random generator;
}
