/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.events.servicestarted;

import java.rmi.RemoteException;
import java.util.Map;

import javax.slee.management.DeployableUnitID;

import com.opencloud.sleetck.lib.AbstractSleeTCKTest;
import com.opencloud.sleetck.lib.TCKTestFailureException;
import com.opencloud.sleetck.lib.resource.TCKActivityID;
import com.opencloud.sleetck.lib.resource.TCKSbbMessage;
import com.opencloud.sleetck.lib.resource.testapi.TCKResourceListener;
import com.opencloud.sleetck.lib.testutils.Assert;
import com.opencloud.sleetck.lib.testutils.BaseTCKResourceListener;
import com.opencloud.sleetck.lib.testutils.FutureResult;

/**
 * AssertionID(1108115): Test of the getService method returns the Service 
 * identifier of the ServiceActivity object’s Service.
 * 
 * AssertionID(1108119): Test of the getService method returns the Service 
 * identifier of the Service that has started.
 * 
 * AssertionID(1108121): Test of The JNDI_NAME constant. This constant specifies 
 * the JNDI location where a ServiceActivityFactory object may be located by an 
 * SBB component in its component environment.
 * 
 * AssertionID(1108123): Test of The JNDI_NAME constant. This constant specifies 
 * the JNDI location where a ServiceActivityContextInterface- Factory object may 
 * be located by an SBB component in its component environement.
 *
 */
public class Test1108115Test extends AbstractSleeTCKTest {

    public static final String SERVICE_DU_PATH_PARAM = "serviceDUPath";

    /**
     * Perform the actual test.
     */

    public void run(FutureResult result) throws Exception {
        this.result = result;
        // Start the DU's service.
        utils().activateServices(duID, true);

    }

    /**
     * Do all the pre-run configuration of the test.
     */

    public void setUp() throws Exception {
        getLog().fine("Connecting to resource");
        resourceListener = new TCKResourceListenerImpl();
        setResourceListener(resourceListener);
        getLog().fine("Installing and activating service");

        // Install the Deployable Units
        String duPath = utils().getTestParams().getProperty(SERVICE_DU_PATH_PARAM);
        duID = utils().install(duPath);
    }

    /**
     * Clean up after the test.
     */

    public void tearDown() throws Exception {
        // cleanup
        super.tearDown();
    }

    private class TCKResourceListenerImpl extends BaseTCKResourceListener {
        // TCKResourceListener methods

        public synchronized void onSbbMessage(TCKSbbMessage message, TCKActivityID calledActivity)
                throws RemoteException {

            Map sbbData = (Map) message.getMessage();
            String sbbTestName = "Test1108115Test";
            String sbbTestResult = (String) sbbData.get("result");
            String sbbTestMessage = (String) sbbData.get("message");
            int assertionID = ((Integer) sbbData.get("id")).intValue();
            getLog().info(
                    "Received message from SBB: testname=" + sbbTestName + ", result=" + sbbTestResult + ", message="
                            + sbbTestMessage + ", id=" + assertionID);
            try {
                Assert.assertEquals(assertionID, "Test " + sbbTestName + " failed.", "pass", sbbTestResult);
                result.setPassed();
            } catch (TCKTestFailureException ex) {
                result.setFailed(assertionID, sbbTestMessage);
            }
        }

        public void onException(Exception exception) throws RemoteException {
            getLog().warning("Received Exception from SBB or resource:");
            getLog().warning(exception);
            result.setError(exception);
        }
    }

    private TCKResourceListener resourceListener;

    private FutureResult result;

    private DeployableUnitID duID;
}
