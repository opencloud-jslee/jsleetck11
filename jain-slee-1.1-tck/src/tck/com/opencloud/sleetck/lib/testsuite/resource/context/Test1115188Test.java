/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.resource.context;

import java.util.HashMap;

import com.opencloud.sleetck.lib.TCKTestErrorException;
import com.opencloud.sleetck.lib.TCKTestFailureException;
import com.opencloud.sleetck.lib.TCKTestResult;
import com.opencloud.sleetck.lib.testsuite.resource.BaseResourceTest;
import com.opencloud.sleetck.lib.testsuite.resource.RAMethods;

/**
 * Test to ensure that getProfileTable() is returning a correct value.
 * <p>
 * Test assertion ID: 1115188, 1115189, 1115191, 1115192
 */
public class Test1115188Test extends BaseResourceTest {

    private static final int ASSERTION_ID = 1115188;

    public TCKTestResult run() throws Exception {
        HashMap resultmap = sendMessage(RAMethods.getProfileTable);
        if (resultmap == null)
            throw new TCKTestFailureException(ASSERTION_ID, "Test timed out while waiting for response to getProfileTable()");

        Object result = resultmap.get("result1");
        if (result instanceof Exception)
            throw new TCKTestFailureException(ASSERTION_ID, "Exception thrown while invoking getProfileTable()", (Exception) result);
        if (result instanceof String)
            throw new TCKTestFailureException(ASSERTION_ID, "An error occured while invoking getProfileTable(): " + result);
        if (!Boolean.TRUE.equals(result))
            throw new TCKTestErrorException("Unexpected result received while invoking getProfileTable(): " + result);

        result = resultmap.get("result2");
        if (result instanceof Exception)
            throw new TCKTestFailureException(1115189,
                    "An exception was thrown while trying to typecast the profile returned by ResourceAdaptorContext.getProfileTable()", (Exception) result);
        if (!Boolean.TRUE.equals(result))
            throw new TCKTestErrorException("Unexpected result received while trying to typecast the profile returned by getProfileTable(): " + result);

        result = resultmap.get("result3");
        if (result instanceof Exception)
            throw new TCKTestFailureException(1115191,
                    "An unexpected exception occured while calling ResourceAdaptorContext.getProfileTable(null) - expected NullPointerException",
                    (Exception) result);
        if (Boolean.FALSE.equals(result))
            throw new TCKTestFailureException(1115191, "ResourceAdaptorContext.getProfileTable(null) failed to throw a NullPointerException");
        if (!Boolean.TRUE.equals(result))
            throw new TCKTestErrorException("Unexpected result received while invoking ResourceAdaptorContext.getProfileTable(null): " + result);

        result = resultmap.get("result4");
        if (result instanceof Exception)
            throw new TCKTestFailureException(
                    1115192,
                    "An unexpected exception occured while calling ResourceAdaptorContext.getProfileTable() with a non-existant profile table - expected UnrecognizedProfileTableNameException",
                    (Exception) result);
        if (Boolean.FALSE.equals(result))
            throw new TCKTestFailureException(1115192,
                    "ResourceAdaptorContext.getProfileTable() failed to throw an UnrecognizedProfileTableNameException when passed a non-existant profile table");
        if (!Boolean.TRUE.equals(result))
            throw new TCKTestErrorException("Unexpected result received while trying to typecast the profile returned by getProfileTable(): " + result);

        return TCKTestResult.passed();
    }

}
