/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.javax.slee.ComponentID;

import com.opencloud.sleetck.lib.AbstractSleeTCKTest;
import com.opencloud.sleetck.lib.DescriptionKeys;
import com.opencloud.sleetck.lib.TCKTestResult;
import com.opencloud.sleetck.lib.testutils.jmx.DeploymentMBeanProxy;

import javax.slee.ComponentID;
import javax.slee.management.DeployableUnitDescriptor;
import javax.slee.management.DeployableUnitID;

public class Test3067Test extends AbstractSleeTCKTest {

    private static final int TEST_ID = 3067;

    /**
     * Perform the actual test.
     */
    public TCKTestResult run() throws Exception {

        DeploymentMBeanProxy duProxy = utils().getDeploymentMBeanProxy();
        DeployableUnitDescriptor duDesc = duProxy.getDescriptor(duID);
        ComponentID components[] = duDesc.getComponents();

        if (components.length < 2) {
            return TCKTestResult.error("Number of installed components incorrect.");
        }

        try {
            components[0].hashCode();
        } catch (Exception e) {
            return TCKTestResult.failed(TEST_ID, "ComponentID.hashCode() threw an exception.");
        }

        // Test 3066 - make sure it overrides hashCode in Object.
        if (components[0].getClass().getMethod("hashCode", new Class[0]) == Object.class.getMethod("hashCode", new Class[0])) {
                return TCKTestResult.failed(3066, "ComponentID.hashCode does not override Object.hashCode");
        }      
        
        try {
            components[0].toString() ;
        } catch (Exception e) {
            return TCKTestResult.failed(3071, "ComponentID.toString() threw an exception.");
        }

        if (components[0].getClass().getMethod("toString", new Class[0]) == Object.class.getMethod("toString", new Class[0])) {
                return TCKTestResult.failed(3070, "ComponentID.toString does not override Object.toString");
        }
        
        return TCKTestResult.passed();
    }

    public void setUp() throws Exception {
        duID = setupService(DescriptionKeys.SERVICE_DU_PATH_PARAM, true);
    }

    private DeployableUnitID duID;
}
