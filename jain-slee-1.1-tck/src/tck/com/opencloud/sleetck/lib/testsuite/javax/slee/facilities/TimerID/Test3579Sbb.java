/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.javax.slee.facilities.TimerID;

import com.opencloud.sleetck.lib.resource.events.TCKResourceEventX;
import com.opencloud.sleetck.lib.sbbutils.BaseTCKSbb;
import com.opencloud.sleetck.lib.sbbutils.TCKSbbUtils;

import javax.naming.InitialContext;
import javax.slee.ActivityContextInterface;
import javax.slee.facilities.TimerFacility;
import javax.slee.facilities.TimerID;
import javax.slee.facilities.TimerOptions;
import java.util.HashMap;

public abstract class Test3579Sbb extends BaseTCKSbb {

    private static final String JNDI_TIMERFACILITY_NAME = "java:comp/env/slee/facilities/timer";

    public static final long TIMEOUT = 3000;

    // Initial event
    public void onTCKResourceEventX1(TCKResourceEventX ev,ActivityContextInterface aci) {

        try {
            HashMap map = new HashMap();

            TimerOptions options = new TimerOptions();
            options.setTimeout(TIMEOUT);

            TimerFacility facility = (TimerFacility) new InitialContext().lookup(JNDI_TIMERFACILITY_NAME);

            TimerID firstTimer = facility.setTimer(aci, null, System.currentTimeMillis(), options);
            try {
                firstTimer.hashCode();
            } catch (Exception e) {
                map.put("Result", new Boolean(false));
                map.put("Message", "TimerID.hashCode() threw an exception.");
                map.put("ID", new Integer(3579));
                TCKSbbUtils.getResourceInterface().sendSbbMessage(map);
                return;
            }

            try {
                if (firstTimer.toString() == null) {
                    map.put("Result", new Boolean(false));
                    map.put("Message", "TimerID.toString() returned null.");
                    map.put("ID", new Integer(3581));
                    TCKSbbUtils.getResourceInterface().sendSbbMessage(map);
                    return;
                }

            } catch (Exception e) {
                map.put("Result", new Boolean(false));
                map.put("Message", "TimerID.toString() threw an exception.");
                map.put("ID", new Integer(3581));
                TCKSbbUtils.getResourceInterface().sendSbbMessage(map);
                return;
            }

            map.put("Result", new Boolean(true));
            map.put("Message", "Ok");
            TCKSbbUtils.getResourceInterface().sendSbbMessage(map);
            return;

        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }

    }
}
