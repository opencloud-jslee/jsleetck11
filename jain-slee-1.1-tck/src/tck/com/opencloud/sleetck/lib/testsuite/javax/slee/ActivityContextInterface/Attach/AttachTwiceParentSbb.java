/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.javax.slee.ActivityContextInterface.Attach;

import com.opencloud.sleetck.lib.sbbutils.BaseTCKSbb;
import com.opencloud.sleetck.lib.sbbutils.TCKSbbUtils;
import com.opencloud.sleetck.lib.resource.events.TCKResourceEventX;

import javax.slee.ActivityContextInterface;
import javax.slee.facilities.Level;
import javax.slee.Sbb;
import javax.slee.SbbLocalObject;
import javax.slee.ChildRelation;

import java.util.HashMap;
import java.util.Iterator;

/**
 * Throws null pointer exception if sbb passed to ACI.attach() is null.
 */

public abstract class AttachTwiceParentSbb extends BaseTCKSbb {

    public abstract ChildRelation getChildRelation();
 
    public void onTCKResourceEventX1(TCKResourceEventX event, ActivityContextInterface aci) {

        try {

            HashMap map = new HashMap();
            ChildRelation childRelation = getChildRelation();
            SbbLocalObject childLocalObject = null;

            // Create the child SBB and attach it.
            childLocalObject = childRelation.create();
            aci.attach(childLocalObject);

            // Attempt to attach the child SBB a second time.
            aci.attach(childLocalObject);

            // Assume success - attaching a child the second time should
            // have done _nothing_ - no exceptions thrown.

            map.put("Result", new Boolean(true));
            map.put("Message", "Ok");
            TCKSbbUtils.getResourceInterface().sendSbbMessage(map);
            return;

        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }

    }
}
