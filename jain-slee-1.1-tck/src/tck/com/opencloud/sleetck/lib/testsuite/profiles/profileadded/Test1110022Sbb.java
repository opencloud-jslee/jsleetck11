/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.profiles.profileadded;

import java.util.HashMap;

import javax.slee.ActivityContextInterface;
import javax.slee.RolledBackContext;
import javax.slee.TransactionRolledbackLocalException;
import javax.slee.profile.ProfileAddedEvent;

import com.opencloud.sleetck.lib.sbbutils.BaseTCKSbb;
import com.opencloud.sleetck.lib.sbbutils.TCKSbbUtils;
import com.opencloud.sleetck.lib.sbbutils.events2.SbbBaseMessageComposer;

public abstract class Test1110022Sbb extends BaseTCKSbb {

    private void sendLogMsgCall (String msg) {
        HashMap map = SbbBaseMessageComposer.getLogMsg(msg);
        try {
            TCKSbbUtils.getResourceInterface().callTest(map);
        }
        catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }
    }

    private void sendResult(boolean result, int id, String msg) {
        HashMap map = SbbBaseMessageComposer.getSetResultMsg(result, id, msg);
        try {
            TCKSbbUtils.getResourceInterface().sendSbbMessage(map);

        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }
    }

    public void onProfileAddedEvent(ProfileAddedEvent event, ActivityContextInterface aci) {

        try {
            ProfileEventsTestsProfileLocal addedProfile;

            addedProfile=(ProfileEventsTestsProfileLocal)event.getAddedProfileLocal();

            //1110022: check that ProfileLocalObject is read-only
            try {
                addedProfile.setValue(2048);
                sendResult(false,1110022,"Setting a new value on the ProfileLocalObject succeeded but should have failed as it is supposed to show read-only behaviour.");
                return;
            }
            catch (TransactionRolledbackLocalException e) {
                sendLogMsgCall("ProfileLocalObject obtained via ProfileAddedEvent is read-only as expected.");
            }
            catch (Exception e) {
                sendResult(false, 1110022,"Wrong exception type, expected javax.slee.TransactionRolledbackLocalException but was "+e.getClass().getName() );
                return;
            }

            sendResult(true, 1110022, "ProfileLocalObject showed read-only behaviour as expected.");

        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }

    }

    public void sbbRolledBack(RolledBackContext context) {
        //ignore rollback caused by ReadOnlyProfileExceptions from tests perspective
    }
}
