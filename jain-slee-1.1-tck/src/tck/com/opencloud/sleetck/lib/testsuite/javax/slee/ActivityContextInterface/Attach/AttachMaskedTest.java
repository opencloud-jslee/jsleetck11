/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.javax.slee.ActivityContextInterface.Attach;

import java.rmi.RemoteException;
import java.util.HashMap;

import com.opencloud.sleetck.lib.AbstractSleeTCKTest;
import com.opencloud.sleetck.lib.TCKTestResult;
import com.opencloud.sleetck.lib.resource.TCKActivityID;
import com.opencloud.sleetck.lib.resource.TCKSbbMessage;
import com.opencloud.sleetck.lib.resource.events.TCKResourceEventX;
import com.opencloud.sleetck.lib.resource.events.TCKResourceEventY;
import com.opencloud.sleetck.lib.resource.testapi.TCKResourceListener;
import com.opencloud.sleetck.lib.resource.testapi.TCKResourceTestInterface;
import com.opencloud.sleetck.lib.testutils.BaseTCKResourceListener;
import com.opencloud.sleetck.lib.testutils.FutureResult;

/**
 * This test deploys a parent and child SBB. The child SBB has an event which is
 * masked on attach. An event is sent that causes the parent to create the child
 * and attach it to the ACI. A second event is sent, which the child should not
 * receive, due to mask-on-attach.
 *
 * This test completes successfully if that event is not received.
 */

public class AttachMaskedTest extends AbstractSleeTCKTest {

    private static final String SERVICE_DU_PATH_PARAM = "serviceDUPath";

    /**
     * Perform the actual test.
     */
    public TCKTestResult run() throws Exception {
        result = new FutureResult(getLog());

        // Create an Activity object.
        resource = utils().getResourceInterface();
        activityName = "AttachMaskedTest";
        activityID = resource.createActivity(activityName);

        // Fire TCKResourceEventX.X1 on this activity.
        getLog().fine("Firing TCKResourceEventX.X1 on activity " + activityName);
        resource.fireEvent(TCKResourceEventX.X1, TCKResourceEventX.X1,activityID, null);

        // Fire an event and ensure the child SBB does not receive it.
        getLog().fine("Firing TCKResourceEventY.Y1 on activity " + activityName);
        resource.fireEvent(TCKResourceEventY.Y1, TCKResourceEventY.Y1,activityID, null);

        // Fire a second event, which is not masked, which causes the child
        // to return a result, dependent on whether Y1 was received.
        getLog().fine("Firing TCKResourceEventY.Y2 on activity " + activityName);
        resource.fireEvent(TCKResourceEventY.Y2, TCKResourceEventY.Y2,activityID, null);

        // Fire the now unmasked event
        resource.fireEvent(TCKResourceEventY.Y1, TCKResourceEventY.Y1,activityID, null);

        // Fire a third event, which is also not masked, which causes the child
        // to check if the unmasked event fired.
        resource.fireEvent(TCKResourceEventX.X2, TCKResourceEventX.X2,activityID, null);

        // Now wait for the response from the SBB.
        return result.waitForResultOrFail(utils().getTestTimeout(),"Timeout waiting for test to complete.", 3009);
    }

    /**
     * Do all the pre-run configuration of the test.
     */
    public void setUp() throws Exception {

        resourceListener = new TCKResourceListenerImpl();
        setResourceListener(resourceListener);

        setupService(SERVICE_DU_PATH_PARAM);
    }

    /**
     * Clean up after the test.
     */
    public void tearDown() throws Exception {
        super.tearDown();
    }

    private class TCKResourceListenerImpl extends BaseTCKResourceListener {
        public synchronized void onSbbMessage(TCKSbbMessage message,TCKActivityID calledActivity) throws RemoteException {
            getLog().info("Received message from SBB");

            HashMap map = (HashMap) message.getMessage();
            Boolean passed = (Boolean) map.get("Result");
            String msgString = (String) map.get("Message");

            if (passed.booleanValue() == true)
                result.setPassed();
            else
                result.setFailed(3009, msgString);
        }

        public void onException(Exception e) throws RemoteException {
            getLog().warning("Received exception from SBB");
            getLog().warning(e);
            result.setError(e);
        }
    }

    private TCKResourceListener resourceListener;
    private FutureResult result;
    private TCKActivityID activityID;
    private TCKResourceTestInterface resource;
    private String activityName;
}
