/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.profiles.profilelocal;

import javax.slee.profile.ProfileContext;
import javax.slee.profile.ProfileLocalObject;

/**
 * Business methods have same name (though different signature) as methods in javax.slee.profile.Profile
 */
public interface Test1110098_9ProfileLocal extends ProfileLocalObject {
    public String getValue();
    public void setValue(String value);
}
