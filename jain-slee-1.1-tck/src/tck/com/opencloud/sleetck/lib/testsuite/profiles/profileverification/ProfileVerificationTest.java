/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.profiles.profileverification;

import com.opencloud.sleetck.lib.AbstractSleeTCKTest;
import com.opencloud.sleetck.lib.TCKTestResult;
import com.opencloud.sleetck.lib.infra.SleeTCKComponentConstants;
import com.opencloud.sleetck.lib.testsuite.profiles.ProfileTestConstants;
import com.opencloud.sleetck.lib.testutils.Assert;
import com.opencloud.sleetck.lib.testutils.ComponentIDLookup;
import com.opencloud.sleetck.lib.testutils.ProfileUtils;
import com.opencloud.sleetck.lib.testutils.jmx.ProfileProvisioningMBeanProxy;

import javax.management.ObjectName;
import javax.slee.profile.ProfileSpecificationID;
import javax.slee.profile.ProfileVerificationException;
import java.util.Iterator;
import java.util.Vector;


public class ProfileVerificationTest extends AbstractSleeTCKTest {

    protected static final String PROFILE_TABLE_NAME = "tck.ProfileVerificationTest.table";
    public static final String PROFILE_SPEC_NAME = "IndexedProfile";

    public static final String VALUE_1 = "value1";
    public static final String INVALID_VALUE = "invalid value";

    /**
     * Run some tests on javax.slee.profile.ProfileManagement's method <code>commitProfile</code> and the <code>profileVerify</code> callback.
     * @return
     * @throws Exception
     */
    public TCKTestResult run() throws Exception {
        // create the profile table
        ProfileProvisioningMBeanProxy profileProvisioning = profileUtils.getProfileProvisioningProxy();
        ProfileSpecificationID profileSpecID = new ComponentIDLookup(utils()).lookupProfileSpecificationID(
                PROFILE_SPEC_NAME, SleeTCKComponentConstants.TCK_VENDOR, "1.0");

        getLog().info("Creating profile table: " + PROFILE_TABLE_NAME);
        profileProvisioning.createProfileTable(profileSpecID, PROFILE_TABLE_NAME);
        tableCreated = true;

        // add profile
        final String name = "A";
        getLog().info("Creating profile " + name);
        ObjectName jmxObjectName = profileProvisioning.createProfile(PROFILE_TABLE_NAME, name);

        IndexedProfileManagementProxy proxy = getProfileProxy(jmxObjectName);

        proxy.editProfile();
        proxy.setValue(VALUE_1);
        proxy.setVerifyStatus(false);
        proxy.commitProfile();
        // Assertion 4343: profileVerify() should be called in response to a request to commit profile changes
        Assert.assertTrue(4343, "profileVerify() was not called in response to a request to commit profile changes",
                          proxy.getVerifyStatus());
        // Assertion 4365: commitProfile() will restore the profile to read only state
        Assert.assertTrue(4365, "Profile should have been restored to read only state by commitProfile()",
                          !proxy.isProfileWriteable());

        // Assertion 1946: A ProfileVerificationException thrown by verifyProfile() should be propagated by commitProfile()
        // Edit the profile to an invalid value - should get exception thrown by profileVerify()
        proxy.editProfile();
        proxy.setValue(INVALID_VALUE);
        try {
            proxy.commitProfile();
            // If we get here no exception was thrown
            Assert.fail(1946, "Expected javax.slee.ProfileVerificationException not thrown");
        } catch (ProfileVerificationException e) {
            // This is the expected exception
            getLog().info("Correct javax.slee.ProfileVerificationException received");
        }

        // Assertion 4365: commitProfile() will restore the profile to read only state
        Assert.assertTrue(4365, "Profile should remain in a writable state if commitProfile() fails to commit the profile",
                          proxy.isProfileWriteable());

        proxy.restoreProfile();

        getLog().info("Set table2 to two non-unique values");
        proxy.editProfile();
        proxy.setTable2(new String[]{"one", "one"});
        proxy.commitProfile();
        getLog().info("Set table1 to two unique values");
        proxy.editProfile();
        proxy.setTable1(new String[]{"one", "two"});
        proxy.commitProfile();
        getLog().info("Set table1 to two non-unique values");
        proxy.editProfile();
        proxy.setTable1(new String[]{"one", "one"});
        try {
            proxy.commitProfile();
            Assert.fail(4368, "Expected javax.slee.ProfileVerificationException not thrown when unique indexed attribute contains duplicate entries");
        } catch (ProfileVerificationException e) { }

        proxy.restoreProfile();

        return TCKTestResult.passed();
    }

    public void setUp() throws Exception {
        String duPath = utils().getTestParams().getProperty(ProfileTestConstants.PROFILE_SPEC_DU_PATH_PARAM);
        getLog().fine("Installing profile specification");
        utils().install(duPath);
        profileUtils = new ProfileUtils(utils());
        activeProxies = new Vector();
    }

    public void tearDown() throws Exception {
        // close profiles
        if (activeProxies != null) {
            getLog().fine("Closing profiles");
            Iterator activeProxiesIter = activeProxies.iterator();
            while (activeProxiesIter.hasNext()) {
                IndexedProfileManagementProxy aProxy = (IndexedProfileManagementProxy) activeProxiesIter.next();
                try {
                    if (aProxy != null) {
                        if (aProxy.isProfileWriteable()) aProxy.restoreProfile();
                        aProxy.closeProfile();
                    }
                } catch (Exception ex) {
                    getLog().warning("Exception caught while trying to close profiles:");
                    getLog().warning(ex);
                }
            }
        }
        // delete profile tables
        try {
            if (profileUtils != null && tableCreated) {
                getLog().fine("Removing profile table");
                profileUtils.removeProfileTable(PROFILE_TABLE_NAME);
            }
        } catch (Exception e) {
            getLog().warning("Caught exception while trying to remove profile table:");
            getLog().warning(e);
        }
        super.tearDown();
    }

    private IndexedProfileManagementProxy getProfileProxy(ObjectName mbeanName) {
        IndexedProfileManagementProxy rProxy = new IndexedProfileManagementProxy(mbeanName, utils().getMBeanFacade());
        activeProxies.addElement(rProxy);
        return rProxy;
    }

    private ProfileUtils profileUtils;
    private boolean tableCreated = false;
    private Vector activeProxies;

}
