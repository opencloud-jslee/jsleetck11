/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.javax.slee.usage.UsageNotification;

import com.opencloud.sleetck.lib.TCKTestFailureException;
import com.opencloud.sleetck.lib.TCKTestResult;
import com.opencloud.sleetck.lib.resource.TCKActivityID;
import com.opencloud.sleetck.lib.resource.events.TCKResourceEventX;
import com.opencloud.sleetck.lib.testsuite.usage.common.GenericUsageMBeanLookup;
import com.opencloud.sleetck.lib.testsuite.usage.common.GenericUsageSbbInstructions;
import com.opencloud.sleetck.lib.testsuite.usage.common.GenericUsageTest;
import com.opencloud.sleetck.lib.testutils.Assert;
import com.opencloud.sleetck.lib.testutils.QueuingNotificationListener;
import com.opencloud.sleetck.lib.testutils.QueuingResourceListener;

import javax.slee.usage.SbbUsageMBean;
import javax.slee.usage.UsageNotification;

public class Test4203Test extends GenericUsageTest {

    private static final String FOO_SET_NAME = "FooSet";

    /**
     * Perform the actual test.
     */
    public TCKTestResult run() throws Exception {

        TCKActivityID activityID = utils().getResourceInterface().createActivity("Test4203InitialActivity");

        GenericUsageSbbInstructions instructionsForFoo = new GenericUsageSbbInstructions(FOO_SET_NAME);
        instructionsForFoo.addFirstCountIncrement(1);
        instructionsForFoo.addFirstCountIncrement(1);
        instructionsForFoo.addTimeBetweenNewConnectionsSamples(2);
        getLog().info("firing event to Sbb");
        utils().getResourceInterface().fireEvent(TCKResourceEventX.X1,instructionsForFoo.toExported(),activityID,null);

        GenericUsageSbbInstructions instructionsForUnnamed = new GenericUsageSbbInstructions(null);
        instructionsForUnnamed.addFirstCountIncrement(4);
        instructionsForUnnamed.addTimeBetweenNewConnectionsSamples(5);
        getLog().info("firing event to Sbb");
        utils().getResourceInterface().fireEvent(TCKResourceEventX.X1,instructionsForUnnamed.toExported(),activityID,null);

        getLog().info("waiting for replies");
        resourceListener.nextMessage();
        resourceListener.nextMessage();
        getLog().info("received replies");

        getLog().info("waiting for usage notifications");

        UsageNotification firstCountFooA = (UsageNotification)notificationListenerFoo.nextNotification();
        UsageNotification firstCountFooB = (UsageNotification)notificationListenerFoo.nextNotification();
        UsageNotification timeBetweenNewConnectionsFoo = (UsageNotification)notificationListenerFoo.nextNotification();
        UsageNotification firstCountUnnamed = (UsageNotification)notificationListenerUnnamed.nextNotification();
        UsageNotification timeBetweenNewConnectionsUnnamed = (UsageNotification)notificationListenerUnnamed.nextNotification();

        getLog().info("auditing usage notifications");

        auditNotification(firstCountFooA,FOO_SET_NAME,"firstCount",true,1);
        auditNotification(firstCountFooB,FOO_SET_NAME,"firstCount",true,2);
        auditNotification(timeBetweenNewConnectionsFoo,FOO_SET_NAME,"timeBetweenNewConnections",false,2);
        auditNotification(firstCountUnnamed,null,"firstCount",true,4);
        auditNotification(timeBetweenNewConnectionsUnnamed,null,"timeBetweenNewConnections",false,5);

        // equals()
        if(!firstCountFooA.equals(firstCountFooB)) return TCKTestResult.failed(4211,"UsageNotification.equals() returned "+
                "false for notifications with the same Service identifier, SBB identifier, usage parameter set name, "+
                "and usage parameter name attributes");
        if(firstCountFooA.equals(timeBetweenNewConnectionsFoo)) return TCKTestResult.failed(4211,
                "UsageNotification.equals() returned true for notifications with different parameter names");
        if(firstCountFooA.equals(firstCountUnnamed)) return TCKTestResult.failed(4211,
                "UsageNotification.equals() returned true for notifications with different parameter set names");
        if(firstCountFooA.equals(timeBetweenNewConnectionsUnnamed)) return TCKTestResult.failed(4211,
                "UsageNotification.equals() returned true for notifications with different parameter set names "+
                "and different parameter names");

        getLog().info("received and checked all "+(instructionsForFoo.getTotalUpdates()+instructionsForUnnamed.getTotalUpdates())+" usage notifications");

        return TCKTestResult.passed();
    }

    private void auditNotification(UsageNotification usageNotification, String parameterSetName,
                                                String parameterName, boolean isCounter, long value) throws TCKTestFailureException {
        getLog().info("Auditing notification: "+usageNotification);

        Assert.assertEquals(4199, "UsageNotification.getType() returned an invalid notification type",
                SbbUsageMBean.USAGE_NOTIFICATION_TYPE, usageNotification.getType());

        Assert.assertEquals(4203, "UsageNotification.getService() returned unexpected ServiceID.",
                mBeanLookup.getServiceID(),usageNotification.getService());

        Assert.assertEquals(4205, "UsageNotification.getSbb() returned unexpected SbbID.",
                mBeanLookup.getSbbID(), usageNotification.getSbb());

        Assert.assertEquals(4207, "UsageNotification.getParameterName() returned incorrect parameter name.",
                parameterName,usageNotification.getUsageParameterName());

        Assert.assertTrue(4209, "UsageNotification.getValue() returned incorrect value. Expected="+value+
                ", actual="+usageNotification.getValue(),value == usageNotification.getValue());

        Assert.assertTrue(4211, "UsageNotification.equals(self) returned false. Notification: "+usageNotification,
                usageNotification.equals(usageNotification));

        Assert.assertEquals(4628, "UsageNotification.getUsageParameterSetName() returned the incorrect parameter set name",
                parameterSetName, usageNotification.getUsageParameterSetName());

        Assert.assertTrue(4630, "UsageNotification.isCounter returned the incorrect value for the \""+parameterName+
                "\" parameter. Expected value="+isCounter+", actual value="+usageNotification.isCounter(),
                isCounter == usageNotification.isCounter());

        int usageNotificationHash = usageNotification.hashCode();
        int expectedHash = usageNotification.getUsageParameterName().hashCode();
        if (usageNotification.getUsageParameterSetName()!=null)
            expectedHash = expectedHash ^ usageNotification.getUsageParameterSetName().hashCode();

        if(usageNotificationHash != expectedHash) throw new TCKTestFailureException(4631,
                "UsageNotification.hashCode() returned an unexpected hash code. Expected="+expectedHash+
                ",actual="+usageNotificationHash+". Hashcode should be the logical XOR of the hash codes "+
                "of the usage parameter set name (if any) and the usage parameter name.");

        try {
            usageNotification.toString();
        } catch (Exception e) {
            throw new TCKTestFailureException(4215, "UsageNotification.toString() threw an exception.",e);
        }

        if (usageNotification.toString() == null) {
                throw new TCKTestFailureException(4215, "UsageNotification.toString() returned null.");
        }
    }

    public void setUp() throws Exception {
        super.setUp();
        setResourceListener(resourceListener = new QueuingResourceListener(utils()));
        mBeanLookup = getGenericUsageMBeanLookup();
        mBeanLookup.getServiceUsageMBeanProxy().createUsageParameterSet(mBeanLookup.getSbbID(),FOO_SET_NAME);
        notificationListenerFoo = new QueuingNotificationListener(utils());
        notificationListenerUnnamed = new QueuingNotificationListener(utils());
        mBeanLookup.getNamedGenericSbbUsageMBeanProxy(FOO_SET_NAME).addNotificationListener(notificationListenerFoo,null,null);
        mBeanLookup.getUnnamedGenericUsageMBeanProxy().addNotificationListener(notificationListenerUnnamed,null,null);
    }

    public void tearDown() throws Exception {
        if(mBeanLookup != null) {
            if(notificationListenerFoo != null)mBeanLookup.getNamedGenericSbbUsageMBeanProxy(FOO_SET_NAME).removeNotificationListener(notificationListenerFoo);
            if(notificationListenerUnnamed != null)mBeanLookup.getUnnamedGenericUsageMBeanProxy().removeNotificationListener(notificationListenerUnnamed);
        }
        super.tearDown();
    }

    private GenericUsageMBeanLookup mBeanLookup;
    private QueuingResourceListener resourceListener;
    private QueuingNotificationListener notificationListenerFoo;
    private QueuingNotificationListener notificationListenerUnnamed;
}
