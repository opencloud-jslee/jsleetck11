/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.facilities.timerfacility.address;

import com.opencloud.sleetck.lib.sbbutils.BaseTCKSbb;
import com.opencloud.sleetck.lib.sbbutils.TCKSbbUtils;
import com.opencloud.sleetck.lib.resource.events.TCKResourceEventX;
import com.opencloud.sleetck.lib.resource.sbbapi.TCKActivity;

import javax.naming.Context;
import javax.naming.InitialContext;

import java.util.HashMap;

import javax.slee.*;
import javax.slee.facilities.*;
import javax.slee.nullactivity.*;

/**
 * Test that TimerFacility TimerEvents are fired to specific addresses.<p>
 *
 * Test Outline: This SBB (Sbb1) creates a null activity and sets a timer on
 * that activity, with a specific address.
 * Two other services, with root SBBs Sbb2 and Sbb3 respectively, are active
 * and are configured to receive TimerEvents as initial events using the
 * AddressProfile initial event selector.
 * Only Sbb2's address profile contains the correct address, so only Sbb2
 * should see the TimerEvent. Both Sbb2 and Sbb3 set a flag in the ACI to
 * say whether they have received the event. When the null activity ends,
 * Sbb1 checks these flags (via ACI attribute aliasing) to confirm that
 * only Sbb2 received the timer event.
 *
 * The test is duplicated for both setTimer variants, since these may be
 * implemented differently in the underlying SLEE implementation.
 */
public abstract class SetTimerAddressSbb1 extends BaseTCKSbb {

    public void setSbbContext(SbbContext context) {
        super.setSbbContext(context);
        try {
            Context myEnv = (Context) new InitialContext().lookup("java:comp/env");

            nullActivityFactory = (NullActivityFactory) myEnv.lookup("slee/nullactivity/factory");
            nullACIFactory = (NullActivityContextInterfaceFactory) myEnv.lookup("slee/nullactivity/activitycontextinterfacefactory");
            timerFacility = (TimerFacility) myEnv.lookup("slee/facilities/timer");
        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }
    }

    // test starts here...
    public void onTCKResourceEventX1(TCKResourceEventX event, ActivityContextInterface aci) {
        try {
            TCKSbbUtils.createTrace(getSbbID(),Level.INFO,"Received start message",null);
            setTestName((String)event.getMessage());

            // ensure we don't miss any timer events
            TimerOptions preserveAll = new TimerOptions();
            preserveAll.setPreserveMissed(TimerPreserveMissed.ALL);

            // create a null aci to set the timer on
            NullActivity nobody = nullActivityFactory.createNullActivity();
            ActivityContextInterface nobodyACI = nullACIFactory.getActivityContextInterface(nobody);

            // create the address
            Address destinationAddress = new Address(AddressPlan.E164, "111");

            // set a single-shot or periodic timer, will fire right away
            if (getTestName().equals("setTimerAddress-single")) {
                TCKSbbUtils.createTrace(getSbbID(), Level.INFO, "creating single-shot timer", null);
                timerFacility.setTimer(nobodyACI, destinationAddress, 0, preserveAll);
            } else if (getTestName().equals("setTimerAddress-periodic")) {
                TCKSbbUtils.createTrace(getSbbID(), Level.INFO, "creating periodic timer", null);
                timerFacility.setTimer(nobodyACI, destinationAddress, 0, 1000, 1, preserveAll);
            }

            // The timer event should ONLY be received by Sbb2, since it has the 111
            // address in its address profile. Sbb2 and Sbb3 will set a flag in a shared
            // activity context if they receive a timer event. This Sbb checks these flags
            // when the ActivityEndEvent arrives, which will occur after the timer fires
            // and there are no more references to the null activity.

        } catch(Exception e) {
            TCKSbbUtils.handleException(e);
        }
    }

    public void onActivityEndEvent(ActivityEndEvent event, ActivityContextInterface aci) {
        try {

            if (aci.getActivity() instanceof NullActivity) {

                TCKSbbUtils.createTrace(getSbbID(),Level.INFO,"Received null activity end event",null);

                // check flags in shared AC
                SetTimerAddressSbb1ActivityContextInterface sharedAC = asSbbActivityContextInterface(aci);

                if (sharedAC.getReceived2() && !sharedAC.getReceived3()) {
                    // correct - only Sbb2 should have recieved the TimerEvent
                    setResultPassed("TimerEvent fired to address correctly");
                }
                else {
                    setResultFailed(1953, "TimerEvent did not fire to address correctly");
                }
                // we are done
                sendResultToTCK();
            }

        } catch(Exception e) {
            TCKSbbUtils.handleException(e);
        }
    }

    // return a static string so that this SBB entity gets selected every time
    // this is so we can get the activity end event
    public InitialEventSelector getStaticConvergenceName(InitialEventSelector ies) {
        ies.setCustomName("foobar");
        return ies;
    }

    public abstract void setTestName(String testName);
    public abstract String getTestName();

    public abstract SetTimerAddressSbb1ActivityContextInterface asSbbActivityContextInterface(ActivityContextInterface aci);

    private void sendResultToTCK() throws Exception {
        HashMap sbbData = new HashMap();
        sbbData.put("testname", getTestName());
        sbbData.put("result", result?"pass":"fail");
        sbbData.put("message", message);
        sbbData.put("id", new Integer(failedAssertionID));
        TCKSbbUtils.getResourceInterface().sendSbbMessage(sbbData);
    }

    private void setResultFailed(int assertionID, String msg) throws Exception {
        result = false;
        failedAssertionID = assertionID;
        message =  "Assertion " + assertionID + " failed: " + msg;
        TCKSbbUtils.createTrace(getSbbID(), Level.WARNING, message, null);
    }

    private void setResultPassed(String msg) throws Exception {
        result = true;
        message = "Success: " + msg;
        TCKSbbUtils.createTrace(getSbbID(), Level.INFO, message, null);
    }

    private TimerFacility timerFacility;
    private NullActivityFactory nullActivityFactory;
    private NullActivityContextInterfaceFactory nullACIFactory;

    private boolean result = false;
    private String message;
    private int failedAssertionID = -1;

}
