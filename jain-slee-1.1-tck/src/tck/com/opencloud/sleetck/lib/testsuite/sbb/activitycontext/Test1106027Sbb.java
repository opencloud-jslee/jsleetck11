/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.sbb.activitycontext;

import java.util.HashMap;

import javax.slee.ActivityContextInterface;
import javax.slee.Address;
import javax.slee.EventContext;
import javax.slee.SLEEException;
import javax.slee.ServiceID;
import javax.slee.TransactionRequiredLocalException;

import com.opencloud.sleetck.lib.resource.events.TCKResourceEventX;
import com.opencloud.sleetck.lib.sbbutils.BaseTCKSbb;
import com.opencloud.sleetck.lib.sbbutils.TCKSbbUtils;

public abstract class Test1106027Sbb extends BaseTCKSbb {

    /*
     * This is the initial event.
     * 
     * Test whether an activity context stored in a CMP field is null
     * after the activity has ended. 
     */
    public void onTCKResourceEventX1(TCKResourceEventX ev, ActivityContextInterface aci, EventContext eventContext) {
        HashMap map;
        MyEventContext myEC;
        map = new HashMap();

        // Try the good case.
        setMyEventContext(eventContext);
        if (!eventContext.equals(getMyEventContext())) {
            // test failed
            map.put("Result", new Boolean(false));
            map.put("ID", new Integer(1106027));
            map.put("Message", "CMP field did not return my Event Context.");
            sendSbbMessage(map);
            return;
        }
        
        // Try the null case.
        setMyEventContext(null);
        if (null != getMyEventContext()) {
            // test failed
            map.put("Result", new Boolean(false));
            map.put("ID", new Integer(1106027));
            map.put("Message", "Null ActivityContext stored in CMP field was not null.");
            sendSbbMessage(map);
            return;
        }

        // Try the invalid EC case.
        myEC = new MyEventContext();
        try {
            setMyEventContext(myEC);
        } catch (IllegalArgumentException e) {
            map.put("Result", new Boolean(true));
            map.put("Message", "InvalidArgumentException thrown as expected.");
            sendSbbMessage(map);
            return;
        }

        map.put("Result", new Boolean(false));
        map.put("Message", "InvalidArgumentException was not thrown as expected.");
        sendSbbMessage(map);
        return;

    }

    public abstract void setMyEventContext(EventContext object);
    public abstract EventContext getMyEventContext();

    private void sendSbbMessage(HashMap map) {
        try {
            TCKSbbUtils.getResourceInterface().sendSbbMessage(map);
        } catch(Exception e) {
            TCKSbbUtils.handleException(e);
        }
    }

    private class MyEventContext implements EventContext {
        /**
         * Get the event object for the event that this Event Context is associated with.
         * @return the event object.
         */
        public Object getEvent(){
            return null;
        }

        /**
         * Get a reference to the Activity Context the event was fired on.
         * @return an <code>ActivityContextInterface</code> object that encapsulates
         *        the Activity Context on which the event was fired.
         */
        public ActivityContextInterface getActivityContextInterface(){
            return null;
        }

        /**
         * Get the address that was provided when the event was fired.
         * @return the address that was provided when the event was fired.  If the
         *        provided address was <code>null</code> this method will also return
         *        <code>null</code>
         */
        public Address getAddress(){
            return null;
        }

        /**
         * Get the component identifier of the Service that was provided when the event
         * was fired.
         * @return the Service component identifier that was provided when the event
         *        was fired.  If the provided Service component identifier was
         *        <code>null</code> this method will also return <code>null</code>.
         */
        public ServiceID getService(){
            return null;
        }

        /**
         * Suspend further delivery of the event associated with this event context.  No further
         * SBBs will receive the event until {@link #resumeDelivery resumeDelivery} is invoked on
         * an event context for the same event, or the system-dependent default timeout is reached,
         * whichever occurs first.
         * <p>
         * This method is a mandatory transactional method.
         * @throws IllegalStateException if event delivery has already been suspended.
         * @throws TransactionRequiredLocalException if this method is invoked without a valid
         *        transaction context.
         * @throws SLEEException if event delivery could not be suspended due to a system-level failure.
         */
        public void suspendDelivery()
            throws IllegalStateException, TransactionRequiredLocalException, SLEEException{}

        /**
         * Suspend further delivery of the event associated with this event context.  No further
         * SBBs will receive the event until {@link #resumeDelivery resumeDelivery} is invoked on
         * an event context for the same event, or the specified timeout is reached, whichever
         * occurs first.
         * <p>
         * This method is a mandatory transactional method.
         * @param timeout the timeout period, measured in milliseconds, before event delivery
         *        will be implicity resumed if <code>resumeDelivery()</code> has not been invoked.
         * @throws IllegalArgumentException if <code>timeout</code> is equal to or less than zero.
         * @throws IllegalStateException if event delivery has already been suspended.
         * @throws TransactionRequiredLocalException if this method is invoked without a valid
         *        transaction context.
         * @throws SLEEException if event delivery could not be suspended due to a system-level failure.
         */
        public void suspendDelivery(int timeout)
            throws IllegalArgumentException, IllegalStateException,
                   TransactionRequiredLocalException, SLEEException{}

        /**
         * Resume previously suspended event delivery of the event associated with this event
         * context.
         * <p>
         * This method is a mandatory transactional method.
         * @throws IllegalStateException if event delivery has not been suspended.
         * @throws TransactionRequiredLocalException if this method is invoked without a valid
         *        transaction context.
         * @throws SLEEException if event delivery could not be resumed due to a system-level failure.
         */
        public void resumeDelivery()
            throws IllegalStateException, TransactionRequiredLocalException, SLEEException{}

        /**
         * Determine if event delivery of the event associated with this event context is currently
         * suspended.
         * @return <code>true</code> if event delivery is currently suspended, <code>false</code>
         *        otherwise.
         * @throws TransactionRequiredLocalException if this method is invoked without a valid
         *        transaction context.
         * @throws SLEEException if the status of event delivery could not be determined due to a
         *        system-level failure.
         */
        public boolean isSuspended()
            throws TransactionRequiredLocalException, SLEEException{
            return true;
        }    
      
    }



}

