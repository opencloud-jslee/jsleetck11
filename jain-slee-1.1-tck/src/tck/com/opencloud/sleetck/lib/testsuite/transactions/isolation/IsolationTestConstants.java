/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.transactions.isolation;

/**
 * 
 */
public class IsolationTestConstants {

    public static final String SBB_EVENT_HANDLER_FIELD = "handler";
    public static final String VALUE_FIELD = "value";
    public static final String SBB_ROLLED_BACK = "SBB_ROLLED_BACK";

}
