/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.activities.activitycontext;

import javax.slee.management.DeployableUnitID;

import com.opencloud.sleetck.lib.*;
import com.opencloud.sleetck.lib.testutils.*;
import com.opencloud.sleetck.lib.resource.TCKSbbMessage;
import com.opencloud.sleetck.lib.resource.testapi.TCKResourceListener;
import com.opencloud.sleetck.lib.resource.TCKActivityID;
import com.opencloud.sleetck.lib.resource.testapi.TCKResourceTestInterface;
import com.opencloud.sleetck.lib.resource.events.TCKResourceEventX;

import com.opencloud.logging.Logable;

import java.rmi.RemoteException;
import java.util.HashMap;

/**
 * The semantics of these accessor methods are also the same as the 
 * semantics of CMP field accessor methods.
 *
 * This test tries to load an SBB whose custom ACI has invalidly declared
 * accessor methods of SetValue and GetValue (capitalized set/get not
 * allowed.)
 */

public class Test587Test implements SleeTCKTest {

    private static final String SERVICE_DU_PATH_PARAM = "serviceDUPath";
    private static final int TEST_ID = 587;

    public void init(SleeTCKTestUtils utils) {
        this.utils = utils;
    }

    /**
     * Perform the actual test.
     */

    public TCKTestResult run() throws Exception {
        result = new FutureResult(utils.getLog());

        try {
            // Install the Deployable Unit.
            utils.getLog().fine("Installing and activating service");
            String duPath = utils.getTestParams().getProperty(SERVICE_DU_PATH_PARAM);
            DeployableUnitID duID = utils.install(duPath);
            utils.getLog().fine("Service installed.  Now activating.");
            utils.activateServices(duID, true); // Activate the service
        } catch (TCKTestErrorException e) {
            // This is a wrapper exception, what exception is it wrappering?

            utils.getLog().fine("Exception caught, determining enclosed exception type.");
            if (e.getEnclosedException().getClass().equals(javax.slee.management.DeploymentException.class))
                return TCKTestResult.passed();

            utils.getLog().fine("Enclosed exception was not of type DeploymentException.");
            // Rethrow the exception.
            throw(e);
        }

        return TCKTestResult.failed(TEST_ID, "Installation of SBB with invalid Activity Context Interface accessor methods was permitted.");
    }

    /**
     * Do all the pre-run configuration of the test.
     */

    public void setUp() throws Exception {
        utils.getLog().fine("Connecting to resource");
        resourceListener = new TCKResourceListenerImpl();
        utils.getResourceInterface().setResourceListener(resourceListener);

    }

    /**
     * Clean up after the test.
     */

    public void tearDown() throws Exception {
        utils.getLog().fine("Disconnecting from resource");
        utils.getResourceInterface().clearActivities();
        utils.getResourceInterface().removeResourceListener();
        utils.getLog().fine("Deactivating and uninstalling service");
        utils.deactivateAllServices();
        utils.uninstallAll();
    }

    private class TCKResourceListenerImpl extends BaseTCKResourceListener {
        public synchronized void onSbbMessage(TCKSbbMessage message, TCKActivityID calledActivity) throws RemoteException {
            utils.getLog().info("Received message from SBB");

            HashMap map = (HashMap) message.getMessage();
            Boolean passed = (Boolean) map.get("Result");
            String msgString = (String) map.get("Message");

            if (passed.booleanValue() == true)
                result.setPassed();
            else
                result.setFailed(TEST_ID, msgString);
        }

        public void onException(Exception e) throws RemoteException {
            utils.getLog().warning("Received exception from SBB");
            utils.getLog().warning(e);
            result.setError(e);
        }
    }

    private SleeTCKTestUtils utils;
    private TCKResourceListener resourceListener;
    private FutureResult result;
}
