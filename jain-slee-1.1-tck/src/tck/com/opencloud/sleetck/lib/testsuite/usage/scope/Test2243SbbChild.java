/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.usage.scope;

import com.opencloud.sleetck.lib.sbbutils.BaseTCKSbb;
import com.opencloud.sleetck.lib.sbbutils.TCKSbbUtils;
import com.opencloud.sleetck.lib.TCKTestErrorException;

import javax.slee.facilities.Level;
import javax.slee.usage.UnrecognizedUsageParameterSetNameException;
import javax.naming.NamingException;

public abstract class Test2243SbbChild extends BaseTCKSbb {

    public void doUpdates() throws NamingException, TCKTestErrorException, UnrecognizedUsageParameterSetNameException {
        TCKSbbUtils.createTrace(getSbbID(),Level.INFO,"doUpdates(): applying updates",null);
        getSbbUsageParameterSet(Test2243Sbb.PARAMETER_SET_NAME).incrementFoo(3);
        getSbbUsageParameterSet(Test2243Sbb.PARAMETER_SET_NAME).sampleBar(4);
        TCKSbbUtils.createTrace(getSbbID(),Level.INFO,"doUpdates(): applied updates",null);
    }

    public abstract Test2243SbbUsage getDefaultSbbUsageParameterSet();
    public abstract Test2243SbbUsage getSbbUsageParameterSet(String name) throws UnrecognizedUsageParameterSetNameException;

}
