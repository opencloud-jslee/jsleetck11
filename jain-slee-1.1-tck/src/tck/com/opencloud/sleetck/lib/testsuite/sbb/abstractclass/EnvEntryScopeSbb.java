/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.sbb.abstractclass;

import com.opencloud.sleetck.lib.sbbutils.TCKSbbUtils;
import com.opencloud.sleetck.lib.resource.events.TCKResourceEventX;

import javax.slee.*;
import javax.slee.facilities.*;

import javax.naming.*;

/**
 * Test scoping of environment entries
 */
public abstract class EnvEntryScopeSbb extends SendResultsSbb {
    public void onTCKResourceEventX1(TCKResourceEventX event, ActivityContextInterface aci) {
        try {
            if (checkEnvEntries()
                && ((EnvEntryScopeChildLocal)getChild1Relation().create()).checkEnvEntries()
                && ((EnvEntryScopeChildLocal)getChild2Relation().create()).checkEnvEntries()
               ) {
                setResultPassed("environment entry scoping tests passed");
            }
        }
        catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }
    }

    private boolean checkEnvEntries() throws Exception {
        Context context = (Context)new InitialContext().lookup("java:comp/env");

        String s = (String)context.lookup("teststring");
        if (!s.equals("parent sbb")) {
            setResultFailed(496, "teststring environment variable does not contain expected value 'parent sbb', instead it has '" + s + "'");
            return false;
        }

        Boolean b = (Boolean)context.lookup("testboolean");
        if (!b.booleanValue()) {
            setResultFailed(496, "testboolean environment variable does not contain expected value 'true', instead it has 'false'");
            return false;
        }

        return true;
    }

    public abstract ChildRelation getChild1Relation();
    public abstract ChildRelation getChild2Relation();
}

