/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.facilities.servicelookupfacility;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;

import javax.naming.NamingException;
import javax.slee.EventTypeID;
import javax.slee.ServiceID;
import javax.slee.facilities.ServiceLookupFacility;
import javax.slee.resource.ReceivableService;
import javax.slee.serviceactivity.ServiceStartedEvent;

import com.opencloud.logging.StdErrLog;
import com.opencloud.sleetck.lib.infra.SleeTCKComponentConstants;
import com.opencloud.sleetck.lib.profileutils.BaseMessageSender;
import com.opencloud.sleetck.lib.rautils.MessageHandler;
import com.opencloud.sleetck.lib.rautils.RMIObjectChannel;
import com.opencloud.sleetck.lib.rautils.TCKRAUtils;
import com.opencloud.sleetck.lib.testsuite.resource.SimpleEvent;

/**
 * AssertionID(1113671): Test This getService method returns the Service component 
 * identifier of the Service described by the ReceivableService object.
 * 
 * AssertionID(1113672): Test This getReceivableEvents method returns an array of 
 * javax.slee.resource.ReceivableService.Receivable-Event objects.
 * 
 * AssertionID(1113673): Test Each ReceivableEvent object identifies an event type 
 * that one or more SBBs in the Service has an event handler method for.
 * 
 * AssertionID(1113678): Test The equals implementation class of a ReceivableService 
 * object must override the Object.equals method.
 * 
 * AssertionID(1113679): Test Two ReceivableService objects must be considered equals 
 * if the Service component identifiers returned by their respective getService methods
 *  are equal.
 * 
 * AssertionID(1113680): Test A ReceivableService.ReceivableEvent object contains 
 * information about a single event type that can be received by a Service.
 * 
 * AssertionID(1113683): Test This getEventType method returns the event type identifier 
 * of the event type described by the ReceivableService.ReceivableEvent object.
 * 
 * AssertionID(1113684): Test This getResourceOption method returns the resource option 
 * specified in the deployment descriptor of the SBB receiving the event type. If the 
 * SBB did not specify a resource option then this method returns null.
 * 
 * AssertionID(1113685): Test This isInitialEvent method returns true if and only if 
 * the event type is received by the root SBB of the Service and the root SBB has flagged 
 * the event type as an initial event type in its deployment descriptor.
 * 
 * AssertionID(1113676): Test The returned array will only include information on the event 
 * types that the Resource Adaptor may fire (as determined by the resource adaptor types it 
 * implements) unless event type checking has been disabled for the Resource Adaptor 
 * (see Section 15.10).
 **/

public class Test1113671MessageListener extends UnicastRemoteObject implements MessageHandler {
    
    public static final int CHECK_IGNORE_RA_TYPE_EVENT_TYPE_FALSE = 1;
    public static final int CHECK_IGNORE_RA_TYPE_EVENT_TYPE_TRUE = 2;
    
    public Test1113671MessageListener(Test1113671ResourceAdaptor ra) throws RemoteException {
        super();

        try {
            out = TCKRAUtils.lookupRMIObjectChannel();
            msgSender = new BaseMessageSender(out, new StdErrLog());
        }
        catch (Exception e) {
            ra.getLog().warning("Exception occurred when trying to acquire RMIObjectChannel: ",e);
        }

        this.ra = ra;
    }

    public boolean handleMessage(Object message) throws RemoteException {

        //run the tests, if both run through successfully and completely (=both return 'true')
        //send success msg back to TCK test
        
        int type = ((Integer)((HashMap)message).get("Type")).intValue();
        
        switch (type) {
        case CHECK_IGNORE_RA_TYPE_EVENT_TYPE_FALSE:            
            if (test_GetService() && test_GetReceivableEvents() 
                    && test_Equals() && test_GetEventType())
                msgSender.sendSuccess(1113671, "Test successful.");
            break;
        case CHECK_IGNORE_RA_TYPE_EVENT_TYPE_TRUE:
            if (test_INGORE_RA_TYPE_EVENT_TYPE_CHECK())
                msgSender.sendSuccess(1113676, "Test successful.");
        }

        return true;
    }
    
    private boolean test_GetService() {

        try {
            ServiceID simpleServiceID = new ServiceID("Test1113671Service1", SleeTCKComponentConstants.TCK_VENDOR, "1.1");
            ReceivableService rs = getReceivableService(simpleServiceID);

            //1113671
            try {
                ServiceID serviceID = rs.getService();
                if (serviceID == null || !serviceID.equals(simpleServiceID)) {
                    msgSender.sendFailure(1113671, "Invalid ServiceID returned from ReceivableService.getService()");
                    return false;
                }
                msgSender.sendLogMsg("getService() returned expected ServiceID");
            }
            catch (Exception e) {
                msgSender.sendFailure(1113671, "Invalid ServiceID returned from ReceivableService.getService()", e);
                return false;
            }
        }
        catch (Exception e) {
            msgSender.sendException(e);
            return false;
        }
        return true;
    }
    
    private boolean test_GetReceivableEvents() {

        try {
            ServiceID simpleServiceID = new ServiceID("Test1113671Service1", SleeTCKComponentConstants.TCK_VENDOR, "1.1");
            ReceivableService rs = getReceivableService(simpleServiceID);

            //1113672
            try {
                ReceivableService.ReceivableEvent[] rEvent = rs.getReceivableEvents();
                if (rEvent == null || rEvent.length == 0) {
                    msgSender.sendFailure(1113672, "Invalid ReceivableService.ReceivableEvent[] returned from ReceivableService.getReceivableService()");
                    return false;
                }
                msgSender.sendLogMsg("getReceivableService() returned expected ReceivableService.ReceivableEvent[]");
            }
            catch (Exception e) {
                msgSender.sendFailure(1113672, "Invalid ReceivableService.ReceivableEvent[] returned from ReceivableService.getReceivableService()", e);
                return false;
            }
            
            //1113673
            try {
                ReceivableService.ReceivableEvent[] rEvent = rs.getReceivableEvents();
                EventTypeID simpleEventType = new EventTypeID(SimpleEvent.SIMPLE_EVENT_NAME, SleeTCKComponentConstants.TCK_VENDOR, "1.1");
                if (!rEvent[0].getEventType().equals(simpleEventType) || rEvent.length < 1) {
                    msgSender.sendFailure(1113673, "Each ReceivableEvent object didn't identity an event type that one or more SBBs in " +
                               "the Service has an event handler method for.");
                    return false;
                }
                msgSender.sendLogMsg("Each ReceivableEvent object identities an event type that one or more SBBs in " +
                                "the Service has an event handler method for.");
            }
            catch (Exception e) {
                msgSender.sendFailure(1113673, "Each ReceivableEvent object didn't identity an event type that one or more SBBs in " +
                        "the Service has an event handler method for.", e);
                return false;
            }
        }
        catch (Exception e) {
            msgSender.sendException(e);
            return false;
        }
        return true;
    }
    
    private boolean test_Equals() {

        try {
            ServiceID simpleServiceID1 = new ServiceID("Test1113671Service1", SleeTCKComponentConstants.TCK_VENDOR, "1.1");
            ServiceID simpleServiceID2 = new ServiceID("Test1113671Service1", SleeTCKComponentConstants.TCK_VENDOR, "1.1");
            ReceivableService rs1 = getReceivableService(simpleServiceID1);
            ReceivableService rs2 = getReceivableService(simpleServiceID2);

            //1113678
            try {
                if (rs1 == null || rs2 == null || !rs1.equals(rs1) || !rs2.equals(rs2)) {
                    msgSender.sendFailure(1113678, "ServiceID didn't eqaul to itself");
                    return false;
                }
                msgSender.sendLogMsg("ServiceID is eqaul to itself");
            }
            catch (Exception e) {
                msgSender.sendFailure(1113678, "ServiceID didn't eqaul to itself", e);
                return false;
            }
            
            //1113679
            try {
                ServiceID serviceID1 = rs1.getService();
                ServiceID serviceID2 = rs2.getService();
                if (!serviceID1.equals(serviceID2) || !rs1.equals(rs2)) {
                    msgSender.sendFailure(1113679, "Two ReceivableService objects didn't equal if the Service component " +
                               "identifiers returned by their respective getService methods are equal.");
                    return false;
                }
                msgSender.sendLogMsg("Two ReceivableService objects must be considered equals if the Service component " +
                                "identifiers returned by their respective getService methods are equal.");
            }
            catch (Exception e) {
                msgSender.sendFailure(1113679, "Two ReceivableService objects didn't equal if the Service component " +
                        "identifiers returned by their respective getService methods are equal.", e);
                return false;
            }
        }
        catch (Exception e) {
            msgSender.sendException(e);
            return false;
        }
        return true;
    }
    
    
    private boolean test_GetEventType() {

        try {
            ServiceID simpleServiceID = new ServiceID("Test1113671Service1", SleeTCKComponentConstants.TCK_VENDOR, "1.1");
            ReceivableService rs = getReceivableService(simpleServiceID);
            
            //1113683
            try {
                ReceivableService.ReceivableEvent[] rEvent = rs.getReceivableEvents();
                EventTypeID simpleEventType = new EventTypeID(SimpleEvent.SIMPLE_EVENT_NAME, SleeTCKComponentConstants.TCK_VENDOR, "1.1");
                if (!rEvent[0].getEventType().equals(simpleEventType) || rEvent.length < 1) {
                    msgSender.sendFailure(1113683, "Invalid EventTypeID returned from ReceivableService.ReceivableEvent.getEventType()");
                    return false;
                }
                msgSender.sendLogMsg("getEventType() returned expected EventTypeID");
            }
            catch (Exception e) {
                msgSender.sendFailure(1113683, "Invalid EventTypeID returned from ReceivableService.ReceivableEvent.getEventType()", e);
                return false;
            }
            
            //1113684
            try {
                ReceivableService.ReceivableEvent[] rEvent = rs.getReceivableEvents();
                if (rEvent[0].getResourceOption()!=null) {
                    msgSender.sendFailure(1113684, "No null returned from ReceivableService.ReceivableEvent.getResourceOption()");
                    return false;
                }
                msgSender.sendLogMsg("getResourceOption() returned expected null");
            }
            catch (Exception e) {
                msgSender.sendFailure(1113684, "No null returned from ReceivableService.ReceivableEvent.getResourceOption()", e);
                return false;
            }
            
            //1113685
            try {
                ReceivableService.ReceivableEvent[] rEvent = rs.getReceivableEvents();
                if (!rEvent[0].isInitialEvent()) {
                    msgSender.sendFailure(1113685, "The false value returned from ReceivableService.ReceivableEvent.isInitialEvent()");
                    return false;
                }
                msgSender.sendLogMsg("isInitialEvent() returned expected true");
            }
            catch (Exception e) {
                msgSender.sendFailure(1113685, "The false value returned from ReceivableService.ReceivableEvent.isInitialEvent()", e);
                return false;
            }
            
        }
        catch (Exception e) {
            msgSender.sendException(e);
            return false;
        }
        return true;
    }
    
    private boolean test_INGORE_RA_TYPE_EVENT_TYPE_CHECK() {

        try {
            ServiceID simpleServiceID = new ServiceID("Test1113671Service2", SleeTCKComponentConstants.TCK_VENDOR, "1.1");
            ReceivableService rs = getReceivableService(simpleServiceID);
            
            //1113676
            try {
                ReceivableService.ReceivableEvent[] rEvent = rs.getReceivableEvents();
                String serviceStartedEvent = "javax.slee.serviceactivity.ServiceStartedEvent";
                EventTypeID serviceStartedEventType = new EventTypeID(serviceStartedEvent, "javax.slee", "1.1");
                EventTypeID Test1113671EventType = new EventTypeID(Test1113671Event.EVENT_NAME, SleeTCKComponentConstants.TCK_VENDOR, "1.1");
                ArrayList eventTypes = new ArrayList();
                for (int i=0; i<rEvent.length; i++) 
                    eventTypes.add(rEvent[i].getEventType());
                boolean passed = eventTypes.size() == 2 && eventTypes.contains(serviceStartedEventType) && eventTypes.contains(Test1113671EventType);
                
                if (!passed) {
                    msgSender.sendFailure(1113676, "There should be two copies of EventTypeID returned from ReceivableService.ReceivableEvent.getEventType()");
                    return false;
                }
                msgSender.sendLogMsg("getEventType() returned expected EventTypeID");
            }
            catch (Exception e) {
                msgSender.sendFailure(1113676, "There should be two copies of EventTypeID returned from ReceivableService.ReceivableEvent.getEventType()", e);
                return false;
            }
        }
        catch (Exception e) {
            msgSender.sendException(e);
            return false;
        }
        return true;
    }
    
    private ReceivableService getReceivableService(ServiceID serviceID) throws NamingException {
        ServiceLookupFacility eventLookup =  getServiceLookupFacility();
        ReceivableService rs;
        try {
            rs = eventLookup.getReceivableService(serviceID);
        }
        catch (Exception e) {
            ra.getLog().warning("getReceivableService throw a UnrecognizedServiceException: " + e);
            rs = null;
        }
            
        return rs;
    }
    
    private ServiceLookupFacility getServiceLookupFacility() throws NamingException {
        return (ServiceLookupFacility) ra.getResourceAdaptorContext().getServiceLookupFacility();
    }
    
    private BaseMessageSender msgSender;
    private RMIObjectChannel out;
    private Test1113671ResourceAdaptor ra;
}

