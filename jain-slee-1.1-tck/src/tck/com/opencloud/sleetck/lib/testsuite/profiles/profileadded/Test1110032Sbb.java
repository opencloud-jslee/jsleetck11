/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.profiles.profileadded;

import java.util.HashMap;

import javax.slee.ActivityContextInterface;
import javax.slee.Address;
import javax.slee.profile.ProfileLocalObject;
import javax.slee.profile.ProfileRemovedEvent;

import com.opencloud.sleetck.lib.sbbutils.BaseTCKSbb;
import com.opencloud.sleetck.lib.sbbutils.TCKSbbUtils;
import com.opencloud.sleetck.lib.sbbutils.events.TCKSbbEvent;
import com.opencloud.sleetck.lib.sbbutils.events.TCKSbbEventImpl;
import com.opencloud.sleetck.lib.sbbutils.events2.SbbBaseMessageComposer;
import com.opencloud.sleetck.lib.sbbutils.events2.SbbBaseMessageConstants;
import com.opencloud.sleetck.lib.sbbutils.events2.SendResultEvent;

public abstract class Test1110032Sbb extends BaseTCKSbb {

    public static final int TYPE_UPDATE_PROFILE = 1024;


    private void sendLogMsgCall (String msg) {
        HashMap map = SbbBaseMessageComposer.getLogMsg(msg);
        try {
            TCKSbbUtils.getResourceInterface().callTest(map);
        }
        catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }
    }

    private Object sendUpdateRequestCall (int newValue, int id) throws Exception {
        HashMap map = SbbBaseMessageComposer.getCustomMessage(TYPE_UPDATE_PROFILE,
                new String[]{"NewValue", SbbBaseMessageConstants.ID},
                new Object[]{new Integer(newValue), new Integer(id)});
        return TCKSbbUtils.getResourceInterface().callTest(map);
    }

    private void sendResult(boolean result, int id, String msg, ActivityContextInterface aci) {
        HashMap map = SbbBaseMessageComposer.getSetResultMsg(result, id, msg);
        setResult(map);
        fireSendResultEvent(new SendResultEvent(), aci, null);
    }

    public void onProfileRemovedEvent(ProfileRemovedEvent event, ActivityContextInterface aci) {

    try {
        sendLogMsgCall("Received ProfileRemovedEvent");

        ProfileEventsTestsProfileLocal removedProfile;

        //1110032, 1110033: test getRemovedProfileLocal and java typecast;
        try {
            removedProfile = (ProfileEventsTestsProfileLocal) event.getRemovedProfileLocal();
        } catch (ClassCastException e) {
            sendResult(false, 1110033, "The Profile Local interface returned by ProfileRemovedEvent.getRemovedProfileLocal() was not of type Test1110032ProfileLocal", aci);
            return;
        }
        sendLogMsgCall("Performed method calls and class casts.");

        //1110032: access the contents of the profile before the removal
        if (removedProfile.getValue() != 42) {
            sendResult(false, 1110032,"The 'value' field in the Test1110032ProfileLocal object representing the values prior to the removal was not correct." ,aci);
            return;
        }
        sendLogMsgCall("Checked expected values for profile before removal.");

        //send message to test to create profile again and set the profile value
        //block as otherwise transaction in which onProfileRemovedEvent was called commits
        sendLogMsgCall("Request tck test to create profile a 2nd time and update profile field value to 2048");
        sendUpdateRequestCall(2048, 1110034);

        //1110034: check that value is still 42 as ProfileLocalObject is a snapshot of the time immediately before  profile was removed
        if (removedProfile.getValue() != 42) {
            sendResult(false,1110034,"The 'value' field in the Test1110032ProfileLocal object should represent a snapshot of the time when the profile was removed and not be subject to changes done to the profile since then.",aci);
            return;
        }
        sendLogMsgCall("Check that values have not been affected by creating profile again and updating the value.");

        //sbb fires message to itself thus allowing the current transaction to complete
        fireTCKSbbEvent(new TCKSbbEventImpl(null),aci,null);

    } catch (Exception e) {
        TCKSbbUtils.handleException(e);
    }

    }

    public void onTCKSbbEvent(TCKSbbEvent event, ActivityContextInterface aci) {
        sendResult(true, 1110032, "Test completed successfully", aci);
    }

    public void onSendResultEvent(SendResultEvent event, ActivityContextInterface aci) {

        try {
            TCKSbbUtils.getResourceInterface().sendSbbMessage(getResult());

        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }
    }

    public abstract void fireSendResultEvent(SendResultEvent event, ActivityContextInterface aci, Address address);
    public abstract void fireTCKSbbEvent(TCKSbbEvent event, ActivityContextInterface aci, Address address);

    public abstract void setResult(HashMap map);
    public abstract HashMap getResult();

    public abstract void setProfileLocalObject(ProfileLocalObject value);
    public abstract ProfileLocalObject getProfileLocalObject();
}
