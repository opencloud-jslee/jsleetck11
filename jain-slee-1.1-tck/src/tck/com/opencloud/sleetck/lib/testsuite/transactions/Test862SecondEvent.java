/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.transactions;

import java.io.Serializable;
import java.util.Random;
                                                                                
public final class Test862SecondEvent implements Serializable {
    public Test862SecondEvent() {
        // generate random id
        id = new Random().nextLong() ^ System.currentTimeMillis();
    }
                                                                                
    public boolean equals(Object o) {
        if (o == this) return true;
        return (o instanceof Test862SecondEvent)
            && ((Test862SecondEvent)o).id == id;
    }
                                                                                
    public int hashCode() {
        return (int)id;
    }
                                                                                
    public String toString() {
        return "Test862SecondEvent[" + id + "]";
    }
                                                                                
                                                                                
    private final long id;
}
