/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.profiles.profileinitialstate;

import com.opencloud.sleetck.lib.AbstractSleeTCKTest;
import com.opencloud.sleetck.lib.TCKTestResult;
import com.opencloud.sleetck.lib.infra.SleeTCKComponentConstants;
import com.opencloud.sleetck.lib.testsuite.profiles.ProfileTestConstants;
import com.opencloud.sleetck.lib.testutils.Assert;
import com.opencloud.sleetck.lib.testutils.ComponentIDLookup;
import com.opencloud.sleetck.lib.testutils.ProfileUtils;
import com.opencloud.sleetck.lib.testutils.jmx.ProfileProvisioningMBeanProxy;

import javax.slee.management.ManagementException;
import javax.slee.profile.ProfileSpecificationID;

/**
 * Test assertion 4333: that when a profile table is creatted the SLEE calls profileInitialize, profileStore
 * and profileVerify in the same transaction.
 */
public class Test4333Test extends AbstractSleeTCKTest {
    private static final String TABLE_NAME = "tck.Test4333Test.table";
    private static final String PROFILE_SPEC_NAME = "Test4333Profile";

    public TCKTestResult run() throws Exception {
        // create the profile table
        ProfileProvisioningMBeanProxy profileProvisioning = profileUtils.getProfileProvisioningProxy();
        ProfileSpecificationID profileSpecID = new ComponentIDLookup(utils()).lookupProfileSpecificationID(
            PROFILE_SPEC_NAME,SleeTCKComponentConstants.TCK_VENDOR,"1.0");
        getLog().info("Creating profile table: "+TABLE_NAME + " spec=" + profileSpecID.toString());
        try {
            profileProvisioning.createProfileTable(profileSpecID, TABLE_NAME);
            isTableCreated = true;
            getLog().info("Created table ok");
        } catch(ManagementException e) {
            Assert.fail(4333, "When a profile table is created profileInitialize, profileStore, and profileVerify should " +
                              "be called in the same transaction");
        }

        return TCKTestResult.passed();
    }

    public void setUp() throws Exception {
        String duPath = utils().getTestParams().getProperty(ProfileTestConstants.PROFILE_SPEC_DU_PATH_PARAM);
        getLog().fine("Installing profile specification");
        utils().install(duPath);
        profileUtils = new ProfileUtils(utils());
    }

    public void tearDown() throws Exception {
        // delete profile table
        try {
            if(profileUtils != null && isTableCreated) {
                getLog().fine("Removing profile table");
                profileUtils.removeProfileTable(TABLE_NAME);
            }
        } catch(Exception e) {
            getLog().warning("Caught exception while trying to remove profile table:");
            getLog().warning(e);
        }
        profileUtils = null;
        isTableCreated = false;
        super.tearDown();
    }

    private ProfileUtils profileUtils;
    private boolean isTableCreated;
}























