/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.events.convergencename.iesInitialize;

import javax.slee.Address;
import javax.slee.EventTypeID;
import com.opencloud.sleetck.lib.resource.TCKActivityID;

/**
 * Encapsulates the data sent via the event message to the event handler
 * methods of the IESInitialValuesTestSbb.
 */
public class IESInitTestEventMessageData {

    public IESInitTestEventMessageData(String eventID, EventTypeID eventTypeID,
                                    TCKActivityID activityID, Address address) {
        this.eventID=eventID;
        this.eventTypeID=eventTypeID;
        this.activityID=activityID;
        this.address=address;
    }

    /**
     * Exports the Object into an Object or array of primitive types,
     * J2SE types or SLEE types.
     */
    public Object toExported() {
        return new Object[] {eventID, eventTypeID, activityID, address};
    }

    /**
     * Returns an IESInitTestEventMessageData Object from the
     * given object in exported form
     */
    public static IESInitTestEventMessageData fromExported(Object exported) {
        Object[] objArray = (Object[])exported;
        return new IESInitTestEventMessageData((String)objArray[0],(EventTypeID)objArray[1],
                                    (TCKActivityID)objArray[2],(Address)objArray[3]);
    }

    // Accessor methods

    public String getEventID() {
        return eventID;
    }

    public EventTypeID getEventTypeID() {
        return eventTypeID;
    }

    public TCKActivityID getActivityID() {
        return activityID;
    }

    public Address getAddress() {
        return address;
    }

    // Private state

    private String eventID;
    private EventTypeID eventTypeID;
    private TCKActivityID activityID;
    private Address address;

}