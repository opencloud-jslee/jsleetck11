/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.javax.slee.profile.ProfileLocalObject;

import com.opencloud.sleetck.lib.testsuite.profiles.ProfileRAInteractionBaseTest;

/**
 * Test TXN related assertions on ProfileCMP objects via RAs.
 */
public class Test1110654Test extends ProfileRAInteractionBaseTest {

    private static final String RA_NAME = "Test1110654RA";
    private static final String SPEC_NAME = "Test1110654Profile";

    public String getRAName() {
        return RA_NAME;
    }

    public String[] getProfileNames(String profileTableName) {
        return new String[]{Test1110654MessageListener.PROFILE_NAME, Test1110654MessageListener.PROFILE_NAME2};
    }

    public String[] getProfileTableNames() {
        return new String[]{Test1110654MessageListener.PROFILE_TABLE_NAME};
    }

    public String getSpecName() {
        return SPEC_NAME;
    }
}
