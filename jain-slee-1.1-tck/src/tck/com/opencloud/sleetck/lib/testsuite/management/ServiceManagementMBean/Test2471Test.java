/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.management.ServiceManagementMBean;

import javax.slee.management.*;
import javax.slee.ComponentID;
import javax.slee.ServiceID;
import javax.slee.InvalidArgumentException;

import com.opencloud.sleetck.lib.*;
import com.opencloud.sleetck.lib.testutils.*;
import com.opencloud.sleetck.lib.resource.TCKSbbMessage;
import com.opencloud.sleetck.lib.resource.testapi.TCKResourceListener;
import com.opencloud.sleetck.lib.resource.TCKActivityID;
import com.opencloud.sleetck.lib.resource.testapi.TCKResourceTestInterface;
import com.opencloud.sleetck.lib.resource.events.TCKResourceEventX;

import com.opencloud.sleetck.lib.testutils.jmx.DeploymentMBeanProxy;
import com.opencloud.sleetck.lib.testutils.jmx.ServiceManagementMBeanProxy;

import com.opencloud.logging.Logable;

import java.rmi.RemoteException;
import java.util.HashMap;

public class Test2471Test implements SleeTCKTest {

    private static final String SERVICE_DU_PATH_PARAM = "DUPath";
    private static final int TEST_ID = 2471;

    public void init(SleeTCKTestUtils utils) {
    this.utils = utils;
    }

    /**
     * Perform the actual test.
     */

    public TCKTestResult run() throws Exception {

    DeploymentMBeanProxy duProxy = utils.getDeploymentMBeanProxy();
    serviceProxy = utils.getMBeanProxyFactory().createServiceManagementMBeanProxy(utils.getSleeManagementMBeanProxy().getServiceManagementMBean());
    DeployableUnitDescriptor duDesc = duProxy.getDescriptor(duID);
    ComponentID components[] = duDesc.getComponents();

    ServiceID services[] = new ServiceID[2];
    int index = 0;
    for (int i = 0; i < components.length; i++) {
        if (components[i] instanceof ServiceID) {
        services[index++] = (ServiceID) components[i];
        }
    }

    for (int i = 0; i < services.length; i++) {
        // Verify that the Service is in the INACTIVE state.
        if (!serviceProxy.getState(services[i]).isInactive()) {
        return TCKTestResult.failed(TEST_ID, "Services are not in the inactive state; cannot continue with test.");
        }
    }

    serviceProxy.activate(services);

    waitForStateChange(services[0], ServiceState.ACTIVE);
    waitForStateChange(services[1], ServiceState.ACTIVE);

    try {
        serviceProxy.deactivate(services);
    } catch (javax.slee.InvalidStateException e) {
        return TCKTestResult.failed(1620, "Failed to deactivate active services.");
    } catch (Exception e) {
        utils.getLog().fine("Exception thrown: " + e);
        return TCKTestResult.error(e);
    }


    for (int i = 0; i < services.length; i++) {
        if (serviceProxy.getState(services[i]).isActive()) {
        return TCKTestResult.failed(1621, "Deactivated service is in the Active state.");
        }
    }

    boolean passed = false;
    try {
        serviceProxy.deactivate(services);
    } catch (javax.slee.InvalidStateException e) {
        passed = true;
    }

    if (!passed)
        return TCKTestResult.failed(1622, "InvalidStateException should have been thrown when deactivating an already inactive service.");

        waitForStateChange(services[0], ServiceState.INACTIVE);
        waitForStateChange(services[1], ServiceState.INACTIVE);

    // Reactivate the services so deactivate tests work
    serviceProxy.activate(services);
    waitForStateChange(services[0], ServiceState.ACTIVE);
    waitForStateChange(services[1], ServiceState.ACTIVE);

    passed = false;
    try {
        serviceProxy.deactivate(new ServiceID[] {});
    } catch (javax.slee.InvalidArgumentException e) {
        passed = true;
    } catch (Exception e) {
        utils.getLog().fine("Exception thrown: " + e);
        return TCKTestResult.error(e);
    }

    if (!passed)
        return TCKTestResult.failed(TEST_ID, "InvalidArgumentException should have been thrown when calling deactivate with an empty ServiceID array.");

    passed = false;
    try {
        serviceProxy.deactivate(new ServiceID[] {services[0], null});
    } catch (javax.slee.InvalidArgumentException e) {
        passed = true;
    }

    if (!passed)
        return TCKTestResult.failed(TEST_ID, "InvalidArgumentException should have been thrown when calling deactivate with a ServiceID array containing a null element.");

    passed = false;
    try {
        serviceProxy.deactivate(new ServiceID[] {services[0], services[1], services[1]});
    } catch (javax.slee.InvalidArgumentException e) {
        passed = true;
    }

    if (!passed)
        return TCKTestResult.failed(TEST_ID, "InvalidArgumentException should have been thrown when calling deactivate with a ServiceID array containing duplicate items.");

    return TCKTestResult.passed();
    }

    private void waitForStateChange(ServiceID serviceID, ServiceState expectedState) throws Exception {

    int count = 0;

    while (true) {
        if (serviceProxy.getState(serviceID).equals(expectedState))
        return;

        if (count > 10)
                throw new OperationTimedOutException("Timeout waiting for service " + serviceID + " to enter state " + expectedState);

        Thread.sleep(500);
        count++;
    }

    }

    /**
     * Do all the pre-run configuration of the test.
     */

    public void setUp() throws Exception {

    utils.getLog().fine("Connecting to resource");
    resourceListener = new TCKResourceListenerImpl();
    utils.getResourceInterface().setResourceListener(resourceListener);
    utils.getLog().fine("Installing and activating service");

    // Install the Deployable Units
    String duPath = utils.getTestParams().getProperty(SERVICE_DU_PATH_PARAM);
    duID = utils.install(duPath);
    }

    /**
     * Clean up after the test.
     */

    public void tearDown() throws Exception {
    utils.getLog().fine("Disconnecting from resource");
        utils.getResourceInterface().clearActivities();
        utils.getResourceInterface().removeResourceListener();
        utils.getLog().fine("Deactivating and uninstalling service");
        utils.deactivateAllServices();
        utils.uninstallAll();
    }

    private class TCKResourceListenerImpl extends BaseTCKResourceListener {
    public synchronized void onSbbMessage(TCKSbbMessage message, TCKActivityID calledActivity) throws RemoteException {
        utils.getLog().info("Received message from SBB");

        HashMap map = (HashMap) message.getMessage();
        Boolean passed = (Boolean) map.get("Result");
        String msgString = (String) map.get("Message");

        if (passed.booleanValue() == true)
        result.setPassed();
        else
        result.setFailed(TEST_ID, msgString);
    }

    public void onException(Exception e) throws RemoteException {
        utils.getLog().warning("Received exception from SBB");
        utils.getLog().warning(e);
        result.setError(e);
    }
    }

    private ServiceManagementMBeanProxy serviceProxy;
    private SleeTCKTestUtils utils;
    private TCKResourceListener resourceListener;
    private FutureResult result;
    private DeployableUnitID duID;
}
