/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.facilities.alarmfacility;

import java.util.HashMap;

import javax.naming.InitialContext;
import javax.slee.ActivityContextInterface;
import javax.slee.facilities.AlarmFacility;
import javax.slee.facilities.AlarmLevel;
import javax.slee.facilities.Tracer;

import com.opencloud.sleetck.lib.resource.events.TCKResourceEventX;
import com.opencloud.sleetck.lib.sbbutils.BaseTCKSbb;
import com.opencloud.sleetck.lib.sbbutils.TCKSbbUtils;

/*  
 *  AssertionID(1113488): Test The raiseAlarm method returns 
 *  a String value which is used to identify the raised alarm.
 *    
 *  AssertionID(1113489): Test If an alarm with the same identifying 
 *  attributes (notification source, alarm type, instanceID) is already 
 *  raised then this method has no effect, and the identifier of the 
 *  existing raised alarm is returned.
 *  
 *  AssertionID(1113490): Test If no such alarm is raised, then a 
 *  new alarm is raised, the SLEE’s AlarmMBean object emits an 
 *  alarm notification, and a SLEE generated alarm identifier for the 
 *  new alarm is returned.
 *  
 */

public abstract class Test1113488Sbb extends BaseTCKSbb {

    private static final String JNDI_ALARMFACILITY_NAME = AlarmFacility.JNDI_NAME;

    public static final String ALARM_MESSAGE = "Test1113488AlarmMessage";

    public static final String NEW_ALARM_MESSAGE = "Test1113488AlarmMessageNEW";

    public static final String ALARM_INSTANCEID = "Test1113488AlarmInstanceID";

    public static final String ALARM_TYPE = "javax.slee.management.Alarm";

    public static final AlarmLevel ALARM_LEVEL_MAJOR = AlarmLevel.MAJOR;

    // Initial event
    public void onTCKResourceEventX1(TCKResourceEventX event, ActivityContextInterface aci) {

        try {
            sbbTracer = getSbbContext().getTracer("com.test");
            sbbTracer.info("Received " + event + " message", null);
            AlarmFacility facility = (AlarmFacility) new InitialContext().lookup(JNDI_ALARMFACILITY_NAME);
            setFirstAlarm(facility.raiseAlarm(ALARM_TYPE, ALARM_INSTANCEID, ALARM_LEVEL_MAJOR, ALARM_MESSAGE));
            if (getFirstAlarm() == null) {
                sendResultToTCK("Test1113488Test", false, 1113489, "A first alarm failed to raise!");
                return;
            }

            String secondAlarmID = facility.raiseAlarm(ALARM_TYPE, ALARM_INSTANCEID, ALARM_LEVEL_MAJOR, ALARM_MESSAGE);
            if (secondAlarmID == null) {
                sendResultToTCK("Test1113488Test", false, 1113489, "A second alarm failed to raise!");
                return;
            }

            if (!secondAlarmID.equals(getFirstAlarm())) {
                sendResultToTCK("Test1113488Test", false, 1113489, "If an alarm with the same identifying attributes (notification "
                        + "source, alarm type, instanceID) is already raised, but this method didn't"
                        + "return the identifier of the existing raised alarm");
                return;
            }

        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }
    }

    // Second event
    public void onTCKResourceEventX2(TCKResourceEventX event, ActivityContextInterface aci) {

        try {
            sbbTracer = getSbbContext().getTracer("com.test");
            sbbTracer.info("Received " + event + " message", null);
            AlarmFacility facility = (AlarmFacility) new InitialContext().lookup(JNDI_ALARMFACILITY_NAME);
            String NEW_ALARM_INSTANCEID = "Test1113488AlarmInstanceIDNEW";
            String NEW_ALARM_TYPE = "javax.slee.management.Alarm.NEW";
            String newAlarmID = facility.raiseAlarm(NEW_ALARM_TYPE, NEW_ALARM_INSTANCEID, AlarmLevel.MINOR,
                    NEW_ALARM_MESSAGE);
            if (newAlarmID == null) {
                sendResultToTCK("Test1113488Test", false, 1113490, "A new alarm failed to raise!");
                return;
            }
            
            if (newAlarmID.equals(getFirstAlarm())) {
                sendResultToTCK("Test1113488Test", false, 1113490, "The second raised alarmID should not equal to the first raised alarmID.");
                return;
            }
        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }
    }

    private void sendResultToTCK(String testName, boolean result, int failedAssertionID,  String message) throws Exception {
        HashMap sbbData = new HashMap();
        sbbData.put("testname", testName);
        sbbData.put("result", result ? "pass" : "fail");
        sbbData.put("message", message);
        sbbData.put("id", new Integer(failedAssertionID));
        TCKSbbUtils.getResourceInterface().sendSbbMessage(sbbData);
    }
    
    public abstract void setFirstAlarm(String alarmID);

    public abstract String getFirstAlarm();

    private Tracer sbbTracer;
}
