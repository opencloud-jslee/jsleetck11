/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.usage.SbbUsageMBean;

import com.opencloud.sleetck.lib.TCKTestResult;
import com.opencloud.sleetck.lib.resource.TCKActivityID;
import com.opencloud.sleetck.lib.resource.events.TCKResourceEventX;
import com.opencloud.sleetck.lib.testsuite.usage.common.GenericUsageMBeanLookup;
import com.opencloud.sleetck.lib.testsuite.usage.common.GenericUsageMBeanProxy;
import com.opencloud.sleetck.lib.testsuite.usage.common.GenericUsageSbbInstructions;
import com.opencloud.sleetck.lib.testsuite.usage.common.GenericUsageTest;
import com.opencloud.sleetck.lib.testutils.QueuingNotificationListener;
import com.opencloud.sleetck.lib.testutils.QueuingResourceListener;

import javax.slee.usage.SampleStatistics;

/**
 * Makes updates to usage parameters, then accesses them with false
 * reset parameter. Checks again to see that the values are not reset.
 * Makes more updates, then accesses the values with true reset parameter.
 * The returned values should represent the combined updates, and the subsequent
 * accesses should return the reset values.
 */
public class Test2326and2370Test extends GenericUsageTest {

    private static final int RESET_FLAG_ASSERTION = 2326;
    private static final int RETURNS_PRESET_VALUE_ASSERTION = 2370;

    public TCKTestResult run() throws Exception {

        TCKActivityID activityID = utils().getResourceInterface().createActivity("Test2326and2370Test-Activity");

        GenericUsageSbbInstructions instructions = new GenericUsageSbbInstructions(null);
        instructions.addFirstCountIncrement(1);
        instructions.addTimeBetweenNewConnectionsSamples(1);

        getLog().fine("sending first update requests");
        sendInstructionsAndWait(activityID,instructions);

        GenericUsageMBeanProxy sbbUsageMBeanProxy = getGenericUsageMBeanLookup().getUnnamedGenericUsageMBeanProxy();

        // firstCount

        getLog().info("first check: check that accessor returns non-reset value");
        long firstCount = sbbUsageMBeanProxy.getFirstCount(false);
        if(firstCount == 0) return TCKTestResult.failed(RETURNS_PRESET_VALUE_ASSERTION,
                "Accessor method for counter type parameter firstCount returned 0, after updates had been made. "+
                "This may indicate that the counter has been illegally reset, or may indicate some other error "+
                "preventing the counter being set. The reset argument was false.");
        if(firstCount != 1) return TCKTestResult.error("Counter-type parameter firstCount was set to an unexpected value; "+
                "should be 1. Actual value:"+firstCount);

        getLog().info("check that accessor returns non-reset value after calling getFirstCount(false), by calling getFirstCount(false) again");
        firstCount = sbbUsageMBeanProxy.getFirstCount(false);
        if(firstCount == 0) return TCKTestResult.failed(RESET_FLAG_ASSERTION,
                "Accessor method for counter type parameter firstCount returned 0, after updates had been made, "+
                "after an accessor call passing false as the reset argument. "+
                "This indicates that the counter has been illegally reset.");
        if(firstCount != 1) return TCKTestResult.error("Counter-type parameter firstCount was set to an unexpected value; "+
                "should be 1. Actual value:"+firstCount);

        // timeBetweenNewConnections

        getLog().info("first check: check that accessor returns non-reset value");
        SampleStatistics sampleStatistics = sbbUsageMBeanProxy.getTimeBetweenNewConnections(false);
        if(sampleStatistics.getSampleCount() == 0) return TCKTestResult.failed(RETURNS_PRESET_VALUE_ASSERTION,
                "Accessor method for sample type parameter timeBetweenNewConnections returned 0 sample count, "+
                "after updates had been made. This may indicate that the parameter has been illegally reset, or may indicate some other error "+
                "preventing the parameter being updated. The reset argument was false.");
        if(sampleStatistics.getSampleCount() != 1) return TCKTestResult.error("Sample-type parameter timeBetweenNewConnections "+
                " was had an unexpected sample count; should be 1. Actual value:"+sampleStatistics.getSampleCount());

        getLog().info("check that accessor returns non-reset value after calling getTimeBetweenNewConnections(false), by calling getTimeBetweenNewConnections(false) again");
        sampleStatistics = sbbUsageMBeanProxy.getTimeBetweenNewConnections(false);
        if(sampleStatistics.getSampleCount() == 0) return TCKTestResult.failed(RESET_FLAG_ASSERTION,
                "Accessor method for sample type parameter timeBetweenNewConnections returned sample count 0, after updates had been made, "+
                "after an accessor call passing false as the reset argument. "+
                "This indicates that the parameter has been illegally reset.");
        if(sampleStatistics.getSampleCount()  != 1) return TCKTestResult.error("Sample-type parameter timeBetweenNewConnections had an unexpected sample count; "+
                "should be 1. Actual value:"+sampleStatistics.getSampleCount());

        getLog().fine("sending second update requests");
        sendInstructionsAndWait(activityID,instructions);

        // firstCount

        getLog().info("check that accessor returns non-reset value, by calling getFirstCount(true)");
        firstCount = sbbUsageMBeanProxy.getFirstCount(true);
        if(firstCount == 0) return TCKTestResult.failed(RETURNS_PRESET_VALUE_ASSERTION,
                "Accessor method for counter type parameter firstCount returned 0, after updates had been made. "+
                "The reset argument was true, but this was the first request to reset the parameter. "+
                "This indicates that the method did not return the pre-reset value.");
        if(firstCount != 2) return TCKTestResult.error("Counter-type parameter firstCount was set to an unexpected value; "+
                "should be 2. Actual value:"+firstCount);

        getLog().info("check that accessor returns the reset value after calling getFirstCount(true), by calling getFirstCount(false)");
        firstCount = sbbUsageMBeanProxy.getFirstCount(false);
        if(firstCount == 2) return TCKTestResult.failed(RESET_FLAG_ASSERTION,
                "Accessor method for counter type parameter firstCount returned 2, "+
                "after an accessor call passing true as the reset argument, with no subsequent updates. "+
                "The counter should have been reset to 0.");
        if(firstCount != 0) return TCKTestResult.error("Counter-type parameter firstCount was set to an unexpected value; "+
                "should be 0. Actual value:"+firstCount);

        // timeBetweenNewConnections

        getLog().info("check that accessor returns non-reset value, by calling getTimeBetweenNewConnections(true)");
        sampleStatistics = sbbUsageMBeanProxy.getTimeBetweenNewConnections(true);
        if(sampleStatistics.getSampleCount() == 0) return TCKTestResult.failed(RETURNS_PRESET_VALUE_ASSERTION,
                "Accessor method for sample type parameter timeBetweenNewConnections returned 0 sample size count, after updates had been made. "+
                "The reset argument was true, but this was the first request to reset the parameter. "+
                "This indicates that the method did not return the pre-reset value.");
        if(sampleStatistics.getSampleCount() != 2) return TCKTestResult.error("Sample-type parameter timeBetweenNewConnections had an unexpected sample count; "+
                "should be 2. Actual value:"+sampleStatistics.getSampleCount());

        getLog().info("check that accessor returns the reset value after calling getTimeBetweenNewConnections(true), by calling getTimeBetweenNewConnections(false)");
        sampleStatistics = sbbUsageMBeanProxy.getTimeBetweenNewConnections(false);
        if(sampleStatistics.getSampleCount() == 2) return TCKTestResult.failed(RESET_FLAG_ASSERTION,
                "Accessor method for sample type parameter timeBetweenNewConnections returned sample count 2, "+
                "after an accessor call passing true as the reset argument, with no subsequent updates. "+
                "The parameter should have been reset.");
        if(sampleStatistics.getSampleCount() != 0) return TCKTestResult.error("Sample-type parameter timeBetweenNewConnections had an unexpected sample count; "+
                "should be 0. Actual value:"+sampleStatistics.getSampleCount());

        getLog().info("parameter checks ok");
        return TCKTestResult.passed();
    }

    private void sendInstructionsAndWait(TCKActivityID activityID, GenericUsageSbbInstructions instructions) throws Exception {
        getLog().fine("firing event to Sbb");
        utils().getResourceInterface().fireEvent(TCKResourceEventX.X1,instructions.toExported(),activityID,null);

        getLog().fine("waiting for replies");
        resourceListener.nextMessage();
        getLog().info("received replies");

        getLog().fine("waiting for usage notifications");
        for (int i = 0; i < instructions.getTotalUpdates(); i++) {
              notificationListener.nextNotification();
        }
        getLog().info("received all "+(instructions.getTotalUpdates())+" usage notifications");
    }

    public void setUp() throws Exception {
        super.setUp();
        setResourceListener(resourceListener = new QueuingResourceListener(utils()));
        mBeanLookup = getGenericUsageMBeanLookup();
        notificationListener = new QueuingNotificationListener(utils());
        mBeanLookup.getUnnamedGenericUsageMBeanProxy().addNotificationListener(notificationListener,null,null);
    }

    public void tearDown() throws Exception {
        if(mBeanLookup != null) {
            if(notificationListener != null)mBeanLookup.getUnnamedGenericUsageMBeanProxy().removeNotificationListener(notificationListener);
        }
        super.tearDown();
    }

    private GenericUsageMBeanLookup mBeanLookup;
    private QueuingResourceListener resourceListener;
    private QueuingNotificationListener notificationListener;

}
