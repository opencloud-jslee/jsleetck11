/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.usage.SbbUsageMBean;

import com.opencloud.sleetck.lib.TCKTestFailureException;
import com.opencloud.sleetck.lib.TCKTestResult;
import com.opencloud.sleetck.lib.testsuite.usage.common.GenericUsageTest;

import javax.management.MBeanInfo;
import javax.management.MBeanOperationInfo;

/**
 * Declare counter-type parameters FirstCount and SecondCount, and sample-type parameters
 * TimeBetweenNewConnections and TimeBetweenErrors in the usage parameter interface.
 * Deploy the Sbb containing the usage parameters, then acces the Sbb usage Mbean.
 * Check that the MBean exposes accessor methods for each parameter name in the required form.
 */
public class Test2321Test extends GenericUsageTest {

    public TCKTestResult run() throws Exception {
        boolean firstCountAccessorFound = false;
        boolean secondCountAccessorFound = false;
        boolean timeBetweenNewConnectionsAccessorFound = false;
        boolean timeBetweenErrorsAccessorFound = false;

        // inspect the SBB usage MBean
        MBeanInfo mBeanInfo = utils().getMBeanFacade().getMBeanInfo(getGenericUsageMBeanLookup().getUnnamedSbbUsageMBeanName());
        getLog().info("Searching for accessor methods...");
        MBeanOperationInfo[] mBeanOperationInfos = mBeanInfo.getOperations();
        for (int i = 0; i < mBeanOperationInfos.length; i++) {
            MBeanOperationInfo mBeanOperationInfo = mBeanOperationInfos[i];
            String operationName = mBeanOperationInfo.getName();
            if(operationName.startsWith("get")) {
                if(operationName.equals("getFirstCount")) firstCountAccessorFound = true;
                else if(operationName.equals("getSecondCount")) secondCountAccessorFound = true;
                else if(operationName.equals("getTimeBetweenNewConnections")) timeBetweenNewConnectionsAccessorFound = true;
                else if(operationName.equals("getTimeBetweenErrors")) timeBetweenErrorsAccessorFound = true;
                else if(!operationName.equals("getSbb") && !(operationName.equals("getService"))) {
                    getLog().warning("Unexpected accessor method found in SbbUsageMBean: "+operationName);
                }
            }
        }
        if(!firstCountAccessorFound) throw new TCKTestFailureException(2321,"Counldn't find accessor method for firstCount parameter");
        if(!secondCountAccessorFound) throw new TCKTestFailureException(2321,"Counldn't find accessor method for secondCount parameter");
        if(!timeBetweenNewConnectionsAccessorFound) throw new TCKTestFailureException(2321,"Counldn't find accessor method for timeBetweenNewConnections parameter");
        if(!timeBetweenErrorsAccessorFound) throw new TCKTestFailureException(2321,"Counldn't find accessor method for timeBetweenErrors parameter");

        return TCKTestResult.passed();

    }

}
