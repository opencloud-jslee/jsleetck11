/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.facilities.eventlookupfacility;

import java.util.HashMap;

import javax.slee.resource.ConfigProperties;
import javax.slee.resource.ResourceAdaptorID;

import com.opencloud.sleetck.lib.AbstractSleeTCKTest;
import com.opencloud.sleetck.lib.TCKTestFailureException;
import com.opencloud.sleetck.lib.TCKTestResult;
import com.opencloud.sleetck.lib.infra.SleeTCKComponentConstants;
import com.opencloud.sleetck.lib.profileutils.BaseMessageAdapter;
import com.opencloud.sleetck.lib.rautils.MessageHandlerRegistry;
import com.opencloud.sleetck.lib.rautils.RMIObjectChannel;
import com.opencloud.sleetck.lib.testutils.FutureResult;
import com.opencloud.sleetck.lib.testutils.jmx.ResourceManagementMBeanProxy;

/**
 * AssertionID(1113654): Test This getEventType method returns the event type identifier 
 * of the event type described by the FireableEventType object.
 * 
 * AssertionID(1113655): This getEventClassName method returns the fully-qualified name 
 * of the event class for the event type described by the FireableEventType object. 
 * This is the class name specified in the event type's deployment descriptor.
 * 
 * AssertionID(1113656): Test This getEventClassLoader method returns a class loader for 
 * the event type described by the FireableEventType object.
 * 
 * AssertionID(1113662): Test This getFireableEventType method returns a FireableEventType 
 * object for the event type identified by the eventType argument.
 * 
 * AssertionID(1113663): Test this method throws a java.lang.NullPointerException if the 
 * eventType argument is null.
 * 
 * AssertionID(1113664): Test It throws a javax.slee.UnrecognizedEventException if the 
 * eventType argument does not identify an event type in the SLEE.
 * 
 * AssertionID(1113665): Test An UnrecognizedEventException is also thrown if the eventType 
 * argument does not identify an event type that the Resource Adaptor may fire (as determined 
 * by the resource adaptor types referenced by the Resource Adaptor) unless event type checking 
 * has been disabled for the Resource Adaptor (see Section 15.10).
 **/
public class Test1113654Test extends AbstractSleeTCKTest {
    private static final String RA_NAME = "Test1113654RA";
    private static final String RA_ENTITY_NAME = RA_NAME+ "_Entity";
    private static final String RA_VENDOR = SleeTCKComponentConstants.TCK_VENDOR;
    private static final String RA_VERSION = "1.1";

    private static final String RESOURCE_DU_PATH_PARAM = "RADUPath";

    public TCKTestResult run() throws Exception {

        futureResult = new FutureResult(getLog());

        ResourceManagementMBeanProxy resourceMBean = utils().getResourceManagementMBeanProxy();

        // Create the RA Entity.
        ResourceAdaptorID raID = new ResourceAdaptorID(RA_NAME, RA_VENDOR, RA_VERSION);
        ConfigProperties configProperties = new ConfigProperties();
        resourceMBean.createResourceAdaptorEntity(raID, RA_ENTITY_NAME, configProperties);

        // Activate the RA Entity
        resourceMBean.activateResourceAdaptorEntity(RA_ENTITY_NAME);

        int assertionID = 1;
        if (getTestName().equals("IgnoreCheckFalse")) {
            assertionID = Test1113654MessageListener.CHECK_IGNORE_RA_TYPE_EVENT_TYPE_FALSE;
        }            
        else if (getTestName().equals("IgnoreCheckTrue")) {
            assertionID = Test1113654MessageListener.CHECK_IGNORE_RA_TYPE_EVENT_TYPE_TRUE;
        }
        else {
            getLog().error("Unexpected test name encountered during test run: " + getTestName());
        }
        
        HashMap map = new HashMap();
        map.put("Type", new Integer(assertionID));
        
        //send message to RA entity which starts doing the TXN tests.
        out.sendMessage(map);

        return futureResult.waitForResult(utils().getTestTimeout());
    }

    public void tearDown() throws Exception {
        try {
            in.clearQueue();

            utils().removeRAEntities();
        } finally {
            super.tearDown();
        }

    }

    protected void setPassed(int assertionID, String msg) {
        getLog().fine(assertionID+": "+msg);
        if (futureResult!=null && !futureResult.isSet())
            futureResult.setPassed();
    }

    protected void setFailed(int assertionID, String msg, Exception e) {
        if (e==null) {
            getLog().fine("FAILURE: "+assertionID+":"+msg);
            if (futureResult!=null && !futureResult.isSet())
                futureResult.setFailed(assertionID, msg);
        }
        else {
            getLog().fine("FAILURE: "+assertionID+":"+msg);
            getLog().fine(e);
            if (futureResult!=null && !futureResult.isSet())
                futureResult.setFailed(new TCKTestFailureException(assertionID, msg, e));
        }
    }

    protected void setError(String msg, Exception e) {
        if (e==null) {
            getLog().warning(msg);
            if (futureResult!=null && !futureResult.isSet())
                futureResult.setError(msg);
        }
        else {
            getLog().warning(msg);
            getLog().warning(e);
            if (futureResult!=null && !futureResult.isSet())
                futureResult.setError(msg, e);
        }
    }


    public void setUp() throws Exception {
        setupService(RESOURCE_DU_PATH_PARAM);

        in = utils().getRMIObjectChannel();
        in.setMessageHandler(new BaseMessageAdapter(getLog()) {
            public void onSetPassed(int assertionID, String msg) { setPassed(assertionID, msg); }
            public void onSetFailed(int assertionID, String msg, Exception e) { setFailed(assertionID, msg, e); }
            public void onLogCall(String msg) { getLog().fine(msg); }
            public void onSetError(String msg, Exception e) { setError(msg, e); }
        });

        out = utils().getMessageHandlerRegistry();

    }
    

    public String getTestName() {
        String testName = "testName";
        return utils().getTestParams().getProperty(testName);
    }

    private MessageHandlerRegistry out;
    private RMIObjectChannel in;
    private FutureResult futureResult;
}
