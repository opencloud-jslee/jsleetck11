/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.events.sbbabstractclass;

import java.util.HashMap;

import javax.slee.ActivityContextInterface;
import javax.slee.CreateException;
import javax.slee.facilities.Tracer;
import javax.slee.profile.ProfileAddedEvent;
import javax.slee.profile.ProfileFacility;
import javax.slee.profile.ProfileTableActivity;
import javax.slee.profile.ProfileTableActivityContextInterfaceFactory;

import com.opencloud.sleetck.lib.resource.events.TCKResourceEventX;
import com.opencloud.sleetck.lib.sbbutils.BaseTCKSbb;
import com.opencloud.sleetck.lib.sbbutils.TCKSbbUtils;

/**
 * AssertionID(1108074): Test The SBB object of the SbbContext object must 
 * be in the Ready state, i.e. it must have an assignedSBB entity, when it 
 * invokes this method. Otherwise, this method throws a java.lang.IllegalStateException.
 */
public abstract class Test1108074Sbb extends BaseTCKSbb {
    public static final String PROFILE_TABLE_NAME = "Test1108074ProfileTable";

    public static final String PROFILE_NAME = "Test1108074Profile";

    public void sbbCreate() throws CreateException {

        try {
            tracer = getSbbContext().getTracer("com.test");
            tracer.info("Test1108074Sbb:sbbCreate()");
            ProfileFacility profileFacility = (ProfileFacility) TCKSbbUtils.getSbbEnvironment().lookup(
                    ProfileFacility.JNDI_NAME.replaceFirst("java:comp/env/", ""));
            ProfileTableActivityContextInterfaceFactory profileTableACIFactory = (ProfileTableActivityContextInterfaceFactory) TCKSbbUtils
                    .getSbbEnvironment().lookup(ProfileTableActivityContextInterfaceFactory.JNDI_NAME.replaceFirst("java:comp/env/", ""));
            ProfileTableActivity profileTableActivity = profileFacility.getProfileTableActivity(PROFILE_TABLE_NAME);
            ActivityContextInterface profileTableACI = profileTableACIFactory
                    .getActivityContextInterface(profileTableActivity);

            boolean passed = false;
            try {
                tracer
                        .info("Test1108074Sbb:calling maskEvent() from the sbbCreate() method. (Expecting an IllegalStateException)");
                getSbbContext().maskEvent(new String[] { "ProfileAddedEvent" }, profileTableACI);

            } catch (IllegalStateException ise) {
                tracer
                        .info(
                                "Test1108074Sbb:calling maskEvent() from the sbbCreate() method did throw an IllegalStateException)",
                                null);
                passed = true;
            }

            if (!passed) {
                sendResultToTCK(1108074, "SLEE did not throw IllegalStateException when an SBB with no "
                        + "assigned entity invoked maskEvent on its own SbbContext", "Test1108074Test", false);
            } else
                sendResultToTCK(1108074, "Test passed, calling maskEvent() from the sbbCreate() method did throw " +
                                "IllegalStateException.", "Test1108074Test", true);
            return;
        } catch (Exception e) {
            throw new RuntimeException(e.getMessage());
        }
    }

    public void sbbExceptionThrown(Exception exception, Object event, ActivityContextInterface aci) {
    }

    public void sbbRolledBack(javax.slee.RolledBackContext context) {
    }

    public void onTCKResourceEventX1(TCKResourceEventX event, ActivityContextInterface aci) {
        // no-op -- implemented in name only
    }

    public void onProfileAddedEvent(ProfileAddedEvent event, ActivityContextInterface aci) {
        // no-op -- implemented in name only
    }

    private void sendResultToTCK(int failedAssertionID, String message, String testName, boolean result) throws Exception {
        HashMap sbbData = new HashMap();
        sbbData.put("testname", testName);
        sbbData.put("result", result ? "pass" : "fail");
        sbbData.put("message", message);
        sbbData.put("id", new Integer(failedAssertionID));
        TCKSbbUtils.getResourceInterface().sendSbbMessage(sbbData);
    }

    private Tracer tracer;
}
