/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.usage.SbbUsageMBean;

import com.opencloud.sleetck.lib.TCKTestResult;
import com.opencloud.sleetck.lib.testsuite.usage.common.GenericUsageTest;
import com.opencloud.sleetck.lib.testutils.Assert;
import com.opencloud.sleetck.lib.testutils.QueuingNotificationListener;
import com.opencloud.sleetck.lib.testutils.jmx.SbbUsageMBeanProxy;

import javax.slee.InvalidStateException;
import javax.slee.SbbID;
import javax.slee.ServiceID;

/**
 * Checks that getSbb() and getService() of SbbUsageMBean return correct values,
 * and that SbbUsageMBean.close() throws an InvalidStateException if notification
 * listeners are still attached to it.
 */
public class Test2337_2338_2341Test extends GenericUsageTest {

    public TCKTestResult run() throws Exception {
        ServiceID genericServiceID = getGenericUsageMBeanLookup().getServiceID();
        SbbID genericSbbID = getGenericUsageMBeanLookup().getSbbID();
        SbbUsageMBeanProxy sbbUsageMBeanProxy = getGenericUsageMBeanLookup().getUnnamedSbbUsageMBeanProxy();

        getLog().info("Checking return value of SbbUsageMBean.getSbb()");
        Assert.assertEquals(2338,"The getSbb() method of the SbbUsageMBean for a parameter interface returned "+
                "an invalid SbbID", genericSbbID, sbbUsageMBeanProxy.getSbb());
        getLog().info("Checking return value of SbbUsageMBean.getService()");
        Assert.assertEquals(2337,"The getService() method of the SbbUsageMBean for a parameter interface returned "+
                "an invalid ServiceID", genericServiceID, sbbUsageMBeanProxy.getService());

        QueuingNotificationListener notificationListener = new QueuingNotificationListener(utils());
        sbbUsageMBeanProxy.addNotificationListener(notificationListener,null,null);

        try {
            sbbUsageMBeanProxy.close();
            return TCKTestResult.failed(2341,"The close() methosd of the SbbUsageMBean failed to throw the expected "+
                    "InvalidStateException when a notification listener was still attached to it.");
        } catch (InvalidStateException e) {
            getLog().info("Caught excpected InvalidStateException after trying to close a SbbUsageMBean which still "+
                    "had a notification listener attached to it. Exception message: "+e.getMessage());
        } finally {
            sbbUsageMBeanProxy.removeNotificationListener(notificationListener);
        }

        return TCKTestResult.passed();
    }

}
