/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.transactions;

import javax.slee.ActivityEndEvent;
import javax.slee.ActivityContextInterface;
import javax.slee.SbbContext;
import javax.slee.Address;
import javax.slee.SbbLocalObject;
import javax.slee.facilities.Level;
import javax.slee.ChildRelation;
import javax.slee.CreateException;

import com.opencloud.sleetck.lib.TCKTestErrorException;
import com.opencloud.sleetck.lib.sbbutils.BaseTCKSbb;
import com.opencloud.sleetck.lib.sbbutils.TCKSbbUtils;
import com.opencloud.sleetck.lib.sbbutils.TCKSbbConstants;
import com.opencloud.sleetck.lib.resource.events.TCKResourceEventX;
import com.opencloud.sleetck.lib.resource.sbbapi.TCKResourceSbbInterface;

import java.util.HashMap;
import java.util.Vector;

public abstract class Test842ChildSbb extends BaseTCKSbb {

    public void onTest842SecondEvent(Test842SecondEvent ev, ActivityContextInterface aci) {

        try {
            Object currentTransaction = TCKSbbUtils.getResourceAdaptorInterface().getTransactionIDAccess().getCurrentTransactionID();
            addTxnID(currentTransaction);
        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }

        // Verify that all the TxnIDs are different.
        for (int i = 0; i < txnIDs.size(); i++) {
            for (int j = 0; j < txnIDs.size(); j++) {
                if (i == j)
                    continue;

                if (txnIDs.get(i).equals(txnIDs.get(j))) {
                    HashMap map = new HashMap();
                    map.put("Result", new Boolean(false));
                    map.put("Message", "Used the same TXN ID for two or more event handlers");
                    try {
                        TCKSbbUtils.getResourceInterface().sendSbbMessage(map);
                    } catch (Exception e) {
                        TCKSbbUtils.handleException(e);
                    }
                    return;
                }
            }
        }

        // Passed

        try {
            HashMap map = new HashMap();
            map.put("Result", new Boolean(true));
            map.put("Message", "Ok");
            TCKSbbUtils.getResourceInterface().sendSbbMessage(map);
        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
            return;
        }
    }

    public void addTxnID(Object txnID) {
        txnIDs.add(txnID);
    }

    Vector txnIDs = new Vector();
}
