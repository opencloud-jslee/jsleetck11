/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.profiles;

import com.opencloud.sleetck.lib.AbstractSleeTCKTest;
import com.opencloud.sleetck.lib.TCKTestResult;
import com.opencloud.sleetck.lib.infra.SleeTCKComponentConstants;
import com.opencloud.sleetck.lib.testsuite.profiles.ProfileTestConstants;
import com.opencloud.sleetck.lib.testsuite.profiles.profileverification.IndexedProfileManagementProxy;
import com.opencloud.sleetck.lib.testutils.Assert;
import com.opencloud.sleetck.lib.testutils.ComponentIDLookup;
import com.opencloud.sleetck.lib.testutils.ProfileUtils;
import com.opencloud.sleetck.lib.testutils.jmx.ProfileProvisioningMBeanProxy;
import com.opencloud.sleetck.lib.testutils.jmx.ProfileMBeanProxy;

import javax.management.ObjectName;
import javax.slee.profile.ProfileSpecificationID;
import java.util.Iterator;
import java.util.Vector;


public class Test1097Test extends AbstractSleeTCKTest {

    protected static final String PROFILE_TABLE_NAME = "tck.ProfileVerificationTest.table";
    public static final String PROFILE_SPEC_NAME = "IndexedProfile";

    /**
     * Run some tests on javax.slee.profile.ProfileManagement's method <code>commitProfile</code> and the <code>profileVerify</code> callback.
     * @return
     * @throws java.lang.Exception
     */
    public TCKTestResult run() throws Exception {
        // create the profile table
        ProfileProvisioningMBeanProxy profileProvisioning = profileUtils.getProfileProvisioningProxy();
        ProfileSpecificationID profileSpecID = new ComponentIDLookup(utils()).lookupProfileSpecificationID(
                PROFILE_SPEC_NAME, SleeTCKComponentConstants.TCK_VENDOR, "1.0");

        getLog().info("Creating profile table: " + PROFILE_TABLE_NAME);
        profileProvisioning.createProfileTable(profileSpecID, PROFILE_TABLE_NAME);
        tableCreated = true;

        // add profile
        final String name = "A";
        getLog().info("Creating profile " + name);
        ObjectName jmxObjectName = profileProvisioning.createProfile(PROFILE_TABLE_NAME, name);

        IndexedProfileManagementProxy proxy = getProfileProxy(jmxObjectName);
        proxy.editProfile();
        proxy.setValue("something valid");
        proxy.commitProfile();
        Assert.assertTrue(1097, "commitProfile returned without throwing an exception so changes to the profile " +
                                "should have been committed",
                          !proxy.isProfileDirty() && !proxy.isProfileWriteable()
                          && proxy.getValue().equals("something valid"));

        return TCKTestResult.passed();
    }

    public void setUp() throws Exception {
        String duPath = utils().getTestParams().getProperty(ProfileTestConstants.PROFILE_SPEC_DU_PATH_PARAM);
        getLog().fine("Installing profile specification");
        utils().install(duPath);
        profileUtils = new ProfileUtils(utils());
        activeProxies = new Vector();
    }

    public void tearDown() throws Exception {
        // close profiles
        if (activeProxies != null) {
            getLog().fine("Closing profiles");
            Iterator activeProxiesIter = activeProxies.iterator();
            while (activeProxiesIter.hasNext()) {
                IndexedProfileManagementProxy aProxy = (IndexedProfileManagementProxy) activeProxiesIter.next();
                try {
                    if (aProxy != null) {
                        if (aProxy.isProfileWriteable()) aProxy.restoreProfile();
                        aProxy.closeProfile();
                    }
                } catch (Exception ex) {
                    getLog().warning("Exception caught while trying to close profiles:");
                    getLog().warning(ex);
                }
            }
        }
        // delete profile tables
        try {
            if (profileUtils != null && tableCreated) {
                getLog().fine("Removing profile table");
                profileUtils.removeProfileTable(PROFILE_TABLE_NAME);
            }
        } catch (Exception e) {
            getLog().warning("Caught exception while trying to remove profile table:");
            getLog().warning(e);
        }
        super.tearDown();
    }

    private IndexedProfileManagementProxy getProfileProxy(ObjectName mbeanName) {
        IndexedProfileManagementProxy rProxy = new IndexedProfileManagementProxy(mbeanName, utils().getMBeanFacade());
        activeProxies.addElement(rProxy);
        return rProxy;
    }

    private ProfileUtils profileUtils;
    private boolean tableCreated = false;
    private Vector activeProxies;

}
