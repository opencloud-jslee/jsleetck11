/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.events.maskperacpersbb;


public interface Controllee extends javax.slee.SbbLocalObject{

    public void initialiseMaskFor( javax.slee.ActivityContextInterface aci ) throws javax.slee.UnrecognizedEventException, javax.slee.NotAttachedException;
    public void installController( Controller controller );
    public boolean hasReceivedFooEvent();
    public boolean hasReceivedBarEvent();

}
