/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.profiles.events;

import com.opencloud.sleetck.lib.TCKTestErrorException;
import com.opencloud.sleetck.lib.TCKTestFailureException;
import com.opencloud.sleetck.lib.resource.events.TCKResourceEventX;
import com.opencloud.sleetck.lib.sbbutils.BaseTCKSbb;
import com.opencloud.sleetck.lib.sbbutils.TCKSbbUtils;
import com.opencloud.sleetck.lib.sbbutils.events.TCKSbbEvent;
import com.opencloud.sleetck.lib.sbbutils.events.TCKSbbEventImpl;
import com.opencloud.sleetck.lib.testsuite.profiles.simpleprofile.SimpleProfileCMP;
import com.opencloud.sleetck.lib.testutils.Assert;

import javax.naming.Context;
import javax.slee.ActivityContextInterface;
import javax.slee.Address;
import javax.slee.TransactionRequiredLocalException;
import javax.slee.facilities.Level;
import javax.slee.profile.*;

/**
 * This sbb expects the test to:
 * Create profile tables X and Y.
 * Fire a TCKResourceEventX with configuration data.
 * Add profile A then profile B to X.
 * Update and commits B.
 * Remove A, then remove B.
 *
 * Each method responds to the test with an ACK message, in the form of the full
 * name of the event class received.
 */
public abstract class ProfileEventsTestSbb extends BaseTCKSbb {

    // Event handler methods

    /**
     * This event is sent to inform the Sbb of data to expect from the profile events
     */
    public void onTCKResourceEventX1(TCKResourceEventX event, ActivityContextInterface aci) {
        try {
            TCKSbbUtils.createTrace(getSbbID(),Level.FINE,"Received TCKResourceEventX event",null);
            ProfileEventsTestEventData eventData = ProfileEventsTestEventData.fromExported(event.getMessage());

            // set CMP fields
            setProfileTableName(eventData.getProfileTableName());
            setFirstProfileName(eventData.getFirstProfileName());
            setSecondProfileName(eventData.getSecondProfileName());

            // attach ourself to the profile table activity
            Context env = TCKSbbUtils.getSbbEnvironment();
            ProfileFacility profileFacility = (ProfileFacility)env.lookup("slee/facilities/profile");
            ProfileTableActivity tableActivity = profileFacility.getProfileTableActivity(eventData.getProfileTableName());
            ProfileTableActivityContextInterfaceFactory profileTableACIFactory =
                (ProfileTableActivityContextInterfaceFactory)env.lookup("slee/facilities/profiletableactivitycontextinterfacefactory");
            ActivityContextInterface profileTableACI = profileTableACIFactory.getActivityContextInterface(tableActivity);
            try {
                profileTableACI.attach(getSbbContext().getSbbLocalObject());
            } catch (Exception ex) {
                throw new TCKTestFailureException(939,"Failed to attach to ProfileTableActivity using attach() due to unexpected Exception",ex);
            }

            // fire an event to ourself before replying to the test -- we want to reply to the test after
            // the TXN in which our attach was made commits
            fireTCKSbbEvent(new TCKSbbEventImpl(null),aci,null);
        } catch(Exception e) {
            TCKSbbUtils.handleException(e);
        }
    }

    public void onTCKSbbEvent(TCKSbbEvent event, ActivityContextInterface aci) {
        try {
            // the attach TXN has committed -- send an ACK to the test as a reply to the TCKResourceEventX event
            TCKSbbUtils.getResourceInterface().sendSbbMessage(TCKResourceEventX.class.getName());
        } catch(Exception e) {
            TCKSbbUtils.handleException(e);
        }
    }

    public void onProfileAddedEvent(ProfileAddedEvent event, ActivityContextInterface aci) {
        try {
            checkCMPFields();
            TCKSbbUtils.createTrace(getSbbID(),Level.FINE,"Received ProfileAddedEvent event:"+event,null);
            String expectName = getIsFirstProfileAdded() ? getSecondProfileName() : getFirstProfileName();
            ProfileID expectedProfile = new ProfileID(getProfileTableName(),expectName);
            validateProfileAddedEvent(event,aci,expectedProfile);
            if(!getIsFirstProfileAdded()) setIsFirstProfileAdded(true);
            checkProfileTableTransaction(event);
            // send an ACK to the test
            TCKSbbUtils.getResourceInterface().sendSbbMessage(ProfileAddedEvent.class.getName());
        } catch(Exception e) {
            TCKSbbUtils.handleException(e);
        }
    }

    public void onProfileRemovedEvent(ProfileRemovedEvent event, ActivityContextInterface aci) {
        try {
            checkCMPFields();
            TCKSbbUtils.createTrace(getSbbID(),Level.FINE,"Received ProfileRemovedEvent event:"+event,null);
            String expectName = getIsFirstProfileRemoved() ? getSecondProfileName() : getFirstProfileName();
            ProfileID expectedProfile = new ProfileID(getProfileTableName(),expectName);
            validateProfileRemovedEvent(event,aci,expectedProfile);
            if(!getIsFirstProfileRemoved()) setIsFirstProfileRemoved(true);
            checkProfileTableTransaction(event);
            // send an ACK to the test
            TCKSbbUtils.getResourceInterface().sendSbbMessage(ProfileRemovedEvent.class.getName());
        } catch(Exception e) {
            TCKSbbUtils.handleException(e);
        }
    }

    public void onProfileUpdatedEvent(ProfileUpdatedEvent event, ActivityContextInterface aci) {
        try {
            checkCMPFields();
            TCKSbbUtils.createTrace(getSbbID(),Level.FINE,"Received ProfileUpdatedEvent event",null);
            // check the address
            Address address = event.getProfileAddress();
            Assert.assertTrue(922,"The address plan of a ProfileUpdatedEvent should be AddressPlan.SLEE_PROFILE",
                address.getAddressPlan().isSleeProfile());
            String expectedAddressString = getProfileTableName()+"/"+getSecondProfileName();
            Assert.assertEquals(922,"The address string of a ProfileUpdatedEvent should be Profile identifier "+
                "encoded as profile_table_name/profile_name",expectedAddressString,address.getAddressString());
            // check the profile ID
            ProfileID expectedProfile = new ProfileID(getProfileTableName(),getSecondProfileName());
            Assert.assertEquals(960,"ProfileUpdatedEvent.getProfile() should return the ID of the profile added to the table",
                expectedProfile,event.getProfile());
            // check that the event was fired on the correct activity
            Object activity = aci.getActivity();
            if(activity == null || !(activity instanceof ProfileTableActivity)) throw new TCKTestFailureException(940,
                "ProfileUpdatedEvents must be fired on a ProfileTableActivity");
            ProfileTableActivity profileTableActivity = (ProfileTableActivity)activity;
            Assert.assertEquals(2409,"A ProfileUpdatedEvent was fired on on the wrong ProfileTableActivity "+
            "(the profile table names did not match).",getProfileTableName(),profileTableActivity.getProfileTableName());
            checkProfileTableTransaction(event);
            // send an ACK to the test
            TCKSbbUtils.getResourceInterface().sendSbbMessage(ProfileUpdatedEvent.class.getName());
        } catch(Exception e) {
            TCKSbbUtils.handleException(e);
        }
    }

    // Fire event methods

    public abstract void fireTCKSbbEvent(TCKSbbEvent event, ActivityContextInterface aci, Address address);

    // Private methods

    private void validateProfileAddedEvent(ProfileAddedEvent event, ActivityContextInterface aci,
                            ProfileID expectedProfile) throws TCKTestFailureException {
        // check the address
        Address address = event.getProfileAddress();
        Assert.assertTrue(922,"The address plan of a ProfileAddedEvent should be AddressPlan.SLEE_PROFILE",
            address.getAddressPlan().isSleeProfile());
        String expectedAddressString = expectedProfile.getProfileTableName()+"/"+expectedProfile.getProfileName();
        Assert.assertEquals(922,"The address string of a ProfileAddedEvent should be Profile identifier "+
            "encoded as profile_table_name/profile_name",expectedAddressString,address.getAddressString());

        // check the profile ID
        Assert.assertEquals(946,"getProfile() should return the ID of the profile added to the table",
            expectedProfile,event.getProfile());
        // check that the event was fired on the correct activity
        Object activity = aci.getActivity();
        if(activity == null || !(activity instanceof ProfileTableActivity)) throw new TCKTestFailureException(940,
            "ProfileAddedEvents must be fired on a ProfileTableActivity");
        ProfileTableActivity profileTableActivity = (ProfileTableActivity)activity;
        Assert.assertEquals(935,"A ProfileAddedEvent was fired on on the wrong ProfileTableActivity "+
        "(the profile table names did not match).",getProfileTableName(),profileTableActivity.getProfileTableName());
    }

    private void validateProfileRemovedEvent(ProfileRemovedEvent event, ActivityContextInterface aci,
                            ProfileID expectedProfile) throws TCKTestFailureException {
        // check the address

        Address address = event.getProfileAddress();
        Assert.assertTrue(922,"The address plan of a ProfileRemovedEvent should be AddressPlan.SLEE_PROFILE",
            address.getAddressPlan().isSleeProfile());
        String expectedAddressString = expectedProfile.getProfileTableName()+"/"+expectedProfile.getProfileName();
        Assert.assertEquals(922,"The address string of a ProfileRemovedEvent should be Profile identifier "+
            "encoded as profile_table_name/profile_name",expectedAddressString,address.getAddressString());

        // check the profile ID
        Assert.assertEquals(948,"getProfile() should return the ID of the profile removed from the table",
            expectedProfile,event.getProfile());
        // check that the event was fired on the correct activity
        Object activity = aci.getActivity();
        if(activity == null || !(activity instanceof ProfileTableActivity)) throw new TCKTestFailureException(940,
            "ProfileRemovedEvents must be fired on a ProfileTableActivity");
        ProfileTableActivity profileTableActivity = (ProfileTableActivity)activity;
        Assert.assertEquals(936,"A ProfileRemovedEvent was fired on on the wrong ProfileTableActivity "+
        "(the profile table names did not match).",getProfileTableName(),profileTableActivity.getProfileTableName());
    }

    private void checkProfileTableTransaction(Object event) throws TCKTestFailureException {
        try {
            getSbbContext().getRollbackOnly();
        } catch (TransactionRequiredLocalException trle) {
            throw new TCKTestFailureException(938,"A profile table event was invoked without a valid transaction context:"+
                " getRollbackOnly() threw a TransactionRequiredLocalException. event="+event,trle);
        }
    }

    /**
     * Checks that the profileTableName, firstProfileName and secondProfileName
     * CMP fields have been set to non-null values.
     * Throws a TCKTestErrorException if any of these fields are null.
     */
    private void checkCMPFields() throws TCKTestErrorException {
        if(getProfileTableName() == null || getFirstProfileName() == null ||
            getSecondProfileName() == null) throw new TCKTestErrorException(
                "ProfileEventsTestSbb: The CMP fields must be set to non null values "+
                "before receiving profile and profile table events.");
    }

    // CMP Fields
    public abstract String getProfileTableName();
    public abstract void setProfileTableName(String name);

    public abstract String getFirstProfileName();
    public abstract void setFirstProfileName(String name);

    public abstract String getSecondProfileName();
    public abstract void setSecondProfileName(String name);

    public abstract boolean getIsFirstProfileAdded();
    public abstract void setIsFirstProfileAdded(boolean value);

    public abstract boolean getIsFirstProfileRemoved();
    public abstract void setIsFirstProfileRemoved(boolean value);

    // Profile CMP accessor
    public abstract SimpleProfileCMP getProfileCMP(ProfileID profileID) throws UnrecognizedProfileTableNameException, UnrecognizedProfileNameException;
}
