/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.transactions.isolation;

import com.opencloud.sleetck.lib.resource.events.TCKResourceEventX;
import com.opencloud.sleetck.lib.resource.events.TCKResourceEventY;
import com.opencloud.sleetck.lib.sbbutils.TCKSbbUtils;
import com.opencloud.sleetck.lib.TCKTestErrorException;

import javax.slee.ActivityContextInterface;
import javax.slee.facilities.ActivityContextNamingFacility;
import javax.slee.facilities.Level;

public abstract class Test2004Sbb2 extends Test2004BaseSbb {

    /**
     * Binds the NullActivity's ACI at TCKResourceEventY1's event type name, and
     * unbinds the ACI from INITIAL_ACI_NAME_2, then calls the test with a null
     * argument, or the Exception caught in the attempt to change the ACI bindings.
     */
    public void onTCKResourceEventY1(TCKResourceEventY event, ActivityContextInterface aci) {
        try {
            createTraceSafe(Level.INFO,"Test2004Sbb2.onTCKResourceEventY1(): Transaction ID: "+getTransactionId());
            Exception exceptionCaught = null;
            ActivityContextNamingFacility acNaming = getActivityContextNamingFacility();
            try {
                ActivityContextInterface nullActivityACI = acNaming.lookup(INITIAL_ACI_NAME_1);
                if(nullActivityACI == null) throw new TCKTestErrorException("ActivityContextInterface binding not found at "+
                        INITIAL_ACI_NAME_1+". This test SBB needs to access this binding to continue. Aborting the test...");
                String newLocation = TCKResourceEventY.Y1;
                createTraceSafe(Level.INFO,"Test2004Sbb2.onTCKResourceEventY1(): binding value at: "+newLocation);
                acNaming.bind(nullActivityACI,newLocation);
                createTraceSafe(Level.INFO,"Test2004Sbb2.onTCKResourceEventY1(): removing binding at: "+INITIAL_ACI_NAME_2);
                acNaming.unbind(INITIAL_ACI_NAME_2);
            } catch (TCKTestErrorException te) {
                throw te;
            } catch (Exception aciEx) {
                exceptionCaught = aciEx;
                createTraceSafe(Level.INFO,"Test2004Sbb2.onTCKResourceEventY1(): Exception received while trying to "+
                        "lookup or change bindings in the ActivityContextNamingFacility: "+aciEx);
            }
            sendResponse(TCKResourceEventY.Y1, exceptionCaught);
        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }
    }

    /**
     * Tries to access the ACI using the binding set by X3 (in a concurrent transaction),
     * and INITIAL_ACI_NAME_3 (which the X3 event handler should have tried to unbind),
     * then passes a response to the test.
     * The response is an array of type Object with two elements, one for each lookup attempt.
     * For each lookup attempt, the response is Boolean.TRUE (if the lookup returned a non-null ACI),
     * or the Exception caught while performing the lookup.
     */
    public void onTCKResourceEventY2(TCKResourceEventY event, ActivityContextInterface aci) {
        try {
            createTraceSafe(Level.INFO,"Test2004Sbb2.onTCKResourceEventY2(): Transaction ID: "+getTransactionId());
            createTraceSafe(Level.INFO,"Test2004Sbb2.onTCKResourceEventY2(): looking up bindings...");
            ActivityContextNamingFacility acNaming = getActivityContextNamingFacility();
            String[] names = {TCKResourceEventX.X3,INITIAL_ACI_NAME_3};
            Object[] responses = new Object[names.length];
            for (int i = 0; i < names.length; i++) {
                String name = names[i];
                try {
                    responses[i] = Boolean.valueOf(acNaming.lookup(names[i]) != null);
                    createTraceSafe(Level.INFO,"Test2004Sbb1.onTCKResourceEventY2(): was binding found at "+name+"?:"+responses[i]);
                } catch (Exception aciEx) {
                    responses[i] = aciEx;
                    createTraceSafe(Level.INFO,"Test2004Sbb1.onTCKResourceEventY2(): Exception received while trying to "+
                            "access ACI at "+names[i]);
                }
            }
            sendResponse(TCKResourceEventY.Y2, responses);
        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }
    }


}
