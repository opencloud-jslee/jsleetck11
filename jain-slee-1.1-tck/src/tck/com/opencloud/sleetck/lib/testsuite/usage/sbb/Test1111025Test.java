/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.usage.sbb;

import java.rmi.RemoteException;

import javax.management.Notification;
import javax.management.ObjectName;
import javax.slee.InvalidStateException;
import javax.slee.SbbID;
import javax.slee.management.ManagementException;
import javax.slee.management.NotificationSource;
import javax.slee.management.SbbNotification;
import javax.slee.usage.SampleStatistics;
import javax.slee.usage.UsageNotification;
import javax.slee.usage.UsageOutOfRangeFilter;
import javax.slee.usage.UsageUpdatedFilter;

import com.opencloud.sleetck.lib.OperationTimedOutException;
import com.opencloud.sleetck.lib.TCKTestErrorException;
import com.opencloud.sleetck.lib.TCKTestFailureException;
import com.opencloud.sleetck.lib.TCKTestResult;
import com.opencloud.sleetck.lib.resource.TCKActivityID;
import com.opencloud.sleetck.lib.resource.TCKSbbMessage;
import com.opencloud.sleetck.lib.resource.events.TCKResourceEventX;
import com.opencloud.sleetck.lib.testsuite.usage.common.GenericUsageSbbInstructions;
import com.opencloud.sleetck.lib.testsuite.usage.common.UsageMBeanProxy;
import com.opencloud.sleetck.lib.testsuite.usage.common.UsageMBeanProxyImpl;
import com.opencloud.sleetck.lib.testutils.BaseTCKResourceListener;
import com.opencloud.sleetck.lib.testutils.FutureResult;
import com.opencloud.sleetck.lib.testutils.QueuingNotificationListener;
import com.opencloud.sleetck.lib.testutils.jmx.MBeanProxyFactory;
import com.opencloud.sleetck.lib.testutils.jmx.ServiceUsageMBeanProxy;
import com.opencloud.sleetck.lib.testutils.jmx.UsageNotificationManagerMBeanProxy;

public class Test1111025Test extends BaseUsageTest {
    public static final int TEST_ID = 1111025;
    public static final String PARAM_SET_NAME = "Param set name";

    public TCKTestResult run() throws Exception {
        TCKTestResult result;
        ObjectName n;
        long stats;
        SampleStatistics sample;
        TCKActivityID activityID = utils().getResourceInterface().createActivity("token activity");
        QueuingNotificationListener notificationListener;

        defaultUsage = getSbbUsageMBean(null);
        namedUsage = getSbbUsageMBean(PARAM_SET_NAME);

        n = defaultUsage.getUsageNotificationManagerMBean();
        UsageNotificationManagerMBeanProxy notifierMgr = proxyFactory.createUsageNotificationManagerMBeanProxy(n);


        // Tests start here:

        if (notifierMgr.getFirstCountNotificationsEnabled())
            return TCKTestResult.failed(1111287, "Usage parameter should have defaulted to not enabled.");

        notifierMgr.setFirstCountNotificationsEnabled(false);
        // Test 1111284: getFirstCountNotificationsEnabled() exists.
        if (notifierMgr.getFirstCountNotificationsEnabled())
            return TCKTestResult.failed(1111027, "Setting notifications-enabled to false failed.");

        notifierMgr.setFirstCountNotificationsEnabled(true);
        if (!notifierMgr.getFirstCountNotificationsEnabled())
            return TCKTestResult.failed(1111027, "Setting notifications-enabled to true failed.");
        // Leave it set to true for the next test.

        // Test on a default parameter set.
        getLog().info("Testing on default parameter set.");
        notificationListener = new QueuingNotificationListener(utils());
        defaultUsage.addNotificationListener(notificationListener, null, null);
        fireInstructions(activityID, null);
        result = checkNotification(notificationListener);
        defaultUsage.removeNotificationListener(notificationListener);
        if (null != result) {
            getLog().info("Default parameter sets failed...");
            return result;
        }

        // Also test the values directly.
        stats = defaultUsage.getFirstCount(false);
        if (stats != 3) return TCKTestResult.failed(TEST_ID, "Usage parameter 'firstCount' had unexpected value:"+Long.valueOf(stats).toString());
        stats = defaultUsage.getSecondCount(false);
        if (stats != 5) return TCKTestResult.failed(TEST_ID, "Usage parameter 'secondCount' had unexpected value:"+Long.valueOf(stats).toString());
        sample = defaultUsage.getTimeBetweenNewConnections(false);
        if (sample.getSampleCount() != 1) return TCKTestResult.failed(TEST_ID, "Usage parameter 'timeBetweenConnections' had unexpected value:"+Long.valueOf(sample.getSampleCount()).toString());
        sample = defaultUsage.getTimeBetweenErrors(false);
        if (sample.getSampleCount() != 1) return TCKTestResult.failed(TEST_ID, "Usage parameter 'timeBetweenErrors' had unexpected value:"+Long.valueOf(sample.getSampleCount()).toString());
        // Test that the value will reset.
        defaultUsage.getFirstCount(true);
        stats = defaultUsage.getFirstCount(false);
        if (stats != 0) return TCKTestResult.failed(TEST_ID, "Parameter did not reset to zero.");

        getLog().info("Default parameter set seemed to work okay.");
        defaultUsage.resetAllUsageParameters();

        // Test on a named parameter set.
        getLog().info("Testing on named parameter set.");
        notificationListener = new QueuingNotificationListener(utils());
        namedUsage.addNotificationListener(notificationListener, null, null);
        fireInstructions(activityID, PARAM_SET_NAME);
        result = checkNotification(notificationListener);
        namedUsage.removeNotificationListener(notificationListener);
        if (null != result) {
            getLog().info("Named parameter sets failed...");
            return result;
        }
        getLog().info("Named parameter set seemed to work okay.");

        // Clear the parameter sets.
        namedUsage.resetAllUsageParameters();


        getLog().debug("Service is: "+services[0].toString());
        getLog().debug("sbbid is: "+sbbid.toString());
        NotificationSource usageSource = new SbbNotification(services[0], sbbid);

        // Test to see if the filters work - with a filter that allows notifications.
        getLog().info("Testing parameter filters");
        notificationListener = new QueuingNotificationListener(utils());
        //SLEE 1.1:
        UsageUpdatedFilter usageUpdatedFilter = new UsageUpdatedFilter(usageSource, "firstCount");
        namedUsage.addNotificationListener(notificationListener, usageUpdatedFilter, null);
        fireInstructions(activityID, PARAM_SET_NAME);
        result = checkNotification(notificationListener);
        if (null != result) {
            getLog().info("Filter tests failed...");
            return result;
        }
        namedUsage.removeNotificationListener(notificationListener);

        // Test to see if the filters work - with a filter that blocks notifications.
        UsageOutOfRangeFilter usageOutOfRangeFilter = new UsageOutOfRangeFilter(usageSource, "firstCount", 1, 1);
        notificationListener = new QueuingNotificationListener(utils());
        namedUsage.addNotificationListener(notificationListener, usageOutOfRangeFilter, null);
        fireInstructions(activityID, PARAM_SET_NAME);
        Notification notification;
        try {
            do {
                notification = notificationListener.nextNotification();
            } while (SbbNotification.USAGE_NOTIFICATION_TYPE.equals(notification.getType()));
            return TCKTestResult.failed(1111224, "UsageOutOfRangeFilter should not let this notification through.");
        } catch (OperationTimedOutException e) { /* is expected.*/ }

        try {
            namedUsage.close();
            return TCKTestResult.failed(1111277, "UsageMBean.close() should have thrown a InvalidStateException");
        } catch (InvalidStateException e) {
            // good - there are still listeners attached.
        }
        getLog().info("Filters seem to work okay.");

        namedUsage.removeNotificationListener(notificationListener);

        notifierMgr.close();
        defaultUsage.close();
        namedUsage.close();

        return TCKTestResult.passed();
    }

    private TCKTestResult checkNotification(QueuingNotificationListener l) throws ManagementException, TCKTestErrorException {
        // I should receive two notifications: one from firstCount and one from secondCount only.
        Notification notification;
        try {
            notification = l.nextNotification();
        } catch (OperationTimedOutException e) {
            return TCKTestResult.failed(new TCKTestFailureException(TEST_ID, "Did not receive notification", e));
        }

        UsageNotification un = (UsageNotification)notification;
        SbbNotification sn = (SbbNotification)un.getNotificationSource();

        getLog().debug("Got notification:"+notification.toString());

        if ("firstCount".equals(un.getUsageParameterName())) {
            getLog().debug("Default parameter set, firstCount value from the notification is: "+un.getValue());
            getLog().debug("Default parameter set, firstCount value directly is: "+defaultUsage.getFirstCount(false));
            if (3 != un.getValue())
                return TCKTestResult.failed(TEST_ID, "firstCount parameter has unexpected value:"+Long.valueOf(un.getValue()).toString());
        } else if ("secondCount".equals(un.getUsageParameterName())) {
            getLog().debug("Default parameter set, secondCount value from the notification is: "+un.getValue());
            getLog().debug("Default parameter set, secondCount value directly is: "+defaultUsage.getSecondCount(false));
            if (5 != un.getValue())
                return TCKTestResult.failed(TEST_ID, "secondCount parameter has unexpected value:"+Long.valueOf(un.getValue()).toString());
        } else return TCKTestResult.failed(TEST_ID, "Usage parameter set name unknown."+un.getUsageParameterSetName());

        if (null == un.getUsageParameterSetName()) { // the default set
            try {
                if (!serviceUsage.getSbbUsageMBean(sbbid).equals(un.getSource()))
                    return TCKTestResult.failed(TEST_ID, "UsageNotification source for default set was not the expected value.");
            } catch(Exception e) {
                return TCKTestResult.error(e);
            }
        } else if (PARAM_SET_NAME.equals(un.getUsageParameterSetName())) {
            try {
                if (!serviceUsage.getSbbUsageMBean(sbbid, PARAM_SET_NAME).equals(un.getSource()))
                    return TCKTestResult.failed(TEST_ID, "UsageNotification source for named set was not the expected value.");
            } catch (Exception e) {
                return TCKTestResult.error(e);
            }
        } else {
            return TCKTestResult.failed(TEST_ID, "Usage parameter set unknown:"+un.getUsageParameterSetName());
        }

        if (!SbbNotification.USAGE_NOTIFICATION_TYPE.equals(sn.getUsageNotificationType()))
            return TCKTestResult.failed(TEST_ID, "Usage Notification type was not what I expected.");
        if (!sn.getSbb().equals(sbbid))
            return TCKTestResult.failed(TEST_ID, "Usage Notification SbbID was not what I expected.");
        if (!sn.getService().equals(services[0]))
            return TCKTestResult.failed(TEST_ID, "Usage Notification ServiceID was not what I expected.");
        return null;
    }


    private UsageMBeanProxy getSbbUsageMBean(String paramSetName) throws TCKTestErrorException {
        ObjectName n;
        try {
            if (null == paramSetName)
                n = serviceUsage.getSbbUsageMBean(sbbid); // Get the default usage parameter set.
            else
                n = serviceUsage.getSbbUsageMBean(sbbid, paramSetName); // Get a named usage parameter set.
            return new UsageMBeanProxyImpl(n, utils().getMBeanFacade());
        } catch(Exception e) {
            throw new TCKTestErrorException("Failed to get SBB usage proxy.", e);
        }
    }

    private void fireInstructions(TCKActivityID activityID, String paramSetName) throws TCKTestErrorException, RemoteException {
        GenericUsageSbbInstructions instr = new GenericUsageSbbInstructions(paramSetName);
        instr.addFirstCountIncrement(3);  // enabled-notifications is not set, should default to "False"
        instr.addSecondCountIncrement(5); // enabled-notifications="True"
        instr.addTimeBetweenNewConnectionsSamples(7); // enabled-notifications is not set, should default to false.
        instr.addTimeBetweenErrorsSample(11); // enabled-notification="False"

        getLog().info("firing event to Sbb");
        utils().getResourceInterface().fireEvent(TCKResourceEventX.X1, instr.toExported(), activityID, null);
    }

    public void setUp() throws Exception {
        super.setUp();

        int i;
        boolean done = false;
        for (i=0; i<services.length; i++) {
            // There should only be one service.
            if (done)
                throw new TCKTestErrorException("There was more than one service in this test.");
            ObjectName serviceUsageName = utils().getServiceManagementMBeanProxy().getServiceUsageMBean(services[i]);
            serviceUsage = utils().getMBeanProxyFactory().createServiceUsageMBeanProxy(serviceUsageName);
        }

        sbbid = new SbbID("GenericUsageSbb", "jain.slee.tck", "1.1");
        serviceUsage.createUsageParameterSet(sbbid, PARAM_SET_NAME);

        utils().getLog().fine("Connecting to resource");
        resourceListener = new TCKResourceListenerImpl();
        utils().getResourceInterface().setResourceListener(resourceListener);

        // short-hand:
        proxyFactory = utils().getMBeanProxyFactory();
    }

    private class TCKResourceListenerImpl extends BaseTCKResourceListener {
        public synchronized void onSbbMessage(TCKSbbMessage message, TCKActivityID activity) throws RemoteException {
            utils().getLog().fine("Received message from SBB: " + message.getMessage());
        }

        public void onException(Exception e) throws RemoteException {
            utils().getLog().warning("Received exception from SBB.");
            utils().getLog().warning(e);
            result.setError(e);
        }
    }

    private TCKResourceListenerImpl resourceListener;
    private SbbID sbbid;
    private MBeanProxyFactory proxyFactory;
    private FutureResult result;
    protected ServiceUsageMBeanProxy serviceUsage;
    private UsageMBeanProxy defaultUsage, namedUsage;
}
