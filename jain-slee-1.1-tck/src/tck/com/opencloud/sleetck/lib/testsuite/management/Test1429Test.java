/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.management;

import com.opencloud.sleetck.lib.*;
import com.opencloud.sleetck.lib.resource.testapi.TCKResourceListener;
import com.opencloud.sleetck.lib.testutils.BaseTCKResourceListener;
import com.opencloud.sleetck.lib.testutils.jmx.impl.EmptyArrays;

import javax.management.*;
import javax.slee.management.DeployableUnitID;
import javax.slee.management.ManagementException;
import javax.slee.management.SleeState;
import java.rmi.RemoteException;

public class Test1429Test implements SleeTCKTest {

    private static final int TEST_ID = 1429;

    public void init(SleeTCKTestUtils utils) {
        this.utils = utils;
    }

    /**
     * Perform the actual test.
     */
    public TCKTestResult run() throws Exception {

        // Here we want to test that get/set methods in the various MBean
        // objects can be accessed via get/set and invoke.

        // Chosen test cases:
        //   SleeManagementMBean.getState()
        //   DeploymentMBean.getDeployableUnits()
        //   ProfileProvisioningMBean.getProfileTables()

        // N.B. there are no set cases currently defined in the
        // javax.slee.management.*MBean classes, nor are there any isFoo
        // cases.

        ObjectName sleeObject = utils.getSleeManagementMBeanName();
        
        Object result = getViaGetAttribute(sleeObject, "State");

        utils.getLog().fine("State attribute is of type: "+ result.getClass().toString());

        SleeState attributeState = (SleeState) getViaGetAttribute(sleeObject, "State");
        SleeState invokeState = (SleeState) getViaInvoke(sleeObject, "State");

        // Check for same result.
        if (!attributeState.equals(invokeState))
            return TCKTestResult.failed(TEST_ID, "Value returned by invoke was not the same as that returned by getAttribute.");

        // Now do DeploymentMBean.getDeployableUnits()
        ObjectName deploymentObject = (ObjectName) getViaInvoke(sleeObject, "DeploymentMBean");

        DeployableUnitID attributeDuIDs[] = (DeployableUnitID []) getViaGetAttribute(deploymentObject, "DeployableUnits");
        DeployableUnitID invokeDuIDs[] = (DeployableUnitID []) getViaInvoke(deploymentObject, "DeployableUnits");

        // Ensure that the contents of attributeDuIDs is the same as invokeDuIDs
        if (attributeDuIDs.length != invokeDuIDs.length)
            return TCKTestResult.failed(TEST_ID, "DU set received via getAttribute is not the same as the set received via invoke.");

        outer: for (int i = 0; i < attributeDuIDs.length; i++) {
            for (int j = 0; j < invokeDuIDs.length; j++) {
                if (attributeDuIDs[i].equals(invokeDuIDs[j])) {
                    continue outer;
                }
            }

            return TCKTestResult.failed(TEST_ID, "DU retrieved via getAttribute was not found in set retrieved via invoke.");
        }

        // ProfileProvisioningMBean.getProfileTables
        ObjectName profileObject = (ObjectName) getViaInvoke(sleeObject, "ProfileProvisioningMBean");

        getViaGetAttribute(profileObject, "ProfileTables");
        getViaInvoke(profileObject, "ProfileTables");
        // Can't test for equality of results here, but any exceptions that
        // prevented either access method being used will have been caught
        // already.

        return TCKTestResult.passed();

    }

    /**
     * Do all the pre-run configuration of the test.
     */
    public void setUp() throws Exception {
        utils.getLog().fine("Connecting to resource");
        resourceListener = new TCKResourceListenerImpl();
        utils.getResourceInterface().setResourceListener(resourceListener);
    }

    /**
     * Clean up after the test.
     */

    public void tearDown() throws Exception {
        utils.getLog().fine("Disconnecting from resource");
        utils.getResourceInterface().clearActivities();
        utils.getResourceInterface().removeResourceListener();
        utils.getLog().fine("Uninstalling the profile spec");
        utils.uninstallAll();
    }

// Maintenance note: the setViaSetAttribute() is not used, as there are currently no setter methods
//  which follow design pattern for a managed attribute in the SLEE management MBeans.
//  If any are added, the setViaSetAttribute() method should be activated, and the appropriate test cases added.

//    /**
//     * Sets the attribute using MBeanServer.setAttribute()
//     */
//    private void setViaSetAttribute(ObjectName objName, String attribute, String value)
//        throws ManagementException, TCKTestErrorException, TCKTestFailureException {
//       try {
//            utils.getMBeanFacade().setAttribute(objName, new Attribute(attribute, value));
//       } catch(InstanceNotFoundException ie) {
//           throw new TCKTestErrorException("Caught Exception while calling MBeanServer.getAttribute()",ie);
//       } catch(ReflectionException re) {
//           throw new TCKTestFailureException(TEST_ID,"Caught ReflectionException while calling MBeanServer.setAttribute(...,\"AttributeA\",...)",re);
//       } catch(AttributeNotFoundException e) {
//           throw new TCKTestFailureException(TEST_ID,"Caught AttributeNotFoundException while calling MBeanServer.setAttribute(...,\"AttributeA\",...)", e);
//       } catch(InvalidAttributeValueException e) {
//           throw new TCKTestFailureException(TEST_ID,"Caught InvalidAttributeValueException while calling MBeanServer.setAttribute(...,\"AttributeA\",...)", e);
//       } catch(MBeanException e) {
//           Exception enclosed = e.getTargetException();
//           if(enclosed instanceof ManagementException) throw (ManagementException)enclosed;
//           if(enclosed instanceof RuntimeException) throw (RuntimeException)enclosed;
//           throw new TCKTestErrorException("Caught unexpected exception",enclosed);
//       }
//    }

    /**
     * Gets the attribute using MBeanServer.getAttribute()
     */
    private Object getViaGetAttribute(ObjectName objName, String attribute)
            throws ManagementException, TCKTestErrorException, TCKTestFailureException {
        try {
            return utils.getMBeanFacade().getAttribute(objName, attribute);
        } catch(InstanceNotFoundException ie) {
            throw new TCKTestErrorException("Caught Exception while calling MBeanServer.getAttribute()",ie);
        } catch(ReflectionException re) {
            throw new TCKTestFailureException(TEST_ID,"Caught ReflectionException while calling MBeanServer.getAttribute(\"AttributeA\")",re);
        } catch(AttributeNotFoundException e) {
            throw new TCKTestFailureException(TEST_ID,"Caught AttributeNotFoundException while calling MBeanServer.getAttribute(\"AttributeA\")", e);
        } catch(MBeanException e) {
            Exception enclosed = e.getTargetException();
            if(enclosed instanceof ManagementException) throw (ManagementException)enclosed;
            if(enclosed instanceof RuntimeException) throw (RuntimeException)enclosed;
            throw new TCKTestErrorException("Caught unexpected exception",enclosed);
        }
    }

// Maintenance note: the setViaInvoke() is not used, as there are currently no setter methods
//  which follow design pattern for a managed attribute in the SLEE management MBeans.
//  If any are added, the setViaSetAttribute() method should be activated, and the appropriate test cases added.

//    /**
//     * Sets the attribute using MBeanServer.invoke(...,"set<attribute>",...)
//     */
//    private void setViaInvoke(ObjectName objName, String attribute, String value) throws TCKTestErrorException, TCKTestFailureException {
//      try {
//         utils.getMBeanFacade().invoke(objName,"set" + attribute, new Object[]{value},new String[]{"java.lang.String"});
//      } catch(InstanceNotFoundException ie) {
//         throw new TCKTestErrorException("Caught Exception while calling MBeanServer.invoke(...,\"set" + attribute + "\",...)",ie);
//      } catch(ReflectionException re) {
//         throw new TCKTestFailureException(TEST_ID,"Caught ReflectionException while calling MBeanServer.invoke(...,\"set" + attribute + "\",...)",re);
//      } catch(MBeanException e) {
//         Exception enclosed = e.getTargetException();
//         if(enclosed instanceof RuntimeException) throw (RuntimeException)enclosed;
//         throw new TCKTestErrorException("Caught unexpected exception",enclosed);
//      }
//    }

    /**
     * Gets the attribute using MBeanServer.invoke(...,"get<attribute>",...)
     */
    private Object getViaInvoke(ObjectName objName, String attribute) throws TCKTestErrorException, TCKTestFailureException {
        try {
            return utils.getMBeanFacade().invoke(objName,"get" + attribute,
                    EmptyArrays.EMPTY_OBJECT_ARRAY,EmptyArrays.EMPTY_STRING_ARRAY);
        } catch(InstanceNotFoundException ie) {
            throw new TCKTestErrorException("Caught Exception while calling MBeanServer.invoke(...,\"get" + attribute + "\",...)",ie);
        } catch(ReflectionException re) {
            throw new TCKTestFailureException(TEST_ID,"Caught ReflectionException while calling MBeanServer.invoke(...,\"get" + attribute + "\",...)",re);
        } catch(MBeanException e) {
            Exception enclosed = e.getTargetException();
            if(enclosed instanceof RuntimeException) throw (RuntimeException)enclosed;
            throw new TCKTestErrorException("Caught unexpected exception",enclosed);
        }
    }

    private class TCKResourceListenerImpl extends BaseTCKResourceListener {
        public void onException(Exception e) throws RemoteException {
            utils.getLog().warning("Received exception from the TCK resource");
            utils.getLog().warning(e);
        }
    }

    private SleeTCKTestUtils utils;
    private TCKResourceListener resourceListener;

}
