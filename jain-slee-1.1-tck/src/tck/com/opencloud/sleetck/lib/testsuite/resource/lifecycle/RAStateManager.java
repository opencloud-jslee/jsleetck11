/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.resource.lifecycle;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Set;

import com.opencloud.logging.Logable;
import com.opencloud.sleetck.lib.TCKTestErrorException;
import com.opencloud.sleetck.lib.TCKTestFailureException;
import com.opencloud.sleetck.lib.rautils.JVMUID;
import com.opencloud.sleetck.lib.rautils.UOID;
import com.opencloud.sleetck.lib.testsuite.resource.RAMethods;
import com.opencloud.sleetck.lib.testsuite.resource.TCKMessage;

/**
 * Resource Adaptor State Manager
 * <p>
 * This class keeps a record of the state of a Resource Adaptor and all its
 * created objects. This is necessary as a SLEE may be doing something clever
 * with pools of Resource Adaptor objects, and there's no way to tell whether a
 * lifecycle state transition is legal unless the state of each object is
 * tracked individually.
 * <p>
 * Covers assertions: 1115504, 1115515, 1115525, 1115530, 1115535, 1115519,
 * 1115512, 1115053
 */
public class RAStateManager {

    public static final int STATE_IMPOSSIBLE = -2;
    public static final int STATE_DOES_NOT_EXIST = -1;
    public static final int STATE_CONSTRUCTED = 0;
    public static final int STATE_UNCONFIGURED = 1;
    public static final int STATE_INACTIVE = 2;
    public static final int STATE_ACTIVE = 3;
    public static final int STATE_STOPPING = 4;

    public RAStateManager(Logable log) {
        this.states = new HashMap();
        this.log = log;
    }

    /**
     * Runs a sequence of LifecycleMessages through the RA State machine in
     * order. If an illegal state transition occurs, a TCKTestFailureException
     * will be thrown with an appropriate assertion ID. If an impossible or
     * unhandled case occurs (such as receiving multiple constructor messages
     * for an RA object), then a TCKTestErrorException will be thrown.
     */
    public void runStateMachine(TCKMessage[] messages) throws TCKTestErrorException, TCKTestFailureException {
        for (int i = 0; i < messages.length; i++)
            handleMessage(messages[i]);
    }

    /**
     * Returns true if an RA Object exists with a state of 'state'.
     */
    public boolean hasRAObjectInState(int state) {
        Set keys = states.keySet();
        Iterator iter = keys.iterator();
        while (iter.hasNext()) {
            UOID key = (UOID) iter.next();
            if (getState(key) == state)
                return true;
        }
        return false;
    }

    //
    // Private
    //

    private void handleMessage(TCKMessage message) throws TCKTestErrorException, TCKTestFailureException {
        int method = message.getMethod();
        UOID rauid = message.getUID();
        int currentState = getState(rauid);

        log.debug("RAStateManager processing message from RA Object: " + message);

        int newState = currentState;
        switch (method) {
        case RAMethods.constructor:
            // DOES_NOT_EXIST -> CONSTRUCTED
            log.debug("Initialising lifecycle state for RAUID: " + rauid);
            if (currentState != STATE_DOES_NOT_EXIST)
                throw new TCKTestErrorException("RA Object constructor impossibly called multiple times: rauid=" + rauid);
            newState = STATE_CONSTRUCTED;
            break;

        case RAMethods.setResourceAdaptorContext:
            // CONSTRUCTED --> UNCONFIGURED
            if (currentState != STATE_CONSTRUCTED)
                throw createTCKTestFailureException(1115504, method, rauid, currentState);
            newState = STATE_UNCONFIGURED;
            break;

        case RAMethods.raConfigure:
            // UNCONFIGURED --> INACTIVE
            if (currentState != STATE_UNCONFIGURED)
                throw createTCKTestFailureException(1115515, method, rauid, currentState);
            newState = STATE_INACTIVE;
            break;

        case RAMethods.raActive:
            // INACTIVE -> ACTIVE
            if (currentState != STATE_INACTIVE)
                throw createTCKTestFailureException(1115525, method, rauid, currentState);
            newState = STATE_ACTIVE;
            break;

        case RAMethods.raStopping:
            // ACTIVE -> STOPPING
            if (currentState != STATE_ACTIVE)
                throw createTCKTestFailureException(1115530, method, rauid, currentState);
            newState = STATE_STOPPING;
            break;

        case RAMethods.raInactive:
            // STOPPING -> INACTIVE
            if (currentState != STATE_STOPPING)
                throw createTCKTestFailureException(1115535, method, rauid, currentState);
            newState = STATE_INACTIVE;
            break;

        case RAMethods.raUnconfigure:
            // INACTIVE --> UNCONFIGURED
            if (currentState != STATE_INACTIVE)
                throw createTCKTestFailureException(1115519, method, rauid, currentState);
            newState = STATE_UNCONFIGURED;
            break;

        case RAMethods.unsetResourceAdaptorContext:
            // UNCONFIGURED --> CONSTRUCTED
            if (getState(rauid) != STATE_UNCONFIGURED)
                throw createTCKTestFailureException(1115512, method, rauid, currentState);
            newState = STATE_CONSTRUCTED;
            break;

        case RAMethods.finalizer:
            // CONSTRUCTED -> DOES_NOT_EXIST
            if (currentState != STATE_CONSTRUCTED)
                throw new TCKTestErrorException("Finalizer called on RA Object in invalid state: " + getStateString(currentState));
            newState = STATE_DOES_NOT_EXIST;
            break;
        default:
            // Ignore non-lifecycle method calls (e.g.
            // verifyEntityConfiguration).
            return;
        }

        setState(rauid, newState);
        checkForMultipleConfiguredRAObjects();
    }

    // Private

    private void setState(UOID rauid, int state) {
        states.put(rauid, new Integer(state));
    }

    private int getState(UOID rauid) {
        Integer state = (Integer) states.get(rauid);
        if (state != null)
            return state.intValue();
        return STATE_DOES_NOT_EXIST;
    }

    private TCKTestFailureException createTCKTestFailureException(int assertionId, int method, UOID rauid, int state) {
        return new TCKTestFailureException(assertionId, "Illegal call to method " + RAMethods.getMethodName(method) + "() in state " + getStateString(state)
                + " on RA Object: " + rauid);
    }

    /**
     * Special check to ensure that we never have multiple RA objects in the
     * configured state in a particular JVM. The SLEE 1.1 specification states
     * that you may only ever have one RA object in the configured or above
     * state (per JVM).
     */
    private void checkForMultipleConfiguredRAObjects() throws TCKTestFailureException {

        HashMap jvmConfiguredRAObjects = new HashMap();

        Set keys = states.keySet();
        Iterator iter = keys.iterator();
        while (iter.hasNext()) {
            UOID key = (UOID) iter.next();
            JVMUID jvmuid = key.getJVMUID();

            int raObjectState = getState(key);
            // Ignore these states as we're only interested in the 'configured'
            // states.
            if (raObjectState == STATE_DOES_NOT_EXIST || raObjectState == STATE_CONSTRUCTED || raObjectState == STATE_UNCONFIGURED)
                continue;

            // If we already have state for a particular JVM, we've encountered
            // an error. The JVM state is only set when a configured RA Object
            // is encountered, so if JVM state already exists a configured RA
            // object alread exists.
            if (jvmConfiguredRAObjects.get(jvmuid) != null) {
                UOID firstKey = (UOID) jvmConfiguredRAObjects.get(jvmuid);
                throw new TCKTestFailureException(1115053, "Multiple configured Resource Adaptor Objects exist in a single JVM: RAUID_1=" + firstKey
                        + ", RAUID_2=" + key);
            } else {
                jvmConfiguredRAObjects.put(jvmuid, key);
            }
        }
    }

    public static String getStateString(int state) {
        switch (state) {
        case STATE_IMPOSSIBLE:
            return "IMPOSSIBLE";
        case STATE_DOES_NOT_EXIST:
            return "DOES_NOT_EXIST";
        case STATE_CONSTRUCTED:
            return "CONSTRUCTED";
        case STATE_UNCONFIGURED:
            return "UNCONFIGURED";
        case STATE_INACTIVE:
            return "INACTIVE";
        case STATE_ACTIVE:
            return "ACTIVE";
        case STATE_STOPPING:
            return "STOPPING";
        default:
            return "UNKNOWN_STATE";
        }
    }

    private final HashMap states;
    private final Logable log;

}
