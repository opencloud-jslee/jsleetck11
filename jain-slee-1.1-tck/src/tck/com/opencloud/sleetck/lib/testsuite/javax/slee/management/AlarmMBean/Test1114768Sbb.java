/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.javax.slee.management.AlarmMBean;

import java.util.HashMap;

import javax.naming.InitialContext;
import javax.slee.ActivityContextInterface;
import javax.slee.facilities.AlarmFacility;
import javax.slee.facilities.AlarmLevel;

import com.opencloud.sleetck.lib.resource.events.TCKResourceEventX;
import com.opencloud.sleetck.lib.sbbutils.BaseTCKSbb;
import com.opencloud.sleetck.lib.sbbutils.TCKSbbUtils;

/*
 * Raise 3 alarms for SBB Notification then send response to Test
 */

public abstract class Test1114768Sbb extends BaseTCKSbb {

    private static final String JNDI_ALARMFACILITY_NAME = AlarmFacility.JNDI_NAME;
    public static final String ALARM_MESSAGE = "Test1114768SbbAlarmMessage";
    public static final String ALARM_INSTANCEID1 = "Test1114768SbbAlarmInstanceID1";
    public static final String ALARM_INSTANCEID2 = "Test1114768SbbAlarmInstanceID2";
    public static final String ALARM_INSTANCEID3 = "Test1114768SbbAlarmInstanceID3";

    // Initial event
    public void onTCKResourceEventX1(TCKResourceEventX ev, ActivityContextInterface aci) {
        HashMap map = new HashMap();

        try {
            AlarmFacility facility = (AlarmFacility) new InitialContext().lookup(JNDI_ALARMFACILITY_NAME);
            String AlarmID1 = facility.raiseAlarm("javax.slee.management.Alarm", ALARM_INSTANCEID1, AlarmLevel.MAJOR, ALARM_MESSAGE);
            String AlarmID2 = facility.raiseAlarm("javax.slee.management.Alarm", ALARM_INSTANCEID2, AlarmLevel.MAJOR, ALARM_MESSAGE);
            String AlarmID3 = facility.raiseAlarm("javax.slee.management.Alarm", ALARM_INSTANCEID3, AlarmLevel.MAJOR, ALARM_MESSAGE);

            map.put("result", new Boolean(true));
            map.put("message", "SBB has raised Alarm: " +ALARM_MESSAGE );
            map.put("id", new Integer(1114768));

            TCKSbbUtils.getResourceInterface().sendSbbMessage(map);


        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }

    }

}
