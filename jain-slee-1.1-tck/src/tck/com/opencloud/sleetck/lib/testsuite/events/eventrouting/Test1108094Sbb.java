/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.events.eventrouting;

import java.util.HashMap;

import javax.slee.ActivityContextInterface;
import javax.slee.Address;
import javax.slee.AddressPlan;
import javax.slee.EventContext;
import javax.slee.InitialEventSelector;
import javax.slee.facilities.Tracer;

import com.opencloud.sleetck.lib.resource.events.TCKResourceEventX;
import com.opencloud.sleetck.lib.sbbutils.BaseTCKSbb;
import com.opencloud.sleetck.lib.sbbutils.TCKSbbUtils;

/**
 * AssertionID(1108094): Test If the addressProfileSelected variable is set to true 
 * and the address is not null but does not locate any Address Profile in the Address 
 * Profile Table of the Service, then no convergence name is created, i.e. 
 * same as setting the initialEvent attribute to false.
 */
public abstract class Test1108094Sbb extends BaseTCKSbb {

    //  -- Initial event selector method -- //

    public InitialEventSelector initialEventSelectorMethod(InitialEventSelector ies) {
        try {
            ies.setAddressProfileSelected(true);

            // set the custom name variable to true to test that this value is not held accross invocations
            ies.setCustomName("test");
        } catch (Exception ex) {
            TCKSbbUtils.handleException(ex);
        }
        return ies;
    }

    // Initial event method.
    public void onTCKResourceEventX1(TCKResourceEventX event, ActivityContextInterface aci, EventContext context) {
        try {
            tracer = getSbbContext().getTracer("com.test");
            Address address = context.getAddress();
            tracer.info("Sbb: Received a TCK event " + event + " on " + address);

            sendResultToTCK(1108094, "Received a TCK event, sending it back to the TestHarness side.", "Test1108094Test", true);

        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }
    }

    private void sendResultToTCK(int failedAssertionID, String message, String testName, boolean result) throws Exception {
        HashMap sbbData = new HashMap();
        sbbData.put("testname", testName);
        sbbData.put("result", result ? "pass" : "fail");
        sbbData.put("message", message);
        sbbData.put("id", new Integer(failedAssertionID));
        TCKSbbUtils.getResourceInterface().sendSbbMessage(sbbData);
    }

    private Tracer tracer;
    
    public static final Address ADDRESS_1 = new Address(AddressPlan.IP, "1.0.0.1");

    public static final Address ADDRESS_2 = new Address(AddressPlan.IP, "1.0.0.2");

    public static final Address ADDRESS_3 = new Address(AddressPlan.IP, "101.210.30.3");

    public static final Address ADDRESS_4 = new Address(AddressPlan.IP, "101.210.30.4");
}
