/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.javax.slee.profile.ProfileContext;

import javax.management.ObjectName;
import javax.slee.profile.ProfileSpecificationID;

import com.opencloud.sleetck.lib.AbstractSleeTCKTest;
import com.opencloud.sleetck.lib.TCKTestFailureException;
import com.opencloud.sleetck.lib.TCKTestResult;
import com.opencloud.sleetck.lib.infra.SleeTCKComponentConstants;
import com.opencloud.sleetck.lib.profileutils.BaseMessageAdapter;
import com.opencloud.sleetck.lib.rautils.RMIObjectChannel;
import com.opencloud.sleetck.lib.resource.TCKActivityID;
import com.opencloud.sleetck.lib.resource.events.TCKResourceEventX;
import com.opencloud.sleetck.lib.resource.testapi.TCKResourceTestInterface;
import com.opencloud.sleetck.lib.testutils.FutureAssertionsResult;
import com.opencloud.sleetck.lib.testutils.ProfileUtils;
import com.opencloud.sleetck.lib.testutils.jmx.ProfileMBeanProxy;
import com.opencloud.sleetck.lib.testutils.jmx.ProfileProvisioningMBeanProxy;


public class Test1110346Test extends AbstractSleeTCKTest {

    private static final String SBB_EVENT_DU_PATH = "sbbEventDUPath";
    private static final String DU_PATH_PARAM= "DUPath";

    private static final String SPEC_NAME = "Test1110346Profile";
    private static final String SPEC_VERSION = "1.0";

    private static final int TEST_ID = 1110346;

    /**
     * This test checks various assertions for the javax.slee.profile.ProfileContext
     * class as outlined in the JAIN SLEE 1.1 API JavaDoc
     */
    public TCKTestResult run() throws Exception {

        TCKTestResult res;
        exResult = null;

        //create a profile table
        ProfileSpecificationID specID = new ProfileSpecificationID(SPEC_NAME, SleeTCKComponentConstants.TCK_VENDOR, SPEC_VERSION);
        profileProvisioning.createProfileTable(specID, Test1110346Sbb.PROFILE_TABLE_NAME);
        getLog().fine("Added profile table "+Test1110346Sbb.PROFILE_TABLE_NAME+" based on profile spec "+SPEC_NAME);

        //create a profile
        ObjectName profile = profileProvisioning.createProfile(Test1110346Sbb.PROFILE_TABLE_NAME, Test1110346Sbb.PROFILE_NAME);
        ProfileMBeanProxy profileProxy = utils().getMBeanProxyFactory().createProfileMBeanProxy(profile);
        getLog().fine("Created profile "+Test1110346Sbb.PROFILE_NAME+" for profile table "+Test1110346Sbb.PROFILE_TABLE_NAME);
        profileProxy.commitProfile();
        profileProxy.closeProfile();

        //acquire TCKResource interface and activityID
        TCKResourceTestInterface resource = utils().getResourceInterface();
        TCKActivityID activityID = resource.createActivity(getClass().getName());

        //Set Sbb CMP field to 'Business' mode
        futureResult = new FutureAssertionsResult(new int[]{1110346}, getLog());
        resource.fireEvent(TCKResourceEventX.X1, new Integer(Test1110346Sbb.SET_BUSINESS), activityID, null);
        futureResult.waitForResult(utils().getTestTimeout());

        //Sbb calls business method
        getLog().fine("Cause Sbb to call a business method...");
        futureResult = new FutureAssertionsResult(new int[]{1110346}, getLog());
        resource.fireEvent(TCKResourceEventX.X1, new Integer(Test1110346Sbb.BUSINESS), activityID, null);
        res = futureResult.waitForResult(utils().getTestTimeout());
        if (!res.isPassed())
            return res;


        //Set Sbb CMP field to 'BUSINESS_ROLLBACK' mode
        futureResult = new FutureAssertionsResult(new int[]{1110346}, getLog());
        resource.fireEvent(TCKResourceEventX.X1, new Integer(Test1110346Sbb.SET_BUSINESS_ROLLBACK), activityID, null);
        futureResult.waitForResult(utils().getTestTimeout());

        //Sbb calls checkRollback business method
        getLog().fine("Cause Sbb to mark a TXN context for rollback and call a business method to check...");
        futureResult = new FutureAssertionsResult(new int[]{1110346}, getLog());
        resource.fireEvent(TCKResourceEventX.X1, new Integer(Test1110346Sbb.BUSINESS_ROLLBACK), activityID, null);


        res = futureResult.waitForResult(utils().getTestTimeout());
        if (!res.isPassed())
            return res;


        if (exResult!=null)
            return exResult;

        return TCKTestResult.passed();
    }

    public void setPassed(int assertionID, String msg) {
        getLog().fine(assertionID+": "+msg);
        if (futureResult!=null && !futureResult.isSet())
            futureResult.setPassed(assertionID);
    }

    public void setFailed(int assertionID, String msg, Exception e) {
        if (e==null) {
            getLog().fine("FAILURE: "+assertionID+":"+msg);
            if (futureResult!=null && !futureResult.isSet())
                futureResult.setFailed(assertionID, msg);

            //in case the currently active futureResult has already been set to passed() before the failure
            //comes in, we have to provide a mechanism to still detect this failure
            setIfEmpty(TCKTestResult.failed(assertionID, msg));
        }
        else {
            getLog().fine("FAILURE: "+assertionID+":"+msg);
            getLog().fine(e);
            if (futureResult!=null && !futureResult.isSet())
                futureResult.setFailed(new TCKTestFailureException(assertionID, msg, e));

            //in case the currently active futureResult has already been set to passed() before the failure
            //comes in, we have to provide a mechanism to still detect this failure
            setIfEmpty(TCKTestResult.failed(new TCKTestFailureException(assertionID, msg, e)));
        }
    }

    public void setError(String msg, Exception e) {
        if (e==null) {
            getLog().warning(msg);
            if (futureResult!=null && !futureResult.isSet())
                futureResult.setError(msg);

            //in case the currently active futureResult has already been set to passed() before the error
            //comes in, we have to provide a mechanism to still detect this error
            setIfEmpty(TCKTestResult.error(msg));
        }
        else {
            getLog().warning(msg);
            getLog().warning(e);
            if (futureResult!=null && !futureResult.isSet())
                futureResult.setError(msg, e);

            //in case the currently active futureResult has already been set to passed() before the error
            //comes in, we have to provide a mechanism to still detect this failure
            setIfEmpty(TCKTestResult.error(msg, e));
        }
    }


    public void setUp() throws Exception {

        setupService(SBB_EVENT_DU_PATH);

        setupService(DU_PATH_PARAM);

        in = utils().getRMIObjectChannel();
        in.setMessageHandler(new BaseMessageAdapter(getLog()) {
            public void onSetPassed(int assertionID, String msg) { setPassed(assertionID, msg); }
            public void onSetFailed(int assertionID, String msg, Exception e) { setFailed(assertionID, msg, e); }
            public void onLogCall(String msg) { getLog().fine(msg); }
            public void onSetError(String msg, Exception e) { setError(msg, e); }

        });

        profileUtils = new ProfileUtils(utils());
        profileProvisioning = profileUtils.getProfileProvisioningProxy();
    }

    public void tearDown() throws Exception {

        in.clearQueue();

        try {
            profileUtils.removeProfileTable(Test1110346Sbb.PROFILE_TABLE_NAME);
        }
        catch (Exception e) {
            getLog().warning("Caught exception while trying to remove profile table:");
            getLog().warning(e);
        }

        super.tearDown();
    }

    private void setIfEmpty(TCKTestResult exResult) {
        if (this.exResult==null)
            this.exResult = exResult;
    }

    private ProfileUtils profileUtils;
    private ProfileProvisioningMBeanProxy profileProvisioning;

    private RMIObjectChannel in;
    private TCKTestResult exResult;
    private FutureAssertionsResult futureResult;

}
