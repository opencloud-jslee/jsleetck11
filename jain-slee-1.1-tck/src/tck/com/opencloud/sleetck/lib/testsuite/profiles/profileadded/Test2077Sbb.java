/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.profiles.profileadded;

import com.opencloud.sleetck.lib.sbbutils.BaseTCKSbb;
import com.opencloud.sleetck.lib.sbbutils.TCKSbbUtils;

import javax.slee.ActivityContextInterface;
import javax.slee.Address;
import javax.slee.profile.*;
import java.util.HashMap;

public abstract class Test2077Sbb extends BaseTCKSbb {

    public void onProfileAddedEvent(ProfileAddedEvent event, ActivityContextInterface aci) {
        try {
            HashMap map = new HashMap();
            map.put("Type", "updateProfile");
            setResult(map);
            fireSendResultEvent(new SendResultEvent(), aci, null);
            return;
        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }
    }

    public void onProfileUpdatedEvent(ProfileUpdatedEvent event, ActivityContextInterface aci) {

        try {
            HashMap map = new HashMap();
            Test2077ProfileCMP beforeUpdateProfile;
            Test2077ProfileCMP afterUpdateProfile;

            try {
                beforeUpdateProfile = (Test2077ProfileCMP) event.getBeforeUpdateProfile();
            } catch (ClassCastException e) {
                map.put("Result", new Boolean(false));
                map.put("ID", new Integer(2078));
                map.put("Message", "The Profile CMP interface returned by ProfileUpdatedEvent.getBeforeUpdateProfile() was not of type Test2077ProfileCMP");
                setResult(map);
                fireSendResultEvent(new SendResultEvent(), aci, null);
                return;
            }

            if (beforeUpdateProfile.getValue() != 42) {
                map.put("Result", new Boolean(false));
                map.put("ID", new Integer(2077));
                map.put("Message", "The 'value' field in the Test2077ProfileCMP was not correct.");
                setResult(map);
                fireSendResultEvent(new SendResultEvent(), aci, null);
                return;
            }

            try {
                afterUpdateProfile = (Test2077ProfileCMP) event.getAfterUpdateProfile();
            } catch (ClassCastException e) {
                map.put("Result", new Boolean(false));
                map.put("ID", new Integer(2078));
                map.put("Message", "The Profile CMP interface returned by ProfileUpdatedEvent.getAfterUpdateProfile() was not of type Test2077ProfileCMP");
                setResult(map);
                fireSendResultEvent(new SendResultEvent(), aci, null);
                return;
            }

            if (afterUpdateProfile.getValue() != 43) {
                map.put("Result", new Boolean(false));
                map.put("ID", new Integer(2077));
                map.put("Message", "The 'value' field in the Test2077ProfileCMP was not correct.");
                setResult(map);
                fireSendResultEvent(new SendResultEvent(), aci, null);
                return;
            }

            map.put("Result", new Boolean(true));
            setResult(map);
            fireSendResultEvent(new SendResultEvent(), aci, null);
            return;

        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }

    }

    public void onSendResultEvent(SendResultEvent event, ActivityContextInterface aci) {

        try {
            TCKSbbUtils.getResourceInterface().sendSbbMessage(getResult());

        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }
    }

    public abstract void fireSendResultEvent(SendResultEvent event, ActivityContextInterface aci, Address address);

    public abstract void setResult(HashMap map);
    public abstract HashMap getResult();

    public abstract Test2077ProfileCMP getProfileCMP(ProfileID profileID) throws UnrecognizedProfileTableNameException, UnrecognizedProfileNameException;

}
