/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.profiles.profileadded;

import com.opencloud.sleetck.lib.TCKTestErrorException;
import com.opencloud.sleetck.lib.TCKTestFailureException;
import com.opencloud.sleetck.lib.resource.events.TCKResourceEventX;
import com.opencloud.sleetck.lib.sbbutils.BaseTCKSbb;
import com.opencloud.sleetck.lib.sbbutils.TCKSbbUtils;

import javax.slee.profile.*;
import javax.slee.*;

import java.util.HashMap;

public abstract class Test2073Sbb extends BaseTCKSbb {

    public void onProfileAddedEvent(ProfileAddedEvent event, ActivityContextInterface aci) {
        try {
            HashMap map = new HashMap();
            map.put("Type", "RemoveProfile");
            setResult(map);
            fireSendResultEvent(new SendResultEvent(), aci, null);
            return;
        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }
    }

    public void onProfileRemovedEvent(ProfileRemovedEvent event, ActivityContextInterface aci) {

        try {
            HashMap map = new HashMap();
            Test2073ProfileCMP removedProfile;


            try {
                removedProfile = (Test2073ProfileCMP) event.getRemovedProfile();
            } catch (ClassCastException e) {
                map.put("Result", new Boolean(false));
                map.put("ID", new Integer(2074));
                map.put("Message", "The Profile CMP interface returned by ProfileAddedEvent.getAddedProfile() was not of type Test2073ProfileCMP");
                setResult(map);
                fireSendResultEvent(new SendResultEvent(), aci, null);
                return;
            }

            if (removedProfile.getValue() != 42) {
                map.put("Result", new Boolean(false));
                map.put("ID", new Integer(2073));
                map.put("Message", "The 'value' field in the Test2073ProfileCMP was not correct.");
                setResult(map);
                fireSendResultEvent(new SendResultEvent(), aci, null);
                return;
            }

            map.put("Result", new Boolean(true));
            setResult(map);
            fireSendResultEvent(new SendResultEvent(), aci, null);
            return;

        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }

    }

    public void onSendResultEvent(SendResultEvent event, ActivityContextInterface aci) {

        try {
            TCKSbbUtils.getResourceInterface().sendSbbMessage(getResult());

        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }
    }

    public abstract void fireSendResultEvent(SendResultEvent event, ActivityContextInterface aci, Address address);

    public abstract void setResult(HashMap map);
    public abstract HashMap getResult();

    public abstract Test2073ProfileCMP getProfileCMP(ProfileID profileID) throws UnrecognizedProfileTableNameException, UnrecognizedProfileNameException;

}
