/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.resource.context;

import java.util.HashMap;

import javax.slee.ServiceID;

import com.opencloud.sleetck.lib.TCKTestErrorException;
import com.opencloud.sleetck.lib.TCKTestFailureException;
import com.opencloud.sleetck.lib.TCKTestResult;
import com.opencloud.sleetck.lib.infra.SleeTCKComponentConstants;
import com.opencloud.sleetck.lib.testsuite.resource.BaseResourceTest;
import com.opencloud.sleetck.lib.testsuite.resource.RAMethods;

/**
 * Test to ensure that getInvokingService() is behaving correctly.
 * <p>
 * Test assertion ID: 1115205, 1115206
 */
public class Test1115205Test extends BaseResourceTest {

    public TCKTestResult run() throws Exception {
        // Test to see that getInvokingService() returns null when called from a
        // non-service thread.
        HashMap resultmap = sendMessage(RAMethods.getInvokingService, new Integer(1115206));
        if (resultmap == null)
            throw new TCKTestFailureException(1115206, "Test timed out while waiting for response to getInvokingService()");
        Object result = resultmap.get("result");
        if (result instanceof Exception)
            throw new TCKTestFailureException(1115206, "getInvokingService() threw an Exception when called from a non-service thread: " + result);
        if (!Boolean.TRUE.equals(result))
            throw new TCKTestFailureException(1115206, "getInvokingService() returned a non-null value when called from a non-service thread: " + result);

        // Check for expected ServiceID return result.
        resultmap = sendMessage(RAMethods.getInvokingService, new Integer(1115205));
        if (resultmap == null)
            throw new TCKTestFailureException(1115205, "Test timed out while waiting for response to getInvokingService()");
        result = resultmap.get("result2");
        if (result instanceof Exception)
            throw new TCKTestFailureException(1115205,
                    "Exception thrown while attempting to test getInvokingService() via an event fired from the ContextResourceAdaptor: " + result);
        if (result == null)
            throw new TCKTestFailureException(1115205, "getInvokingService() returned a null value when called from a service thread");

        ServiceID testID = new ServiceID("Test1115205Service", SleeTCKComponentConstants.TCK_VENDOR, "1.1");
        if (!testID.equals(result))
            throw new TCKTestFailureException(1115205, "getInvokingService() returned an incorrect ServiceID when invoked from a service thread: " + result
                    + " - expected '" + testID + "'");
        if (Boolean.TRUE.equals(result))
            throw new TCKTestErrorException("Unexpected result received while invoking getInvokingService(): " + result);

        return TCKTestResult.passed();
    }
}