/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.javax.slee.EventContext;

import java.util.HashMap;

import javax.slee.ActivityContextInterface;
import javax.slee.EventContext;
import javax.slee.InitialEventSelector;
import javax.slee.facilities.Tracer;

import com.opencloud.sleetck.lib.resource.events.TCKResourceEventX;
import com.opencloud.sleetck.lib.sbbutils.BaseTCKSbb;
import com.opencloud.sleetck.lib.sbbutils.TCKSbbUtils;
/**
 * AssertionID(1108126): Test An EventContext object may be stored in CMP 
 * field of an SBB if the type of the CMP field is this interface.
 */
public abstract class Test1108126Sbb extends BaseTCKSbb {
    public static final String TRACE_MESSAGE_TCKResourceEventX1 = "Test1108126Sbb:I got TCKResourceEventX1 on ActivityA";
    public static final String TRACE_MESSAGE_TCKResourceEventX2 = "Test1108126Sbb:I got TCKResourceEventX2 on ActivityB";
    public static final String TRACE_MESSAGE_Test1108126Event = "Test1108126Sbb:I got Test1108126Event";
    public static int waitPeriodMs = 10000;
    
    
    //  return a static string so that this SBB entity gets selected every time
    public InitialEventSelector initialEventSelector(InitialEventSelector ies) {
        ies.setCustomName("test");
        return ies;
    }
    
    // Sbb1 receives TCKResourceEventX1 on ActivityA
    public void onTCKResourceEventX1(TCKResourceEventX event, ActivityContextInterface aci, EventContext context) {
        try {
            //send message to TCK: I got TCKResourceEventX1 on ActivityA
            tracer = getSbbContext().getTracer("com.test");
            tracer.info(TRACE_MESSAGE_TCKResourceEventX1);
            
            //store context in CMP field
            setEventContext(context);
            
            //call suspendDelivery() method now, we assumed the default timeout 
            //on the SLEE will be more than 10000ms to complete this test.
            context.suspendDelivery();
            
        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }
    }
    
    // Sbb1 receives TCKResourceEventX2 on ActivityB
    public void onTCKResourceEventX2(TCKResourceEventX event, ActivityContextInterface aci, EventContext context) {
        try {
            //send message to TCK: I got TCKResourceEventX2 on ActivityB
            tracer = getSbbContext().getTracer("com.test");
            tracer.info(TRACE_MESSAGE_TCKResourceEventX2);
            //get EventContext ec from CMP field
            EventContext ec = getEventContext();

            //assert ec.isSuspended(), check ec has been suspended or not 
            if (!ec.isSuspended()) {
                sendResultToTCK("Test1108126Test", false, "ERROR: The event delivery hasn't been suspended, the " +
                                "EventContext.isSuspended() returned false!", 1108126);
                return;
            }
            
            if (ec == null) {
                sendResultToTCK("Test1108126Test", false, "ERROR: The EventContext object which was stored in CMP" +
                                "field of an SBB did not match the original EventContext object " +
                                "was given via the first event handler method in the different transactions.", 1108126);
            }
            else { 
                sendResultToTCK("Test1108126Test", true, "An EventContext object can be stored in CMP field of " +
                        "an SBB if the type of the CMP field is this interface.", 1108126);
            }
            
            try {
                if (ec.isSuspended())  
                    ec.resumeDelivery();
            } catch (Exception e) {
                tracer.severe("Failed to resume the suspended event delivery.");
            }
                
        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }
    }
    
    private void sendResultToTCK(String testName, boolean result, String message, int failedAssertionID) throws Exception {
        HashMap sbbData = new HashMap();
        sbbData.put("testname", testName);
        sbbData.put("result", result ? "pass" : "fail");
        sbbData.put("message", message);
        sbbData.put("id", new Integer(failedAssertionID));
        TCKSbbUtils.getResourceInterface().sendSbbMessage(sbbData);
    }
    
    // receivedEndEvent flag
    public abstract void setEventContext(EventContext eventContext);
    public abstract EventContext getEventContext();

    private Tracer tracer = null;
}
