/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.management.sleestate;

import com.opencloud.sleetck.lib.AbstractSleeTCKTest;
import com.opencloud.sleetck.lib.TCKTestResult;
import com.opencloud.sleetck.lib.resource.TCKActivityID;
import com.opencloud.sleetck.lib.resource.events.TCKResourceEventX;
import com.opencloud.sleetck.lib.testutils.BaseTCKResourceListener;
import com.opencloud.sleetck.lib.testutils.jmx.SleeManagementMBeanProxy;

import javax.slee.management.SleeState;
import javax.slee.management.SleeStateChangeNotification;
import java.rmi.RemoteException;

/**
 * Test assertion 1882, that the SLEE will not create an Sbb entity while it is in the starting state.  Try to get the
 * SLEE to create an Sbb entity while in the starting state by restarting the SLEE and firing an initial event after
 * invoking SLEE start.  Use a callback from the Sbb's onCreate() to determine the SLEE state at the time of the Sbb
 * entities creation.
 */
public class Test1882Test extends AbstractSleeTCKTest {
    public TCKTestResult run() throws Exception {
        TCKActivityID activity = utils().getResourceInterface().createActivity("Test1882AActivity");

        // Stop the SLEE
        getLog().info("Stopping SLEE...");
        management.stop();
        SleeStateChangeNotification noti = stateListener.nextNotification();
        long timeout = System.currentTimeMillis() + utils().getTestTimeout();
        while (noti.getNewState() != SleeState.STOPPED) {
            if (timeout < System.currentTimeMillis())
                return TCKTestResult.error("Timeout waiting for the SLEE to stop");

            noti = stateListener.nextNotification();
        }

        getLog().info("SLEE State: " + noti.getNewState());
        getLog().info("Starting SLEE...");
        // Restart the SLEE
        management.start();

        try {
            utils().getResourceInterface().fireEvent(TCKResourceEventX.X1, null, activity, null);
        } catch (Exception e) {
            // If an exception is received while firing an event on a SLEE in the starting state the test passes
            return TCKTestResult.passed();
        }

        // Wait for the SLEE to enter running state and process the initial event
        synchronized (testLock) {
            testLock.wait(utils().getTestTimeout());
        }

        if (null == testResult) {
            if (management.getState() == SleeState.RUNNING)
                setTestResult(TCKTestResult.error("Expected SBB entity not created in response to an initial event once " +
                                                  "the SLEE reached the RUNNING state"));
            else
                setTestResult(TCKTestResult.error("Error restarting the SLEE, the SLEE should reach the RUNNING state " +
                                                  "after restart"));
        }

        return testResult;
    }

    public void setUp() throws Exception {
        super.setUp();
        testResult = null;
        stateListener = new QueuingSleeStateListener(utils());
        management = utils().getSleeManagementMBeanProxy();
        management.addNotificationListener(stateListener, null, null);
        setResourceListener(new ResourceListenerImpl());
    }

    public void tearDown() throws Exception {
        if (management != null)
            management.removeNotificationListener(stateListener);
        super.tearDown();
    }

    public class ResourceListenerImpl extends BaseTCKResourceListener {
        public Object onSbbCall(Object args) {
            try {
                SleeState state = management.getState();
                // Check the SLEE state
                if (state == SleeState.STARTING) {
                    setTestResult(TCKTestResult.failed(1882, "An event was delivered to an SBB entitiy while the SLEE " +
                                                             "was in the STARTING state"));
                } else if (state == SleeState.RUNNING) {
                    setTestResult(TCKTestResult.passed());
                }
            } catch (Exception e) {
                setTestResult(TCKTestResult.error(e));
            }
            synchronized (testLock) {
                testLock.notifyAll();
            }
            return null;
        }

        public void onException(Exception exception) throws RemoteException {
            utils().getLog().warning("An Exception was received from the SBB or the TCK resource:");
            utils().getLog().warning(exception);
        }
    }

    private void setTestResult(TCKTestResult testResult) {
        this.testResult = testResult;
    }

    TCKTestResult testResult;
    Object testLock = new Object();
    SleeManagementMBeanProxy management;
    QueuingSleeStateListener stateListener;
}
