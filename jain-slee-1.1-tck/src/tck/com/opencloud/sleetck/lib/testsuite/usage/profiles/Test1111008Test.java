/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.usage.profiles;

import java.rmi.RemoteException;
import java.util.HashMap;

import javax.management.Notification;
import javax.management.NotificationListener;
import javax.management.ObjectName;
import javax.slee.ComponentID;
import javax.slee.management.DeployableUnitDescriptor;
import javax.slee.management.TraceNotification;
import javax.slee.profile.ProfileSpecificationID;
import javax.slee.usage.SampleStatistics;

import com.opencloud.logging.Logable;
import com.opencloud.sleetck.lib.TCKTestResult;
import com.opencloud.sleetck.lib.resource.TCKActivityID;
import com.opencloud.sleetck.lib.resource.TCKSbbMessage;
import com.opencloud.sleetck.lib.resource.events.TCKResourceEventX;
import com.opencloud.sleetck.lib.resource.testapi.TCKResourceTestInterface;
import com.opencloud.sleetck.lib.sbbutils.events2.SbbBaseMessageConstants;
import com.opencloud.sleetck.lib.testsuite.usage.common.UsageMBeanProxy;
import com.opencloud.sleetck.lib.testsuite.usage.common.UsageMBeanProxyImpl;
import com.opencloud.sleetck.lib.testutils.BaseTCKResourceListener;
import com.opencloud.sleetck.lib.testutils.FutureResult;
import com.opencloud.sleetck.lib.testutils.ProfileUtils;
import com.opencloud.sleetck.lib.testutils.jmx.DeploymentMBeanProxy;
import com.opencloud.sleetck.lib.testutils.jmx.ProfileMBeanProxy;
import com.opencloud.sleetck.lib.testutils.jmx.ProfileTableUsageMBeanProxy;
import com.opencloud.sleetck.lib.testutils.jmx.SleeManagementMBeanProxy;
import com.opencloud.sleetck.lib.testutils.jmx.TraceMBeanProxy;
import com.opencloud.sleetck.lib.testutils.jmx.impl.TraceMBeanProxyImpl;

public class Test1111008Test extends BaseProfileTest {

    public static final String TCK_SBB_EVENT_DU_PATH_PARAM = "eventDUPath";

    private static final String SERVICE_DU_PATH_PARAM = "DUPath";
    public static final String PROFILE_TABLE_NAME = "Test1111008ProfileTable";
    public static final String PROFILE_NAME = "Test1111008Profile";
    private static final int TEST_ID = 1111008;

    /**
     * Perform the actual test.
     */

    public TCKTestResult run() throws Exception {
        Logable log =  utils().getLog();
        result = new FutureResult(log);

        // Create the profile table.
        DeploymentMBeanProxy duProxy = utils().getDeploymentMBeanProxy();
        DeployableUnitDescriptor duDesc = duProxy.getDescriptor(duID);
        ComponentID components[] = duDesc.getComponents();

        // Create profile tables.
        for (int i = 0; i < components.length; i++) {
            if (components[i] instanceof ProfileSpecificationID) {
                ProfileSpecificationID profSpecID = (ProfileSpecificationID) components[i];
                profileProxy.createProfileTable(profSpecID, PROFILE_TABLE_NAME);
                log.fine("Created profile table "+PROFILE_TABLE_NAME+ " for profile specification "+profSpecID.getName()+" "+profSpecID.getVersion()+", "+profSpecID.getVendor());
            }
        }

        createNamedParameterSet(PROFILE_TABLE_NAME);

        // Create a profile, set some values, then commit it.
        ObjectName profile = profileProxy.createProfile(PROFILE_TABLE_NAME, PROFILE_NAME);
        ProfileMBeanProxy proxy = utils().getMBeanProxyFactory().createProfileMBeanProxy(profile);
        log.fine("Created profile "+PROFILE_NAME+" for profile table "+PROFILE_TABLE_NAME);
        proxy.commitProfile();
        proxy.closeProfile();
        log.fine("Committed and closed profile.");

        // Request that the named usage parameter set gets updated.
        TCKResourceTestInterface resource = utils().getResourceInterface();
        TCKActivityID activityID = resource.createActivity(getClass().getName());
        getLog().fine("About to fire named usage parameter update request to SBB");
        resource.fireEvent(TCKResourceEventX.X1, null, activityID, null);

        // Now wait for the result to come back.
        getLog().fine("Waiting for result");
        return result.waitForResult(5000);

    }

    /**
     * Do all the pre-run configuration of the test.
     */

    public void setUp() throws Exception {

        getLog().fine("Installing TCKSbbEvent deployable unit");
        String eventDUPath = utils().getTestParams().getProperty(TCK_SBB_EVENT_DU_PATH_PARAM);
        utils().install(eventDUPath);

        utils().getLog().fine("Installing and activating service");
        // Install the Deployable Units
        String duPath = utils().getTestParams().getProperty(SERVICE_DU_PATH_PARAM);
        duID = utils().install(duPath);

        // Start the DU's service.
        utils().activateServices(duID, true);

        profileProxy = new ProfileUtils(utils()).getProfileProvisioningProxy();

        utils().getLog().fine("Connecting to resource");
        resourceListener = new TCKResourceListenerImpl();
        utils().getResourceInterface().setResourceListener(resourceListener);

    }

    /**
     * Clean up after the test.
     */

    public void tearDown() throws Exception {

        try {
            profileProxy.removeProfile(PROFILE_TABLE_NAME, PROFILE_NAME);
        } catch (Exception e) {
        }

        try {
            profileProxy.removeProfileTable(PROFILE_TABLE_NAME);
        } catch (Exception e) {
        }

        utils().getLog().fine("Disconnecting from resource");
        utils().getResourceInterface().clearActivities();
        utils().getResourceInterface().removeResourceListener();
        utils().getLog().fine("Deactivating and uninstalling service");
        utils().deactivateAllServices();
        utils().uninstallAll();
    }

    //private resource listener implementation
    private class TCKResourceListenerImpl extends BaseTCKResourceListener {

        public synchronized void onSbbMessage(TCKSbbMessage message, TCKActivityID activity) throws RemoteException {
            utils().getLog().fine("Received message from SBB ");

            HashMap map = (HashMap) message.getMessage();
            Boolean passed = (Boolean) map.get(SbbBaseMessageConstants.RESULT);

            try {
                // check that the named param can be read
                ObjectName profileTableUsageMBeanName = profileProxy.getProfileTableUsageMBean(PROFILE_TABLE_NAME);
                ProfileTableUsageMBeanProxy p = utils().getMBeanProxyFactory().createProfileTableUsageMBeanProxy(profileTableUsageMBeanName);

                ObjectName usageMBeanName = p.getUsageMBean(p.getUsageParameterSets()[0]); // can look up name in p.
                UsageMBeanProxy u = new UsageMBeanProxyImpl(usageMBeanName, utils().getMBeanFacade());
                long count = u.getFirstCount(false);

            } catch (Exception e) {
                utils().getLog().error(e);
                result.setError(e);
                return;
            }

            if (passed.booleanValue()) {
                utils().getLog().fine((String)map.get(SbbBaseMessageConstants.MSG));
                result.setPassed();
            } else {
                result.setFailed(TEST_ID, (String)map.get(SbbBaseMessageConstants.MSG));
            }
        }

        public void onException(Exception e) throws RemoteException {
            utils().getLog().warning("Received exception from SBB.");
            utils().getLog().warning(e);
            result.setError(e);
        }

    }


    private TCKResourceListenerImpl resourceListener;
    private FutureResult result;
}
