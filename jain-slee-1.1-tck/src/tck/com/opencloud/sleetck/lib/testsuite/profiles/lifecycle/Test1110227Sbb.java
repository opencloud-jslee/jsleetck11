/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.profiles.lifecycle;

import javax.naming.Context;
import javax.slee.ActivityContextInterface;
import javax.slee.SbbContext;
import javax.slee.profile.ProfileFacility;
import javax.slee.profile.ProfileTable;

import com.opencloud.logging.StdErrLog;
import com.opencloud.sleetck.lib.profileutils.BaseMessageSender;
import com.opencloud.sleetck.lib.rautils.RMIObjectChannel;
import com.opencloud.sleetck.lib.rautils.TCKRAUtils;
import com.opencloud.sleetck.lib.resource.events.TCKResourceEventX;
import com.opencloud.sleetck.lib.sbbutils.BaseTCKSbb;
import com.opencloud.sleetck.lib.sbbutils.TCKSbbUtils;

public abstract class Test1110227Sbb extends BaseTCKSbb {

    public static final String PROFILE_TABLE_NAME = "Test1110227ProfileTable";
    public static final String PROFILE_NAME = "Test1110227Profile";
    public static final String PROFILE_NAME2 = "Test1110227Profile2";
    public static final String PROFILE_NAME3 = "Test1110227Profile3";

    public static final String INIT = "Init";
    public static final String ROLLBACK = "Rollback";
    public static final String MODIFIED = "Modified";


    //Sbb lifecycle method. Acquire RMIObjectChannel output stream
    public void setSbbContext(SbbContext context) {
        super.setSbbContext(context);

        try {
            out = TCKRAUtils.lookupRMIObjectChannel();
            msgSender = new BaseMessageSender(out, new StdErrLog());
        }
        catch (Exception e) {
            context.getTracer(getSbbID().getName()).fine("An error occured creating an RMIObjectChannel:", e);
        }

    }

    public void onTCKResourceEventX1(TCKResourceEventX event, ActivityContextInterface aci) {
        try {
            Context env = TCKSbbUtils.getSbbEnvironment();
            ProfileFacility profileFacility = (ProfileFacility)env.lookup("slee/facilities/profile");
            msgSender.sendLogMsg("Obtained profileFacility object.");
            ProfileTable profileTable = profileFacility.getProfileTable(PROFILE_TABLE_NAME);
            msgSender.sendLogMsg("Obtained profileTable object.");

            //1110281: profileVerify is not invoked for any transaction which is originated by the
            //SLEE during event delivery or as a consequence of Profile Local method invocations
            //by any SLEE component (e.g. SBB or Resource Adaptor).
            //try to somehow animate the SLEE to call profileVerify (which would be a violation of assertion 1110281)
            //in case the SLEE would call profileVerify the current TXN would be marked for rollback,
            //thus a failure would be sent via sbbRolledBack.
            msgSender.sendLogMsg("Set CMP value so that profileVerify would cause rollback if it was called by the SLEE.");
            Test1110227ProfileLocal profileLocal = (Test1110227ProfileLocal) profileTable.find(PROFILE_NAME);
            profileLocal.setStringValue(ROLLBACK);
            profileLocal.business();

            Test1110227ProfileLocal profileLocal2 = (Test1110227ProfileLocal) profileTable.create(PROFILE_NAME2);
            profileLocal2.setStringValue(ROLLBACK);
            profileLocal2.business();

            if (getSbbContext().getRollbackOnly())
                msgSender.sendFailure(Test1110227Profile.profileVerifyOnlyCalledForManagement, "Call to profileVerify occured but should not have happened.");
            else
                msgSender.sendSuccess(Test1110227Profile.profileVerifyOnlyCalledForManagement, "As expected call to profileVerify did not occur.");

        } catch(Exception e) {
            msgSender.sendException(e);
        }
    }

    private RMIObjectChannel out;
    private BaseMessageSender msgSender;
}
