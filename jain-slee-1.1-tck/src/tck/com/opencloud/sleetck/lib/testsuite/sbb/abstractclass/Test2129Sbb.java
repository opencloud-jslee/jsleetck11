/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.sbb.abstractclass;

import com.opencloud.sleetck.lib.resource.events.TCKResourceEventX;
import com.opencloud.sleetck.lib.sbbutils.BaseTCKSbb;
import com.opencloud.sleetck.lib.sbbutils.TCKSbbUtils;

import javax.slee.ActivityContextInterface;
import javax.slee.facilities.Level;
import java.util.HashMap;

public abstract class Test2129Sbb extends BaseTCKSbb {

    public void sbbCreate() throws javax.slee.CreateException {
        createTraceSafe(Level.INFO,"Throwing RuntimeException from sbbCreate() to trigger call to sbbExceptionThrown()");
        throw new RuntimeException("Test2129Exception");
    }

    public void sbbExceptionThrown(Exception ex, Object ev, ActivityContextInterface aci) {

        try {
            HashMap map = new HashMap();

            // Verify that this SBB is in the pooled state.

            // If getSbbContext() returns null we're in the Does Not Exist state
            if (getSbbContext() == null) {
                map.put("Result", new Boolean(false));
                map.put("Message", "SBB in the Pooled state when RuntimeException was thrown (from sbbCreate()) was not " +
                        "in the Pooled state when sbbExceptionThrown was called. getSbbContext() returned null, implying " +
                        "that the SBB is in the Does Not Exist state");
                TCKSbbUtils.getResourceInterface().sendSbbMessage(map);
                return;
            } else createTraceSafe(Level.INFO, "In sbbExceptionThrown(), getSbbContext() did not return null. We are "+
                    "not in the Does Not Exist state");

            try {
                getSbbContext().getActivities();
            } catch (IllegalStateException e) {
                createTraceSafe(Level.INFO, "SbbContext.getActivities() threw the expected IllegalStateException, " +
                        "implying that we're not in the Ready state. " +
                        "(the IllegalStateException indicates that this SBB is not assigned to an SBB entity). " +
                        "We must therefore be in the Pooled state as expected");
                map.put("Result", new Boolean(true));
                map.put("Message", "Ok");
                TCKSbbUtils.getResourceInterface().sendSbbMessage(map);
                return;
            }

            map.put("Result", new Boolean(false));
            map.put("Message", "SBB in the Pooled state when RuntimeException was thrown (from sbbCreate()) was not in the " +
                    "Pooled state when sbbExceptionThrown was called. (SbbContext.getActivities() did not throw the expected " +
                    "IllegalStateException, implying that the SBB is assigned to an SBB entity, and is in the Ready state)");
            TCKSbbUtils.getResourceInterface().sendSbbMessage(map);
            return;
        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }

    }

    public void sbbRolledBack(javax.slee.RolledBackContext context) {
    }


    // Initial event
    public void onTCKResourceEventX1(TCKResourceEventX ev, ActivityContextInterface aci) {
    }

}
