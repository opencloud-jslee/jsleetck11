/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.javax.slee.SbbLocalObject.isIdentical;

import javax.slee.ActivityEndEvent;
import javax.slee.ActivityContextInterface;
import javax.slee.SbbContext;
import javax.slee.Address;
import javax.slee.SbbLocalObject;
import javax.slee.facilities.Level;
import javax.slee.ChildRelation;

import com.opencloud.sleetck.lib.sbbutils.BaseTCKSbb;
import com.opencloud.sleetck.lib.sbbutils.TCKSbbUtils;
import com.opencloud.sleetck.lib.sbbutils.TCKSbbConstants;
import com.opencloud.sleetck.lib.resource.events.TCKResourceEventX;
import com.opencloud.sleetck.lib.resource.TCKActivityID;
import com.opencloud.sleetck.lib.resource.sbbapi.TCKActivity;
import com.opencloud.sleetck.lib.resource.sbbapi.TCKResourceSbbInterface;
import com.opencloud.sleetck.lib.resource.sbbapi.TCKActivityContextInterfaceFactory;

import java.util.HashMap;

/**
 * SbbLocalObject.isIdentical(SbbLocalObject)
 */

public abstract class Test3297Sbb extends BaseTCKSbb {

    public void onTCKResourceEventX1(TCKResourceEventX event, ActivityContextInterface aci) {

        try {
            HashMap map = new HashMap();

            if (getSbbContext().getSbbLocalObject().isIdentical(getSbbContext().getSbbLocalObject()) == false) {

                map.put("Result", new Boolean(false));
                map.put("Message", "SbbLocalObject.isIdentical(SbbLocalObject) returned false instead of true.");
                TCKSbbUtils.getResourceInterface().sendSbbMessage(map);
                return;
            }

            // Create a Child, and verify that its SbbLocalObject is not identical to the parent's SbbLocalObject.
            SbbLocalObject childLocalObject = getChildRelation().create();
            if (getSbbContext().getSbbLocalObject().isIdentical(childLocalObject)) {
                map.put("Result", new Boolean(false));
                map.put("Message", "SbbLocalObject.identical(SbbLocalObject) returned true instead of false.");
                TCKSbbUtils.getResourceInterface().sendSbbMessage(map);
                return;
            }

            map.put("Result", new Boolean(true));
            map.put("Message", "Ok");
            TCKSbbUtils.getResourceInterface().sendSbbMessage(map);
            return;

        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }

    }

    public abstract ChildRelation getChildRelation();

}
