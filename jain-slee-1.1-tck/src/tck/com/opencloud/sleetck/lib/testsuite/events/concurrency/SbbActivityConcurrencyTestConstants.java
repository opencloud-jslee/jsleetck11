/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.events.concurrency;

/**
 * Defines constants used by the SbbActivityConcurrencyTest
 */
public interface SbbActivityConcurrencyTestConstants {

    // -- Codes used to indicate the nature of a sbb -> test call -- //

    public static final int SBB1_RECEIVED_X1         = 1;
    public static final int SBB1_RECEIVED_X2         = 2;
    public static final int SBB1_RECEIVED_Y1         = 3;
    public static final int SBB2_RECEIVED_Y2         = 4;

    public static final int SBB1_RECEIVED_X2_ROLL_BACK = 5;
    public static final int SBB2_RECEIVED_Y2_ROLL_BACK = 6;

}