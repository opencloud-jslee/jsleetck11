/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.profiles.lifecycle;

import javax.slee.CreateException;
import javax.slee.profile.Profile;
import javax.slee.profile.ProfileContext;
import javax.slee.profile.ProfileVerificationException;

import com.opencloud.logging.Logable;
import com.opencloud.logging.StdErrLog;
import com.opencloud.sleetck.lib.profileutils.BaseMessageSender;
import com.opencloud.sleetck.lib.rautils.RMIObjectChannel;
import com.opencloud.sleetck.lib.rautils.TCKRAUtils;

/**
 *
 */
public abstract class Test1110305Profile implements Test1110305ProfileCMP, Profile, Test1110305ProfileManagement  {

    public static final String THROW_EXCEPTION = "ThrowException";
    public static final String INIT = "Init";

    public Test1110305Profile() {
        Logable log = new StdErrLog();

        try {
            out = TCKRAUtils.lookupRMIObjectChannel();
            msgSender = new BaseMessageSender(out, log);
        }
        catch (Exception e) {
            log.error("An error occured creating an RMIObjectChannel:");
            log.error(e);
        }

        objectIsInvalid=false;

    }

    public void setProfileContext(ProfileContext context) {
        context.getTracer("setProfileContext").info("setProfileContext called");

        this.context = context;

        //1110306: after RuntimeException has been thrown no further methods are invoked on the same object
        if (objectIsInvalid)
            msgSender.sendFailure( 1110306, "'setProfileContext' was called on profile object after RuntimeException occurred.");

    }

    public void unsetProfileContext() {
        context.getTracer("unsetProfileContext").info("unsetProfileContext called");

        context = null;

        //1110306: after RuntimeException has been thrown no further methods are invoked on the same object
        if (objectIsInvalid)
            msgSender.sendFailure( 1110306, "'unsetProfileContext' was called on profile object after RuntimeException occurred.");

    }

    public void profileInitialize() {
        context.getTracer("profileInitialize").info("profileInitialize called");

        //1110306: after RuntimeException has been thrown no further methods are invoked on the same object
        if (objectIsInvalid)
            msgSender.sendFailure( 1110306, "'profileInitialize' was called on profile object after RuntimeException occurred.");

    }

    public void profilePostCreate() throws CreateException {
        context.getTracer("profilePostCreate").info("profilePostCreate called");

        //1110306: after RuntimeException has been thrown no further methods are invoked on the same object
        if (objectIsInvalid)
            msgSender.sendFailure( 1110306, "'profilePostCreate' was called on profile object after RuntimeException occurred.");

    }

    public void profileActivate() {
        context.getTracer("profileActivate").info("profileActivate called");

        //1110306: after RuntimeException has been thrown no further methods are invoked on the same object
        if (objectIsInvalid)
            msgSender.sendFailure( 1110306, "'profileActivate' was called on profile object after RuntimeException occurred.");

    }

    public void profilePassivate() {
        context.getTracer("profilePassivate").info("profilePassivate called");

        //1110306: after RuntimeException has been thrown no further methods are invoked on the same object
        if (objectIsInvalid)
            msgSender.sendFailure( 1110306, "'profilePassivate' was called on profile object after RuntimeException occurred.");

    }

    public void profileLoad() {
        context.getTracer("profileLoad").info("profileLoad called");

        //1110306: after RuntimeException has been thrown no further methods are invoked on the same object
        if (objectIsInvalid)
            msgSender.sendFailure( 1110306, "'profileLoad' was called on profile object after RuntimeException occurred.");

    }

    public void profileStore() {
        context.getTracer("profileStore").info("profileStore called");

        //1110306: after RuntimeException has been thrown no further methods are invoked on the same object
        if (objectIsInvalid)
            msgSender.sendFailure( 1110306, "'profileStore' was called on profile object after RuntimeException occurred.");

    }

    public void profileRemove() {
        context.getTracer("profileRemove").info("profileRemove called");

        //1110306: after RuntimeException has been thrown no further methods are invoked on the same object
        if (objectIsInvalid)
            msgSender.sendFailure( 1110306, "'profileRemove' was called on profile object after RuntimeException occurred.");

    }

    public void profileVerify() throws ProfileVerificationException {
        context.getTracer("profileVerify").info("profileVerify called");

        //1110306: after RuntimeException has been thrown no further methods are invoked on the same object
        if (objectIsInvalid)
            msgSender.sendFailure( 1110306, "'profileVerify' was called on profile object after RuntimeException occurred.");

        if (context.getProfileName().equals(Test1110305Sbb.LIFECYCLE_PROFILE) &&
                getValue().equals(THROW_EXCEPTION)){

            objectIsInvalid = true;
            throw new RuntimeException("Testexception.");
        }
    }

    public void business() {
        context.getTracer("business").fine("Business method called.");

        //1110306: after RuntimeException has been thrown no further methods are invoked on the same object
        if (objectIsInvalid)
            msgSender.sendFailure( 1110306, "'business' was called on profile object after RuntimeException occurred.");

        if (context.getProfileName().equals(Test1110305Sbb.BUSINESS_PROFILE) &&
                getValue().equals(THROW_EXCEPTION)){

            objectIsInvalid = true;
            throw new RuntimeException("Testexception.");
        }
    }

    public void manage() {
        context.getTracer("manage").fine("Management method called.");

        //1110306: after RuntimeException has been thrown no further methods are invoked on the same object
        if (objectIsInvalid)
            msgSender.sendFailure( 1110306, "'manage' was called on profile object after RuntimeException occurred.");

        if (context.getProfileName().equals(Test1110305Sbb.MANAGEMENT_PROFILE) &&
                getValue().equals(THROW_EXCEPTION)) {

            objectIsInvalid = true;
            throw new RuntimeException("Testexception.");
        }
    }

    private ProfileContext context;

    private RMIObjectChannel out;
    private BaseMessageSender msgSender;


    private boolean objectIsInvalid;

}
