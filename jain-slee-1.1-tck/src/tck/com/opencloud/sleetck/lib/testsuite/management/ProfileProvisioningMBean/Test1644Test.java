/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.management.ProfileProvisioningMBean;

import javax.slee.management.*;
import javax.slee.ComponentID;
import javax.slee.ServiceID;
import javax.slee.profile.ProfileSpecificationID;
import javax.slee.profile.UnrecognizedProfileNameException;
import javax.management.ObjectName;

import com.opencloud.sleetck.lib.*;
import com.opencloud.sleetck.lib.testutils.*;
import com.opencloud.sleetck.lib.resource.TCKSbbMessage;
import com.opencloud.sleetck.lib.resource.testapi.TCKResourceListener;
import com.opencloud.sleetck.lib.resource.TCKActivityID;
import com.opencloud.sleetck.lib.resource.testapi.TCKResourceTestInterface;
import com.opencloud.sleetck.lib.resource.events.TCKResourceEventX;
import com.opencloud.sleetck.lib.testutils.ProfileUtils;

import com.opencloud.sleetck.lib.testutils.jmx.DeploymentMBeanProxy;
import com.opencloud.sleetck.lib.testutils.jmx.ServiceManagementMBeanProxy;
import com.opencloud.sleetck.lib.testutils.jmx.ProfileProvisioningMBeanProxy;
import com.opencloud.sleetck.lib.testutils.jmx.ProfileMBeanProxy;

import com.opencloud.logging.Logable;

import java.rmi.RemoteException;
import java.util.HashMap;

public class Test1644Test implements SleeTCKTest {

    private static final String SERVICE_DU_PATH_PARAM = "DUPath";
    private static final int TEST_ID = 1644;

    public void init(SleeTCKTestUtils utils) {
        this.utils = utils;
    }

    /**
     * Perform the actual test.
     */

    public TCKTestResult run() throws Exception {

        ProfileProvisioningMBeanProxy profileProxy = profileUtils.getProfileProvisioningProxy();
        DeploymentMBeanProxy duProxy = utils.getDeploymentMBeanProxy();
        DeployableUnitDescriptor duDesc = duProxy.getDescriptor(duID);
        ComponentID components[] = duDesc.getComponents();

        for (int i = 0; i < components.length; i++) {
            if (components[i] instanceof ProfileSpecificationID) {
                ProfileSpecificationID profileSpecID = (ProfileSpecificationID) components[i];

                // Create the profile table.
                try {
                    profileProxy.createProfileTable(profileSpecID, "Test1644ProfileTable");
                } catch (Exception e) {
                    return TCKTestResult.error("Failed to create profile table.");
                }

                // Create a profile - 1644
                ObjectName jmxObjectName = null;
                try {
                    jmxObjectName = profileProxy.createProfile("Test1644ProfileTable", "Test1644Profile");
                } catch (Exception e) {
                    return TCKTestResult.failed(TEST_ID, "ProfileProvisioningMBean.createProfile(String, String) threw an Exception.");
                }

                if (jmxObjectName == null) {
                    return TCKTestResult.failed(TEST_ID, "ProfileProvisioningMBean.createProfile(String, String) didn't return a valid ObjectName object.");
                }

                // Commit the created profile - this makes it available to management clients and SBBs.
                ProfileMBeanProxy proxy = utils.getMBeanProxyFactory().createProfileMBeanProxy(jmxObjectName);
                proxy.commitProfile();


                // Try retrieving the profile - 1646
                // This getProfile() call doesn't cause any strangeness.
                try {
                    ObjectName profileObject = profileProxy.getProfile("Test1644ProfileTable", "Test1644Profile");
                    if (!profileObject.equals(jmxObjectName)) {
                        return TCKTestResult.failed(1646, "ProfileProvisioningMBean.getProfile(String, String) returned the wrong (or no) profile.");
                    }
                } catch (Exception e) {
                    return TCKTestResult.failed(1646, "ProfileProvisioningMBean.getProfile(String, String) returned the wrong (or no) profile.");
                }

                // Remove the profile - 1645
                try {
                    profileProxy.removeProfile("Test1644ProfileTable", "Test1644Profile");
                } catch (Exception e) {
                    return TCKTestResult.failed(1645, "ProfileProvisioningMBean.removeProfile(String, String) threw an Exception.");
                }

                // Verify that the removed profile no longer exists
                try {
                    profileProxy.getProfile("Test1644ProfileTable", "Test1644Profile");
                } catch (UnrecognizedProfileNameException e) {
                    return TCKTestResult.passed();
                }

                return TCKTestResult.failed(1661, "getProfile() should have thrown UnrecognizedProfileNameException, but didn't.");

            }
        }

        return TCKTestResult.error("No ProfileSpecification found.");
    }

    /**
     * Do all the pre-run configuration of the test.
     */

    public void setUp() throws Exception {

        utils.getLog().fine("Connecting to resource");
        resourceListener = new TCKResourceListenerImpl();
        utils.getResourceInterface().setResourceListener(resourceListener);
        utils.getLog().fine("Installing and activating service");

        profileUtils = new ProfileUtils(utils);

        // Install the Deployable Units
        String duPath = utils.getTestParams().getProperty(SERVICE_DU_PATH_PARAM);
        duID = utils.install(duPath);
        utils.activateServices(duID, true);

    }

    /**
     * Clean up after the test.
     */

    public void tearDown() throws Exception {

        ProfileProvisioningMBeanProxy profileProxy = profileUtils.getProfileProvisioningProxy();
        try {
            profileProxy.removeProfile("Test1644ProfileTable", "Test1644Profile");
        } catch (Exception e) {
        }
        try {
            profileProxy.removeProfileTable("Test1644ProfileTable");
        } catch (Exception e) {
        }
        utils.getLog().fine("Disconnecting from resource");
        utils.getResourceInterface().clearActivities();
        utils.getResourceInterface().removeResourceListener();
        utils.getLog().fine("Deactivating and uninstalling service");
        utils.deactivateAllServices();
        utils.uninstallAll();
    }

    private class TCKResourceListenerImpl extends BaseTCKResourceListener {
        public synchronized void onSbbMessage(TCKSbbMessage message, TCKActivityID calledActivity) throws RemoteException {
            utils.getLog().info("Received message from SBB");

            HashMap map = (HashMap) message.getMessage();
            Boolean passed = (Boolean) map.get("Result");
            String msgString = (String) map.get("Message");

            if (passed.booleanValue() == true)
                result.setPassed();
            else
                result.setFailed(TEST_ID, msgString);
        }

        public void onException(Exception e) throws RemoteException {
            utils.getLog().warning("Received exception from SBB");
            utils.getLog().warning(e);
            result.setError(e);
        }
    }

    private SleeTCKTestUtils utils;
    private TCKResourceListener resourceListener;
    private FutureResult result;
    private DeployableUnitID duID;
    private ProfileUtils profileUtils;
}
