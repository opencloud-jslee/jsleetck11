/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.events.convergencename.eventisunique;

import com.opencloud.sleetck.lib.sbbutils.BaseTCKSbb;
import com.opencloud.sleetck.lib.sbbutils.TCKSbbUtils;
import com.opencloud.sleetck.lib.resource.events.TCKResourceEventX;

import javax.slee.ActivityContextInterface;
import javax.slee.Address;
import javax.slee.CreateException;
import javax.slee.facilities.Level;
import javax.slee.nullactivity.NullActivity;
import javax.slee.nullactivity.NullActivityFactory;
import javax.slee.nullactivity.NullActivityContextInterfaceFactory;
import javax.naming.InitialContext;
import javax.naming.NamingException;


/**
 * SBB for testing assertion 1963.
 */
public abstract class Test1963Sbb extends BaseTCKSbb {

    /**
     * Callback to the test so the test can count the number of SBB entities created
     * @throws CreateException
     */
    public void sbbCreate() throws CreateException {
        callTest("SBB Created");
    }

    /**
     * Create an event and a new NullActivity and fire the event twice on the activity.  Because the SBB includes Event
     * in its convergence name calculation a new SBB entity should be created for each event (even though both events are
     * in fact the same object).
     * @param event
     * @param aci
     */
    public void onTCKResourceEventX1(TCKResourceEventX event, ActivityContextInterface aci) {
        createTrace("- onTCKResourceEventX1 -");
        Test1963Event newEvent = new Test1963Event(0);
        NullActivity activity = createNullActivity();
        ActivityContextInterface nullaci = getNullActivityContextInterface(activity);
        fireTest1963Event(newEvent, nullaci, null);
        fireTest1963Event(newEvent, nullaci, null);
        activity.endActivity();
    }

    public void onTest1963Event(Test1963Event event, ActivityContextInterface aci) {
        createTrace("- onTest1963Event -");
    }

    public abstract void fireTest1963Event(Test1963Event event, ActivityContextInterface aci, Address address);

    private NullActivity createNullActivity() {
        try {
            NullActivityFactory factory = (NullActivityFactory) new InitialContext().lookup("java:comp/env/slee/nullactivity/factory");
            return factory.createNullActivity();
        } catch (NamingException e) {
            TCKSbbUtils.handleException(e);
            return null;
        }
    }

    private ActivityContextInterface getNullActivityContextInterface(NullActivity nullActivity) {
        try {
            NullActivityContextInterfaceFactory factory = (NullActivityContextInterfaceFactory)
                    new InitialContext().lookup("java:comp/env/slee/nullactivity/activitycontextinterfacefactory");
            return factory.getActivityContextInterface(nullActivity);
        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
            return null;
        }
    }

    private void createTrace(String message) {
        try {
            TCKSbbUtils.createTrace(getSbbID(), Level.INFO, message, null);
        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }
    }

    private void callTest(Object o) {
        createTrace("Synchronous call to test: " + o);
        try {
            TCKSbbUtils.getResourceInterface().callTest(o);
        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }
    }

    // CMP fields
    public abstract String getInitialEventID();
    public abstract void setInitialEventID(String id);
}
