/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.profiles.readonly;

import java.rmi.RemoteException;
import java.util.HashMap;

import javax.management.ObjectName;
import javax.slee.profile.ProfileSpecificationID;
import javax.slee.profile.UnrecognizedProfileNameException;

import com.opencloud.sleetck.lib.AbstractSleeTCKTest;
import com.opencloud.sleetck.lib.OperationTimedOutException;
import com.opencloud.sleetck.lib.TCKTestErrorException;
import com.opencloud.sleetck.lib.TCKTestFailureException;
import com.opencloud.sleetck.lib.TCKTestResult;
import com.opencloud.sleetck.lib.infra.SleeTCKComponentConstants;
import com.opencloud.sleetck.lib.resource.TCKActivityID;
import com.opencloud.sleetck.lib.resource.TCKSbbMessage;
import com.opencloud.sleetck.lib.resource.events.TCKResourceEventX;
import com.opencloud.sleetck.lib.resource.testapi.TCKResourceTestInterface;
import com.opencloud.sleetck.lib.sbbutils.events2.SbbBaseMessageConstants;
import com.opencloud.sleetck.lib.testutils.ProfileUtils;
import com.opencloud.sleetck.lib.testutils.QueuingResourceListener;
import com.opencloud.sleetck.lib.testutils.jmx.ProfileMBeanProxy;
import com.opencloud.sleetck.lib.testutils.jmx.ProfileProvisioningMBeanProxy;


public abstract class ReadOnlyIsTrueBaseTest extends AbstractSleeTCKTest {

    private static final String SERVICE_DU_PATH_PARAM= "serviceDUPath";

    private static final String SPEC_VERSION = "1.0";

    protected abstract String getSpecName();

    /**
     * This test is a base class for the Tests Test11110067 and Test1110068.
     */
    public TCKTestResult run() throws Exception {

        String specName = getSpecName();

        //Create a profile table
        ProfileProvisioningMBeanProxy profileProvisioning = profileUtils.getProfileProvisioningProxy();
        ProfileSpecificationID specID = new ProfileSpecificationID(specName, SleeTCKComponentConstants.TCK_VENDOR, SPEC_VERSION);
        profileProvisioning.createProfileTable(specID, ReadOnlyIsTrueTestsSbb.PROFILE_TABLE_NAME);
        getLog().fine("Added profile table "+ReadOnlyIsTrueTestsSbb.PROFILE_TABLE_NAME);

        //Create a profile via management view
        ObjectName profile = profileProvisioning.createProfile(ReadOnlyIsTrueTestsSbb.PROFILE_TABLE_NAME, ReadOnlyIsTrueTestsSbb.PROFILE_NAME);
        ProfileMBeanProxy profileProxy = utils().getMBeanProxyFactory().createProfileMBeanProxy(profile);
        getLog().fine("Created profile "+ReadOnlyIsTrueTestsSbb.PROFILE_NAME+" for profile table "+ReadOnlyIsTrueTestsSbb.PROFILE_TABLE_NAME);


        //1110066: Whether profile is marked as readonly or not does not affect the management view
        //that continues to be read-write.
        try {
            utils().getMBeanFacade().invoke(profile, "setValue", new Object[]{"42"}, new String[]{"java.lang.String"});
            getLog().fine("Called set accessor via management interface.");
        } catch (Exception e) {
            return TCKTestResult.failed(1110066, "Exception occured when trying to set a CMP field value on " +
                    "a read-only profile via management client.");
        }

        //commit and close the Profile
        profileProxy.commitProfile();
        profileProxy.closeProfile();
        getLog().fine("Commit and close profile "+ReadOnlyIsTrueTestsSbb.PROFILE_NAME);

        getLog().fine("Start sending events to SLEE to get Sbb to do the profile modifications...");
        sendResourceEvent(ReadOnlyIsTrueTestsSbb.CMP_SET);

        sendResourceEvent(ReadOnlyIsTrueTestsSbb.PROF_LOCAL_SET);

        sendResourceEvent(ReadOnlyIsTrueTestsSbb.PROF_LOCAL_INDIR_SET);

        sendResourceEvent(ReadOnlyIsTrueTestsSbb.PROF_TAB_CREATE);

        sendResourceEvent(ReadOnlyIsTrueTestsSbb.PROF_TAB_REMOVE);

        sendResourceEvent(ReadOnlyIsTrueTestsSbb.PROF_LOCAL_REMOVE);
        getLog().fine("Completed test sequence successfully.");

        //check that profile is still there
        try {
            profile = profileProvisioning.getProfile(ReadOnlyIsTrueTestsSbb.PROFILE_TABLE_NAME, ReadOnlyIsTrueTestsSbb.PROFILE_NAME);
            profileProxy = utils().getMBeanProxyFactory().createProfileMBeanProxy(profile);
            String value = (String)utils().getMBeanFacade().invoke(profile, "getValue", new Object[]{}, new String[]{});
            if (!"42".equals(value))
                return TCKTestResult.failed(1110071, "Profile "+ReadOnlyIsTrueTestsSbb.PROFILE_NAME+" was modified by SLEE component though profile spec is declared read-only.");
        }
        catch (UnrecognizedProfileNameException e) {
            return TCKTestResult.failed(1110074, "Profile "+ReadOnlyIsTrueTestsSbb.PROFILE_NAME+" was removed by SLEE component though profile spec is declared read-only.");
        }
        getLog().fine("Checked that profile "+ReadOnlyIsTrueTestsSbb.PROFILE_NAME+" has not been deleted or modified by SLEE component.");

        //check that second profile has not been created
        try {
            profile = profileProvisioning.getProfile(ReadOnlyIsTrueTestsSbb.PROFILE_TABLE_NAME, ReadOnlyIsTrueTestsSbb.PROFILE_NAME2);
            return TCKTestResult.failed(1110073, "Profile "+ReadOnlyIsTrueTestsSbb.PROFILE_NAME2+" was created by SLEE component though profile spec is declared read-only.");
        }
        catch (UnrecognizedProfileNameException e) {
            getLog().fine("Checked that profile "+ReadOnlyIsTrueTestsSbb.PROFILE_NAME2+" has not been created by SLEE component.");
        }

        return TCKTestResult.passed();
    }

    public void sendResourceEvent(int operationID) throws TCKTestErrorException, RemoteException, TCKTestFailureException {
      // send an initial event to the test
      TCKResourceTestInterface resource = utils().getResourceInterface();
      TCKActivityID activityID = resource.createActivity(getClass().getName());
      Object message = new Integer(operationID);
      resource.fireEvent(TCKResourceEventX.X1, message, activityID, null);

      try {
          TCKSbbMessage reply = resourceListener.nextMessage();
          HashMap map = (HashMap) reply.getMessage();
          int type = ((Integer)map.get(SbbBaseMessageConstants.TYPE)).intValue();

          switch (type) {
          case SbbBaseMessageConstants.TYPE_SET_RESULT:
              String msg = (String) map.get(SbbBaseMessageConstants.MSG);
              int id = ((Integer)map.get(SbbBaseMessageConstants.ID)).intValue();
              boolean result = ((Boolean)map.get(SbbBaseMessageConstants.RESULT)).booleanValue();
              if (result)
                  getLog().fine(id+": "+msg);
              else {
                  getLog().fine("FAILURE: "+msg);
                  throw new TCKTestFailureException(id, msg);
              }
              break;
          }
      } catch (OperationTimedOutException ex) {
          throw new TCKTestErrorException("Timed out waiting for processing of initial resource event.",ex);
      }
    }

    public void setUp() throws Exception {

        setupService(SERVICE_DU_PATH_PARAM);

        profileUtils = new ProfileUtils(utils());
        resourceListener = new QueuingResourceListener(utils()) {
            public Object onSbbCall(Object argument) throws Exception {
                HashMap map = (HashMap) argument;
                int type = ((Integer)map.get(SbbBaseMessageConstants.TYPE)).intValue();
                switch (type) {
                case SbbBaseMessageConstants.TYPE_LOG_MSG:
                    getLog().fine((String)map.get(SbbBaseMessageConstants.MSG));
                }
                return null;
            }
        };
        setResourceListener(resourceListener);
    }

    public void tearDown() throws Exception {

        try {
            profileUtils.removeProfileTable(ReadOnlyIsTrueTestsSbb.PROFILE_TABLE_NAME);
        }
        catch (Exception e) {
            getLog().warning("Caught exception while trying to remove profile table:");
            getLog().warning(e);
        }

        super.tearDown();
    }

    private ProfileUtils profileUtils;
    private QueuingResourceListener resourceListener;
}
