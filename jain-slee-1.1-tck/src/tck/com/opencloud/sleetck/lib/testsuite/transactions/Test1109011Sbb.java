/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.transactions;

import java.util.HashMap;

import javax.slee.ActivityContextInterface;
import javax.slee.RolledBackContext;
import javax.slee.SbbContext;

import com.opencloud.logging.StdErrLog;
import com.opencloud.sleetck.lib.profileutils.BaseMessageSender;
import com.opencloud.sleetck.lib.rautils.RMIObjectChannel;
import com.opencloud.sleetck.lib.rautils.TCKRAUtils;
import com.opencloud.sleetck.lib.resource.events.TCKResourceEventX;
import com.opencloud.sleetck.lib.sbbutils.BaseTCKSbb;

public abstract class Test1109011Sbb extends BaseTCKSbb {
    //custom messaging constants
    public static final int TYPE_EVENTHANDLER = 1025;
    public static final int TYPE_ROLLBACK = 1026;
    public static final int TYPE_CMP_VALUE = 1027;


    //Sbb lifecycle method. Acquire RMIObjectChannel output stream
    public void setSbbContext(SbbContext context) {
        super.setSbbContext(context);

        try {
            out = TCKRAUtils.lookupRMIObjectChannel();
            msgSender = new BaseMessageSender(out, new StdErrLog());
        }
        catch (Exception e) {
            context.getTracer(getSbbID().getName()).fine("An error occured creating an RMIObjectChannel:", e);
        }

    }

    // Sbb lifecycle rollback method
    public void sbbRolledBack(RolledBackContext context) {

        try {
            msgSender.sendLogMsg("sbbRolledBack called.");

            HashMap map = msgSender.getBaseConfig();
            map.put("SbbID", getSbbIDValue());
            msgSender.sendCustomMessage(TYPE_ROLLBACK, map);
            msgSender.sendLogMsg("Sent Sbb message.");

            //in case the SLEE implementation breaks 1109013, i.e. calls sbbRolledBack
            //over and over again, we need sth that stops the endless loop
            if (rolledbackCount>0)
                return;
            rolledbackCount++;

            //1109014: check that CMP field is NOT 'newValue' otherwise report failure to test
            if ("newValue".equals(getStringValue()))
                msgSender.sendFailure(1109014, "The TXN sbbRolledBack() is called with has to be a new TXN.");
            else
                msgSender.sendLogMsg("sbbRolledBack() was executed in a new TXN as expected.");


            setStringValue("sbbRolledBack");

            msgSender.sendLogMsg("Mark sbbRolledBack TXN for rollback.");
            getSbbContext().setRollbackOnly();

        } catch (Exception e) {
            msgSender.sendException(e);
        }

    }

    // Start the test
    public void onTCKResourceEventX2(TCKResourceEventX ev, ActivityContextInterface aci) {
        try {
            msgSender.sendLogMsg("Received TCKResourceEvent.X2");

            //send sbbID to test
            HashMap map = msgSender.getBaseConfig();
            map.put("SbbID", getSbbIDValue());
            msgSender.sendCustomMessage(TYPE_EVENTHANDLER, map);
            msgSender.sendLogMsg("Sent Sbb message.");

            //Set CMP field to new Value, then check in sbbRolledBack that value is back to default, thus proving that the TXN is new
            setStringValue("newValue");

            msgSender.sendLogMsg("Mark event handler TXN for rollback.");
            getSbbContext().setRollbackOnly();

        } catch (Exception e) {
            msgSender.sendException(e);
        }
    }

    // Initial event
    public void onTCKResourceEventX1(TCKResourceEventX ev, ActivityContextInterface aci) {
        //Send ACK to the test
        try {
            if (getSbbIDValue()==null) {
                msgSender.sendLogMsg("Set CMP field from 'null' to "+ev.getMessage());
                setSbbIDValue((String)ev.getMessage());
                msgSender.sendSuccess(1109011, "Received TCKResourceEvent.X1 with message "+ev.getMessage());
            }

        } catch (Exception e) {
            msgSender.sendException(e);
        }
    }

    public void onTCKResourceEventX3(TCKResourceEventX ev, ActivityContextInterface aci) {
        //Send current value of CMP field 'stringValue' to the test
        try {
            HashMap map = new HashMap();
            map.put("stringValue", getStringValue());
            msgSender.sendCustomMessage(TYPE_CMP_VALUE, map);
        } catch (Exception e) {
            msgSender.sendException(e);
        }
    }

    private RMIObjectChannel out;

    public abstract void setStringValue(String value);
    public abstract String getStringValue();

    public abstract void setSbbIDValue(String value);
    public abstract String getSbbIDValue();
    private BaseMessageSender msgSender;

    private static int rolledbackCount=0;

}
