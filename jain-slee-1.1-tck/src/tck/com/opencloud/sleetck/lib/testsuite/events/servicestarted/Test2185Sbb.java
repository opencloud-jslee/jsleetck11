/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.events.servicestarted;

import com.opencloud.sleetck.lib.sbbutils.BaseTCKSbb;
import com.opencloud.sleetck.lib.sbbutils.TCKSbbUtils;

import javax.slee.ActivityContextInterface;
import javax.slee.Address;
import javax.slee.serviceactivity.ServiceStartedEvent;
import java.util.HashMap;

public abstract class Test2185Sbb extends BaseTCKSbb {

    // Initial event
    public void onServiceStartedEvent(ServiceStartedEvent event, ActivityContextInterface aci) {

        try {
            HashMap map = new HashMap();
            map.put("Result", new Boolean(true));
            setResult(map);

            // If the ACI is ending send the result straight back to
            // the TCK straight away.
            // -- We only need to send the result via a second event the
            // first time we see this event.
            if (aci.isEnding())
                TCKSbbUtils.getResourceInterface().sendSbbMessage(map);
            else
                fireSendResultEvent(new SendResultEvent(), aci, null);

        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }
    }

    public void onSendResultEvent(SendResultEvent event, ActivityContextInterface aci) {
        try {
            TCKSbbUtils.getResourceInterface().sendSbbMessage(getResult());
        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }
    }

    public abstract void fireSendResultEvent(SendResultEvent event, ActivityContextInterface aci, Address address);

    public abstract void setResult(HashMap map);

    public abstract HashMap getResult();

}
