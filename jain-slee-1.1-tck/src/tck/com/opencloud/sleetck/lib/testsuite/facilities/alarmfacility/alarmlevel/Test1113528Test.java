/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.facilities.alarmfacility.alarmlevel;

import javax.slee.facilities.AlarmLevel;

import com.opencloud.sleetck.lib.AbstractSleeTCKTest;
import com.opencloud.sleetck.lib.TCKTestResult;

/*
 * AssertionID(1113528): Test Each of the is<Level> methods determines if this AlarmLevel object 
 * represents the <LEVEL> level, and is equivalent to (this == <LEVEL>).
 *
 * AssertionID(1113616): Test This method is effectively equivalent to the conditional test: 
 * (this == CLEAR), ie. the code: if (alarmLevel.isClear()) ...  is interchangeable with the 
 * code: if (alarmLevel == AlarmLevel.CLEAR) ... 
 *
 * AssertionID(1113620): Test This method is effectively equivalent to the conditional test: 
 * (this == CRITICAL), ie. the code: if (alarmLevel.isCritical()) ...  is interchangeable 
 * with the code: if (alarmLevel == AlarmLevel.CRITICAL) ... 
 *
 * AssertionID(1113624): Test This method is effectively equivalent to the conditional test: 
 * (this == MAJOR), ie. the code: if (alarmLevel.isMajor()) ...  is interchangeable with the 
 * code: if (alarmLevel == AlarmLevel.MAJOR) ... 
 *
 * AssertionID(1113628): Test This method is effectively equivalent to the conditional test: 
 * (this == WARNING), ie. the code: if (alarmLevel.isWarning()) ...  is interchangeable with 
 * the code: if (alarmLevel == AlarmLevel.WARNING) ... 
 *
 * AssertionID(1113632): Test This method is effectively equivalent to the conditional test: 
 * (this == INDETERMINATE), ie. the code: if (alarmLevel.isIndeterminate()) ...  is interchangeable 
 * with the code: if (alarmLevel == AlarmLevel.INDETERMINATE) ... 
 *
 * AssertionID(1113636): Test This method is effectively equivalent to the conditional test: 
 * (this == MINOR), ie. the code: if (alarmLevel.isMinor()) ...  is interchangeable with the code: 
 * if (alarmLevel == AlarmLevel.MINOR) ... 
 * 
 */
public class Test1113528Test extends AbstractSleeTCKTest {

    /**
     * Perform the actual test.
     */

    public TCKTestResult run() throws Exception {
        AlarmLevel alarmLevel = null;

        // 1113528:CLEAR
        alarmLevel = AlarmLevel.CLEAR;
        try {
            if (!alarmLevel.isClear()) {
                return TCKTestResult.failed(1113528, "This alarmLevel.isClear() method didn't return true");
            }
            if (alarmLevel != AlarmLevel.CLEAR) {
                return TCKTestResult.failed(1113528, "This alarmLevel argument didn't return AlarmLevel.CLEAR");
            }
            getLog().info(
                    "Test Each of the isClear() methods determines if this AlarmLevel object "
                            + "represents the AlarmLevel.CLEAR level, and is equivalent to this == AlarmLevel.CLEAR");
        } catch (Exception e) {
            return TCKTestResult.failed(1113528, "alarmLevel.isClear() threw an exception.");
        }

        // 1113616
        try {
            if (alarmLevel.isClear() != (alarmLevel == AlarmLevel.CLEAR)) {
                return TCKTestResult.failed(1113616, "This isClear() method didn't equivalent to "
                        + "the conditional test: (this == CLEAR)");
            }
            getLog().info("This isClear() method is effectively equivalent to the conditional " + "test: this == CLEAR");
        } catch (Exception e) {
            return TCKTestResult.failed(1113616, "alarmLevel.isClear() equal test threw an exception.");
        }

        // 1113528:CRITICAL
        alarmLevel = AlarmLevel.CRITICAL;
        try {
            if (!alarmLevel.isCritical()) {
                return TCKTestResult.failed(1113528, "This alarmLevel.isCritical() method didn't return true");
            }
            if (alarmLevel != AlarmLevel.CRITICAL) {
                return TCKTestResult.failed(1113528, "This alarmLevel argument didn't return AlarmLevel.CRITICAL");
            }
            getLog().info("Test Each of the isCritical() methods determines if this AlarmLevel object "
                                    + "represents the AlarmLevel.CRITICAL level, and is equivalent to this == AlarmLevel.CRITICAL");
        } catch (Exception e) {
            return TCKTestResult.failed(1113528, "alarmLevel.isCritical() threw an exception.");
        }

        // 1113620
        try {
            if (alarmLevel.isCritical() != (alarmLevel == AlarmLevel.CRITICAL)) {
                return TCKTestResult.failed(1113620, "This isCritical() method didn't equivalent to "
                        + "the conditional test: (this == CRITICAL)");
            }
            getLog().info("This isCritical() method is effectively equivalent to the conditional "
                                    + "test: this == CRITICAL");
        } catch (Exception e) {
            return TCKTestResult.failed(1113620, "alarmLevel.isCritical() equal test threw an exception.");
        }

        // 1113528:MAJOR
        alarmLevel = AlarmLevel.MAJOR;
        try {
            if (!alarmLevel.isMajor()) {
                return TCKTestResult.failed(1113528, "This alarmLevel.isMajor() method didn't return true");
            }
            if (alarmLevel != AlarmLevel.MAJOR) {
                return TCKTestResult.failed(1113528, "This alarmLevel argument didn't return AlarmLevel.MAJOR");
            }
            getLog().info(
                    "Test Each of the isMajor() methods determines if this AlarmLevel object "
                            + "represents the AlarmLevel.MAJOR level, and is equivalent to this == AlarmLevel.MAJOR");
        } catch (Exception e) {
            return TCKTestResult.failed(1113528, "alarmLevel.isMajor() threw an exception.");
        }

        // 1113624
        try {
            if (alarmLevel.isMajor() != (alarmLevel == AlarmLevel.MAJOR)) {
                return TCKTestResult.failed(1113624, "This isMajor() method didn't equivalent to "
                        + "the conditional test: (this == MAJOR)");
            }
            getLog().info("This isMajor() method is effectively equivalent to the conditional " + "test: this == MAJOR");
        } catch (Exception e) {
            return TCKTestResult.failed(1113624, "alarmLevel.isMajor() equal test threw an exception.");
        }

        // 1113528:WARNING
        alarmLevel = AlarmLevel.WARNING;
        try {
            if (!alarmLevel.isWarning()) {
                return TCKTestResult.failed(1113528, "This alarmLevel.isWarning() method didn't return true");
            }
            if (alarmLevel != AlarmLevel.WARNING) {
                return TCKTestResult.failed(1113528, "This alarmLevel argument didn't return AlarmLevel.WARNING");
            }
            getLog().info("Test Each of the isWarning() methods determines if this AlarmLevel object "
                                    + "represents the AlarmLevel.WARNING level, and is equivalent to this == AlarmLevel.WARNING");
        } catch (Exception e) {
            return TCKTestResult.failed(1113528, "alarmLevel.isWarning() threw an exception.");
        }

        // 1113628
        try {
            if (alarmLevel.isWarning() != (alarmLevel == AlarmLevel.WARNING)) {
                return TCKTestResult.failed(1113628, "This isWarning() method didn't equivalent to "
                        + "the conditional test: (this == WARNING)");
            }
            getLog().info(
                    "This isWarning() method is effectively equivalent to the conditional " + "test: this == WARNING");
        } catch (Exception e) {
            return TCKTestResult.failed(1113628, "alarmLevel.isWarning() equal test threw an exception.");
        }

        // 1113528:INDETERMINATE
        alarmLevel = AlarmLevel.INDETERMINATE;
        try {
            if (!alarmLevel.isIndeterminate()) {
                return TCKTestResult.failed(1113528, "This alarmLevel.isIndeterminate() method didn't return true");
            }
            if (alarmLevel != AlarmLevel.INDETERMINATE) {
                return TCKTestResult.failed(1113528, "This alarmLevel argument didn't return AlarmLevel.INDETERMINATE");
            }
            getLog().info("Test Each of the isIndeterminate() methods determines if this AlarmLevel object "
                                    + "represents the AlarmLevel.INDETERMINATE level, and is equivalent to this == AlarmLevel.INDETERMINATE");
        } catch (Exception e) {
            return TCKTestResult.failed(1113528, "alarmLevel.isIndeterminate() threw an exception.");
        }

        // 1113632
        try {
            if (alarmLevel.isIndeterminate() != (alarmLevel == AlarmLevel.INDETERMINATE)) {
                return TCKTestResult.failed(1113632, "This isIndeterminate() method didn't equivalent to "
                        + "the conditional test: (this == INDETERMINATE)");
            }
            getLog().info(
                    "This isIndeterminate() method is effectively equivalent to the conditional "
                            + "test: this == INDETERMINATE");
        } catch (Exception e) {
            return TCKTestResult.failed(1113632, "alarmLevel.isIndeterminate() equal test threw an exception.");
        }

        // 1113528:MINOR
        alarmLevel = AlarmLevel.MINOR;
        try {
            if (!alarmLevel.isMinor()) {
                return TCKTestResult.failed(1113528, "This alarmLevel.isMinor() method didn't return true");
            }
            if (alarmLevel != AlarmLevel.MINOR) {
                return TCKTestResult.failed(1113528, "This alarmLevel argument didn't return AlarmLevel.MINOR");
            }
            getLog().info(
                    "Test Each of the isMinor() methods determines if this AlarmLevel object "
                            + "represents the AlarmLevel.MINOR level, and is equivalent to this == AlarmLevel.MINOR");
        } catch (Exception e) {
            return TCKTestResult.failed(1113528, "alarmLevel.isMinor() threw an exception.");
        }

        // 1113636
        try {
            if (alarmLevel.isMinor() != (alarmLevel == AlarmLevel.MINOR)) {
                return TCKTestResult.failed(1113636, "This isMinor() method didn't equivalent to "
                        + "the conditional test: (this == MINOR)");
            }
            getLog().info("This isMinor() method is effectively equivalent to the conditional " + "test: this == MINOR");
        } catch (Exception e) {
            return TCKTestResult.failed(1113636, "alarmLevel.isMinor() equal test threw an exception.");
        }

        return TCKTestResult.passed();
    }

    /**
     * Do all the pre-run configuration of the test.
     */

    public void setUp() throws Exception {

    }

    /**
     * Clean up after the test.
     */

    public void tearDown() throws Exception {
    }
}
