/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.profiles.profilespec;

import javax.slee.profile.ProfileLocalObject;

/**
 * Test reentrant behaviour
 */
public interface Test1110225ProfileLocal extends ProfileLocalObject {
    public static final String PROFILE_NAME = "Test1110225Profile";
    public static final String PROFILE_NAME2 = "Test1110225Profile2";
    public static final String PROFILE_TABLE_NAME = "Test1110225ProfileTable";

    public boolean callFromTestSbb();
    public boolean callFromA();
    public boolean callFromB();
}
