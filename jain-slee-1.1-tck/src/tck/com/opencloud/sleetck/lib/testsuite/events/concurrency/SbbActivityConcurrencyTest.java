/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.events.concurrency;

import com.opencloud.sleetck.lib.AbstractSleeTCKTest;
import com.opencloud.sleetck.lib.TCKTestErrorException;
import com.opencloud.sleetck.lib.TCKTestFailureException;
import com.opencloud.sleetck.lib.TCKTestResult;
import com.opencloud.sleetck.lib.resource.TCKActivityID;
import com.opencloud.sleetck.lib.resource.events.TCKResourceEventX;
import com.opencloud.sleetck.lib.resource.events.TCKResourceEventY;
import com.opencloud.sleetck.lib.resource.testapi.TCKResourceTestInterface;
import com.opencloud.sleetck.lib.testutils.BaseTCKResourceListener;
import com.opencloud.sleetck.lib.testutils.FutureResult;
import com.opencloud.util.Future;

import javax.slee.Address;
import javax.slee.AddressPlan;
import java.rmi.RemoteException;

/**
 * Tests assertion 771 - that multiple Sbb entities do not concurrently
 * receive events on a single Activity Context.
 * It tests this by blocking inside event handlers and ensuring that no other event handlers
 * are called for the same activity during the block.
 */
public class SbbActivityConcurrencyTest extends AbstractSleeTCKTest {

    // Test parameters

    private static final String WAIT_PERIOD_MS_PARAM = "waitPeriodMs";
    public static final String SERVICE1_DU_PATH_PARAM = "service1DUPath";
    public static final String SERVICE2_DU_PATH_PARAM = "service2DUPath";

    // SleeTCKTest methods

    public TCKTestResult run() throws Exception {
        TCKResourceTestInterface resource = utils().getResourceInterface();
        String testCase = "Fire events on an activity to multiple instances of the same Sbb component";
        getLog().info("Creating activity A");
        TCKActivityID activityA = resource.createActivity("SbbActivityConcurrencyTest-ActvityA");
        TCKTestResult testCaseAResult = testConcurrency(activityA, TCKResourceEventX.X1,
            SbbActivityConcurrencyTestConstants.SBB1_RECEIVED_X1,
            TCKResourceEventX.X2, SbbActivityConcurrencyTestConstants.SBB1_RECEIVED_X2,
            SbbActivityConcurrencyTestConstants.SBB1_RECEIVED_X2_ROLL_BACK,
            2, testCase); // expect 2 event 2 ACKs, because both Sbb1 instances should receive event 2
        if(!testCaseAResult.isPassed()) return testCaseAResult;

        testCase = "Fire events on an activity to different Sbb components";
        getLog().info("Creating activity B");
        TCKActivityID activityB = resource.createActivity("SbbActivityConcurrencyTest-ActvityB");
        return testConcurrency(activityB, TCKResourceEventY.Y1, SbbActivityConcurrencyTestConstants.SBB1_RECEIVED_Y1,
            TCKResourceEventY.Y2, SbbActivityConcurrencyTestConstants.SBB2_RECEIVED_Y2,
                SbbActivityConcurrencyTestConstants.SBB2_RECEIVED_Y2_ROLL_BACK, 1, testCase);
    }

    public void setUp() throws Exception {
        waitPeriodMs = Integer.parseInt(utils().getTestParams().getProperty(WAIT_PERIOD_MS_PARAM));
        setResourceListener(new ResourceListenerImpl());
        setupService(SERVICE1_DU_PATH_PARAM, true);
        setupService(SERVICE2_DU_PATH_PARAM, true);
    }

    // Private methods

    private TCKTestResult testConcurrency(TCKActivityID activityID,
            String event1Type, int event1ACKCode, String event2Type, int event2ACKCode, int event2RollbackCode,
            int expectedEvent2ACKs, String testCase) throws TCKTestErrorException, RemoteException {

        getLog().info("Test case: "+testCase);

        // clear test case state
        isEvent1Received = isEvent1ProcessingFinished = isEvent2ProcessingFinished = isBlocking = stopBlocking = false;
        event2HandlerACKCount = event2RolledBackCount = 0;
        event1ObjectID = event2ObjectID = 0;
        testCaseResult = new FutureResult(getLog());

        // set test case configuration
        this.activityID=activityID;
        this.event1Type=event1Type;
        this.event1ACKCode=event1ACKCode;
        this.event2Type=event2Type;
        this.event2ACKCode=event2ACKCode;
        this.event2RollbackCode=event2RollbackCode;
        this.expectedEvent2ACKs=expectedEvent2ACKs;
        this.testCase=testCase;

        // execute the test case
        getLog().info("Firing an "+event1Type+" event on activity: "+activityID+", on a new address");
        synchronized(sbbCallLock) {
            event1ObjectID = utils().getResourceInterface().fireEvent(event1Type,null,activityID,nextAddress());
        }
        long timeout = utils().getTestTimeout() + waitPeriodMs; // we will block the 1st event handler for waitPeriodMs
        getLog().fine("Total timeout is "+timeout+" ms");
        try {
            return testCaseResult.waitForResult(timeout);
        } catch (Future.TimeoutException ex) {
            String testState = "isEvent1Received="+isEvent1Received+",isEvent1ProcessingFinished="+isEvent1ProcessingFinished+
                    ",event2HandlerACKCount count="+event2HandlerACKCount+",event2RolledBackCount count="+event2RolledBackCount+
                    ",isEvent2ProcessingFinished="+isEvent2ProcessingFinished+
                    ",event1ObjectID="+event1ObjectID+",event2ObjectID="+event2ObjectID+",isBlocking="+isBlocking;
            return TCKTestResult.error("Timed out while waiting for event handler invocations. Test case:"+testCase+
                ". Test state: "+testState,ex);
        } finally {
            waitToUnblock();
            waitForEventProcessing();
        }
    }

    /**
     * Returns a unique address with AddressPlan IP
     */
    private synchronized Address nextAddress() {
        return new Address(AddressPlan.IP,"1.0.0."+addressSuffix++);
    }

    private String formatCallCode(int callCode) {
        switch (callCode) {
            case SbbActivityConcurrencyTestConstants.SBB1_RECEIVED_X1: return "SBB1 RECEIVED X1";
            case SbbActivityConcurrencyTestConstants.SBB1_RECEIVED_X2: return "SBB1 RECEIVED X2";
            case SbbActivityConcurrencyTestConstants.SBB1_RECEIVED_Y1: return "SBB1 RECEIVED Y1";
            case SbbActivityConcurrencyTestConstants.SBB2_RECEIVED_Y2: return "SBB2 RECEIVED Y2";
            default: return "(Invalid call code:"+callCode+")";
        }
    }

    /**
     * Waits until the event handler call unblocks
     */
    private void waitToUnblock() {
        synchronized (sbbCallLock) {
            if(isBlocking) {
                stopBlocking = true;
                sbbCallLock.notifyAll();
                while(isBlocking) {
                    try {
                        getLog().finest("Waiting for sbb call to unblock...");
                        sbbCallLock.wait();
                        getLog().finest("Unblocked");
                    } catch (InterruptedException ie) { /* no-op */ }
                }
            }
        }
    }

    /**
     * Waits until event processing is finished for both events
     */
    private void waitForEventProcessing() {
        synchronized(sbbCallLock) {
            if(!isEvent1ProcessingFinished || !isEvent2ProcessingFinished) {
                long now = System.currentTimeMillis();
                long timeoutAt = now + utils().getTestTimeout();
                getLog().info("Will wait for "+utils().getTestTimeout()+" ms, or until event processing finishes for both events");
                while(now < timeoutAt && (!isEvent1ProcessingFinished || !isEvent2ProcessingFinished)) {
                    try {
                        sbbCallLock.wait(timeoutAt - now);
                    } catch (InterruptedException e) { /* no-op */ }
                    now = System.currentTimeMillis();
                }
            }
        }
    }

    // Private classes

    private class ResourceListenerImpl extends BaseTCKResourceListener {

        // TCKResourceListener methods

        /**
         * Called synchronously by the Sbb.
         * It expects an Integer argument representing the call code
         * This method is responsible for:
         * a) blocking method invocations
         * b) checking that method invocations are not made in parallel for the activity
         */
        public Object onSbbCall(Object argument) throws Exception {
            try {
                synchronized (sbbCallLock) {
                    int code = ((Integer)argument).intValue();
                    getLog().info("Received call from sbb. Code="+formatCallCode(code));

                    if(code == event1ACKCode) { // EVENT 1
                        isEvent1Received = true;
                        getLog().info("Firing a "+event2Type+" event on activity "+activityID);
                        event2ObjectID = utils().getResourceInterface().fireEvent(event2Type,null,activityID,nextAddress());
                        isBlocking = true;
                        try {
                            long now = System.currentTimeMillis();
                            long timeoutAt = now + waitPeriodMs;
                            getLog().info("Will wait for "+waitPeriodMs+" ms, or until a result is set");
                            while(now < timeoutAt && !testCaseResult.isSet() && !stopBlocking) {
                                sbbCallLock.wait(timeoutAt - now);
                                now = System.currentTimeMillis();
                            }
                            getLog().info("Stopped blocking.");
                        } finally {
                            isBlocking = false;
                            sbbCallLock.notifyAll();
                        }
                    } else if(code == event2ACKCode) { // EVENT 2
                        if(isBlocking) {
                            // we received a syncronous call from an sbb during another method invocation on the same activity
                            throw new TCKTestFailureException(771,
                                "Serial method invocation was not maintained on a single activity. Test case:"+testCase);
                        }
                        event2HandlerACKCount++;
                    } else if(code == event2RollbackCode) { // sbbRolledBack() for EVENT 2
                        event2RolledBackCount++;
                    } else {
                        throw new TCKTestErrorException("Unexpected code passed to onSbbCall() during "+testCase+" test case: "+
                                formatCallCode(code));
                    }
                    checkIsSatisfied();
                }
            } catch(Exception exception) {
                onException(exception);
            }
            return null; // no return object
        }

        public void onEventProcessingSuccessful(long eventObjectID) throws RemoteException {
            onEventProcessingCompleted(eventObjectID,true,null,null);
        }

        public void onEventProcessingFailed(long eventObjectID, String message, Exception exception) {
            onEventProcessingCompleted(eventObjectID,false,message,exception);
        }

        /**
         * Called by onEventProcessingSuccessful and onEventProcessingFailed()
         * @param eventObjectID the eventObjectID passed to the calling method
         * @param wasSuccess true for onEventProcessingSuccessful, false for onEventProcessingFailed
         * @param message the message passed to onEventProcessingFailed, or null
         * @param exception the Exception passed to onEventProcessingFailed, or null
         */
        private void onEventProcessingCompleted(long eventObjectID, boolean wasSuccess, String message, Exception exception) {
            synchronized (sbbCallLock) {
                if(eventObjectID == event2ObjectID) {
                    if(event2HandlerACKCount < expectedEvent2ACKs) {
                        String methodName = wasSuccess ? "onEventProcessingSuccessful()" : "onEventProcessingFailed()";
                        getLog().warning("Received "+methodName+" call for "+event2Type+" event before the expected number of " +
                                "ACKs were received from the "+event2Type+" event handler. "+
                                "This is allowed by the test, as the SLEE may choose to timeout the "+event2Type+
                                " event while waiting for the blocking callback to return, or rollback event delivery attempts. " +
                                "Number of sbbRolledBack() ACKs received for the event: "+event2RolledBackCount);
                        if(!wasSuccess) {
                            getLog().warning("Failure message="+message+". Failure exception follows:");
                            getLog().warning(exception);
                        }
                    }
                    isEvent2ProcessingFinished = true;
                    checkIsSatisfied();
                } else if(eventObjectID == event1ObjectID) {
                    isEvent1ProcessingFinished = true;
                    if(wasSuccess) {
                        getLog().fine("Received onEventProcessingSuccessful() callback for "+event1Type+
                                " event. eventObjectID="+eventObjectID);
                    } else {
                        onException(new TCKTestErrorException("Unexpected call to onEventProcessingFailed() received for "+
                                event1Type+" event. eventObjectID="+eventObjectID+". Failure message="+message,exception));
                    }
                } else {
                    if(wasSuccess) {
                        getLog().warning("Received unexpected onEventProcessingSuccessful() callback for an event " +
                                "which doesn't appear to be either of the events fired in this test case. eventObjectID="+eventObjectID);
                    } else {
                        onException(new TCKTestErrorException("Unexpected call to onEventProcessingFailed() received "+
                                "for an event which doesn't appear to be either of the events fired in this test case. "+
                                "eventObjectID="+eventObjectID+". Failure message="+message,exception));
                    }
                }
            }
        }

        public void onException(Exception exception) {
            if(exception instanceof TCKTestFailureException) testCaseResult.setFailed((TCKTestFailureException)exception);
            else testCaseResult.setError(exception);
        }

        private void checkIsSatisfied() {
            synchronized (sbbCallLock) {
                if(event2HandlerACKCount >= expectedEvent2ACKs || isEvent2ProcessingFinished) {
                    if(!isBlocking && !testCaseResult.isSet()) {
                        getLog().info("All callbacks received serially");
                        testCaseResult.setPassed();
                    }
                }
            }
        }

    }

    // Private state

    private int waitPeriodMs; // the number of milliseconds to block during a method invocation
    private Object sbbCallLock = new Object();
    private int addressSuffix = 1;

    // test case configuration
    private String testCase;
    private TCKActivityID activityID;
    private String event1Type;
    private int event1ACKCode;
    private String event2Type;
    private int event2ACKCode;
    private int event2RollbackCode;
    private int expectedEvent2ACKs;

    // test case state
    private FutureResult testCaseResult;

    private volatile boolean isEvent1Received;
    private volatile int event2HandlerACKCount;
    private volatile int event2RolledBackCount;

    private volatile boolean isEvent1ProcessingFinished;
    private volatile boolean isEvent2ProcessingFinished;

    private volatile long event1ObjectID;
    private volatile long event2ObjectID;

    private boolean isBlocking;
    private boolean stopBlocking;

}
