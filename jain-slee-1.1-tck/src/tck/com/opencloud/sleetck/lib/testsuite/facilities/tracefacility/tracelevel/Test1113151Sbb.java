/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.facilities.tracefacility.tracelevel;

import java.util.HashMap;

import javax.slee.ActivityContextInterface;
import javax.slee.facilities.TraceLevel;
import javax.slee.facilities.Tracer;

import com.opencloud.sleetck.lib.resource.events.TCKResourceEventX;
import com.opencloud.sleetck.lib.sbbutils.BaseTCKSbb;
import com.opencloud.sleetck.lib.sbbutils.TCKSbbUtils;

/*
 * AssertionID (1113151): Test The isHigherLevel method returns true if the 
 * level represented by this TraceLevel object is higher or more severe than 
 * the level specified by TraceLevel object passed as the other argument.
 * 
 * AssertionID (1113152): Test OFF has a higher level than SEVERE.

 * AssertionID (1113154): Test it throws a java.lang.NullPointerException 
 * if the other argument is null.
 * 
 * AssertionID (1113157): Test the fromInt and toInt methods allow conversion 
 * between the TraceLevel object form and numeric form.
 * 
 * AssertionID (1113158): Test The fromInt method throws a java.lang.IllegalArgumentException 
 * if the level argument is not one of the eight integer representations.
 * 
 * AssertionID (1113159): Test The fromString method get a TraceLevel object from a string 
 * value. It returns a TraceLevel object corresponding to level.
 * 
 * AssertionID (1113160): Test The fromString method throws java.lang.NullPointerException 
 * - if level is null.
 * 
 * AssertionID (1113161): Test The fromString method throws java.lang.IllegalArgumentException 
 * - if level is not a valid trace level string.
 * 
 */
public abstract class Test1113151Sbb extends BaseTCKSbb {

    // Initial event
    public void onTCKResourceEventX1(TCKResourceEventX event, ActivityContextInterface aci) {

        try {
            tracer = getSbbContext().getTracer("com.test");
            tracer.info("Received " + event + " message", null);

            doTest1113151Test();
        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }
    }

    private void sendResultToTCK(String testName, boolean result, int failedAssertionID,  String message) throws Exception {
        HashMap sbbData = new HashMap();
        sbbData.put("testname", testName);
        sbbData.put("result", result ? "pass" : "fail");
        sbbData.put("message", message);
        sbbData.put("id", new Integer(failedAssertionID));
        TCKSbbUtils.getResourceInterface().sendSbbMessage(sbbData);
    }

    private void doTest1113151Test() throws Exception {
        TraceLevel traceLevel = TraceLevel.FINEST;

        //1113151:OFF
        try {
            traceLevel = TraceLevel.OFF;
            if (traceLevel != TraceLevel.OFF) {
                sendResultToTCK("Test1113151Test", false, 1113151, "This traceLevel argument didn't return " + "TraceLevel.OFF");
                return;
            }

            // try to compare traceLevel with TraceLevel.HIGHER firstly, returns false.
            if (traceLevel.isHigherLevel(TraceLevel.OFF)) {
                sendResultToTCK("Test1113151Test", false, 1113151, "This isHigherLevel method didn't return "
                        + "the correct value (false) due to TraceLevel.OFF has "
                        + "the same value than TraceLevel.OFF.");
                return;
            }
            // try to compare traceLevel with TraceLevel.LOWER secondly, returns true.
            if (!traceLevel.isHigherLevel(TraceLevel.SEVERE)) {
                sendResultToTCK("Test1113151Test", false, 1113151, "This isHigherLevel method didn't return "
                        + "the correct value (true) due to TraceLevel.OFF has "
                        + "higher value than TraceLevel.SEVERE.");
                return;
            }

            if (!traceLevel.isHigherLevel(TraceLevel.WARNING)) {
                sendResultToTCK("Test1113151Test", false, 1113151, "This isHigherLevel method didn't return "
                        + "the correct value (true) due to TraceLevel.OFF has "
                        + "higher value than TraceLevel.WARNING.");
                return;
            }

            if (!traceLevel.isHigherLevel(TraceLevel.INFO)) {
                sendResultToTCK("Test1113151Test", false, 1113151, "This isHigherLevel method didn't return "
                        + "the correct value (true) due to TraceLevel.OFF has " + "higher value than TraceLevel.INFO.");
                return;
            }

            if (!traceLevel.isHigherLevel(TraceLevel.CONFIG)) {
                sendResultToTCK("Test1113151Test", false, 1113151, "This isHigherLevel method didn't return "
                        + "the correct value (true) due to TraceLevel.OFF has "
                        + "higher value than TraceLevel.CONFIG.");
                return;
            }

            if (!traceLevel.isHigherLevel(TraceLevel.FINE)) {
                sendResultToTCK("Test1113151Test", false, 1113151, "This isHigherLevel method didn't return "
                        + "the correct value (true) due to TraceLevel.OFF has " + "higher value than TraceLevel.FINE.");
                return;
            }

            if (!traceLevel.isHigherLevel(TraceLevel.FINER)) {
                sendResultToTCK("Test1113151Test", false, 1113151, "This isHigherLevel method didn't return "
                        + "the correct value (true) due to TraceLevel.OFF has " + "higher value than TraceLevel.FINER.");
                return;
            }

            if (!traceLevel.isHigherLevel(TraceLevel.FINEST)) {
                sendResultToTCK("Test1113151Test", false, 1113151, "This isHigherLevel method didn't return "
                        + "the correct value (true) due to TraceLevel.OFF has "
                        + "higher value than TraceLevel.FINEST.");
                return;
            }

            tracer.info("got expected value of the isHigherLevel method.", null);
        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }

        //1113151:SEVERE
        try {
            traceLevel = TraceLevel.SEVERE;
            if (traceLevel != TraceLevel.SEVERE) {
                sendResultToTCK("Test1113151Test", false, 1113151, "This traceLevel argument didn't return " + "TraceLevel.SEVERE");
                return;
            }

            // try to compare traceLevel with TraceLevel.HIGHER firstly, returns false.
            if (traceLevel.isHigherLevel(TraceLevel.OFF)) {
                sendResultToTCK("Test1113151Test", false, 1113151, "This isHigherLevel method didn't return "
                        + "the correct value (false) due to TraceLevel.SEVERE has "
                        + "lower value than TraceLevel.OFF.");
                return;
            }

            if (traceLevel.isHigherLevel(TraceLevel.SEVERE)) {
                sendResultToTCK("Test1113151Test", false, 1113151, "This isHigherLevel method didn't return "
                        + "the correct value (false) due to TraceLevel.SEVERE has "
                        + "the same value than TraceLevel.SEVERE.");
                return;
            }
            // try to compare traceLevel with TraceLevel.LOWER secondly, returns true.

            if (!traceLevel.isHigherLevel(TraceLevel.WARNING)) {
                sendResultToTCK("Test1113151Test", false, 1113151, "This isHigherLevel method didn't return "
                        + "the correct value (true) due to TraceLevel.SEVERE has "
                        + "higher value than TraceLevel.WARNING.");
                return;
            }

            if (!traceLevel.isHigherLevel(TraceLevel.INFO)) {
                sendResultToTCK("Test1113151Test", false, 1113151, "This isHigherLevel method didn't return "
                        + "the correct value (true) due to TraceLevel.SEVERE has "
                        + "higher value than TraceLevel.INFO.");
                return;
            }

            if (!traceLevel.isHigherLevel(TraceLevel.CONFIG)) {
                sendResultToTCK("Test1113151Test", false, 1113151, "This isHigherLevel method didn't return "
                        + "the correct value (true) due to TraceLevel.SEVERE has "
                        + "higher value than TraceLevel.CONFIG.");
                return;
            }

            if (!traceLevel.isHigherLevel(TraceLevel.FINE)) {
                sendResultToTCK("Test1113151Test", false, 1113151, "This isHigherLevel method didn't return "
                        + "the correct value (true) due to TraceLevel.SEVERE has "
                        + "higher value than TraceLevel.FINE.");
                return;
            }

            if (!traceLevel.isHigherLevel(TraceLevel.FINER)) {
                sendResultToTCK("Test1113151Test", false, 1113151, "This isHigherLevel method didn't return "
                        + "the correct value (true) due to TraceLevel.SEVERE has "
                        + "higher value than TraceLevel.FINER.");
                return;
            }

            if (!traceLevel.isHigherLevel(TraceLevel.FINEST)) {
                sendResultToTCK("Test1113151Test", false, 1113151, "This isHigherLevel method didn't return "
                        + "the correct value (true) due to TraceLevel.SEVERE has "
                        + "higher value than TraceLevel.FINEST.");
                return;
            }

            tracer.info("got expected value of the isHigherLevel method.", null);
        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }

        //1113151:WARNING
        try {
            traceLevel = TraceLevel.WARNING;
            if (traceLevel != TraceLevel.WARNING) {
                sendResultToTCK("Test1113151Test", false, 1113151, "This traceLevel argument didn't return " + "TraceLevel.WARNING");
                return;
            }

            // try to compare traceLevel with TraceLevel.HIGHER firstly, returns false.
            if (traceLevel.isHigherLevel(TraceLevel.OFF)) {
                sendResultToTCK("Test1113151Test", false, 1113151, "This isHigherLevel method didn't return "
                        + "the correct value (false) due to TraceLevel.WARNING has "
                        + "lower value than TraceLevel.OFF.");
                return;
            }

            if (traceLevel.isHigherLevel(TraceLevel.SEVERE)) {
                sendResultToTCK("Test1113151Test", false, 1113151, "This isHigherLevel method didn't return "
                        + "the correct value (false) due to TraceLevel.WARNING has "
                        + "lower value than TraceLevel.SEVERE.");
                return;
            }

            if (traceLevel.isHigherLevel(TraceLevel.WARNING)) {
                sendResultToTCK("Test1113151Test", false, 1113151, "This isHigherLevel method didn't return "
                        + "the correct value (false) due to TraceLevel.WARNING has "
                        + "the same value than TraceLevel.WARNING.");
                return;
            }
            // try to compare traceLevel with TraceLevel.LOWER secondly, returns true.

            if (!traceLevel.isHigherLevel(TraceLevel.INFO)) {
                sendResultToTCK("Test1113151Test", false, 1113151, "This isHigherLevel method didn't return "
                        + "the correct value (true) due to TraceLevel.WARNING has "
                        + "higher value than TraceLevel.INFO.");
                return;
            }

            if (!traceLevel.isHigherLevel(TraceLevel.CONFIG)) {
                sendResultToTCK("Test1113151Test", false, 1113151, "This isHigherLevel method didn't return "
                        + "the correct value (true) due to TraceLevel.WARNING has "
                        + "higher value than TraceLevel.CONFIG.");
                return;
            }

            if (!traceLevel.isHigherLevel(TraceLevel.FINE)) {
                sendResultToTCK("Test1113151Test", false, 1113151, "This isHigherLevel method didn't return "
                        + "the correct value (true) due to TraceLevel.WARNING has "
                        + "higher value than TraceLevel.FINE.");
                return;
            }

            if (!traceLevel.isHigherLevel(TraceLevel.FINER)) {
                sendResultToTCK("Test1113151Test", false, 1113151, "This isHigherLevel method didn't return "
                        + "the correct value (true) due to TraceLevel.WARNING has "
                        + "higher value than TraceLevel.FINER.");
                return;
            }

            if (!traceLevel.isHigherLevel(TraceLevel.FINEST)) {
                sendResultToTCK("Test1113151Test", false, 1113151, "This isHigherLevel method didn't return "
                        + "the correct value (true) due to TraceLevel.WARNING has "
                        + "higher value than TraceLevel.FINEST.");
                return;
            }

            tracer.info("got expected value of the isHigherLevel method.", null);
        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }

        //1113151:INFO
        try {
            traceLevel = TraceLevel.INFO;
            if (traceLevel != TraceLevel.INFO) {
                sendResultToTCK("Test1113151Test", false, 1113151, "This traceLevel argument didn't return " + "TraceLevel.INFO");
                return;
            }

            // try to compare traceLevel with TraceLevel.HIGHER firstly, returns false.
            if (traceLevel.isHigherLevel(TraceLevel.OFF)) {
                sendResultToTCK("Test1113151Test", false, 1113151, "This isHigherLevel method didn't return "
                        + "the correct value (false) due to TraceLevel.INFO has " + "lower value than TraceLevel.OFF.");
                return;
            }

            if (traceLevel.isHigherLevel(TraceLevel.SEVERE)) {
                sendResultToTCK("Test1113151Test", false, 1113151, "This isHigherLevel method didn't return "
                        + "the correct value (false) due to TraceLevel.INFO has "
                        + "lower value than TraceLevel.SEVERE.");
                return;
            }

            if (traceLevel.isHigherLevel(TraceLevel.WARNING)) {
                sendResultToTCK("Test1113151Test", false, 1113151, "This isHigherLevel method didn't return "
                        + "the correct value (false) due to TraceLevel.INFO has "
                        + "lower value than TraceLevel.WARNING.");
                return;
            }

            if (traceLevel.isHigherLevel(TraceLevel.INFO)) {
                sendResultToTCK("Test1113151Test", false, 1113151, "This isHigherLevel method didn't return "
                        + "the correct value (false) due to TraceLevel.INFO has "
                        + "the same value than TraceLevel.INFO.");
                return;
            }

            // try to compare traceLevel with TraceLevel.LOWER secondly, returns true.

            if (!traceLevel.isHigherLevel(TraceLevel.CONFIG)) {
                sendResultToTCK("Test1113151Test", false, 1113151, "This isHigherLevel method didn't return "
                        + "the correct value (true) due to TraceLevel.INFO has "
                        + "higher value than TraceLevel.CONFIG.");
                return;
            }

            if (!traceLevel.isHigherLevel(TraceLevel.FINE)) {
                sendResultToTCK("Test1113151Test", false, 1113151, "This isHigherLevel method didn't return "
                        + "the correct value (true) due to TraceLevel.INFO has " + "higher value than TraceLevel.FINE.");
                return;
            }

            if (!traceLevel.isHigherLevel(TraceLevel.FINER)) {
                sendResultToTCK("Test1113151Test", false, 1113151, "This isHigherLevel method didn't return "
                        + "the correct value (true) due to TraceLevel.INFO has "
                        + "higher value than TraceLevel.FINER.");
                return;
            }

            if (!traceLevel.isHigherLevel(TraceLevel.FINEST)) {
                sendResultToTCK("Test1113151Test", false, 1113151, "This isHigherLevel method didn't return "
                        + "the correct value (true) due to TraceLevel.INFO has "
                        + "higher value than TraceLevel.FINEST.");
                return;
            }

            tracer.info("got expected value of the isHigherLevel method.", null);
        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }

        //1113151:CONFIG
        try {
            traceLevel = TraceLevel.CONFIG;
            if (traceLevel != TraceLevel.CONFIG) {
                sendResultToTCK("Test1113151Test", false, 1113151, "This traceLevel argument didn't return " + "TraceLevel.CONFIG");
                return;
            }

            // try to compare traceLevel with TraceLevel.HIGHER firstly, returns false.
            if (traceLevel.isHigherLevel(TraceLevel.OFF)) {
                sendResultToTCK("Test1113151Test", false, 1113151, "This isHigherLevel method didn't return "
                        + "the correct value (false) due to TraceLevel.CONFIG has "
                        + "lower value than TraceLevel.OFF.");
                return;
            }

            if (traceLevel.isHigherLevel(TraceLevel.SEVERE)) {
                sendResultToTCK("Test1113151Test", false, 1113151, "This isHigherLevel method didn't return "
                        + "the correct value (false) due to TraceLevel.CONFIG has "
                        + "lower value than TraceLevel.SEVERE.");
                return;
            }

            if (traceLevel.isHigherLevel(TraceLevel.WARNING)) {
                sendResultToTCK("Test1113151Test", false, 1113151, "This isHigherLevel method didn't return "
                        + "the correct value (false) due to TraceLevel.CONFIG has "
                        + "lower value than TraceLevel.WARNING.");
                return;
            }

            if (traceLevel.isHigherLevel(TraceLevel.INFO)) {
                sendResultToTCK("Test1113151Test", false, 1113151, "This isHigherLevel method didn't return "
                        + "the correct value (false) due to TraceLevel.CONFIG has "
                        + "lower value than TraceLevel.INFO.");
                return;
            }

            if (traceLevel.isHigherLevel(TraceLevel.CONFIG)) {
                sendResultToTCK("Test1113151Test", false, 1113151, "This isHigherLevel method didn't return "
                        + "the correct value (false) due to TraceLevel.CONFIG has "
                        + "the same value than TraceLevel.CONFIG.");
                return;
            }

            // try to compare traceLevel with TraceLevel.LOWER secondly, returns true.

            if (!traceLevel.isHigherLevel(TraceLevel.FINE)) {
                sendResultToTCK("Test1113151Test", false, 1113151, "This isHigherLevel method didn't return "
                        + "the correct value (true) due to TraceLevel.CONFIG has "
                        + "higher value than TraceLevel.FINE.");
                return;
            }

            if (!traceLevel.isHigherLevel(TraceLevel.FINER)) {
                sendResultToTCK("Test1113151Test", false, 1113151, "This isHigherLevel method didn't return "
                        + "the correct value (true) due to TraceLevel.CONFIG has "
                        + "higher value than TraceLevel.FINER.");
                return;
            }

            if (!traceLevel.isHigherLevel(TraceLevel.FINEST)) {
                sendResultToTCK("Test1113151Test", false, 1113151, "This isHigherLevel method didn't return "
                        + "the correct value (true) due to TraceLevel.CONFIG has "
                        + "higher value than TraceLevel.FINEST.");
                return;
            }

            tracer.info("got expected value of the isHigherLevel method.", null);
        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }

        //1113151:FINE
        try {
            traceLevel = TraceLevel.FINE;
            if (traceLevel != TraceLevel.FINE) {
                sendResultToTCK("Test1113151Test", false, 1113151, "This traceLevel argument didn't return " + "TraceLevel.FINE");
                return;
            }

            // try to compare traceLevel with TraceLevel.HIGHER firstly, returns false.
            if (traceLevel.isHigherLevel(TraceLevel.OFF)) {
                sendResultToTCK("Test1113151Test", false, 1113151, "This isHigherLevel method didn't return "
                        + "the correct value (false) due to TraceLevel.FINE has " + "lower value than TraceLevel.OFF.");
                return;
            }

            if (traceLevel.isHigherLevel(TraceLevel.SEVERE)) {
                sendResultToTCK("Test1113151Test", false, 1113151, "This isHigherLevel method didn't return "
                        + "the correct value (false) due to TraceLevel.FINE has "
                        + "lower value than TraceLevel.SEVERE.");
                return;
            }

            if (traceLevel.isHigherLevel(TraceLevel.WARNING)) {
                sendResultToTCK("Test1113151Test", false, 1113151, "This isHigherLevel method didn't return "
                        + "the correct value (false) due to TraceLevel.FINE has "
                        + "lower value than TraceLevel.WARNING.");
                return;
            }

            if (traceLevel.isHigherLevel(TraceLevel.INFO)) {
                sendResultToTCK("Test1113151Test", false, 1113151, "This isHigherLevel method didn't return "
                        + "the correct value (false) due to TraceLevel.FINE has " + "lower value than TraceLevel.INFO.");
                return;
            }

            if (traceLevel.isHigherLevel(TraceLevel.CONFIG)) {
                sendResultToTCK("Test1113151Test", false, 1113151, "This isHigherLevel method didn't return "
                        + "the correct value (false) due to TraceLevel.FINE has "
                        + "lower value than TraceLevel.CONFIG.");
                return;
            }

            if (traceLevel.isHigherLevel(TraceLevel.FINE)) {
                sendResultToTCK("Test1113151Test", false, 1113151, "This isHigherLevel method didn't return "
                        + "the correct value (false) due to TraceLevel.FINE has "
                        + "the same value than TraceLevel.FINE.");
                return;
            }

            // try to compare traceLevel with TraceLevel.LOWER secondly, returns true.

            if (!traceLevel.isHigherLevel(TraceLevel.FINER)) {
                sendResultToTCK("Test1113151Test", false, 1113151, "This isHigherLevel method didn't return "
                        + "the correct value (true) due to TraceLevel.FINE has "
                        + "higher value than TraceLevel.FINER.");
                return;
            }

            if (!traceLevel.isHigherLevel(TraceLevel.FINEST)) {
                sendResultToTCK("Test1113151Test", false, 1113151, "This isHigherLevel method didn't return "
                        + "the correct value (true) due to TraceLevel.FINE has "
                        + "higher value than TraceLevel.FINEST.");
                return;
            }

            tracer.info("got expected value of the isHigherLevel method.", null);
        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }

        //1113151:FINER
        try {
            traceLevel = TraceLevel.FINER;
            if (traceLevel != TraceLevel.FINER) {
                sendResultToTCK("Test1113151Test", false, 1113151, "This traceLevel argument didn't return " + "TraceLevel.FINER");
                return;
            }

            // try to compare traceLevel with TraceLevel.HIGHER firstly, returns false.
            if (traceLevel.isHigherLevel(TraceLevel.OFF)) {
                sendResultToTCK("Test1113151Test", false, 1113151, "This isHigherLevel method didn't return "
                        + "the correct value (false) due to TraceLevel.FINER has " + "lower value than TraceLevel.OFF.");
                return;
            }

            if (traceLevel.isHigherLevel(TraceLevel.SEVERE)) {
                sendResultToTCK("Test1113151Test", false, 1113151, "This isHigherLevel method didn't return "
                        + "the correct value (false) due to TraceLevel.FINER has "
                        + "lower value than TraceLevel.SEVERE.");
                return;
            }

            if (traceLevel.isHigherLevel(TraceLevel.WARNING)) {
                sendResultToTCK("Test1113151Test", false, 1113151, "This isHigherLevel method didn't return "
                        + "the correct value (false) due to TraceLevel.FINER has "
                        + "lower value than TraceLevel.WARNING.");
                return;
            }

            if (traceLevel.isHigherLevel(TraceLevel.INFO)) {
                sendResultToTCK("Test1113151Test", false, 1113151, "This isHigherLevel method didn't return "
                        + "the correct value (false) due to TraceLevel.FINER has "
                        + "lower value than TraceLevel.INFO.");
                return;
            }

            if (traceLevel.isHigherLevel(TraceLevel.CONFIG)) {
                sendResultToTCK("Test1113151Test", false, 1113151, "This isHigherLevel method didn't return "
                        + "the correct value (false) due to TraceLevel.FINER has "
                        + "lower value than TraceLevel.CONFIG.");
                return;
            }

            if (traceLevel.isHigherLevel(TraceLevel.FINE)) {
                sendResultToTCK("Test1113151Test", false, 1113151, "This isHigherLevel method didn't return "
                        + "the correct value (false) due to TraceLevel.FINER has "
                        + "lower value than TraceLevel.FINE.");
                return;
            }

            if (traceLevel.isHigherLevel(TraceLevel.FINER)) {
                sendResultToTCK("Test1113151Test", false, 1113151, "This isHigherLevel method didn't return "
                        + "the correct value (false) due to TraceLevel.FINER has "
                        + "the same value than TraceLevel.FINER.");
                return;
            }

            // try to compare traceLevel with TraceLevel.LOWER secondly, returns true.

            if (!traceLevel.isHigherLevel(TraceLevel.FINEST)) {
                sendResultToTCK("Test1113151Test", false, 1113151, "This isHigherLevel method didn't return "
                        + "the correct value (true) due to TraceLevel.FINER has "
                        + "higher value than TraceLevel.FINEST.");
                return;
            }

            tracer.info("got expected value of the isHigherLevel method.", null);
        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }

        //1113151:FINEST
        try {
            traceLevel = TraceLevel.FINEST;
            if (traceLevel != TraceLevel.FINEST) {
                sendResultToTCK("Test1113151Test", false, 1113151, "This traceLevel argument didn't return " + "TraceLevel.FINEST");
                return;
            }

            // try to compare traceLevel with TraceLevel.HIGHER firstly, returns false.
            if (traceLevel.isHigherLevel(TraceLevel.OFF)) {
                sendResultToTCK("Test1113151Test", false, 1113151, "This isHigherLevel method didn't return "
                        + "the correct value (false) due to TraceLevel.FINEST has "
                        + "lower value than TraceLevel.OFF.");
                return;
            }

            if (traceLevel.isHigherLevel(TraceLevel.SEVERE)) {
                sendResultToTCK("Test1113151Test", false, 1113151, "This isHigherLevel method didn't return "
                        + "the correct value (false) due to TraceLevel.FINEST has "
                        + "lower value than TraceLevel.SEVERE.");
                return;
            }

            if (traceLevel.isHigherLevel(TraceLevel.WARNING)) {
                sendResultToTCK("Test1113151Test", false, 1113151, "This isHigherLevel method didn't return "
                        + "the correct value (false) due to TraceLevel.FINEST has "
                        + "lower value than TraceLevel.WARNING.");
                return;
            }

            if (traceLevel.isHigherLevel(TraceLevel.INFO)) {
                sendResultToTCK("Test1113151Test", false, 1113151, "This isHigherLevel method didn't return "
                        + "the correct value (false) due to TraceLevel.FINEST has "
                        + "lower value than TraceLevel.INFO.");
                return;
            }

            if (traceLevel.isHigherLevel(TraceLevel.CONFIG)) {
                sendResultToTCK("Test1113151Test", false, 1113151, "This isHigherLevel method didn't return "
                        + "the correct value (false) due to TraceLevel.FINEST has "
                        + "lower value than TraceLevel.CONFIG.");
                return;
            }

            if (traceLevel.isHigherLevel(TraceLevel.FINE)) {
                sendResultToTCK("Test1113151Test", false, 1113151, "This isHigherLevel method didn't return "
                        + "the correct value (false) due to TraceLevel.FINEST has "
                        + "lower value than TraceLevel.FINE.");
                return;
            }

            if (traceLevel.isHigherLevel(TraceLevel.FINER)) {
                sendResultToTCK("Test1113151Test", false, 1113151, "This isHigherLevel method didn't return "
                        + "the correct value (false) due to TraceLevel.FINEST has "
                        + "lower value than TraceLevel.FINER.");
                return;
            }

            if (traceLevel.isHigherLevel(TraceLevel.FINEST)) {
                sendResultToTCK("Test1113151Test", false, 1113151, "This isHigherLevel method didn't return "
                        + "the correct value (false) due to TraceLevel.FINEST has "
                        + "the same value than TraceLevel.FINEST.");
                return;
            }

            // try to compare traceLevel with TraceLevel.LOWER secondly, returns true.

            tracer.info("got expected value of the isHigherLevel method.", null);
        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }
        // reset alarmLevel back to AlarmLevel.MAJOR
        traceLevel = TraceLevel.FINE;

        //1113152
        TraceLevel offTraceLevel = TraceLevel.OFF;
        TraceLevel SevereTraceLevel = TraceLevel.SEVERE;
        try {
            if (!offTraceLevel.isHigherLevel(SevereTraceLevel)) {
                sendResultToTCK("Test1113151Test", false, 1113152, "This isHigherLevel method didn't return "
                        + "the correct value (true) due to TraceLevel.OFF has "
                        + "higher value than TraceLevel.SEVERE.");
                return;
            }

            tracer.info("got expected value of the isHigherLevel method.", null);
        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }

        //1113154
        boolean passed1113154 = false;
        try {
            tracer = getSbbContext().getTracer("com.test");
            traceLevel = tracer.getTraceLevel();
            if (traceLevel != TraceLevel.FINE) {
                sendResultToTCK("Test1113151Test", false, 1113154, "This traceLevel argument didn't return " + "TraceLevel.FINE");
                return;
            }

            traceLevel.isHigherLevel(null);
        } catch (java.lang.NullPointerException e) {
            tracer.info("got expected NullPointerException", null);
            passed1113154 = true;
        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }

        if (!passed1113154) {
            sendResultToTCK("Test1113151Test", false, 1113154,
                    "TraceLevel.isHigherLevel(null) should have thrown java.lang.NullPointerException.");
            return;
        }

        int intLevel = TraceLevel.LEVEL_OFF;
        //1113157:OFF
        // try conversion from the TraceLevel object form to numeric form.
        try {
            traceLevel = TraceLevel.fromInt(intLevel);
            if (traceLevel.toInt() != intLevel) {
                sendResultToTCK("Test1113151Test", false, 1113157, "This traceLevel.toInt method didn't return numberic TraceLevel.LEVEL_OFF "
                        + "form which specified by the TraceLevel.OFF object form.");
                return;
            }
        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }
        // try conversion from numeric form to the TraceLevel object form .
        try {
            traceLevel = TraceLevel.OFF;
            intLevel = traceLevel.toInt();
            if (!traceLevel.equals(TraceLevel.fromInt(intLevel))) {
                sendResultToTCK("Test1113151Test", false, 1113157, "This TraceLevel.fromInt method didn't return the TraceLevel.OFF "
                        + "object form which specified by numberic TraceLevel.LEVEL_OFF form.");
                return;
            }
        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }

        //1113157:SEVERE
        intLevel = TraceLevel.LEVEL_SEVERE;
        // try conversion from the TraceLevel object form to numeric form.
        try {
            traceLevel = TraceLevel.fromInt(intLevel);
            if (traceLevel.toInt() != intLevel) {
                sendResultToTCK("Test1113151Test", false, 1113157, "This traceLevel.toInt method didn't return numberic TraceLevel.LEVEL_SEVERE "
                        + "form which specified by the TraceLevel.SEVERE object form.");
                return;
            }
        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }
        // try conversion from numeric form to the TraceLevel object form .
        try {
            traceLevel = TraceLevel.SEVERE;
            intLevel = traceLevel.toInt();
            if (!traceLevel.equals(TraceLevel.fromInt(intLevel))) {
                sendResultToTCK("Test1113151Test", false, 1113157, "This TraceLevel.fromInt method didn't return the TraceLevel.SEVERE "
                        + "object form which specified by numberic TraceLevel.LEVEL_SEVERE form.");
                return;
            }
        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }

        //1113157:WARNING
        intLevel = TraceLevel.LEVEL_WARNING;
        // try conversion from the TraceLevel object form to numeric form.
        try {
            traceLevel = TraceLevel.fromInt(intLevel);
            if (traceLevel.toInt() != intLevel) {
                sendResultToTCK("Test1113151Test", false, 1113157,
                        "This traceLevel.toInt method didn't return numberic TraceLevel.LEVEL_WARNING "
                                + "form which specified by the TraceLevel.WARNING object form.");
                return;
            }
        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }
        // try conversion from numeric form to the TraceLevel object form .
        try {
            traceLevel = TraceLevel.WARNING;
            intLevel = traceLevel.toInt();
            if (!traceLevel.equals(TraceLevel.fromInt(intLevel))) {
                sendResultToTCK("Test1113151Test", false, 1113157, "This TraceLevel.fromInt method didn't return the TraceLevel.WARNING "
                        + "object form which specified by numberic TraceLevel.LEVEL_WARNING form.");
                return;
            }
        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }

        //1113157:INFO
        intLevel = TraceLevel.LEVEL_INFO;
        // try conversion from the TraceLevel object form to numeric form.
        try {
            traceLevel = TraceLevel.fromInt(intLevel);
            if (traceLevel.toInt() != intLevel) {
                sendResultToTCK("Test1113151Test", false, 1113157, "This traceLevel.toInt method didn't return numberic TraceLevel.LEVEL_INFO "
                        + "form which specified by the TraceLevel.INFO object form.");
                return;
            }
        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }
        // try conversion from numeric form to the TraceLevel object form .
        try {
            traceLevel = TraceLevel.INFO;
            intLevel = traceLevel.toInt();
            if (!traceLevel.equals(TraceLevel.fromInt(intLevel))) {
                sendResultToTCK("Test1113151Test", false, 1113157, "This TraceLevel.fromInt method didn't return the TraceLevel.INFO "
                        + "object form which specified by numberic TraceLevel.LEVEL_INFO form.");
                return;
            }
        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }

        //1113157:CONFIG
        intLevel = TraceLevel.LEVEL_CONFIG;
        // try conversion from the TraceLevel object form to numeric form.
        try {
            traceLevel = TraceLevel.fromInt(intLevel);
            if (traceLevel.toInt() != intLevel) {
                sendResultToTCK("Test1113151Test", false, 1113157, "This traceLevel.toInt method didn't return numberic TraceLevel.LEVEL_CONFIG "
                        + "form which specified by the TraceLevel.CONFIG object form.");
                return;
            }
        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }
        // try conversion from numeric form to the TraceLevel object form .
        try {
            traceLevel = TraceLevel.CONFIG;
            intLevel = traceLevel.toInt();
            if (!traceLevel.equals(TraceLevel.fromInt(intLevel))) {
                sendResultToTCK("Test1113151Test", false, 1113157, "This TraceLevel.fromInt method didn't return the TraceLevel.CONFIG "
                        + "object form which specified by numberic TraceLevel.LEVEL_CONFIG form.");
                return;
            }
        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }

        //1113157:FINE
        intLevel = TraceLevel.LEVEL_FINE;
        // try conversion from the TraceLevel object form to numeric form.
        try {
            traceLevel = TraceLevel.fromInt(intLevel);
            if (traceLevel.toInt() != intLevel) {
                sendResultToTCK("Test1113151Test", false, 1113157, "This traceLevel.toInt method didn't return numberic TraceLevel.LEVEL_FINE "
                        + "form which specified by the TraceLevel.FINE object form.");
                return;
            }
        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }
        // try conversion from numeric form to the TraceLevel object form .
        try {
            traceLevel = TraceLevel.FINE;
            intLevel = traceLevel.toInt();
            if (!traceLevel.equals(TraceLevel.fromInt(intLevel))) {
                sendResultToTCK("Test1113151Test", false, 1113157, "This TraceLevel.fromInt method didn't return the TraceLevel.FINE "
                        + "object form which specified by numberic TraceLevel.LEVEL_FINE form.");
                return;
            }
        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }

        //1113157:FINER
        intLevel = TraceLevel.LEVEL_FINER;
        // try conversion from the TraceLevel object form to numeric form.
        try {
            traceLevel = TraceLevel.fromInt(intLevel);
            if (traceLevel.toInt() != intLevel) {
                sendResultToTCK("Test1113151Test", false, 1113157, "This traceLevel.toInt method didn't return numberic TraceLevel.LEVEL_FINER "
                        + "form which specified by the TraceLevel.FINER object form.");
                return;
            }
        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }
        // try conversion from numeric form to the TraceLevel object form .
        try {
            traceLevel = TraceLevel.FINER;
            intLevel = traceLevel.toInt();
            if (!traceLevel.equals(TraceLevel.fromInt(intLevel))) {
                sendResultToTCK("Test1113151Test", false, 1113157, "This TraceLevel.fromInt method didn't return the TraceLevel.FINER "
                        + "object form which specified by numberic TraceLevel.LEVEL_FINER form.");
                return;
            }
        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }

        //1113157:FINEST
        intLevel = TraceLevel.LEVEL_FINEST;
        // try conversion from the TraceLevel object form to numeric form.
        try {
            traceLevel = TraceLevel.fromInt(intLevel);
            if (traceLevel.toInt() != intLevel) {
                sendResultToTCK("Test1113151Test", false, 1113157, "This traceLevel.toInt method didn't return numberic TraceLevel.LEVEL_FINEST "
                        + "form which specified by the TraceLevel.FINEST object form.");
                return;
            }
        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }
        // try conversion from numeric form to the TraceLevel object form .
        try {
            traceLevel = TraceLevel.FINEST;
            intLevel = traceLevel.toInt();
            if (!traceLevel.equals(TraceLevel.fromInt(intLevel))) {
                sendResultToTCK("Test1113151Test", false, 1113157, "This TraceLevel.fromInt method didn't return the TraceLevel.FINEST "
                        + "object form which specified by numberic TraceLevel.LEVEL_FINEST form.");
                return;
            }
        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }

        //1113158
        int minusLevel = -1;
        boolean passed1113158 = false;
        try {
            traceLevel = TraceLevel.fromInt(minusLevel);
        } catch (java.lang.IllegalArgumentException iae) {
            tracer.info("got expected IllegalArgumentException with a negative value for the level argument.", null);
            passed1113158 = true;
        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }

        if (!passed1113158) {
            sendResultToTCK("Test1113151Test", false, 1113158, "TraceLevel.fromInt(-1) should have thrown java.lang.IllegalArgumentException.");
            return;
        }

        int invalidLevel = 8;
        passed1113158 = false;
        try {
            traceLevel = TraceLevel.fromInt(invalidLevel);
        } catch (java.lang.IllegalArgumentException iae) {
            tracer.info("got expected IllegalArgumentException with an invalid value for the level argument.", null);
            passed1113158 = true;
        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }

        if (!passed1113158) {
            sendResultToTCK("Test1113151Test", false, 1113158, "TraceLevel.fromInt(8) should have thrown java.lang.IllegalArgumentException.");
            return;
        }

        String strLevel, strlcLevel;

        //1113159:FINEST
        strLevel = TraceLevel.FINEST_STRING;
        strlcLevel = TraceLevel.FINEST_STRING.toLowerCase();
        // try conversion from the TraceLevel object form to a string value (upper case).
        try {
            traceLevel = TraceLevel.fromString(strLevel);
            if (traceLevel.toString() != strLevel) {
                sendResultToTCK("Test1113151Test", false, 1113159, "This traceLevel.toString method didn't return a string value (upper case)"
                        + "which specified by the TraceLevel object form.");
                return;
            }
        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }

        // try conversion from the TraceLevel object form to a string value (lower case).
        try {
            traceLevel = TraceLevel.fromString(strlcLevel);
            if (!traceLevel.toString().equalsIgnoreCase(strlcLevel)) {
                sendResultToTCK("Test1113151Test", false, 1113159, "This traceLevel.toString method didn't return a string value (lower case)"
                        + "which specified by the TraceLevel object form.");
                return;
            }
        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }

        // try conversion from a string value (upper case) to the TraceLevel object form .
        try {
            tracer = getSbbContext().getTracer("com.test");
            traceLevel = tracer.getTraceLevel();
            strLevel = traceLevel.toString();
            if (!traceLevel.equals(TraceLevel.fromString(strLevel))) {
                sendResultToTCK("Test1113151Test", false, 1113159, "This TraceLevel.fromString method didn't return the TraceLevel "
                        + "object form which specified by a string value (upper case).");
                return;
            }
        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }

        // try conversion from a string value (lower case) to the TraceLevel object form .
        try {
            tracer = getSbbContext().getTracer("com.test");
            traceLevel = tracer.getTraceLevel();
            strlcLevel = traceLevel.toString().toLowerCase();
            if (!traceLevel.equals(TraceLevel.fromString(strlcLevel))) {
                sendResultToTCK("Test1113151Test", false, 1113159,
                        "This TraceLevel.fromString(lowercaseLevel) method didn't return the TraceLevel "
                                + "object form which specified by a string value (lower case).");
                return;
            }
        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }

        //1113159:FINER
        strLevel = TraceLevel.FINER_STRING;
        strlcLevel = TraceLevel.FINER_STRING.toLowerCase();
        // try conversion from the TraceLevel object form to a string value (upper case).
        try {
            traceLevel = TraceLevel.fromString(strLevel);
            if (traceLevel.toString() != strLevel) {
                sendResultToTCK("Test1113151Test", false, 1113159, "This traceLevel.toString method didn't return a string value (upper case)"
                        + "which specified by the TraceLevel object form.");
                return;
            }
        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }

        // try conversion from the TraceLevel object form to a string value (lower case).
        try {
            traceLevel = TraceLevel.fromString(strlcLevel);
            if (!traceLevel.toString().equalsIgnoreCase(strlcLevel)) {
                sendResultToTCK("Test1113151Test", false, 1113159, "This traceLevel.toString method didn't return a string value (lower case)"
                        + "which specified by the TraceLevel object form.");
                return;
            }
        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }

        // try conversion from a string value (upper case) to the TraceLevel object form .
        try {
            tracer = getSbbContext().getTracer("com.test");
            traceLevel = tracer.getTraceLevel();
            strLevel = traceLevel.toString();
            if (!traceLevel.equals(TraceLevel.fromString(strLevel))) {
                sendResultToTCK("Test1113151Test", false, 1113159, "This TraceLevel.fromString method didn't return the TraceLevel "
                        + "object form which specified by a string value (upper case).");
                return;
            }
        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }

        // try conversion from a string value (lower case) to the TraceLevel object form .
        try {
            tracer = getSbbContext().getTracer("com.test");
            traceLevel = tracer.getTraceLevel();
            strlcLevel = traceLevel.toString().toLowerCase();
            if (!traceLevel.equals(TraceLevel.fromString(strlcLevel))) {
                sendResultToTCK("Test1113151Test", false, 1113159,
                        "This TraceLevel.fromString(lowercaseLevel) method didn't return the TraceLevel "
                                + "object form which specified by a string value (lower case).");
                return;
            }
        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }

        //1113159:FINE
        strLevel = TraceLevel.FINE_STRING;
        strlcLevel = TraceLevel.FINE_STRING.toLowerCase();
        // try conversion from the TraceLevel object form to a string value (upper case).
        try {
            traceLevel = TraceLevel.fromString(strLevel);
            if (traceLevel.toString() != strLevel) {
                sendResultToTCK("Test1113151Test", false, 1113159, "This traceLevel.toString method didn't return a string value (upper case)"
                        + "which specified by the TraceLevel object form.");
                return;
            }
        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }

        // try conversion from the TraceLevel object form to a string value (lower case).
        try {
            traceLevel = TraceLevel.fromString(strlcLevel);
            if (!traceLevel.toString().equalsIgnoreCase(strlcLevel)) {
                sendResultToTCK("Test1113151Test", false, 1113159, "This traceLevel.toString method didn't return a string value (lower case)"
                        + "which specified by the TraceLevel object form.");
                return;
            }
        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }

        // try conversion from a string value (upper case) to the TraceLevel object form .
        try {
            tracer = getSbbContext().getTracer("com.test");
            traceLevel = tracer.getTraceLevel();
            strLevel = traceLevel.toString();
            if (!traceLevel.equals(TraceLevel.fromString(strLevel))) {
                sendResultToTCK("Test1113151Test", false, 1113159, "This TraceLevel.fromString method didn't return the TraceLevel "
                        + "object form which specified by a string value (upper case).");
                return;
            }
        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }

        // try conversion from a string value (lower case) to the TraceLevel object form .
        try {
            tracer = getSbbContext().getTracer("com.test");
            traceLevel = tracer.getTraceLevel();
            strlcLevel = traceLevel.toString().toLowerCase();
            if (!traceLevel.equals(TraceLevel.fromString(strlcLevel))) {
                sendResultToTCK("Test1113151Test", false, 1113159,
                        "This TraceLevel.fromString(lowercaseLevel) method didn't return the TraceLevel "
                                + "object form which specified by a string value (lower case).");
                return;
            }
        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }

        //1113159:CONFIG
        strLevel = TraceLevel.CONFIG_STRING;
        strlcLevel = TraceLevel.CONFIG_STRING.toLowerCase();
        // try conversion from the TraceLevel object form to a string value (upper case).
        try {
            traceLevel = TraceLevel.fromString(strLevel);
            if (traceLevel.toString() != strLevel) {
                sendResultToTCK("Test1113151Test", false, 1113159, "This traceLevel.toString method didn't return a string value (upper case)"
                        + "which specified by the TraceLevel object form.");
                return;
            }
        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }

        // try conversion from the TraceLevel object form to a string value (lower case).
        try {
            traceLevel = TraceLevel.fromString(strlcLevel);
            if (!traceLevel.toString().equalsIgnoreCase(strlcLevel)) {
                sendResultToTCK("Test1113151Test", false, 1113159, "This traceLevel.toString method didn't return a string value (lower case)"
                        + "which specified by the TraceLevel object form.");
                return;
            }
        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }

        // try conversion from a string value (upper case) to the TraceLevel object form .
        try {
            tracer = getSbbContext().getTracer("com.test");
            traceLevel = tracer.getTraceLevel();
            strLevel = traceLevel.toString();
            if (!traceLevel.equals(TraceLevel.fromString(strLevel))) {
                sendResultToTCK("Test1113151Test", false, 1113159, "This TraceLevel.fromString method didn't return the TraceLevel "
                        + "object form which specified by a string value (upper case).");
                return;
            }
        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }

        // try conversion from a string value (lower case) to the TraceLevel object form .
        try {
            tracer = getSbbContext().getTracer("com.test");
            traceLevel = tracer.getTraceLevel();
            strlcLevel = traceLevel.toString().toLowerCase();
            if (!traceLevel.equals(TraceLevel.fromString(strlcLevel))) {
                sendResultToTCK("Test1113151Test", false, 1113159,
                        "This TraceLevel.fromString(lowercaseLevel) method didn't return the TraceLevel "
                                + "object form which specified by a string value (lower case).");
                return;
            }
        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }

        //1113159:INFO
        strLevel = TraceLevel.INFO_STRING;
        strlcLevel = TraceLevel.INFO_STRING.toLowerCase();
        // try conversion from the TraceLevel object form to a string value (upper case).
        try {
            traceLevel = TraceLevel.fromString(strLevel);
            if (traceLevel.toString() != strLevel) {
                sendResultToTCK("Test1113151Test", false, 1113159, "This traceLevel.toString method didn't return a string value (upper case)"
                        + "which specified by the TraceLevel object form.");
                return;
            }
        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }

        // try conversion from the TraceLevel object form to a string value (lower case).
        try {
            traceLevel = TraceLevel.fromString(strlcLevel);
            if (!traceLevel.toString().equalsIgnoreCase(strlcLevel)) {
                sendResultToTCK("Test1113151Test", false, 1113159, "This traceLevel.toString method didn't return a string value (lower case)"
                        + "which specified by the TraceLevel object form.");
                return;
            }
        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }

        // try conversion from a string value (upper case) to the TraceLevel object form .
        try {
            tracer = getSbbContext().getTracer("com.test");
            traceLevel = tracer.getTraceLevel();
            strLevel = traceLevel.toString();
            if (!traceLevel.equals(TraceLevel.fromString(strLevel))) {
                sendResultToTCK("Test1113151Test", false, 1113159, "This TraceLevel.fromString method didn't return the TraceLevel "
                        + "object form which specified by a string value (upper case).");
                return;
            }
        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }

        // try conversion from a string value (lower case) to the TraceLevel object form .
        try {
            tracer = getSbbContext().getTracer("com.test");
            traceLevel = tracer.getTraceLevel();
            strlcLevel = traceLevel.toString().toLowerCase();
            if (!traceLevel.equals(TraceLevel.fromString(strlcLevel))) {
                sendResultToTCK("Test1113151Test", false, 1113159,
                        "This TraceLevel.fromString(lowercaseLevel) method didn't return the TraceLevel "
                                + "object form which specified by a string value (lower case).");
                return;
            }
        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }

        //1113159:WARNING
        strLevel = TraceLevel.WARNING_STRING;
        strlcLevel = TraceLevel.WARNING_STRING.toLowerCase();
        // try conversion from the TraceLevel object form to a string value (upper case).
        try {
            traceLevel = TraceLevel.fromString(strLevel);
            if (traceLevel.toString() != strLevel) {
                sendResultToTCK("Test1113151Test", false, 1113159, "This traceLevel.toString method didn't return a string value (upper case)"
                        + "which specified by the TraceLevel object form.");
                return;
            }
        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }

        // try conversion from the TraceLevel object form to a string value (lower case).
        try {
            traceLevel = TraceLevel.fromString(strlcLevel);
            if (!traceLevel.toString().equalsIgnoreCase(strlcLevel)) {
                sendResultToTCK("Test1113151Test", false, 1113159, "This traceLevel.toString method didn't return a string value (lower case)"
                        + "which specified by the TraceLevel object form.");
                return;
            }
        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }

        // try conversion from a string value (upper case) to the TraceLevel object form .
        try {
            tracer = getSbbContext().getTracer("com.test");
            traceLevel = tracer.getTraceLevel();
            strLevel = traceLevel.toString();
            if (!traceLevel.equals(TraceLevel.fromString(strLevel))) {
                sendResultToTCK("Test1113151Test", false, 1113159, "This TraceLevel.fromString method didn't return the TraceLevel "
                        + "object form which specified by a string value (upper case).");
                return;
            }
        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }

        // try conversion from a string value (lower case) to the TraceLevel object form .
        try {
            tracer = getSbbContext().getTracer("com.test");
            traceLevel = tracer.getTraceLevel();
            strlcLevel = traceLevel.toString().toLowerCase();
            if (!traceLevel.equals(TraceLevel.fromString(strlcLevel))) {
                sendResultToTCK("Test1113151Test", false, 1113159,
                        "This TraceLevel.fromString(lowercaseLevel) method didn't return the TraceLevel "
                                + "object form which specified by a string value (lower case).");
                return;
            }
        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }

        //1113159:SEVERE
        strLevel = TraceLevel.SEVERE_STRING;
        strlcLevel = TraceLevel.SEVERE_STRING.toLowerCase();
        // try conversion from the TraceLevel object form to a string value (upper case).
        try {
            traceLevel = TraceLevel.fromString(strLevel);
            if (traceLevel.toString() != strLevel) {
                sendResultToTCK("Test1113151Test", false, 1113159, "This traceLevel.toString method didn't return a string value (upper case)"
                        + "which specified by the TraceLevel object form.");
                return;
            }
        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }

        // try conversion from the TraceLevel object form to a string value (lower case).
        try {
            traceLevel = TraceLevel.fromString(strlcLevel);
            if (!traceLevel.toString().equalsIgnoreCase(strlcLevel)) {
                sendResultToTCK("Test1113151Test", false, 1113159, "This traceLevel.toString method didn't return a string value (lower case)"
                        + "which specified by the TraceLevel object form.");
                return;
            }
        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }

        // try conversion from a string value (upper case) to the TraceLevel object form .
        try {
            tracer = getSbbContext().getTracer("com.test");
            traceLevel = tracer.getTraceLevel();
            strLevel = traceLevel.toString();
            if (!traceLevel.equals(TraceLevel.fromString(strLevel))) {
                sendResultToTCK("Test1113151Test", false, 1113159, "This TraceLevel.fromString method didn't return the TraceLevel "
                        + "object form which specified by a string value (upper case).");
                return;
            }
        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }

        // try conversion from a string value (lower case) to the TraceLevel object form .
        try {
            tracer = getSbbContext().getTracer("com.test");
            traceLevel = tracer.getTraceLevel();
            strlcLevel = traceLevel.toString().toLowerCase();
            if (!traceLevel.equals(TraceLevel.fromString(strlcLevel))) {
                sendResultToTCK("Test1113151Test", false, 1113159,
                        "This TraceLevel.fromString(lowercaseLevel) method didn't return the TraceLevel "
                                + "object form which specified by a string value (lower case).");
                return;
            }
        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }

        //1113159:OFF
        strLevel = TraceLevel.OFF_STRING;
        strlcLevel = TraceLevel.OFF_STRING.toLowerCase();
        // try conversion from the TraceLevel object form to a string value (upper case).
        try {
            traceLevel = TraceLevel.fromString(strLevel);
            if (traceLevel.toString() != strLevel) {
                sendResultToTCK("Test1113151Test", false, 1113159, "This traceLevel.toString method didn't return a string value (upper case)"
                        + "which specified by the TraceLevel object form.");
                return;
            }
        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }

        // try conversion from the TraceLevel object form to a string value (lower case).
        try {
            traceLevel = TraceLevel.fromString(strlcLevel);
            if (!traceLevel.toString().equalsIgnoreCase(strlcLevel)) {
                sendResultToTCK("Test1113151Test", false, 1113159, "This traceLevel.toString method didn't return a string value (lower case)"
                        + "which specified by the TraceLevel object form.");
                return;
            }
        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }

        // try conversion from a string value (upper case) to the TraceLevel object form .
        try {
            tracer = getSbbContext().getTracer("com.test");
            traceLevel = tracer.getTraceLevel();
            strLevel = traceLevel.toString();
            if (!traceLevel.equals(TraceLevel.fromString(strLevel))) {
                sendResultToTCK("Test1113151Test", false, 1113159, "This TraceLevel.fromString method didn't return the TraceLevel "
                        + "object form which specified by a string value (upper case).");
                return;
            }
        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }

        // try conversion from a string value (lower case) to the TraceLevel object form .
        try {
            tracer = getSbbContext().getTracer("com.test");
            traceLevel = tracer.getTraceLevel();
            strlcLevel = traceLevel.toString().toLowerCase();
            if (!traceLevel.equals(TraceLevel.fromString(strlcLevel))) {
                sendResultToTCK("Test1113151Test", false, 1113159,
                        "This TraceLevel.fromString(lowercaseLevel) method didn't return the TraceLevel "
                                + "object form which specified by a string value (lower case).");
                return;
            }
        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }

        //1113160
        boolean passed1113160 = false;
        try {
            traceLevel = TraceLevel.fromString(null);
        } catch (java.lang.NullPointerException e) {
            tracer.info("got expected NullPointerException with null value for the level argument.", null);
            passed1113160 = true;
        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }

        if (!passed1113160) {
            sendResultToTCK("Test1113151Test", false, 1113160, "TraceLevel.fromString(null) should have thrown java.lang.NullPointerException.");
            return;
        }

        //1113161
        // try this invalid trace level string "FINES", should catch IllegalArgumentException
        String invalidStrLevel = TraceLevel.FINE_STRING.concat("S");
        boolean passed1113161 = false;
        try {
            traceLevel = TraceLevel.fromString(invalidStrLevel);
        } catch (java.lang.IllegalArgumentException iae) {
            tracer
                    .info(
                            "got expected IllegalArgumentException with an invalid value (upper case) for the level string argument.",
                            null);
            passed1113161 = true;
        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }

        if (!passed1113161) {
            sendResultToTCK("Test1113151Test", false, 1113161,
                    "TraceLevel.fromString(FINES) should have thrown java.lang.IllegalArgumentException.");
            return;
        }

        String lowercaseLevel = TraceLevel.FINE_STRING.toLowerCase().concat("s");
        passed1113161 = false;
        try {
            traceLevel = TraceLevel.fromString(lowercaseLevel);
        } catch (java.lang.IllegalArgumentException iae) {
            tracer
                    .info(
                            "got expected IllegalArgumentException with an invalid value (lower case) for the level string argument.",
                            null);
            passed1113161 = true;
        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }

        if (!passed1113161) {
            sendResultToTCK("Test1113151Test", false, 1113161,
                    "TraceLevel.fromString(fines) should have thrown java.lang.IllegalArgumentException.");
            return;
        }

        sendResultToTCK("Test1113151Test", true, 1113151, "This TraceLevel.isHigherLevel tests passed!");
    }
    
    private Tracer tracer = null;
}
