/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.events.eventcontext;

import javax.slee.ActivityContextInterface;
import javax.slee.InitialEventSelector;
import javax.slee.EventContext;
import javax.slee.facilities.Tracer;

import com.opencloud.sleetck.lib.sbbutils.BaseTCKSbb;
import com.opencloud.sleetck.lib.sbbutils.TCKSbbUtils;
import com.opencloud.sleetck.lib.resource.TCKActivityID;
import com.opencloud.sleetck.lib.resource.events.TCKResourceEventX;
import com.opencloud.sleetck.lib.resource.sbbapi.TCKActivity;

import java.util.HashMap;

/**
 * AssertionID(1108043): Test If the transaction does not commit then the Event
 * Context will not be “resumed” and therefore the event will not be delivered
 * to any other eligible SBB entities.
 */
public abstract class Test1108043Sbb2 extends BaseTCKSbb {
    public static final String TRACE_MESSAGE_TCKResourceEventX1 = "Test1108043Sbb2:I got TCKResourceEventX1 on ActivityA";

    public static final String TRACE_MESSAGE_TCKResourceEventX2A = "Test1108043Sbb2:I got TCKResourceEventX2 on ActivityA";
    public static final String TRACE_MESSAGE_TCKResourceEventX2B = "Test1108043Sbb2:I got TCKResourceEventX2 on ActivityB";

    public static final String TRACE_MESSAGE_TCKResourceEventX3 = "Test1108043Sbb2:I got TCKResourceEventX3 on ActivityB";

    public InitialEventSelector initialEventSelector(InitialEventSelector ies) {
        ies.setCustomName("test");
        return ies;
    }

    public void onTCKResourceEventX1(TCKResourceEventX event, ActivityContextInterface aci, EventContext context) {
        try {
            // send message to TCK: I got TCKResourceEventX1 on ActivityA
            tracer = getSbbContext().getTracer("com.test");
            tracer.info(TRACE_MESSAGE_TCKResourceEventX1);
            setPassedX1(true);
            if (!getPassed())
            sendResultToTCK("Test1108043Test", false, "SBB2:onTCKResourceEventX1-ERROR: When the transaction does not commit in the first SBB, then the "
                            + "Event Context will not be resumed, and the event TCKResourceEventX1 on ActivityA "
                            + "should not been delivered to the second SBB.", 1108043);
            return;

        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }
    }

    public void onTCKResourceEventX2(TCKResourceEventX event, ActivityContextInterface aci, EventContext context) {
        try {
            // send message to TCK: I got TCKResourceEventX1 on ActivityA
            tracer = getSbbContext().getTracer("com.test");

            TCKActivity activity = (TCKActivity) context.getActivityContextInterface().getActivity();
            TCKActivityID activityID = activity.getID();
            if (activityID.getName().equals(Test1108043Test.activityNameA)) {
                tracer.info(TRACE_MESSAGE_TCKResourceEventX2A);
                setPassedX2(true);
                if (!getPassed())
                sendResultToTCK("Test1108043Test", false, "SBB2:onTCKResourceEventX2-ERROR: When the transaction does not commit in the first SBB, then the "
                                + "Event Context will not be resumed, and the event TCKResourceEventX2 on ActivityA "
                                + "should not been delivered to the second SBB.", 1108043);
                return;
            }
            else if (activityID.getName().equals(Test1108043Test.activityNameB)) {
                tracer.info(TRACE_MESSAGE_TCKResourceEventX2B);
                setPassed(true);
            }

        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }
    }

    public void onTCKResourceEventX3(TCKResourceEventX event, ActivityContextInterface aci, EventContext context) {
        try {
            // send message to TCK: I got TCKResourceEventX3 on ActivityB
            tracer = getSbbContext().getTracer("com.test");
            tracer.info(TRACE_MESSAGE_TCKResourceEventX3);

            if (!getPassedX1() && !getPassedX2()) {
                sendResultToTCK("Test1108043Test", true, "If the transaction does not commit then the Event Context will not be resumed "
                        + "and therefore the event will not be delivered to any other eligible SBB entities.", 1108043);

            } else {
                sendResultToTCK("Test1108043Test", false, "SBB2:onTCKResourceEventX3-ERROR: When the transaction does not commit in the first SBB, then the "
                                + "Event Context will not be resumed, and the event should not been delivered to the "
                                + "second SBB.", 1108043);
            }
            return;
        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }
    }

    private void sendResultToTCK(String testName, boolean result, String message, int failedAssertionID) throws Exception {
        HashMap sbbData = new HashMap();
        sbbData.put("testname", testName);
        sbbData.put("result", result ? "pass" : "fail");
        sbbData.put("message", message);
        sbbData.put("id", new Integer(failedAssertionID));
        TCKSbbUtils.getResourceInterface().sendSbbMessage(sbbData);
    }

    private Tracer tracer = null;

    public abstract void setPassedX1(boolean passedX1);
    public abstract boolean getPassedX1();
    
    public abstract void setPassedX2(boolean passedX2);
    public abstract boolean getPassedX2();
    
    public abstract void setPassed(boolean passed);
    public abstract boolean getPassed();
}
