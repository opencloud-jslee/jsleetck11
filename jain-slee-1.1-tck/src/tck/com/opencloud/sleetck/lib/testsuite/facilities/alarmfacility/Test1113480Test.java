/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.facilities.alarmfacility;

import java.rmi.RemoteException;
import java.util.Map;

import javax.management.Notification;
import javax.management.NotificationListener;
import javax.slee.management.AlarmNotification;
import javax.slee.management.DeployableUnitID;

import com.opencloud.sleetck.lib.AbstractSleeTCKTest;
import com.opencloud.sleetck.lib.DescriptionKeys;
import com.opencloud.sleetck.lib.TCKTestFailureException;
import com.opencloud.sleetck.lib.resource.TCKActivityID;
import com.opencloud.sleetck.lib.resource.TCKSbbMessage;
import com.opencloud.sleetck.lib.resource.events.TCKResourceEventX;
import com.opencloud.sleetck.lib.resource.testapi.TCKResourceListener;
import com.opencloud.sleetck.lib.resource.testapi.TCKResourceTestInterface;
import com.opencloud.sleetck.lib.testutils.Assert;
import com.opencloud.sleetck.lib.testutils.BaseTCKResourceListener;
import com.opencloud.sleetck.lib.testutils.FutureResult;
import com.opencloud.sleetck.lib.testutils.jmx.AlarmMBeanProxy;

/*  
 *  AssertionID(1113480): Test The alarmType argument. This 
 *  argument specifies the type of alarm being generated. 
 *  It will be included in the AlarmNotification object emitted 
 *  by the AlarmMBean object for the alarm notification.
 *    
 *  AssertionID(1113482): Test the instanceID argument. This 
 *  argument specifies the particular instance of the alarm 
 *  type that is occuring.
 *  
 *  AssertionID(1113483): Test The alarmlevel argument. This 
 *  argument takes an AlarmLevel object that specifies the 
 *  alarm level of the alarm notification.
 *  
 *  AssertionID(1113485): Test The message argument. This 
 *  argument specifies the message that will be placed into 
 *  the message attribute of the AlarmNotification object 
 *  emitted by the AlarmMBean object for the alarm notification 
 *  if the alarm is raised as a result of this method.
 *  
 */

public class Test1113480Test extends AbstractSleeTCKTest {

    /**
     * Perform the actual test.
     */
    public void run(FutureResult result) throws Exception {
        this.result = result;

        String activityName = "Test1113480Test";
        TCKResourceTestInterface resource = utils().getResourceInterface();
        TCKActivityID activityID = resource.createActivity(activityName);
        getLog().info("Firing event: " + TCKResourceEventX.X1);

        // kickoff the test
        resource.fireEvent(TCKResourceEventX.X1, testName, activityID, null);
    }

    /**
     * Do all the pre-run configuration of the test.
     */

    public void setUp() throws Exception {

        getLog().fine("Connecting to resource");
        resourceListener = new TCKResourceListenerImpl();
        setResourceListener(resourceListener);
        getLog().fine("Installing and activating service");

        // Install the Deployable Units
        String duPath = utils().getTestParams().getProperty(DescriptionKeys.SERVICE_DU_PATH_PARAM);
        duID = utils().install(duPath);

        // Activate the DU
        utils().activateServices(duID, true);

        alarmMBeanProxy = utils().getAlarmMBeanProxy();
        listener = new AlarmNotificationListenerImpl();
        alarmMBeanProxy.addNotificationListener(listener, null, null);
    }

    /**
     * Clean up after the test.
     */

    public void tearDown() throws Exception {

        String[] listActiveAlarms = alarmMBeanProxy.getAlarms();
        if (alarmMBeanProxy.getAlarms().length > 0)
            for (int i = 0; i < listActiveAlarms.length; i++)
                alarmMBeanProxy.clearAlarm(listActiveAlarms[i]);

        if (null != alarmMBeanProxy)
            alarmMBeanProxy.removeNotificationListener(listener);
        // cleanup
        super.tearDown();
    }

    private class TCKResourceListenerImpl extends BaseTCKResourceListener {
        // TCKResourceListener methods

        public synchronized void onSbbMessage(TCKSbbMessage message, TCKActivityID calledActivity)
                throws RemoteException {

            Map sbbData = (Map) message.getMessage();
            String sbbTestName = "Test1113480Test";
            String sbbTestResult = (String) sbbData.get("result");
            String sbbTestMessage = (String) sbbData.get("message");
            int assertionID = ((Integer) sbbData.get("id")).intValue();
            getLog().info(
                    "Received message from SBB: testname=" + sbbTestName + ", result=" + sbbTestResult + ", message="
                            + sbbTestMessage + ", id=" + assertionID);
            try {
                Assert.assertEquals(assertionID, "Test " + sbbTestName + " failed.", "pass", sbbTestResult);
                result.setPassed();
            } catch (TCKTestFailureException ex) {
                result.setFailed(assertionID, sbbTestMessage);
            }
        }

        public void onException(Exception exception) throws RemoteException {
            getLog().warning("Received Exception from SBB or resource:");
            getLog().warning(exception);
            result.setError(exception);
        }
    }

    public class AlarmNotificationListenerImpl implements NotificationListener {
        public final void handleNotification(Notification notification, Object handback) {

            if (notification instanceof AlarmNotification) {
                AlarmNotification alarmNotification = (AlarmNotification) notification;

                // 1113480
                if (!alarmNotification.getAlarmType().equals(Test1113480Sbb.ALARM_TYPE)) {
                    result.setFailed(1113480, "AlarmNotification.getAlarmType() didn't return the type of alarm being created in sbb or service.");
                    return;
                }

                // 1113482
                if (!alarmNotification.getInstanceID().equals(Test1113480Sbb.ALARM_INSTANCEID)) {
                    result.setFailed(1113482, "AlarmNotification.getInstanceID didn't return the particular instance of the alarm type that is occuring in sbb or service.");
                    return;
                }

                // 1113483
                if (!alarmNotification.getAlarmLevel().equals(Test1113480Sbb.ALARM_LEVEL_MAJOR)) {
                    result.setFailed(1113483, "AlarmNotification.getAlarmLevel() didn't return the alarm level of the alarm notification that is occuring in sbb or service.");
                    return;
                }

                // 1113485
                if (!alarmNotification.getMessage().equals(Test1113480Sbb.ALARM_MESSAGE)) {
                    result.setFailed(1113485, "When an alarm is raised, an alarm notification didn't emit by the AlarmMbean.");
                    return;
                }

                result.setPassed();
                return;
            }

            getLog().info("Notification received: " + notification);
            return;
        }
    }

    private TCKResourceListener resourceListener;

    private NotificationListener listener;

    private DeployableUnitID duID;

    private AlarmMBeanProxy alarmMBeanProxy;

    private FutureResult result;
    
    private String testName = "Test1113480";
}
