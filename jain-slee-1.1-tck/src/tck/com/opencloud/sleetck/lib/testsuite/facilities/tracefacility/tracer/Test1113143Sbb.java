/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.facilities.tracefacility.tracer;

import java.util.HashMap;

import javax.slee.ActivityContextInterface;
import javax.slee.facilities.TraceLevel;
import javax.slee.facilities.Tracer;

import com.opencloud.sleetck.lib.resource.events.TCKResourceEventX;
import com.opencloud.sleetck.lib.sbbutils.BaseTCKSbb;
import com.opencloud.sleetck.lib.sbbutils.TCKSbbUtils;

/*
 * AssertionID (1113143): Test The trace method throws a 
 * java.lang.NullPointerException if any of the arguments, except cause, 
 * are null.
 * 
 * AssertionID (1113144): Test It throws a java.lang.IllegalArgumentException 
 * if the specified trace level is TraceLevel.OFF.
 *
 */
public abstract class Test1113143Sbb extends BaseTCKSbb {
    public static final String TRACE_MESSAGE = "Test1113143TraceMessage";

    // Initial event
    public void onTCKResourceEventX1(TCKResourceEventX event, ActivityContextInterface aci) {

        try {
            sbbTracer = getSbbContext().getTracer("com.test");
            sbbTracer.info("Received " + event + " message", null);

            doTest1113143Test();
        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }
    }

    private void sendResultToTCK(String testName, boolean result, int failedAssertionID,  String message) throws Exception {
        HashMap sbbData = new HashMap();
        sbbData.put("testname", testName);
        sbbData.put("result", result ? "pass" : "fail");
        sbbData.put("message", message);
        sbbData.put("id", new Integer(failedAssertionID));
        TCKSbbUtils.getResourceInterface().sendSbbMessage(sbbData);
    }

    private void doTest1113143Test() throws Exception {
        sbbTracer.info("Testing Assertion Number 1113143...");
        
        Tracer tracer = null;
        
        boolean passed = false;

        //1113143
        try {
            tracer = getSbbContext().getTracer("");
            // The trace method throws a java.lang.NullPointerException if TraceLevel is null.
            tracer.trace(null, TRACE_MESSAGE);
        } catch (java.lang.NullPointerException e) {
            sbbTracer.info("got expected NullPointerException", null);
            passed = true;
        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }

        if (!passed) {
            sendResultToTCK("Test1113143Test", false, 1113143, "Tracer.trace(null, message) should have thrown java.lang.NullPointerException.");
            return;
        }

        try {
            tracer = getSbbContext().getTracer("");
            // The trace method throws a java.lang.NullPointerException if TraceLevel is null.
            tracer.trace(null, TRACE_MESSAGE, null);
        } catch (java.lang.NullPointerException e) {
            sbbTracer.info("got expected NullPointerException", null);
            passed = true;
        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }

        if (!passed) {
            sendResultToTCK("Test1113143Test", false, 1113143,
                    "Tracer.trace(null, message, null) should have thrown java.lang.NullPointerException.");
            return;
        }

        try {
            tracer = getSbbContext().getTracer("");
            // The trace method throws a java.lang.NullPointerException if message is null.
            tracer.trace(TraceLevel.FINE, null);
        } catch (java.lang.NullPointerException e) {
            sbbTracer.info("got expected NullPointerException", null);
            passed = true;
        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }

        if (!passed) {
            sendResultToTCK("Test1113143Test", false, 1113143,
                    "Tracer.trace(TraceLevel, null) should have thrown java.lang.NullPointerException.");
            return;
        }

        try {
            tracer = getSbbContext().getTracer("");
            // The trace method throws a java.lang.NullPointerException if message is null.
            tracer.trace(TraceLevel.FINE, null, null);
        } catch (java.lang.NullPointerException e) {
            sbbTracer.info("got expected NullPointerException", null);
            passed = true;
        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }

        if (!passed) {
            sendResultToTCK("Test1113143Test", false, 1113143,
                    "Tracer.trace(TraceLevel, null, null) should have thrown java.lang.NullPointerException.");
            return;
        }

        try {
            tracer = getSbbContext().getTracer("");
            // The trace method throws a java.lang.NullPointerException if TraceLevel and message are null.
            tracer.trace(null, null);
        } catch (java.lang.NullPointerException e) {
            sbbTracer.info("got expected NullPointerException", null);
            passed = true;
        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }

        if (!passed) {
            sendResultToTCK("Test1113143Test", false, 1113143, "Tracer.trace(null, null) should have thrown java.lang.NullPointerException.");
            return;
        }

        try {
            tracer = getSbbContext().getTracer("");
            // The trace method throws a java.lang.NullPointerException if TraceLevel and message are null.
            tracer.trace(null, null, null);
        } catch (java.lang.NullPointerException e) {
            sbbTracer.info("got expected NullPointerException", null);
            passed = true;
        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }

        if (!passed) {
            sendResultToTCK("Test1113143Test", false, 1113143,
                    "Tracer.trace(null, null, null) should have thrown java.lang.NullPointerException.");
            return;
        }
        
        sbbTracer.info("Testing Assertion Number 1113144...");
        //1113144
        try {
            tracer = getSbbContext().getTracer("");
            tracer.trace(TraceLevel.OFF, TRACE_MESSAGE);
        } catch (java.lang.IllegalArgumentException iae) {
            sbbTracer.info("got expected IllegalArgumentException", null);
            passed = true;
        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }

        if (!passed) {
            sendResultToTCK("Test1113143Test", false, 1113144,
                    "Tracer.trace(TraceLevel.OFF, message) should have thrown java.lang.IllegalArgumentException.");
            return;
        }

        try {
            tracer = getSbbContext().getTracer("");
            tracer.trace(TraceLevel.OFF, TRACE_MESSAGE, null);
        } catch (java.lang.IllegalArgumentException iae) {
            sbbTracer.info("got expected IllegalArgumentException", null);
            passed = true;
        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }

        if (!passed) {
            sendResultToTCK("Test1113143Test", false, 1113144,
                    "Tracer.trace(TraceLevel.OFF, message, null) should have thrown java.lang.IllegalArgumentException.");
            return;
        }

        sendResultToTCK("Test1113143Test", true, 1113143, "This Tracer.trace tests passed!");
    }


    private Tracer sbbTracer;

}
