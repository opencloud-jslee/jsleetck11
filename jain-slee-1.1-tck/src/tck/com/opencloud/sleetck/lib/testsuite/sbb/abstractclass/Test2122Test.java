/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.sbb.abstractclass;

import com.opencloud.sleetck.lib.OperationTimedOutException;
import com.opencloud.sleetck.lib.SleeTCKTest;
import com.opencloud.sleetck.lib.SleeTCKTestUtils;
import com.opencloud.sleetck.lib.TCKTestResult;
import com.opencloud.sleetck.lib.resource.TCKActivityID;
import com.opencloud.sleetck.lib.resource.events.TCKResourceEventX;
import com.opencloud.sleetck.lib.resource.testapi.TCKResourceTestInterface;
import com.opencloud.sleetck.lib.testutils.QueuingResourceListener;

import javax.slee.management.DeployableUnitID;
import java.util.HashMap;

public class Test2122Test implements SleeTCKTest {

    private static final String SERVICE_DU_PATH_PARAM = "DUPath";
    private static final int TEST_ID = 2122;

    public void init(SleeTCKTestUtils utils) {
        this.utils = utils;
    }

    /**
     * Perform the actual test.
     */

    public TCKTestResult run() throws Exception {

        TCKResourceTestInterface resource = utils.getResourceInterface();
        TCKActivityID activityID = resource.createActivity("Test2122InitialActivity");
        resource.fireEvent(TCKResourceEventX.X1, TCKResourceEventX.X1, activityID, null);

        // Wait for timeout - if message received test has failed.
        try {
            Object sbbMessage = resourceListener.nextMessage().getMessage();
            HashMap map = (HashMap) sbbMessage;

            utils.getLog().fine("Received message from SBB " + map.get("Message"));

            return TCKTestResult.failed(TEST_ID, (String) map.get("Message"));
        } catch (OperationTimedOutException e) { // If timed out, then sbbRolledBack not called.
            return TCKTestResult.passed();
        }

    }

    /**
     * Do all the pre-run configuration of the test.
     */

    public void setUp() throws Exception {

        resourceListener = new QueuingResourceListener(utils);
        utils.getResourceInterface().setResourceListener(resourceListener);

        utils.getLog().fine("Installing and activating service");

        // Install the Deployable Units
        String duPath = utils.getTestParams().getProperty(SERVICE_DU_PATH_PARAM);
        duID = utils.install(duPath);

        // Activate the DU
        utils.activateServices(duID, true);

    }

    /**
     * Clean up after the test.
     */

    public void tearDown() throws Exception {
        utils.getLog().fine("Ending and purging activities");
        utils.getResourceInterface().clearActivities();
        utils.getLog().fine("Deactivating and uninstalling service");
        utils.deactivateAllServices();
        utils.uninstallAll();
        utils.getLog().fine("Disconnecting from resource");
        utils.getResourceInterface().removeResourceListener();
    }

    private SleeTCKTestUtils utils;
    private QueuingResourceListener resourceListener;
    private DeployableUnitID duID;
}
