/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.javax.slee.management.ServiceUsageMBean;

import com.opencloud.sleetck.lib.AbstractSleeTCKTest;
import com.opencloud.sleetck.lib.TCKTestResult;
import com.opencloud.sleetck.lib.resource.TCKActivityID;
import com.opencloud.sleetck.lib.resource.events.TCKResourceEventX;
import com.opencloud.sleetck.lib.testsuite.usage.common.GenericUsageMBeanLookup;
import com.opencloud.sleetck.lib.testsuite.usage.common.GenericUsageSbbInstructions;
import com.opencloud.sleetck.lib.testsuite.usage.common.UsageMBeanLookup;
import com.opencloud.sleetck.lib.testsuite.usage.common.GenericUsageMBeanProxy;
import com.opencloud.sleetck.lib.testutils.QueuingNotificationListener;
import com.opencloud.sleetck.lib.testutils.QueuingResourceListener;

import javax.slee.SbbID;

/**
 * Deploys 3 SBBS -- SBB A and SBB B 2 in one service, and SBB C in another service.
 * Update usage parameter values for all three SBBs.
 * Resets values in SBB A only.
 * Check that values for SBB A are reset.
 * Checks that values for SBB B and SBB C are not reset.
 * Update usage parameter values for all three SBBs.
 * Resets values the service containing A and B (but not C).
 * Checks that values in SBB A and SBB B are reset.
 * Checks that values for SBB C are not reset.
 */
public class ResetParametersTest extends AbstractSleeTCKTest {

    private static final String SERVICE_DU_PATH_1_PARAM = "serviceDUPath1";
    private static final String SERVICE_DU_PATH_2_PARAM = "serviceDUPath2";
    private static final String PARAMETER_SET_NAME = "NamedParameterSet";

    public TCKTestResult run() throws Exception {
        TCKActivityID activityID = utils().getResourceInterface().createActivity("ResetParametersTest-Activity");

        GenericUsageSbbInstructions instructionsForNamed = new GenericUsageSbbInstructions(PARAMETER_SET_NAME);
        instructionsForNamed.addFirstCountIncrement(5);
        instructionsForNamed.addTimeBetweenNewConnectionsSamples(10);
        GenericUsageSbbInstructions instructionsForUnnamed = new GenericUsageSbbInstructions(null);
        instructionsForUnnamed.addFirstCountIncrement(6);
        instructionsForUnnamed.addTimeBetweenNewConnectionsSamples(11);
        GenericUsageSbbInstructions[] instructionSets = new GenericUsageSbbInstructions[]{instructionsForNamed,instructionsForUnnamed};

        getLog().fine("Sanity checking that we have zero/empty values for each parameter");
        GenericUsageMBeanLookup[] usageMBeanLookups = {usageMBeanLookupA,usageMBeanLookupB,usageMBeanLookupC};
        StringBuffer parametersBuf = new StringBuffer();
        for (int i = 0; i < usageMBeanLookups.length; i++) {
            if(areAnyParametersNotReset(usageMBeanLookups[i],parametersBuf)) return TCKTestResult.error(
                    "The following parameters had non-zero / non-empty values for the "+usageMBeanLookups[i].getSbbID()+
                    " SBB, before any updates were made via the usage parameters interface: "+parametersBuf.toString());
        }

        getLog().info("Setting values in each SBB");
        sendInstructionsAndWait(activityID, instructionSets);

        getLog().fine("Checking for non-zero/non-empty values for each SBB");
        for (int i = 0; i < usageMBeanLookups.length; i++) {
            if(areAnyParametersReset(usageMBeanLookups[i],parametersBuf)) return TCKTestResult.error(
                    "The following parameters had zero / empty values for the "+usageMBeanLookups[i].getSbbID()+
                    " SBB, after updates were made via the usage parameters interface: "+parametersBuf.toString());
        }

        getLog().info("Resetting values in SBB A only");
        SbbID sbbA = usageMBeanLookupA.getSbbID();
        usageMBeanLookupA.getServiceUsageMBeanProxy().resetAllUsageParameters(sbbA);

        getLog().info("Checking for zero/empty values in SBB A");
        if(areAnyParametersNotReset(usageMBeanLookupA,parametersBuf)) return TCKTestResult.failed(4618,
                "The following parameters had non-zero / non-empty values for the "+sbbA+" SBB, after "+
                "resetAllUsageParameters(SbbID) was called with the id of the SBB: "+parametersBuf.toString());

        getLog().info("Checking for non-zero/non-empty values for the other SBBs");
        if(areAnyParametersReset(usageMBeanLookupB,parametersBuf)) return TCKTestResult.failed(4618,
                "The following parameters had their values reset for the "+usageMBeanLookupB.getSbbID()+" SBB, after "+
                "resetAllUsageParameters(SbbID) was called with the id of another SBB in the same service: "+
                parametersBuf.toString());
        if(areAnyParametersReset(usageMBeanLookupC,parametersBuf)) return TCKTestResult.failed(4618,
                "The following parameters had their values reset for the "+usageMBeanLookupC.getSbbID()+" SBB, after "+
                "resetAllUsageParameters(SbbID) was called with the id of an SBB in another service: "+
                parametersBuf.toString());

        getLog().info("Setting values in each SBB");
        sendInstructionsAndWait(activityID, instructionSets);

        getLog().fine("Checking for non-zero/non-empty values for each SBB");
        for (int i = 0; i < usageMBeanLookups.length; i++) {
            if(areAnyParametersReset(usageMBeanLookups[i],parametersBuf)) return TCKTestResult.error(
                    "The following parameters had zero / empty values for the "+usageMBeanLookups[i].getSbbID()+
                    " SBB, after updates were made via the usage parameters interface: "+parametersBuf.toString());
        }

        getLog().info("Resetting values the service containing A and B, but not C");
        usageMBeanLookupA.getServiceUsageMBeanProxy().resetAllUsageParameters();

        getLog().info("Checking for zero/empty values in SBB A and B");
        GenericUsageMBeanLookup[] usageMBeanLookupAndB = {usageMBeanLookupA,usageMBeanLookupB};
        for (int i = 0; i < usageMBeanLookupAndB.length; i++) {
            GenericUsageMBeanLookup usageMBeanLookup = usageMBeanLookupAndB[i];
            if(areAnyParametersNotReset(usageMBeanLookup,parametersBuf)) return TCKTestResult.failed(4595,
                "The following parameters had non-zero / non-empty values for the "+usageMBeanLookup.getSbbID()+" SBB, "+
                "after resetAllUsageParameters() was called on the ServiceUsageMBean representing the service which "+
                "contained the SBB: "+parametersBuf.toString());
        }

        getLog().info("Checking for non-zero/non-empty values for the SBB in the other service");
        if(areAnyParametersReset(usageMBeanLookupC,parametersBuf)) return TCKTestResult.failed(4595,
                "The following parameters had their values reset for the "+usageMBeanLookupC.getSbbID()+" SBB, after "+
                "resetAllUsageParameters() was called on a ServiceUsageMBean representing a service which "+
                "did not contain the SBB: "+parametersBuf.toString());

        return TCKTestResult.passed();
    }

    /**
     * Checks to see if any relevant parameters in the inidicated SBB have non-reset values
     * (non-zero counters or non-empty sample sets).
     * Stores the names of any non-reset parameters in the given StringBuffer, then returns whether or not
     * any non-reset parameters were detected.
     * @param usageMBeanLookup The GenericUsageMBeanLookup for the SBB whose usage parameters are to be inspected
     * @param notResetParametersBuf The StringBuffer to store the names of non-reset parameters in
     * @return whether or not any non-reset parameters were detected
     */
    private boolean areAnyParametersNotReset(GenericUsageMBeanLookup usageMBeanLookup, StringBuffer notResetParametersBuf) throws Exception {
        GenericUsageMBeanProxy usageMBeanProxyUnnamed = usageMBeanLookup.getUnnamedGenericUsageMBeanProxy();
        GenericUsageMBeanProxy usageMBeanProxyNamed = usageMBeanLookup.getNamedGenericSbbUsageMBeanProxy(PARAMETER_SET_NAME);
        long firstCountUnnamed = usageMBeanProxyUnnamed.getFirstCount(false);
        long timeBetweenNewConnectionsUnnamedCount = usageMBeanProxyUnnamed.getTimeBetweenNewConnections(false).getSampleCount();
        long firstCountNamed = usageMBeanProxyNamed.getFirstCount(false);
        long timeBetweenNewConnectionsNamedCount = usageMBeanProxyNamed.getTimeBetweenNewConnections(false).getSampleCount();

        if(firstCountNamed != 0L || firstCountUnnamed != 0L ||
                  timeBetweenNewConnectionsNamedCount != 0L || timeBetweenNewConnectionsUnnamedCount != 0L) {
            if(firstCountUnnamed != 0L) notResetParametersBuf.append("(unnamed-parameter-set):firstCount="+firstCountUnnamed+";");
            if(firstCountNamed   != 0L) notResetParametersBuf.append(PARAMETER_SET_NAME+":firstCount="+firstCountNamed+";");
            if(timeBetweenNewConnectionsUnnamedCount != 0L) notResetParametersBuf.append(
                    "(unnamed-parameter-set):timeBetweenNewConnections,sample count="+timeBetweenNewConnectionsUnnamedCount+";");
            if(timeBetweenNewConnectionsNamedCount != 0L) notResetParametersBuf.append(
                    PARAMETER_SET_NAME+":timeBetweenNewConnections,sample count="+timeBetweenNewConnectionsNamedCount+";");
            return true;
        }
        return false;
    }

    /**
     * Checks to see if any relevant parameters in the inidicated SBB have reset values
     * (zeroed counters or empty sample sets).
     * Stores the names of any reset parameters in the given StringBuffer, then returns whether or not
     * any reset parameters were detected.
     * @param usageMBeanLookup The GenericUsageMBeanLookup for the SBB whose usage parameters are to be inspected
     * @param notResetParametersBuf The StringBuffer to store the names of reset parameters in
     * @return whether or not any reset parameters were detected
     */
    private boolean areAnyParametersReset(GenericUsageMBeanLookup usageMBeanLookup, StringBuffer notResetParametersBuf) throws Exception {
        GenericUsageMBeanProxy usageMBeanProxyUnnamed = usageMBeanLookup.getUnnamedGenericUsageMBeanProxy();
        GenericUsageMBeanProxy usageMBeanProxyNamed = usageMBeanLookup.getNamedGenericSbbUsageMBeanProxy(PARAMETER_SET_NAME);
        long firstCountUnnamed = usageMBeanProxyUnnamed.getFirstCount(false);
        long timeBetweenNewConnectionsUnnamedCount = usageMBeanProxyUnnamed.getTimeBetweenNewConnections(false).getSampleCount();
        long firstCountNamed = usageMBeanProxyNamed.getFirstCount(false);
        long timeBetweenNewConnectionsNamedCount = usageMBeanProxyNamed.getTimeBetweenNewConnections(false).getSampleCount();

        if(firstCountNamed == 0L || firstCountUnnamed == 0L ||
                  timeBetweenNewConnectionsNamedCount == 0L || timeBetweenNewConnectionsUnnamedCount == 0L) {
            if(firstCountUnnamed == 0L) notResetParametersBuf.append("(unnamed-parameter-set):firstCount;");
            if(firstCountNamed   == 0L) notResetParametersBuf.append(PARAMETER_SET_NAME+":firstCount;");
            if(timeBetweenNewConnectionsUnnamedCount == 0L) notResetParametersBuf.append(
                    "(unnamed-parameter-set):timeBetweenNewConnections;");
            if(timeBetweenNewConnectionsNamedCount == 0L) notResetParametersBuf.append(
                    PARAMETER_SET_NAME+":timeBetweenNewConnections;");
            return true;
        }
        return false;
    }

    private void sendInstructionsAndWait(TCKActivityID activityID, GenericUsageSbbInstructions[] instructionSets) throws Exception {
        for (int i = 0; i < instructionSets.length; i++) {
            GenericUsageSbbInstructions instructionSet = instructionSets[i];
            getLog().fine("firing event to SBBs");
            utils().getResourceInterface().fireEvent(TCKResourceEventX.X1,instructionSet.toExported(),activityID,null);

            getLog().fine("waiting for replies");
            resourceListener.nextMessage();
            resourceListener.nextMessage();
            resourceListener.nextMessage();
            getLog().info("received replies");

            getLog().fine("waiting for usage notifications");
            for (int updateIndex = 0; updateIndex < instructionSet.getTotalUpdates() * 3; updateIndex++) {
                notificationListener.nextNotification();
            }
            getLog().info("received all "+(instructionSet.getTotalUpdates() * 3)+" usage notifications");
        }
    }

    public void setUp() throws Exception {
        setupService(SERVICE_DU_PATH_1_PARAM, true);
        setupService(SERVICE_DU_PATH_2_PARAM, true);

        setResourceListener(resourceListener = new QueuingResourceListener(utils()));

        notificationListener = new QueuingNotificationListener(utils());

        usageMBeanLookupA = new GenericUsageMBeanLookup("ResetParametersTestService","ResetParametersTestChildSbbA",utils());
        usageMBeanLookupB = new GenericUsageMBeanLookup("ResetParametersTestService","ResetParametersTestChildSbbB",utils());
        usageMBeanLookupC = new GenericUsageMBeanLookup(utils());

        UsageMBeanLookup[] usageMBeanLookups = {usageMBeanLookupA,usageMBeanLookupB,usageMBeanLookupC};
        for (int i = 0; i < usageMBeanLookups.length; i++) {
            UsageMBeanLookup usageMBeanLookup = usageMBeanLookups[i];
            SbbID sbbID = usageMBeanLookup.getSbbID();
            usageMBeanLookup.getServiceUsageMBeanProxy().createUsageParameterSet(sbbID,PARAMETER_SET_NAME);
            usageMBeanLookup.getUnnamedSbbUsageMBeanProxy().addNotificationListener(notificationListener,null,null);
            usageMBeanLookup.getNamedSbbUsageMBeanProxy(PARAMETER_SET_NAME).addNotificationListener(notificationListener,null,null);
        }
    }

    public void tearDown() throws Exception {
        UsageMBeanLookup[] usageMBeanLookups = {usageMBeanLookupA,usageMBeanLookupB,usageMBeanLookupC};
        for (int i = 0; i < usageMBeanLookups.length; i++) {
            UsageMBeanLookup usageMBeanLookup = usageMBeanLookups[i];
            if(usageMBeanLookup!=null){
                usageMBeanLookup.getUnnamedSbbUsageMBeanProxy().removeNotificationListener(notificationListener);
                usageMBeanLookup.getNamedSbbUsageMBeanProxy(PARAMETER_SET_NAME).removeNotificationListener(notificationListener);
            }
        }
        for (int i = 0; i < usageMBeanLookups.length; i++) {
            if(usageMBeanLookups[i]!=null)usageMBeanLookups[i].closeAllMBeans();
        }
        super.tearDown();
    }

    private QueuingResourceListener resourceListener;
    private QueuingNotificationListener notificationListener;
    private GenericUsageMBeanLookup usageMBeanLookupA;
    private GenericUsageMBeanLookup usageMBeanLookupB;
    private GenericUsageMBeanLookup usageMBeanLookupC;

}
