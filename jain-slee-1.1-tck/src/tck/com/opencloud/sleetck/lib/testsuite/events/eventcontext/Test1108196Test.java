/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.events.eventcontext;

import java.rmi.RemoteException;
import java.util.Map;

import com.opencloud.sleetck.lib.AbstractSleeTCKTest;
import com.opencloud.sleetck.lib.TCKTestFailureException;
import com.opencloud.sleetck.lib.resource.TCKActivityID;
import com.opencloud.sleetck.lib.resource.TCKSbbMessage;
import com.opencloud.sleetck.lib.resource.events.TCKResourceEventX;
import com.opencloud.sleetck.lib.resource.testapi.TCKResourceListener;
import com.opencloud.sleetck.lib.resource.testapi.TCKResourceTestInterface;
import com.opencloud.sleetck.lib.testutils.Assert;
import com.opencloud.sleetck.lib.testutils.BaseTCKResourceListener;
import com.opencloud.sleetck.lib.testutils.FutureResult;


/**
 * AssertionID(1108196): Test In case of a programming error in the SBB, 
 * event delivery is only ever suspended for a time period. This time 
 * period can be provided by the SBB as an argument, or if not provided 
 * it is a platform specific default.
 * 
 * AssertionID(1108197): Test The SLEE automatically resumes the delivery
 * of the event after the period elapses.
 */

public class Test1108196Test extends AbstractSleeTCKTest {

    public static final String SERVICE1_DU_PATH_PARAM = "service1DUPath";

    public static final String SERVICE2_DU_PATH_PARAM = "service2DUPath";

    public static final String activityNameA = "Test1108196Test-ActivityA";

    public static final String activityNameB = "Test1108196Test-ActivityB";
    
    private static final int defaultTimeout = 25000;

    /**
     * Perform the actual test.
     */

    public void run(FutureResult result) throws Exception {
        this.result = result;

        TCKResourceTestInterface resource = utils().getResourceInterface();
        TCKActivityID activityA = resource.createActivity(activityNameA);
        TCKActivityID activityB = resource.createActivity(activityNameB);

        // TCK test starts activityA and fires event TCKResourceEventX1
        getLog().fine("Firing TCKResourceEventX1 on Test1108196Test-ActivityA.");
        resource.fireEvent(TCKResourceEventX.X1, testName, activityA, null);

        expectedCountSbb1 = 1; 
        expectedCountSbb2 = 0;
        
        synchronized (this) {
            wait(1000);
        }

        if (expectedCountSbb1 != receivedCountSbb1 || receivedCountSbb2 != expectedCountSbb2) {
            result.setFailed(1108196, "First check: Expected number of trace messages not received, traces were not delivered by "
                    + "the TraceFacility (expected " + (expectedCountSbb1 + expectedCountSbb2) + ", received "
                    + (receivedCountSbb1 + receivedCountSbb2) + ")");
            return;
        }

        // TCK test starts activityB and fires event TCKResourceEventX2
        getLog().fine("Firing TCKResourceEventX2 on Test1108196Test-ActivityB.");
        resource.fireEvent(TCKResourceEventX.X2, testName, activityB, null);

        expectedCountSbb1 = 2; 
        expectedCountSbb2 = 1;
        
        synchronized (this) {
            wait(1000);
        }
        
        if (expectedCountSbb1 != receivedCountSbb1 || receivedCountSbb2 != expectedCountSbb2 ) {
            result.setFailed(1108196, "Second check: Expected number of trace messages not received, traces were not delivered by "
                    + "the TraceFacility (expected " + (expectedCountSbb1 + expectedCountSbb2) + ", received "
                    + (receivedCountSbb1 + receivedCountSbb2) + ")");
            return;
        }
        
        expectedCountSbb1 = 2; 
        expectedCountSbb2 = 2;
        
        // We assumed that EventContext.suspendDelivery() here
        // Check again, now the suspended event delivery on Activity A would 
        // be resumed after five seconds.

        if (getTestName().equals("NOTIMEOUT")) {
            synchronized (this) {
                //30s is the default timeout of EventContext.suspendDelivery()
                wait(30000);
            }
        }            
        else if (getTestName().equals("TIMEOUT")) {
            // We assumed that EventContext.suspendDelivery() here
            synchronized (this) {
                wait(5000);
            }
        }
        else {
            result.setError("Unexpected test name encountered during test run: " + getTestName());
            return;
        }
        
        if (expectedCountSbb1 != receivedCountSbb1 || receivedCountSbb2 != expectedCountSbb2 ) {
            result.setFailed(1108197, "Second check: Expected number of trace messages not received, traces were not delivered by "
                    + "the TraceFacility (expected " + (expectedCountSbb1 + expectedCountSbb2) + ", received "
                    + (receivedCountSbb1 + receivedCountSbb2) + ")");
            return;
        }


        // TCK test fires event TCKResourceEventX3 on activityA and activityB
        getLog().fine("Firing TCKResourceEventX3 on Test1108196Test-ActivityA and Test1108196Test-ActivityB.");
        resource.fireEvent(TCKResourceEventX.X3, testName, activityA, null);
        resource.fireEvent(TCKResourceEventX.X3, testName, activityB, null);

        getLog().fine("Waiting for test pass indicator from SBB.");

        expectedCountSbb1 = expectedCountSbb2 = 4;

        synchronized (this) {
            wait(defaultTimeout - 7000);
        }

        if (expectedCountSbb1 == receivedCountSbb1 && receivedCountSbb2 == expectedCountSbb2) 
            result.setPassed();
        else
            result.setFailed(1108197, "Fourth check: Expected number of trace messages not received, traces were not delivered by "
                    + "the TraceFacility (expected " + (expectedCountSbb1 + expectedCountSbb2) + ", received "
                    + (receivedCountSbb1 + receivedCountSbb2) + ")");
    }

    /**
     * Do all the pre-run configuration of the test.
     */

    public void setUp() throws Exception {
        getLog().fine("Connecting to resource");
        resourceListener = new TCKResourceListenerImpl();
        setResourceListener(resourceListener);
        getLog().fine("Installing and activating service");
        testName = getTestName();
        setupService(SERVICE1_DU_PATH_PARAM, true);
        setupService(SERVICE2_DU_PATH_PARAM, true);
    }

    /**
     * Clean up after the test.
     */

    public void tearDown() throws Exception {
        super.tearDown();
    }

    public String getTestName() {
        String testName = "testName";
        return utils().getTestParams().getProperty(testName);
    }

    private class TCKResourceListenerImpl extends BaseTCKResourceListener {
        // TCKResourceListener methods

        public synchronized void onSbbMessage(TCKSbbMessage message, TCKActivityID calledActivity)
        throws RemoteException {
        
            Map sbbData = (Map) message.getMessage();
            String sbbTestName = "Test1108196Test";
            String sbbTestResult = (String) sbbData.get("result");
            String sbbTestMessage = (String) sbbData.get("message");
            int assertionID = ((Integer) sbbData.get("id")).intValue();
            getLog().info(
                    "Received message from SBB: testname=" + sbbTestName + ", result=" + sbbTestResult + ", message="
                            + sbbTestMessage + ", id=" + assertionID);
            try {
                Assert.assertEquals(assertionID, "Test " + sbbTestName + " failed.", "pass", sbbTestResult);
                if (sbbTestMessage.contains("Sbb1"))
                    receivedCountSbb1++;
                else if (sbbTestMessage.contains("Sbb2"))
                    receivedCountSbb2++;
            } catch (TCKTestFailureException ex) {
                result.setFailed(assertionID, sbbTestMessage);
            }
        }

        public void onException(Exception exception) throws RemoteException {
            getLog().warning("Received Exception from SBB or resource:");
            getLog().warning(exception);
            result.setError(exception);
        }
    }

    private TCKResourceListener resourceListener;
    private FutureResult result;    
    private int receivedCountSbb1 = 0;
    private int receivedCountSbb2 = 0;
    private int expectedCountSbb1;
    private int expectedCountSbb2;
    private String testName = "Test1108196";
}
