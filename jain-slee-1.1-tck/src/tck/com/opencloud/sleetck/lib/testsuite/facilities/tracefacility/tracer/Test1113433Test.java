/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.facilities.tracefacility.tracer;

import javax.management.Notification;
import javax.management.NotificationListener;
import javax.management.ObjectName;
import javax.slee.ComponentID;
import javax.slee.SbbID;
import javax.slee.ServiceID;
import javax.slee.facilities.TraceLevel;
import javax.slee.management.DeployableUnitDescriptor;
import javax.slee.management.DeployableUnitID;
import javax.slee.management.SbbNotification;
import javax.slee.management.TraceNotification;

import com.opencloud.sleetck.lib.AbstractSleeTCKTest;
import com.opencloud.sleetck.lib.DescriptionKeys;
import com.opencloud.sleetck.lib.TCKTestErrorException;
import com.opencloud.sleetck.lib.resource.TCKActivityID;
import com.opencloud.sleetck.lib.resource.events.TCKResourceEventX;
import com.opencloud.sleetck.lib.resource.testapi.TCKResourceTestInterface;
import com.opencloud.sleetck.lib.testutils.FutureResult;
import com.opencloud.sleetck.lib.testutils.jmx.DeploymentMBeanProxy;
import com.opencloud.sleetck.lib.testutils.jmx.SleeManagementMBeanProxy;
import com.opencloud.sleetck.lib.testutils.jmx.TraceMBeanProxy;
import com.opencloud.sleetck.lib.testutils.jmx.impl.TraceMBeanProxyImpl;

/*
 * AssertionID (1113433): Test these utility methods are the 
 * equivalent of calling the trace method with the trace level 
 * specified in the method name.
 */
public class Test1113433Test extends AbstractSleeTCKTest {
    private static final int defaultTimeout = 25000;

    /**
     * Perform the actual test.
     */
    public void run(FutureResult result) throws Exception {
        this.result = result;
        if (getTestName().equals("SEVERE")) {
            expectedTraceNotifications = 4;
            receivedTraceNotifications = 0;
            doTest1113433Test(TraceLevel.SEVERE);
        }

        else if (getTestName().equals("WARNING")) {
            expectedTraceNotifications = 12;
            receivedTraceNotifications = 0;
            doTest1113433Test(TraceLevel.WARNING);
        } else if (getTestName().equals("INFO")) {
            expectedTraceNotifications = 24;
            receivedTraceNotifications = 0;
            doTest1113433Test(TraceLevel.INFO);
        } else if (getTestName().equals("CONFIG")) {
            expectedTraceNotifications = 40;
            receivedTraceNotifications = 0;
            doTest1113433Test(TraceLevel.CONFIG);
        } else if (getTestName().equals("FINE")) {
            expectedTraceNotifications = 60;
            receivedTraceNotifications = 0;
            doTest1113433Test(TraceLevel.FINE);
        } else if (getTestName().equals("FINER")) {
            expectedTraceNotifications = 84;
            receivedTraceNotifications = 0;
            doTest1113433Test(TraceLevel.FINER);
        } else if (getTestName().equals("FINEST")) {
            expectedTraceNotifications = 112;
            receivedTraceNotifications = 0;
            doTest1113433Test(TraceLevel.FINEST);
        }

        synchronized (this) {
            wait(defaultTimeout);
        }

        if (expectedTraceNotifications == receivedTraceNotifications)
            result.setPassed();
        else
            result.setFailed(1113433,
                    "Expected number of trace messages not received, traces were not delivered by "
                            + "the TraceFacility (expected " + expectedTraceNotifications + ", received "
                            + receivedTraceNotifications + ")");

    }

    public void doTest1113433Test(TraceLevel traceLevel) throws Exception {

        TraceMBeanProxy traceMBeanProxy = utils().getMBeanProxyFactory().createTraceMBeanProxy(
                utils().getSleeManagementMBeanProxy().getTraceMBean());

        if (sbbID == null)
            throw new TCKTestErrorException("sbbID not found for " + DescriptionKeys.SERVICE_DU_PATH_PARAM);
        if (serviceID == null)
            throw new TCKTestErrorException("serviceID not found for " + DescriptionKeys.SERVICE_DU_PATH_PARAM);

        SbbNotification sbbNotification = new SbbNotification(serviceID, sbbID);
        try {
            getLog().fine(
                    "Starting to test The trace level of a particular tracer "
                            + "is equal to the trace level assigned to it by an Administrator "
                            + "using a TraceMBean object.");
            traceMBeanProxy.setTraceLevel(sbbNotification, "com.test", traceLevel);
        } catch (Exception e) {
            getLog().warning(e);
            result.setError("ERROR!", e);
        }

        String activityName = "Test1113433Test";
        TCKResourceTestInterface resource = utils().getResourceInterface();
        TCKActivityID activityID = resource.createActivity(activityName);
        getLog().info("Firing event: " + testName);

        // kickoff the test
        resource.fireEvent(TCKResourceEventX.X1, testName, activityID, null);
    }

    /**
     * Do all the pre-run configuration of the test.
     */

    public void setUp() throws Exception {

        getLog().fine("Connecting to resource");
        getLog().fine("Installing and activating service");

        // Install the Deployable Units
        String duPath = utils().getTestParams().getProperty(DescriptionKeys.SERVICE_DU_PATH_PARAM);
        duID = utils().install(duPath);

        // Activate the DU
        utils().activateServices(duID, true);

        // Setup TraceNotificationListener
        SleeManagementMBeanProxy proxy = utils().getSleeManagementMBeanProxy();
        traceMBeanName = proxy.getTraceMBean();
        listener = new TraceNotificationListenerImpl();

        tracembean = new TraceMBeanProxyImpl(traceMBeanName, utils().getMBeanFacade());
        tracembean.addNotificationListener(listener, null, null);

        DeploymentMBeanProxy duProxy = utils().getDeploymentMBeanProxy();
        DeployableUnitDescriptor duDesc = duProxy.getDescriptor(duID);
        ComponentID components[] = duDesc.getComponents();
        // Get the SbbID from the components.
        for (int i = 0; i < components.length; i++) {
            if (components[i] instanceof ServiceID) {
                getLog().fine("Setting serviceID value.");
                serviceID = (ServiceID) components[i];
                continue;
            }

            if (components[i] instanceof SbbID) {
                getLog().fine("Setting sbbID value.");
                sbbID = (SbbID) components[i];
                continue;
            }
        }

    }

    /**
     * Clean up after the test.
     */

    public void tearDown() throws Exception {
        if (null != tracembean)
            tracembean.removeNotificationListener(listener);
        // cleanup
        super.tearDown();
    }

    public String getTestName() {
        String testName = "testName";
        return utils().getTestParams().getProperty(testName);
    }

    public class TraceNotificationListenerImpl implements NotificationListener {
        public final void handleNotification(Notification notification, Object handback) {

            if (notification instanceof TraceNotification) {
                TraceNotification traceNotification = (TraceNotification) notification;
                getLog().debug("Received trace notification " + traceNotification);

                if (traceNotification.getType().equals(SbbNotification.TRACE_NOTIFICATION_TYPE)) {
                    if (traceNotification.getMessage().equals(Test1113433Sbb.TRACE_MESSAGE_SEVERE))
                        receivedTraceNotifications = receivedTraceNotifications + TraceLevel.LEVEL_SEVERE;
                    else if (traceNotification.getMessage().equals(Test1113433Sbb.TRACE_MESSAGE_WARNING))
                        receivedTraceNotifications = receivedTraceNotifications + TraceLevel.LEVEL_WARNING;
                    else if (traceNotification.getMessage().equals(Test1113433Sbb.TRACE_MESSAGE_INFO))
                        receivedTraceNotifications = receivedTraceNotifications + TraceLevel.LEVEL_INFO;
                    else if (traceNotification.getMessage().equals(Test1113433Sbb.TRACE_MESSAGE_CONFIG))
                        receivedTraceNotifications = receivedTraceNotifications + TraceLevel.LEVEL_CONFIG;
                    else if (traceNotification.getMessage().equals(Test1113433Sbb.TRACE_MESSAGE_FINE))
                        receivedTraceNotifications = receivedTraceNotifications + TraceLevel.LEVEL_FINE;
                    else if (traceNotification.getMessage().equals(Test1113433Sbb.TRACE_MESSAGE_FINER))
                        receivedTraceNotifications = receivedTraceNotifications + TraceLevel.LEVEL_FINER;
                    else if (traceNotification.getMessage().equals(Test1113433Sbb.TRACE_MESSAGE_FINEST))
                        receivedTraceNotifications = receivedTraceNotifications + TraceLevel.LEVEL_FINEST;
                }
                return;
            }

            return;
        }
    }

    private NotificationListener listener;

    private FutureResult result;

    private DeployableUnitID duID;

    private String testName = "Test1113433";
    
    private SbbID sbbID;

    private ServiceID serviceID;

    private ObjectName traceMBeanName;

    private TraceMBeanProxy tracembean;

    private int receivedTraceNotifications;

    private int expectedTraceNotifications;

}