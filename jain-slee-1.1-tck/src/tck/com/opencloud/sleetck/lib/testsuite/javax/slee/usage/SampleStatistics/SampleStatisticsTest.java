/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.javax.slee.usage.SampleStatistics;

import com.opencloud.sleetck.lib.TCKTestFailureException;
import com.opencloud.sleetck.lib.TCKTestResult;
import com.opencloud.sleetck.lib.resource.TCKActivityID;
import com.opencloud.sleetck.lib.resource.events.TCKResourceEventX;
import com.opencloud.sleetck.lib.testsuite.usage.common.GenericUsageMBeanProxy;
import com.opencloud.sleetck.lib.testsuite.usage.common.GenericUsageSbbInstructions;
import com.opencloud.sleetck.lib.testsuite.usage.common.GenericUsageTest;
import com.opencloud.sleetck.lib.testutils.QueuingNotificationListener;
import com.opencloud.sleetck.lib.testutils.QueuingResourceListener;

import javax.slee.usage.SampleStatistics;
import javax.management.Notification;
import java.math.BigDecimal;

/**
 * Checks the return values of SampleStatistics with various sample sets.
 */
public class SampleStatisticsTest extends GenericUsageTest {

    public TCKTestResult run() throws Exception {
        activityID = utils().getResourceInterface().createActivity("SampleStatisticsTest-Activity");
        clearExpectedSet();
        /*
          Cases covered:
          sample count: 0, 1, many
          minimum: Long.MAX_VALUE,Long.MAX_VALUE-1,Long.MIN_VALUE,Long.MIN_VALUE+1,-1,0,1
          maximum: Long.MAX_VALUE,Long.MAX_VALUE-1,Long.MIN_VALUE,Long.MIN_VALUE+1,-1,0,1
          mean: +ve,0,-ve,fractions,Long.MAX_VALUE,Long.MIN_VALUE
        */
        testSampleSet(new long[]{2,3,5,7,11,-2,-3,-5,-7,-11,-11},0.5d);
        testSampleSet(new long[]{Long.MAX_VALUE,Long.MAX_VALUE-1,1,0,-1,Long.MIN_VALUE+1,Long.MIN_VALUE},0.5d);
        testSampleSet(new long[]{Long.MIN_VALUE,Long.MIN_VALUE+1,-1,0,1,Long.MAX_VALUE-1,Long.MAX_VALUE},0.5d);
        getLog().info("All checks passed");
        return TCKTestResult.passed();
    }

    private void testSampleSet(long[] samples, double errorThresholdForMean) throws Exception {
        resetStatistics();
        auditStatistics(0d);
        for (int i = 0; i < samples.length; i++) {
            long sample = samples[i];
            doUpdateAndWait(sample);
            auditStatistics(errorThresholdForMean);
        }
    }

    private void resetStatistics() throws Exception {
        sbbUsageMBeanProxy.getTimeBetweenNewConnections(true);
        clearExpectedSet();
    }

    private void clearExpectedSet() {
        expectedMaximum = Long.MIN_VALUE;
        expectedMinimum = Long.MAX_VALUE;
        expectedSampleCount = 0;
        accumulatedSamplesSum = null;
        sampleSetBuffer = new StringBuffer("<Empty set>");
    }

    private void doUpdateAndWait(long sampleValue) throws Exception {
        // do the update
        getLog().fine("Sending update instructions to the SBB via an event. New sample value: "+sampleValue);
        GenericUsageSbbInstructions instructions = new GenericUsageSbbInstructions(null);
        instructions.addTimeBetweenNewConnectionsSamples(sampleValue);
        utils().getResourceInterface().fireEvent(TCKResourceEventX.X1,instructions.toExported(),activityID, null);
        // wait for indication of completion
        getLog().fine("Waiting for a reply from the SBB");
        resourceListener.nextMessage();
        getLog().fine("Received reply from SBB. Waiting for notification of update");
        Notification usageNotification = notificationListener.nextNotification();
        getLog().fine("Received notification of update: "+usageNotification);
        // update our expected statistics
        expectedMaximum = Math.max(expectedMaximum,sampleValue);
        expectedMinimum = Math.min(expectedMinimum,sampleValue);
        BigDecimal sampleBigDecimal = new BigDecimal(Long.toString(sampleValue));
        if(expectedSampleCount++ == 0) {
            accumulatedSamplesSum = sampleBigDecimal;
            sampleSetBuffer = new StringBuffer(sampleValue+"");
        } else {
            accumulatedSamplesSum = accumulatedSamplesSum.add(sampleBigDecimal);
            sampleSetBuffer.append(","+sampleValue);
        }
    }

    private void auditStatistics(double errorThresholdForMean) throws Exception {
        double expectedMean = expectedSampleCount == 0 ? 0.0d :
                              accumulatedSamplesSum.divide(new BigDecimal(expectedSampleCount),50,
                                        BigDecimal.ROUND_HALF_UP).doubleValue();
        getLog().info("Auditing statistics. Expected values: expectedSampleCount="+expectedSampleCount+
                ";expectedMinimum="+expectedMinimum+";expectedMaximum="+expectedMaximum+";expectedMean="+expectedMean);
        SampleStatistics statistics = sbbUsageMBeanProxy.getTimeBetweenNewConnections(false); // don't reset
        // sample count
        long sampleCountReturned = statistics.getSampleCount();
        if(sampleCountReturned != expectedSampleCount) {
            throw new TCKTestFailureException(4638,"SampleStatistics.getSampleCount() returned an invalid value. "+
                    "Expected="+expectedSampleCount+"; sample count returned="+sampleCountReturned+
                    "; Sample set:{"+sampleSetBuffer+"}");
        }
        // minimum
        long minimumReturned = statistics.getMinimum();
        if(minimumReturned != expectedMinimum) {
            int violatedAssertion = expectedSampleCount == 0 ? 4642 : // re: default is Long.MAX_VALUE
                                                               4640;  // re: minumum is lowest of accumulated set
            throw new TCKTestFailureException(violatedAssertion,
                    "SampleStatistics.getMinimum() returned an invalid value. "+
                    "Expected="+expectedMinimum+"; minimum value returned="+minimumReturned+
                    "; Sample set:{"+sampleSetBuffer+"}");
        }
        // maximum
        long maximumReturned = statistics.getMaximum();
        if(maximumReturned != expectedMaximum) {
            int violatedAssertion = expectedSampleCount == 0 ? 4645 : // re: default is Long.MIN_VALUE
                                                               4643;  // re: minimum is highest of accumulated set
            throw new TCKTestFailureException(violatedAssertion,
                    "SampleStatistics.getMaximum() returned an invalid value. "+
                    "Expected="+expectedMaximum+"; maximum value returned="+maximumReturned+
                    "; Sample set:{"+sampleSetBuffer+"}");
        }
        // mean
        double meanReturned = statistics.getMean();
        if(meanReturned != expectedMean) {
            double differenceInMeans = Math.abs(expectedMean - meanReturned);
            if(differenceInMeans > errorThresholdForMean) {
                int violatedAssertion = expectedSampleCount == 0 ? 4648 : // re: default is 0.0
                                                                   4646;  // re: mean of accumulated samples
                throw new TCKTestFailureException(violatedAssertion,
                        "SampleStatistics.getMean() returned an invalid value. "+
                        "Expected="+expectedMean+"; mean value returned="+meanReturned+"; error threshold:"+errorThresholdForMean+
                        "; Sample set:{"+sampleSetBuffer+"}");
            } else getLog().warning("Tolerated difference between expected and returned mean of "+differenceInMeans+
                    ". Expected="+expectedMean+"; returned="+meanReturned+"; Sample set:{"+sampleSetBuffer.toString()+"}");
        }
    }

    public void setUp() throws Exception {
        super.setUp();
        setResourceListener(resourceListener = new QueuingResourceListener(utils()));
        sbbUsageMBeanProxy = getGenericUsageMBeanLookup().getUnnamedGenericUsageMBeanProxy();
        notificationListener = new QueuingNotificationListener(utils());
        sbbUsageMBeanProxy.addNotificationListener(notificationListener, null, null);
    }

    public void tearDown() throws Exception {
        if(notificationListener != null) {
            try {
                sbbUsageMBeanProxy.removeNotificationListener(notificationListener);
            } catch (Exception e) { getLog().warning(e); }
        }
        sbbUsageMBeanProxy = null;
        super.tearDown();
    }

    private TCKActivityID activityID;
    private QueuingResourceListener resourceListener;
    private QueuingNotificationListener notificationListener;
    private GenericUsageMBeanProxy sbbUsageMBeanProxy;

    private long expectedMinimum;
    private long expectedMaximum;
    private long expectedSampleCount;
    private BigDecimal accumulatedSamplesSum;
    private StringBuffer sampleSetBuffer;

}
