/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.preparatory.sleestate;

import com.opencloud.logging.Logable;
import com.opencloud.sleetck.lib.*;
import com.opencloud.sleetck.lib.testutils.ExceptionsUtil;
import com.opencloud.sleetck.lib.testutils.jmx.SleeManagementMBeanProxy;
import com.opencloud.sleetck.lib.testutils.statetracking.StateMachineTrackerUtil;
import com.opencloud.sleetck.lib.testutils.statetracking.TrackingStateNotificationsListener;

import javax.management.MBeanException;
import javax.slee.InvalidStateException;
import javax.slee.management.ManagementException;
import javax.slee.management.SleeState;
import java.util.Iterator;

/**
 * Tests the state transitions of the SLEE.
 *
 * What to test:
 * All legal state transitions work -
 *  - don't throw an exception
 *  - the state changes
 *  - a notification is fired
 * All illegal state change requests fail -
 *  - the correct exception is thrown
 *  - the state does not change
 *
 * Also implicitly tests:
 * The TCK Plugin - invocations and notification forwarding.
 *
 * How:
 * Registers a state notification listener with the SLEE,
 * then transitions through all legal state transitions.
 * Attempts all illegal state transitions also.
 */
public class SleeStateTest implements SleeTCKTest {

    // Implementation of SleeTCKTest methods

    public void init(SleeTCKTestUtils utils) {
        this.utils=utils;
    }

    public void setUp() throws Exception {
        log = utils.getLog();
        int[] allStates = {SleeState.SLEE_STOPPED,SleeState.SLEE_STARTING,SleeState.SLEE_RUNNING,SleeState.SLEE_STOPPING};
        stateTracker = new StateMachineTrackerUtil(allStates,log);
        buildStateMachineModel();
        stateListener = new TrackingStateNotificationsListener(stateTracker,log);
        managementMBean = utils.getMBeanProxyFactory().createSleeManagementMBeanProxy(
                                        utils.getSleeManagementMBeanName());
        managementMBean.addNotificationListener(stateListener, null, null);
        stateTracker.shiftState(managementMBean.getState().toInt()); // set tracker to current slee state
    }

    public void tearDown() throws Exception {
        if(managementMBean != null) managementMBean.removeNotificationListener(stateListener);
    }

    /**
     * Pre-conditions: In RUNNING state
     */
    public TCKTestResult run() {
        try {
            testStopFromRunning();
            testStopFromStopped();
            testStartFromStopped();
            testStartFromRunning();
            return TCKTestResult.passed();
        } catch(TCKTestErrorException te) {
            return TCKTestResult.error(te);
        } catch(TCKTestFailureException tf) {
            return TCKTestResult.failed(tf);
        }
    }

    // Private methods

    /**
     * Pre-conditions: In RUNNING state
     * Post-conditions: In STOPPED state
     */
    private void testStopFromRunning() throws TCKTestFailureException, TCKTestErrorException {
        log.finer("testing stop() from RUNNING state");
        try {
            assertInitialState(SleeState.RUNNING);
            stateTracker.setDestination(SleeState.SLEE_STOPPED);
            preTransition();
            managementMBean.stop();
            postTransition();
            assertFinalState(SleeState.STOPPED);
        } catch(Exception e) {
            handleException(e,"stop() from RUNNING state");
        }
    }

    /**
     * Pre-conditions: In STOPPED state
     * Post-conditions: In STOPPED state
     */
    private void testStopFromStopped() throws TCKTestFailureException, TCKTestErrorException {
        log.finer("testing stop() from STOPPED state");
        try {
            assertInitialState(SleeState.STOPPED);
            preTransition();
            InvalidStateException caught = null;
            try {
                managementMBean.stop();
            } catch (InvalidStateException ise) { caught = ise; }
            if(caught == null) throw new TCKTestFailureException(TCKTestResult.NO_ASSERTION,
                "Expected InvalidStateException while testing stop() from STOPPED state, but none thrown");
            postTransition();
            assertFinalState(SleeState.STOPPED);
        } catch(Exception e) {
            handleException(e,"stop() from STOPPED state");
        }
    }

    /**
     * Pre-conditions: In STOPPED state
     * Post-conditions: In RUNNING state
     */
    private void testStartFromStopped() throws TCKTestFailureException, TCKTestErrorException {
        log.finer("testing start() from STOPPED state");
        try {
            assertInitialState(SleeState.STOPPED);
            stateTracker.setDestination(SleeState.SLEE_RUNNING);
            preTransition();
            managementMBean.start();
            postTransition();
            assertFinalState(SleeState.RUNNING);
        } catch(Exception e) {
            handleException(e,"start() from STOPPED state");
        }
    }

    /**
     * Pre-conditions: In RUNNING state
     * Post-conditions: In RUNNING state
     */
    protected void testStartFromRunning() throws TCKTestFailureException, TCKTestErrorException {
        log.finer("testing start() from RUNNING state");
        try {
            assertInitialState(SleeState.RUNNING);
            preTransition();
            InvalidStateException caught = null;
            try {
                managementMBean.start();
            } catch (InvalidStateException ise) { caught = ise; }
            if(caught == null) throw new TCKTestFailureException(TCKTestResult.NO_ASSERTION,
                "Expected InvalidStateException while testing start() from RUNNING state, but none thrown");
            postTransition();
            assertFinalState(SleeState.RUNNING);
        } catch(Exception e) {
            handleException(e,"start() from RUNNING state");
        }
    }

    private void assertInitialState(SleeState expected) throws ManagementException, TCKTestErrorException {
        SleeState currentState = managementMBean.getState();
        if(!currentState.equals(expected)) throw new TCKTestErrorException(
            "Pre-condition not met: expected state was "+expected+
            ", found state was "+currentState);
    }

    private void assertFinalState(SleeState expected) throws ManagementException, TCKTestFailureException, TCKTestErrorException {
        SleeState currentState = managementMBean.getState();
        if(!currentState.equals(expected)) throw new TCKTestFailureException(
            TCKTestResult.NO_ASSERTION,
            "Post-condition not met: expected state was "+expected+
             ", found state was "+currentState);
    }

    private void preTransition() {
        stateTracker.clearTransitions();
        stateTracker.startRecording();
        stateTracker.stateUnknown();
    }

    private void postTransition() throws TCKTestErrorException, TCKTestFailureException {
        stateTracker.waitForDestination(utils.getTestTimeout()); // (will do nothing when no destination is set)
        stateTracker.stopRecording();
        Iterator badMoves = stateTracker.getIllegalTransitions();
        if(badMoves.hasNext()) {
            StateMachineTrackerUtil.Transition transition =
                (StateMachineTrackerUtil.Transition)badMoves.next();
            String fromFormatted = SleeState.fromInt(transition.getFromState()).toString();
            String toFormatted = SleeState.fromInt(transition.getToState()).toString();
            throw new TCKTestFailureException(TCKTestResult.NO_ASSERTION,"Illegal state transition detected in SleeProvider, from "+
                            fromFormatted+" to "+toFormatted);
        }
    }

    private void handleException(Exception e, String currentTest)
                        throws TCKTestErrorException, TCKTestFailureException {
        if(e instanceof TCKTestErrorException) throw (TCKTestErrorException)e;
        if(e instanceof TCKTestFailureException) throw (TCKTestFailureException)e;
        if(e instanceof InvalidStateException)
            throw new TCKTestFailureException(TCKTestResult.NO_ASSERTION,"Caught InvalidStateException while testing "+currentTest+":"+ExceptionsUtil.formatThrowable(e));
        if(e instanceof MBeanException) {
            MBeanException mbe = (MBeanException)e;
            throw new TCKTestErrorException("Caught MBeanException while testing "+currentTest+
            ". Nested exception:"+ExceptionsUtil.formatThrowable(mbe.getTargetException()));
        }
        throw new TCKTestErrorException("Caught exception while testing "+currentTest+":"+ExceptionsUtil.formatThrowable(e));
    }

    /**
     * Builds the model of the slee provider's state machine
     * in the state tracker by adding all valid state transitions.
     */
    private void buildStateMachineModel() {
        stateTracker.addTransition(SleeState.SLEE_STOPPED,SleeState.SLEE_STARTING);
        stateTracker.addTransition(SleeState.SLEE_STARTING,SleeState.SLEE_RUNNING);
        stateTracker.addTransition(SleeState.SLEE_STARTING,SleeState.SLEE_STOPPING);
        stateTracker.addTransition(SleeState.SLEE_RUNNING,SleeState.SLEE_STOPPING);
        stateTracker.addTransition(SleeState.SLEE_STOPPING,SleeState.SLEE_STOPPED);
    }

    // -- Private state -- //

    private StateMachineTrackerUtil stateTracker;
    private TrackingStateNotificationsListener stateListener;
    private SleeManagementMBeanProxy managementMBean;
    private Logable log;
    private SleeTCKTestUtils utils;

    // -- Private constants -- //

}
