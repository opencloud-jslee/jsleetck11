/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.facilities.tracefacility.tracer;

import javax.management.Notification;
import javax.management.NotificationListener;
import javax.management.ObjectName;
import javax.slee.management.TraceNotification;
import javax.slee.management.SbbNotification;

import com.opencloud.sleetck.lib.AbstractSleeTCKTest;
import com.opencloud.sleetck.lib.TCKTestResult;
import com.opencloud.sleetck.lib.resource.TCKActivityID;
import com.opencloud.sleetck.lib.resource.events.TCKResourceEventX;
import com.opencloud.sleetck.lib.testutils.Assert;
import com.opencloud.sleetck.lib.testutils.jmx.SleeManagementMBeanProxy;
import com.opencloud.sleetck.lib.testutils.jmx.TraceMBeanProxy;
import com.opencloud.sleetck.lib.testutils.jmx.impl.TraceMBeanProxyImpl;

/**
 * Assertion ID (1113118): Test The effects of operations invokedon 
 * this facility occur regardless of the state or outcome of any enclosing 
 * transaction.
 */
public final class Test1113118Test extends AbstractSleeTCKTest {
    public TCKTestResult run() throws Exception {
        TCKActivityID activity = utils().getResourceInterface().createActivity("Test1113118Activity");

        receivedTraceNotifications = 0;
        expectedTraceNotifications = 4; // Each event will produce two traces

        // fire 2 events
        utils().getResourceInterface().fireEvent(TCKResourceEventX.X1, null, activity, null);
        utils().getResourceInterface().fireEvent(TCKResourceEventX.X1, null, activity, null);
        synchronized (this) {
            wait(utils().getTestTimeout());
        }

        Assert.assertTrue(1113118, "Expected number of trace messages not received, traces were not delivered by "
                + "the TraceFacility (expected " + expectedTraceNotifications + ", received "
                + receivedTraceNotifications + ")", expectedTraceNotifications == receivedTraceNotifications);

        return TCKTestResult.passed();
    }

    public void setUp() throws Exception {
        super.setUp();
        SleeManagementMBeanProxy proxy = utils().getSleeManagementMBeanProxy();
        traceMBeanName = proxy.getTraceMBean();
        listener = new TraceNotificationListenerImpl();

        tracembean = new TraceMBeanProxyImpl(traceMBeanName, utils().getMBeanFacade());
        tracembean.addNotificationListener(listener, null, null);
    }

    public void tearDown() throws Exception {
        if (null != tracembean)
            tracembean.removeNotificationListener(listener);
        super.tearDown();
    }

    public class TraceNotificationListenerImpl implements NotificationListener {
        public final void handleNotification(Notification notification, Object o) {
            if (notification instanceof TraceNotification) {
                TraceNotification n = (TraceNotification) notification;
                getLog().info("Got Trace: " + n);
                if (n.getType().equals(SbbNotification.TRACE_NOTIFICATION_TYPE))
                    receivedTraceNotifications++;
            }
        }
    }

    private ObjectName traceMBeanName;

    private NotificationListener listener;

    private TraceMBeanProxy tracembean;

    private int expectedTraceNotifications;

    private int receivedTraceNotifications;
}
