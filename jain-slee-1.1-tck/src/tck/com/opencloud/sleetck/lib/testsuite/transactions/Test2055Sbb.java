/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.transactions;

import com.opencloud.sleetck.lib.resource.events.TCKResourceEventX;
import com.opencloud.sleetck.lib.sbbutils.BaseTCKSbb;
import com.opencloud.sleetck.lib.sbbutils.TCKSbbUtils;

import javax.slee.ActivityContextInterface;
import javax.slee.RolledBackContext;
import javax.slee.facilities.Level;
import java.util.HashMap;

public abstract class Test2055Sbb extends BaseTCKSbb {

    public void sbbExceptionThrown(Exception exception, Object event, ActivityContextInterface aci) {
        try {
            HashMap map = new HashMap();

            Object currentTransaction = TCKSbbUtils.getResourceAdaptorInterface().getTransactionIDAccess().getCurrentTransactionID();
            map.put("TXNID2", currentTransaction);
            TCKSbbUtils.getResourceInterface().sendSbbMessage(map);
        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }
    }

    // Initial event
    public void onTCKResourceEventX1(TCKResourceEventX ev, ActivityContextInterface aci) {

        try {
            HashMap map = new HashMap();

            Object currentTransaction = TCKSbbUtils.getResourceAdaptorInterface().getTransactionIDAccess().getCurrentTransactionID();
            map.put("TXNID", currentTransaction);
            TCKSbbUtils.getResourceInterface().sendSbbMessage(map);
        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }

        throw new RuntimeException("Test2055Exception");
    }

    // Override sbbRolledBack to write a trace message rather than throw an Exception, as we expect to receive the call
    public void sbbRolledBack(RolledBackContext context) {
        createTraceSafe(Level.INFO,"Received expected sbbRolledBack() call");
    }

}
