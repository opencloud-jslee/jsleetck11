/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.events.servicestarted;

import java.rmi.RemoteException;
import java.util.HashMap;

import javax.slee.management.DeployableUnitID;
import javax.slee.management.SleeState;
import javax.slee.management.SleeStateChangeNotification;

import com.opencloud.sleetck.lib.AbstractSleeTCKTest;
import com.opencloud.sleetck.lib.OperationTimedOutException;
import com.opencloud.sleetck.lib.TCKTestResult;
import com.opencloud.sleetck.lib.resource.TCKActivityID;
import com.opencloud.sleetck.lib.resource.TCKSbbMessage;
import com.opencloud.sleetck.lib.resource.testapi.TCKResourceListener;
import com.opencloud.sleetck.lib.testutils.BaseTCKResourceListener;
import com.opencloud.sleetck.lib.testutils.FutureResult;
import com.opencloud.sleetck.lib.testutils.jmx.SleeManagementMBeanProxy;

public class Test2185Test extends AbstractSleeTCKTest {

    private static final String SERVICE_DU_PATH_PARAM = "DUPath";
    private static final int TEST_ID = 2185;

    /**
     * Perform the actual test.
     */
    public TCKTestResult run() throws Exception {
        result = new FutureResult(getLog());

        // Start the DU's service.
        utils().activateServices(duID, true);

        return result.waitForResultOrFail(utils().getTestTimeout(), "Timeout waiting for test result", TEST_ID);
    }

    /**
     * Do all the pre-run configuration of the test.
     */
    public void setUp() throws Exception {

        resourceListener = new TCKResourceListenerImpl();
        setResourceListener(resourceListener);

        stateListener = new QueuingSleeStateListener(utils());
        management = utils().getSleeManagementMBeanProxy();
        management.addNotificationListener(stateListener, null, null);

        getLog().fine("Installing and activating service");

        // Install the Deployable Units
        String duPath = utils().getTestParams().getProperty(SERVICE_DU_PATH_PARAM);
        duID = utils().install(duPath);

    }

    /**
     * Clean up after the test.
     */
    public void tearDown() throws Exception {
        super.tearDown();
    }

    private class TCKResourceListenerImpl extends BaseTCKResourceListener {
        public synchronized void onSbbMessage(TCKSbbMessage message, TCKActivityID activity) throws RemoteException {
            getLog().info("Received message from SBB: " + message.getMessage());

            if (count == 0) {
                count++;

                // Stop the SLEE, start the SLEE.

                try {
                    management.stop();
                    waitForStateChange(SleeState.STOPPED);

                    management.start();
                } catch (Exception e) {
                    onException(e);
                    return;
                }

                // At some point after this the ServiceStartedEvent should be
                // fired.
                return;
            }

            HashMap map = (HashMap) message.getMessage();
            Boolean passed = (Boolean) map.get("Result");
            String msgString = (String) map.get("Message");

            if (passed.booleanValue() == true)
                result.setPassed();
            else
                result.setFailed(TEST_ID, msgString);

        }

        public void onException(Exception e) throws RemoteException {
            getLog().warning("Received exception from SBB.");
            getLog().warning(e);
            result.setError(e);
        }

        private void waitForStateChange(SleeState expectedState) throws Exception {
            SleeStateChangeNotification stateChange = null;
            try {
                while (stateChange == null || !stateChange.getNewState().equals(expectedState)) {
                    getLog().fine("Waiting to move to the " + expectedState    + " state.");

                    stateChange = stateListener.nextNotification();
                    getLog().fine("SLEE has moved to the " + stateChange + " state.");
                }
            } catch (OperationTimedOutException e) {
                onException(e);
            }

        }

    }

    private DeployableUnitID duID;
    private TCKResourceListener resourceListener;
    private FutureResult result;
    private int count = 0;
    private QueuingSleeStateListener stateListener;
    private SleeManagementMBeanProxy management;
}
