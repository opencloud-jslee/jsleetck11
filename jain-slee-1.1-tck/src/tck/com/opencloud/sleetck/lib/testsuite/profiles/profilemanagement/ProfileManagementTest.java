/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.profiles.profilemanagement;

import com.opencloud.sleetck.lib.*;
import com.opencloud.sleetck.lib.testutils.jmx.*;
import com.opencloud.sleetck.lib.testsuite.profiles.ProfileTestConstants;
import com.opencloud.sleetck.lib.testutils.ProfileUtils;
import com.opencloud.sleetck.lib.testutils.ComponentIDLookup;
import com.opencloud.sleetck.lib.testutils.Assert;
import com.opencloud.sleetck.lib.infra.SleeTCKComponentConstants;

import javax.slee.profile.*;
import javax.slee.InvalidStateException;

import javax.management.ObjectName;
import javax.management.Attribute;
import javax.management.MBeanException;
import java.util.Vector;
import java.util.Iterator;


public class ProfileManagementTest extends AbstractSleeTCKTest {

    protected static final String PROFILE_TABLE_NAME = "tck.ProfileManagementTest.table";
    public static final String PROFILE_SPEC_NAME = "PMProfile";

    /**
     * Runs some tests on javax.slee.profile.ProfileManagement methods <code>markProfileDirty</code> and
     * <code>isProfileValid</code>.
     * @return
     * @throws Exception
     */
    public TCKTestResult run() throws Exception {
        // create the profile table
        ProfileProvisioningMBeanProxy profileProvisioning = profileUtils.getProfileProvisioningProxy();
        ProfileSpecificationID profileSpecID = new ComponentIDLookup(utils()).lookupProfileSpecificationID(
                PROFILE_SPEC_NAME, SleeTCKComponentConstants.TCK_VENDOR, "1.0");

        getLog().info("Creating profile table: " + PROFILE_TABLE_NAME);

        profileProvisioning.createProfileTable(profileSpecID, PROFILE_TABLE_NAME);
        tableCreated = true;

        // add profile
        final String name = "A";
        getLog().info("Creating profile " + name);
        // The handle returned here is read-write by default.
        ObjectName jmxObjectName = profileProvisioning.createProfile(PROFILE_TABLE_NAME, name);
        PMProfileManagementProxy proxy = getProfileProxy(jmxObjectName);

        // Need to commit the newly created profile - the profiles don't exist outside the object name returned by createProfile() until committed.
        proxy.commitProfile();
        proxy.closeProfile();

        // Get a new handle - this should be read-only by default
        jmxObjectName = profileProvisioning.getProfile(PROFILE_TABLE_NAME, name);
        proxy = getProfileProxy(jmxObjectName);

        // Verify that the profile is read-only
        Assert.assertTrue(2267, "isProfileWriteable() should have returned false for a profile retrieved via getProfile() that hasn't subsequently had editProfile() called", !proxy.isProfileWriteable());

        // Assertion 1032: markProfileDirty will explicitly mark a profile as dirty
        //proxy.testMarkDirty();
        proxy.editProfile();
        proxy.testMarkDirty();
        Assert.assertTrue(1032, "markProfileDirty() should have explicitly set the profiles dirty flag",
                          proxy.isProfileDirty());

        Assert.assertTrue(1040, "isProfileValid() should return true for a valid profile",
                          proxy.testIsProfileValid(new ProfileID(PROFILE_TABLE_NAME, "A")));
        Assert.assertTrue(1040, "isProfileValid() should return false for a profile that does not exist",
                          !proxy.testIsProfileValid(new ProfileID(PROFILE_TABLE_NAME, "X")));
        proxy.restoreProfile();

        // Assertion 1085: Attempt to invoke a setter method on a non-writable profile will throw InvalidStateException
        // Test calling setValue() via invoke() (using proxy class)
        try {
            proxy.setValue("cannot set");
            Assert.fail(1085, "Did not get an exception when attempting to invoke setter on a profile in read only " +
                              "state");
        } catch (InvalidStateException e) {
            getLog().info("got expected InvalidStateException");
        } catch (Exception e) {
            Assert.fail(1085, "Got unexpected exception when attempting to invoke setter on a profile in read only " +
                              "state: " + e);
        }

        // Test using setAttribute()
        try {
            utils().getMBeanFacade().setAttribute(jmxObjectName, new Attribute("Value", "cannot set"));
        } catch (MBeanException e) {
            if (e.getTargetException() instanceof javax.slee.InvalidStateException) {
                getLog().info("got expected InvalidStateException");
            } else {
                Assert.fail(1085, "Got unexpected exception when using setAttribute() to update a CMP attribute on a " +
                                  "profile in a read only state: " + e);
            }
        } catch (Exception e) {
            Assert.fail(1085, "Got unexpected exception when using setAttribute() to update a CMP attribute on a " +
                              "profile in a read only state: " + e);
        }

        // Test invoking via a management method
        try {
            proxy.setValueIndirectly("cannot set");
            Assert.fail(1085, "Did not get an exception when a management method attemped to invoke setter on a " +
                              "profile in read only state");
        } catch (InvalidStateException e) {
            getLog().info("got expected InvalidStateException");
        } catch (Exception e) {
            Assert.fail(1085, "Got unexpected exception when a management method attemps to invoke setter on a " +
                              "profile in read only state: " + e);
        }

        return TCKTestResult.passed();
    }

    public void setUp() throws Exception {
        String duPath = utils().getTestParams().getProperty(ProfileTestConstants.PROFILE_SPEC_DU_PATH_PARAM);
        getLog().fine("Installing profile specification");
        utils().install(duPath);
        profileUtils = new ProfileUtils(utils());
        activeProxies = new Vector();
    }

    public void tearDown() throws Exception {
        // close profiles
        if (activeProxies != null) {
            getLog().fine("Closing profiles");
            Iterator activeProxiesIter = activeProxies.iterator();
            while (activeProxiesIter.hasNext()) {
                PMProfileManagementProxy aProxy = (PMProfileManagementProxy) activeProxiesIter.next();
                try {
                    if (aProxy != null) {
                        if (aProxy.isProfileWriteable()) aProxy.restoreProfile();
                        aProxy.closeProfile();
                    }
                } catch (Exception ex) {
                    getLog().warning("Exception caught while trying to close profiles:");
                    getLog().warning(ex);
                }
            }
        }
        // delete profile tables
        try {
            if (profileUtils != null && tableCreated) {
                getLog().fine("Removing profile table");
                profileUtils.removeProfileTable(PROFILE_TABLE_NAME);
            }
        } catch (Exception e) {
            getLog().warning("Caught exception while trying to remove profile table:");
            getLog().warning(e);
        }
        super.tearDown();
    }

    private PMProfileManagementProxy getProfileProxy(ObjectName mbeanName) {
        PMProfileManagementProxy rProxy = new PMProfileManagementProxy(mbeanName, utils().getMBeanFacade());
        activeProxies.addElement(rProxy);
        return rProxy;
    }

    private ProfileUtils profileUtils;
    private boolean tableCreated = false;
    private Vector activeProxies;

}
