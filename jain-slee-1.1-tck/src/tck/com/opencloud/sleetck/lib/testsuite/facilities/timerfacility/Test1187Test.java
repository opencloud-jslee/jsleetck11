/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.facilities.timerfacility;

import com.opencloud.sleetck.lib.AbstractSleeTCKTest;
import com.opencloud.sleetck.lib.TCKTestResult;
import com.opencloud.sleetck.lib.resource.TCKActivityID;
import com.opencloud.sleetck.lib.resource.events.TCKResourceEventX;
import com.opencloud.sleetck.lib.testutils.BaseTCKResourceListener;

import java.rmi.RemoteException;

/**
 * Test assertion 1187 : that the timer facility will release all its references to an activity when the activity is
 * ended.  An Sbb creates a null activity, and assigns it to a timer, then ends the activity.  The test fails if the
 * timer event is ever received.
 */
public class Test1187Test extends AbstractSleeTCKTest {
    public TCKTestResult run() throws Exception {
        setResourceListener(new ResourceListener());
        TCKActivityID activity = utils().getResourceInterface().createActivity("Test1187Activity");
        utils().getResourceInterface().fireEvent(TCKResourceEventX.X1, null, activity, null);

        synchronized (stateLock) {
            stateLock.wait(testTimeout);
            if(exceptionReceived != null) return TCKTestResult.error("An Exception was received from the SBB or the TCK resource");
            else if(timerEventReceived) return TCKTestResult.failed(1187, "A timer event was delivered after the associated " +
                    "activity has ended");
            else return TCKTestResult.passed();
        }
    }

    public void setUp() throws Exception {
        testTimeout = Integer.parseInt(utils().getTestParams().getProperty("waitPeriodMS"));
        timerEventReceived = false;
        exceptionReceived = null;
        super.setUp();
    }

    public class ResourceListener extends BaseTCKResourceListener {
        public Object onSbbCall(Object args) {
            getLog().info("onSbbCall: SBB reveived a TimerEvent on the NullActivity");
            synchronized (stateLock) {
                timerEventReceived = true;
                stateLock.notifyAll();
            }
            return null;
        }

        public void onException(Exception exception) throws RemoteException {
            synchronized(stateLock) {
                exceptionReceived = exception;
                stateLock.notifyAll();
            }
        }
    }

    private Object stateLock = new Object();
    private boolean timerEventReceived = false;
    private long testTimeout;
    private Exception exceptionReceived;

}
