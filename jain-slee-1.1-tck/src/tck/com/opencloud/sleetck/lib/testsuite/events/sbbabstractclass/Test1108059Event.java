/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.events.sbbabstractclass;

import java.io.Serializable;
import java.util.Random;

public final class Test1108059Event implements Serializable {
    public Test1108059Event() {
        // generate random id
        id = new Random().nextLong() ^ System.currentTimeMillis();
    }

    public boolean equals(Object o) {
        if (o == this)
            return true;
        return (o instanceof Test1108059Event) && ((Test1108059Event) o).id == id;
    }

    public int hashCode() {
        return (int) id;
    }

    public String toString() {
        return "Test1108059Event[" + id + "]";
    }

    private final long id;
}
