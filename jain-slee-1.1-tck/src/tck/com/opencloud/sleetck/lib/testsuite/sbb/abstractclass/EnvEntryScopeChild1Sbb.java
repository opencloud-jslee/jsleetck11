/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.sbb.abstractclass;

import javax.naming.*;

/**
 * Test scoping of environment entries
 */
public abstract class EnvEntryScopeChild1Sbb extends SendResultsSbb {
    public boolean checkEnvEntries() throws Exception {
        Context context = (Context)new InitialContext().lookup("java:comp/env");

        String s = (String)context.lookup("teststring");
        if (!s.equals("child sbb")) {
            setResultFailed(496, "teststring environment variable does not contain expected value 'parent sbb', instead it has '" + s + "'");
            return false;
        }

        try {
            context.lookup("testboolean");
            setResultFailed(496, "teststring environment variable present when it shouldn't be");
            return false;
        }
        catch (NameNotFoundException nnfe) {}

        return true;
    }
}

