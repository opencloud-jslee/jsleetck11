/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.management.sleestate;

import com.opencloud.sleetck.lib.resource.sbbapi.TCKActivity;
import com.opencloud.sleetck.lib.resource.TCKActivityID;
import com.opencloud.sleetck.lib.resource.events.TCKResourceEventX;
import com.opencloud.sleetck.lib.sbbutils.TCKSbbUtils;
import com.opencloud.sleetck.lib.sbbutils.BaseTCKSbb;
import javax.slee.ActivityContextInterface;
import javax.slee.facilities.Level;

/**
 * A simple Sbb which defines a single event handler method as
 * a means of getting attached to an activity.
 */
public abstract class SleeStateMachineTestSbb extends BaseTCKSbb {

    public void onTCKResourceEventX1(TCKResourceEventX event, ActivityContextInterface aci) {
        try {
            // create a trace message
            TCKActivity activity = (TCKActivity)aci.getActivity();
            TCKActivityID activityID = activity.getID();
            String traceMessage = "Received TCKResourceEventX.X1 event on activity "+activityID;
            TCKSbbUtils.createTrace(getSbbID(),Level.FINE,traceMessage,null);
            // send an ACK to the test
            TCKSbbUtils.getResourceInterface().sendSbbMessage(TCKResourceEventX.X1);
        } catch(Exception e) {
            TCKSbbUtils.handleException(e);
        }
    }

}
