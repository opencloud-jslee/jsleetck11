/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.profiles.lifecycle;

import java.util.HashMap;

import javax.slee.CreateException;
import javax.slee.profile.Profile;
import javax.slee.profile.ProfileContext;
import javax.slee.profile.ProfileVerificationException;

import com.opencloud.logging.Logable;
import com.opencloud.logging.StdErrLog;
import com.opencloud.sleetck.lib.profileutils.BaseMessageSender;
import com.opencloud.sleetck.lib.rautils.RMIObjectChannel;
import com.opencloud.sleetck.lib.rautils.TCKRAUtils;

/**
 *
 */
public abstract class Test1110227Profile implements Test1110227ProfileCMP, Profile  {

    public static final int setProfileContextCalledAfterConstructor = 1110234;
    public static final int setProfileContextNoCMPAccess = 1110236;
    public static final int unsetProfileContextNoCMPAccess = 1110240;
    public static final int profileInitializeCMPDefaultValues = 1110245;
    public static final int profileRemoveCalledBeforeRemoveCMP = 1110261;
    public static final int profileRemoveStillInReadyState = 1110263;
    public static final int profileVerifyCalled = 1110276;
    public static final int profileVerifyInReadyState = 1110278;
    public static final int profileVerifyAfterStore = 1110280;
    public static final int profileVerifyTXNContext = 1110280;
    public static final int profileVerifyOnlyCalledForManagement = 1110281;
    public static final int profileVerifyNotOnDefaultProfile = 1110282;
    public static final int profileInitCalledInPooledState =  1110289;
    public static final int profilePostCreateCalledInPooledState =  1110289;
    public static final int profileInitCalledForDefaultProfile = 1110290;
    public static final int businessMethodCalledInReadyState = 1110294;
    public static final int profileLoadCalledInReadyState = 1110294;
    public static final int profileStoreCalledInReadyState = 1110294;
    public static final int profileObjectAssocOneTable = 1110304;
    public static final int profilePassivateNoCMPAccess = 1110259;
    public static final int passivateCalledAfterStore = 1110296;
    public static final int passivateCalledInReadyState = 1110296;
    public static final int profileActivateNoCMPAccess = 1110255;
    public static final int activateCalledInPooledState = 1110293;
    public static final int notPreceededByInitializeCantBeDefaultProfile = 1110291;
    public static final int unsetProfileContextCalledInPooledState = 1110698;




    public Test1110227Profile() {
        Logable log = new StdErrLog();

        try {
            out = TCKRAUtils.lookupRMIObjectChannel();
            msgSender = new BaseMessageSender(out, log);
        }
        catch (Exception e) {
            log.error("An error occured creating an RMIObjectChannel:");
            log.error(e);
        }

        profileTableName = null;

        methodTrace = AFTER_CONSTRUCTOR;
        stateTrace = AFTER_CONSTRUCTOR;
    }

    public void setProfileContext(ProfileContext context) {
        context.getTracer("setProfileContext").info("setProfileContext called");

        //1110234: setProfileContext is called after creating a new profile object instance
        if (methodTrace!=AFTER_CONSTRUCTOR) {
            methodTrace = INVALID;
            msgSender.sendFailure(setProfileContextCalledAfterConstructor, "'SetProfileContext()' was not correctly called immediately after constructor call.");
            return;
        }
        msgSender.sendSuccess( setProfileContextCalledAfterConstructor, "'SetProfileContext()' was correctly called immediately after constructor call.");
        methodTrace = AFTER_SET_CONTEXT;
        stateTrace = POOLED;

        //1110236: CMP accessor may not be called here
        try {
            getValue();
            msgSender.sendFailure( setProfileContextNoCMPAccess, "Calling 'getValue()' within 'setProfileContext' " +
                    "succeeded but should have caused an exception.");
        }
        catch (IllegalStateException e) {
            msgSender.sendSuccess( setProfileContextNoCMPAccess, "setProfileContext(): Caught exception as expected: "+e);
        }
        catch (Exception e) {
            msgSender.sendFailure( setProfileContextNoCMPAccess, "Wrong type of exception thrown: "+e.getClass().getName()+
                    " Expected: java.lang.IllegalStateException");
        }

        this.context = context;
        profileTableName = context.getProfileTableName();
    }

    public void unsetProfileContext() {
        context.getTracer("unsetProfileContext").info("unsetProfileContext called");

        //1110698: Part of Transition from Pooled to DoesntExist state
        if (stateTrace == POOLED)
            msgSender.sendSuccess( unsetProfileContextCalledInPooledState, "'unsetProfileContext': Called in Pooled state.");
        else
            msgSender.sendFailure( unsetProfileContextCalledInPooledState, "'unsetProfileContext': Not called in Pooled state.");

        //1110304: A profile object is only ever associated with one profile table
        if (!profileTableName.equals(context.getProfileTableName()))
            msgSender.sendFailure( profileObjectAssocOneTable, "Varying profile table name found. A profile object may only be associated " +
                    "with one and the same profile table during its lifetime.");

        //1110240: CMP accessor may not be called here
        try {
            getValue();
            msgSender.sendFailure( unsetProfileContextNoCMPAccess, "Calling 'getValue()' within 'unsetProfileContext' " +
                    "succeeded but should have caused an exception.");
        }
        catch (IllegalStateException e) {
            msgSender.sendSuccess( unsetProfileContextNoCMPAccess, "unsetProfileContext(): Caught exception as expected: "+e);
        }
        catch (Exception e) {
            msgSender.sendFailure( unsetProfileContextNoCMPAccess, "Wrong type of exception thrown: "+e.getClass().getName()+
                    " Expected: java.lang.IllegalStateException");
        }

        context = null;

        methodTrace = AFTER_UNSET_CONTEXT;
        stateTrace = DOESNTEXIST;
    }

    public void profileInitialize() {
        context.getTracer("profileInitialize").info("profileInitialize called");

        //1110304: A profile object is only ever associated with one profile table
        if (!profileTableName.equals(context.getProfileTableName()))
            msgSender.sendFailure( profileObjectAssocOneTable, "Varying profile table name found. A profile object may only be associated " +
                    "with one and the same profile table during its lifetime.");


        //1110289: Part of Transition from Pooled to Ready state
        if (stateTrace == POOLED)
            msgSender.sendSuccess( profileInitCalledInPooledState, "'profileInitialize': Called in Pooled state.");
        else
            msgSender.sendFailure( profileInitCalledInPooledState, "'profileInitialize': Not called in Pooled state.");

        //1110245: The SLEE guarantees that the get accessors initially return the java language default values
        int std_int = this.getValue();
        long std_long = this.getLongValue();
        boolean std_bool = this.getBoolValue();
        String std_Str = this.getStringValue();
        HashMap std_obj = this.getObjValue();
        if (std_int==0 && std_long==0 && !std_bool && std_Str==null && std_obj==null)
            msgSender.sendSuccess( profileInitializeCMPDefaultValues, "'profileInitialize': CMP get methods initially returned Java language defaults as expected.");
        else
            msgSender.sendFailure( profileInitializeCMPDefaultValues, "'profileInitialize': CMP get methods did NOT initially return Java language defaults.");

        setValue(2);
        setValue2(8);
        setStringValue(Test1110227Sbb.INIT);

        //1110290: check that profileInitialize is only called for the default profile
        try {
            context.getProfileName();
            msgSender.sendFailure( profileInitCalledForDefaultProfile, "profileInitialize may only be called for a profile table's default profile.");
        }
        catch (Exception e) {
            msgSender.sendSuccess( profileInitCalledForDefaultProfile, "profileInitialize has been called for a profile table's default profile.");
        }

        methodTrace = AFTER_PROFILE_INIT;
        stateTrace = AFTER_PROFILE_INIT;
    }

    public void profilePostCreate() throws CreateException {
        context.getTracer("profilePostCreate").info("profilePostCreate called");

        //1110304: A profile object is only ever associated with one profile table
        if (!profileTableName.equals(context.getProfileTableName()))
            msgSender.sendFailure( profileObjectAssocOneTable, "Varying profile table name found. A profile object may only be associated " +
                    "with one and the same profile table during its lifetime.");


        //1110289: Part of Transition from Pooled to Ready state
        if (stateTrace == POOLED || stateTrace == AFTER_PROFILE_INIT)
            msgSender.sendSuccess( profilePostCreateCalledInPooledState, "'profilePostCreate': Called in Pooled state or directly after profileInitialize.");
        else
            msgSender.sendFailure( profilePostCreateCalledInPooledState, "'profilePostCreate': Not called in Pooled state or directly after profileInitialize.");

        //1110291: If profilePostCreate has not been preceeded by a call to profileInitialize() the
        //associated profile may not be the default profile
        if (methodTrace!=AFTER_PROFILE_INIT && context.getProfileName()==null)
                msgSender.sendFailure( notPreceededByInitializeCantBeDefaultProfile, "If profilePostCreate has not been preceeded by a call to" +
                        " profileInitialize the associated profile can't be a profile table's default profile.");

        methodTrace = AFTER_POST_CREATE;
        stateTrace = READY;
    }

    public void profileActivate() {
        context.getTracer("profileActivate").info("profileActivate called");

        //1110304: A profile object is only ever associated with one profile table
        if (!profileTableName.equals(context.getProfileTableName()))
            msgSender.sendFailure( profileObjectAssocOneTable, "Varying profile table name found. A profile object may only be associated " +
                    "with one and the same profile table during its lifetime.");

        //1110293: Part of Transition from Pooled to Ready state
        if (stateTrace == POOLED)
            msgSender.sendSuccess( activateCalledInPooledState, "'profileActivate': Called in Pooled state.");
        else
            msgSender.sendFailure( activateCalledInPooledState, "'profileActivate': Not called in Pooled state.");

        //1110255: CMP accessor may not be called here
        try {
            getValue();
            msgSender.sendFailure( profileActivateNoCMPAccess, "Calling 'getValue()' within 'profileActivate' " +
                    "succeeded but should have caused an exception.");
        }
        catch (IllegalStateException e) {
            msgSender.sendSuccess( profileActivateNoCMPAccess, "profileActivate(): Caught exception as expected: "+e);
        }
        catch (Exception e) {
            msgSender.sendFailure( profileActivateNoCMPAccess, "Wrong type of exception thrown: "+e.getClass().getName()+
                    " Expected: java.lang.IllegalStateException");
        }

        methodTrace = AFTER_ACTIVATE;
        stateTrace = READY;
    }

    public void profilePassivate() {
        context.getTracer("profilePassivate").info("profilePassivate called");

        //1110304: A profile ojbect is only ever associated with one profile table
        if (!profileTableName.equals(context.getProfileTableName()))
            msgSender.sendFailure( profileObjectAssocOneTable, "Varying profile table name found. A profile object may only be associated " +
                    "with one and the same profile table during its lifetime.");

        //1110296: Transition from Ready to Pooled state
        if (stateTrace == READY)
            msgSender.sendSuccess( passivateCalledInReadyState, "'profilePassivate': Called in READY state.");
        else
            msgSender.sendFailure( passivateCalledInReadyState, "'profilePassivate': Not called in READY state.");

        //1110296: Call must have been preceeded by a call to profileStore()
        if (methodTrace == AFTER_STORE)
            msgSender.sendSuccess( passivateCalledAfterStore, "'profilePassivate' has correctly been preceeded by a call to profileStore.");
        else
            msgSender.sendFailure( passivateCalledAfterStore, "'profilePassivate' should have been preceeded by a call to profileStore.");

        //1110259: CMP accessor may not be called here
        try {
            getValue();
            msgSender.sendFailure( profilePassivateNoCMPAccess, "Calling 'getValue()' within 'profilePassivate' " +
                    "succeeded but should have caused an exception.");
        }
        catch (IllegalStateException e) {
            msgSender.sendSuccess( profilePassivateNoCMPAccess, "profilePassivate(): Caught exception as expected: " + e);
        }
        catch (Exception e) {
            msgSender.sendFailure( profilePassivateNoCMPAccess, "Wrong type of exception thrown: " + e.getClass().getName() +
                    " Expected: java.lang.IllegalStateException");
        }

        methodTrace = AFTER_PASSIVATE;
        stateTrace = POOLED;
    }

    public void profileLoad() {
        context.getTracer("profileLoad").info("profileLoad called");

        //1110304: A profile object is only ever associated with one profile table
        if (!profileTableName.equals(context.getProfileTableName()))
            msgSender.sendFailure( profileObjectAssocOneTable, "Varying profile table name found. A profile object may only be associated " +
                    "with one and the same profile table during its lifetime.");

        //1110294: profileLoad and profileStore can be invoked for synchronization in the READY state
        if (stateTrace == READY)
            msgSender.sendSuccess( profileLoadCalledInReadyState, "'profileLoad': Called in Ready state.");
        else
            msgSender.sendFailure( profileLoadCalledInReadyState, "'profileLoad': Not called in Ready state.");

        methodTrace = AFTER_LOAD;
        stateTrace = READY;
    }

    public void profileStore() {
        context.getTracer("profileStore").info("profileStore called");

        //1110304: A profile object is only ever associated with one profile table
        if (!profileTableName.equals(context.getProfileTableName()))
            msgSender.sendFailure( profileObjectAssocOneTable, "Varying profile table name found. A profile object may only be associated " +
                    "with one and the same profile table during its lifetime.");

        //1110294: profileLoad and profileStore can be invoked for synchronization in the READY state
        if (stateTrace == READY)
            msgSender.sendSuccess( profileStoreCalledInReadyState, "'profileStore': Called in Ready state.");
        else
            msgSender.sendFailure( profileStoreCalledInReadyState, "'profileStore': Not called in Ready state.");

        methodTrace= AFTER_STORE;
        stateTrace = READY;
    }

    public void profileRemove() {
        context.getTracer("profileRemove").info("profileRemove called");

        //1110304: A profile ojbect is only ever associated with one profile table
        if (!profileTableName.equals(context.getProfileTableName()))
            msgSender.sendFailure( profileObjectAssocOneTable, "Varying profile table name found. A profile object may only be associated " +
                    "with one and the same profile table during its lifetime.");

        //1110261: The SLEE invokes the profileRemove method on a Profile object before the SLEE
        //removes the Profile which the Profile object is assigned to.
        //i.e. the CMP fields' values are still available here
        if (getValue()==2 && getValue2()==8)
            msgSender.sendSuccess( profileRemoveCalledBeforeRemoveCMP, "'profileRemove()' was correctly called before the profile which the " +
                    "profile object was assigned to was removed.");
        else
            msgSender.sendFailure( profileRemoveCalledBeforeRemoveCMP, "'profileRemove()': the profile that this profile objects was" +
                    " assigned to should be removed AFTER this method completed, thus the CMP accessors should " +
                    "still have been accessible at this stage.");

        //1110263: The Profile object is in the Ready state when profileRemove is invoked and it will enter
        //the Pooled state after the method returns.
        if (stateTrace == READY)
            msgSender.sendSuccess( profileRemoveStillInReadyState, "profileRemove was called on profile object in READY state.");
        else
            msgSender.sendFailure( profileRemoveStillInReadyState, "profileRemove may only be called on profile object in READY state.");

        methodTrace = AFTER_REMOVE;
        stateTrace = POOLED;
    }

    public void profileVerify() throws ProfileVerificationException {
        context.getTracer("profileVerify").info("profileVerify called");

        //1110304: A profile ojbect is only ever associated with one profile table
        if (!profileTableName.equals(context.getProfileTableName()))
            msgSender.sendFailure( profileObjectAssocOneTable, "Varying profile table name found. A profile object may only be associated " +
                    "with one and the same profile table during its lifetime.");

        //1110276: The SLEE invokes the profileVerify method on a Profile object after the
        //management client has invoked the commit-Profile method on a Profile MBean
        msgSender.sendSuccess( profileVerifyCalled, "profileVerify has been called.");

        //1110278: This method is invoked on a profile in the READY state
        if (stateTrace == READY)
            msgSender.sendSuccess( profileVerifyInReadyState, "profileVerify was called on profile object in READY state.");
        else
            msgSender.sendFailure( profileVerifyInReadyState, "profileVerify may only be called on profile object in READY state.");

        //1110280: This method has to be invoked after the profileStore method...
        if (methodTrace == AFTER_STORE)
            msgSender.sendSuccess( profileVerifyAfterStore, "profileVerify was called after profileStore.");
        else
            msgSender.sendFailure( profileVerifyAfterStore, "profileVerify() should have been called after a call to profileStore().");


        //1110280: ...it is running in a TXN context (thus rollback can be initiated)
        //and this TXN context must be the same as the context begun by the management clients editProfile method
        //2 CMP changes happened: 1st: set to 'Rollback' after 'editProfile' but before 'commit'. 2nd: set to 'Modified' as seen below
        //check that marking current TXN context for rollback causes BOTH changes to be reverted to the initial value 'Init'
        if (getStringValue().equals(Test1110227Sbb.ROLLBACK)) {
            setStringValue(Test1110227Sbb.MODIFIED);
            context.setRollbackOnly();
        }

        methodTrace = AFTER_VERIFY;
        stateTrace = READY;
    }

    public boolean business() {
        context.getTracer("business").fine("Business method called.");

        //1110304: A profile object is only ever associated with one profile table
        if (!profileTableName.equals(context.getProfileTableName()))
            msgSender.sendFailure( profileObjectAssocOneTable, "Varying profile table name found. A profile object may only be associated " +
                    "with one and the same profile table during its lifetime.");

        //1110294: business and management methods are invoked in the READY state
        if (stateTrace == READY)
            msgSender.sendSuccess( businessMethodCalledInReadyState, "Business method called in Ready state.");
        else
            msgSender.sendFailure( businessMethodCalledInReadyState, "Business method not called in Ready state.");

        return (getValue()==2 && getValue2()==8);
    }

    private ProfileContext context;

    private int stateTrace;
    private int methodTrace;


    public static final int INVALID = -1;
    public static final int AFTER_CONSTRUCTOR = 1;
    public static final int AFTER_SET_CONTEXT = 2;
    public static final int AFTER_PROFILE_INIT = 4;
    public static final int AFTER_POST_CREATE  = 5;
    public static final int AFTER_ACTIVATE = 6;
    public static final int AFTER_LOAD = 7;
    public static final int AFTER_VERIFY = 8;
    public static final int AFTER_STORE = 9;
    public static final int AFTER_PASSIVATE = 10;
    public static final int AFTER_REMOVE = 11;
    public static final int AFTER_UNSET_CONTEXT = 12;

    public static final int DOESNTEXIST = 0;
    public static final int POOLED = 14;
    public static final int READY = 15;

    private RMIObjectChannel out;
    private BaseMessageSender msgSender;

    private String profileTableName;
}
