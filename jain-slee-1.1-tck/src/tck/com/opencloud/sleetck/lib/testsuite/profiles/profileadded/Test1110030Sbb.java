/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.profiles.profileadded;

import java.util.HashMap;

import javax.slee.ActivityContextInterface;
import javax.slee.RolledBackContext;
import javax.slee.TransactionRolledbackLocalException;
import javax.slee.profile.ProfileUpdatedEvent;

import com.opencloud.sleetck.lib.sbbutils.BaseTCKSbb;
import com.opencloud.sleetck.lib.sbbutils.TCKSbbUtils;
import com.opencloud.sleetck.lib.sbbutils.events2.SbbBaseMessageComposer;

public abstract class Test1110030Sbb extends BaseTCKSbb {

    private void sendLogMsgCall (String msg) {
        HashMap map = SbbBaseMessageComposer.getLogMsg(msg);
        try {
            TCKSbbUtils.getResourceInterface().callTest(map);
        }
        catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }
    }

    private void sendResult(boolean result, int id, String msg) {
        HashMap map = SbbBaseMessageComposer.getSetResultMsg(result, id, msg);
        try {
            TCKSbbUtils.getResourceInterface().sendSbbMessage(map);

        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }
    }

    public void onProfileUpdatedEvent(ProfileUpdatedEvent event, ActivityContextInterface aci) {

        try {
            // make sure that ProfileUpdatedEvent is only processed once
            if (getStopProcessUpdateEvent()) {
                sendLogMsgCall("ProfileUpdatedEvent has already been received once before. Discarding this event otherwise we would run into an endless loop.");
                return;
            }

            setStopProcessUpdateEvent(true);
            sendLogMsgCall("Received ProfileUpdatedEvent");

            ProfileEventsTestsProfileLocal beforeUpdateProfile;
            ProfileEventsTestsProfileLocal afterUpdateProfile;

            beforeUpdateProfile = (ProfileEventsTestsProfileLocal) event.getBeforeUpdateProfileLocal();
            afterUpdateProfile = (ProfileEventsTestsProfileLocal) event.getAfterUpdateProfileLocal();

            //1110030: check that ProfileLocalObjects are read-only
            try {
                beforeUpdateProfile.setValue(2048);
                sendResult(false, 1110030,"Setting new value succeeded but should have failed (behaviour is supposed to be read-only).");
                return;
            }
            catch (TransactionRolledbackLocalException e) {
                sendLogMsgCall("ProfileLocalObject showed read-only behaviour as expected.");
            }
            catch (Exception e) {
                sendResult(false, 1110030,"Wrong exception type, expected javax.slee.TransactionRolledbackLocalException but was "+e.getClass().getName());
                return;
            }
            //1110030: check that ProfileLocalObjects are read-only
            try {
                afterUpdateProfile.setValue(2048);
                sendResult(false, 1110030,"Setting new value succeeded but should have failed (behaviour is supposed to be read-only).");
                return;
            }
            catch (TransactionRolledbackLocalException e) {
                sendLogMsgCall("ProfileLocalObject showed read-only behaviour as expected.");
            }
            catch (Exception e) {
                sendResult(false, 1110030,"Wrong exception type, expected javax.slee.TransactionRolledbackLocalException but was "+e.getClass().getName());
                return;
            }

            sendResult(true, 1110030, "ProfileLocalObjects showed read-only behaviour as expected.");


        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }
    }

    public void sbbRolledBack(RolledBackContext context) {
        //ignore rollback caused by ReadOnlyProfileExceptions from tests perspective
    }

    public abstract void setStopProcessUpdateEvent(boolean value);
    public abstract boolean getStopProcessUpdateEvent();

}
