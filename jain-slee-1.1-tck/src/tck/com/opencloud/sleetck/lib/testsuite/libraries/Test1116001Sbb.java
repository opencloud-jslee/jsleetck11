/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.libraries;

import java.security.AccessController;
import java.security.PrivilegedAction;

import javax.slee.ActivityContextInterface;

import com.opencloud.sleetck.lib.TCKTestFailureException;
import com.opencloud.sleetck.lib.resource.events.TCKResourceEventX;
import com.opencloud.sleetck.lib.sbbutils.BaseTCKSbb;
import com.opencloud.sleetck.lib.sbbutils.TCKSbbUtils;
import com.opencloud.sleetck.lib.testsuite.libraries.exampleClass.ExampleClass;
import com.opencloud.sleetck.lib.testsuite.libraries.exampleClass.ExampleClass2;

public abstract class Test1116001Sbb extends BaseTCKSbb {

    public void onTCKResourceEventX1(TCKResourceEventX event, ActivityContextInterface aci) {
        try {
            final ExampleClass ec = new ExampleClass();
            final ExampleClass2 ec2 = new ExampleClass2();
            ClassLoader cl1, cl2;

            cl1 = (ClassLoader)AccessController.doPrivileged(new PrivilegedAction() {
                public Object run()  {
                    return  ec.getClass().getClassLoader();
                }
            } );

            cl2 = (ClassLoader)AccessController.doPrivileged(new PrivilegedAction() {
                public Object run()  {
                    return  ec2.getClass().getClassLoader();
                }
            } );

            if (cl1 != cl2) throw new TCKTestFailureException(1116006, "Different classloaders were used.");
            if (null == cl1.getResource("com/opencloud/sleetck/lib/testsuite/libraries/exampleClass/ExampleResource.txt")) {
                throw new TCKTestFailureException(1116005, "Could not find resource in included jars.");
            }
            if (null == cl2.getResource("com/opencloud/sleetck/lib/testsuite/libraries/exampleClass/ExampleResource2.txt")) {
                throw new TCKTestFailureException(1116004, "Could not find resource directly in this library");
            }

            TCKSbbUtils.getResourceInterface().sendSbbMessage(ec.helloWorld());
        } catch(Exception e) {
            TCKSbbUtils.handleException(e);
        }
    }
}
