/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.profiles.profilelocal;

import com.opencloud.sleetck.lib.testsuite.profiles.ProfileRAInteractionBaseTest;

/**
 * Test assertions on ProfileLocal objects via RAs.
 */
public class Test1110101_2Test extends ProfileRAInteractionBaseTest {

    private static final String RA_NAME = "Test1110101_2RA";

    private static final String SPEC_NAME = "Test1110101_2Profile";
    private static final String SPEC_VERSION = "1.0";

    public String getRAName() {
        return RA_NAME;
    }

    public String[] getProfileNames(String profileTableName) {
        return new String[]{Test1110101_2MessageListener.PROFILE_NAME};
    }

    public String[] getProfileTableNames() {
        return new String[]{Test1110101_2MessageListener.PROFILE_TABLE_NAME};
    }

    public String getSpecName() {
        return SPEC_NAME;
    }
}
