/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.resource.endpoint;

import java.util.HashMap;

import com.opencloud.sleetck.lib.TCKTestResult;
import com.opencloud.sleetck.lib.testsuite.resource.BaseResourceTest;
import com.opencloud.sleetck.lib.testsuite.resource.RAMethods;

/**
 * Test to ensure that startActivitySuspended() is mandatory transactional.
 * <p>
 * Test assertion: 1115420
 */
public class Test1115420Test extends BaseResourceTest {
    private final static int ASSERTION_ID = 1115420;

    public TCKTestResult run() throws Exception {
        HashMap results = sendMessage(RAMethods.startActivitySuspended, new Integer(ASSERTION_ID));
        checkResult(results, "result1", ASSERTION_ID, "startActivitySuspended() failed to throw a TransactionRequiredLocalException outside of a transaction");
        checkResult(results, "result2", ASSERTION_ID, "startActivitySuspended() threw a TransactionRequiredLocalException inside of a transaction");
        return TCKTestResult.passed();
    }
}
