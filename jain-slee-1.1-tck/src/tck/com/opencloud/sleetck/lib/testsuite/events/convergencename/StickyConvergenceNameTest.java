/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.events.convergencename;

import com.opencloud.sleetck.lib.TCKTestResult;
import com.opencloud.sleetck.lib.resource.TCKActivityID;
import com.opencloud.sleetck.lib.resource.events.TCKResourceEventX;
import com.opencloud.sleetck.lib.testutils.ProfileUtils;
import com.opencloud.sleetck.lib.testutils.jmx.AddressProfileProxy;
import com.opencloud.sleetck.lib.testutils.jmx.ProfileProvisioningMBeanProxy;
import com.opencloud.sleetck.lib.testutils.jmx.impl.AddressProfileProxyImpl;

import javax.management.ObjectName;
import javax.slee.Address;
import javax.slee.AddressPlan;
import java.util.Vector;

/**
 * Test assertion 2012: that convergence names for SBB entities are not recalculated once generated.  This is tested by
 * firing events with identical input parameters using every available combination of initial event selector variables
 * and verifying that a new SBB is created for every event.
 */
public class StickyConvergenceNameTest extends AbstractMultiVariableSelectionTest {

    private static final String PROFILE_1 = "PROFILE_1"; // a1 a2
    private static final String ADDRESS_PROFILE_TABLE = "tck.AddressAndProfileTestProfile";
    private static final Address ADDRESS_1 = new Address(AddressPlan.IP, "1.0.0.1");
    private ProfileUtils profileUtils;

    public TCKTestResult run() throws Exception {
        TCKActivityID activityID = utils().getResourceInterface().createActivity("Test2012Activity");
        int eventNumber = 0;

        for (int i = 0; i < getCombinations().size(); i++) {
            String eventID = String.valueOf(eventNumber++);
            Vector variables = (Vector) getCombinations().elementAt(i);
            InitialEventSelectorParameters iesParams = this.createIESParamsFromSelected(variables, "Test2012");
            getLog().info("Testing combination: " + variables);
            sendEventAndWait(TCKResourceEventX.X1, eventID, activityID, ADDRESS_1, eventID, iesParams);
        }

        return TCKTestResult.passed();
    }

    public void setUp() throws Exception {
        // setup the address profile table before installing the service
        profileUtils = new ProfileUtils(utils());
        profileProxy = profileUtils.getProfileProvisioningProxy();
        profileUtils.createStandardAddressProfileTable(ADDRESS_PROFILE_TABLE);
        setupAddressProfile(ADDRESS_PROFILE_TABLE, PROFILE_1, new Address[] {ADDRESS_1});
        super.setUp();
    }

    public void tearDown() throws Exception {
        super.tearDown();
        if (profileUtils != null) {
            profileProxy.removeProfile(ADDRESS_PROFILE_TABLE, PROFILE_1);
            profileUtils.removeProfileTable(ADDRESS_PROFILE_TABLE);
        }
    }

    private void setupAddressProfile(String tableName, String profileName, Address[] addresses) throws Exception {
        ObjectName objectName = profileProxy.createProfile(tableName, profileName);
        AddressProfileProxy addressProfileProxy = new AddressProfileProxyImpl(objectName, utils().getMBeanFacade());
        addressProfileProxy.editProfile();
        addressProfileProxy.setAddresses(addresses);
        addressProfileProxy.commitProfile();
    }

    private ProfileProvisioningMBeanProxy profileProxy;
}
