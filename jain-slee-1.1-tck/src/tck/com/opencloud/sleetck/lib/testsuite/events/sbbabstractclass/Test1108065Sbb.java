/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.events.sbbabstractclass;

import java.util.HashMap;

import javax.slee.ActivityContextInterface;
import javax.slee.Address;
import javax.slee.EventContext;
import javax.slee.ServiceID;
import javax.slee.facilities.Tracer;

import com.opencloud.sleetck.lib.resource.events.TCKResourceEventX;
import com.opencloud.sleetck.lib.sbbutils.BaseTCKSbb;
import com.opencloud.sleetck.lib.sbbutils.TCKSbbUtils;

/**
 * AssertionID(1108065): Test The second method signature provides access to the
 * Event Context associated with the event.
 */
public abstract class Test1108065Sbb extends BaseTCKSbb {

    // Initial event method.
    public void onTCKResourceEventX1(TCKResourceEventX event, ActivityContextInterface aci, EventContext context) {
        try {
            tracer = getSbbContext().getTracer("com.test");
            tracer.info("Received event TCKResourceEventX");
            Test1108065Event testEvent = new Test1108065Event();

            ServiceID serviceID = context.getService();
            Address address = context.getAddress();
            ActivityContextInterface activityContextInterface = context.getActivityContextInterface();

            fireTest1108065Event(testEvent, activityContextInterface, address, serviceID);

        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }
    }

    public void onTest1108065Event(Test1108065Event event, ActivityContextInterface aci, EventContext context) {
        try {
            tracer = getSbbContext().getTracer("com.test");
            tracer.info("Received the first event " + event + " with EventConext.");

            Test1108065Event sentEvent = (Test1108065Event) context.getEvent();
            if (!event.equals(sentEvent))
                sendResultToTCK(1108065, "This onEventName(event,activity,eventContext) method didn't return "
                        + "the event object of the event that this Event Context is associated with.", 
                        "Test1108065Test", false);
            else
                sendResultToTCK(1108065, "Test of The second method signature provides access to the Event Context "
                        + "associated with the event passed!", "Test1108065Test", true);
            return;

        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }

    }

    public abstract void fireTest1108065Event(Test1108065Event event, ActivityContextInterface aci, Address address,
            ServiceID serviceID);

    private void sendResultToTCK(int failedAssertionID, String message, String testName, boolean result) throws Exception {
        HashMap sbbData = new HashMap();
        sbbData.put("testname", testName);
        sbbData.put("result", result ? "pass" : "fail");
        sbbData.put("message", message);
        sbbData.put("id", new Integer(failedAssertionID));
        TCKSbbUtils.getResourceInterface().sendSbbMessage(sbbData);
    }

    private Tracer tracer;
}
