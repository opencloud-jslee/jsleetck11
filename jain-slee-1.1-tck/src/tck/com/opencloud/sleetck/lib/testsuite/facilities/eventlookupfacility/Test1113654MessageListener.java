/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.facilities.eventlookupfacility;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.HashMap;

import javax.naming.NamingException;
import javax.slee.EventTypeID;
import javax.slee.UnrecognizedEventException;
import javax.slee.facilities.EventLookupFacility;
import javax.slee.resource.FireableEventType;

import com.opencloud.logging.StdErrLog;
import com.opencloud.sleetck.lib.infra.SleeTCKComponentConstants;
import com.opencloud.sleetck.lib.profileutils.BaseMessageSender;
import com.opencloud.sleetck.lib.rautils.MessageHandler;
import com.opencloud.sleetck.lib.rautils.RMIObjectChannel;
import com.opencloud.sleetck.lib.rautils.TCKRAUtils;
import com.opencloud.sleetck.lib.testsuite.resource.SimpleEvent;

/**
 * AssertionID(1113654): Test This getEventType method returns the event type identifier 
 * of the event type described by the FireableEventType object.
 * 
 * AssertionID(1113655): This getEventClassName method returns the fully-qualified name 
 * of the event class for the event type described by the FireableEventType object. 
 * This is the class name specified in the event type's deployment descriptor.
 * 
 * AssertionID(1113656): Test This getEventClassLoader method returns a class loader for 
 * the event type described by the FireableEventType object.
 * 
 * AssertionID(1113662): Test This getFireableEventType method returns a FireableEventType 
 * object for the event type identified by the eventType argument.
 * 
 * AssertionID(1113663): Test this method throws a java.lang.NullPointerException if the 
 * eventType argument is null.
 * 
 * AssertionID(1113664): Test It throws a javax.slee.UnrecognizedEventException if the 
 * eventType argument does not identify an event type in the SLEE.
 * 
 * AssertionID(1113665): Test An UnrecognizedEventException is also thrown if the eventType 
 * argument does not identify an event type that the Resource Adaptor may fire (as determined 
 * by the resource adaptor types referenced by the Resource Adaptor) unless event type checking 
 * has been disabled for the Resource Adaptor (see Section 15.10).
 **/

public class Test1113654MessageListener extends UnicastRemoteObject implements MessageHandler {
    
    public static final int CHECK_IGNORE_RA_TYPE_EVENT_TYPE_FALSE = 1;
    public static final int CHECK_IGNORE_RA_TYPE_EVENT_TYPE_TRUE = 2;

    public Test1113654MessageListener(Test1113654ResourceAdaptor ra) throws RemoteException {
        super();

        try {
            out = TCKRAUtils.lookupRMIObjectChannel();
            msgSender = new BaseMessageSender(out, new StdErrLog());
        }
        catch (Exception e) {
            ra.getLog().warning("Exception occurred when trying to acquire RMIObjectChannel: ",e);
        }

        this.ra = ra;
    }

    public boolean handleMessage(Object message) throws RemoteException {

        //run the tests, if both run through successfully and completely (=both return 'true')
        //send success msg back to TCK test
        int type = ((Integer)((HashMap)message).get("Type")).intValue();
        
        switch (type) {
        case CHECK_IGNORE_RA_TYPE_EVENT_TYPE_FALSE:
            if (test_GetEventType() && test_GetEventClassName() 
                    && test_GetEventClassLoader() && test_GetFireableEventType())
                msgSender.sendSuccess(1113654, "Test successful.");
        case CHECK_IGNORE_RA_TYPE_EVENT_TYPE_TRUE:
            if (test_INGORE_RA_TYPE_EVENT_TYPE_CHECK())
                msgSender.sendSuccess(1113665, "Test successful.");
        }
        return true;
    }
    
    private boolean test_GetEventType() {

        try {
            EventTypeID simpleEventTypeID = new EventTypeID(SimpleEvent.SIMPLE_EVENT_NAME, SleeTCKComponentConstants.TCK_VENDOR, "1.1");
            FireableEventType fet = getFireableEventType(simpleEventTypeID);

            //1113654
            try {
                EventTypeID eventTypeID = fet.getEventType();
                if (eventTypeID == null || !eventTypeID.equals(simpleEventTypeID)) {
                    msgSender.sendFailure(1113654, "Invalid EventTypeID returned from FireableEventType.getEventType()");
                    return false;
                }
                msgSender.sendLogMsg("getEventType() returned expected EventID");
            }
            catch (Exception e) {
                msgSender.sendFailure(1113654, "Invalid EventTypeID returned from FireableEventType.getEventType()", e);
                return false;
            }
        }
        catch (Exception e) {
            msgSender.sendException(e);
            return false;
        }
        return true;
    }
    
    private boolean test_GetEventClassName() {
        try {
            EventTypeID simpleEventTypeID = new EventTypeID(SimpleEvent.SIMPLE_EVENT_NAME, SleeTCKComponentConstants.TCK_VENDOR, "1.1");
            FireableEventType fet = getFireableEventType(simpleEventTypeID);

            //1113655
            try {
                String className = fet.getEventClassName();
                if (className == null || !className.equals(SimpleEvent.SIMPLE_EVENT_NAME)) {
                    msgSender.sendFailure(1113655, "Incorrect class name returned from FireableEventType.getEventClassName()");
                    return false;
                }
                msgSender.sendLogMsg("getEventClassName() returned expected event class name.");
            }
            catch (Exception e) {
                msgSender.sendFailure(1113655, "Exception occured when calling FireableEventType.getEventClassName(): ", e);
                return false;
            }
        }
        catch (Exception e) {
            msgSender.sendException(e);
            return false;
        }
        return true;
    }
    
    private boolean test_GetEventClassLoader() {
        try {
            EventTypeID simpleEventTypeID = new EventTypeID(SimpleEvent.SIMPLE_EVENT_NAME, SleeTCKComponentConstants.TCK_VENDOR, "1.1");
            FireableEventType fet = getFireableEventType(simpleEventTypeID);

            //1113656
            try {
                ClassLoader classLoader = fet.getEventClassLoader();
                if (classLoader == null || classLoader.loadClass(fet.getEventClassName()) == null) {
                    msgSender.sendFailure(1113656, "Incorrect class name returned from FireableEventType.getEventClassName()");
                    return false;
                }
                msgSender.sendLogMsg("getEventClassName() returned expected event class name.");
            }
            catch (ClassNotFoundException cnfe) {
                msgSender.sendFailure(1113656, "ClassNotFoundException occured when calling FireableEventType.getEventClassName(): ", cnfe);
            }
            catch (Exception e) {
                msgSender.sendFailure(1113656, "Exception occured when calling FireableEventType.getEventClassName(): ", e);
                return false;
            }
        }
        catch (Exception e) {
            msgSender.sendException(e);
            return false;
        }
        return true;
    }
    
    private boolean test_GetFireableEventType() {

        try {
            EventLookupFacility elf = getEventLookupFacility();

            //1113662
            EventTypeID eventTypeID = new EventTypeID(SimpleEvent.SIMPLE_EVENT_NAME, SleeTCKComponentConstants.TCK_VENDOR, "1.1");
            FireableEventType fet;
            try {
                fet = elf.getFireableEventType(eventTypeID);
                if (fet == null || !fet.getEventType().equals(eventTypeID)) {
                    msgSender.sendFailure(1113662, "Invalid FireableEventType returned from EventLookupFacility.getFireableEventType()");
                    return false;
                }
                msgSender.sendLogMsg("getEventID()returned expected EventID");
            }
            catch (Exception e) {
                msgSender.sendFailure(1113662, "Invalid FireableEventType returned from EventLookupFacility.getFireableEventType(): " + e);
                return false;
            }

            //1113663: NPE if eventType is null
            try {
                fet = elf.getFireableEventType(null);
                msgSender.sendFailure(1113663, "getFireableEventType with null name did not throw a NullPointerException");
                return false;
            }
            catch (NullPointerException npe) {
                msgSender.sendLogMsg("getFireableEventType threw NullPointerException as expected: "+npe);
            } catch (Exception e) {
                msgSender.sendFailure(1113663, "getFireableEventType with null name threw unexpected exception: " + e);
                return false;
            }
            
            //1113664: UnrecognizedEventException if the eventType argument does not identify an event type in the SLEE.
            String serviceStartedEvent = "javax.slee.serviceactivity.ServiceStartedEvent";
            EventTypeID serviceStartedEventType = new EventTypeID(serviceStartedEvent, "javax.slee", "1.1");
            try {
                fet = elf.getFireableEventType(serviceStartedEventType);
                msgSender.sendFailure(1113664, "getFireableEventType with an unrecognized event type did not throw a UnrecognizedEventException");
                return false;
            }
            catch (UnrecognizedEventException uee) {
                msgSender.sendLogMsg("FireableEventType() threw UnrecognizedEventException as expected when called with an unrecognized event type");
            } catch (Exception e) {
                msgSender.sendFailure(1113664, "getFireableEventType with an unrecognized event type did not throw a UnrecognizedEventException: " + e);
                return false;
            }

            //1113664: dummyEventTypeID is not installed in the SLEE, so we should still expect the 
            //UnrecognizedEventException is thrown.
            EventTypeID dummyEventTypeID = new EventTypeID("name-does-not-exist", "vendor-does-not-exist","verson-does-not-exist");
            try {
                fet = elf.getFireableEventType(dummyEventTypeID);
                msgSender.sendFailure(1113664, "getFireableEventType with an uninstalled event type did not throw a UnrecognizedEventException");
                return false;
            }
            catch (UnrecognizedEventException uee) {
                msgSender.sendLogMsg("FireableEventType() threw UnrecognizedEventException as expected when called with an unrecognized event type");
            } catch (Exception e) {
                msgSender.sendFailure(1113664, "getFireableEventType with an uninstalled event type did not throw a UnrecognizedEventException: " + e);
                return false;
            }

        }
        catch (Exception e) {
            msgSender.sendException(e);
            return false;
        }
        return true;
    }
    
    private boolean test_INGORE_RA_TYPE_EVENT_TYPE_CHECK() {

        try {
            EventLookupFacility elf = getEventLookupFacility();

            //1113662
            EventTypeID eventTypeID = new EventTypeID(SimpleEvent.SIMPLE_EVENT_NAME, SleeTCKComponentConstants.TCK_VENDOR, "1.1");
            FireableEventType fet;
            try {
                fet = elf.getFireableEventType(eventTypeID);
                if (fet == null || !fet.getEventType().equals(eventTypeID)) {
                    msgSender.sendFailure(1113662, "Invalid FireableEventType returned from EventLookupFacility.getFireableEventType()");
                    return false;
                }
                msgSender.sendLogMsg("FireableEventType()returned expected EventID");
            }
            catch (Exception e) {
                msgSender.sendFailure(1113662, "Invalid FireableEventType returned from EventLookupFacility.getFireableEventType(): " + e);
                return false;
            }

            //1113663: NPE if eventType is null
            try {
                fet = elf.getFireableEventType(null);
                msgSender.sendFailure(1113663, "getFireableEventType with null name did not throw a NullPointerException");
                return false;
            }
            catch (NullPointerException npe) {
                msgSender.sendLogMsg("getFireableEventType threw NullPointerException as expected: "+npe);
            } catch (Exception e) {
                msgSender.sendFailure(1113663, "getFireableEventType with null name threw unexpected exception: " + e);
                return false;
            }
            
            //1113665: UnrecognizedEventException didn't throw with INGORE-RA-TYPE-EVENT-TYPE-CHECK sets to true
            //if the eventType argument does not identify an event type in the SLEE.
            String serviceStartedEvent = "javax.slee.serviceactivity.ServiceStartedEvent";
            EventTypeID serviceStartedEventType = new EventTypeID(serviceStartedEvent, "javax.slee", "1.1");
            try {
                fet = elf.getFireableEventType(serviceStartedEventType);
                msgSender.sendLogMsg("FireableEventType() didn't threw UnrecognizedEventException as expected when called with an unrecognized event type");
            }
            catch (UnrecognizedEventException uee) {
                msgSender.sendFailure(1113665, "getFireableEventType threw UnrecognizedEventException as unexpected when called with an unrecognized event type");
                return false;
            } catch (Exception e) {
                msgSender.sendFailure(1113665, "getFireableEventType with an unrecognized event type did not throw a UnrecognizedEventException: " + e + fet.toString());
                return false;
            }
            
            //1113665: dummyEventTypeID is not installed in the SLEE, so we should still expect the 
            //UnrecognizedEventException is thrown.
            EventTypeID dummyEventTypeID = new EventTypeID("name-does-not-exist", "vendor-does-not-exist","verson-does-not-exist");
            try {
                fet = elf.getFireableEventType(dummyEventTypeID);
                msgSender.sendFailure(1113665, "getFireableEventType with an uninstalled event type did not throw a UnrecognizedEventException");
                return false;
            }
            catch (UnrecognizedEventException uee) {
                msgSender.sendLogMsg("FireableEventType() threw UnrecognizedEventException as expected when called with an unrecognized event type");
            } catch (Exception e) {
                msgSender.sendFailure(1113665, "getFireableEventType with an uninstalled event type did not throw a UnrecognizedEventException: " + e);
                return false;
            }
        }
        catch (Exception e) {
            msgSender.sendException(e);
            return false;
        }
        return true;
    }
    
    private FireableEventType getFireableEventType(EventTypeID eventType) throws NamingException {
        EventLookupFacility eventLookup =  getEventLookupFacility();
        FireableEventType fet;
        try {
            fet = eventLookup.getFireableEventType(eventType);
        }
        catch (Exception e) {
            ra.getLog().warning("getFireableEventType throw a UnrecognizedEventException: " + e);
            fet = null;
        }
            
        return fet;
    }
    
    private EventLookupFacility getEventLookupFacility() throws NamingException {
        return (EventLookupFacility) ra.getResourceAdaptorContext().getEventLookupFacility();
    }
    
    private BaseMessageSender msgSender;
    private RMIObjectChannel out;
    private Test1113654ResourceAdaptor ra;


}

