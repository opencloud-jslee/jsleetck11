/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.events.eventcontext;

import javax.slee.ActivityContextInterface;
import javax.slee.InitialEventSelector;
import javax.slee.EventContext;
import javax.slee.facilities.Tracer;

import com.opencloud.sleetck.lib.sbbutils.BaseTCKSbb;
import com.opencloud.sleetck.lib.sbbutils.TCKSbbUtils;
import com.opencloud.sleetck.lib.resource.TCKActivityID;
import com.opencloud.sleetck.lib.resource.events.TCKResourceEventX;
import com.opencloud.sleetck.lib.resource.sbbapi.TCKActivity;

import java.util.HashMap;

/**
 * AssertionID(1108043): Test If the transaction does not commit then the 
 * Event Context will not be “resumed” and therefore the event will not be 
 * delivered to any other eligible SBB entities.
 */
public abstract class Test1108042Sbb2 extends BaseTCKSbb {
    public static final String TRACE_MESSAGE_TCKResourceEventX1 = "Test1108042Sbb2:I got TCKResourceEventX1 on ActivityA";

    public static final String TRACE_MESSAGE_TCKResourceEventX2A = "Test1108042Sbb2:I got TCKResourceEventX2 on ActivityA";

    public static final String TRACE_MESSAGE_TCKResourceEventX2B = "Test1108042Sbb2:I got TCKResourceEventX2 on ActivityB";

    public static final String TRACE_MESSAGE_TCKResourceEventX3A = "Test1108042Sbb2:I got TCKResourceEventX3 on ActivityA";

    public static final String TRACE_MESSAGE_TCKResourceEventX3B = "Test1108042Sbb2:I got TCKResourceEventX3 on ActivityB";

    public InitialEventSelector initialEventSelector(InitialEventSelector ies) {
        ies.setCustomName("test");
        return ies;
    }

    public void onTCKResourceEventX1(TCKResourceEventX event, ActivityContextInterface aci, EventContext context) {
        try {
            //send message to TCK: I got TCKResourceEventX1 on ActivityA
            tracer = getSbbContext().getTracer("com.test");
            tracer.info(TRACE_MESSAGE_TCKResourceEventX1);
            if (!getHaveResumedX1()) 
                setPassedX1(true);

        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }
    }

    public void onTCKResourceEventX2(TCKResourceEventX event, ActivityContextInterface aci, EventContext context) {
        try {
            //send message to TCK: I got TCKResourceEventX1 on ActivityA
            tracer = getSbbContext().getTracer("com.test");
            TCKActivity activity = (TCKActivity) context.getActivityContextInterface().getActivity();
            TCKActivityID activityID = activity.getID();
            if (activityID.getName().equals(Test1108042Test.activityNameB)) {
                tracer.info(TRACE_MESSAGE_TCKResourceEventX2B);
                setHaveResumedX1(true);
                if (getPassedX1())
                    setPassedX1(false);
            } else if (activityID.getName().equals(Test1108042Test.activityNameA)) {
                tracer.info(TRACE_MESSAGE_TCKResourceEventX2A);
                if (!getHaveResumedX1()) 
                    setPassedX2(true);
            }

        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }
    }

    public void onTCKResourceEventX3(TCKResourceEventX event, ActivityContextInterface aci, EventContext context) {
        try {
            //send message to TCK: I got TCKResourceEventX3 on ActivityB
            tracer = getSbbContext().getTracer("com.test");

            TCKActivity activity = (TCKActivity) context.getActivityContextInterface().getActivity();
            TCKActivityID activityID = activity.getID();
            if (activityID.getName().equals(Test1108042Test.activityNameB)) {
                tracer.info(TRACE_MESSAGE_TCKResourceEventX3B);
                if (getPassedX1() || getPassedX2()) {
                    sendResultToTCK("Test1108042Test", false, "SBB2:onTCKResourceEventX3-ERROR: Sbb2 should not receive either TCKResourceEventX1 on ActivityA or "
                                    + "TCKResourceEventX2 on ActivityA.", 1108043);
                }
                return;
            } else if (activityID.getName().equals(Test1108042Test.activityNameA)) {
                tracer.info(TRACE_MESSAGE_TCKResourceEventX3A);
            }
        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }
    }

    private void sendResultToTCK(String testName, boolean result, String message, int failedAssertionID) throws Exception {
        HashMap sbbData = new HashMap();
        sbbData.put("testname", testName);
        sbbData.put("result", result ? "pass" : "fail");
        sbbData.put("message", message);
        sbbData.put("id", new Integer(failedAssertionID));
        TCKSbbUtils.getResourceInterface().sendSbbMessage(sbbData);
    }
    
    public abstract void setHaveResumedX1(boolean haveResumedX1);
    public abstract boolean getHaveResumedX1();

    public abstract void setPassedX1(boolean passedX1);
    public abstract boolean getPassedX1();
    
    public abstract void setPassedX2(boolean passedX2);
    public abstract boolean getPassedX2();

    private Tracer tracer = null;

}
