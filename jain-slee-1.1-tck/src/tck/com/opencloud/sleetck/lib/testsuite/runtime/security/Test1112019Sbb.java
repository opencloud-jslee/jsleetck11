/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.runtime.security;

import javax.naming.Context;
import javax.slee.ActivityContextInterface;
import javax.slee.RolledBackContext;
import javax.slee.SbbContext;
import javax.slee.profile.ProfileFacility;
import javax.slee.profile.ProfileTable;

import com.opencloud.logging.StdErrLog;
import com.opencloud.sleetck.lib.profileutils.BaseMessageSender;
import com.opencloud.sleetck.lib.rautils.RMIObjectChannel;
import com.opencloud.sleetck.lib.rautils.TCKRAUtils;
import com.opencloud.sleetck.lib.resource.events.TCKResourceEventX;
import com.opencloud.sleetck.lib.sbbutils.BaseTCKSbb;
import com.opencloud.sleetck.lib.sbbutils.TCKSbbUtils;

/**
 * Tests an Sbb's security access using AccessController.checkPermission()
 */
public abstract class Test1112019Sbb extends BaseTCKSbb {

    public static final String PROFILE_TABLE_NAME = "Test1112019ProfileTable";
    public static final String PROFILE_NAME = "Test1112019Profile";

    //Sbb lifecycle method. Acquire RMIObjectChannel output stream
    public void setSbbContext(SbbContext context) {
        super.setSbbContext(context);

        try {
            out = TCKRAUtils.lookupRMIObjectChannel();
            msgSender = new BaseMessageSender(out, new StdErrLog());
        }
        catch (Exception e) {
            context.getTracer(getSbbID().getName()).fine("An error occured creating an RMIObjectChannel:", e);
        }

    }

    public void onTCKResourceEventX1(TCKResourceEventX event, ActivityContextInterface aci) {
        try {
            getSbbContext().getTracer("Test1112019Sbb").fine("SBB Received event.");
            msgSender.sendLogMsg("SBB Received event.");

            Context env = TCKSbbUtils.getSbbEnvironment();
            ProfileFacility profileFacility = (ProfileFacility)env.lookup("slee/facilities/profile");
            ProfileTable profileTable = profileFacility.getProfileTable(PROFILE_TABLE_NAME);
            Test1112019ProfileLocal profileLocal = (Test1112019ProfileLocal)profileTable.find(PROFILE_NAME);

            msgSender.sendLogMsg("Sbb calls manage() method via ProfileLocal interface....");
            profileLocal.manage();
            msgSender.sendLogMsg("Give lifecycle methods some time to do their work.");
            Thread.sleep(2000);

            msgSender.sendSuccess(1112019, "Passed");

        } catch(Exception e) {
            msgSender.sendError(e.getMessage());
        }
    }

    public void sbbRolledBack(RolledBackContext context) {
        msgSender.sendError("Unexpected rollback occured.");
    }

    private RMIObjectChannel out;
    private BaseMessageSender msgSender;

}
