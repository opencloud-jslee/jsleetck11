/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.javax.slee.profile.AddressProfileCMP;

import com.opencloud.sleetck.lib.resource.events.TCKResourceEventX;
import com.opencloud.sleetck.lib.sbbutils.BaseTCKSbb;
import com.opencloud.sleetck.lib.sbbutils.TCKSbbUtils;

import javax.naming.InitialContext;
import javax.slee.ActivityContextInterface;
import javax.slee.Address;
import javax.slee.AddressPlan;
import javax.slee.profile.*;
import java.util.HashMap;
import java.util.Iterator;

public abstract class Test4266Sbb extends BaseTCKSbb {

    private static final String JNDI_PROFILEFACILITY_NAME = "java:comp/env/slee/facilities/profile";
    public static final String PROFILE_NAME = "Test4266Profile";

    // Initial event
    public void onTCKResourceEventX1(TCKResourceEventX ev, ActivityContextInterface aci) {

        try {

            ProfileFacility facility = (ProfileFacility) new InitialContext().lookup(JNDI_PROFILEFACILITY_NAME);
            Iterator iter = facility.getProfiles("AddressProfileSpec").iterator();

            boolean passed = false;

            while (iter.hasNext()) {
                ProfileID profileID = (ProfileID) iter.next();

                if (profileID.getProfileName().equals(PROFILE_NAME)) {
                    AddressProfileCMP profileCMP = getProfileCMP(profileID);
                    Address [] addresses = profileCMP.getAddresses();
                    
                    if (addresses.length == 1 && addresses[0].getAddressPlan().equals(AddressPlan.IP) && addresses[0].getAddressString().equals("127.0.0.1"))
                        passed = true;
                }

            }

            HashMap map = new HashMap();
            map.put("Type", "Result");
            map.put("Result", new Boolean(passed));
            if (passed) {
                map.put("Message", "Ok");
            } else {
                map.put("ID", new Integer(4268));
                map.put("Message", "AddressProfileCMP.set/getAddresses() not setting/getting addresses correctly.");
            }

            TCKSbbUtils.getResourceInterface().sendSbbMessage(map);

        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }
    }

    public abstract AddressProfileCMP getProfileCMP(ProfileID profileID) throws UnrecognizedProfileTableNameException, UnrecognizedProfileNameException;

}
