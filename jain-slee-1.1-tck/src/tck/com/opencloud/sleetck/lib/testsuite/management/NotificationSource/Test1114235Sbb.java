/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.management.NotificationSource;

import javax.slee.ActivityContextInterface;

import com.opencloud.sleetck.lib.resource.events.TCKResourceEventX;
import com.opencloud.sleetck.lib.sbbutils.BaseTCKSbb;

/*
 * AssertionID (1114235): Blank SBB
 * The ProfileTableNotification class identifies a profile table as the source of a notification.
 *
 */
public abstract class Test1114235Sbb extends BaseTCKSbb {

    // Initial event
    public void onTCKResourceEventX1(TCKResourceEventX event, ActivityContextInterface aci) {

    }

    private String testName = "Test1114235Test";


}

