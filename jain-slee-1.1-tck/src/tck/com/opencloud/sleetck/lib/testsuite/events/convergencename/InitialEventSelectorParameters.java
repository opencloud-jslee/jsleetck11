/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.events.convergencename;

import javax.slee.Address;

/**
 * The InitialEventSelectorParameters class holds parameters to be interpreted by
 * initial event selector method of the ConvergenceNameTestSbb.
 */
public class InitialEventSelectorParameters {

    public InitialEventSelectorParameters(boolean selectActivityContextFlag,
            boolean selectAddressFlag, boolean selectAddressProfileFlag,
            boolean selectEventFlag, boolean selectEventTypeFlag,
            String customName, boolean changePossibleInitialEventFlag,
            boolean possibleInitialEventFlag, boolean changeAddressFlag, Address newAddress) {

        this.selectActivityContextFlag=selectActivityContextFlag;
        this.selectAddressFlag=selectAddressFlag;
        this.selectAddressProfileFlag=selectAddressProfileFlag;
        this.selectEventFlag=selectEventFlag;
        this.selectEventTypeFlag=selectEventTypeFlag;
        this.customName=customName;
        this.changePossibleInitialEventFlag=changePossibleInitialEventFlag;
        this.possibleInitialEventFlag=possibleInitialEventFlag;
        this.changeAddressFlag=changeAddressFlag;
        this.newAddress=newAddress;
    }

    /**
     * Exports the Object into an Object or array of primitive types,
     * J2SE types or SLEE types.
     */
    public Object toExported() {
        boolean[] flags = {selectActivityContextFlag,selectAddressFlag,
            selectAddressProfileFlag,selectEventFlag,selectEventTypeFlag,
            changePossibleInitialEventFlag,possibleInitialEventFlag,changeAddressFlag};
        return new Object[] {flags,customName,newAddress};
    }

    /**
     * Returns an InitialEventSelectorParameters Object from the
     * given object in exported form
     */
    public static InitialEventSelectorParameters fromExported(Object exported) {
        Object[] objArray = (Object[])exported;
        boolean[] flags = (boolean[])objArray[0];
        String customName = (String)objArray[1];
        Address newAddress = (Address)objArray[2];
        int flagIndex = 0;
        return new InitialEventSelectorParameters(flags[flagIndex++],
                flags[flagIndex++],flags[flagIndex++],flags[flagIndex++],flags[flagIndex++],
                customName,flags[flagIndex++],flags[flagIndex++],flags[flagIndex++],newAddress);
    }

    // Accessor methods

    public boolean getSelectActivityContextFlag() {
        return selectActivityContextFlag;
    }

    public boolean getSelectAddressFlag() {
        return selectAddressFlag;
    }

    public boolean getSelectAddressProfileFlag() {
        return selectAddressProfileFlag;
    }

    public boolean getSelectEventFlag() {
        return selectEventFlag;
    }

    public boolean getSelectEventTypeFlag() {
        return selectEventTypeFlag;
    }

    public String getCustomName() {
        return customName;
    }

    public boolean getChangePossibleInitialEventFlag() {
        return changePossibleInitialEventFlag;
    }

    public boolean getPossibleInitialEventFlag() {
        return possibleInitialEventFlag;
    }

    public boolean getChangeAddressFlag() {
        return changeAddressFlag;
    }

    public Address getNewAddress() {
        return newAddress;
    }

    public String toString() {
        return "selectActivityContextFlag=" + selectActivityContextFlag
                +  ", selectAddressFlag=" + selectAddressFlag
                +  ", selectAddressProfileFlag=" + selectAddressProfileFlag
                +  ", selectEventFlag=" + selectEventFlag
                +  ", selectEventTypeFlag=" + selectEventTypeFlag
                +  ", customName=" + customName
                +  ", changePossibleInitialEventFlag=" + changePossibleInitialEventFlag
                +  ", possibleInitialEventFlag=" + possibleInitialEventFlag
                +  ", changeAddressFlag=" + changeAddressFlag
                +  ", newAddress=" + newAddress;
    }

    // Private state

    private boolean selectActivityContextFlag;
    private boolean selectAddressFlag;
    private boolean selectAddressProfileFlag;
    private boolean selectEventFlag;
    private boolean selectEventTypeFlag;
    private String customName;
    private boolean changePossibleInitialEventFlag;
    private boolean possibleInitialEventFlag;
    private boolean changeAddressFlag;
    private Address newAddress;

}