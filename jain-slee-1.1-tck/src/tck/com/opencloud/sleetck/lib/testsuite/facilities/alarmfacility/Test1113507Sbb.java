/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.facilities.alarmfacility;

import java.util.HashMap;

import javax.naming.InitialContext;
import javax.slee.ActivityContextInterface;
import javax.slee.facilities.AlarmFacility;
import javax.slee.facilities.AlarmLevel;
import javax.slee.facilities.Tracer;

import com.opencloud.sleetck.lib.resource.events.TCKResourceEventX;
import com.opencloud.sleetck.lib.sbbutils.BaseTCKSbb;
import com.opencloud.sleetck.lib.sbbutils.TCKSbbUtils;

/*  
 *  AssertionID(1113507): Test If one or more alarms are cleared 
 *  by this method, the SLEE’s AlarmMBean object emits an alarm 
 *  notification for each cleared alarm with the alarm level set 
 *  to AlarmLevel.CLEAR.
 *    
 *  AssertionID(1113509): Test The alarmType argument. This argument 
 *  specifies the alarm type of the alarms to be cleared.
 *  
 *  AssertionID(1113510): Test The clearAlarms(String) method throws 
 *  a java.lang.NullPointerException if the alarmID argument is null.
 *  
 */

public abstract class Test1113507Sbb extends BaseTCKSbb {

    public static final String ALARM_MESSAGE_CRITICAL = "CRITICAL:Test1113484AlarmMessage";

    public static final String ALARM_MESSAGE_MAJOR = "MAJOR:Test1113484AlarmMessage";

    public static final String ALARM_MESSAGE_WARNING = "WARNING:Test1113484AlarmMessage";

    public static final String ALARM_MESSAGE_INDETERMINATE = "INDETERMINATE:Test1113484AlarmMessage";

    public static final String ALARM_MESSAGE_MINOR = "MINOR:Test1113484AlarmMessage";

    public static final String ALARM_INSTANCEID = "Test1113507AlarmInstanceID";

    public static final String ALARM_TYPE = "javax.slee.management.Alarm";

    public static final AlarmLevel ALARM_LEVEL = AlarmLevel.MAJOR;

    // Initial event
    public void onTCKResourceEventX1(TCKResourceEventX ev, ActivityContextInterface aci) {

        try {
            tracer = this.getSbbContext().getTracer("com.test");
            tracer.info("Received " + ev + " message", null);

            doTest1113507Test();
            

        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }
    }

    private void doTest1113507Test() throws Exception {

        AlarmFacility facility = getAlarmFacility();

        //1113507
        try {
            //raise an alarm
            String alarmID1 = facility.raiseAlarm(ALARM_TYPE, "Test1113507AlarmInstanceID1", AlarmLevel.CRITICAL,
                    ALARM_MESSAGE_CRITICAL);
            tracer.info("Raising an alarm " + alarmID1);
            String alarmID2 = facility.raiseAlarm(ALARM_TYPE, "Test1113507AlarmInstanceID2", AlarmLevel.MAJOR,
                    ALARM_MESSAGE_MAJOR);
            tracer.info("Raising an alarm " + alarmID2);
            String alarmID3 = facility.raiseAlarm(ALARM_TYPE, "Test1113507AlarmInstanceID3", AlarmLevel.WARNING,
                    ALARM_MESSAGE_WARNING);
            tracer.info("Raising an alarm " + alarmID3);
            String alarmID4 = facility.raiseAlarm(ALARM_TYPE, "Test1113507AlarmInstanceID4", AlarmLevel.INDETERMINATE,
                    ALARM_MESSAGE_INDETERMINATE);
            tracer.info("Raising an alarm " + alarmID4);
            String alarmID5 = facility.raiseAlarm(ALARM_TYPE, "Test1113507AlarmInstanceID5", AlarmLevel.MINOR,
                    ALARM_MESSAGE_MINOR);
            tracer.info("Raising an alarm " + alarmID5);
            String[] alarmIDs = { alarmID1, alarmID2, alarmID3, alarmID4, alarmID5 };

            setFirstAlarm(alarmIDs);

            //clear the alarm has been raised
            if (facility.clearAlarms(ALARM_TYPE) != alarmIDs.length) {
                sendResultToTCK("Test1113507Test", false, 1113509, "The return value of this facility.clearAlarms(alarmType) method "
                        + "didn't match the number of alarms that supposed to be cleared!"
                        + "AlarmFacility.clearAlarms(Alarm_type)= " + facility.clearAlarms(ALARM_TYPE)
                        + "vs Total alarms= " + alarmIDs.length);
                return;
            }

        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }

        boolean passed = false;
        //1113510
        try {
            facility.clearAlarms(null);
        } catch (java.lang.NullPointerException e) {
            tracer.info("got expected NullPointerException when ALARM_TYPE is null", null);
            passed = true;
        }

        if (!passed) {
            sendResultToTCK("Test1113507Test", false, 1113510, "AlarmFacility.clearAlarms(null) "
                    + "should have thrown java.lang.NullPointerException.");
            return;
        }

    }

    private void sendResultToTCK(String testName, boolean result, int failedAssertionID,  String message) throws Exception {
        HashMap sbbData = new HashMap();
        sbbData.put("testname", testName);
        sbbData.put("result", result ? "pass" : "fail");
        sbbData.put("message", message);
        sbbData.put("id", new Integer(failedAssertionID));
        TCKSbbUtils.getResourceInterface().sendSbbMessage(sbbData);
    }

    private AlarmFacility getAlarmFacility() throws Exception {
        AlarmFacility facility = null;
        String JNDI_ALARMFACILITY_NAME = AlarmFacility.JNDI_NAME;
        try {
            facility = (AlarmFacility) new InitialContext().lookup(JNDI_ALARMFACILITY_NAME);
        } catch (Exception e) {
            tracer.warning("got unexpected Exception: " + e, null);
        }
        return facility;
    }

    public abstract void setFirstAlarm(String[] alarmID);

    public abstract String[] getFirstAlarm();

    private Tracer tracer;

}
