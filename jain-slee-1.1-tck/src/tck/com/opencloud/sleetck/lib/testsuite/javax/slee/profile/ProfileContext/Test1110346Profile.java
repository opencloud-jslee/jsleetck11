/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.javax.slee.profile.ProfileContext;

import javax.slee.CreateException;
import javax.slee.profile.Profile;
import javax.slee.profile.ProfileContext;
import javax.slee.profile.ProfileVerificationException;

import com.opencloud.logging.Logable;
import com.opencloud.logging.StdErrLog;
import com.opencloud.sleetck.lib.profileutils.BaseMessageSender;
import com.opencloud.sleetck.lib.rautils.RMIObjectChannel;
import com.opencloud.sleetck.lib.rautils.TCKRAUtils;

/**
 * The profile spec's Profile abstract class.
 */
public abstract class Test1110346Profile implements ProfileContextTestsProfileCMP, Profile  {

    public Test1110346Profile() {
        Logable log = new StdErrLog();

        try {
            out = TCKRAUtils.lookupRMIObjectChannel();
            msgSender = new BaseMessageSender(out, log);
        }
        catch (Exception e) {
            log.error("An error occured creating an RMIObjectChannel:");
            log.error(e);
        }
    }

    public void setProfileContext(ProfileContext context) {
        context.getTracer("setProfileContext").info("setProfileContext called");

        this.context = context;
    }

    public void unsetProfileContext() {
        context.getTracer("unsetProfileContext").info("unsetProfileContext called");

        context = null;
    }

    public void profileInitialize() {
        context.getTracer("profileInitialize").info("profileInitialize called");
    }

    public void profilePostCreate() throws CreateException {
        context.getTracer("profilePostCreate").info("profilePostCreate called");
    }

    public void profileActivate() {
        context.getTracer("profileActivate").info("profileActivate called");
    }

    public void profilePassivate() {
        context.getTracer("profilePassivate").info("profilePassivate called");
    }

    public void profileLoad() {
        context.getTracer("profileLoad").info("profileLoad called");
    }

    public void profileStore() {
        context.getTracer("profileStore").info("profileStore called");
    }

    public void profileRemove() {
        context.getTracer("profileRemove").info("profileRemove called");
    }

    public void profileVerify() throws ProfileVerificationException {
        context.getTracer("profileVerify").info("profileVerify called");
    }

    public void business() {
        context.getTracer("business").fine("Business method called.");

        //1110315: getProfileName() method may only be invoked during a profilePostCreate method
        //invocation, or during any method invocation on a Profile object while the
        //Profile object is in the Ready state. Otherwise a java.lang.IllegalStateException
        //is thrown.
        try {
            context.getProfileName();
            msgSender.sendLogMsg( 1110315+ ": Calling getProfileName in business method succeeded as expected.");
        }
        catch (Exception e) {
            msgSender.sendFailure( 1110315, "Calling getProfileName in business method should have succeeded but threw exception: "+e);
        }

        //1110319, 1110320: getProfileTable returns ProfileTable object, this can be cast to the
        //profile specs ProfileTable object type
        try {
            ProfileContextTestsProfileTable profileTable = (ProfileContextTestsProfileTable)context.getProfileTable();
            msgSender.sendLogMsg( 1110319+ ": getProfileTable called in business method returns object that can be successfully cast into the profile specs ProfileTable type.");
        }
        catch (ClassCastException e) {
            msgSender.sendFailure( 1110319, "getProfileTable called in business method returned an object that could not be cast into the profile specs ProfileTable type: "+e);
        }
        catch (Exception e) {
            msgSender.sendFailure( 1110319, "Calling getProfileTable in business method should have succeeded but threw exception: "+e);
        }

        //1110324, 1110325: getProfileTable(String) returns ProfileTable object for a specific profileTable, this can be cast to the
        //corresponding profile specs ProfileTable object type
        try {
            ProfileContextTestsProfileTable profileTable = (ProfileContextTestsProfileTable)context.getProfileTable(Test1110346Sbb.PROFILE_TABLE_NAME);
            msgSender.sendLogMsg( 1110324+ ": getProfileTable(String) called in business method returns object that can be successfully cast into the profile specs ProfileTable type.");
        }
        catch (ClassCastException e) {
            msgSender.sendFailure( 1110324, "getProfileTable(String) called in business method returned an object that could not be cast into the profile specs ProfileTable type: "+e);
        }
        catch (Exception e) {
            msgSender.sendFailure( 1110324, "Calling getProfileTable(String) in business method should have succeeded but threw exception: "+e);
        }

        //1110332, 1110333, 1110334, 1110335: getProfileLocalObject returns ProfileLocal object for the profile associated with this profile object
        //which can be typecast into the profile specs ProfileLocal type.
        //the method should fail if profilePassivate is called for the default profile
        try {
            ProfileContextTestsProfileLocal profileLocal = (ProfileContextTestsProfileLocal)context.getProfileLocalObject();
            if (context.getProfileName()==null)
                msgSender.sendFailure( 1110335, "getProfileLocalObject called in business method should have thrown an exception as associated profile is the default profile");
            else
                msgSender.sendLogMsg( 1110335+ ": getProfileLocalObject called in business method returns object that can be successfully cast into the profile specs ProfileLocal interface type");
        }
        catch (IllegalStateException e) {
            if (context.getProfileName()!=null)
                msgSender.sendFailure( 1110332, "getProfileLocalObject called in business method threw an exception though associated profile is not the default profile: "+e);
            else
                msgSender.sendLogMsg( 1110332+ ": getProfileLocalObject called in business method correctly threw an exception as associated profile is the default profile: "+e);
        }
        catch (Exception e) {
            msgSender.sendFailure( 1110332, "Wrong type of exception: "+e.getClass().getName()+" Expected: java.lang.IllegalStateException");
        }

        //1110346: The getRollbackOnly method returns true if the current transaction has been marked for
        //rollback and false otherwise.
        if (context.getRollbackOnly())
            msgSender.sendFailure( 1110346, "getRollbackOnly should have returned 'false' as current TXN was not marked for rollback.");
        else
            msgSender.sendLogMsg(1110346+": getRollbackOnly returned 'false' as expected.");
    }

    public void getRollbackTest() {

        //1110346: The getRollbackOnly method returns true if the current transaction has been marked for
        //rollback and false otherwise.
        if (!context.getRollbackOnly())
            msgSender.sendFailure( 1110346, "getRollbackOnly should have returned 'true' as current TXN was marked for rollback.");
        else
            msgSender.sendLogMsg(1110346+": getRollbackOnly returned 'true' as expected.");

    }

    private ProfileContext context;

    private RMIObjectChannel out;
    private BaseMessageSender msgSender;

}
