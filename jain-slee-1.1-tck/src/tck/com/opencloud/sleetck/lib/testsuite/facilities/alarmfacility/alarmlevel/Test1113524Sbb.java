/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.facilities.alarmfacility.alarmlevel;

import java.util.HashMap;

import javax.naming.InitialContext;
import javax.slee.ActivityContextInterface;
import javax.slee.facilities.AlarmFacility;
import javax.slee.facilities.AlarmLevel;
import javax.slee.facilities.Tracer;

import com.opencloud.sleetck.lib.resource.events.TCKResourceEventX;
import com.opencloud.sleetck.lib.sbbutils.BaseTCKSbb;
import com.opencloud.sleetck.lib.sbbutils.TCKSbbUtils;

/*
 * AssertionID (1113524): Test The isHigherLevel method returns true if the 
 * level represented by this AlarmLevel object is higher or more severe than 
 * the level specified by AlarmLevel object passed as the other argument.
 * 
 * AssertionID (1113525): Test CLEAR has a higher level than CRITICAL,

 * AssertionID (1113527): Test it throws a java.lang.NullPointerException 
 * if the other argument is null.
 * 
 * AssertionID (1113530): Test the fromInt and toInt methods allow conversion 
 * between the AlarmLevel object form and numeric form.
 * 
 * AssertionID (1113531): Test The fromInt method throws a java.lang.IllegalArgumentException 
 * if the level argument is not one of the six integer representations.
 * 
 * AssertionID (1113532): Test The fromString method allows conversion between String and AlarmLevel 
 * forms. The conversion is case insensitive.
 * 
 * AssertionID (1113533): Test The fromString method throws java.lang.NullPointerException 
 * - if the argument is null.
 * 
 * AssertionID (1113534): Test The fromString method throws a java.lang.IllegalArgumentException
 * if the argument is not an alarm level known to this class.
 * 
 */
public abstract class Test1113524Sbb extends BaseTCKSbb {

    // Initial event
    public void onTCKResourceEventX1(TCKResourceEventX event, ActivityContextInterface aci) {

        try {
            tracer = this.getSbbContext().getTracer("com.test");
            tracer.info("Received " + event + " message", null);

            doTest1113524Test();
        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }
    }

    private void sendResultToTCK(String testName, boolean result, int failedAssertionID, String message)
            throws Exception {
        HashMap sbbData = new HashMap();
        sbbData.put("testname", testName);
        sbbData.put("result", result ? "pass" : "fail");
        sbbData.put("message", message);
        sbbData.put("id", new Integer(failedAssertionID));
        TCKSbbUtils.getResourceInterface().sendSbbMessage(sbbData);
    }

    private void doTest1113524Test() throws Exception {
        AlarmLevel alarmLevel = AlarmLevel.MAJOR;
        AlarmFacility facility = getAlarmFacility();

        //1113524:CLEAR
        try {
            alarmLevel = AlarmLevel.CLEAR;
            //alarmLevel should return TraceLevel.CLEAR here
            if (alarmLevel != AlarmLevel.CLEAR) {
                sendResultToTCK("Test1113524Test", false, 1113524, "This alarmLevel argument didn't return "
                        + "AlarmLevel.CLEAR");
                return;
            }

            // try to compare alarmLevel with AlarmLevel.HIGHER firstly, returns false.
            if (alarmLevel.isHigherLevel(AlarmLevel.CLEAR)) {
                sendResultToTCK("Test1113524Test", false, 1113524, "This isHigherLevel method didn't return "
                        + "the correct value (false) due to AlarmLevel.CLEAR has "
                        + "the same value than AlarmLevel.CLEAR.");
                return;
            }

            // try to compare alarmLevel with AlarmLevel.LOWER secondly, returns true.
            if (!alarmLevel.isHigherLevel(AlarmLevel.CRITICAL)) {
                sendResultToTCK("Test1113524Test", false, 1113524, "This isHigherLevel method didn't return "
                        + "the correct value (true) due to AlarmLevel.CLEAR has "
                        + "higher value than AlarmLevel.CRITICAL.");
                return;
            }

            if (!alarmLevel.isHigherLevel(AlarmLevel.MAJOR)) {
                sendResultToTCK("Test1113524Test", false, 1113524, "This isHigherLevel method didn't return "
                        + "the correct value (true) due to AlarmLevel.CLEAR has "
                        + "higher value than AlarmLevel.MAJOR.");
                return;
            }

            if (!alarmLevel.isHigherLevel(AlarmLevel.WARNING)) {
                sendResultToTCK("Test1113524Test", false, 1113524, "This isHigherLevel method didn't return "
                        + "the correct value (true) due to AlarmLevel.CLEAR has "
                        + "higher value than AlarmLevel.WARNING.");
                return;
            }

            if (!alarmLevel.isHigherLevel(AlarmLevel.INDETERMINATE)) {
                sendResultToTCK("Test1113524Test", false, 1113524, "This isHigherLevel method didn't return "
                        + "the correct value (true) due to AlarmLevel.CLEAR has "
                        + "higher value than AlarmLevel.INDETERMINATE.");
                return;
            }

            if (!alarmLevel.isHigherLevel(AlarmLevel.MINOR)) {
                sendResultToTCK("Test1113524Test", false, 1113524, "This isHigherLevel method didn't return "
                        + "the correct value (true) due to AlarmLevel.CLEAR has "
                        + "higher value than AlarmLevel.MINOR.");
                return;
            }

            tracer.info("got expected value of the isHigherLevel method.", null);
        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }

        //1113524:CRITICAL
        try {
            alarmLevel = AlarmLevel.CRITICAL;
            //alarmLevel should return TraceLevel.CRITICAL here
            if (alarmLevel != AlarmLevel.CRITICAL) {
                sendResultToTCK("Test1113524Test", false, 1113524, "This alarmLevel argument didn't return "
                        + "AlarmLevel.CRITICAL");
                return;
            }

            // try to compare alarmLevel with AlarmLevel.HIGHER firstly, returns false.
            if (alarmLevel.isHigherLevel(AlarmLevel.CLEAR)) {
                sendResultToTCK("Test1113524Test", false, 1113524, "This isHigherLevel method didn't return "
                        + "the correct value (false) due to AlarmLevel.CRITICAL has "
                        + "lower value than AlarmLevel.CLEAR.");
                return;
            }

            if (alarmLevel.isHigherLevel(AlarmLevel.CRITICAL)) {
                sendResultToTCK("Test1113524Test", false, 1113524, "This isHigherLevel method didn't return "
                        + "the correct value (false) due to AlarmLevel.CRITICAL has "
                        + "the same value than AlarmLevel.CRITICAL.");
                return;
            }

            // try to compare alarmLevel with AlarmLevel.LOWER secondly, returns true.
            if (!alarmLevel.isHigherLevel(AlarmLevel.MAJOR)) {
                sendResultToTCK("Test1113524Test", false, 1113524, "This isHigherLevel method didn't return "
                        + "the correct value (true) due to AlarmLevel.CRITICAL has "
                        + "higher value than AlarmLevel.MAJOR.");
                return;
            }

            if (!alarmLevel.isHigherLevel(AlarmLevel.WARNING)) {
                sendResultToTCK("Test1113524Test", false, 1113524, "This isHigherLevel method didn't return "
                        + "the correct value (true) due to AlarmLevel.CRITICAL has "
                        + "higher value than AlarmLevel.WARNING.");
                return;
            }

            if (!alarmLevel.isHigherLevel(AlarmLevel.INDETERMINATE)) {
                sendResultToTCK("Test1113524Test", false, 1113524, "This isHigherLevel method didn't return "
                        + "the correct value (true) due to AlarmLevel.CRITICAL has "
                        + "higher value than AlarmLevel.INDETERMINATE.");
                return;
            }

            if (!alarmLevel.isHigherLevel(AlarmLevel.MINOR)) {
                sendResultToTCK("Test1113524Test", false, 1113524, "This isHigherLevel method didn't return "
                        + "the correct value (true) due to AlarmLevel.CRITICAL has "
                        + "higher value than AlarmLevel.MINOR.");
                return;
            }

            tracer.info("got expected value of the isHigherLevel method.", null);
        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }

        //1113524:MAJOR
        try {
            alarmLevel = AlarmLevel.MAJOR;
            //alarmLevel should return TraceLevel.MAJOR here
            if (alarmLevel != AlarmLevel.MAJOR) {
                sendResultToTCK("Test1113524Test", false, 1113524, "This alarmLevel argument didn't return "
                        + "AlarmLevel.MAJOR");
                return;
            }

            // try to compare alarmLevel with AlarmLevel.CRITICAL firstly, returns false.

            if (alarmLevel.isHigherLevel(AlarmLevel.CLEAR)) {
                sendResultToTCK("Test1113524Test", false, 1113524, "This isHigherLevel method didn't return "
                        + "the correct value (false) due to AlarmLevel.MAJOR has "
                        + "lower value than AlarmLevel.CLEAR.");
                return;
            }

            if (alarmLevel.isHigherLevel(AlarmLevel.CRITICAL)) {
                sendResultToTCK("Test1113524Test", false, 1113524, "This isHigherLevel method didn't return "
                        + "the correct value (false) due to AlarmLevel.MAJOR has "
                        + "lower value than AlarmLevel.CRITICAL.");
                return;
            }

            if (alarmLevel.isHigherLevel(AlarmLevel.MAJOR)) {
                sendResultToTCK("Test1113524Test", false, 1113524, "This isHigherLevel method didn't return "
                        + "the correct value (false) due to AlarmLevel.MAJOR has "
                        + "lower value than AlarmLevel.MAJOR.");
                return;
            }

            // try to compare alarmLevel with AlarmLevel.MINOR secondly, returns true.
            if (!alarmLevel.isHigherLevel(AlarmLevel.WARNING)) {
                sendResultToTCK("Test1113524Test", false, 1113524, "This isHigherLevel method didn't return "
                        + "the correct value (true) due to AlarmLevel.MAJOR has "
                        + "higher value than AlarmLevel.WARNING.");
                return;
            }

            if (!alarmLevel.isHigherLevel(AlarmLevel.INDETERMINATE)) {
                sendResultToTCK("Test1113524Test", false, 1113524, "This isHigherLevel method didn't return "
                        + "the correct value (true) due to AlarmLevel.MAJOR has "
                        + "higher value than AlarmLevel.INDETERMINATE.");
                return;
            }

            if (!alarmLevel.isHigherLevel(AlarmLevel.MINOR)) {
                sendResultToTCK("Test1113524Test", false, 1113524, "This isHigherLevel method didn't return "
                        + "the correct value (true) due to AlarmLevel.MAJOR has "
                        + "higher value than AlarmLevel.MINOR.");
                return;
            }

            tracer.info("got expected value of the isHigherLevel method.", null);
        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }

        //1113524:WARNING
        try {
            alarmLevel = AlarmLevel.WARNING;
            //alarmLevel should return TraceLevel.WARNING here
            if (alarmLevel != AlarmLevel.WARNING) {
                sendResultToTCK("Test1113524Test", false, 1113524, "This alarmLevel argument didn't return "
                        + "AlarmLevel.WARNING");
                return;
            }

            // try to compare alarmLevel with AlarmLevel.CRITICAL firstly, returns false.

            if (alarmLevel.isHigherLevel(AlarmLevel.CLEAR)) {
                sendResultToTCK("Test1113524Test", false, 1113524, "This isHigherLevel method didn't return "
                        + "the correct value (false) due to AlarmLevel.WARNING has "
                        + "lower value than AlarmLevel.CLEAR.");
                return;
            }

            if (alarmLevel.isHigherLevel(AlarmLevel.CRITICAL)) {
                sendResultToTCK("Test1113524Test", false, 1113524, "This isHigherLevel method didn't return "
                        + "the correct value (false) due to AlarmLevel.WARNING has "
                        + "lower value than AlarmLevel.CRITICAL.");
                return;
            }

            if (alarmLevel.isHigherLevel(AlarmLevel.MAJOR)) {
                sendResultToTCK("Test1113524Test", false, 1113524, "This isHigherLevel method didn't return "
                        + "the correct value (false) due to AlarmLevel.WARNING has "
                        + "lower value than AlarmLevel.MAJOR");
                return;
            }

            if (alarmLevel.isHigherLevel(AlarmLevel.WARNING)) {
                sendResultToTCK("Test1113524Test", false, 1113524, "This isHigherLevel method didn't return "
                        + "the correct value (false) due to AlarmLevel.WARNING has "
                        + "the same value than AlarmLevel.WARNING");
                return;
            }

            // try to compare alarmLevel with AlarmLevel.MINOR secondly, returns true.

            if (!alarmLevel.isHigherLevel(AlarmLevel.INDETERMINATE)) {
                sendResultToTCK("Test1113524Test", false, 1113524, "This isHigherLevel method didn't return "
                        + "the correct value (true) due to AlarmLevel.WARNING has "
                        + "higher value than AlarmLevel.INDETERMINATE.");
                return;
            }

            if (!alarmLevel.isHigherLevel(AlarmLevel.MINOR)) {
                sendResultToTCK("Test1113524Test", false, 1113524, "This isHigherLevel method didn't return "
                        + "the correct value (true) due to AlarmLevel.WARNING has "
                        + "higher value than AlarmLevel.MINOR.");
                return;
            }

            tracer.info("got expected value of the isHigherLevel method.", null);
        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }

        //1113524:INDETERMINATE
        try {
            alarmLevel = AlarmLevel.INDETERMINATE;
            //alarmLevel should return TraceLevel.INDETERMINATE here
            if (alarmLevel != AlarmLevel.INDETERMINATE) {
                sendResultToTCK("Test1113524Test", false, 1113524, "This alarmLevel argument didn't return "
                        + "AlarmLevel.INDETERMINATE");
                return;
            }

            // try to compare alarmLevel with AlarmLevel.CRITICAL firstly, returns false.

            if (alarmLevel.isHigherLevel(AlarmLevel.CLEAR)) {
                sendResultToTCK("Test1113524Test", false, 1113524, "This isHigherLevel method didn't return "
                        + "the correct value (false) due to AlarmLevel.INDETERMINATE has "
                        + "lower value than AlarmLevel.CLEAR.");
                return;
            }

            if (alarmLevel.isHigherLevel(AlarmLevel.CRITICAL)) {
                sendResultToTCK("Test1113524Test", false, 1113524, "This isHigherLevel method didn't return "
                        + "the correct value (false) due to AlarmLevel.INDETERMINATE has "
                        + "lower value than AlarmLevel.CRITICAL.");
                return;
            }

            if (alarmLevel.isHigherLevel(AlarmLevel.MAJOR)) {
                sendResultToTCK("Test1113524Test", false, 1113524, "This isHigherLevel method didn't return "
                        + "the correct value (false) due to AlarmLevel.INDETERMINATE has "
                        + "lower value than AlarmLevel.MAJOR.");
                return;
            }

            if (alarmLevel.isHigherLevel(AlarmLevel.WARNING)) {
                sendResultToTCK("Test1113524Test", false, 1113524, "This isHigherLevel method didn't return "
                        + "the correct value (false) due to AlarmLevel.INDETERMINATE has "
                        + "lower value than AlarmLevel.WARNING.");
                return;
            }

            if (alarmLevel.isHigherLevel(AlarmLevel.INDETERMINATE)) {
                sendResultToTCK("Test1113524Test", false, 1113524, "This isHigherLevel method didn't return "
                        + "the correct value (false) due to AlarmLevel.INDETERMINATE has "
                        + "the same value than AlarmLevel.INDETERMINATE.");
                return;
            }

            // try to compare alarmLevel with AlarmLevel.MINOR secondly, returns true.
            if (!alarmLevel.isHigherLevel(AlarmLevel.MINOR)) {
                sendResultToTCK("Test1113524Test", false, 1113524, "This isHigherLevel method didn't return "
                        + "the correct value (true) due to AlarmLevel.INDETERMINATE has "
                        + "higher value than AlarmLevel.MINOR.");
                return;
            }

            tracer.info("got expected value of the isHigherLevel method.", null);
        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }

        //1113524:MINOR
        try {
            alarmLevel = AlarmLevel.MINOR;
            //alarmLevel should return TraceLevel.MINOR here
            if (alarmLevel != AlarmLevel.MINOR) {
                sendResultToTCK("Test1113524Test", false, 1113524, "This alarmLevel argument didn't return "
                        + "AlarmLevel.MINOR");
                return;
            }

            // try to compare alarmLevel with AlarmLevel.CRITICAL firstly, returns false.

            if (alarmLevel.isHigherLevel(AlarmLevel.CLEAR)) {
                sendResultToTCK("Test1113524Test", false, 1113524, "This isHigherLevel method didn't return "
                        + "the correct value (false) due to AlarmLevel.MINOR has "
                        + "lower value than AlarmLevel.CLEAR.");
                return;
            }

            if (alarmLevel.isHigherLevel(AlarmLevel.CRITICAL)) {
                sendResultToTCK("Test1113524Test", false, 1113524, "This isHigherLevel method didn't return "
                        + "the correct value (false) due to AlarmLevel.MINOR has "
                        + "lower value than AlarmLevel.CRITICAL.");
                return;
            }

            if (alarmLevel.isHigherLevel(AlarmLevel.MAJOR)) {
                sendResultToTCK("Test1113524Test", false, 1113524, "This isHigherLevel method didn't return "
                        + "the correct value (false) due to AlarmLevel.MINOR has "
                        + "lower value than AlarmLevel.MAJOR.");
                return;
            }

            if (alarmLevel.isHigherLevel(AlarmLevel.WARNING)) {
                sendResultToTCK("Test1113524Test", false, 1113524, "This isHigherLevel method didn't return "
                        + "the correct value (false) due to AlarmLevel.MINOR has "
                        + "lower value than AlarmLevel.WARNING.");
                return;
            }

            if (alarmLevel.isHigherLevel(AlarmLevel.INDETERMINATE)) {
                sendResultToTCK("Test1113524Test", false, 1113524, "This isHigherLevel method didn't return "
                        + "the correct value (false) due to AlarmLevel.MINOR has "
                        + "lower value than AlarmLevel.INDETERMINATE.");
                return;
            }

            if (alarmLevel.isHigherLevel(AlarmLevel.MINOR)) {
                sendResultToTCK("Test1113524Test", false, 1113524, "This isHigherLevel method didn't return "
                        + "the correct value (false) due to AlarmLevel.MINOR has "
                        + "the same value than AlarmLevel.MINOR.");
                return;
            }

            tracer.info("got expected value of the isHigherLevel method.", null);
        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }
        //reset alarmLevel back to AlarmLevel.MAJOR
        alarmLevel = AlarmLevel.MAJOR;

        //1113525
        AlarmLevel clearAlarmLevel = AlarmLevel.CLEAR;
        AlarmLevel criticalAlarmLevel = AlarmLevel.CRITICAL;
        criticalAlarmLevel = AlarmLevel.CRITICAL;
        try {
            if (!clearAlarmLevel.isHigherLevel(criticalAlarmLevel)) {
                sendResultToTCK("Test1113524Test", false, 1113525, "This isHigherLevel method didn't return "
                        + "the correct value (true) due to AlarmLevel.CLEAR has "
                        + "higher value than AlarmLevel.CRITICAL.");
                return;
            }

            tracer.info("got expected value of the isHigherLevel method.", null);
        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }

        //1113527
        boolean passed1113527 = false;
        boolean isHigherLevel = false;
        try {
            isHigherLevel = alarmLevel.isHigherLevel(null);
        } catch (java.lang.NullPointerException e) {
            tracer.info("got expected NullPointerException", null);
            passed1113527 = true;
        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }

        if (!passed1113527) {
            sendResultToTCK("Test1113524Test", false, 1113527,
                    "AlarmLevel.isHigherLevel(null) should have thrown java.lang.NullPointerException.");
            return;
        }

        int intLevel = AlarmLevel.LEVEL_CLEAR;
        //1113530:CLEAR
        // try conversion from the AlarmLevel object form to numeric form.
        try {
            alarmLevel = AlarmLevel.fromInt(intLevel);
            if (alarmLevel.toInt() != intLevel) {
                sendResultToTCK("Test1113524Test", false, 1113530,
                        "This alarmLevel.toInt method didn't return numberic AlarmLevel.LEVEL_CLEAR "
                                + "form which specified by the AlarmLevel.CLEAR object form.");
                return;
            }
        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }
        // try conversion from numeric form to the AlarmLevel object form .
        alarmLevel = AlarmLevel.CLEAR;
        try {
            intLevel = alarmLevel.toInt();
            if (!alarmLevel.equals(AlarmLevel.fromInt(intLevel))) {
                sendResultToTCK("Test1113524Test", false, 1113530,
                        "This AlarmLevel.fromInt method didn't return the AlarmLevel.CLEAR "
                                + "object form which specified by numberic AlarmLevel.LEVEL_CLEAR form.");
                return;
            }
        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }

        //1113530:CRITICAL
        // try conversion from the AlarmLevel object form to numeric form.
        intLevel = AlarmLevel.LEVEL_CRITICAL;
        try {
            alarmLevel = AlarmLevel.fromInt(intLevel);
            if (alarmLevel.toInt() != intLevel) {
                sendResultToTCK("Test1113524Test", false, 1113530,
                        "This alarmLevel.toInt method didn't return numberic AlarmLevel.LEVEL_CRITICAL "
                                + "form which specified by the AlarmLevel.CRITICAL object form.");
                return;
            }
        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }
        // try conversion from numeric form to the AlarmLevel object form .
        alarmLevel = AlarmLevel.CRITICAL;
        try {
            intLevel = alarmLevel.toInt();
            if (!alarmLevel.equals(AlarmLevel.fromInt(intLevel))) {
                sendResultToTCK("Test1113524Test", false, 1113530,
                        "This AlarmLevel.fromInt method didn't return the AlarmLevel.CRITICAL "
                                + "object form which specified by numberic AlarmLevel.LEVEL_CRITICAL form.");
                return;
            }
        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }

        //1113530:MAJOR
        // try conversion from the AlarmLevel object form to numeric form.
        intLevel = AlarmLevel.LEVEL_MAJOR;
        try {
            alarmLevel = AlarmLevel.fromInt(intLevel);
            if (alarmLevel.toInt() != intLevel) {
                sendResultToTCK("Test1113524Test", false, 1113530,
                        "This alarmLevel.toInt method didn't return numberic AlarmLevel.LEVEL_MAJOR "
                                + "form which specified by the AlarmLevel.MAJOR object form.");
                return;
            }
        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }
        // try conversion from numeric form to the AlarmLevel object form .
        alarmLevel = AlarmLevel.MAJOR;
        try {
            intLevel = alarmLevel.toInt();
            if (!alarmLevel.equals(AlarmLevel.fromInt(intLevel))) {
                sendResultToTCK("Test1113524Test", false, 1113530,
                        "This AlarmLevel.fromInt method didn't return the AlarmLevel.MAJOR "
                                + "object form which specified by numberic AlarmLevel.LEVEL_MAJOR form.");
                return;
            }
        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }

        //1113530:WARNING
        // try conversion from the AlarmLevel object form to numeric form.
        intLevel = AlarmLevel.LEVEL_WARNING;
        try {
            alarmLevel = AlarmLevel.fromInt(intLevel);
            if (alarmLevel.toInt() != intLevel) {
                sendResultToTCK("Test1113524Test", false, 1113530,
                        "This alarmLevel.toInt method didn't return numberic AlarmLevel.LEVEL_WARNING "
                                + "form which specified by the AlarmLevel.WARNING object form.");
                return;
            }
        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }
        // try conversion from numeric form to the AlarmLevel object form .
        alarmLevel = AlarmLevel.WARNING;
        try {
            intLevel = alarmLevel.toInt();
            if (!alarmLevel.equals(AlarmLevel.fromInt(intLevel))) {
                sendResultToTCK("Test1113524Test", false, 1113530,
                        "This AlarmLevel.fromInt method didn't return the AlarmLevel.WARNING "
                                + "object form which specified by numberic AlarmLevel.LEVEL_WARNING form.");
                return;
            }
        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }

        //1113530:INDETERMINATE
        // try conversion from the AlarmLevel object form to numeric form.
        intLevel = AlarmLevel.LEVEL_INDETERMINATE;
        try {
            alarmLevel = AlarmLevel.fromInt(intLevel);
            if (alarmLevel.toInt() != intLevel) {
                sendResultToTCK("Test1113524Test", false, 1113530,
                        "This alarmLevel.toInt method didn't return numberic AlarmLevel.LEVEL_INDETERMINATE "
                                + "form which specified by the AlarmLevel.INDETERMINATE object form.");
                return;
            }
        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }
        // try conversion from numeric form to the AlarmLevel object form .
        alarmLevel = AlarmLevel.INDETERMINATE;
        try {
            intLevel = alarmLevel.toInt();
            if (!alarmLevel.equals(AlarmLevel.fromInt(intLevel))) {
                sendResultToTCK("Test1113524Test", false, 1113530,
                        "This AlarmLevel.fromInt method didn't return the AlarmLevel.INDETERMINATE "
                                + "object form which specified by numberic AlarmLevel.LEVEL_INDETERMINATE form.");
                return;
            }
        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }

        //1113530:MINOR
        // try conversion from the AlarmLevel object form to numeric form.
        intLevel = AlarmLevel.LEVEL_MINOR;
        try {
            alarmLevel = AlarmLevel.fromInt(intLevel);
            if (alarmLevel.toInt() != intLevel) {
                sendResultToTCK("Test1113524Test", false, 1113530,
                        "This alarmLevel.toInt method didn't return numberic AlarmLevel.LEVEL_MINOR "
                                + "form which specified by the AlarmLevel.MINOR object form.");
                return;
            }
        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }
        // try conversion from numeric form to the AlarmLevel object form .
        alarmLevel = AlarmLevel.MINOR;
        try {
            intLevel = alarmLevel.toInt();
            if (!alarmLevel.equals(AlarmLevel.fromInt(intLevel))) {
                sendResultToTCK("Test1113524Test", false, 1113530,
                        "This AlarmLevel.fromInt method didn't return the AlarmLevel.MINOR "
                                + "object form which specified by numberic AlarmLevel.LEVEL_MINOR form.");
                return;
            }
        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }
        //reset alarmLevel back to AlarmLevel.MAJOR
        alarmLevel = AlarmLevel.MAJOR;

        //1113531
        int minusLevel = -1;
        boolean passed1113531 = false;
        try {
            alarmLevel = AlarmLevel.fromInt(minusLevel);
        } catch (java.lang.IllegalArgumentException iae) {
            tracer.info("got expected IllegalArgumentException with a negative value for the level argument.", null);
            passed1113531 = true;
        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }

        if (!passed1113531) {
            sendResultToTCK("Test1113524Test", false, 1113531,
                    "AlarmLevel.fromInt(-1) should have thrown java.lang.IllegalArgumentException.");
            return;
        }

        int invalidLevel = 6;
        passed1113531 = false;
        try {
            alarmLevel = AlarmLevel.fromInt(invalidLevel);
        } catch (java.lang.IllegalArgumentException iae) {
            tracer.info("got expected IllegalArgumentException with an invalid value for the level argument.", null);
            passed1113531 = true;
        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }

        if (!passed1113531) {
            sendResultToTCK("Test1113524Test", false, 1113531,
                    "AlarmLevel.fromInt(6) should have thrown java.lang.IllegalArgumentException.");
            return;
        }

        String strLevel, strlcLevel;

        //1113532:MINOR
        strLevel = AlarmLevel.MINOR_STRING;
        strlcLevel = AlarmLevel.MINOR_STRING.toLowerCase();
        // try conversion from the AlarmLevel object form to a string value (upper case).
        try {
            alarmLevel = AlarmLevel.fromString(strLevel);
            if (!alarmLevel.toString().equals(strLevel)) {
                sendResultToTCK("Test1113524Test", false, 1113532,
                        "This alarmLevel.toString method didn't return a string value (upper case)"
                                + "which specified by the AlarmLevel object form.");
                return;
            }
        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }

        // try conversion from the AlarmLevel object form to a string value (lower case).
        try {
            alarmLevel = AlarmLevel.fromString(strlcLevel);
            if (!alarmLevel.toString().equalsIgnoreCase(strlcLevel)) {
                sendResultToTCK("Test1113524Test", false, 1113532,
                        "This alarmLevel.toString method didn't return a string value (lower case)"
                                + "which specified by the AlarmLevel object form.");
                return;
            }
        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }

        // try conversion from a string value (upper case) to the AlarmLevel object form .
        alarmLevel = AlarmLevel.MINOR;
        try {
            strLevel = alarmLevel.toString();
            if (!alarmLevel.equals(AlarmLevel.fromString(strLevel))) {
                sendResultToTCK("Test1113524Test", false, 1113532,
                        "This AlarmLevel.fromString method didn't return the AlarmLevel "
                                + "object form which specified by a string value (upper case).");
                return;
            }
        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }

        // try conversion from a string value (lower case) to the AlarmLevel object form .
        try {
            strlcLevel = alarmLevel.toString().toLowerCase();
            if (!alarmLevel.equals(AlarmLevel.fromString(strlcLevel))) {
                sendResultToTCK("Test1113524Test", false, 1113532,
                        "This AlarmLevel.fromString(lowercaseLevel) method didn't return the AlarmLevel "
                                + "object form which specified by a string value (lower case).");
                return;
            }
        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }

        //1113532:INDETERMINATE
        strLevel = AlarmLevel.INDETERMINATE_STRING;
        strlcLevel = AlarmLevel.INDETERMINATE_STRING.toLowerCase();
        // try conversion from the AlarmLevel object form to a string value (upper case).
        try {
            alarmLevel = AlarmLevel.fromString(strLevel);
            if (!alarmLevel.toString().equals(strLevel)) {
                sendResultToTCK("Test1113524Test", false, 1113532,
                        "This alarmLevel.toString method didn't return a string value (upper case)"
                                + "which specified by the AlarmLevel object form.");
                return;
            }
        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }

        // try conversion from the AlarmLevel object form to a string value (lower case).
        try {
            alarmLevel = AlarmLevel.fromString(strlcLevel);
            if (!alarmLevel.toString().equalsIgnoreCase(strlcLevel)) {
                sendResultToTCK("Test1113524Test", false, 1113532,
                        "This alarmLevel.toString method didn't return a string value (lower case)"
                                + "which specified by the AlarmLevel object form.");
                return;
            }
        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }

        // try conversion from a string value (upper case) to the AlarmLevel object form .
        alarmLevel = AlarmLevel.INDETERMINATE;
        try {
            strLevel = alarmLevel.toString();
            if (!alarmLevel.equals(AlarmLevel.fromString(strLevel))) {
                sendResultToTCK("Test1113524Test", false, 1113532,
                        "This AlarmLevel.fromString method didn't return the AlarmLevel "
                                + "object form which specified by a string value (upper case).");
                return;
            }
        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }

        // try conversion from a string value (lower case) to the AlarmLevel object form .
        try {
            strlcLevel = alarmLevel.toString().toLowerCase();
            if (!alarmLevel.equals(AlarmLevel.fromString(strlcLevel))) {
                sendResultToTCK("Test1113524Test", false, 1113532,
                        "This AlarmLevel.fromString(lowercaseLevel) method didn't return the AlarmLevel "
                                + "object form which specified by a string value (lower case).");
                return;
            }
        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }

        //1113532:WARNING
        strLevel = AlarmLevel.WARNING_STRING;
        strlcLevel = AlarmLevel.WARNING_STRING.toLowerCase();
        // try conversion from the AlarmLevel object form to a string value (upper case).
        try {
            alarmLevel = AlarmLevel.fromString(strLevel);
            if (!alarmLevel.toString().equals(strLevel)) {
                sendResultToTCK("Test1113524Test", false, 1113532,
                        "This alarmLevel.toString method didn't return a string value (upper case)"
                                + "which specified by the AlarmLevel object form.");
                return;
            }
        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }

        // try conversion from the AlarmLevel object form to a string value (lower case).
        try {
            alarmLevel = AlarmLevel.fromString(strlcLevel);
            if (!alarmLevel.toString().equalsIgnoreCase(strlcLevel)) {
                sendResultToTCK("Test1113524Test", false, 1113532,
                        "This alarmLevel.toString method didn't return a string value (lower case)"
                                + "which specified by the AlarmLevel object form.");
                return;
            }
        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }

        // try conversion from a string value (upper case) to the AlarmLevel object form .
        alarmLevel = AlarmLevel.WARNING;
        try {
            strLevel = alarmLevel.toString();
            if (!alarmLevel.equals(AlarmLevel.fromString(strLevel))) {
                sendResultToTCK("Test1113524Test", false, 1113532,
                        "This AlarmLevel.fromString method didn't return the AlarmLevel "
                                + "object form which specified by a string value (upper case).");
                return;
            }
        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }

        // try conversion from a string value (lower case) to the AlarmLevel object form .
        try {
            strlcLevel = alarmLevel.toString().toLowerCase();
            if (!alarmLevel.equals(AlarmLevel.fromString(strlcLevel))) {
                sendResultToTCK("Test1113524Test", false, 1113532,
                        "This AlarmLevel.fromString(lowercaseLevel) method didn't return the AlarmLevel "
                                + "object form which specified by a string value (lower case).");
                return;
            }
        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }

        //1113532:MAJOR
        strLevel = AlarmLevel.MAJOR_STRING;
        strlcLevel = AlarmLevel.MAJOR_STRING.toLowerCase();
        // try conversion from the AlarmLevel object form to a string value (upper case).
        try {
            alarmLevel = AlarmLevel.fromString(strLevel);
            if (!alarmLevel.toString().equals(strLevel)) {
                sendResultToTCK("Test1113524Test", false, 1113532,
                        "This alarmLevel.toString method didn't return a string value (upper case)"
                                + "which specified by the AlarmLevel object form.");
                return;
            }
        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }

        // try conversion from the AlarmLevel object form to a string value (lower case).
        try {
            alarmLevel = AlarmLevel.fromString(strlcLevel);
            if (!alarmLevel.toString().equalsIgnoreCase(strlcLevel)) {
                sendResultToTCK("Test1113524Test", false, 1113532,
                        "This alarmLevel.toString method didn't return a string value (lower case)"
                                + "which specified by the AlarmLevel object form.");
                return;
            }
        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }

        // try conversion from a string value (upper case) to the AlarmLevel object form .
        alarmLevel = AlarmLevel.MAJOR;
        try {
            strLevel = alarmLevel.toString();
            if (!alarmLevel.equals(AlarmLevel.fromString(strLevel))) {
                sendResultToTCK("Test1113524Test", false, 1113532,
                        "This AlarmLevel.fromString method didn't return the AlarmLevel "
                                + "object form which specified by a string value (upper case).");
                return;
            }
        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }

        // try conversion from a string value (lower case) to the AlarmLevel object form .
        try {
            strlcLevel = alarmLevel.toString().toLowerCase();
            if (!alarmLevel.equals(AlarmLevel.fromString(strlcLevel))) {
                sendResultToTCK("Test1113524Test", false, 1113532,
                        "This AlarmLevel.fromString(lowercaseLevel) method didn't return the AlarmLevel "
                                + "object form which specified by a string value (lower case).");
                return;
            }
        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }

        //1113532:CRITICAL
        strLevel = AlarmLevel.CRITICAL_STRING;
        strlcLevel = AlarmLevel.CRITICAL_STRING.toLowerCase();
        // try conversion from the AlarmLevel object form to a string value (upper case).
        try {
            alarmLevel = AlarmLevel.fromString(strLevel);
            if (!alarmLevel.toString().equals(strLevel)) {
                sendResultToTCK("Test1113524Test", false, 1113532,
                        "This alarmLevel.toString method didn't return a string value (upper case)"
                                + "which specified by the AlarmLevel object form.");
                return;
            }
        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }

        // try conversion from the AlarmLevel object form to a string value (lower case).
        try {
            alarmLevel = AlarmLevel.fromString(strlcLevel);
            if (!alarmLevel.toString().equalsIgnoreCase(strlcLevel)) {
                sendResultToTCK("Test1113524Test", false, 1113532,
                        "This alarmLevel.toString method didn't return a string value (lower case)"
                                + "which specified by the AlarmLevel object form.");
                return;
            }
        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }

        // try conversion from a string value (upper case) to the AlarmLevel object form .
        alarmLevel = AlarmLevel.CRITICAL;
        try {
            strLevel = alarmLevel.toString();
            if (!alarmLevel.equals(AlarmLevel.fromString(strLevel))) {
                sendResultToTCK("Test1113524Test", false, 1113532,
                        "This AlarmLevel.fromString method didn't return the AlarmLevel "
                                + "object form which specified by a string value (upper case).");
                return;
            }
        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }

        // try conversion from a string value (lower case) to the AlarmLevel object form .
        try {
            strlcLevel = alarmLevel.toString().toLowerCase();
            if (!alarmLevel.equals(AlarmLevel.fromString(strlcLevel))) {
                sendResultToTCK("Test1113524Test", false, 1113532,
                        "This AlarmLevel.fromString(lowercaseLevel) method didn't return the AlarmLevel "
                                + "object form which specified by a string value (lower case).");
                return;
            }
        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }

        //1113532:CLEAR
        strLevel = AlarmLevel.CLEAR_STRING;
        strlcLevel = AlarmLevel.CLEAR_STRING.toLowerCase();
        // try conversion from the AlarmLevel object form to a string value (upper case).
        try {
            alarmLevel = AlarmLevel.fromString(strLevel);
            if (!alarmLevel.toString().equals(strLevel)) {
                sendResultToTCK("Test1113524Test", false, 1113532,
                        "This alarmLevel.toString method didn't return a string value (upper case)"
                                + "which specified by the AlarmLevel object form.");
                return;
            }
        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }

        // try conversion from the AlarmLevel object form to a string value (lower case).
        try {
            alarmLevel = AlarmLevel.fromString(strlcLevel);
            if (!alarmLevel.toString().equalsIgnoreCase(strlcLevel)) {
                sendResultToTCK("Test1113524Test", false, 1113532,
                        "This alarmLevel.toString method didn't return a string value (lower case)"
                                + "which specified by the AlarmLevel object form.");
                return;
            }
        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }

        // try conversion from a string value (upper case) to the AlarmLevel object form .
        alarmLevel = AlarmLevel.CLEAR;
        try {
            strLevel = alarmLevel.toString();
            if (!alarmLevel.equals(AlarmLevel.fromString(strLevel))) {
                sendResultToTCK("Test1113524Test", false, 1113532,
                        "This AlarmLevel.fromString method didn't return the AlarmLevel "
                                + "object form which specified by a string value (upper case).");
                return;
            }
        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }

        // try conversion from a string value (lower case) to the AlarmLevel object form .
        try {
            strlcLevel = alarmLevel.toString().toLowerCase();
            if (!alarmLevel.equals(AlarmLevel.fromString(strlcLevel))) {
                sendResultToTCK("Test1113524Test", false, 1113532,
                        "This AlarmLevel.fromString(lowercaseLevel) method didn't return the AlarmLevel "
                                + "object form which specified by a string value (lower case).");
                return;
            }
        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }

        //1113533
        boolean passed1113533 = false;
        try {
            alarmLevel = AlarmLevel.fromString(null);
        } catch (java.lang.NullPointerException e) {
            tracer.info("got expected NullPointerException with null value for the level argument.", null);
            passed1113533 = true;
        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }

        if (!passed1113533) {
            sendResultToTCK("Test1113524Test", false, 1113533,
                    "AlarmLevel.fromString(null) should have thrown java.lang.NullPointerException.");
            return;
        }

        //1113534
        // try this invalid trace level string "MINORS", should catch IllegalArgumentException
        String invalidStrLevel = AlarmLevel.MINOR_STRING.concat("S");
        boolean passed1113534 = false;
        try {
            alarmLevel = AlarmLevel.fromString(invalidStrLevel);
        } catch (java.lang.IllegalArgumentException iae) {
            tracer
                    .info(
                            "got expected IllegalArgumentException with an invalid value (upper case) for the level string argument.",
                            null);
            passed1113534 = true;
        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }

        if (!passed1113534) {
            sendResultToTCK("Test1113524Test", false, 1113534,
                    "AlarmLevel.fromString(MINORS) should have thrown java.lang.IllegalArgumentException.");
            return;
        }

        String lowercaseLevel = AlarmLevel.MINOR_STRING.toLowerCase().concat("s");
        passed1113534 = false;
        try {
            alarmLevel = AlarmLevel.fromString(lowercaseLevel);
        } catch (java.lang.IllegalArgumentException iae) {
            tracer
                    .info(
                            "got expected IllegalArgumentException with an invalid value (lower case) for the level string argument.",
                            null);
            passed1113534 = true;
        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }

        if (!passed1113534) {
            sendResultToTCK("Test1113524Test", false, 1113534,
                    "AlarmLevel.fromString(fines) should have thrown java.lang.IllegalArgumentException.");
            return;
        }

        sendResultToTCK("Test1113524Test", true, 1113524, "This AlarmLevel.isHigherLevel tests passed!");
    }

    private AlarmFacility getAlarmFacility() throws Exception {
        AlarmFacility facility = null;
        String JNDI_ALARMFACILITY_NAME = AlarmFacility.JNDI_NAME;
        try {
            facility = (AlarmFacility) new InitialContext().lookup(JNDI_ALARMFACILITY_NAME);
        } catch (Exception e) {
            tracer.warning("got unexpected Exception: " + e, null);
        }
        return facility;
    }

    private Tracer tracer;

}
