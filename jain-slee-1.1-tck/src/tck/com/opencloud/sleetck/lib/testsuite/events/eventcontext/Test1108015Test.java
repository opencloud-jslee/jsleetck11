/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.events.eventcontext;

import java.rmi.RemoteException;
import java.util.Map;

import javax.slee.management.DeployableUnitID;

import com.opencloud.sleetck.lib.AbstractSleeTCKTest;
import com.opencloud.sleetck.lib.DescriptionKeys;
import com.opencloud.sleetck.lib.TCKTestFailureException;
import com.opencloud.sleetck.lib.resource.TCKActivityID;
import com.opencloud.sleetck.lib.resource.TCKSbbMessage;
import com.opencloud.sleetck.lib.resource.events.TCKResourceEventX;
import com.opencloud.sleetck.lib.resource.testapi.TCKResourceListener;
import com.opencloud.sleetck.lib.resource.testapi.TCKResourceTestInterface;
import com.opencloud.sleetck.lib.testutils.Assert;
import com.opencloud.sleetck.lib.testutils.BaseTCKResourceListener;
import com.opencloud.sleetck.lib.testutils.FutureResult;

/**
 * AssertionID(1108015): Test This getEvent method returns the event 
 * object of the event that this Event Context is associated with. 
 * 
 * AssertionID(1108175): Test This getActivityContextInterface method 
 * returns the Activity Context Interface for the Activity which the 
 * event was fired on. The returned Activity Context is the generic 
 * Activity Context Interface.
 * 
 * AssertionID(1108176): Test This getAddress method returns the address 
 * which was provided when the event was fired.
 * 
 * AssertionID(1108178): Test This getService method returns the component 
 * identifier of the Service provided when the event was fired.
 * 
 * AssertionID(1108028): Test The java.lang.IllegalArgumentException is 
 * thrown if the timeout argument is zero or a negative value.
 * 
 */
public class Test1108015Test extends AbstractSleeTCKTest {

    /**
     * Perform the actual test.
     */

    public void run(FutureResult result) throws Exception {
        this.result = result;

        TCKResourceTestInterface resource = utils().getResourceInterface();
        TCKActivityID activityID = resource.createActivity("Test1108015InitialActivity");

        getLog().fine("Firing TCKResourceEventX1 on Test1108015InitialActivity.");
        resource.fireEvent(TCKResourceEventX.X1, testName, activityID, Test1108015Sbb.ADDRESS_1);
    }

    /**
     * Do all the pre-run configuration of the test.
     */

    public void setUp() throws Exception {
        getLog().fine("Connecting to resource");
        getLog().fine("Installing and activating service");
        resourceListener = new TCKResourceListenerImpl();

        setResourceListener(resourceListener);

        // Install the Deployable Unit.
        String duPath = utils().getTestParams().getProperty(DescriptionKeys.SERVICE_DU_PATH_PARAM);
        DeployableUnitID duID = utils().install(duPath);
        utils().activateServices(duID, true); // Activate the service
    }

    /**
     * Clean up after the test.
     */

    public void tearDown() throws Exception {
        // cleanup
        super.tearDown();
    }

    private class TCKResourceListenerImpl extends BaseTCKResourceListener {
        // TCKResourceListener methods

        public synchronized void onSbbMessage(TCKSbbMessage message, TCKActivityID calledActivity)
                throws RemoteException {

            Map sbbData = (Map) message.getMessage();
            String sbbTestName = "Test1108015Test";
            String sbbTestResult = (String) sbbData.get("result");
            String sbbTestMessage = (String) sbbData.get("message");
            int assertionID = ((Integer) sbbData.get("id")).intValue();
            getLog().info(
                    "Received message from SBB: testname=" + sbbTestName + ", result=" + sbbTestResult + ", message="
                            + sbbTestMessage + ", id=" + assertionID);
            try {
                Assert.assertEquals(assertionID, "Test " + sbbTestName + " failed.", "pass", sbbTestResult);
                result.setPassed();
            } catch (TCKTestFailureException ex) {
                result.setFailed(assertionID, sbbTestMessage);
            }
        }

        public void onException(Exception exception) throws RemoteException {
            getLog().warning("Received Exception from SBB or resource:");
            getLog().warning(exception);
            result.setError(exception);
        }
    }

    private TCKResourceListener resourceListener;

    private FutureResult result;
    
    private final static String testName = "Test1108015";
}