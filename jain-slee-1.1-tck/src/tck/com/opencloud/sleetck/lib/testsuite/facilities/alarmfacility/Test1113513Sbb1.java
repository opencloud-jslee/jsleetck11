/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.facilities.alarmfacility;

import java.util.HashMap;

import com.opencloud.sleetck.lib.resource.events.TCKResourceEventY;
import com.opencloud.sleetck.lib.sbbutils.BaseTCKSbb;
import com.opencloud.sleetck.lib.sbbutils.TCKSbbUtils;

import javax.naming.InitialContext;
import javax.slee.ActivityContextInterface;
import javax.slee.facilities.AlarmFacility;
import javax.slee.facilities.AlarmLevel;
import javax.slee.facilities.Tracer;

/*  
 * AssertionID(1113513): Test The return value of this clearAlarms() 
 * method is the number of alarms that were cleared.
 * 
 */
public abstract class Test1113513Sbb1 extends BaseTCKSbb {

    public static final String ALARM_MESSAGE = "Sbb1:Test1113513AlarmMessageSbb1";

    public static final String ALARM_INSTANCEID = "Test1113513AlarmInstanceIDSbb1";

    // Initial event
    public void onTCKResourceEventY1(TCKResourceEventY ev, ActivityContextInterface aci) {

        try {
            tracer = this.getSbbContext().getTracer("com.test");
            tracer.info("Received " + ev + " message", null);
            AlarmFacility facility = getAlarmFacility();
            String[] alarmIDs = new String[5];

            //raise an alarm
            alarmIDs[0] = facility.raiseAlarm("javax.slee.management.Alarm1", "Test1113513AlarmInstanceID1",
                    AlarmLevel.CRITICAL, ALARM_MESSAGE);
            alarmIDs[1] = facility.raiseAlarm("javax.slee.management.Alarm2", "Test1113513AlarmInstanceID2",
                    AlarmLevel.MAJOR, ALARM_MESSAGE);
            alarmIDs[2] = facility.raiseAlarm("javax.slee.management.Alarm3", "Test1113513AlarmInstanceID3",
                    AlarmLevel.WARNING, ALARM_MESSAGE);
            alarmIDs[3] = facility.raiseAlarm("javax.slee.management.Alarm4", "Test1113513AlarmInstanceID4",
                    AlarmLevel.INDETERMINATE, ALARM_MESSAGE);
            alarmIDs[4] = facility.raiseAlarm("javax.slee.management.Alarm5", "Test1113513AlarmInstanceID5",
                    AlarmLevel.MINOR, ALARM_MESSAGE);
            setAlarmIDs(alarmIDs);

        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }

    }

    // Third event
    public void onTCKResourceEventY2(TCKResourceEventY ev, ActivityContextInterface aci) {

        try {
            tracer = this.getSbbContext().getTracer("com.test");
            tracer.info("Received " + ev + " message again", null);
            AlarmFacility facility = getAlarmFacility();

            if (facility.clearAlarms() != getAlarmIDs().length) {
                sendResultToTCK("Test1113513Test", false, 1113513, "The return value of this facility.clearAlarms() method "
                        + "didn't match the number of alarms that supposed to be cleared!"
                        + "AlarmFacility.clearAlarms()= " + facility.clearAlarms() + "vs Total alarms= "
                        + getAlarmIDs().length * 2);
            }

            //try to clear the alarm again, should fail because of if there are no
            // alarms of this type are active
            if (facility.clearAlarms() != 0) {
                sendResultToTCK("Test1113513Test", false, 1113513, "There should be no alarms of this type are active, but "
                        + "facility.clearAlarms() returned a number of active alarms which" + "need to be cleared.");
                return;
            }

            sendResultToTCK("Test1113513Test", true, 1113513,"This method did only clear alarms associated with the notification "
                    + "source of the alarm facility object.!");
            return;

        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }

    }

    private void sendResultToTCK(String testName, boolean result, int failedAssertionID,  String message) throws Exception {
        HashMap sbbData = new HashMap();
        sbbData.put("testname", testName);
        sbbData.put("result", result ? "pass" : "fail");
        sbbData.put("message", message);
        sbbData.put("id", new Integer(failedAssertionID));
        TCKSbbUtils.getResourceInterface().sendSbbMessage(sbbData);
    }

    public abstract void setAlarmIDs(String[] alarmIDs);
    public abstract String[] getAlarmIDs();
    
    private AlarmFacility getAlarmFacility() throws Exception {
        AlarmFacility facility = null;
        String JNDI_ALARMFACILITY_NAME = AlarmFacility.JNDI_NAME;
        try {
            facility = (AlarmFacility) new InitialContext().lookup(JNDI_ALARMFACILITY_NAME);
        } catch (Exception e) {
            tracer.warning("got unexpected Exception: " + e, null);
        }
        return facility;
    }

    private Tracer tracer; 
}
