/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.profiles.readonly;

import com.opencloud.sleetck.lib.AbstractSleeTCKTest;
import com.opencloud.sleetck.lib.TCKTestResult;

/**
 * Test for assertion 1110075: A method implemented in the Profile abstract class can be defined in
 * both the Profile Local interface (e.g. as a business method) and the Profile Management interface
 * (e.g. as a management method).
 */
public class Test1110075Test extends AbstractSleeTCKTest {

    private static String DEFINED_TWICE_DU_PATH_PARAM = "definedTwiceDUPath";


    public TCKTestResult run() throws Exception {

        try {
            setupService(DEFINED_TWICE_DU_PATH_PARAM);
        } catch (Exception e) {
            return TCKTestResult.failed(1110075, "Deployment of profile spec with method 'manage()' defined " +
                    "as both, management and business method, failed with exception: "+e);
        }

        return TCKTestResult.passed();
    }

    public void setUp() throws Exception {
        //overrides behaviour of super class
    }
}
