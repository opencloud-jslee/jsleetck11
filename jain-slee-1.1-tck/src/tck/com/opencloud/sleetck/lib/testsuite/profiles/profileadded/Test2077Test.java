/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.profiles.profileadded;

import com.opencloud.sleetck.lib.SleeTCKTest;
import com.opencloud.sleetck.lib.SleeTCKTestUtils;
import com.opencloud.sleetck.lib.TCKTestResult;
import com.opencloud.sleetck.lib.resource.TCKActivityID;
import com.opencloud.sleetck.lib.resource.TCKSbbMessage;
import com.opencloud.sleetck.lib.resource.testapi.TCKResourceListener;
import com.opencloud.sleetck.lib.testutils.BaseTCKResourceListener;
import com.opencloud.sleetck.lib.testutils.FutureResult;
import com.opencloud.sleetck.lib.testutils.ProfileUtils;
import com.opencloud.sleetck.lib.testutils.jmx.DeploymentMBeanProxy;
import com.opencloud.sleetck.lib.testutils.jmx.ProfileMBeanProxy;
import com.opencloud.sleetck.lib.testutils.jmx.ProfileProvisioningMBeanProxy;

import javax.management.Attribute;
import javax.management.ObjectName;
import javax.slee.ComponentID;
import javax.slee.management.DeployableUnitDescriptor;
import javax.slee.management.DeployableUnitID;
import javax.slee.profile.ProfileSpecificationID;
import java.rmi.RemoteException;
import java.util.HashMap;

public class Test2077Test implements SleeTCKTest {

    private static final String SERVICE_DU_PATH_PARAM = "DUPath";
    private static final int TEST_ID = 2077;

    public void init(SleeTCKTestUtils utils) {
        this.utils = utils;
    }

    /**
     * Perform the actual test.
     */

    public TCKTestResult run() throws Exception {
        result = new FutureResult(utils.getLog());

        // Create the profile table.
        profileProvisioningProxy = new ProfileUtils(utils).getProfileProvisioningProxy();
        DeploymentMBeanProxy duProxy = utils.getDeploymentMBeanProxy();
        DeployableUnitDescriptor duDesc = duProxy.getDescriptor(duID);
        ComponentID components[] = duDesc.getComponents();

        for (int i = 0; i < components.length; i++) {
            if (components[i] instanceof ProfileSpecificationID) {
                profileProvisioningProxy.createProfileTable((ProfileSpecificationID) components[i], "Test2077ProfileTable");
            }
        }

        // Create a profile, set some values, then commit it.
        profileMBeanName = profileProvisioningProxy.createProfile("Test2077ProfileTable", "Test2077Profile");
        profileMBeanProxy = utils.getMBeanProxyFactory().createProfileMBeanProxy(profileMBeanName);

        // Set 'value' to '42'
        utils.getMBeanFacade().setAttribute(profileMBeanName, new Attribute("Value", new Integer(42)));

        // Commit the profile, this should generate a ProfileAddedEvent
        profileMBeanProxy.commitProfile();

        return result.waitForResultOrFail(utils.getTestTimeout(), "Timeout waiting for test result", TEST_ID);
    }

    /**
     * Do all the pre-run configuration of the test.
     */

    public void setUp() throws Exception {

        utils.getLog().fine("Connecting to resource");
        resourceListener = new TCKResourceListenerImpl();
        utils.getResourceInterface().setResourceListener(resourceListener);

        utils.getLog().fine("Installing and activating service");

        // Install the Deployable Units
        String duPath = utils.getTestParams().getProperty(SERVICE_DU_PATH_PARAM);
        duID = utils.install(duPath);

        // Start the DU's service.
        utils.activateServices(duID, true);
    }

    /**
     * Clean up after the test.
     */

    public void tearDown() throws Exception {

        if (profileMBeanProxy != null) {
            try {
                profileMBeanProxy.closeProfile();
            } catch(Exception e) { /*no-op*/ }
        }
        try {
            profileProvisioningProxy.removeProfile("Test2077ProfileTable", "Test2077Profile");
        } catch (Exception e) {
        }

        try {
            profileProvisioningProxy.removeProfileTable("Test2077ProfileTable");
        } catch (Exception e) {
        }

        utils.getLog().fine("Disconnecting from resource");
        utils.getResourceInterface().clearActivities();
        utils.getResourceInterface().removeResourceListener();
        utils.getLog().fine("Deactivating and uninstalling service");
        utils.deactivateAllServices();
        utils.uninstallAll();
    }

    private class TCKResourceListenerImpl extends BaseTCKResourceListener {
        public synchronized void onSbbMessage(TCKSbbMessage message, TCKActivityID activity) throws RemoteException {
            utils.getLog().info("Received message from SBB: " + message.getMessage());

            HashMap map = (HashMap) message.getMessage();

            String type = (String) map.get("Type");
            if (type != null) {
                try {
                    profileMBeanProxy.editProfile();

                    // Set 'value' to '43'
                    utils.getMBeanFacade().setAttribute(profileMBeanName, new Attribute("Value", new Integer(43)));

                    // Commit the profile, this should generate a ProfileUpdatedEvent
                    profileMBeanProxy.commitProfile();

                } catch (Exception e) {
                    onException(e);
                }

            } else {
                Boolean passed = (Boolean) map.get("Result");
                String msgString = (String) map.get("Message");
                Integer id = (Integer) map.get("ID");

                if (passed.booleanValue() == true)
                    result.setPassed();
                else
                    result.setFailed(id.intValue(), msgString);
            }
        }

        public void onException(Exception e) throws RemoteException {
            utils.getLog().warning("Received exception from SBB.");
            utils.getLog().warning(e);
            result.setError(e);
        }

    }

    private SleeTCKTestUtils utils;
    private TCKResourceListener resourceListener;
    private FutureResult result;
    private DeployableUnitID duID;
    private ProfileProvisioningMBeanProxy profileProvisioningProxy;
    private ObjectName profileMBeanName;
    private ProfileMBeanProxy profileMBeanProxy;
}
