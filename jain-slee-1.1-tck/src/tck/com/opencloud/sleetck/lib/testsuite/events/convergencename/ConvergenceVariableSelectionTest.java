/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.events.convergencename;

import com.opencloud.sleetck.lib.TCKTestErrorException;
import com.opencloud.sleetck.lib.TCKTestResult;
import com.opencloud.sleetck.lib.resource.TCKActivityID;
import com.opencloud.sleetck.lib.resource.events.TCKResourceEventX;
import com.opencloud.sleetck.lib.resource.testapi.TCKResourceTestInterface;

import javax.slee.Address;
import javax.slee.AddressPlan;
import java.util.Iterator;

/**
 * ConvergenceVariableSelectionTest tests the SLEE's handling convergence name
 * selection when one or more convergence name variables are selected.
 *
 * It only supports the following variables: ActivityContext, Address, EventType, and CustomName
 */
public class ConvergenceVariableSelectionTest extends AbstractConvergenceNameTest {

    // -- Constants -- //

    private static final String ADDRESS_A = ADDRESS_PREFIX+"1";
    private static final String ADDRESS_B = ADDRESS_PREFIX+"2";
    private static final String CUSTOM_NAME_A = "CUSTOM_NAME_A";
    private static final String CUSTOM_NAME_B = "CUSTOM_NAME_B";
    private static final String ACTIVITY_NAME_A = ACTIVITY_ID_PREFIX+"A";
    private static final String ACTIVITY_NAME_B = ACTIVITY_ID_PREFIX+"B";

    public void setUp() throws Exception {
        super.setUp();
        // validate selected variables
        if(selected() == null || selected().isEmpty()) throw new TCKTestErrorException(
            "At least one convergence name variable must be selected for this test.");
        if(selected().contains(ADDRESS_PROFILE)) throw new TCKTestErrorException(
            "The "+ADDRESS_PROFILE+" variable is not supported by this test.");
        if(selected().contains(EVENT_OBJECT)) throw new TCKTestErrorException(
            "The "+EVENT_OBJECT+" variable is not supported by this test.");
    }

    public TCKTestResult run() throws Exception {

        TCKResourceTestInterface resource = utils().getResourceInterface();

        // prepare convergence name variables for the first event

        String eventType = TCKResourceEventX.X1;
        Address address = new Address(AddressPlan.IP,ADDRESS_A);
        TCKActivityID activityID = resource.createActivity(ACTIVITY_NAME_A);
        String customName = CUSTOM_NAME_A;
        int eventNumber = 1;
        String eventID = Integer.toString(eventNumber);
        String firstEventID = eventID;

        // fire the first event
        sendEventAndWait(eventType,eventID,activityID,address,eventID,
                         createIESParamsFromSelected(customName));

        // send the second event with the same convergence name values as the first event

        eventID = Integer.toString(++eventNumber);
        sendEventAndWait(eventType,eventID,activityID,address,firstEventID, // expect it to go the existing instance
                        createIESParamsFromSelected(customName));

        // iterate through the selected variables, varying one each time before firing an event
        Iterator selectedIter = selected().iterator();
        while(selectedIter.hasNext()) {
            Object variableToVary = selectedIter.next();

            // alter the specified variable

            if(ADDRESS.equals(variableToVary)) address = new Address(AddressPlan.IP,ADDRESS_B);
            if(EVENT_TYPE.equals(variableToVary)) eventType = TCKResourceEventX.X2;
            if(CUSTOM_NAME.equals(variableToVary)) customName = CUSTOM_NAME_B;
            if(ACTIVITY_CONTEXT.equals(variableToVary)) activityID = resource.createActivity(ACTIVITY_NAME_B);

            // send the next event, which should cause a new SBB instance to be created
            eventID = Integer.toString(++eventNumber);
            sendEventAndWait(eventType,eventID,activityID,address,eventID,
                             createIESParamsFromSelected(customName));
        }

        return TCKTestResult.passed();
    }

    /**
     * Creates and returns an InitialEventSelectorParameters object
     * based on the selected convergence name variables, and the given
     * custom name.
     */
    private InitialEventSelectorParameters createIESParamsFromSelected(String customName) {
        return new InitialEventSelectorParameters(
            selected().contains(ACTIVITY_CONTEXT),
            selected().contains(ADDRESS),
            false, // address profile
            false, // event object
            selected().contains(EVENT_TYPE),
            customName,
            false, false, // IES initial event flag
            false, null); // don't change the address in the IES
    }

}