/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.profiles.profileabstractclass;

import javax.slee.CreateException;
import javax.slee.profile.Profile;
import javax.slee.profile.ProfileContext;
import javax.slee.profile.ProfileVerificationException;
import javax.slee.usage.UnrecognizedUsageParameterSetNameException;

/**
 * GetUsageParameterSet method 2nd form is included (-> should deploy)
 */
public abstract class Test1110093_19Profile implements ProfileAbstractClassTestsProfileCMP, Profile  {

    public abstract Test1110093ProfileUsage getUsageParameterSet(String name)
        throws UnrecognizedUsageParameterSetNameException;

    public void setProfileContext(ProfileContext context) {
    }

    public void unsetProfileContext() {
    }

    public void profileInitialize() {
    }

    public void profilePostCreate() throws CreateException {
    }

    public void profileActivate() {
    }

    public void profilePassivate() {
    }

    public void profileLoad() {
    }

    public void profileStore() {
    }

    public void profileRemove() {
    }

    public void profileVerify() throws ProfileVerificationException {
    }
}
