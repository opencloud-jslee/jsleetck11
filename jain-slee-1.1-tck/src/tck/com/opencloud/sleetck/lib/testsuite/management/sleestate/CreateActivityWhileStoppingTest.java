/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.management.sleestate;

import com.opencloud.sleetck.lib.AbstractSleeTCKTest;
import com.opencloud.sleetck.lib.OperationTimedOutException;
import com.opencloud.sleetck.lib.TCKTestErrorException;
import com.opencloud.sleetck.lib.TCKTestResult;
import com.opencloud.sleetck.lib.infra.SleeTCKComponentConstants;
import com.opencloud.sleetck.lib.resource.TCKActivityID;
import com.opencloud.sleetck.lib.resource.events.TCKResourceEventX;
import com.opencloud.sleetck.lib.resource.events.TCKResourceEventY;
import com.opencloud.sleetck.lib.resource.testapi.TCKResourceTestInterface;
import com.opencloud.sleetck.lib.testsuite.profiles.ProfileTestConstants;
import com.opencloud.sleetck.lib.testutils.*;
import com.opencloud.sleetck.lib.testutils.jmx.SleeManagementMBeanProxy;
import com.opencloud.util.Future;

import javax.slee.management.SleeState;
import javax.slee.profile.ProfileSpecificationID;
import java.rmi.RemoteException;

/**
 * Tests assertion 1473, that no new Activity objects are accepted from
 * resource adaptor entities or created by the SLEE in the STOPPING state
 * This covers the following activity types:
 * ProfileTableActivity, NullActivity, TCKActivity.
 */
public class CreateActivityWhileStoppingTest extends AbstractSleeTCKTest {

    // -- Constants -- //

    // Test cases: Activity types
    private static final int PROFILE_TABLE_ACTIVITY = 1;
    private static final int NULL_ACTIVITY = 3;
    private static final int TCK_ACTIVITY = 4;

    // -- Implementation of SleeTCKTest methods -- //

    public TCKTestResult run() throws Exception {
        int[] activityTypes = {NULL_ACTIVITY,PROFILE_TABLE_ACTIVITY,TCK_ACTIVITY};
        TCKTestResult rResult = null;
        for (int i = 0; i < activityTypes.length; i++) {
            rResult = testCreateActivity(activityTypes[i]);
            if(!rResult.isPassed()) break; // abort on the first failure/error
        }
        return rResult;
    }

    public void setUp() throws Exception {
        resourceListener = new ResourceListenerImpl();
        setResourceListener(resourceListener);
        profileUtils = new ProfileUtils(utils());
        utils().install(utils().getTestParams().getProperty(ProfileTestConstants.PROFILE_SPEC_DU_PATH_PARAM));
        profileSpecID = new ComponentIDLookup(utils()).lookupProfileSpecificationID(
                ProfileTestConstants.SIMPLE_PROFILE_SPEC_NAME,SleeTCKComponentConstants.TCK_VENDOR,"1.0");
        super.setUp();
    }

    public void tearDown() throws Exception {
        resourceListener = null;
        profileUtils = null;
        super.tearDown();
    }

    // Private methods

    private TCKTestResult testCreateActivity(int activityType) throws Exception {
        try {
            // reset flags
            isTestingCreateTCKActivity = isSecondEventACKReceived = false;
            resourceListener.resetFlags();
            // create an activity and attach an Sbb to it by sending it an initial event
            getLog().info("Firing an event on an activity to attach an Sbb to the activity");
            stallingActivityID = utils().getResourceInterface().createActivity("tck.CreateActivityWhileStoppingTest.StallingActivity."+activityType);
            synchronized(resourceListener) {
                x1EventObjectID = utils().getResourceInterface().fireEvent(TCKResourceEventX.X1,null,stallingActivityID,null);
            }
            waitForSbbAttach();
            getLog().info("The Sbb is attached to the activity");
            SleeManagementMBeanProxy management = utils().getSleeManagementMBeanProxy();
            getLog().info("Calling SleeManagementMBean.stop()");
            management.stop();
            SleeState currentState = management.getState();
            if(currentState.isStopping()) getLog().info("Current state: "+currentState);
            else throw new TCKTestErrorException("Couldn't test STOPPING state semantics, "+
                "because the SLEE was not in the STOPPING state following stop(). Current state: "+currentState);
            testCaseResult = new FutureResult(getLog());
            switch (activityType) {
                case PROFILE_TABLE_ACTIVITY:    return testCreateProfileTableActivity();
                case NULL_ACTIVITY:             return testCreateNullActivity();
                case TCK_ACTIVITY:              return testCreateTCKActivity();
                default: throw new IllegalArgumentException("Unrecognized activity type code: "+activityType);
            }
        } finally {
            getLog().info("Ending the activity used to stall the transition to the STOPPING state");
            endActivitySilent(stallingActivityID);
            getLog().info("Restarting the SLEE");
            SleeStarter.startSlee(utils().getSleeManagementMBeanProxy(),utils().getTestTimeout());
        }
    }

    private TCKTestResult testCreateProfileTableActivity() throws Exception {
        String profileTableName = "tck.CreateActivityWhileStoppingTest.tableA";
        boolean createdTable = false;
        try {
            profileUtils.getProfileProvisioningProxy().createProfileTable(profileSpecID,profileTableName);
            createdTable = true;
            getLog().info("Firing a Y1 event to the Sbb to prompt an attempt to lookup a ProfileTableActivity");
            synchronized(resourceListener) {
                y1EventObjectID = utils().getResourceInterface().fireEvent(TCKResourceEventY.Y1,profileTableName,stallingActivityID,null);
            }
            try {
                return testCaseResult.waitForResult(utils().getTestTimeout());
            } catch (Future.TimeoutException toe) {
                // allow this only if the SLEE has ended the activity itself, which would prevent event delivery to the Sbb
                if(resourceListener.hasActivityEnded()) {
                    getLog().warning("Wasn't able to test ProfileTableActivity creation because the SLEE "+
                    "timed out the activity which was holding it in the STOPPING state (this is valid behaviour)");
                    return TCKTestResult.passed();
                } else throw new TCKTestErrorException("testCreateProfileTableActivity(): Timed out while waiting for the Sbb to receive the Y1 event");
            }
        } finally {
            if(createdTable) profileUtils.removeProfileTable(profileTableName);
        }
    }

    private TCKTestResult testCreateNullActivity() throws Exception {
        getLog().info("Firing an X2 event to the Sbb to prompt an attempt to create a NullActivity");
        synchronized(resourceListener) {
            x2EventObjectID = utils().getResourceInterface().fireEvent(TCKResourceEventX.X2,null,stallingActivityID,null);
        }
        try {
            return testCaseResult.waitForResult(utils().getTestTimeout());
        } catch (Future.TimeoutException toe) {
            // allow this only if the SLEE has ended the activity itself, which would prevent event delivery to the Sbb
            if(resourceListener.hasActivityEnded()) {
                getLog().warning("Wasn't able to test NullActivity creation because the SLEE "+
                "timed out the activity which was holding it in the STOPPING state (this is valid behaviour)");
                return TCKTestResult.passed();
            } else throw new TCKTestErrorException("testCreateNullActivity(): Timed out while waiting to the Sbb to receive the Y1 event");
        }
    }

    private TCKTestResult testCreateTCKActivity() throws Exception {
        isTestingCreateTCKActivity = true;
        TCKResourceTestInterface resource = utils().getResourceInterface();
        TCKActivityID createdActivity = null;
        try {
            createdActivity = resource.createActivity("tck.CreateActivityWhileStoppingTest.IllegalActivity");
            if(createdActivity != null) {
                getLog().warning("testCreateTCKActivity(): Successfully created a TCKActivity "+
                 "via the resource adaptor. activity ID:"+createdActivity+
                 ". Current SLEE state:"+utils().getSleeManagementMBeanProxy().getState());
            } else {
                getLog().info("testCreateTCKActivity(): Attempt to create an Activity failed during/after the STOPPING state "+
                 "(returned ID was null). Will interpret this as a pass.");
                return TCKTestResult.passed();
            }
            getLog().info("Firing an event on the new activity to test whether it was accepted by the SLEE container itself");
            try {
                utils().getResourceInterface().fireEvent(TCKResourceEventY.Y2,null,createdActivity,null);
                getLog().warning("testCreateTCKActivity(): Fired an event on a TCKActivity that was"+
                 "created during/after the STOPPING state, and received no Exception. Current SLEE state:"+
                    utils().getSleeManagementMBeanProxy().getState());
            } catch (Exception ex) {
                getLog().info("testCreateTCKActivity(): Attempt to fire an event on a TCKActivity that was"+
                 "created during/after the STOPPING state threw an Exception. Will interpret this as a pass. Exception:");
                getLog().warning(ex);
                return TCKTestResult.passed();
            }
            try {
                return testCaseResult.waitForResult(utils().getTestTimeout());
            } catch (Future.TimeoutException ex) {
                // We should have received either an ACK or a failure callback for the event.
                // Allow this timeout only if the SLEE has ended the stalled activity itself,
                // as this could prevent event delivery to the Sbb (the SLEE would be free to stop)
                if(resourceListener.hasActivityEnded()) {
                    getLog().warning("Wasn't able to test TCKActivity creation because the SLEE "+
                    "timed out the activity which was holding it in the STOPPING state (this is valid behaviour)");
                    return TCKTestResult.passed();
                } else throw new TCKTestErrorException("testCreateTCKActivity(): Timed out while waiting the Sbb "+
                    "to receive the Y2 event, or a failure callback for Y2.");
            }
        } finally {
            endActivitySilent(createdActivity);
        }
    }

    /**
     * Attempts the end the given activity, if live and not null,
     * and logs any Exceptions caught
     */
    private void endActivitySilent(TCKActivityID activityToEnd) {
        if(activityToEnd != null) {
            try {
                if(utils().getResourceInterface().isLive(activityToEnd)) {
                    utils().getResourceInterface().endActivity(activityToEnd);
                }
            } catch (Exception ex) {
                getLog().warning("Received Exception while trying to end TCKActivity "+activityToEnd+". Exception:");
                getLog().warning(ex);
            }
        }
    }

    /**
     * Waits for the Sbb to receive the event (and therfore become attached to the activity)
     */
    private void waitForSbbAttach() throws OperationTimedOutException {
        synchronized (resourceListener) {
            long now = System.currentTimeMillis();
            long timeoutAt = now + utils().getTestTimeout();
            while(timeoutAt > now && !resourceListener.hasSbbAttached()) {
                try {
                    resourceListener.wait(timeoutAt - now);
                    now = System.currentTimeMillis();
                } catch (InterruptedException ie) { /* no-op */ }
            }
            if(!resourceListener.hasSbbAttached()) throw new OperationTimedOutException(
                "Timed out while waiting for the Sbb to receive an event");
        }
    }

    private String formatCode(int sbbCallCode) {
        switch (sbbCallCode) {
            case CreateActivityWhileStoppingTestConstants.NULL_ACTIVITY_CREATED_FAILED: return "NULL_ACTIVITY_CREATED_FAILED";
            case CreateActivityWhileStoppingTestConstants.NULL_ACTIVITY_CREATE_SUCCEEDED: return "NULL_ACTIVITY_CREATE_SUCCEEDED";
            case CreateActivityWhileStoppingTestConstants.PROFILE_TABLE_ACTIVITY_FOUND: return "PROFILE_TABLE_ACTIVITY_FOUND";
            case CreateActivityWhileStoppingTestConstants.PROFILE_TABLE_ACTIVITY_NOT_FOUND: return "PROFILE_TABLE_ACTIVITY_NOT_FOUND";
            case CreateActivityWhileStoppingTestConstants.RECEIVED_X1: return "RECEIVED_X1";
            case CreateActivityWhileStoppingTestConstants.RECEIVED_Y2: return "RECEIVED_Y2";
            default: return "(Unrecognized Sbb call code: "+sbbCallCode+")";
        }
    }

    // -- Private classes -- //

    private class ResourceListenerImpl extends BaseTCKResourceListener {

        public synchronized Object onSbbCall(Object argument) throws Exception {
            try {
                int callCode = ((Integer)argument).intValue();
                getLog().info("Received call from Sbb:"+formatCode(callCode));
                SleeState currentState = utils().getSleeManagementMBeanProxy().getState();
                switch (callCode) {
                    case CreateActivityWhileStoppingTestConstants.RECEIVED_X1:
                        hasSbbAttached = true;
                        notifyAll();
                        break;
                    case CreateActivityWhileStoppingTestConstants.NULL_ACTIVITY_CREATED_FAILED:
                        testCaseResult.setPassed();
                        break;
                    case CreateActivityWhileStoppingTestConstants.NULL_ACTIVITY_CREATE_SUCCEEDED:
                        testCaseResult.setFailed(1473,"The Sbb created a NullActivity object during the STOPPING or STOPPED state. Current SLEE state:"+currentState);
                        break;
                    case CreateActivityWhileStoppingTestConstants.PROFILE_TABLE_ACTIVITY_FOUND:
                        testCaseResult.setFailed(1473,"The Sbb found a ProfileTableActivity for an profile table created during "+
                        "the STOPPING or STOPPED state. Current SLEE state:"+currentState);
                        break;
                    case CreateActivityWhileStoppingTestConstants.PROFILE_TABLE_ACTIVITY_NOT_FOUND:
                        testCaseResult.setPassed();
                        break;
                    case CreateActivityWhileStoppingTestConstants.RECEIVED_Y2:
                        testCaseResult.setFailed(1473,"The Sbb received an event on an activity which was created "+
                        "during the STOPPING or STOPPED state. Current SLEE state: "+currentState);
                        break;
                    default: throw new TCKTestErrorException("Unrecognized call code from Sbb: "+callCode);
                }
                if(!(callCode == CreateActivityWhileStoppingTestConstants.RECEIVED_X1)) isSecondEventACKReceived = true;
            } catch (Exception ex) {
                onException(ex);
            }
            return null;
        }

        public synchronized void onEventProcessingSuccessful(long eventObjectID) throws RemoteException {
            getLog().info("onEventProcessingSuccessful(eventObjectID="+eventObjectID+")");
            if(eventObjectID != x1EventObjectID && !isSecondEventACKReceived) {
                onSecondEventNotDelivered(eventObjectID);
            }
        }

        public synchronized void onEventProcessingFailed(long eventObjectID, String message, Exception exception) {
            getLog().info("onEventProcessingFailed(eventObjectID="+eventObjectID+", message="+message);
            if(eventObjectID == x1EventObjectID) {
                StringBuffer buf = new StringBuffer("Received onEventProcessingFailed() callback for X1 event");
                if(message != null) buf.append(message);
                onException(new TCKTestErrorException(buf.toString(),exception));
            } else if(!isSecondEventACKReceived) {
                onSecondEventNotDelivered(eventObjectID);
            }
        }

        public synchronized void onActivityContextInvalid(TCKActivityID activityID) {
            if(activityID.equals(stallingActivityID)) {
                hasActivityEnded = true;
                getLog().info("ResourceListenerImpl: received notification of activity end for stalling activity");
            } else onException(new TCKTestErrorException("Received unexpected onActivityContextInvalid() callback for activity: "+activityID));
        }

        public synchronized void onException(Exception e) {
            utils().getLog().warning("Received Exception from resource:");
            utils().getLog().warning(e);
            if(isTestingCreateTCKActivity) {
                utils().getLog().warning("Not aborting the test, because the current test is testCreateTCKActivity. "+
                        "The resource or resource adaptor may be throwing an Exception because of the attempt to "+
                        "create a new activity while the SLEE is in the STOPPING state.");
            } else {
                testCaseResult.setError(e);
            }
        }

        /**
         * Returns true if and only if the listener has received a message from the Sbb
         * (i.e. the Sbb has been attached to the activity)
         */
        public synchronized boolean hasSbbAttached() {
            return hasSbbAttached;
        }

        /**
         * Returns true if and only if the listener has received notification that the stalling TCK activity
         * has ended
         */
        public synchronized boolean hasActivityEnded() {
            return hasActivityEnded;
        }

        /**
         * Resets the hasSbbAttached and hasActivityEnded flags
         */
        public void resetFlags() {
            hasSbbAttached = hasActivityEnded = false;
        }

        /**
         * This method is called when onEventProcessingFailed() or onEventProcessingSuccessful() is called
         * for the second event before ACK is received from the second event handler
         */
        private synchronized void onSecondEventNotDelivered(long eventObjectID) {
            if(eventObjectID == x2EventObjectID || eventObjectID == y1EventObjectID) {
                String eventName = eventObjectID == x2EventObjectID ? "X2" : "Y1";
                if(hasActivityEnded()) {
                    getLog().info("Event delivery failed for "+eventName+" event after the stalling "+
                            "activity ended. Allow this: as the SLEE may have stopped after the activity ended. "+
                            "Will allow the test case to timeout...");
                } else {
                    testCaseResult.setError("Event delivery failed for "+eventName+" event before the stalling "+
                            "activity ended");
                }
            } else {
                getLog().info("The Y2 event was not delivered. Will interpret this as a pass "+
                    "(assuming the delivery failed because the activity was not created inside the SLEE)");
                testCaseResult.setPassed();
            }
        }

        private boolean hasSbbAttached;
        private boolean hasActivityEnded;

    }

    // -- Private state -- //

    private ProfileSpecificationID profileSpecID;
    private ProfileUtils profileUtils;
    private ResourceListenerImpl resourceListener;
    private TCKActivityID stallingActivityID;
    private FutureResult testCaseResult;
    private long x1EventObjectID;
    private long x2EventObjectID;
    private long y1EventObjectID;
    private boolean isSecondEventACKReceived;
    private boolean isTestingCreateTCKActivity;

}
