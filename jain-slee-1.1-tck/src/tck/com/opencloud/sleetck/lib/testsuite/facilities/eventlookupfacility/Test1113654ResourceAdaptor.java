/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.facilities.eventlookupfacility;

import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.slee.Address;
import javax.slee.ServiceID;
import javax.slee.facilities.EventLookupFacility;
import javax.slee.resource.ActivityHandle;
import javax.slee.resource.ConfigProperties;
import javax.slee.resource.FailureReason;
import javax.slee.resource.FireableEventType;
import javax.slee.resource.InvalidConfigurationException;
import javax.slee.resource.Marshaler;
import javax.slee.resource.ReceivableService;
import javax.slee.resource.ResourceAdaptorContext;

import com.opencloud.sleetck.lib.rautils.BaseTCKRA;
import com.opencloud.sleetck.lib.rautils.MessageHandler;
import com.opencloud.sleetck.lib.rautils.MessageHandlerRegistry;
import com.opencloud.sleetck.lib.rautils.TCKRAUtils;
import com.opencloud.sleetck.lib.rautils.TraceLogger;

/**
 * AssertionID(1113654): Test This getEventType method returns the event type identifier 
 * of the event type described by the FireableEventType object.
 * 
 * AssertionID(1113655): This getEventClassName method returns the fully-qualified name 
 * of the event class for the event type described by the FireableEventType object. 
 * This is the class name specified in the event type's deployment descriptor.
 * 
 * AssertionID(1113656): Test This getEventClassLoader method returns a class loader for 
 * the event type described by the FireableEventType object.
 * 
 * AssertionID(1113662): Test This getFireableEventType method returns a FireableEventType 
 * object for the event type identified by the eventType argument.
 * 
 * AssertionID(1113663): Test this method throws a java.lang.NullPointerException if the 
 * eventType argument is null.
 * 
 * AssertionID(1113664): Test It throws a javax.slee.UnrecognizedEventException if the 
 * eventType argument does not identify an event type in the SLEE.
 * 
 * AssertionID(1113665): Test An UnrecognizedEventException is also thrown if the eventType 
 * argument does not identify an event type that the Resource Adaptor may fire (as determined 
 * by the resource adaptor types referenced by the Resource Adaptor) unless event type checking 
 * has been disabled for the Resource Adaptor (see Section 15.10).
 **/
public class Test1113654ResourceAdaptor extends BaseTCKRA {

    public void setResourceAdaptorContext(ResourceAdaptorContext context) {
        super.setResourceAdaptorContext(context);
        setTracer(context.getTracer("Test1113654ResourceAdaptor: eventlookupfacility test RA"));
        try {
            MessageHandlerRegistry registry = TCKRAUtils.lookupMessageHandlerRegistry();
            messageHandler = new Test1113654MessageListener(this);
            registry.registerMessageHandler(messageHandler);
        } catch (Exception e) {
            getLog().severe("Test1113654ResourceAdaptor: An error occured during setResourceAdaptorContext()", e);
        }
    }

    public void unsetResourceAdaptorContext() {
        try {
            MessageHandlerRegistry registry = TCKRAUtils.lookupMessageHandlerRegistry();
            if (messageHandler != null)
                registry.unregisterMessageHandler(messageHandler);
        } catch (Exception e) {
            getLog().severe("Test1113654ResourceAdaptor: An error occured during unsetResourceAdaptorContext()", e);
        }
        setTracer(null);
        super.unsetResourceAdaptorContext();
    }

    public void activityEnded(ActivityHandle arg0) {
    }

    public void activityUnreferenced(ActivityHandle arg0) {
    }

    public void administrativeRemove(ActivityHandle arg0) {
    }

    public void raConfigurationUpdate(ConfigProperties arg0) {
    }

    public void raConfigure(ConfigProperties arg0) {
    }

    public void raUnconfigure() {
    }

    public void raActive() {
    }

    public void raInactive() {
    }

    public void raStopping() {
    }

    public void raVerifyConfiguration(ConfigProperties arg0) throws InvalidConfigurationException {
    }

    public void eventProcessingFailed(ActivityHandle arg0, FireableEventType arg1, Object arg2, Address arg3, ReceivableService arg4, int arg5, FailureReason arg6) {
    }

    public void eventProcessingSuccessful(ActivityHandle arg0, FireableEventType arg1, Object arg2, Address arg3, ReceivableService arg4, int arg5) {
    }

    public void eventUnreferenced(ActivityHandle arg0, FireableEventType arg1, Object arg2, Address arg3, ReceivableService arg4, int arg5) {
    }

    public Object getActivity(ActivityHandle arg0) {
        return null;
    }

    public ActivityHandle getActivityHandle(Object arg0) {
        return null;
    }

    public Marshaler getMarshaler() {
        return null;
    }

    public void queryLiveness(ActivityHandle arg0) {
    }

    public void serviceActive(ReceivableService arg0) {
    }

    public void serviceInactive(ReceivableService arg0) {
    }

    public void serviceStopping(ReceivableService arg0) {
    }
    
    public TraceLogger getLog() {
        return super.getLog();
    }

    private MessageHandler messageHandler = null;
}
