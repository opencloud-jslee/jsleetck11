/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.sbb.lifecycle;

import java.util.HashMap;

import javax.slee.ServiceID;
import javax.slee.management.DeployableUnitID;

import com.opencloud.sleetck.lib.OperationTimedOutException;
import com.opencloud.sleetck.lib.SleeTCKTest;
import com.opencloud.sleetck.lib.SleeTCKTestUtils;
import com.opencloud.sleetck.lib.TCKTestResult;
import com.opencloud.sleetck.lib.resource.TCKActivityID;
import com.opencloud.sleetck.lib.resource.events.TCKResourceEventX;
import com.opencloud.sleetck.lib.resource.testapi.TCKResourceTestInterface;
import com.opencloud.sleetck.lib.testutils.FutureResult;
import com.opencloud.sleetck.lib.testutils.QueuingResourceListener;


public class Test1106001Test implements SleeTCKTest {

    private static final String SERVICE_DU_PATH_PARAM = "DUPath";
    private static final int TEST_ID = 1106001;

    public void init(SleeTCKTestUtils utils) {
        this.utils = utils;
    }

    /**
     * Perform the actual test.
     */

    public TCKTestResult run() throws Exception {
        ServiceID serviceID = new ServiceID("Test1106001TestService", "jain.slee.tck", "1.1");
        result = new FutureResult(utils.getLog());
        HashMap message;
        boolean keepgoing;
        
        // Activate the service - causes some SBB lifecycle events.
        utils.getServiceManagementMBeanProxy().activate(serviceID);

        TCKResourceTestInterface resource = utils.getResourceInterface();
        TCKActivityID activityID = resource.createActivity("Test1106001InitialActivity");
        resource.fireEvent(TCKResourceEventX.X1, TCKResourceEventX.X1, activityID, null);

        // Deactivate the service - causes some SBB lifecycle events.
        utils.getLog().fine("Deactivating and uninstalling service");
        utils.getServiceManagementMBeanProxy().deactivate(serviceID);

        while(true) {
            try {
                message = (HashMap)resourceListener.nextMessage().getMessage();
                utils.getLog().info("Got message from SBB Method: "+message);
                if (null == message) break;
                if (null != message.get("TCKResult")) return (TCKTestResult)message.get("TCKResult");
            } catch (OperationTimedOutException e) {
                break;
            }
        }
        
        
        return TCKTestResult.passed();
    }

    /**
     * Do all the pre-run configuration of the test.
     */

    public void setUp() throws Exception {

        utils.getLog().fine("Connecting to resource");
        resourceListener = new QueuingResourceListener(utils);
        utils.getResourceInterface().setResourceListener(resourceListener);

        utils.getLog().fine("Installing service");
        String duPath = utils.getTestParams().getProperty(SERVICE_DU_PATH_PARAM);
        duID = utils.install(duPath);
    }

    /**
     * Clean up after the test.
     */

    public void tearDown() throws Exception {
        utils.getLog().fine("Disconnecting from resource");
        utils.getResourceInterface().clearActivities();
        utils.getResourceInterface().removeResourceListener();

        utils.uninstallAll();
    }

    private SleeTCKTestUtils utils;
    private FutureResult result;
    private DeployableUnitID duID;
    private QueuingResourceListener resourceListener;
}