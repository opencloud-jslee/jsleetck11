/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.management.ResourceManagementMBean;

import java.util.HashSet;

import javax.management.ObjectName;
import javax.slee.management.DeployableUnitID;
import javax.slee.management.ResourceAdaptorEntityState;
import javax.slee.management.UsageParameterSetNameAlreadyExistsException;
import javax.slee.resource.ConfigProperties;
import javax.slee.resource.ResourceAdaptorID;
import javax.slee.usage.UnrecognizedUsageParameterSetNameException;

import com.opencloud.sleetck.lib.AbstractSleeTCKTest;
import com.opencloud.sleetck.lib.TCKTestResult;
import com.opencloud.sleetck.lib.infra.SleeTCKComponentConstants;
import com.opencloud.sleetck.lib.rautils.MessageHandler;
import com.opencloud.sleetck.lib.rautils.RMIObjectChannel;
import com.opencloud.sleetck.lib.rautils.UOID;
import com.opencloud.sleetck.lib.testsuite.resource.RAMethods;
import com.opencloud.sleetck.lib.testsuite.resource.TCKMessage;
import com.opencloud.sleetck.lib.testutils.FutureResult;
import com.opencloud.sleetck.lib.testutils.jmx.ResourceManagementMBeanProxy;
import com.opencloud.sleetck.lib.testutils.jmx.ResourceUsageMBeanProxy;
import com.opencloud.sleetck.lib.testutils.jmx.SbbUsageMBeanProxy;
import com.opencloud.sleetck.lib.testsuite.resource.SimpleUsageParameterInterface;

/**
 * Test  getting Resource Adaptor properties through the ResourceManagementMBean interface.
 * <p>
 * Test assertions: 1114096
 */
public class Test1114125Test extends AbstractSleeTCKTest {

    private static final String RA_NAME = "TCK_Context_Test_RA";
    private static final String RA_ENTITY_NAME1 = RA_NAME + "_Entity1";
    private static final String RA_VENDOR = SleeTCKComponentConstants.TCK_VENDOR;
    private static final String RA_VERSION = "1.1";

    private static final String RESOURCE_DU_PATH_PARAM = "resourceDUPath";
    private static final String SERVICE_DU_PATH_PARAM = "serviceDUPath";

    public TCKTestResult run() throws Exception {
        TCKTestResult result1;

        resourceMBean = utils().getResourceManagementMBeanProxy();
        getLog().info("Created ResourceManagementMBeanProxy: " + resourceMBean );

        FutureResult future = new FutureResult(getLog());

        RMIObjectChannel in = utils().getRMIObjectChannel();
        in.setMessageHandler(new CallbackListener(future, 1));

        // Create an RA Entity (based on RA Context Resource Adaptor)
        raID = new ResourceAdaptorID(RA_NAME, RA_VENDOR, RA_VERSION);
        resourceMBean.createResourceAdaptorEntity(raID, RA_ENTITY_NAME1, new ConfigProperties());
        resourceMBean.activateResourceAdaptorEntity(RA_ENTITY_NAME1);

        // Do the ResourceUsageMBean tests
        ResourceUsageMBeanProxy resourceUsageMBean = utils().getResourceUsageMBeanProxy(RA_ENTITY_NAME1);
        getLog().fine("Created UsageMBeanProxy");

        result1 = doResourceUsageMBeanCheck(resourceUsageMBean, RA_ENTITY_NAME1);

        resourceMBean.deactivateResourceAdaptorEntity(RA_ENTITY_NAME1);
        getLog().info("Waiting for RA Entity to enter INACTIVE state");
        ResourceAdaptorEntityState raState = null;
        long timeout = System.currentTimeMillis() + utils().getTestTimeout();
        while (System.currentTimeMillis() < timeout) {
            raState = resourceMBean.getState(RA_ENTITY_NAME1);
            if (raState.isInactive())
                break;
            Thread.sleep(500);
        }
        getLog().fine("Current RA state = " +raState);
        if (!ResourceAdaptorEntityState.INACTIVE.equals(raState))
            return TCKTestResult.failed(1114125, "RA did not transition into the INACTIVE state within test timeout");

          return result1;
    }

    public void tearDown() throws Exception {
        try {
            utils().removeRAEntities();
        } finally {
            super.tearDown();
        }
    }

    public void setUp() throws Exception {
        String duPath = utils().getTestParams().getProperty(RESOURCE_DU_PATH_PARAM);
        getLog().fine("Installing DU: " + duPath );
        utils().install(duPath);
    }

    //
    // Private
    //

    private class CallbackListener implements MessageHandler {
        public CallbackListener(FutureResult result, int expectedCount) {
            this.result = result;
            this.expectedCount = expectedCount;
            getLog().fine("CallbackListener received message ");
        }

        public boolean handleMessage(Object objectMessage) {
            if (objectMessage instanceof TCKMessage) {
                TCKMessage message = (TCKMessage) objectMessage;
                UOID uoid = message.getUID();
                int method = message.getMethod();
                if (method == RAMethods.raConfigure) {
                    synchronized (raEntities) {
                        raEntities.add(uoid);
                        if (raEntities.size() == expectedCount)
                            result.setPassed();
                   }
                } else if (method == RAMethods.raUnconfigure) {
                    synchronized (raEntities) {
                        raEntities.remove(uoid);
                    }
                }
                return true;
            }
            return false;
        }

        private FutureResult result;
        private int expectedCount;
        private HashSet raEntities = new HashSet();

    };

    private TCKTestResult doResourceUsageMBeanCheck(ResourceUsageMBeanProxy theResourceUsageMBean, String theEntity) throws InterruptedException {
        String ENTITY1_PARAM_SET_NAME = "testUsageParameter1";
        String BAD_PARAM_SET_NAME = "badParamSet";
        String EXPECTED_RA_OBJ_NAME = "javax.slee.management.usage:type=ResourceUsage,raEntityName=\"TCK_Context_Test_RA_Entity1\"";
        String EXPECTED_USAGE_OBJ_NAME = "javax.slee.usage:type=Usage,notificationSource=javax.slee.management.usage.raentity,raEntityName=\"TCK_Context_Test_RA_Entity1\"";
        String EXPECTED_USAGE_PARAM_OBJ_NAME = "javax.slee.usage:type=Usage,notificationSource=javax.slee.management.usage.raentity," +
                "raEntityName=\"TCK_Context_Test_RA_Entity1\",parameterSetName=\"testUsageParameter1\"";
        String EXPECTED_USAGE_NOTIFY_OBJ_NAME = "javax.slee.usage:type=UsageNotificationManager,notificationSource=javax.slee.management.usage.raentity," +
                "raEntityName=\"TCK_Context_Test_RA_Entity1\"";
        String usageParamSet = null;

        //1114432 Get object name of getResourceUsageMBean
        try {
            ResourceManagementMBeanProxy resourceManagementMBean = utils().getResourceManagementMBeanProxy();
            ObjectName objectName = resourceManagementMBean.getResourceUsageMBean(theEntity);
            getLog().fine("1114432: ResourceUsageMBean = "+objectName);
            if (objectName.equals(new ObjectName(EXPECTED_RA_OBJ_NAME))){
                logSuccessfulCheck(1114432);
            } else {
                return TCKTestResult.failed(1114432, "getResourceUsageMBean() returned: " +objectName);
            }
        }
        catch (Exception e) {
            getLog().warning(e);
            return TCKTestResult.failed(1114432, "getResourceManagementMBean() failed");
        }

        //1114102 - getEntityName
        try {
            String entityName = theResourceUsageMBean.getEntityName();
            getLog().fine("1114102: getEntityName = "+entityName);
            if (entityName.equals(theEntity)) {
                logSuccessfulCheck(1114102);
            } else {
                return TCKTestResult.failed(1114102, "entityName() returned wrong Entity " +entityName);
            }
        }
        catch (Exception e) {
            getLog().warning(e);
            return TCKTestResult.failed(1114102, "getEntityName() failed");
        }

        //1114131 - createUsageParameterSet
        try {
            theResourceUsageMBean.createUsageParameterSet(ENTITY1_PARAM_SET_NAME);
               logSuccessfulCheck(1114131);
        }
        catch (Exception e) {
            getLog().warning(e);
            return TCKTestResult.failed(1114131, "createUsageParameterSet() failed");
        }

        //1114067 - createUsageParameterSet when already exists
        try {
            theResourceUsageMBean.createUsageParameterSet(ENTITY1_PARAM_SET_NAME);
            return TCKTestResult.failed(1114067, "createUsageParameterSet() did not recognise already exists");
        }
        catch (UsageParameterSetNameAlreadyExistsException e) {
               logSuccessfulCheck(1114067);
        }
        catch (Exception e) {
            getLog().warning(e);
            return TCKTestResult.failed(1114067, "createUsageParameterSet() failed");
        }

        //1114097 - getUsageParameterSets()
        try {
            boolean found = false;
            String[] paramSets = theResourceUsageMBean.getUsageParameterSets();
            if (paramSets.length > 0) {
                for (int i=0; i < paramSets.length; i++) {
                    getLog().fine("1114097: getUsageParameterSets = "+paramSets[i].toString());
                    if (paramSets[i].toString().equals(ENTITY1_PARAM_SET_NAME)) {
                        usageParamSet = paramSets[i];
                        logSuccessfulCheck(1114097);
                        found = true;
                    }
                 }
                   if (!found) {
                       return TCKTestResult.failed(1114097, "getUsageParameterSets did not contain RA Entity Parameter Set: " + ENTITY1_PARAM_SET_NAME);
                }
            }
         }
         catch (Exception e) {
            getLog().warning(e);
            return TCKTestResult.failed(1114097, "getUsageParameterSets() failed");
        }

         //1114087 - resetAllUsageParameters()
         try {
             // reset the Usage Parameters
             theResourceUsageMBean.resetAllUsageParameters();

             // RA's do not have a means of getUsageParameterSet() so there is no way of checking that the value has actually been reset
             logSuccessfulCheck(1114087);
         }
         catch (Exception e) {
             getLog().warning(e);
             return TCKTestResult.failed(1114087, "resetAllUsageParameters() failed");
         }

         // 1114066 getUsageMBean
         try {
             ObjectName objectName = theResourceUsageMBean.getUsageMBean();
             getLog().fine("1114066: getUsageMBean = "+objectName);
             if (objectName.equals(new ObjectName(EXPECTED_USAGE_OBJ_NAME))){
                 logSuccessfulCheck(1114066);
             } else {
                 return TCKTestResult.failed(1114066, "getUsageMBean() returned: " +objectName);
             }
         }
         catch (Exception e) {
             getLog().warning(e);
             return TCKTestResult.failed(1114066, "getUsageMBean() failed");
         }

         // 1114431 getUsageMBean
         try {
             ObjectName objectName = theResourceUsageMBean.getUsageMBean(ENTITY1_PARAM_SET_NAME);
             getLog().fine("1114431: getUsageMBean = "+objectName);
             if (objectName.equals(new ObjectName(EXPECTED_USAGE_PARAM_OBJ_NAME))){
                 logSuccessfulCheck(1114431);
             } else {
                 return TCKTestResult.failed(1114431, "getUsageMBean() returned: " +objectName);
             }
        }
         catch (Exception e) {
             getLog().warning(e);
             return TCKTestResult.failed(1114431, "getUsageMBean() failed");
         }

         // 1114062 getUsageMBean Exception
         try {
            javax.management.ObjectName objectName = theResourceUsageMBean.getUsageMBean(BAD_PARAM_SET_NAME);
            return TCKTestResult.failed(1114062, "getUsageMBean() did not recognise parameter set does not exist");
        }
        catch (UnrecognizedUsageParameterSetNameException e) {
               logSuccessfulCheck(1114062);
        }
        catch (Exception e) {
            getLog().warning(e);
            return TCKTestResult.failed(1114062, "getUsageMBean() failed");
        }

        // 1114063 - getUsageNotificationManagerMBean
        try {
            ObjectName objectName = theResourceUsageMBean.getUsageNotificationManagerMBean();
            getLog().fine("1114063: getUsageNotificationManagerMBean = "+objectName);
            if (objectName.equals(new ObjectName(EXPECTED_USAGE_NOTIFY_OBJ_NAME))){
                logSuccessfulCheck(1114063);
            } else {
                return TCKTestResult.failed(1114063, "getUsageNotificationManagerMBean() returned: " +objectName);
            }
        }
        catch (Exception e) {
            getLog().warning(e);
            return TCKTestResult.failed(1114063, "getUsageNotificationManagerMBean() failed");
        }

        // 1114080 - removeUsageParameterSet
        try {
            boolean found = false;
            theResourceUsageMBean.removeUsageParameterSet(ENTITY1_PARAM_SET_NAME);
            String[] paramSets = theResourceUsageMBean.getUsageParameterSets();
            if (paramSets.length == 0) {
                getLog().fine("1114080: No Parameter sets present ");
                logSuccessfulCheck(1114080);
            } else {
                getLog().fine("1114080: Number of Parameter sets = "+paramSets.length);
                for (int i=0; i < paramSets.length; i++) {
                    getLog().fine("1114097: getUsageParameterSets = "+paramSets[i].toString());
                    if (paramSets[i].toString().equals(ENTITY1_PARAM_SET_NAME))
                        found = true;
                }
                if (!found)
                    logSuccessfulCheck(1114080);
                else
                    return TCKTestResult.failed(1114080, "removeUsageParameterSet() did not remove Parameter Set");
            }
        }
        catch (Exception e) {
            getLog().warning(e);
            return TCKTestResult.failed(1114080, "removeUsageParameterSet() failed");
        }

        // 1114064 - close()
        try {
            theResourceUsageMBean.close();
            logSuccessfulCheck(1114064);
        }
        catch (Exception e) {
            getLog().warning(e);
            return TCKTestResult.failed(1114064, "close() raised exception ");
        }

        return TCKTestResult.passed();
    }


    private void logSuccessfulCheck(int assertionID) {
        utils().getLog().info("Check for assertion "+assertionID+" OK");
    }

    private ResourceAdaptorID raID;
    private ResourceManagementMBeanProxy resourceMBean;

}
