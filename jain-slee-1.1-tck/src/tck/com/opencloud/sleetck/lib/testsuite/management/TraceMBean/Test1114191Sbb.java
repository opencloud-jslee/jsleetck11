/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.management.TraceMBean;

import javax.slee.ActivityContextInterface;
import javax.slee.facilities.TraceLevel;
import javax.slee.facilities.Tracer;

import com.opencloud.sleetck.lib.resource.events.TCKResourceEventX;
import com.opencloud.sleetck.lib.sbbutils.BaseTCKSbb;

/*
 * AssertionID (1114191): Create a Tracer
 */
public abstract class Test1114191Sbb extends BaseTCKSbb {

    // Initial event
    public void onTCKResourceEventX1(TCKResourceEventX ev, ActivityContextInterface aci) {
        Tracer tracer = getSbbContext().getTracer("com.foo");
        tracer.trace(TraceLevel.INFO, "Message from Test1114191Sbb");
    }

}

