/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.facilities.activitycontextnamingfacility;

import com.opencloud.sleetck.lib.resource.events.TCKResourceEventX;
import com.opencloud.sleetck.lib.sbbutils.BaseTCKSbb;
import com.opencloud.sleetck.lib.sbbutils.TCKSbbUtils;

import javax.slee.ActivityContextInterface;
import javax.slee.facilities.ActivityContextNamingFacility;
import javax.slee.nullactivity.NullActivity;
import javax.slee.nullactivity.NullActivityContextInterfaceFactory;
import javax.slee.nullactivity.NullActivityFactory;
import java.util.HashMap;

public abstract class Test1364Sbb extends BaseTCKSbb {

    public static final String ACTIVITY_NAME = "Test1364NullActivity";
    public static final String DEFAULT_ACTIVITY_NAME = "Test1364DefaultActivity";

    // Initial event
    public void onTCKResourceEventX1(TCKResourceEventX ev, ActivityContextInterface aci) {

        try {
            HashMap map = new HashMap();
            
            // Create a NullActivity and get its ActivityContextInterface
            NullActivityFactory factory = (NullActivityFactory) TCKSbbUtils.getSbbEnvironment().lookup("slee/nullactivity/factory");
            NullActivity nullActivity = factory.createNullActivity();
            NullActivityContextInterfaceFactory aciFactory = (NullActivityContextInterfaceFactory) TCKSbbUtils.getSbbEnvironment().lookup("slee/nullactivity/activitycontextinterfacefactory");
            ActivityContextInterface nullACI = aciFactory.getActivityContextInterface(nullActivity);

            // Bind it in the ACNF
            ActivityContextNamingFacility facility = (ActivityContextNamingFacility) TCKSbbUtils.getSbbEnvironment().lookup("slee/facilities/activitycontextnaming");
            facility.bind(nullACI, ACTIVITY_NAME);

            // Also bind the default Activity
            facility.bind(aci, DEFAULT_ACTIVITY_NAME);
            nullActivity.endActivity();

            // Unbind the null activity. then try to look it up.
            facility.unbind(ACTIVITY_NAME);

            ActivityContextInterface theACI = facility.lookup(ACTIVITY_NAME);
            if (theACI != null) {
                map.put("Result", new Boolean(false));
                map.put("Message", "Was able to lookup a no-longer-bound ActivityContext from the ActivityContextNamingFacility.");
            } else {
                theACI = facility.lookup(DEFAULT_ACTIVITY_NAME);
                if (theACI == null) {
                    map.put("Result", new Boolean(false));
                    map.put("Message", "Was unable to lookup a bound ActivityContext from the ActivityContextNamingFacility.");
                } else {
                    map.put("Result", new Boolean(true));
                    map.put("Message", "Ok");
                }
            }

            try {
                facility.unbind(DEFAULT_ACTIVITY_NAME);
            } catch (Exception e) {
            }


            TCKSbbUtils.getResourceInterface().sendSbbMessage(map);
        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }

    }

}
