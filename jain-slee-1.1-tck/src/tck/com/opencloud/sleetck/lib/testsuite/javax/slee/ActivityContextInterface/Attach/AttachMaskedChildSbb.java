/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.javax.slee.ActivityContextInterface.Attach;

import java.util.HashMap;

import javax.slee.ActivityContextInterface;

import com.opencloud.sleetck.lib.resource.events.TCKResourceEventX;
import com.opencloud.sleetck.lib.resource.events.TCKResourceEventY;
import com.opencloud.sleetck.lib.sbbutils.BaseTCKSbb;
import com.opencloud.sleetck.lib.sbbutils.TCKSbbUtils;

/**
 * This child SBB should receive TCKResourceEventY1 after it has been attached
 * to the ACI.
 */

public abstract class AttachMaskedChildSbb extends BaseTCKSbb {

    public void onTCKResourceEventY1(TCKResourceEventY event,ActivityContextInterface aci) {
        setMaskedEventTriggered(true);
    }

    public void onTCKResourceEventY2(TCKResourceEventY event,ActivityContextInterface aci) {
        HashMap map = new HashMap();

        if (getMaskedEventTriggered()) {
            map.put("Result", new Boolean(false));
            map.put("Message","Child SBB received onTCKResourceEventY1, which had the mask-on-attach attribute set to True.");
            try {
                TCKSbbUtils.getResourceInterface().sendSbbMessage(map);
            } catch (Exception f) {
                TCKSbbUtils.handleException(f);
            }
        }

        // Unmask all masked events.
        try {
            getSbbContext().maskEvent(new String[0], aci);
            // getSbbContext().maskEvent(null, aci);
        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }
    }

    public void onTCKResourceEventX2(TCKResourceEventX event,ActivityContextInterface aci) {

        HashMap map = new HashMap();

        if (!getMaskedEventTriggered()) {
            map.put("Result", new Boolean(false));
            map.put("Message","TCKResourceEventY1 has been unmasked, but child SBB didn't receive the event.");
        } else {
            map.put("Result", new Boolean(true));
            map.put("Message", "Ok");
        }

        try {
            TCKSbbUtils.getResourceInterface().sendSbbMessage(map);
        } catch (Exception f) {
            TCKSbbUtils.handleException(f);
        }

    }

    public abstract boolean getMaskedEventTriggered();

    public abstract void setMaskedEventTriggered(boolean value);

}
