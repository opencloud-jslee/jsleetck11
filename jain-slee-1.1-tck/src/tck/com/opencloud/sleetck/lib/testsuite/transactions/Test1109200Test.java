/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.transactions;

import com.opencloud.sleetck.lib.AbstractSleeTCKTest;
import com.opencloud.sleetck.lib.TCKTestResult;
import com.opencloud.sleetck.lib.OperationTimedOutException;
import com.opencloud.sleetck.lib.testutils.QueuingResourceListener;
import com.opencloud.sleetck.lib.testutils.Assert;
import com.opencloud.sleetck.lib.resource.TCKActivityID;
import com.opencloud.sleetck.lib.resource.events.TCKResourceEventX;


public class Test1109200Test extends AbstractSleeTCKTest {

    private static final String SERVICE1_DU_PATH = "serviceDUPath1";
    private static final String SERVICE2_DU_PATH = "serviceDUPath2";

    public TCKTestResult run() throws Exception {
        QueuingResourceListener listener = new QueuingResourceListener(utils());
        setResourceListener(listener);

        TCKActivityID activity = utils().getResourceInterface().createActivity("Test1109200Activity");

        //test sbbCreate() on Test1109200Sbb1
        utils().getResourceInterface().fireEvent(TCKResourceEventX.X1, null, activity, null);
        try {
            String message = (String) listener.nextMessage().getMessage();
            getLog().info("Got unexpected result from SBB: " + message);
            Assert.fail(1109200, "Got event from SBB that should not exist because of Exception thrown in sbbCreate()");
        } catch (OperationTimedOutException e) {
            getLog().info("Got expected timeout exception waiting for result from Sbb1.");
        }

        //test sbbPostCreate() on Test1109200Sbb2
        utils().getResourceInterface().fireEvent(TCKResourceEventX.X2, null, activity, null);
        try {
            String message = (String) listener.nextMessage().getMessage();
            getLog().info("Got unexpected result from SBB: " + message);
            Assert.fail(1109200, "Got event from SBB that should not exist because of Exception thrown in sbbPostCreate()");
        } catch (OperationTimedOutException e) {
            getLog().info("Got expected timeout exception waiting for result from Sbb2.");
        }

        return TCKTestResult.passed();

    }

    public void setUp() throws Exception {
        setupService(SERVICE1_DU_PATH);

        setupService(SERVICE2_DU_PATH);
    }

}
