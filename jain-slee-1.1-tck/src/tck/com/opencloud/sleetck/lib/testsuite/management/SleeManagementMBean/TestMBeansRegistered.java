/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.management.SleeManagementMBean;

import com.opencloud.sleetck.lib.AbstractSleeTCKTest;
import com.opencloud.sleetck.lib.SleeTCKTestUtils;
import com.opencloud.sleetck.lib.SleeTCKTest;
import com.opencloud.sleetck.lib.TCKTestResult;
import com.opencloud.sleetck.lib.TCKTestFailureException;
import com.opencloud.sleetck.lib.TCKTestErrorException;
import com.opencloud.sleetck.lib.testutils.ExceptionsUtil;
import com.opencloud.sleetck.lib.testutils.jmx.SleeManagementMBeanProxy;
import javax.slee.management.SleeState;
import javax.management.ObjectName;

/**
 * Tests assertion 1505, that the SleeManagementMBean ensures that each object name
 * returned by an MBean accessor method is registered in the MBeanServer.
 */
public class TestMBeansRegistered implements SleeTCKTest {

    public void init(SleeTCKTestUtils utils) { this.utils=utils; }
    public void setUp() {}
    public void tearDown() {}

    public TCKTestResult run() throws Exception {
        SleeManagementMBeanProxy management = utils.getSleeManagementMBeanProxy();
        checkIsRegistered(management.getDeploymentMBean(),"getDeploymentMBean");
        checkIsRegistered(management.getServiceManagementMBean(),"getServiceManagementMBean");
        checkIsRegistered(management.getProfileProvisioningMBean(),"getProfileProvisioningMBean");
        checkIsRegistered(management.getTraceMBean(),"getTraceMBean");
        checkIsRegistered(management.getAlarmMBean(),"getAlarmMBean");
        return TCKTestResult.passed();
    }

    private void checkIsRegistered(ObjectName objName, String methodName) throws TCKTestFailureException, TCKTestErrorException {
        utils.getLog().info("Checking object name "+objName+" returned by "+methodName);
        if(!utils.getMBeanFacade().isRegistered(objName)) throw new TCKTestFailureException(1505,
            "The object name returned by "+methodName+" was not registered in the MBeanServer. objName="+objName);
        utils.getLog().info("Object name was registered");
    }

    private SleeTCKTestUtils utils;

}