/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.usage.sbb;

import javax.slee.ActivityContextInterface;
import javax.slee.usage.UnrecognizedUsageParameterSetNameException;

import com.opencloud.sleetck.lib.resource.events.TCKResourceEventX;
import com.opencloud.sleetck.lib.sbbutils.BaseTCKSbb;
import com.opencloud.sleetck.lib.sbbutils.TCKSbbUtils;
import com.opencloud.sleetck.lib.testsuite.usage.common.GenericUsageSbbInstructions;

/**
 * An SBB providing common functionality for the usage tests.
 *
 * The SBB receives instructions via a TCKResourceEventX1 event,
 * then sends a message indicating completion back to the TCK test.
 *
 * The same functionality is available via the doUpdates() method
 * via the GenericUsageSbbLocal interface.
 *
 * Note that the reply message only indicates that the usage parameter
 * update calls have been made -- it does not indicate they have been
 * applied. A test should wait for the usage notifications before checking
 * usage parameter values.
 *
 * The payload of the message sent back to the test is the instructions object received.
 */

public abstract class GenericUsageSbb extends BaseTCKSbb {

    public void onTCKResourceEventX1(TCKResourceEventX event, ActivityContextInterface aci) {
        try {
            GenericUsageSbbInstructions instructions = GenericUsageSbbInstructions.fromExported(event.getMessage());
            doUpdates(instructions);
        } catch(Exception e) {
            TCKSbbUtils.handleException(e);
        }
    }

    public void doUpdates(GenericUsageSbbInstructions instructions) {
        try {
            String parameterSetName = instructions.getParameterSetName();

            getSbbContext().getTracer("GenericUsageSbb").fine("doUpdates(): received instructions: "+instructions);

            // access the usage parameter interface
            GenericUsageParameterInterface usageParameterInterface =
                parameterSetName == null ? getDefaultSbbUsageParameterSet() :
                    getSbbUsageParameterSet(parameterSetName);

                // alter the usage parameters as per the received instructions
                if(instructions.getFirstCountIncrements() != null) {
                    long[] values = instructions.getFirstCountIncrements();
                    for (int i = 0; i < values.length; i++) {
                        getSbbContext().getTracer("GenericUsageSbb").fine("doUpdates(): incrementing FirstCount by "+values[i]);
                        usageParameterInterface.incrementFirstCount(values[i]);
                    }
                }
                if(instructions.getSecondCountIncrements() != null) {
                    long[] values = instructions.getSecondCountIncrements();
                    for (int i = 0; i < values.length; i++) {
                        getSbbContext().getTracer("GenericUsageSbb").fine("doUpdates(): incrementing SecondCount by "+values[i]);
                        usageParameterInterface.incrementSecondCount(values[i]);
                    }
                }
                if(instructions.getTimeBetweenNewConnectionsSamples() != null) {
                    long[] values = instructions.getTimeBetweenNewConnectionsSamples();
                    for (int i = 0; i < values.length; i++) {
                        getSbbContext().getTracer("GenericUsageSbb").fine("doUpdates(): adding TimeBetweenNewConnections sample of "+values[i]);
                        usageParameterInterface.sampleTimeBetweenNewConnections(values[i]);
                    }
                }
                if(instructions.getTimeBetweenErrorsSamples() != null) {
                    long[] values = instructions.getTimeBetweenErrorsSamples();
                    for (int i = 0; i < values.length; i++) {
                        getSbbContext().getTracer("GenericUsageSbb").fine("doUpdates(): adding TimeBetweenErrors sample of "+values[i]);
                        usageParameterInterface.sampleTimeBetweenErrors(values[i]);
                    }
                }
                // send a message back to the test to confirm the updates, along with the initial event payload
                getSbbContext().getTracer("GenericUsageSbb").fine("doUpdates(): replying to test");
                TCKSbbUtils.getResourceInterface().sendSbbMessage(instructions.toExported());
        } catch(Exception e) {
            TCKSbbUtils.handleException(e);
        }
    }

    public abstract GenericUsageParameterInterface getDefaultSbbUsageParameterSet();
    public abstract GenericUsageParameterInterface getSbbUsageParameterSet(String name) throws UnrecognizedUsageParameterSetNameException;

}
