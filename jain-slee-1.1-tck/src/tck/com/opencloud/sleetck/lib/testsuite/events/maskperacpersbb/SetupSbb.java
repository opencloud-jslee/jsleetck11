/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.events.maskperacpersbb;

import com.opencloud.sleetck.lib.sbbutils.BaseTCKSbb;
import com.opencloud.sleetck.lib.sbbutils.TCKSbbUtils;
import com.opencloud.sleetck.lib.sbbutils.events.TCKSbbEventImpl;
import com.opencloud.sleetck.lib.sbbutils.events.TCKSbbEvent;
import com.opencloud.sleetck.lib.resource.events.TCKResourceEventX;
import com.opencloud.sleetck.lib.resource.sbbapi.TCKActivity;
import javax.slee.facilities.*;
import javax.slee.*;
import javax.slee.nullactivity.*;
import javax.naming.InitialContext;

public abstract class SetupSbb extends BaseTCKSbb {

    public void setSbbContext( SbbContext ctx ){
        super.setSbbContext(ctx);
        try{
        InitialContext ic = new InitialContext();
        nullFactory = (NullActivityFactory)ic.lookup("java:comp/env/slee/nullactivity/factory");
        nullAcFactory = (NullActivityContextInterfaceFactory)ic.lookup("java:comp/env/slee/nullactivity/activitycontextinterfacefactory");
        acnf = (ActivityContextNamingFacility)ic.lookup("java:comp/env/slee/facilities/activitycontextnaming");
        }catch( javax.naming.NamingException ne ){
            throw new RuntimeException(ne.getMessage());
        }
    }


    public void onTCKResourceEventX1(TCKResourceEventX event, ActivityContextInterface aci) {

          try{


              NullActivity nullActivity = nullFactory.createNullActivity();
              ActivityContextInterface nullAci = nullAcFactory.getActivityContextInterface( nullActivity );
              SbbLocalObject child = getChildRelation().create();
              nullAci.attach( child );
              aci.attach( child );

              // only have 1 of these active for the service, so a hardcoded name is ok
              acnf.bind(nullAci,NAME_BINDING);

              ((Controllee)child).initialiseMaskFor( nullAci );
              ((Controllee)child).installController( (Controller)getSbbContext().getSbbLocalObject() );
              fireFooEvent( new TCKSbbEventImpl(new Integer(6)), nullAci, null );
              fireBarEvent( new TCKSbbEventImpl(new Integer(7)), nullAci, null );

          } catch( NameAlreadyBoundException nabe ){
              throw new RuntimeException( nabe.getMessage() );
          }catch( javax.slee.CreateException ce ){
              throw new RuntimeException( ce.getMessage() );
          }catch( UnrecognizedActivityException uae ){
              throw new RuntimeException("the activity is not recognized");
          }catch( UnrecognizedEventException uee ){
              throw new RuntimeException("the event could not be masked because it is not recognized");
          }catch( NotAttachedException nae ){
              throw new RuntimeException("the event could not be masked because the SBB is not attached");
          }

    }

    public void onActivityEndEvent( ActivityEndEvent event, ActivityContextInterface aci ){

        if( aci.getActivity() instanceof TCKActivity ){
            ActivityContextInterface nullAci = acnf.lookup(NAME_BINDING);
            NullActivity nullActivity = (NullActivity) nullAci.getActivity();
            nullActivity.endActivity();
        }

    }

    public void eventReceivedBy( Controllee sbb ){
        if( sbb.hasReceivedFooEvent() ){
            sendTestFail("child received the Foo event when it should not have");
            return;
        }
        if( !sbb.hasReceivedBarEvent() ){
            sendTestFail("child did not receive the Bar event when it should have");
            return;
        }

        sendTestSuccess();
    }

    private void sendTestSuccess(){
        sendMessage( "SUCCESS","" );
    }

    private void sendTestFail(String reason){
        sendMessage("FAILURE", reason);
    }

    private void sendMessage(String arg1, String arg2) {
        try {
            TCKSbbUtils.createTrace(getSbbID(),Level.FINE,"sending a message back with values: " + arg1 +", and " +arg2,null);
            // send a reply: the value of the boolean message
            String[] message = new String[2];
            message[0] = arg1; message[1] = arg2;
            TCKSbbUtils.getResourceInterface().sendSbbMessage(message);
        } catch(Exception e) {
            TCKSbbUtils.handleException(e);
        }
    }

    public abstract void fireFooEvent( TCKSbbEvent event, ActivityContextInterface aci, Address addr );

    public abstract void fireBarEvent( TCKSbbEvent event, ActivityContextInterface aci, Address addr );

    public abstract ChildRelation getChildRelation();

    private NullActivityFactory nullFactory;
    private NullActivityContextInterfaceFactory nullAcFactory;
    private ActivityContextNamingFacility acnf;

    private static final String NAME_BINDING = "events.maskperacpersbb";
}
