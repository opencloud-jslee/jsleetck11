/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.usage.common;

/**
 * Encapsulates the data sent via the event message to the event handler
 * methods of the GenericUsageSbb.
 */
public class GenericUsageSbbInstructions {

    // -- Contructors -- //

    /**
     * @param parameterSetName The parameter set name to alter. If null, indicates the
     *  un-named parameter set.
     */
    public GenericUsageSbbInstructions(String parameterSetName) {
        this(parameterSetName,null,null,null,null);
    }

    /**
     * @param parameterSetName The parameter set name to alter. If null, indicates the
     *  un-named parameter set.
     */
    public GenericUsageSbbInstructions(String parameterSetName,
                                       long[] firstCountIncrements, long[] secondCountIncrements,
                                       long[] timeBetweenNewConnectionsSamples, long[] timeBetweenErrorsSamples) {
        this.parameterSetName = parameterSetName;
        this.firstCountIncrements = firstCountIncrements;
        this.secondCountIncrements = secondCountIncrements;
        this.timeBetweenNewConnectionsSamples = timeBetweenNewConnectionsSamples;
        this.timeBetweenErrorsSamples = timeBetweenErrorsSamples;
    }

    // -- Mutators -- //

    public synchronized void addFirstCountIncrement(long value) {
        firstCountIncrements = addToSet(firstCountIncrements,value);
    }

    public synchronized void addSecondCountIncrement(long value) {
        secondCountIncrements = addToSet(secondCountIncrements,value);
    }

    public synchronized void addTimeBetweenNewConnectionsSamples(long value) {
        timeBetweenNewConnectionsSamples = addToSet(timeBetweenNewConnectionsSamples,value);
    }

    public synchronized void addTimeBetweenErrorsSample(long value) {
        timeBetweenErrorsSamples = addToSet(timeBetweenErrorsSamples,value);
    }

    // -- Accessors -- //

    public String getParameterSetName() {
        return parameterSetName;
    }

    public long[] getFirstCountIncrements() {
        return firstCountIncrements;
    }

    public long[] getSecondCountIncrements() {
        return secondCountIncrements;
    }

    public long[] getTimeBetweenNewConnectionsSamples() {
        return timeBetweenNewConnectionsSamples;
    }

    public long[] getTimeBetweenErrorsSamples() {
        return timeBetweenErrorsSamples;
    }

    /**
     * @return the total number of updates requested
     */
    public int getTotalUpdates() {
        int total = 0;
        if(firstCountIncrements != null) total += firstCountIncrements.length;
        if(secondCountIncrements != null) total += secondCountIncrements.length;
        if(timeBetweenNewConnectionsSamples != null) total += timeBetweenNewConnectionsSamples.length;
        if(timeBetweenErrorsSamples != null) total += timeBetweenErrorsSamples.length;
        return total;
    }

    // -- Export/Import -- //

    /**
     * Exports the Object into an Object or array of primitive types,
     * J2SE types or SLEE types.
     */
    public synchronized Object toExported() {
        long[][] arrays = new long[][] {firstCountIncrements,secondCountIncrements,
            timeBetweenNewConnectionsSamples,timeBetweenErrorsSamples};
        return new Object[]{parameterSetName,arrays};
    }

    /**
     * Returns a GenericUsageSbbInstructions Object from the
     * given object in exported form
     */
    public static GenericUsageSbbInstructions fromExported(Object exported) {
        Object[] parameterSetNameAndArrays = (Object[])exported;
        String parameterSetName = (String)parameterSetNameAndArrays[0];
        long[][] arrays = (long[][])parameterSetNameAndArrays[1];
        return new GenericUsageSbbInstructions(parameterSetName,arrays[0],arrays[1],arrays[2],arrays[3]);
    }

    // -- Private methods -- //

    private long[] addToSet(long[] set, long value) {
        if(set == null) {
            return new long[]{value};
        } else {
            long[] rSet = new long[set.length + 1];
            System.arraycopy(set,0,rSet,0,set.length);
            rSet[rSet.length - 1] = value;
            return rSet;
        }
    }

    // -- Private state -- //

    private String parameterSetName;
    private long[] firstCountIncrements;
    private long[] secondCountIncrements;
    private long[] timeBetweenNewConnectionsSamples;
    private long[] timeBetweenErrorsSamples;

}
