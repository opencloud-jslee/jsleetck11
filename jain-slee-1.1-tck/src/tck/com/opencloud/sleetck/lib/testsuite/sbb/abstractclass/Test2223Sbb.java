/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.sbb.abstractclass;

import com.opencloud.sleetck.lib.resource.events.TCKResourceEventX;
import com.opencloud.sleetck.lib.sbbutils.BaseTCKSbb;
import com.opencloud.sleetck.lib.sbbutils.TCKSbbUtils;

import javax.slee.ActivityContextInterface;
import javax.slee.RolledBackContext;
import javax.slee.facilities.Level;

public abstract class Test2223Sbb extends BaseTCKSbb {

    // Initial event
    public void onTCKResourceEventX1(TCKResourceEventX ev, ActivityContextInterface aci) {
        try {
            getDefaultSbbUsageParameterSet().incrementFoo(10);
            getDefaultSbbUsageParameterSet().sampleBar(20);
            getSbbContext().setRollbackOnly();
        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }
    }

    public void sbbRolledBack(RolledBackContext context) {
        createTraceSafe(Level.INFO,"Test2223Sbb:sbbRolledBack() called as expected");
    }

    public abstract Test2223SbbUsage getDefaultSbbUsageParameterSet();
    // @@     public abstract Test2223SbbUsage getSbbUsageParameterSet(String name) throws javax.slee.usage.UnrecognizedSbbParameterSetNameException;

}
