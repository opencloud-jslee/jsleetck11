/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.profiles.lifecycle;

import javax.management.Attribute;
import javax.management.ObjectName;
import javax.slee.profile.ProfileSpecificationID;

import com.opencloud.sleetck.lib.AbstractSleeTCKTest;
import com.opencloud.sleetck.lib.TCKTestFailureException;
import com.opencloud.sleetck.lib.TCKTestResult;
import com.opencloud.sleetck.lib.infra.SleeTCKComponentConstants;
import com.opencloud.sleetck.lib.profileutils.BaseMessageAdapter;
import com.opencloud.sleetck.lib.rautils.RMIObjectChannel;
import com.opencloud.sleetck.lib.resource.TCKActivityID;
import com.opencloud.sleetck.lib.resource.events.TCKResourceEventX;
import com.opencloud.sleetck.lib.resource.testapi.TCKResourceTestInterface;
import com.opencloud.sleetck.lib.testutils.FutureAssertionsResult;
import com.opencloud.sleetck.lib.testutils.ProfileUtils;
import com.opencloud.sleetck.lib.testutils.jmx.ProfileMBeanProxy;
import com.opencloud.sleetck.lib.testutils.jmx.ProfileProvisioningMBeanProxy;


public class Test1110227Test extends AbstractSleeTCKTest {

    private static final String DU_PATH_PARAM= "DUPath";

    private static final String SPEC_NAME = "Test1110227Profile";
    private static final String SPEC_VERSION = "1.0";


    /**
     * Test for profile objects' lifecycle methods and behaviour.
     */
    public TCKTestResult run() throws Exception {


            TCKTestResult result;
            exResult = null;

            futureResult = new FutureAssertionsResult(new int[]{
                    Test1110227Profile.setProfileContextCalledAfterConstructor,
                    Test1110227Profile.setProfileContextNoCMPAccess,
                    Test1110227Profile.profileInitializeCMPDefaultValues,
                    Test1110227Profile.profileInitCalledInPooledState,
                    Test1110227Profile.profileInitCalledForDefaultProfile}, getLog());
            //1110234, 1110236: create a profile table
            ProfileSpecificationID specID = new ProfileSpecificationID(SPEC_NAME, SleeTCKComponentConstants.TCK_VENDOR, SPEC_VERSION);
            profileProvisioning.createProfileTable(specID, Test1110227Sbb.PROFILE_TABLE_NAME);
            getLog().fine("Added profile table "+Test1110227Sbb.PROFILE_TABLE_NAME+" based on profile spec "+SPEC_NAME);

            //check that profilecontext has been set and profileInitialize has been called for the default profile
            result = futureResult.waitForResult(utils().getTestTimeout());
            if (!result.isPassed())
                return result;



            futureResult = new FutureAssertionsResult(new int[]{
                    Test1110227Profile.setProfileContextCalledAfterConstructor,
                    Test1110227Profile.setProfileContextNoCMPAccess,
                    Test1110227Profile.profileVerifyCalled,
                    Test1110227Profile.profileVerifyInReadyState,
                    Test1110227Profile.profileVerifyAfterStore}, getLog());
            //1110276, 1110278, 1110280: add a profile to the table
            ObjectName profile = profileProvisioning.createProfile(Test1110227Sbb.PROFILE_TABLE_NAME, Test1110227Sbb.PROFILE_NAME);
            ProfileMBeanProxy profileProxy = utils().getMBeanProxyFactory().createProfileMBeanProxy(profile);
            getLog().fine("Created profile "+Test1110227Sbb.PROFILE_NAME+" for profile table "+Test1110227Sbb.PROFILE_TABLE_NAME);
            //commit and close the Profile
            profileProxy.commitProfile();
            //check that profileVerify has been called, method returned normally
            result = futureResult.waitForResult(utils().getTestTimeout());
            if (!result.isPassed())
                return result;



            //1110280: start a new transaction for profile modification
            profileProxy.editProfile();
            //set CMP field to special value so that profileVerify can pick it up and causes TXN to rollback
            utils().getMBeanFacade().setAttribute(profile, new Attribute("StringValue", Test1110227Sbb.ROLLBACK));
            try {
                profileProxy.commitProfile();
                return TCKTestResult.failed(Test1110227Profile.profileVerifyTXNContext, "commitProfile() should have thrown an exception as TXN was marked " +
                        "for rollback.");
            }
            catch (Exception e) {
                getLog().fine("Caught exception as expected: "+e);
            }
            //Check that CMP value has been changed to initial value
            String value = (String)utils().getMBeanFacade().getAttribute(profile, "StringValue");
            if (!value.equals(Test1110227Sbb.INIT))
                return TCKTestResult.failed(Test1110227Profile.profileVerifyTXNContext, "commitProfile() should have thrown an exception as TXN was marked " +
                "for rollback.");
            else
                getLog().fine("CMP changes have been reverted as expected.");

            profileProxy.closeProfile();



            //1110281: profileVerify is not called due to modifications done via SLEE components.
            //get an Sbb to run a business method, do some CMP changes and create another profile.
            //An exception would be handed back from the Sbb in case profileVerify
            //has been invoked during the operations performed in the event handler
            getLog().fine("Try to get an Sbb to cause profileVerify being called.");
            // send an initial event to the test
            TCKResourceTestInterface resource = utils().getResourceInterface();
            TCKActivityID activityID = resource.createActivity(getClass().getName());

            futureResult = new FutureAssertionsResult(new int[]{
                    Test1110227Profile.profileVerifyOnlyCalledForManagement
                    }, getLog());
            resource.fireEvent(TCKResourceEventX.X1, null, activityID, null);
            result = futureResult.waitForResult(utils().getTestTimeout());
            if (!result.isPassed())
                return result;



            ObjectName profile3 = profileProvisioning.createProfile(Test1110227Sbb.PROFILE_TABLE_NAME, Test1110227Sbb.PROFILE_NAME3);
            ProfileMBeanProxy profileProxy3 = utils().getMBeanProxyFactory().createProfileMBeanProxy(profile3);
            getLog().fine("Created profile "+Test1110227Sbb.PROFILE_NAME3+" for profile table "+Test1110227Sbb.PROFILE_TABLE_NAME);
            //commit and close the Profile
            profileProxy3.commitProfile();
            futureResult = new FutureAssertionsResult(new int[]{
                    Test1110227Profile.profileRemoveCalledBeforeRemoveCMP,
                    Test1110227Profile.profileRemoveStillInReadyState
                    },getLog());
            //1110263, 1110261: Remove the profile and check that appropriate responses have been sent by the profile object
            getLog().fine("Remove profile "+Test1110227Sbb.PROFILE_NAME3+" and check that appropriate responses have been sent.");
            profileProvisioning.removeProfile(Test1110227Sbb.PROFILE_TABLE_NAME, Test1110227Sbb.PROFILE_NAME3);
            result = futureResult.waitForResult(utils().getTestTimeout());
            if (!result.isPassed())
                return result;



            //1110282: profileVerify is not invoked due to commit of changes on the profile table's
            //default profile
            getLog().fine("Obtain the default profile.");
            ObjectName defaultProfile = profileProvisioning.getDefaultProfile(Test1110227Sbb.PROFILE_TABLE_NAME);
            ProfileMBeanProxy defaultProfileProxy = utils().getMBeanProxyFactory().createProfileMBeanProxy(defaultProfile);



            //1110290: check that CMP values for default profile have been set correctly during profileInitialize
            getLog().fine("Check CMP values for default profile.");
            if ( !Test1110227Sbb.INIT.equals(utils().getMBeanFacade().getAttribute(defaultProfile, "StringValue")))
                return TCKTestResult.failed(Test1110227Profile.profileInitCalledForDefaultProfile, "CMP values for default profiles not set correctly during profileInitialize().");



            getLog().fine("Start TXN for modifying the default profile.");
            defaultProfileProxy.editProfile();
            utils().getMBeanFacade().setAttribute(defaultProfile, new Attribute("StringValue", Test1110227Sbb.ROLLBACK));
            try {
                getLog().fine("Commit changes to default profile");
                defaultProfileProxy.commitProfile();
                getLog().fine("Commit was successful for default profile as expected.");
            }
            catch (Exception e) {
                return TCKTestResult.failed(Test1110227Profile.profileVerifyNotOnDefaultProfile, "commitProfile() should have succeeded as profileVerify is not called for changes to the Default Profile.");
            }



            //finally check whether any failures or errors arrived while no FutureResult was waiting to be set.
            if (exResult!=null)
                return exResult;
            return TCKTestResult.passed();
    }

    public void setPassed(int assertionID, String msg) {
        getLog().fine(assertionID+": "+msg);
        if (futureResult!=null && !futureResult.isSet())
            futureResult.setPassed(assertionID);
    }

    public void setFailed(int assertionID, String msg, Exception e) {
        if (e==null) {
            getLog().fine("FAILURE: "+assertionID+":"+msg);
            if (futureResult!=null && !futureResult.isSet())
                futureResult.setFailed(assertionID, msg);

            //in case the currently active futureResult has already been set to passed() before the failure
            //comes in, we have to provide a mechanism to still detect this failure
            setIfEmpty(TCKTestResult.failed(assertionID, msg));
        }
        else {
            getLog().fine("FAILURE: "+assertionID+":"+msg);
            getLog().fine(e);
            if (futureResult!=null && !futureResult.isSet())
                futureResult.setFailed(new TCKTestFailureException(assertionID, msg, e));

            //in case the currently active futureResult has already been set to passed() before the failure
            //comes in, we have to provide a mechanism to still detect this failure
            setIfEmpty(TCKTestResult.failed(new TCKTestFailureException(assertionID, msg, e)));
        }
    }

    public void setError(String msg, Exception e) {
        if (e==null) {
            getLog().warning(msg);
            if (futureResult!=null && !futureResult.isSet())
                futureResult.setError(msg);

            //in case the currently active futureResult has already been set to passed() before the error
            //comes in, we have to provide a mechanism to still detect this error
            setIfEmpty(TCKTestResult.error(msg));
        }
        else {
            getLog().warning(msg);
            getLog().warning(e);
            if (futureResult!=null && !futureResult.isSet())
                futureResult.setError(msg, e);

            //in case the currently active futureResult has already been set to passed() before the error
            //comes in, we have to provide a mechanism to still detect this failure
            setIfEmpty(TCKTestResult.error(msg, e));
        }
    }

    public void setUp() throws Exception {

        setupService(DU_PATH_PARAM);

        in = utils().getRMIObjectChannel();
        in.setMessageHandler(new BaseMessageAdapter(getLog()) {
            public void onSetPassed(int assertionID, String msg) { setPassed(assertionID, msg); }
            public void onSetFailed(int assertionID, String msg, Exception e) { setFailed(assertionID, msg, e); }
            public void onLogCall(String msg) { getLog().fine(msg); }
            public void onSetError(String msg, Exception e) { setError(msg, e); }

        });

        profileUtils = new ProfileUtils(utils());
        profileProvisioning = profileUtils.getProfileProvisioningProxy();
    }

    public void tearDown() throws Exception {

        in.clearQueue();

        try {
            profileUtils.removeProfileTable(Test1110227Sbb.PROFILE_TABLE_NAME);
        }
        catch (Exception e) {
            getLog().warning("Caught exception while trying to remove profile table:");
            getLog().warning(e);
        }

        super.tearDown();
    }

    private void setIfEmpty(TCKTestResult exResult) {
        if (this.exResult==null)
            this.exResult = exResult;
    }

    private ProfileUtils profileUtils;
    private ProfileProvisioningMBeanProxy profileProvisioning;

    private RMIObjectChannel in;

    private FutureAssertionsResult futureResult;
    private TCKTestResult exResult;
}
