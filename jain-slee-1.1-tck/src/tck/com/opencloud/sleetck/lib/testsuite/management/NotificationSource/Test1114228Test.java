/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.management.NotificationSource;

import java.rmi.RemoteException;
import java.util.Map;

import javax.management.Notification;
import javax.management.NotificationListener;
import javax.management.ObjectName;
import javax.slee.ComponentID;
import javax.slee.SbbID;
import javax.slee.ServiceID;
import javax.slee.facilities.TraceLevel;
import javax.slee.management.DeployableUnitDescriptor;
import javax.slee.management.DeployableUnitID;
import javax.slee.management.NotificationSource;
import javax.slee.management.SbbNotification;
import javax.slee.management.TraceNotification;

import com.opencloud.sleetck.lib.AbstractSleeTCKTest;
import com.opencloud.sleetck.lib.DescriptionKeys;
import com.opencloud.sleetck.lib.TCKTestErrorException;
import com.opencloud.sleetck.lib.TCKTestFailureException;
import com.opencloud.sleetck.lib.TCKTestResult;
import com.opencloud.sleetck.lib.resource.TCKActivityID;
import com.opencloud.sleetck.lib.resource.TCKSbbMessage;
import com.opencloud.sleetck.lib.resource.events.TCKResourceEventX;
import com.opencloud.sleetck.lib.resource.testapi.TCKResourceListener;
import com.opencloud.sleetck.lib.resource.testapi.TCKResourceTestInterface;
import com.opencloud.sleetck.lib.testutils.Assert;
import com.opencloud.sleetck.lib.testutils.BaseTCKResourceListener;
import com.opencloud.sleetck.lib.testutils.FutureResult;
import com.opencloud.sleetck.lib.testutils.jmx.DeploymentMBeanProxy;
import com.opencloud.sleetck.lib.testutils.jmx.SleeManagementMBeanProxy;
import com.opencloud.sleetck.lib.testutils.jmx.TraceMBeanProxy;
import com.opencloud.sleetck.lib.testutils.jmx.impl.TraceMBeanProxyImpl;

/*
 * AssertionID (114228): The SbbNotification class identifies an SBB
 * as the source of a notification. The Trace Facility will accept
 * the trace message and propagate the trace message by emitting a
 * TraceNotification object from a TraceMBean object.
 *
 */
public class Test1114228Test extends  AbstractSleeTCKTest {

    /**
     * Perform the actual test.
     */
    public void run(FutureResult result) throws Exception {
        this.result = result;
        expectedTraceNotifications = 1;
        doTest11142228Test(TraceLevel.INFO);

        synchronized (this) {
            wait(utils().getTestTimeout());
        }

        if ((passed) && (expectedTraceNotifications == receivedTraceNotifications))
            result.setPassed();
        else
            result.setFailed(1114228, "Expected number of trace messages not received, traces were not delivered by " +
                    "the TraceFacility (expected " + expectedTraceNotifications + ", received " + receivedTraceNotifications + ")");

    }

    public void doTest11142228Test(TraceLevel traceLevel) throws Exception {

        TraceMBeanProxy traceMBeanProxy = utils().getTraceMBeanProxy();

        if(sbbID == null) throw new TCKTestErrorException("sbbID not found for "+DescriptionKeys.SERVICE_DU_PATH_PARAM);
        if(serviceID == null) throw new TCKTestErrorException("serviceID not found for "+DescriptionKeys.SERVICE_DU_PATH_PARAM);

        sbbNotification = new SbbNotification(serviceID, sbbID);
        try {
            getLog().fine("Starting to test: Set trace level of the tracer ");
            traceMBeanProxy.setTraceLevel(sbbNotification, "com.foo", traceLevel);
        }
        catch (Exception e) {
            getLog().warning(e);
            TCKTestResult.error("ERROR!", e);
        }

        String activityName = "Test1114228Test";
        TCKResourceTestInterface resource = utils().getResourceInterface();
        TCKActivityID activityID = resource.createActivity(activityName);
        getLog().info("Firing event: " + testName );

        // kickoff the test
        resource.fireEvent(TCKResourceEventX.X1, testName, activityID, null);
    }


    /**
     * Do all the pre-run configuration of the test.
     */

    public void setUp() throws Exception {

        getLog().fine("Connecting to resource");
        getLog().fine("Installing and activating service");

        resourceListener = new TCKResourceListenerImpl();
        setResourceListener(resourceListener);

        // Install the Deployable Units
        String duPath = utils().getTestParams().getProperty(DescriptionKeys.SERVICE_DU_PATH_PARAM);
        duID = utils().install(duPath);

        // Activate the DU
        utils().activateServices(duID, true);

        // Setup TraceNotificationListener
        SleeManagementMBeanProxy proxy = utils().getSleeManagementMBeanProxy();
        traceMBeanName = proxy.getTraceMBean();
        listener = new TraceNotificationListenerImpl();

        tracembean = new TraceMBeanProxyImpl(traceMBeanName, utils().getMBeanFacade());
        tracembean.addNotificationListener(listener, null, null);

        DeploymentMBeanProxy duProxy = utils().getDeploymentMBeanProxy();
        DeployableUnitDescriptor duDesc = duProxy.getDescriptor(duID);
        ComponentID components[] = duDesc.getComponents();
        // Get the SbbID from the components.
        for (int i = 0; i < components.length; i++) {
            if (components[i] instanceof ServiceID) {
                getLog().fine("Setting serviceID value.");
                serviceID = (ServiceID) components[i];
                continue;
            }

            if (components[i] instanceof SbbID) {
                getLog().fine("Setting sbbID value.");
                sbbID = (SbbID) components[i];
                continue;
            }
        }

    }

    /**
     * Clean up after the test.
     */

    public void tearDown() throws Exception {
        if (null != tracembean)
            tracembean.removeNotificationListener(listener);
        // cleanup
        utils().getLog().fine("Disconnecting from resource");
        utils().getResourceInterface().clearActivities();
        utils().getLog().fine("Deactivating and uninstalling service");
        utils().deactivateAllServices();
        utils().uninstallAll();

    }


    public String getTestName() {
        String testName = "testName";
        return utils().getTestParams().getProperty(testName);
    }

    private class TCKResourceListenerImpl extends BaseTCKResourceListener {
        // TCKResourceListener methods

        public synchronized void onSbbMessage(TCKSbbMessage message, TCKActivityID calledActivity) throws RemoteException {

            Map sbbData = (Map)message.getMessage();
            String sbbTestName = "Test1114228Test";
            String sbbTestResult = (String)sbbData.get("result");
            String sbbTestMessage = (String)sbbData.get("message");
            int assertionID = ((Integer)sbbData.get("id")).intValue();
            getLog().info("Received message from SBB: testname="+ sbbTestName+", result="+sbbTestResult+", message="+sbbTestMessage+", id="+assertionID);
            try {
                Assert.assertEquals(assertionID,"Test " + sbbTestName + " failed.", "pass", sbbTestResult);
                result.setPassed();
            } catch (TCKTestFailureException ex) {
                result.setFailed(ex);
            }
        }

        public void onException(Exception exception) throws RemoteException {
            getLog().warning("Received Exception from SBB or resource:");
            getLog().warning(exception);
            result.setError(exception);
        }
    }


    public class TraceNotificationListenerImpl implements NotificationListener {
        public final void handleNotification(Notification notification, Object handback) {

        try {
            if (notification instanceof TraceNotification) {
                TraceNotification traceNotification = (TraceNotification) notification;
                getLog().fine("Received Trace Notification from: " + traceNotification);

                if (traceNotification.getNotificationSource() instanceof SbbNotification) {
                    if (traceNotification.getType().equals(SbbNotification.TRACE_NOTIFICATION_TYPE)) {
                        NotificationSource notificationSource =  traceNotification.getNotificationSource();
                        SbbNotification sbbNotification = (SbbNotification) notificationSource;
                        passed = doNotficationSourcesCheck(sbbNotification);
                        if (passed) {
                            logSuccessfulCheck(1114228);
                            receivedTraceNotifications++;
                        } else {
                            result.setFailed(1114228, "NotficationSources Check failed");
                        }
                    }
                }
            }
        } catch (Exception e) {
                utils().getLog().warning(e);
                getLog().warning("1114228: FAILED. Received erroneous Trace Message: " + notification.getMessage());
        }

     }
    }


    private boolean doNotficationSourcesCheck(SbbNotification theNotification) {
        SbbNotification expectedSbbNotification = null;
        boolean passed = true;
        String notification;

        //1114221
        try {
            ServiceID notifyServiceID = theNotification.getService();
            getLog().fine("1114221: getService = "+notifyServiceID);
            if (notifyServiceID.getName().equals("Test1114228TestService")) {
                logSuccessfulCheck(1114221);
            } else {
                result.setFailed(1114221, "getService() failed");
                passed = false;
            }
        }
        catch (Exception e) {
            getLog().warning(e);
            result.setFailed(1114221, "getService() failed");
            return false;
         }

        //1114220
        try {
            SbbID notifySbbID = theNotification.getSbb();
            getLog().fine("1114220: getService = "+notifySbbID);
            if (notifySbbID.getName().equals("Test1114228Test")) {
                logSuccessfulCheck(1114220);
            } else {
                result.setFailed(1114220, "getSbb() failed");
                passed = false;
            }
        }
        catch (Exception e) {
            getLog().warning(e);
            result.setFailed(1114220, "getSbb() failed");
            return false;
        }

        //1114227
        try {
            notification = theNotification.toString();
            getLog().fine("1114227: toString() : " +sbbNotification);
            logSuccessfulCheck(1114227);
        }
        catch (Exception e) {
            getLog().warning(e);
            result.setFailed(1114227, "Incorrect Notification Name");
            return false;
        }

        //1114230
        try {
            theNotification.hashCode();
            logSuccessfulCheck(1114230);
        } catch (Exception e) {
            result.setFailed(1114230, ".hashCode() failed");
            return false;
        }

        // 1114223
        expectedSbbNotification = sbbNotification;
        try {
            boolean areEqual = theNotification.equals(expectedSbbNotification);
            getLog().fine("1114223: theNotification.equals : "+theNotification + ", " +expectedSbbNotification);
            if (areEqual == true) {
                logSuccessfulCheck(1114223);
            } else {
                result.setFailed(1114223, ".equals() failed");
                passed = false;
            }
        }
        catch (Exception e) {
            getLog().warning(e);
            result.setFailed(1114223, ".equals() failed");
            return false;
        }

        // 1114224
        try {
            int areCompared = theNotification.compareTo(expectedSbbNotification);
            if (areCompared == 0) {
                logSuccessfulCheck(1114224);
            } else {
                result.setFailed(1114224, ".compareTo() failed");
                passed = false;
            }
         }
        catch (Exception e) {
            getLog().warning(e);
            result.setFailed(1114224, ".compareTo() failed");
            return false;
        }

        //1114226
        try {
            String alarmNotify = theNotification.getAlarmNotificationType();
            getLog().fine("1114226: getAlarmNotificationType = "+alarmNotify);
            if (alarmNotify.equals(SbbNotification.ALARM_NOTIFICATION_TYPE)) {
                logSuccessfulCheck(1114226);
            } else {
                passed = false;
                result.setFailed(1114226, "getAlarmNotificationType() failed");
            }
        }
        catch (Exception e) {
            getLog().warning(e);
            result.setFailed(1114226, "getAlarmNotificationType() failed");
            return false;
        }

        //1114222
        try {
            String alarmNotify = theNotification.getTraceNotificationType();
            getLog().fine("1114222: getTraceNotificationType = "+alarmNotify);
            if (alarmNotify.equals(SbbNotification.TRACE_NOTIFICATION_TYPE)) {
                logSuccessfulCheck(1114222);
            } else {
                result.setFailed(1114222, "getTraceNotificationType() failed");
                passed = false;
            }
        }
        catch (Exception e) {
            getLog().warning(e);
            result.setFailed(1114222, "getTraceNotificationType() failed");
            return false;
        }

        //1114229
        try {
            String alarmNotify = theNotification.getUsageNotificationType();
            getLog().fine("1114229: getUsageNotificationType = "+alarmNotify);
            if (alarmNotify.equals(SbbNotification.USAGE_NOTIFICATION_TYPE))  {
                logSuccessfulCheck(1114229);
            } else {
                result.setFailed(1114222, "getTraceNotificationType() failed");
                passed = false;
            }
        }
        catch (Exception e) {
            getLog().warning(e);
            result.setFailed(1114229, "getUsageNotificationType() failed");
            return false;
        }

        //1114225
        try {
            String usageMBeanProperties = theNotification.getUsageMBeanProperties();
            getLog().fine("1114225: getUsageMBeanProperties = "+usageMBeanProperties);
            if (usageMBeanProperties.equals("serviceName=\"Test1114228TestService\",serviceVendor=\"jain.slee.tck\",serviceVersion=\"1.1\",sbbName=\"Test1114228Test\",sbbVendor=\"jain.slee.tck\",sbbVersion=\"1.1\"")) {
                logSuccessfulCheck(1114225);
            } else {
                result.setFailed(1114225, "getUsageNotificationType() failed");
                passed = false;
            }
        }
        catch (Exception e) {
            getLog().warning(e);
            result.setFailed(1114225, "getUsageNotificationType() failed");
            return false;
        }

        //111414
        try {
            String usageMBeanProperties = theNotification.getUsageMBeanProperties(serviceID, sbbID);
            getLog().fine("111414: getUsageMBeanProperties = "+usageMBeanProperties);
            if (usageMBeanProperties.equals("serviceName=\"Test1114228TestService\",serviceVendor=\"jain.slee.tck\",serviceVersion=\"1.1\",sbbName=\"Test1114228Test\",sbbVendor=\"jain.slee.tck\",sbbVersion=\"1.1\"")) {
                logSuccessfulCheck(111414);
            } else {
                result.setFailed(111414, "getUsageNotificationType() failed");
                passed = false;
            }
        }
        catch (Exception e) {
            getLog().warning(e);
            result.setFailed(111414, "getUsageNotificationType() failed");
            return false;
        }

        return passed;
    }


    private void logSuccessfulCheck(int assertionID) {
        utils().getLog().info("Check for assertion "+assertionID+" OK");
    }

    private TCKResourceListener resourceListener;
    private NotificationListener listener;
    private FutureResult result;
    private DeployableUnitID duID;
    private String testName = "Test1114228";
    private int assertionID = 1114228;
    private SbbID sbbID;
    private ServiceID serviceID;
    private ObjectName traceMBeanName;
    private TraceMBeanProxy tracembean;
    private int receivedTraceNotifications = 0;
    private int expectedTraceNotifications;
    private boolean passed = false;
    private SbbNotification sbbNotification;

}