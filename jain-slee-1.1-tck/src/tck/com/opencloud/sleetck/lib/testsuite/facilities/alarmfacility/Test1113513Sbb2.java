/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.facilities.alarmfacility;

import javax.naming.InitialContext;
import javax.slee.ActivityContextInterface;
import javax.slee.facilities.AlarmFacility;
import javax.slee.facilities.AlarmLevel;
import javax.slee.facilities.Tracer;

import com.opencloud.sleetck.lib.resource.events.TCKResourceEventX;
import com.opencloud.sleetck.lib.sbbutils.BaseTCKSbb;
import com.opencloud.sleetck.lib.sbbutils.TCKSbbUtils;

/*  
 * AssertionID(1113513): Test The return value of this clearAlarms() 
 * method is the number of alarms that were cleared.
 * 
 */
public abstract class Test1113513Sbb2 extends BaseTCKSbb {

    public static final String ALARM_MESSAGE = "Sbb2:Test1113513AlarmMessageSbb2";

    public static final String ALARM_INSTANCEID = "Test1113513AlarmInstanceIDSbb2";

    public static final String ALARM_TYPE = "javax.slee.management.Alarm";

    // Initial event
    public void onTCKResourceEventX1(TCKResourceEventX ev, ActivityContextInterface aci) {
        try {
            tracer = this.getSbbContext().getTracer("com.test");
            tracer.info("Received " + testName + " message", null);
            AlarmFacility facility = getAlarmFacility();

            // raise an alarm
            String[] alarmIDs = {
                    facility.raiseAlarm(ALARM_TYPE, "Test1113513AlarmInstanceID1", AlarmLevel.CRITICAL, ALARM_MESSAGE),
                    facility.raiseAlarm(ALARM_TYPE, "Test1113513AlarmInstanceID2", AlarmLevel.MAJOR, ALARM_MESSAGE),
                    facility.raiseAlarm(ALARM_TYPE, "Test1113513AlarmInstanceID3", AlarmLevel.WARNING, ALARM_MESSAGE),
                    facility.raiseAlarm(ALARM_TYPE, "Test1113513AlarmInstanceID4", AlarmLevel.INDETERMINATE,
                            ALARM_MESSAGE),
                    facility.raiseAlarm(ALARM_TYPE, "Test1113513AlarmInstanceID5", AlarmLevel.MINOR, ALARM_MESSAGE) };

        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }

    }

    private AlarmFacility getAlarmFacility() throws Exception {
        AlarmFacility facility = null;
        String JNDI_ALARMFACILITY_NAME = AlarmFacility.JNDI_NAME;
        try {
            facility = (AlarmFacility) new InitialContext().lookup(JNDI_ALARMFACILITY_NAME);
        } catch (Exception e) {
            tracer.warning("got unexpected Exception: " + e, null);
        }
        return facility;
    }

    private String testName = "Test1113513Test";

    private Tracer tracer;
}
