/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.profiles.profilerefs;

import java.util.Vector;
import java.util.Iterator;
import javax.slee.management.ManagementException;
import javax.slee.InvalidStateException;
import javax.slee.profile.ProfileSpecificationID;
import javax.slee.profile.ProfileID;
import javax.slee.profile.ProfileAlreadyExistsException;
import javax.slee.profile.UnrecognizedProfileNameException;
import javax.slee.profile.UnrecognizedProfileTableNameException;
import javax.slee.profile.ProfileVerificationException;
import javax.management.ObjectName;
import com.opencloud.sleetck.lib.AbstractSleeTCKTest;
import com.opencloud.sleetck.lib.DescriptionKeys;
import com.opencloud.sleetck.lib.TCKTestErrorException;
import com.opencloud.sleetck.lib.TCKTestFailureException;
import com.opencloud.sleetck.lib.TCKTestResult;
import com.opencloud.sleetck.lib.resource.testapi.TCKResourceTestInterface;
import com.opencloud.sleetck.lib.resource.TCKActivityID;
import com.opencloud.sleetck.lib.resource.events.TCKResourceEventX;
import com.opencloud.sleetck.lib.testsuite.profiles.ProfileTestConstants;
import com.opencloud.sleetck.lib.testutils.ProfileUtils;
import com.opencloud.sleetck.lib.testutils.ComponentIDLookup;
import com.opencloud.sleetck.lib.testutils.jmx.ProfileProvisioningMBeanProxy;
import com.opencloud.sleetck.lib.testutils.QueuingResourceListener;
import com.opencloud.sleetck.lib.infra.SleeTCKComponentConstants;
import com.opencloud.sleetck.lib.testutils.jmx.ProfileMBeanProxy;

/**
 * Tests assertions 914, 988 and 1977, regarding profiles referencing
 * other profiles.
 *
 * The test creates profiles A, B, C, and X, then removes C, then
 * creates the following references:
 * X -> A
 * X -> B
 * X -> C
 * X -> X
 * A -> X
 * B -> X
 */
public class ManyToManyProfilesTest extends AbstractSleeTCKTest {

    private static final String PROFILE_TABLE_NAME = "tck.ManyToManyProfilesTest.table";
    private static final String PROFILE_SPEC_NAME = "ManyToMany";
    private static final String INDEXED_ATTRIBUTE = "name";

    // Public methods

    public TCKTestResult run() throws Exception {
        // create the profile table
        ProfileProvisioningMBeanProxy profileProvisioning = profileUtils.getProfileProvisioningProxy();
        ProfileSpecificationID profileSpecID = new ComponentIDLookup(utils()).lookupProfileSpecificationID(
            PROFILE_SPEC_NAME,SleeTCKComponentConstants.TCK_VENDOR,"1.0");
        getLog().info("Creating profile table: "+PROFILE_TABLE_NAME);
        profileProvisioning.createProfileTable(profileSpecID,PROFILE_TABLE_NAME);
        tableCreated = true;

        // create profiles X, A, B, and C
        ProfileID profileA = new ProfileID(PROFILE_TABLE_NAME,"A");
        ProfileID profileB = new ProfileID(PROFILE_TABLE_NAME,"B");
        ProfileID profileC = new ProfileID(PROFILE_TABLE_NAME,"C");
        ProfileID profileX = new ProfileID(PROFILE_TABLE_NAME,"X");
        ProfileID[] profileIDs = {profileA,profileB,profileC,profileX};
        for (int i = 0 ; i < profileIDs.length; i++) {
            getLog().fine("Creating profile"+profileIDs[i]);
            javax.management.ObjectName profile = profileProvisioning.createProfile(PROFILE_TABLE_NAME,profileIDs[i].getProfileName());
            ProfileMBeanProxy profProxy = utils().getMBeanProxyFactory().createProfileMBeanProxy(profile);
            profProxy.commitProfile();
        }
        // get a proxy for each profile
        ManyToManyProfileProxy profileProxyA = getProfileProxy(profileA.getProfileName());
        ManyToManyProfileProxy profileProxyB = getProfileProxy(profileB.getProfileName());
        ManyToManyProfileProxy profileProxyC = getProfileProxy(profileC.getProfileName());
        ManyToManyProfileProxy profileProxyX = getProfileProxy(profileX.getProfileName());
        // add the profile proxies in the same order as the profile IDs
        activeProfileProxies.add(profileProxyA);
        activeProfileProxies.add(profileProxyB);
        activeProfileProxies.add(profileProxyC);
        activeProfileProxies.add(profileProxyX);
        // set the name attribute of each profile to the profile name
        getLog().fine("Setting the "+INDEXED_ATTRIBUTE+" attribute of each profile to its profile name");
        for (int i = 0; i < activeProfileProxies.size(); i++) {
            ManyToManyProfileProxy aProxy = (ManyToManyProfileProxy)activeProfileProxies.elementAt(i);
            aProxy.editProfile();
            aProxy.setName(profileIDs[i].getProfileName());
            aProxy.commitProfile();
        }

        // close and remove profile C
        profileProxyC.closeProfile();
        activeProfileProxies.remove(profileProxyC);
        getLog().info("Removing profile "+profileC);
        profileProvisioning.removeProfile(PROFILE_TABLE_NAME,profileC.getProfileName());

        // create and commit references between profiles using ProfileIDs and ProfileID arrays
        getLog().info("Creating references between profiles");

        // X -> A
        performEdit(profileProxyX,profileA,988,"Couldn't create a reference to another profile using a ProfileID");
        // X -> A,B
        performEdit(profileProxyX,new ProfileID[]{profileA,profileB},914,"Couldn't create a reference to two other profiles using a ProfileID array");
        // X -> C
        performEdit(profileProxyX,profileC,1977,"Couldn't create a reference to a deleted profile");
        // X -> X
        performEdit(profileProxyX,profileX,988,"Couldn't create a reference from a profile to itself");
        // A -> X
        performEdit(profileProxyA,profileX,914,"Couldn't create a reference from A to X when X references A");
        // B -> X
        performEdit(profileProxyB,profileX,914,"Couldn't create a reference from B to X when X references A and B, and A references X");

        return TCKTestResult.passed();
    }

    public void setUp() throws Exception {
        // install profile spec
        getLog().fine("Installing profile specification");
        String profDUPath = utils().getTestParams().getProperty(ProfileTestConstants.PROFILE_SPEC_DU_PATH_PARAM);
        utils().install(profDUPath);
        // set up the resource listener
        resourceListener = new QueuingResourceListener(utils());
        setResourceListener(resourceListener);
        profileUtils = new ProfileUtils(utils());
        activeProfileProxies = new Vector();
    }

    public void tearDown() throws Exception {
        // close profiles
        if(activeProfileProxies != null) {
            getLog().fine("Closing profiles");
            Iterator activeProfileProxiesIter = activeProfileProxies.iterator();
            while(activeProfileProxiesIter.hasNext()) {
                ManyToManyProfileProxy aProxy = (ManyToManyProfileProxy)activeProfileProxiesIter.next();
                try {
                    if(aProxy != null) {
                        if(aProxy.isProfileWriteable()) aProxy.restoreProfile();
                        aProxy.closeProfile();
                    }
                } catch (Exception ex) {
                    getLog().warning("Exception caught while trying to close profile:");
                    getLog().warning(ex);
                }
            }
        }
        // remove the profile table
        try {
            if(profileUtils != null && tableCreated) {
                getLog().fine("Removing profile table");
                profileUtils.removeProfileTable(PROFILE_TABLE_NAME);
            }
        } catch(Exception e) {
            getLog().warning("Caught exception while trying to remove profile table:");
            getLog().warning(e);
        }
        super.tearDown();
    }

    // Private methods

    private void performEdit(ManyToManyProfileProxy profile, ProfileID newSingleProfile, int assertionID,
            String failMessage) throws TCKTestFailureException, TCKTestErrorException, ManagementException, InvalidStateException {
        getLog().info("Creating a reference from profile "+profile.getName()+" to "+newSingleProfile);
        profile.editProfile();
        profile.setSingleProfile(newSingleProfile);
        try {
            profile.commitProfile();
        } catch (ProfileVerificationException verificationEx) {
            throw new TCKTestFailureException(assertionID,failMessage,verificationEx);
        }
    }

    private void performEdit(ManyToManyProfileProxy profile, ProfileID[] newProfileArray, int assertionID,
            String failMessage) throws TCKTestFailureException, TCKTestErrorException, ManagementException, InvalidStateException {
        getLog().info("Creating an array of references from profile "+profile.getName()+" to a set of profiles");
        profile.editProfile();
        profile.setProfileArray(newProfileArray);
        try {
            profile.commitProfile();
        } catch (ProfileVerificationException verificationEx) {
            throw new TCKTestFailureException(assertionID,failMessage,verificationEx);
        }
    }

    private ManyToManyProfileProxy getProfileProxy(String profileName) throws TCKTestErrorException,
                    UnrecognizedProfileNameException, UnrecognizedProfileTableNameException, ManagementException {
        ObjectName mbeanName = profileUtils.getProfileProvisioningProxy().getProfile(PROFILE_TABLE_NAME,profileName);
        return new ManyToManyProfileProxy(mbeanName,utils().getMBeanFacade());
    }

    // Private state

    private QueuingResourceListener resourceListener;
    private Vector activeProfileProxies;
    private ProfileUtils profileUtils;
    private boolean tableCreated = false;

}
