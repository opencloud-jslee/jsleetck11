/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.facilities.tracefacility.tracelevel;

import javax.slee.ActivityContextInterface;
import javax.slee.facilities.TraceLevel;
import javax.slee.facilities.Tracer;

import com.opencloud.sleetck.lib.resource.events.TCKResourceEventX;
import com.opencloud.sleetck.lib.sbbutils.BaseTCKSbb;
import com.opencloud.sleetck.lib.sbbutils.TCKSbbUtils;

/*
 * AssertionID (1113153): Test when OFF is specified in a trace filter, 
 * no traces at a lower level than OFF, which is all traces, will be emitted.
 */
public abstract class Test1113153Sbb extends BaseTCKSbb {
    public static final String TRACE_MESSAGE_SEVERE = "SEVERE:Test1113153TraceMessage";

    public static final String TRACE_MESSAGE_WARNING = "WARNING:Test1113153TraceMessage";

    public static final String TRACE_MESSAGE_INFO = "INFO:Test1113153TraceMessage";

    public static final String TRACE_MESSAGE_CONFIG = "CONFIG:Test1113153TraceMessage";

    public static final String TRACE_MESSAGE_FINE = "FINE:Test1113153TraceMessage";

    public static final String TRACE_MESSAGE_FINER = "FINER:Test1113153TraceMessage";

    public static final String TRACE_MESSAGE_FINEST = "FINEST:Test1113153TraceMessage";

    // Initial event
    public void onTCKResourceEventX1(TCKResourceEventX event, ActivityContextInterface aci) {

        try {

            doTest1113153Test();

        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }
    }

    private void doTest1113153Test() throws Exception {
        Tracer tracer = null;
        // 1113153
        try {
            tracer = getSbbContext().getTracer("com.test");
            if (tracer.isTraceable(TraceLevel.SEVERE))
                tracer.trace(TraceLevel.SEVERE, TRACE_MESSAGE_SEVERE);
            if (tracer.isTraceable(TraceLevel.WARNING))
                tracer.trace(TraceLevel.WARNING, TRACE_MESSAGE_WARNING);
            if (tracer.isTraceable(TraceLevel.INFO))
                tracer.trace(TraceLevel.INFO, TRACE_MESSAGE_INFO);
            if (tracer.isTraceable(TraceLevel.CONFIG))
                tracer.trace(TraceLevel.CONFIG, TRACE_MESSAGE_CONFIG);
            if (tracer.isTraceable(TraceLevel.FINE))
                tracer.trace(TraceLevel.FINE, TRACE_MESSAGE_FINE);
            if (tracer.isTraceable(TraceLevel.FINER))
                tracer.trace(TraceLevel.FINER, TRACE_MESSAGE_FINER);
            if (tracer.isTraceable(TraceLevel.FINEST))
                tracer.trace(TraceLevel.FINEST, TRACE_MESSAGE_FINEST);
        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }
    }

}
