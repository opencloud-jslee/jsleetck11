/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.javax.slee.management.ServiceUsageMBean;

import com.opencloud.sleetck.lib.resource.events.TCKResourceEventX;
import com.opencloud.sleetck.lib.sbbutils.BaseTCKSbb;
import com.opencloud.sleetck.lib.sbbutils.TCKSbbUtils;
import com.opencloud.sleetck.lib.testsuite.usage.common.GenericUsageSbbInstructions;
import com.opencloud.sleetck.lib.testsuite.usage.common.GenericUsageSbbLocal;

import javax.slee.ActivityContextInterface;
import javax.slee.ChildRelation;

/**
 * Calls doUpdates() on both child SBBs from a TCKResourceEventX1 event handler
 */
public abstract class ResetParametersTestParentSbb extends BaseTCKSbb {

    public void onTCKResourceEventX1(TCKResourceEventX event, ActivityContextInterface aci) {
        try {
            GenericUsageSbbInstructions instructions = GenericUsageSbbInstructions.fromExported(event.getMessage());
            ((GenericUsageSbbLocal)getChildRelationA().create()).doUpdates(instructions);
            ((GenericUsageSbbLocal)getChildRelationB().create()).doUpdates(instructions);
        } catch(Exception e) {
            TCKSbbUtils.handleException(e);
        }
    }

    public abstract ChildRelation getChildRelationA();
    public abstract ChildRelation getChildRelationB();

}
