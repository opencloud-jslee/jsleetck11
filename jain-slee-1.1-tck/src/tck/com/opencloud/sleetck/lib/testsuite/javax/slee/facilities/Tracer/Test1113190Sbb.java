/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.javax.slee.facilities.Tracer;

import javax.slee.ActivityContextInterface;
import javax.slee.facilities.Tracer;

import com.opencloud.sleetck.lib.resource.events.TCKResourceEventX;
import com.opencloud.sleetck.lib.sbbutils.BaseTCKSbb;
import com.opencloud.sleetck.lib.sbbutils.TCKSbbUtils;

/*
 * AssertionID (1113190): Test This tracer.severe method has no effect 
 * and returns silently if isSevereEnabled() returns false.
 * 
 * AssertionID (1113212): Test This tracer.warning method has no effect 
 * and returns silently if isSevereEnabled() returns false.
 * 
 * AssertionID (1113234): Test This tracer.info method has no effect 
 * and returns silently if isSevereEnabled() returns false.
 * 
 * AssertionID (1113256): Test This tracer.config method has no effect 
 * and returns silently if isSevereEnabled() returns false.
 * 
 * AssertionID (1113278): Test This tracer.fine method has no effect 
 * and returns silently if isSevereEnabled() returns false.
 * 
 * AssertionID (1113300): Test This tracer.finer method has no effect 
 * and returns silently if isSevereEnabled() returns false.
 * 
 * AssertionID (1113322): Test This tracer.finest method has no effect 
 * and returns silently if isSevereEnabled() returns false.
 *
 */
public abstract class Test1113190Sbb extends BaseTCKSbb {
    public static final String TRACE_MESSAGE_SEVERE = "SEVERE:Test1113190TraceMessage";

    public static final String TRACE_MESSAGE_WARNING = "WARNING:Test1113190TraceMessage";

    public static final String TRACE_MESSAGE_INFO = "INFO:Test1113190TraceMessage";

    public static final String TRACE_MESSAGE_CONFIG = "CONFIG:Test1113190TraceMessage";

    public static final String TRACE_MESSAGE_FINE = "FINE:Test1113190TraceMessage";

    public static final String TRACE_MESSAGE_FINER = "FINER:Test1113190TraceMessage";

    public static final String TRACE_MESSAGE_FINEST = "FINEST:Test1113190TraceMessage";

    // Initial event
    public void onTCKResourceEventX1(TCKResourceEventX event, ActivityContextInterface aci) {

        try {

            doTest1113190Test(event, aci);

        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }
    }

    private void doTest1113190Test(TCKResourceEventX event, ActivityContextInterface aci) throws Exception {
        Tracer tracer = null;

        try {
            setTestName((String) event.getMessage());
            tracer = getSbbContext().getTracer("com.test");
            //1113190
            if (getTestName().equals("SEVERE")) {
                    tracer.severe(TRACE_MESSAGE_SEVERE);
                    tracer.severe(TRACE_MESSAGE_SEVERE, null);
            }

            //1113212
            if (getTestName().equals("WARNING")) {
                    tracer.warning(TRACE_MESSAGE_WARNING);
                    tracer.warning(TRACE_MESSAGE_WARNING, null);
            }

            //1113234
            if (getTestName().equals("INFO")) {
                    tracer.info(TRACE_MESSAGE_INFO);
                    tracer.info(TRACE_MESSAGE_INFO, null);
            }

            //1113256
            if (getTestName().equals("CONFIG")) {
                if (tracer.isConfigEnabled()) {
                    tracer.config(TRACE_MESSAGE_CONFIG);
                    tracer.config(TRACE_MESSAGE_CONFIG, null);
                }
            }

            //1113278
            if (getTestName().equals("FINE")) {
                    tracer.fine(TRACE_MESSAGE_FINE);
                    tracer.fine(TRACE_MESSAGE_FINE, null);
            }

            //1113300
            if (getTestName().equals("FINER")) {
                    tracer.finer(TRACE_MESSAGE_FINER);
                    tracer.finer(TRACE_MESSAGE_FINER, null);
            }

            //1113322
            if (getTestName().equals("FINEST")) {
                    tracer.finest(TRACE_MESSAGE_FINEST);
                    tracer.finest(TRACE_MESSAGE_FINEST, null);
            }
        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }
    }

    public abstract void setTestName(String testName);

    public abstract String getTestName();

}
