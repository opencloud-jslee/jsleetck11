/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.resource.endpoint;

import java.util.HashMap;

import com.opencloud.sleetck.lib.TCKTestErrorException;
import com.opencloud.sleetck.lib.TCKTestFailureException;
import com.opencloud.sleetck.lib.TCKTestResult;
import com.opencloud.sleetck.lib.testsuite.resource.BaseResourceTest;
import com.opencloud.sleetck.lib.testsuite.resource.RAMethods;

/**
 * Test to ensure that startActivityTransacted() (both forms) are mandatory
 * transactional.
 * <p>
 * Test assertion: 1115258
 */
public class Test1115258Test extends BaseResourceTest {

    private final static int ASSERTION_ID = 1115258;

    public TCKTestResult run() throws Exception {
        HashMap results = sendMessage(RAMethods.startActivityTransacted, new Integer(ASSERTION_ID));
        if (results == null)
            throw new TCKTestErrorException("Test timed out while waiting for response from test component");
        Object result = results.get("result1");
        if (result instanceof Exception)
            throw new TCKTestFailureException(ASSERTION_ID, "Unexpected exception thrown during test:", (Exception) result);
        if (Boolean.FALSE.equals(result))
            throw new TCKTestFailureException(ASSERTION_ID,
                    "startActivityTransacted(handle) failed to throw a TransactionRequiredLocalException when called without an enclosing transaction");
        if (!Boolean.TRUE.equals(result))
            throw new TCKTestErrorException("Unexpected result received from test component: " + result);

        result = results.get("result2");
        if (result instanceof Exception)
            throw new TCKTestFailureException(ASSERTION_ID, "Unexpected exception thrown during test:", (Exception) result);
        if (Boolean.FALSE.equals(result))
            throw new TCKTestFailureException(ASSERTION_ID,
                    "startActivityTransacted(handle, flags) failed to throw a TransactionRequiredLocalException when called without an enclosing transaction");
        if (!Boolean.TRUE.equals(result))
            throw new TCKTestErrorException("Unexpected result received from test component: " + result);

        return TCKTestResult.passed();
    }

}
