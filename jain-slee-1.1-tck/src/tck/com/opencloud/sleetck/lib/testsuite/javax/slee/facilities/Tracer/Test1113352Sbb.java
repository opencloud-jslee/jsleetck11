/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.javax.slee.facilities.Tracer;

import javax.slee.ActivityContextInterface;
import javax.slee.facilities.TraceLevel;
import javax.slee.facilities.Tracer;

import com.opencloud.sleetck.lib.resource.events.TCKResourceEventX;
import com.opencloud.sleetck.lib.sbbutils.BaseTCKSbb;
import com.opencloud.sleetck.lib.sbbutils.TCKSbbUtils;

/*
 * AssertionID (1113352): Test This Tracer.trace(TraceLevel level, 
 * java.lang.String message) method has no effect 
 * and returns silently if isSevereEnabled() returns false.
 * 
 * AssertionID (1113361): Test This Tracer.trace(TraceLevel level, 
 * java.lang.String message,  java.lang.Throwable cause) method has 
 * no effect and returns silently if isSevereEnabled() returns false.
 *
 */
public abstract class Test1113352Sbb extends BaseTCKSbb {
    public static final String TRACE_MESSAGE_SEVERE = "SEVERE:Test1113352TraceMessage";

    public static final String TRACE_MESSAGE_WARNING = "WARNING:Test1113352TraceMessage";

    public static final String TRACE_MESSAGE_INFO = "INFO:Test1113352TraceMessage";

    public static final String TRACE_MESSAGE_CONFIG = "CONFIG:Test1113352TraceMessage";

    public static final String TRACE_MESSAGE_FINE = "FINE:Test1113352TraceMessage";

    public static final String TRACE_MESSAGE_FINER = "FINER:Test1113352TraceMessage";

    public static final String TRACE_MESSAGE_FINEST = "FINEST:Test1113352TraceMessage";

    // Initial event
    public void onTCKResourceEventX1(TCKResourceEventX event, ActivityContextInterface aci) {

        try {

            doTest1113352Test(event, aci);

        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }
    }

    private void doTest1113352Test(TCKResourceEventX event, ActivityContextInterface aci) throws Exception {
        Tracer tracer = null;

        try {
            setTestName((String) event.getMessage());
            tracer = getSbbContext().getTracer("com.test");
            //1113352:SEVERE
            if (getTestName().equals("SEVERE")) {
                    tracer.trace(TraceLevel.SEVERE, TRACE_MESSAGE_SEVERE);
                    tracer.trace(TraceLevel.SEVERE, TRACE_MESSAGE_SEVERE, null);
            }

            //1113352:WARNING
            if (getTestName().equals("WARNING")) {
                    tracer.trace(TraceLevel.WARNING, TRACE_MESSAGE_WARNING);
                    tracer.trace(TraceLevel.WARNING, TRACE_MESSAGE_WARNING, null);
            }

            //1113352:INFO
            if (getTestName().equals("INFO")) {
                    tracer.trace(TraceLevel.INFO, TRACE_MESSAGE_INFO);
                    tracer.trace(TraceLevel.INFO, TRACE_MESSAGE_INFO, null);
            }

            //1113352:CONFIG
            if (getTestName().equals("CONFIG")) {
                    tracer.trace(TraceLevel.CONFIG, TRACE_MESSAGE_CONFIG);
                    tracer.trace(TraceLevel.CONFIG, TRACE_MESSAGE_CONFIG, null);
            }

            //1113352:FINE
            if (getTestName().equals("FINE")) {
                    tracer.trace(TraceLevel.FINE, TRACE_MESSAGE_FINE);
                    tracer.trace(TraceLevel.FINE, TRACE_MESSAGE_FINE, null);
            }

            //1113352:FINER
            if (getTestName().equals("FINER")) {
                    tracer.trace(TraceLevel.FINER, TRACE_MESSAGE_FINER);
                    tracer.trace(TraceLevel.FINER, TRACE_MESSAGE_FINER, null);
            }

            //1113352:FINEST
            if (getTestName().equals("FINEST")) {
                    tracer.trace(TraceLevel.FINEST, TRACE_MESSAGE_FINEST);
                    tracer.trace(TraceLevel.FINEST, TRACE_MESSAGE_FINEST, null);
            }
        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }
    }

    public abstract void setTestName(String testName);

    public abstract String getTestName();

}
