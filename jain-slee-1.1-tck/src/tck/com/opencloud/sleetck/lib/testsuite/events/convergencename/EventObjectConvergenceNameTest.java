/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.events.convergencename;

import java.util.Vector;
import java.util.StringTokenizer;
import com.opencloud.sleetck.lib.TCKTestResult;
import com.opencloud.sleetck.lib.resource.testapi.TCKResourceTestInterface;
import com.opencloud.sleetck.lib.resource.TCKActivityID;
import com.opencloud.sleetck.lib.resource.events.TCKResourceEventX;
import javax.slee.Address;
import javax.slee.AddressPlan;

/**
 * The EventObjectConvergenceNameTest tests the SLEE's handling of the
 * convergence name when the EventObject parameter is selected, and different
 * event objects are passed for each invocation.
 */
public class EventObjectConvergenceNameTest extends AbstractConvergenceNameTest {

    public TCKTestResult run() throws Exception {

        TCKResourceTestInterface resource = utils().getResourceInterface();
        String eventType = TCKResourceEventX.X1;
        Address address = new Address(AddressPlan.IP,ADDRESS_PREFIX+"1");
        TCKActivityID activityID = resource.createActivity(ACTIVITY_ID_PREFIX+("1"));
        InitialEventSelectorParameters iesParams = new InitialEventSelectorParameters(
            false,false,false,true,false,null,false,false,false,null); // select event only

        // send event 1
        sendEventAndWait(eventType,"1",activityID,address,"1",iesParams);

        // send event 2 with a different event object, but otherwise
        // the same convergence name values as event 1
        sendEventAndWait(eventType,"2",activityID,address,"2",iesParams);

        return TCKTestResult.passed();
    }

}