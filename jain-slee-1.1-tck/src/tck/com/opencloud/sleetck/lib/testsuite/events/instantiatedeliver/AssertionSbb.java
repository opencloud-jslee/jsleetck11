/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.events.instantiatedeliver;

import com.opencloud.sleetck.lib.sbbutils.BaseTCKSbb;
import com.opencloud.sleetck.lib.sbbutils.TCKSbbUtils;
import com.opencloud.sleetck.lib.resource.events.TCKResourceEventX;
import com.opencloud.sleetck.lib.resource.events.TCKResourceEventY;
import com.opencloud.sleetck.lib.resource.sbbapi.TCKActivity;
import javax.slee.facilities.*;
import javax.slee.*;
import javax.slee.nullactivity.*;
import javax.naming.InitialContext;

public abstract class AssertionSbb extends BaseTCKSbb{

    public void sbbCreate() throws CreateException {
        setReceivedX1( false );
    }

    public void onTCKResourceEventX1(TCKResourceEventX event, ActivityContextInterface aci) {
        setReceivedX1( true );
    }

    public void sbbRemove(){
        if( getReceivedX1() )
            sendTestSuccess();
        else
            sendTestFail("did not receive event");
    }

    private void sendTestSuccess(){
        sendMessage( "SUCCESS","" );
    }

    private void sendTestFail(String reason){
        sendMessage("FAILURE", reason);
    }

    private void sendMessage(String arg1, String arg2) {
        try {
            TCKSbbUtils.createTrace(getSbbID(),Level.FINE,"sending a message back with values: " + arg1 +", and " +arg2,null);
            // send a reply: the value of the boolean message
            String[] message = new String[2];
            message[0] = arg1; message[1] = arg2;
            TCKSbbUtils.getResourceInterface().sendSbbMessage(message);
        } catch(Exception e) {
            TCKSbbUtils.handleException(e);
        }
    }


    public abstract boolean getReceivedX1();
    public abstract void setReceivedX1( boolean value );

}
