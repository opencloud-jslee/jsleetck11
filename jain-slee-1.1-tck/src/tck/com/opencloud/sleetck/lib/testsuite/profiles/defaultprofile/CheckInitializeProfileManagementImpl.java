/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.profiles.defaultprofile;

import javax.slee.profile.ProfileManagement;
import javax.slee.profile.ProfileVerificationException;

public abstract class CheckInitializeProfileManagementImpl
                implements CheckInitializeProfileCMP, CheckInitializeProfileManagement, ProfileManagement {

    public void profileInitialize() {
        setValue(INITIALIZED_VALUE);
        setTransientValue2(INITIALIZED_VALUE);
    }

    public void profileLoad() {
        // no-op
    }

    public void profileStore() {
        setValue2(getTransientValue2());
    }

    public void profileVerify() throws ProfileVerificationException {
        // no-op
    }

    // Profile management methods
    public String getTransientValue2() {
        return value2;
    }
    public void setTransientValue2(String value2) {
        this.value2 = value2;
    }

    private String value2;
}