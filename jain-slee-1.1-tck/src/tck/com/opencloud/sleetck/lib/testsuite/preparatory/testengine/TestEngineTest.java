/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.preparatory.testengine;

import com.opencloud.sleetck.lib.*;

/**
 * Very basic test which simply checks that the setUp method is called.
 * Useful for testing whether the test engine can find and run a test.
 */
public class TestEngineTest implements SleeTCKTest {

    public void init(SleeTCKTestUtils utils) {
        this.utils = utils;
        wasSetUpCalled = false;
    }

    public TCKTestResult run() {
        utils.getLog().info("Running TestEngineTest");
        return wasSetUpCalled ? TCKTestResult.passed() :
               TCKTestResult.failed(TCKTestResult.NO_ASSERTION,"The setUp() method was not called.");
    }

    public void setUp() throws Exception {
        wasSetUpCalled = true;
    }

    public void tearDown() {}

    private boolean wasSetUpCalled;
    private SleeTCKTestUtils utils;

}