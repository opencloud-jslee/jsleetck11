/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.activities.activitycontext;

import javax.slee.ActivityEndEvent;
import javax.slee.ActivityContextInterface;
import javax.slee.SbbContext;
import javax.slee.Address;
import javax.slee.facilities.Level;

import com.opencloud.sleetck.lib.sbbutils.BaseTCKSbb;
import com.opencloud.sleetck.lib.sbbutils.TCKSbbUtils;
import com.opencloud.sleetck.lib.sbbutils.TCKSbbConstants;
import com.opencloud.sleetck.lib.resource.events.TCKResourceEventX;
import com.opencloud.sleetck.lib.resource.TCKActivityID;
import com.opencloud.sleetck.lib.resource.sbbapi.TCKActivity;
import com.opencloud.sleetck.lib.resource.sbbapi.TCKResourceSbbInterface;
import com.opencloud.sleetck.lib.resource.sbbapi.TCKActivityContextInterfaceFactory;

import java.util.HashMap;

public abstract class Test560Sbb extends BaseTCKSbb {

    public void onActivityEndEvent(ActivityEndEvent event, ActivityContextInterface aci) {
        try {

            // Check that it's the created activity that's ending, not the default activity.
            TCKActivity endedActivity = (TCKActivity)aci.getActivity();
            if (endedActivity.getID().equals(getActivityID())) {
                HashMap map = new HashMap();
                try {
                    fireVerifyEvent(new Test560Event(), aci, null);
                } catch (IllegalStateException e) {
                    map.put("Result", new Boolean(true));
                    map.put("Message", "Ok");
                    TCKSbbUtils.getResourceInterface().sendSbbMessage(map);
                    return;
                }

                map.put("Result", new Boolean(false));
                map.put("Message", "Expected to receive IllegalStateException, did not.");
                TCKSbbUtils.getResourceInterface().sendSbbMessage(map);
            }
        } catch (Exception ex) {
            TCKSbbUtils.handleException(ex);
        }

    }

    public void onTCKResourceEventX1(TCKResourceEventX event, ActivityContextInterface aci) {

        try {
            // Create an activity, attach this SBB to it.
            TCKActivityID activityID = TCKSbbUtils.getResourceInterface().createActivity("Test560SecondActivity");
            TCKResourceSbbInterface resource = TCKSbbUtils.getResourceInterface();
            TCKActivityContextInterfaceFactory factory = (TCKActivityContextInterfaceFactory) TCKSbbUtils.getSbbEnvironment().lookup(TCKSbbConstants.TCK_ACI_FACTORY_LOCATION);
            ActivityContextInterface testAci = factory.getActivityContextInterface(resource.getActivity(activityID));

            testAci.attach(getSbbContext().getSbbLocalObject());

            setActivityID(activityID);

            // End that activity.
            resource.endActivity(activityID);

        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }

    }

    public abstract void fireVerifyEvent(Test560Event event, ActivityContextInterface aci, Address address);

    // CMP field accessors
    public abstract TCKActivityID getActivityID();
    public abstract void setActivityID(TCKActivityID activityID);

}
