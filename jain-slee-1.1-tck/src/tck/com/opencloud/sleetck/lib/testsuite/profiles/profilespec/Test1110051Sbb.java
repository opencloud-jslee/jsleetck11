/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.profiles.profilespec;

import java.util.HashMap;

import javax.naming.Context;
import javax.slee.ActivityContextInterface;
import javax.slee.Address;
import javax.slee.profile.ProfileFacility;
import javax.slee.profile.ProfileTable;

import com.opencloud.sleetck.lib.resource.events.TCKResourceEventX;
import com.opencloud.sleetck.lib.sbbutils.BaseTCKSbb;
import com.opencloud.sleetck.lib.sbbutils.TCKSbbUtils;
import com.opencloud.sleetck.lib.sbbutils.events2.SbbBaseMessageComposer;
import com.opencloud.sleetck.lib.sbbutils.events2.SendResultEvent;

public abstract class Test1110051Sbb extends BaseTCKSbb {

    public static final String PROFILE_TABLE_NAME = "Test1110051ProfileTable";
    public static final String PROFILE_NAME = "Test1110051Profile";

    private void sendLogMsgCall (String msg) {
        HashMap map = SbbBaseMessageComposer.getLogMsg(msg);
        try {
            TCKSbbUtils.getResourceInterface().callTest(map);
        }
        catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }
    }

    private void sendResult(boolean result, int id, String msg, ActivityContextInterface aci) {
        HashMap map = SbbBaseMessageComposer.getSetResultMsg(result, id, msg);
        setResult(map);
        fireSendResultEvent(new SendResultEvent(), aci, null);
    }

    public void onSendResultEvent(SendResultEvent event, ActivityContextInterface aci) {

        try {
            TCKSbbUtils.getResourceInterface().sendSbbMessage(getResult());

        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }
    }

    public void onTCKResourceEventX1(TCKResourceEventX event, ActivityContextInterface aci) {
        try {

            sendLogMsgCall("Received TCKResourceEventX1.");

            //Get ProfileFacility from naming context
            Context env = TCKSbbUtils.getSbbEnvironment();
            ProfileFacility profileFacility = (ProfileFacility)env.lookup("slee/facilities/profile");

            sendLogMsgCall("Obtained ProfileFacility from naming context.");

            //use profileFacility to get Profile a profile table interface (since JAIN SLEE 1.1)
            ProfileTable profileTable = profileFacility.getProfileTable(PROFILE_TABLE_NAME);

            sendLogMsgCall("Obtained ProfileTable object through Profile Facility.");

            //check that returned ProfileTable object is really implemented
            if (profileTable == null) {
                sendResult(false, 1110052, "The SLEE's default ProfileTable interface implementation was found to be invalid. " +
                        "(ProfileFacility.getProfileTable("+PROFILE_TABLE_NAME+") returned 'null')", aci);
                return;
            }

            sendLogMsgCall("Checked that ProfileTable object is valid.");

            //test get/set accessor methods
            ProfileSpecTestsProfileCMP profileCMP = (ProfileSpecTestsProfileCMP)profileTable.find(PROFILE_NAME);
            profileCMP.setValue("newValue");
            sendLogMsgCall("Set new value for profile attribute.");
            if (!profileCMP.getValue().equals("newValue"))
                sendResult(false, 1110061, "Failed to use set/get accessor methods properly.", aci);
            sendLogMsgCall("Checked that new value has been successfully set and could be obtained by 'get' accessor.");

            sendResult(true, 1110052, "Test successful.", aci);

        } catch(Exception e) {
            TCKSbbUtils.handleException(e);
        }
    }

    public abstract void fireSendResultEvent(SendResultEvent event, ActivityContextInterface aci, Address address);

    public abstract void setResult(HashMap map);
    public abstract HashMap getResult();
}
