/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.facilities.profilefacility;

import java.rmi.RemoteException;
import java.util.Map;

import javax.slee.ComponentID;
import javax.slee.management.DeployableUnitDescriptor;
import javax.slee.management.DeployableUnitID;
import javax.slee.profile.ProfileSpecificationID;

import com.opencloud.sleetck.lib.AbstractSleeTCKTest;
import com.opencloud.sleetck.lib.DescriptionKeys;
import com.opencloud.sleetck.lib.TCKTestFailureException;
import com.opencloud.sleetck.lib.resource.TCKActivityID;
import com.opencloud.sleetck.lib.resource.TCKSbbMessage;
import com.opencloud.sleetck.lib.resource.events.TCKResourceEventX;
import com.opencloud.sleetck.lib.resource.testapi.TCKResourceListener;
import com.opencloud.sleetck.lib.resource.testapi.TCKResourceTestInterface;
import com.opencloud.sleetck.lib.testutils.Assert;
import com.opencloud.sleetck.lib.testutils.BaseTCKResourceListener;
import com.opencloud.sleetck.lib.testutils.FutureResult;
import com.opencloud.sleetck.lib.testutils.ProfileUtils;
import com.opencloud.sleetck.lib.testutils.jmx.DeploymentMBeanProxy;
import com.opencloud.sleetck.lib.testutils.jmx.ProfileMBeanProxy;
import com.opencloud.sleetck.lib.testutils.jmx.ProfileProvisioningMBeanProxy;

/*
 * AssertionID(1113032): Test The getProfileTable method returns the ProfileTable 
 * object of the Profile Table specified by the profileTableName argument.
 * AssertionID(1113033): Test the returned object may be cast to the profile table 
 * interface defined by the Profile Specification the Profile Table was created from 
 * using the standard Java typecast mechanism.
 * AssertionID(1113036): Test If the profileTableName argument does not identify 
 * a Profile Table recognized by the SLEE, the method throws a 
 * javax.slee.profile.UnrecognizedProfileTableNameException.
 * AssertionID(1113037): Test If the profileTableName argument is null, the method 
 * throws a java.lang.NullPointerException.
 */
public class Test1113032Test extends AbstractSleeTCKTest {

    /**
     * Perform the actual test.
     */
    public void run(FutureResult result) throws Exception {
        this.result = result;
        String activityName = "Test1113032Test";
        TCKResourceTestInterface resource = utils().getResourceInterface();
        TCKActivityID activityID = resource.createActivity(activityName);
        getLog().info("Firing event: " + testName);

        // kickoff the test
        resource.fireEvent(TCKResourceEventX.X1, testName, activityID, null);
    }

    /**
     * Do all the pre-run configuration of the test.
     */

    public void setUp() throws Exception {

        getLog().fine("Connecting to resource");
        resourceListener = new TCKResourceListenerImpl();
        setResourceListener(resourceListener);

        DeployableUnitID duID = setupService(DescriptionKeys.SERVICE_DU_PATH_PARAM, true);

        // Create a Profile Table, and some Profiles.
        ProfileProvisioningMBeanProxy profileProxy = new ProfileUtils(utils()).getProfileProvisioningProxy();
        DeploymentMBeanProxy duProxy = utils().getDeploymentMBeanProxy();
        DeployableUnitDescriptor duDesc = duProxy.getDescriptor(duID);
        ComponentID components[] = duDesc.getComponents();

        for (int i = 0; i < components.length; i++) {
            if (components[i] instanceof ProfileSpecificationID) {
                ProfileSpecificationID profileSpecID = (ProfileSpecificationID) components[i];

                profileProxy.createProfileTable(profileSpecID, Test1113032Sbb.PROFILE_TABLE_NAME);
                javax.management.ObjectName profile = profileProxy.createProfile(Test1113032Sbb.PROFILE_TABLE_NAME,
                        Test1113032Sbb.PROFILE_NAME);
                ProfileMBeanProxy profProxy = utils().getMBeanProxyFactory().createProfileMBeanProxy(profile);
                profProxy.commitProfile();
            }
        }

    }

    /**
     * Clean up after the test.
     */

    public void tearDown() throws Exception {
        ProfileProvisioningMBeanProxy profileProxy = new ProfileUtils(utils()).getProfileProvisioningProxy();
        try {
            profileProxy.removeProfile(Test1113032Sbb.PROFILE_TABLE_NAME, Test1113032Sbb.PROFILE_NAME);
        } catch (Exception e) {
        }
        try {
            profileProxy.removeProfileTable(Test1113032Sbb.PROFILE_TABLE_NAME);
        } catch (Exception e) {
        }

        // cleanup
        super.tearDown();
    }

    private class TCKResourceListenerImpl extends BaseTCKResourceListener {
        // TCKResourceListener methods

        public synchronized void onSbbMessage(TCKSbbMessage message, TCKActivityID calledActivity)
                throws RemoteException {

            Map sbbData = (Map) message.getMessage();
            String sbbTestName = "Test1113032Test";
            String sbbTestResult = (String) sbbData.get("result");
            String sbbTestMessage = (String) sbbData.get("message");
            int assertionID = ((Integer) sbbData.get("id")).intValue();
            getLog().info(
                    "Received message from SBB: testname=" + sbbTestName + ", result=" + sbbTestResult + ", message="
                            + sbbTestMessage + ", id=" + assertionID);
            try {
                Assert.assertEquals(assertionID, "Test " + sbbTestName + " failed.", "pass", sbbTestResult);
                result.setPassed();
            } catch (TCKTestFailureException ex) {
                result.setFailed(assertionID, sbbTestMessage);
            }
        }

        public void onException(Exception exception) throws RemoteException {
            getLog().warning("Received Exception from SBB or resource:");
            getLog().warning(exception);
            result.setError(exception);
        }
    }

    // private SleeTCKTestUtils utils;
    private TCKResourceListener resourceListener;

    private FutureResult result;

    private String testName = "Test1113032";
}
