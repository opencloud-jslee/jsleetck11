/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.usage.profiles;

import java.rmi.RemoteException;
import java.util.HashMap;

import javax.management.ObjectName;

import com.opencloud.sleetck.lib.TCKTestResult;
import com.opencloud.sleetck.lib.resource.TCKActivityID;
import com.opencloud.sleetck.lib.resource.TCKSbbMessage;
import com.opencloud.sleetck.lib.resource.events.TCKResourceEventX;
import com.opencloud.sleetck.lib.resource.testapi.TCKResourceTestInterface;
import com.opencloud.sleetck.lib.sbbutils.events2.SbbBaseMessageConstants;
import com.opencloud.sleetck.lib.testsuite.usage.common.UsageMBeanProxy;
import com.opencloud.sleetck.lib.testsuite.usage.common.UsageMBeanProxyImpl;
import com.opencloud.sleetck.lib.testutils.BaseTCKResourceListener;
import com.opencloud.sleetck.lib.testutils.FutureResult;
import com.opencloud.sleetck.lib.testutils.jmx.ProfileTableUsageMBeanProxy;

public class Test1111076Test extends BaseProfileTest {

    public static final String PROFILE_TABLE_NAME = "Test1111076ProfileTable";
    public static final String PROFILE_NAME = "Test1111076Profile";

    // Enum:
    private static boolean DEFAULT_MESSAGE = true;
    private static boolean NAMED_MESSAGE = false;

    /**
     * Perform the actual test.
     */

    public TCKTestResult run() throws Exception {
        result = new FutureResult(utils().getLog());

        try {
            createProfileTable(PROFILE_TABLE_NAME);
            createProfile(PROFILE_TABLE_NAME, PROFILE_NAME);
            createNamedParameterSet(PROFILE_TABLE_NAME);

            // Create a new activity.
            TCKResourceTestInterface resource = utils().getResourceInterface();
            TCKActivityID activityID = resource.createActivity(getClass().getName());

            // Request that the default usage parameter gets updated.
            String message = new String("Default");
            getLog().fine("About to fire default usage parameter update request to the SBB");
            resource.fireEvent(TCKResourceEventX.X1, message, activityID, null);

            // Request that the named usage parameter set gets updated.
            message = new String("Named");
            getLog().fine("About to fire named usage parameter update request to SBB");
            resource.fireEvent(TCKResourceEventX.X1, message, activityID, null);

            getLog().fine("Waiting for result");
            // Now wait for the result to come back.
            return result.waitForResult(5000);

        } finally {
            removeNamedParameterSet(PROFILE_TABLE_NAME);
            removeProfile(PROFILE_TABLE_NAME, PROFILE_NAME);
            removeProfileTable(PROFILE_TABLE_NAME);
        }

    }

    /**
     * Do all the pre-run configuration of the test.
     */

    public void setUp() throws Exception {
        super.setUp();

        setResourceListener(new TCKResourceListenerImpl());
    }

    //private resource listener implementation
    private class TCKResourceListenerImpl extends BaseTCKResourceListener {

        public synchronized void onSbbMessage(TCKSbbMessage message, TCKActivityID activity) throws RemoteException {
            utils().getLog().fine("Received message from SBB: " + message.getMessage());

            //unpack the transferred message - for no reason.
            HashMap map = (HashMap) message.getMessage();
            Integer id = (Integer) map.get(SbbBaseMessageConstants.ID);
            Boolean passed = (Boolean) map.get(SbbBaseMessageConstants.RESULT);
            String msgString = (String) map.get(SbbBaseMessageConstants.MSG);

            // Determine which semaphore to use (the test waits for message receipt on these).
            if (msgString.equals("Default")) {
                handleMessage(id, DEFAULT_MESSAGE);
                maybeFinished(DEFAULT_MESSAGE);
            } else if (msgString.equals("Named")) {
                handleMessage(id, NAMED_MESSAGE);
                maybeFinished(NAMED_MESSAGE);
            } else {
                String err = "Received malformed messages from SBB.";
                utils().getLog().error(err);
                result.setError(err);
            }
        }

        private void handleMessage(Integer id, boolean messageType) {
            long stats;
            try {
                // Check the usage parameters
                ObjectName profileTableUsageMBeanName = profileProxy.getProfileTableUsageMBean(PROFILE_TABLE_NAME);
                ProfileTableUsageMBeanProxy p = utils().getMBeanProxyFactory().createProfileTableUsageMBeanProxy(profileTableUsageMBeanName);

                ObjectName usageMBeanName = (messageType == DEFAULT_MESSAGE) ? p.getUsageMBean() : p.getUsageMBean(p.getUsageParameterSets()[0]); // can look up name in p.
                //stats = (Long)(utils().getMBeanFacade().invoke(usageMBeanName, "getFirstCount", new Object[] {false}, null));
                // That previous line could also be:
                UsageMBeanProxy u = new UsageMBeanProxyImpl(usageMBeanName, utils().getMBeanFacade());
                stats = u.getFirstCount(false);

                if (stats != 1) {
                    result.setFailed(id==null?1111076:id.intValue(), "Expected usage parameter to have value 1, but got "+stats);
                }
            } catch (Exception e) {
                utils().getLog().error(e);
                result.setError(e);
                return;
            }
        }

        private synchronized void maybeFinished(boolean messageType) {
            if (messageType == DEFAULT_MESSAGE)
                messageAReceived = true;
            else if (messageType == NAMED_MESSAGE)
                messageBReceived = true;

            if (messageAReceived && messageBReceived)
                result.setPassed();
        }

        public void onException(Exception e) throws RemoteException {
            utils().getLog().warning("Received exception from SBB.");
            utils().getLog().warning(e);
            result.setError(e);
        }

    }

    private boolean messageAReceived = false;
    private boolean messageBReceived = false;
    private FutureResult result;
}
