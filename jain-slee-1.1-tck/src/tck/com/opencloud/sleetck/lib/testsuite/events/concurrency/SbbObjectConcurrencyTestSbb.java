/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.events.concurrency;

import com.opencloud.sleetck.lib.sbbutils.BaseTCKSbb;
import com.opencloud.sleetck.lib.sbbutils.TCKSbbUtils;
import com.opencloud.sleetck.lib.resource.events.TCKResourceEventX;
import com.opencloud.sleetck.lib.resource.events.TCKResourceEventY;
import com.opencloud.sleetck.lib.resource.events.TCKResourceEvent;
import com.opencloud.sleetck.lib.TCKTestErrorException;
import javax.slee.ActivityContextInterface;
import javax.slee.facilities.Level;

/**
 * The Sbb class for the SbbObjectConcurrencyTest.
 * It receives events and sbb callbacks, and calls the test synchronously
 * with ACK messages that block as appropriate.
 * Calls to the test pass an int array argument in the following form: {code,sbbObjectID}
 */
public abstract class SbbObjectConcurrencyTestSbb extends BaseTCKSbb {

    public void onTCKResourceEventX1(TCKResourceEventX event, ActivityContextInterface aci) {
        createTraceSafe(Level.INFO,"Received TCKResourceEventX1");
        try {
            callTest(SbbObjectConcurrencyTestConstants.RECEIVED_X1); // this call will block
        } catch (Exception ex) {
            TCKSbbUtils.handleException(ex);
        }
        // trigger calls to sbbExceptionThrown and sbbRolledBack
        throw new RuntimeException("Thrown to cause invocations of sbbExceptionThrown and sbbRolledBack");
    }

    public void onTCKResourceEventY1(TCKResourceEventY event, ActivityContextInterface aci) {
        createTraceSafe(Level.INFO,"Received TCKResourceEventY1");
        try {
            callTest(SbbObjectConcurrencyTestConstants.RECEIVED_Y1);
        } catch (Exception ex) {
            TCKSbbUtils.handleException(ex);
        }
    }

    public void onTCKResourceEventY2(TCKResourceEventY event, ActivityContextInterface aci) {
        createTraceSafe(Level.INFO,"Received TCKResourceEventY2");
        try {
            callTest(SbbObjectConcurrencyTestConstants.RECEIVED_Y2);
        } catch (Exception ex) {
            TCKSbbUtils.handleException(ex);
        }
    }

    public void onTCKResourceEventY3(TCKResourceEventY event, ActivityContextInterface aci) {
        createTraceSafe(Level.INFO,"Received TCKResourceEventY3");
        try {
            callTest(SbbObjectConcurrencyTestConstants.RECEIVED_Y3);
        } catch (Exception ex) {
            TCKSbbUtils.handleException(ex);
        }
    }

    public void sbbExceptionThrown(Exception exception, Object event, ActivityContextInterface aci) {
        createTraceSafe(Level.INFO,"sbbExceptionThrown called");
        boolean expected = false; // we only expect to receive this call for the X1 event
        if(event instanceof TCKResourceEventX) {
            TCKResourceEventX asXEvent = (TCKResourceEventX)event;
            if(TCKResourceEventX.X1.equals(asXEvent.getEventTypeName())) {
                expected = true;
                try {
                    callTest(SbbObjectConcurrencyTestConstants.SBB_EXCEPTION_THROWN); // this call will block
                } catch(Exception e2) {
                    TCKSbbUtils.handleException(e2);
                }
            }
        }
        if(!expected) TCKSbbUtils.handleException(new TCKTestErrorException("sbbExceptionThrown() called. event="+event+";aci="+aci,exception));
    }

    public void sbbRolledBack(javax.slee.RolledBackContext context) {
        try {
            createTraceSafe(Level.INFO,"sbbRolledBack called");
            boolean handled = false; // we only expect to receive this call for the TCKResourceEvents
            if(context.getEvent() instanceof TCKResourceEvent) {
                TCKResourceEvent asTCKResourceEvent = (TCKResourceEvent) context.getEvent();
                int callCode = -1;
                if(TCKResourceEventX.X1.equals(asTCKResourceEvent.getEventTypeName())) {
                    callCode = SbbObjectConcurrencyTestConstants.SBB_ROLLED_BACK_X1;
                } else if(TCKResourceEventY.Y1.equals(asTCKResourceEvent.getEventTypeName())) {
                    callCode = SbbObjectConcurrencyTestConstants.SBB_ROLLED_BACK_Y1;
                } else if(TCKResourceEventY.Y2.equals(asTCKResourceEvent.getEventTypeName())) {
                    callCode = SbbObjectConcurrencyTestConstants.SBB_ROLLED_BACK_Y2;
                } else if(TCKResourceEventY.Y3.equals(asTCKResourceEvent.getEventTypeName())) {
                    callCode = SbbObjectConcurrencyTestConstants.SBB_ROLLED_BACK_Y3;
                }
                if(callCode != -1) {
                    handled = true;
                    callTest(callCode); // this call will block in the X1 case
                }
            }
            if(!handled)TCKSbbUtils.handleException(new TCKTestErrorException("Unexpected roll back. event="+context.getEvent()+";aci="+context.getActivityContextInterface()));
        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }
    }

    /**
     * Calls the test with the given message, and returns the result of the call
     */
    private Object callTest(int code) throws Exception {
        ensureSbbObjectID();
        int[] argument = {code,sbbObjectID};
        return TCKSbbUtils.getResourceInterface().callTest(argument);
    }

    /**
     * Ensures that the unique sbb ID is set.
     * If not set, this method asks the test for a unique ID.
     */
    private void ensureSbbObjectID() throws Exception {
        synchronized (sbbObjectIDLock) {
            if(sbbObjectID == -1) {
                int[] argument = {SbbObjectConcurrencyTestConstants.REQUEST_SBB_OID,0};
                Integer response = (Integer)TCKSbbUtils.getResourceInterface().callTest(argument);
                sbbObjectID = response.intValue();
            }
        }
    }

    private int sbbObjectID = -1; // the globally unique sbb object ID (unique within the scope of this test)
    private Object sbbObjectIDLock = new Object();

}
