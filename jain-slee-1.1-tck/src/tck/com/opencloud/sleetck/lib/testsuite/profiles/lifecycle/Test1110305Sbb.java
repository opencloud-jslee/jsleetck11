/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.profiles.lifecycle;

import javax.naming.Context;
import javax.slee.ActivityContextInterface;
import javax.slee.RolledBackContext;
import javax.slee.SbbContext;
import javax.slee.profile.ProfileFacility;
import javax.slee.profile.ProfileTable;

import com.opencloud.logging.StdErrLog;
import com.opencloud.sleetck.lib.profileutils.BaseMessageSender;
import com.opencloud.sleetck.lib.rautils.RMIObjectChannel;
import com.opencloud.sleetck.lib.rautils.TCKRAUtils;
import com.opencloud.sleetck.lib.resource.events.TCKResourceEventX;
import com.opencloud.sleetck.lib.sbbutils.BaseTCKSbb;
import com.opencloud.sleetck.lib.sbbutils.TCKSbbUtils;

public abstract class Test1110305Sbb extends BaseTCKSbb {

    public static final String PROFILE_TABLE_NAME = "Test1110305ProfileTable";
    public static final String BUSINESS_PROFILE = "BusinessProfile";
    public static final String MANAGEMENT_PROFILE = "ManagementProfile";
    public static final String LIFECYCLE_PROFILE = "LifecycleProfile";

    //Sbb lifecycle method. Acquire RMIObjectChannel output stream
    public void setSbbContext(SbbContext context) {
        super.setSbbContext(context);

        try {
            out = TCKRAUtils.lookupRMIObjectChannel();
            msgSender = new BaseMessageSender(out, new StdErrLog());
        }
        catch (Exception e) {
            context.getTracer(getSbbID().getName()).fine("An error occured creating an RMIObjectChannel:", e);
        }

    }

    public void onTCKResourceEventX1(TCKResourceEventX event, ActivityContextInterface aci) {
        try {
            Context env = TCKSbbUtils.getSbbEnvironment();
            ProfileFacility profileFacility = (ProfileFacility)env.lookup("slee/facilities/profile");
            msgSender.sendLogMsg("Obtained profileFacility object.");
            ProfileTable profileTable = profileFacility.getProfileTable(PROFILE_TABLE_NAME);
            msgSender.sendLogMsg("Obtained profileTable object.");

            //call business method on profile object which should throw a RuntimeException and
            //transition profile object into DOESNTEXIST state
            Test1110305ProfileLocal profileLocal = (Test1110305ProfileLocal) profileTable.find(BUSINESS_PROFILE);
            msgSender.sendLogMsg("Obtained profileLocal object.");

            try {
                profileLocal.business();
                msgSender.sendError("Exception should have occured as business method throws an unchecked exception.");
                return;
            }
            catch (Exception e) {
                msgSender.sendLogMsg("Exception occurred as expected: "+e);
            }
        } catch(Exception e) {
            msgSender.sendException(e);
        }
    }

    public void sbbRolledBack(RolledBackContext context) {
        //send ACK to the TCK test to indicate that the test completed successfully
        msgSender.sendSuccess(1110305, "Test completed as expected");
    }

    private RMIObjectChannel out;
    private BaseMessageSender msgSender;
}
