/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.sbb.abstractclass;

import com.opencloud.sleetck.lib.TCKTestErrorException;
import com.opencloud.sleetck.lib.resource.events.TCKResourceEventX;
import com.opencloud.sleetck.lib.sbbutils.BaseTCKSbb;
import com.opencloud.sleetck.lib.sbbutils.TCKSbbUtils;

import javax.slee.ActivityContextInterface;
import javax.slee.ChildRelation;
import javax.slee.CreateException;
import javax.slee.RolledBackContext;
import javax.slee.facilities.Level;
import java.util.HashMap;

public abstract class Test455Sbb extends BaseTCKSbb {

    // Initial event
    public void onTCKResourceEventX1(TCKResourceEventX ev, ActivityContextInterface aci) {
    }

    public void sbbPostCreate() throws CreateException {
        throw new NullPointerException("Test455NullPointerException");
    }

    public void sbbExceptionThrown(Exception exception, Object event, ActivityContextInterface aci) {
        if (exception instanceof NullPointerException
                && exception.getMessage().equals("Test455NullPointerException")) {
            HashMap map = new HashMap();
            map.put("Result", new Boolean(true));
            map.put("Message", "Ok");
            try {
                TCKSbbUtils.getResourceInterface().sendSbbMessage(map);
            } catch (Exception e) {
                TCKSbbUtils.handleException(e);
            }
        } else {
            TCKSbbUtils.handleException(new TCKTestErrorException("sbbExceptionThrown() called with unexpected Exception. event="+event+";aci="+aci,exception));
        }
    }

    // ChildRelation method for the Child SBB.
    public abstract ChildRelation getChildRelation();

    // Override sbbRolledBack to write a trace message rather than throw an Exception, as we expect to receive the call
    public void sbbRolledBack(RolledBackContext context) {
        createTraceSafe(Level.INFO,"Received expected sbbRolledBack() call");
    }   

}
