/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.facilities.tracefacility;

import com.opencloud.sleetck.lib.sbbutils.BaseTCKSbb;
import com.opencloud.sleetck.lib.sbbutils.TCKSbbUtils;
import com.opencloud.sleetck.lib.resource.events.TCKResourceEventX;

import javax.slee.ActivityContextInterface;
import javax.slee.facilities.Level;

/**
 * 
 */
public abstract class Test4484Sbb extends BaseTCKSbb {
    public void onTCKResourceEventX1(TCKResourceEventX event, ActivityContextInterface aci) {
        try {
            TCKSbbUtils.createTrace(getSbbID(), Level.INFO, "onTCKResourceEventX1- enter", null);
            getSbbContext().setRollbackOnly();
            TCKSbbUtils.createTrace(getSbbID(), Level.INFO, "onTCKResourceEventX1- exit", null);
        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }
    }

    public void sbbRolledBack(javax.slee.RolledBackContext context) {}

}
