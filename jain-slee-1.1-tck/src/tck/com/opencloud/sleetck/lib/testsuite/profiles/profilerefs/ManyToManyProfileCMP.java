/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.profiles.profilerefs;

import javax.slee.profile.ProfileID;

public interface ManyToManyProfileCMP {

    public String getName();
    public void setName(String name);

    public ProfileID getSingleProfile();
    public void setSingleProfile(ProfileID profile);

    public ProfileID[] getProfileArray();
    public void setProfileArray(ProfileID[] profiles);

}