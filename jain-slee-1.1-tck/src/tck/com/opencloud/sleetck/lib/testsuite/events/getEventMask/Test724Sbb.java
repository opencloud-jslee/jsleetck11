/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.events.getEventMask;

import java.util.HashMap;

import javax.slee.ActivityContextInterface;
import javax.slee.NotAttachedException;
import javax.slee.nullactivity.NullActivity;
import javax.slee.nullactivity.NullActivityContextInterfaceFactory;
import javax.slee.nullactivity.NullActivityFactory;

import com.opencloud.sleetck.lib.resource.events.TCKResourceEventX;
import com.opencloud.sleetck.lib.sbbutils.BaseTCKSbb;
import com.opencloud.sleetck.lib.sbbutils.TCKSbbUtils;

public abstract class Test724Sbb extends BaseTCKSbb {

    // Initial event method.
    public void onTCKResourceEventX1(TCKResourceEventX ev, ActivityContextInterface aci) {

        try {

            HashMap map = new HashMap();

            // Create a NullActivity and get its ActivityContextInterface
            NullActivityFactory factory = (NullActivityFactory) TCKSbbUtils.getSbbEnvironment().lookup("slee/nullactivity/factory");
            NullActivity nullActivity = factory.createNullActivity();

            NullActivityContextInterfaceFactory aciFactory = (NullActivityContextInterfaceFactory) TCKSbbUtils.getSbbEnvironment().lookup("slee/nullactivity/activitycontextinterfacefactory");
            ActivityContextInterface nullACI = aciFactory.getActivityContextInterface(nullActivity);

            // Do _NOT_ attach this SBB to the NullActivity.

            boolean passed = false;
            try {
                // Call SbbContext.getEventMask(ACI that isn't attached to this
                // SBB)
                getSbbContext().getEventMask(nullACI);
            } catch (NotAttachedException e) {
                passed = true;
            }

            if (passed) {
                passed = false;
                try {
                    map.put("ID", new Integer(3264));
                    getSbbContext().maskEvent(null, nullACI);
                } catch (NotAttachedException e) {
                    passed = true;
                }
            } else {
                map.put("ID", new Integer(724));
            }

            // Clean up -- end the NullActivity
            nullActivity.endActivity();

            if (passed) {
                map.put("Result", new Boolean(true));
                map.put("Message", "Ok");
            } else {
                map.put("Result", new Boolean(false));
                map.put("Message","Should have received javax.slee.NotAttachedException");
            }

            TCKSbbUtils.getResourceInterface().sendSbbMessage(map);

        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }
    }
}
