/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.javax.slee.usage.UsageThresholdFilter;

import com.opencloud.sleetck.lib.AbstractSleeTCKTest;
import com.opencloud.sleetck.lib.OperationTimedOutException;
import com.opencloud.sleetck.lib.TCKTestFailureException;
import com.opencloud.sleetck.lib.TCKTestResult;
import com.opencloud.sleetck.lib.resource.TCKActivityID;
import com.opencloud.sleetck.lib.resource.events.TCKResourceEventX;
import com.opencloud.sleetck.lib.resource.events.TCKResourceEventY;
import com.opencloud.sleetck.lib.testsuite.usage.common.GenericUsageSbbInstructions;
import com.opencloud.sleetck.lib.testsuite.usage.common.UsageMBeanLookup;
import com.opencloud.sleetck.lib.testutils.QueuingNotificationListener;
import com.opencloud.sleetck.lib.testutils.QueuingResourceListener;

import javax.management.Notification;
import javax.slee.SbbID;
import javax.slee.ServiceID;
import javax.slee.usage.UsageNotification;
import javax.slee.usage.UsageThresholdFilter;
import java.util.LinkedList;
import java.util.List;
import java.util.Iterator;

/**
 * Tests assertions related to the UsageThresholdFilter.
 * Tries to create a UsageThresholdFilter will null arguments.
 * Checks that notifications which are not UsageNoficiations are supressed,
 * by registering a listener and filter with the TraceNoficiationMBean
 * and firing trace messages.
 * Checks that noficiations with matching Service id, Sbb id and parameter name
 * are received, and that notifications without matching Service id, Sbb id or
 * parameter name are supressed.
 * Checks that notifications which represent a cross in the specified threshold are
 * delivered, and that all other notifications are blocked.
 */
public class UsageThresholdFilterTest extends AbstractSleeTCKTest {

    private static final String CHOSEN_PARAMETER_NAME = "timeBetweenNewConnections";

    private static final long   THRESHOLD       = 10;
    private static final long[] SAMPLE_SET      = {
           // test in the upwards direction
           8,9,10,9,8,      // reach but don't pass threshold -- expect {}
           9,10,11,         // reach and pass                 -- expect {11}
           9,               // (cross back down)              -- expect {9}
           10,10,11,11,     // reach, stay at, cross threshold-- expect {11}
           9,               // (cross back down)              -- expect {9}
           11,              // jump over threshold            -- expect {11}
           // repeat in the opposite direction
           12,11,10,11,12,  // reach but don't pass threshold -- expect {}
           11,10,9,         // reach and pass                 -- expect {9}
           11,              // (cross back up)                -- expect {11}
           10,10,9,9,       // reach, stay at, cross threshold-- expect {9}
           11,              // (cross back up)                -- expect {11}
           9};              // jump over threshold            -- expect {9}
    private static final long[] EXPECT_TO_PASS  = {                     // as above
                                                                        11,
                                                                        9,
                                                                        11,
                                                                        9,
                                                                        11,

                                                                        9,
                                                                        11,
                                                                        9,
                                                                        11,
                                                                        9};

    public TCKTestResult run() throws Exception {

        checkConstructorExceptions();

        notificationListener = new QueuingNotificationListener(utils());
        usageThresholdFilter = new UsageThresholdFilter(chosenServiceID,chosenSbbID,CHOSEN_PARAMETER_NAME,THRESHOLD);

        // register the listener and filter with the four SBBs, to check sbb id/service id filtering
        UsageMBeanLookup[] usageMBeanLookups = {usageMBeanLookupChildA,usageMBeanLookupChildB,
                                                usageMBeanLookupParentA,usageMBeanLookupParentB};
        for (int i = 0; i < usageMBeanLookups.length; i++) {
            UsageMBeanLookup usageMBeanLookup = usageMBeanLookups[i];
            usageMBeanLookup.getUnnamedSbbUsageMBeanProxy().addNotificationListener(
                    notificationListener,usageThresholdFilter,null);
        }
        // register the listener and filter with the trace MBean, to check filtering on notification type
        utils().getTraceMBeanProxy().addNotificationListener(notificationListener,usageThresholdFilter,null);

        // fire a set of instructions to all four SBBs
        GenericUsageSbbInstructions instructions = new GenericUsageSbbInstructions(null);
        instructions.addFirstCountIncrement(1);
        instructions.addSecondCountIncrement(2);
        instructions.addTimeBetweenErrorsSample(3);
        for (int i = 0; i < SAMPLE_SET.length; i++) {
            instructions.addTimeBetweenNewConnectionsSamples(SAMPLE_SET[i]);
        }
        TCKActivityID activityID = utils().getResourceInterface().createActivity("UsageThresholdFilterTest-Activity");
        utils().getResourceInterface().fireEvent(TCKResourceEventX.X1,instructions.toExported(),activityID,null);
        utils().getResourceInterface().fireEvent(TCKResourceEventX.X2,instructions.toExported(),activityID,null);
        utils().getResourceInterface().fireEvent(TCKResourceEventY.Y1,instructions.toExported(),activityID,null);

        getLog().info("Waiting for replies from the SBBs");
        resourceListener.nextMessage();
        resourceListener.nextMessage();

        getLog().info("Waiting for usage notifications");
        for (int i = 0; i < EXPECT_TO_PASS.length; i++) {
            auditNotification(getNextNotificationOrFail());
        }

        // we don't expect any more notifications at this stage -- cause and wait for a new valid one
        // so that we when we receive it we know that there weren't any outstaning invalid notifications
        getLog().info("Sending second request to SBB A");
        instructions = new GenericUsageSbbInstructions(null);
        instructions.addTimeBetweenNewConnectionsSamples(20); // cross the threshold: 9 -> 20
        expectedValues.add(new Long(20));
        utils().getResourceInterface().fireEvent(TCKResourceEventX.X2,instructions.toExported(),activityID,null);
        getLog().info("Waiting for reply from the SBB");
        resourceListener.nextMessage();
        getLog().info("Waiting for usage notification");

        auditNotification(getNextNotificationOrFail());

        return TCKTestResult.passed();
    }

    private void checkConstructorExceptions() throws TCKTestFailureException {
        try {
            new UsageThresholdFilter(chosenServiceID,chosenSbbID,null,THRESHOLD);
            throw new TCKTestFailureException(4230,"Failed to fire a NullPointerException while trying to create a UsageThresholdFilter with"+
                    "a null parameter name");
        } catch (NullPointerException e) {
            getLog().info("Caught excpected NullPointerException while trying to create a UsageThresholdFilter with "+
                    "a null parameter name");
        }
        try {
            new UsageThresholdFilter(chosenServiceID,null,CHOSEN_PARAMETER_NAME,THRESHOLD);
            throw new TCKTestFailureException(4230,"Failed to fire a NullPointerException while trying to create a UsageThresholdFilter with"+
                    "a null sbb id");
        } catch (NullPointerException e) {
            getLog().info("Caught excpected NullPointerException which trying to create a UsageThresholdFilter with "+
                    "a null sbb id");
        }
        try {
            new UsageThresholdFilter(null,chosenSbbID,CHOSEN_PARAMETER_NAME,THRESHOLD);
            throw new TCKTestFailureException(4230,"Failed to fire a NullPointerException while trying to create a UsageThresholdFilter with"+
                    "a null service id");
        } catch (NullPointerException e) {
            getLog().info("Caught excpected NullPointerException which trying to create a UsageThresholdFilter with "+
                    "a null service id");
        }
    }

    private void auditNotification(Notification notification) throws Exception {
        getLog().info("Auditing notification: "+notification);
        if(!(notification instanceof UsageNotification)) throw new TCKTestFailureException(4228,
                "UsageThresholdFilter allowed a notification to pass which was not a UsageNotification: "+notification);
        UsageNotification usageNotification = (UsageNotification)notification;
        // service ID
        if(!usageNotification.getService().equals(chosenServiceID)) throw new TCKTestFailureException(4227,
                "UsageThresholdFilter allowed a notification to pass with a non-matching ServiceID. Chosen ServiceID:"+chosenServiceID+
                ", found ServiceID:"+usageNotification.getService());
        // sbb ID
        if(!usageNotification.getSbb().equals(chosenSbbID)) throw new TCKTestFailureException(4227,
                "UsageThresholdFilter allowed a notification to pass with a non-matching SbbID. Chosen SbbID:"+chosenSbbID+
                ", found SbbID:"+usageNotification.getSbb());
        // parameter name
        if(!usageNotification.getUsageParameterName().equals(CHOSEN_PARAMETER_NAME)) throw new TCKTestFailureException(4227,
                "UsageThresholdFilter allowed a notification to pass with a non-matching parameter name. Chosen parameter name:"+CHOSEN_PARAMETER_NAME+
                ", found parameter name:"+usageNotification.getUsageParameterName());

        // check that this is the next notification we expect, i.e. that we haven't missed any expected notifications
        long nextExpectedValue = ((Long)expectedValues.get(notificationsReceived-1)).longValue();
        if(nextExpectedValue != usageNotification.getValue()) {
            throw new TCKTestFailureException(4225,
                "Missed expected usage notification with value "+nextExpectedValue+
                ", which appears to have been illegally filtered by the UsageThresholdFilter. "+
                "Expected values:"+formatLongs(expectedValues)+
                "; number of received expected values:"+(notificationsReceived-1)+"; value of this notification:"+
                usageNotification.getValue()+". Unfiltered sample set for first updates:"+formatLongs(SAMPLE_SET));
        }
    }

    private Notification getNextNotificationOrFail() throws TCKTestFailureException {
        try {
            Notification rNotification = notificationListener.nextNotification();
            getLog().info("Received notification: "+rNotification);
            notificationsReceived++;
            return rNotification;
        } catch (OperationTimedOutException e) {
            throw new TCKTestFailureException(4225,"Timed out while waiting for expected UsageNotification, "+
                    "indicating that the notification was filtered illegally by the UsageThresholdFilter. Notifications received so far: "+
                    notificationsReceived);
        }
    }

    private String formatLongs(List listOfLongs) {
        if(listOfLongs.isEmpty()) return "(empty set)";
        Iterator iter = listOfLongs.iterator();
        StringBuffer buf = new StringBuffer("("+iter.next());
        while(iter.hasNext()) {
            buf.append(","+iter.next());
        }
        return buf.append(")").toString();
    }

    private String formatLongs(long[] listOfLongs) {
        if(listOfLongs.length == 0) return "(empty set)";
        StringBuffer buf = new StringBuffer("("+listOfLongs[0]);
        for (int i = 1; i < listOfLongs.length; i++) {
            buf.append(","+listOfLongs[i]);
        }
        return buf.append(")").toString();
    }

    public void setUp() throws Exception {
        super.setUp();
        notificationsReceived = 0;
        expectedValues = new LinkedList();
        for (int i = 0; i < EXPECT_TO_PASS.length; i++) {
            expectedValues.add(new Long(EXPECT_TO_PASS[i]));
        }
        usageMBeanLookupParentA = new UsageMBeanLookup("UsageFilterTestsServiceA","UsageFilterTestsParentSbbA",utils());
        usageMBeanLookupChildA  = new UsageMBeanLookup("UsageFilterTestsServiceA","UsageFilterTestsChildSbb",utils());
        usageMBeanLookupParentB = new UsageMBeanLookup("UsageFilterTestsServiceB","UsageFilterTestsParentSbbB",utils());
        usageMBeanLookupChildB  = new UsageMBeanLookup("UsageFilterTestsServiceB","UsageFilterTestsChildSbb",utils());
        chosenSbbID = usageMBeanLookupChildA.getSbbID();
        chosenServiceID = usageMBeanLookupChildA.getServiceID();
        setResourceListener(resourceListener = new QueuingResourceListener(utils()));
    }

    public void tearDown() throws Exception {
        UsageMBeanLookup[] usageMBeanLookups = {usageMBeanLookupChildA,usageMBeanLookupChildB,
                                                usageMBeanLookupParentA,usageMBeanLookupParentB};
        if(notificationListener != null) {
            for (int i = 0; i < usageMBeanLookups.length; i++) {
                UsageMBeanLookup usageMBeanLookup = usageMBeanLookups[i];
                try {
                    usageMBeanLookup.getUnnamedSbbUsageMBeanProxy().removeNotificationListener(notificationListener);
                } catch (Exception e) { getLog().warning(e); }
            }
            try {
                utils().getTraceMBeanProxy().removeNotificationListener(notificationListener);
            } catch (Exception e) { getLog().warning(e); }
        }
        for (int i = 0; i < usageMBeanLookups.length; i++) {
            UsageMBeanLookup usageMBeanLookup = usageMBeanLookups[i];
            if(usageMBeanLookup != null) usageMBeanLookup.closeAllMBeans();
        }
        super.tearDown();
    }

    private QueuingResourceListener resourceListener;
    private UsageMBeanLookup usageMBeanLookupParentA;
    private UsageMBeanLookup usageMBeanLookupChildA;
    private UsageMBeanLookup usageMBeanLookupParentB;
    private UsageMBeanLookup usageMBeanLookupChildB;
    private QueuingNotificationListener notificationListener;
    private UsageThresholdFilter usageThresholdFilter;
    private int notificationsReceived;
    private SbbID chosenSbbID;
    private ServiceID chosenServiceID;
    // a list representing the notifications we expect to pass
    private LinkedList expectedValues;

}
