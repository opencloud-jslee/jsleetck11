/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.management.NotificationSource;

import javax.slee.ActivityContextInterface;
import javax.slee.facilities.TraceLevel;
import javax.slee.facilities.Tracer;

import com.opencloud.sleetck.lib.resource.events.TCKResourceEventX;
import com.opencloud.sleetck.lib.sbbutils.BaseTCKSbb;
import com.opencloud.sleetck.lib.sbbutils.TCKSbbUtils;

/*
 * AssertionID (1114380):
 * An SBB notification source is a component installed in the SLEE, and a Trace
 * notification is generated and broadcast to their respective MBean notification
 * listeners.
 *
 */

public abstract class Test1114380Sbb extends BaseTCKSbb {
    public static final String TRACE_MESSAGE_SEVERE = "SEVERE:Test1114380TraceMessage";
    public static final String TRACE_MESSAGE_WARNING = "WARNING:Test1114380TraceMessage";
    public static final String TRACE_MESSAGE_INFO = "INFO:Test1114380TraceMessage";
    public static final String TRACE_MESSAGE_CONFIG = "CONFIG:Test1114380TraceMessage";
    public static final String TRACE_MESSAGE_FINE = "FINE:Test1114380TraceMessage";
    public static final String TRACE_MESSAGE_FINER = "FINER:Test1114380TraceMessage";
    public static final String TRACE_MESSAGE_FINEST = "FINEST:Test1114380TraceMessage";

    // Initial event
    public void onTCKResourceEventX1(TCKResourceEventX event, ActivityContextInterface aci) {

        Tracer tracer = null;
        try {
            tracer = getSbbContext().getTracer("com.foo");
            tracer.trace(TraceLevel.INFO, TRACE_MESSAGE_INFO);
        }catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }

    }

    private String testName = "Test1114380Test";


}
