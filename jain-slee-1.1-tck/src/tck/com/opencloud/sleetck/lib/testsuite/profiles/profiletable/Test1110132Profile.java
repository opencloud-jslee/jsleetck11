/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.profiles.profiletable;

import javax.slee.Address;
import javax.slee.AddressPlan;
import javax.slee.CreateException;
import javax.slee.profile.Profile;
import javax.slee.profile.ProfileContext;
import javax.slee.profile.ProfileVerificationException;


public abstract class Test1110132Profile implements Test1110132ProfileCMP, Profile  {

    public static final String CREATE_EXCEPTION_PROFILE="Test1110132CreateExceptionProfile";

    public void setProfileContext(ProfileContext context) {
        this.context = context;
    }

    public void unsetProfileContext() {
    }

    public void profileInitialize() {
        setStringValue("0");
        setIntValue(0);
        setAddress(new Address(AddressPlan.SLEE_PROFILE,"XY/0"));
    }

    public void profilePostCreate() throws CreateException {
        String name = context.getProfileName();

        if (name!=null && name.equals(CREATE_EXCEPTION_PROFILE)) {
            throw new CreateException("Testexception raised.");
        }
    }

    public void profileActivate() {
    }

    public void profilePassivate() {
    }

    public void profileLoad() {
    }

    public void profileStore() {
    }

    public void profileRemove() {
    }

    public void profileVerify() throws ProfileVerificationException {
    }

    private ProfileContext context;

}
