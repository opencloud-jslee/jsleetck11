/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.resource.context;

import java.util.HashMap;

import com.opencloud.sleetck.lib.TCKTestErrorException;
import com.opencloud.sleetck.lib.TCKTestFailureException;
import com.opencloud.sleetck.lib.TCKTestResult;
import com.opencloud.sleetck.lib.testsuite.resource.BaseResourceTest;
import com.opencloud.sleetck.lib.testsuite.resource.RAMethods;

/**
 * Test to ensure that getDefaultUsageParameterSet() is returning the correct
 * result.
 * <p>
 * Test assertion ID: 1115194, 1115195
 */
public class Test1115194Test extends BaseResourceTest {

    private static final int ASSERTION_ID = 1115194;

    public TCKTestResult run() throws Exception {
        HashMap resultmap = sendMessage(RAMethods.getDefaultUsageParameterSet);
        if (resultmap == null)
            throw new TCKTestFailureException(ASSERTION_ID, "Test timed out while waiting for response to getDefaultUsageParameterSet()");

        // Check valid DefaultUsageParameterSet
        Object result = resultmap.get("result1");
        if (result instanceof Exception)
            throw new TCKTestFailureException(ASSERTION_ID, "Unexpected exception thrown while invoking getDefaultUsageParameterSet()", (Exception) result);
        if (result instanceof String)
            throw new TCKTestFailureException(ASSERTION_ID, "An error occured while invoking getDefaultUsageParameterSet(): " + result);
        if (!Boolean.TRUE.equals(result))
            throw new TCKTestErrorException("Unexpected result received while invoking getDefaultUsageParameterSet(): " + result);

        result = resultmap.get("result2");
        if (result instanceof Exception)
            throw new TCKTestFailureException(1115195,
                    "An exception was thrown while trying to typecast the UsageParamaterSet returned by ResourceAdaptorContext.getDefaultUsageParameterSet()",
                    (Exception) result);
        if (!Boolean.TRUE.equals(result))
            throw new TCKTestErrorException(
                    "Unexpected result received while trying to typecast the UsageParamaterSet returned by getDefaultUsageParameterSet(): " + result);

        return TCKTestResult.passed();
    }
}
