/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.resource.endpoint;

import java.util.HashMap;

import com.opencloud.sleetck.lib.TCKTestErrorException;
import com.opencloud.sleetck.lib.TCKTestFailureException;
import com.opencloud.sleetck.lib.TCKTestResult;
import com.opencloud.sleetck.lib.testsuite.resource.BaseResourceTest;
import com.opencloud.sleetck.lib.testsuite.resource.RAMethods;

/**
 * Test to ensure that startActivity() is throwing correct exceptions.
 * <p>
 * Test assertion: 1115247, 1115249
 */
public class Test1115247Test extends BaseResourceTest {

    public TCKTestResult run() throws Exception {

        HashMap results = sendMessage(RAMethods.startActivity, new Integer(1115247));
        if (results == null)
            throw new TCKTestErrorException("Test timed out while waiting for response from test component");

        // NullPointerException check 1 (null, null)
        Object result1 = results.get("result1");
        if (result1 instanceof Exception)
            throw new TCKTestFailureException(1115247, "Unexpected exception thrown during test:", (Exception) result1);
        if (Boolean.FALSE.equals(result1))
            throw new TCKTestFailureException(1115247, "startActivity() failed to throw a NullPointerException with null handle and activity object");
        if (!Boolean.TRUE.equals(result1))
            throw new TCKTestErrorException("Unexpected result received from test component: " + result1);

        // NullPointerException check 2 (null, new Object())
        Object result2 = results.get("result2");
        if (result2 instanceof Exception)
            throw new TCKTestFailureException(1115247, "Unexpected exception thrown during test:", (Exception) result2);
        if (Boolean.FALSE.equals(result2))
            throw new TCKTestFailureException(1115247, "startActivity() failed to throw a NullPointerException with null handle");
        if (!Boolean.TRUE.equals(result2))
            throw new TCKTestErrorException("Unexpected result received from test component: " + result2);

        // NullPointerException check 3 (handle, null))
        Object result3 = results.get("result3");
        if (result3 instanceof Exception)
            throw new TCKTestFailureException(1115247, "Unexpected exception thrown during test:", (Exception) result3);
        if (Boolean.FALSE.equals(result3))
            throw new TCKTestFailureException(1115247, "startActivity() failed to throw a NullPointerException with null activity object");
        if (!Boolean.TRUE.equals(result3))
            throw new TCKTestErrorException("Unexpected result received from test component: " + result3);

        // ActivityAlreadyExistsException
        Object result4 = results.get("result4");
        if (result4 instanceof Exception)
            throw new TCKTestFailureException(1115247, "Unexpected exception thrown during test:", (Exception) result4);
        if (Boolean.FALSE.equals(result4))
            throw new TCKTestFailureException(1115247, "startActivity() failed to throw a ActivityAlreadyExistsException");
        if (!Boolean.TRUE.equals(result4))
            throw new TCKTestErrorException("Unexpected result received from test component: " + result4);

        return TCKTestResult.passed();
    }
}
