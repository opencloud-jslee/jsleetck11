/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.usage.SbbUsageMBean;

import com.opencloud.sleetck.lib.TCKTestResult;
import com.opencloud.sleetck.lib.resource.TCKActivityID;
import com.opencloud.sleetck.lib.resource.events.TCKResourceEventX;
import com.opencloud.sleetck.lib.testsuite.usage.common.GenericUsageMBeanLookup;
import com.opencloud.sleetck.lib.testsuite.usage.common.GenericUsageMBeanProxy;
import com.opencloud.sleetck.lib.testsuite.usage.common.GenericUsageSbbInstructions;
import com.opencloud.sleetck.lib.testsuite.usage.common.GenericUsageTest;
import com.opencloud.sleetck.lib.testutils.QueuingNotificationListener;
import com.opencloud.sleetck.lib.testutils.QueuingResourceListener;

import javax.slee.usage.SampleStatistics;

/**
 * Makes updates to usage parameters, then accesses them with false
 * reset parameter.
 * Calls SbbUsageMBean.resetAllUsageParameters(), then checks that the
 * accessors return reset values.
 */
public class Test2342Test extends GenericUsageTest {

    private static final int ASSERTION_ID = 2342;

    public TCKTestResult run() throws Exception {

        TCKActivityID activityID = utils().getResourceInterface().createActivity("Test2326and2370Test-Activity");

        GenericUsageSbbInstructions instructions = new GenericUsageSbbInstructions(null);
        instructions.addFirstCountIncrement(1);
        instructions.addTimeBetweenNewConnectionsSamples(1);

        getLog().fine("sending update requests");
        sendInstructionsAndWait(activityID,instructions);

        GenericUsageMBeanProxy sbbUsageMBeanProxy = getGenericUsageMBeanLookup().getUnnamedGenericUsageMBeanProxy();

        // firstCount

        getLog().info("first check: check that accessor returns non-reset value");
        long firstCount = sbbUsageMBeanProxy.getFirstCount(false);
        if(firstCount == 0) return TCKTestResult.error(
                "Accessor method for counter type parameter firstCount returned 0, after updates had been made. "+
                "This may indicate that the counter has been illegally reset, or may indicate some other error "+
                "preventing the counter being set. The reset argument was false.");
        if(firstCount != 1) return TCKTestResult.error("Counter-type parameter firstCount was set to an unexpected value; "+
                "should be 1. Actual value:"+firstCount);

        // timeBetweenNewConnections

        getLog().info("first check: check that accessor returns non-reset value");
        SampleStatistics sampleStatistics = sbbUsageMBeanProxy.getTimeBetweenNewConnections(false);
        if(sampleStatistics.getSampleCount() == 0) return TCKTestResult.error(
                "Accessor method for sample type parameter timeBetweenNewConnections returned 0 sample count, "+
                "after updates had been made. This may indicate that the parameter has been illegally reset, or may indicate some other error "+
                "preventing the parameter being updated. The reset argument was false.");
        if(sampleStatistics.getSampleCount() != 1) return TCKTestResult.error("Sample-type parameter timeBetweenNewConnections "+
                " was had an unexpected sample count; should be 1. Actual value:"+sampleStatistics.getSampleCount());

        // resetAllUsageParameters()
        getLog().info("Calling resetAllUsageParameters()");
        sbbUsageMBeanProxy.resetAllUsageParameters();

        // firstCount

        getLog().info("check that accessor returns the reset value after calling resetAllUsageParameters()");
        firstCount = sbbUsageMBeanProxy.getFirstCount(false);
        if(firstCount == 2) return TCKTestResult.failed(ASSERTION_ID,
                "Accessor method for counter type parameter firstCount returned 2, "+
                "after calling resetAllUsageParameters(), with no subsequent updates. "+
                "The counter should have been reset to 0.");
        if(firstCount != 0) return TCKTestResult.error("Counter-type parameter firstCount was set to an unexpected value; "+
                "should be 0. Actual value:"+firstCount);

        // timeBetweenNewConnections

        getLog().info("check that accessor returns the reset value after calling resetAllUsageParameters()");
        sampleStatistics = sbbUsageMBeanProxy.getTimeBetweenNewConnections(false);
        if(sampleStatistics.getSampleCount() == 2) return TCKTestResult.failed(ASSERTION_ID,
                "Accessor method for sample type parameter timeBetweenNewConnections returned sample count 2, "+
                "after calling resetAllUsageParameters(), with no subsequent updates. "+
                "The parameter should have been reset.");
        if(sampleStatistics.getSampleCount() != 0) return TCKTestResult.error("Sample-type parameter timeBetweenNewConnections had an unexpected sample count; "+
                "should be 0. Actual value:"+sampleStatistics.getSampleCount());

        getLog().info("parameter checks ok");
        return TCKTestResult.passed();
    }

    private void sendInstructionsAndWait(TCKActivityID activityID, GenericUsageSbbInstructions instructions) throws Exception {
        getLog().fine("firing event to Sbb");
        utils().getResourceInterface().fireEvent(TCKResourceEventX.X1,instructions.toExported(),activityID,null);

        getLog().fine("waiting for replies");
        resourceListener.nextMessage();
        getLog().info("received replies");

        getLog().fine("waiting for usage notifications");
        for (int i = 0; i < instructions.getTotalUpdates(); i++) {
              notificationListener.nextNotification();
        }
        getLog().info("received all "+(instructions.getTotalUpdates())+" usage notifications");
    }

    public void setUp() throws Exception {
        super.setUp();
        setResourceListener(resourceListener = new QueuingResourceListener(utils()));
        mBeanLookup = getGenericUsageMBeanLookup();
        notificationListener = new QueuingNotificationListener(utils());
        mBeanLookup.getUnnamedGenericUsageMBeanProxy().addNotificationListener(notificationListener,null,null);
    }

    public void tearDown() throws Exception {
        if(mBeanLookup != null) {
            if(notificationListener != null)mBeanLookup.getUnnamedGenericUsageMBeanProxy().removeNotificationListener(notificationListener);
        }
        super.tearDown();
    }

    private GenericUsageMBeanLookup mBeanLookup;
    private QueuingResourceListener resourceListener;
    private QueuingNotificationListener notificationListener;

}
