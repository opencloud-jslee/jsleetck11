/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.transactions;

import java.rmi.RemoteException;
import java.util.HashMap;

import com.opencloud.sleetck.lib.AbstractSleeTCKTest;
import com.opencloud.sleetck.lib.TCKTestResult;
import com.opencloud.sleetck.lib.resource.TCKActivityID;
import com.opencloud.sleetck.lib.resource.TCKSbbMessage;
import com.opencloud.sleetck.lib.resource.events.TCKResourceEventX;
import com.opencloud.sleetck.lib.resource.testapi.TCKResourceListener;
import com.opencloud.sleetck.lib.resource.testapi.TCKResourceTestInterface;
import com.opencloud.sleetck.lib.testutils.BaseTCKResourceListener;
import com.opencloud.sleetck.lib.testutils.FutureResult;

public class Test905Test extends AbstractSleeTCKTest {

    private static final String SERVICE_DU_PATH_PARAM = "DUPath";
    private static final int TEST_ID = 905;

    /**
     * Perform the actual test.
     */
    public TCKTestResult run() throws Exception {
        result = new FutureResult(getLog());

        TCKResourceTestInterface resource = utils().getResourceInterface();
        TCKActivityID activityID = resource.createActivity("Test905InitialActivity");
        resource.fireEvent(TCKResourceEventX.X1, TCKResourceEventX.X1, activityID, null);

        return result.waitForResultOrFail(utils().getTestTimeout(), "Timeout waiting for test result.", TEST_ID);
    }

    /**
     * Do all the pre-run configuration of the test.
     */
    public void setUp() throws Exception {

        resourceListener = new TCKResourceListenerImpl();
        setResourceListener(resourceListener);

        setupService(SERVICE_DU_PATH_PARAM);
    }

    private class TCKResourceListenerImpl extends BaseTCKResourceListener {
        public synchronized void onSbbMessage(TCKSbbMessage message, TCKActivityID calledActivity) throws RemoteException {

            HashMap map = (HashMap) message.getMessage();
            String type = (String) map.get("Type");

            getLog().fine("Received message " + type);

            if (type.equals("ExceptionThrown")) {
                boolean data = ((Boolean) map.get("Data")).booleanValue();
                exceptionThrown = data;
                return;
            }

            if (type.equals("RolledBack")) {
                boolean data = ((Boolean) map.get("Data")).booleanValue();
                rolledBack = data;
                return;
            }

            if (type.equals("TXN")) {
                txnID = map.get("Data");
                return;
            }

            if (type.equals("TXN2")) {
                txnID2 = map.get("Data");
                return;
            }

            // Tally up the results and return test/fail as appropriate.
            if (type.equals("Result")) {

                if (!exceptionThrown) {
                    result.setFailed(906, "sbbExceptionThrown() method was not thrown.");
                    return;
                }

                if (!rolledBack) {
                    result.setFailed(908, "The transaction that RuntimeException was thrown in was not rolled back.");
                    return;
                }

                if (txnID == null) { // Some other test will have failed prior to this one.
                    result.setError("The child event handler method was not invoked within a transaction.");
                    return;
                }

                if (txnID2 == null) {
                    result.setFailed(909, "The sbbRolledBack() method was not invoked within a transaction.");
                    return;
                }

                if (txnID == txnID2) {
                    result.setFailed(909, "The sbbRolledBack() method was not invoked in a separate transaction.");
                    return;
                }

                result.setPassed();

                return;
            }

            result.setError("Received unexpected message type from SBB: " + type);
        }

        public void onException(Exception e) throws RemoteException {
            getLog().warning("Received exception from SBB");
            getLog().warning(e);
            result.setError(e);
        }
    }

    // Variables sent from the SBB during the test procedure.
    private boolean exceptionThrown = false;
    private boolean rolledBack = false;
    private Object txnID = null;
    private Object txnID2 = null;

    // General test variables.
    private TCKResourceListener resourceListener;
    private FutureResult result;
}
