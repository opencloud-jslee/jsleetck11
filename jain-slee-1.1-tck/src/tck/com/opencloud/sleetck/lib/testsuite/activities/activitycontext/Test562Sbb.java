/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.activities.activitycontext;

import javax.slee.ActivityEndEvent;
import javax.slee.ActivityContextInterface;
import javax.slee.SbbContext;
import javax.slee.Address;
import javax.slee.facilities.Level;
import javax.slee.nullactivity.NullActivity;
import javax.slee.nullactivity.NullActivityFactory;
import javax.slee.nullactivity.NullActivityContextInterfaceFactory;

import com.opencloud.sleetck.lib.sbbutils.BaseTCKSbb;
import com.opencloud.sleetck.lib.sbbutils.TCKSbbUtils;
import com.opencloud.sleetck.lib.sbbutils.TCKSbbConstants;
import com.opencloud.sleetck.lib.resource.events.TCKResourceEventX;
import com.opencloud.sleetck.lib.resource.TCKActivityID;
import com.opencloud.sleetck.lib.resource.sbbapi.TCKActivity;
import com.opencloud.sleetck.lib.resource.sbbapi.TCKResourceSbbInterface;
import com.opencloud.sleetck.lib.resource.sbbapi.TCKActivityContextInterfaceFactory;

import java.util.HashMap;

public abstract class Test562Sbb extends BaseTCKSbb {

    public void onActivityEndEvent(ActivityEndEvent event, ActivityContextInterface aci) {

        // Check if the pending event was received and indicate test pass/failure.

        try {
            HashMap map = new HashMap();
            if (getEventReceived() == true) {
                map.put("Result", new Boolean(true));
                map.put("Message", "Ok");
            } else {
                map.put("Result", new Boolean(false));
                map.put("Message", "Did not receive CustomEvent, which was fired on the Activity Context Interface before the Activity was ended.");
            }
            TCKSbbUtils.getResourceInterface().sendSbbMessage(map);
        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }
    }

    public void onTCKResourceEventX1(TCKResourceEventX event, ActivityContextInterface aci) {
        try {

            // Create a NullActivity.
            NullActivityFactory factory = (NullActivityFactory) TCKSbbUtils.getSbbEnvironment().lookup("slee/nullactivity/factory");
            NullActivity nullActivity = factory.createNullActivity();

            // Get its Activity Context Interface.
            NullActivityContextInterfaceFactory aciFactory = (NullActivityContextInterfaceFactory) TCKSbbUtils.getSbbEnvironment().lookup("slee/nullactivity/activitycontextinterfacefactory");
            ActivityContextInterface nullACI = aciFactory.getActivityContextInterface(nullActivity);

            // Attach to the NullActivity.
            nullACI.attach(getSbbContext().getSbbLocalObject());

            // Fire an event on the NullActivity's ACI.
            fireCustomEvent(new Test562Event(), nullACI, null);

            // End the NullActivity.
            nullActivity.endActivity();

        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }
    }

    public void onCustomEvent(Test562Event event, ActivityContextInterface aci) {
        // Indicate that this 'pending' event was received.
        setEventReceived(true);
    }

    public abstract void fireCustomEvent(Test562Event event, ActivityContextInterface aci, Address address);

    public abstract void setEventReceived(boolean received);
    public abstract boolean getEventReceived();

}
