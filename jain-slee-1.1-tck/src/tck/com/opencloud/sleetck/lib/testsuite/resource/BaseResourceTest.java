/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.resource;

import java.rmi.RemoteException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Set;

import javax.slee.ComponentID;
import javax.slee.ServiceID;
import javax.slee.management.DeployableUnitDescriptor;
import javax.slee.management.DeployableUnitID;
import javax.slee.profile.ProfileSpecificationID;
import javax.slee.resource.ConfigProperties;
import javax.slee.resource.ResourceAdaptorID;

import com.opencloud.sleetck.lib.AbstractSleeTCKTest;
import com.opencloud.sleetck.lib.DescriptionKeys;
import com.opencloud.sleetck.lib.TCKTestErrorException;
import com.opencloud.sleetck.lib.TCKTestFailureException;
import com.opencloud.sleetck.lib.infra.SleeTCKComponentConstants;
import com.opencloud.sleetck.lib.rautils.MessageHandler;
import com.opencloud.sleetck.lib.rautils.MessageHandlerRegistry;
import com.opencloud.sleetck.lib.rautils.RMIObjectChannel;
import com.opencloud.sleetck.lib.rautils.UOID;
import com.opencloud.sleetck.lib.testutils.ProfileUtils;
import com.opencloud.sleetck.lib.testutils.jmx.DeploymentMBeanProxy;
import com.opencloud.sleetck.lib.testutils.jmx.ProfileProvisioningMBeanProxy;
import com.opencloud.sleetck.lib.testutils.jmx.ResourceManagementMBeanProxy;
import com.opencloud.sleetck.lib.testutils.jmx.ResourceUsageMBeanProxy;
import com.opencloud.sleetck.lib.testutils.jmx.ServiceManagementMBeanProxy;
import com.opencloud.util.Future;
import com.opencloud.util.Future.TimeoutException;

public class BaseResourceTest extends AbstractSleeTCKTest {

    protected static final String PROFILE_TABLE_NAME_PARAM = "profileTableName";
    protected static final String PROFILE_SPEC_NAME_PARAM = "profileSpecName";
    protected static final String PROFILE_SPEC_VERSION = "1.1";
    protected static final String RESOURCE_LINK_NAME_PARAM = "resourceLinkName";
    protected static final String USAGE_SET_NAME_PARAM = "usageParameterSetName";
    protected static final String SKIP_RA_SETUP_PARAM = "skipRASetup";

    public void setUp() throws Exception {
        in = utils().getRMIObjectChannel();
        out = utils().getMessageHandlerRegistry();

        profileTableName = utils().getTestParams().getProperty(PROFILE_TABLE_NAME_PARAM);
        profileSpecName = utils().getTestParams().getProperty(PROFILE_SPEC_NAME_PARAM);
        raLinkName = utils().getTestParams().getProperty(RESOURCE_LINK_NAME_PARAM);
        serviceDUPath = utils().getTestParams().getProperty(DescriptionKeys.SERVICE_DU_PATH_PARAM);
        raDUPath = utils().getTestParams().getProperty(DescriptionKeys.RESOURCE_DU_PATH_PARAM);
        usageSetName = utils().getTestParams().getProperty(USAGE_SET_NAME_PARAM);

        skipRASetup = "true".equalsIgnoreCase(utils().getTestParams().getProperty(SKIP_RA_SETUP_PARAM));

        ResourceManagementMBeanProxy resourceMBean = utils().getResourceManagementMBeanProxy();
        DeploymentMBeanProxy deploymentMBean = utils().getDeploymentMBeanProxy();
        ServiceManagementMBeanProxy serviceMBean = utils().getServiceManagementMBeanProxy();

        ResourceAdaptorID resourceAdaptorID = null;

        if (raDUPath == null)
            throw new TCKTestErrorException("Missing test parameter: " + DescriptionKeys.RESOURCE_DU_PATH_PARAM);

        // Resource Adaptor
        getLog().info("Installing resource adaptor: " + raDUPath);
        DeployableUnitID raDUID = utils().install(raDUPath);

        // Determine what the RA Entity should be called.
        getLog().info("Determining resource adaptor and entity names.");
        resourceAdaptorID = getResourceAdaptorID(raDUID);
        String raName = resourceAdaptorID.getName();
        raEntityName = raName + "_Entity";
        getLog().info("Resource Adaptor = \"" + resourceAdaptorID + "\", Resource Adaptor Entity = " + raEntityName);

        // Profile Table
        if (profileTableName != null && profileSpecName != null) {
            getLog().info("Creating profile table '" + profileTableName + "' from profile specification '" + profileSpecName + "'");
            ProfileUtils profileUtils = new ProfileUtils(utils());
            ProfileProvisioningMBeanProxy profileMBean = profileUtils.getProfileProvisioningProxy();
            ProfileSpecificationID profileSpecID = new ProfileSpecificationID(profileSpecName, SleeTCKComponentConstants.TCK_VENDOR, PROFILE_SPEC_VERSION);
            profileMBean.createProfileTable(profileSpecID, profileTableName);
        }

        // Create Entity
        if (!skipRASetup) {
            getLog().info("Creating Resource Adaptor Entity: " + raEntityName);
            resourceMBean.createResourceAdaptorEntity(resourceAdaptorID, raEntityName, new ConfigProperties());

            // Create usage parameter set
            if (usageSetName != null) {
                getLog().info("Creating Usage Parameter Set: " + usageSetName);
                ResourceUsageMBeanProxy resourceUsageMBean = utils().getResourceUsageMBeanProxy(raEntityName);
                resourceUsageMBean.createUsageParameterSet(usageSetName);
            }

            // Bind Link name
            if (raLinkName != null) {
                getLog().info("Binding RA Entity '" + raEntityName + "' to link name '" + raLinkName + "'");
                resourceMBean.bindLinkName(raEntityName, raLinkName);
            }
        }

        // Install Service
        if (serviceDUPath != null) {
            String absolutePath = utils().getDeploymentUnitURL(serviceDUPath);

            getLog().info("Installing service: " + absolutePath);

            DeployableUnitID serviceDUID = deploymentMBean.install(absolutePath);
            ServiceID serviceID = getServiceID(serviceDUID);
            serviceMBean.activate(serviceID);
        }

        // Activate RA Entity
        if (!skipRASetup) {
            if (raEntityName != null) {
                getLog().info("Activating Resource Adaptor Entity: " + raEntityName);
                resourceMBean.activateResourceAdaptorEntity(raEntityName);
            }
        }
    }

    public void tearDown() throws Exception {
        // Uninstall service.
        ResourceManagementMBeanProxy resourceMBean = utils().getResourceManagementMBeanProxy();
        DeploymentMBeanProxy deploymentMBean = utils().getDeploymentMBeanProxy();

        Exception error = null;

        // Deactivate RA Entity
        if (!skipRASetup) {
            try {
                getLog().info("Deactivating Resource Adaptor Entity: " + raEntityName);
                utils().deactivateResourceAdaptorEntity(raEntityName);
            } catch (Exception e) {
                getLog().error(e);
                if (error == null)
                    error = e;
            }
        }

        // Deactivate and remove service
        if (serviceDUPath != null) {
            try {
                getLog().info("Uninstalling service: " + serviceDUPath);
                String relativePath = utils().getTestParams().getProperty(DescriptionKeys.SERVICE_DU_PATH_PARAM);
                String serviceDuPath = utils().getDeploymentUnitURL(relativePath);

                DeployableUnitID serviceDUID = deploymentMBean.getDeployableUnit(serviceDuPath);
                ServiceID serviceID = getServiceID(serviceDUID);
                utils().deactivateService(serviceID);
                deploymentMBean.uninstall(serviceDUID);
            } catch (Exception e) {
                getLog().error(e);
                if (error == null)
                    error = e;
            }
        }

        if (!skipRASetup) {
            // Unbind Link name
            if (raLinkName != null) {
                try {
                    getLog().info("Unbinding RA Link Name: " + raLinkName);
                    resourceMBean.unbindLinkName(raLinkName);
                } catch (Exception e) {
                    getLog().error(e);
                    if (error == null)
                        error = e;

                }
            }

            // Remove usage parameter set
            if (usageSetName != null) {
                try {
                    getLog().info("Removing Usage Parameter Set: " + usageSetName);
                    ResourceUsageMBeanProxy resourceUsageMBean = utils().getResourceUsageMBeanProxy(raEntityName);
                    resourceUsageMBean.removeUsageParameterSet(usageSetName);
                } catch (Exception e) {
                    getLog().error(e);
                    if (error == null)
                        error = e;

                }
            }

            // Remove RA Entity
            try {
                getLog().info("Removing Resource Adaptor Entity: " + raEntityName);
                resourceMBean.removeResourceAdaptorEntity(raEntityName);
            } catch (Exception e) {
                getLog().error(e);
                if (error == null)
                    error = e;
            }
        }

        // Remove profile table.
        try {
            if (profileTableName != null) {
                getLog().info("Removing profile table: " + profileTableName);
                ProfileUtils profileUtils = new ProfileUtils(utils());
                ProfileProvisioningMBeanProxy profileMBean = profileUtils.getProfileProvisioningProxy();
                profileMBean.removeProfileTable(profileTableName);
            }
        } catch (Exception e) {
            getLog().error(e);
            if (error == null)
                error = e;

        }

        try {
            utils().removeRAEntities();
        } catch (Exception e) {
            getLog().error(e);
            if (error == null)
                error = e;
        }

        // RA removal will be taken care of by super class.
        try {
            super.tearDown();
        } catch (Exception e) {
            getLog().error(e);
            if (error == null)
                error = e;
        }

        if (error != null)
            throw new TCKTestErrorException("An unexpected exception was thrown during test: ", error);
    }

    //
    // Protected
    //

    protected void sendMessage(int method, Object argument, MessageHandler customMessageHandler, int sequenceID) {
        TCKMessage message = new TCKMessage(testUID, sequenceID, method, argument);
        getLog().info("Sending message to test component(s): " + message);
        try {
            in.setMessageHandler(customMessageHandler);
            out.sendMessage(message);
        } catch (RemoteException e) {
            getLog().error(e);
        }
    }

    protected HashMap sendMessage(int method) {
        return sendMessage(method, null);
    }

    protected HashMap sendMessage(int method, Object argument) {
        TCKMessage message = new TCKMessage(testUID, nextMessageID(), method, argument);
        getLog().info("Sending message to test component(s): " + message);
        int expectedResponse = message.getSequenceID();
        ResponseListener listener = new ResponseListener(expectedResponse);
        try {
            in.setMessageHandler(listener);
            out.sendMessage(message);
        } catch (RemoteException e) {
            getLog().error(e);
            return null;
        }
        return listener.getResultMap();
    }

    protected class ResponseListener implements MessageHandler {
        public ResponseListener(int expectedResponse) {
            this.expectedResponse = expectedResponse;
        }

        public boolean handleMessage(Object obj) throws RemoteException {
            getLog().info("Received message from test component: " + obj);

            if (!(obj instanceof TCKMessage)) {
                getLog().error("Unhandled message type: " + obj);
                return false;
            }
            TCKMessage message = (TCKMessage) obj;

            // Ignore additional messages of the same type, or further message
            // from a previous test (this should hopefully avoid problems with
            // multiple-jvm SLEEs).
            if (message.getSequenceID() != expectedResponse || future.isSet())
                return true;

            future.setValue(message.getArgument());
            return true;
        }

        public HashMap getResultMap() {
            HashMap result;
            try {
                result = (HashMap) future.getValue(utils().getTestTimeout());
            } catch (TimeoutException e) {
                return null;
            }
            return result;
        }

        private int expectedResponse = -1;
        private Future future = new Future();
    }

    protected class MultiResponseListener implements MessageHandler {

        public MultiResponseListener(int sequenceID) {
            this.expectedResponse = sequenceID;
        }

        public boolean handleMessage(Object obj) throws RemoteException {
            getLog().info("Received message from test component: " + obj);

            if (!(obj instanceof TCKMessage)) {
                getLog().error("Unhandled message type: " + obj);
                return false;
            }
            TCKMessage message = (TCKMessage) obj;

            if (message.getSequenceID() != expectedResponse)
                return true;

            HashMap results = (HashMap) message.getArgument();
            if (results == null)
                return true;

            Set keys = results.keySet();
            Iterator iter = keys.iterator();
            while (iter.hasNext()) {
                String key = (String) iter.next();
                Future future = getFuture(key);
                if (future != null) {
                    if (future.isSet())
                        continue;
                    future.setValue(results.get(key));
                }
            }

            return true;
        }

        // Call this one or more times before setting this class as a listener.
        public void addExpectedResult(String name) {
            futures.put(name, new Future());
        }

        // Call this to block until the result of the expected name is returned.
        public Object getResult(String name) {
            if (futures.get(name) == null)
                throw new IllegalArgumentException("Unknown result name (not set as expected result): " + name);

            Object result;
            try {
                Future future = (Future) futures.get(name);
                result = future.getValue(utils().getTestTimeout());
            } catch (TimeoutException e) {
                return null;
            }
            return result;
        }

        protected Future getFuture(String name) {
            return (Future) futures.get(name);
        }

        protected HashMap futures = new HashMap();
        protected int expectedResponse = -1;
    }

    protected TCKMessage[] castMessages(Object[] objects) {
        TCKMessage[] messages = new TCKMessage[objects.length];
        for (int i = 0; i < objects.length; i++)
            messages[i] = (TCKMessage) objects[i];
        return messages;
    }

    protected ServiceID getServiceID(DeployableUnitID duID) throws TCKTestErrorException {
        DeployableUnitDescriptor descriptor;
        try {
            descriptor = utils().getDeploymentMBeanProxy().getDescriptor(duID);
        } catch (Exception e) {
            throw new TCKTestErrorException("An error occured while attempting to find a ServiceID contained in DU: " + duID, e);
        }
        ComponentID[] components = descriptor.getComponents();
        for (int i = 0; i < components.length; i++)
            if (components[i] instanceof ServiceID)
                return (ServiceID) components[i];
        return null;
    }

    protected ResourceAdaptorID getResourceAdaptorID(DeployableUnitID duID) throws TCKTestErrorException {
        DeployableUnitDescriptor descriptor;
        try {
            descriptor = utils().getDeploymentMBeanProxy().getDescriptor(duID);
        } catch (Exception e) {
            throw new TCKTestErrorException("An error occured while attempting to find a ResourceAdaptorID contained in DU: " + duID, e);
        }
        ComponentID[] components = descriptor.getComponents();
        for (int i = 0; i < components.length; i++)
            if (components[i] instanceof ResourceAdaptorID)
                return (ResourceAdaptorID) components[i];
        return null;
    }

    protected String getResourceAdaptorEntityName() {
        return raEntityName;
    }

    protected int nextMessageID() {
        return nextMessageID++;
    }

    protected void checkResult(Object result, int assertion) throws TCKTestErrorException, TCKTestFailureException {
        if (result instanceof Exception)
            throw new TCKTestFailureException(assertion, "Unexpected exception thrown during test:", (Exception) result);
        if (!Boolean.TRUE.equals(result))
            throw new TCKTestErrorException("Unexpected result received from test component: " + result);
    }

    protected void checkResult(Object result, int assertion, String failureMessage) throws TCKTestErrorException, TCKTestFailureException {
        if (result instanceof Exception)
            throw new TCKTestFailureException(assertion, "Unexpected exception thrown during test:", (Exception) result);
        if (Boolean.FALSE.equals(result))
            throw new TCKTestFailureException(assertion, failureMessage);
        if (!Boolean.TRUE.equals(result))
            throw new TCKTestErrorException("Unexpected result received from test component: " + result);
    }

    protected void checkResult(HashMap results, String resultsField, int assertion, String failureMessage) throws TCKTestErrorException, TCKTestFailureException {
        getLog().fine("Checking result '" + resultsField + "' for test success status.");
        if (results == null)
            throw new TCKTestErrorException("No results were returned from test component.");
            
        Object result = results.get(resultsField);
        checkResult(result, assertion, failureMessage);
        getLog().fine("Result passed.");
    }

    protected RMIObjectChannel in;
    protected MessageHandlerRegistry out;

    protected static final UOID testUID = UOID.createUOID();

    //
    // Private
    //

    private static int nextMessageID = 0;

    // These should be configured during setup().
    private String profileTableName;
    private String profileSpecName;
    private String raLinkName;
    private String serviceDUPath;
    private String raDUPath;
    private String raEntityName;
    private String usageSetName;
    private boolean skipRASetup = false;

}
