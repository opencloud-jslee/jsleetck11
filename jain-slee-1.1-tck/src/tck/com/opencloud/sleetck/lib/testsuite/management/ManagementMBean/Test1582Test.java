/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.management.ManagementMBean;

import com.opencloud.sleetck.lib.AbstractSleeTCKTest;
import com.opencloud.sleetck.lib.TCKTestResult;
import com.opencloud.sleetck.lib.testutils.jmx.DeploymentMBeanProxy;

import javax.slee.management.DependencyException;
import javax.slee.management.DeployableUnitID;

public class Test1582Test extends AbstractSleeTCKTest {

    private static final String SBB_DU_PATH_PARAM = "sbbDUPath";
    private static final String SERVICE_DU_PATH_PARAM = "serviceDUPath";
    private static final int TEST_ID = 1582;

    /**
     * Perform the actual test.
     */
    public TCKTestResult run() throws Exception {
        // Should not be permitted to uninstall sbbDuID while serviceDuID is installed.
        DeploymentMBeanProxy duProxy = utils().getDeploymentMBeanProxy();
        try {
            duProxy.uninstall(sbbDuID);
        } catch (DependencyException e) {
            return TCKTestResult.passed();
        }

        return TCKTestResult.failed(TEST_ID, "Was permitted to uninstall an SBB that a Service depends on. " +
                "sbbDuID="+sbbDuID+",serviceDuID="+serviceDuID);
    }

    /**
     * Do all the pre-run configuration of the test.
     */
    public void setUp() throws Exception {
        // Install the Deployable Units
        String duPath = utils().getTestParams().getProperty(SBB_DU_PATH_PARAM);
        sbbDuID = utils().install(duPath);
        duPath = utils().getTestParams().getProperty(SERVICE_DU_PATH_PARAM);
        serviceDuID = utils().install(duPath);
    }

    private DeployableUnitID serviceDuID, sbbDuID;
}
