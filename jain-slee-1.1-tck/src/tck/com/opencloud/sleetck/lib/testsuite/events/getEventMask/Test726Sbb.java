/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.events.getEventMask;

import javax.slee.ActivityEndEvent;
import javax.slee.ActivityContextInterface;
import javax.slee.SbbContext;
import javax.slee.Address;
import javax.slee.SbbLocalObject;
import javax.slee.facilities.Level;
import javax.slee.ChildRelation;

import javax.slee.nullactivity.NullActivityFactory;
import javax.slee.nullactivity.NullActivity;
import javax.slee.nullactivity.NullActivityContextInterfaceFactory;
import javax.slee.facilities.ActivityContextNamingFacility;

import com.opencloud.sleetck.lib.sbbutils.BaseTCKSbb;
import com.opencloud.sleetck.lib.sbbutils.TCKSbbUtils;
import com.opencloud.sleetck.lib.sbbutils.TCKSbbConstants;
import com.opencloud.sleetck.lib.resource.events.TCKResourceEventX;
import com.opencloud.sleetck.lib.resource.TCKActivityID;
import com.opencloud.sleetck.lib.resource.sbbapi.TCKActivity;
import com.opencloud.sleetck.lib.resource.sbbapi.TCKResourceSbbInterface;
import com.opencloud.sleetck.lib.resource.sbbapi.TCKActivityContextInterfaceFactory;

import java.util.HashMap;
import java.util.Iterator;

public abstract class Test726Sbb extends BaseTCKSbb {

    // Initial event method.
    public void onTCKResourceEventX1(TCKResourceEventX ev, ActivityContextInterface aci) {

        try {

            HashMap map = new HashMap();

            // Mask out Test726Event.
            getSbbContext().maskEvent(new String [] { "Test726Event" }, aci);

            // Fetch the set of masked events and verify that the set of masked events is correct.
            String [] maskedEvents = getSbbContext().getEventMask(aci);

            if (maskedEvents.length == 1 && maskedEvents[0].equals("Test726Event")) {
                map.put("Result", new Boolean(true));
                map.put("Message", "Ok");
                TCKSbbUtils.getResourceInterface().sendSbbMessage(map);
                return;
            }

            map.put("Result", new Boolean(false));
            if (maskedEvents.length == 0)
                map.put("Message", "There were no masked events, Test726Event should be masked.");

            if (maskedEvents.length >= 2)
                map.put("Message", "There were " + maskedEvents.length + "masked events, when there should have been only one: Test726Event.");

            if (maskedEvents.length == 1)
                map.put("Message", "The masked event was " + maskedEvents[0] + ", it should have been Test726Event.");

            TCKSbbUtils.getResourceInterface().sendSbbMessage(map);
        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }
    }

    public void onTCKResourceEventX2(TCKResourceEventX event, ActivityContextInterface aci) {
    }

    public void onTest726Event(Test726Event event, ActivityContextInterface aci) {
    }

}
