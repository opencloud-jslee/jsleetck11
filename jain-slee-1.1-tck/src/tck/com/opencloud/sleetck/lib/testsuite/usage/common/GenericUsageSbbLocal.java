/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.usage.common;

import javax.slee.SbbLocalObject;

/**
 * SbbLocalObject interface for the GenericUsageSbbSbb
 */
public interface GenericUsageSbbLocal extends SbbLocalObject {

    public void doUpdates(GenericUsageSbbInstructions instructions);

}
