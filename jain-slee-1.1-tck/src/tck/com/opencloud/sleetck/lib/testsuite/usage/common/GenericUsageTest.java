/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.usage.common;

import com.opencloud.sleetck.lib.AbstractSleeTCKTest;

/**
 * Base class for the usage tests.
 *
 * Accesses and provides accessors for the usage MBean names and interfaces via
 * the GenericUsageMBeanLookup class.
 */
public abstract class GenericUsageTest extends AbstractSleeTCKTest {

    // -- AbstractSleeTCKTest methods -- //

    public void setUp() throws Exception {
        super.setUp();
        genericUsageMBeanLookup = new GenericUsageMBeanLookup(utils());
    }

    public void tearDown() throws Exception {
        if(genericUsageMBeanLookup != null) genericUsageMBeanLookup.closeAllMBeans();
        super.tearDown();
    }

    // -- Access to usage MBean names and interfaces -- //

    public GenericUsageMBeanLookup getGenericUsageMBeanLookup() {
        return genericUsageMBeanLookup;
    }

    private GenericUsageMBeanLookup genericUsageMBeanLookup;

}
