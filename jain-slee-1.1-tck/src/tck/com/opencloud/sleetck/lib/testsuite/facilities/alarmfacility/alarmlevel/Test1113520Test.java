/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.facilities.alarmfacility.alarmlevel;

import javax.slee.facilities.AlarmLevel;

import com.opencloud.sleetck.lib.AbstractSleeTCKTest;
import com.opencloud.sleetck.lib.TCKTestResult;

/*
 * AssertionID (1113520): Test A singleton instance of each enumerated value is guaranteed 
 * (via an implementation of the readResolve method, see java.io.Serializable for more details), 
 * so that equality tests using == are always evaluated correctly.
 *
 * AssertionID (1113645): Test It returns true if obj is an instance of this class representing 
 * the same alarm level as this, false otherwise.
 *
 * AssertionID (1113648): Test It returns a hash code value.  
 *
 * AssertionID (1113651): Test It returns the textual representation of this alarm level object.
 *
 */
public class Test1113520Test extends AbstractSleeTCKTest {

    /**
     * Perform the actual test.
     */

    public TCKTestResult run() throws Exception {
        // 1113520:CLEAR
        try {
            if (AlarmLevel.CLEAR != AlarmLevel.fromInt(AlarmLevel.LEVEL_CLEAR)) {
                return TCKTestResult.failed(1113520,
                        "This AlarmLevel.CLEAR didn't equivalent to AlarmLevel.fromInt(AlarmLevel.LEVEL_CLEAR)");
            }

            if (AlarmLevel.CLEAR.toInt() != AlarmLevel.LEVEL_CLEAR) {
                return TCKTestResult.failed(1113520,
                        "This AlarmLevel.CLEAR.toInt() didn't equivalent to AlarmLevel.LEVEL_CLEAR");
            }
            getLog().info("The equality test for AlarmLevel.CLEAR");
        } catch (Exception e) {
            return TCKTestResult.failed(1113520, "The equality test of AlarmLevel.CLEAR threw an exception.");
        }

        // 1113520:CRITICAL
        try {
            if (AlarmLevel.CRITICAL != AlarmLevel.fromInt(AlarmLevel.LEVEL_CRITICAL)) {
                return TCKTestResult.failed(1113520,
                        "This AlarmLevel.CRITICAL didn't equivalent to AlarmLevel.fromInt(AlarmLevel.LEVEL_CRITICAL)");
            }
            if (AlarmLevel.CRITICAL.toInt() != AlarmLevel.LEVEL_CRITICAL) {
                return TCKTestResult.failed(1113520,
                        "This AlarmLevel.CRITICAL.toInt() didn't equivalent to AlarmLevel.LEVEL_CRITICAL");
            }
            getLog().info("The equality test for AlarmLevel.CRITICAL");
        } catch (Exception e) {
            return TCKTestResult.failed(1113520, "The equality test of AlarmLevel.CRITICAL threw an exception.");
        }

        // 1113520:MAJOR
        try {
            if (AlarmLevel.MAJOR != AlarmLevel.fromInt(AlarmLevel.LEVEL_MAJOR)) {
                return TCKTestResult.failed(1113520,
                        "This AlarmLevel.MAJOR didn't equivalent to AlarmLevel.fromInt(AlarmLevel.LEVEL_MAJOR)");
            }
            if (AlarmLevel.MAJOR.toInt() != AlarmLevel.LEVEL_MAJOR) {
                return TCKTestResult.failed(1113520,
                        "This AlarmLevel.MAJOR.toInt() didn't equivalent to AlarmLevel.LEVEL_MAJOR");
            }
            getLog().info("The equality test for AlarmLevel.MAJOR");
        } catch (Exception e) {
            return TCKTestResult.failed(1113520, "The equality test of AlarmLevel.MAJOR threw an exception.");
        }

        // 1113520:WARNING
        try {
            if (AlarmLevel.WARNING != AlarmLevel.fromInt(AlarmLevel.LEVEL_WARNING)) {
                return TCKTestResult.failed(1113520,
                        "This AlarmLevel.WARNING didn't equivalent to AlarmLevel.fromInt(AlarmLevel.LEVEL_WARNING)");
            }
            if (AlarmLevel.WARNING.toInt() != AlarmLevel.LEVEL_WARNING) {
                return TCKTestResult.failed(1113520,
                        "This AlarmLevel.WARNING.toInt() didn't equivalent to AlarmLevel.LEVEL_WARNING");
            }
            getLog().info("The equality test for AlarmLevel.WARNING");
        } catch (Exception e) {
            return TCKTestResult.failed(1113520, "The equality test of AlarmLevel.WARNING threw an exception.");
        }

        // 1113520:INDETERMINATE
        try {
            if (AlarmLevel.INDETERMINATE != AlarmLevel.fromInt(AlarmLevel.LEVEL_INDETERMINATE)) {
                return TCKTestResult
                        .failed(1113520,
                                "This AlarmLevel.INDETERMINATE didn't equivalent to AlarmLevel.fromInt(AlarmLevel.LEVEL_INDETERMINATE)");
            }
            if (AlarmLevel.INDETERMINATE.toInt() != AlarmLevel.LEVEL_INDETERMINATE) {
                return TCKTestResult.failed(1113520,
                        "This AlarmLevel.INDETERMINATE.toInt() didn't equivalent to AlarmLevel.LEVEL_INDETERMINATE");
            }
            getLog().info("The equality test for AlarmLevel.INDETERMINATE");
        } catch (Exception e) {
            return TCKTestResult.failed(1113520, "The equality test of AlarmLevel.INDETERMINATE threw an exception.");
        }

        // 1113520:MINOR
        try {
            if (AlarmLevel.MINOR != AlarmLevel.fromInt(AlarmLevel.LEVEL_MINOR)) {
                return TCKTestResult.failed(1113520,
                        "This AlarmLevel.MINOR didn't equivalent to AlarmLevel.fromInt(AlarmLevel.LEVEL_MINOR)");
            }
            if (AlarmLevel.MINOR.toInt() != AlarmLevel.LEVEL_MINOR) {
                return TCKTestResult.failed(1113520,
                        "This AlarmLevel.MINOR.toInt() didn't equivalent to AlarmLevel.LEVEL_MINOR");
            }
            getLog().info("The equality test for AlarmLevel.MINOR");
        } catch (Exception e) {
            return TCKTestResult.failed(1113520, "The equality test of AlarmLevel.MINOR threw an exception.");
        }

        // 1113645
        AlarmLevel firstAlarmLevel = AlarmLevel.MAJOR;
        AlarmLevel secondAlarmLevel = AlarmLevel.MINOR;
        if (firstAlarmLevel.equals(secondAlarmLevel))
            return TCKTestResult.failed(1113645, "AlarmLevel.equals(different AlarmLevel) returned true.");

        if (!firstAlarmLevel.equals(firstAlarmLevel))
            return TCKTestResult.failed(1113645, "AlarmLevel.equals(self) returned false.");

        // 1113648
        try {
            firstAlarmLevel.hashCode();
        } catch (Exception e) {
            return TCKTestResult.failed(1113648, "firstAlarmLevel.hashCode() threw an exception.");
        }

        // 1113651
        try {
            firstAlarmLevel.toString();
        } catch (Exception e) {
            return TCKTestResult.failed(1113651, "firstAlarmLevel.toString() threw an exception.");
        }

        if (firstAlarmLevel.toString() == null)
            return TCKTestResult.failed(1113651, "firstAlarmLevel.toString() returned null.");

        return TCKTestResult.passed();
    }

    /**
     * Do all the pre-run configuration of the test.
     */

    public void setUp() throws Exception {

    }

    /**
     * Clean up after the test.
     */

    public void tearDown() throws Exception {
    }
}
