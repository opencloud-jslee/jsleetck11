/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.resource.endpoint;

import java.rmi.RemoteException;
import java.util.HashMap;

import com.opencloud.sleetck.lib.TCKTestErrorException;
import com.opencloud.sleetck.lib.TCKTestFailureException;
import com.opencloud.sleetck.lib.TCKTestResult;
import com.opencloud.sleetck.lib.rautils.MessageHandler;
import com.opencloud.sleetck.lib.testsuite.resource.BaseResourceTest;
import com.opencloud.sleetck.lib.testsuite.resource.RAMethods;
import com.opencloud.sleetck.lib.testsuite.resource.TCKMessage;
import com.opencloud.util.Future;
import com.opencloud.util.Future.TimeoutException;

/**
 * Test to check that that events fired using fireEventTransacted don't actually
 * get fired if the transaction rolls back.
 * <p>
 * Tests assertions: 1115242
 */
public class Test1115242Test extends BaseResourceTest {

    private static final int ASSERTION_ID = 1115242;

    public TCKTestResult run() throws Exception {

        int sequenceID = nextMessageID();
        ResponseListener listener = new ResponseListener(sequenceID);
        sendMessage(RAMethods.fireEventTransacted, new Integer(ASSERTION_ID), listener, sequenceID);

        Object sbbResult = listener.getSbbResult();
        Object raResult = listener.getRAResult();

        if (raResult == null)
            throw new TCKTestErrorException("Test timed out while waiting for response from test resource adaptor component");
        if (raResult instanceof Exception)
            throw new TCKTestFailureException(ASSERTION_ID, "An exception was thrown by the test resource adaptor component", (Exception) raResult);
        if (!Boolean.TRUE.equals(raResult))
            throw new TCKTestErrorException("Unexpected result received from test resource adaptor component: " + raResult);
        if (sbbResult != null)
            throw new TCKTestFailureException(ASSERTION_ID, "Test received event fired confirmation from test sbb, despite enclosing transaction rollback");

        return TCKTestResult.passed();
    }

    private class ResponseListener implements MessageHandler {

        public ResponseListener(int sequenceID) {
            this.expectedResponse = sequenceID;
        }

        public boolean handleMessage(Object obj) throws RemoteException {
            getLog().info("Received message from test component: " + obj);

            if (!(obj instanceof TCKMessage)) {
                getLog().error("Unhandled message type: " + obj);
                return false;
            }
            TCKMessage message = (TCKMessage) obj;

            if (message.getSequenceID() != expectedResponse)
                return true;

            HashMap results = (HashMap) message.getArgument();
            Object raResult = results.get("result1");
            Object sbbResult = results.get("result2");

            if (raResult != null && !raFuture.isSet())
                raFuture.setValue(raResult);
            else if (sbbResult != null && !sbbFuture.isSet())
                sbbFuture.setValue(sbbResult);

            return true;
        }

        public Object getSbbResult() {
            Object result;
            try {
                result = sbbFuture.getValue(utils().getTestTimeout());
            } catch (TimeoutException e) {
                return null;
            }
            return result;
        }

        public Object getRAResult() {
            Object result;
            try {
                result = raFuture.getValue(utils().getTestTimeout());
            } catch (TimeoutException e) {
                return null;
            }
            return result;
        }

        private Future sbbFuture = new Future();
        private Future raFuture = new Future();

        private int expectedResponse = -1;
    }

}
