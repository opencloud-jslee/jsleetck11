/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.events.eventcontext;

import java.util.HashMap;

import javax.slee.ActivityContextInterface;
import javax.slee.EventContext;
import javax.slee.InitialEventSelector;
import javax.slee.facilities.Tracer;

import com.opencloud.sleetck.lib.resource.events.TCKResourceEventX;
import com.opencloud.sleetck.lib.sbbutils.BaseTCKSbb;
import com.opencloud.sleetck.lib.sbbutils.TCKSbbUtils;

/**
 * AssertionID(1108033): Test This isSuspended method determines if the 
 * delivery of the event associated with this Event Context is suspended.
 * 
 * AssertionID(1108034): Test This method returns true if the event 
 * delivery is suspended or false otherwise.
 *
 */
public abstract class Test1108033Sbb2 extends BaseTCKSbb {
    public static final String TRACE_MESSAGE_TCKResourceEventX1 = "Test1108033Sbb2:I got TCKResourceEventX1 on ActivityA";

    public static final String TRACE_MESSAGE_TCKResourceEventX2 = "Test1108033Sbb2:I got TCKResourceEventX2 on ActivityB";

    public InitialEventSelector initialEventSelector(InitialEventSelector ies) {
        ies.setCustomName("test");
        return ies;
    }

    public void onTCKResourceEventX1(TCKResourceEventX event, ActivityContextInterface aci, EventContext context) {
        try {
            //send message to TCK: I got TCKResourceEventX1 on ActivityA
            tracer = getSbbContext().getTracer("com.test");
            tracer.info(TRACE_MESSAGE_TCKResourceEventX1);

            //If the delivery of the event associated with this Event Context is suspended,
            //the event handler method in Sbb2 should not receive anything.
            setPassed(true);

        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }
    }

    public void onTCKResourceEventX2(TCKResourceEventX event, ActivityContextInterface aci, EventContext context) {
        try {
            //send message to TCK: I got TCKResourceEventX2 on ActivityB
            tracer = getSbbContext().getTracer("com.test");
            tracer.info("Begin: " + TRACE_MESSAGE_TCKResourceEventX2);

            if (!getPassed()) {
                sendResultToTCK("Test1108033Test", true, "This method returns true if the event delivery is suspended.", 1108033);
            } else {
                sendResultToTCK("Test1108033Test", false, "SBB2:onTCKResourceEventX2-ERROR: If the delivery of the event associated with "
                        + "this Event Context is suspended in Sbb1, the event handler "
                        + "method in Sbb2 should not receive anything.", 1108033);
            }
            tracer.info("End: " + TRACE_MESSAGE_TCKResourceEventX2);
            return;

        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }
    }

    private void sendResultToTCK(String testName, boolean result, String message, int failedAssertionID) throws Exception {
        HashMap sbbData = new HashMap();
        sbbData.put("testname", testName);
        sbbData.put("result", result ? "pass" : "fail");
        sbbData.put("message", message);
        sbbData.put("id", new Integer(failedAssertionID));
        TCKSbbUtils.getResourceInterface().sendSbbMessage(sbbData);
    }

    private Tracer tracer = null;

    public abstract void setPassed(boolean passed);
    public abstract boolean getPassed();

}
