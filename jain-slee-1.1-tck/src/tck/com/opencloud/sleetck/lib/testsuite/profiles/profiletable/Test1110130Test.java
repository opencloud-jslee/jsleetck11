/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.profiles.profiletable;

import javax.slee.profile.ProfileTable;

import com.opencloud.sleetck.lib.AbstractSleeTCKTest;
import com.opencloud.sleetck.lib.TCKTestErrorException;
import com.opencloud.sleetck.lib.TCKTestFailureException;
import com.opencloud.sleetck.lib.TCKTestResult;

interface Test1110130_1ProfileTable extends ProfileTable {

}

public class Test1110130Test extends AbstractSleeTCKTest {

    private static final String PROFILE_TABLE_PCK_PRIVATE_DU_PATH_PARAM= "profileTablePackagePrivateDUPath";
    private static final String QUERY_MISS_DU_PATH_PARAM= "queryMissDUPath";
    private static final String ALL_OK_DU_PATH_PARAM= "allOKDUPath";
    private static final String NAME_NO_MATCH_DU_PATH_PARAM= "nameNoMatchDUPath";
    private static final String PREFIX_OMIT_DU_PATH_PARAM= "prefixOmitDUPath";
    private static final String NO_UPPERCASE_DU_PATH_PARAM= "noUppercaseDUPath";
    private static final String MANY_PARAMS_DU_PATH_PARAM= "manyParamsDUPath";
    private static final String FEW_PARAMS_DU_PATH_PARAM= "fewParamsDUPath";
    private static final String WRONG_ORDER_DU_PATH_PARAM= "wrongOrderDUPath";


    private static final int TEST_ID = 1110130;

    /**
     * The actual test, deploying a number of profile specs and checking that deployment does or doesn't work as expected
     */
    public TCKTestResult run() throws Exception {

        //Spec 1: ProfileTable interface is not declared as public
        getLog().fine("Deploying profile spec.");
        try {
            setupService(PROFILE_TABLE_PCK_PRIVATE_DU_PATH_PARAM);
            throw new TCKTestFailureException(1110128,"Deployment of Profile spec with ProfileTable interface not declared public should fail but was successful.");
        }
        catch (TCKTestErrorException e) {
            getLog().fine("Caught expected exception when trying to deploy profile spec. Exception: "+e.getMessage());
        }

        //Spec 3: All queries defined in Profile spec are correctly defined in ProfileTable interface
        getLog().fine("Deploying profile spec.");
        try {
            setupService(ALL_OK_DU_PATH_PARAM);
            getLog().fine("Deployment of Profile spec with ProfileTable interface defining all query methods worked fine.");
        }
        catch (TCKTestErrorException e) {
            throw new TCKTestFailureException(1110130,"Deployment of Profile spec with ProfileTable interface correctly defining all query methods failed.",e);
        }

        //Spec 4: name not matching exactly
        getLog().fine("Deploying profile spec.");
        try {
            setupService(NAME_NO_MATCH_DU_PATH_PARAM);
            throw new TCKTestFailureException(1110186,"Deployment of Profile spec with query method name in ProfileTable " +
                    "interface not exactly matching the name in the deployment descriptor" +
                    " should have failed but succeeded.");
        }
        catch (TCKTestErrorException e) {
            getLog().fine("Caught expected exception when trying to deploy profile spec. Exception: "+e.getMessage());
        }

        //Spec 5: 'query' prefix omitted
        getLog().fine("Deploying profile spec.");
        try {
            setupService(PREFIX_OMIT_DU_PATH_PARAM);
            throw new TCKTestFailureException(1110186,"Deployment of Profile spec with query method name in ProfileTable " +
                    "interface not having 'query' as prefix" +
                    " should have failed but succeeded.");
        }
        catch (TCKTestErrorException e) {
            getLog().fine("Caught expected exception when trying to deploy profile spec. Exception: "+e.getMessage());
        }

        //Spec 6: First letter after 'query' not uppercase
        getLog().fine("Deploying profile spec.");
        try {
            setupService(NO_UPPERCASE_DU_PATH_PARAM);
            throw new TCKTestFailureException(1110186,"Deployment of Profile spec with first letter after 'query' " +
                    "not uppercase" +
                    " should have failed but succeeded.");
        }
        catch (TCKTestErrorException e) {
            getLog().fine("Caught expected exception when trying to deploy profile spec. Exception: "+e.getMessage());
        }

        //Spec 7: Too many method parameters
        getLog().fine("Deploying profile spec.");
        try {
            setupService(MANY_PARAMS_DU_PATH_PARAM);
            throw new TCKTestFailureException(1110187,"Deployment of Profile spec with too many parameters in " +
                    "query method" +
                    " should have failed but succeeded.");
        }
        catch (TCKTestErrorException e) {
            getLog().fine("Caught expected exception when trying to deploy profile spec. Exception: "+e.getMessage());
        }

        //Spec 8: Too few method parameters
        getLog().fine("Deploying profile spec.");
        try {
            setupService(FEW_PARAMS_DU_PATH_PARAM);
            throw new TCKTestFailureException(1110187,"Deployment of Profile spec with too few parameters in " +
                    "query method" +
                    " should have failed but succeeded.");
        }
        catch (TCKTestErrorException e) {
            getLog().fine("Caught expected exception when trying to deploy profile spec. Exception: "+e.getMessage());
        }

        //Spec 9: Parameters are in wrong order
        getLog().fine("Deploying profile spec.");
        try {
            setupService(WRONG_ORDER_DU_PATH_PARAM);
            throw new TCKTestFailureException(1110187,"Deployment of Profile spec with wrong parameter order" +
                    " in query method" +
                    " should have failed but succeeded.");
        }
        catch (TCKTestErrorException e) {
            getLog().fine("Caught expected exception when trying to deploy profile spec. Exception: "+e.getMessage());
        }

        //Spec 2: 4 queries defined in Profile spec, one not defined in ProfileTable interface
        getLog().fine("Deploying profile spec.");
        try {
            setupService(QUERY_MISS_DU_PATH_PARAM);
            throw new TCKTestFailureException(1110130,"Deployment of Profile spec with ProfileTable interface not defining all query methods should fail but was successful.");
        }
        catch (TCKTestErrorException e) {
            getLog().fine("Caught expected exception when trying to deploy profile spec. Exception: "+e.getMessage());
        }

        return TCKTestResult.passed();
    }


    public void setUp() throws Exception {

    }

    public void tearDown() throws Exception {


        super.tearDown();
    }

}
