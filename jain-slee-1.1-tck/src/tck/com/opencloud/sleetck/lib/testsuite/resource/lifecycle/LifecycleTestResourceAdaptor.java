/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.resource.lifecycle;

import javax.slee.resource.ConfigProperties;
import javax.slee.resource.InvalidConfigurationException;
import javax.slee.resource.ResourceAdaptorContext;

import com.opencloud.sleetck.lib.rautils.BaseTCKRA;
import com.opencloud.sleetck.lib.testsuite.resource.RAMethods;
import com.opencloud.sleetck.lib.testsuite.resource.TCKMessage;

/**
 * SLEE 1.1 Lifecycle testing Resource Adaptor
 */
public class LifecycleTestResourceAdaptor extends BaseTCKRA {

    public LifecycleTestResourceAdaptor() {
        super();
        sendLifecycleMessage(RAMethods.constructor);
    }

    protected void finalize() throws Throwable {
        sendLifecycleMessage(RAMethods.finalizer);
        super.finalize();
    }
  
    //
    // Resource Adaptor lifecycle methods
    //

    public void raConfigurationUpdate(ConfigProperties properties) {
        getLog().info("raConfigurationUpdate()");
        sendLifecycleMessage(RAMethods.raConfigurationUpdate, sanitizeConfigProperties(properties));
    }

    public void raConfigure(ConfigProperties properties) {
        getLog().info("raConfigure()");
        sendLifecycleMessage(RAMethods.raConfigure, sanitizeConfigProperties(properties));
    }

    public void raUnconfigure() {
        getLog().info("raUnconfigure()");
        sendLifecycleMessage(RAMethods.raUnconfigure);
    }

    public void raActive() {
        getLog().info("raActive()");
        sendLifecycleMessage(RAMethods.raActive);
    }

    public void raInactive() {
        getLog().info("raInactive()");
        sendLifecycleMessage(RAMethods.raInactive);
    }

    public void raStopping() {
        getLog().info("raStopping()");
        sendLifecycleMessage(RAMethods.raStopping);
    }

    public void raVerifyConfiguration(ConfigProperties properties) throws InvalidConfigurationException {
        getLog().info("raVerifyConfiguration()");
        sendLifecycleMessage(RAMethods.raVerifyConfiguration, sanitizeConfigProperties(properties));
    }

    public void setResourceAdaptorContext(ResourceAdaptorContext context) {
        // ResourceAdaptorContext is not serializable, so pass a string back as
        // the argument.
        if (context == null)
            sendLifecycleMessage(RAMethods.setResourceAdaptorContext, null);
        else {
            setTracer(context.getTracer("TCK Lifecycle RA"));
            getLog().info("setResourceAdaptorContext()");
            sendLifecycleMessage(RAMethods.setResourceAdaptorContext, "<context>");
        }
    }

    public void unsetResourceAdaptorContext() {
        getLog().info("unsetResourceAdaptorContext()");
        sendLifecycleMessage(RAMethods.unsetResourceAdaptorContext);
        setTracer(null);
    }

    public Object getResourceAdaptorInterface(String className) {
        getLog().info("getResourceAdaptorInterface()");
        sendLifecycleMessage(RAMethods.getResourceAdaptorInterface, className);
        return super.getResourceAdaptorInterface(className);
    }

    //
    // Private
    //

    private void sendLifecycleMessage(int method) {
        sendLifecycleMessage(method, null);
    }

    private void sendLifecycleMessage(int method, Object argument) {
        sendMessage(new TCKMessage(getRAUID(), method, argument));
    }

}
