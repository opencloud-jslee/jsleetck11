/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.transactions;

import javax.slee.ActivityEndEvent;
import javax.slee.ActivityContextInterface;
import javax.slee.SbbContext;
import javax.slee.Address;
import javax.slee.SbbLocalObject;
import javax.slee.facilities.Level;
import javax.slee.ChildRelation;
import javax.slee.CreateException;

import com.opencloud.sleetck.lib.TCKTestErrorException;
import com.opencloud.sleetck.lib.sbbutils.BaseTCKSbb;
import com.opencloud.sleetck.lib.sbbutils.TCKSbbUtils;
import com.opencloud.sleetck.lib.sbbutils.TCKSbbConstants;
import com.opencloud.sleetck.lib.resource.events.TCKResourceEventX;
import com.opencloud.sleetck.lib.resource.events.TCKResourceEventY;
import com.opencloud.sleetck.lib.resource.sbbapi.TCKResourceSbbInterface;

import java.util.HashMap;
import java.util.Iterator;

public abstract class Test856Sbb extends BaseTCKSbb {

    public void sbbRolledBack(javax.slee.RolledBackContext context) {
    }

    // Initial event
    public void onTCKResourceEventX1(TCKResourceEventX ev, ActivityContextInterface aci) {
        try {

            // Create child and attach to the ACI.
            aci.attach(getChildRelation().create());
        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }
    }

    public void onTCKResourceEventX2(TCKResourceEventX ev, ActivityContextInterface aci) {

        try {

            // Sanity check.
            if (getChildRelation().size() != 1) {
                HashMap map = new HashMap();
                map.put("Result", new Boolean(false));
                map.put("Message", "Incorrect number of children in child relation, cannot continue.");
                TCKSbbUtils.getResourceInterface().sendSbbMessage(map);
                return;
            }

            // Find the child, remove it.
            Iterator iter = getChildRelation().iterator();
            SbbLocalObject child = (SbbLocalObject) iter.next();

            // Remove the child.
            child.remove();

            // Mark TXN for rollback.
            getSbbContext().setRollbackOnly();

        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }

    }

    public void onTCKResourceEventY1(TCKResourceEventY ev, ActivityContextInterface aci) {
        try {
            fireTest856Event(new Test856Event(), aci, null);
        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }
    }

    public void onTest856SecondEvent(Test856SecondEvent event, ActivityContextInterface aci) {
        try {
            HashMap map = new HashMap();
            map.put("Result", new Boolean(false));
            map.put("Message", "Child removed in a TXN that was subsequently marked for rollback no longer exists, it should.");
            TCKSbbUtils.getResourceInterface().sendSbbMessage(map);
        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }
    }

    public abstract void fireTest856Event(Test856Event event, ActivityContextInterface aci, Address address);
    public abstract void fireTest856SecondEvent(Test856SecondEvent event, ActivityContextInterface aci, Address address);

    public abstract ChildRelation getChildRelation();

}
