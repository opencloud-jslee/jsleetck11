/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.facilities.profilefacility;

import java.rmi.RemoteException;
import java.util.Map;

import com.opencloud.sleetck.lib.AbstractSleeTCKTest;
import com.opencloud.sleetck.lib.DescriptionKeys;
import com.opencloud.sleetck.lib.TCKTestFailureException;
import com.opencloud.sleetck.lib.resource.TCKActivityID;
import com.opencloud.sleetck.lib.resource.TCKSbbMessage;
import com.opencloud.sleetck.lib.resource.events.TCKResourceEventX;
import com.opencloud.sleetck.lib.resource.testapi.TCKResourceListener;
import com.opencloud.sleetck.lib.resource.testapi.TCKResourceTestInterface;
import com.opencloud.sleetck.lib.testutils.Assert;
import com.opencloud.sleetck.lib.testutils.BaseTCKResourceListener;
import com.opencloud.sleetck.lib.testutils.FutureResult;

/**
 * AssertionID (1113023): Test The JNDI_NAME constant. This constant specifies
 * the JNDI location where a ProfileFacility object may be located by an SBB
 * component in its component environment.
 */
public class Test1113023Test extends AbstractSleeTCKTest {

    public void run(FutureResult result) throws Exception {
        this.result = result;
        String activityName = "Test1113023Test";
        TCKResourceTestInterface resource = utils().getResourceInterface();
        TCKActivityID activityID = resource.createActivity(activityName);
        getLog().info("Firing event: " + testName);

        // kickoff the test
        resource.fireEvent(TCKResourceEventX.X1, testName, activityID, null);
    }

    public void setUp() throws Exception {
        getLog().fine("Connecting to resource");
        resourceListener = new TCKResourceListenerImpl();
        setResourceListener(resourceListener);

        // this wil install the service DU
        setupService(DescriptionKeys.SERVICE_DU_PATH_PARAM, true);

    }

    public void tearDown() throws Exception {

        // cleanup
        super.tearDown();
    }

    // Private classes

    private class TCKResourceListenerImpl extends BaseTCKResourceListener {

        // TCKResourceListener methods

        public synchronized void onSbbMessage(TCKSbbMessage message, TCKActivityID calledActivity)
                throws RemoteException {
            Map sbbData = (Map) message.getMessage();
            String sbbTestName = "jndi";
            String sbbTestResult = (String) sbbData.get("result");
            String sbbTestMessage = (String) sbbData.get("message");
            int assertionID = ((Integer) sbbData.get("id")).intValue();
            getLog().info(
                    "Received message from SBB: testname=" + sbbTestName + ", result=" + sbbTestResult + ", message="
                            + sbbTestMessage + ", id=" + assertionID);
            try {
                if (sbbTestName.equals(testName)) {

                    Assert.assertEquals(assertionID, "Test " + testName + " failed: " + sbbTestMessage, "pass",
                            sbbTestResult);
                    result.setPassed();

                } else
                    result.setError("Invalid response sent by SBB: " + sbbTestName);
            } catch (TCKTestFailureException ex) {
                result.setFailed(assertionID, sbbTestMessage);
            }
        }

        public void onException(Exception exception) throws RemoteException {
            getLog().warning("Received Exception from SBB or resource:");
            getLog().warning(exception);
            result.setError(exception);
        }

    }

    // Private state

    private TCKResourceListener resourceListener;

    private FutureResult result;

    private String testName = "jndi";
}
