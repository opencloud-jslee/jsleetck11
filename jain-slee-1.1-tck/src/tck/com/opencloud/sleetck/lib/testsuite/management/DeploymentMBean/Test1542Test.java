/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.management.DeploymentMBean;

import com.opencloud.sleetck.lib.AbstractSleeTCKTest;
import com.opencloud.sleetck.lib.TCKTestResult;
import com.opencloud.sleetck.lib.testutils.jmx.DeploymentMBeanProxy;

import javax.slee.ComponentID;
import javax.slee.management.DeployableUnitDescriptor;
import javax.slee.management.DeployableUnitID;
import javax.slee.resource.ResourceAdaptorTypeID;

public class Test1542Test extends AbstractSleeTCKTest {

    private static final String SERVICE_DU_PATH_PARAM = "DUPath";
    private static final int TEST_ID = 1542;

    /**
     * Perform the actual test.
     */
    public TCKTestResult run() throws Exception {

        DeployableUnitID duID;

        // Install the Deployable Units
        String duPath = utils().getTestParams().getProperty(SERVICE_DU_PATH_PARAM);
        try {
            duID = utils().install(duPath);
        } catch (Exception e) {
            return TCKTestResult.failed(TEST_ID, "Failed to install deployable unit containing two different Resource Adaptor Types.");
        }

        DeploymentMBeanProxy duProxy = utils().getDeploymentMBeanProxy();
        DeployableUnitDescriptor duDesc = duProxy.getDescriptor(duID);
        ComponentID components[] = duDesc.getComponents();

        ResourceAdaptorTypeID ratypes[] = new ResourceAdaptorTypeID[2];

        for (int i = 0; i < components.length; i++) {
            if (components[i] instanceof ResourceAdaptorTypeID) {
                if (ratypes[0] == null) {
                    ratypes[0] = (ResourceAdaptorTypeID) components[i];
                    continue;
                }

                if (ratypes[1] == null) {
                    ratypes[1] = (ResourceAdaptorTypeID) components[i];
                    break;
                }
            }
        }

        if (ratypes[1] == null) {
            return TCKTestResult.failed(TEST_ID, "Failed to load both Resource Adaptor Types.");
        }

        return TCKTestResult.passed();
    }

    // Empty implementation
    public void setUp() throws Exception {}

}
