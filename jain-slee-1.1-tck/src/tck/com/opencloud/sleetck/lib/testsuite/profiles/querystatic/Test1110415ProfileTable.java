/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.profiles.querystatic;

import java.util.Collection;

import javax.slee.Address;
import javax.slee.profile.ProfileTable;

public interface Test1110415ProfileTable extends ProfileTable {

    public Collection queryAllParamTypesMatches1NotReadOnly(
            String StringParam,
            Character CharParam,
            Integer IntParam,
            Boolean BoolParam,
            Double DoubleParam,
            Byte ByteParam,
            Short ShortParam,
            Long LongParam,
            Float FloatParam,
            char charPrimitiveParam,
            int intPrimitiveParam,
            boolean boolPrimitiveParam,
            double doublePrimitiveParam,
            byte bytePrimitiveParam,
            short shortPrimitiveParam,
            long longPrimitiveParam,
            float floatPrimitiveParam);
    public Collection queryHasDescriptionNoMatchMaxIsReadOnly();
    public Collection queryDefaultReadOnlyNoMaxMatches();
    public Collection queryNoDescriptionNoOptions();


    public Collection queryCompareIntEquals();
    public Collection queryCompareIntNotEquals();
    public Collection queryCompareIntLessThan();
    public Collection queryCompareIntLessThanOrEquals();
    public Collection queryCompareIntGreaterThan();
    public Collection queryCompareIntGreaterThanOrEquals();


    public Collection queryCompareStringValue();
    public Collection queryCompareStringParam(String param);
    public Collection queryCompareIntObj();
    public Collection queryCompareCharObj();
    public Collection queryCompareBoolObj();
    public Collection queryCompareDoubleObj();
    public Collection queryCompareByteObj();
    public Collection queryCompareShortObj();
    public Collection queryCompareLongObj();
    public Collection queryCompareFloatObj();
    public Collection queryCompareInt();
    public Collection queryCompareChar();
    public Collection queryCompareBool();
    public Collection queryCompareDouble();
    public Collection queryCompareByte();
    public Collection queryCompareShort();
    public Collection queryCompareLong();
    public Collection queryCompareFloat();
    public Collection queryCompareAddress(Address param);

    public Collection queryLongestPrefixMatchValue();
    public Collection queryLongestPrefixMatchParam(String param);


    public Collection queryHasPrefixValue();
    public Collection queryHasPrefixParam(String param);


    public Collection queryRangeMatchStringValue();
    public Collection queryRangeMatchStringParam(String from_param, String to_param);
    public Collection queryRangeMatchStringParamValueMix(String from_param);

    public Collection queryRangeMatchIntObj();
    public Collection queryRangeMatchCharObj();
    public Collection queryRangeMatchBoolObj();
    public Collection queryRangeMatchDoubleObj();
    public Collection queryRangeMatchByteObj();
    public Collection queryRangeMatchShortObj();
    public Collection queryRangeMatchLongObj();
    public Collection queryRangeMatchFloatObj();
    public Collection queryRangeMatchInt();
    public Collection queryRangeMatchChar();
    public Collection queryRangeMatchBool();
    public Collection queryRangeMatchDouble();
    public Collection queryRangeMatchByte();
    public Collection queryRangeMatchShort();
    public Collection queryRangeMatchLong();
    public Collection queryRangeMatchFloat();


    public Collection queryAnd2Compares();
    public Collection queryAndMoreElements();


    public Collection queryOr2Compares();
    public Collection queryOrMoreElements();


    public Collection queryNot1Compare();
}
