/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.transactions;

import com.opencloud.sleetck.lib.resource.events.TCKResourceEventX;
import com.opencloud.sleetck.lib.sbbutils.BaseTCKSbb;
import com.opencloud.sleetck.lib.sbbutils.TCKSbbUtils;

import javax.slee.ActivityContextInterface;
import javax.slee.facilities.ActivityContextNamingFacility;
import java.util.HashMap;

public abstract class Test866Sbb extends BaseTCKSbb {

    private static final String ACTIVITY_NAME = "Test866Activity";

    public void sbbRolledBack(javax.slee.RolledBackContext context) {
    }

    // Initial event
    public void onTCKResourceEventX1(TCKResourceEventX ev,ActivityContextInterface aci) {

        try {
            ActivityContextNamingFacility facility = (ActivityContextNamingFacility) TCKSbbUtils.getSbbEnvironment().lookup("slee/facilities/activitycontextnaming");
            facility.bind(aci, ACTIVITY_NAME);

            getSbbContext().setRollbackOnly();

        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }
    }

    public void onTCKResourceEventX2(TCKResourceEventX ev,ActivityContextInterface aci) {

        try {
            HashMap map = new HashMap();

            ActivityContextNamingFacility facility = (ActivityContextNamingFacility) TCKSbbUtils.getSbbEnvironment().lookup("slee/facilities/activitycontextnaming");
            ActivityContextInterface theAci = facility.lookup(ACTIVITY_NAME);
            if (theAci == null) {
                map.put("Result", new Boolean(true));
                map.put("Message", "Ok");
            } else {
                map.put("Result", new Boolean(false));
                map.put("Message","ActivityContextNamingFacility.bind() did not execute in the same transaction as the method that called it.");
            }

            TCKSbbUtils.getResourceInterface().sendSbbMessage(map);
        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }
    }

}
