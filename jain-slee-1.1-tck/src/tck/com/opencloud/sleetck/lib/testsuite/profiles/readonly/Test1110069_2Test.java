/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.profiles.readonly;

import com.opencloud.sleetck.lib.testsuite.profiles.ProfileRAInteractionBaseTest;

/**
  * This test checks whether profiles can be created, updated, removed by RA's for a profile spec
  * with 'profile-read-only' attribute set to 'false'.
 */
public class Test1110069_2Test extends ProfileRAInteractionBaseTest {

    private static final String RA_NAME = "Test1110069_2RA";
    private static final String SPEC_NAME = "Test1110069_2Profile";

    public String getRAName() {
        return RA_NAME;
    }

    //dont create any profiles during setup phase, all profiles are created in the
    //RA's actual test code
    public String[] getProfileNames(String profileTableName) {
        return new String[]{};
    }

    public String[] getProfileTableNames() {
        return new String[]{Test1110069_2MessageListener.PROFILE_TABLE_NAME};
    }

    public String getSpecName() {
        return SPEC_NAME;
    }
}
