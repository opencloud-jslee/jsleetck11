/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.facilities.tracefacility.tracer;

import java.util.HashMap;

import javax.slee.ActivityContextInterface;
import javax.slee.facilities.TraceLevel;
import javax.slee.facilities.Tracer;

import com.opencloud.sleetck.lib.resource.events.TCKResourceEventX;
import com.opencloud.sleetck.lib.sbbutils.BaseTCKSbb;
import com.opencloud.sleetck.lib.sbbutils.TCKSbbUtils;

/*
 * AssertionID (1113432): Test these utility methods are the equivalent 
 * of calling the isTraceable method with the trace level specified in 
 * the method name.
 */
public abstract class Test1113432Sbb extends BaseTCKSbb {
    
    // Initial event
    public void onTCKResourceEventX1(TCKResourceEventX event, ActivityContextInterface aci) {

        try {
            sbbTracer = getSbbContext().getTracer("com.test");
            sbbTracer.info("Received "+event+" message",null);

            doTest1113432Test();
        } catch(Exception e) {
            TCKSbbUtils.handleException(e);
        }
    }

    private void sendResultToTCK(String testName, boolean result, int failedAssertionID,  String message) throws Exception {
        HashMap sbbData = new HashMap();
        sbbData.put("testname", testName);
        sbbData.put("result", result ? "pass" : "fail");
        sbbData.put("message", message);
        sbbData.put("id", new Integer(failedAssertionID));
        TCKSbbUtils.getResourceInterface().sendSbbMessage(sbbData);
    }

    private void doTest1113432Test() throws Exception {
        Tracer tracer = null; 

        //1113432:SEVERE
        try {
            tracer = getSbbContext().getTracer("com.test");
            
            if (tracer.isSevereEnabled() != tracer.isTraceable(TraceLevel.SEVERE)) {
                sendResultToTCK("Test1113432Test", false, 1113432, "This isSevereEnabled() method didn't equivalent to " +
                                "isTraceable(TraceLevel.SEVERE) method");
                return;
            }
            
            sbbTracer.info("got expected value of the isSevereEnabled() method which is equivalent to " +
                                "isTraceable(TraceLevel.SEVERE) method.", null);
        }catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }
        
        //1113432:WARNING
        try {
            tracer = getSbbContext().getTracer("com.test");
            
            if (tracer.isWarningEnabled() != tracer.isTraceable(TraceLevel.WARNING)) {
                sendResultToTCK("Test1113432Test", false, 1113432, "This isWarningEnabled() method didn't equivalent to " +
                                "isTraceable(TraceLevel.WARNING) method");
                return;
            }
            
            sbbTracer.info("got expected value of the isWarningEnabled() method which is equivalent to " +
                                "isTraceable(TraceLevel.WARNING) method.", null);
        }catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }
        
        //1113432:INFO
        try {
            tracer = getSbbContext().getTracer("com.test");
            
            if (tracer.isInfoEnabled() != tracer.isTraceable(TraceLevel.INFO)) {
                sendResultToTCK("Test1113432Test", false, 1113432, "This isInfoEnabled() method didn't equivalent to " +
                                "isTraceable(TraceLevel.INFO) method");
                return;
            }
            
            sbbTracer.info("got expected value of the isInfoEnabled() method which is equivalent to " +
                                "isTraceable(TraceLevel.INFO) method.", null);
        }catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }
        
        //1113432:CONFIG
        try {
            tracer = getSbbContext().getTracer("com.test");
            
            if (tracer.isConfigEnabled() != tracer.isTraceable(TraceLevel.CONFIG)) {
                sendResultToTCK("Test1113432Test", false, 1113432, "This isConfigEnabled() method didn't equivalent to " +
                                "isTraceable(TraceLevel.CONFIG) method");
                return;
            }
            
            sbbTracer.info("got expected value of the isConfigEnabled() method which is equivalent to " +
                                "isTraceable(TraceLevel.CONFIG) method.", null);
        }catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }
        
        //1113432:FINE
        try {
            tracer = getSbbContext().getTracer("com.test");
            
            if (tracer.isFineEnabled() != tracer.isTraceable(TraceLevel.FINE)) {
                sendResultToTCK("Test1113432Test", false, 1113432, "This isFineEnabled() method didn't equivalent to " +
                                "isTraceable(TraceLevel.FINE) method");
                return;
            }
            
            sbbTracer.info("got expected value of the isFineEnabled() method which is equivalent to " +
                                "isTraceable(TraceLevel.FINE) method.", null);
        }catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }
        
        //1113432:FINER
        try {
            tracer = getSbbContext().getTracer("com.test");
            
            if (tracer.isFinerEnabled() != tracer.isTraceable(TraceLevel.FINER)) {
                sendResultToTCK("Test1113432Test", false, 1113432, "This isFinerEnabled() method didn't equivalent to " +
                                "isTraceable(TraceLevel.FINER) method");
                return;
            }
            
            sbbTracer.info("got expected value of the isFinerEnabled() method which is equivalent to " +
                                "isTraceable(TraceLevel.FINER) method.", null);
        }catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }
        
        //1113432:FINEST
        try {
            tracer = getSbbContext().getTracer("com.test");
            
            if (tracer.isFinestEnabled() != tracer.isTraceable(TraceLevel.FINEST)) {
                sendResultToTCK("Test1113432Test", false, 1113432, "This isFinestEnabled() method didn't equivalent to " +
                                "isTraceable(TraceLevel.FINEST) method");
                return;
            }
            
            sbbTracer.info("got expected value of the isFinestEnabled() method which is equivalent to " +
                                "isTraceable(TraceLevel.FINEST) method.", null);
        }catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }
        
        boolean passed = false;
        //1113193
        try {
            tracer = getSbbContext().getTracer("com.test");
            // The trace method throws a java.lang.NullPointerException if message is null.
            if (tracer.isSevereEnabled())
                tracer.severe(null);
        }
        catch (java.lang.NullPointerException e) {
            sbbTracer.info("got expected NullPointerException", null);
            passed = true;
        }
        catch (Exception e) {
        TCKSbbUtils.handleException(e);
        } 
        
        if (!passed) { 
            sendResultToTCK("Test1113432Test", false, 1113193, "tracer.Severe(null) should have thrown java.lang.NullPointerException.");
            return;
        }
        
        //1113201
        try {
            tracer = getSbbContext().getTracer("com.test");
            // The trace method throws a java.lang.NullPointerException if message is null.
            if (tracer.isSevereEnabled())
                tracer.severe(null,null);
        }
        catch (java.lang.NullPointerException e) {
            sbbTracer.info("got expected NullPointerException", null);
            passed = true;
        }
        catch (Exception e) {
        TCKSbbUtils.handleException(e);
        } 
        
        if (!passed) { 
            sendResultToTCK("Test1113432Test", false, 1113201, "tracer.Severe(null,null) should have thrown java.lang.NullPointerException.");
            return;
        }
        
        //1113215
        try {
            tracer = getSbbContext().getTracer("com.test");
            // The trace method throws a java.lang.NullPointerException if message is null.
            if (tracer.isWarningEnabled())
                tracer.warning(null);
        }
        catch (java.lang.NullPointerException e) {
            sbbTracer.info("got expected NullPointerException", null);
            passed = true;
        }
        catch (Exception e) {
        TCKSbbUtils.handleException(e);
        } 
        
        if (!passed) { 
            sendResultToTCK("Test1113432Test", false, 1113215, "tracer.warning(null) should have thrown java.lang.NullPointerException.");
            return;
        }
        
        //1113223
        try {
            tracer = getSbbContext().getTracer("com.test");
            // The trace method throws a java.lang.NullPointerException if message is null.
            if (tracer.isWarningEnabled())
                tracer.warning(null,null);
        }
        catch (java.lang.NullPointerException e) {
            sbbTracer.info("got expected NullPointerException", null);
            passed = true;
        }
        catch (Exception e) {
        TCKSbbUtils.handleException(e);
        } 
        
        if (!passed) { 
            sendResultToTCK("Test1113432Test", false, 1113223, "tracer.warning(null) should have thrown java.lang.NullPointerException.");
            return;
        }
        
        //1113237
        try {
            tracer = getSbbContext().getTracer("com.test");
            // The trace method throws a java.lang.NullPointerException if message is null.
            if (tracer.isInfoEnabled())
                tracer.info(null);
        }
        catch (java.lang.NullPointerException e) {
            sbbTracer.info("got expected NullPointerException", null);
            passed = true;
        }
        catch (Exception e) {
        TCKSbbUtils.handleException(e);
        } 
        
        if (!passed) { 
            sendResultToTCK("Test1113432Test", false, 1113237, "tracer.info(null) should have thrown java.lang.NullPointerException.");
            return;
        }
        
        //1113245
        try {
            tracer = getSbbContext().getTracer("com.test");
            // The trace method throws a java.lang.NullPointerException if message is null.
            if (tracer.isInfoEnabled())
                tracer.info(null,null);
        }
        catch (java.lang.NullPointerException e) {
            sbbTracer.info("got expected NullPointerException", null);
            passed = true;
        }
        catch (Exception e) {
        TCKSbbUtils.handleException(e);
        } 
        
        if (!passed) { 
            sendResultToTCK("Test1113432Test", false, 1113245, "tracer.info(null) should have thrown java.lang.NullPointerException.");
            return;
        }
        
        //1113259
        try {
            tracer = getSbbContext().getTracer("com.test");
            // The trace method throws a java.lang.NullPointerException if message is null.
            if (tracer.isConfigEnabled())
                tracer.config(null);
        }
        catch (java.lang.NullPointerException e) {
            sbbTracer.info("got expected NullPointerException", null);
            passed = true;
        }
        catch (Exception e) {
        TCKSbbUtils.handleException(e);
        } 
        
        if (!passed) { 
            sendResultToTCK("Test1113432Test", false, 1113259, "tracer.config(null) should have thrown java.lang.NullPointerException.");
            return;
        }
        
        //1113267
        try {
            tracer = getSbbContext().getTracer("com.test");
            // The trace method throws a java.lang.NullPointerException if message is null.
            if (tracer.isConfigEnabled())
                tracer.config(null,null);
        }
        catch (java.lang.NullPointerException e) {
            sbbTracer.info("got expected NullPointerException", null);
            passed = true;
        }
        catch (Exception e) {
        TCKSbbUtils.handleException(e);
        } 
        
        if (!passed) { 
            sendResultToTCK("Test1113432Test", false, 1113267, "tracer.config(null) should have thrown java.lang.NullPointerException.");
            return;
        }
        
        //1113281
        try {
            tracer = getSbbContext().getTracer("com.test");
            // The trace method throws a java.lang.NullPointerException if message is null.
            if (tracer.isFineEnabled())
                tracer.fine(null);
        }
        catch (java.lang.NullPointerException e) {
            sbbTracer.info("got expected NullPointerException", null);
            passed = true;
        }
        catch (Exception e) {
        TCKSbbUtils.handleException(e);
        } 
        
        if (!passed) { 
            sendResultToTCK("Test1113432Test", false, 1113281, "tracer.fine(null) should have thrown java.lang.NullPointerException.");
            return;
        }
        
        //1113289
        try {
            tracer = getSbbContext().getTracer("com.test");
            // The trace method throws a java.lang.NullPointerException if message is null.
            if (tracer.isFineEnabled())
                tracer.fine(null,null);
        }
        catch (java.lang.NullPointerException e) {
            sbbTracer.info("got expected NullPointerException", null);
            passed = true;
        }
        catch (Exception e) {
        TCKSbbUtils.handleException(e);
        } 
        
        if (!passed) { 
            sendResultToTCK("Test1113432Test", false, 1113289, "tracer.fine(null) should have thrown java.lang.NullPointerException.");
            return;
        }
        
        //1113303
        try {
            tracer = getSbbContext().getTracer("com.test");
            // The trace method throws a java.lang.NullPointerException if message is null.
            if (tracer.isFinerEnabled())
                tracer.finer(null);
        }
        catch (java.lang.NullPointerException e) {
            sbbTracer.info("got expected NullPointerException", null);
            passed = true;
        }
        catch (Exception e) {
        TCKSbbUtils.handleException(e);
        } 
        
        if (!passed) { 
            sendResultToTCK("Test1113432Test", false, 1113303, "tracer.finer(null) should have thrown java.lang.NullPointerException.");
            return;
        }
        
        //1113311
        try {
            tracer = getSbbContext().getTracer("com.test");
            // The trace method throws a java.lang.NullPointerException if message is null.
            if (tracer.isFinerEnabled())
                tracer.finer(null,null);
        }
        catch (java.lang.NullPointerException e) {
            sbbTracer.info("got expected NullPointerException", null);
            passed = true;
        }
        catch (Exception e) {
        TCKSbbUtils.handleException(e);
        } 
        
        if (!passed) { 
            sendResultToTCK("Test1113432Test", false, 1113311, "tracer.finer(null) should have thrown java.lang.NullPointerException.");
            return;
        }
                
        
        //1113325
        try {
            tracer = getSbbContext().getTracer("com.test");
            // The trace method throws a java.lang.NullPointerException if message is null.
            if (tracer.isFinestEnabled())
                tracer.finest(null);
        }
        catch (java.lang.NullPointerException e) {
            sbbTracer.info("got expected NullPointerException", null);
            passed = true;
        }
        catch (Exception e) {
        TCKSbbUtils.handleException(e);
        } 
        
        if (!passed) { 
            sendResultToTCK("Test1113432Test", false, 1113325, "tracer.finest(null) should have thrown java.lang.NullPointerException.");
            return;
        }
        
        //1113333
        try {
            tracer = getSbbContext().getTracer("com.test");
            // The trace method throws a java.lang.NullPointerException if message is null.
            if (tracer.isFinestEnabled())
                tracer.finest(null,null);
        }
        catch (java.lang.NullPointerException e) {
            sbbTracer.info("got expected NullPointerException", null);
            passed = true;
        }
        catch (Exception e) {
        TCKSbbUtils.handleException(e);
        } 
        
        if (!passed) { 
            sendResultToTCK("Test1113432Test", false, 1113333, "tracer.finest(null) should have thrown java.lang.NullPointerException.");
            return;
        }
        
        sendResultToTCK("Test1113432Test", true, 1113432,"This Tracer.isTraceable method tests passed!");
    }

    private Tracer sbbTracer;
}

