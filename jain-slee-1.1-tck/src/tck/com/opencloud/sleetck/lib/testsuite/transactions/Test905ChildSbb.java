/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.transactions;

import java.util.HashMap;

import javax.slee.ActivityContextInterface;

import com.opencloud.sleetck.lib.sbbutils.BaseTCKSbb;
import com.opencloud.sleetck.lib.sbbutils.TCKSbbUtils;

public abstract class Test905ChildSbb extends BaseTCKSbb {

    public static final String MESSAGE = "Test905RuntimeException";

    public void sbbExceptionThrown(Exception exception, Object event, ActivityContextInterface aci) {

        if (exception instanceof java.lang.RuntimeException && exception.getMessage().equals(MESSAGE)) {
            // Assertion 906 - sbbExceptionThrown() is invoked.
            try {
                HashMap map = new HashMap();
                map.put("Type", "ExceptionThrown");
                map.put("Data", new Boolean(true));
                TCKSbbUtils.getResourceInterface().sendSbbMessage(map);
            } catch (Exception e) {
                TCKSbbUtils.handleException(e);
            }
            return;
        }

        super.sbbExceptionThrown(exception, event, aci);
    }

    public void sbbRolledBack(javax.slee.RolledBackContext context) {
        // TXN ID data for assertion 909
        try {
            HashMap map = new HashMap();
            map.put("Type", "TXN2");
            map.put("Data", TCKSbbUtils.getResourceAdaptorInterface().getTransactionIDAccess().getCurrentTransactionID());
            TCKSbbUtils.getResourceInterface().sendSbbMessage(map);
        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }
    }

    public void onTest905Event(Test905Event event, ActivityContextInterface aci) {
        // Set CMP field on the parent, which should get rolled back.
        getRealParentSbbLocalObject().setInteger(42);

        // Send the TXN ID back to the TCK in preparation for assertion 909.
        try {
            HashMap map = new HashMap();
            map.put("Type", "TXN");
            map.put("Data", TCKSbbUtils.getResourceAdaptorInterface().getTransactionIDAccess().getCurrentTransactionID());
            TCKSbbUtils.getResourceInterface().sendSbbMessage(map);
        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }

        // Throw RuntimeException to trigger the test case.
        throw new RuntimeException(MESSAGE);
    }

    public void setParentSbbLocalObject(Test905SbbLocalObject sbbLocalObject) {
        setRealParentSbbLocalObject(sbbLocalObject);
    }

    public abstract void setRealParentSbbLocalObject(Test905SbbLocalObject sbbLocalObject);
    public abstract Test905SbbLocalObject getRealParentSbbLocalObject();
}
