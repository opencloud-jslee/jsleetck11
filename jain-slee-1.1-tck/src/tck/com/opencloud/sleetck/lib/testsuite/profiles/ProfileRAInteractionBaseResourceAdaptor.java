/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.profiles;

import java.rmi.RemoteException;

import javax.slee.Address;
import javax.slee.ServiceID;
import javax.slee.resource.ActivityHandle;
import javax.slee.resource.ConfigProperties;
import javax.slee.resource.FailureReason;
import javax.slee.resource.FireableEventType;
import javax.slee.resource.InvalidConfigurationException;
import javax.slee.resource.Marshaler;
import javax.slee.resource.ReceivableService;
import javax.slee.resource.ResourceAdaptorContext;

import com.opencloud.sleetck.lib.rautils.BaseTCKRA;
import com.opencloud.sleetck.lib.rautils.MessageHandler;
import com.opencloud.sleetck.lib.rautils.MessageHandlerRegistry;
import com.opencloud.sleetck.lib.rautils.TCKRAUtils;
import com.opencloud.sleetck.lib.rautils.TraceLogger;

/**
 * Resource adaptor which works as Slee-side receptor for TCK test messages as
 * only RA's have access to TXN manager.
 */
public abstract class ProfileRAInteractionBaseResourceAdaptor extends BaseTCKRA {

    protected abstract MessageHandler getMessageHandler() throws RemoteException;

    public void setResourceAdaptorContext(ResourceAdaptorContext context) {
        super.setResourceAdaptorContext(context);
        setTracer(context.getTracer("Profile test RA"));
        try {
            MessageHandlerRegistry registry = TCKRAUtils.lookupMessageHandlerRegistry();
            messageHandler = getMessageHandler();
            registry.registerMessageHandler(messageHandler);
        } catch (Exception e) {
            getLog().severe("An error occured during setResourceAdaptorContext()", e);
        }
    }

    public void unsetResourceAdaptorContext() {
        try {
            MessageHandlerRegistry registry = TCKRAUtils.lookupMessageHandlerRegistry();
            if (messageHandler != null)
                registry.unregisterMessageHandler(messageHandler);
        } catch (Exception e) {
            getLog().severe("An error occured during unsetResourceAdaptorContext()", e);
        }
        setTracer(null);
        super.unsetResourceAdaptorContext();
    }

    public void activityEnded(ActivityHandle arg0) {
    }

    public void activityUnreferenced(ActivityHandle arg0) {
    }

    public void administrativeRemove(ActivityHandle arg0) {
    }

    public void raConfigurationUpdate(ConfigProperties arg0) {
    }

    public void raConfigure(ConfigProperties arg0) {
    }

    public void raUnconfigure() {
    }

    public void raActive() {
    }

    public void raInactive() {
    }

    public void raStopping() {
    }

    public void raVerifyConfiguration(ConfigProperties arg0) throws InvalidConfigurationException {
    }

    public void eventProcessingFailed(ActivityHandle handle, FireableEventType eventType, Object event, Address address, ReceivableService serviceInfo, int flags, FailureReason reason) {
    }

    public void eventProcessingSuccessful(ActivityHandle handle, FireableEventType eventType, Object event, Address address, ReceivableService serviceInfo, int flags) {
    }

    public void eventUnreferenced(ActivityHandle handle, FireableEventType eventType, Object event, Address address, ReceivableService serviceInfo, int flags) {
    }

    public Object getActivity(ActivityHandle arg0) {
        return null;
    }

    public ActivityHandle getActivityHandle(Object arg0) {
        return null;
    }

    public Marshaler getMarshaler() {
        return null;
    }

    public void queryLiveness(ActivityHandle arg0) {
    }

    public void serviceActive(ReceivableService serviceInfo) {
    }

    public void serviceInactive(ReceivableService serviceInfo) {
    }

    public void serviceStopping(ReceivableService serviceInfo) {
    }

    public TraceLogger getLog() {
        return super.getLog();
    }

    private MessageHandler messageHandler = null;
}
