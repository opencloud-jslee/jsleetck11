/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.profiles;

import java.util.HashMap;

import javax.management.ObjectName;
import javax.slee.profile.ProfileSpecificationID;
import javax.slee.resource.ConfigProperties;
import javax.slee.resource.ResourceAdaptorID;

import com.opencloud.sleetck.lib.AbstractSleeTCKTest;
import com.opencloud.sleetck.lib.TCKTestFailureException;
import com.opencloud.sleetck.lib.TCKTestResult;
import com.opencloud.sleetck.lib.infra.SleeTCKComponentConstants;
import com.opencloud.sleetck.lib.profileutils.BaseMessageAdapter;
import com.opencloud.sleetck.lib.rautils.MessageHandlerRegistry;
import com.opencloud.sleetck.lib.rautils.RMIObjectChannel;
import com.opencloud.sleetck.lib.testutils.FutureResult;
import com.opencloud.sleetck.lib.testutils.ProfileUtils;
import com.opencloud.sleetck.lib.testutils.jmx.ProfileMBeanProxy;
import com.opencloud.sleetck.lib.testutils.jmx.ProfileProvisioningMBeanProxy;
import com.opencloud.sleetck.lib.testutils.jmx.ResourceManagementMBeanProxy;

/**
 *
 */
public abstract class ProfileRAInteractionBaseTest extends AbstractSleeTCKTest {

    private static final String RA_VENDOR = SleeTCKComponentConstants.TCK_VENDOR;
    private static final String RA_VERSION = "1.1";
    private static final String SPEC_VERSION = "1.0";

    private static final String PROFILE_SPEC_DU_PATH_PARAM = "ProfileSpecDUPath";
    private static final String RESOURCE_DU_PATH_PARAM = "RADUPath";

    public abstract String getRAName();
    public abstract String getSpecName();
    public abstract String[] getProfileTableNames();
    public abstract String[] getProfileNames(String profileTableName);


    public TCKTestResult run() throws Exception {

        String raName = getRAName();
        String raEntityName = raName + "_Entity";

        futureResult = new FutureResult(getLog());

        ResourceManagementMBeanProxy resourceMBean = utils().getResourceManagementMBeanProxy();

        // Create the RA Entity.
        ResourceAdaptorID raID = new ResourceAdaptorID(raName, RA_VENDOR, RA_VERSION);
        ConfigProperties configProperties = new ConfigProperties();
        resourceMBean.createResourceAdaptorEntity(raID, raEntityName, configProperties);

        // Activate the RA Entity
        resourceMBean.activateResourceAdaptorEntity(raEntityName);

        HashMap map = new HashMap();
        map.put("Type", new Integer(1));

        //send message to RA entity which starts doing the TXN tests.
        out.sendMessage(map);

        return futureResult.waitForResult(utils().getTestTimeout());
    }

    private void setupProfile(String profileTableName, String profileName) throws Exception {
        //Create a profile via management view
        ObjectName profile = profileProvisioning.createProfile(profileTableName, profileName);
        ProfileMBeanProxy profileProxy = utils().getMBeanProxyFactory().createProfileMBeanProxy(profile);
        getLog().fine("Created profile "+profileName+" for profile table "+profileTableName);

        //commit and close the Profile
        profileProxy.commitProfile();
        profileProxy.closeProfile();
        getLog().fine("Commit and close profile "+profileName);
    }

    private void setupProfileTable(String profileTableName) throws Exception {
        String specName = getSpecName();

        ProfileSpecificationID specID = new ProfileSpecificationID(specName, SleeTCKComponentConstants.TCK_VENDOR, SPEC_VERSION);
        profileProvisioning.createProfileTable(specID, profileTableName);
        getLog().fine("Added profile table "+profileTableName);

        String[] profiles = getProfileNames(profileTableName);
        for (int i=0; i<profiles.length; i++)
            setupProfile(profileTableName, profiles[i]);
    }

    public void tearDown() throws Exception {
        try {
            in.clearQueue();

            utils().removeRAEntities();

            String[] profileTables = getProfileTableNames();
            try {
                for (int i = 0; i < profileTables.length; i++)
                    profileUtils.removeProfileTable(profileTables[i]);
            }
            catch (Exception e) {
                getLog().warning("Exception when trying to remove profile table: ");
                getLog().warning(e);
            }

        } finally {
            super.tearDown();
        }

    }

    protected void setPassed(int assertionID, String msg) {
        getLog().fine(assertionID+": "+msg);
        if (futureResult!=null && !futureResult.isSet())
            futureResult.setPassed();
    }

    protected void setFailed(int assertionID, String msg, Exception e) {
        if (e==null) {
            getLog().fine("FAILURE: "+assertionID+":"+msg);
            if (futureResult!=null && !futureResult.isSet())
                futureResult.setFailed(assertionID, msg);
        }
        else {
            getLog().fine("FAILURE: "+assertionID+":"+msg);
            getLog().fine(e);
            if (futureResult!=null && !futureResult.isSet())
                futureResult.setFailed(new TCKTestFailureException(assertionID, msg, e));
        }
    }

    protected void setError(String msg, Exception e) {
        if (e==null) {
            getLog().warning(msg);
            if (futureResult!=null && !futureResult.isSet())
                futureResult.setError(msg);
        }
        else {
            getLog().warning(msg);
            getLog().warning(e);
            if (futureResult!=null && !futureResult.isSet())
                futureResult.setError(msg, e);
        }
    }


    public void setUp() throws Exception {
        setupService(PROFILE_SPEC_DU_PATH_PARAM);

        profileUtils = new ProfileUtils(utils());
        profileProvisioning = profileUtils.getProfileProvisioningProxy();

        setupService(RESOURCE_DU_PATH_PARAM);

        in = utils().getRMIObjectChannel();
        in.setMessageHandler(new BaseMessageAdapter(getLog()) {
            public void onSetPassed(int assertionID, String msg) { setPassed(assertionID, msg); }
            public void onSetFailed(int assertionID, String msg, Exception e) { setFailed(assertionID, msg, e); }
            public void onLogCall(String msg) { getLog().fine(msg); }
            public void onSetError(String msg, Exception e) { setError(msg, e); }
        });

        out = utils().getMessageHandlerRegistry();

        //Create and setup profile tables and profiles
        String[] profileTables = getProfileTableNames();
        for (int i=0; i<profileTables.length; i++) {
            getLog().fine("Starting to setup table: "+profileTables[i]);
            setupProfileTable(profileTables[i]);
        }

    }

    private MessageHandlerRegistry out;
    private RMIObjectChannel in;
    private FutureResult futureResult;
    private ProfileUtils profileUtils;
    private ProfileProvisioningMBeanProxy profileProvisioning;

}
