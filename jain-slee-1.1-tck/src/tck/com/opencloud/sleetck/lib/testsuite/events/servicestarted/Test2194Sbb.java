/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.events.servicestarted;

import com.opencloud.sleetck.lib.sbbutils.BaseTCKSbb;
import com.opencloud.sleetck.lib.sbbutils.TCKSbbUtils;

import javax.slee.ActivityContextInterface;
import javax.slee.ActivityEndEvent;
import javax.slee.Address;
import javax.slee.facilities.Level;
import javax.slee.serviceactivity.ServiceActivity;
import javax.slee.serviceactivity.ServiceStartedEvent;
import java.util.HashMap;

public abstract class Test2194Sbb extends BaseTCKSbb {

    public void onActivityEndEvent(ActivityEndEvent event, ActivityContextInterface aci) {
        try {
            TCKSbbUtils.createTrace(getSbbID(), Level.FINE, "In ActivityEndEvent on Activity " + aci.getActivity(), null);
            if (aci.getActivity() instanceof ServiceActivity) {
                HashMap map = new HashMap();
                map.put("Result", new Boolean(true));
                // Can't fire on an Activity that's ending
                TCKSbbUtils.getResourceInterface().sendSbbMessage(map);
            }
        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }
    }

    // Initial event
    public void onServiceStartedEvent(ServiceStartedEvent event, ActivityContextInterface aci) {
        try {
            TCKSbbUtils.createTrace(getSbbID(), Level.FINE, "In ServiceStartedEvent on Activity " + aci.getActivity(), null);
            setResult(new HashMap());
            fireSendResultEvent(new SendResultEvent(), aci, null);
        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }
    }

    public void onSendResultEvent(SendResultEvent event, ActivityContextInterface aci) {
        try {
            TCKSbbUtils.createTrace(getSbbID(), Level.FINE, "In SendResultEvent on Activity " + aci.getActivity(), null);
            TCKSbbUtils.getResourceInterface().sendSbbMessage(getResult());
        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }
    }

    public abstract void fireSendResultEvent(SendResultEvent event, ActivityContextInterface aci, Address address);

    public abstract void setResult(HashMap map);
    public abstract HashMap getResult();

}
