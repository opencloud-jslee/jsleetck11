/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.events.eventcontext;

import javax.slee.ActivityContextInterface;
import javax.slee.InitialEventSelector;
import javax.slee.EventContext;
import javax.slee.facilities.Tracer;

import com.opencloud.sleetck.lib.sbbutils.BaseTCKSbb;
import com.opencloud.sleetck.lib.sbbutils.TCKSbbUtils;
import com.opencloud.sleetck.lib.resource.TCKActivityID;
import com.opencloud.sleetck.lib.resource.events.TCKResourceEventX;
import com.opencloud.sleetck.lib.resource.sbbapi.TCKActivity;

import java.util.HashMap;

/**
 * AssertionID(1108043): Test If the transaction does not commit then the 
 * Event Context will not be “resumed” and therefore the event will not be 
 * delivered to any other eligible SBB entities.
 */
public abstract class Test1108042Sbb1 extends BaseTCKSbb {
    public static final String TRACE_MESSAGE_TCKResourceEventX1 = "Test1108042Sbb1:I got TCKResourceEventX1 on ActivityA";

    public static final String TRACE_MESSAGE_TCKResourceEventX2A = "Test1108042Sbb1:I got TCKResourceEventX2 on ActivityA";

    public static final String TRACE_MESSAGE_TCKResourceEventX2B = "Test1108042Sbb1:I got TCKResourceEventX2 on ActivityB";

    public static final String TRACE_MESSAGE_TCKResourceEventX3A = "Test1108042Sbb1:I got TCKResourceEventX3 on ActivityA";

    public static final String TRACE_MESSAGE_TCKResourceEventX3B = "Test1108042Sbb1:I got TCKResourceEventX3 on ActivityB";

    public InitialEventSelector initialEventSelector(InitialEventSelector ies) {
        ies.setCustomName("test");
        return ies;
    }

    public void onTCKResourceEventX1(TCKResourceEventX event, ActivityContextInterface aci, EventContext context) {
        try {
            //send message to TCK: I got TCKResourceEventX1 on ActivityA
            tracer = getSbbContext().getTracer("com.test");
            tracer.info(TRACE_MESSAGE_TCKResourceEventX1);
            
            setEventContext(context);
            //call suspendDelivery() method now, we assumed the default timeout 
            //on the SLEE will be more than 10000ms to complete this test.
            context.suspendDelivery();

        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }
    }

    public void onTCKResourceEventX2(TCKResourceEventX event, ActivityContextInterface aci, EventContext context) {
        try {
            tracer = getSbbContext().getTracer("com.test");

            TCKActivity activity = (TCKActivity) context.getActivityContextInterface().getActivity();
            TCKActivityID activityID = activity.getID();
            if (activityID.getName().equals(Test1108042Test.activityNameB)) {

                //send message to TCK: I got TCKResourceEventX2 on ActivityB
                tracer.info(TRACE_MESSAGE_TCKResourceEventX2B);

                try {
                    EventContext ec = getEventContext();
                    if (ec.isSuspended()) {
                        //call resumeDelivery method now
                        ec.resumeDelivery();
                    }
                }
                catch (Exception e) {
                    tracer.severe("ERROR onTCKResourceEventX2, the event delivery failed to be resumed");
                    sendResultToTCK("Test1108042Test", false, "SBB1:onTCKResourceEventX2-ERROR: The event delivery failed to be resumed", 1108043);
                    return;
                }

                return;
            } else if (activityID.getName().equals(Test1108042Test.activityNameA)) {
                //send message to TCK: I got TCKResourceEventX2 on ActivityA
                tracer = getSbbContext().getTracer("com.test");
                tracer.info(TRACE_MESSAGE_TCKResourceEventX2A);
                return;
            }

        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }
    }

    public void onTCKResourceEventX3(TCKResourceEventX event, ActivityContextInterface aci, EventContext context) {
        try {
            //send message to TCK: I got TCKResourceEventX3 on ActivityA
            tracer = getSbbContext().getTracer("com.test");

            TCKActivity activity = (TCKActivity) context.getActivityContextInterface().getActivity();
            TCKActivityID activityID = activity.getID();
            if (activityID.getName().equals(Test1108042Test.activityNameA)) {
                tracer.info(TRACE_MESSAGE_TCKResourceEventX3A);

                //assert context.isSuspended(), check ec has been suspended or not 
                EventContext ec = getEventContext();
                if (ec != null && ec.isSuspended()) {
                    sendResultToTCK("Test1108042Test", false, "SBB1:onTCKResourceEventX3-ERROR: Sbb1 onTCKResourceEventX3, the event delivery has been suspended, the "
                            + "EventContext.isSuspended() returned true!", 1108043);
                } else
                    sendResultToTCK("Test1108042Test", true, "Sbb1: Test The event processing by the SLEE for this event is resumed "
                            + "if the transaction which invoked this method commits.", 1108043);
                return;
            } else if (activityID.getName().equals(Test1108042Test.activityNameB)) {
                tracer.info(TRACE_MESSAGE_TCKResourceEventX3B);
            }
        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }
    }

    public abstract void setEventContext(EventContext eventContext);
    public abstract EventContext getEventContext();


    private void sendResultToTCK(String testName, boolean result, String message, int failedAssertionID) throws Exception {
        HashMap sbbData = new HashMap();
        sbbData.put("testname", testName);
        sbbData.put("result", result ? "pass" : "fail");
        sbbData.put("message", message);
        sbbData.put("id", new Integer(failedAssertionID));
        TCKSbbUtils.getResourceInterface().sendSbbMessage(sbbData);
    }

    private Tracer tracer = null;
}
