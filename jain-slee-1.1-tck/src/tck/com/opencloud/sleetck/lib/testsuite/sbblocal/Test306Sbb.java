/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.sbblocal;

import javax.slee.ActivityEndEvent;
import javax.slee.ActivityContextInterface;
import javax.slee.SbbContext;
import javax.slee.Address;
import javax.slee.SbbLocalObject;
import javax.slee.facilities.Level;
import javax.slee.ChildRelation;

import com.opencloud.sleetck.lib.sbbutils.BaseTCKSbb;
import com.opencloud.sleetck.lib.sbbutils.TCKSbbUtils;
import com.opencloud.sleetck.lib.sbbutils.TCKSbbConstants;
import com.opencloud.sleetck.lib.resource.events.TCKResourceEventX;
import com.opencloud.sleetck.lib.resource.TCKActivityID;
import com.opencloud.sleetck.lib.resource.sbbapi.TCKActivity;
import com.opencloud.sleetck.lib.resource.sbbapi.TCKResourceSbbInterface;
import com.opencloud.sleetck.lib.resource.sbbapi.TCKActivityContextInterfaceFactory;

import java.util.HashMap;

/**
 * If an SBB does not define an SBB specific local interface, then
 * the SBB's local interface is SBBLocalObject.
 */

public abstract class Test306Sbb extends BaseTCKSbb {

    public void sbbRemove() {
        try {
            TCKSbbUtils.createTrace(getSbbID(), Level.FINE,
                                    "In sbbRemove", null);
            setSeenSbbRemove(true);

            HashMap map = new HashMap();
            if (getSeenSbbStore() && getSeenSbbRemove()) {
                map.put("Result", new Boolean(true));
                map.put("Message", "Ok");
            } else {
                map.put("Result", new Boolean(false));
                map.put("Message", "Did not see calls to sbbStore() and sbbRemove() during SBB removal.");
            }
            TCKSbbUtils.getResourceInterface().sendSbbMessage(map);

        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }
    }

    public void sbbStore() {
        try {
            TCKSbbUtils.createTrace(getSbbID(), Level.FINE,
                                    "In sbbStore", null);
            setSeenSbbStore(true);
        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }
    }

    public void onTCKResourceEventX1(TCKResourceEventX event, ActivityContextInterface aci) {
        try {
            setSeenSbbStore(false);
            //            getSbbContext().getSbbLocalObject().remove();
        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }
    }

    public abstract boolean getSeenSbbRemove();
    public abstract void setSeenSbbRemove(boolean b);
    public abstract boolean getSeenSbbStore();
    public abstract void setSeenSbbStore(boolean b);

}
