/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.transactions;

import com.opencloud.sleetck.lib.resource.events.TCKResourceEventX;
import com.opencloud.sleetck.lib.sbbutils.BaseTCKSbb;
import com.opencloud.sleetck.lib.sbbutils.TCKSbbUtils;

import javax.slee.ActivityContextInterface;
import javax.slee.RolledBackContext;
import javax.slee.facilities.Level;
import java.util.HashMap;

public abstract class Test1904Sbb extends BaseTCKSbb {

    // Initial event
    public void onTCKResourceEventX1(TCKResourceEventX ev, ActivityContextInterface aci) {

        try {
            setTransaction(TCKSbbUtils.getResourceAdaptorInterface().getTransactionIDAccess().getCurrentTransactionID());
        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
            return;
        }

        checkTransaction();

        HashMap map = new HashMap();

        if (getSbbContext().getRollbackOnly()) {
            map.put("Result", new Boolean(true));
            map.put("Message", "Ok");
        } else {
            map.put("Result", new Boolean(false));
            map.put("Message", "setRollbackOnly() was called from an SBB invoked mandatory transactional method, but the TXN was not marked for rollback after that method returned.");
        }
        try {
            TCKSbbUtils.getResourceInterface().sendSbbMessage(map);
        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }
    }

    public void checkTransaction() {
        try {
            getSbbContext().setRollbackOnly();
        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }
    }

    // Override sbbRolledBack to write a trace message rather than throw an Exception, as we expect to receive the call
    public void sbbRolledBack(RolledBackContext context) {
        createTraceSafe(Level.INFO,"Test1904Sbb:received sbbRolledBack() call");
    }

    public abstract void setTransaction(Object txnID);
    public abstract Object getTransaction();
}
