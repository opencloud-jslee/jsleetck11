/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.events.servicestarted;

import java.rmi.RemoteException;
import java.util.Map;

import javax.slee.ComponentID;
import javax.slee.ServiceID;
import javax.slee.management.DeployableUnitDescriptor;
import javax.slee.management.DeployableUnitID;
import javax.slee.management.ServiceState;

import com.opencloud.sleetck.lib.AbstractSleeTCKTest;
import com.opencloud.sleetck.lib.TCKTestFailureException;
import com.opencloud.sleetck.lib.resource.TCKActivityID;
import com.opencloud.sleetck.lib.resource.TCKSbbMessage;
import com.opencloud.sleetck.lib.resource.testapi.TCKResourceListener;
import com.opencloud.sleetck.lib.testutils.Assert;
import com.opencloud.sleetck.lib.testutils.BaseTCKResourceListener;
import com.opencloud.sleetck.lib.testutils.FutureResult;
import com.opencloud.sleetck.lib.testutils.jmx.DeploymentMBeanProxy;
import com.opencloud.sleetck.lib.testutils.jmx.ServiceManagementMBeanProxy;

/**
 * AssertionID(1108102): Test A new Service Started Event type is defined. 
 * This event is fired only to the starting service.
 *
 */
public class Test1108102Test extends AbstractSleeTCKTest {

    public static final String SERVICE1_DU_PATH_PARAM = "service1DUPath";

    public static final String SERVICE2_DU_PATH_PARAM = "service2DUPath";

    /**
     * Perform the actual test.
     */

    public void run(FutureResult result) throws Exception {
        this.result = result;

        DeploymentMBeanProxy duProxy = utils().getDeploymentMBeanProxy();
        ServiceManagementMBeanProxy serviceProxy = utils().getServiceManagementMBeanProxy();
        DeployableUnitDescriptor duDesc1 = duProxy.getDescriptor(duID1);
        ComponentID components1[] = duDesc1.getComponents();

        ServiceID firstService = null;
        ServiceID secondService = null;

        for (int i = 0; i < components1.length; i++) {
            if (components1[i] instanceof ServiceID) {
                ServiceID service = (ServiceID) components1[i];

                if (firstService == null) {
                    firstService = service;
                    continue;
                }
            }
        }

        if (firstService == null) {
            result.setError("Failed to the first test services.");
            return;
        }

        getLog().fine("Activating the first service");
        serviceProxy.activate(firstService);
        
        synchronized (this) {
            wait(3000);
        }
        
        if (!recieveServiceAFlag) {
            result.setFailed(1108102, "Test harness didn't receive a ServiceActivated event for the first service.");
            return;
        }
            
        if (!ServiceState.ACTIVE.equals(serviceProxy.getState(firstService))) {
            result.setError("First service " + firstService + " failed to activate.");
            return;
        }
        
        
        getLog().fine("The service " + firstService + " is in Active state");
        
        DeployableUnitDescriptor duDesc2 = duProxy.getDescriptor(duID2);
        ComponentID components2[] = duDesc2.getComponents();
        
        for (int i = 0; i < components2.length; i++) {
            if (components2[i] instanceof ServiceID) {
                ServiceID service = (ServiceID) components2[i];
                
                if (secondService == null) {
                    secondService = service;
                    break;
                }
            }
        }
        
        if (secondService == null) {
            result.setError("Failed to the second test services.");
            return;
        }
        
        if (!ServiceState.INACTIVE.equals(serviceProxy.getState(secondService))) {
            result.setError("Second service " + secondService + " should be inactive but was not.");
            return;
        }
        
        getLog().fine("Activating the second service");
        serviceProxy.activate(secondService);
        recieveServiceAFlag = false;
        
        synchronized (this) {
            wait(3000);
        }
        
        if (!recieveServiceAFlag && recieveServiceBFlag) {
            result.setPassed();
        }
        else {
            result.setFailed(1108102, "Test harness didn't receive a ServiceActivated event for the second service.");
        }
        

    }

    /**
     * Do all the pre-run configuration of the test.
     */

    public void setUp() throws Exception {
        getLog().fine("Connecting to resource");
        resourceListener = new TCKResourceListenerImpl();
        setResourceListener(resourceListener);
        getLog().fine("Installing service");

        String duPath1 = utils().getTestParams().getProperty(SERVICE1_DU_PATH_PARAM);
        String duPath2 = utils().getTestParams().getProperty(SERVICE2_DU_PATH_PARAM);

        duID1 = utils().install(duPath1);
        duID2 = utils().install(duPath2);
    }

    /**
     * Clean up after the test.
     */

    public void tearDown() throws Exception {
        // cleanup
        super.tearDown();
    }

    private class TCKResourceListenerImpl extends BaseTCKResourceListener {
        // TCKResourceListener methods

        public synchronized void onSbbMessage(TCKSbbMessage message, TCKActivityID calledActivity)
                throws RemoteException {

            Map sbbData = (Map) message.getMessage();
            String sbbTestName = "Test1108102Test";
            String sbbTestResult = (String) sbbData.get("result");
            String sbbTestMessage = (String) sbbData.get("message");
            int assertionID = ((Integer) sbbData.get("id")).intValue();
            getLog().info(
                    "Received message from SBB: testname=" + sbbTestName + ", result=" + sbbTestResult + ", message="
                            + sbbTestMessage + ", id=" + assertionID);
            try {
                Assert.assertEquals(assertionID, "Test " + sbbTestName + " failed.", "pass", sbbTestResult);
                if (sbbTestMessage.contains("name=Test1108102Service1")) {
                    recieveServiceAFlag = true;
                }
                if (sbbTestMessage.contains("name=Test1108102Service2")) {
                    recieveServiceBFlag = true;
                }
            } catch (TCKTestFailureException ex) {
                result.setFailed(assertionID, sbbTestMessage);
            }
        }

        public void onException(Exception exception) throws RemoteException {
            getLog().warning("Received Exception from SBB or resource:");
            getLog().warning(exception);
            result.setError(exception);
        }
    }

    private TCKResourceListener resourceListener;

    private FutureResult result;

    private DeployableUnitID duID1, duID2;
    
    private boolean recieveServiceAFlag = false;
    private boolean recieveServiceBFlag = false;
}