/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.javax.slee.AddressPresentation;

import com.opencloud.sleetck.lib.SleeTCKTest;
import com.opencloud.sleetck.lib.SleeTCKTestUtils;
import com.opencloud.sleetck.lib.TCKTestResult;

import javax.slee.AddressPresentation;

public class Test3406Test implements SleeTCKTest {

    public void init(SleeTCKTestUtils utils) {}

    /**
     * Perform the actual test.
     */

    public TCKTestResult run() throws Exception {

        try {
            AddressPresentation.fromInt(AddressPresentation.ADDRESS_PRESENTATION_UNDEFINED);
        } catch (Exception e) {
            return TCKTestResult.failed(3406, "AddressPresentation.fromInt(int) threw an exception.");
        }

        try {
            if (AddressPresentation.fromInt(AddressPresentation.ADDRESS_PRESENTATION_UNDEFINED).toInt() != AddressPresentation.ADDRESS_PRESENTATION_UNDEFINED)
                return TCKTestResult.failed(3409, "AddressPresentation.fromInt() returned incorrect value.");
        } catch (Exception e) {
            return TCKTestResult.failed(3408, "AddressPresentation.fromInt() threw an exception.");
        }

        try {
            if (!AddressPresentation.fromInt(AddressPresentation.ADDRESS_PRESENTATION_UNDEFINED).isUndefined())
                return TCKTestResult.failed(3411, "AddressPresentation.isUndefined() returned false for UNDEFINED type.");
            if (AddressPresentation.fromInt(AddressPresentation.ADDRESS_PRESENTATION_ALLOWED).isUndefined())
                return TCKTestResult.failed(3411, "AddressPresentation.isUndefined() returned true for incorrect type.");
        } catch (Exception e) {
            return TCKTestResult.failed(3410, "AddressPresentation.isUndefined() threw an exception.");
        }

        try {
            if (!AddressPresentation.fromInt(AddressPresentation.ADDRESS_PRESENTATION_ALLOWED).isAllowed())
                return TCKTestResult.failed(3413, "AddressPresentation.isAllowed() returned false for ALLOWED type.");
            if (AddressPresentation.fromInt(AddressPresentation.ADDRESS_PRESENTATION_UNDEFINED).isAllowed())
                return TCKTestResult.failed(3413, "AddressPresentation.isAllowed() returned true for incorrect type.");
        } catch (Exception e) {
            return TCKTestResult.failed(3412, "AddressPresentation.isAllowed() threw an exception.");
        }

        try {
            if (!AddressPresentation.fromInt(AddressPresentation.ADDRESS_PRESENTATION_RESTRICTED).isRestricted())
                return TCKTestResult.failed(3415, "AddressPresentation.isRestricted() returned false for RESTRICTED type.");
            if (AddressPresentation.fromInt(AddressPresentation.ADDRESS_PRESENTATION_ALLOWED).isRestricted())
                return TCKTestResult.failed(3415, "AddressPresentation.isRestricted() returned true for incorrect type.");
        } catch (Exception e) {
            return TCKTestResult.failed(3414, "AddressPresentation.isRestricted() threw an exception.");
        }

        try {
            if (!AddressPresentation.fromInt(AddressPresentation.ADDRESS_PRESENTATION_ADDRESS_NOT_AVAILABLE).isAddressNotAvailable())
                return TCKTestResult.failed(3417, "AddressPresentation.isAddressNotAvailable() returned false for ADDRESS_NOT_AVAILABLE type.");
            if (AddressPresentation.fromInt(AddressPresentation.ADDRESS_PRESENTATION_ALLOWED).isAddressNotAvailable())
                return TCKTestResult.failed(3417, "AddressPresentation.isAddressNotAvailable() returned true for incorrect type.");
        } catch (Exception e) {
            return TCKTestResult.failed(3416, "AddressPresentation.isAddressnotavailable() threw an exception.");
        }

        try {
            AddressPresentation addressPres = AddressPresentation.fromInt(AddressPresentation.ADDRESS_PRESENTATION_UNDEFINED);
            if (!addressPres.equals(addressPres))
                return TCKTestResult.failed(3419, "AddressPresentation.equals(AddressPresentation) returned false for equal objects.");
            
            if (addressPres.equals(AddressPresentation.fromInt(AddressPresentation.ADDRESS_PRESENTATION_RESTRICTED)))
                return TCKTestResult.failed(3419, "AddressPresentation.equals(AddressPresentation) returned true for non-equal objects.");
        } catch (Exception e) {
            return TCKTestResult.failed(3418, "AddressPresentation.equals() threw an exception.");
        }

        try {
            AddressPresentation.fromInt(AddressPresentation.ADDRESS_PRESENTATION_UNDEFINED).hashCode();
        } catch (Exception e) {
            return TCKTestResult.failed(3420, "AddressPresentation.hashCode() threw an exception.");
        }

        try {
            AddressPresentation.fromInt(AddressPresentation.ADDRESS_PRESENTATION_UNDEFINED).toString();
        } catch (Exception e) {
            return TCKTestResult.failed(3422, "AddressPresentation.toString() threw an exception.");
        }

        return TCKTestResult.passed();
    }


    /**
     * Do all the pre-run configuration of the test.
     */

    public void setUp() throws Exception {
    }

    /**
     * Clean up after the test.
     */

    public void tearDown() throws Exception {
    }

}
