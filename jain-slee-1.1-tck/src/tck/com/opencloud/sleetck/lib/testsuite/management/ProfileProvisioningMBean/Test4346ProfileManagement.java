/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.management.ProfileProvisioningMBean;

import com.opencloud.sleetck.lib.sbbutils.TCKSbbUtils;
import javax.slee.profile.ProfileManagement;
import javax.slee.profile.ProfileVerificationException;

public abstract class Test4346ProfileManagement implements ProfileManagement, Test4346ProfileCMP {

    public static final int FAIL = 1;
    public static final int PASS = 2;
    

    public void profileInitialize() {
        try {
            markProfileDirty();
            setAttributeA(new Integer(PASS));
        } catch (Exception e) {
            setAttributeA(new Integer(FAIL));
        }
    }

    public void profileLoad() {}
    public void profileStore() {}
    public void profileVerify() throws ProfileVerificationException {}
}
