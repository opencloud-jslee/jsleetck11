/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.resource.context;

import java.util.HashMap;

import com.opencloud.sleetck.lib.TCKTestErrorException;
import com.opencloud.sleetck.lib.TCKTestFailureException;
import com.opencloud.sleetck.lib.TCKTestResult;
import com.opencloud.sleetck.lib.testsuite.resource.BaseResourceTest;
import com.opencloud.sleetck.lib.testsuite.resource.RAMethods;

/**
 * Test to ensure that getSleeTransactionManager() is returning a correct value.
 * <p>
 * Test assertion ID: 1115185
 */
public class Test1115185Test extends BaseResourceTest {

    private static final int ASSERTION_ID = 1115185;

    public TCKTestResult run() throws Exception {
        HashMap resultmap = sendMessage(RAMethods.getSleeTransactionManager);
        if (resultmap == null)
            throw new TCKTestFailureException(ASSERTION_ID, "Test timed out while waiting for response to getSleeTransactionManager()");
        Object result = resultmap.get("result");
        if (result instanceof Exception)
            throw new TCKTestFailureException(ASSERTION_ID, "Exception thrown while invoking getSleeTransactionManager()", (Exception) result);
        if (result instanceof String)
            throw new TCKTestFailureException(ASSERTION_ID, "An error occured while invoking getSleeTransactionManager(): " + result);
        if (!Boolean.TRUE.equals(result))
            throw new TCKTestErrorException("Unexpected result received while invoking getSleeTransactionManager(): " + result);

        return TCKTestResult.passed();
    }
}
