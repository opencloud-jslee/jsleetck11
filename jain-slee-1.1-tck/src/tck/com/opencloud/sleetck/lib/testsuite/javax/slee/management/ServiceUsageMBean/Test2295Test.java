/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.javax.slee.management.ServiceUsageMBean;

import com.opencloud.sleetck.lib.AbstractSleeTCKTest;
import com.opencloud.sleetck.lib.TCKTestResult;
import com.opencloud.sleetck.lib.testsuite.usage.common.UsageMBeanLookup;
import com.opencloud.sleetck.lib.testutils.jmx.ServiceUsageMBeanProxy;

import javax.slee.SbbID;

/**
 * Create two usage parameter sets in SBB A and one set in SBB B,
 * checks the set returned by getUsageParameterSets() for the SBB A,
 * removes one of the two sets for SBB A, then checks that only the other set exists
 * for SBB A.
 */
public class Test2295Test extends AbstractSleeTCKTest {

    private static final String PARENT_SBB_PARAMETER_SET_1 = "PARENT_SBB_PARAMETER_SET_1";
    private static final String PARENT_SBB_PARAMETER_SET_2 = "PARENT_SBB_PARAMETER_SET_2";
    private static final String CHILD_SBB_PARAMETER_SET_3  = "CHILD_SBB_PARAMETER_SET_3";

    public TCKTestResult run() throws Exception {

        ServiceUsageMBeanProxy serviceUsageMBeanProxy = mBeanLookupParent.getServiceUsageMBeanProxy();
        SbbID parentSbbID = mBeanLookupParent.getSbbID();
        SbbID childSbbID  = mBeanLookupChild.getSbbID();

        // create two usage parameter sets in one SBB and one set in another SBB
        serviceUsageMBeanProxy.createUsageParameterSet(parentSbbID,PARENT_SBB_PARAMETER_SET_1);
        serviceUsageMBeanProxy.createUsageParameterSet(parentSbbID,PARENT_SBB_PARAMETER_SET_2);
        serviceUsageMBeanProxy.createUsageParameterSet(childSbbID, CHILD_SBB_PARAMETER_SET_3);

        // check the set returned by getUsageParameterSets()
        String[] parameterSetNames = serviceUsageMBeanProxy.getUsageParameterSets(parentSbbID);
        if(!contains(parameterSetNames,PARENT_SBB_PARAMETER_SET_1)) return TCKTestResult.failed(4573,
                "Parameter set "+PARENT_SBB_PARAMETER_SET_1+" not visible from getUsageParameterSets() after creation.");
        if(!contains(parameterSetNames,PARENT_SBB_PARAMETER_SET_2)) return TCKTestResult.failed(4573,
                "Parameter set "+PARENT_SBB_PARAMETER_SET_2+" not visible from getUsageParameterSets() after creation.");
        if(contains(parameterSetNames,CHILD_SBB_PARAMETER_SET_3)) return TCKTestResult.failed(4573,
                "Parameter set "+CHILD_SBB_PARAMETER_SET_3+" in set returned by a call to getUsageParameterSets() with "+
                "the SbbID of another SBB");
        if(parameterSetNames.length > 2) return TCKTestResult.failed(4573,
                "getUsageParameterSets() returned some unexpected parameter set names. Expected set="+
                formatArray(new String[]{PARENT_SBB_PARAMETER_SET_1,PARENT_SBB_PARAMETER_SET_2})+"; returned set="+
                formatArray(parameterSetNames));

        // remove one of the sets for the parent SBB
        serviceUsageMBeanProxy.removeUsageParameterSet(parentSbbID,PARENT_SBB_PARAMETER_SET_1);

        // check that only the other set exists
        parameterSetNames = serviceUsageMBeanProxy.getUsageParameterSets(parentSbbID);
        if(contains(parameterSetNames,PARENT_SBB_PARAMETER_SET_1)) return TCKTestResult.failed(2295,
                "Parameter set "+PARENT_SBB_PARAMETER_SET_1+" still visible from getUsageParameterSets() after removal "+
                "via removeUsageParameterSet().");
        if(!contains(parameterSetNames,PARENT_SBB_PARAMETER_SET_2)) return TCKTestResult.failed(2295,
                "Parameter set "+PARENT_SBB_PARAMETER_SET_2+" not visible from getUsageParameterSets() after removal of another set "+
                "via removeUsageParameterSet().");

        return TCKTestResult.passed();
    }

    public void setUp() throws Exception {
        super.setUp();
        mBeanLookupParent = new UsageMBeanLookup("Test2243Service","Test2243Sbb",utils());
        mBeanLookupChild  = new UsageMBeanLookup("Test2243Service","Test2243SbbChild",utils());
    }

    public void tearDown() throws Exception {
        if(mBeanLookupParent != null)mBeanLookupParent.closeAllMBeans();
        if(mBeanLookupChild  != null)mBeanLookupChild.closeAllMBeans();
        super.tearDown();
    }

    private boolean contains(String[] allNames, String name) {
        for (int i = 0; i < allNames.length; i++) {
            if(allNames[i].equals(name)) return true;
        }
        return false;
    }

    private String formatArray(String[] array) {
        if(array == null || array.length == 0) {
            return "{<empty set>}";
        } else {
            StringBuffer buf = new StringBuffer("{").append(array[0]);
            for (int i = 1; i < array.length; i++) {
                buf.append(array[i]);
            }
            return buf.append("}").toString();
        }
    }

    private UsageMBeanLookup mBeanLookupParent;
    private UsageMBeanLookup mBeanLookupChild;

}
