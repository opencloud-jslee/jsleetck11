/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.events.priority;

import com.opencloud.sleetck.lib.resource.events.TCKResourceEventX;
import com.opencloud.sleetck.lib.sbbutils.BaseTCKSbb;
import com.opencloud.sleetck.lib.sbbutils.TCKSbbUtils;

import javax.slee.ActivityContextInterface;
import javax.slee.Address;
import javax.slee.ChildRelation;
import javax.slee.SbbLocalObject;

public abstract class Test786Sbb extends BaseTCKSbb {

    // Initial event method.
    public void onTCKResourceEventX1(TCKResourceEventX event, ActivityContextInterface aci) {
        try {
            SbbLocalObject child = getChildRelation().create();
            aci.attach(child);

            child = getSecondChildRelation().create();
            aci.attach(child);

            fireTest786Event(new Test786Event(), aci, null); // Allow the children to create and attach their children.
            fireTest786SecondEvent(new Test786SecondEvent(), aci, null);
        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }
    }

    public abstract void fireTest786Event(Test786Event event, ActivityContextInterface aci, Address address);
    public abstract void fireTest786SecondEvent(Test786SecondEvent event, ActivityContextInterface aci, Address address);

    public abstract ChildRelation getChildRelation();
    public abstract ChildRelation getSecondChildRelation();
}
