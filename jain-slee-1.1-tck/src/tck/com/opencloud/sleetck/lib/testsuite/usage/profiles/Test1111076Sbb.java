/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.usage.profiles;

import java.util.HashMap;

import javax.naming.InitialContext;
import javax.slee.ActivityContextInterface;
import javax.slee.profile.ProfileFacility;
import javax.slee.profile.ProfileTable;

import com.opencloud.sleetck.lib.TCKException;
import com.opencloud.sleetck.lib.resource.events.TCKResourceEventX;
import com.opencloud.sleetck.lib.sbbutils.BaseTCKSbb;
import com.opencloud.sleetck.lib.sbbutils.TCKSbbUtils;
import com.opencloud.sleetck.lib.sbbutils.events2.SbbBaseMessageConstants;

public abstract class Test1111076Sbb extends BaseTCKSbb {

    public void onTCKResourceEventX1(TCKResourceEventX event, ActivityContextInterface aci) {
        HashMap map = new HashMap();
        String message;
        ProfileFacility facility;
        ProfileTable pt;
        Test1111076ProfileLocal addedProfile;

        try {
            facility = (ProfileFacility) new InitialContext().lookup(ProfileFacility.JNDI_NAME);
            pt = facility.getProfileTable(Test1111076Test.PROFILE_TABLE_NAME);
            addedProfile = (Test1111076ProfileLocal)pt.find(Test1111076Test.PROFILE_NAME);
            getSbbContext().getTracer("Test1111076Sbb").fine("Got profile: "+addedProfile.toString());

            String someTests = addedProfile.doSomeTests();
            if (!someTests.equals("passed"))
                TCKSbbUtils.handleException(new TCKException(someTests));

            map.put(SbbBaseMessageConstants.TYPE, new Integer(SbbBaseMessageConstants.TYPE_SET_RESULT));
            map.put(SbbBaseMessageConstants.RESULT, new Boolean(true));

            message = (String)event.getMessage();
            if (message.equals("Default")) {
                addedProfile.incrementDefaultParameterSet();
                map.put(SbbBaseMessageConstants.MSG, "Default");
            } else if (message.equals("Named")) {
                addedProfile.incrementNamedParameterSet();
                map.put(SbbBaseMessageConstants.MSG, "Named");
            } else {
                throw new TCKException("Malformed event.");
            }

            TCKSbbUtils.getResourceInterface().sendSbbMessage(map);
        } catch(Exception e) {
            TCKSbbUtils.handleException(e);
        }
    }
}
