/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.profiles.defaultprofile;

import com.opencloud.sleetck.lib.AbstractSleeTCKTest;
import com.opencloud.sleetck.lib.TCKTestResult;
import com.opencloud.sleetck.lib.infra.SleeTCKComponentConstants;
import com.opencloud.sleetck.lib.testsuite.profiles.ProfileTestConstants;
import com.opencloud.sleetck.lib.testutils.Assert;
import com.opencloud.sleetck.lib.testutils.ComponentIDLookup;
import com.opencloud.sleetck.lib.testutils.ProfileUtils;
import com.opencloud.sleetck.lib.testutils.jmx.ProfileProvisioningMBeanProxy;

import javax.slee.profile.ProfileSpecificationID;
import javax.slee.profile.UnrecognizedProfileTableNameException;

/**
 * Tests assertion 1979, that the default profile for a profile table is deleted when the profile
 * table is deleted.
 */
public class DefaultProfileDeletionTest extends AbstractSleeTCKTest {

    private static final String PROFILE_TABLE_NAME = "tck.DefaultProfileDeletionTest.table";

    public TCKTestResult run() throws Exception {
        // create the profile tables
        ProfileProvisioningMBeanProxy profileProvisioning = profileUtils.getProfileProvisioningProxy();
        ProfileSpecificationID profileSpecID = new ComponentIDLookup(utils()).lookupProfileSpecificationID(
            ProfileTestConstants.SIMPLE_PROFILE_SPEC_NAME,SleeTCKComponentConstants.TCK_VENDOR,"1.0");
        getLog().info("Creating profile table: " + PROFILE_TABLE_NAME);
        profileProvisioning.createProfileTable(profileSpecID, PROFILE_TABLE_NAME);
        getLog().info("Removing profile table: " + PROFILE_TABLE_NAME);
        profileProvisioning.removeProfileTable(PROFILE_TABLE_NAME);

        try {
            profileProvisioning.getDefaultProfile(PROFILE_TABLE_NAME);
            Assert.fail(1979, "getDefaultProfile for a profile table should throw an exception after the profile " +
                              "has been deleted");
        } catch (UnrecognizedProfileTableNameException e) {
            getLog().info("Got expected UnrecognizedProfileTableNameException");
        }

        return TCKTestResult.passed();
    }

    public void setUp() throws Exception {
        String duPath = utils().getTestParams().getProperty(ProfileTestConstants.PROFILE_SPEC_DU_PATH_PARAM);
        getLog().fine("Installing profile specification");
        utils().install(duPath);
        profileUtils = new ProfileUtils(utils());
    }

    public void tearDown() throws Exception {
        profileUtils = null;
        super.tearDown();
    }

    private ProfileUtils profileUtils;
}