/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.profiles.events;

import javax.slee.ActivityContextInterface;
import javax.slee.Address;
import javax.slee.profile.ProfileUpdatedEvent;

import com.opencloud.sleetck.lib.sbbutils.BaseTCKSbb;
import com.opencloud.sleetck.lib.sbbutils.TCKSbbUtils;
import com.opencloud.sleetck.lib.sbbutils.events.TCKSbbEvent;
import com.opencloud.sleetck.lib.sbbutils.events.TCKSbbEventImpl;

/**
 * This sbb expects the test to:
 * Create a profile table.
 * Add a profile to the profile table.
 * Update a profile CMP field value (-> ProfileUpdatedEvent)
 *
 * The Sbb responds to the test with an ACK message, in the form of the full
 * name of the event class received.
 */
public abstract class Test1010003_2Sbb extends BaseTCKSbb {

    public void onProfileUpdatedEvent(ProfileUpdatedEvent event, ActivityContextInterface aci) {
        getSbbContext().getTracer("Test1010003_2Sbb").fine("Received ProfileUpdatedEvent event:"+event,null);

        fireTCKSbbEvent(new TCKSbbEventImpl(ProfileUpdatedEvent.class.getName()), aci, null);
    }

    public void onTCKSbbEvent(TCKSbbEvent event, ActivityContextInterface aci) {
        try {
            // the attach TXN has committed -- send an ACK to the test as a reply
            TCKSbbUtils.getResourceInterface().sendSbbMessage(event.getMessage());
        } catch(Exception e) {
            TCKSbbUtils.handleException(e);
        }
    }

    public abstract void fireTCKSbbEvent(TCKSbbEvent event, ActivityContextInterface aci, Address address);
}
