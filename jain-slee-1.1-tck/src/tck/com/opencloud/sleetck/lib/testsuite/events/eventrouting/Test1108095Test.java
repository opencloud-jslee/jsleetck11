/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.events.eventrouting;

import java.rmi.RemoteException;

import com.opencloud.sleetck.lib.AbstractSleeTCKTest;
import com.opencloud.sleetck.lib.resource.TCKActivityID;
import com.opencloud.sleetck.lib.resource.TCKSbbMessage;
import com.opencloud.sleetck.lib.resource.events.TCKResourceEventX;
import com.opencloud.sleetck.lib.resource.testapi.TCKResourceListener;
import com.opencloud.sleetck.lib.resource.testapi.TCKResourceTestInterface;
import com.opencloud.sleetck.lib.testutils.BaseTCKResourceListener;
import com.opencloud.sleetck.lib.testutils.FutureResult;

/**
 * AssertionID(1108095): Test The default-priority element of the service 
 * deployment descriptor element of a Service specifies the initial event 
 * delivery priority of the root SBB entities instantiated by the SLEE for 
 * the Service. 
 */
public class Test1108095Test extends AbstractSleeTCKTest {

    public static final String SERVICE1_DU_PATH_PARAM = "service1DUPath";
    public static final String SERVICE2_DU_PATH_PARAM = "service2DUPath";
    public static final String SERVICE3_DU_PATH_PARAM = "service3DUPath";

    /**
     * Perform the actual test.
     */

    public void run(FutureResult result) throws Exception {
        this.result = result;

        TCKResourceTestInterface resource = utils().getResourceInterface();
        TCKActivityID activityID = resource.createActivity("Test1108095InitialActivity");

        resource.fireEvent(TCKResourceEventX.X1, testName, activityID, null);

    }

    /**
     * Do all the pre-run configuration of the test.
     */

    public void setUp() throws Exception {
        getLog().fine("Connecting to resource");
        resourceListener = new TCKResourceListenerImpl();
        setResourceListener(resourceListener);
        getLog().fine("Installing and activating service");

        setupService(SERVICE1_DU_PATH_PARAM, true);
        setupService(SERVICE2_DU_PATH_PARAM, true);
        setupService(SERVICE3_DU_PATH_PARAM, true);
    }

    /**
     * Clean up after the test.
     */

    public void tearDown() throws Exception {
        // cleanup
        super.tearDown();
    }

    private class TCKResourceListenerImpl extends BaseTCKResourceListener {
        public synchronized void onSbbMessage(TCKSbbMessage message, TCKActivityID calledActivity)
                throws RemoteException {
            //getLog().info("Received message from SBB");

            int seq = ((Integer) message.getMessage()).intValue();
            getLog().info("Received message from SBB with seq number = " + seq);
            if (seq >= prevSeq) {result.setFailed(1108095,
                                "Root SBBs of services with differing default priorities received their initial events in the wrong order.");
                return;
            }

            if (seq == MAX_SEQ) {
                result.setPassed();
                return;
            }

            prevSeq = seq;
        }

        public void onException(Exception e) throws RemoteException {
            getLog().warning("Received exception from SBB");
            getLog().warning(e);
            result.setError(e);
        }
    }

    private TCKResourceListener resourceListener;

    private FutureResult result;

    private final static String testName = "Test1108095";

    private static final int MAX_SEQ = -5;

    private int prevSeq = 20;
}