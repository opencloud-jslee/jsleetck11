/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.management.SleeManagementMBean;

import com.opencloud.sleetck.lib.AbstractSleeTCKTest;
import com.opencloud.sleetck.lib.SleeTCKTestUtils;
import com.opencloud.sleetck.lib.SleeTCKTest;
import com.opencloud.sleetck.lib.TCKTestResult;
import com.opencloud.sleetck.lib.TCKTestFailureException;
import com.opencloud.sleetck.lib.TCKTestErrorException;
import com.opencloud.sleetck.lib.testutils.ExceptionsUtil;
import com.opencloud.sleetck.lib.testutils.Assert;
import com.opencloud.sleetck.lib.testutils.jmx.SleeManagementMBeanProxy;
import javax.slee.management.SleeState;
import javax.slee.management.DeploymentMBean;
import javax.slee.management.ServiceManagementMBean;
import javax.slee.management.ProfileProvisioningMBean;
import javax.slee.management.TraceMBean;
import javax.slee.management.AlarmMBean;
import javax.management.ObjectName;
import javax.management.MBeanInfo;
import javax.management.JMException;

/**
 * Tests assertions 4037, 4039, 4041, 4043, and 4045 - that the correct MBeans
 * are returned by the MBean accessor methods in SleeManagementMBean.
 */
public class TestMBeanAccessors implements SleeTCKTest {

    public void init(SleeTCKTestUtils utils) { this.utils=utils; }
    public void setUp() {}
    public void tearDown() {}

    public TCKTestResult run() throws Exception {
        SleeManagementMBeanProxy management = utils.getSleeManagementMBeanProxy();
        checkMBean(management.getDeploymentMBean(),"getDeploymentMBean",DeploymentMBean.class.getName(),4037);
        checkMBean(management.getServiceManagementMBean(),"getServiceManagementMBean",ServiceManagementMBean.class.getName(),4039);
        checkMBean(management.getProfileProvisioningMBean(),"getProfileProvisioningMBean",ProfileProvisioningMBean.class.getName(),4041);
        checkMBean(management.getTraceMBean(),"getTraceMBean",TraceMBean.class.getName(),4043);
        checkMBean(management.getAlarmMBean(),"getAlarmMBean",AlarmMBean.class.getName(),4045);
        return TCKTestResult.passed();
    }

    private void checkMBean(ObjectName objName, String methodName, String expectedMBeanClassName, int assertionID)
            throws TCKTestFailureException, TCKTestErrorException, JMException {
        utils.getLog().info("Checking that MBean "+objName+" returned by "+methodName+" is a "+expectedMBeanClassName);
        if(!utils.getMBeanFacade().isInstanceOf(objName,expectedMBeanClassName)) throw new TCKTestFailureException(assertionID,
            "MBean "+objName+" returned by "+methodName+" is not an instance of "+expectedMBeanClassName);
    }

    private SleeTCKTestUtils utils;

}