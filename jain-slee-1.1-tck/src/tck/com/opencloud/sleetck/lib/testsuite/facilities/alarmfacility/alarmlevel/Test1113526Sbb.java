/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.facilities.alarmfacility.alarmlevel;

import javax.naming.InitialContext;
import javax.slee.ActivityContextInterface;
import javax.slee.facilities.AlarmFacility;
import javax.slee.facilities.AlarmLevel;
import javax.slee.facilities.Tracer;

import com.opencloud.sleetck.lib.resource.events.TCKResourceEventX;
import com.opencloud.sleetck.lib.sbbutils.BaseTCKSbb;
import com.opencloud.sleetck.lib.sbbutils.TCKSbbUtils;

/*
 * AssertionID (1113526): Test when CLEAR is specified in an alarm filter 
 * (see Section 14.14.4), only alarm notifications that indicate an alarm 
 * has been cleared will be emitted.
 */
public abstract class Test1113526Sbb extends BaseTCKSbb {
    public static final String ALARM_MESSAGE_CRITICAL = "CRITICAL:Test1113526TraceMessage";

    public static final String ALARM_MESSAGE_MAJOR = "MAJOR:Test1113526TraceMessage";

    public static final String ALARM_MESSAGE_WARNING = "WARNING:Test1113526TraceMessage";

    public static final String ALARM_MESSAGE_INDETERMINATE = "INDETERMINATE:Test1113526TraceMessage";

    public static final String ALARM_MESSAGE_MINOR = "MINOR:Test1113526TraceMessage";

    public static final String ALARM_TYPE = "javax.slee.management.Alarm";

    // Initial event
    public void onTCKResourceEventX1(TCKResourceEventX event, ActivityContextInterface aci) {

        try {
            tracer = this.getSbbContext().getTracer("com.test");
            tracer.info("Received " + testName + " message", null);

            doTest1113526Test();

        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }
    }

    private void doTest1113526Test() throws Exception {
        AlarmFacility facility = getAlarmFacility();
        // 1113526
        try {
            // raise all alarms with different AlarmLevel severity
            String[] alarmIDs = {
                    facility.raiseAlarm(ALARM_TYPE, "Test1113526AlarmInstanceID1", AlarmLevel.CRITICAL,
                            ALARM_MESSAGE_CRITICAL),
                    facility.raiseAlarm(ALARM_TYPE, "Test1113526AlarmInstanceID2", AlarmLevel.MAJOR,
                            ALARM_MESSAGE_MAJOR),
                    facility.raiseAlarm(ALARM_TYPE, "Test1113526AlarmInstanceID3", AlarmLevel.WARNING,
                            ALARM_MESSAGE_WARNING),
                    facility.raiseAlarm(ALARM_TYPE, "Test1113526AlarmInstanceID4", AlarmLevel.INDETERMINATE,
                            ALARM_MESSAGE_INDETERMINATE),
                    facility.raiseAlarm(ALARM_TYPE, "Test1113526AlarmInstanceID5", AlarmLevel.MINOR,
                            ALARM_MESSAGE_MINOR) };
        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }
    }

    private AlarmFacility getAlarmFacility() throws Exception {
        AlarmFacility facility = null;
        String JNDI_ALARMFACILITY_NAME = AlarmFacility.JNDI_NAME;
        try {
            facility = (AlarmFacility) new InitialContext().lookup(JNDI_ALARMFACILITY_NAME);
        } catch (Exception e) {
            tracer.warning("got unexpected Exception: " + e, null);
        }
        return facility;
    }

    private String testName = "Test1113526Test";

    private Tracer tracer;
}
