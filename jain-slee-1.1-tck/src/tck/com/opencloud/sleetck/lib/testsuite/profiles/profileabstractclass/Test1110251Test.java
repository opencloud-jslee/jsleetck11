/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.profiles.profileabstractclass;

import java.rmi.RemoteException;
import java.util.HashMap;

import javax.management.Attribute;
import javax.management.ObjectName;
import javax.slee.management.ManagementException;
import javax.slee.profile.ProfileSpecificationID;
import javax.slee.profile.ProfileVerificationException;
import javax.slee.profile.UnrecognizedProfileNameException;

import com.opencloud.sleetck.lib.AbstractSleeTCKTest;
import com.opencloud.sleetck.lib.OperationTimedOutException;
import com.opencloud.sleetck.lib.TCKTestErrorException;
import com.opencloud.sleetck.lib.TCKTestFailureException;
import com.opencloud.sleetck.lib.TCKTestResult;
import com.opencloud.sleetck.lib.infra.SleeTCKComponentConstants;
import com.opencloud.sleetck.lib.resource.TCKActivityID;
import com.opencloud.sleetck.lib.resource.TCKSbbMessage;
import com.opencloud.sleetck.lib.resource.events.TCKResourceEventX;
import com.opencloud.sleetck.lib.resource.testapi.TCKResourceTestInterface;
import com.opencloud.sleetck.lib.sbbutils.events2.SbbBaseMessageConstants;
import com.opencloud.sleetck.lib.testutils.ProfileUtils;
import com.opencloud.sleetck.lib.testutils.QueuingResourceListener;
import com.opencloud.sleetck.lib.testutils.jmx.ProfileMBeanProxy;
import com.opencloud.sleetck.lib.testutils.jmx.ProfileProvisioningMBeanProxy;


public class Test1110251Test extends AbstractSleeTCKTest {

    private static final String DU_PATH_PARAM= "DUPath";

    private static final String SPEC_NAME = "Test1110251Profile";
    private static final String SPEC_VERSION = "1.0";

    /**
     * Test some Profile abstract class' lifecycle methods.
     */
    public TCKTestResult run() throws Exception {

        //create a profile table
        ProfileSpecificationID specID = new ProfileSpecificationID(SPEC_NAME, SleeTCKComponentConstants.TCK_VENDOR, SPEC_VERSION);
        profileProvisioning.createProfileTable(specID, Test1110251Sbb.PROFILE_TABLE_NAME);
        getLog().fine("Added profile table "+Test1110251Sbb.PROFILE_TABLE_NAME+" based on profile spec "+SPEC_NAME);

        //try to add a profile to the table
        //we can't access the 'inside' of the management clients createProfile() transaction here
        getLog().fine("Try to create profile "+Test1110251Sbb.PROFILE_NAME+" via management client...");
        try {
            profileProvisioning.createProfile(Test1110251Sbb.PROFILE_TABLE_NAME, Test1110251Sbb.PROFILE_NAME);
            return TCKTestResult.failed(1110251, "Create operation through management client succeeded but should have " +
                    "thrown a javax.slee.management.ManagementException exception.");
        }
        catch (ManagementException e) {
                getLog().fine("Caught expected exception: javax.slee.management.ManagementException.");
        }
        catch (Exception e) {
            return TCKTestResult.failed(1110251, "Wrong type of exception: "+e.getClass().getName()
                    +" javax.slee.management.ManagementException expected");
        }

        getLog().fine("Try to create profile "+Test1110251Sbb.PROFILE_NAME+" via SLEE component...");
        //try to get an Sbb to create the profile, profilePostCreate marks TXN for rollback
        sendResourceEvent(Test1110251Sbb.POSTCREATE_ROLLBACK);



        //make sure PROFILE_NAME has not been successfully created as the Sbb event handler TXN rolled back
        try {
            profileProvisioning.getProfile(Test1110251Sbb.PROFILE_TABLE_NAME, Test1110251Sbb.PROFILE_NAME);
            return TCKTestResult.failed(1110251, "Create operation through SBB component succeeded but " +
                    "profile "+ Test1110251Sbb.PROFILE_NAME +" should not have been created.");
        }
        catch (UnrecognizedProfileNameException e) {
            getLog().fine("Caught expected exception: "+e.getClass().getName());
        }
        catch (Exception e) {
            return TCKTestResult.failed(1110251, "Wrong type of exception: "+e.getClass().getName()
                    +" javax.slee.profile.UnrecognizedProfileNameException expected");
        }

        //create the profile that shall be removed by the Sbb or management client
        getLog().fine("Create profile "+Test1110251Sbb.ROLLBACK_REMOVE_PROFILE);
        ObjectName profile = profileProvisioning.createProfile(Test1110251Sbb.PROFILE_TABLE_NAME, Test1110251Sbb.ROLLBACK_REMOVE_PROFILE);
        ProfileMBeanProxy profileProxy = utils().getMBeanProxyFactory().createProfileMBeanProxy(profile);
        profileProxy.commitProfile();
        profileProxy.closeProfile();

        //try to remove the profile via management client. the profile object's profileRemove()
        //method will set the TXN for rollback in this case, thus the operation should fail.
        getLog().fine("Try to remove profile "+Test1110251Sbb.ROLLBACK_REMOVE_PROFILE+" via management client...");
        try {
            profileProvisioning.removeProfile(Test1110251Sbb.PROFILE_TABLE_NAME, Test1110251Sbb.ROLLBACK_REMOVE_PROFILE);
            return TCKTestResult.failed(1110266, "Remove operation through management client succeeded but should have " +
                "thrown a javax.slee.management.ManagementException exception.");
        }
        catch (ManagementException e) {
            getLog().fine("Caught expected exception: javax.slee.management.ManagementException.");
        }
        catch (Exception e) {
            return TCKTestResult.failed(1110266, "Wrong type of exception: "+e.getClass().getName()
                    +" javax.slee.management.ManagementException expected.");
        }

        //try to get an Sbb to remove the profile, profileRemove marks the TXN for rollback
        getLog().fine("Try to remove profile "+Test1110251Sbb.ROLLBACK_REMOVE_PROFILE+" via SLEE component...");
        sendResourceEvent(Test1110251Sbb.REMOVE_ROLLBACK);



        //make sure ROLLBACK_REMOVE_PROFILE has not been successfully removed as the Sbb event handler TXN rolled back
        try {
            profileProvisioning.getProfile(Test1110251Sbb.PROFILE_TABLE_NAME, Test1110251Sbb.ROLLBACK_REMOVE_PROFILE);
            getLog().fine("Profile "+Test1110251Sbb.ROLLBACK_REMOVE_PROFILE+" still exists after Sbb component's remove operation rolled back.");
        }
        catch (Exception e) {
            return TCKTestResult.failed(1110266, "Remove operation through SBB component succeeded but " +
                    "should have failed.");
        }


        //create the profile that shall be removed by the Sbb or management client
        getLog().fine("Create profile "+Test1110251Sbb.ROLLBACK_STORE_PROFILE);
        ObjectName profile2 = profileProvisioning.createProfile(Test1110251Sbb.PROFILE_TABLE_NAME, Test1110251Sbb.ROLLBACK_STORE_PROFILE);
        ProfileMBeanProxy profileProxy2 = utils().getMBeanProxyFactory().createProfileMBeanProxy(profile2);

        try {
            getLog().fine("Set 'value' CMP field to 'Rollback'");
            utils().getMBeanFacade().setAttribute(profile2, new Attribute("Value", "Rollback"));
            //either in commitProfile() or when the profile is removed profileStore() is
            //expected to be called (?)
            profileProxy2.commitProfile();
            profileProxy2.closeProfile();
            //try to remove the profile via management client. the profile object's profileStore()
            //method will set the TXN for rollback in this case, thus the operation should fail.
            getLog().fine("Try to cause SLEE call to profileStore method for profile "+Test1110251Sbb.ROLLBACK_STORE_PROFILE+" via management client...");
            profileProvisioning.removeProfile(Test1110251Sbb.PROFILE_TABLE_NAME, Test1110251Sbb.ROLLBACK_STORE_PROFILE);
            return TCKTestResult.failed(1110275, "Remove operation through management client succeeded but should have " +
                "thrown a javax.slee.management.ManagementException exception.");
        }
        catch (ManagementException e) {
            getLog().fine("Caught expected exception: javax.slee.management.ManagementException.");
        }
        catch (Exception e) {
            getLog().fine("Caught wrong type of exception.");
            return TCKTestResult.failed(1110275, "Wrong type of exception: "+e.getClass().getName()
                    +" javax.slee.management.ManagementException expected.");
        }


        //create the profile whose profileVerify method shall fail on commit
        getLog().fine("Create profile "+Test1110251Sbb.VERIFY_EXCEPTION_PROFILE);
        ObjectName profile3 = profileProvisioning.createProfile(Test1110251Sbb.PROFILE_TABLE_NAME, Test1110251Sbb.VERIFY_EXCEPTION_PROFILE);
        ProfileMBeanProxy profileProxy3 = utils().getMBeanProxyFactory().createProfileMBeanProxy(profile3);

        try {
            profileProxy3.commitProfile();
            profileProxy3.closeProfile();
            return TCKTestResult.failed(1110279, "Commit succeeded but should have thrown an exception as" +
                    " profileVerify has been configured to throw a ProfileVerificationException.");
        }
        catch (ProfileVerificationException e) {
            getLog().fine("Caught expected exception: "+e);
        }
        catch (Exception e) {
            return TCKTestResult.failed(1110279, "Wrong type of exception: "+e.getClass().getName()
                    +" javax.slee.profile.ProfileVerificationException expected.");
        }

        return TCKTestResult.passed();

    }

    private void sendResourceEvent(int operationID) throws TCKTestErrorException, RemoteException, TCKTestFailureException {
        // send an initial event to the test
        TCKResourceTestInterface resource = utils().getResourceInterface();
        TCKActivityID activityID = resource.createActivity(getClass().getName());

        resource.fireEvent(TCKResourceEventX.X1, new Integer(operationID), activityID, null);

        try {
            //wait for reply from Sbb
            TCKSbbMessage reply = resourceListener.nextMessage();

            HashMap map = (HashMap) reply.getMessage();
            int type = ((Integer)map.get(SbbBaseMessageConstants.TYPE)).intValue();
            String msg = (String) map.get(SbbBaseMessageConstants.MSG);
            int id = ((Integer)map.get(SbbBaseMessageConstants.ID)).intValue();
            boolean result = ((Boolean)map.get(SbbBaseMessageConstants.RESULT)).booleanValue();

            switch (type) {
            case SbbBaseMessageConstants.TYPE_SET_RESULT:
                if (result) {
                    getLog().fine(id+": "+msg);
                }
                else {
                    getLog().fine("FAILURE: "+msg);
                    throw new TCKTestFailureException(id, msg);
                }
                break;
            }
        } catch (OperationTimedOutException ex) {
            throw new TCKTestErrorException("Timed out waiting for processing of initial resource event.", ex);
        }
    }

    public void setUp() throws Exception {

        setupService(DU_PATH_PARAM);

        profileUtils = new ProfileUtils(utils());
        profileProvisioning = profileUtils.getProfileProvisioningProxy();

        resourceListener = new QueuingResourceListener(utils()) {
            public Object onSbbCall(Object argument) throws Exception {
                HashMap map = (HashMap) argument;
                int type = ((Integer)map.get(SbbBaseMessageConstants.TYPE)).intValue();
                switch (type) {
                case SbbBaseMessageConstants.TYPE_LOG_MSG:
                    getLog().fine((String)map.get(SbbBaseMessageConstants.MSG));
                    break;
                }
                return null;
            }
        };
        setResourceListener(resourceListener);
    }

    public void tearDown() throws Exception {

        try {
            profileUtils.removeProfileTable(Test1110251Sbb.PROFILE_TABLE_NAME);
        }
        catch (Exception e) {
            getLog().warning("Caught exception while trying to remove profile table:");
            getLog().warning(e);
        }

        super.tearDown();
    }

    private ProfileUtils profileUtils;
    private QueuingResourceListener resourceListener;
    private ProfileProvisioningMBeanProxy profileProvisioning;
}
