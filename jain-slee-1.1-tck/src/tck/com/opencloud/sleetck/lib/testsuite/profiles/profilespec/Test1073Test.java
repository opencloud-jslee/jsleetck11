/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.profiles.profilespec;

import com.opencloud.sleetck.lib.AbstractSleeTCKTest;
import com.opencloud.sleetck.lib.TCKTestResult;
import com.opencloud.sleetck.lib.testsuite.profiles.ProfileTestConstants;
import com.opencloud.sleetck.lib.testutils.Assert;

import javax.slee.management.DeployableUnitID;
import javax.slee.management.DeploymentException;


public class Test1073Test extends AbstractSleeTCKTest {

    /**
     * Runs some tests on javax.slee.profile.ProfileManagement methods <code>markProfileDirty</code> and
     * <code>isProfileValid</code> and wrapping of exceptions thrown by management class methods.
     * @return
     * @throws Exception
     */
    public TCKTestResult run() throws Exception {
        String duPath = utils().getTestParams().getProperty(ProfileTestConstants.PROFILE_SPEC_DU_PATH_PARAM);
        getLog().fine("Installing profile specification");
        String fullURL = utils().getDeploymentUnitURL(duPath);
        try {
            DeployableUnitID du = utils().getDeploymentMBeanProxy().install(fullURL);
            Assert.fail(1073, "Should receive an exception when deploying a profile spec with duplicate methods");
            if (utils().getDeploymentMBeanProxy().isInstalled(du))
                utils().getDeploymentMBeanProxy().uninstall(du);
        } catch (DeploymentException e) {
            getLog().info("caught expected DeploymentException");
        } catch (Exception e) {
            Assert.fail(1073, "Got unexpected exception when deploying a profile spec with duplicate methods:" + e);
        }

        return TCKTestResult.passed();
    }

    public void setUp() throws Exception {
    }
}
