/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.facilities.profilefacility;

import com.opencloud.sleetck.lib.resource.events.TCKResourceEventX;
import com.opencloud.sleetck.lib.sbbutils.BaseTCKSbb;
import com.opencloud.sleetck.lib.sbbutils.TCKSbbUtils;

import javax.naming.InitialContext;
import javax.slee.ActivityContextInterface;
import javax.slee.profile.ProfileFacility;
import javax.slee.profile.ProfileID;
import javax.slee.profile.UnrecognizedProfileNameException;
import javax.slee.profile.UnrecognizedProfileTableNameException;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;

public abstract class Test2087Sbb extends BaseTCKSbb {

    private static final String JNDI_PROFILEFACILITY_NAME = "java:comp/env/slee/facilities/profile";
    public static final String PROFILE_TABLE_NAME = "Test2087ProfileTable";
    public static final String PROFILE_NAME = "Test2087Profile";

    // Initial event
    public void onTCKResourceEventX1(TCKResourceEventX ev, ActivityContextInterface aci) {

        try {
            ProfileFacility facility = (ProfileFacility) new InitialContext().lookup(JNDI_PROFILEFACILITY_NAME);

            HashMap map = new HashMap();
            boolean passed = false;
            try {
                facility.getProfiles(null);
            } catch (NullPointerException e) {
                passed = true;
            }

            if (!passed) {
                map.put("Result", new Boolean(false));
                map.put("ID", new Integer(2362));
                map.put("Message", "ProfileFacility.getProfiles(null) should have thrown a NullPointerException but did not.");
                TCKSbbUtils.getResourceInterface().sendSbbMessage(map);
                return;
            }

            passed = false;
            try {
                facility.getProfiles("NonExistentProfileTable");
            } catch (UnrecognizedProfileTableNameException e) {
                passed = true;
            }

            if (!passed) {
                map.put("Result", new Boolean(false));
                map.put("ID", new Integer(2090));
                map.put("Message", "ProfileFacility.getProfiles(BogusName) should have thrown an UnrecognizedProfileTableNameException but did not.");
                TCKSbbUtils.getResourceInterface().sendSbbMessage(map);
                return;
            }

            Collection collection = facility.getProfiles(PROFILE_TABLE_NAME);

            // 2086 - getProfiles() returns a collection populated by ProfileID
            Iterator iter = collection.iterator();
            while (iter.hasNext()) {
                ProfileID profileID = null;
                try {
                    profileID = (ProfileID) iter.next();
                } catch (ClassCastException e) {
                    map.put("Result", Boolean.FALSE);
                    map.put("ID", new Integer(2086));
                    map.put("Message", "Collection returned by ProfileFacility.getProfiles(name) contained non-ProfileID objects.");
                    TCKSbbUtils.getResourceInterface().sendSbbMessage(map);
                    return;
                }

                if (!profileID.getProfileName().equals(PROFILE_NAME)) {
                    map.put("Result", Boolean.FALSE);
                    map.put("ID", new Integer(2086));
                    map.put("Message", "Collection returned by ProfileFacility.getProfiles(name) contained a ProfileID object representing a profile not in the collection.");
                    TCKSbbUtils.getResourceInterface().sendSbbMessage(map);
                    return;
                }
            }

            
            collection = facility.getProfiles(PROFILE_TABLE_NAME);
            ProfileID profileID = (ProfileID) collection.iterator().next();
            passed = false;
            try {
                collection.add(profileID);
            } catch (UnsupportedOperationException e) {
                passed = true;
            }


            if (!passed) {
                map.put("Result", new Boolean(false));
                map.put("ID", new Integer(2087));
                map.put("Message", "Collection returned by ProfileFacility.getProfiles(Name) was not immutable.");
                TCKSbbUtils.getResourceInterface().sendSbbMessage(map);
                return;
            }

            iter = collection.iterator();
            while (iter.hasNext()) {
                profileID = (ProfileID) iter.next();
                if (!profileID.getProfileName().equals(PROFILE_NAME)) {
                    map.put("Result", new Boolean(false));
                    map.put("ID", new Integer(2088));
                    map.put("Message", "Collection of ProfileIDs in a profile table contained the default profile.");
                    TCKSbbUtils.getResourceInterface().sendSbbMessage(map);
                    return;
                }
            }

            map.put("Type", "Result");
            map.put("Result", new Boolean(passed));
            map.put("Message", "Ok");
            TCKSbbUtils.getResourceInterface().sendSbbMessage(map);
        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }

    }

    public abstract Test2087ProfileCMP getProfileCMP(ProfileID profileID) throws UnrecognizedProfileTableNameException, UnrecognizedProfileNameException;

}
