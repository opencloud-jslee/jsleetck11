/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.facilities.profilefacility;

import java.util.HashMap;

import javax.naming.InitialContext;
import javax.slee.ActivityContextInterface;
import javax.slee.facilities.Tracer;
import javax.slee.profile.ProfileFacility;

import com.opencloud.sleetck.lib.resource.events.TCKResourceEventX;
import com.opencloud.sleetck.lib.sbbutils.BaseTCKSbb;
import com.opencloud.sleetck.lib.sbbutils.TCKSbbUtils;

/**
 * AssertionID (1113023): Test The JNDI_NAME constant. This constant specifies
 * the JNDI location where a ProfileFacility object may be located by an SBB
 * component in its component environment.
 */
public abstract class Test1113023Sbb extends BaseTCKSbb {

    public void onTCKResourceEventX1(TCKResourceEventX event, ActivityContextInterface aci) {

        try {
            tracer = getSbbContext().getTracer("com.test");
            tracer.info("Sbb: Received a TCK event " + event);

            doJNDITest();

        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }
    }

    private void doJNDITest() throws Exception {
        ProfileFacility pf = getProfileFacility();
        if (pf != null)
            sendResultToTCK("Test1113023Test", true, 1113023, "Found ProfileFacility object in JNDI at " + ProfileFacility.JNDI_NAME);
        else
            sendResultToTCK("Test1113023Test", false, 1113023, "Could not find ProfileFacility object in JNDI at " + ProfileFacility.JNDI_NAME);
    }

    private void sendResultToTCK(String testName, boolean result, int failedAssertionID,  String message) throws Exception {
        HashMap sbbData = new HashMap();
        sbbData.put("testname", testName);
        sbbData.put("result", result ? "pass" : "fail");
        sbbData.put("message", message);
        sbbData.put("id", new Integer(failedAssertionID));
        TCKSbbUtils.getResourceInterface().sendSbbMessage(sbbData);
    }


    private ProfileFacility getProfileFacility() throws Exception {
        ProfileFacility pf = null;
        String JNDI_PROFILEFACILITY_NAME = ProfileFacility.JNDI_NAME;
        try {
            pf = (ProfileFacility) new InitialContext().lookup(JNDI_PROFILEFACILITY_NAME);
        } catch (Exception e) {
            tracer.warning("got unexpected Exception: " + e, null);
        }
        return pf;
    }

    private Tracer tracer;
}
