/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.profiles.defaultprofile;

public interface CheckInitializeProfileCMP {

    public static final String INITIALIZED_VALUE = "INITIALIZED";

    public String getValue();
    public void setValue(String value);
    public String getValue2();
    public void setValue2(String value2);

}