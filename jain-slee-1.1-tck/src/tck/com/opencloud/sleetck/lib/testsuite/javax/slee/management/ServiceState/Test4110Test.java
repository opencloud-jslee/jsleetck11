/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.javax.slee.management.ServiceState;

import com.opencloud.sleetck.lib.SleeTCKTest;
import com.opencloud.sleetck.lib.SleeTCKTestUtils;
import com.opencloud.sleetck.lib.TCKTestResult;

import javax.slee.management.DeployableUnitID;
import javax.slee.management.ServiceState;

public class Test4110Test implements SleeTCKTest {

    private static final String SERVICE_DU_PATH_PARAM = "DUPath";

    public void init(SleeTCKTestUtils utils) {
        this.utils = utils;
    }

    /**
     * Perform the actual test.
     */

    public TCKTestResult run() throws Exception {

        if (ServiceState.fromInt(ServiceState.SERVICE_ACTIVE).toInt() != ServiceState.SERVICE_ACTIVE)
            return TCKTestResult.failed(4113, "ServiceState.to/fromInt() not working correctly.");

        if (!ServiceState.STOPPING.isStopping())
            return TCKTestResult.failed(4119, "ServiceState.isStopping() returned false for ServiceState.SERVICE_STOPPING object.");

        if (ServiceState.fromInt(ServiceState.SERVICE_ACTIVE).equals(ServiceState.fromInt(ServiceState.SERVICE_INACTIVE)))
            return TCKTestResult.failed(4121, "ServiceState.equals(ServiceState) returned true for different states.");

        if (!ServiceState.fromInt(ServiceState.SERVICE_ACTIVE).equals(ServiceState.fromInt(ServiceState.SERVICE_ACTIVE)))
            return TCKTestResult.failed(4121, "ServiceState.equals(ServiceState) returned false for the same states.");

        try {
            ServiceState.ACTIVE.hashCode();
        } catch (Exception e) {
            return TCKTestResult.failed(4123, "ServiceState.hashCode() threw an exception.");
        }

        try {
            ServiceState.ACTIVE.toString();
        } catch (Exception e) {
            return TCKTestResult.failed(4125, "ServiceState.toString() threw an exception.");
        }
        
        if (ServiceState.ACTIVE.toString() == null)
            return TCKTestResult.failed(4125, "ServiceState.toString() returned null.");


        return TCKTestResult.passed();
    }

    /**
     * Do all the pre-run configuration of the test.
     */

    public void setUp() throws Exception {

        // Install the Deployable Units
        String duPath = utils.getTestParams().getProperty(SERVICE_DU_PATH_PARAM);
        duID = utils.install(duPath);

        // Activate the DU
        utils.activateServices(duID, true);

    }

    /**
     * Clean up after the test.
     */

    public void tearDown() throws Exception {
        utils.getLog().fine("Disconnecting from resource");
        utils.getResourceInterface().clearActivities();
        utils.getResourceInterface().removeResourceListener();
        utils.getLog().fine("Deactivating and uninstalling service");
        utils.deactivateAllServices();
        utils.uninstallAll();
    }

    private SleeTCKTestUtils utils;
    private DeployableUnitID duID;
}
