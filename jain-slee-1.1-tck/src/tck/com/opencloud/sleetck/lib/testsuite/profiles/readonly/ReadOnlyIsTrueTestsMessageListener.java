/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.profiles.readonly;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;

import javax.slee.TransactionRolledbackLocalException;
import javax.slee.profile.ProfileTable;
import javax.slee.profile.ReadOnlyProfileException;
import javax.slee.transaction.SleeTransaction;
import javax.slee.transaction.SleeTransactionManager;
import javax.transaction.Status;

import com.opencloud.logging.StdErrLog;
import com.opencloud.sleetck.lib.profileutils.BaseMessageSender;
import com.opencloud.sleetck.lib.rautils.MessageHandler;
import com.opencloud.sleetck.lib.rautils.RMIObjectChannel;
import com.opencloud.sleetck.lib.rautils.TCKRAUtils;

/**
 * Provides callback method for handling messages sent from the TCK test to the RA.
 * The actual test code is located in the 'handleMessage()' method.
 */
public class ReadOnlyIsTrueTestsMessageListener extends UnicastRemoteObject implements MessageHandler {

    public static final String PROFILE_TABLE_NAME = "ReadOnlyIsTrueTestsMessageListenerProfileTable";
    public static final String PROFILE_NAME = "ReadOnlyIsTrueTestsMessageListenerProfile";
    public static final String PROFILE_NAME2 = "ReadOnlyIsTrueTestsMessageListenerProfile2";

    private ReadOnlyIsTrueTestsResourceAdaptor ra;

    public ReadOnlyIsTrueTestsMessageListener(ReadOnlyIsTrueTestsResourceAdaptor ra) throws RemoteException {
        super();

        try {
            out = TCKRAUtils.lookupRMIObjectChannel();
            msgSender = new BaseMessageSender(out, new StdErrLog());
        }
        catch (Exception e) {
            ra.getLog().warning("Exception occurred when trying to acquire RMIObjectChannel: ",e);
        }

        this.ra = ra;
    }

    public boolean handleMessage(Object message) throws RemoteException {

        //run the tests, if both run through successfully and completely (=both return 'true')
        //send success msg back to TCK test
        if (test_PROF_LOCAL_SET() && test_PROF_LOCAL_INDIR_SET() && test_PROF_TAB_CREATE() &&
                test_PROF_TAB_REMOVE() && test_PROF_LOCAL_REMOVE() ) {
            msgSender.sendSuccess(1110068, "Test successful.");
        }
        return true;
    }

    private boolean test_PROF_LOCAL_REMOVE() {
        SleeTransactionManager txnManager = ra.getResourceAdaptorContext().getSleeTransactionManager();
        SleeTransaction txn=null;

        try {
            ProfileTable profileTable = ra.getResourceAdaptorContext().getProfileTable(PROFILE_TABLE_NAME);
            //start a new TXN as profileTable.find is mandatory transactional
            txn = txnManager.beginSleeTransaction();

            //1110074: Sbb may not remove a profile via ProfileLocal interface
            try {
                ReadOnlyTestsProfileLocal profileLocal = (ReadOnlyTestsProfileLocal) profileTable.find(PROFILE_NAME);
                profileLocal.remove();
                msgSender.sendFailure(1110074, "Removing profile " +PROFILE_NAME +" via ProfileLocal interface "+
                        " was successful but should have failed as profile spec is marked as read-only.");
                return false;
            }
            catch ( TransactionRolledbackLocalException e) {
                msgSender.sendLogMsg("Caught expected exception when removing profile via ProfileLocal interface: "+e.getClass().getName());
            }
            catch (Exception e) {
                msgSender.sendFailure(1110074, "Wrong type of exception received: "+e.getClass().getName()+" Expected: javax.slee.TransactionRolledbackLocalException");
                return false;
            }

            txn.rollback();
            txn = null;

        } catch (Exception e) {
            msgSender.sendException(e);
            return false;
        } finally {
            try {
                if (txn!=null)
                    txn.rollback();
            } catch (Exception ex) {
                ra.getLog().warning("Error occured when trying to rollback RA started TXN.", ex);
                msgSender.sendLogMsg("Exception occurred when trying to safely rollback RA started TXN: "+ex.getMessage());
            }
        }

        return true;
    }

    private boolean test_PROF_TAB_REMOVE() {
        SleeTransactionManager txnManager = ra.getResourceAdaptorContext().getSleeTransactionManager();
        SleeTransaction txn=null;

        try {
            ProfileTable profileTable = ra.getResourceAdaptorContext().getProfileTable(PROFILE_TABLE_NAME);
            //start a new TXN as profileTable.find is mandatory transactional
            txn = txnManager.beginSleeTransaction();

            //1110073: Sbb may not remove a profile via ProfileTable interface
            try {
                profileTable.remove(PROFILE_NAME);
                msgSender.sendFailure(1110073, "Removing profile " +PROFILE_NAME +" via ProfileTable interface "+
                        " was successful but should have failed as profile spec is marked as read-only.");
                return false;
            }
            catch (ReadOnlyProfileException e) {
                msgSender.sendLogMsg("Caught expected exception when removing profile via ProfileTable interface: "+e.getClass().getName());
            }
            catch (Exception e) {
                msgSender.sendFailure(1110073, "Wrong type of exception received: "+e.getClass().getName()+" Expected: javax.slee.profile.ReadOnlyProfileException");
                return false;
            }

            txn.commit();
            txn = null;

        } catch (Exception e) {
            msgSender.sendException(e);
            return false;
        } finally {
            try {
                if (txn!=null)
                    txn.rollback();
            } catch (Exception ex) {
                ra.getLog().warning("Error occured when trying to rollback RA started TXN.", ex);
                msgSender.sendLogMsg("Exception occurred when trying to safely rollback RA started TXN: "+ex.getMessage());
            }
        }

        return true;
    }

    private boolean test_PROF_TAB_CREATE() {
        SleeTransactionManager txnManager = ra.getResourceAdaptorContext().getSleeTransactionManager();
        SleeTransaction txn=null;

        try {
            ProfileTable profileTable = ra.getResourceAdaptorContext().getProfileTable(PROFILE_TABLE_NAME);
            //start a new TXN as profileTable.find is mandatory transactional
            txn = txnManager.beginSleeTransaction();

            //1110073: Sbb may not create a profile via ProfileTable interface
            try {
                profileTable.create(PROFILE_NAME2);
                msgSender.sendFailure(1110073, "Creating profile " +PROFILE_NAME2 +" via ProfileTable interface "+
                        " was successful but should have failed as profile spec is marked as read-only.");
                return false;
            }
            catch (ReadOnlyProfileException e) {
                msgSender.sendLogMsg("Caught expected exception when creating profile via ProfileTable interface: "+e.getClass().getName());
            }
            catch (Exception e) {
                msgSender.sendFailure(1110073, "Wrong type of exception received: "+e.getClass().getName()+" Expected: javax.slee.profile.ReadOnlyProfileException");
                return false;
            }

            txn.commit();
            txn = null;

        } catch (Exception e) {
            msgSender.sendException(e);
            return false;
        } finally {
            try {
                if (txn!=null)
                    txn.rollback();
            } catch (Exception ex) {
                ra.getLog().warning("Error occured when trying to rollback RA started TXN.", ex);
                msgSender.sendLogMsg("Exception occurred when trying to safely rollback RA started TXN: "+ex.getMessage());
            }
        }

        return true;
    }

    private boolean test_PROF_LOCAL_INDIR_SET() {
        SleeTransactionManager txnManager = ra.getResourceAdaptorContext().getSleeTransactionManager();
        SleeTransaction txn=null;

        try {
            ProfileTable profileTable = ra.getResourceAdaptorContext().getProfileTable(PROFILE_TABLE_NAME);
            //start a new TXN as profileTable.find is mandatory transactional
            txn = txnManager.beginSleeTransaction();

            //1110071: Profile may not be modified via a call to a Local interface business method whose implementation calls a CMP set accessor
            ReadOnlyTestsProfileLocal profileLocal = (ReadOnlyTestsProfileLocal) profileTable.find(PROFILE_NAME);
            try {
                profileLocal.localSetValue("newValue");
                msgSender.sendFailure(1110071,"Invoking business method on ProfileLocal interface that indirectly calls CMP set accessor" +
                        " was successful but should have failed as profile spec is marked as read-only.");
                return false;
            }
            catch ( TransactionRolledbackLocalException e) {
                msgSender.sendLogMsg("Caught expected exception when setting new profile value indirectly via business method on ProfileLocal interface: "+e.getClass().getName());
            }
            catch (Exception e) {
                msgSender.sendFailure(1110071, "Wrong type of exception received: "+e.getClass().getName()+
                        " Expected: javax.slee.TransactionRolledbackLocalException");
                return false;
            }

            txn.rollback();
            txn = null;

        } catch (Exception e) {
            msgSender.sendException(e);
            return false;
        } finally {
            try {
                if (txn!=null)
                    txn.rollback();
            } catch (Exception ex) {
                ra.getLog().warning("Error occured when trying to rollback RA started TXN.", ex);
                msgSender.sendLogMsg("Exception occurred when trying to safely rollback RA started TXN: "+ex.getMessage());
            }
        }

        return true;
    }

    private boolean test_PROF_LOCAL_SET() {
        SleeTransactionManager txnManager = ra.getResourceAdaptorContext().getSleeTransactionManager();
        SleeTransaction txn=null;

        try {
            ProfileTable profileTable = ra.getResourceAdaptorContext().getProfileTable(PROFILE_TABLE_NAME);
            //start a new TXN as profileTable.find is mandatory transactional
            txn = txnManager.beginSleeTransaction();

            //1110071: Profile may not be modified via set on Local interface
            ReadOnlyTestsProfileLocal profileLocal = (ReadOnlyTestsProfileLocal) profileTable.find(PROFILE_NAME);
            try {
                profileLocal.setValue("newValue");
                msgSender.sendFailure(1110071,"Set accessor method on ProfileLocal interface was successful but should have " +
                        "failed as profile spec is marked as read-only.");
                return false;
            }
            catch ( TransactionRolledbackLocalException e) {
                msgSender.sendLogMsg("Caught expected exception when setting new profile value via ProfileLocal interface: "+e.getClass().getName());
            }
            catch (Exception e) {
                msgSender.sendFailure(1110071,"Wrong type of exception received: "+e.getClass().getName()+
                        " Expected: javax.slee.TransactionRolledbackLocalException");
                return false;
            }

            //1110072: In the case of a method on the Profile Local interface being invoked,
            //the ReadOnlyProfileException thrown from the CMP setter method in the Profile object
            //causes the current transaction to be marked for rollback if the
            //ReadOnlyProfileException propagates unhandled out of the Profile object
            if (txn.getStatus()!= Status.STATUS_MARKED_ROLLBACK ) {
                msgSender.sendFailure(1110072, "Current TXN context should have been marked for rollback " +
                        "as the raised ReadOnlyProfileException propagated unhandled out of the Profile object.");
            }

            txn.rollback();
            txn = null;

        } catch (Exception e) {
            msgSender.sendException(e);
            return false;
        } finally {
            try {
                if (txn!=null)
                    txn.rollback();
            } catch (Exception ex) {
                ra.getLog().warning("Error occured when trying to rollback RA started TXN.", ex);
                msgSender.sendLogMsg("Exception occurred when trying to safely rollback RA started TXN: "+ex.getMessage());
            }
        }

        return true;
    }

    private BaseMessageSender msgSender;
    private RMIObjectChannel out;

}

