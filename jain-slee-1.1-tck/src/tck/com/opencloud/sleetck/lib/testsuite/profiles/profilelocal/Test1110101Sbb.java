/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.profiles.profilelocal;

import java.util.HashMap;
import java.util.Hashtable;

import javax.naming.Context;
import javax.slee.ActivityContextInterface;
import javax.slee.RolledBackContext;
import javax.slee.TransactionRolledbackLocalException;
import javax.slee.profile.ProfileFacility;
import javax.slee.profile.ProfileTable;
import javax.slee.profile.ReadOnlyProfileException;

import com.opencloud.sleetck.lib.resource.events.TCKResourceEventX;
import com.opencloud.sleetck.lib.sbbutils.BaseTCKSbb;
import com.opencloud.sleetck.lib.sbbutils.TCKSbbUtils;
import com.opencloud.sleetck.lib.sbbutils.events2.SbbBaseMessageComposer;

public abstract class Test1110101Sbb extends BaseTCKSbb {

    public static final int PASS_BY_REF = 0;
    public static final int TRY_CMP_GET_SET = 1;
    public static final int PASS_NON_RMI_IIOP = 2;
    public static final int CHECK_ROLLBACK = 3;
    public static final int CHECK_REMOVE = 4;
    public static final int GET_SET_PROFILE_LOCAL_VIA_BUSINESS = 5;

    public static final String PROFILE_TABLE_NAME = "Test1110101ProfileTable";
    public static final String PROFILE_NAME = "Test1110101Profile";

    private void sendLogMsgCall (String msg) {
        HashMap map = SbbBaseMessageComposer.getLogMsg(msg);
        try {
            TCKSbbUtils.getResourceInterface().callTest(map);
        }
        catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }
    }

    private void sendResult(boolean result, int id, String msg) {
        HashMap map = SbbBaseMessageComposer.getSetResultMsg(result, id, msg);
        try {
            TCKSbbUtils.getResourceInterface().sendSbbMessage(map);

        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }
    }

    private void do_PASS_BY_REF(ProfileTable profileTable) {
        //1110099: Business methods defined in ProfileLocal interface have Pass-by-reference semantics
        Test1110101ProfileLocal profileLocal = (Test1110101ProfileLocal) profileTable.find(PROFILE_NAME);
        Hashtable t = new Hashtable();
        String key = "key";
        String value = "value";
        Hashtable t_new = profileLocal.businessInsert(t, key, value);
        if (t==t_new && t.containsKey(key))
            sendResult(true, 1110099, "Pass-By-Ref worked fine.");
        else
            sendResult(false, 1110099, "Pass-By-Ref test failed.");
    }

    private void do_TRY_CMP_GET_SET(ProfileTable profileTable) {
        //1110101: If ProfileLocal interface extends ProfileCMP interface all CMP get/set methods are available to Sbb
        Test1110101ProfileLocal profileLocal = (Test1110101ProfileLocal) profileTable.find(PROFILE_NAME);
        profileLocal.setValue("new value");
        profileLocal.getValue();
        profileLocal.setValue2("new value");
        profileLocal.getValue2();
        sendResult(true, 1110101, "Using accessors defined in CMP interface via ProfileLocal interface worked fine.");
    }

    private void do_GET_SET_PROFILE_LOCAL_VIA_BUSINESS(ProfileTable profileTable) {
        //1110642: Receive the Profile local object as a result of invoking a business method on a
        //Profile local object. The callee may receive the Profile local object through one of its
        //input arguments. The caller may receive the Profile local object through the return argument.
        Test1110101ProfileLocal profileLocal = (Test1110101ProfileLocal) profileTable.find(PROFILE_NAME);
        Test1110101ProfileLocal internal = (Test1110101ProfileLocal)profileLocal.businessGetProfileLocal();
        if (!internal.getProfileName().equals(PROFILE_NAME)) {
            sendResult(false, 1110642, "Obtaining ProfileLocal object via business method call did not return a proper ProfileLocal object for the chosen profile.");
            return;
        }

        if (!profileLocal.businessCompareProfileLocal(profileLocal)) {
            sendResult(false, 1110642, "Sending ProfileLocal object via business method call did not work as expected.");
            return;
        }

        sendResult(true, 1110642, "Receiving and setting a ProfileLocal object via business method call worked as expected.");
    }

    private void do_PASS_NON_RMI_IIOP(ProfileTable profileTable) {
        //1110106: As pass-by-ref semantics are in place non-RMI-IIOP types can be used as arguments and return types
        Test1110101ProfileLocal profileLocal = (Test1110101ProfileLocal) profileTable.find(PROFILE_NAME);
        MyNonSerializable obj = new MyNonSerializable();
        MyNonSerializable obj_new = profileLocal.businessNonSerial(obj, "new value");
        if (obj==obj_new && obj.value.equals("new value"))
            sendResult(true, 1110106, "Passing non-RMI-IIOP parameter worked fine.");
        else
            sendResult(false, 1110106, "Passing non-RMI-IIOP parameter failed.");
    }

    private void do_CHECK_ROLLBACK(ProfileTable profileTable) {
        //1110120+1110122: If a business method throws an unchecked exception current transaction is marked
        //for rollback and a TransactionRolledbackLocalException is thrown
        Test1110101ProfileLocal profileLocal = (Test1110101ProfileLocal) profileTable.find(PROFILE_NAME);
        ReadOnlyProfileException rope = new ReadOnlyProfileException("Testexception");
        try {
            profileLocal.raiseException(rope);
            sendResult(false, 1110120, "TransactionRolledbackLocalException should have been thrown.");
            return;
        }
        catch (TransactionRolledbackLocalException e) {
            sendLogMsgCall("TransactionRolledbackLocalException was received as expected after business method threw an unchecked exception.");
            //11101126: The runtime exception thrown by the Profile object will be the cause of the TransactionRolledbackLocalException i.e. it is accessible via java.lang.Throwable.getCause().
            if (rope.equals(e.getCause()))
                sendLogMsgCall("Cause of the TransactionRolledbackLocalException was the raised ReadOnlyProfileException as expected.");
            else {
                sendResult(false, 11101126, "Cause of the TransactionRolledbackLocalException should have been the raised ReadOnlyProfileException but was "+e.getCause());
                return;
            }
        }
        catch (Exception e) {
            sendResult(false, 1110120, "Wrong type of exception thrown: "+e.getClass().getName()+
                    " Expected: javax.slee.TransactionRolledbackLocalException");
            return;
        }
        if (getSbbContext().getRollbackOnly())
            sendResult(true, 1110122, "As expected TXN was marked for rollback after business method threw an unchecked exception.");
        else
            sendResult(false, 1110122, "TransactionRolledbackLocalException was caught but transaction has not been marked for rollback.");
    }

    private void do_CHECK_REMOVE(ProfileTable profileTable) {
        //1110651: If a ProfileLocal object refers to a Profile which has already been removed
        //a TransactionRolledbackLocalException is thrown (changed in PFD!! Previously was NoSuchObjectLocalException)
        Test1110101ProfileLocal profileLocal = (Test1110101ProfileLocal) profileTable.find(PROFILE_NAME);
        profileTable.remove(PROFILE_NAME);
        try {
            profileLocal.getValue();
            sendResult(false, 1110651, "TransactionRolledbackLocalException should have been thrown as profile is already removed.");
        }
        catch (TransactionRolledbackLocalException e) {
            sendResult(true, 1110651, "TransactionRolledbackLocalException caught as expected when trying to call methods on an invalid ProfileLocal object.");
        }
        catch (Exception e) {
            sendResult(false, 1110651, "Wrong type of exception thrown: "+e.getClass().getName()+
                    " Expected: javax.slee.TransactionRolledbackLocalException");
        }
    }


    public void onTCKResourceEventX1(TCKResourceEventX event, ActivityContextInterface aci) {
        try {

            int operationID = ((Integer)event.getMessage()).intValue();
            Context env = TCKSbbUtils.getSbbEnvironment();
            ProfileFacility profileFacility = (ProfileFacility)env.lookup("slee/facilities/profile");
            ProfileTable profileTable = profileFacility.getProfileTable(PROFILE_TABLE_NAME);
            Test1110101ProfileLocal profileLocal = null;

            switch (operationID) {
            case PASS_BY_REF:
                do_PASS_BY_REF(profileTable);
                break;
            case TRY_CMP_GET_SET:
                do_TRY_CMP_GET_SET(profileTable);
                break;
            case GET_SET_PROFILE_LOCAL_VIA_BUSINESS:
                do_GET_SET_PROFILE_LOCAL_VIA_BUSINESS(profileTable);
                break;
            case PASS_NON_RMI_IIOP:
                do_PASS_NON_RMI_IIOP(profileTable);
                break;
            case CHECK_ROLLBACK:
                do_CHECK_ROLLBACK(profileTable);
                break;
            case CHECK_REMOVE:
                do_CHECK_REMOVE(profileTable);
                break;
            }

        } catch(Exception e) {
            TCKSbbUtils.handleException(e);
        }
    }

    public void sbbRolledBack(RolledBackContext context) {
        //ignore rollback from tests perspective, otherwise on rollback a TCKError is propagated to the TCK test.
    }

}
