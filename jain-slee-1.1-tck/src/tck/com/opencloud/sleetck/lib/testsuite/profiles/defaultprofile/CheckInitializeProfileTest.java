/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.profiles.defaultprofile;

import javax.management.ObjectName;
import javax.slee.management.ManagementException;
import javax.slee.profile.ProfileSpecificationID;
import javax.slee.profile.UnrecognizedProfileNameException;
import javax.slee.profile.UnrecognizedProfileTableNameException;

import com.opencloud.sleetck.lib.AbstractSleeTCKTest;
import com.opencloud.sleetck.lib.TCKTestErrorException;
import com.opencloud.sleetck.lib.TCKTestFailureException;
import com.opencloud.sleetck.lib.TCKTestResult;
import com.opencloud.sleetck.lib.infra.SleeTCKComponentConstants;
import com.opencloud.sleetck.lib.testsuite.profiles.ProfileTestConstants;
import com.opencloud.sleetck.lib.testutils.Assert;
import com.opencloud.sleetck.lib.testutils.ComponentIDLookup;
import com.opencloud.sleetck.lib.testutils.ProfileUtils;
import com.opencloud.sleetck.lib.testutils.jmx.ProfileProvisioningMBeanProxy;

/**
 * Checks assertions 924 and 926 regarding profile initialization.
 */
public class CheckInitializeProfileTest extends AbstractSleeTCKTest {

    private static final String TABLE_NAME = "tck.CheckInitializeProfileTest.table";
    private static final String PROFILE_SPEC_NAME = "CheckInitializeProfile";

    public TCKTestResult run() throws Exception {
        // create the profile table
        ProfileProvisioningMBeanProxy profileProvisioning = profileUtils.getProfileProvisioningProxy();
        ProfileSpecificationID profileSpecID = new ComponentIDLookup(utils()).lookupProfileSpecificationID(
            PROFILE_SPEC_NAME,SleeTCKComponentConstants.TCK_VENDOR,"1.0");
        getLog().info("Creating profile table: "+TABLE_NAME);
        profileProvisioning.createProfileTable(profileSpecID,TABLE_NAME);
        isTableCreated = true;

        // check that the default profile has been initialized
        ObjectName defaultProfileName = profileProvisioning.getDefaultProfile(TABLE_NAME);
        CheckInitializeProfileProxy defaultProfileProxy = getProfileProxy(defaultProfileName);
        getLog().info("Checking default profile");
        String initialValue = defaultProfileProxy.getValue();
        if(initialValue == null) throw new TCKTestFailureException(924,"The default profile was not initialized.");
        Assert.assertEquals(924,"The default profile was initialized to an unexpected value",
            CheckInitializeProfileCMP.INITIALIZED_VALUE,initialValue);

        // Check that the instance variable of the default profile are also initialized
        String initialValue2 = defaultProfileProxy.getValue2();
        getLog().info("initial Value2:" + initialValue2);
        if(initialValue2 == null) throw new TCKTestFailureException(4330,"profileInitialize did not initialize the " +
                                                                         "profile's transient fields");
        Assert.assertEquals(4330,"profileInitialize initialized the profile's transient fields to an unexpected value",
            CheckInitializeProfileCMP.INITIALIZED_VALUE, initialValue2);

        // create a new profile and check that it takes its values from the default profile
        getLog().info("Creating and checking a new profile");
        ObjectName firstNewProfileName = profileProvisioning.createProfile(TABLE_NAME,"FirstProfile");
        CheckInitializeProfileProxy firstNewProfileProxy = getProfileProxy(firstNewProfileName);
        initialValue = firstNewProfileProxy.getValue();
        Assert.assertEquals(926,"A new profile did not take its values from the default profile",
            CheckInitializeProfileCMP.INITIALIZED_VALUE,initialValue);

        // edit the default profile
        String newDefaultValue = "Changed";
        getLog().info("Editing default profile for table "+TABLE_NAME);
        defaultProfileProxy.editProfile();
        defaultProfileProxy.setValue(newDefaultValue);
        defaultProfileProxy.commitProfile();
        defaultProfileProxy.closeProfile();

        // create another profile and check that it takes its values from the
        // latest version of the default profile
        getLog().info("Creating and checking another new profile");
        ObjectName secondNewProfileName = profileProvisioning.createProfile(TABLE_NAME,"SecondProfile");
        CheckInitializeProfileProxy secondNewProfileProxy = getProfileProxy(secondNewProfileName);
        initialValue = secondNewProfileProxy.getValue();
        if(!newDefaultValue.equals(initialValue)) {
            if(initialValue.equals(CheckInitializeProfileCMP.INITIALIZED_VALUE)) Assert.fail(926,
                "A new profile did not take its values from the current version of the default profile; "+
                "it copied its values from the initial version of the default profile");
            else Assert.fail(926,"A new profile did not take its values from the default profile. "+
                "Current default value="+newDefaultValue+", value in new profile="+initialValue);
        }
        return TCKTestResult.passed();
    }

    public void setUp() throws Exception {
        String duPath = utils().getTestParams().getProperty(ProfileTestConstants.PROFILE_SPEC_DU_PATH_PARAM);
        getLog().fine("Installing profile specification");
        utils().install(duPath);
        profileUtils = new ProfileUtils(utils());
    }

    public void tearDown() throws Exception {
        // delete profile table
        try {
            if(profileUtils != null && isTableCreated) {
                getLog().fine("Removing profile table");
                profileUtils.removeProfileTable(TABLE_NAME);
            }
        } catch(Exception e) {
            getLog().warning("Caught exception while trying to remove profile table:");
            getLog().warning(e);
        }
        profileUtils = null;
        isTableCreated = false;
        super.tearDown();
    }

    private CheckInitializeProfileProxy getProfileProxy(ObjectName mbeanName) throws TCKTestErrorException,
                    UnrecognizedProfileNameException, UnrecognizedProfileTableNameException, ManagementException {
        CheckInitializeProfileProxy rProxy = new CheckInitializeProfileProxy(mbeanName,utils().getMBeanFacade());
        return rProxy;
    }

    private ProfileUtils profileUtils;
    private boolean isTableCreated;

}
