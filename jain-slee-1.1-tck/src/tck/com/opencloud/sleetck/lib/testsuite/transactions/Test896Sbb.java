/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.transactions;

import javax.slee.ActivityEndEvent;
import javax.slee.ActivityContextInterface;
import javax.slee.SbbContext;
import javax.slee.Address;
import javax.slee.SbbLocalObject;
import javax.slee.facilities.Level;
import javax.slee.ChildRelation;
import javax.slee.CreateException;
import javax.slee.TransactionRolledbackLocalException;

import com.opencloud.sleetck.lib.TCKTestErrorException;
import com.opencloud.sleetck.lib.sbbutils.BaseTCKSbb;
import com.opencloud.sleetck.lib.sbbutils.TCKSbbUtils;
import com.opencloud.sleetck.lib.sbbutils.TCKSbbConstants;
import com.opencloud.sleetck.lib.resource.events.TCKResourceEventX;
import com.opencloud.sleetck.lib.resource.sbbapi.TCKResourceSbbInterface;

import java.util.HashMap;
import java.util.Iterator;

public abstract class Test896Sbb extends BaseTCKSbb {

    public static final String MESSAGE = "Test896RuntimeException";

        /*
          public void sbbExceptionThrown(Exception exception, Object event, ActivityContextInterface aci) {
          //        if (exception instanceof java.lang.RuntimeException && exception.getMessage().equals(MESSAGE)) {
          try {
          TCKSbbUtils.createTrace(getSbbID(), Level.FINE, "In sbbExceptionThrown(): " + exception.getClass().toString(), null);
          } catch (Exception e) {
          }
          
          if (exception instanceof java.lang.RuntimeException) {
          //            setExceptionSeen(true);
          return;
          }
          super.sbbExceptionThrown(exception, event, aci);
          }
        */

    public void sbbRolledBack(javax.slee.RolledBackContext context) {
    }

    // Initial event
    public void onTCKResourceEventX1(TCKResourceEventX ev, ActivityContextInterface aci) {

        Test896ChildSbbLocalObject child = null;
        try {
            child = (Test896ChildSbbLocalObject) getChildRelation().create();
        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
            return;
        }

        boolean passed = false;
        try {
            child.throwRuntimeException();
        } catch (TransactionRolledbackLocalException e) {
            passed = true;
        }

        HashMap map = new HashMap();

        if (!passed) {
            map.put("Result", new Boolean(false));
            map.put("Message", "TransactionRolledbackLocalException was not received as expected.");
            map.put("ID", new Integer(899));
            try {
                TCKSbbUtils.getResourceInterface().sendSbbMessage(map);
            } catch (Exception e) {
                TCKSbbUtils.handleException(e);
            }
            return;
        }
        
        passed = false;
        try {
            // Child SBB no longer exists, calling local object method on it should fail.
            child.isExceptionSeen();
        } catch (TransactionRolledbackLocalException e) {
            passed = true;
        }

        if (passed == false) {
            map.put("Result", new Boolean(false));
            map.put("Message", "Child SBB object whose invoked method threw RuntimeException was not discarded.");
            map.put("ID", new Integer(898));
            try {
                TCKSbbUtils.getResourceInterface().sendSbbMessage(map);
            } catch (Exception e) {
                TCKSbbUtils.handleException(e);
            }
            return;
        }


        if (getSbbContext().getRollbackOnly()) {
            map.put("Result", new Boolean(true));
            map.put("Message", "Ok");
            try {
                TCKSbbUtils.getResourceInterface().sendSbbMessage(map);
            } catch (Exception e) {
                TCKSbbUtils.handleException(e);
            }
            return;
        }

        map.put("Result", new Boolean(false));
        map.put("Message", "The transaction was not marked for rollback.");
        map.put("ID", new Integer(896));
        try {
            TCKSbbUtils.getResourceInterface().sendSbbMessage(map);
        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }
    }

    public abstract ChildRelation getChildRelation();


}
