/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.events.eventcontext;

import javax.slee.ActivityContextInterface;
import javax.slee.InitialEventSelector;
import javax.slee.EventContext;
import javax.slee.facilities.Tracer;

import com.opencloud.sleetck.lib.sbbutils.BaseTCKSbb;
import com.opencloud.sleetck.lib.sbbutils.TCKSbbUtils;
import com.opencloud.sleetck.lib.resource.events.TCKResourceEventX;

import java.util.HashMap;

/**
 * AssertionID(1108026): Test If the transaction does not commit then 
 * the Event Context will not be “suspended”.
 */

public abstract class Test1108026Sbb2 extends BaseTCKSbb {
    public static final String TRACE_MESSAGE_TCKResourceEventX1 = "Test1108026Sbb2:I got TCKResourceEventX1 on ActivityA";

    public static final String TRACE_MESSAGE_TCKResourceEventX2 = "Test1108026Sbb2:I got TCKResourceEventX2 on ActivityA";

    public InitialEventSelector initialEventSelector(InitialEventSelector ies) {
        ies.setCustomName("test");
        return ies;
    }

    public void onTCKResourceEventX1(TCKResourceEventX event, ActivityContextInterface aci, EventContext context) {
        try {
            //send message to TCK: I got TCKResourceEventX1 on ActivityA
            tracer = getSbbContext().getTracer("com.test");
            tracer.info(TRACE_MESSAGE_TCKResourceEventX1);

            setPassed(true);

        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }
    }

    public void onTCKResourceEventX2(TCKResourceEventX event, ActivityContextInterface aci, EventContext context) {
        try {
            //send message to TCK: I got TCKResourceEventX2 on ActivityA
            tracer = getSbbContext().getTracer("com.test");
            tracer.info(TRACE_MESSAGE_TCKResourceEventX2);

            if (getPassed()) {
                sendResultToTCK("Test1108026Test", true, "If the transaction does not commit then the Event Context will not be suspended, "
                        + "the event should be able to be delivered to the second SBB.", 1108026);

            } else {
                sendResultToTCK("Test1108026Test", false, "SBB2:onTCKResourceEventX2-ERROR: When the transaction does not commit in the first SBB, then the "
                        + "Event Context will not be suspended, and the event has not been delivered to the "
                        + "second SBB.", 1108026);
            }
            return;
        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }
    }

    private void sendResultToTCK(String testName, boolean result, String message, int failedAssertionID) throws Exception {
        HashMap sbbData = new HashMap();
        sbbData.put("testname", testName);
        sbbData.put("result", result ? "pass" : "fail");
        sbbData.put("message", message);
        sbbData.put("id", new Integer(failedAssertionID));
        TCKSbbUtils.getResourceInterface().sendSbbMessage(sbbData);
    }

    private Tracer tracer = null;

    public abstract void setPassed(boolean passed);
    public abstract boolean getPassed();

}
