/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.facilities.timerfacility;

import com.opencloud.sleetck.lib.resource.events.TCKResourceEventX;
import com.opencloud.sleetck.lib.sbbutils.BaseTCKSbb;
import com.opencloud.sleetck.lib.sbbutils.TCKSbbUtils;
import com.opencloud.sleetck.lib.sbbutils.events.TCKSbbEvent;
import com.opencloud.sleetck.lib.sbbutils.events.TCKSbbEventImpl;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.slee.*;
import javax.slee.facilities.Level;
import javax.slee.facilities.TimerFacility;
import javax.slee.facilities.TimerID;
import javax.slee.facilities.TimerOptions;
import javax.slee.nullactivity.NullActivity;
import javax.slee.nullactivity.NullActivityContextInterfaceFactory;
import javax.slee.nullactivity.NullActivityFactory;
import java.util.HashMap;

/**
 * Test that TimerFacility.setTimer() methods increment aci ref counts
 */
public abstract class SetTimerRefCountSbb extends BaseTCKSbb {

    public void setSbbContext(SbbContext context) {
        super.setSbbContext(context);
        try {
            Context myEnv = (Context) new InitialContext().lookup("java:comp/env");

            nullActivityFactory = (NullActivityFactory) myEnv.lookup("slee/nullactivity/factory");
            nullACIFactory = (NullActivityContextInterfaceFactory) myEnv.lookup("slee/nullactivity/activitycontextinterfacefactory");
            timerFacility = (TimerFacility) myEnv.lookup("slee/facilities/timer");
        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }
    }

    // test starts here...
    public void onTCKResourceEventX1(TCKResourceEventX event, ActivityContextInterface aci) {
        try {
            TCKSbbUtils.createTrace(getSbbID(),Level.INFO,"Received start message",null);
            setTestName((String)event.getMessage());

            // create a null aci
            NullActivity nobody = nullActivityFactory.createNullActivity();
            ActivityContextInterface nobodyACI = nullACIFactory.getActivityContextInterface(nobody);

            // set a timer on the aci but don't attach - this ensures only the facility has
            // a reference to the aci
            // use either variant of setTimer()
            if (getTestName().equals("setTimerRefCount1")) {
                setTimerID(timerFacility.setTimer(nobodyACI, null, 0, new TimerOptions()));
            } else {
                setTimerID(timerFacility.setTimer(nobodyACI, null, 0, 60000, 0, new TimerOptions()));
            }

            // fire an event - if the SLEE correctly increments the aci ref count, we should
            // receive this event before the aci's ActivityEndEvent
            fireCustomEvent(new TCKSbbEventImpl(new Integer(0)), nobodyACI, null);

        } catch(Exception e) {
            TCKSbbUtils.handleException(e);
        }
    }

    public void onCustomEvent(TCKSbbEvent event, ActivityContextInterface aci) {
        try {
            TCKSbbUtils.createTrace(getSbbID(),Level.INFO,"Received custom event",null);

            if (getReceivedEndEvent()) {
                // FAILED - we should have got this event before the end event
                setResultFailed(1185, "setTimer() did not increment aci ref count");
            } else {
                // PASSED
                setResultPassed("setTimer() incremented aci ref count correctly");
            }

            // detach from the null aci so it ends implicitly
            aci.detach(getSbbContext().getSbbLocalObject());

            // cancel the timer
            timerFacility.cancelTimer(getTimerID());

            // we are done
            sendResultToTCK();

        } catch(Exception e) {
            TCKSbbUtils.handleException(e);
        }
    }

    public void onActivityEndEvent(ActivityEndEvent event, ActivityContextInterface aci) {
        try {
            TCKSbbUtils.createTrace(getSbbID(),Level.INFO,"Received activity end event",null);

            if (aci.getActivity() instanceof NullActivity) {
                setReceivedEndEvent(true);
            }

        } catch(Exception e) {
            TCKSbbUtils.handleException(e);
        }
    }

    // return a static string so that this SBB entity gets selected every time
    public InitialEventSelector getStaticConvergenceName(InitialEventSelector ies) {
        ies.setCustomName("foobar");
        return ies;
    }

    public abstract void fireCustomEvent(TCKSbbEvent event, ActivityContextInterface aci, Address address);

    // receivedEndEvent flag
    public abstract void setReceivedEndEvent(boolean received);
    public abstract boolean getReceivedEndEvent();

    // timerID
    public abstract void setTimerID(TimerID timerID);
    public abstract TimerID getTimerID();

    public abstract void setTestName(String testName);
    public abstract String getTestName();

    private void sendResultToTCK() throws Exception {
        HashMap sbbData = new HashMap();
        sbbData.put("testname", getTestName());
        sbbData.put("result", result?"pass":"fail");
        sbbData.put("message", message);
        sbbData.put("id", new Integer(failedAssertionID));
        TCKSbbUtils.getResourceInterface().sendSbbMessage(sbbData);
    }

    private void setResultFailed(int assertionID, String msg) throws Exception {
        result = false;
        failedAssertionID = assertionID;
        message =  "Assertion " + assertionID + " failed: " + msg;
        TCKSbbUtils.createTrace(getSbbID(), Level.WARNING, message, null);
    }

    private void setResultPassed(String msg) throws Exception {
        result = true;
        message = "Success: " + msg;
        TCKSbbUtils.createTrace(getSbbID(), Level.INFO, message, null);
    }

    private NullActivityFactory nullActivityFactory;
    private NullActivityContextInterfaceFactory nullACIFactory;
    private TimerFacility timerFacility;

    private boolean result = false;
    private String message;
    private int failedAssertionID = -1;

}