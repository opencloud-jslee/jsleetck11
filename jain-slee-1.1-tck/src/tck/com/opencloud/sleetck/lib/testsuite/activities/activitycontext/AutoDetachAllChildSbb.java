/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.activities.activitycontext;

import com.opencloud.sleetck.lib.sbbutils.BaseTCKSbb;
import com.opencloud.sleetck.lib.sbbutils.TCKSbbUtils;
import com.opencloud.sleetck.lib.resource.events.TCKResourceEventY;

import javax.slee.ActivityContextInterface;
import javax.slee.Address;
import javax.slee.facilities.Level;
import javax.slee.facilities.ActivityContextNamingFacility;
import javax.slee.SbbContext;
import javax.slee.SbbLocalObject;

import java.util.HashMap;

/**
 * This SBB detaches itself from its Activity Context when onTCKResourceEventY1
 * is received, in order to get itself cleaned up and unloaded.
 */

public abstract class AutoDetachAllChildSbb extends BaseTCKSbb {

    public void onChildEvent(AutoDetachAllEvent event, ActivityContextInterface aci) {

        try {
            TCKSbbUtils.createTrace(getSbbID(), Level.FINE, "In onChildEvent in ChildSbb.", null);

            // Look up Activity "AutoDetachAllTest"
            ActivityContextNamingFacility facility = (ActivityContextNamingFacility) TCKSbbUtils.getSbbEnvironment().lookup("slee/facilities/activitycontextnaming");
            ActivityContextInterface myAci = facility.lookup("AutoDetachAllTest");
            
            // Fire a failed event on the Parent's ACI.
            fireFailedEvent(new AutoDetachAllFailedEvent(), myAci, null);
        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }
    }

    public abstract void fireFailedEvent(AutoDetachAllFailedEvent event, ActivityContextInterface aci, Address address);

}
