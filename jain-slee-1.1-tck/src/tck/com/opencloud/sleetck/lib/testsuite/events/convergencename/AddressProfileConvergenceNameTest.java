/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.events.convergencename;

import com.opencloud.sleetck.lib.TCKTestResult;
import com.opencloud.sleetck.lib.resource.TCKActivityID;
import com.opencloud.sleetck.lib.resource.events.TCKResourceEventX;
import com.opencloud.sleetck.lib.resource.testapi.TCKResourceTestInterface;
import com.opencloud.sleetck.lib.testutils.ProfileUtils;
import com.opencloud.sleetck.lib.testutils.jmx.AddressProfileProxy;
import com.opencloud.sleetck.lib.testutils.jmx.impl.AddressProfileProxyImpl;

import javax.management.ObjectName;
import javax.slee.Address;
import javax.slee.AddressPlan;

/**
 * Perform tests on convergence name selection using AddressProfile.  The method of convergence name calculation is
 * driven by the test description xml.
 */
public class AddressProfileConvergenceNameTest extends AbstractConvergenceNameTest {

    // -- Constants -- //

    // Profile names
    private static final String PROFILE_1 = "PROFILE_1"; // a1    a3
    private static final String PROFILE_2 = "PROFILE_2"; //    a2
    private static final String PROFILE_3 = "PROFILE_3"; //          a4

    private static final String ADDRESS_PROFILE_TABLE = "tck.AddressAndProfileTestProfile";

    private static final Address ADDRESS_1 = new Address(AddressPlan.IP, "1.0.0.1");
    private static final Address ADDRESS_2 = new Address(AddressPlan.IP, "1.0.0.2");
    private static final Address ADDRESS_3 = new Address(AddressPlan.IP, "1.0.0.3");
    private static final Address ADDRESS_4 = new Address(AddressPlan.IP, "1.0.0.4");

    private ProfileUtils profileUtils;

    public TCKTestResult run() throws Exception {
        // Configure IES params for AddressProfile (only effective if the test description configures a service that
        // utilises an IES method).
        InitialEventSelectorParameters iesParams = new InitialEventSelectorParameters(false, false, true, false, false,
                                                                                      "custom_name", false, false, false, null);
        TCKResourceTestInterface resource = utils().getResourceInterface();

        String eventType = TCKResourceEventX.X1;
        TCKActivityID activityID = resource.createActivity(ACTIVITY_ID_PREFIX + (currentActivityIDSuffix++));

        // Send events for two addresses that exist in different address profiles
        sendEventAndWait(eventType, "1", activityID, ADDRESS_1, "1", iesParams);
        sendEventAndWait(eventType, "2", activityID, ADDRESS_2, "2", iesParams);
        // Send an event for an address in a profile for which an SBB entity was already created -- no new SBB expected
        sendEventAndWait(eventType, "3", activityID, ADDRESS_3, "1", iesParams);
        // Send an event for an address in a profile for which an SBB entity does not yett exist -- expect a new SBB
        sendEventAndWait(eventType, "4", activityID, ADDRESS_4, "4", iesParams);

        return TCKTestResult.passed();
    }

    public void setUp() throws Exception {
        // setup the address profile table before installing the service
        profileUtils = new ProfileUtils(utils());
        profileUtils.createStandardAddressProfileTable(ADDRESS_PROFILE_TABLE);
        setupAddressProfile(ADDRESS_PROFILE_TABLE, PROFILE_1, new Address[]{ADDRESS_1, ADDRESS_3});
        setupAddressProfile(ADDRESS_PROFILE_TABLE, PROFILE_2, new Address[]{ADDRESS_2});
        setupAddressProfile(ADDRESS_PROFILE_TABLE, PROFILE_3, new Address[]{ADDRESS_4});
        super.setUp();
    }

    private void setupAddressProfile(String tableName, String profileName, Address[] addresses) throws Exception {
        ObjectName objectName = profileUtils.getProfileProvisioningProxy().createProfile(tableName, profileName);
        AddressProfileProxy addressProfileProxy = new AddressProfileProxyImpl(objectName,utils().getMBeanFacade());
        addressProfileProxy.editProfile();
        addressProfileProxy.setAddresses(addresses);
        addressProfileProxy.commitProfile();
    }

    public void tearDown() throws Exception {
        super.tearDown();
        if (profileUtils != null) {
            profileUtils.getProfileProvisioningProxy().removeProfile(ADDRESS_PROFILE_TABLE, PROFILE_1);
            profileUtils.getProfileProvisioningProxy().removeProfile(ADDRESS_PROFILE_TABLE, PROFILE_2);
            profileUtils.removeProfileTable(ADDRESS_PROFILE_TABLE);
        }
    }

    private int currentActivityIDSuffix = 0;
}
