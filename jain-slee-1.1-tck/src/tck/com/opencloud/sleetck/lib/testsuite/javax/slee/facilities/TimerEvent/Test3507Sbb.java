/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.javax.slee.facilities.TimerEvent;

import com.opencloud.sleetck.lib.resource.events.TCKResourceEventX;
import com.opencloud.sleetck.lib.sbbutils.BaseTCKSbb;
import com.opencloud.sleetck.lib.sbbutils.TCKSbbUtils;

import javax.naming.InitialContext;
import javax.slee.ActivityContextInterface;
import javax.slee.facilities.TimerEvent;
import javax.slee.facilities.TimerFacility;
import javax.slee.facilities.TimerID;
import javax.slee.facilities.TimerOptions;
import java.util.HashMap;

public abstract class Test3507Sbb extends BaseTCKSbb {

    private static final String JNDI_TIMERFACILITY_NAME = "java:comp/env/slee/facilities/timer";
    private static final long TIMEOUT = 10000;

    // Initial event
    public void onTCKResourceEventX1(TCKResourceEventX ev, ActivityContextInterface aci) {

        try {
            TimerOptions options = new TimerOptions();
            options.setTimeout(TIMEOUT);

            TimerFacility facility = (TimerFacility) new InitialContext().lookup(JNDI_TIMERFACILITY_NAME);

            setFireTime(System.currentTimeMillis());
            setFirstTimer(facility.setTimer(aci, null, getFireTime(), options));

        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }

    }

    public void onTimerEvent(TimerEvent event, ActivityContextInterface aci) {

        try {

            HashMap map = new HashMap();
            // Must be fired before requested time + timeout.
            if (event.getScheduledTime() > getFireTime() + TIMEOUT) {
                map.put("Result", new Boolean(false));
                map.put("Type", "Result");
                map.put("ID", new Integer(3509));
                map.put("Message", "TimerEvent.getScheduledTime() returned incorrect value.");
                TCKSbbUtils.getResourceInterface().sendSbbMessage(map);
                return;
            }

            // getExpireTime() should be close to the current time.
            map.put("Type", "TimeDifference");
            map.put("TimeDifference", new Long(System.currentTimeMillis() - event.getExpiryTime()));
            TCKSbbUtils.getResourceInterface().sendSbbMessage(map);

            // Recreate the hash map
            map = new HashMap();
            map.put("Type", "Result");

            if (event.getPeriod() != Long.MAX_VALUE) {
                map.put("Result", new Boolean(false));
                map.put("Message", "TimerEvent.getPeriod() returned value other than Long.MAX_VALUE for a non-repeating timer.");
                map.put("ID", new Integer(3513));
                TCKSbbUtils.getResourceInterface().sendSbbMessage(map);
                return;
            }

            if (event.getNumRepetitions() != 1) {
                map.put("Result", new Boolean(false));
                map.put("Message", "TimerEvent.getNumRepetitions() returned " + event.getNumRepetitions() + " when 1 was expected.");
                map.put("ID", new Integer(3517));
                TCKSbbUtils.getResourceInterface().sendSbbMessage(map);
                return;
            }

            if (event.getRemainingRepetitions() != 0) {
                map.put("Result", new Boolean(false));
                map.put("Message", "TimerEvent.getRemainingRepetitions() returned " + event.getRemainingRepetitions() + " when 0 was expected.");
                map.put("ID", new Integer(3522));
                TCKSbbUtils.getResourceInterface().sendSbbMessage(map);
                return;
            }

            if (event.getMissedRepetitions() != 0) {
                map.put("Result", new Boolean(false));
                map.put("Message", "TimerEvent.getMissedRepetitions() returned " + event.getMissedRepetitions() + " when 0 was expected.");
                map.put("ID", new Integer(3524));
                TCKSbbUtils.getResourceInterface().sendSbbMessage(map);
                return;
            }

            if (event.getTimerID().equals(getFirstTimer())) {
                map.put("Result", new Boolean(true));
                map.put("Message", "Ok");
                TCKSbbUtils.getResourceInterface().sendSbbMessage(map);
                return;
            }

            map.put("Result", new Boolean(false));
            map.put("ID", new Integer(3507));
            map.put("Message", "TimerEvent.getTimerID() did not return a valid TimerID object.");
            TCKSbbUtils.getResourceInterface().sendSbbMessage(map);
            return;

        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }

    }

    public abstract void setFirstTimer(TimerID timerID);
    public abstract TimerID getFirstTimer();

    public abstract void setFireTime(long time);
    public abstract long getFireTime();

}

