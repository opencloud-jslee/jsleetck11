/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.events.servicestarted;

import java.rmi.RemoteException;
import java.util.Map;

import javax.slee.ComponentID;
import javax.slee.ServiceID;
import javax.slee.management.DeployableUnitDescriptor;
import javax.slee.management.DeployableUnitID;
import javax.slee.management.ServiceState;
import javax.slee.management.SleeState;
import javax.slee.management.SleeStateChangeNotification;

import com.opencloud.sleetck.lib.AbstractSleeTCKTest;
import com.opencloud.sleetck.lib.OperationTimedOutException;
import com.opencloud.sleetck.lib.rautils.RMIObjectChannel;
import com.opencloud.sleetck.lib.testutils.FutureResult;
import com.opencloud.sleetck.lib.testutils.jmx.DeploymentMBeanProxy;
import com.opencloud.sleetck.lib.testutils.jmx.ServiceManagementMBeanProxy;
import com.opencloud.sleetck.lib.testutils.jmx.SleeManagementMBeanProxy;

/**
 * AssertionID(1108169): Test of A Service starts when the persistent state of the 
 * Service says that the Service should be active, and a previously stopped SLEE 
 * transitions to the Running state.
 *
 */
public class Test1108169Test extends AbstractSleeTCKTest {

    public static final String SERVICE_DU_PATH_PARAM = "serviceDUPath";

    /**
     * Perform the actual test.
     */

    public void run(FutureResult result) throws Exception {
        this.result = result;

        try {
            management.start();
        } catch (Exception e) {
            result.setError("Failed to start a previosly stopped SLEE into the Running state");
            return;
        }

        //check the SLEE is in running state
        if (!management.getState().isRunning()) {
            getLog().warning("Current state: " + management.getState());
            result.setError("ERROR! Current state: " + management.getState());
            return;
        } else
            getLog().info("Current state: " + management.getState());

        synchronized (this) {
            wait(5000);
        }

        Object[] messages = in.readQueue(10000);
        for (int i = 0; i < messages.length; i++) {
            Map sbbData = (Map) messages[i];
            String sbbTestName = (String) sbbData.get("testName");
            String sbbTestResult = (String) sbbData.get("result");
            String sbbTestMessage = (String) sbbData.get("message");
            int assertionID = ((Integer) sbbData.get("id")).intValue();
            getLog().info(
                    "Received message from SBB: testname=" + sbbTestName + ", result=" + sbbTestResult + ", message="
                            + sbbTestMessage + ", id=" + assertionID);
            receivedCount++;
        }

        if (receivedCount == expectedCount)
            result.setPassed();
        else
            result.setFailed(1108169, "Expected number of successful messages not received, messages were"
                    + "not delivered by the SBBs (expected " + expectedCount + ", received " + receivedCount + ")");
    }

    /**
     * Do all the pre-run configuration of the test.
     */

    public void setUp() throws Exception {
        getLog().fine("Connecting to RMIObjectChannel");
        in = utils().getRMIObjectChannel();
        
        stateListener = new QueuingSleeStateListener(utils());
        management = utils().getSleeManagementMBeanProxy();
        management.addNotificationListener(stateListener, null, null);

        getLog().fine("Installing a service");

        // Install the Deployable Units
        String duPath = utils().getTestParams().getProperty(SERVICE_DU_PATH_PARAM);
        DeployableUnitID duID = utils().install(duPath);
        utils().activateServices(duID, true);

        DeploymentMBeanProxy duProxy = utils().getDeploymentMBeanProxy();
        ServiceManagementMBeanProxy serviceProxy = utils().getServiceManagementMBeanProxy();
        DeployableUnitDescriptor duDesc = duProxy.getDescriptor(duID);
        ComponentID components[] = duDesc.getComponents();

        ServiceID firstService = null;

        for (int i = 0; i < components.length; i++) {
            if (components[i] instanceof ServiceID) {
                ServiceID service = (ServiceID) components[i];

                if (firstService == null) {
                    firstService = service;
                    break;
                }
            }
        }

        if (firstService == null) {
            result.setError("Failed to find the test service.");
            return;
        }

        ServiceState serviceState = serviceProxy.getState(firstService);
        if (!serviceState.isActive()) {
            getLog().warning("Current service state: " + serviceState);
            result.setError("ERROR! Current service state: " + serviceState);
            return;
        } else
            getLog().info("Current service state: " + serviceState);

        management = utils().getSleeManagementMBeanProxy();
        getLog().info("Calling SleeManagementMBean.getState()");
        try {
            management.stop();
            waitForStateChange(SleeState.STOPPED);
        } catch (Exception e) {
            onException(e);
            return;
        }

        SleeState currentState = management.getState();
        if (currentState == null) { 
            getLog().severe("Current state: " + currentState);
            result.setError("ERROR! returned " + currentState + " value for the current state");
            return;
        }
        
        if (!currentState.isStopped()) {
            getLog().warning("Current state: " + currentState);
            result.setError("ERROR! Current state: " + currentState);
            return;
        } else
            getLog().info("Current state: " + currentState);

    }
    
    private void waitForStateChange(SleeState expectedState) throws Exception {
        SleeStateChangeNotification stateChange = null;
        try {
            while (stateChange == null || !stateChange.getNewState().equals(expectedState)) {
                getLog().fine("Waiting to move to the " + expectedState    + " state.");

                stateChange = stateListener.nextNotification();
                getLog().fine("SLEE has moved to the " + stateChange + " state.");
            }
        } catch (OperationTimedOutException e) {
            onException(e);
        }

    }
    
    public void onException(Exception e) throws RemoteException {
        getLog().warning("Received exception from SBB.");
        getLog().warning(e);
        result.setError(e);
    }

    /**
     * Clean up after the test.
     */

    public void tearDown() throws Exception {
        // cleanup
        super.tearDown();
    }

    private FutureResult result;

    private SleeManagementMBeanProxy management;

    private static final int expectedCount = 2;

    private int receivedCount = 0;
    
    private QueuingSleeStateListener stateListener;

    private RMIObjectChannel in;
}
