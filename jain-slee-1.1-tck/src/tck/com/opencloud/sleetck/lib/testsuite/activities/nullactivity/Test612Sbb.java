/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.activities.nullactivity;

import javax.slee.ActivityEndEvent;
import javax.slee.ActivityContextInterface;
import javax.slee.SbbContext;
import javax.slee.Address;
import javax.slee.facilities.Level;
import javax.slee.nullactivity.NullActivity;
import javax.slee.nullactivity.NullActivityFactory;
import javax.slee.nullactivity.NullActivityContextInterfaceFactory;

import com.opencloud.sleetck.lib.sbbutils.BaseTCKSbb;
import com.opencloud.sleetck.lib.sbbutils.TCKSbbUtils;
import com.opencloud.sleetck.lib.sbbutils.TCKSbbConstants;
import com.opencloud.sleetck.lib.resource.events.TCKResourceEventX;
import com.opencloud.sleetck.lib.resource.TCKActivityID;
import com.opencloud.sleetck.lib.resource.sbbapi.TCKActivity;
import com.opencloud.sleetck.lib.resource.sbbapi.TCKResourceSbbInterface;
import com.opencloud.sleetck.lib.resource.sbbapi.TCKActivityContextInterfaceFactory;

import java.util.HashMap;

/**
 * If the NullActivity object has already ended, subsequent endActivity
 * method invocations are ignored.
 *
 * When endActivity() is called one of several things can happen:
 *  a) an TransactionRequiredLocalException or SLEEException can be thrown.
 *  b) nothing happens in the case of an already ended NullActivity.
 *  c) the ActivityEndEvent is received by SBBs attached to the NullActivity.
 *
 * If the NullActivity has already ended, the only thing that should happen
 * is b).  However, we have to check that neither a) nor c) occur.  Checking
 * for a) is trivial.  Checking for c) is non-trivial: in the test pass case
 * the EndActivityEvent would not be received, as the NullActivity has ended
 * we can't fire an Event on it to be received after the EndActivityEvent.
 *
 * Currently this test only tests for a) as I can't see a way to get a
 * pass for c) without waiting for a test timeout, which is bad.
 */

public abstract class Test612Sbb extends BaseTCKSbb {

    public void onTCKResourceEventX1(TCKResourceEventX event, ActivityContextInterface aci) {

        try {

            // Create a NullActivity
            NullActivityFactory factory = (NullActivityFactory) TCKSbbUtils.getSbbEnvironment().lookup("slee/nullactivity/factory");
            NullActivity nullActivity = factory.createNullActivity();

            // Get the NullActivity's ACI
            NullActivityContextInterfaceFactory aciFactory = (NullActivityContextInterfaceFactory) TCKSbbUtils.getSbbEnvironment().lookup("slee/nullactivity/activitycontextinterfacefactory");
            ActivityContextInterface nullACI = aciFactory.getActivityContextInterface(nullActivity);

            // Attach this SBB to it.
            nullACI.attach(getSbbContext().getSbbLocalObject());

            // End the null activity so that the SBB can be unloaded.
            nullActivity.endActivity();

            HashMap map = new HashMap();

            // Subsequent calls of the endActivity() call should be ignored.
            try {
                nullActivity.endActivity();
            } catch (Exception f) {

                map.put("Result", new Boolean(false));
                map.put("Message", "Second endActivity() call on the same NullActivity object caused an exception.");
                TCKSbbUtils.getResourceInterface().sendSbbMessage(map);
                return;
            }

            map.put("Result", new Boolean(true));
            map.put("Message", "Ok");
            TCKSbbUtils.getResourceInterface().sendSbbMessage(map);
            return;

        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }

    }

}
