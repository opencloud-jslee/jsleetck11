/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.events.eventcontext;

import java.util.HashMap;

import javax.slee.ActivityContextInterface;
import javax.slee.Address;
import javax.slee.EventContext;
import javax.slee.ServiceID;
import javax.slee.facilities.Tracer;

import com.opencloud.sleetck.lib.resource.events.TCKResourceEventX;
import com.opencloud.sleetck.lib.sbbutils.BaseTCKSbb;
import com.opencloud.sleetck.lib.sbbutils.TCKSbbUtils;

/**
 * AssertionID(1108179): Test this EventContext.getService() method will 
 * be null if no service component identifier was provided, or if the 
 * provided identifier was null.
 *
 */
public abstract class Test1108179Sbb1 extends BaseTCKSbb {

    public void onTCKResourceEventX1(TCKResourceEventX event, ActivityContextInterface aci, EventContext context) {
        try {
            tracer = getSbbContext().getTracer("com.test");
            tracer.info("Received event TCKResourceEventX.");

            Test1108179Event testEvent = new Test1108179Event();

            tracer.info("TestName equals to NULLSERVICEID");
            // If the provided ServiceID is null
            ServiceID nullServiceID = null;
            fireTest1108179Event(testEvent, aci, null, nullServiceID);

        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }
    }

    public void onTest1108179Event(Test1108179Event event, ActivityContextInterface aci, EventContext context) {
        try {
            tracer = getSbbContext().getTracer("com.test");
            tracer.info("Received event Test1108179Event.");
            if (context.getService() != null)
                sendResultToTCK("Test1108179Test", false, "SBB1:onTest1108179Event-ERROR: This EventContext.getService() method didn't return null "
                        + "if the provided identifier was null.", 1108179);
            else
                sendResultToTCK("Test1108179Test", true, "This EventContext.getService() method returned null if no service "
                        + "component identifier was provided, or if the provided identifier was null", 1108179);
        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }

    }

    public abstract void fireTest1108179Event(Test1108179Event event, ActivityContextInterface aci, Address address,
            ServiceID serviceID);

    private void sendResultToTCK(String testName, boolean result, String message, int failedAssertionID) throws Exception {
        HashMap sbbData = new HashMap();
        sbbData.put("testname", testName);
        sbbData.put("result", result ? "pass" : "fail");
        sbbData.put("message", message);
        sbbData.put("id", new Integer(failedAssertionID));
        TCKSbbUtils.getResourceInterface().sendSbbMessage(sbbData);
    }

    private Tracer tracer;
}
