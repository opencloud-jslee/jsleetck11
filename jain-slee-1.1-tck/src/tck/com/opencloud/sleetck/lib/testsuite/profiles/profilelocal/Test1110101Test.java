/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.profiles.profilelocal;

import java.rmi.RemoteException;
import java.util.HashMap;

import javax.management.ObjectName;
import javax.slee.profile.ProfileSpecificationID;

import com.opencloud.sleetck.lib.AbstractSleeTCKTest;
import com.opencloud.sleetck.lib.OperationTimedOutException;
import com.opencloud.sleetck.lib.TCKTestErrorException;
import com.opencloud.sleetck.lib.TCKTestFailureException;
import com.opencloud.sleetck.lib.TCKTestResult;
import com.opencloud.sleetck.lib.infra.SleeTCKComponentConstants;
import com.opencloud.sleetck.lib.resource.TCKActivityID;
import com.opencloud.sleetck.lib.resource.TCKSbbMessage;
import com.opencloud.sleetck.lib.resource.events.TCKResourceEventX;
import com.opencloud.sleetck.lib.resource.testapi.TCKResourceTestInterface;
import com.opencloud.sleetck.lib.sbbutils.events2.SbbBaseMessageConstants;
import com.opencloud.sleetck.lib.testutils.ProfileUtils;
import com.opencloud.sleetck.lib.testutils.QueuingResourceListener;
import com.opencloud.sleetck.lib.testutils.jmx.ProfileMBeanProxy;
import com.opencloud.sleetck.lib.testutils.jmx.ProfileProvisioningMBeanProxy;


public class Test1110101Test extends AbstractSleeTCKTest {

    private static final String SERVICE_DU_PATH_PARAM= "serviceDUPath";

    private static final String SPEC_NAME = "Test1110101Profile";
    private static final String SPEC_VERSION = "1.0";

    /**
     * Test assertions on ProfileLocal objects via Sbbs.
     */
    public TCKTestResult run() throws Exception {

        //Create a profile table
        ProfileSpecificationID specID = new ProfileSpecificationID(SPEC_NAME, SleeTCKComponentConstants.TCK_VENDOR, SPEC_VERSION);
        profileProvisioning.createProfileTable(specID, Test1110101Sbb.PROFILE_TABLE_NAME);
        getLog().fine("Added profile table "+Test1110101Sbb.PROFILE_TABLE_NAME);

        //Create a profile via management view
        ObjectName profile = profileProvisioning.createProfile(Test1110101Sbb.PROFILE_TABLE_NAME, Test1110101Sbb.PROFILE_NAME);
        ProfileMBeanProxy profileProxy = utils().getMBeanProxyFactory().createProfileMBeanProxy(profile);
        getLog().fine("Created profile "+Test1110101Sbb.PROFILE_NAME+" for profile table "+Test1110101Sbb.PROFILE_TABLE_NAME);

        //set value
        utils().getMBeanFacade().invoke(profile, "setValue", new Object[]{"42"}, new String[]{"java.lang.String"});
        getLog().fine("Called set accessor via management interface.");
        utils().getMBeanFacade().invoke(profile, "setValue2", new Object[]{"42"}, new String[]{"java.lang.String"});
        getLog().fine("Called set accessor2 via management interface.");

        //commit and close the Profile
        profileProxy.commitProfile();
        profileProxy.closeProfile();
        getLog().fine("Commit and close profile "+Test1110101Sbb.PROFILE_NAME);

        getLog().fine("Start sending events to SLEE to get Sbb to do the ProfileLocal interface tests...");
        sendResourceEvent(Test1110101Sbb.PASS_BY_REF);

        sendResourceEvent(Test1110101Sbb.TRY_CMP_GET_SET);

        sendResourceEvent(Test1110101Sbb.GET_SET_PROFILE_LOCAL_VIA_BUSINESS);

        sendResourceEvent(Test1110101Sbb.PASS_NON_RMI_IIOP);

        sendResourceEvent(Test1110101Sbb.CHECK_ROLLBACK);

        sendResourceEvent(Test1110101Sbb.CHECK_REMOVE);

        getLog().fine("Completed test sequence successfully.");


        return TCKTestResult.passed();
    }

    public void sendResourceEvent(int operationID) throws TCKTestErrorException, RemoteException, TCKTestFailureException {
      // send an initial event to the test
      TCKResourceTestInterface resource = utils().getResourceInterface();
      TCKActivityID activityID = resource.createActivity(getClass().getName());
      Object message = new Integer(operationID);
      resource.fireEvent(TCKResourceEventX.X1, message, activityID, null);

      try {
          TCKSbbMessage reply = resourceListener.nextMessage();
          HashMap map = (HashMap) reply.getMessage();
          int type = ((Integer)map.get(SbbBaseMessageConstants.TYPE)).intValue();

          switch (type) {
          case SbbBaseMessageConstants.TYPE_SET_RESULT:
              String msg = (String) map.get(SbbBaseMessageConstants.MSG);
              int id = ((Integer)map.get(SbbBaseMessageConstants.ID)).intValue();
              boolean result = ((Boolean)map.get(SbbBaseMessageConstants.RESULT)).booleanValue();
              if (result)
                  getLog().fine(id+": "+msg);
              else {
                  getLog().fine("FAILURE: "+msg);
                  throw new TCKTestFailureException(id, msg);
              }
              break;
          }
      } catch (OperationTimedOutException ex) {
          throw new TCKTestErrorException("Timed out waiting for processing of initial resource event.", ex);
      }
    }

    public void setUp() throws Exception {

        setupService(SERVICE_DU_PATH_PARAM);

        profileUtils = new ProfileUtils(utils());
        profileProvisioning = profileUtils.getProfileProvisioningProxy();

        resourceListener = new QueuingResourceListener(utils()) {
            public Object onSbbCall(Object argument) throws Exception {
                HashMap map = (HashMap) argument;
                int type = ((Integer)map.get(SbbBaseMessageConstants.TYPE)).intValue();
                switch (type) {
                case SbbBaseMessageConstants.TYPE_LOG_MSG:
                    getLog().fine((String)map.get(SbbBaseMessageConstants.MSG));
                }
                return null;
            }
        };
        setResourceListener(resourceListener);
    }

    public void tearDown() throws Exception {

        try {
            profileUtils.removeProfileTable(Test1110101Sbb.PROFILE_TABLE_NAME);
        }
        catch (Exception e) {
            getLog().warning("Caught exception while trying to remove profile table:");
            getLog().warning(e);
        }

        super.tearDown();
    }

    private ProfileUtils profileUtils;
    private ProfileProvisioningMBeanProxy profileProvisioning;
    private QueuingResourceListener resourceListener;
}
