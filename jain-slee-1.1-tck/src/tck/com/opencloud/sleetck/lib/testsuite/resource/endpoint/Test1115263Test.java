/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.resource.endpoint;

import java.util.HashMap;

import javax.slee.management.ResourceAdaptorEntityState;

import com.opencloud.sleetck.lib.TCKTestErrorException;
import com.opencloud.sleetck.lib.TCKTestFailureException;
import com.opencloud.sleetck.lib.TCKTestResult;
import com.opencloud.sleetck.lib.testsuite.resource.BaseResourceTest;
import com.opencloud.sleetck.lib.testsuite.resource.RAMethods;
import com.opencloud.sleetck.lib.testutils.jmx.ResourceManagementMBeanProxy;

/**
 * Test to ensure that startActivity() is throwing correct exceptions while in
 * the Stopping and Inactive state.
 * <p>
 * Test assertion: 1115263
 */
public class Test1115263Test extends BaseResourceTest {

    private static final int ASSERTION_ID = 1115263;

    public TCKTestResult run() throws Exception {

        // Setup RA
        getLog().info("Creating activity in RA to prevent transition to stopped state during deactivation");
        HashMap arguments = new HashMap();
        arguments.put("assertion", new Integer(ASSERTION_ID));
        arguments.put("argument", "startActivity");
        HashMap results = sendMessage(RAMethods.startActivityTransacted, arguments);
        if (results == null)
            throw new TCKTestErrorException("Test timed out while waiting for response from test component");
        Object result = results.get("result");
        if (result instanceof Exception)
            throw new TCKTestFailureException(ASSERTION_ID, "Unexpected exception thrown during activity creation:", (Exception) result);
        if (!Boolean.TRUE.equals(result))
            throw new TCKTestErrorException("Unexpected result received from test component: " + result);

        // Stop RA Entity
        String raEntityName = getResourceAdaptorEntityName();
        getLog().info("Stopping RA Entity: " + raEntityName);
        ResourceManagementMBeanProxy resourceMBean = utils().getResourceManagementMBeanProxy();
        resourceMBean.deactivateResourceAdaptorEntity(raEntityName);

        // Wait for 'stopping' state.
        getLog().info("Waiting for RA Entity to enter Stopping state");
        ResourceAdaptorEntityState raState = null;
        long timeout = System.currentTimeMillis() + utils().getTestTimeout();
        while (System.currentTimeMillis() < timeout) {
            raState = resourceMBean.getState(raEntityName);
            if (raState.isStopping())
                break;
            Thread.sleep(500);
        }
        if (!ResourceAdaptorEntityState.STOPPING.equals(raState))
            throw new TCKTestErrorException("RA did not transition into the Stopping state within test timeout");

        // Fire test event
        getLog().info("Asking RA to call startActivityTransacted() while in stopping state");
        arguments = new HashMap();
        arguments.put("assertion", new Integer(ASSERTION_ID));
        results = sendMessage(RAMethods.startActivityTransacted, arguments);

        // Verify we're still in the stopping state - harmless check if
        // everything is working correctly.
        raState = resourceMBean.getState(raEntityName);
        if (!ResourceAdaptorEntityState.STOPPING.equals(raState))
            throw new TCKTestErrorException("RA entity was no in STOPPING state when it should have been");

        // Check results
        if (results == null)
            throw new TCKTestErrorException("Test timed out while waiting for response from test component");
        result = results.get("result1");
        if (result instanceof Exception)
            throw new TCKTestFailureException(ASSERTION_ID, "Unexpected exception thrown during test:", (Exception) result);
        if (Boolean.FALSE.equals(result))
            throw new TCKTestFailureException(ASSERTION_ID, "startActivityTransacted(handle) failed to throw a IllegalStateException while in Stopping state");
        if (!Boolean.TRUE.equals(result))
            throw new TCKTestErrorException("Unexpected result received from test component: " + result);
        result = results.get("result2");
        if (result instanceof Exception)
            throw new TCKTestFailureException(ASSERTION_ID, "Unexpected exception thrown during test:", (Exception) result);
        if (Boolean.FALSE.equals(result))
            throw new TCKTestFailureException(ASSERTION_ID,
                    "startActivityTransacted(handle, flags) failed to throw a IllegalStateException while in Stopping state");
        if (!Boolean.TRUE.equals(result))
            throw new TCKTestErrorException("Unexpected result received from test component: " + result);

        // Deactivate RA
        getLog().info("Asking RA to resume raStopping");
        arguments = new HashMap();
        arguments.put("assertion", new Integer(ASSERTION_ID));
        arguments.put("argument", "endActivity");
        results = sendMessage(RAMethods.endActivity, arguments);
        if (results == null)
            throw new TCKTestErrorException("Test timed out while waiting for response from test component");
        result = results.get("result");
        if (result instanceof Exception)
            throw new TCKTestFailureException(ASSERTION_ID, "Unexpected exception thrown during activity creation:", (Exception) result);
        if (!Boolean.TRUE.equals(result))
            throw new TCKTestErrorException("Unexpected result received from test component: " + result);

        // Wait until management interface says the RA is stopped
        getLog().info("Waiting for RA Entity to enter Inactive state");
        raState = null;
        timeout = System.currentTimeMillis() + utils().getTestTimeout();
        while (System.currentTimeMillis() < timeout) {
            raState = resourceMBean.getState(raEntityName);
            if (raState.isInactive())
                break;
            Thread.sleep(500);
        }
        if (!ResourceAdaptorEntityState.INACTIVE.equals(raState))
            throw new TCKTestErrorException("RA did not transition into the Inactive state within test timeout");

        // Fire test event 2
        getLog().info("Asking RA to call startActivityTransacted() while in Inactive state");
        arguments = new HashMap();
        arguments.put("assertion", new Integer(ASSERTION_ID));
        results = sendMessage(RAMethods.startActivityTransacted, arguments);

        // Check results
        if (results == null)
            throw new TCKTestErrorException("Test timed out while waiting for response from test component");
        result = results.get("result1");
        if (result instanceof Exception)
            throw new TCKTestFailureException(ASSERTION_ID, "Unexpected exception thrown during test:", (Exception) result);
        if (Boolean.FALSE.equals(result))
            throw new TCKTestFailureException(ASSERTION_ID, "startActivityTransacted() failed to throw a IllegalStateException while in Inactive state");
        if (!Boolean.TRUE.equals(result))
            throw new TCKTestErrorException("Unexpected result received from test component: " + result);
        result = results.get("result2");
        if (result instanceof Exception)
            throw new TCKTestFailureException(ASSERTION_ID, "Unexpected exception thrown during test:", (Exception) result);
        if (Boolean.FALSE.equals(result))
            throw new TCKTestFailureException(ASSERTION_ID,
                    "startActivityTransacted(handle, flags) failed to throw a IllegalStateException while in Stopping state");
        if (!Boolean.TRUE.equals(result))
            throw new TCKTestErrorException("Unexpected result received from test component: " + result);

        return TCKTestResult.passed();
    }

}
