/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.javax.slee.ActivityContextInterface.Detach;

import com.opencloud.sleetck.lib.sbbutils.BaseTCKSbb;
import com.opencloud.sleetck.lib.sbbutils.TCKSbbUtils;
import com.opencloud.sleetck.lib.resource.events.TCKResourceEventY;

import javax.slee.ActivityContextInterface;
import javax.slee.facilities.Level;
import javax.slee.SbbContext;

import java.util.HashMap;

/**
 * The child SBB should not receive TCKResourceEventY1 after it has been
 * detached from the ACI.
 */

public abstract class DetachEventsChildSbb extends BaseTCKSbb {

    public void sbbCreate() throws javax.slee.CreateException {
        setEventTriggered(false);
    }

    public void onTCKResourceEventY1(TCKResourceEventY event, ActivityContextInterface aci) {
        HashMap map = new HashMap();

        setEventTriggered(true);

        map.put("Result", new Boolean(false));
        map.put("Message", "Child SBB received event after being detached from the ACI.");
        try {
            TCKSbbUtils.getResourceInterface().sendSbbMessage(map);
        } catch (Exception f) {
            TCKSbbUtils.handleException(f);
        }
    }

    public boolean wasEventTriggered() {
        return getEventTriggered();
    }

    public abstract void setEventTriggered(boolean foo);
    public abstract boolean getEventTriggered();

}
