/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.usage.sbb;

import javax.slee.ActivityContextInterface;
import javax.slee.usage.UnrecognizedUsageParameterSetNameException;

import com.opencloud.sleetck.lib.resource.events.TCKResourceEventX;
import com.opencloud.sleetck.lib.sbbutils.BaseTCKSbb;
import com.opencloud.sleetck.lib.sbbutils.TCKSbbUtils;


public abstract class Test1111068Sbb extends BaseTCKSbb {

    public void onTCKResourceEventX1(TCKResourceEventX event, ActivityContextInterface aci) {
        try {
            getSbbContext().getTracer("Test1111068Sbb").fine("Test1111068Sbb got message.");
        } catch (Exception e) {}
        
        try { // Test 1111070 
            GenericUsageParameterInterface bob = getSbbUsageParameterSet(null);
            TCKSbbUtils.getResourceInterface().sendSbbMessage("getSbbUsageParameterSet(null) did not throw NullPointerException.");
        } catch (NullPointerException e) {
            // good.
        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }

        try { // Test 1111071
            GenericUsageParameterInterface bob = getSbbUsageParameterSet("no such set");
            TCKSbbUtils.getResourceInterface().sendSbbMessage("getSbbUsageParameterSet(null) did not throw UnrecognizedUsageParameterSetNameException.");
        } catch (UnrecognizedUsageParameterSetNameException e) {
            // good.
        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }
        
        // send a message back to the test to confirm the updates, along with the initial event payload
        try {

            getSbbContext().getTracer("Test1111068Sbb").fine("Test1111068Sbb replying.");
            TCKSbbUtils.getResourceInterface().sendSbbMessage("passed");
        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }
        
    }

    // Test 1111068: test that both param set methods don't need to be there if not used.
    public abstract GenericUsageParameterInterface getSbbUsageParameterSet(String name) throws UnrecognizedUsageParameterSetNameException;

}
