/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.facilities.servicelookupfacility;

import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.slee.Address;
import javax.slee.ServiceID;
import javax.slee.facilities.EventLookupFacility;
import javax.slee.resource.ActivityHandle;
import javax.slee.resource.ConfigProperties;
import javax.slee.resource.FailureReason;
import javax.slee.resource.FireableEventType;
import javax.slee.resource.InvalidConfigurationException;
import javax.slee.resource.Marshaler;
import javax.slee.resource.ReceivableService;
import javax.slee.resource.ResourceAdaptorContext;

import com.opencloud.sleetck.lib.rautils.BaseTCKRA;
import com.opencloud.sleetck.lib.rautils.MessageHandler;
import com.opencloud.sleetck.lib.rautils.MessageHandlerRegistry;
import com.opencloud.sleetck.lib.rautils.TCKRAUtils;
import com.opencloud.sleetck.lib.rautils.TraceLogger;

/**
 * AssertionID(1113671): Test This getService method returns the Service component 
 * identifier of the Service described by the ReceivableService object.
 * 
 * AssertionID(1113672): Test This getReceivableEvents method returns an array of 
 * javax.slee.resource.ReceivableService.Receivable-Event objects.
 * 
 * AssertionID(1113673): Test Each ReceivableEvent object identifies an event type 
 * that one or more SBBs in the Service has an event handler method for.
 * 
 * AssertionID(1113678): Test The equals implementation class of a ReceivableService 
 * object must override the Object.equals method.
 * 
 * AssertionID(1113679): Test Two ReceivableService objects must be considered equals 
 * if the Service component identifiers returned by their respective getService methods
 *  are equal.
 * 
 * AssertionID(1113680): Test A ReceivableService.ReceivableEvent object contains 
 * information about a single event type that can be received by a Service.
 * 
 * AssertionID(1113683): Test This getEventType method returns the event type identifier 
 * of the event type described by the ReceivableService.ReceivableEvent object.
 * 
 * AssertionID(1113684): Test This getResourceOption method returns the resource option 
 * specified in the deployment descriptor of the SBB receiving the event type. If the 
 * SBB did not specify a resource option then this method returns null.
 * 
 * AssertionID(1113685): Test This isInitialEvent method returns true if and only if 
 * the event type is received by the root SBB of the Service and the root SBB has flagged 
 * the event type as an initial event type in its deployment descriptor.
 * 
 * AssertionID(1113676): Test The returned array will only include information on the event 
 * types that the Resource Adaptor may fire (as determined by the resource adaptor types it 
 * implements) unless event type checking has been disabled for the Resource Adaptor 
 * (see Section 15.10).
 **/

public class Test1113671ResourceAdaptor extends BaseTCKRA {

    public void setResourceAdaptorContext(ResourceAdaptorContext context) {
        super.setResourceAdaptorContext(context);
        setTracer(context.getTracer("Test1113671ResourceAdaptor: eventlookupfacility test RA"));
        try {
            MessageHandlerRegistry registry = TCKRAUtils.lookupMessageHandlerRegistry();
            messageHandler = new Test1113671MessageListener(this);
            registry.registerMessageHandler(messageHandler);
        } catch (Exception e) {
            getLog().severe("Test1113671ResourceAdaptor: An error occured during setResourceAdaptorContext()", e);
        }
    }

    public void unsetResourceAdaptorContext() {
        try {
            MessageHandlerRegistry registry = TCKRAUtils.lookupMessageHandlerRegistry();
            if (messageHandler != null)
                registry.unregisterMessageHandler(messageHandler);
        } catch (Exception e) {
            getLog().severe("Test1113671ResourceAdaptor: An error occured during unsetResourceAdaptorContext()", e);
        }
        setTracer(null);
        super.unsetResourceAdaptorContext();
    }

    public void activityEnded(ActivityHandle arg0) {
    }

    public void activityUnreferenced(ActivityHandle arg0) {
    }

    public void administrativeRemove(ActivityHandle arg0) {
    }

    public void raConfigurationUpdate(ConfigProperties arg0) {
    }

    public void raConfigure(ConfigProperties arg0) {
    }

    public void raUnconfigure() {
    }

    public void raActive() {
    }

    public void raInactive() {
    }

    public void raStopping() {
    }

    public void raVerifyConfiguration(ConfigProperties arg0) throws InvalidConfigurationException {
    }

    public void eventProcessingFailed(ActivityHandle arg0, FireableEventType arg1, Object arg2, Address arg3, ReceivableService arg4, int arg5, FailureReason arg6) {
    }

    public void eventProcessingSuccessful(ActivityHandle arg0, FireableEventType arg1, Object arg2, Address arg3, ReceivableService arg4, int arg5) {
    }

    public void eventUnreferenced(ActivityHandle arg0, FireableEventType arg1, Object arg2, Address arg3, ReceivableService arg4, int arg5) {
    }

    public Object getActivity(ActivityHandle arg0) {
        return null;
    }

    public ActivityHandle getActivityHandle(Object arg0) {
        return null;
    }

    public Marshaler getMarshaler() {
        return null;
    }

    public void queryLiveness(ActivityHandle arg0) {
    }

    public void serviceActive(ReceivableService arg0) {
    }

    public void serviceInactive(ReceivableService arg0) {
    }

    public void serviceStopping(ReceivableService arg0) {
    }
    
    public TraceLogger getLog() {
        return super.getLog();
    }

    private MessageHandler messageHandler = null;
}
