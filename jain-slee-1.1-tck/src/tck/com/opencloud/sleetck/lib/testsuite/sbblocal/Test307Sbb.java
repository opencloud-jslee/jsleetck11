/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.sbblocal;

import javax.slee.ActivityEndEvent;
import javax.slee.ActivityContextInterface;
import javax.slee.SbbContext;
import javax.slee.Address;
import javax.slee.SbbLocalObject;
import javax.slee.facilities.Level;
import javax.slee.ChildRelation;

import javax.slee.nullactivity.NullActivityFactory;
import javax.slee.nullactivity.NullActivity;
import javax.slee.nullactivity.NullActivityContextInterfaceFactory;
import javax.slee.facilities.ActivityContextNamingFacility;

import com.opencloud.sleetck.lib.sbbutils.BaseTCKSbb;
import com.opencloud.sleetck.lib.sbbutils.TCKSbbUtils;
import com.opencloud.sleetck.lib.sbbutils.TCKSbbConstants;
import com.opencloud.sleetck.lib.resource.events.TCKResourceEventX;
import com.opencloud.sleetck.lib.resource.TCKActivityID;
import com.opencloud.sleetck.lib.resource.sbbapi.TCKActivity;
import com.opencloud.sleetck.lib.resource.sbbapi.TCKResourceSbbInterface;
import com.opencloud.sleetck.lib.resource.sbbapi.TCKActivityContextInterfaceFactory;

import java.util.HashMap;
import java.util.Iterator;


/**
 * It removes the SBB entity from the ChildRelation object that the SBB
 *  entity belongs to.
 */

public abstract class Test307Sbb extends BaseTCKSbb {

    public void onTCKResourceEventX1(TCKResourceEventX event, ActivityContextInterface aci) {
        try {

            // HashMap for the test result.
            HashMap map = new HashMap();

            // Get the ChildRelation object - we could just call getChild(). each time too.
            ChildRelation childRelation = getChild();

            // Create a child and attach it to the ACI.
            SbbLocalObject child = childRelation.create();
            aci.attach(child);

            // Remove the child.
            child.remove();

            // It should no longer exist in the ChildRelation.
            Iterator iter = childRelation.iterator();
            while (iter.hasNext()) {
                SbbLocalObject localObject = (SbbLocalObject) iter.next();
                if (localObject.isIdentical(child)) {
                    // Found in child relation, failure.
                    map.put("Result", new Boolean(false));
                    map.put("Message", "Child's SbbLocalObject still existed in the ChildRelation object.");
                    TCKSbbUtils.getResourceInterface().sendSbbMessage(map);
                    return;
                }
            }

            // No longer exists, success.
            map.put("Result", new Boolean(true));
            map.put("Message", "Ok");
            TCKSbbUtils.getResourceInterface().sendSbbMessage(map);

        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }
    }

    public abstract ChildRelation getChild();

}
