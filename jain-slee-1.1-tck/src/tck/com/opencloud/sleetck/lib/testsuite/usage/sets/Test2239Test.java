/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.usage.sets;

import com.opencloud.sleetck.lib.TCKTestResult;
import com.opencloud.sleetck.lib.resource.TCKActivityID;
import com.opencloud.sleetck.lib.resource.events.TCKResourceEventX;
import com.opencloud.sleetck.lib.testsuite.usage.common.GenericUsageMBeanLookup;
import com.opencloud.sleetck.lib.testsuite.usage.common.GenericUsageSbbInstructions;
import com.opencloud.sleetck.lib.testsuite.usage.common.GenericUsageTest;
import com.opencloud.sleetck.lib.testutils.QueuingNotificationListener;
import com.opencloud.sleetck.lib.testutils.QueuingResourceListener;

/**
 * Makes updates to parameters of the same name in different parameter sets, then
 * checks that the updates were independent.
 */
public class Test2239Test extends GenericUsageTest {

    private static final int TEST_ID = 2239;
    private static final String FOO_SET_NAME = "FooSet";
    private static final String BAR_SET_NAME = "BarSet";

    public TCKTestResult run() throws Exception {

        TCKActivityID activityID = utils().getResourceInterface().createActivity("token activity");

        GenericUsageSbbInstructions instructionsForFoo = new GenericUsageSbbInstructions(FOO_SET_NAME);
        instructionsForFoo.addFirstCountIncrement(1);
        instructionsForFoo.addTimeBetweenNewConnectionsSamples(2);
        getLog().info("firing event to Sbb");
        utils().getResourceInterface().fireEvent(TCKResourceEventX.X1,instructionsForFoo.toExported(),activityID,null);

        GenericUsageSbbInstructions instructionsForBar = new GenericUsageSbbInstructions(BAR_SET_NAME);
        instructionsForBar.addFirstCountIncrement(4);
        instructionsForBar.addTimeBetweenNewConnectionsSamples(5);
        getLog().info("firing event to Sbb");
        utils().getResourceInterface().fireEvent(TCKResourceEventX.X1,instructionsForBar.toExported(),activityID,null);

        getLog().info("waiting for replies");
        resourceListener.nextMessage();
        resourceListener.nextMessage();
        getLog().info("received replies");

        getLog().info("waiting for usage notifications");
        for (int i = 0; i < instructionsForFoo.getTotalUpdates(); i++) {
              notificationListenerFoo.nextNotification();
        }
        for (int i = 0; i < instructionsForBar.getTotalUpdates(); i++) {
              notificationListenerBar.nextNotification();
        }
        getLog().info("received all "+(instructionsForFoo.getTotalUpdates()+instructionsForBar.getTotalUpdates())+" usage notifications");

        getLog().info("checking that parameter updates to both sets were independent");

        long firstCountForFoo = mBeanLookup.getNamedGenericSbbUsageMBeanProxy(FOO_SET_NAME).getFirstCount(false);
        long firstCountForBar = mBeanLookup.getNamedGenericSbbUsageMBeanProxy(BAR_SET_NAME).getFirstCount(false);
        double timeBetweenNewConnectionsMeanForFoo =
            mBeanLookup.getNamedGenericSbbUsageMBeanProxy(FOO_SET_NAME).getTimeBetweenNewConnections(false).getMean();
        double timeBetweenNewConnectionsMeanForBar =
            mBeanLookup.getNamedGenericSbbUsageMBeanProxy(BAR_SET_NAME).getTimeBetweenNewConnections(false).getMean();

        String expectedValues = "firstCountForFoo="+1+",firstCountForBar="+4+
            "timeBetweenNewConnectionsMeanForFoo="+2+"timeBetweenNewConnectionsMeanForBar="+5;
        String actualValues = "firstCountForFoo="+firstCountForFoo+",firstCountForBar="+firstCountForBar+
            "timeBetweenNewConnectionsMeanForFoo="+timeBetweenNewConnectionsMeanForFoo+
            "timeBetweenNewConnectionsMeanForBar="+timeBetweenNewConnectionsMeanForBar;

        if(firstCountForFoo == 5) return TCKTestResult.failed(TEST_ID,"Updates to firstCount usage parameter in different "+
                "parameter sets were combined -- the parameter sets should be independent.");
        if(firstCountForBar == 5) return TCKTestResult.failed(TEST_ID,"Updates to firstCount usage parameter in different "+
                "parameter sets were combined -- the parameter sets should be independent.");
        if(timeBetweenNewConnectionsMeanForFoo == 3.5) return TCKTestResult.failed(TEST_ID,"Updates to timeBetweenNewConnections usage parameter in different "+
                "parameter sets were combined -- the parameter sets should be independent.");
        if(timeBetweenNewConnectionsMeanForBar == 3.5) return TCKTestResult.failed(TEST_ID,"Updates to timeBetweenNewConnections usage parameter in different "+
                "parameter sets were combined -- the parameter sets should be independent.");
        if(firstCountForFoo != 1 || firstCountForBar != 4 || timeBetweenNewConnectionsMeanForFoo != 2 ||
                 timeBetweenNewConnectionsMeanForBar != 5) {
            return TCKTestResult.failed(TEST_ID,"Usage parameter values were not set as expected. Expected values: "+
                    expectedValues+", actual values: "+actualValues);
        }
        getLog().info("parameter checks ok");
        return TCKTestResult.passed();
    }

    public void setUp() throws Exception {
        super.setUp();
        setResourceListener(resourceListener = new QueuingResourceListener(utils()));
        mBeanLookup = getGenericUsageMBeanLookup();
        mBeanLookup.getServiceUsageMBeanProxy().createUsageParameterSet(mBeanLookup.getSbbID(),FOO_SET_NAME);
        mBeanLookup.getServiceUsageMBeanProxy().createUsageParameterSet(mBeanLookup.getSbbID(),BAR_SET_NAME);
        notificationListenerFoo = new QueuingNotificationListener(utils());
        notificationListenerBar = new QueuingNotificationListener(utils());
        mBeanLookup.getNamedGenericSbbUsageMBeanProxy(FOO_SET_NAME).addNotificationListener(notificationListenerFoo,null,null);
        mBeanLookup.getNamedGenericSbbUsageMBeanProxy(BAR_SET_NAME).addNotificationListener(notificationListenerBar,null,null);
    }

    public void tearDown() throws Exception {
        if(mBeanLookup != null) {
            if(notificationListenerFoo != null)mBeanLookup.getNamedGenericSbbUsageMBeanProxy(FOO_SET_NAME).removeNotificationListener(notificationListenerFoo);
            if(notificationListenerBar != null)mBeanLookup.getNamedGenericSbbUsageMBeanProxy(BAR_SET_NAME).removeNotificationListener(notificationListenerBar);
        }
        super.tearDown();
    }

    private GenericUsageMBeanLookup mBeanLookup;
    private QueuingResourceListener resourceListener;
    private QueuingNotificationListener notificationListenerFoo;
    private QueuingNotificationListener notificationListenerBar;

}
