/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.resource.endpoint;

import java.util.HashMap;

import javax.slee.ActivityContextInterface;

import com.opencloud.sleetck.lib.rautils.UOID;
import com.opencloud.sleetck.lib.testsuite.resource.BaseResourceSbb;
import com.opencloud.sleetck.lib.testsuite.resource.RAMethods;
import com.opencloud.sleetck.lib.testsuite.resource.SimpleEvent;

public abstract class Test1115234Sbb extends BaseResourceSbb {

    private static final int ASSERTION_ID = 1115234;

    // Event handlers
    public void onSimpleEvent(SimpleEvent event, ActivityContextInterface aci) {
        tracer.info("onSimpleEvent() event handler called with event: " + event);

        int sequenceID = event.getSequenceID();
        Object payload = event.getPayload();

        HashMap results = new HashMap();
        if ("A".equals(payload)) {
            results.put("result-sbb1", Boolean.TRUE);
        } else if ("B".equals(payload)) {
            results.put("result-sbb2", Boolean.TRUE);
            sendSbbMessage(sbbUID, sequenceID, RAMethods.endActivityTransacted, results);
            return;
        }

        try {
            getSbbInterface().executeTestLogic(new Integer(ASSERTION_ID));
        } catch (Exception e) {
            results.put("result-sbb1", e);
        }

        sendSbbMessage(sbbUID, sequenceID, RAMethods.endActivityTransacted, results);
    }

    // CMP field 'InitialEvent' of type SimpleEvent
    public abstract SimpleEvent getInitialEvent();
    public abstract void setInitialEvent(SimpleEvent event);

    private static final UOID sbbUID = UOID.createUOID();
}
