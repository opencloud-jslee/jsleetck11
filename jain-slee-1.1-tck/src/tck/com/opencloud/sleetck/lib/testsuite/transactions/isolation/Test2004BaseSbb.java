/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.transactions.isolation;

import com.opencloud.sleetck.lib.TCKTestErrorException;
import com.opencloud.sleetck.lib.resource.TCKTestCallException;
import com.opencloud.sleetck.lib.resource.events.TCKResourceEvent;
import com.opencloud.sleetck.lib.sbbutils.BaseTCKSbb;
import com.opencloud.sleetck.lib.sbbutils.TCKSbbUtils;

import javax.naming.NamingException;
import javax.slee.RolledBackContext;
import javax.slee.facilities.ActivityContextNamingFacility;
import javax.slee.facilities.Level;
import java.rmi.RemoteException;
import java.util.HashMap;
import java.util.Map;

public abstract class Test2004BaseSbb extends BaseTCKSbb {

    public static final String INITIAL_ACI_NAME_1 = "Test2004NullActivity_Initial_Binding_1";
    public static final String INITIAL_ACI_NAME_2 = "Test2004NullActivity_Initial_Binding_2";
    public static final String INITIAL_ACI_NAME_3 = "Test2004NullActivity_Initial_Binding_3";

    public void sbbRolledBack(RolledBackContext context) {
        if(context.getEvent() instanceof TCKResourceEvent) {
            try {
                TCKResourceEvent event = (TCKResourceEvent)context.getEvent();
                String eventTypeName = event.getEventTypeName();
                Map args = new HashMap();
                args.put(IsolationTestConstants.SBB_EVENT_HANDLER_FIELD, eventTypeName);
                args.put(IsolationTestConstants.SBB_ROLLED_BACK,null); // use this key as a flag to indicate SBB_ROLLED_BACK
                createTraceSafe(Level.WARNING,"sbbRolledBack() called for "+eventTypeName+ "event. Calling test");
                TCKSbbUtils.getResourceInterface().callTest(args);
            } catch (Exception e) {
                TCKSbbUtils.handleException(e);
            }

        } else super.sbbRolledBack(context);
    }

    protected ActivityContextNamingFacility getActivityContextNamingFacility() throws NamingException {
        return (ActivityContextNamingFacility)TCKSbbUtils.getSbbEnvironment().lookup("slee/facilities/activitycontextnaming");
    }

    /**
     * Send a response with event handler name and optional value back to the test.  Depending on the event handler
     * and context the test may block before returning.
     * @param handlerName the name of the event handler, which is the event type name of the event
     * @param argument the optional argument to pass to the test
     */
    protected Object sendResponse(String handlerName, Object argument) throws NamingException, TCKTestErrorException, TCKTestCallException, RemoteException {
        Map args = new HashMap();
        args.put(IsolationTestConstants.SBB_EVENT_HANDLER_FIELD, handlerName);
        args.put(IsolationTestConstants.VALUE_FIELD, argument);
        createTraceSafe(Level.INFO,"Sending response for "+handlerName+" event handler. Argument:"+argument);
        return TCKSbbUtils.getResourceInterface().callTest(args);
    }

    /**
     * Returns a unique ID identifying the transaction.
     */
    protected String getTransactionId() throws NamingException, TCKTestErrorException {
        return TCKSbbUtils.getResourceAdaptorInterface().getTransactionIDAccess().getCurrentTransactionID().toString();
    }
}
