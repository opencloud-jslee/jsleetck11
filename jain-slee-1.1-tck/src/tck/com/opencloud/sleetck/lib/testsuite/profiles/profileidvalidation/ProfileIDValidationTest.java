/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.profiles.profileidvalidation;

import com.opencloud.sleetck.lib.AbstractSleeTCKTest;
import com.opencloud.sleetck.lib.TCKTestFailureException;
import com.opencloud.sleetck.lib.TCKTestResult;
import com.opencloud.sleetck.lib.testsuite.profiles.ProfileTestConstants;
import com.opencloud.sleetck.lib.testutils.ProfileUtils;

import javax.slee.Address;
import javax.slee.AddressPlan;
import javax.slee.profile.ProfileID;

public class ProfileIDValidationTest extends AbstractSleeTCKTest {

    protected static final String PROFILE_TABLE_NAME = "tck.ProfileIDValidationTest.table";

    /**
     * Createa a profile table and fires an event to ProfileSecurityTestSbb to test that the profile setter returns
     * the correct exception.
     */
    public TCKTestResult run() throws Exception {
        try {
            new ProfileID("MyTable", "A");
            getLog().info("Created a ProfileID with valid profile table name");
        } catch (Exception e) {
            throw new TCKTestFailureException(931, "Got Exception creating ProfileID with valid table name", e);
        }

        try {
            new ProfileID(new Address(AddressPlan.SLEE_PROFILE, "MyTable/A"));
            getLog().info("Created a ProfileID with valid AddressPlan");
        } catch (Exception e) {
            throw new TCKTestFailureException(931, "Got IllegalArgumentException creating ProfileID with valid AddressPlan", e);
        }

        try {
            new ProfileID("/", "A");
            throw new TCKTestFailureException(931, "Creation of ProfileID with invalid name should produce IllegalArgumentException");
        } catch (IllegalArgumentException e) {
            getLog().info("Got IllegalArgumentException creating ProfileID with '/' in table name");
        } catch (Exception e) {
            throw new TCKTestFailureException(931, "Received incorrect exception creating a ProfileID with invalid table, expected IllegalArgumentException", e);
        }
        try {
            new ProfileID(new Address(AddressPlan.URI, "MyTable/A"));
            throw new TCKTestFailureException(931, "Creation of ProfileID with invalid address plan should produce IllegalArgumentException");
        } catch (IllegalArgumentException e) {
            getLog().info("Got IllegalArgumentException creating ProfileID with invalid AddressPlan");
        } catch (Exception e) {
            throw new TCKTestFailureException(931, "Received incorrect exception creating a ProfileID with invalid address plan, expected IllegalArgumentException", e);
        }

        getLog().info("Creating a valid ProfileID");
        ProfileID test = new ProfileID("MyProfile", "A");
        try {
            test.setProfileID("MyTable", "B");
            getLog().info("Updated a ProfileID with valid profile table name");
        } catch (Exception e) {
            throw new TCKTestFailureException(931, "Got Exception updating a ProfileID with valid table name",  e);
        }

        try {
            test.setProfileID("/", "A");
            throw new TCKTestFailureException(931, "Creation of ProfileID with invalid name should produce IllegalArgumentException");
        } catch (IllegalArgumentException e) {
            getLog().info("Got IllegalArgumentException creating ProfileID with '/' in table name");
        } catch (Exception e) {
            throw new TCKTestFailureException(931, "Received incorrect exception creating a ProfileID with invalid table name, expected IllegalArgumentException", e);
        }

        return TCKTestResult.passed();
    }

    public void setUp() throws Exception {
        String duPath = utils().getTestParams().getProperty(ProfileTestConstants.PROFILE_SPEC_DU_PATH_PARAM);
        getLog().fine("Installing profile specification");
        utils().install(duPath);
        profileUtils = new ProfileUtils(utils());
    }

    public void tearDown() throws Exception {
        // delete profile tables
        try {
            if (profileUtils != null && tableCreated) {
                getLog().fine("Removing profile table");
                profileUtils.removeProfileTable(PROFILE_TABLE_NAME);
            }
        } catch (Exception e) {
            getLog().warning("Caught exception while trying to remove profile table:");
            getLog().warning(e);
        }
        super.tearDown();
    }

    private ProfileUtils profileUtils;
    private boolean tableCreated = false;

}
