/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.usage.profiles;

import javax.slee.usage.UnrecognizedUsageParameterSetNameException;

public abstract class Test1111076ProfileAbstractClass extends BaseProfileAbstractClass {
    public abstract Test1111076ProfileInterface getUsageParameterSet(String name) throws UnrecognizedUsageParameterSetNameException;
    public abstract Test1111076ProfileInterface getDefaultUsageParameterSet();

    public void incrementDefaultParameterSet() {
        Test1111076ProfileInterface ps;
        ps = getDefaultUsageParameterSet();
        ps.incrementFirstCount(1);
    }        
        
    public void incrementNamedParameterSet() throws javax.slee.usage.UnrecognizedUsageParameterSetNameException {
        Test1111076ProfileInterface ps;
        ps = getUsageParameterSet(PARAM_SET_NAME);
        ps.incrementFirstCount(1);
    }
    
    public String doSomeTests() {
        Test1111076ProfileInterface ps;
        
        try { // Test 1111081
            ps = getUsageParameterSet(null);
            return "getUsageParameterSet(null) did not throw NullPointerException.";
        } catch (NullPointerException e) {
            // good.
        } catch (UnrecognizedUsageParameterSetNameException e) {
            // unlikely...
            return "getUsageParameterSet(null) threw a UnrecognizedUsageParameterSetNameException!";
        }
        
        try { // Test 1111082
            ps = getUsageParameterSet("No such usage parameter set by this name");
            return "getUsageParameterSet(\"...\") did not throw UnrecognizedUsageParameterSetNameException.";
        } catch (UnrecognizedUsageParameterSetNameException e) {
            // good.
        }
        return "passed";
    }
}
