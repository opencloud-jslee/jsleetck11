/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.javax.slee.management.DeployableUnitDescriptor;

import javax.slee.management.*;
import javax.slee.ComponentID;
import javax.slee.ServiceID;

import com.opencloud.sleetck.lib.*;
import com.opencloud.sleetck.lib.testutils.*;
import com.opencloud.sleetck.lib.resource.TCKSbbMessage;
import com.opencloud.sleetck.lib.resource.testapi.TCKResourceListener;
import com.opencloud.sleetck.lib.resource.TCKActivityID;
import com.opencloud.sleetck.lib.resource.testapi.TCKResourceTestInterface;
import com.opencloud.sleetck.lib.resource.events.TCKResourceEventX;

import com.opencloud.sleetck.lib.testutils.jmx.DeploymentMBeanProxy;
import com.opencloud.sleetck.lib.testutils.jmx.ServiceManagementMBeanProxy;

import com.opencloud.logging.Logable;

import java.rmi.RemoteException;
import java.util.HashMap;

public class Test3740Test implements SleeTCKTest {

    private static final String SERVICE_DU_PATH_PARAM = "DUPath";
    private static final int TEST_ID = 3740;

    public void init(SleeTCKTestUtils utils) {
        this.utils = utils;
    }

    /**
     * Perform the actual test.
     */

    public TCKTestResult run() throws Exception {

        DeploymentMBeanProxy duProxy = utils.getDeploymentMBeanProxy();
        DeployableUnitDescriptor duDesc = duProxy.getDescriptor(duID);
        

        if (duDesc.getURL().equals(utils.getDeploymentUnitURL(utils.getTestParams().getProperty(SERVICE_DU_PATH_PARAM))))
            return TCKTestResult.passed();

        return TCKTestResult.failed(TEST_ID, "DeployableUnitDescriptor.getURL() didn't return the URL of the installed Deployable Unit.");
    }

    /**
     * Do all the pre-run configuration of the test.
     */

    public void setUp() throws Exception {

        // Install the Deployable Units
        String duPath = utils.getTestParams().getProperty(SERVICE_DU_PATH_PARAM);
        duID = utils.install(duPath);

        // Activate the DU
        utils.activateServices(duID, true);

    }

    /**
     * Clean up after the test.
     */

    public void tearDown() throws Exception {
        utils.getLog().fine("Disconnecting from resource");
        utils.getResourceInterface().clearActivities();
        utils.getResourceInterface().removeResourceListener();
        utils.getLog().fine("Deactivating and uninstalling service");
        utils.deactivateAllServices();
        utils.uninstallAll();
    }

    private SleeTCKTestUtils utils;
    private DeployableUnitID duID;
}
