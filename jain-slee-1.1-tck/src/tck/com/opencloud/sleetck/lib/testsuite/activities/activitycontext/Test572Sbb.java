/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.activities.activitycontext;

import javax.slee.ActivityEndEvent;
import javax.slee.ActivityContextInterface;
import javax.slee.SbbContext;

import com.opencloud.sleetck.lib.sbbutils.BaseTCKSbb;
import com.opencloud.sleetck.lib.sbbutils.TCKSbbUtils;
import com.opencloud.sleetck.lib.sbbutils.TCKSbbConstants;
import com.opencloud.sleetck.lib.resource.events.TCKResourceEventX;
import com.opencloud.sleetck.lib.resource.TCKActivityID;
import com.opencloud.sleetck.lib.resource.sbbapi.TCKResourceSbbInterface;
import com.opencloud.sleetck.lib.resource.sbbapi.TCKActivityContextInterfaceFactory;

import java.util.HashMap;

public abstract class Test572Sbb extends BaseTCKSbb {

    public void onTCKResourceEventX1(TCKResourceEventX event, ActivityContextInterface aci) {

        try {
            // Create a new Activity.
            TCKActivityID activityID = TCKSbbUtils.getResourceInterface().createActivity("Test572SecondActivity");
            TCKResourceSbbInterface resource = TCKSbbUtils.getResourceInterface();
            TCKActivityContextInterfaceFactory factory = (TCKActivityContextInterfaceFactory) TCKSbbUtils.getSbbEnvironment().lookup(TCKSbbConstants.TCK_ACI_FACTORY_LOCATION);
            ActivityContextInterface myAci = factory.getActivityContextInterface(resource.getActivity(activityID));

            // Take a record of this SBB's Activity Context attributes.
            Test572SbbActivityContextInterface thisACI = asSbbActivityContextInterface(myAci);
            thisACI.setValue(42);
            int value = thisACI.getValue();

            // Attach the SBB to the Activity Context.
            myAci.attach(getSbbContext().getSbbLocalObject());

            // Verify that the SBB's Activity Context attributes still have the
            // same value they had before.
            HashMap map = new HashMap();
            if (thisACI.getValue() != value) {
                map.put("Result", new Boolean(false));
                map.put("Message", "Attaching the SBB to the ACI changed the 'value' Activity Context attribute.");
            } else {
                map.put("Result", new Boolean(true));
                map.put("Message", "Ok");
            }

            // End that activity, so the SBB can be cleaned up properly.
            resource.endActivity(activityID);

            // Send the test fail/success message _after_ ending the activity.
            TCKSbbUtils.getResourceInterface().sendSbbMessage(map);

        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }

    }

    public abstract Test572SbbActivityContextInterface asSbbActivityContextInterface(ActivityContextInterface aci);

}
