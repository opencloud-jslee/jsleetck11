/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.javax.slee.ActivityContextInterface.Attach;

import com.opencloud.sleetck.lib.AbstractSleeTCKTest;
import com.opencloud.sleetck.lib.TCKTestResult;
import com.opencloud.sleetck.lib.resource.TCKActivityID;
import com.opencloud.sleetck.lib.resource.TCKSbbMessage;
import com.opencloud.sleetck.lib.resource.events.TCKResourceEventX;
import com.opencloud.sleetck.lib.resource.testapi.TCKResourceListener;
import com.opencloud.sleetck.lib.resource.testapi.TCKResourceTestInterface;
import com.opencloud.sleetck.lib.testutils.BaseTCKResourceListener;
import com.opencloud.sleetck.lib.testutils.FutureResult;

import java.rmi.RemoteException;

/**
 * Deploy service containing Parent and Child SBBs.
 * Send event to parent SBB, which causes it to:
 * create the child, remove the child, then attempt to attach the now invalid child
 * to the Activity
 */
public class AttachInvalidTest extends AbstractSleeTCKTest {

    /**
     * Perform the actual test.
     */
    public TCKTestResult run() throws Exception {
        futureResult = new FutureResult(getLog());

        // Create an Activity object.
        TCKResourceTestInterface resource = utils().getResourceInterface();
        String activityName = "AttachInvalidTest";
        TCKActivityID activityID = resource.createActivity(activityName);

        // Fire TCKResourceEventX.X1 on this activity.
        getLog().fine("Firing TCKResourceEventX.X1 on activity " + activityName);
        resource.fireEvent(TCKResourceEventX.X1, TCKResourceEventX.X1, activityID, null);

        // Now wait for the response from the SBB.
        return futureResult.waitForResultOrFail(utils().getTestTimeout(), "No response from the SBB", 3018);
    }

    /**
     * Do all the pre-run configuration of the test.
     */
    public void setUp() throws Exception {
        getLog().fine("Connecting to resource");
        resourceListener = new TCKResourceListenerImpl();
        utils().getResourceInterface().setResourceListener(resourceListener);

        super.setUp();
    }

    private class TCKResourceListenerImpl extends BaseTCKResourceListener {
        public synchronized void onSbbMessage(TCKSbbMessage message, TCKActivityID calledActivity) throws RemoteException {
            getLog().info("Received result from SBB");
            futureResult.setResult(TCKTestResult.fromExported(message.getMessage()));
        }

        public void onException(Exception e) throws RemoteException {
            getLog().warning("Received exception from SBB");
            getLog().warning(e);
            futureResult.setError(e);
        }
    }

    private TCKResourceListener resourceListener;
    private FutureResult futureResult;
}
