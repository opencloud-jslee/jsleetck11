/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.profiles.profilelocal;

import javax.slee.profile.ProfileID;
import javax.slee.profile.ProfileLocalObject;

/**
 * Business methods have same name (though different signature) as methods in javax.slee.profile.ProfileManagement
 */
public interface Test1110098_7ProfileLocal extends ProfileLocalObject {
    public boolean  isProfileDirty( ) ;
    public boolean  isProfileValid(ProfileID id ) ;
    public void     markProfileDirty( ) ;
}
