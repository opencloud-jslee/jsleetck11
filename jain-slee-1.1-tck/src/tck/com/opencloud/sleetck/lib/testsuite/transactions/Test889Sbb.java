/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.transactions;

import javax.slee.ActivityEndEvent;
import javax.slee.ActivityContextInterface;
import javax.slee.SbbContext;
import javax.slee.Address;
import javax.slee.SbbLocalObject;
import javax.slee.facilities.Level;
import javax.slee.ChildRelation;
import javax.slee.CreateException;

import com.opencloud.sleetck.lib.TCKTestErrorException;
import com.opencloud.sleetck.lib.sbbutils.BaseTCKSbb;
import com.opencloud.sleetck.lib.sbbutils.TCKSbbUtils;
import com.opencloud.sleetck.lib.sbbutils.TCKSbbConstants;
import com.opencloud.sleetck.lib.resource.events.TCKResourceEventX;
import com.opencloud.sleetck.lib.resource.events.TCKResourceEventY;
import com.opencloud.sleetck.lib.resource.sbbapi.TCKResourceSbbInterface;

import java.util.HashMap;
import java.util.Iterator;

public abstract class Test889Sbb extends BaseTCKSbb {

    public void sbbRolledBack(javax.slee.RolledBackContext context) {
    }

    // Initial event
    public void onTCKResourceEventX1(TCKResourceEventX ev, ActivityContextInterface aci) {
        try {
            fireTest889Event(new Test889Event(), aci, null);
            fireTest889SecondEvent(new Test889SecondEvent(), aci, null);
        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }
    }

    public void onTest889Event(Test889Event event, ActivityContextInterface aci) {

        setEventReceived(true);

        if (getSecondEventReceived()) {
            try {
                HashMap map = new HashMap();
                map.put("Result", new Boolean(false));
                map.put("Message", "The second fired event was received first.");
                TCKSbbUtils.getResourceInterface().sendSbbMessage(map);
            } catch (Exception e) {
                TCKSbbUtils.handleException(e);
            }
            return;
        }

    }

    public void onTest889SecondEvent(Test889SecondEvent event, ActivityContextInterface aci) {

        setSecondEventReceived(true);

        if (getEventReceived()) {
            try {
                HashMap map = new HashMap();
                map.put("Result", new Boolean(true));
                map.put("Message", "Ok");
                TCKSbbUtils.getResourceInterface().sendSbbMessage(map);
            } catch (Exception e) {
                TCKSbbUtils.handleException(e);
            }
        }

    }

    public abstract void fireTest889Event(Test889Event event, ActivityContextInterface aci, Address address);
    public abstract void fireTest889SecondEvent(Test889SecondEvent event, ActivityContextInterface aci, Address address);

    public abstract void setSecondEventReceived(boolean received);
    public abstract boolean getSecondEventReceived();

    public abstract void setEventReceived(boolean received);
    public abstract boolean getEventReceived();

}
