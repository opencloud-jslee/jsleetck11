/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.events.eventcontext;

import javax.slee.ActivityContextInterface;
import javax.slee.InitialEventSelector;
import javax.slee.EventContext;
import javax.slee.facilities.Tracer;

import com.opencloud.sleetck.lib.sbbutils.BaseTCKSbb;
import com.opencloud.sleetck.lib.sbbutils.TCKSbbUtils;
import com.opencloud.sleetck.lib.resource.events.TCKResourceEventX;

import java.util.HashMap;

/**
 * AssertionID(1108045): Test The javax.slee.IllegalStateException is thrown if
 * the Event Context is not in “suspended state”, e.g. event delivery had not
 * been suspended or event delivery has already been resumed.
 */
public abstract class Test1108045Sbb2 extends BaseTCKSbb {
    public static final String TRACE_MESSAGE_TCKResourceEventX1 = "Test1108045Sbb2:I got TCKResourceEventX1 on ActivityA";

    public InitialEventSelector initialEventSelector(InitialEventSelector ies) {
        ies.setCustomName("test");
        return ies;
    }

    public void onTCKResourceEventX1(TCKResourceEventX event, ActivityContextInterface aci, EventContext context) {
        try {
            // send message to TCK: I got TCKResourceEventX1 on ActivityA
            tracer = getSbbContext().getTracer("com.test");
            tracer.info(TRACE_MESSAGE_TCKResourceEventX1);
            boolean passed = false;
            try {
                context.resumeDelivery();
            } catch (java.lang.IllegalStateException e) {
                tracer.info("got expected IllegalStateException if the event delivery had not been suspended.");
                passed = true;
            }

            if (!passed) {
                sendResultToTCK("Test1108045Test", false, "SBB2:onTCKResourceEventX1-ERROR: EventContext.resumeDelivery() "
                        + "should have thrown java.lang.IllegalStateException "
                        + "if the event delivery had not been suspended.", 1108045);
                return;
            }

        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }
    }

    private void sendResultToTCK(String testName, boolean result, String message, int failedAssertionID) throws Exception {
        HashMap sbbData = new HashMap();
        sbbData.put("testname", testName);
        sbbData.put("result", result ? "pass" : "fail");
        sbbData.put("message", message);
        sbbData.put("id", new Integer(failedAssertionID));
        TCKSbbUtils.getResourceInterface().sendSbbMessage(sbbData);
    }

    private Tracer tracer = null;

}
