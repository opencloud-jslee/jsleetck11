/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.resource.lifecycle;

import javax.slee.resource.ConfigProperties;
import javax.slee.resource.ResourceAdaptorID;

import com.opencloud.sleetck.lib.AbstractSleeTCKTest;
import com.opencloud.sleetck.lib.TCKTestFailureException;
import com.opencloud.sleetck.lib.TCKTestResult;
import com.opencloud.sleetck.lib.infra.SleeTCKComponentConstants;
import com.opencloud.sleetck.lib.rautils.RMIObjectChannel;
import com.opencloud.sleetck.lib.testsuite.resource.TCKMessage;
import com.opencloud.sleetck.lib.testutils.jmx.ResourceManagementMBeanProxy;

/**
 * Basic RA lifecycle callback methods test.
 * <p>
 * Covers assertions: {(RAStateManager) 1115504, 1115515, 1115525, 1115530,
 * 1115535, 1115519, 1115512, 1115053}, 1115517 , 1115527, 1115538, 1115522
 */
public class Test1115504Test extends AbstractSleeTCKTest {

    private static final String RA_NAME = "TCK_Lifecycle_Test_RA";
    private static final String RA_ENTITY_NAME = RA_NAME + "_Entity";
    private static final String RA_VENDOR = SleeTCKComponentConstants.TCK_VENDOR;
    private static final String RA_VERSION = "1.1";

    private static final String RESOURCE_DU_PATH_PARAM = "resourceDUPath";

    public TCKTestResult run() throws Exception {
        RMIObjectChannel in = utils().getRMIObjectChannel();

        ResourceManagementMBeanProxy resourceMBean = utils().getResourceManagementMBeanProxy();

        // Create a lifecycle state manager.
        RAStateManager stateManager = new RAStateManager(getLog());

        // Create the RA Entity. This should trigger several callback methods.
        ResourceAdaptorID raID = new ResourceAdaptorID(RA_NAME, RA_VENDOR, RA_VERSION);
        ConfigProperties configProperties = new ConfigProperties();
        resourceMBean.createResourceAdaptorEntity(raID, RA_ENTITY_NAME, configProperties);

        TCKMessage[] messages = castMessages(in.readQueue(10000));
        stateManager.runStateMachine(messages);

        // Check we ended up in the correct state (INACTIVE) after the
        // createRAEntity management command.
        if (!stateManager.hasRAObjectInState(RAStateManager.STATE_INACTIVE))
            throw new TCKTestFailureException(1115517, "RA Entity did not have any RA Objects in the correct state ("
                    + RAStateManager.getStateString(RAStateManager.STATE_INACTIVE) + ") after ResourceManagmentMBean.createResourceAdaptorEntity() returned");

        // Activate the RA Entity
        resourceMBean.activateResourceAdaptorEntity(RA_ENTITY_NAME);
        messages = castMessages(in.readQueue(10000));
        stateManager.runStateMachine(messages);

        // Check we ended up in the correct state (ACTIVE) after the
        // activateRAEntity management command.
        if (!stateManager.hasRAObjectInState(RAStateManager.STATE_ACTIVE))
            throw new TCKTestFailureException(1115527, "RA Entity did not have any RA Objects in the correct state ("
                    + RAStateManager.getStateString(RAStateManager.STATE_ACTIVE) + ") after ResourceManagmentMBean.activateResourceAdaptorEntity() returned");

        // Deactive the RA Entity
        resourceMBean.deactivateResourceAdaptorEntity(RA_ENTITY_NAME);
        messages = castMessages(in.readQueue(10000));
        stateManager.runStateMachine(messages);

        if (!stateManager.hasRAObjectInState(RAStateManager.STATE_INACTIVE))
            throw new TCKTestFailureException(1115538, "RA Entity did not have any RA Objects in the correct state ("
                    + RAStateManager.getStateString(RAStateManager.STATE_INACTIVE)
                    + ") after ResourceManagmentMBean.deactivateResourceAdaptorEntity() returned");

        resourceMBean.removeResourceAdaptorEntity(RA_ENTITY_NAME);
        messages = castMessages(in.readQueue(10000));
        stateManager.runStateMachine(messages);

        if (stateManager.hasRAObjectInState(RAStateManager.STATE_INACTIVE) || stateManager.hasRAObjectInState(RAStateManager.STATE_ACTIVE)
                || stateManager.hasRAObjectInState(RAStateManager.STATE_STOPPING))
            throw new TCKTestFailureException(1115522, "Resource Adaptor still had objects in the INACTIVE, ACTIVE, or STOPPING state after entity removal");

        return TCKTestResult.passed();
    }

    public void tearDown() throws Exception {
        try {
            utils().removeRAEntities();
        } finally {
            super.tearDown();
        }

    }

    public void setUp() throws Exception {
        String duPath = utils().getTestParams().getProperty(RESOURCE_DU_PATH_PARAM);
        utils().install(duPath);
    }

    //
    // Private
    //

    private TCKMessage[] castMessages(Object[] objects) {
        TCKMessage[] messages = new TCKMessage[objects.length];
        for (int i = 0; i < objects.length; i++)
            messages[i] = (TCKMessage) objects[i];
        return messages;
    }

}
