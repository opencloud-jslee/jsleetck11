/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.facilities.timerfacility.address;

import com.opencloud.sleetck.lib.sbbutils.BaseTCKSbb;
import com.opencloud.sleetck.lib.sbbutils.TCKSbbUtils;
import com.opencloud.sleetck.lib.resource.events.TCKResourceEventX;
import com.opencloud.sleetck.lib.resource.sbbapi.TCKActivity;

import javax.naming.Context;
import javax.naming.InitialContext;

import java.util.HashMap;

import javax.slee.*;
import javax.slee.facilities.*;
import javax.slee.nullactivity.*;
import javax.slee.profile.*;

/**
 * Test for TimerFacility TimerEvents fired to specific addresses
 */
public abstract class SetTimerAddressSbb2 extends BaseTCKSbb {

    public void onTimerEvent(TimerEvent event, ActivityContextInterface aci) {
        try {
            TCKSbbUtils.createTrace(getSbbID(),Level.INFO,"Received timer event",null);

            SetTimerAddressSbb2ActivityContextInterface sharedAC = asSbbActivityContextInterface(aci);
            // flag that we received the timer event
            sharedAC.setReceived(true);

            sharedAC.detach(getSbbContext().getSbbLocalObject());

        } catch(Exception e) {
            TCKSbbUtils.handleException(e);
        }
    }

    public abstract SetTimerAddressSbb2ActivityContextInterface asSbbActivityContextInterface(ActivityContextInterface aci);
    public abstract SetTimerAddressSbb2ProfileCMP getProfileCMP(ProfileID profileID) throws UnrecognizedProfileTableNameException, UnrecognizedProfileNameException;


}
