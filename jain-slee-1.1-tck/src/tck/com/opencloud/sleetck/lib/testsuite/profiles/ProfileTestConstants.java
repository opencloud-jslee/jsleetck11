/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.profiles;

/**
 * Defines constants common to many profile tests.
 */
public class ProfileTestConstants {

    // Test Parameters

    public static final String PROFILE_SPEC_DU_PATH_PARAM = "profileSpecDUPath";
    public static final String SIMPLE_PROFILE_SPEC_NAME = "SimpleProfile";
    public static final String SIMPLE_PROFILE_11_SPEC_NAME = "SimpleProfile11";

}