/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.javax.slee.usage.UsageUpdatedFilter;

import com.opencloud.sleetck.lib.AbstractSleeTCKTest;
import com.opencloud.sleetck.lib.OperationTimedOutException;
import com.opencloud.sleetck.lib.TCKTestFailureException;
import com.opencloud.sleetck.lib.TCKTestResult;
import com.opencloud.sleetck.lib.resource.TCKActivityID;
import com.opencloud.sleetck.lib.resource.events.TCKResourceEventX;
import com.opencloud.sleetck.lib.resource.events.TCKResourceEventY;
import com.opencloud.sleetck.lib.testsuite.usage.common.GenericUsageSbbInstructions;
import com.opencloud.sleetck.lib.testsuite.usage.common.UsageMBeanLookup;
import com.opencloud.sleetck.lib.testutils.QueuingNotificationListener;
import com.opencloud.sleetck.lib.testutils.QueuingResourceListener;

import javax.management.Notification;
import javax.slee.SbbID;
import javax.slee.ServiceID;
import javax.slee.usage.UsageNotification;
import javax.slee.usage.UsageUpdatedFilter;
import java.util.LinkedList;
import java.util.List;
import java.util.Iterator;

/**
 * Tests assertions related to the UsageUpdatedFilter.
 * Tries to create a UsageUpdatedFilter will null arguments.
 * Checks that notifications which are not UsageNoficiations are supressed,
 * by registering a listener and filter with the TraceNoficiationMBean
 * and firing trace messages.
 * Checks that noficiations with matching Service id, Sbb id and parameter name
 * are received, and that notifications without matching Service id, Sbb id or
 * parameter name are supressed.
 */
public class UsageUpdatedFilterTest extends AbstractSleeTCKTest {

    private static final String CHOSEN_PARAMETER_NAME = "timeBetweenNewConnections";

    public TCKTestResult run() throws Exception {

        checkNullPointerExceptions();

        notificationListener = new QueuingNotificationListener(utils());
        usageUpdatedFilter = new UsageUpdatedFilter(chosenServiceID,chosenSbbID,CHOSEN_PARAMETER_NAME);

        // register the listener and filter with the four SBBs, to check sbb id/service id filtering
        UsageMBeanLookup[] usageMBeanLookups = {usageMBeanLookupChildA,usageMBeanLookupChildB,
                                                usageMBeanLookupParentA,usageMBeanLookupParentB};
        for (int i = 0; i < usageMBeanLookups.length; i++) {
            UsageMBeanLookup usageMBeanLookup = usageMBeanLookups[i];
            usageMBeanLookup.getUnnamedSbbUsageMBeanProxy().addNotificationListener(
                    notificationListener,usageUpdatedFilter,null);
        }

        // register the listener and filter with the trace MBean, to check filtering on notification type
        utils().getTraceMBeanProxy().addNotificationListener(notificationListener,usageUpdatedFilter,null);

        // fire a set of instructions to all four SBBs
        GenericUsageSbbInstructions instructions = new GenericUsageSbbInstructions(null);
        instructions.addFirstCountIncrement(1);
        instructions.addSecondCountIncrement(2);
        instructions.addTimeBetweenErrorsSample(3);
        instructions.addTimeBetweenNewConnectionsSamples(4);
        expectedValues.add(new Long(4));
        TCKActivityID activityID = utils().getResourceInterface().createActivity("UsageUpdatedFilterTest-Activity");
        utils().getResourceInterface().fireEvent(TCKResourceEventX.X1,instructions.toExported(),activityID,null);
        utils().getResourceInterface().fireEvent(TCKResourceEventX.X2,instructions.toExported(),activityID,null);
        utils().getResourceInterface().fireEvent(TCKResourceEventY.Y1,instructions.toExported(),activityID,null);

        getLog().info("Waiting for replies from the SBBs");
        resourceListener.nextMessage();
        resourceListener.nextMessage();

        getLog().info("Waiting for usage notifications");
        auditNotification(getNextNotificationOrFail());

        // we don't expect any more notifications at this stage -- cause and wait for a new valid one
        // so that we when we receive it we know that there weren't any outstaning invalid notifications
        getLog().info("Sending second request to SBB A");
        instructions = new GenericUsageSbbInstructions(null);
        instructions.addTimeBetweenNewConnectionsSamples(6);
        expectedValues.add(new Long(6));
        utils().getResourceInterface().fireEvent(TCKResourceEventX.X2,instructions.toExported(),activityID,null);
        getLog().info("Waiting for reply from the SBB");
        resourceListener.nextMessage();
        getLog().info("Waiting for usage notification");

        auditNotification(getNextNotificationOrFail());

        return TCKTestResult.passed();
    }

    private void checkNullPointerExceptions() throws TCKTestFailureException {
        try {
            new UsageUpdatedFilter(chosenServiceID,chosenSbbID,null);
            throw new TCKTestFailureException(4237,"Failed to fire a NullPointerException while trying to create a UsageUpdatedFilter with"+
                    "a null parameter name");
        } catch (NullPointerException e) {
            getLog().info("Caught excpected NullPointerException while trying to create a UsageUpdatedFilter with "+
                    "a null parameter name");
        }
        try {
            new UsageUpdatedFilter(chosenServiceID,null,CHOSEN_PARAMETER_NAME);
            throw new TCKTestFailureException(4237,"Failed to fire a NullPointerException while trying to create a UsageUpdatedFilter with"+
                    "a null sbb id");
        } catch (NullPointerException e) {
            getLog().info("Caught excpected NullPointerException which trying to create a UsageUpdatedFilter with "+
                    "a null sbb id");
        }
        try {
            new UsageUpdatedFilter(null,chosenSbbID,CHOSEN_PARAMETER_NAME);
            throw new TCKTestFailureException(4237,"Failed to fire a NullPointerException while trying to create a UsageUpdatedFilter with"+
                    "a null service id");
        } catch (NullPointerException e) {
            getLog().info("Caught excpected NullPointerException which trying to create a UsageUpdatedFilter with "+
                    "a null service id");
        }
    }

    private void auditNotification(Notification notification) throws Exception {
        getLog().info("Auditing notification: "+notification);
        if(!(notification instanceof UsageNotification)) throw new TCKTestFailureException(4235,
                "UsageUpadtedFilter allowed a notification to pass which was not a UsageNotification: "+notification);
        UsageNotification usageNotification = (UsageNotification)notification;
        // service ID
        if(!usageNotification.getService().equals(chosenServiceID)) throw new TCKTestFailureException(4234,
                "UsageUpadtedFilter allowed a notification to pass with a non-matching ServiceID. Chosen ServiceID:"+chosenServiceID+
                ", found ServiceID:"+usageNotification.getService());
        // sbb ID
        if(!usageNotification.getSbb().equals(chosenSbbID)) throw new TCKTestFailureException(4234,
                "UsageUpadtedFilter allowed a notification to pass with a non-matching SbbID. Chosen SbbID:"+chosenSbbID+
                ", found SbbID:"+usageNotification.getSbb());
        // parameter name
        if(!usageNotification.getUsageParameterName().equals(CHOSEN_PARAMETER_NAME)) throw new TCKTestFailureException(4234,
                "UsageUpadtedFilter allowed a notification to pass with a non-matching parameter name. Chosen parameter name:"+CHOSEN_PARAMETER_NAME+
                ", found parameter name:"+usageNotification.getUsageParameterName());

        // check that this is the next notification we expect, i.e. that we haven't missed any expected notifications
        long nextExpectedValue = ((Long)expectedValues.get(notificationsReceived-1)).longValue();
        if(nextExpectedValue != usageNotification.getValue()) {
            throw new TCKTestFailureException(4234,
                "Missed expected usage notification with value "+nextExpectedValue+
                ", which appears to have been illegally filtered by the UsageUpdatedFilter. "+
                "Expected values:"+formatLongs(expectedValues)+
                "; number of received expected values:"+(notificationsReceived-1)+"; value of this notification:"+
                usageNotification.getValue());
        }
    }

    private Notification getNextNotificationOrFail() throws TCKTestFailureException {
        try {
            Notification rNotification = notificationListener.nextNotification();
            getLog().info("Received notification: "+rNotification);
            notificationsReceived++;
            return rNotification;
        } catch (OperationTimedOutException e) {
            throw new TCKTestFailureException(4234,"Timed out while waiting for expected UsageNotification, "+
                    "indicating that the notification was filtered illegally by the UsageUpdatedFilter. Notifications received so far: "+
                    notificationsReceived);
        }
    }

    private String formatLongs(List listOfLongs) {
        if(listOfLongs.isEmpty()) return "(empty set)";
        Iterator iter = listOfLongs.iterator();
        StringBuffer buf = new StringBuffer("("+iter.next());
        while(iter.hasNext()) {
            buf.append(","+iter.next());
        }
        return buf.append(")").toString();
    }

    public void setUp() throws Exception {
        super.setUp();
        notificationsReceived = 0;
        expectedValues = new LinkedList();
        usageMBeanLookupParentA = new UsageMBeanLookup("UsageFilterTestsServiceA","UsageFilterTestsParentSbbA",utils());
        usageMBeanLookupChildA  = new UsageMBeanLookup("UsageFilterTestsServiceA","UsageFilterTestsChildSbb",utils());
        usageMBeanLookupParentB = new UsageMBeanLookup("UsageFilterTestsServiceB","UsageFilterTestsParentSbbB",utils());
        usageMBeanLookupChildB  = new UsageMBeanLookup("UsageFilterTestsServiceB","UsageFilterTestsChildSbb",utils());
        chosenSbbID = usageMBeanLookupChildA.getSbbID();
        chosenServiceID = usageMBeanLookupChildA.getServiceID();
        setResourceListener(resourceListener = new QueuingResourceListener(utils()));
    }

    public void tearDown() throws Exception {
        UsageMBeanLookup[] usageMBeanLookups = {usageMBeanLookupChildA,usageMBeanLookupChildB,
                                                usageMBeanLookupParentA,usageMBeanLookupParentB};
        if(notificationListener != null) {
            for (int i = 0; i < usageMBeanLookups.length; i++) {
                UsageMBeanLookup usageMBeanLookup = usageMBeanLookups[i];
                try {
                    usageMBeanLookup.getUnnamedSbbUsageMBeanProxy().removeNotificationListener(notificationListener);
                } catch (Exception e) { getLog().warning(e); }
            }
            try {
                utils().getTraceMBeanProxy().removeNotificationListener(notificationListener);
            } catch (Exception e) { getLog().warning(e); }
        }
        for (int i = 0; i < usageMBeanLookups.length; i++) {
            UsageMBeanLookup usageMBeanLookup = usageMBeanLookups[i];
            if(usageMBeanLookup != null) usageMBeanLookup.closeAllMBeans();
        }
        super.tearDown();
    }

    private QueuingResourceListener resourceListener;
    private UsageMBeanLookup usageMBeanLookupParentA;
    private UsageMBeanLookup usageMBeanLookupChildA;
    private UsageMBeanLookup usageMBeanLookupParentB;
    private UsageMBeanLookup usageMBeanLookupChildB;
    private QueuingNotificationListener notificationListener;
    private UsageUpdatedFilter usageUpdatedFilter;
    private int notificationsReceived;
    private SbbID chosenSbbID;
    private ServiceID chosenServiceID;
    // a list representing the notifications we expect to pass
    private LinkedList expectedValues;

}
