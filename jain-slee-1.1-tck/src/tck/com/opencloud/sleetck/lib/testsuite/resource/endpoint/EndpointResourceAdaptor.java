/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.resource.endpoint;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.HashMap;

import javax.slee.EventTypeID;
import javax.slee.SLEEException;
import javax.slee.TransactionRequiredLocalException;
import javax.slee.UnrecognizedEventException;
import javax.slee.UnrecognizedServiceException;
import javax.slee.resource.ActivityAlreadyExistsException;
import javax.slee.resource.ActivityFlags;
import javax.slee.resource.ActivityHandle;
import javax.slee.resource.ActivityIsEndingException;
import javax.slee.resource.ConfigProperties;
import javax.slee.resource.EventFlags;
import javax.slee.resource.FireEventException;
import javax.slee.resource.FireableEventType;
import javax.slee.resource.IllegalEventException;
import javax.slee.resource.ResourceAdaptorContext;
import javax.slee.resource.SleeEndpoint;
import javax.slee.resource.UnrecognizedActivityHandleException;
import javax.slee.transaction.SleeTransactionManager;
import javax.transaction.Transaction;

import com.opencloud.sleetck.lib.infra.SleeTCKComponentConstants;
import com.opencloud.sleetck.lib.rautils.BaseTCKRA;
import com.opencloud.sleetck.lib.rautils.MessageHandler;
import com.opencloud.sleetck.lib.rautils.MessageHandlerRegistry;
import com.opencloud.sleetck.lib.rautils.TCKRAUtils;
import com.opencloud.sleetck.lib.resource.adaptor11.TCKActivityHandleImpl;
import com.opencloud.sleetck.lib.testsuite.resource.SimpleEvent;
import com.opencloud.sleetck.lib.testsuite.resource.TCKMessage;

/**
 * SLEE 1.1 Endpoint testing Resource Adaptor
 */
public class EndpointResourceAdaptor extends BaseTCKRA {

    //
    // Resource Adaptor
    //

    public void activityUnreferenced(ActivityHandle handle) {
        getLog().info("activityUnreferenced(): " + handle + " (ending)");
        try {
            endpoint.endActivity(handle);
        }
        catch (UnrecognizedActivityHandleException uahe) {
         // ignore - the activity may have already ended
        }
    }

    public void raConfigure(ConfigProperties properties) {
        isConfigured = true;
    }

    public void raUnconfigure() {
        isConfigured = false;
    }

    public void setResourceAdaptorContext(ResourceAdaptorContext context) {
        super.setResourceAdaptorContext(context);
        setTracer(context.getTracer("Endpoint RA"));
        try {
            MessageHandlerRegistry registry = TCKRAUtils.lookupMessageHandlerRegistry();
            messageHandler = new EndpointMessageListener(this);
            registry.registerMessageHandler(messageHandler);
        } catch (Exception e) {
            getLog().severe("An error occured during setResourceAdaptorContext()", e);
        }
        endpoint = context.getSleeEndpoint();
        transactionManager = context.getSleeTransactionManager();
    }

    public void unsetResourceAdaptorContext() {
        transactionManager = null;
        endpoint = null;
        try {
            MessageHandlerRegistry registry = TCKRAUtils.lookupMessageHandlerRegistry();
            if (messageHandler != null)
                registry.unregisterMessageHandler(messageHandler);
        } catch (Exception e) {
            getLog().severe("An error occured during unsetResourceAdaptorContext()", e);
        }
        setTracer(null);
        super.unsetResourceAdaptorContext();
    }

    public boolean isConfigured() {
        return isConfigured;
    }

    public void executeTestLogic(Object obj) throws Exception {
        getLog().info("Received message from sbb: " + obj.toString());

        int assertion = -1;
        if (obj instanceof Integer)
            assertion = ((Integer) obj).intValue();

        HashMap results = null;
        switch (assertion) {
        case 1115303:
            results = test1115303b();
            break;
        case 1115276:
            results = test1115276b();
            break;
        case 1115232:
            results = test1115232b();
            break;
        case 1115233:
            results = test1115233b();
            break;
        case 1115234:
            results = test1115234b();
            break;
        case 1115277:
            results = test1115277b();
            break;
        case 1115278:
            results = test1115278b();
            break;
        case 1115316:
            results = test1115316b();
            break;

        default:
            getLog().warning("Unhandled message received from sbb: " + obj);
        }

        if (results != null)
            sendEndpointMessage(lastSequenceID, 0, results);
    }

    //
    // Private
    //

    private ResourceAdaptorContext getContext() {
        return super.getResourceAdaptorContext();
    }

    private void sendEndpointMessage(int sequenceID, int method) {
        sendEndpointMessage(sequenceID, method, null);
    }

    private void sendEndpointMessage(int sequenceID, int method, Object argument) {
        sendMessage(new TCKMessage(getRAUID(), sequenceID, method, argument));
    }

    private class EndpointMessageListener extends UnicastRemoteObject implements MessageHandler {

        public EndpointMessageListener(EndpointResourceAdaptor ra) throws RemoteException {
            super();
            this.ra = ra;
        }

        public boolean handleMessage(Object obj) throws RemoteException {
            if (!isConfigured())
                return false;

            ra.getLog().info("Received message from test: " + obj.toString());
            TCKMessage message = (TCKMessage) obj;
            int sequenceID = message.getSequenceID();
            lastSequenceID = sequenceID;
            int methodID = message.getMethod();
            HashMap results = null;

            Object argument = message.getArgument();
            int assertion = -1;

            if (argument instanceof Integer)
                assertion = ((Integer) argument).intValue();
            if (argument instanceof HashMap) {
                HashMap arguments = (HashMap) argument;
                assertion = ((Integer) arguments.get("assertion")).intValue();
                argument = arguments.get("argument");
            }

            switch (assertion) {
            // startActivity
            case 1115211:
                // 1115211, 1115212
                results = test1115211();
                break;
            case 1115247:
                // 1115247, 1115249
                results = test1115247();
                break;
            case 1115248:
                results = test1115248(argument);
                break;
            // startActivityTransacted
            case 1115214:
                // 1115214
                results = test1115214();
                break;
            case 1115215:
                results = test1115215();
                break;
            case 1115217:
                results = test1115217();
                break;
            case 1115258:
                results = test1115258();
                break;
            case 1115262:
                results = test1115262();
                break;
            case 1115263:
                results = test1115263(argument);
                break;
            case 1115265:
                results = test1115265(sequenceID);
                break;
            // endActivity
            case 1115257:
                results = test1115257();
                break;
            case 1115276:
                results = test1115276();
                break;
            case 1115280:
                results = test1115280();
                break;
            case 1115277:
                results = test1115277(sequenceID);
                break;
            case 1115278:
                results = test1115278(sequenceID);
                break;
            // endActivityTransacted
            case 1115231:
                results = test1115231();
                break;
            case 1115240:
                results = test1115240();
                break;
            case 1115232:
                results = test1115232(sequenceID);
                break;
            case 1115233:
                results = test1115233(sequenceID);
                break;
            case 1115234:
                results = test1115234(sequenceID);
                break;
            // fireEvent
            case 1115238:
                results = test1115238(sequenceID);
                break;
            case 1115255:
                results = test1115255();
                break;
            case 1115298:
                results = test1115298();
                break;
            case 1115299:
                results = test1115299();
                break;
            case 1115300:
                results = test1115300();
                break;
            case 1115303:
                results = test1115303(sequenceID);
                break;
            case 1115296:
                results = test1115296(sequenceID);
                break;
            // fireEventTransacted
            case 1115241:
                results = test1115241(sequenceID);
                break;
            case 1115242:
                results = test1115242(sequenceID);
                break;
            case 1115306:
                results = test1115306(sequenceID);
                break;
            case 1115308:
                results = test1115308(sequenceID);
                break;
            case 1115309:
                results = test1115309();
                break;
            case 1115311:
                results = test1115311();
                break;
            case 1115312:
                results = test1115312();
                break;
            case 1115313:
                results = test1115313();
                break;
            case 1115314:
                results = test1115314();
                break;
            case 1115316:
                results = test1115316(sequenceID);
                break;
            // endActivityTransacted
            case 1115291:
                results = test1115291();
                break;
            // suspendActivity
            case 1115417:
                results = test1115417();
                break;
            case 1115413:
                results = test1115413(sequenceID);
                break;
            // startActivitySuspended
            case 1115420:
                results = test1115420();
                break;
            case 1115418:
                results = test1115418(sequenceID);
                break;
            default:
                ra.getLog().warning("Unhandled message received from test: " + message);
                return false;
            }

            if (results != null)
                sendEndpointMessage(sequenceID, methodID, results);
            return true;
        }

        private transient EndpointResourceAdaptor ra = null;
    }

    private void fireEvent(ActivityHandle handle, int sequenceID) throws IllegalEventException, NullPointerException, IllegalArgumentException, UnrecognizedServiceException,
            SLEEException, UnrecognizedActivityHandleException, ActivityIsEndingException, UnrecognizedEventException, FireEventException {
        fireEvent(handle, sequenceID, null);
    }

    private void fireEvent(ActivityHandle handle, int sequenceID, Object payload) throws IllegalEventException, NullPointerException, IllegalArgumentException,
            UnrecognizedServiceException, SLEEException, UnrecognizedActivityHandleException, ActivityIsEndingException, UnrecognizedEventException, FireEventException {
        EventTypeID eventTypeID = new EventTypeID(SimpleEvent.SIMPLE_EVENT_NAME, SleeTCKComponentConstants.TCK_VENDOR, "1.1");
        FireableEventType eventType = getContext().getEventLookupFacility().getFireableEventType(eventTypeID);
        endpoint.fireEvent(handle, eventType, new SimpleEvent(sequenceID, payload), null, null);
    }

    private void fireEventTransacted(ActivityHandle handle, int sequenceID, Object payload) throws IllegalEventException, NullPointerException, IllegalArgumentException,
            UnrecognizedServiceException, SLEEException, UnrecognizedActivityHandleException, ActivityIsEndingException, UnrecognizedEventException, FireEventException {
        EventTypeID eventTypeID = new EventTypeID(SimpleEvent.SIMPLE_EVENT_NAME, SleeTCKComponentConstants.TCK_VENDOR, "1.1");
        FireableEventType eventType = getContext().getEventLookupFacility().getFireableEventType(eventTypeID);
        endpoint.fireEventTransacted(handle, eventType, new SimpleEvent(sequenceID, payload), null, null);
    }

    private void fireEventTransacted(ActivityHandle handle, int sequenceID) throws IllegalEventException, NullPointerException, IllegalArgumentException,
            UnrecognizedServiceException, SLEEException, UnrecognizedActivityHandleException, ActivityIsEndingException, UnrecognizedEventException, FireEventException {
        fireEventTransacted(handle, sequenceID, null);
    }

    //
    // Test logic
    //

    // 1115211
    private HashMap test1115211() {
        HashMap results = new HashMap();
        ActivityHandle handle = new TCKActivityHandleImpl(System.currentTimeMillis(), 1115211);

        boolean activityStarted = false;
        try {
            transactionManager.beginSleeTransaction();
            try {
                endpoint.startActivity(handle, handle);
                activityStarted = true;

                Transaction suspended = transactionManager.suspend();
                try {
                    transactionManager.beginSleeTransaction();
                    try {
                        /*
                         * Attempting to start the same activity again in a new transaction should throw an exception
                         * indicating that the Slee believes it to exist already.
                         */
                        try {
                            endpoint.startActivity(handle, handle);
                            /*
                             * If no exception occured, set a 'failed' result. Also try to clean up activity.
                             */
                            results.put("result", Boolean.FALSE);
                        } catch (ActivityAlreadyExistsException e) {
                            results.put("result", Boolean.TRUE); // Result
                            // indicating
                            // success.
                        }
                    } finally {
                        transactionManager.commit();
                    }
                } finally {
                    transactionManager.resume(suspended);
                }
            } finally {
                transactionManager.commit();
                if (activityStarted)
                    endpoint.endActivity(handle);
            }
        } catch (Exception e) {
            // Unexpected exception - indicates a test failure
            results.put("result", e);
        }

        return results;
    }

    // 1115214
    private HashMap test1115214() {
        HashMap results = new HashMap();

        ActivityHandle handle = new TCKActivityHandleImpl(System.currentTimeMillis(), 1115214);
        try {
            endpoint.startActivityTransacted(handle, handle);
            results.put("result", Boolean.FALSE);
        } catch (TransactionRequiredLocalException trle) {
            // Result indicating success.
            results.put("result", Boolean.TRUE);
        } catch (Exception e) {
            results.put("result", e); // Unexpected exception
        }

        return results;
    }

    // 1115215, 1115216
    private HashMap test1115215() {
        HashMap results = new HashMap();
        boolean activityStarted = false;
        try {
            ActivityHandle handle = new TCKActivityHandleImpl(System.currentTimeMillis(), 1115215);
            try {
                transactionManager.beginSleeTransaction();
                try {
                    endpoint.startActivityTransacted(handle, handle);
                    activityStarted = true;
                    Transaction suspended = transactionManager.suspend();
                    try {
                        transactionManager.beginSleeTransaction();
                        try {
                            // Activity should not exist in this transaction,
                            // so trying to end it should throw an exception.
                            endpoint.endActivityTransacted(handle);
                            results.put("result1", Boolean.FALSE);
                        } catch (UnrecognizedActivityHandleException e) {
                            results.put("result1", Boolean.TRUE);
                        } catch (SLEEException e) {
                            results.put("result1", Boolean.TRUE);
                        } catch (Exception e) {
                            results.put("result1", e);
                        } finally {
                            transactionManager.rollback();
                        }
                    } finally {
                        transactionManager.resume(suspended);
                    }
                } finally {
                    transactionManager.commit();
                }

                // Handle should now be created - check to see that it was
                // (using both transacted and non-transacted method calls).

                try {
                    // Should throw ActivityAlreadyExistsException
                    endpoint.startActivity(handle, handle);
                    results.put("result2", Boolean.FALSE);
                } catch (ActivityAlreadyExistsException e) {
                    results.put("result2", Boolean.TRUE);
                } catch (Exception e) {
                    results.put("result2", e);
                }

                transactionManager.beginSleeTransaction();
                try {
                    try {
                        // Should throw ActivityAlreadyExistsException
                        endpoint.startActivityTransacted(handle, handle);
                        results.put("result3", Boolean.FALSE);
                    } catch (ActivityAlreadyExistsException e) {
                        results.put("result3", Boolean.TRUE);
                    } catch (Exception e) {
                        results.put("result3", e);
                    }
                } finally {
                    transactionManager.rollback();
                }
            } finally {
                if (activityStarted)
                    endpoint.endActivity(handle);
            }
        } catch (Exception e) {
            results.put("result1", e);
        }

        return results;
    }

    // 1115217
    private HashMap test1115217() {
        HashMap results = new HashMap();
        try {
            ActivityHandle handle = new TCKActivityHandleImpl(System.currentTimeMillis(), 1115217);

            getLog().info("Starting transaction 1");
            transactionManager.beginSleeTransaction();
            try {
                getLog().info("Starting activity transacted: " + handle);
                endpoint.startActivityTransacted(handle, handle);
            } finally {
                getLog().info("Rolling back transaction 1");
                transactionManager.rollback();
            }

            getLog().info("Starting transaction 2");
            transactionManager.beginSleeTransaction();
            try {
                try {
                    // Should not throw a ActivityAlreadyExistsException
                    getLog().info("Starting activity transacted: " + handle);
                    endpoint.startActivityTransacted(handle, handle);
                    results.put("result", Boolean.TRUE);
                } catch (ActivityAlreadyExistsException e) {
                    results.put("result", Boolean.FALSE);
                } catch (Exception e) {
                    results.put("result", e);
                }
            } finally {
                getLog().info("Rolling back transaction 2");
                transactionManager.rollback();
            }

            // Brute force test cleanup (just in case)
            try {
                endpoint.endActivity(handle);
            } catch (Exception e) {
                getLog().info("Ignoring exception caught during test cleanup", e);
            }

        } catch (Exception e) {
            results.put("result", e);
        }
        return results;
    }

    // 1115231
    private HashMap test1115231() {
        HashMap results = new HashMap();

        ActivityHandle handle = new TCKActivityHandleImpl(System.currentTimeMillis(), 1115231);
        try {
            endpoint.startActivity(handle, handle);
            try {
                endpoint.endActivityTransacted(handle);
                results.put("result", Boolean.FALSE);
            } catch (TransactionRequiredLocalException trle) {
                results.put("result", Boolean.TRUE);
            } finally {
                endpoint.endActivity(handle);
            }
        } catch (Exception e) {
            results.put("result", e);
        }

        return results;
    }

    /**
     * Test to ensure that endActivityTransacted() is correctly transitioning the Activity's Activity Context to the
     * Ending state, and that the transition is visible to the current and subsequent transactions.
     * <p>
     * This test relies on service priority working correctly (it uses multiple sbbs).
     * <p>
     * Test assertion: 1115232
     */
    private HashMap test1115232(int sequenceID) {
        HashMap results = new HashMap();

        TCKActivityHandleImpl handle = new TCKActivityHandleImpl(System.currentTimeMillis(), 1115232);
        lastActivityHandle = handle;
        results.put("result-ra1", Boolean.TRUE);
        try {
            endpoint.startActivity(handle, handle);
            fireEvent(handle, sequenceID);
            // Note: Activity is intentionally ended by SBB, not here - any
            // errors there are we have no cleanup.
        } catch (Exception e) {
            results.put("result-ra1", e);
        }

        return results;
    }

    private HashMap test1115232b() {
        HashMap results = new HashMap();

        results.put("result-ra2", Boolean.TRUE);
        try {
            endpoint.endActivityTransacted(lastActivityHandle);
        } catch (Exception e) {
            results.put("result-ra2", e);
        }

        return results;
    }

    /**
     * Test to ensure that endActivityTransacted() does not transition the Activity's Activity Context to the Ending
     * state if the transaction is rolled back.
     * <p>
     * This test relies on service priority working correctly (it uses multiple sbbs).
     * <p>
     * Test assertion: 1115233
     */
    private HashMap test1115233(int sequenceID) {
        HashMap results = new HashMap();

        TCKActivityHandleImpl handle = new TCKActivityHandleImpl(System.currentTimeMillis(), 1115233);
        lastActivityHandle = handle;
        results.put("result-ra1", Boolean.TRUE);
        try {
            endpoint.startActivity(handle, handle);
            fireEvent(handle, sequenceID);
            // Note: Activity is intentionally ended by SBB, not here - any
            // errors there and we have no cleanup.
        } catch (Exception e) {
            results.put("result-ra1", e);
        }

        return results;
    }

    private HashMap test1115233b() {
        HashMap results = new HashMap();

        results.put("result-ra2", Boolean.TRUE);
        try {
            endpoint.endActivityTransacted(lastActivityHandle);
        } catch (Exception e) {
            results.put("result-ra2", e);
        }

        return results;
    }

    /**
     * Test to ensure that the activity state change caused by endActivityTransacted() is only visible in the currently
     * transcation.
     * <p>
     * Test assertion: 1115234
     */
    private HashMap test1115234(int sequenceID) {
        HashMap results = new HashMap();

        TCKActivityHandleImpl handle = new TCKActivityHandleImpl(System.currentTimeMillis(), 1115234);
        lastActivityHandle = handle;
        results.put("result-ra1", Boolean.TRUE);
        try {
            endpoint.startActivity(handle, handle);
            getLog().info("Firing event A (non-transacted)");
            fireEvent(handle, sequenceID, "A");
            // Note: Activity is intentionally ended by SBB, not here - any
            // errors there and we have no cleanup.
        } catch (Exception e) {
            results.put("result-ra1", e);
        }

        return results;
    }

    private HashMap test1115234b() {
        HashMap results = new HashMap();

        results.put("result-ra2", Boolean.TRUE);
        try {
            getLog().info("Ending activity (transacted)");
            endpoint.endActivityTransacted(lastActivityHandle);
            getLog().info("Firing event B (non-transacted)");
            fireEvent(lastActivityHandle, lastSequenceID, "B");
        } catch (Exception e) {
            results.put("result-ra2", e);
        }

        return results;
    }

    // 1115238
    // Test that fireEvent is a non-transactional method by firing an event in
    // a transaction which is then rolled back, and checking that it is still
    // delivered to an Sbb (i.e it was not enrolled in the transaction).
    private HashMap test1115238(int sequenceID) {
        HashMap results = new HashMap();
        ActivityHandle handle = new TCKActivityHandleImpl(System.currentTimeMillis(), 1115238);

        results.put("result1", Boolean.TRUE);
        // Test that fire event is behaving in a non-transactional way.
        try {
            transactionManager.beginSleeTransaction();
            try {
                endpoint.startActivity(handle, handle);
                try {
                    fireEvent(handle, sequenceID);
                } finally {
                    endpoint.endActivity(handle);
                }
            } finally {
                transactionManager.rollback();
            }
        } catch (Exception e) {
            getLog().severe("An error occured during test event firing for Test1115238Test", e);
            results.put("result1", e);
        }

        return results;
    }

    private HashMap test1115240() {
        HashMap results = new HashMap();
        ActivityHandle handle = new TCKActivityHandleImpl(System.currentTimeMillis(), 1115240);

        try {
            endpoint.startActivity(handle, handle);
            try {
                try {
                    fireEventTransacted(handle, 0);
                    results.put("result", Boolean.FALSE);
                } catch (TransactionRequiredLocalException trle) {
                    results.put("result", Boolean.TRUE);
                }
            } finally {
                endpoint.endActivity(handle);
            }
        } catch (Exception e) {
            results.put("result", e);
        }
        return results;
    }

    private HashMap test1115241(int sequenceID) {
        HashMap results = new HashMap();
        ActivityHandle handle = new TCKActivityHandleImpl(System.currentTimeMillis(), 1115241);

        results.put("result1", Boolean.TRUE);
        try {
            getLog().info("Beginning transaction");
            transactionManager.beginSleeTransaction();
            try {
                getLog().info("Starting activity (transacted): " + handle);
                endpoint.startActivityTransacted(handle, handle);
                try {
                    getLog().info("Firing event (transacted)");
                    fireEventTransacted(handle, sequenceID);
                } finally {
                    getLog().info("Ending activity (transacted)");
                    endpoint.endActivityTransacted(handle);
                }
            } finally {
                getLog().info("Committing transaction");
                transactionManager.commit();
            }
        } catch (Exception e) {
            results.put("result1", e);
        }
        return results;
    }

    private HashMap test1115242(int sequenceID) {
        HashMap results = new HashMap();
        ActivityHandle handle = new TCKActivityHandleImpl(System.currentTimeMillis(), 1115242);

        results.put("result1", Boolean.TRUE);
        try {
            endpoint.startActivity(handle, handle);
            try {
                transactionManager.beginSleeTransaction();
                try {
                    fireEventTransacted(handle, sequenceID);
                } finally {
                    transactionManager.rollback();
                }
            } finally {
                endpoint.endActivity(handle);
            }
        } catch (Exception e) {
            results.put("result1", e);
        }

        return results;
    }

    // 1115247, 1115249
    private HashMap test1115247() {
        HashMap results = new HashMap();
        ActivityHandle handle = new TCKActivityHandleImpl(System.currentTimeMillis(), 1115247);

        // NPE check 1
        try {
            endpoint.startActivity(null, null);
            results.put("result1", Boolean.FALSE);
        } catch (NullPointerException npe) {
            results.put("result1", Boolean.TRUE);
        } catch (Exception e) {
            results.put("result1", e);
        }

        // NPE check 2
        try {
            endpoint.startActivity(null, new Object());
            results.put("result2", Boolean.FALSE);
        } catch (NullPointerException npe) {
            results.put("result2", Boolean.TRUE);
        } catch (Exception e) {
            results.put("result2", e);
        }

        // NPE check 3
        boolean failed = false;
        try {
            endpoint.startActivity(handle, null);
            results.put("result3", Boolean.FALSE);
            failed = true;
        } catch (NullPointerException npe) {
            results.put("result3", Boolean.TRUE);
        } catch (Exception e) {
            results.put("result3", e);
        } finally {
            if (failed)
                endpoint.endActivity(handle);
        }

        // ActivityAlreadyExistsException check 1
        try {
            endpoint.startActivity(handle, handle);
            try {
                try {
                    // Check for ActivityAlreadyExistsException
                    endpoint.startActivity(handle, handle);
                    results.put("result4", Boolean.FALSE);
                } catch (ActivityAlreadyExistsException aaee) {
                    results.put("result4", Boolean.TRUE);
                }
            } finally {
                endpoint.endActivity(handle);
            }
        } catch (Exception e) {
            results.put("result4", e);
        }
        return results;
    }

    // This will be invoked multiple times during the test for 1115248
    private HashMap test1115248(Object argument) {
        HashMap results = new HashMap();

        if ("startActivity".equals(argument)) {
            getLog().info("Creating activity in RA to prevent RA Entity from stopping");
            lastActivityHandle = new TCKActivityHandleImpl(System.currentTimeMillis(), 1115246);
            try {
                endpoint.startActivity(lastActivityHandle, lastActivityHandle);
                results.put("result", Boolean.TRUE);
            } catch (Exception e) {
                results.put("result", e);
            }
            return results;
        } else if ("endActivity".equals(argument)) {
            getLog().info("Removing activity from RA to allow RA to transition into the Inactive state");
            try {
                endpoint.endActivity(lastActivityHandle);
                results.put("result", Boolean.TRUE);
            } catch (Exception e) {
                results.put("result", e);
            }
            return results;
        }

        getLog().info("Testing that startActivity() throws an IllegalStateException");
        ActivityHandle handle = new TCKActivityHandleImpl(System.currentTimeMillis(), 1115248);
        boolean failed = false;
        try {
            endpoint.startActivity(handle, handle);
            results.put("result", Boolean.FALSE);
            failed = true;
        } catch (IllegalStateException ise) {
            results.put("result", Boolean.TRUE);
        } catch (Exception e) {
            results.put("result", e);
        }

        if (failed) {
            try {
                endpoint.endActivity(handle);
            } catch (Exception e) {
                getLog().warning("Exception caught while cleaning up failed test.", e);
            }
        }

        return results;
    }

    private HashMap test1115255() {
        HashMap results = new HashMap();
        ActivityHandle handle = new TCKActivityHandleImpl(System.currentTimeMillis(), 1115255);

        try {
            transactionManager.beginSleeTransaction();
            try {
                endpoint.startActivityTransacted(handle, handle);
                try {
                    try {
                        fireEvent(handle, 1115255);
                        results.put("result", Boolean.FALSE);
                    } catch (UnrecognizedActivityHandleException e) {
                        results.put("result", Boolean.TRUE);
                    }
                } finally {
                    endpoint.endActivityTransacted(handle);
                }
            } finally {
                transactionManager.commit();
            }
        } catch (Exception e) {
            results.put("result", e);
        }
        return results;
    }

    private HashMap test1115257() {
        HashMap results = new HashMap();
        ActivityHandle handle = new TCKActivityHandleImpl(System.currentTimeMillis(), 1115257);

        boolean failed = false;
        try {
            transactionManager.beginSleeTransaction();
            try {
                endpoint.startActivityTransacted(handle, handle);
                try {
                    try {
                        endpoint.endActivity(handle);
                        results.put("result", Boolean.FALSE);
                        failed = true;
                    } catch (UnrecognizedActivityHandleException e) {
                        results.put("result", Boolean.TRUE);
                    }
                } finally {
                    try {
                        endpoint.endActivityTransacted(handle);
                    } catch (Exception e) {
                        // If the test failed, don't obscure that error with
                        // another. We still want to run the 'teardown' logic
                        // though, just in case.
                        if (!failed)
                            throw e;
                    }
                }
            } finally {
                transactionManager.commit();
            }
        } catch (Exception e) {
            results.put("result", e);
        }
        return results;
    }

    private HashMap test1115258() {
        HashMap results = new HashMap();
        ActivityHandle handle = new TCKActivityHandleImpl(System.currentTimeMillis(), 1115258);

        boolean failed1 = false;
        try {
            endpoint.startActivityTransacted(handle, handle);
            results.put("result1", Boolean.FALSE);
            failed1 = true;
        } catch (TransactionRequiredLocalException trle) {
            results.put("result1", Boolean.TRUE);
        } catch (Exception e) {
            results.put("result1", e);
        }

        // Ignore errors on cleanup so they don't obscure test result.
        if (failed1) {
            try {
                endpoint.endActivityTransacted(handle);
            } catch (Exception e) {
                getLog().warning("Exception caught while cleaning up failed test", e);
            }
        }

        boolean failed2 = false;
        try {
            endpoint.startActivityTransacted(handle, handle, ActivityFlags.NO_FLAGS);
            results.put("result2", Boolean.FALSE);
            failed2 = true;
        } catch (TransactionRequiredLocalException trle) {
            results.put("result2", Boolean.TRUE);
        } catch (Exception e) {
            results.put("result2", e);
        }

        // Ignore errors on cleanup so they don't obscure test result.
        if (failed2) {
            try {
                endpoint.endActivityTransacted(handle);
            } catch (Exception e) {
                getLog().warning("Exception caught while cleaning up failed test", e);
            }
        }

        return results;
    }

    // This will be invoked multiple times during the test for 1115263
    private HashMap test1115263(Object argument) {
        HashMap results = new HashMap();

        if ("startActivity".equals(argument)) {
            getLog().info("Creating activity in RA to prevent RA Entity from stopping");
            lastActivityHandle = new TCKActivityHandleImpl(System.currentTimeMillis(), 1115261);
            try {
                endpoint.startActivity(lastActivityHandle, lastActivityHandle);
                results.put("result", Boolean.TRUE);
            } catch (Exception e) {
                results.put("result", e);
            }
            return results;
        } else if ("endActivity".equals(argument)) {
            getLog().info("Removing activity from RA to allow RA to transition into the Inactive state");
            try {
                endpoint.endActivity(lastActivityHandle);
                results.put("result", Boolean.TRUE);
            } catch (Exception e) {
                results.put("result", e);
            }
            return results;
        }

        getLog().info("Testing that startActivityTransacted(handle, handle) throws an IllegalStateException");
        ActivityHandle handle = new TCKActivityHandleImpl(System.currentTimeMillis(), 1115263);
        boolean failed1 = false;
        try {
            endpoint.startActivityTransacted(handle, handle);
            results.put("result1", Boolean.FALSE);
            failed1 = true;
        } catch (IllegalStateException ise) {
            results.put("result1", Boolean.TRUE);
        } catch (Exception e) {
            results.put("result1", e);
        }

        if (failed1) {
            try {
                endpoint.endActivityTransacted(handle);
            } catch (Exception e) {
                getLog().warning("Exception caught while cleaning up failed test.", e);
            }
        }

        getLog().info("Testing that startActivityTransacted(handle, handle) throws an IllegalStateException");
        boolean failed2 = false;
        try {
            endpoint.startActivityTransacted(handle, handle, ActivityFlags.NO_FLAGS);
            results.put("result2", Boolean.FALSE);
            failed2 = true;
        } catch (IllegalStateException ise) {
            results.put("result2", Boolean.TRUE);
        } catch (Exception e) {
            results.put("result2", e);
        }

        if (failed2) {
            try {
                endpoint.endActivityTransacted(handle);
            } catch (Exception e) {
                getLog().warning("Exception caught while cleaning up failed test.", e);
            }
        }

        return results;
    }

    // 1115262, 1115265
    /**
     * Test to ensure that startActivityTransacted is throwing NullPointerException.
     */
    private HashMap test1115262() {
        HashMap results = new HashMap();

        // startActivityTransacted(null, null)
        getLog().info("1) Checking for expected NullPointerException");
        try {
            transactionManager.beginSleeTransaction();
            try {
                try {
                    endpoint.startActivityTransacted(null, null);
                    results.put("result1", Boolean.FALSE);
                } catch (NullPointerException npe) {
                    results.put("result1", Boolean.TRUE);
                }
            } finally {
                transactionManager.rollback();
            }
        } catch (Exception e) {
            results.put("result1", e);
        }

        // startActivityTransacted(handle, null)
        getLog().info("2) Checking for expected NullPointerException");
        TCKActivityHandleImpl handle = new TCKActivityHandleImpl(System.currentTimeMillis(), 1115262);
        try {
            transactionManager.beginSleeTransaction();
            try {
                try {
                    endpoint.startActivityTransacted(handle, null);
                    results.put("result2", Boolean.FALSE);
                } catch (NullPointerException npe) {
                    results.put("result2", Boolean.TRUE);
                }
            } finally {
                transactionManager.rollback();
            }
        } catch (Exception e) {
            results.put("result2", e);
        }

        // startActivityTransacted(null, object)
        getLog().info("3) Checking for expected NullPointerException");
        try {
            transactionManager.beginSleeTransaction();
            try {
                try {
                    endpoint.startActivityTransacted(null, new Object());
                    results.put("result3", Boolean.FALSE);
                } catch (NullPointerException npe) {
                    results.put("result3", Boolean.TRUE);
                }
            } finally {
                transactionManager.rollback();
            }
        } catch (Exception e) {
            results.put("result3", e);
        }

        // startActivityTransacted(null, null, flags)
        getLog().info("4) Checking for expected NullPointerException");
        try {
            transactionManager.beginSleeTransaction();
            try {
                try {
                    endpoint.startActivityTransacted(null, null, ActivityFlags.NO_FLAGS);
                    results.put("result4", Boolean.FALSE);
                } catch (NullPointerException npe) {
                    results.put("result4", Boolean.TRUE);
                }
            } finally {
                transactionManager.rollback();
            }
        } catch (Exception e) {
            results.put("result4", e);
        }

        // startActivityTransacted(handle, null, flags)
        getLog().info("5) Checking for expected NullPointerException");
        handle = new TCKActivityHandleImpl(System.currentTimeMillis(), 1115262);
        try {
            transactionManager.beginSleeTransaction();
            try {
                try {
                    endpoint.startActivityTransacted(handle, null, ActivityFlags.NO_FLAGS);
                    results.put("result5", Boolean.FALSE);
                } catch (NullPointerException npe) {
                    results.put("result5", Boolean.TRUE);
                }
            } finally {
                transactionManager.rollback();
            }
        } catch (Exception e) {
            results.put("result5", e);
        }

        // startActivityTransacted(null, object, flags)
        getLog().info("6) Checking for expected NullPointerException");
        try {
            transactionManager.beginSleeTransaction();
            try {
                try {
                    endpoint.startActivityTransacted(null, new Object(), ActivityFlags.NO_FLAGS);
                    results.put("result6", Boolean.FALSE);
                } catch (NullPointerException npe) {
                    results.put("result6", Boolean.TRUE);
                }
            } finally {
                transactionManager.rollback();
            }
        } catch (Exception e) {
            results.put("result6", e);
        }

        return results;
    }

    /**
     * Test to check that startActivityTransacted() is throwing ActivityAlreadyExistsException correctly.
     * <p>
     * This test covers two scenarios:<br>
     * a) Non-transacted activity exists in slee when startActivityTransacted() called, b) Transacted activity exists in
     * current transaction when startActivityTransacted() called.
     */
    private HashMap test1115265(int sequenceID) {
        HashMap results = new HashMap();

        /*
         * Non-transacted activity already exists in the SLEE.
         */
        getLog().info("1a) Checking behaviour of startActivityTransacted(handle, handle) against non-transacted activity");
        ActivityHandle handle = new TCKActivityHandleImpl(System.currentTimeMillis() + 1, 1115265, "1a");
        try {
            endpoint.startActivity(handle, handle);
            transactionManager.beginSleeTransaction();
            try {
                try {
                    // Check for ActivityAlreadyExistsException
                    endpoint.startActivityTransacted(handle, handle);
                    results.put("result1a", Boolean.FALSE);
                } catch (ActivityAlreadyExistsException aaee) {
                    results.put("result1a", Boolean.TRUE);
                } finally {
                    endpoint.endActivity(handle);
                }
            } finally {
                transactionManager.rollback();
            }
        } catch (Exception e) {
            results.put("result1a", e);
        }

        getLog().info("1b) Checking behaviour of startActivityTransacted(handle, flags) against non-transacted activity");
        handle = new TCKActivityHandleImpl(System.currentTimeMillis() + 2, 1115265, "1b");
        try {
            endpoint.startActivity(handle, handle);
            transactionManager.beginSleeTransaction();
            try {
                try {
                    // Check for ActivityAlreadyExistsException
                    endpoint.startActivityTransacted(handle, handle, EventFlags.NO_FLAGS);
                    results.put("result1b", Boolean.FALSE);
                } catch (ActivityAlreadyExistsException aaee) {
                    results.put("result1b", Boolean.TRUE);
                } finally {
                    endpoint.endActivity(handle);
                }
            } finally {
                transactionManager.rollback();
            }
        } catch (Exception e) {
            results.put("result1b", e);
        }

        /*
         * Transacted activity already exists in current transaction.
         */
        getLog().info("2a) Checking behaviour of startActivityTransacted(handle, handle) against transacted activity");
        handle = new TCKActivityHandleImpl(System.currentTimeMillis() + 3, 1115265, "2a");
        try {
            transactionManager.beginSleeTransaction();
            try {
                endpoint.startActivityTransacted(handle, handle);
                try {
                    try {
                        // Check for ActivityAlreadyExistsException
                        endpoint.startActivityTransacted(handle, handle);
                        results.put("result2a", Boolean.FALSE);
                    } catch (ActivityAlreadyExistsException aaee) {
                        results.put("result2a", Boolean.TRUE);
                    }
                } finally {
                    endpoint.endActivityTransacted(handle);
                }
            } finally {
                transactionManager.commit();
            }
        } catch (Exception e) {
            results.put("result2a", e);
        }

        getLog().info("2b) Checking behaviour of startActivityTransacted(handle, flags) against transacted activity");
        handle = new TCKActivityHandleImpl(System.currentTimeMillis() + 4, 1115265, "2b");
        // startActivityTransacted(handle, flags)
        try {
            transactionManager.beginSleeTransaction();
            try {
                endpoint.startActivityTransacted(handle, handle);
                try {
                    try {
                        // Check for ActivityAlreadyExistsException
                        endpoint.startActivityTransacted(handle, handle, ActivityFlags.NO_FLAGS);
                        results.put("result2b", Boolean.FALSE);
                    } catch (ActivityAlreadyExistsException aaee) {
                        results.put("result2b", Boolean.TRUE);
                    }
                } finally {
                    endpoint.endActivityTransacted(handle);
                }
            } finally {
                transactionManager.commit();
            }
        } catch (Exception e) {
            results.put("result2b", e);
        }

        getLog().info("3a) Checking behaviour of startActivityTransacted(handle, handle) against transacted activity from another transaction");
        handle = new TCKActivityHandleImpl(System.currentTimeMillis() + 5, 1115265, "3a");

        /*
         * Transacted activity already exists in a different transaction. Only one, not both of the transactions should
         * not commit. One of them may or may not throw an ActivityAlreadyExists exception depending on the concurrency
         * control model in use.
         */
        try {
            getLog().info("Creating transaction A");
            transactionManager.beginSleeTransaction();
            try {
                getLog().info("Starting transacted activity in A");
                try {
                    endpoint.startActivityTransacted(handle, handle);
                } catch (Exception e) {
                    getLog().info("Exception caught during activity start in transaction A: ", e);
                }
                getLog().info("Firing transacted event on A");
                try {
                    fireEventTransacted(handle, sequenceID, "A");
                } catch (Exception e) {
                    getLog().info("Exception caught during event firing in transaction A: ", e);
                }
            } finally {
                getLog().info("Suspending transaction A");
                Transaction suspended = transactionManager.suspend();
                new Thread(new Test1115265Runnable(suspended, 10000)).start();
            }

            getLog().info("Creating transaction B");
            transactionManager.beginSleeTransaction();
            try {
                try {
                    getLog().info("Starting transacted activity in B");
                    // This may block if pessimistic concurrency control is in
                    // use. If it does block, an ActivityAlreadyExists exception
                    // is expected.
                    endpoint.startActivityTransacted(handle, handle);
                    getLog().info("Firing transacted event on B");
                    fireEventTransacted(handle, sequenceID, "B");
                } catch (Exception e) {
                    getLog().info("Exception caught during test:", e);
                }
                getLog().info("Committing transaction B");
            } finally {
                transactionManager.commit();
            }
        } catch (Exception e) {
            getLog().info("Exception caught during test:", e);
        }

        try {
            endpoint.endActivity(handle);
        } catch (Exception e) {
            getLog().info("Exception caught during test cleanup:", e);
        }

        results.put("result3", Boolean.TRUE);

        return results;
    }

    private class Test1115265Runnable implements Runnable {
        public Test1115265Runnable(Transaction transaction, int sleepPeriod) {
            this.sleepPeriod = sleepPeriod;
            this.transaction = transaction;
        }

        public void run() {
            try {
                getLog().info("Resuming transaction A in " + sleepPeriod + "ms");
                Thread.sleep(sleepPeriod);
                getLog().info("Resuming transaction A");
                transactionManager.resume(transaction);
                getLog().info("Committing transaction A");
                transaction.commit();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        private final Transaction transaction;
        private final int sleepPeriod;

    }

    // 1115276 - Test that endActivity() is non-transactional.
    private HashMap test1115276() {
        HashMap results = new HashMap();

        ActivityHandle handle = new TCKActivityHandleImpl(System.currentTimeMillis(), 1115276);
        results.put("result1", Boolean.TRUE);
        try {
            endpoint.startActivity(handle, handle);

            transactionManager.beginSleeTransaction();
            try {
                // This should cause an onActivityEndEvent to be fired.
                endpoint.endActivity(handle);
            } finally {
                transactionManager.rollback();
            }

            // No cleanup here. We can't safely do a brute-force cleanup
            // because the test relies on the ActivityEndEvent being being fired
            // on the sbb. A brute-force cleanup will obviously invoke that
            // logic, making the test useless.
            //
            // TODO - This could be made smarter by having the method call block
            // until the result of the SBB event handling is determined (to
            // allow for cleanup). At least then an incorrect SLEE
            // implementation won't cause problems with the TCK.
        } catch (Exception e) {
            getLog().severe("An error occured during test for assertion 1115276: ", e);
            results.put("result1", e);
        }

        return results;
    }

    private HashMap test1115276b() {
        HashMap results = new HashMap();
        results.put("result2", Boolean.TRUE);
        return results;
    }

    private HashMap test1115277(int sequenceID) {
        HashMap results = new HashMap();

        TCKActivityHandleImpl handle = new TCKActivityHandleImpl(System.currentTimeMillis(), 1115277);
        lastActivityHandle = handle;
        results.put("result-ra1", Boolean.TRUE);
        try {
            endpoint.startActivity(handle, handle);
            fireEvent(handle, sequenceID);
        } catch (Exception e) {
            results.put("result-ra1", e);
        }
        return results;
    }

    private HashMap test1115277b() {
        HashMap results = new HashMap();

        results.put("result-ra2", Boolean.TRUE);
        try {
            getLog().info("Ending activity from RA");
            endpoint.endActivity(lastActivityHandle);
            try {
                getLog().info("Firing event from RA on ending activity");
                fireEvent(lastActivityHandle, lastSequenceID, "ra");
            } catch (ActivityIsEndingException e) {
                getLog().info("Ignoring exception thrown when trying to fire an event on ending activity", e);
            }
        } catch (Exception e) {
            results.put("result-ra2", e);
        }

        return results;
    }

    private HashMap test1115278(int sequenceID) {
        HashMap results = new HashMap();

        TCKActivityHandleImpl handle = new TCKActivityHandleImpl(System.currentTimeMillis(), 1115278);
        lastActivityHandle = handle;
        results.put("result-ra1", Boolean.TRUE);
        try {
            endpoint.startActivity(handle, handle);
            fireEvent(handle, sequenceID);
        } catch (Exception e) {
            results.put("result-ra1", e);
        }
        return results;
    }

    private HashMap test1115278b() {
        HashMap results = new HashMap();

        results.put("result-ra2", Boolean.TRUE);
        try {
            getLog().info("Ending activity from RA");
            endpoint.endActivity(lastActivityHandle);

            getLog().info("Ending activity from RA (again)");
            endpoint.endActivity(lastActivityHandle);
        } catch (Exception e) {
            results.put("result-ra2", e);
        }

        return results;
    }

    private HashMap test1115280() {
        HashMap results = new HashMap();

        try {
            endpoint.endActivity(null);
            results.put("result1", Boolean.FALSE);
        } catch (NullPointerException e) {
            results.put("result1", Boolean.TRUE);
        } catch (Exception e) {
            results.put("result1", e);
        }

        ActivityHandle handle = new TCKActivityHandleImpl(System.currentTimeMillis(), 1115280);
        try {
            endpoint.endActivity(handle);
            results.put("result2", Boolean.FALSE);
        } catch (UnrecognizedActivityHandleException e) {
            results.put("result2", Boolean.TRUE);
        } catch (Exception e) {
            results.put("result2", e);
        }

        return results;
    }

    private HashMap test1115291() {
        HashMap results = new HashMap();
        try {
            transactionManager.beginSleeTransaction();
            try {
                try {
                    endpoint.endActivityTransacted(null);
                    results.put("result1", Boolean.FALSE);
                } catch (NullPointerException e) {
                    results.put("result1", Boolean.TRUE);
                }
            } finally {
                transactionManager.rollback();
            }
        } catch (Exception e) {
            results.put("result1", e);
        }

        try {
            ActivityHandle handle = new TCKActivityHandleImpl(System.currentTimeMillis(), 1115291);
            try {
                transactionManager.beginSleeTransaction();
                try {
                    endpoint.endActivityTransacted(handle);
                    results.put("result2", Boolean.FALSE);
                } catch (UnrecognizedActivityHandleException e) {
                    results.put("result2", Boolean.TRUE);
                }
            } finally {
                transactionManager.rollback();
            }
        } catch (Exception e) {
            results.put("result2", e);
        }

        return results;
    }

    private HashMap test1115298() {
        int ASSERTION_ID = 1115298;

        HashMap results = new HashMap();

        ActivityHandle handle = new TCKActivityHandleImpl(System.currentTimeMillis(), ASSERTION_ID);
        SimpleEvent event = new SimpleEvent(ASSERTION_ID);

        try {
            EventTypeID eventTypeID = new EventTypeID(SimpleEvent.SIMPLE_EVENT_NAME, SleeTCKComponentConstants.TCK_VENDOR, "1.1");
            FireableEventType fireableEventType = getContext().getEventLookupFacility().getFireableEventType(eventTypeID);

            endpoint.startActivity(handle, handle);
            try {
                try {
                    endpoint.fireEvent(null, fireableEventType, event, null, null);
                    results.put("result1", Boolean.FALSE);
                } catch (NullPointerException npe) {
                    results.put("result1", Boolean.TRUE);
                } catch (Exception e) {
                    results.put("result1", e);
                }

                try {
                    endpoint.fireEvent(handle, fireableEventType, null, null, null);
                    results.put("result2", Boolean.FALSE);
                } catch (NullPointerException npe) {
                    results.put("result2", Boolean.TRUE);
                } catch (Exception e) {
                    results.put("result2", e);
                }

                try {
                    endpoint.fireEvent(handle, fireableEventType, null, null, null, EventFlags.NO_FLAGS);
                    results.put("result3", Boolean.FALSE);
                } catch (NullPointerException npe) {
                    results.put("result3", Boolean.TRUE);
                } catch (Exception e) {
                    results.put("result3", e);
                }

                try {
                    endpoint.fireEvent(handle, fireableEventType, null, null, null, EventFlags.NO_FLAGS);
                    results.put("result4", Boolean.FALSE);
                } catch (NullPointerException npe) {
                    results.put("result4", Boolean.TRUE);
                } catch (Exception e) {
                    results.put("result4", e);
                }
            } finally {
                endpoint.endActivity(handle);
            }
        } catch (Exception e) {
            results.put("result1", e);
        }

        return results;
    }

    private HashMap test1115299() {
        int ASSERTION_ID = 1115299;

        HashMap results = new HashMap();

        ActivityHandle handle = new TCKActivityHandleImpl(System.currentTimeMillis(), ASSERTION_ID);
        SimpleEvent event = new SimpleEvent(ASSERTION_ID);

        try {
            EventTypeID eventTypeID = new EventTypeID(SimpleEvent.SIMPLE_EVENT_NAME, SleeTCKComponentConstants.TCK_VENDOR, "1.1");
            FireableEventType fireableEventType = getContext().getEventLookupFacility().getFireableEventType(eventTypeID);

            try {
                endpoint.fireEvent(handle, fireableEventType, event, null, null);
                results.put("result1", Boolean.FALSE);
            } catch (UnrecognizedActivityHandleException e) {
                results.put("result1", Boolean.TRUE);
            } catch (Exception e) {
                results.put("result1", e);
            }

            try {
                endpoint.fireEvent(handle, fireableEventType, event, null, null, EventFlags.NO_FLAGS);
                results.put("result2", Boolean.FALSE);
            } catch (UnrecognizedActivityHandleException e) {
                results.put("result2", Boolean.TRUE);
            } catch (Exception e) {
                results.put("result2", e);
            }
        } catch (Exception e) {
            results.put("result1", e);
        }

        return results;
    }

    public class IllegalFireableEventType implements FireableEventType {
        public ClassLoader getEventClassLoader() {
            return ClassLoader.getSystemClassLoader();
        }

        public String getEventClassName() {
            return "com.opencloud.sleetck.lib.testsuite.resource.SimpleEvent";
        }

        public EventTypeID getEventType() {
            return new EventTypeID(SimpleEvent.SIMPLE_EVENT_NAME, SleeTCKComponentConstants.TCK_VENDOR, "1.1");
        }
    }

    private HashMap test1115300() {
        int ASSERTION_ID = 1115300;

        HashMap results = new HashMap();

        ActivityHandle handle = new TCKActivityHandleImpl(System.currentTimeMillis(), ASSERTION_ID);
        SimpleEvent event = new SimpleEvent(ASSERTION_ID);

        try {
            EventTypeID eventTypeID = new EventTypeID(SimpleEvent.SIMPLE_EVENT_NAME, SleeTCKComponentConstants.TCK_VENDOR, "1.1");
            IllegalFireableEventType illegalEventType = new IllegalFireableEventType();

            endpoint.startActivity(handle, handle);
            try {
                // Invalid FireableEventType
                try {
                    endpoint.fireEvent(handle, illegalEventType, event, null, null);
                    results.put("result1", Boolean.FALSE);
                } catch (IllegalEventException e) {
                    results.put("result1", Boolean.TRUE);
                } catch (Exception e) {
                    results.put("result1", e);
                }

                // Invalid FireableEventType
                try {
                    endpoint.fireEvent(handle, illegalEventType, event, null, null, EventFlags.NO_FLAGS);
                    results.put("result2", Boolean.FALSE);
                } catch (IllegalEventException e) {
                    results.put("result2", Boolean.TRUE);
                } catch (Exception e) {
                    results.put("result2", e);
                }
            } finally {
                endpoint.endActivity(handle);
            }
        } catch (Exception e) {
            results.put("result1", e);
        }

        return results;
    }

    private HashMap test1115303(int sequenceID) {
        int ASSERTION_ID = 1115303;

        HashMap results = new HashMap();
        lastActivityHandle = new TCKActivityHandleImpl(System.currentTimeMillis(), ASSERTION_ID);
        ActivityHandle handle = lastActivityHandle;

        results.put("result1", Boolean.TRUE);
        try {
            EventTypeID eventTypeID = new EventTypeID(SimpleEvent.SIMPLE_EVENT_NAME, SleeTCKComponentConstants.TCK_VENDOR, "1.1");
            FireableEventType fireableEventType = getContext().getEventLookupFacility().getFireableEventType(eventTypeID);

            SimpleEvent event = new SimpleEvent(sequenceID);

            endpoint.startActivity(handle, handle);
            try {
                endpoint.fireEvent(handle, fireableEventType, event, null, null);
            } finally {
                endpoint.endActivity(handle);
            }
        } catch (Exception e) {
            results.put("result1", e);
        }

        return results;
    }

    private HashMap test1115303b() {
        HashMap results = new HashMap();

        SimpleEvent dummyEvent = new SimpleEvent(-1);
        EventTypeID eventTypeID = new EventTypeID(SimpleEvent.SIMPLE_EVENT_NAME, SleeTCKComponentConstants.TCK_VENDOR, "1.1");

        try {
            FireableEventType fireableEventType = getContext().getEventLookupFacility().getFireableEventType(eventTypeID);
            try {
                endpoint.fireEvent(lastActivityHandle, fireableEventType, dummyEvent, null, null);
                results.put("result3", Boolean.FALSE);
            } catch (ActivityIsEndingException e) {
                getLog().info("fireEvent() correctly caused an ActivityIsEndingException to be thrown");
                results.put("result3", Boolean.TRUE);
            }
        } catch (Exception e) {
            results.put("result3", e);
        }

        return results;
    }

    private HashMap test1115296(int sequenceID) {
        int ASSERTION_ID = 1115296;

        HashMap results = new HashMap();

        ActivityHandle handle = new TCKActivityHandleImpl(System.currentTimeMillis(), ASSERTION_ID);
        EventTypeID eventTypeID = new EventTypeID(SimpleEvent.SIMPLE_EVENT_NAME, SleeTCKComponentConstants.TCK_VENDOR, "1.1");

        results.put("result1", Boolean.TRUE);

        try {
            FireableEventType fireableEventType = getContext().getEventLookupFacility().getFireableEventType(eventTypeID);

            endpoint.startActivity(handle, handle);
            try {
                // Fire a number of events at the activity and check that they
                // are delivered in the correct order.
                for (int i = 1; i <= 42; i++) {
                    SimpleEvent event = new SimpleEvent(sequenceID, new Integer(i));
                    endpoint.fireEvent(handle, fireableEventType, event, null, null);
                }
            } finally {
                endpoint.endActivity(handle);
            }
        } catch (Exception e) {
            results.put("result1", e);
        }

        return results;
    }

    // 1115306 - Check that transacted events are only delivered if transaction
    // commits.
    private HashMap test1115306(int sequenceID) {
        HashMap results = new HashMap();

        ActivityHandle handle = new TCKActivityHandleImpl(System.currentTimeMillis(), 1115306);
        results.put("result-ra", Boolean.TRUE);
        try {
            endpoint.startActivity(handle, handle);
            try {
                getLog().info("Starting transaction A");
                transactionManager.beginSleeTransaction();
                try {
                    getLog().info("Firing transacted event");
                    fireEventTransacted(handle, sequenceID, "A");
                } finally {
                    getLog().info("Committing transaction A");
                    transactionManager.commit();
                }

                getLog().info("Starting transaction B");
                transactionManager.beginSleeTransaction();
                try {
                    getLog().info("Firing transacted event");
                    fireEventTransacted(handle, sequenceID, "B");
                } finally {
                    getLog().info("Rolling back transaction B");
                    transactionManager.rollback();
                }
            } finally {
                endpoint.endActivity(handle);
            }
        } catch (Exception e) {
            results.put("result-ra", e);
        }
        return results;
    }

    private HashMap test1115308(int sequenceID) {
        int ASSERTION_ID = 1115308;

        HashMap results = new HashMap();

        ActivityHandle handle = new TCKActivityHandleImpl(System.currentTimeMillis(), ASSERTION_ID);
        EventTypeID eventTypeID = new EventTypeID(SimpleEvent.SIMPLE_EVENT_NAME, SleeTCKComponentConstants.TCK_VENDOR, "1.1");

        results.put("result1", Boolean.TRUE);

        try {
            FireableEventType fireableEventType = getContext().getEventLookupFacility().getFireableEventType(eventTypeID);

            endpoint.startActivity(handle, handle);
            try {
                transactionManager.beginSleeTransaction();
                try {
                    // Fire a number of events at the activity and check that
                    // they are delivered in the correct order.
                    for (int i = 1; i <= 42; i++) {
                        SimpleEvent event = new SimpleEvent(sequenceID, new Integer(i));
                        endpoint.fireEventTransacted(handle, fireableEventType, event, null, null);
                    }
                } finally {
                    transactionManager.commit();
                }
            } finally {
                endpoint.endActivity(handle);
            }
        } catch (Exception e) {
            results.put("result1", e);
        }

        return results;
    }

    // 1115309
    private HashMap test1115309() {
        HashMap results = new HashMap();

        ActivityHandle handle = new TCKActivityHandleImpl(System.currentTimeMillis(), 1115309);
        try {
            endpoint.startActivity(handle, handle);
            try {
                fireEventTransacted(handle, 0);
                results.put("result", Boolean.FALSE);
            } catch (TransactionRequiredLocalException trle) {
                results.put("result", Boolean.TRUE);
            } finally {
                endpoint.endActivity(handle);
            }
        } catch (Exception e) {
            results.put("result", e);
        }

        return results;
    }

    // 1115311
    private HashMap test1115311() {
        HashMap results = new HashMap();

        ActivityHandle handle = new TCKActivityHandleImpl(System.currentTimeMillis(), 1115311);

        try {
            EventTypeID eventTypeID = new EventTypeID(SimpleEvent.SIMPLE_EVENT_NAME, SleeTCKComponentConstants.TCK_VENDOR, "1.1");
            FireableEventType fireableEventType = getContext().getEventLookupFacility().getFireableEventType(eventTypeID);

            transactionManager.beginSleeTransaction();
            try {
                endpoint.startActivity(handle, handle);
                try {
                    try {
                        endpoint.fireEventTransacted(handle, fireableEventType, null, null, null);
                        results.put("result1", Boolean.FALSE);
                    } catch (NullPointerException npe) {
                        results.put("result1", Boolean.TRUE);
                    } catch (Exception e) {
                        results.put("result1", e);
                    }

                    try {
                        endpoint.fireEventTransacted(null, fireableEventType, new SimpleEvent(0), null, null);
                        results.put("result2", Boolean.FALSE);
                    } catch (NullPointerException npe) {
                        results.put("result2", Boolean.TRUE);
                    } catch (Exception e) {
                        results.put("result2", e);
                    }
                } finally {
                    endpoint.endActivity(handle);
                }
            } finally {
                transactionManager.commit();
            }
        } catch (Exception e) {
            results.put("result1", e);
        }

        return results;
    }

    // 1115312
    private HashMap test1115312() {
        HashMap results = new HashMap();

        ActivityHandle handle = new TCKActivityHandleImpl(System.currentTimeMillis(), 1115312, "(1115312a) valid activity handle");
        ActivityHandle badHandle = new TCKActivityHandleImpl(System.currentTimeMillis(), 987654321, "(1115312a) invalid activity handle");

        try {
            EventTypeID eventTypeID = new EventTypeID(SimpleEvent.SIMPLE_EVENT_NAME, SleeTCKComponentConstants.TCK_VENDOR, "1.1");
            FireableEventType fireableEventType = getContext().getEventLookupFacility().getFireableEventType(eventTypeID);

            transactionManager.beginSleeTransaction();
            try {
                endpoint.startActivity(handle, handle);
                try {
                    try {
                        endpoint.fireEventTransacted(badHandle, fireableEventType, new SimpleEvent(0), null, null);
                        results.put("result1", Boolean.FALSE);
                    } catch (UnrecognizedActivityHandleException uahe) {
                        results.put("result1", Boolean.TRUE);
                    } catch (Exception e) {
                        results.put("result1", e);
                    }
                } finally {
                    endpoint.endActivity(handle);
                }
            } finally {
                transactionManager.commit();
            }
        } catch (Exception e) {
            results.put("result1", e);
        }

        handle = new TCKActivityHandleImpl(System.currentTimeMillis() + 1, 1115312, "(1115312b) valid activity handle");
        badHandle = new TCKActivityHandleImpl(System.currentTimeMillis() + 1, 987654321, "(1115312b) invalid activity handle");

        try {
            EventTypeID eventTypeID = new EventTypeID(SimpleEvent.SIMPLE_EVENT_NAME, SleeTCKComponentConstants.TCK_VENDOR, "1.1");
            FireableEventType fireableEventType = getContext().getEventLookupFacility().getFireableEventType(eventTypeID);

            transactionManager.beginSleeTransaction();
            try {
                endpoint.startActivity(handle, handle);
                try {
                    try {
                        endpoint.fireEventTransacted(badHandle, fireableEventType, new SimpleEvent(1), null, null, EventFlags.NO_FLAGS);
                        results.put("result2", Boolean.FALSE);
                    } catch (UnrecognizedActivityHandleException uahe) {
                        results.put("result2", Boolean.TRUE);
                    } catch (Exception e) {
                        results.put("result2", e);
                    }
                } finally {
                    endpoint.endActivity(handle);
                }
            } finally {
                transactionManager.commit();
            }
        } catch (Exception e) {
            results.put("result2", e);
        }

        return results;
    }

    private HashMap test1115313() {
        int ASSERTION_ID = 1115313;

        HashMap results = new HashMap();

        ActivityHandle handle = new TCKActivityHandleImpl(System.currentTimeMillis(), ASSERTION_ID);
        SimpleEvent event = new SimpleEvent(ASSERTION_ID);

        try {
            EventTypeID eventTypeID = new EventTypeID(SimpleEvent.SIMPLE_EVENT_NAME, SleeTCKComponentConstants.TCK_VENDOR, "1.1");

            IllegalFireableEventType illegalEventType = new IllegalFireableEventType();

            endpoint.startActivity(handle, handle);
            transactionManager.beginSleeTransaction();
            try {
                try {
                    try {
                        endpoint.fireEventTransacted(handle, illegalEventType, event, null, null);
                        results.put("result1", Boolean.FALSE);
                    } catch (IllegalEventException e) {
                        results.put("result1", Boolean.TRUE);
                    } catch (Exception e) {
                        results.put("result1", e);
                    }

                    try {
                        endpoint.fireEventTransacted(handle, illegalEventType, event, null, null, EventFlags.NO_FLAGS);
                        results.put("result2", Boolean.FALSE);
                    } catch (IllegalEventException e) {
                        results.put("result2", Boolean.TRUE);
                    } catch (Exception e) {
                        results.put("result2", e);
                    }
                } finally {
                    transactionManager.commit();
                }
            } finally {
                endpoint.endActivity(handle);
            }
        } catch (Exception e) {
            results.put("result1", e);
        }

        return results;
    }

    private HashMap test1115314() {
        int ASSERTION_ID = 1115314;

        HashMap results = new HashMap();

        ActivityHandle handle = new TCKActivityHandleImpl(System.currentTimeMillis(), ASSERTION_ID);
        CustomEventObject event = new CustomEventObject(ASSERTION_ID);

        try {
            EventTypeID eventTypeID = new EventTypeID(SimpleEvent.SIMPLE_EVENT_NAME, SleeTCKComponentConstants.TCK_VENDOR, "1.1");
            FireableEventType fireableEventType = getContext().getEventLookupFacility().getFireableEventType(eventTypeID);

            endpoint.startActivity(handle, handle);
            transactionManager.beginSleeTransaction();
            try {
                try {
                    // Fire event with a bogus event object (i.e. one which is
                    // not a
                    // SimpleEvent).
                    try {
                        endpoint.fireEvent(handle, fireableEventType, event, null, null);
                        results.put("result1", Boolean.FALSE);
                    } catch (IllegalEventException e) {
                        results.put("result1", Boolean.TRUE);
                    } catch (Exception e) {
                        results.put("result1", e);
                    }

                    try {
                        endpoint.fireEvent(handle, fireableEventType, event, null, null, EventFlags.NO_FLAGS);
                        results.put("result2", Boolean.FALSE);
                    } catch (IllegalEventException e) {
                        results.put("result2", Boolean.TRUE);
                    } catch (Exception e) {
                        results.put("result2", e);
                    }
                } finally {
                    transactionManager.commit();
                }
            } finally {
                endpoint.endActivity(handle);
            }
        } catch (Exception e) {
            results.put("result1", e);
        }

        return results;
    }

    private HashMap test1115316(int sequenceID) {
        int ASSERTION_ID = 1115316;

        HashMap results = new HashMap();
        lastActivityHandle = new TCKActivityHandleImpl(System.currentTimeMillis(), ASSERTION_ID);
        ActivityHandle handle = lastActivityHandle;

        results.put("result1", Boolean.TRUE);
        try {
            EventTypeID eventTypeID = new EventTypeID(SimpleEvent.SIMPLE_EVENT_NAME, SleeTCKComponentConstants.TCK_VENDOR, "1.1");
            FireableEventType fireableEventType = getContext().getEventLookupFacility().getFireableEventType(eventTypeID);

            SimpleEvent event = new SimpleEvent(sequenceID);

            endpoint.startActivity(handle, handle);
            try {
                transactionManager.beginSleeTransaction();
                try {
                    endpoint.fireEventTransacted(handle, fireableEventType, event, null, null);
                } finally {
                    transactionManager.commit();
                }
            } finally {
                endpoint.endActivity(handle);
            }
        } catch (Exception e) {
            results.put("result1", e);
        }

        return results;
    }

    private HashMap test1115316b() {
        HashMap results = new HashMap();

        SimpleEvent dummyEvent = new SimpleEvent(-1);
        EventTypeID eventTypeID = new EventTypeID(SimpleEvent.SIMPLE_EVENT_NAME, SleeTCKComponentConstants.TCK_VENDOR, "1.1");

        try {
            FireableEventType fireableEventType = getContext().getEventLookupFacility().getFireableEventType(eventTypeID);
            try {
                endpoint.fireEventTransacted(lastActivityHandle, fireableEventType, dummyEvent, null, null);
                results.put("result3", Boolean.FALSE);
            } catch (ActivityIsEndingException e) {
                getLog().info("fireEvent() correctly caused an ActivityIsEndingException to be thrown");
                results.put("result3", Boolean.TRUE);
            }
        } catch (Exception e) {
            results.put("result3", e);
        }

        return results;
    }

    // suspendActivity - mandatory transactional testing
    private HashMap test1115417() {
        int ASSERTION_ID = 1115417;
        HashMap results = new HashMap();

        TCKActivityHandleImpl handle = new TCKActivityHandleImpl(System.currentTimeMillis(), ASSERTION_ID);
        try {
            endpoint.startActivity(handle, handle);
            try {
                getLog().info("(1) Checking suspendActivity() is mandatory transactional.");
                endpoint.suspendActivity(handle);
                results.put("result1", Boolean.FALSE);
                getLog().severe("(1) suspendActivity() did not throw TransactionRequiredLocalException outside a transactional context");
            } catch (TransactionRequiredLocalException e) {
                getLog().info("(1) suspendActivity() correctly threw TransactionRequiredLocalException outside a transactional context");
                results.put("result1", Boolean.TRUE);
            }
        } catch (Exception e) {
            results.put("result", e);
        } finally {
            try {
                endpoint.endActivity(handle);
            } catch (Exception e) {
                getLog().severe("An error occured during test cleanup - could not end activity: " + handle);
            }
        }

        return results;
    }

    private HashMap test1115413(int sequenceID) {
        int ASSERTION_ID = 1115413;
        HashMap results = new HashMap();

        TCKActivityHandleImpl handle = new TCKActivityHandleImpl(System.currentTimeMillis(), ASSERTION_ID);
        try {
            endpoint.startActivity(handle, handle);
            EventTypeID eventTypeID = new EventTypeID(SimpleEvent.SIMPLE_EVENT_NAME, SleeTCKComponentConstants.TCK_VENDOR, "1.1");
            FireableEventType fireableEventType = getContext().getEventLookupFacility().getFireableEventType(eventTypeID);
            SimpleEvent event = new SimpleEvent(sequenceID, "from RA");
            endpoint.fireEvent(handle, fireableEventType, event, null, null);
            results.put("result-ra1", Boolean.TRUE);
        } catch (Exception e) {
            results.put("result-ra1", e);
        } finally {
            endpoint.endActivity(handle);
        }

        return results;
    }

    // startActivitySuspended - mandatory transactional testing
    private HashMap test1115420() {
        int ASSERTION_ID = 1115420;
        HashMap results = new HashMap();

        TCKActivityHandleImpl handle = new TCKActivityHandleImpl(System.currentTimeMillis(), ASSERTION_ID, "(1)");

        boolean started = false; // Used for attempting cleanup in error case
        getLog().info("(1) Checking startActivitySuspended() is mandatory transactional.");
        try {
            endpoint.startActivitySuspended(handle, handle);
            started = true;
            getLog().severe("(1) startActivitySuspended() did not throw TransactionRequiredLocalException outside of a transactional context");
            results.put("result1", Boolean.FALSE);
        } catch (TransactionRequiredLocalException e) {
            getLog().info("(1) startActivitySuspended() correctly threw TransactionRequiredLocalException outside of a transactional context");
            results.put("result1", Boolean.TRUE);
        } catch (Exception e) {
            getLog().info("(1) startActivitySuspended() threw an unexpected exception outside of a transactional context");
            results.put("result1", e);
        } finally {
            if (started) {
                try {
                    endpoint.endActivity(handle);
                } catch (Exception e) {
                    getLog().warning("(1) An exception was thrown trying to clean up test state.", e);
                }
            }
        }

        handle = new TCKActivityHandleImpl(System.currentTimeMillis() + 1, ASSERTION_ID, "(2)");
        try {
            getContext().getSleeTransactionManager().begin();

            getLog().info("(2) Checking startActivitySuspended() is mandatory transactional.");
            started = false;
            try {
                endpoint.startActivitySuspended(handle, handle);
                started = true;
                getLog().info("(2) startActivitySuspended() correctly did not throw a TransactionRequiredLocalException inside of a transactional context");
                results.put("result2", Boolean.TRUE);
            } catch (TransactionRequiredLocalException e) {
                getLog().info("(2) startActivitySuspended() incorrectly threw a TransactionRequiredLocalException inside of a transactional context");
                results.put("result2", Boolean.FALSE);
            } catch (Exception e) {
                getLog().info("(2) startActivitySuspended() threw an unexpected exception inside of a transactional context");
                results.put("result2", e);
            } finally {
                if (started) {
                    try {
                        endpoint.endActivity(handle);
                    } catch (Exception e) {
                        getLog().warning("(2) An exception was thrown trying to clean up test state.", e);
                    }
                }
            }
            getContext().getSleeTransactionManager().rollback();
        } catch (Exception e) {
            getLog().warning("(2) Unexpected exeption thrown during test", e);
            results.put("result2", e);
            return results;
        }

        return results;
    }

    // startActivitySuspended behavioural test
    private HashMap test1115418(int sequenceID) {
        int ASSERTION_ID = 1115418;
        HashMap results = new HashMap();

        TCKActivityHandleImpl handle = new TCKActivityHandleImpl(System.currentTimeMillis(), ASSERTION_ID);
        try {
            endpoint.startActivity(handle, handle);
            EventTypeID eventTypeID = new EventTypeID(SimpleEvent.SIMPLE_EVENT_NAME, SleeTCKComponentConstants.TCK_VENDOR, "1.1");
            FireableEventType fireableEventType = getContext().getEventLookupFacility().getFireableEventType(eventTypeID);
            SimpleEvent event = new SimpleEvent(sequenceID, "from RA");
            endpoint.fireEvent(handle, fireableEventType, event, null, null);
            results.put("result-ra1", Boolean.TRUE);
        } catch (Exception e) {
            results.put("result-ra1", e);
        } finally {
            endpoint.endActivity(handle);
        }

        return results;
    }

    // Custom event object used by test for assertion 1115301 and 1115314
    class CustomEventObject {
        public CustomEventObject(int assertionID) {
            this.assertionID = assertionID;
        }

        public String toString() {
            return "CustomEventObject[" + assertionID + "]";
        }

        private final int assertionID;
    }

    private int lastSequenceID = 0;
    private TCKActivityHandleImpl lastActivityHandle = null;

    private SleeEndpoint endpoint;
    private SleeTransactionManager transactionManager;
    private boolean isConfigured = false;
    private MessageHandler messageHandler = null;
}
