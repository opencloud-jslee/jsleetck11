/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.facilities.profilefacility;

import javax.slee.ActivityEndEvent;
import javax.slee.ActivityContextInterface;
import javax.slee.SbbContext;
import javax.slee.Address;
import javax.slee.SbbLocalObject;
import javax.slee.facilities.Level;
import javax.slee.ChildRelation;
import javax.slee.CreateException;
import javax.slee.profile.*;
import javax.slee.InvalidArgumentException;

import com.opencloud.sleetck.lib.sbbutils.BaseTCKSbb;
import com.opencloud.sleetck.lib.sbbutils.TCKSbbUtils;
import com.opencloud.sleetck.lib.sbbutils.TCKSbbConstants;
import com.opencloud.sleetck.lib.resource.events.TCKResourceEventX;
import com.opencloud.sleetck.lib.resource.sbbapi.TCKResourceSbbInterface;

import java.util.HashMap;
import java.util.Iterator;

import javax.naming.*;

import javax.slee.facilities.ActivityContextNamingFacility;
import javax.slee.nullactivity.NullActivity;
import javax.slee.nullactivity.NullActivityFactory;
import javax.slee.nullactivity.NullActivityContextInterfaceFactory;

public abstract class Test1397Sbb extends BaseTCKSbb {

    private static final String JNDI_PROFILEFACILITY_NAME = "java:comp/env/slee/facilities/profile";
    public static final String PROFILE_TABLE_NAME = "Test1397ProfileTable";
    public static final String PROFILE_NAME = "Test1397Profile";

    // Initial event
    public void onTCKResourceEventX1(TCKResourceEventX ev, ActivityContextInterface aci) {

        try {
            HashMap map = new HashMap();

            ProfileFacility facility = (ProfileFacility) new InitialContext().lookup(JNDI_PROFILEFACILITY_NAME);

            // 1397
            ProfileTableActivity activity = facility.getProfileTableActivity(PROFILE_TABLE_NAME);
            if (activity == null) {
                map.put("Result", new Boolean(false));
                map.put("Message", "Invalid activity returned from ProfileFacility.getProfileTableActivity()");
                map.put("ID", new Integer(1397));
                TCKSbbUtils.getResourceInterface().sendSbbMessage(map);
                return;
            }
            
            // 1398
            boolean passed = false;
            try {
                activity = facility.getProfileTableActivity("Foo");
            } catch (javax.slee.profile.UnrecognizedProfileTableNameException e) {
                passed = true;
            }

            if (!passed) {
                map.put("Result", new Boolean(false));
                map.put("Message", "ProfileFacility.getProfileTableActivity(InvalidProfileTableName) should have thrown UnrecognizedProfileTableNameException.");
                map.put("ID", new Integer(1398));
                TCKSbbUtils.getResourceInterface().sendSbbMessage(map);
                return;
            }
                
            map.put("Result", new Boolean(true));
            map.put("Message", "Ok");
            TCKSbbUtils.getResourceInterface().sendSbbMessage(map);

        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }

    }

    public abstract Test1397ProfileCMP getProfileCMP(ProfileID profileID) throws UnrecognizedProfileTableNameException, UnrecognizedProfileNameException;

}
