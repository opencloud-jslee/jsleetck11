/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.resource.endpoint;

import java.util.HashMap;

import javax.slee.ActivityContextInterface;
import javax.slee.ActivityEndEvent;
import javax.slee.Address;
import javax.slee.resource.ActivityIsEndingException;

import com.opencloud.sleetck.lib.rautils.UOID;
import com.opencloud.sleetck.lib.testsuite.resource.BaseResourceSbb;
import com.opencloud.sleetck.lib.testsuite.resource.RAMethods;
import com.opencloud.sleetck.lib.testsuite.resource.SimpleEvent;

public abstract class Test1115277Sbb extends BaseResourceSbb {

    private static final int ASSERTION_ID = 1115277;

    // Event handlers
    public void onSimpleEvent(SimpleEvent event, ActivityContextInterface aci) {
        tracer.info("onSimpleEvent() event handler called with event: " + event);
        
        int sequenceID = event.getSequenceID();
        Object payload = event.getPayload();

        HashMap results = new HashMap();
        if ("sbb".equals(payload)) {
            results.put("result-illegal-sbb", Boolean.TRUE);
            sendSbbMessage(sbbUID, sequenceID, RAMethods.endActivity, results);
            return;
        } else if ("ra".equals(payload)) {
            results.put("result-illegal-ra", Boolean.TRUE);
            sendSbbMessage(sbbUID, sequenceID, RAMethods.endActivity, results);
            return;
        }

        tracer.info("Setting CMP field with initial event");
        setInitialEvent(event);

        // End activity and attempt to fire an event from RA.
        try {
            getSbbInterface().executeTestLogic(new Integer(ASSERTION_ID));

            tracer.info("Checking ending state of Activity Context");
            // Check ending state
            if (aci.isEnding()) {
                tracer.info("Activity Context is correctly ending.");
                results.put("result-sbb1", Boolean.TRUE);
            } else {
                tracer.info("Activity Context is incorrectly not ending.");
                results.put("result-sbb1", Boolean.FALSE);
            }

            // Try to fire an event (from the SBB). This should not get
            // delivered.
            try {
                fireSimpleEvent(new SimpleEvent(sequenceID, "sbb"), aci, null);
            } catch (IllegalStateException e) {
                tracer.info("Ignoring exception thrown when trying to fire an event on ending activity", e);
            }
        } catch (Exception e) {
            results.put("result-sbb1", e);
        }

        sendSbbMessage(sbbUID, sequenceID, RAMethods.endActivity, results);
    }

    public abstract void fireSimpleEvent(SimpleEvent event, ActivityContextInterface aci, Address address);

    public void onActivityEndEvent(ActivityEndEvent event, ActivityContextInterface aci) {
        HashMap results = new HashMap();
        tracer.info("onActivityEnd() event handler called with event: " + event);

        tracer.info("Getting initial event from CMP field");
        SimpleEvent initialEvent = getInitialEvent();
        int sequenceID = initialEvent.getSequenceID();

        if (sequenceID == -1)
            return;

        results.put("result-sbb2", Boolean.TRUE);
        sendSbbMessage(sbbUID, sequenceID, RAMethods.endActivity, results);
    }

    // CMP field 'InitialEvent' of type SimpleEvent
    public abstract SimpleEvent getInitialEvent();
    public abstract void setInitialEvent(SimpleEvent event);

    private static final UOID sbbUID = UOID.createUOID();
}
