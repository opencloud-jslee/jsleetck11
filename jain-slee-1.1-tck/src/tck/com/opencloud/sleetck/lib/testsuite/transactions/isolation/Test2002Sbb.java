/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.transactions.isolation;

import com.opencloud.sleetck.lib.resource.events.TCKResourceEventX;
import com.opencloud.sleetck.lib.resource.events.TCKResourceEventY;
import com.opencloud.sleetck.lib.sbbutils.BaseTCKSbb;
import com.opencloud.sleetck.lib.sbbutils.TCKSbbUtils;

import javax.slee.ActivityContextInterface;
import javax.slee.InitialEventSelector;
import javax.slee.facilities.Level;
import java.util.HashMap;
import java.util.Map;

public abstract class Test2002Sbb extends BaseTCKSbb {
    /**
     * Initialise the value for the test CMP field.  The initial value is taken from the events message argument.
     * @param event
     * @param aci
     */
    public void onTCKResourceEventX1(TCKResourceEventX event, ActivityContextInterface aci) {
        createTrace("X1: enter");
        setValue((String) event.getMessage());
        handleEvent("X1", event.getMessage());
        createTrace("X1: exit");
    }

    /**
     * Change the value of the test CMP field to a new value supplied in the events message argument then perform a
     * blocking callback to the test.
     * @param event
     * @param aci
     */
    public void onTCKResourceEventX2(TCKResourceEventX event, ActivityContextInterface aci) {
        createTrace("X2: enter");
        setValue((String) event.getMessage());
        handleEvent("X2", event.getMessage());      // will block
        createTrace("X2: exit");
    }

    /**
     * Simply callback to the test.  The test will fire this event on an activity to guarantee that the previous txn
     * on this activity is committed (or rolled back) before un-blocking a current transaction on a second activity
     * @param event
     * @param aci
     */
    public void onTCKResourceEventY1(TCKResourceEventY event, ActivityContextInterface aci) {
        createTrace("Y1: enter");
        handleEvent("Y1", event.getMessage());
        createTrace("Y1: exit");
    }

    /**
     * Retrieves the value of the test CMP field and passes it back to the test twice.  On the first callback to the
     * test the test will block until the transaction modifying the CMP value commits (unblocking X2), so this event handler
     * will check the value before and after commit of the other transaction.
     * @param event
     * @param aci
     */
    public void onTCKResourceEventY2(TCKResourceEventY event, ActivityContextInterface aci) {
        createTrace("Y2: enter");
        String value;
        value = getValue();
        createTrace("Y2: value  (first time): "+value);
        handleEvent("Y2", value);

        value = getValue();
        createTrace("Y2: value (second time): "+value);
        handleEvent("Y2", value);
        createTrace("Y2: exit");
    }

    public void sbbRolledBack(javax.slee.RolledBackContext context) {
        // no-op: disable throwing an exception when the Sbb is rolled back
    }

    public InitialEventSelector initialEventSelectorMethod(InitialEventSelector ies) {
        ies.setCustomName("foo");
        return ies;
    }

    // CMP abstract methods
    public abstract String getValue();
    public abstract void setValue(String value);

    // Private methods
    private void handleEvent(String handlerName, Object value) {
        try {
            Map args = new HashMap();
            args.put(IsolationTestConstants.SBB_EVENT_HANDLER_FIELD, handlerName);
            args.put(IsolationTestConstants.VALUE_FIELD, value);

            createTrace("Sending response: " + args.toString());
            TCKSbbUtils.getResourceInterface().callTest(args);
        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }
    }

    protected void createTrace(String trace) {
        try {
            TCKSbbUtils.createTrace(getSbbID(), Level.INFO, transactionId() + ": " + trace,  null);
        } catch (Exception e) {}
    }

    private String transactionId() {
        try {
            return TCKSbbUtils.getResourceAdaptorInterface().getTransactionIDAccess().getCurrentTransactionID().toString();
        } catch (Exception e) {
            return null;
        }
    }
}
