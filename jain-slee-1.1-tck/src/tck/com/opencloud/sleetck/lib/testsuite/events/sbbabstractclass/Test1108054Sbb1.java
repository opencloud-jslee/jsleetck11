/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.events.sbbabstractclass;

import java.util.HashMap;

import javax.slee.ActivityContextInterface;
import javax.slee.Address;
import javax.slee.EventContext;
import javax.slee.ServiceID;
import javax.slee.facilities.Tracer;

import com.opencloud.sleetck.lib.resource.events.TCKResourceEventX;
import com.opencloud.sleetck.lib.sbbutils.BaseTCKSbb;
import com.opencloud.sleetck.lib.sbbutils.TCKSbbUtils;

/**
 * AssertionID(1108054): Test An additional fire event method has been 
 * added. This method allows the SBB to fire an event to a particular 
 * target Service on an Activity Context.
 */
public abstract class Test1108054Sbb1 extends BaseTCKSbb {

    // Initial event method.
    public void onTCKResourceEventX1(TCKResourceEventX event, ActivityContextInterface aci, EventContext context) {
        try {
            tracer = getSbbContext().getTracer("com.test");
            tracer.info("Sbb1: Received a TCK event " + event);
            setTestName((String) event.getMessage());

            Test1108054Event testEvent = new Test1108054Event();

            setServiceID(getSbbContext().getService());
            tracer.info("The particular target ServiceID is: " + getServiceID());

            //1108181:event_class==null;
            boolean passed1108181 = false;
            try {
                fireTest1108054Event(null, aci, null, getServiceID());
            } catch (java.lang.NullPointerException e) {
                tracer.info("got expected NullPointerException when the event class is null", null);
                passed1108181 = true;
            }

            if (!passed1108181) {
                sendResultToTCK(1108181, "fireEventName(null, ActivityContextInterface, Address, ServiceID) "
                        + "should have thrown java.lang.NullPointerException.", "Test1108054Test", false);
                return;
            }

            //1108181:ActivityContextInterface==null;
            passed1108181 = false;
            try {
                fireTest1108054Event(testEvent, null, null, getServiceID());
            } catch (java.lang.NullPointerException e) {
                tracer.info("got expected NullPointerException when the ActivityContextInterface is null", null);
                passed1108181 = true;
            }

            if (!passed1108181) {
                sendResultToTCK(1108181, "fireEventName(EventClass, null, Address, ServiceID) "
                        + "should have thrown java.lang.NullPointerException.", "Test1108054Test", false);
                return;
            }

            if (getTestName().equals("SERVICEID")) {
                fireTest1108054Event(testEvent, aci, null, getServiceID());
            }
            else if (getTestName().equals("NULLSERVICEID")) {
                fireTest1108054Event(testEvent, aci, null, null);
            }
            else {
                tracer.severe("Unexpected test name encountered during X1 event handler: " + getTestName());
                return;
            }
            
        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }
    }

    public void onTest1108054Event(Test1108054Event event, ActivityContextInterface aci, EventContext context) {
        try {
            tracer = getSbbContext().getTracer("com.test");
            tracer.info("Sbb1: Received a custom event " + event);
            if (getTestName().equals("SERVICEID"))
                setPassed(true);
            else if (getTestName().equals("NULLSERVICEID"))
                sendResultToTCK(1108054, "Sbb1: SBB entities belonging to all Services are eligible to receive the event."
                        + "Sbb1 received the custom event " + event, "Test1108054Test", true);
            else {
                tracer.severe("Unexpected test name encountered during X1 event handler: " + getTestName());
                return;
            }

        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }

    }

    public void onTCKResourceEventX2(TCKResourceEventX event, ActivityContextInterface aci, EventContext context) {
        try {
            tracer = getSbbContext().getTracer("com.test");
            tracer.info("Sbb1: Received a TCK event " + event);

            if (getTestName().equals("SERVICEID")) {
                if (!getPassed())
                    sendResultToTCK(1108054, "Sbb1 fired an event to a particular target Service " + getServiceID() + ", "
                            + "but no event received in this Service.", "Test1108054Test", false);
                else
                    sendResultToTCK(1108054, "The test of this method allows the SBB to fire an event to a particular"
                            + "target Service " + getServiceID() + " on an Activity Context passed.", "Test1108054Test", true);
                return;
            }
        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }
    }

    public abstract void fireTest1108054Event(Test1108054Event event, ActivityContextInterface aci, Address address,
            ServiceID serviceID);

    private void sendResultToTCK(int failedAssertionID, String message, String testName, boolean result) throws Exception {
        HashMap sbbData = new HashMap();
        sbbData.put("testname", testName);
        sbbData.put("result", result ? "pass" : "fail");
        sbbData.put("message", message);
        sbbData.put("id", new Integer(failedAssertionID));
        TCKSbbUtils.getResourceInterface().sendSbbMessage(sbbData);
    }

    public abstract void setTestName(String testName);
    public abstract String getTestName();

    public abstract void setServiceID(ServiceID serviceID);
    public abstract ServiceID getServiceID();

    public abstract void setPassed(boolean passed);
    public abstract boolean getPassed();

    private Tracer tracer;
}
