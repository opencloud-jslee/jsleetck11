/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.events.FireEventNotReady;

import com.opencloud.sleetck.lib.sbbutils.BaseTCKSbb;
import com.opencloud.sleetck.lib.sbbutils.TCKSbbUtils;
import com.opencloud.sleetck.lib.sbbutils.events.TCKSbbEventImpl;
import com.opencloud.sleetck.lib.sbbutils.events.TCKSbbEvent;

import javax.naming.InitialContext;
import javax.slee.ActivityContextInterface;
import javax.slee.facilities.ActivityContextNamingFacility;
import javax.slee.facilities.Level;

public abstract class AssertionSbb extends BaseTCKSbb {

    public void sbbCreate() throws javax.slee.CreateException {
        try{
            ActivityContextNamingFacility acNaming = (ActivityContextNamingFacility)new InitialContext().lookup("java:comp/env/slee/facilities/activitycontextnaming");
            ActivityContextInterface aci = acNaming.lookup("name");
            fireFooEvent( new TCKSbbEventImpl(new Integer(6)), aci, null );
            sendTestFail();
        }catch( IllegalStateException ise ){
            sendTestSuccess();
        }catch( Exception e ){
            throw new RuntimeException( e.getMessage() );
        }

    }

    private void sendTestSuccess(){
        sendBooleanMessage( true );
    }

    private void sendTestFail(){
        sendBooleanMessage(false);
    }

    private void sendBooleanMessage(boolean bool) {
        try {
            TCKSbbUtils.createTrace(getSbbID(),Level.FINE,"sending a boolean message back with value: " + bool,null);
            // send a reply: the value of the boolean message
            Boolean message = new Boolean( bool );
            TCKSbbUtils.getResourceInterface().sendSbbMessage(message);
        } catch(Exception e) {
            TCKSbbUtils.handleException(e);
        }
    }

    public abstract void fireFooEvent( TCKSbbEvent event , ActivityContextInterface aci, javax.slee.Address addr );
}
