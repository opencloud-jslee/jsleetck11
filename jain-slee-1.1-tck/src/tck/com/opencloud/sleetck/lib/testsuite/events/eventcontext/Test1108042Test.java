/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.events.eventcontext;

import com.opencloud.sleetck.lib.*;
import com.opencloud.sleetck.lib.testutils.*;
import com.opencloud.sleetck.lib.resource.TCKSbbMessage;
import com.opencloud.sleetck.lib.resource.testapi.TCKResourceListener;
import com.opencloud.sleetck.lib.resource.TCKActivityID;
import com.opencloud.sleetck.lib.resource.testapi.TCKResourceTestInterface;
import com.opencloud.sleetck.lib.resource.events.TCKResourceEventX;

import java.rmi.RemoteException;
import java.util.Map;

/**
 * AssertionID(1108042): Test The event processing by the SLEE for this event is
 * resumed if the transaction which invoked this method commits.
 */
public class Test1108042Test extends AbstractSleeTCKTest {

    public static final String SERVICE1_DU_PATH_PARAM = "service1DUPath";

    public static final String SERVICE2_DU_PATH_PARAM = "service2DUPath";

    public static final String activityNameA = "Test1108042Test-ActivityA";

    public static final String activityNameB = "Test1108042Test-ActivityB";
    
    private static final int defaultTimeout = 25000;
    
    /**
     * Perform the actual test.
     */

    public void run(FutureResult result) throws Exception {
        this.result = result;

        TCKResourceTestInterface resource = utils().getResourceInterface();
        TCKActivityID activityA = resource.createActivity(activityNameA);
        TCKActivityID activityB = resource.createActivity(activityNameB);

        // TCK test starts activityA and fires event TCKResourceEventX1
        getLog().fine("Firing TCKResourceEventX1 on Test1108042Test-ActivityA.");
        resource.fireEvent(TCKResourceEventX.X1, testName, activityA, null);

        synchronized (this) {
            wait(2000);
        }

        // TCK test starts activityA and activityB and fires event
        // TCKResourceEventX2
        getLog().fine("Firing TCKResourceEventX2 on Test1108042Test-ActivityA and Test1108042Test-ActivityB.");
        resource.fireEvent(TCKResourceEventX.X2, testName, activityA, null);
        resource.fireEvent(TCKResourceEventX.X2, testName, activityB, null);

        synchronized (this) {
            wait(2000);
        }

        getLog().fine("Firing TCKResourceEventX3 on Test1108042Test-ActivityA and Test1108042Test-ActivityB.");
        resource.fireEvent(TCKResourceEventX.X3, testName, activityA, null);
        resource.fireEvent(TCKResourceEventX.X3, testName, activityB, null);

        synchronized (this) {
            wait(defaultTimeout);
        }

    }

    /**
     * Do all the pre-run configuration of the test.
     */

    public void setUp() throws Exception {
        getLog().fine("Connecting to resource");
        resourceListener = new TCKResourceListenerImpl();
        setResourceListener(resourceListener);
        getLog().fine("Installing and activating service");

        setupService(SERVICE1_DU_PATH_PARAM, true);
        setupService(SERVICE2_DU_PATH_PARAM, true);
    }

    /**
     * Clean up after the test.
     */

    public void tearDown() throws Exception {
        // cleanup
        super.tearDown();
    }

    private class TCKResourceListenerImpl extends BaseTCKResourceListener {
        // TCKResourceListener methods

        public synchronized void onSbbMessage(TCKSbbMessage message, TCKActivityID calledActivity)
                throws RemoteException {
            Map sbbData = (Map) message.getMessage();
            String sbbTestName = "Test1108042Test";
            String sbbTestResult = (String) sbbData.get("result");
            String sbbTestMessage = (String) sbbData.get("message");
            int assertionID = ((Integer) sbbData.get("id")).intValue();
            getLog().info(
                    "Received message from SBB: testname=" + sbbTestName + ", result=" + sbbTestResult + ", message="
                            + sbbTestMessage + ", id=" + assertionID);
            try {
                Assert.assertEquals(assertionID, "Test " + sbbTestName + " failed.", "pass", sbbTestResult);
                result.setPassed();
            } catch (TCKTestFailureException ex) {
                result.setFailed(assertionID, sbbTestMessage);
            }
        }

        public void onException(Exception exception) throws RemoteException {
            getLog().warning("Received Exception from SBB or resource:");
            getLog().warning(exception);
            result.setError(exception);
        }
    }

    private TCKResourceListener resourceListener;

    private FutureResult result;

    private final static String testName = "Test1108042";
}
