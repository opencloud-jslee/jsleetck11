/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.transactions;

import javax.slee.ActivityEndEvent;
import javax.slee.ActivityContextInterface;
import javax.slee.SbbContext;
import javax.slee.Address;
import javax.slee.SbbLocalObject;
import javax.slee.facilities.Level;
import javax.slee.ChildRelation;
import javax.slee.CreateException;
import javax.slee.TransactionRolledbackLocalException;

import com.opencloud.sleetck.lib.TCKTestErrorException;
import com.opencloud.sleetck.lib.sbbutils.BaseTCKSbb;
import com.opencloud.sleetck.lib.sbbutils.TCKSbbUtils;
import com.opencloud.sleetck.lib.sbbutils.TCKSbbConstants;
import com.opencloud.sleetck.lib.resource.events.TCKResourceEventX;
import com.opencloud.sleetck.lib.resource.sbbapi.TCKResourceSbbInterface;

import java.util.HashMap;
import java.util.Iterator;

public abstract class Test896ChildSbb extends BaseTCKSbb {

    public static final String MESSAGE = "Test896RuntimeException";

    public void sbbExceptionThrown(Exception exception, Object event, ActivityContextInterface aci) {
        //        if (exception instanceof java.lang.RuntimeException && exception.getMessage().equals(MESSAGE)) {
        try {
            TCKSbbUtils.createTrace(getSbbID(), Level.FINE, "In child's sbbExceptionThrown(): " + exception.getClass().toString(), null);
        } catch (Exception e) {
        }

        if (exception instanceof java.lang.RuntimeException) {
            setExceptionSeen(true);
            return;
        }

        super.sbbExceptionThrown(exception, event, aci);
    }

    public void sbbRolledBack(javax.slee.RolledBackContext context) {
    }

    public void throwRuntimeException() {
        throw new RuntimeException(MESSAGE);
    }

    public boolean isExceptionSeen() {
        return getExceptionSeen();
    }

    public abstract void setExceptionSeen(boolean seen);
    public abstract boolean getExceptionSeen();

}
