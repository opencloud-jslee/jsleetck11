/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.facilities.timerfacility;

import com.opencloud.sleetck.lib.sbbutils.BaseTCKSbb;
import com.opencloud.sleetck.lib.sbbutils.TCKSbbUtils;
import com.opencloud.sleetck.lib.resource.events.TCKResourceEventX;
import com.opencloud.sleetck.lib.resource.sbbapi.TCKActivity;

import javax.naming.Context;
import javax.naming.InitialContext;

import java.util.HashMap;

import javax.slee.*;
import javax.slee.facilities.*;
import javax.slee.nullactivity.*;

/**
 * Test TimerFacility.setTimer() periodic timer method
 */
public abstract class SetTimerPeriodicSbb extends BaseTCKSbb {

    public void setSbbContext(SbbContext context) {
        super.setSbbContext(context);
        try {
            Context myEnv = (Context) new InitialContext().lookup("java:comp/env");

            timerFacility = (TimerFacility) myEnv.lookup("slee/facilities/timer");
        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }
    }

    // test starts here...
    public void onTCKResourceEventX1(TCKResourceEventX event, ActivityContextInterface aci) {
        try {
            TCKSbbUtils.createTrace(getSbbID(),Level.INFO,"Received start message",null);
            setTestName((String)event.getMessage());

            // use ALL to ensure we receive all events.
            TimerOptions preserveAll = new TimerOptions();
            preserveAll.setPreserveMissed(TimerPreserveMissed.ALL);

            // this timer should fire straight away since startTime < currentTime
            if (getTestName().equals("setTimerPeriodic1")) {
                timerFacility.setTimer(aci, null, 0, 5000, 1, preserveAll);
            }
            // this timer should fire after 5s (can't really test timing though, just make sure
            // it fires at all!)
            else if (getTestName().equals("setTimerPeriodic2")) {
                timerFacility.setTimer(aci, null, System.currentTimeMillis() + 5000, 2000, 1, preserveAll);
            }
            // test for infitely repeating timers - well make sure it does at least 2 reps anyway
            else if (getTestName().equals("setTimerPeriodic3")) {
                timerFacility.setTimer(aci, null, System.currentTimeMillis() + 5000, 2000, 0, preserveAll);
            }
            // test for finite repeating timers - 2 reps
            else if (getTestName().equals("setTimerPeriodic4")) {
                timerFacility.setTimer(aci, null, System.currentTimeMillis() + 5000, 2000, 2, preserveAll);
            }

        } catch(Exception e) {
            TCKSbbUtils.handleException(e);
        }
    }

    public void onTimerEvent(TimerEvent event, ActivityContextInterface aci) {
        try {
            TCKSbbUtils.createTrace(getSbbID(),Level.INFO,"Received timer event",null);

            setNumTimerEvents(getNumTimerEvents() + 1);

            boolean finished = false;

            if (getTestName().equals("setTimerPeriodic1") || getTestName().equals("setTimerPeriodic2")) {
                setResultPassed("setTimer() periodic timer fired correctly");
                finished = true;
            }
            else {
                if (getNumTimerEvents() == 2) {
                    setResultPassed("setTimer() periodic timer fired correctly");
                    finished = true;
                }
            }

            // NB: no failed case - if timer never fires then JavaTest will timeout and
            // fail the test.

            // we are done
            if (finished) sendResultToTCK();

        } catch(Exception e) {
            TCKSbbUtils.handleException(e);
        }
    }

    public abstract void setTestName(String testName);
    public abstract String getTestName();

    public abstract void setNumTimerEvents(int numTimerEvents);
    public abstract int getNumTimerEvents();

    private void sendResultToTCK() throws Exception {
        HashMap sbbData = new HashMap();
        sbbData.put("testname", getTestName());
        sbbData.put("result", result?"pass":"fail");
        sbbData.put("message", message);
        sbbData.put("id", new Integer(failedAssertionID));
        TCKSbbUtils.getResourceInterface().sendSbbMessage(sbbData);
    }

    private void setResultFailed(int assertionID, String msg) throws Exception {
        result = false;
        failedAssertionID = assertionID;
        message =  "Assertion " + assertionID + " failed: " + msg;
        TCKSbbUtils.createTrace(getSbbID(), Level.WARNING, message, null);
    }

    private void setResultPassed(String msg) throws Exception {
        result = true;
        message = "Success: " + msg;
        TCKSbbUtils.createTrace(getSbbID(), Level.INFO, message, null);
    }

    private TimerFacility timerFacility;

    private boolean result = false;
    private String message;
    private int failedAssertionID = -1;

}
