/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.javax.slee.SbbLocalObject.remove;

import javax.slee.ActivityEndEvent;
import javax.slee.ActivityContextInterface;
import javax.slee.Address;
import javax.slee.SbbLocalObject;
import javax.slee.facilities.Level;
import javax.slee.ChildRelation;

import com.opencloud.sleetck.lib.sbbutils.BaseTCKSbb;
import com.opencloud.sleetck.lib.sbbutils.TCKSbbUtils;
import com.opencloud.sleetck.lib.sbbutils.TCKSbbConstants;
import com.opencloud.sleetck.lib.resource.events.TCKResourceEventX;
import com.opencloud.sleetck.lib.resource.TCKActivityID;
import com.opencloud.sleetck.lib.resource.sbbapi.TCKActivity;
import com.opencloud.sleetck.lib.resource.sbbapi.TCKResourceSbbInterface;
import com.opencloud.sleetck.lib.resource.sbbapi.TCKActivityContextInterfaceFactory;

import java.util.HashMap;

public abstract class Test3318SecondChildSbb extends BaseTCKSbb {

    public void onTest3318EventTwo(Test3318EventTwo event, ActivityContextInterface aci) {

        try {
            HashMap map = new HashMap();
            map.put("Result", new Boolean(true));
            map.put("Message", "Ok");
            TCKSbbUtils.getResourceInterface().sendSbbMessage(map);
        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }
    }

}
