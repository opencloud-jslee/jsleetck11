/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.management.ManagementMBean;

import com.opencloud.sleetck.lib.AbstractSleeTCKTest;
import com.opencloud.sleetck.lib.TCKTestErrorException;
import com.opencloud.sleetck.lib.TCKTestResult;

import javax.slee.management.DeployableUnitID;

public class Test1575Test extends AbstractSleeTCKTest {

    private static final String SERVICE_DU_PATH_PARAM = "serviceDUPath";
    private static final int TEST_ID = 1575;

    /**
     * Perform the actual test.
     */
    public TCKTestResult run() throws Exception {

        DeployableUnitID duID;
        // Install the Deployable Unit.
        String duPath = utils().getTestParams().getProperty(SERVICE_DU_PATH_PARAM);

        try {
            duID = utils().install(duPath);
        } catch (TCKTestErrorException e) {
            if (e.getEnclosedException().getClass().equals(javax.slee.management.DeploymentException.class)) {
                return TCKTestResult.passed();
            } else {
                throw (e);
            }
        }

        return TCKTestResult.failed(TEST_ID,
                "Succeeded in installing a service whose root SBB was not already installed, " +
                "and wasn't in the service's deployable unit. DeployableUnitID:"+duID);
    }

    // Empty implementation
    public void setUp() throws Exception {}

}
