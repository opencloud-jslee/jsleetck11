/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.resource.context;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.HashMap;
import java.util.Timer;

import javax.slee.EventTypeID;
import javax.slee.ServiceID;
import javax.slee.facilities.AlarmFacility;
import javax.slee.facilities.AlarmLevel;
import javax.slee.facilities.EventLookupFacility;
import javax.slee.facilities.Tracer;
import javax.slee.profile.ProfileTable;
import javax.slee.profile.UnrecognizedProfileTableNameException;
import javax.slee.resource.ActivityHandle;
import javax.slee.resource.ConfigProperties;
import javax.slee.resource.FireableEventType;
import javax.slee.resource.ResourceAdaptorContext;
import javax.slee.resource.ResourceAdaptorID;
import javax.slee.resource.ResourceAdaptorTypeID;
import javax.slee.resource.SleeEndpoint;
import javax.slee.transaction.SleeTransactionManager;
import javax.slee.usage.UnrecognizedUsageParameterSetNameException;

import com.opencloud.sleetck.lib.infra.SleeTCKComponentConstants;
import com.opencloud.sleetck.lib.rautils.BaseTCKRA;
import com.opencloud.sleetck.lib.rautils.MessageHandler;
import com.opencloud.sleetck.lib.rautils.MessageHandlerRegistry;
import com.opencloud.sleetck.lib.rautils.TCKRAUtils;
import com.opencloud.sleetck.lib.resource.adaptor11.TCKActivityHandleImpl;
import com.opencloud.sleetck.lib.testsuite.resource.RAMethods;
import com.opencloud.sleetck.lib.testsuite.resource.SimpleEvent;
import com.opencloud.sleetck.lib.testsuite.resource.SimpleProfileTable;
import com.opencloud.sleetck.lib.testsuite.resource.SimpleSbbInterface;
import com.opencloud.sleetck.lib.testsuite.resource.SimpleUsageParameterInterface;
import com.opencloud.sleetck.lib.testsuite.resource.TCKMessage;

/**
 * SLEE 1.1 Endpoint testing Resource Adaptor
 */
public class ContextResourceAdaptor extends BaseTCKRA implements SimpleSbbInterface {

    //
    // Resource Adaptor
    //

    public void raConfigure(ConfigProperties properties) {
        isConfigured = true;
    }

    public void raUnconfigure() {
        isConfigured = false;
    }

    public void setResourceAdaptorContext(ResourceAdaptorContext context) {
        super.setResourceAdaptorContext(context);
        setTracer(context.getTracer("Context RA"));
        try {
            MessageHandlerRegistry registry = TCKRAUtils.lookupMessageHandlerRegistry();
            messageHandler = new ContextMessageListener(this);
            registry.registerMessageHandler(messageHandler);
        } catch (Exception e) {
            getLog().severe("An error occured during setResourceAdaptorContext()", e);
        }
    }

    public void unsetResourceAdaptorContext() {
        try {
            MessageHandlerRegistry registry = TCKRAUtils.lookupMessageHandlerRegistry();
            if (messageHandler != null)
                registry.unregisterMessageHandler(messageHandler);
        } catch (Exception e) {
            getLog().severe("An error occured during unsetResourceAdaptorContext()", e);
        }
        setTracer(null);
        super.unsetResourceAdaptorContext();
    }

    //
    // Private
    //

    private boolean isConfigured() {
        return isConfigured;
    }

    private void sendContextMessage(int sequenceID, int method, Object argument) {
        sendMessage(new TCKMessage(getRAUID(), sequenceID, method, argument));
    }

    private ResourceAdaptorContext getContext() {
        return getResourceAdaptorContext();
    }

    private class ContextMessageListener extends UnicastRemoteObject implements MessageHandler {

        public ContextMessageListener(ContextResourceAdaptor ra) throws RemoteException {
            super();
            this.ra = ra;
        }

        public boolean handleMessage(Object obj) throws RemoteException {
            if (!isConfigured())
                return false;
            ra.getLog().info("Received message from test: " + obj.toString());
            TCKMessage message = (TCKMessage) obj;
            int sequenceID = message.getSequenceID();

            HashMap results = new HashMap();
            switch (message.getMethod()) {
            case RAMethods.getEntityName:
                String entityName = null;
                entityName = ra.getContext().getEntityName();
                results.put("result", entityName);
                sendContextMessage(sequenceID, RAMethods.getEntityName, results);
                break;
            case RAMethods.getSleeEndpoint:
                results.put("result", Boolean.TRUE);
                try {
                    SleeEndpoint endpoint = ra.getContext().getSleeEndpoint();
                    if (endpoint == null)
                        results.put("result", "ResourceAdaptorContext.getSleeEndpoint() returned null");
                } catch (Exception e) {
                    results.put("result", e);
                }
                sendContextMessage(sequenceID, RAMethods.getSleeEndpoint, results);
                break;
            case RAMethods.getAlarmFacility:
                results.put("result", Boolean.TRUE);
                try {
                    AlarmFacility alarmFacility = ra.getContext().getAlarmFacility();
                    if (alarmFacility == null)
                        results.put("result", Boolean.FALSE);
                    else {
                        // alarmType, instanceID, level, message
                        String alarmID = alarmFacility.raiseAlarm("TestAlarm", "1115179", AlarmLevel.CRITICAL, "Alarm message");
                    }
                } catch (Exception e) {
                    results.put("result", e);
                }
                sendContextMessage(sequenceID, RAMethods.getAlarmFacility, results);
                break;
            case RAMethods.getTracer:
                results.put("result1", Boolean.TRUE);
                try {
                    Tracer tracer = ra.getContext().getTracer("ContextResourceAdaptor Test Tracer");
                    if (tracer == null)
                        results.put("result1", "ResourceAdaptorContext.getTracer() returned null");
                } catch (Exception e) {
                    results.put("result1", e);
                }

                try {
                    // This should generate a NPE
                    Tracer tracer = ra.getContext().getTracer(null);
                } catch (Exception e) {
                    results.put("result2", e);
                }

                try {
                    // This should generate an IllegalArgumentException
                    Tracer tracer = ra.getContext().getTracer("com..mycompany");
                } catch (Exception e) {
                    results.put("result3", e);
                }

                sendContextMessage(sequenceID, RAMethods.getTracer, results);
                break;
            case RAMethods.getEventLookupFacility:
                results.put("result", Boolean.TRUE);
                try {
                    // This should generate a NPE
                    EventLookupFacility elf = ra.getContext().getEventLookupFacility();
                    if (elf == null)
                        results.put("result", "ResourceAdaptorContext.getEventLookupFacility() returned null");
                } catch (Exception e) {
                    results.put("result", e);
                }
                sendContextMessage(sequenceID, RAMethods.getEventLookupFacility, results);
                break;
            case RAMethods.getSleeTransactionManager:
                results.put("result", Boolean.TRUE);
                try {
                    // This should generate a NPE
                    SleeTransactionManager stm = ra.getContext().getSleeTransactionManager();
                    if (stm == null)
                        results.put("result", "ResourceAdaptorContext.getSleeTransactionManager() returned null");
                } catch (Exception e) {
                    results.put("result", e);
                }
                sendContextMessage(sequenceID, RAMethods.getSleeTransactionManager, results);
                break;
            case RAMethods.getTimer:
                results.put("result1", Boolean.TRUE);
                Timer t = null;
                try {
                    t = ra.getContext().getTimer();
                    if (t == null)
                        results.put("result", "ResourceAdaptorContext.getTimer() returned null");
                } catch (Exception e) {
                    results.put("result1", e);
                }

                if (t != null) {
                    results.put("result2", Boolean.TRUE);
                    try {
                        t.cancel();
                        results.put("result2", Boolean.FALSE);
                    } catch (UnsupportedOperationException uoe) {
                    } catch (Exception e) {
                        results.put("result2", e);
                    }
                }
                sendContextMessage(sequenceID, RAMethods.getTimer, results);
                break;
            case RAMethods.getProfileTable:
                results.put("result1", Boolean.TRUE);
                ProfileTable table = null;
                try {
                    table = ra.getContext().getProfileTable("TCKTestTable");
                    if (table == null)
                        results.put("result", "ResourceAdaptorContext.getProfileTable(\"TestTableName\") returned null");
                } catch (Exception e) {
                    results.put("result1", e);
                }

                // Check typecasting works correctly.
                if (table != null) {
                    results.put("result2", Boolean.TRUE);
                    try {
                        SimpleProfileTable spt = (SimpleProfileTable) table;
                    } catch (Exception e) {
                        results.put("result2",
                                "ProfileTable retuned by ResourceAdaptorContext.getProfileTable(\"TestTableName\") could not be typecast correctly");
                    }
                }

                // Check NullPointerException is thrown
                results.put("result3", Boolean.TRUE);
                table = null;
                try {
                    table = ra.getContext().getProfileTable(null);
                    results.put("result3", Boolean.FALSE);
                } catch (NullPointerException e) {
                } catch (Exception e) {
                    results.put("result3", e);
                }

                // Check UnrecognizedProfileTableNameException is thrown
                results.put("result4", Boolean.TRUE);
                table = null;
                try {
                    table = ra.getContext().getProfileTable("ATableWhichDoesNotExist");
                    results.put("result4", Boolean.FALSE);
                } catch (UnrecognizedProfileTableNameException e) {
                } catch (Exception e) {
                    results.put("result4", e);
                }

                sendContextMessage(sequenceID, RAMethods.getProfileTable, results);
                break;
            case RAMethods.getDefaultUsageParameterSet:
                results.put("result1", Boolean.TRUE);
                Object dups = null;
                try {
                    dups = ra.getContext().getDefaultUsageParameterSet();
                    if (dups == null)
                        results.put("result1", "ResourceAdaptorContext.getDefaultUsageParameterSet() returned null");
                } catch (Exception e) {
                    results.put("result1", e);
                }

                // Typecast
                if (dups != null) {
                    results.put("result2", Boolean.TRUE);
                    try {
                        SimpleUsageParameterInterface supt = (SimpleUsageParameterInterface) dups;
                    } catch (Exception e) {
                        results.put("result2", e);
                    }
                }

                sendContextMessage(sequenceID, RAMethods.getDefaultUsageParameterSet, results);
                break;
            case RAMethods.getUsageParameterSet:
                results.put("result1", Boolean.TRUE);

                Object ups = null;
                try {
                    ups = ra.getContext().getUsageParameterSet("TCKTestUsageParameterSet");
                    if (ups == null)
                        results.put("result1", "ResourceAdaptorContext.getDefaultUsageParameterSet() returned null");
                } catch (Exception e) {
                    results.put("result1", e);
                }

                // Typecast
                if (ups != null) {
                    results.put("result2", Boolean.TRUE);
                    try {
                        SimpleUsageParameterInterface supt = (SimpleUsageParameterInterface) ups;
                    } catch (Exception e) {
                        results.put("result2", e);
                    }
                }

                // NullPointerException
                results.put("result3", Boolean.TRUE);
                try {
                    ups = ra.getContext().getUsageParameterSet(null);
                    results.put("result3", Boolean.FALSE);
                } catch (NullPointerException e) {
                } catch (Exception e) {
                    results.put("result3", e);
                }

                // UnrecognizedUsageParameterSetNameException
                results.put("results4", Boolean.TRUE);
                try {
                    ups = ra.getContext().getUsageParameterSet("AParameterSetWhichDoesNotExist");
                    results.put("results4", Boolean.FALSE);
                } catch (UnrecognizedUsageParameterSetNameException uupsne) {
                } catch (Exception e) {
                    results.put("results4", e);
                }

                sendContextMessage(sequenceID, RAMethods.getUsageParameterSet, results);
                break;
            case RAMethods.getInvokingService:
                Integer assertion = (Integer) message.getArgument();
                int value = assertion != null ? assertion.intValue() : -1;
                if (value == 1115206) {
                    results.put("result", Boolean.TRUE);
                    try {
                        ServiceID serviceID = ra.getContext().getInvokingService();
                        if (serviceID != null)
                            results.put("result", serviceID);
                    } catch (Exception e) {
                        results.put("result", e);
                    }
                    sendContextMessage(sequenceID, RAMethods.getInvokingService, results);
                } else if (value == 1115205) {
                    try {
                        // Fire event to force service invocation
                        EventTypeID eventTypeID = new EventTypeID(SimpleEvent.SIMPLE_EVENT_NAME, SleeTCKComponentConstants.TCK_VENDOR, "1.1");
                        FireableEventType fireableEventType = ra.getContext().getEventLookupFacility().getFireableEventType(eventTypeID);
                        SleeEndpoint endpoint = ra.getContext().getSleeEndpoint();
                        ActivityHandle handle = new TCKActivityHandleImpl(System.currentTimeMillis(), 0);
                        endpoint.startActivity(handle, handle);
                        endpoint.fireEvent(handle, fireableEventType, new SimpleEvent(sequenceID), null, null);
                        endpoint.endActivity(handle);
                    } catch (Exception e) {
                        ra.getLog().severe("An error occured firing SimpleEvent: ", e);
                        results.put("result2", e);
                        sendContextMessage(sequenceID, RAMethods.getInvokingService, results);
                    }
                } else {
                    ra.getLog().severe("TCK sent garbage to ContextResourceAdaptor while testing getInvokingService: argument=" + assertion);
                }
                break;
            case RAMethods.getResourceAdaptor:
                // test getResourceAdaptor returns correct ID
                // 1115406
                ResourceAdaptorID expectedID = new ResourceAdaptorID("TCK_Context_Test_RA", "jain.slee.tck", "1.1");
                ResourceAdaptorID returnedID = getContext().getResourceAdaptor();

                if (expectedID.equals(returnedID)) {
                    ra.getLog().info("getResourceAdaptor() returned expected Resource Adaptor ID.");
                    results.put("result1", Boolean.TRUE);
                } else {
                    ra.getLog().severe("getResourceAdaptor() returned unexpected Resource Adaptor ID - test failure.");
                    results.put("result1", Boolean.FALSE);
                }
                sendContextMessage(sequenceID, RAMethods.getResourceAdaptor, results);
                break;

            case RAMethods.getResourceAdaptorTypes:
                ResourceAdaptorTypeID expectedType = new ResourceAdaptorTypeID("TCK_Simple_Resource_Type", "jain.slee.tck", "1.1");
                ResourceAdaptorTypeID[] types = getContext().getResourceAdaptorTypes();

                // 1115407
                ra.getLog().info("Checking getResourceAdaptorTypes() returned a non-null value.");
                if (types == null) {
                    ra.getLog().severe("getResourceAdaptorTypes() returned a null value - test failure");
                    results.put("result1", Boolean.FALSE);
                    sendContextMessage(sequenceID, RAMethods.getResourceAdaptorTypes, results);
                    break;
                } else {
                    ra.getLog().info("getResourceAdaptorTypes() returned a non-null value");
                    results.put("result1", Boolean.TRUE);
                }

                // 1115408
                ra.getLog().info("Checking getResourceAdaptorTypes() returned expected value.");
                results.put("result2", Boolean.FALSE);
                for (int i = 0; i < types.length; i++) {
                    ra.getLog().info("Checking RA type: " + types[i]);
                    if (expectedType.equals(types[i])) {
                        ra.getLog().info("Expected type found.");
                        results.put("result2", Boolean.TRUE);
                    } else {
                        ra.getLog().severe("Unexpected RA type found - test failure.");
                        results.put("result2", Boolean.FALSE);
                        break;
                    }
                }
                sendContextMessage(sequenceID, RAMethods.getResourceAdaptorTypes, results);
                break;
            }

            return true;
        }

        private transient ContextResourceAdaptor ra = null;
    }

    private boolean isConfigured = false;
    private MessageHandler messageHandler = null;
}
