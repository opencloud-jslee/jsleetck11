/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.javax.slee.ComponentID;

import com.opencloud.sleetck.lib.AbstractSleeTCKTest;
import com.opencloud.sleetck.lib.DescriptionKeys;
import com.opencloud.sleetck.lib.TCKTestResult;
import com.opencloud.sleetck.lib.testutils.jmx.DeploymentMBeanProxy;

import javax.slee.ComponentID;
import javax.slee.management.DeployableUnitDescriptor;
import javax.slee.management.DeployableUnitID;

public class Test3063Test extends AbstractSleeTCKTest {

    private static final int TEST_ID = 3063;

    /**
     * Perform the actual test.
     */
    public TCKTestResult run() throws Exception {

        DeploymentMBeanProxy duProxy = utils().getDeploymentMBeanProxy();
        DeployableUnitDescriptor duDesc = duProxy.getDescriptor(duID);
        ComponentID components[] = duDesc.getComponents();

        if (components.length < 2) {
            return TCKTestResult.error("Number of installed components incorrect.");
        }

        if (components[0].equals(components[1])) {
            return TCKTestResult.failed(TEST_ID, "ComponentID.equals() returned true for non-equal components.");
        }

        if (!components[0].equals(components[0])) {
            TCKTestResult.failed(TEST_ID, "ComponentID.equals() returned false for equal components.");
        }

        if (components[0].getClass().getMethod("equals", new Class[] {Object.class}) == Object.class.getMethod("equals", new Class[] {Object.class})) {
                return TCKTestResult.failed(3062, "ComponentID.equals does not override Object.equals");
        }      
        
        return TCKTestResult.passed();
    }

    public void setUp() throws Exception {
        duID = setupService(DescriptionKeys.SERVICE_DU_PATH_PARAM, true);
    }

    private DeployableUnitID duID;

}
