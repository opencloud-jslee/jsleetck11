/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.facilities.tracefacility.tracer;

import javax.slee.ActivityContextInterface;

import com.opencloud.sleetck.lib.resource.events.TCKResourceEventX;
import com.opencloud.sleetck.lib.sbbutils.BaseTCKSbb;

/*
 * AssertionID (1113111): Test Tracer name components must be at least one character 
 * in length. This means that the tracer name “com.foo” is a legal name,
 * 
 * AssertionID (1113428): Test whereas “com..foo” is not, as the middle name component 
 * of this name is zero-length.
 * 
 * AssertionID (1113112): Test The empty string, “”, denotes the root tracer. The root 
 * tracer sits at the top of a tracer hierarchy and is the ancestor of all tracers. 
 * The root tracer always exists and always has an assigned trace level. The default 
 * trace level for all root tracers is TraceLevel.OFF.
 */
public abstract class Test1113111Sbb extends BaseTCKSbb {

    // Initial event
    public void onTCKResourceEventX1(TCKResourceEventX ev, ActivityContextInterface aci) {
    }

    public void sbbRolledBack(javax.slee.RolledBackContext context) {
    }
}
