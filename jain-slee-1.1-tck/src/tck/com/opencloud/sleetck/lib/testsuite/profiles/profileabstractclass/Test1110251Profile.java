/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.profiles.profileabstractclass;

import javax.slee.CreateException;
import javax.slee.profile.Profile;
import javax.slee.profile.ProfileContext;
import javax.slee.profile.ProfileVerificationException;

/**
 *
 */
public abstract class Test1110251Profile implements ProfileAbstractClassTestsProfileCMP, Profile  {

    public Test1110251Profile() {
        this.isDefaultProfile = false;
    }

    public void setProfileContext(ProfileContext context) {
        this.context = context;
    }

    public void unsetProfileContext() {
        this.context = null;
    }

    public void profileInitialize() {
        isDefaultProfile = true;
    }

    public void profilePostCreate() throws CreateException {
        //to prove that this method runs in the same TXN CXT as
        //the caller, set a CMP value and mark the context associated
        //with this method for rollback

        if (isDefaultProfile)
            return;

        String profileName= context.getProfileName();

        if (Test1110251Sbb.RAISE_EXCEPTION_PROFILE.equals(profileName))
            throw new CreateException("Testexception.");

        if (Test1110251Sbb.PROFILE_NAME.equals(profileName))
            context.setRollbackOnly();

        setValue("42");
    }

    public void profileActivate() {
    }

    public void profilePassivate() {
    }

    public void profileLoad() {
    }

    public void profileStore() {
        //1110275: profileStore is invoked with a TXN context
        String value = getValue();
        context.getTracer("profileStore").info("Value is: "+value);

        if ("Rollback".equals(value)) {
            context.setRollbackOnly();
            context.getTracer("profileStore").info("Marked TXN for rollback.");
        }

    }

    public void profileRemove() {
        context.getTracer("profileRemove").info("profileRemove called.");
        String profileName = context.getProfileName();

        //1110266: This method is invoked with a transaction context...
        if (Test1110251Sbb.ROLLBACK_REMOVE_PROFILE.equals(profileName))
            context.setRollbackOnly();

    }

    public void profileVerify() throws ProfileVerificationException {
        String profileName= context.getProfileName();

        if (Test1110251Sbb.VERIFY_EXCEPTION_PROFILE.equals(profileName))
            throw new ProfileVerificationException("Testexception.");
    }

    public boolean business() {
        return "42".equals(getValue());
    }

    public void business2() {

    }

    private ProfileContext context;
    private boolean isDefaultProfile;
}
