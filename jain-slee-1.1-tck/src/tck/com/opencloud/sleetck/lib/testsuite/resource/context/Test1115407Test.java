/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.resource.context;

import java.util.HashMap;

import com.opencloud.sleetck.lib.TCKTestResult;
import com.opencloud.sleetck.lib.testsuite.resource.BaseResourceTest;
import com.opencloud.sleetck.lib.testsuite.resource.RAMethods;

/**
 * Test to ensure that getResourceAdaptorTypes() is returning a correct value.
 * <p>
 * Test assertion ID: 1115407
 */
public class Test1115407Test extends BaseResourceTest {

    public TCKTestResult run() throws Exception {
        HashMap results = sendMessage(RAMethods.getResourceAdaptorTypes, new Integer(1115407));
        
        checkResult(results, "result1", 1115407, "getResourceadaptorTypes() returned null value");
        checkResult(results, "result2", 1115408, "getResourceadaptorTypes() returned unexpected value");

        return TCKTestResult.passed();
    }
}
