/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.resource.context;

import java.util.HashMap;

import com.opencloud.sleetck.lib.TCKTestFailureException;
import com.opencloud.sleetck.lib.TCKTestResult;
import com.opencloud.sleetck.lib.testsuite.resource.BaseResourceTest;
import com.opencloud.sleetck.lib.testsuite.resource.RAMethods;

/**
 * Test to ensure that getEntityName() is returning the correct value.
 * <p>
 * Test assertion ID: 1115175
 */
public class Test1115175Test extends BaseResourceTest {
    private static final int ASSERTION_ID = 1115175;

    public TCKTestResult run() throws Exception {
        HashMap resultmap = sendMessage(RAMethods.getEntityName);
        if (resultmap == null)
            throw new TCKTestFailureException(ASSERTION_ID, "Test timed out while waiting for response to getEntityName()");
        Object result = resultmap.get("result");
        if (!getResourceAdaptorEntityName().equals(result))
            throw new TCKTestFailureException(ASSERTION_ID, "getEntityName() returned an incorrect value: " + result);

        return TCKTestResult.passed();
    }
}
