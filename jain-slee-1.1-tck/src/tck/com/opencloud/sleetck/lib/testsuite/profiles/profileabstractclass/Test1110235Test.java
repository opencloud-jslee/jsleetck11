/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.profiles.profileabstractclass;

import com.opencloud.sleetck.lib.AbstractSleeTCKTest;
import com.opencloud.sleetck.lib.TCKTestErrorException;
import com.opencloud.sleetck.lib.TCKTestFailureException;
import com.opencloud.sleetck.lib.TCKTestResult;

public class Test1110235Test extends AbstractSleeTCKTest {

    private static final String ABSTRACT_LIFECYCLE_DU_PATH_PARAM= "abstractDUPath";
    private static final String FINAL_LIFECYCLE_DU_PATH_PARAM= "finalDUPath";

    /**
     * This test trys to deploy profile specs that define Profile abstract classes
     * with lifecycle methods declared abstract or final. Deployment should fail
     * in both cases.
     */
    public TCKTestResult run() throws Exception {

        //Spec 1: Profile abstract class has abstract lifecycle methods
        getLog().fine("Deploying profile spec.");
        try {
            setupService(ABSTRACT_LIFECYCLE_DU_PATH_PARAM);
            throw new TCKTestFailureException(1110699,"Deployment of Profile spec with Profile abstract class " +
                    "defining lifecycle callbacks as abstract was successful but should have failed.");
        }
        catch (TCKTestErrorException e) {
            getLog().fine("Caught expected exception when trying to deploy profile spec. Exception: "+e.getMessage());
        }

        //Spec 2: Profile abstract class has final lifecycle methods
        getLog().fine("Deploying profile spec.");
        try {
            setupService(FINAL_LIFECYCLE_DU_PATH_PARAM);
            throw new TCKTestFailureException(11101120,"Deployment of Profile spec with Profile abstract class " +
            "defining lifecycle callbacks as final was successful but should have failed.");
        }
        catch (TCKTestErrorException e) {
            getLog().fine("Caught expected exception when trying to deploy profile spec. Exception: "+e.getMessage());
        }

        return TCKTestResult.passed();
    }

    public void setUp() throws Exception {

    }

    public void tearDown() throws Exception {

        super.tearDown();
    }
}
