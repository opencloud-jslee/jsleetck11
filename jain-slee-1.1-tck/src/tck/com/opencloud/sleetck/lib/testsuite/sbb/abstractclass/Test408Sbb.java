/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.sbb.abstractclass;

import javax.slee.ActivityEndEvent;
import javax.slee.ActivityContextInterface;
import javax.slee.SbbContext;
import javax.slee.Address;
import javax.slee.SbbLocalObject;
import javax.slee.facilities.Level;
import javax.slee.ChildRelation;
import javax.slee.CreateException;

import javax.slee.profile.UnrecognizedProfileTableNameException;
import javax.slee.profile.UnrecognizedProfileNameException;
import javax.slee.profile.ProfileID;

import com.opencloud.sleetck.lib.sbbutils.BaseTCKSbb;
import com.opencloud.sleetck.lib.sbbutils.TCKSbbUtils;
import com.opencloud.sleetck.lib.sbbutils.TCKSbbConstants;
import com.opencloud.sleetck.lib.resource.events.TCKResourceEventX;
import com.opencloud.sleetck.lib.resource.TCKActivityID;
import com.opencloud.sleetck.lib.resource.sbbapi.TCKActivity;
import com.opencloud.sleetck.lib.resource.sbbapi.TCKResourceSbbInterface;
import com.opencloud.sleetck.lib.resource.sbbapi.TCKActivityContextInterfaceFactory;

import java.util.HashMap;
import java.util.Iterator;

public abstract class Test408Sbb extends BaseTCKSbb {

    public void sbbCreate() throws CreateException {

        try{
            HashMap map = new HashMap();

            try {
                ProfileID profileID = new ProfileID("Test408ProfileTable", "Test408Profile");
                getProfileCMP(profileID);
            } catch (IllegalStateException e) {
                map.put("Type", "Result");
                map.put("Result", new Boolean(true));
                map.put("Message", "Ok");
                TCKSbbUtils.getResourceInterface().sendSbbMessage(map);
                return;
            }

            map.put("Type", "Result");
            map.put("Result", new Boolean(false));
            map.put("Message", "Expected IllegalStateException when calling the SBB abstract class get profile CMP method with SBB not in the Ready state.");
            TCKSbbUtils.getResourceInterface().sendSbbMessage(map);
            return;
        } catch (Exception e) {
        }
    }

    // Initial event handler, fires secondary event to send "EndTest" signal
    // and process the result sent earlier from sbbCreate().
    public void onTCKResourceEventX1(TCKResourceEventX event, ActivityContextInterface aci) {
        try {
            fireSendResultEvent(new SendResultEvent(), aci, null);
        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }
    }

    public void onSendResultEvent(SendResultEvent event, ActivityContextInterface aci) {

        try {
            HashMap map = new HashMap();
            map.put("Type", "EndTest");
            TCKSbbUtils.getResourceInterface().sendSbbMessage(map);
        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }

    }

    public abstract void fireSendResultEvent(SendResultEvent event, ActivityContextInterface aci, Address address);
    

    // get profile CMP method.
    public abstract Test408ProfileCMP getProfileCMP(ProfileID profileID) throws UnrecognizedProfileTableNameException, UnrecognizedProfileNameException;


}
