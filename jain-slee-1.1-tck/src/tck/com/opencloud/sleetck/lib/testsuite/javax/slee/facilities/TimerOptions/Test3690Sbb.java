/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.javax.slee.facilities.TimerOptions;

import com.opencloud.sleetck.lib.resource.events.TCKResourceEventX;
import com.opencloud.sleetck.lib.sbbutils.BaseTCKSbb;
import com.opencloud.sleetck.lib.sbbutils.TCKSbbUtils;

import javax.slee.ActivityContextInterface;
import javax.slee.facilities.TimerOptions;
import java.util.HashMap;

public abstract class Test3690Sbb extends BaseTCKSbb {

    // Initial event
    public void onTCKResourceEventX1(TCKResourceEventX ev, ActivityContextInterface aci) {

        try {

            HashMap map = new HashMap();
            TimerOptions options = new TimerOptions();

            try {
                options.hashCode();
            } catch (Exception e) {
                map.put("Result", new Boolean(false));
                map.put("ID", new Integer(3690));
                map.put("Message", "TimerOptions.hashCode() threw an exception.");
                TCKSbbUtils.getResourceInterface().sendSbbMessage(map);
                return;
            }

            try {
                if (options.toString() == null) {
                    map.put("Result", new Boolean(false));
                    map.put("ID", new Integer(3692));
                    map.put("Message", "TimerOptions.toString() returned null.");
                    try {
                        TCKSbbUtils.getResourceInterface().sendSbbMessage(map);
                    } catch (Exception e) {
                        TCKSbbUtils.handleException(e);
                    }
                    return;
                }
            } catch (Exception e) {
                map.put("Result", new Boolean(false));
                map.put("ID", new Integer(3692));
                map.put("Message", "TimerOptions.toString() threw an exception.");
                TCKSbbUtils.getResourceInterface().sendSbbMessage(map);
                return;
            }                
                
            map.put("Result", new Boolean(true));
            map.put("Message", "Ok");
            TCKSbbUtils.getResourceInterface().sendSbbMessage(map);
            return;

        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }

    }

}

