/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.management.ManagementMBean;

import com.opencloud.sleetck.lib.AbstractSleeTCKTest;
import com.opencloud.sleetck.lib.TCKTestResult;
import com.opencloud.sleetck.lib.testutils.jmx.DeploymentMBeanProxy;

import javax.slee.management.DependencyException;
import javax.slee.management.DeployableUnitID;

public class Test1583Test extends AbstractSleeTCKTest {

    private static final String SBB_DU_PATH_PARAM = "sbbDUPath";
    private static final String EVENT_DU_PATH_PARAM = "eventDUPath";
    private static final int TEST_ID = 1583;

    /**
     * Perform the actual test.
     */
    public TCKTestResult run() throws Exception {
        DeploymentMBeanProxy duProxy = utils().getDeploymentMBeanProxy();
        try {
            duProxy.uninstall(eventDuID);
        } catch (DependencyException e) {
            return TCKTestResult.passed();
        }
        return TCKTestResult.failed(TEST_ID, "Was able to uninstall an Event that was still in use by an SBB. "+
                "eventDuID="+eventDuID+",sbbDuID="+sbbDuID);
    }

    /**
     * Do all the pre-run configuration of the test.
     */

    public void setUp() throws Exception {
        getLog().fine("Installing and activating service");
        // Install the Deployable Units
        String duPath = utils().getTestParams().getProperty(EVENT_DU_PATH_PARAM);
        eventDuID = utils().install(duPath);
        duPath = utils().getTestParams().getProperty(SBB_DU_PATH_PARAM);
        sbbDuID = utils().install(duPath);
    }

    private DeployableUnitID eventDuID, sbbDuID;
}
