/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.profiles.querydynamic;

import java.util.Collection;

import javax.management.Attribute;
import javax.management.ObjectName;
import javax.slee.Address;
import javax.slee.AddressPlan;
import javax.slee.profile.ProfileSpecificationID;
import javax.slee.profile.query.And;
import javax.slee.profile.query.Equals;
import javax.slee.profile.query.GreaterThan;
import javax.slee.profile.query.GreaterThanOrEquals;
import javax.slee.profile.query.HasPrefix;
import javax.slee.profile.query.LessThan;
import javax.slee.profile.query.LessThanOrEquals;
import javax.slee.profile.query.LongestPrefixMatch;
import javax.slee.profile.query.Not;
import javax.slee.profile.query.NotEquals;
import javax.slee.profile.query.Or;
import javax.slee.profile.query.QueryExpression;
import javax.slee.profile.query.RangeMatch;

import com.opencloud.sleetck.lib.AbstractSleeTCKTest;
import com.opencloud.sleetck.lib.TCKTestErrorException;
import com.opencloud.sleetck.lib.TCKTestFailureException;
import com.opencloud.sleetck.lib.TCKTestResult;
import com.opencloud.sleetck.lib.infra.SleeTCKComponentConstants;
import com.opencloud.sleetck.lib.testutils.ProfileUtils;
import com.opencloud.sleetck.lib.testutils.jmx.ProfileMBeanProxy;
import com.opencloud.sleetck.lib.testutils.jmx.ProfileProvisioningMBeanProxy;


public class Test1110475Test extends AbstractSleeTCKTest {

    private static final String SERVICE_DU_PATH_PARAM= "serviceDUPath";

    private static final String SPEC_NAME = "Test1110475Profile";
    private static final String SPEC_VERSION = "1.0";
    private static final String PROFILE_TABLE_NAME = "Test1110475ProfileTable";
    private static final String PROFILE_NAME0_1 = "Test1110475Profile0_1";
    private static final String PROFILE_NAME0_2 = "Test1110475Profile0_2";
    private static final String PROFILE_NAME1_1 = "Test1110475Profile1_1";
    private static final String PROFILE_NAME1_2 = "Test1110475Profile1_2";
    private static final String PROFILE_NAME2_1 = "Test1110475Profile2_1";
    private static final String PROFILE_NAME2_2 = "Test1110475Profile2_2";
    private static final String PROFILE_NAME3 = "Test1110475Profile3";


//  ********************* start of declaration of dynamic queries for test *************** //
    public static final QueryExpression equalsString = new Equals("stringValue", "42");
    public static final QueryExpression equalsIntObj = new Equals("intObjValue", new Integer(42));
    public static final QueryExpression equalsCharObj = new Equals("charObjValue", new Character('X'));
    public static final QueryExpression equalsBoolObj = new Equals("boolObjValue", new Boolean(true));
    public static final QueryExpression equalsDoubleObj = new Equals("doubleObjValue", new Double(42));
    public static final QueryExpression equalsByteObj = new Equals("byteObjValue", new Byte("42"));
    public static final QueryExpression equalsShortObj = new Equals("shortObjValue", new Short("42"));
    public static final QueryExpression equalsLongObj = new Equals("longObjValue", new Long(42));
    public static final QueryExpression equalsFloatObj = new Equals("floatObjValue", new Float(42));
    public static final QueryExpression equalsInt = new Equals("intValue", new Integer(42));
    public static final QueryExpression equalsChar = new Equals("charValue", new Character('X'));
    public static final QueryExpression equalsBool = new Equals("boolValue", new Boolean(true));
    public static final QueryExpression equalsDouble = new Equals("doubleValue", new Double(42));
    public static final QueryExpression equalsByte = new Equals("byteValue", new Byte("42"));
    public static final QueryExpression equalsShort = new Equals("shortValue", new Short("42"));
    public static final QueryExpression equalsLong = new Equals("longValue", new Long(42));
    public static final QueryExpression equalsFloat = new Equals("floatValue", new Float(42));
    public static final QueryExpression equalsAddress = new Equals("address", new Address(AddressPlan.SLEE_PROFILE, "XY/X"));
    public static final QueryExpression[] equals={
        equalsString, equalsIntObj, equalsCharObj, equalsBoolObj,
        equalsDoubleObj, equalsByteObj, equalsShortObj, equalsLongObj, equalsFloatObj, equalsInt,
        equalsChar, equalsBool, equalsDouble, equalsByte, equalsShort, equalsLong,
        equalsFloat, equalsAddress,
        };

    public static final QueryExpression notEqualsString = new NotEquals("stringValue", "42");
    public static final QueryExpression notEqualsIntObj = new NotEquals("intObjValue", new Integer(42));
    public static final QueryExpression notEqualsCharObj = new NotEquals("charObjValue", new Character('X'));
    public static final QueryExpression notEqualsBoolObj = new NotEquals("boolObjValue", new Boolean(true));
    public static final QueryExpression notEqualsDoubleObj = new NotEquals("doubleObjValue", new Double(42));
    public static final QueryExpression notEqualsByteObj = new NotEquals("byteObjValue", new Byte("42"));
    public static final QueryExpression notEqualsShortObj = new NotEquals("shortObjValue", new Short("42"));
    public static final QueryExpression notEqualsLongObj = new NotEquals("longObjValue", new Long(42));
    public static final QueryExpression notEqualsFloatObj = new NotEquals("floatObjValue", new Float(42));
    public static final QueryExpression notEqualsInt = new NotEquals("intValue", new Integer(42));
    public static final QueryExpression notEqualsChar = new NotEquals("charValue", new Character('X'));
    public static final QueryExpression notEqualsBool = new NotEquals("boolValue", new Boolean(true));
    public static final QueryExpression notEqualsDouble = new NotEquals("doubleValue", new Double(42));
    public static final QueryExpression notEqualsByte = new NotEquals("byteValue", new Byte("42"));
    public static final QueryExpression notEqualsShort = new NotEquals("shortValue", new Short("42"));
    public static final QueryExpression notEqualsLong = new NotEquals("longValue", new Long(42));
    public static final QueryExpression notEqualsFloat = new NotEquals("floatValue", new Float(42));
    public static final QueryExpression notEqualsAddress = new NotEquals("address", new Address(AddressPlan.SLEE_PROFILE, "XY/X"));
    public static final QueryExpression[] notEquals={
        notEqualsString, notEqualsIntObj, notEqualsCharObj, notEqualsBoolObj,
        notEqualsDoubleObj, notEqualsByteObj, notEqualsShortObj, notEqualsLongObj, notEqualsFloatObj, notEqualsInt,
        notEqualsChar, notEqualsBool, notEqualsDouble, notEqualsByte, notEqualsShort, notEqualsLong,
        notEqualsFloat, notEqualsAddress,
        };

    public static final QueryExpression lessThanString = new LessThan("stringValue", "42");
    public static final QueryExpression lessThanIntObj = new LessThan("intObjValue", new Integer(42));
    public static final QueryExpression lessThanCharObj = new LessThan("charObjValue", new Character('X'));
    public static final QueryExpression lessThanBoolObj = new LessThan("boolObjValue", new Boolean(true));
    public static final QueryExpression lessThanDoubleObj = new LessThan("doubleObjValue", new Double(42));
    public static final QueryExpression lessThanByteObj = new LessThan("byteObjValue", new Byte("42"));
    public static final QueryExpression lessThanShortObj = new LessThan("shortObjValue", new Short("42"));
    public static final QueryExpression lessThanLongObj = new LessThan("longObjValue", new Long(42));
    public static final QueryExpression lessThanFloatObj = new LessThan("floatObjValue", new Float(42));
    public static final QueryExpression lessThanInt = new LessThan("intValue", new Integer(42));
    public static final QueryExpression lessThanChar = new LessThan("charValue", new Character('X'));
    public static final QueryExpression lessThanBool = new LessThan("boolValue", new Boolean(true));
    public static final QueryExpression lessThanDouble = new LessThan("doubleValue", new Double(42));
    public static final QueryExpression lessThanByte = new LessThan("byteValue", new Byte("42"));
    public static final QueryExpression lessThanShort = new LessThan("shortValue", new Short("42"));
    public static final QueryExpression lessThanLong = new LessThan("longValue", new Long(42));
    public static final QueryExpression lessThanFloat = new LessThan("floatValue", new Float(42));
    public static final QueryExpression[] lessThan={
        lessThanString, lessThanIntObj, lessThanCharObj, lessThanBoolObj,
        lessThanDoubleObj, lessThanByteObj, lessThanShortObj, lessThanLongObj, lessThanFloatObj, lessThanInt,
        lessThanChar, lessThanBool, lessThanDouble, lessThanByte, lessThanShort, lessThanLong,
        lessThanFloat};


    public static final QueryExpression lessThanOrEqualsString = new LessThanOrEquals("stringValue", "42");
    public static final QueryExpression lessThanOrEqualsIntObj = new LessThanOrEquals("intObjValue", new Integer(42));
    public static final QueryExpression lessThanOrEqualsCharObj = new LessThanOrEquals("charObjValue", new Character('X'));
    public static final QueryExpression lessThanOrEqualsBoolObj = new LessThanOrEquals("boolObjValue", new Boolean(true));
    public static final QueryExpression lessThanOrEqualsDoubleObj = new LessThanOrEquals("doubleObjValue", new Double(42));
    public static final QueryExpression lessThanOrEqualsByteObj = new LessThanOrEquals("byteObjValue", new Byte("42"));
    public static final QueryExpression lessThanOrEqualsShortObj = new LessThanOrEquals("shortObjValue", new Short("42"));
    public static final QueryExpression lessThanOrEqualsLongObj = new LessThanOrEquals("longObjValue", new Long(42));
    public static final QueryExpression lessThanOrEqualsFloatObj = new LessThanOrEquals("floatObjValue", new Float(42));
    public static final QueryExpression lessThanOrEqualsInt = new LessThanOrEquals("intValue", new Integer(42));
    public static final QueryExpression lessThanOrEqualsChar = new LessThanOrEquals("charValue", new Character('X'));
    public static final QueryExpression lessThanOrEqualsBool = new LessThanOrEquals("boolValue", new Boolean(true));
    public static final QueryExpression lessThanOrEqualsDouble = new LessThanOrEquals("doubleValue", new Double(42));
    public static final QueryExpression lessThanOrEqualsByte = new LessThanOrEquals("byteValue", new Byte("42"));
    public static final QueryExpression lessThanOrEqualsShort = new LessThanOrEquals("shortValue", new Short("42"));
    public static final QueryExpression lessThanOrEqualsLong = new LessThanOrEquals("longValue", new Long(42));
    public static final QueryExpression lessThanOrEqualsFloat = new LessThanOrEquals("floatValue", new Float(42));
    public static final QueryExpression[] lessThanOrEquals={
        lessThanOrEqualsString, lessThanOrEqualsIntObj, lessThanOrEqualsCharObj, lessThanOrEqualsBoolObj,
        lessThanOrEqualsDoubleObj, lessThanOrEqualsByteObj, lessThanOrEqualsShortObj, lessThanOrEqualsLongObj, lessThanOrEqualsFloatObj, lessThanOrEqualsInt,
        lessThanOrEqualsChar, lessThanOrEqualsBool, lessThanOrEqualsDouble, lessThanOrEqualsByte, lessThanOrEqualsShort, lessThanOrEqualsLong,
        lessThanOrEqualsFloat};

    public static final QueryExpression greaterThanString = new GreaterThan("stringValue", "42");
    public static final QueryExpression greaterThanIntObj = new GreaterThan("intObjValue", new Integer(42));
    public static final QueryExpression greaterThanCharObj = new GreaterThan("charObjValue", new Character('X'));
    public static final QueryExpression greaterThanBoolObj = new GreaterThan("boolObjValue", new Boolean(true));
    public static final QueryExpression greaterThanDoubleObj = new GreaterThan("doubleObjValue", new Double(42));
    public static final QueryExpression greaterThanByteObj = new GreaterThan("byteObjValue", new Byte("42"));
    public static final QueryExpression greaterThanShortObj = new GreaterThan("shortObjValue", new Short("42"));
    public static final QueryExpression greaterThanLongObj = new GreaterThan("longObjValue", new Long(42));
    public static final QueryExpression greaterThanFloatObj = new GreaterThan("floatObjValue", new Float(42));
    public static final QueryExpression greaterThanInt = new GreaterThan("intValue", new Integer(42));
    public static final QueryExpression greaterThanChar = new GreaterThan("charValue", new Character('X'));
    public static final QueryExpression greaterThanBool = new GreaterThan("boolValue", new Boolean(true));
    public static final QueryExpression greaterThanDouble = new GreaterThan("doubleValue", new Double(42));
    public static final QueryExpression greaterThanByte = new GreaterThan("byteValue", new Byte("42"));
    public static final QueryExpression greaterThanShort = new GreaterThan("shortValue", new Short("42"));
    public static final QueryExpression greaterThanLong = new GreaterThan("longValue", new Long(42));
    public static final QueryExpression greaterThanFloat = new GreaterThan("floatValue", new Float(42));
    public static final QueryExpression[] greaterThan={
        greaterThanString, greaterThanIntObj, greaterThanCharObj, greaterThanBoolObj,
        greaterThanDoubleObj, greaterThanByteObj, greaterThanShortObj, greaterThanLongObj, greaterThanFloatObj, greaterThanInt,
        greaterThanChar, greaterThanBool, greaterThanDouble, greaterThanByte, greaterThanShort, greaterThanLong,
        greaterThanFloat};

    public static final QueryExpression greaterThanOrEqualsString = new GreaterThanOrEquals("stringValue", "42");
    public static final QueryExpression greaterThanOrEqualsIntObj = new GreaterThanOrEquals("intObjValue", new Integer(42));
    public static final QueryExpression greaterThanOrEqualsCharObj = new GreaterThanOrEquals("charObjValue", new Character('X'));
    public static final QueryExpression greaterThanOrEqualsBoolObj = new GreaterThanOrEquals("boolObjValue", new Boolean(true));
    public static final QueryExpression greaterThanOrEqualsDoubleObj = new GreaterThanOrEquals("doubleObjValue", new Double(42));
    public static final QueryExpression greaterThanOrEqualsByteObj = new GreaterThanOrEquals("byteObjValue", new Byte("42"));
    public static final QueryExpression greaterThanOrEqualsShortObj = new GreaterThanOrEquals("shortObjValue", new Short("42"));
    public static final QueryExpression greaterThanOrEqualsLongObj = new GreaterThanOrEquals("longObjValue", new Long(42));
    public static final QueryExpression greaterThanOrEqualsFloatObj = new GreaterThanOrEquals("floatObjValue", new Float(42));
    public static final QueryExpression greaterThanOrEqualsInt = new GreaterThanOrEquals("intValue", new Integer(42));
    public static final QueryExpression greaterThanOrEqualsChar = new GreaterThanOrEquals("charValue", new Character('X'));
    public static final QueryExpression greaterThanOrEqualsBool = new GreaterThanOrEquals("boolValue", new Boolean(true));
    public static final QueryExpression greaterThanOrEqualsDouble = new GreaterThanOrEquals("doubleValue", new Double(42));
    public static final QueryExpression greaterThanOrEqualsByte = new GreaterThanOrEquals("byteValue", new Byte("42"));
    public static final QueryExpression greaterThanOrEqualsShort = new GreaterThanOrEquals("shortValue", new Short("42"));
    public static final QueryExpression greaterThanOrEqualsLong = new GreaterThanOrEquals("longValue", new Long(42));
    public static final QueryExpression greaterThanOrEqualsFloat = new GreaterThanOrEquals("floatValue", new Float(42));
    public static final QueryExpression[] greaterThanOrEquals={
        greaterThanOrEqualsString, greaterThanOrEqualsIntObj, greaterThanOrEqualsCharObj, greaterThanOrEqualsBoolObj,
        greaterThanOrEqualsDoubleObj, greaterThanOrEqualsByteObj, greaterThanOrEqualsShortObj, greaterThanOrEqualsLongObj, greaterThanOrEqualsFloatObj, greaterThanOrEqualsInt,
        greaterThanOrEqualsChar, greaterThanOrEqualsBool, greaterThanOrEqualsDouble, greaterThanOrEqualsByte, greaterThanOrEqualsShort, greaterThanOrEqualsLong,
        greaterThanOrEqualsFloat};

    public static final QueryExpression rangeMatchString = new RangeMatch("stringValue", "23", "42");
    public static final QueryExpression rangeMatchIntObj = new RangeMatch("intObjValue", new Integer(23), new Integer(42));
    public static final QueryExpression rangeMatchCharObj = new RangeMatch("charObjValue", new Character('X'), new Character('Z'));
    public static final QueryExpression rangeMatchBoolObj = new RangeMatch("boolObjValue", new Boolean(false), new Boolean(true));
    public static final QueryExpression rangeMatchDoubleObj = new RangeMatch("doubleObjValue", new Double(23), new Double(42));
    public static final QueryExpression rangeMatchByteObj = new RangeMatch("byteObjValue", new Byte("23"), new Byte("42"));
    public static final QueryExpression rangeMatchShortObj = new RangeMatch("shortObjValue", new Short("23"), new Short("42"));
    public static final QueryExpression rangeMatchLongObj = new RangeMatch("longObjValue", new Long(23), new Long(42));
    public static final QueryExpression rangeMatchFloatObj = new RangeMatch("floatObjValue", new Float(23), new Float(42));
    public static final QueryExpression rangeMatchInt = new RangeMatch("intValue", new Integer(23), new Integer(42));
    public static final QueryExpression rangeMatchChar = new RangeMatch("charValue", new Character('X'), new Character('Z'));
    public static final QueryExpression rangeMatchBool = new RangeMatch("boolValue", new Boolean(false), new Boolean(true));
    public static final QueryExpression rangeMatchDouble = new RangeMatch("doubleValue", new Double(23), new Double(42));
    public static final QueryExpression rangeMatchByte = new RangeMatch("byteValue", new Byte("23"), new Byte("42"));
    public static final QueryExpression rangeMatchShort = new RangeMatch("shortValue", new Short("23"), new Short("42"));
    public static final QueryExpression rangeMatchLong = new RangeMatch("longValue", new Long(23), new Long(42));
    public static final QueryExpression rangeMatchFloat = new RangeMatch("floatValue", new Float(23), new Float(42));
    public static final QueryExpression[] rangeMatch={
        rangeMatchString, rangeMatchIntObj, rangeMatchCharObj, rangeMatchBoolObj,
        rangeMatchDoubleObj, rangeMatchByteObj, rangeMatchShortObj, rangeMatchLongObj, rangeMatchFloatObj, rangeMatchInt,
        rangeMatchChar, rangeMatchBool, rangeMatchDouble, rangeMatchByte, rangeMatchShort, rangeMatchLong,
        rangeMatchFloat};

    public static final QueryExpression hasPrefix = new HasPrefix("stringValue", "42");

    public static final QueryExpression longestPrefixMatch = new LongestPrefixMatch("stringValue","42");

    public static final QueryExpression and2Arg = new And(equalsString, equalsInt);
    public static final QueryExpression and3Arg = new And(new QueryExpression[]{equalsString, equalsInt, equalsShort});
    public static final QueryExpression or2Arg = new Or(equalsString, equalsInt);
    public static final QueryExpression or3Arg = new Or(new QueryExpression[]{equalsString, equalsInt, equalsShort});
    public static final QueryExpression not1Equals = new Not(equalsString);
    public static final QueryExpression andComplex = new And(new QueryExpression[]{equalsString, longestPrefixMatch, hasPrefix, rangeMatchInt, and2Arg, or2Arg, new Not(equalsByte)});
    public static final QueryExpression orComplex = new Or(new QueryExpression[]{equalsString, longestPrefixMatch, hasPrefix, rangeMatchInt, and2Arg, or2Arg, new Not(equalsByte)});
    public static final QueryExpression notComplex = new Not(andComplex);

    public static final QueryExpression[] and = {and2Arg, and3Arg, andComplex};
    public static final QueryExpression[] or = {or2Arg, or3Arg, orComplex};
    public static final QueryExpression[] not = {not1Equals, notComplex};

//********************* end of declaration of dynamic queries for test *************** //


    public static final int TEST_ID = 1110475;

    public static final int RUN_QUERIES = 0;

    private void setupProfile(String profileName,
            String stringValue, int intValue, char charValue,
            boolean boolValue, double doubleValue, byte byteValue, short shortValue,
            long longValue, float floatValue, Integer intObjValue, Character charObjValue,
            Boolean boolObjValue, Double doubleObjValue, Byte byteObjValue, Short shortObjValue,
            Long longObjValue, Float floatObjValue, Address address) throws Exception {

        ObjectName profile = profileProvisioning.createProfile(PROFILE_TABLE_NAME, profileName);
        ProfileMBeanProxy profileProxy = utils().getMBeanProxyFactory().createProfileMBeanProxy(profile);
        getLog().fine("Added profile "+profileName);

        utils().getMBeanFacade().setAttribute(profile, new Attribute("StringValue", stringValue));
        utils().getMBeanFacade().setAttribute(profile, new Attribute("IntValue", new Integer(intValue)));
        utils().getMBeanFacade().setAttribute(profile, new Attribute("CharValue", new Character(charValue)));
        utils().getMBeanFacade().setAttribute(profile, new Attribute("BoolValue", new Boolean(boolValue)));
        utils().getMBeanFacade().setAttribute(profile, new Attribute("DoubleValue", new Double(doubleValue)));
        utils().getMBeanFacade().setAttribute(profile, new Attribute("ByteValue", new Byte(byteValue)));
        utils().getMBeanFacade().setAttribute(profile, new Attribute("ShortValue", new Short(shortValue)));
        utils().getMBeanFacade().setAttribute(profile, new Attribute("LongValue", new Long(longValue)));
        utils().getMBeanFacade().setAttribute(profile, new Attribute("FloatValue", new Float(floatValue)));
        utils().getMBeanFacade().setAttribute(profile, new Attribute("IntObjValue", intObjValue));
        utils().getMBeanFacade().setAttribute(profile, new Attribute("CharObjValue", charObjValue));
        utils().getMBeanFacade().setAttribute(profile, new Attribute("BoolObjValue", boolObjValue));
        utils().getMBeanFacade().setAttribute(profile, new Attribute("DoubleObjValue", doubleObjValue));
        utils().getMBeanFacade().setAttribute(profile, new Attribute("ByteObjValue", byteObjValue));
        utils().getMBeanFacade().setAttribute(profile, new Attribute("ShortObjValue", shortObjValue));
        utils().getMBeanFacade().setAttribute(profile, new Attribute("LongObjValue", longObjValue));
        utils().getMBeanFacade().setAttribute(profile, new Attribute("FloatObjValue", floatObjValue));
        utils().getMBeanFacade().setAttribute(profile, new Attribute("Address", address));

        getLog().fine("Set CMP field values for profile "+profileName);

        profileProxy.commitProfile();
        profileProxy.closeProfile();
    }

    /**
     * This test trys to run some dynamic queries that cover a vast range of theoretically possible
     * combinations and variations.
     */
    public TCKTestResult run() throws Exception {

        //Create profile tables
        ProfileSpecificationID specID = new ProfileSpecificationID(SPEC_NAME, SleeTCKComponentConstants.TCK_VENDOR, SPEC_VERSION);
        profileProvisioning.createProfileTable(specID, PROFILE_TABLE_NAME);
        getLog().fine("Added profile table "+PROFILE_TABLE_NAME);

        //create profiles
        setupProfile(PROFILE_NAME0_1,
                "42", 42, 'X', true, (double)42, (byte)42, (short)42, (long)42, (float)42,
                new Integer(42), new Character('X'), new Boolean(true), new Double(42), new Byte("42"), new Short("42"), new Long(42), new Float(42), new Address(AddressPlan.SLEE_PROFILE, "XY/X"));
        setupProfile(PROFILE_NAME0_2,
                "42", 42, 'X', true, (double)42, (byte)42, (short)42, (long)42, (float)42,
                new Integer(42), new Character('X'), new Boolean(true), new Double(42), new Byte("42"), new Short("42"), new Long(42), new Float(42), new Address(AddressPlan.SLEE_PROFILE, "XY/X"));



        setupProfile(PROFILE_NAME1_1,
                "41", 41, 'Y', true, (double)41, (byte)41, (short)41, (long)41, (float)41,
                new Integer(41), new Character('Y'), new Boolean(true), new Double(41), new Byte("41"), new Short("41"), new Long(41), new Float(41), new Address(AddressPlan.SLEE_PROFILE, "XY/Y"));
        setupProfile(PROFILE_NAME1_2,
                "41", 41, 'Y', true, (double)41, (byte)41, (short)41, (long)41, (float)41,
                new Integer(41), new Character('Y'), new Boolean(true), new Double(41), new Byte("41"), new Short("41"), new Long(41), new Float(41), new Address(AddressPlan.SLEE_PROFILE, "XY/Y"));



        setupProfile(PROFILE_NAME2_1,
                "23", 23, 'Z', true, (double)23, (byte)23, (short)23, (long)23, (float)23,
                new Integer(23), new Character('Z'), new Boolean(true), new Double(23), new Byte("23"), new Short("23"), new Long(23), new Float(23), new Address(AddressPlan.SLEE_PROFILE, "XY/Z"));
        setupProfile(PROFILE_NAME2_2,
                "23", 23, 'Z', true, (double)23, (byte)23, (short)23, (long)23, (float)23,
                new Integer(23), new Character('Z'), new Boolean(true), new Double(23), new Byte("23"), new Short("23"), new Long(23), new Float(23), new Address(AddressPlan.SLEE_PROFILE, "XY/Z"));



        setupProfile(PROFILE_NAME3,
                "99", 99, 'A', false, (double)99, (byte)99, (short)99, (long)99, (float)99,
                new Integer(99), new Character('A'), new Boolean(false), new Double(99), new Byte("99"), new Short("99"), new Long(99), new Float(99), new Address(AddressPlan.SLEE_PROFILE, "XY/A"));


        getLog().fine("Start firing the above queries into the SLEE...");

        //1110475: Equals matches profiles according to the attributes 'equals' method
        testQueryBunch(equals, new int[]{2,2,2,6,2,2,2,2,2,2,2,6,2,2,2,2,2,2,},1110475);

        //1110480: Equals matches profiles according to the attributes 'equals' method
        testQueryBunch(notEquals, new int[]{5,5,5,1,5,5,5,5,5,5,5,1,5,5,5,5,5,5,},1110480);

        //1110485: lessThan matches profiles according to the attributes 'compareTo' method
        testQueryBunch(lessThan, new int[]{4,4,1,1,4,4,4,4,4,4,1,1,4,4,4,4,4}, 1110485);

        //1110492: lessThanOrEquals matches profiles according to the attributes 'compareTo' method
        testQueryBunch(lessThanOrEquals, new int[]{6,6,3,7,6,6,6,6,6,6,3,7,6,6,6,6,6}, 1110492);

        //1110499: greaterThan matches profiles according to the attributes 'compareTo' method
        testQueryBunch(greaterThan, new int[]{1,1,4,0,1,1,1,1,1,1,4,0,1,1,1,1,1}, 1110499);

        //1110506: greaterThanOrEquals matches profiles according to the attributes 'compareTo' method
        testQueryBunch(greaterThanOrEquals, new int[]{3,3,6,6,3,3,3,3,3,3,6,6,3,3,3,3,3}, 1110506);

        //1110544: rangeMatch
        testQueryBunch(rangeMatch, new int[]{6,6,6,7,6,6,6,6,6,6,6,7,6,6,6,6,6}, 1110544);

        //1110533: longestPrefixMatch
        testQuery(longestPrefixMatch, 2, 1110533);

        //1110539: hasPrefix
        testQuery(hasPrefix, 2, 1110539);

        //1110512: and
        testQueryBunch(and, new int[]{2, 2, 0}, 1110512);

        //1110519: or
        testQueryBunch(or, new int[]{2, 2, 7}, 1110519);

        //1110526: not
        testQueryBunch(not, new int[]{5, 7}, 1110526);

        getLog().fine("Completed test sequence successfully.");


        return TCKTestResult.passed();
    }

    private void testQuery(QueryExpression query, int expReturn, int assertionID) throws TCKTestFailureException {
        Collection col;

        //run query
        try {
            col = profileProvisioning.getProfilesByDynamicQuery(PROFILE_TABLE_NAME, query);
        } catch (Exception e) {
            throw new TCKTestFailureException(assertionID, "Exception occurred when executing query "+query, e);
        }

        //check returned profiles
        if (col.size()!=expReturn)
            throw new TCKTestFailureException(assertionID, "The following query did not return the expected number of profiles: "+query+". Number of found profiles was "+col.size()+", expected "+expReturn);
        else
            getLog().fine("Query "+query+" returned "+expReturn+" profiles as expected.");

    }

    private void testQueryBunch(QueryExpression[] queries, int[] expReturns, int assertionID) throws Exception {
        if (queries.length!=expReturns.length)
            throw new TCKTestErrorException("Length of query array and list of expected return values do not match.");

        for (int i=0; i<queries.length; i++) {
            testQuery(queries[i], expReturns[i], assertionID);
        }
    }

    private void testQueryBunch(QueryExpression[] queries, int expReturn, int assertionID) throws Exception {
        int[] expReturns = new int[queries.length];
        for (int i=0; i< queries.length; i++)
            expReturns[i] = expReturn;

        testQueryBunch(queries, expReturns, assertionID);
    }

    public void setUp() throws Exception {

        setupService(SERVICE_DU_PATH_PARAM);

        profileUtils = new ProfileUtils(utils());
        profileProvisioning = profileUtils.getProfileProvisioningProxy();

    }

    public void tearDown() throws Exception {

        try {
            profileUtils.removeProfileTable(PROFILE_TABLE_NAME);
        }
        catch (Exception e) {
            getLog().warning("Caught exception while trying to remove profile table:");
            getLog().warning(e);
        }

        super.tearDown();
    }

    private ProfileUtils profileUtils;
    private ProfileProvisioningMBeanProxy profileProvisioning;
}