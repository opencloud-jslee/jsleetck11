/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.facilities.alarmfacility.alarm;

import java.util.HashMap;

import javax.naming.InitialContext;
import javax.slee.ActivityContextInterface;
import javax.slee.facilities.AlarmFacility;
import javax.slee.facilities.AlarmLevel;
import javax.slee.facilities.Tracer;

import com.opencloud.sleetck.lib.resource.events.TCKResourceEventX;
import com.opencloud.sleetck.lib.sbbutils.BaseTCKSbb;
import com.opencloud.sleetck.lib.sbbutils.TCKSbbUtils;

/*  AssertionID(1113467): Test For example if we consider an alarm 
 *  which is raised, cleared and then raised again the two different 
 *  periods where the alarm is raised are distinguished by different 
 *  alarm identifiers.  
 */
public abstract class Test1113467Sbb extends BaseTCKSbb {

    private static final String JNDI_ALARMFACILITY_NAME = AlarmFacility.JNDI_NAME;

    public static final String ALARM_MESSAGE = "Test1113467AlarmMessage";

    public static final String ALARM_INSTANCEID = "Test1113467AlarmInstanceID";

    // Initial event
    public void onTCKResourceEventX1(TCKResourceEventX event, ActivityContextInterface aci) {

        try {
            tracer = getSbbContext().getTracer("com.test");
            tracer.info("Recieved " + event + " message");
            AlarmFacility facility = (AlarmFacility) new InitialContext().lookup(JNDI_ALARMFACILITY_NAME);
            setFirstAlarm(facility.raiseAlarm("javax.slee.management.Alarm", ALARM_INSTANCEID, AlarmLevel.MAJOR,
                    ALARM_MESSAGE));
            
            if (!facility.clearAlarm(getFirstAlarm()))
                tracer.severe("The first alarm which was raised cannot be cleared!");


        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }

    }

    // Second event
    public void onTCKResourceEventX2(TCKResourceEventX event, ActivityContextInterface aci) {

        try {
            tracer = getSbbContext().getTracer("com.test");
            tracer.info("Recieved " + event + " message");
            AlarmFacility facility = (AlarmFacility) new InitialContext().lookup(JNDI_ALARMFACILITY_NAME);

            setSecondAlarm(facility.raiseAlarm("javax.slee.management.Alarm", ALARM_INSTANCEID, AlarmLevel.MAJOR,
                    ALARM_MESSAGE));

            if (getSecondAlarm().equals(getFirstAlarm()))
                sendResultToTCK("Test1113467Test", false, 1113467, " The alarm is raised didn't distinguish by different alarm identifiers.");
            
        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }

    }
    
    // Third event
    public void onTCKResourceEventX3(TCKResourceEventX event, ActivityContextInterface aci) {

        try {
            tracer = getSbbContext().getTracer("com.test");
            tracer.info("Recieved " + event + " message");
            AlarmFacility facility = (AlarmFacility) new InitialContext().lookup(JNDI_ALARMFACILITY_NAME);

            if (!facility.clearAlarm(getSecondAlarm()))
                tracer.severe("The second alarm which was raised cannot be cleared!");

            String alarmID = facility.raiseAlarm("javax.slee.management.Alarm", ALARM_INSTANCEID, AlarmLevel.MAJOR,
                    ALARM_MESSAGE);

            if (alarmID.equals(getSecondAlarm()))
                sendResultToTCK("Test1113467Test", false, 1113467, " The alarm is raised didn't distinguish by different alarm identifiers again.");

            sendResultToTCK("Test1113467Test", true, 1113467, "The alarm is raised are distinguished by different alarm identifiers test passed.");

        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }

    }

    private void sendResultToTCK(String testName, boolean result, int failedAssertionID,  String message) throws Exception {
        HashMap sbbData = new HashMap();
        sbbData.put("testname", testName);
        sbbData.put("result", result ? "pass" : "fail");
        sbbData.put("message", message);
        sbbData.put("id", new Integer(failedAssertionID));
        TCKSbbUtils.getResourceInterface().sendSbbMessage(sbbData);
    }

    public abstract void setFirstAlarm(String alarmID);

    public abstract String getFirstAlarm();
    
    public abstract void setSecondAlarm(String alarmID);

    public abstract String getSecondAlarm();

    private Tracer tracer;
}
