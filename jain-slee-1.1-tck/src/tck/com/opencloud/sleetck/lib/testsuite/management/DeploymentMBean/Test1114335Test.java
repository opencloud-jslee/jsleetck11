/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.management.DeploymentMBean;

import com.opencloud.sleetck.lib.AbstractSleeTCKTest;
import com.opencloud.sleetck.lib.TCKTestResult;
import com.opencloud.sleetck.lib.testutils.jmx.DeploymentMBeanProxy;

import javax.slee.ServiceID;
import javax.slee.SbbID;
import javax.slee.UnrecognizedServiceException;
import javax.slee.management.DeployableUnitID;

/* Test1114335
 * This test checks the getSbbs() methods of the DeploymentMBean interface
 */


public class Test1114335Test extends AbstractSleeTCKTest {

    private static final String SERVICE_DU_PATH_PARAM = "DUPath";
    private static final int TEST_ID = 1114335;

    /**
     * Perform the actual test.
     */
    public TCKTestResult run() throws Exception {
        DeployableUnitID duID;
        ServiceID[] serviceIDs;

        // Install the Deployable Units
        String duPath = utils().getTestParams().getProperty(SERVICE_DU_PATH_PARAM);
        try {
            String fullURL = utils().getDeploymentUnitURL(duPath);
            duID = utils().getDeploymentMBeanProxy().install(fullURL);
        } catch (Exception e) {
            return TCKTestResult.failed(TEST_ID, "Failed to install deployable unit.");
        }

        DeploymentMBeanProxy duProxy = utils().getDeploymentMBeanProxy();

        try{

            try {
                 serviceIDs = duProxy.getServices();
                 if (serviceIDs.length == 0)
                     return TCKTestResult.failed(1114335, "DeploymentMBean.getServices() returned incorrect number of services.");
                 SbbID[] sbbIDs = duProxy.getSbbs(serviceIDs[0]);
                 if (sbbIDs.length == 0)
                     return TCKTestResult.failed(1114335, "DeploymentMBean.getServices() returned incorrect number of SBBs.");
                 logSuccessfulCheck(1114335);
            } catch (NullPointerException e) {
                 return TCKTestResult.failed(1114335, "DeploymentMBean.getSbbs(serviceID) didn't contain serviceID");
            } catch (UnrecognizedServiceException e) {
                 return TCKTestResult.failed(1114335, "DeploymentMBean.getSbbs(serviceID) didn't recognise serviceID");
            } catch (Exception e) {
                 return TCKTestResult.failed(1114335, "DeploymentMBean.getSbbs(serviceID) has thrown Exception: " + e.getClass().toString());
            }

            try {
                serviceIDs[0] = new ServiceID("Non", "Existent", "Service");
                duProxy.getSbbs(serviceIDs[0]);
                return TCKTestResult.failed(1114539, "DeploymentMBean.getSbbs(serviceID) didn't throw UnrecognizedServiceException");
            } catch (UnrecognizedServiceException e) {
                logSuccessfulCheck(1114539);
            } catch (Exception e) {
                return TCKTestResult.failed(1114539, "DeploymentMBean.getSbbs(serviceID) didn't throw UnrecognizedServiceException but " + e.getClass().toString());
            }

            try {
                serviceIDs = null;
                duProxy.getSbbs(serviceIDs[0]);
                return TCKTestResult.failed(1114538, "DeploymentMBean.getSbbs(serviceID) didn't throw NullPointerException");
            } catch (NullPointerException e) {
                logSuccessfulCheck(1114538);
            } catch (Exception e) {
                return TCKTestResult.failed(1114538, "DeploymentMBean.getSbbs(serviceID) didn't throw NullPointerException but " + e.getClass().toString());
            }

        } finally {
            utils().getLog().fine("Deactivating and uninstalling service");
            duProxy.uninstall(duID);
        }

        return TCKTestResult.passed();

    }

    /**
     * Do all the pre-run configuration of the test.
     */
    public void setUp() throws Exception {
        // no-op
    }

    /**
     * Clean up after the test.
     */
    public void tearDown() throws Exception {
        utils().getLog().fine("Disconnecting from resource");
        utils().getResourceInterface().clearActivities();
        utils().getResourceInterface().removeResourceListener();
    }

    private void logSuccessfulCheck(int assertionID) {
        utils().getLog().info("Check for assertion "+assertionID+" OK");
    }

}