/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.transactions;

import javax.transaction.Status;

public class StatusUtil {

    public static String STATUS_ACTIVE = "STATUS_ACTIVE";
    public static String STATUS_COMMITTED = "STATUS_COMMITTED";
    public static String STATUS_COMMITTING = "STATUS_COMMITTING";
    public static String STATUS_MARKED_ROLLBACK = "STATUS_MARKED_ROLLBACK";
    public static String STATUS_NO_TRANSACTION = "STATUS_NO_TRANSACTION";
    public static String STATUS_PREPARED = "STATUS_PREPARED";
    public static String STATUS_PREPARING = "STATUS_PREPARING";
    public static String STATUS_ROLLEDBACK = "STATUS_ROLLEDBACK";
    public static String STATUS_ROLLING_BACK = "STATUS_ROLLING_BACK";
    public static String STATUS_UNKNOWN = "STATUS_UNKNOWN";
    public static String INVALID = "INVALID";

    public static String statusToString(int status) {
        switch (status) {
        case Status.STATUS_ACTIVE:
            return STATUS_ACTIVE;
        case Status.STATUS_COMMITTED:
            return STATUS_COMMITTED;
        case Status.STATUS_COMMITTING:
            return STATUS_COMMITTING;
        case Status.STATUS_MARKED_ROLLBACK:
            return STATUS_MARKED_ROLLBACK;
        case Status.STATUS_NO_TRANSACTION:
            return STATUS_NO_TRANSACTION;
        case Status.STATUS_PREPARED:
            return STATUS_PREPARED;
        case Status.STATUS_PREPARING:
            return STATUS_PREPARING;
        case Status.STATUS_ROLLEDBACK:
            return STATUS_ROLLEDBACK;
        case Status.STATUS_ROLLING_BACK:
            return STATUS_ROLLING_BACK;
        case Status.STATUS_UNKNOWN:
            return STATUS_UNKNOWN;
        default:
            return INVALID;
        }
    }

}
