/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.usage.common;

import com.opencloud.sleetck.lib.SleeTCKTestUtils;
import com.opencloud.sleetck.lib.TCKTestErrorException;

import javax.slee.UnrecognizedComponentException;
import javax.slee.InvalidArgumentException;
import javax.slee.UnrecognizedSbbException;
import javax.slee.usage.UnrecognizedUsageParameterSetNameException;
import javax.slee.management.ManagementException;
import javax.management.ObjectName;
import java.util.HashMap;
import java.util.Iterator;

/**
 * Extends UsageMBeanLookup to provide explicit access to GenericUsageMBeanProxy interfaces
 */
public class GenericUsageMBeanLookup extends UsageMBeanLookup {

    public GenericUsageMBeanLookup(String serviceName, String sbbName, SleeTCKTestUtils testUtils)
                throws UnrecognizedComponentException, ManagementException, TCKTestErrorException, InvalidArgumentException {
        super(serviceName, sbbName, testUtils);
        namedGenericSbbUsageMBeanProxies = new HashMap();
        unnamedGenericUsageMBeanProxy = new GenericUsageMBeanProxyImpl(getUnnamedSbbUsageMBeanName(),testUtils.getMBeanFacade());
    }

    /**
     * Constructor form which assumes service name "GenericUsageService" and SBB name "GenericUsageSbb"
     */
    public GenericUsageMBeanLookup(SleeTCKTestUtils testUtils) throws UnrecognizedComponentException, ManagementException, TCKTestErrorException, InvalidArgumentException {
        this("GenericUsageService", "GenericUsageSbb", testUtils);
    }

    public GenericUsageMBeanProxy getUnnamedGenericUsageMBeanProxy() {
        return unnamedGenericUsageMBeanProxy;
    }

    public GenericUsageMBeanProxy getNamedGenericSbbUsageMBeanProxy(String name)
                throws TCKTestErrorException, ManagementException, InvalidArgumentException,
                       UnrecognizedSbbException, UnrecognizedUsageParameterSetNameException {
        synchronized(namedGenericSbbUsageMBeanProxies) {
            GenericUsageMBeanProxy rProxy = (GenericUsageMBeanProxy)namedGenericSbbUsageMBeanProxies.get(name);
            if(rProxy == null) {
                ObjectName namedGenericSbbUsageMBeanName = getServiceUsageMBeanProxy().getSbbUsageMBean(getSbbID(),name);
                rProxy = new GenericUsageMBeanProxyImpl(namedGenericSbbUsageMBeanName,getUtils().getMBeanFacade());
                namedGenericSbbUsageMBeanProxies.put(namedGenericSbbUsageMBeanName,rProxy);
            }
            return rProxy;
        }
    }

    public void closeAllMBeans() {
        super.closeAllMBeans();
        synchronized(namedGenericSbbUsageMBeanProxies) {
            Iterator toCloseIter = namedGenericSbbUsageMBeanProxies.values().iterator();
            while (toCloseIter.hasNext()) {
                GenericUsageMBeanProxy toClose = (GenericUsageMBeanProxy) toCloseIter.next();
                try { toClose.close(); } catch (Exception e) { /* no-op */ }
            }
        }
        try { unnamedGenericUsageMBeanProxy.close(); } catch (Exception e) { /* no-op */ }
    }

    private GenericUsageMBeanProxy unnamedGenericUsageMBeanProxy;
    private HashMap namedGenericSbbUsageMBeanProxies;

}
