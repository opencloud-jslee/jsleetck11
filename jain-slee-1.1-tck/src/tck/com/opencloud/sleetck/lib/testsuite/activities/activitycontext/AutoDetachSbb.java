/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.activities.activitycontext;

import java.util.HashMap;

import javax.slee.ActivityContextInterface;
import javax.slee.ActivityEndEvent;
import javax.slee.Address;
import javax.slee.facilities.ActivityContextNamingFacility;
import javax.slee.facilities.Level;
import javax.slee.nullactivity.NullActivity;
import javax.slee.nullactivity.NullActivityContextInterfaceFactory;
import javax.slee.nullactivity.NullActivityFactory;

import com.opencloud.sleetck.lib.resource.events.TCKResourceEventX;
import com.opencloud.sleetck.lib.sbbutils.BaseTCKSbb;
import com.opencloud.sleetck.lib.sbbutils.TCKSbbUtils;

/**
 * 1. Send Event X1 to SBB from Test harness. 2. Create new NullActivity, attach
 * SBB to it, end the NullActivity. 3. In the ActivityEndEvent handler, fire a
 * custom event on the original activity. 4. In the custom event handler make
 * sure the created activity is not attached to the SBB.
 */

public abstract class AutoDetachSbb extends BaseTCKSbb {

    /**
     * This event is delivered to the SBB when an activity the SBB is attached
     * to is ended. After delivering this event the SLEE should detach the SBB
     * from the activity.
     */
    public void onActivityEndEvent(ActivityEndEvent event, ActivityContextInterface aci) {

        try {

            TCKSbbUtils.createTrace(getSbbID(), Level.FINE,    "Second Activity name: " + getSecondActivityName(), null);
            TCKSbbUtils.createTrace(getSbbID(), Level.FINE,    "First Activity name: " + getFirstActivityName(), null);

            // Check if this is the second activity context.
            ActivityContextNamingFacility facility = (ActivityContextNamingFacility) TCKSbbUtils.getSbbEnvironment().lookup("slee/facilities/activitycontextnaming");

            ActivityContextInterface secondAci = facility.lookup(getSecondActivityName());
            ActivityContextInterface firstAci = facility.lookup(getFirstActivityName());

            // If it's the first ACI, unbind the two names.
            if (firstAci.getActivity().equals(aci.getActivity())) {
                // facility.unbind(getSecondActivityName());
                facility.unbind(getFirstActivityName());
                return;
            }

            // Ensure that this event was fired on the right ACI - we don't need
            // to do anything with
            // the end of the original activity.
            if (secondAci.getActivity().equals(aci.getActivity())) {
                TCKSbbUtils.createTrace(getSbbID(), Level.FINE,    "Received ActivityEndEvent on the second Activity.",null);

                // Fire a custom event on the first Activity to cause it to
                // check test failure/success.
                fireCustomEvent(new AutoDetachEvent(), firstAci, null);
                return;
            }

        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }
    }

    /**
     * The first event received by the SBB. The SBB will create a new Activity
     * and attach itself to it, and then it will end the activity.
     */

    public void onTCKResourceEventX1(TCKResourceEventX event, ActivityContextInterface aci) {

        try {

            TCKSbbUtils.createTrace(getSbbID(), Level.FINE,    "Received TCKResourceEventX1.", null);

            // Store a record of the name of the first Activity.
            setFirstActivityName("AutoDetachTest");
            setSecondActivityName("AutoDetachTestSecond");

            ActivityContextNamingFacility facility = (ActivityContextNamingFacility) TCKSbbUtils.getSbbEnvironment().lookup("slee/facilities/activitycontextnaming");
            facility.bind(aci, getFirstActivityName());

            // Create a NullActivity.
            NullActivityFactory factory = (NullActivityFactory) TCKSbbUtils.getSbbEnvironment().lookup("slee/nullactivity/factory");
            NullActivity nullActivity = factory.createNullActivity();

            // Get its Activity Context Interface.
            NullActivityContextInterfaceFactory aciFactory = (NullActivityContextInterfaceFactory) TCKSbbUtils.getSbbEnvironment().lookup("slee/nullactivity/activitycontextinterfacefactory");
            ActivityContextInterface nullACI = aciFactory.getActivityContextInterface(nullActivity);

            // Attach to the NullActivity.
            nullACI.attach(getSbbContext().getSbbLocalObject());
            facility.bind(nullACI, getSecondActivityName());

            // End the NullActivity.
            ((NullActivity) nullACI.getActivity()).endActivity();

        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }

    }

    public void onCustomEvent(AutoDetachEvent event, ActivityContextInterface origAci) {

        // Figure out if we're still attached to the created activity or not.

        try {

            TCKSbbUtils.createTrace(getSbbID(), Level.FINE, "Received customEvent.", null);

            HashMap map = new HashMap();
            ActivityContextInterface acis[] = getSbbContext().getActivities();
            if (acis.length != 1) {
                map.put("Result", new Boolean(false));
                map.put("Message", "SBB is still attached to Activity.");
                TCKSbbUtils.getResourceInterface().sendSbbMessage(map);
                return;
            }

            map.put("Result", new Boolean(true));
            map.put("Message", "Ok");
            TCKSbbUtils.getResourceInterface().sendSbbMessage(map);
            return;
        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }
    }

    public abstract void fireCustomEvent(AutoDetachEvent event, ActivityContextInterface aci, Address address);

    public abstract void setFirstActivityName(String name);

    public abstract String getFirstActivityName();

    public abstract void setSecondActivityName(String name);

    public abstract String getSecondActivityName();

}
