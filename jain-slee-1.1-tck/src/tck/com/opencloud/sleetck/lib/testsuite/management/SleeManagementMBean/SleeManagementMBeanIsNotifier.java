/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.management.SleeManagementMBean;

import com.opencloud.sleetck.lib.AbstractSleeTCKTest;
import com.opencloud.sleetck.lib.SleeTCKTestUtils;
import com.opencloud.sleetck.lib.SleeTCKTest;
import com.opencloud.sleetck.lib.TCKTestResult;
import com.opencloud.sleetck.lib.TCKTestFailureException;
import com.opencloud.sleetck.lib.TCKTestErrorException;
import com.opencloud.sleetck.lib.testutils.ExceptionsUtil;
import com.opencloud.sleetck.lib.testutils.jmx.SleeManagementMBeanProxy;
import javax.slee.management.SleeState;
import javax.management.ObjectName;
import javax.management.NotificationBroadcaster;

/**
 * Tests assertion 4445, that the SleeManagementMBean implements the
 * javax.management.NotificationBroadcaster interface.
 */
public class SleeManagementMBeanIsNotifier implements SleeTCKTest {

    public void init(SleeTCKTestUtils utils) { this.utils=utils; }
    public void setUp() {}
    public void tearDown() {}

    public TCKTestResult run() throws Exception {
        ObjectName mBeanName = utils.getSleeManagementMBeanName();
        String interfaceName = NotificationBroadcaster.class.getName();
        utils.getLog().info("Checking that "+mBeanName+" implements "+interfaceName);
        if(!utils.getMBeanFacade().isInstanceOf(mBeanName,interfaceName)) {
            return TCKTestResult.failed(4445,"SleeManagementMBean "+mBeanName+" does not implement "+interfaceName+
                " according to MBeanServer.isInstanceOf()");
        } else {
            return TCKTestResult.passed();
        }
    }

    private SleeTCKTestUtils utils;

}