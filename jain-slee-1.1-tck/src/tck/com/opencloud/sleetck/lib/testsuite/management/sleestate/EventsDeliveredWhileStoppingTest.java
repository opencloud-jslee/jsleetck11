/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.management.sleestate;

import com.opencloud.sleetck.lib.*;
import com.opencloud.sleetck.lib.resource.TCKActivityID;
import com.opencloud.sleetck.lib.resource.events.TCKResourceEventX;
import com.opencloud.sleetck.lib.resource.testapi.TCKResourceTestInterface;
import com.opencloud.sleetck.lib.testutils.BaseTCKResourceListener;
import com.opencloud.sleetck.lib.testutils.SleeStarter;
import com.opencloud.sleetck.lib.testutils.jmx.SleeManagementMBeanProxy;

import javax.slee.management.ManagementException;
import javax.slee.management.SleeState;
import java.rmi.RemoteException;
import java.util.HashSet;
import java.util.Set;

/**
 * Tests that events are delivered on existing activities when the SLEE is in the Stopping state.
 */
public class EventsDeliveredWhileStoppingTest extends AbstractSleeTCKTest {

    // -- Constants -- //

    // -- Implementation of SleeTCKTest methods -- //

    public TCKTestResult run() throws Exception {
        try {
            resourceListener.resetState();
            getLog().info("Firing an initial event on an activity to attach an Sbb to the activity");
            activityID = resource.createActivity("tck.CreateActivityWhileStoppingTest.Activity");
            synchronized(resourceListener) {
                x1EventObjectID = resource.fireEvent(TCKResourceEventX.X1,null,activityID,null);
            }
            try {
                // Wait for the Sbb to receive the event (and therfore become attached to the activity)
                resourceListener.waitForACKOrFailure(EventsDeliveredWhileStoppingTestConstants.SBB1_RECEIVED_X1);
                getLog().info("The Sbb is attached to the activity");
            } catch (OperationTimedOutException ex) {
                throw new TCKTestErrorException("Timed out while waiting for the Sbb to receive the first event");
            }
            SleeManagementMBeanProxy management = utils().getSleeManagementMBeanProxy();
            getLog().info("Calling SleeManagementMBean.stop()");
            management.stop();
            getLog().info("Current state: "+management.getState());

            // execute the test cases
            testEventDelivery(TCKResourceEventX.X2,EventsDeliveredWhileStoppingTestConstants.SBB1_RECEIVED_X2,
                1982,"Fire an event on an existing activity to an Sbb entity created in the SLEE_RUNNING state");

        } finally {
            getLog().info("Ending the activity used to stall the transition to the STOPPING state");
            endActivitySilent(activityID);
            getLog().info("Restarting the SLEE");
            SleeStarter.startSlee(utils().getSleeManagementMBeanProxy(),utils().getTestTimeout());
        }
        return TCKTestResult.passed();
    }

    public void setUp() throws Exception {
        resource = utils().getResourceInterface();
        resourceListener = new ResourceListenerImpl();
        setResourceListener(resourceListener);
        super.setUp();
    }

    public void tearDown() throws Exception {
        super.tearDown();
        resourceListener = null;
        resource = null;
    }

    // Private methods

    /**
     * Tests the delivery of the given event, then returns whether the test should continue
     */
    private boolean testEventDelivery(String eventTypeName, int expectedACKCode, int assertionID, String testCase) throws Exception {
        if(checkStoppingState()) {
            getLog().info("Executing test case: "+testCase);
            getLog().info("Firing a "+eventTypeName+" event");
            synchronized(resourceListener) {
                secondEventTypeName = eventTypeName;
                secondEventCallCode = expectedACKCode;
            }
            resource.fireEvent(eventTypeName,null,activityID,null);
            try {
                resourceListener.waitForACKOrFailure(expectedACKCode);
                getLog().info("Received ACK for event");
                return true;
            } catch (OperationTimedOutException ex) {
                if(resourceListener.hasActivityEnded()) {
                    getLog().warning("The event was not delivered, because the activity on which "+
                    "the event was fired was ended by the SLEE");
                    return false; // exit the test, as the activity has ended
                } else {
                    throw new TCKTestFailureException(assertionID,
                        "Timed out while waiting for an event to be delivered in the "+
                        "STOPPING state. The activity on which it was fired has not yet been ended. Test case: "+testCase,ex);
                }
            }
        } else return false;
    }

    /**
     * Checks the integrity of the current SLEE state, then returns whether or not
     * the SLEE is in the STOPPING state.
     * This method throws an Exception if it detects that the SLEE is in an invalid state.
     */
    private boolean checkStoppingState() throws TCKTestErrorException, TCKTestFailureException, ManagementException {
        SleeState currentState = utils().getSleeManagementMBeanProxy().getState();
        if(currentState.isStopping()) {
            return true;
        } else {
            if(currentState.isStopped()) {
                if(!resourceListener.hasActivityEnded()) {
                    throw new TCKTestFailureException(1886,"The SLEE moved to the STOPPED state before all activities were ended "+
                        "(no callback was received to indicate that the activity context had been moved to the invalid state)");
                } else {
                    getLog().warning("The SLEE ended the activity context, then transitioned to the STOPPED state. "+
                        "This prevents the continuation of this test, but it is valid behaviour for the SLEE.");
                    return false;
                }
            } else throw new TCKTestErrorException("The SLEE is not in the STOPPING or STOPPED state. "+
                "Current state="+currentState+". This indicates either a call to start the SLEE "+
                "by a third party, or an illegal stontaneous state transition by the SLEE.");
        }
    }

    /**
     * Attempts the end the given activity, if live and not null,
     * and logs any Exceptions caught
     */
    private void endActivitySilent(TCKActivityID activityToEnd) {
        if(activityToEnd != null) {
            try {
                if(resource.isLive(activityToEnd)) {
                    resource.endActivity(activityToEnd);
                }
            } catch (Exception ex) {
                getLog().warning("Received Exception while trying to end TCKActivity "+activityToEnd+". Exception:");
                getLog().warning(ex);
            }
        }
    }

    private String formatCode(int sbbCallCode) {
        switch (sbbCallCode) {
            case EventsDeliveredWhileStoppingTestConstants.SBB1_RECEIVED_X1: return "SBB1_RECEIVED_X1";
            case EventsDeliveredWhileStoppingTestConstants.SBB1_RECEIVED_X2: return "SBB1_RECEIVED_X2";
            default: return "(Unrecognized Sbb call code: "+sbbCallCode+")";
        }
    }

    // -- Private classes -- //

    private class ResourceListenerImpl extends BaseTCKResourceListener {

        // -- TCKResourceListener methods -- //

        public synchronized Object onSbbCall(Object argument) throws Exception {
            try {
                Integer callCode = ((Integer)argument);
                getLog().info("Received event ACK from Sbb. ACK type:"+formatCode(callCode.intValue()));
                receivedACKs.add(callCode);
                notifyAll();
            } catch (Exception ex) {
                onException(ex);
            }
            return null;
        }

        public synchronized void onEventProcessingSuccessful(long eventObjectID) throws RemoteException {
            getLog().info("onEventProcessingSuccessful(eventObjectID="+eventObjectID+")");
            if(eventObjectID != x1EventObjectID && !isSecondEventACKReceived()) {
                onSecondEventNotDelivered();
            }
        }

        public synchronized void onEventProcessingFailed(long eventObjectID, String message, Exception exception) {
            getLog().info("onEventProcessingFailed(eventObjectID="+eventObjectID+", message="+message);
            if(eventObjectID == x1EventObjectID) {
                StringBuffer buf = new StringBuffer("Received onEventProcessingFailed() callback for X1 event");
                if(message != null) buf.append(message);
                onException(new TCKTestErrorException(buf.toString(),exception));
            } else if(!isSecondEventACKReceived()) {
                onSecondEventNotDelivered();
            }
        }

        public synchronized void onActivityContextInvalid(TCKActivityID activityID) {
            if(activityID.equals(activityID)) {
                hasActivityEnded = true;
                getLog().info("ResourceListenerImpl: received notification of activity end for the activity");
            } else onException(new TCKTestErrorException("Received unexpected onActivityContextInvalid() callback for activity: "+activityID));
        }

        public synchronized void onException(Exception e) {
            utils().getLog().warning("Received Exception from resource:");
            utils().getLog().warning(e);
            if(failureOrError != null) failureOrError = e;
            notifyAll();
        }

        // -- Introduced methods -- //

        /**
         * Waits until one of the following
         * (1) the given ACK is received
         * (2) a Failure or Error is received via the resource
         * (3) the timeout is reached
         * An Exception is thrown in cases (2) and (3)
         */
        public synchronized void waitForACKOrFailure(int code) throws Exception {
            Integer wrappedCode = new Integer(code);
            long now = System.currentTimeMillis();
            long timeoutAt = now + utils().getTestTimeout();
            while(now < timeoutAt && !receivedACKs.contains(wrappedCode) && failureOrError == null) {
                try {
                    wait(timeoutAt - now);
                } catch (InterruptedException ie) { /* no-op */ }
                now = System.currentTimeMillis();
            }
            if(failureOrError != null) throw failureOrError;
            if(!receivedACKs.contains(wrappedCode)) throw new OperationTimedOutException(
                "Timed out while waiting for ACK from Sbb. Expected ACK code: "+formatCode(code));
        }

        /**
         * Returns true if and only if the listener has received notification that the stalling TCK activity
         * has ended
         */
        public synchronized boolean hasActivityEnded() {
            return hasActivityEnded;
        }

        public synchronized boolean isSecondEventACKReceived() {
            return receivedACKs.contains(new Integer(secondEventCallCode));
        }

        /**
         * Resets the hasActivityEnded flag and clears the receivedACKs set
         */
        public synchronized void resetState() {
            hasActivityEnded = false;
            receivedACKs.clear();
        }

        /**
         * This method is called when onEventProcessingFailed() or onEventProcessingSuccessful() is called
         * for the second event before ACK is received from the second event handler
         */
        private synchronized void onSecondEventNotDelivered() {
            if(hasActivityEnded()) {
                getLog().info("Event delivery failed for "+secondEventTypeName+" event, after the stalling "+
                    "activity ended. Allow this: as the SLEE may have stopped after the activity ended. "+
                    "Will allow the test to timeout...");
            } else {
                failureOrError = new TCKTestErrorException("Event delivery failed for "+secondEventTypeName+" event, "+
                    "before the stalling activity ended");
            }
        }

        private boolean hasActivityEnded;
        private Set receivedACKs = new HashSet();
        private Exception failureOrError;

    }

    // -- Private state -- //

    private ResourceListenerImpl resourceListener;
    private TCKResourceTestInterface resource;
    private TCKActivityID activityID;
    private long x1EventObjectID;
    private String secondEventTypeName;
    private int secondEventCallCode;

}
