/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.resource;

import java.util.HashMap;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.slee.ActivityContextInterface;
import javax.slee.CreateException;
import javax.slee.RolledBackContext;
import javax.slee.Sbb;
import javax.slee.SbbContext;
import javax.slee.facilities.Tracer;

import com.opencloud.sleetck.lib.rautils.TCKRAConstants;
import com.opencloud.sleetck.lib.rautils.UOID;

/**
 * Basic SBB class extended by various test SBBs.
 */
public abstract class BaseResourceSbb implements Sbb {

    //
    // Sbb
    //

    public void sbbActivate() {
    }

    public void sbbCreate() throws CreateException {
    }

    public void sbbExceptionThrown(Exception exception, Object event, ActivityContextInterface aci) {
    }

    public void sbbLoad() {
    }

    public void sbbPassivate() {
    }

    public void sbbPostCreate() throws CreateException {
    }

    public void sbbRemove() {
    }

    public void sbbRolledBack(RolledBackContext context) {
    }

    public void sbbStore() {
    }

    public void setSbbContext(SbbContext context) {
        this.sbbContext = context;
        this.tracer = context.getTracer("SimpleSBB");
    }

    public void unsetSbbContext() {
        tracer = null;
        sbbContext = null;
    }

    //
    // Protected
    //

    protected void detach(ActivityContextInterface aci) {
        aci.detach(sbbContext.getSbbLocalObject());
    }
    
    protected SbbContext getSbbContext() {
        return sbbContext;
    }

    protected SimpleSbbInterface getSbbInterface() throws NamingException {
        return (SimpleSbbInterface) getSbbEnvironment().lookup(TCKRAConstants.SIMPLE_RESOURCE_ADAPTOR_LOCATION);
    }

    protected SimpleActivityContextInterfaceFactory getACIFactory() throws NamingException {
        return (SimpleActivityContextInterfaceFactory) getSbbEnvironment().lookup(TCKRAConstants.SIMPLE_ACI_FACTORY_LOCATION);
    }
    
    protected Context getSbbEnvironment() throws NamingException {
        Context initCtx = new InitialContext();
        return (Context) initCtx.lookup("java:comp/env");
    }

    protected void sendSbbMessage(UOID sbbUID, int sequenceID, int methodID, HashMap results) {
        try {
            SimpleSbbInterface sbbInterface = getSbbInterface();
            sbbInterface.sendSbbMessage(new TCKMessage(sbbUID, sequenceID, methodID, results));
        } catch (Exception e) {
            tracer.severe("Exception caught while trying to send message from SBB to TCK", e);
        }
    }

    protected Tracer tracer;

    private SbbContext sbbContext;
}
