/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.profiles.profiletxcontext;

/**
 * Define some CMP fields for the CheckProfileStore profile
 */
public interface CheckProfileTXProfileCMP {

    // Values set from the test client

    public String getValue2();
    public void setValue2(String value2);

    public String getValue3();
    public void setValue3(String value3);

    // The values as seen in the management method setValue3ViaManagementMethod()

    public String getValue2InManagementMethod();
    public void setValue2InManagementMethod(String value2InManagementMethod);
    
    // The values as seen in profileStore()

    public String getValue1InProfileStore();
    public void setValue1InProfileStore(String value1InProfileStore);

    public String getValue2InProfileStore();
    public void setValue2InProfileStore(String value2InProfileStore);

    public String getValue3InProfileStore();
    public void setValue3InProfileStore(String value3InProfileStore);

    // Set to make the commit fail in profileVerify()

    public boolean getValid();
    public void setValid(boolean valid);

}
