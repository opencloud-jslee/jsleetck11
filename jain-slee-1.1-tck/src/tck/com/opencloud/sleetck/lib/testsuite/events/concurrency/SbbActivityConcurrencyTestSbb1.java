/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.events.concurrency;

import com.opencloud.sleetck.lib.resource.events.TCKResourceEventX;
import com.opencloud.sleetck.lib.resource.events.TCKResourceEventY;
import com.opencloud.sleetck.lib.resource.events.TCKResourceEvent;

import javax.slee.ActivityContextInterface;
import javax.slee.RolledBackContext;

public abstract class SbbActivityConcurrencyTestSbb1 extends BaseSbbActivityConcurrencyTestSbb {

    public void onTCKResourceEventX1(TCKResourceEventX event, ActivityContextInterface aci) {
        callTest(SbbActivityConcurrencyTestConstants.SBB1_RECEIVED_X1,"TCKResourceEventX1");
    }

    public void onTCKResourceEventX2(TCKResourceEventX event, ActivityContextInterface aci) {
        callTest(SbbActivityConcurrencyTestConstants.SBB1_RECEIVED_X2,"TCKResourceEventX2");
    }

    public void onTCKResourceEventY1(TCKResourceEventY event, ActivityContextInterface aci) {
        callTest(SbbActivityConcurrencyTestConstants.SBB1_RECEIVED_Y1,"TCKResourceEventY1");
    }

    public void sbbRolledBack(RolledBackContext context) {
        Object event = context.getEvent();
        if(event instanceof TCKResourceEvent && TCKResourceEventX.X2.equals(((TCKResourceEvent)event).getEventTypeName())) {
            callTest(SbbActivityConcurrencyTestConstants.SBB1_RECEIVED_X2_ROLL_BACK,"sbbRolledBack() for TCKResourceEventX2");
        } else super.sbbRolledBack(context);
    }

}