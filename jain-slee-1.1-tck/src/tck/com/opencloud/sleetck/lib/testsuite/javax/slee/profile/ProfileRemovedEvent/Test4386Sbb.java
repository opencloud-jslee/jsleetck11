/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.javax.slee.profile.ProfileRemovedEvent;

import com.opencloud.sleetck.lib.resource.events.TCKResourceEventX;
import com.opencloud.sleetck.lib.sbbutils.BaseTCKSbb;
import com.opencloud.sleetck.lib.sbbutils.TCKSbbUtils;

import javax.naming.InitialContext;
import javax.slee.ActivityContextInterface;
import javax.slee.Address;
import javax.slee.profile.*;
import java.util.HashMap;

public abstract class Test4386Sbb extends BaseTCKSbb {

    private static final String JNDI_PROFILEFACILITY_NAME = "java:comp/env/slee/facilities/profile";
    public static final String PROFILE_TABLE_NAME = "Test4386ProfileTable";
    public static final String PROFILE_NAME = "Test4386Profile";

    // Initial event
    public void onTCKResourceEventX1(TCKResourceEventX ev, ActivityContextInterface aci) {

        try {

            ProfileFacility facility = (ProfileFacility) new InitialContext().lookup(JNDI_PROFILEFACILITY_NAME);
            ProfileTableActivity activity = facility.getProfileTableActivity(PROFILE_TABLE_NAME);

            ProfileTableActivityContextInterfaceFactory factory = (ProfileTableActivityContextInterfaceFactory) TCKSbbUtils.getSbbEnvironment().lookup("slee/facilities/profiletableactivitycontextinterfacefactory");
            ActivityContextInterface profileTableACI = factory.getActivityContextInterface(activity);
            profileTableACI.attach(getSbbContext().getSbbLocalObject());

            fireTest4386Event(new Test4386Event(), aci, null);
        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }

    }

    public void onTest4386Event(Test4386Event ev, ActivityContextInterface aci) {
        try {
            HashMap map = new HashMap();
            map.put("Type", "RemoveProfile");
            TCKSbbUtils.getResourceInterface().sendSbbMessage(map);
        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }

    }

    public void onProfileRemovedEvent(ProfileRemovedEvent event, ActivityContextInterface aci) {
        try {
            HashMap map = new HashMap();

            if (!(event instanceof ProfileRemovedEvent)) {
                map.put("Type", "Result");
                map.put("Result", new Boolean(false));
                map.put("ID", new Integer(4386));
                TCKSbbUtils.getResourceInterface().sendSbbMessage(map);
                return;
            }

            // 4389 - getProfile(), returns the Profile Identifier for the profile that was updated.
            ProfileID profileID = event.getProfile();
            map.put("Type", "ProfileID");
            map.put("ProfileID", profileID);
            TCKSbbUtils.getResourceInterface().sendSbbMessage(map);

            map = new HashMap();

            // 4751/4752 - getProfileAddress()
            Address address = event.getProfileAddress();
        if (!(address.getAddressPlan().isSleeProfile())) {
            map.put("Type", "Result");
            map.put("Result", new Boolean(false));
            map.put("ID", new Integer(4752));
            map.put("Message", "ProfileRemovedEvent.getProfileAddress() returned an Address with the wrong address plan:"+address.getAddressPlan());
            TCKSbbUtils.getResourceInterface().sendSbbMessage(map);
            return;
        }
        String expectedAddressString = PROFILE_TABLE_NAME+"/"+PROFILE_NAME;
        if (!(address.getAddressString().equals(expectedAddressString))) {
            map.put("Type", "Result");
            map.put("Result", new Boolean(false));
            map.put("ID", new Integer(4751));
            map.put("Message", "The address returned by ProfileRemovedEvent.getProfileAddress() had an incorrect address string. "+
                    "Excpected: "+expectedAddressString+". Returned: "+address.getAddressString());
            TCKSbbUtils.getResourceInterface().sendSbbMessage(map);
            return;
        }

            map.put("Type", "Result");
            map.put("Result", new Boolean(true));
            map.put("Message", "OK");
            TCKSbbUtils.getResourceInterface().sendSbbMessage(map);
            return;
        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }
    }

    public abstract void fireTest4386Event(Test4386Event event, ActivityContextInterface aci, Address address);

    public abstract Test4386ProfileCMP getProfileCMP(ProfileID profileID) throws UnrecognizedProfileTableNameException, UnrecognizedProfileNameException;
}
