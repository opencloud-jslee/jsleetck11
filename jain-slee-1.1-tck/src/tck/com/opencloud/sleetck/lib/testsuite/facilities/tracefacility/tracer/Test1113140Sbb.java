/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.facilities.tracefacility.tracer;

import javax.slee.ActivityContextInterface;
import javax.slee.facilities.TraceLevel;
import javax.slee.facilities.Tracer;

import com.opencloud.sleetck.lib.resource.events.TCKResourceEventX;
import com.opencloud.sleetck.lib.sbbutils.BaseTCKSbb;
import com.opencloud.sleetck.lib.sbbutils.TCKSbbUtils;

/*
 * AssertionID (1113140): Test the message argument. This argument specifies 
 * the message that will be placed into the message attribute of the 
 * TraceNotification object emitted by the TraceMBean object for the trace 
 * message.
 */
public abstract class Test1113140Sbb extends BaseTCKSbb {
    public static final String TRACE_MESSAGE = "Test1113140TraceMessage";

    // Initial event
    public void onTCKResourceEventX1(TCKResourceEventX event, ActivityContextInterface aci) {

        try {

            doTest1113140Test();

        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }
    }

    private void doTest1113140Test() throws Exception {
        Tracer tracer = null;
        // 1113140
        try {
            tracer = getSbbContext().getTracer("");
            if (tracer.isTraceable(TraceLevel.INFO)) {
                tracer.trace(TraceLevel.INFO, TRACE_MESSAGE);
                tracer.trace(TraceLevel.INFO, TRACE_MESSAGE, null);
            }
        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }
    }

}
