/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.sbb.activitycontext;

import java.util.HashMap;

import javax.naming.InitialContext;
import javax.slee.ActivityContextInterface;
import javax.slee.EventContext;
import javax.slee.profile.AddressProfileLocal;
import javax.slee.profile.ProfileFacility;
import javax.slee.profile.ProfileLocalObject;
import javax.slee.profile.ProfileTable;

import com.opencloud.sleetck.lib.resource.events.TCKResourceEventX;
import com.opencloud.sleetck.lib.sbbutils.BaseTCKSbb;
import com.opencloud.sleetck.lib.sbbutils.TCKSbbUtils;

public abstract class Test1106032Sbb extends BaseTCKSbb {

    /*
     * This is the initial event.
     * 
     * Store the value of a profile in my CMP field. 
     */
    public void onTCKResourceEventX1(TCKResourceEventX ev, ActivityContextInterface aci, EventContext eventContext) {
        HashMap map;
        ProfileLocalObject myProfile;
        ProfileFacility facility;
        ProfileTable pt;

        map = new HashMap();

        try {
            facility = (ProfileFacility) new InitialContext().lookup(ProfileFacility.JNDI_NAME);
            pt = facility.getProfileTable(Test1106032Test.PROFILE_TABLE_NAME);
            myProfile = (AddressProfileLocal)pt.find(Test1106032Test.PROFILE_NAME);

            setMyProfile(myProfile);
        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }

        map.put("Result", new Boolean(true));
        map.put("Message", "Profile found and stored in CMP field.");
        sendSbbMessage(map);
        return;

    }

    /*
     * Make sure that the value of my CMP field is null.
     * (The profile has been removed by the test).
     * 
     */
    public void onTCKResourceEventX2(TCKResourceEventX ev, ActivityContextInterface aci, EventContext eventContext) {
        HashMap map;
        map = new HashMap();
        ProfileLocalObject myProfile;
        
        myProfile = getMyProfile();
        if (null != myProfile) {
            map.put("Result", new Boolean(false));
            map.put("Message", "My profile in a CMP field was not null.");
            sendSbbMessage(map);
            return;
        }
        map.put("Result", new Boolean(true));
        map.put("Message", "Test passed.");
        sendSbbMessage(map);
        return;
    }

    public abstract void setMyProfile(ProfileLocalObject object);
    public abstract ProfileLocalObject getMyProfile();

    private void sendSbbMessage(HashMap map) {
        try {
            TCKSbbUtils.getResourceInterface().sendSbbMessage(map);
        } catch(Exception e) {
            TCKSbbUtils.handleException(e);
        }
    }
}

