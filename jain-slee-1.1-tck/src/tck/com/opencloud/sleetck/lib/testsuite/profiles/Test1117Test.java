/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.profiles;

import com.opencloud.sleetck.lib.AbstractSleeTCKTest;
import com.opencloud.sleetck.lib.TCKTestResult;
import com.opencloud.sleetck.lib.infra.SleeTCKComponentConstants;
import com.opencloud.sleetck.lib.testsuite.profiles.profileverification.IndexedProfileManagementProxy;
import com.opencloud.sleetck.lib.testutils.Assert;
import com.opencloud.sleetck.lib.testutils.ComponentIDLookup;
import com.opencloud.sleetck.lib.testutils.ProfileUtils;
import com.opencloud.sleetck.lib.testutils.jmx.ProfileProvisioningMBeanProxy;

import javax.management.ObjectName;
import javax.slee.profile.ProfileSpecificationID;
import javax.slee.profile.UnrecognizedProfileNameException;
import javax.slee.profile.UnrecognizedProfileTableNameException;
import java.util.Iterator;
import java.util.Vector;

public class Test1117Test extends AbstractSleeTCKTest {

    private static final String PROFILE_TABLE_NAME = "tck.Test1117Test.table";

    public TCKTestResult run() throws Exception {
        // create the profile tables
        ProfileProvisioningMBeanProxy profileProvisioning = profileUtils.getProfileProvisioningProxy();
        ProfileSpecificationID profileSpecID = new ComponentIDLookup(utils()).lookupProfileSpecificationID(
            ProfileTestConstants.SIMPLE_PROFILE_SPEC_NAME,SleeTCKComponentConstants.TCK_VENDOR,"1.0");

        getLog().info("Creating profile table: " + PROFILE_TABLE_NAME);
        profileProvisioning.createProfileTable(profileSpecID, PROFILE_TABLE_NAME);
        tableCreated = true;

        // Tests getDefaultProfile for a profile that exists
        ObjectName jmx = profileUtils.getProfileProvisioningProxy().getDefaultProfile(PROFILE_TABLE_NAME);
        Assert.assertTrue(1117, "getDefaultProfile returned null, expected the ObjectName for the default profile of " +
                                "the specified table",
                          jmx != null);
        Assert.assertTrue(1117, "getDefaultProfile returned the ObjectName of a profile MBean that is not registered " +
                                "with the MBean server",
                          utils().getMBeanFacade().isRegistered(jmx));

        // Tests getProfiles for profiles that exist
        jmx = profileUtils.getProfileProvisioningProxy().createProfile(PROFILE_TABLE_NAME, "A");
        getProfileProxy(jmx).commitProfile();
        ObjectName jmx2 = profileUtils.getProfileProvisioningProxy().getProfile(PROFILE_TABLE_NAME, "A");

        Assert.assertTrue(1117, "getProfile returned null, expecting the ObjectName of the specified profile",
                          jmx2 != null);
        Assert.assertTrue(1117, "createProfile returned the ObjectName of a profile MBean that is not registered " +
                                "with the MBean server",
                          utils().getMBeanFacade().isRegistered(jmx));
        Assert.assertTrue(1117, "getProfile returned the ObjectName of a profile MBean that is not registered " +
                                "with the MBean server",
                          utils().getMBeanFacade().isRegistered(jmx2));

        jmx = profileUtils.getProfileProvisioningProxy().createProfile(PROFILE_TABLE_NAME, "B");
        getProfileProxy(jmx).commitProfile();
        ObjectName jmx3 = profileUtils.getProfileProvisioningProxy().getProfile(PROFILE_TABLE_NAME, "B");
        Assert.assertTrue(1117, "getProfile returned identical ObjectName's for different profiles",
                          !jmx2.equals(jmx3));

        // Test getDefaultProfile for a profile that does not exist
        try {
            profileUtils.getProfileProvisioningProxy().getDefaultProfile("non-existant_table");
            Assert.fail(1117, "getDefaultProfile should throw UnrecognizedProfileTableNameException if invoked with a " +
                              "non-existant profile table");
        } catch (UnrecognizedProfileTableNameException e) { }

        // Test getProfile for a profile that does not exist
        try {
            profileUtils.getProfileProvisioningProxy().getProfile("non-existant_table", "A");
            Assert.fail(1117, "getProfile should throw UnrecognizedProfileTableNameException if invoked with a non-existant " +
                              "profile table");
        } catch (UnrecognizedProfileTableNameException e) { }

        try {
            profileUtils.getProfileProvisioningProxy().getProfile(PROFILE_TABLE_NAME, "X");
            Assert.fail(1117, "getProfile should throw UnrecognizedProfileNameException if invoked with a non-existant " +
                              "profile table");
        } catch (UnrecognizedProfileNameException e) { }

        return TCKTestResult.passed();
    }

    public void setUp() throws Exception {
        String duPath = utils().getTestParams().getProperty(ProfileTestConstants.PROFILE_SPEC_DU_PATH_PARAM);
        getLog().fine("Installing profile specification");
        utils().install(duPath);
        profileUtils = new ProfileUtils(utils());
        activeProxies = new Vector();
    }

    public void tearDown() throws Exception {
        // close profiles
        if (activeProxies != null) {
            getLog().fine("Closing profiles");
            Iterator activeProxiesIter = activeProxies.iterator();
            while (activeProxiesIter.hasNext()) {
                IndexedProfileManagementProxy aProxy = (IndexedProfileManagementProxy) activeProxiesIter.next();
                try {
                    if (aProxy != null) {
                        if (aProxy.isProfileWriteable()) aProxy.restoreProfile();
                        aProxy.closeProfile();
                    }
                } catch (Exception ex) {
                    getLog().warning("Exception caught while trying to close profiles:");
                    getLog().warning(ex);
                }
            }
        }
        // delete profile tables
        try {
            if (profileUtils != null && tableCreated) {
                getLog().fine("Removing profile table");
                profileUtils.removeProfileTable(PROFILE_TABLE_NAME);
            }
        } catch (Exception e) {
            getLog().warning("Caught exception while trying to remove profile table:");
            getLog().warning(e);
        }
        super.tearDown();
    }

    private IndexedProfileManagementProxy getProfileProxy(ObjectName mbeanName) {
        IndexedProfileManagementProxy rProxy = new IndexedProfileManagementProxy(mbeanName, utils().getMBeanFacade());
        activeProxies.addElement(rProxy);
        return rProxy;
    }


    private ProfileUtils profileUtils;
    private boolean tableCreated = false;
    private Vector activeProxies;
}
