/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.transactions;

import java.util.HashMap;

import javax.slee.resource.ConfigProperties;
import javax.slee.resource.ResourceAdaptorID;

import com.opencloud.sleetck.lib.AbstractSleeTCKTest;
import com.opencloud.sleetck.lib.TCKTestFailureException;
import com.opencloud.sleetck.lib.TCKTestResult;
import com.opencloud.sleetck.lib.infra.SleeTCKComponentConstants;
import com.opencloud.sleetck.lib.profileutils.BaseMessageAdapter;
import com.opencloud.sleetck.lib.rautils.MessageHandlerRegistry;
import com.opencloud.sleetck.lib.rautils.RMIObjectChannel;
import com.opencloud.sleetck.lib.testutils.FutureResult;
import com.opencloud.sleetck.lib.testutils.jmx.ResourceManagementMBeanProxy;

/**
 *
 */
public abstract class TransactionsBaseTest extends AbstractSleeTCKTest {

    private static final String RA_VENDOR = SleeTCKComponentConstants.TCK_VENDOR;
    private static final String RA_VERSION = "1.1";

    private static final String RESOURCE_DU_PATH_PARAM = "RADUPath";

    public abstract String getRAName();

    public TCKTestResult run() throws Exception {

        String raName = getRAName();
        String raEntityName = raName + "_Entity";

        futureResult = new FutureResult(getLog());

        ResourceManagementMBeanProxy resourceMBean = utils().getResourceManagementMBeanProxy();

        // Create the RA Entity.
        ResourceAdaptorID raID = new ResourceAdaptorID(raName, RA_VENDOR, RA_VERSION);
        ConfigProperties configProperties = new ConfigProperties();
        resourceMBean.createResourceAdaptorEntity(raID, raEntityName, configProperties);

        // Activate the RA Entity
        resourceMBean.activateResourceAdaptorEntity(raEntityName);

        HashMap map = new HashMap();
        map.put("Type", new Integer(1));

        //send message to RA entity which starts doing the TXN tests.
        out.sendMessage(map);

        return futureResult.waitForResult(utils().getTestTimeout());
    }

    public void tearDown() throws Exception {
        try {
            in.clearQueue();

            utils().removeRAEntities();
        } finally {
            super.tearDown();
        }

    }

    protected void setPassed(int assertionID, String msg) {
        getLog().fine(assertionID+": "+msg);
        if (futureResult!=null && !futureResult.isSet())
            futureResult.setPassed();
    }

    protected void setFailed(int assertionID, String msg, Exception e) {
        if (e==null) {
            getLog().fine("FAILURE: "+assertionID+":"+msg);
            if (futureResult!=null && !futureResult.isSet())
                futureResult.setFailed(assertionID, msg);
        }
        else {
            getLog().fine("FAILURE: "+assertionID+":"+msg);
            getLog().fine(e);
            if (futureResult!=null && !futureResult.isSet())
                futureResult.setFailed(new TCKTestFailureException(assertionID, msg, e));
        }
    }

    protected void setError(String msg, Exception e) {
        if (e==null) {
            getLog().warning(msg);
            if (futureResult!=null && !futureResult.isSet())
                futureResult.setError(msg);
        }
        else {
            getLog().warning(msg);
            getLog().warning(e);
            if (futureResult!=null && !futureResult.isSet())
                futureResult.setError(msg, e);
        }
    }


    public void setUp() throws Exception {
        setupService(RESOURCE_DU_PATH_PARAM);

        in = utils().getRMIObjectChannel();
        in.setMessageHandler(new BaseMessageAdapter(getLog()) {
            public void onSetPassed(int assertionID, String msg) { setPassed(assertionID, msg); }
            public void onSetFailed(int assertionID, String msg, Exception e) { setFailed(assertionID, msg, e); }
            public void onLogCall(String msg) { getLog().fine(msg); }
            public void onSetError(String msg, Exception e) { setError(msg, e); }
        });

        out = utils().getMessageHandlerRegistry();

    }

    private MessageHandlerRegistry out;
    private RMIObjectChannel in;
    private FutureResult futureResult;
}
