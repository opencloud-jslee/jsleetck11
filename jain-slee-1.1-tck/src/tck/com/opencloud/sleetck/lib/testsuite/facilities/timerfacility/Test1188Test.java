/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.facilities.timerfacility;

import com.opencloud.sleetck.lib.AbstractSleeTCKTest;
import com.opencloud.sleetck.lib.TCKTestResult;
import com.opencloud.sleetck.lib.resource.TCKActivityID;
import com.opencloud.sleetck.lib.resource.events.TCKResourceEventX;
import com.opencloud.sleetck.lib.testutils.BaseTCKResourceListener;
import com.opencloud.sleetck.lib.testutils.FutureResult;

import javax.slee.Address;
import javax.slee.AddressPlan;
import java.rmi.RemoteException;

/**
 * Test assertion 1188 :
 */
public class Test1188Test extends AbstractSleeTCKTest {
    public TCKTestResult run() throws Exception {
        testResult = new FutureResult(getLog());
        setResourceListener(new ResourceListener());
        TCKActivityID activity = utils().getResourceInterface().createActivity("Test1188Activity");
        utils().getResourceInterface().fireEvent(TCKResourceEventX.X1, null, activity, new Address(AddressPlan.IP, "1.0.0.1"));
        return testResult.waitForResultOrFail(testTimeout,"Expected timer event was not received",1188);
    }

    public void setUp() throws Exception {
        testTimeout = Integer.parseInt(utils().getTestParams().getProperty("waitPeriodMS"));
        super.setUp();
    }

    public class ResourceListener extends BaseTCKResourceListener {
        public synchronized Object onSbbCall(Object args) {
            Object[] fields = (Object[]) args;
            getLog().info("Message received from SBB: " + fields[0]);
            if (fields[0].equals("onTimerEvent")) {
                if(fields[1].equals(Boolean.TRUE)) {
                    testResult.setFailed(1188, "An activity reference held by the " +
                            "TimerFacility did not prevent reclamation of the activity - " +
                            "the timer was fired on an activity context in the ending state");
                } else {
                    getLog().info("The timer was fired on an activity context which was not in the ending state");
                    testResult.setPassed();
                }
            } else if (fields[0].equals("onActivityEndEvent") && !testResult.isSet()) {
                testResult.setFailed(1188, "An ActivityEndEvent was received before the timer was cancelled");
            }
            return null;
        }

        public void onException(Exception exception) throws RemoteException {
            testResult.setError("An Exception was received from the SBB or the TCK resource",exception);
        }
    }

    private long testTimeout;
    private FutureResult testResult;

}
