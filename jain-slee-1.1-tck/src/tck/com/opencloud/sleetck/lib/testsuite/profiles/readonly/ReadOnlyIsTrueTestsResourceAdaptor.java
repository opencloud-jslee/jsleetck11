/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.profiles.readonly;

import java.rmi.RemoteException;

import com.opencloud.sleetck.lib.rautils.MessageHandler;
import com.opencloud.sleetck.lib.testsuite.profiles.ProfileRAInteractionBaseResourceAdaptor;

/**
 * Resource adaptor which works as Slee-side receptor for TCK test messages as
 * only RA's have access to TXN manager.
 */
public class ReadOnlyIsTrueTestsResourceAdaptor extends ProfileRAInteractionBaseResourceAdaptor {

    protected MessageHandler getMessageHandler() throws RemoteException {
        return new ReadOnlyIsTrueTestsMessageListener(this);
    }
}
