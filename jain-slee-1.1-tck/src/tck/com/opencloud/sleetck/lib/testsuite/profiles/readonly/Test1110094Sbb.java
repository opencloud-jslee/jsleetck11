/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.profiles.readonly;

import java.util.HashMap;

import javax.naming.Context;
import javax.slee.ActivityContextInterface;
import javax.slee.profile.ProfileFacility;
import javax.slee.profile.ProfileTable;

import com.opencloud.sleetck.lib.resource.events.TCKResourceEventX;
import com.opencloud.sleetck.lib.sbbutils.BaseTCKSbb;
import com.opencloud.sleetck.lib.sbbutils.TCKSbbUtils;
import com.opencloud.sleetck.lib.sbbutils.events2.SbbBaseMessageComposer;

public abstract class Test1110094Sbb extends BaseTCKSbb {

    public static final String PROFILE_TABLE_NAME = "Test1110094ProfileTable";
    public static final String PROFILE_NAME = "Test1110094Profile";


    private void sendResult(boolean result, int id, String msg) {
        HashMap map = SbbBaseMessageComposer.getSetResultMsg(result, id, msg);
        try {
            TCKSbbUtils.getResourceInterface().sendSbbMessage(map);
        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }
    }

    public void onTCKResourceEventX1(TCKResourceEventX event, ActivityContextInterface aci) {
        try {
            Context env = TCKSbbUtils.getSbbEnvironment();
            ProfileFacility profileFacility = (ProfileFacility)env.lookup("slee/facilities/profile");

            ProfileTable profileTable = profileFacility.getProfileTable(PROFILE_TABLE_NAME);

            ReadOnlyTestsProfileLocal profileLocal = (ReadOnlyTestsProfileLocal)profileTable.find(PROFILE_NAME);

            //1110094: These business methods may invoke the CMP accessor methods.
            try {
                profileLocal.localSetValue("newValue");
                sendResult(true, 1110094, "Using Get/Set accessors to CMP fields via business method worked as expected.");
            } catch (Exception e) {
                sendResult(false, 1110094, "Exception occurred when trying to use Get/Set accessors to CMP fields via business method: "+e);
                return;
            }

        } catch(Exception e) {
            TCKSbbUtils.handleException(e);
        }
    }
}
