/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.profiles.profiletxcontext;

import javax.slee.profile.ProfileManagement;
import javax.slee.profile.ProfileVerificationException;

/**
 *
 */
public abstract class CheckProfileTXProfileManagementImpl
        implements ProfileManagement, CheckProfileTXProfileCMP, CheckProfileTXProfileManagement {

    // ProfileManagement methods

    public void profileInitialize() {
        // Provide the 'clean' state of the profile
        setValue2(INITIAL_VALUE);
        setValue3(INITIAL_VALUE);
        setValue2InManagementMethod(INITIAL_VALUE);
        setValue1InProfileStore(INITIAL_VALUE);
        setValue2InProfileStore(INITIAL_VALUE);
        setValue3InProfileStore(INITIAL_VALUE);
        setValid(true);
    }

    public void profileLoad() {
        value1 = CHANGED_VALUE;
    }

    public void profileStore() {
        setValue1InProfileStore(value1);
        setValue2InProfileStore(getValue2());
        setValue3InProfileStore(getValue3());
    }

    public void profileVerify() throws ProfileVerificationException {
        if (!getValid()) {
            throw new ProfileVerificationException("Profile marked as invalid, intentionally. " +
                    "(Expect to see this Exception once during the test)");
        }
    }

    // CheckProfileTXProfileManagement methods

    public void setValue3ViaManagementMethod(String value3) {
        setValue3(value3);
        setValue2InManagementMethod(getValue2());
    }

    private String value1 = INITIAL_VALUE;

}
