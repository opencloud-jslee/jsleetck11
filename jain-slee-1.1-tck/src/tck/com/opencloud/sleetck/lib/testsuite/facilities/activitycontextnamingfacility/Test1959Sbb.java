/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.facilities.activitycontextnamingfacility;

import com.opencloud.sleetck.lib.resource.TCKActivityID;
import com.opencloud.sleetck.lib.resource.events.TCKResourceEventX;
import com.opencloud.sleetck.lib.resource.sbbapi.TCKActivity;
import com.opencloud.sleetck.lib.resource.sbbapi.TCKActivityContextInterfaceFactory;
import com.opencloud.sleetck.lib.sbbutils.BaseTCKSbb;
import com.opencloud.sleetck.lib.sbbutils.TCKSbbConstants;
import com.opencloud.sleetck.lib.sbbutils.TCKSbbUtils;

import javax.slee.ActivityContextInterface;
import javax.slee.ActivityEndEvent;
import javax.slee.facilities.ActivityContextNamingFacility;
import javax.slee.facilities.Level;
import java.util.HashMap;

public abstract class Test1959Sbb extends BaseTCKSbb {

    public static final String ACTIVITY_NAME = "SbbCreatedTest1959TCKActivity";

    public void onActivityEndEvent(ActivityEndEvent event, ActivityContextInterface aci) {
        try {
            TCKSbbUtils.createTrace(getSbbID(), Level.FINE, "In onActivityEndEvent.", null);
        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }
    }

    // This event is sent by the test after receiving confirmation that the activity has ended
    public void onTCKResourceEventX2(TCKResourceEventX ev, ActivityContextInterface aci) {
        try {
            TCKSbbUtils.createTrace(getSbbID(), Level.FINE, "In onTCKResourceEventX2()", null);

            ActivityContextNamingFacility facility = (ActivityContextNamingFacility) TCKSbbUtils.getSbbEnvironment().lookup("slee/facilities/activitycontextnaming");

            HashMap map = new HashMap();
            if (facility.lookup(ACTIVITY_NAME) == null) {
                map.put("Result", new Boolean(true));
                map.put("Message", "Ok");
            } else {
                map.put("Result", new Boolean(false));
                map.put("Message", "ActivityContextNamingFacility did not release its reference to Activity Context after the Activity had been ended.");

                facility.unbind(ACTIVITY_NAME);
            }

            TCKSbbUtils.getResourceInterface().sendSbbMessage(map);
        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }

    }

    // Initial event
    public void onTCKResourceEventX1(TCKResourceEventX ev, ActivityContextInterface aci) {

        try {
            TCKSbbUtils.createTrace(getSbbID(), Level.FINE, "In onTCKResourceEventX1()", null);

            // Create a TCKActivity and get its ActivityContextInterface
            TCKActivityID tckActivityID = TCKSbbUtils.getResourceInterface().createActivity(ACTIVITY_NAME);
            TCKActivity tckActivity = TCKSbbUtils.getResourceInterface().getActivity(tckActivityID);
            TCKActivityContextInterfaceFactory aciFactory = (TCKActivityContextInterfaceFactory) TCKSbbUtils.getSbbEnvironment().lookup(TCKSbbConstants.TCK_ACI_FACTORY_LOCATION);
            ActivityContextInterface tckActivityACI = aciFactory.getActivityContextInterface(tckActivity);

            // Bind it in the ACNF
            ActivityContextNamingFacility facility = (ActivityContextNamingFacility) TCKSbbUtils.getSbbEnvironment().lookup("slee/facilities/activitycontextnaming");
            facility.bind(tckActivityACI, ACTIVITY_NAME);

            // Attach to the created TCKActivity's ACI.
            tckActivityACI.attach(getSbbContext().getSbbLocalObject());

            // End the create TCKActivity - expect to receive an ActivityEndEvent.
            tckActivity.endActivity();

            TCKSbbUtils.createTrace(getSbbID(), Level.FINE, "Ended TCKActivity.", null);

        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }

    }

}
