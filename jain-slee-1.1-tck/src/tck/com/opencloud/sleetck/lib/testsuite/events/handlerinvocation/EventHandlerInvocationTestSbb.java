/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.events.handlerinvocation;

import com.opencloud.sleetck.lib.sbbutils.BaseTCKSbb;
import com.opencloud.sleetck.lib.sbbutils.TCKSbbUtils;
import com.opencloud.sleetck.lib.resource.events.TCKResourceEventX;
import com.opencloud.sleetck.lib.resource.sbbapi.TCKActivity;
import javax.slee.ActivityContextInterface;
import javax.slee.facilities.Level;

public abstract class EventHandlerInvocationTestSbb extends BaseTCKSbb {

    public void onTCKResourceEventX1(TCKResourceEventX event, ActivityContextInterface aci) {
        handleEvent(event,TCKResourceEventX.X1);
    }

    public void onTCKResourceEventX2(TCKResourceEventX event, ActivityContextInterface aci) {
        handleEvent(event,TCKResourceEventX.X2);
    }

    private void handleEvent(TCKResourceEventX event, String eventTypeName) {
        try {
            TCKSbbUtils.createTrace(getSbbID(),Level.FINE,"Received "+eventTypeName+" event",null);
            // send a reply: the event type received and the event type as stored in the message
            String[] message = new String[]{eventTypeName,(String)event.getMessage()};
            TCKSbbUtils.getResourceInterface().sendSbbMessage(message);
        } catch(Exception e) {
            TCKSbbUtils.handleException(e);
        }
    }

}