/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.management.ProfileProvisioningMBean;

import javax.slee.profile.ProfileManagement;
import javax.slee.profile.ProfileVerificationException;

public abstract class Test1649ProfileManagement implements ProfileManagement, Test1649ProfileCMP {

    public void profileInitialize() {
        setAttributeA(true);
        setAttributeB(new String [] { "Foo", "Bar", "Baz" });
        setAttributeC("Foo");
        setAttributeD(new boolean [] { true, false });
    }

    public void profileLoad() {}
    public void profileStore() {}
    public void profileVerify() throws ProfileVerificationException {}
}
