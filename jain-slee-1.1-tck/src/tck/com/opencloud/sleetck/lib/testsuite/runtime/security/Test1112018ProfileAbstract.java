/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.runtime.security;

import java.util.PropertyPermission;

import com.opencloud.sleetck.lib.TCKTestErrorException;
import com.opencloud.sleetck.lib.TCKTestFailureException;


public abstract class Test1112018ProfileAbstract extends BaseSecurityProfile implements Test1112018ProfileCMP {
    private final static int TEST_ID = 1112018;

    public void businessMethod() throws TCKTestFailureException, TCKTestErrorException {
        // Isolate-security-permissions is True.
        checkGrant(new PropertyPermission("tcktest.profile", "write"), TEST_ID);

        checkPermissions(TEST_ID);
    }
}
