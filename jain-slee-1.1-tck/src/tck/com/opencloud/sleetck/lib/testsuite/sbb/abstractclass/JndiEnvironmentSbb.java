/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.sbb.abstractclass;

import com.opencloud.sleetck.lib.sbbutils.TCKSbbUtils;
import com.opencloud.sleetck.lib.resource.events.TCKResourceEventX;

import java.util.HashMap;

import javax.slee.*;
import javax.slee.facilities.*;
import javax.slee.profile.*;
import javax.slee.nullactivity.*;

import javax.naming.*;

/**
 * Test legal reentrancy from s6.15.1.1.1
 */
public abstract class JndiEnvironmentSbb extends SendResultsSbb {
    public void onTCKResourceEventX1(TCKResourceEventX event, ActivityContextInterface aci) {
        try {
            Context context = (Context)new InitialContext().lookup("java:comp/env");

            checkJndi(context, "slee/facilities/timer", TimerFacility.class, 1409);
            checkJndi(context, "slee/facilities/alarm", AlarmFacility.class, 1410);
            checkJndi(context, "slee/facilities/trace", TraceFacility.class, 1411);
            checkJndi(context, "slee/facilities/activitycontextnaming", ActivityContextNamingFacility.class, 1413);
            checkJndi(context, "slee/facilities/profile", ProfileFacility.class, 1414);
            checkJndi(context, "slee/facilities/profiletableactivitycontextinterfacefactory", ProfileTableActivityContextInterfaceFactory.class, 1415);
            checkJndi(context, "slee/nullactivity/factory", NullActivityFactory.class, 623);
            checkJndi(context, "slee/nullactivity/activitycontextinterfacefactory", NullActivityContextInterfaceFactory.class, 624);

            setResultPassed("jndi environment tests passed");
        }
        catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }
    }

    private void checkJndi(Context context, String name, Class expectedType, int assertionID) {
        try {
            try {
                Object o = context.lookup(name);
                if (expectedType.isAssignableFrom(o.getClass())) {
                    TCKSbbUtils.createTrace(getSbbID(), Level.INFO, "jndi name " + name + " returned correct object type", null);
                }
                else {
                    setResultFailed(assertionID, "expected type of object returned from jndi name " + name + " was " + expectedType.getName() + ", but got object of type " + o.getClass().getName());
                }
            }
            catch (NameNotFoundException nnfe) {
                setResultFailed(assertionID, "jndi name " + name + " not found (" + nnfe + ")");
            }
        }
        catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }
    }
}

