/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.usage.sets;

import com.opencloud.sleetck.lib.TCKTestResult;
import com.opencloud.sleetck.lib.testsuite.usage.common.GenericUsageMBeanLookup;
import com.opencloud.sleetck.lib.testsuite.usage.common.GenericUsageTest;
import com.opencloud.sleetck.lib.testsuite.usage.common.UsageMBeanLookup;
import com.opencloud.sleetck.lib.testutils.jmx.ServiceUsageMBeanProxy;

import javax.slee.InvalidArgumentException;
import javax.slee.SbbID;
import javax.slee.management.UsageParameterSetNameAlreadyExistsException;

/**
 * Checks that createUsageParameterSet() throws an InvalidArgumentException for
 * a zero length parameter set name or an SbbID of an Sbb which does not define
 * a usage parameter interface, and that it throws a NullPointerException for
 * a null parameter set name or a null SbbID.
 */
public class Test2294and4558Test extends GenericUsageTest {

    private static final int TEST_ID_INVALID_ARG  = 2294;
    private static final int TEST_ID_NULL_POINTER = 4558;
    private static final int TEST_ID_DUPLICATE_NAME = 4561;

    public TCKTestResult run() throws Exception {

        GenericUsageMBeanLookup genericMBeanLookup = getGenericUsageMBeanLookup();
        ServiceUsageMBeanProxy genericServiceUsageMBeanProxy = genericMBeanLookup.getServiceUsageMBeanProxy();
        try {
            // zero length name
            genericServiceUsageMBeanProxy.createUsageParameterSet(genericMBeanLookup.getSbbID(), "");
            return TCKTestResult.failed(TEST_ID_INVALID_ARG,"createUsageParameterSet() failed to throw the expected "+
                    "InvalidArgumentException with a zero length parameter set name");
        } catch (InvalidArgumentException e) {
            getLog().info("Caught expected InvalidArgumentException from createUsageParameterSet() after passing a "+
                    "zero length parameter set name: "+e);
        }

        UsageMBeanLookup test2294UsageMBeanLookup = null;
        try {
            // sbb with no usage parameter interface
            test2294UsageMBeanLookup = new UsageMBeanLookup("Test2294Service","Test2294Sbb",utils());
            ServiceUsageMBeanProxy test2294ServiceUsageMBeanProxy = test2294UsageMBeanLookup.getServiceUsageMBeanProxy();
            SbbID test2294SbbID = test2294UsageMBeanLookup.getSbbID();
            test2294ServiceUsageMBeanProxy.createUsageParameterSet(test2294SbbID, "foo");
            return TCKTestResult.failed(TEST_ID_INVALID_ARG,"createUsageParameterSet() failed to throw the expected "+
                    "InvalidArgumentException with the SbbID of an Sbb which does not define a usage parameter interface");
        } catch (InvalidArgumentException e) {
            getLog().info("Caught expected InvalidArgumentException from createUsageParameterSet() after passing the SbbID "+
                    "of an Sbb which does not define a usage parameter interface: "+e);
        } finally {
            if(test2294UsageMBeanLookup != null)test2294UsageMBeanLookup.closeAllMBeans();
        }

        // null parameter set name
        try {
            genericServiceUsageMBeanProxy.createUsageParameterSet(genericMBeanLookup.getSbbID(), null);
            return TCKTestResult.failed(TEST_ID_NULL_POINTER,"createUsageParameterSet() failed to throw the expected "+
                    "NullPointerException with a null parameter set name");
        } catch (NullPointerException e) {
            getLog().info("Caught expected NullPointerException from createUsageParameterSet() after passing "+
                    "a null parameter set name: "+e);
        }

        // null SbbID
        try {
            genericServiceUsageMBeanProxy.createUsageParameterSet(null, "foo");
            return TCKTestResult.failed(TEST_ID_NULL_POINTER,"createUsageParameterSet() failed to throw the expected "+
                    "NullPointerException with a null SbbID");
        } catch (NullPointerException e) {
            getLog().info("Caught expected NullPointerException from createUsageParameterSet() after passing a null SbbID: "+e);
        }

        // duplicate parameter set name
        String duplicateName = "duplicateName";
        genericServiceUsageMBeanProxy.createUsageParameterSet(genericMBeanLookup.getSbbID(), duplicateName);
        try {
            genericServiceUsageMBeanProxy.createUsageParameterSet(genericMBeanLookup.getSbbID(), duplicateName);
            return TCKTestResult.failed(TEST_ID_DUPLICATE_NAME,"createUsageParameterSet() failed to throw the expected "+
                    "UsageParameterSetNameAlreadyExistsException with a parameter set name which has already been used "+
                    "for the same SBB");
        } catch (UsageParameterSetNameAlreadyExistsException e) {
            getLog().info("Caught expected UsageParameterSetNameAlreadyExistsException from createUsageParameterSet() "+
                    "after passing a parameter set name which has already been used for the same SBB");
        }

        return TCKTestResult.passed();

    }

    public void setUp() throws Exception {
        super.setUp();
        setupService("serviceDUPath-2294", true);
    }

}
