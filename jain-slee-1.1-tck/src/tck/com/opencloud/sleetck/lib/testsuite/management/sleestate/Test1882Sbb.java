/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.management.sleestate;

import com.opencloud.sleetck.lib.resource.events.TCKResourceEventX;
import com.opencloud.sleetck.lib.sbbutils.BaseTCKSbb;
import com.opencloud.sleetck.lib.sbbutils.TCKSbbUtils;

import javax.slee.ActivityContextInterface;
import javax.slee.CreateException;
import javax.slee.facilities.Level;

/**
 * This Sbb will callback to the test in it's onCreate callback.  The test will use the callback to check that Sbb
 * Entities are not created while the SLEE is in STARTING state.
 */
public abstract class Test1882Sbb extends BaseTCKSbb {
    public void sbbCreate() throws CreateException {
        try {
            TCKSbbUtils.createTrace(getSbbID(), Level.INFO, "sbbCreate - enter -", null);
            TCKSbbUtils.getResourceInterface().callTest(null);
            TCKSbbUtils.createTrace(getSbbID(), Level.INFO, "sbbCreate - exit -", null);
        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }
    }

    public void onTCKResourceEventX1(TCKResourceEventX event, ActivityContextInterface aci) {
    }
}
