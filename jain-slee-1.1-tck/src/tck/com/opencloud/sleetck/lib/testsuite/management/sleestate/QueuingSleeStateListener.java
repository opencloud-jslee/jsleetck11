/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.management.sleestate;

import com.opencloud.sleetck.lib.SleeTCKTestUtils;
import com.opencloud.sleetck.lib.TCKTestResult;
import com.opencloud.sleetck.lib.OperationTimedOutException;
import com.opencloud.logging.Logable;
import javax.management.Notification;
import javax.management.NotificationListener;
import javax.slee.management.SleeState;
import javax.slee.management.SleeStateChangeNotification;
import java.rmi.RemoteException;
import java.util.LinkedList;

/**
 * An implementation of the NotificationListener interface which
 * privides a queue, and a blocking interface for querying the queue.
 */
public class QueuingSleeStateListener implements NotificationListener {

    public QueuingSleeStateListener(SleeTCKTestUtils utils) {
        this.utils = utils;
        queue = new LinkedList();
    }

    // -- Implementation of NotificationListener -- //

    /**
     * Adds the given Notification to the queue
     */
    public synchronized void handleNotification(Notification notification, Object handback) {
        if(notification instanceof SleeStateChangeNotification) {
            // log the state change
            SleeStateChangeNotification stateChange = (SleeStateChangeNotification)notification;
            SleeState newState = stateChange.getNewState();
            SleeState previousState = stateChange.getOldState();
            utils.getLog().fine("StateNotificationsListener:received state change to "+newState+" from "+previousState);
            // add to the queue
            queue.addLast(notification);
            notify();
        }
    }

    // -- Blocking interface -- //

    /**
     * Blocks until a Notification is found in the queue, then returns the Notification.
     * @return the next Notification in the queue
     * @throws OperationTimedOutException if the default timeout is reached while waiting
     */
    public synchronized SleeStateChangeNotification nextNotification() throws OperationTimedOutException {

        long now = System.currentTimeMillis();
        long timeoutAt = now + utils.getTestTimeout();

        while (now < timeoutAt) {
            while (queue.isEmpty() && now < timeoutAt) {
                try {
                    wait(timeoutAt - now);
                } catch(InterruptedException ie) { /* no-op*/ }
                now = System.currentTimeMillis();
            }
            if (!queue.isEmpty()) {
                return (SleeStateChangeNotification)queue.removeFirst();
            }
        }
        throw new OperationTimedOutException("Timed out waiting for a Notification");
    }

    /**
     * Returns whether or not the queue currently has any items in it
     */
    public synchronized boolean hasNotifications() {
        return !queue.isEmpty();
    }

    // -- Private state -- //

    private final SleeTCKTestUtils utils;
    private final LinkedList queue;

}
