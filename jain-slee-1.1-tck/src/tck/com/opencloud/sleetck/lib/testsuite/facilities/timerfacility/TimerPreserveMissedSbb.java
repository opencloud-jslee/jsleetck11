/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.facilities.timerfacility;

import com.opencloud.sleetck.lib.sbbutils.BaseTCKSbb;
import com.opencloud.sleetck.lib.sbbutils.TCKSbbUtils;
import com.opencloud.sleetck.lib.resource.events.TCKResourceEventX;
import com.opencloud.sleetck.lib.resource.events.TCKResourceEventY;
import com.opencloud.sleetck.lib.resource.sbbapi.TCKActivity;

import javax.naming.Context;
import javax.naming.InitialContext;

import java.util.HashMap;

import javax.slee.*;
import javax.slee.facilities.*;
import javax.slee.nullactivity.*;

/**
 * Test for correct behaviour using the various TimerPreserveMissed options.
 * Note we can only test for correct ALL or _LAST behaviour,
 * since these will deliver all (ALL) or at least one (LAST)
 * timer events. NONE will deliver 0 or more events, untestable.<P>
 * We do not have a way of causing late timer events, but we can make it
 * difficult for the SLEE to deliver events on time by specifying a period
 * of 1ms (the minimum). In any case we should see the correct number of
 * events.
 */
public abstract class TimerPreserveMissedSbb extends BaseTCKSbb {

    public void setSbbContext(SbbContext context) {
        super.setSbbContext(context);
        try {
            Context myEnv = (Context) new InitialContext().lookup("java:comp/env");

            timerFacility = (TimerFacility) myEnv.lookup("slee/facilities/timer");
        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }
    }

    // test starts here...
    public void onTCKResourceEventX1(TCKResourceEventX event, ActivityContextInterface aci) {
        try {
            TCKSbbUtils.createTrace(getSbbID(),Level.INFO,"Received start message",null);
            setTestName((String)event.getMessage());

            TimerOptions preserveAll = new TimerOptions();
            preserveAll.setPreserveMissed(TimerPreserveMissed.ALL);
            TimerOptions preserveLast = new TimerOptions();
            preserveLast.setPreserveMissed(TimerPreserveMissed.LAST);

            if (getTestName().equals("TimerPreserveMissed-periodic-all")) {
                // expect to see all 5 timer events
                timerFacility.setTimer(aci, null, 0, 1, 5, preserveAll);
            }
            else if (getTestName().equals("TimerPreserveMissed-periodic-last")) {
                // expect to see at least one timer event
                timerFacility.setTimer(aci, null, 0, 1, 5, preserveLast);
            }
            else if (getTestName().equals("TimerPreserveMissed-single-all")) {
                // expect to see exactly one timer event
                timerFacility.setTimer(aci, null, 0, preserveAll);
            }
            else if (getTestName().equals("TimerPreserveMissed-single-last")) {
                // expect to see exactly one timer event
                timerFacility.setTimer(aci, null, 0, preserveLast);
            }

        } catch(Exception e) {
            TCKSbbUtils.handleException(e);
        }
    }

    // this event stops the test (fired by test harness after 10s)
    public void onTCKResourceEventY1(TCKResourceEventY event, ActivityContextInterface aci) {
        try {
            if (getTestName().equals("TimerPreserveMissed-periodic-all")) {
                // expect to see all 5 timer events
                if (getNumTimerEvents() == 5) { // passed
                    setResultPassed("periodic timer with ALL completed successfully");
                }
                else {
                    setResultFailed(1225, "periodic timer with ALL did not preserve all timer events.");
                }
            }
            else if (getTestName().equals("TimerPreserveMissed-periodic-last")) {
                // expect to see at least one timer event
                if ((getNumTimerEvents() >= 1) && (getNumTimerEvents() <= 5)) { // passed
                    setResultPassed("periodic timer with LAST completed successfully");
                }
                else {
                    setResultFailed(1227, "periodic timer with LAST did not preserve at least one timer event.");
                }
            }
            else if (getTestName().equals("TimerPreserveMissed-single-all")) {
                // expect to see exactly one timer event
                if (getNumTimerEvents() == 1) { // passed
                    setResultPassed("single timer with ALL completed successfully");
                }
                else {
                    setResultFailed(1225, "single timer with ALL missed timer event.");
                }
            }
            else if (getTestName().equals("TimerPreserveMissed-single-last")) {
                // expect to see exactly one timer event
                if (getNumTimerEvents() == 1) { // passed
                    setResultPassed("single timer with LAST completed successfully");
                }
                else {
                    setResultFailed(1227, "single timer with LAST missed timer event.");
                }
            }

            sendResultToTCK();

        } catch(Exception e) {
            TCKSbbUtils.handleException(e);
        }
    }

    public void onTimerEvent(TimerEvent event, ActivityContextInterface aci) {
        try {
            TCKSbbUtils.createTrace(getSbbID(),Level.INFO,"Received timer event",null);

            setNumTimerEvents(getNumTimerEvents() + 1);

        } catch(Exception e) {
            TCKSbbUtils.handleException(e);
        }
    }

    public abstract void setTestName(String testName);
    public abstract String getTestName();

    public abstract void setNumTimerEvents(int numTimerEvents);
    public abstract int getNumTimerEvents();

    private void sendResultToTCK() throws Exception {
        HashMap sbbData = new HashMap();
        sbbData.put("testname", getTestName());
        sbbData.put("result", result?"pass":"fail");
        sbbData.put("message", message);
        sbbData.put("id", new Integer(failedAssertionID));
        TCKSbbUtils.getResourceInterface().sendSbbMessage(sbbData);
    }

    private void setResultFailed(int assertionID, String msg) throws Exception {
        result = false;
        failedAssertionID = assertionID;
        message =  "Assertion " + assertionID + " failed: " + msg;
        TCKSbbUtils.createTrace(getSbbID(), Level.WARNING, message, null);
    }

    private void setResultPassed(String msg) throws Exception {
        result = true;
        message = "Success: " + msg;
        TCKSbbUtils.createTrace(getSbbID(), Level.INFO, message, null);
    }

    private TimerFacility timerFacility;

    private boolean result = false;
    private String message;
    private int failedAssertionID = -1;

}
