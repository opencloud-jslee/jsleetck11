/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.usage.profiles;

import javax.management.ObjectName;
import javax.slee.ComponentID;
import javax.slee.management.DeployableUnitDescriptor;
import javax.slee.management.DeployableUnitID;
import javax.slee.profile.ProfileSpecificationID;

import com.opencloud.logging.Logable;
import com.opencloud.sleetck.lib.AbstractSleeTCKTest;
import com.opencloud.sleetck.lib.TCKTestErrorException;
import com.opencloud.sleetck.lib.testutils.ProfileUtils;
import com.opencloud.sleetck.lib.testutils.jmx.DeploymentMBeanProxy;
import com.opencloud.sleetck.lib.testutils.jmx.ProfileMBeanProxy;
import com.opencloud.sleetck.lib.testutils.jmx.ProfileProvisioningMBeanProxy;
import com.opencloud.sleetck.lib.testutils.jmx.ProfileTableUsageMBeanProxy;

public abstract class BaseProfileTest extends AbstractSleeTCKTest  {
    public static final String TCK_SBB_EVENT_DU_PATH_PARAM = "eventDUPath";
    private static final String SERVICE_DU_PATH_PARAM = "DUPath";
    // PARAM_SET_NAME also defined in BaseProfileAbstractClass.
    public static final String PARAM_SET_NAME = "FooParameterSet";

    protected void createProfileTable(String tableName) throws Exception {
        DeploymentMBeanProxy duProxy = utils().getDeploymentMBeanProxy();
        DeployableUnitDescriptor duDesc = duProxy.getDescriptor(duID);
        ComponentID components[] = duDesc.getComponents();

        for (int i = 0; i < components.length; i++) {
            if (components[i] instanceof ProfileSpecificationID) {
                ProfileSpecificationID profSpecID = (ProfileSpecificationID) components[i];
                profileProxy.createProfileTable(profSpecID, tableName);
                log.fine("Created profile table "+tableName+ " for profile specification "+profSpecID.getName()+" "+profSpecID.getVersion()+", "+profSpecID.getVendor());
            }
        }
    }

    protected void removeProfileTable(String tableName) {
        try {
            profileProxy.removeProfileTable(tableName);
        } catch (Exception e) {}
    }

    protected void createProfile(String tableName, String profileName) throws Exception {
        // Create a profile, set some values, then commit it.
        ObjectName profile = profileProxy.createProfile(tableName, profileName);
        ProfileMBeanProxy proxy = utils().getMBeanProxyFactory().createProfileMBeanProxy(profile);
        log.fine("Created profile "+profileName+" for profile table "+tableName);
        proxy.commitProfile();
        proxy.closeProfile();
        log.fine("Committed and closed profile.");
    }

    protected void removeProfile(String profileTableName, String profileName) {
        try {
            profileProxy.removeProfile(profileTableName, profileName);
        } catch (Exception e) {}
    }

    protected void createNamedParameterSet(String tableName) throws TCKTestErrorException {
        try {
            ObjectName profileTableUsageMBeanName = profileProxy.getProfileTableUsageMBean(tableName);
            ProfileTableUsageMBeanProxy p = utils().getMBeanProxyFactory().createProfileTableUsageMBeanProxy(profileTableUsageMBeanName);
            p.createUsageParameterSet(PARAM_SET_NAME);
            p.close();
        } catch (Exception e) {
            throw new TCKTestErrorException("Could not create named parameter set", e);
        }
    }

    protected void removeNamedParameterSet(String tableName) throws TCKTestErrorException {
        try {
            ObjectName profileTableUsageMBeanName = profileProxy.getProfileTableUsageMBean(tableName);
            ProfileTableUsageMBeanProxy p = utils().getMBeanProxyFactory().createProfileTableUsageMBeanProxy(profileTableUsageMBeanName);
            p.removeUsageParameterSet(PARAM_SET_NAME);
            p.close();
        } catch (Exception e) {
            throw new TCKTestErrorException("Could not remove named parameter set", e);
        }
    }


    public void setUp() throws Exception {
        log = utils().getLog();
        super.setupService(TCK_SBB_EVENT_DU_PATH_PARAM);

        duID = super.setupService(SERVICE_DU_PATH_PARAM);
        profileProxy = new ProfileUtils(utils()).getProfileProvisioningProxy();
    }

    protected ProfileProvisioningMBeanProxy profileProxy;
    protected DeployableUnitID duID;
    protected Logable log;
}
