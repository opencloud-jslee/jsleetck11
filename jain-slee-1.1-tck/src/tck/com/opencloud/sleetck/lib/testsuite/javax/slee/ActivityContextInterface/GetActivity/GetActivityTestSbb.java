/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.javax.slee.ActivityContextInterface.GetActivity;

import com.opencloud.sleetck.lib.sbbutils.BaseTCKSbb;
import com.opencloud.sleetck.lib.sbbutils.TCKSbbUtils;
import com.opencloud.sleetck.lib.resource.sbbapi.TCKActivity;
import com.opencloud.sleetck.lib.resource.events.TCKResourceEventX;

import javax.slee.ActivityContextInterface;
import javax.slee.facilities.Level;

import java.util.HashMap;

public abstract class GetActivityTestSbb extends BaseTCKSbb {

    public void onTCKResourceEventX1(TCKResourceEventX event, ActivityContextInterface aci) {
        Object activity = null;
        HashMap map = new HashMap();

        try {
            TCKSbbUtils.createTrace(getSbbID(), Level.FINE, "Received event.", null);
            activity = aci.getActivity();
            if (activity == null) {
                map.put("Result", new Boolean(false));
                map.put("Message", "Returned activity was null");
                TCKSbbUtils.getResourceInterface().sendSbbMessage(map);
                return;
            }

            if (!(activity instanceof TCKActivity)) {
                map.put("Result", new Boolean(false));
                map.put("Message", "Returned activity was not a TCKActivity, therefore was not the "+
                    "activity on which the event was fired");
                TCKSbbUtils.getResourceInterface().sendSbbMessage(map);
                return;
            }

            // Send a response to the test.
            map.put("Result", new Boolean(true));
            map.put("Message", "Ok");
            TCKSbbUtils.getResourceInterface().sendSbbMessage(map);
        } catch (Exception e) {
            // Send a response to the test.
            TCKSbbUtils.handleException(e);
        }
    }

}
