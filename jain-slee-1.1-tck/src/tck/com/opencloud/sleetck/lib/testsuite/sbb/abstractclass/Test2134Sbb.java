/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.sbb.abstractclass;

import javax.slee.ActivityEndEvent;
import javax.slee.ActivityContextInterface;
import javax.slee.SbbContext;
import javax.slee.Address;
import javax.slee.SbbLocalObject;
import javax.slee.facilities.Level;
import javax.slee.ChildRelation;
import javax.slee.CreateException;

import com.opencloud.sleetck.lib.sbbutils.BaseTCKSbb;
import com.opencloud.sleetck.lib.sbbutils.TCKSbbUtils;
import com.opencloud.sleetck.lib.sbbutils.TCKSbbConstants;
import com.opencloud.sleetck.lib.resource.events.TCKResourceEventX;
import com.opencloud.sleetck.lib.resource.sbbapi.TCKResourceSbbInterface;

import java.util.HashMap;
import java.util.Iterator;

public abstract class Test2134Sbb extends BaseTCKSbb {


    public void sbbPostCreate() throws javax.slee.CreateException {

        // Create the child relation.
        try {
            getChildRelation().create();
        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }

    }

    // Initial event
    public void onTCKResourceEventX1(TCKResourceEventX ev, ActivityContextInterface aci) {

        try {

            // Disconnect this root SBB from the AC.
            aci.detach(getSbbContext().getSbbLocalObject());


        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }
    }

    public void sbbRemove() {

        try {

            HashMap map = new HashMap();
            map.put("Count", new Integer(1));
            TCKSbbUtils.getResourceInterface().sendSbbMessage(map);

        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }


    }

    // ChildRelation method for the Child SBB.
    public abstract ChildRelation getChildRelation();

}
