/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.events.eventrouting;

import java.util.HashMap;

import javax.slee.ActivityContextInterface;
import javax.slee.EventContext;
import javax.slee.InitialEventSelector;
import javax.slee.facilities.Tracer;

import com.opencloud.sleetck.lib.TCKTestFailureException;
import com.opencloud.sleetck.lib.resource.events.TCKResourceEventX;
import com.opencloud.sleetck.lib.sbbutils.BaseTCKSbb;
import com.opencloud.sleetck.lib.sbbutils.TCKSbbUtils;

/**
 * AssertionID(1108091): Test The initialEvent attribute is set to true.
 */
public abstract class Test1108091Sbb extends BaseTCKSbb {

    public InitialEventSelector initialEventSelectorMethod(InitialEventSelector ies) {
        try {
            // check the event
            TCKResourceEventX event = (TCKResourceEventX) ies.getEvent();
            if (event == null)
                throw new TCKTestFailureException(1108091, "The event variable is null");

            if (!ies.isInitialEvent()) {
                sendResultToTCK(1108091, "The isInitialEvent variable in the InitialEventSelector was set to false",
                        "Test1108091Test", false);
            }

            // set the custom name variable to true to test that this value is not held accross invocations
            ies.setCustomName("test");
            // set the isInitialEvent variable to false to test that this value is not held accross invocations
            ies.setInitialEvent(false);

            sendResultToTCK(1108091, "Test of assertion ID 1108091 passed!", "Test1108091Test", true);

        } catch (Exception ex) {
            TCKSbbUtils.handleException(ex);
        }
        return ies;
    }

    // Initial event method.
    public void onTCKResourceEventX1(TCKResourceEventX event, ActivityContextInterface aci, EventContext context) {
        try {
            tracer = getSbbContext().getTracer("com.test");
            tracer.info("Sbb: Received a TCK event " + event);

        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }
    }

    private void sendResultToTCK(int failedAssertionID, String message, String testName, boolean result)
            throws Exception {
        HashMap sbbData = new HashMap();
        sbbData.put("testname", testName);
        sbbData.put("result", result ? "pass" : "fail");
        sbbData.put("message", message);
        sbbData.put("id", new Integer(failedAssertionID));
        TCKSbbUtils.getResourceInterface().sendSbbMessage(sbbData);
    }

    private Tracer tracer;
}
