/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.javax.slee.profile.ProfileFacility;

import javax.slee.ActivityEndEvent;
import javax.slee.ActivityContextInterface;
import javax.slee.SbbContext;
import javax.slee.Address;
import javax.slee.SbbLocalObject;
import javax.slee.facilities.Level;
import javax.slee.ChildRelation;
import javax.slee.CreateException;
import javax.slee.profile.*;
import javax.slee.InvalidArgumentException;

import com.opencloud.sleetck.lib.sbbutils.BaseTCKSbb;
import com.opencloud.sleetck.lib.sbbutils.TCKSbbUtils;
import com.opencloud.sleetck.lib.sbbutils.TCKSbbConstants;
import com.opencloud.sleetck.lib.resource.events.TCKResourceEventX;
import com.opencloud.sleetck.lib.resource.events.TCKResourceEventY;
import com.opencloud.sleetck.lib.resource.sbbapi.TCKResourceSbbInterface;

import java.util.HashMap;
import java.util.Iterator;

import javax.naming.*;

import javax.slee.facilities.ActivityContextNamingFacility;
import javax.slee.nullactivity.NullActivity;
import javax.slee.nullactivity.NullActivityFactory;
import javax.slee.nullactivity.NullActivityContextInterfaceFactory;

public abstract class Test4459Sbb extends BaseTCKSbb {

    private static final String JNDI_PROFILEFACILITY_NAME = "java:comp/env/slee/facilities/profile";
    public static final String PROFILE_TABLE_NAME = "Test4459ProfileTable";
    public static final String PROFILE_NAME = "Test4459Profile";

    // Initial event
    public void onTCKResourceEventX1(TCKResourceEventX ev, ActivityContextInterface aci) {

        try {
            TCKSbbUtils.createTrace(getSbbID(), Level.FINE, "In TCKResourceEventX1.", null);

            ProfileFacility facility = (ProfileFacility) new InitialContext().lookup(JNDI_PROFILEFACILITY_NAME);

            ProfileID profileID = facility.getProfileByIndexedAttribute(PROFILE_TABLE_NAME, "intVal", new Integer(42));
            HashMap map = new HashMap();
            map.put("Type", "ProfileID");
            map.put("ProfileID", profileID);
            TCKSbbUtils.getResourceInterface().sendSbbMessage(map);
        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }

    }

    public abstract Test4459ProfileCMP getProfileCMP(ProfileID profileID) throws UnrecognizedProfileTableNameException, UnrecognizedProfileNameException;

}
