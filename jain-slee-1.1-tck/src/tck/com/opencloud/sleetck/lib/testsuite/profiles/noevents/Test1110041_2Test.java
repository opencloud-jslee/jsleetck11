/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.profiles.noevents;

import javax.management.Attribute;
import javax.management.ObjectName;
import javax.slee.profile.ProfileAddedEvent;
import javax.slee.profile.ProfileRemovedEvent;
import javax.slee.profile.ProfileSpecificationID;
import javax.slee.profile.ProfileUpdatedEvent;

import com.opencloud.logging.Logable;
import com.opencloud.sleetck.lib.AbstractSleeTCKTest;
import com.opencloud.sleetck.lib.OperationTimedOutException;
import com.opencloud.sleetck.lib.TCKTestResult;
import com.opencloud.sleetck.lib.infra.SleeTCKComponentConstants;
import com.opencloud.sleetck.lib.resource.TCKSbbMessage;
import com.opencloud.sleetck.lib.testutils.ProfileUtils;
import com.opencloud.sleetck.lib.testutils.QueuingResourceListener;
import com.opencloud.sleetck.lib.testutils.jmx.ProfileMBeanProxy;
import com.opencloud.sleetck.lib.testutils.jmx.ProfileProvisioningMBeanProxy;

public class Test1110041_2Test extends AbstractSleeTCKTest{
    private static final String PROFILE_TABLE_NAME = "Test1110041_2ProfileTable";
    private static final String PROFILE_NAME = "Test1110041_2Profile";
    private static final String SERVICE_DU_PATH_PARAM = "DUPath";

    private static final String PROFILE_SPEC_NAME = "Test1110041ProfileNoEvents";
    private static final String PROFILE_SPEC_VERSION = "1.0";


    /**
     * This test tests the profile spec attribute 'profile-events-enabled' set to 'false'.
     * A profile is created, updated, removed if an ACK message from the Sbb in form of
     * the full event class name is received the test fails, if sth else is received the test errors.
     */
    public TCKTestResult run() throws Exception {
        Logable log = utils().getLog();

        // create the profile tables
       ProfileSpecificationID profSpecID = new ProfileSpecificationID(PROFILE_SPEC_NAME, SleeTCKComponentConstants.TCK_VENDOR, PROFILE_SPEC_VERSION);
       profileProvisioning.createProfileTable(profSpecID, PROFILE_TABLE_NAME);
       log.fine("Created profile table "+PROFILE_TABLE_NAME+ " for profile specification "+profSpecID.getName()+" "+profSpecID.getVersion()+", "+profSpecID.getVendor());

        // -- Add profile to the profile table and expect the SBB NOT to receive ProfileAddedEvent -- //
        getLog().fine("Adding profile "+PROFILE_NAME+" to the profile table");
        ObjectName profile = profileProvisioning.createProfile(PROFILE_TABLE_NAME,PROFILE_NAME);
        ProfileMBeanProxy profProxy = utils().getMBeanProxyFactory().createProfileMBeanProxy(profile);
        profProxy.commitProfile();
        // wait if a ProfileAddedEvent is received by the SBB
        try {
            TCKSbbMessage reply = resourceListener.nextMessage();

            if(ProfileAddedEvent.class.getName().equals(reply.getMessage()))
                return TCKTestResult.failed(1110041,"Sbb received ProfileAddedEvent though ProfileEvents are switched off in profile spec.");
            else
                return TCKTestResult.error("Received unexpected reply from SBB after adding a profile. Message="+reply.getMessage());

        } catch (OperationTimedOutException ex) {
            log.fine("No ProfileAddedEvent received by Sbb as expected.");
        }

        // -- Update the profile and expect the SBB to NOT receive a ProfileUpdatedEvent -- //
        profile = profileProvisioning.getProfile(PROFILE_TABLE_NAME, PROFILE_NAME);
        profProxy = utils().getMBeanProxyFactory().createProfileMBeanProxy(profile);
        //Change value to new value '42'
        profProxy.editProfile();
        utils().getMBeanFacade().setAttribute(profile, new Attribute("Value", new Integer(42)));
        log.fine("Set 'value' to '42'");
        profProxy.commitProfile();
        // wait if a ProfileUpdatedEvent is received by the SBB
        try {
            TCKSbbMessage reply = resourceListener.nextMessage();

            if(ProfileUpdatedEvent.class.getName().equals(reply.getMessage()))
                return TCKTestResult.failed(1110041,"Sbb received ProfileUpdatedEvent though ProfileEvents are switched off in profile spec.");
            else
                return TCKTestResult.error("Received unexpected reply from SBB after updating a profile. Message="+reply.getMessage());

        } catch (OperationTimedOutException ex) {
            log.fine("No ProfileUpdatedEvent received by Sbb as expected.");
        }

        // -- Remove the profile from the profile table and expect the SBB to receive the ProfileRemovedEvent -- //
        getLog().fine("Removing profile "+PROFILE_NAME+" from the profile table");
        profileProvisioning.removeProfile(PROFILE_TABLE_NAME,PROFILE_NAME);
        // wait for a ProfileRemovedEvent to be received by the SBB
        try {
            TCKSbbMessage reply = resourceListener.nextMessage();
            if(ProfileRemovedEvent.class.getName().equals(reply.getMessage()))
                return TCKTestResult.failed(1110041,"Sbb received ProfileRemovedEvent though ProfileEvents are switched off in profile spec.");
            else
                return TCKTestResult.error("Received unexpected reply from SBB after removing a profile. Message="+reply.getMessage());

        } catch (OperationTimedOutException ex) {
            log.fine("No ProfileRemovedEvent received by Sbb as expected.");
        }

        return TCKTestResult.passed();
    }

    public void setUp() throws Exception {

        // install profile spec and service
        setupService(SERVICE_DU_PATH_PARAM);

        // set up the resource listener
        resourceListener = new QueuingResourceListener(utils());
        setResourceListener(resourceListener);

        // Create profileUtils and profileProvisioning proxy
        profileUtils = new ProfileUtils(utils());
        profileProvisioning = profileUtils.getProfileProvisioningProxy();
    }

    public void tearDown() throws Exception {
        // remove profile tables
        try {
            profileProvisioning.removeProfileTable(PROFILE_TABLE_NAME);

        } catch (Exception e) {
            getLog().warning("Exception occured when trying to remove profile table: ");
            getLog().warning(e);
        }

        super.tearDown();
    }

    private QueuingResourceListener resourceListener;
    private ProfileUtils profileUtils;
    private ProfileProvisioningMBeanProxy profileProvisioning;
}
