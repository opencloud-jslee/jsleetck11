/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.management.NotificationSource;

import javax.management.Notification;
import javax.management.NotificationListener;
import javax.management.ObjectName;
import javax.slee.facilities.TraceLevel;
import javax.slee.management.DeployableUnitDescriptor;
import javax.slee.management.DeployableUnitID;
import javax.slee.management.NotificationSource;
import javax.slee.management.ProfileTableNotification;
import javax.slee.management.TraceNotification;
import javax.slee.profile.ProfileSpecificationID;

import com.opencloud.sleetck.lib.AbstractSleeTCKTest;
import com.opencloud.sleetck.lib.DescriptionKeys;
import com.opencloud.sleetck.lib.TCKTestResult;
import com.opencloud.sleetck.lib.infra.SleeTCKComponentConstants;
import com.opencloud.sleetck.lib.resource.TCKActivityID;
import com.opencloud.sleetck.lib.resource.events.TCKResourceEventX;
import com.opencloud.sleetck.lib.resource.testapi.TCKResourceTestInterface;
import com.opencloud.sleetck.lib.testutils.FutureResult;
import com.opencloud.sleetck.lib.testutils.ProfileUtils;
import com.opencloud.sleetck.lib.testutils.jmx.DeploymentMBeanProxy;
import com.opencloud.sleetck.lib.testutils.jmx.ProfileProvisioningMBeanProxy;
import com.opencloud.sleetck.lib.testutils.jmx.SleeManagementMBeanProxy;
import com.opencloud.sleetck.lib.testutils.jmx.TraceMBeanProxy;
import com.opencloud.sleetck.lib.testutils.jmx.impl.TraceMBeanProxyImpl;

/*
 * AssertionID (1114235): The ProfileTableNotification class identifies
 * a profile table as the source of a notification.
 *
 */
public class Test1114235Test extends  AbstractSleeTCKTest {

    public static final String SPEC_NAME = "Test1114235Profile";
    public static final String SPEC_VERSION = "1.1";
    public static final String PROFILE_TABLE_NAME = "Test1114235ProfileTable";
    public static final String PROFILE_NAME = "Test1114235Profile";


    /**
     * Perform the actual test.
     */
    public void run(FutureResult result) throws Exception {
        this.result = result;
        expectedTraceNotifications = 1;
        getLog().fine("Running Test 1114235...");
        doTest1114235Test(TraceLevel.INFO);

        synchronized (this) {
            wait(utils().getTestTimeout());
        }

        if ((passed == true) && (expectedTraceNotifications == receivedTraceNotifications))
            result.setPassed();
        else
            result.setFailed(assertionID, "Expected number of trace messages not received, traces were not delivered by " +
                    "the TraceFacility (expected " + expectedTraceNotifications + ", received " + receivedTraceNotifications + ")");

    }

    public void doTest1114235Test(TraceLevel traceLevel) throws Exception {

        //Create a profile table
        try {
            getLog().fine("Creating profile table "+PROFILE_TABLE_NAME);
            ProfileSpecificationID specID = new ProfileSpecificationID(SPEC_NAME, SleeTCKComponentConstants.TCK_VENDOR, SPEC_VERSION);
            profileProvisioning.createProfileTable(specID, PROFILE_TABLE_NAME);
            getLog().fine("Added profile table "+PROFILE_TABLE_NAME);
        }
        catch (Exception e) {
            e.printStackTrace();
            getLog().warning(e);
            TCKTestResult.error("ERROR creating Profile Table", e);
        }


        String activityName = "Test1114235Test";
        TCKResourceTestInterface resource = utils().getResourceInterface();
        TCKActivityID activityID = resource.createActivity(activityName);
        getLog().info("Firing event: " + testName );

        // kickoff the test
        resource.fireEvent(TCKResourceEventX.X1, testName, activityID, null);

    }


    /**
     * Do all the pre-run configuration of the test.
     */

    public void setUp() throws Exception {

        getLog().fine("Connecting to resource");
        getLog().fine("Installing and activating service");

        // Install the Deployable Units
        String duPath = utils().getTestParams().getProperty(DescriptionKeys.SERVICE_DU_PATH_PARAM);
        duID = utils().install(duPath);

        // Activate the DU
        utils().activateServices(duID, true);

        // Setup TraceNotificationListener
        SleeManagementMBeanProxy proxy = utils().getSleeManagementMBeanProxy();
        traceMBeanName = proxy.getTraceMBean();
        listener = new TraceNotificationListenerImpl();

        tracembean = new TraceMBeanProxyImpl(traceMBeanName, utils().getMBeanFacade());
        tracembean.addNotificationListener(listener, null, null);

        DeploymentMBeanProxy duProxy = utils().getDeploymentMBeanProxy();
        DeployableUnitDescriptor duDesc = duProxy.getDescriptor(duID);

        getLog().fine("Creating Profile Utils...");
        profileUtils = new ProfileUtils(utils());
        profileProvisioning = profileUtils.getProfileProvisioningProxy();
    }

    /**
     * Clean up after the test.
     */

    public void tearDown() throws Exception {
        profileProvisioning.removeProfileTable(PROFILE_TABLE_NAME);
        if (null != tracembean)
            tracembean.removeNotificationListener(listener);
        // cleanup
        utils().getLog().fine("Disconnecting from resource");
        utils().getResourceInterface().clearActivities();
        utils().getLog().fine("Deactivating and uninstalling service");
        utils().deactivateAllServices();
        utils().uninstallAll();

    }


    public class TraceNotificationListenerImpl implements NotificationListener {
        public final void handleNotification(Notification notification, Object handback) {

        try {
            if (notification instanceof TraceNotification) {
                TraceNotification traceNotification = (TraceNotification) notification;
                getLog().fine("Received Trace Notification from: " + traceNotification);

                if ((traceNotification.getNotificationSource() instanceof ProfileTableNotification) && (AssertionChecked != true)) {
                    if (traceNotification.getType().equals(ProfileTableNotification.TRACE_NOTIFICATION_TYPE)) {
                        logSuccessfulCheck(1114235);
                        NotificationSource notificationSource =  traceNotification.getNotificationSource();
                        ProfileTableNotification profileTableNotification = (ProfileTableNotification) notificationSource;
                        passed = doNotficationSourcesCheck(profileTableNotification);
                        if (passed == true) {
                            receivedTraceNotifications++;
                            AssertionChecked = true;
                        } else {
                            result.setFailed(1114235, "NotficationSources Check failed");
                        }
                    }
                }
            }
        } catch (Exception e) {
                utils().getLog().warning(e);
                getLog().warning("1114235: FAILED. Received erroneous Trace Message: " + notification.getMessage());
        }

     }

   }

    private boolean doNotficationSourcesCheck(ProfileTableNotification theNotification) {
        boolean passed = true;

        //1114234
        try {
            String notificationStrVal = theNotification.toString();
            utils().getLog().fine("1114234: ProfileTableNotification = "+notificationStrVal);
            logSuccessfulCheck(1114234);
        }
        catch (Exception e) {
            utils().getLog().warning(e);
            result.setFailed(1114234, "toString() failed");
            return false;
        }

           // 1114232
        ProfileTableNotification expectedProfileNotification = new ProfileTableNotification(PROFILE_TABLE_NAME);
        ProfileTableNotification notExpectedProfileNotification = new ProfileTableNotification("NonExistantProfileTable");
        try {
            getLog().fine("1114232: theNotification.equals : "+theNotification + ", " +expectedProfileNotification);
            if (theNotification.equals(expectedProfileNotification)) {
                logSuccessfulCheck(1114232);
            } else {
                result.setFailed(1114232, ".equals() failed");
                passed = false;
            }
        }
        catch (Exception e) {
            getLog().warning(e);
            result.setFailed(1114232, ".equals() failed");
            return false;
        }

        // 1114238
        try {
            int areCompared;
            areCompared  = theNotification.compareTo(expectedProfileNotification);
            if (areCompared == 0) {
                areCompared = theNotification.compareTo(notExpectedProfileNotification);
                getLog().fine("11142238: notExpectedProfileNotification = : " +notExpectedProfileNotification);
                if (areCompared != 0)
                      logSuccessfulCheck(1114238);
                else
                    result.setFailed(1114238, ".compareTo() failed non-equal test");
            } else {
                result.setFailed(1114238, ".compareTo() failed");
                passed = false;
            }
        }
        catch (Exception e) {
            getLog().warning(e);
            result.setFailed(1114238, ".compareTo() failed");
            return false;
        }

        //1114237
        try {
            theNotification.hashCode();
            logSuccessfulCheck(1114237);
        } catch (Exception e) {
            result.setFailed(1114237, ".hashCode() failed");
            return false;
        }

        //1114233
        try {
            String alarmNotify = theNotification.getAlarmNotificationType();
            utils().getLog().fine("1114233: getAlarmNotificationType = "+alarmNotify);
            if (alarmNotify.equals(ProfileTableNotification.ALARM_NOTIFICATION_TYPE)) {
                logSuccessfulCheck(1114233);
            } else {
                result.setFailed(1114233, "getAlarmNotificationType() failed");
                passed = false;
            }
        }
        catch (Exception e) {
            utils().getLog().warning(e);
            result.setFailed(1114233, "getAlarmNotificationType() failed");
            return false;
        }

        //1114236
        try {
            String alarmNotify = theNotification.getTraceNotificationType();
            utils().getLog().fine("1114236: getTraceNotificationType = "+alarmNotify);
            if (alarmNotify.equals(ProfileTableNotification.TRACE_NOTIFICATION_TYPE)) {
                logSuccessfulCheck(1114236);
            } else {
                result.setFailed(1114236, "getTraceNotificationType() failed");
                passed = false;
            }
        }
        catch (Exception e) {
            utils().getLog().warning(e);
            result.setFailed(1114236, "getTraceNotificationType() failed");
            return false;
        }

        //1114231
        try {
            String alarmNotify = theNotification.getUsageNotificationType();
            utils().getLog().fine("1114231: getUsageNotificationType = "+alarmNotify);
            if (alarmNotify.equals(ProfileTableNotification.USAGE_NOTIFICATION_TYPE)) {
                logSuccessfulCheck(1114231);
            } else {
                result.setFailed(1114231, "getUsageNotificationType() failed");
                passed = false;
            }
       }
        catch (Exception e) {
            utils().getLog().warning(e);
            result.setFailed(1114231, "getUsageNotificationType() failed");
            return false;
        }

        return passed;

    }

    private void logSuccessfulCheck(int assertionID) {
        utils().getLog().info("Check for assertion "+assertionID+" OK");
    }

    private boolean AssertionChecked = false;
    private ProfileUtils profileUtils;
    private ProfileProvisioningMBeanProxy profileProvisioning;
    private NotificationListener listener;
    private FutureResult result;
    private DeployableUnitID duID;
    private String testName = "Test1114235";
    private int assertionID;
    private ObjectName traceMBeanName;
    private TraceMBeanProxy tracembean;
    private int receivedTraceNotifications = 0;
    private int expectedTraceNotifications;
    private boolean passed = false;
}
