/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.usage.SbbUsageMBean;

import com.opencloud.sleetck.lib.TCKTestErrorException;
import com.opencloud.sleetck.lib.sbbutils.BaseTCKSbb;
import com.opencloud.sleetck.lib.sbbutils.TCKSbbUtils;

import javax.naming.NamingException;
import javax.slee.facilities.Level;
import javax.slee.usage.UnrecognizedUsageParameterSetNameException;

public abstract class Test2365SbbChildB extends BaseTCKSbb {

    public void doUpdates() throws NamingException, TCKTestErrorException, UnrecognizedUsageParameterSetNameException {
        TCKSbbUtils.createTrace(getSbbID(),Level.INFO,"doUpdates(): applying updates",null);
        getSbbUsageParameterSet(Test2365Sbb.PARAMETER_SET_NAME).sampleFoo(3);
        TCKSbbUtils.createTrace(getSbbID(),Level.INFO,"doUpdates(): applied updates",null);
    }

    public abstract Test2365SbbUsageB getDefaultSbbUsageParameterSet();
    public abstract Test2365SbbUsageB getSbbUsageParameterSet(String name) throws UnrecognizedUsageParameterSetNameException;

}
