/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.usage.scope;

import com.opencloud.sleetck.lib.resource.events.TCKResourceEventX;
import com.opencloud.sleetck.lib.sbbutils.BaseTCKSbb;
import com.opencloud.sleetck.lib.sbbutils.TCKSbbUtils;

import javax.slee.ActivityContextInterface;
import javax.slee.ChildRelation;
import javax.slee.usage.UnrecognizedUsageParameterSetNameException;
import javax.slee.facilities.Level;

public abstract class Test2243Sbb extends BaseTCKSbb {

    public static final String PARAMETER_SET_NAME = "Test2243Test-ParameterSet";

    // Initial event
    public void onTCKResourceEventX1(TCKResourceEventX event, ActivityContextInterface aci) {
        try {
            // update usage parameters
            TCKSbbUtils.createTrace(getSbbID(),Level.INFO,"onTCKResourceEventX1(): updating usage parameters",null);
            getSbbUsageParameterSet(PARAMETER_SET_NAME).incrementFoo(1);
            getSbbUsageParameterSet(PARAMETER_SET_NAME).sampleBar(2);
            // delegate to the child to do its updates
            TCKSbbUtils.createTrace(getSbbID(),Level.INFO,"onTCKResourceEventX1(): asking the child to do its updates",null);
            ((Test2243SbbChildLocal)getChildRelation().create()).doUpdates();
            // send a message back to the test to confirm the updates
            TCKSbbUtils.createTrace(getSbbID(),Level.INFO,"onTCKResourceEventX1(): replying to test",null);
            TCKSbbUtils.getResourceInterface().sendSbbMessage(null);
        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }
    }

    public abstract ChildRelation getChildRelation();

    public abstract Test2243SbbUsage getDefaultSbbUsageParameterSet();
    public abstract Test2243SbbUsage getSbbUsageParameterSet(String name) throws UnrecognizedUsageParameterSetNameException;

}
