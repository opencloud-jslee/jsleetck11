/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.facilities.alarmfacility;

import javax.management.Notification;
import javax.management.NotificationListener;
import javax.slee.facilities.AlarmLevel;
import javax.slee.management.AlarmNotification;
import javax.slee.management.SbbNotification;

import com.opencloud.sleetck.lib.AbstractSleeTCKTest;
import com.opencloud.sleetck.lib.resource.TCKActivityID;
import com.opencloud.sleetck.lib.resource.events.TCKResourceEventX;
import com.opencloud.sleetck.lib.resource.events.TCKResourceEventY;
import com.opencloud.sleetck.lib.resource.testapi.TCKResourceTestInterface;
import com.opencloud.sleetck.lib.testutils.FutureResult;
import com.opencloud.sleetck.lib.testutils.jmx.AlarmMBeanProxy;

/*  
 *  AssertionID(1113512): Test If one or more alarms are cleared by 
 *  this method, the SLEE’s AlarmMBean object emits an alarm notification 
 *  for each cleared alarm with the alarm level set to AlarmLevel.CLEAR.
 *  
 *  AssertionID(1113588): Test This method only affects alarms belonging 
 *  to the notification source associated with the AlarmFacility object instance.
 *
 */

public class Test1113512Test extends AbstractSleeTCKTest {

    public static final String SERVICE1_DU_PATH_PARAM = "service1DUPath";

    public static final String SERVICE2_DU_PATH_PARAM = "service2DUPath";

    /**
     * Perform the actual test.
     */
    public void run(FutureResult result) throws Exception {
        this.result = result;

        String activityName = "Test1113512Test";
        TCKResourceTestInterface resource = utils().getResourceInterface();
        TCKActivityID activityID = resource.createActivity(activityName);

        getLog().info("Firing event: " + TCKResourceEventX.X1);
        resource.fireEvent(TCKResourceEventX.X1, testName, activityID, null);

        getLog().info("Firing event: " + TCKResourceEventY.Y1);
        resource.fireEvent(TCKResourceEventY.Y1, testName, activityID, null);
    }

    /**
     * Do all the pre-run configuration of the test.
     */

    public void setUp() throws Exception {

        getLog().fine("Connecting to resource");
        getLog().fine("Installing and activating service");

        setupService(SERVICE1_DU_PATH_PARAM, true);
        setupService(SERVICE2_DU_PATH_PARAM, true);

        alarmMBeanProxy = utils().getAlarmMBeanProxy();
        listener = new AlarmNotificationListenerImpl();
        alarmMBeanProxy.addNotificationListener(listener, null, null);

    }

    /**
     * Clean up after the test.
     */

    public void tearDown() throws Exception {

        String[] listActiveAlarms = alarmMBeanProxy.getAlarms();
        if (alarmMBeanProxy.getAlarms().length > 0)
            for (int i = 0; i < listActiveAlarms.length; i++)
                alarmMBeanProxy.clearAlarm(listActiveAlarms[i]);

        if (null != alarmMBeanProxy)
            alarmMBeanProxy.removeNotificationListener(listener);
        // cleanup
        super.tearDown();
    }

    public class AlarmNotificationListenerImpl implements NotificationListener {
        public final void handleNotification(Notification notification, Object handback) {

            if (notification instanceof AlarmNotification) {
                AlarmNotification alarmNotification = (AlarmNotification) notification;

                SbbNotification sbbNotification = (SbbNotification) alarmNotification.getNotificationSource();
                int inActiveAlarms = 0;
                int activeAlarms = 0;

                if (alarmNotification.getMessage().equals(Test1113512Sbb1.ALARM_MESSAGE)) {
                    try {
                        // 1113512
                        inActiveAlarms = alarmMBeanProxy.getAlarms(sbbNotification).length;
                        if (inActiveAlarms >= 1) {
                            result.setFailed(1113512, "Request that all alarms belonging to the notification "
                                    + "source associated with the AlarmFacility object be cleared. So there "
                                    + "should be no alarms need to be cleared, but the result returned "
                                    + inActiveAlarms + " currently active in the slee.");
                            return;
                        }
                        
                        if (alarmNotification.getAlarmLevel().equals(AlarmLevel.CLEAR)) {
                            receivedAlarmNotifications++;
                        }
                    } catch (Exception e) {
                        getLog().warning("Received Exception from SBB or resource:");
                        getLog().warning(e);
                        result.setError(e);
                    }
                } else if (alarmNotification.getMessage().equals(Test1113512Sbb2.ALARM_MESSAGE)) {
                    try {
                        receivedRaisedAlarmNotifications++;
                        
                        // 1113588
                        activeAlarms = alarmMBeanProxy.getAlarms(sbbNotification).length;
                        if (activeAlarms != Test1113512Sbb2.ACTIVE_ALARMS && receivedRaisedAlarmNotifications == 5) {
                            result.setFailed(1113588, "There should be five active alarms raised by sbb2/service2 "
                                    + "in the SLEE, but the result returned " + activeAlarms + " currently "
                                    + "active in the slee.");
                        }
                        
                        
                        if (alarmNotification.getAlarmLevel().equals(AlarmLevel.CLEAR)) {
                            receivedAlarmNotifications++;
                        }

                        if (receivedRaisedAlarmNotifications == 5) {
                            if (inActiveAlarms == 0 && activeAlarms == Test1113512Sbb2.ACTIVE_ALARMS && receivedAlarmNotifications == 5)
                                result.setPassed();
                            else
                                result.setFailed(1113512, "There should be no active alarms in Sbb1, but it returned "
                                            + inActiveAlarms
                                            + "and there should be five active alarms in Sbb2, but it returned "
                                            + activeAlarms+ ":" + receivedAlarmNotifications);
                        }
                        return;

                    } catch (Exception e) {
                        getLog().warning("Received Exception from SBB or resource:");
                        getLog().warning(e);
                        result.setError(e);
                    }
                }
                return;
            }

            getLog().info("Notification received: " + notification);
            return;
        }
    }

    private NotificationListener listener;

    private AlarmMBeanProxy alarmMBeanProxy;

    private FutureResult result;
    
    private String testName = "Test1113512";
    
    private int receivedAlarmNotifications = 0;
    
    private int receivedRaisedAlarmNotifications = 0;
}
