/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.events.sbbabstractclass;

import java.util.HashMap;

import javax.slee.ActivityContextInterface;
import javax.slee.Address;
import javax.slee.ServiceID;
import javax.slee.facilities.Tracer;
import javax.slee.nullactivity.NullActivity;
import javax.slee.nullactivity.NullActivityContextInterfaceFactory;
import javax.slee.nullactivity.NullActivityFactory;
import javax.slee.EventContext;

import com.opencloud.sleetck.lib.resource.events.TCKResourceEventX;
import com.opencloud.sleetck.lib.sbbutils.BaseTCKSbb;
import com.opencloud.sleetck.lib.sbbutils.TCKSbbUtils;

/**
 * AssertionID(1108075): Test This method throws a javax.slee.NotAttachedException 
 * if the SBB entity is not attached to the specified Activity Context.
 * 
 * AssertionID(1108082): Test This method throws a java.lang.NullPointerException
 *  if the activity argument is null.
 *  
 * AssertionID(1108083): Test This method throws a java.slee.UnrecognizedEventException 
 * if one of the event names specified does not identify an event type that the SBB can 
 * receive.
 */
public abstract class Test1108075Sbb extends BaseTCKSbb {

    public void onTCKResourceEventX1(TCKResourceEventX event, ActivityContextInterface aci, EventContext context) {
        try {

            tracer = getSbbContext().getTracer("com.test");
            tracer.info("Received event TCKResourceEventX1");

            // Test 1108075
            // Create a NullActivity and get its ActivityContextInterface
            NullActivityFactory factory = (NullActivityFactory) TCKSbbUtils.getSbbEnvironment().lookup(
                    NullActivityFactory.JNDI_NAME.replaceFirst("java:comp/env/", ""));
            NullActivity nullActivity = factory.createNullActivity();
            NullActivityContextInterfaceFactory aciFactory = (NullActivityContextInterfaceFactory) TCKSbbUtils
                    .getSbbEnvironment().lookup(NullActivityContextInterfaceFactory.JNDI_NAME.replaceFirst("java:comp/env/", ""));
            ActivityContextInterface nullACI = aciFactory.getActivityContextInterface(nullActivity);

            // Do _NOT_ attach this SBB to the NullActivity.

            boolean passed = false;
            try {
                // Call SbbContext.maskEvent(ACI that isn't attached to this SBB)
                getSbbContext().maskEvent(null, nullACI);
            } catch (javax.slee.NotAttachedException e) {
                passed = true;
            }

            // Clean up -- end the NullActivity
            nullActivity.endActivity();

            if (!passed) {
                sendResultToTCK(1108075, "getSbbContext().maskEvent(null, nullACI) "
                        + "should have thrown javax.slee.NotAttachedException.", "Test1108075Test", false);
                return;
            }
            tracer.info("Passed 1108075");
            // Test 1108083

            // Valid set of names.
            try {
                getSbbContext().maskEvent(new String[] { "Test1108075Event", "TCKResourceEventX2" }, aci);
            } catch (javax.slee.UnrecognizedEventException e) {
                sendResultToTCK(1108082, "SbbContext.maskEvent() failed when given valid set of event names.", "Test1108075Test", false);
                return;
            }

            passed = false;
            try {
                // Set of invalid (gobbledigook) names.
                getSbbContext().maskEvent(new String[] { "FredEvent" }, aci);
            } catch (javax.slee.UnrecognizedEventException e) {
                tracer.info("SbbContext.maskEvent() did throw exception when given a non-existent event.");
                passed = true;
            }

            // If the exception wasn't thrown...
            if (passed == false) {
                sendResultToTCK(1108082,
                        "SbbContext.maskEvent() didn't throw exception when given a non-existent event.", "Test1108075Test", false);
                return;
            }

            // Reset the pass/fail marker.
            passed = false;
            try {
                // Otherwise valid set containing event with direction fire (invalid)
                getSbbContext().maskEvent(new String[] { "Test1108075SecondEvent" }, aci);
            } catch (javax.slee.UnrecognizedEventException e) {
                passed = true;
            }

            // If the exception wasn't thrown...
            if (passed == false) {
                sendResultToTCK(1108082,
                        "SbbContext.maskEvent() didn't throw exception when given an event with direction of fire.", "Test1108075Test", false);
                return;
            }
            tracer.info("Passed 1108082");
            //1108182:activity==null;
            passed = false;
            try {
                getSbbContext().maskEvent(new String[] { "Test1108075Event" }, null);
            } catch (java.lang.NullPointerException e) {
                tracer.info("got expected NullPointerException when the activity argument is null", null);
                passed = true;
            }

            if (!passed) {
                sendResultToTCK(1108182, "getSbbContext().maskEvent(eventNames, null) "
                        + "should have thrown java.lang.NullPointerException.", "Test1108075Test", false);
                return;
            }
            tracer.info("Passed 1108083");
            sendResultToTCK(1108182, "Test SbbContext interface maskEvent methods passed!", "Test1108075Test", true);

        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }
    }

    // Fire and receive event.
    public abstract void fireTest1108075Event(Test1108075Event event, ActivityContextInterface aci, Address address,
            ServiceID serviceID);

    public void onTest1108075Event(Test1108075Event event, ActivityContextInterface aci, EventContext context) {
        try {
            tracer = getSbbContext().getTracer("com.test");
            tracer.info("Received event Test1108075Event");
        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }
    }

    // Receive-only event.
    public void onTCKResourceEventX2(TCKResourceEventX event, ActivityContextInterface aci, EventContext context) {
        try {
            tracer = getSbbContext().getTracer("com.test");
            tracer.info("Received event TCKResourceEventX2");
        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }
    }

    // Fire-only event.
    public abstract void fireTest1108075SecondEvent(Test1108075SecondEvent event, ActivityContextInterface aci,
            Address address, ServiceID serviceID);

    private void sendResultToTCK(int failedAssertionID, String message, String testName, boolean result) throws Exception {
        HashMap sbbData = new HashMap();
        sbbData.put("testname", testName);
        sbbData.put("result", result ? "pass" : "fail");
        sbbData.put("message", message);
        sbbData.put("id", new Integer(failedAssertionID));
        TCKSbbUtils.getResourceInterface().sendSbbMessage(sbbData);
    }

    private Tracer tracer;
}
