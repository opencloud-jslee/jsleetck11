/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.events.convergencename.eventisunique;

import com.opencloud.sleetck.lib.AbstractSleeTCKTest;
import com.opencloud.sleetck.lib.TCKTestResult;
import com.opencloud.sleetck.lib.resource.TCKActivityID;
import com.opencloud.sleetck.lib.resource.events.TCKResourceEventX;
import com.opencloud.sleetck.lib.testutils.BaseTCKResourceListener;
import com.opencloud.sleetck.lib.testutils.FutureResult;

import java.rmi.RemoteException;

/**
 * Test assertion 1965: that all events are unique.  On receiving an X1 event the SBB will callback to the test to check
 * whether another event should be fired (the test will allow this only once to prevent an infinite loop).  If the response
 * is true the SBB will use it's fire event method to re-fire the event it received.  Because Event is included in the
 * initial event selector for the SBB a different convergence name should be generated.
 * This test should create two SBB entities (the same event is fired twice - once by the test and once by the SBB).
 */
public class Test1965Test extends AbstractSleeTCKTest {

    public static final int EXPECTED_SBBS_CREATED = 2;

    public TCKTestResult run() throws Exception {
        ResourceListener listener = new ResourceListener();
        testResult = new FutureResult(getLog());
        setResourceListener(listener);
        sbbsCreated = 0;
        TCKActivityID activityID = utils().getResourceInterface().createActivity("Test1965Activity");

        long eventObjectId = utils().getResourceInterface().fireEvent(TCKResourceEventX.X1, null, activityID, null);
        utils().getResourceInterface().fireEvent(eventObjectId, TCKResourceEventX.X1, null, activityID, null);

        return testResult.waitForResultOrFail(utils().getTestTimeout(),"The expected number of SBB entities was not created. " +
                "This implies that multiple firings of the same event were not treated as unique events by the SLEE",1965);
    }

    public class ResourceListener extends BaseTCKResourceListener {
        public synchronized Object onSbbCall(Object args) {
            getLog().info("An SBB was created");
            if(++sbbsCreated == EXPECTED_SBBS_CREATED) {
                getLog().info("The expected number of SBB entities was created");
                testResult.setPassed();
            }
            return null;
        }

        public synchronized void onException(Exception exception) throws RemoteException {
            testResult.setError("An Exception was received from the SBB or the TCK resource",exception);
        }
    }

    private int sbbsCreated;
    private FutureResult testResult;
}
