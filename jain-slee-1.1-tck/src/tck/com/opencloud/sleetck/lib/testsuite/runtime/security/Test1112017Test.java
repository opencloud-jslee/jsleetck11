/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.runtime.security;

import java.rmi.RemoteException;

import javax.management.ObjectName;
import javax.slee.profile.ProfileSpecificationID;

import com.opencloud.sleetck.lib.AbstractSleeTCKTest;
import com.opencloud.sleetck.lib.TCKTestErrorException;
import com.opencloud.sleetck.lib.TCKTestResult;
import com.opencloud.sleetck.lib.infra.SleeTCKComponentConstants;
import com.opencloud.sleetck.lib.resource.TCKActivityID;
import com.opencloud.sleetck.lib.resource.TCKSbbMessage;
import com.opencloud.sleetck.lib.resource.events.TCKResourceEventX;
import com.opencloud.sleetck.lib.resource.testapi.TCKResourceTestInterface;
import com.opencloud.sleetck.lib.testutils.BaseTCKResourceListener;
import com.opencloud.sleetck.lib.testutils.FutureResult;
import com.opencloud.sleetck.lib.testutils.ProfileUtils;
import com.opencloud.sleetck.lib.testutils.jmx.ProfileMBeanProxy;
import com.opencloud.sleetck.lib.testutils.jmx.ProfileProvisioningMBeanProxy;


public class Test1112017Test extends AbstractSleeTCKTest {
    public static final String DU_PATH_PARAM= "serviceDUPath";
    public static final String SPEC_NAME = "Test1112017Profile";
    public static final String SPEC_VERSION = "1.1";
    public static final String PROFILE_TABLE_NAME = "Test1112017ProfileTable";
    public static final String PROFILE_NAME = "Test1112017Profile";
    public static final int    TEST_ID = 1112017;

    private void setupServiceAndProfile(String specName, String DUPathParam) throws Exception {
        //Deploy profile spec
        if (null == setupService(DUPathParam)) {
            throw new TCKTestErrorException("Could not deploy Test1112017Profile service.");
        }
        
        //Create a profile table
        ProfileSpecificationID specID = new ProfileSpecificationID(specName, SleeTCKComponentConstants.TCK_VENDOR, SPEC_VERSION);
        profileProvisioning.createProfileTable(specID, PROFILE_TABLE_NAME);
        getLog().fine("Added profile table "+PROFILE_TABLE_NAME);

        //Create a profile via management view
        profile = profileProvisioning.createProfile(PROFILE_TABLE_NAME, PROFILE_NAME);
        profileProxy = utils().getMBeanProxyFactory().createProfileMBeanProxy(profile);
        if (null == profileProxy) {
            throw new TCKTestErrorException("Could not create profile.");
        }
        profileProxy.commitProfile();
        profileProxy.closeProfile();
        getLog().fine("Created profile "+PROFILE_NAME+" for profile table "+PROFILE_TABLE_NAME);
    }

    public TCKTestResult run() throws Exception {
        getLog().fine("Run tests for profile spec "+SPEC_NAME);

        ResourceListener listener = new ResourceListener();
        testResult = new FutureResult(getLog());
        setResourceListener(listener);
        
        // Create a new activity.
        TCKResourceTestInterface resource = utils().getResourceInterface();
        TCKActivityID activityID = resource.createActivity(getClass().getName());

        resource.fireEvent(TCKResourceEventX.X1, null, activityID, null);
        return testResult.waitForResult(utils().getTestTimeout());
        
     }

    public void setUp() throws Exception {
        profileUtils = new ProfileUtils(utils());
        profileProvisioning = profileUtils.getProfileProvisioningProxy();
        //Run tests for spec with distinct ProfileManagement interface
        setupServiceAndProfile(SPEC_NAME, DU_PATH_PARAM);
    }

    public void tearDown() throws Exception {
        profileProvisioning.removeProfileTable(PROFILE_TABLE_NAME);
        super.tearDown();
    }
    
    public class ResourceListener extends BaseTCKResourceListener {
        public void onSbbMessage(TCKSbbMessage message, TCKActivityID calledActivity) throws RemoteException {
            utils().getLog().info((String)message.getMessage());
            testResult.setPassed();
        }

        public synchronized void onException(Exception exception) throws RemoteException {
            testResult.setError("An Exception was received from the SBB or the TCK resource",exception);
        }

    }

    private ProfileUtils profileUtils;
    private ProfileProvisioningMBeanProxy profileProvisioning;
    private ObjectName profile;
    private ProfileMBeanProxy profileProxy;
    FutureResult testResult;
}
