/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.profiles.events;

/**
 * Encapsulates the data sent via the event message to the event handler
 * methods of the ProfileEventsTestSbb.
 */
public class ProfileEventsTestEventData {

    public ProfileEventsTestEventData(String profileTableName, String firstProfileName,
                    String secondProfileName) {
        this.profileTableName=profileTableName;
        this.firstProfileName=firstProfileName;
        this.secondProfileName=secondProfileName;
    }

    /**
     * Exports the Object into an Object or array of primitive types,
     * J2SE types or SLEE types.
     */
    public Object toExported() {
        return new String[] {profileTableName, firstProfileName, secondProfileName};
    }

    /**
     * Returns an ProfileEventsTestEventData Object from the
     * given object in exported form
     */
    public static ProfileEventsTestEventData fromExported(Object exported) {
        String[] stringArray = (String[])exported;
        String profileTableName = stringArray[0];
        String firstProfileName = stringArray[1];
        String secondProfileName = stringArray[2];
        return new ProfileEventsTestEventData(profileTableName,firstProfileName,secondProfileName);
    }

    // Accessor methods

    public String getProfileTableName() {
        return profileTableName;
    }

    public String getFirstProfileName() {
        return firstProfileName;
    }

    public String getSecondProfileName() {
        return secondProfileName;
    }

    // Private state

    private String profileTableName;
    private String firstProfileName;
    private String secondProfileName;

}