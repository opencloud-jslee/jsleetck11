/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.events.initialeventselector;

import com.opencloud.sleetck.lib.sbbutils.BaseTCKSbb;
import com.opencloud.sleetck.lib.sbbutils.TCKSbbUtils;
import com.opencloud.sleetck.lib.resource.events.TCKResourceEventX;
import com.opencloud.sleetck.lib.TCKTestFailureException;

import javax.slee.ActivityContextInterface;
import javax.slee.CreateException;
import javax.slee.InitialEventSelector;
import javax.slee.facilities.Level;

/**
 * Implements an SBB to test the assertion that the SLEE will only call the initial event selector method
 * on SBB objects in the POOLED state.
 */
public abstract class Test752Sbb extends BaseTCKSbb {
    private static final String STATE_POOLED = "pooled";
    private static final String STATE_ACTIVE = "active";

    // Tracking state
    private String state = STATE_POOLED;

    public void sbbCreate() throws CreateException {
        createTrace("sbbCreate()");
        super.sbbCreate();
        this.state = STATE_ACTIVE;
    }

    public void sbbActivate() {
        createTrace("sbbActivate()");
        if (! this.state.equals(STATE_POOLED)) {
            TCKSbbUtils.handleException(new TCKTestFailureException(368, "The SBB's sbbActivate() method was invoked " +
                                                                         "while the SBB object was not in the POOLED state"));
        }
        super.sbbActivate();
        this.state = STATE_ACTIVE;
    }

    public void sbbPassivate() {
        createTrace("sbbPassivate()");
        if (! this.state.equals(STATE_ACTIVE)) {
            TCKSbbUtils.handleException(new TCKTestFailureException(371, "The SBB's sbbPassivate() method was invoked " +
                                                                         "while the SBB object was not in the ACTIVE state"));
        }      
        super.sbbPassivate();
        this.state = STATE_POOLED;
    }

    public void sbbRemove() {
        createTrace("sbbRemove()");
        if (! this.state.equals(STATE_ACTIVE)) {
            TCKSbbUtils.handleException(new TCKTestFailureException(2463, "The SBB's sbbRemoved() method was invoked " +
                                                                         "while the SBB object was not in the ACTIVE state"));
        }
        super.sbbRemove();
        this.state = STATE_POOLED;
    }

    public void onTCKResourceEventX1(TCKResourceEventX event, ActivityContextInterface aci) {

    }

    public void onTCKResourceEventX2(TCKResourceEventX event, ActivityContextInterface aci) {

    }

    public InitialEventSelector initialEventSelector(InitialEventSelector ies) {
        createTrace("initialEventSelector()");
        if (! this.state.equals(STATE_POOLED)) {
            TCKSbbUtils.handleException(new TCKTestFailureException(752, "The SBB's initial event selector method was invoked " +
                                                                         "while the SBB object was not in the POOLED state"));
        }
        ies.setCustomName("Test752");
        return ies;
    }

    private void createTrace(String message) {
        try {
            TCKSbbUtils.createTrace(getSbbID(), Level.INFO, message, null);
        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }
    }
}
