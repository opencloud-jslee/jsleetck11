/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.facilities.alarmfacility;

import java.util.HashMap;

import javax.naming.InitialContext;
import javax.slee.ActivityContextInterface;
import javax.slee.facilities.AlarmFacility;
import javax.slee.facilities.AlarmLevel;
import javax.slee.facilities.Tracer;

import com.opencloud.sleetck.lib.resource.events.TCKResourceEventX;
import com.opencloud.sleetck.lib.sbbutils.BaseTCKSbb;
import com.opencloud.sleetck.lib.sbbutils.TCKSbbUtils;

/*  AssertionID(1113474): Test An alarm will be raised if there 
 * is no existing alarm with matching identifying attributes raised.
 * 
 *  AssertionID(1113476): Test When an alarm is raised an alarm 
 *  notification is emitted by the Alarm Mbean.
 *    
 */
public abstract class Test1113474Sbb extends BaseTCKSbb {

    private static final String JNDI_ALARMFACILITY_NAME = AlarmFacility.JNDI_NAME;
    

    public static final String ALARM_MESSAGE = "Test1113474AlarmMessage";

    public static final String ALARM_INSTANCEID1 = "Test1113474AlarmInstanceID1";
    public static final String ALARM_INSTANCEID2 = "Test1113474AlarmInstanceID2";

    // Initial event
    public void onTCKResourceEventX1(TCKResourceEventX event, ActivityContextInterface aci) {
        try {
            sbbTracer = getSbbContext().getTracer("com.test");
            sbbTracer.info("Received " + event + " message", null);
            AlarmFacility facility = (AlarmFacility) new InitialContext().lookup(JNDI_ALARMFACILITY_NAME);
            
            String alarmID = facility.raiseAlarm("javax.slee.management.Alarm", ALARM_INSTANCEID1, AlarmLevel.MAJOR,
                    ALARM_MESSAGE);
            if (alarmID == null)
                sendResultToTCK("Test1113474Test", false, 1113474, "An alarm didn't raised even there is no existing alarm with "
                        + "matching identifying attributes raised.");
            
            String sameAlarmID = facility.raiseAlarm("javax.slee.management.Alarm", ALARM_INSTANCEID1,
                    AlarmLevel.CRITICAL, ALARM_MESSAGE);
            if (!sameAlarmID.equals(alarmID))
                sendResultToTCK("Test1113474Test", false, 1113474,
                        "If an alarm with the same identifying attributes (notification source, alarm type, and instance ID) "
                                + "is already active in the SLEE, this method has no effect and the alarm identifier of the existing active alarm "
                                + "is returned. But it didn't return the alarm identifier of the existing active alarm.");
            
            String newAlarmID1 = facility.raiseAlarm("javax.slee.management.Alarm", ALARM_INSTANCEID2,
                    AlarmLevel.CRITICAL, ALARM_MESSAGE);
            if (newAlarmID1 == null)
                sendResultToTCK("Test1113474Test", false, 1113474, "An alarm didn't raised even there is no existing alarm with "
                        + "matching identifying attributes (differernt AlarmInstance) raised.");
            
            String newAlarmID2 = facility.raiseAlarm("javax.slee.management.Alarm1", ALARM_INSTANCEID2,
                    AlarmLevel.CRITICAL, ALARM_MESSAGE);
            if (newAlarmID2 == null)
                sendResultToTCK("Test1113474Test", false, 1113474, "An alarm didn't raised even there is no existing alarm with "
                        + "matching identifying attributes (differernt AlarmType) raised.");
        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }
    }

    private void sendResultToTCK(String testName, boolean result, int failedAssertionID,  String message) throws Exception {
        HashMap sbbData = new HashMap();
        sbbData.put("testname", testName);
        sbbData.put("result", result ? "pass" : "fail");
        sbbData.put("message", message);
        sbbData.put("id", new Integer(failedAssertionID));
        TCKSbbUtils.getResourceInterface().sendSbbMessage(sbbData);
    }

    private Tracer sbbTracer;
}
