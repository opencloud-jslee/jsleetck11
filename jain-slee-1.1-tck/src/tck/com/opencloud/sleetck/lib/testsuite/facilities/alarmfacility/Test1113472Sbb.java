/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.facilities.alarmfacility;

import java.util.HashMap;

import javax.naming.InitialContext;
import javax.slee.ActivityContextInterface;
import javax.slee.facilities.AlarmFacility;
import javax.slee.facilities.Tracer;

import com.opencloud.sleetck.lib.resource.events.TCKResourceEventX;
import com.opencloud.sleetck.lib.sbbutils.BaseTCKSbb;
import com.opencloud.sleetck.lib.sbbutils.TCKSbbUtils;

/**
 * AssertionID(1113472): Tests This constant specifies the JNDI 
 * location where an AlarmFacility object may be located by an SBB 
 * or Resource Adaptor component in its component environment.
 * 
 * AssertionID(1113553): Test The value of this constant is 
 * "java:comp/env/slee/facilities/alarm". 
 */
public abstract class Test1113472Sbb extends BaseTCKSbb {

    public void onTCKResourceEventX1(TCKResourceEventX event, ActivityContextInterface aci) {

        try {
            sbbTracer = getSbbContext().getTracer("com.test");
            sbbTracer.info("Received " + event + " message", null);
            doJNDITest();
        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }
    }

    private void sendResultToTCK(String testName, boolean result, int failedAssertionID,  String message) throws Exception {
        HashMap sbbData = new HashMap();
        sbbData.put("testname", testName);
        sbbData.put("result", result ? "pass" : "fail");
        sbbData.put("message", message);
        sbbData.put("id", new Integer(failedAssertionID));
        TCKSbbUtils.getResourceInterface().sendSbbMessage(sbbData);
    }

    private void doJNDITest() throws Exception {
        AlarmFacility alarmFacility = getAlarmFacility();
        //1113472
        if (alarmFacility == null) {
            sendResultToTCK("Test1113472Test", false, 1113472, "Could not find AlarmFacility object in JNDI at " + AlarmFacility.JNDI_NAME);
            return;
        }
        sbbTracer.info("got expected AlarmFacility object in JNDI at " + AlarmFacility.JNDI_NAME, null);

        ///1113062
        String JNDI_ACNF_NAME = AlarmFacility.JNDI_NAME;
        if (JNDI_ACNF_NAME != "java:comp/env/slee/facilities/alarm") {
            sendResultToTCK("Test1113472Test", false, 1113553, "The value of this JNDI_NAME is not equal to java:comp/env/slee/facilities/alarm");
            return;
        }
        sbbTracer.info("got expected value of this AlarmFacility.JNDI_NAME", null);

        sendResultToTCK("Test1113472Test", true, 1113472,"AlarmFacility.JNDI_NAME tests passed");
    }

    private AlarmFacility getAlarmFacility() throws Exception {
        AlarmFacility alarmFacility = null;
        String JNDI_ALARMFACILITY_NAME = AlarmFacility.JNDI_NAME;
        try {
            alarmFacility = (AlarmFacility) new InitialContext().lookup(JNDI_ALARMFACILITY_NAME);
        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }
        return alarmFacility;
    }

    private Tracer sbbTracer;
}