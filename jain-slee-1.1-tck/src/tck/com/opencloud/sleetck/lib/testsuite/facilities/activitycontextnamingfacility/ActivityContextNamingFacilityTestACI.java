/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.facilities.activitycontextnamingfacility;

import javax.slee.ActivityContextInterface;

public interface ActivityContextNamingFacilityTestACI extends ActivityContextInterface {
    /** key - used to identify an ACI that we will name and lookup using ACNF. */
    public void setKey(int key);
    public int getKey();
}
