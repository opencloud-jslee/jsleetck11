/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.facilities.alarmfacility;

import javax.management.Notification;
import javax.management.NotificationListener;
import javax.slee.facilities.AlarmLevel;
import javax.slee.management.AlarmNotification;
import javax.slee.management.DeployableUnitID;

import com.opencloud.sleetck.lib.AbstractSleeTCKTest;
import com.opencloud.sleetck.lib.DescriptionKeys;
import com.opencloud.sleetck.lib.resource.TCKActivityID;
import com.opencloud.sleetck.lib.resource.events.TCKResourceEventX;
import com.opencloud.sleetck.lib.resource.testapi.TCKResourceTestInterface;
import com.opencloud.sleetck.lib.testutils.FutureResult;
import com.opencloud.sleetck.lib.testutils.jmx.AlarmMBeanProxy;

/*  
 *  AssertionID(1113498): Test If the alarm is cleared, the 
 *  SLEE’s AlarmMBean object emits an alarm notification for 
 *  the alarm with the alarm level set to AlarmLevel.CLEAR.
 *    
 *  AssertionID(1113500): Test The alarmID argument. This 
 *  argument specifies the alarm identifier of the alarm being 
 *  cleared.
 *  
 *  AssertionID(1113501): Test The clearAlarm method throws 
 *  a java.lang.NullPointerException if the alarmID argument is 
 *  null.
 *  
 */

public class Test1113498Test extends AbstractSleeTCKTest {

    /**
     * Perform the actual test.
     */
    public void run(FutureResult result) throws Exception {
        this.result = result;

        String activityName = "Test1113498Test";
        TCKResourceTestInterface resource = utils().getResourceInterface();
        TCKActivityID activityID = resource.createActivity(activityName);
        getLog().info("Firing event: " + TCKResourceEventX.X1);

        // kickoff the test
        resource.fireEvent(TCKResourceEventX.X1, testName, activityID, null);
    }

    /**
     * Do all the pre-run configuration of the test.
     */

    public void setUp() throws Exception {

        getLog().fine("Connecting to resource");
        getLog().fine("Installing and activating service");

        // Install the Deployable Units
        String duPath = utils().getTestParams().getProperty(DescriptionKeys.SERVICE_DU_PATH_PARAM);
        duID = utils().install(duPath);

        // Activate the DU
        utils().activateServices(duID, true);

        alarmMBeanProxy = utils().getAlarmMBeanProxy();
        listener = new AlarmNotificationListenerImpl();
        alarmMBeanProxy.addNotificationListener(listener, null, null);
    }

    /**
     * Clean up after the test.
     */

    public void tearDown() throws Exception {

        String[] listActiveAlarms = alarmMBeanProxy.getAlarms();
        if (alarmMBeanProxy.getAlarms().length > 0)
            for (int i = 0; i < listActiveAlarms.length; i++)
                alarmMBeanProxy.clearAlarm(listActiveAlarms[i]);

        if (null != alarmMBeanProxy)
            alarmMBeanProxy.removeNotificationListener(listener);
        // cleanup
        super.tearDown();
    }

    public class AlarmNotificationListenerImpl implements NotificationListener {
        public final void handleNotification(Notification notification, Object handback) {

            if (notification instanceof AlarmNotification) {
                AlarmNotification alarmNotification = (AlarmNotification) notification;
               
                // 1113498
                if (alarmNotification.getAlarmLevel().equals(AlarmLevel.MAJOR)) {
                    getLog().info(
                            "Received the raised alarm " + alarmNotification + " with "
                                    + alarmNotification.getAlarmLevel());
                } else if (alarmNotification.getAlarmLevel().equals(AlarmLevel.CLEAR)) {
                    getLog().info(
                            "Received the cleared alarm " + alarmNotification + " with "
                                    + alarmNotification.getAlarmLevel());
                    // 1113500
                    String clearedAlarmID = alarmNotification.getAlarmID();
                    try {
                        if (alarmMBeanProxy.isActive(clearedAlarmID)) {
                            result.setFailed(1113500, "The cleared alarmID is still currently active in the SLEE.");
                            return;
                        }
                    } catch (Exception e) {
                        getLog().warning("Received Exception from SBB or resource:");
                        getLog().warning(e);
                        result.setError(e);
                    }
                    
                    result.setPassed();
                } else {
                    result.setFailed(1113498,
                                    "AlarmNotification.getAlarmLevel() didn't return the level of alarm being created in sbb or service.");
                }
                return;
            }

            getLog().info("Notification received: " + notification);
            return;
        }
    }

    private NotificationListener listener;

    private DeployableUnitID duID;

    private AlarmMBeanProxy alarmMBeanProxy;

    private FutureResult result;
    
    private String testName = "Test113498";
}
