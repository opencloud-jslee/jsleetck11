/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.events.maskEvent;

import javax.slee.ActivityEndEvent;
import javax.slee.ActivityContextInterface;
import javax.slee.SbbContext;
import javax.slee.Address;
import javax.slee.SbbLocalObject;
import javax.slee.facilities.Level;
import javax.slee.ChildRelation;

import javax.slee.nullactivity.NullActivityFactory;
import javax.slee.nullactivity.NullActivity;
import javax.slee.nullactivity.NullActivityContextInterfaceFactory;
import javax.slee.facilities.ActivityContextNamingFacility;

import com.opencloud.sleetck.lib.sbbutils.BaseTCKSbb;
import com.opencloud.sleetck.lib.sbbutils.TCKSbbUtils;
import com.opencloud.sleetck.lib.sbbutils.TCKSbbConstants;
import com.opencloud.sleetck.lib.resource.events.TCKResourceEventX;
import com.opencloud.sleetck.lib.resource.TCKActivityID;
import com.opencloud.sleetck.lib.resource.sbbapi.TCKActivity;
import com.opencloud.sleetck.lib.resource.sbbapi.TCKResourceSbbInterface;
import com.opencloud.sleetck.lib.resource.sbbapi.TCKActivityContextInterfaceFactory;

import java.util.HashMap;
import java.util.Iterator;

public abstract class Test1903Sbb extends BaseTCKSbb {

    // Initial event method.
    public void onTCKResourceEventX1(TCKResourceEventX ev, ActivityContextInterface aci) {

        try {
            HashMap map = new HashMap();

            // Set the set of masked events to a known value.
            getSbbContext().maskEvent(new String [] { "TCKResourceEventX1" }, aci);

            // maskEvent(null, aci)
            getSbbContext().maskEvent(null, aci);

            String [] mask = getSbbContext().getEventMask(aci);
            if (mask.length != 0) {
                map.put("Result", new Boolean(false));
                map.put("Message", "SbbContext.maskEvent(null, aci) didn't unmask all events.");
                TCKSbbUtils.getResourceInterface().sendSbbMessage(map);
                return;
            }


            // Reset the set of masked events to a known value.
            getSbbContext().maskEvent(new String [] { "TCKResourceEventX1" }, aci);

            // maskEvent(new String [] { }, aci);
            getSbbContext().maskEvent(new String [] { }, aci);
            
            mask = getSbbContext().getEventMask(aci);
            if (mask.length != 0) {
                map.put("Result", new Boolean(false));
                map.put("Message", "SbbContext.maskEvent(new String [] { }, aci) didn't unmask all events.");
                TCKSbbUtils.getResourceInterface().sendSbbMessage(map);
                return;
            }

            map.put("Result", new Boolean(true));
            map.put("Message", "Ok");
            TCKSbbUtils.getResourceInterface().sendSbbMessage(map);

        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }


    }

}
