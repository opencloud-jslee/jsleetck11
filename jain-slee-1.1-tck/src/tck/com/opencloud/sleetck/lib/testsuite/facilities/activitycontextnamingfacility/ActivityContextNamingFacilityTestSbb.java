/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.facilities.activitycontextnamingfacility;

import com.opencloud.sleetck.lib.sbbutils.BaseTCKSbb;
import com.opencloud.sleetck.lib.sbbutils.TCKSbbUtils;
import com.opencloud.sleetck.lib.resource.events.TCKResourceEventX;
import com.opencloud.sleetck.lib.resource.sbbapi.TCKActivity;

import javax.naming.Context;
import javax.naming.InitialContext;

import java.util.HashMap;

import javax.slee.*;
import javax.slee.facilities.*;

/**
 * Test ActivityContextNamingFacility functions from spec s12.6.
 */
public abstract class ActivityContextNamingFacilityTestSbb extends BaseTCKSbb {

    public void onTCKResourceEventX1(TCKResourceEventX event, ActivityContextInterface aci) {

        try {
            testName = (String)event.getMessage();
            TCKSbbUtils.createTrace(getSbbID(),Level.INFO,"Received "+testName+" message",null);

            if (testName.equals("jndi")) doJNDITest();
            else if (testName.equals("bind")) doBindTest(aci);
            else if (testName.equals("unbind")) doUnbindTest(aci);
            else if (testName.equals("lookup")) doLookupTest(aci);

            // send result response
            sendResultToTCK();

        } catch(Exception e) {
            TCKSbbUtils.handleException(e);
        }
    }

    private void sendResultToTCK() throws Exception {
        HashMap sbbData = new HashMap();
        sbbData.put("testname", testName);
        sbbData.put("result", result?"pass":"fail");
        sbbData.put("message", message);
        sbbData.put("id", new Integer(failedAssertionID));
        TCKSbbUtils.getResourceInterface().sendSbbMessage(sbbData);
    }

    private void setResultFailed(int assertionID, String msg) throws Exception {
        result = false;
        failedAssertionID = assertionID;
        message =  "Assertion " + assertionID + " failed: " + msg;
        TCKSbbUtils.createTrace(getSbbID(), Level.WARNING, message, null);
    }

    private void setResultPassed(String msg) throws Exception {
        result = true;
        message = "Success: " + msg;
        TCKSbbUtils.createTrace(getSbbID(), Level.INFO, message, null);
    }

    private void doJNDITest() throws Exception {
        ActivityContextNamingFacility acnf = getACNF();
        if (acnf != null) setResultPassed("Found ActivityContextNamingFacility object in JNDI at " + JNDI_ACNF_NAME);
        else setResultFailed(1413, "Could not find ActivityContextNamingFacility object in JNDI at " + JNDI_ACNF_NAME);
    }

    private void doBindTest(ActivityContextInterface aci) throws Exception {
        ActivityContextNamingFacility acnf = getACNF();

        // test that correct exceptions are thrown

        // 1. bind with null aci should throw NullPointerException - Assertion 1360
        try {
            acnf.bind(null, "noddy");
            setResultFailed(1360, "bind with null aci did not throw a NullPointerException");
            return;
        } catch (NullPointerException npe) {
            TCKSbbUtils.createTrace(getSbbID(),Level.INFO,"got expected NullPointerExcepion",null);
        } catch (Exception e) {
            setResultFailed(1360, "bind with null aci threw unexpected exception: " + e);
            return;
        }

        // 2. bind with null name should throw NullPointerException - Assertion 1360
        try {
            acnf.bind(aci, null);
            setResultFailed(1360, "bind with null name did not throw a NullPointerException");
            return;
        } catch (NullPointerException npe) {
            TCKSbbUtils.createTrace(getSbbID(),Level.INFO,"got expected NullPointerExcepion",null);
        } catch (Exception e) {
            setResultFailed(1360, "bind with null name threw unexpected exception: " + e);
            return;
        }

        // 3. bind with empty name should throw IllegalArgumentException - Assertion 1362
        try {
            acnf.bind(aci, "");
            setResultFailed(1362, "bind with zero-length name did not throw a IllegalArgumentException");
            return;
        } catch (IllegalArgumentException iae) {
            TCKSbbUtils.createTrace(getSbbID(),Level.INFO,"got expected IllegalArgumentExcepion",null);
        } catch (Exception e) {
            setResultFailed(1362, "bind with zero-length name threw unexpected exception: " + e);
            return;
        }

        // 4. test for NameAlreadyBoundException - Assertion 1361
        acnf.bind(aci, "foo");
        acnf.bind(aci, "bar");
        try {
            acnf.bind(aci, "foo");
            setResultFailed(1361, "bind to already bound name did not throw a NameAlreadyBoundException");
            return;
        } catch (NameAlreadyBoundException nabe) {
            TCKSbbUtils.createTrace(getSbbID(),Level.INFO,"got expected NameAlreadyBoundExcepion",null);
        } catch (Exception e) {
            setResultFailed(1361, "bind to already bound name threw unexpected exception: " + e);
            return;
        }

        acnf.unbind("foo");
        acnf.unbind("bar");

        setResultPassed("ActivityContextNamingFacility.bind() tests passed");
    }

    private void doUnbindTest(ActivityContextInterface aci) throws Exception {
        ActivityContextNamingFacility acnf = getACNF();

        // test that correct exceptions are thrown

        // 1. unbind with null name should throw NullPointerException - Assertion 1366
        try {
            acnf.unbind(null);
            setResultFailed(1366, "unbind with null name did not throw a NullPointerException");
            return;
        } catch (NullPointerException npe) {
            TCKSbbUtils.createTrace(getSbbID(),Level.INFO,"got expected NullPointerExcepion",null);
        } catch (Exception e) {
            setResultFailed(1366, "unbind with null name threw unexpected exception: " + e);
            return;
        }

        // 2. unbind with unbound name should throw NameNotBoundException
        try {
            acnf.unbind("name-does-not-exist");
            setResultFailed(1367, "bind with unbound name did not throw a NameNotBoundException");
            return;
        } catch (NameNotBoundException nnbe) {
            TCKSbbUtils.createTrace(getSbbID(),Level.INFO,"got expected NameNotBoundExcepion",null);
        } catch (Exception e) {
            setResultFailed(1367, "unbind with unbound name threw unexpected exception: " + e);
            return;
        }

        // test for correct bind/unbind behaviour

        acnf.bind(aci, "testname");
        try {
            acnf.bind(aci, "testname"); // should throw NameAlreadyBoundException -> bind worked
            setResultFailed(1361, "bind with already bound name did not throw a NameAlreadyBoundException");
            return;
        } catch (NameAlreadyBoundException nabe) {}

        acnf.unbind("testname");

        try {
            acnf.bind(aci, "testname"); // should succeed -> unbind worked
        } catch (NameAlreadyBoundException nabe) {
            setResultFailed(1365, "bind with unbound name threw a NameAlreadyBoundException - unbind failed");
            return;
        }
        acnf.unbind("testname");

        setResultPassed("ActivityContextNamingFacillity.unbind() tests passed");
    }

    private void doLookupTest(ActivityContextInterface aci) throws Exception {
        ActivityContextNamingFacility acnf = getACNF();
        ActivityContextInterface dummyaci = null;

        // test that correct exceptions are thrown

        // 1. lookup with null name should throw NullPointerException - Assertion 1371
        try {
            dummyaci = acnf.lookup(null);
            setResultFailed(1371, "lookup with null name did not throw a NullPointerException");
            return;
        } catch (NullPointerException npe) {
            TCKSbbUtils.createTrace(getSbbID(),Level.INFO,"got expected NullPointerException",null);
        } catch (Exception e) {
            setResultFailed(1371, "lookup with null name threw unexpected exception: " + e);
            return;
        }

        // 2. lookup with unbound name should return null - Assertion 1370
        dummyaci = acnf.lookup("name-does-not-exist");
        if (dummyaci != null) {
            setResultFailed(1370, "lookup with unbound name did not return null");
            return;
        }

        // create an ac
        ActivityContextNamingFacilityTestACI testaci = asSbbActivityContextInterface(aci);
        // set some cmp state so we can identify the ac later
        int key = 666;
        testaci.setKey(key);
        // bind ac to a name
        acnf.bind(testaci, "testname");

        // lookup name, test it is same ac - Assertion 1372
        ActivityContextNamingFacilityTestACI testaci2 = asSbbActivityContextInterface(acnf.lookup("testname"));
        int key2 = testaci2.getKey();
        if (key != key2) {
            setResultFailed(1372, "lookup did not return same aci");
            return;
        }

        acnf.unbind("testname");

        setResultPassed("ActivityContextNamingFacillity.lookup() tests passed");
    }

    private ActivityContextNamingFacility getACNF() throws Exception {
        ActivityContextNamingFacility acnf = null;
        try {
            acnf = (ActivityContextNamingFacility) new InitialContext().lookup(JNDI_ACNF_NAME);
        } catch (Exception e) {
            TCKSbbUtils.createTrace(getSbbID(),Level.WARNING,"got unexpected Exception: " + e,null);
        }
        return acnf;
    }

    public abstract ActivityContextNamingFacilityTestACI asSbbActivityContextInterface(ActivityContextInterface aci);

    private static final String JNDI_ACNF_NAME = "java:comp/env/slee/facilities/activitycontextnaming";

    private boolean result = false;
    private String message;
    private String testName;
    private int failedAssertionID = -1;


}