/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.management.ResourceManagementMBean;

import java.util.HashSet;

import javax.slee.ComponentID;
import javax.slee.InvalidStateException;
import javax.slee.SbbID;
import javax.slee.ServiceID;
import javax.slee.management.ServiceState;
import javax.slee.management.DeployableUnitDescriptor;
import javax.slee.management.DeployableUnitID;
import javax.slee.management.LinkNameAlreadyBoundException;
import javax.slee.management.ResourceAdaptorEntityAlreadyExistsException;
import javax.slee.management.ResourceAdaptorEntityState;
import javax.slee.management.UnrecognizedResourceAdaptorEntityException;
import javax.slee.management.UnrecognizedResourceAdaptorException;
import javax.slee.resource.ConfigProperties;
import javax.slee.resource.ResourceAdaptorID;

import com.opencloud.sleetck.lib.AbstractSleeTCKTest;
import com.opencloud.sleetck.lib.TCKTestErrorException;
import com.opencloud.sleetck.lib.TCKTestResult;
import com.opencloud.sleetck.lib.infra.SleeTCKComponentConstants;
import com.opencloud.sleetck.lib.rautils.MessageHandler;
import com.opencloud.sleetck.lib.rautils.RMIObjectChannel;
import com.opencloud.sleetck.lib.rautils.UOID;
import com.opencloud.sleetck.lib.testsuite.resource.RAMethods;
import com.opencloud.sleetck.lib.testsuite.resource.TCKMessage;
import com.opencloud.sleetck.lib.testutils.FutureResult;
import com.opencloud.sleetck.lib.testutils.jmx.DeploymentMBeanProxy;
import com.opencloud.sleetck.lib.testutils.jmx.ResourceManagementMBeanProxy;
import com.opencloud.sleetck.lib.testutils.jmx.ServiceManagementMBeanProxy;
import com.opencloud.sleetck.lib.testutils.jmx.SleeManagementMBeanProxy;


/**
 * Test getting Resource Adaptor properties through the ResourceManagementMBean interface.
 */

public class Test1114096Test extends AbstractSleeTCKTest {

    private static final String RA_NAME = "TCK_Context_Test_RA";
    private static final String RA_ENTITY_NAME1 = RA_NAME + "_Entity1";
    private static final String RA_ENTITY_NAME2 = RA_NAME + "_Entity2";
    private static final String RA_ENTITY_BAD = RA_NAME + "_Entity666";
    private static final String RA_VENDOR = SleeTCKComponentConstants.TCK_VENDOR;
    private static final String RA_VERSION = "1.1";
    protected static final String RESOURCE_LINK_NAME_PARAM = "resourceLinkName";

    private static final String RESOURCE_DU_PATH_PARAM = "resourceDUPath";
    private static final String SERVICE_DU_PATH_PARAM = "serviceDUPath";

    public TCKTestResult run() throws Exception {
        TCKTestResult result1;
        TCKTestResult result2;

        FutureResult future = new FutureResult(getLog());

        RMIObjectChannel in = utils().getRMIObjectChannel();
        in.setMessageHandler(new CallbackListener(future, 1));

        result1 = doResourceManagementMBeanCheck(resourceMBean);

        getLog().info("Waiting for RA Entity to enter INACTIVE state");
        ResourceAdaptorEntityState raState = null;
        long timeout = System.currentTimeMillis() + utils().getTestTimeout();
        while (System.currentTimeMillis() < timeout) {
            raState = resourceMBean.getState(theEntity);
            if (raState.isInactive())
                break;
            Thread.sleep(500);
        }
        getLog().info("Current RA state = " +raState);

           if (!ResourceAdaptorEntityState.INACTIVE.equals(raState))
               utils().getLog().error("RA did not transition into the INACTIVE state within test timeout");

         result2 = checkResourceAdaptorsFinished(resourceMBean);

        if (!result1.equals(TCKTestResult.passed())) {
            return result1;
        } else {
             return result2;
        }
    }

    public void tearDown() throws Exception {
        try {
             // Final clean up checks
            utils().getLog().fine("Disconnecting from resource");
            utils().removeRAEntities();
            utils().getResourceInterface().clearActivities();
            super.tearDown();
        } catch (Exception e) {
                getLog().warning(e);
                getLog().warning("ERROR: problems in test tear down");
        }
    }

    public void setUp() throws Exception {
        DeploymentMBeanProxy deploymentMBean = utils().getDeploymentMBeanProxy();
        ServiceManagementMBeanProxy serviceMBean = utils().getServiceManagementMBeanProxy();

        // Deploy the RA
        String raDUPath = utils().getTestParams().getProperty(RESOURCE_DU_PATH_PARAM);
        getLog().fine("Installing DU: " + raDUPath );
        raDUID = utils().install(raDUPath);

        // Create an RA Entity (based on RA Context Resource Adaptor)
        raEntityName = RA_ENTITY_NAME1;
        resourceMBean = utils().getResourceManagementMBeanProxy();
        raID = new ResourceAdaptorID(RA_NAME, RA_VENDOR, RA_VERSION);
        resourceMBean.createResourceAdaptorEntity(raID, RA_ENTITY_NAME1, new ConfigProperties());

        // Bind Link name
        raLinkName = "slee/resources/tck/simple";
        getLog().info("Binding RA Entity '" + raEntityName + "' to link name '" + raLinkName + "'");
        resourceMBean.bindLinkName(raEntityName, raLinkName);

        // Install Service
        String serviceDUPath = utils().getTestParams().getProperty(SERVICE_DU_PATH_PARAM);

        if (serviceDUPath != null) {
            DeployableUnitDescriptor descriptor = null;

            String absolutePath = utils().getDeploymentUnitURL(serviceDUPath);
            getLog().info("Installing service: " + absolutePath);
            sbbDUID = deploymentMBean.install(absolutePath);

            try {
                 descriptor = utils().getDeploymentMBeanProxy().getDescriptor(sbbDUID);
            } catch (Exception e) {
                throw new TCKTestErrorException("An error occured while attempting to find a ServiceID contained in DU: " + sbbDUID, e);
            }

            ComponentID[] components = descriptor.getComponents();
            for (int i = 0; i < components.length; i++) {
                if (components[i] instanceof ServiceID) {
                    getLog().fine("Setting serviceID value.");
                    serviceID = (ServiceID) components[i];
                    continue;
                }

                if (components[i] instanceof SbbID) {
                    getLog().fine("Setting sbbID value.");
                    sbbID = (SbbID) components[i];
                    continue;
                }
            }
            serviceMBean.activate(serviceID);
        }

        // Activate the RA
        resourceMBean.activateResourceAdaptorEntity(raEntityName);
        getLog().fine("SBB & RA are active");

    }

    //
    // Private
    //

    private class CallbackListener implements MessageHandler {
        public CallbackListener(FutureResult result, int expectedCount) {
            this.result = result;
            this.expectedCount = expectedCount;
            getLog().fine("CallbackListener received message ");
        }

        public boolean handleMessage(Object objectMessage) {
            if (objectMessage instanceof TCKMessage) {
                TCKMessage message = (TCKMessage) objectMessage;
                UOID uoid = message.getUID();
                int method = message.getMethod();
                if (method == RAMethods.raConfigure) {
                    synchronized (raEntities) {
                        raEntities.add(uoid);
                        if (raEntities.size() == expectedCount)
                            result.setPassed();
                    }
                } else if (method == RAMethods.raUnconfigure) {
                    synchronized (raEntities) {
                        raEntities.remove(uoid);
                    }
                }
                return true;
            }
            return false;
        }

        private FutureResult result;
        private int expectedCount;
        private HashSet raEntities = new HashSet();

    };

    private TCKTestResult doResourceManagementMBeanCheck(ResourceManagementMBeanProxy theResourceManagementMBean) throws InterruptedException {
        ResourceAdaptorEntityState currentState = null;
        long timeout = 0;
        ConfigProperties configProperties;
        String newConfigStr = null;
        ResourceAdaptorID badRAid = new ResourceAdaptorID("TCK_Non_Existant_RA", RA_VENDOR, RA_VERSION);

        //1114096
        try {
            SleeManagementMBeanProxy proxy = utils().getSleeManagementMBeanProxy();
            javax.management.ObjectName objectName = proxy.getResourceManagementMBean();
            String strObjectName = objectName.toString();
            getLog().fine("1114096: getResourceManagementMBean = "+strObjectName);
            if (strObjectName.equals("javax.slee.management:name=ResourceManagement")) {
                logSuccessfulCheck(1114096);
            } else {
                return TCKTestResult.failed(1114096, "getResourceManagementMBean() returned: " +objectName);
            }
        }
        catch (Exception e) {
            getLog().warning(e);
            return TCKTestResult.failed(1114096, "getResourceManagementMBean() failed");
        }

        //1114108 - getResourceAdaptorEntities
        try {
            boolean found = false;
            resourceEntities  = theResourceManagementMBean.getResourceAdaptorEntities();
            getLog().fine("1114108: getResourceAdaptorEntities = "+resourceEntities[0].toString());
            if ((resourceEntities.length > 0)) {
                for (int i=0; i < resourceEntities.length; i++) {
                    if (resourceEntities[i].equals(RA_ENTITY_NAME1))
                        found = true;
                }
                if (found)
                     logSuccessfulCheck(1114108);
                else
                    return TCKTestResult.failed(1114108, "getResourceAdaptorEntities() failed ro return correct resourceEntities");
            } else {
                return TCKTestResult.failed(1114108, "getResourceAdaptorEntities() failed ro return resourceEntities");
            }
        }
        catch (Exception e) {
            getLog().warning(e);
            return TCKTestResult.failed(1114108, "getResourceAdaptorEntities() failed");
        }

        //1114069 - getLinkNames for all RA's
        try {
            linkNames  = theResourceManagementMBean.getLinkNames();
            logSuccessfulCheck(1114069);
        }
        catch (Exception e) {
            getLog().warning(e);
            return TCKTestResult.failed(1114069, "getLinkNames() failed");
        }

        //1114116 - getConfigurationProperties for the ResourceAdaptorID
        try {
            configProperties  = theResourceManagementMBean.getConfigurationProperties(raID);
            getLog().fine("1114116: getConfigurationProperties = "+configProperties.toString());
            logSuccessfulCheck(1114116);
        }
        catch (Exception e) {
            getLog().warning(e);
            return TCKTestResult.failed(1114116, "getConfigurationProperties() failed");
        }

        //1114053 - getResourceUsageMBean
        try {
            javax.management.ObjectName usageObject  = theResourceManagementMBean.getResourceUsageMBean("TCK_Context_Test_RA_Entity1");
            String strObjectName = usageObject.toString();
            getLog().fine("1114053: getResourceUsageMBean = "+usageObject);
            if (strObjectName.equals("javax.slee.management.usage:type=ResourceUsage,raEntityName=\"TCK_Context_Test_RA_Entity1\"")) {
                logSuccessfulCheck(1114053);
            } else {
                return TCKTestResult.failed(1114053, "Incorrect ResourceManagementMBean Object Name: "+strObjectName);
            }
        }
        catch (Exception e) {
            getLog().warning(e);
            return TCKTestResult.failed(1114053, "getResourceUsageMBean() failed");
        }

         //1114092 - createResourceAdaptor failure
         try {
             configProperties = new ConfigProperties();
             logSuccessfulCheck(1114055);
             theResourceManagementMBean.createResourceAdaptorEntity(badRAid, RA_ENTITY_NAME2, configProperties);
             return TCKTestResult.failed(1114092, "createResourceAdaptorEntity() failed to throw UnrecognizedResourceAdaptorException");
         }
         catch (UnrecognizedResourceAdaptorException e){
             logSuccessfulCheck(1114092);
         }
         catch (Exception e) {
             getLog().warning(e);
             return TCKTestResult.failed(1114092, "createResourceAdaptorEntity() failed");
         }

        // Create new RA - raID2
        raID2 = new ResourceAdaptorID(RA_NAME, RA_VENDOR, RA_VERSION);

        //1114141 - createResourceAdaptorEntity
        try {
            theResourceManagementMBean.createResourceAdaptorEntity(raID2, theEntity, configProperties);
            getLog().fine("1114141: createResourceAdaptorEntity = "+theEntity);
            currentState = theResourceManagementMBean.getState(theEntity);
            if (currentState.isInactive()){
                 logSuccessfulCheck(1114141);
            } else {
                return TCKTestResult.failed(1114141, "current State of raID2 is not INACTIVE but: " +currentState.toString());
            }
        }
        catch (Exception e) {
            getLog().warning(e);
            return TCKTestResult.failed(1114141, "createResourceAdaptorEntity() failed");
        }

        //1114068 - createResourceAdaptorEntity again & check failure
        try {
            configProperties = new ConfigProperties();
            theResourceManagementMBean.createResourceAdaptorEntity(raID2, theEntity, configProperties);
            return TCKTestResult.failed(1114068, "createResourceAdaptorEntity() failed to throw ResourceAdaptorEntityAlreadyExistsException");
        }
        catch (ResourceAdaptorEntityAlreadyExistsException e){
            logSuccessfulCheck(1114068);
        }
        catch (Exception e) {
            getLog().warning(e);
            return TCKTestResult.failed(1114068, "createResourceAdaptorEntity() failed");
        }

        //1114116 - getConfigurationProperties for the ResourceAdaptorID
        try {
            configProperties  = theResourceManagementMBean.getConfigurationProperties(raID2);
            getLog().fine("1114116: getConfigurationProperties = "+configProperties.toString());
            logSuccessfulCheck(1114116);
         }
        catch (Exception e) {
            getLog().warning(e);
            return TCKTestResult.failed(1114116, "getConfigurationProperties() failed");
        }

        //1114091 - getConfigurationProperties for the ResourceAdaptorEntity
        try {
            configProperties  = theResourceManagementMBean.getConfigurationProperties(theEntity);
            getLog().fine("1114091: getConfigurationProperties = "+configProperties.toString());
            logSuccessfulCheck(1114091);
        }
        catch (Exception e) {
            getLog().warning(e);
            return TCKTestResult.failed(1114091, "getConfigurationProperties() failed");
        }

        //1114122 - getResourceAdaptorEntities for new RA
        try {
            boolean found = false;
            resourceEntities  = theResourceManagementMBean.getResourceAdaptorEntities(raID2);
            if (resourceEntities.length > 0) {
                for (int i=0; i < resourceEntities.length; i++) {
                    getLog().fine("1114122: getResourceAdaptorEntities = "+resourceEntities[i].toString());
                    if (resourceEntities[i].equals(theEntity)){
                        logSuccessfulCheck(1114122);
                        found = true;
                    }
                 }
                 if (!found) {
                    return TCKTestResult.failed(1114122, "getResourceAdaptorEntities for raID2 did not contain RA Entity Name: " + theEntity.toString());
                }
            } else {
                return TCKTestResult.failed(1114122, "getResourceAdaptorEntities for raID2 did not contain RA Entity Name");
            }
        }
        catch (Exception e) {
            getLog().warning(e);
            return TCKTestResult.failed(1114122, "getResourceAdaptorEntities() failed");
        }

        //1114134 - updateConfigurationProperties
        Boolean propertyValue = new Boolean(true);
        ConfigProperties.Property newConfigProperty = new ConfigProperties.Property("$TCK_Test_1114096_Property$", "java.lang.Boolean", propertyValue);
        try {
            logSuccessfulCheck(1114132);
            newConfigStr = newConfigProperty.toString();
            logSuccessfulCheck(1114082);
            newConfigStr = newConfigStr.substring(newConfigStr.indexOf('[')+1, newConfigStr.indexOf(']')); // extract Property value
            getLog().fine("1114134: updateConfigurationProperties with: "+newConfigStr);
            configProperties.addProperty(newConfigProperty);
            theResourceManagementMBean.updateConfigurationProperties("TCK_Context_Test_RA_Entity2", configProperties);
        }
        catch (Exception e) {
            getLog().warning(e);
            return TCKTestResult.failed(1114132, "updateConfigurationProperties() failed");
        }

        //1114135 - check ConfigurationProperties updated
        try {
            configProperties = theResourceManagementMBean.getConfigurationProperties(theEntity);
            String configPropertiesStr = configProperties.toString();
            getLog().fine("Updated Config Properties = " +configPropertiesStr);
            logSuccessfulCheck(1114043);

            // Extract the property & check validity
            ConfigProperties.Property updatedProperty = configProperties.getProperty("$TCK_Test_1114096_Property$");
            if (updatedProperty != null) {
                if (updatedProperty.equals(newConfigProperty)) {
                    logSuccessfulCheck(1114134);
                    logSuccessfulCheck(1114135);
                } else {
                    return TCKTestResult.failed(1114135, "updateConfigurationProperties for raID2 did not contain new Property");
                }
            }
        }
        catch (Exception e) {
            getLog().warning(e);
            return TCKTestResult.failed(1114135, "getConfigurationProperties() failed");
        }

        //1114090 - getLinkNames for new RA Entity
        try {
            // There should be no link names for this entity
            linkNames  = theResourceManagementMBean.getLinkNames(theEntity);
            if (linkNames.length == 0) {
                logSuccessfulCheck(1114090);
            } else {
                getLog().warning("1114090: Found getLinkNames for raID2 = "+linkNames[0]);
            }
        }
        catch (Exception e) {
            getLog().warning(e);
            return TCKTestResult.failed(1114090, "getLinkNames() failed");
        }

        //1114090 - getLinkNames for first RA Entity
        try {
            linkNames  = theResourceManagementMBean.getLinkNames(raEntityName);
            if (linkNames.length > 0) {
                boolean found = false;
                for (int i=0; i<linkNames.length; i++) {
                    getLog().fine("1114090: New getLinkNames for raID = "+linkNames[i]);
                    if (linkNames[i].toString().equals(bindLinkName)) {
                        logSuccessfulCheck(1114117);
                        found = true;
                    }
                }
                if (!found)
                    return TCKTestResult.failed(1114090, "Incorrect LinkNames for raID: Expected: " +bindLinkName);
            } else {
                return TCKTestResult.failed(1114090, "Did not find LinkNames for raID");
            }
        }
        catch (Exception e) {
            getLog().warning(e);
            return TCKTestResult.failed(1114090, "getLinkNames() failed");
        }

        //1114109 - Check bound SBB
        try {
            SbbID boundSbb = new SbbID("Test1114096Sbb","jain.slee.tck","1.1");
            SbbID[] sbbIDs  = theResourceManagementMBean.getBoundSbbs(bindLinkName);
            if (sbbIDs.length > 0) {
                boolean found = false;
                for (int i=0; i<sbbIDs.length; i++) {
                    getLog().fine("1114109: getBoundSbbs = "+sbbIDs[i].toString());
                    if (sbbIDs[i].equals(boundSbb)) {
                        logSuccessfulCheck(1114109);
                        found = true;
                    }
                }
                if (!found)
                    return TCKTestResult.failed(1114109, "Incorrect SBB id for raID");
            } else {
                return TCKTestResult.failed(1114109, "Did not find an SBB bound to LinkName for raID");
            }
        }
        catch (Exception e) {
            getLog().warning(e);
            return TCKTestResult.failed(1114109, "getResourceAdaptorEntities() failed");
        }

        //1114119 - bindLinkNames for theEntity when already bound
        try {
            theResourceManagementMBean.bindLinkName(raEntityName, bindLinkName);
            return TCKTestResult.failed(1114119, "bindLinkName() failed to throw LinkNameAlreadyBoundException");
        }
        catch (LinkNameAlreadyBoundException e) {
            getLog().fine("1114119: RA entity already bound: " +raEntityName +" to " +bindLinkName);
            logSuccessfulCheck(1114119);
        }
        catch (Exception e) {
            getLog().warning(e);
            return TCKTestResult.failed(1114119, "bindLinkName() failed");
        }

        //1114139 - Activate Unknown Entity
        try {
            theResourceManagementMBean.activateResourceAdaptorEntity(RA_ENTITY_BAD);
            return TCKTestResult.failed(1114139, "activateResourceAdaptorEntity() failed to throw UnrecognizedResourceAdaptorEntityException");
        }
        catch (UnrecognizedResourceAdaptorEntityException e) {
            logSuccessfulCheck(1114139);
        }
        catch (Exception e) {
            getLog().warning(e);
            return TCKTestResult.failed(1114139, "activateResourceAdaptorEntity() failed");
        }

        //1114130 - Activate theEntity (already inactive from created)
        try {
            currentState = theResourceManagementMBean.getState(theEntity);
        } catch (Exception e) {
            getLog().warning(e);
            return TCKTestResult.failed(1114140, "getState() failed");
        }
        getLog().fine("1114140: Initial State for raID2 = "+currentState.toString());
        if (!currentState.isInactive()){
            return TCKTestResult.failed(1114130, "currentState is not INACTIVE but: " +currentState.toString());
        }

        try {
            theResourceManagementMBean.activateResourceAdaptorEntity(theEntity);
        }
        catch (Exception e) {
            getLog().warning(e);
            return TCKTestResult.failed(1114130, "activateResourceAdaptorEntity() failed");
        }

        //1114140 - getState for new RA - should be active
        timeout = System.currentTimeMillis() + utils().getTestTimeout();
        while (System.currentTimeMillis() < timeout) {
            try {
                currentState = theResourceManagementMBean.getState(theEntity);
            } catch (Exception e) {
                getLog().warning(e);
                return TCKTestResult.failed(1114140, "getState() failed");
            }
            if (currentState.isActive())
                break;
            Thread.sleep(500);
        }

        getLog().fine("1114140: Final State for raID2 = "+currentState.toString());
        if (currentState.isActive()){
            logSuccessfulCheck(1114130);
        } else {
            return TCKTestResult.failed(1114130, "currentState is not ACTIVE but: " +currentState.toString());
        }

        //1114121 - updateConfigurationProperties
        try {
            propertyValue = new Boolean(true);
            newConfigProperty = new ConfigProperties.Property("$TCK_Test_1114096_Active_Property$", "java.lang.Boolean", propertyValue);
            newConfigStr = newConfigProperty.toString();
            newConfigStr = newConfigStr.substring(newConfigStr.indexOf('[')+1, newConfigStr.indexOf(']')); // extract Property value
            getLog().fine("1114121: updateConfigurationProperties with: "+newConfigStr);
            configProperties.addProperty(newConfigProperty);
            theResourceManagementMBean.updateConfigurationProperties("TCK_Context_Test_RA_Entity2", configProperties);
            return TCKTestResult.failed(1114121, "updateConfigurationProperties() failed to throw InvalidStateException");
        }
        catch (InvalidStateException e) {
            logSuccessfulCheck(1114121);
        }
       catch (Exception e) {
            getLog().warning(e);
            return TCKTestResult.failed(1114121, "updateConfigurationProperties() failed");
        }

        //1114126 - Activate theEntity again
        try {
            theResourceManagementMBean.activateResourceAdaptorEntity(theEntity);
            return TCKTestResult.failed(1114126, "Activate RA should have failed as RA is already active");
        }
        catch (InvalidStateException e) {
            logSuccessfulCheck(1114126);
        }
        catch (Exception e) {
            getLog().warning(e);
            return TCKTestResult.failed(1114126, "activateResourceAdaptorEntity() failed");
        }

        //1114127 - deactivate theEntity
        getLog().fine("1114127: deactivateResourceAdaptorEntity then wait to check raID2 is no longer active");
        try {
            currentState = theResourceManagementMBean.getState(theEntity);
        } catch (Exception e) {
            getLog().warning(e);
            return TCKTestResult.failed(1114140, "getState() failed");
        }
        if (!currentState.isActive()){
            return TCKTestResult.failed(1114127, "currentState is not ACTIVE but: " +currentState.toString());
        }
        try {
            theResourceManagementMBean.deactivateResourceAdaptorEntity(theEntity);
        }
        catch (Exception e) {
            getLog().warning(e);
            return TCKTestResult.failed(1114127, "activateResourceAdaptorEntity() failed");
        }

        getLog().info("Waiting for RA Entity to enter Stopping state");
        timeout = System.currentTimeMillis() + utils().getTestTimeout();
        while (System.currentTimeMillis() < timeout) {
            try {
                currentState = theResourceManagementMBean.getState(theEntity);
            } catch (Exception e) {
                getLog().warning(e);
                return TCKTestResult.failed(1114140, "getState() failed");
            }
            // It's possible that the RA Entity transitioned too quickly and is already Inactive.
            // If this is the case there's nothing we can do, so just break and continue.
            if (currentState.isStopping() || currentState.isInactive())
                break;
            Thread.sleep(500);
        }

        //1114140 - getState for new RA - should be Stopping
        getLog().fine("1114140: currentState for raID2 = "+currentState.toString());
        if (currentState.isStopping()){
            logSuccessfulCheck(1114127);
        } else if (currentState.isInactive()) {
            getLog().info("Was waiting for RA Entity to enter Stopping state, but it was already Inactive by the time we checked.  Nothing we can do about this, so continue.");
        } else {
            return TCKTestResult.failed(1114127, "currentState is not STOPPING but: " +currentState.toString());
        }

        return TCKTestResult.passed();

    }

    private TCKTestResult checkResourceAdaptorsFinished(ResourceManagementMBeanProxy theResourceManagementMBean) {

        //1114140 - getState for new RA - should be inactive
        getLog().fine("checkResourceAdaptorsFinished: Checking raID2 is no longer active");
        try {
            ResourceAdaptorEntityState currentState = theResourceManagementMBean.getState(theEntity);
            getLog().fine("1114140: currentState for raID2 = "+currentState.toString());
            if (currentState.isInactive()){
                logSuccessfulCheck(1114140);
            } else {
                return TCKTestResult.failed(1114140, "currentState is not INACTIVE but: " +currentState.toString());
            }
        }
        catch (Exception e) {
            getLog().warning(e);
            return TCKTestResult.failed(1114140, "getState() failed");
        }

        // Remove SBB
        // Deactivate RA Entity
        try {
            getLog().info("Deactivating Resource Adaptor Entity: " + raEntityName);
            utils().deactivateResourceAdaptorEntity(raEntityName);
        } catch (Exception e) {
            getLog().warning(e);
            return TCKTestResult.failed(1114127, "deactivateResourceAdaptorEntity () failed for " +raEntityName);
        }

        // Deactivate and remove service
            try {
                ServiceState serviceState;
                DeploymentMBeanProxy deploymentMBean = utils().getDeploymentMBeanProxy();
                ServiceManagementMBeanProxy serviceMBean = utils().getServiceManagementMBeanProxy();
                getLog().info("Deactivating Service: " + serviceID);
                serviceMBean.deactivate(serviceID);
                long timeout = System.currentTimeMillis() + utils().getTestTimeout();
                while (System.currentTimeMillis() < timeout) {
                    serviceState = serviceMBean.getState(serviceID);
                    if (serviceState.isInactive())
                        break;
                    Thread.sleep(500);
                }
                getLog().info("Uninstall SBB: " + sbbDUID);
                deploymentMBean.uninstall(sbbDUID);
            } catch (Exception e) {
                getLog().warning(e);
                return TCKTestResult.error("Uninstall service failed for " +raEntityName);
            }

        //1114114 - unbindLinkName for theEntity
        try {
            theResourceManagementMBean.unbindLinkName(bindLinkName);
        }
        catch (Exception e) {
            getLog().warning(e);
            return TCKTestResult.failed(1114114, "unbindLinkName() failed");
        }

        //1114090 - getLinkNames to check unbindLinkName
        try {
            linkNames  = theResourceManagementMBean.getLinkNames(raEntityName);
            if (linkNames.length > 0) {
                    return TCKTestResult.failed(1114114, "unbindLinkName still has LinkNames for raID");
            } else {
                logSuccessfulCheck(1114114);
            }
        }
        catch (Exception e) {
            getLog().warning(e);
            return TCKTestResult.failed(1114090, "getLinkNames() failed");
        }

        //1114050 - removeResourceAdaptorEntity
        try {
            getLog().fine("1114050: Call to removeResourceAdaptorEntity for "+theEntity);
            theResourceManagementMBean.removeResourceAdaptorEntity(theEntity);
        }
        catch (Exception e) {
            getLog().warning(e);
            return TCKTestResult.failed(1114050, "removeResourceAdaptorEntity() failed");
        }

        //1114108 - getResourceAdaptorEntities to check removeResourceAdaptorEntity
        try {
            boolean found = false;
            resourceEntities  = theResourceManagementMBean.getResourceAdaptorEntities();
            for (int i = 0; i < resourceEntities.length; i++) {
                getLog().fine("1114108: getResourceAdaptorEntities = "+resourceEntities[i]);
                if (resourceEntities[i].contains(theEntity)) {
                    logSuccessfulCheck(1114050);
                    found = true;
                }
            }
            if (found)
                return TCKTestResult.failed(1114050, "removeResourceAdaptorEntity() failed to remove RA Entity ");
        }
        catch (Exception e) {
            getLog().warning(e);
            return TCKTestResult.failed(1114108, "getResourceAdaptorEntities() failed");
        }

        return TCKTestResult.passed();
    }

    private void logSuccessfulCheck(int assertionID) {
        utils().getLog().info("Check for assertion "+assertionID+" OK");
    }

    private ResourceAdaptorID raID;
    private String theEntity = RA_ENTITY_NAME2;
    private ResourceAdaptorID raID2 = null;
    private String[] linkNames;
    private String[] resourceEntities;
    private ResourceManagementMBeanProxy resourceMBean;
    private String bindLinkName = "slee/resources/tck/simple";
    private String raLinkName;
    private String raEntityName;
    private DeployableUnitID raDUID;
    private DeployableUnitID sbbDUID;
    private SbbID sbbID;
    private ServiceID serviceID;


}
