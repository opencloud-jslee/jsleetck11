/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.events.convergencename.iesInitialize;

import com.opencloud.sleetck.lib.*;
import com.opencloud.sleetck.lib.infra.SleeTCKComponentConstants;
import com.opencloud.sleetck.lib.testutils.*;
import com.opencloud.sleetck.lib.testutils.jmx.*;
import com.opencloud.sleetck.lib.testutils.ExceptionsUtil;
import com.opencloud.sleetck.lib.resource.TCKActivityID;
import com.opencloud.sleetck.lib.resource.TCKSbbMessage;
import com.opencloud.sleetck.lib.resource.testapi.*;
import com.opencloud.sleetck.lib.resource.events.*;

import java.rmi.RemoteException;
import javax.slee.Address;
import javax.slee.AddressPlan;
import javax.slee.EventTypeID;
import javax.slee.management.ComponentDescriptor;

/**
 * Tests that the initial values of the InitialEventSelector are set
 * correctly.
 */
public class IESInitialValuesTest extends AbstractSleeTCKTest {

    public void setUp() throws Exception {
        super.setUp();

        // lookup the event ID of both event types
        ComponentIDLookup idLookup = new ComponentIDLookup(utils());
        x1EventTypeID = idLookup.lookupEventTypeID(TCKResourceEventX.X1,SleeTCKComponentConstants.TCK_VENDOR,"1.0");
        x2EventTypeID = idLookup.lookupEventTypeID(TCKResourceEventX.X2,SleeTCKComponentConstants.TCK_VENDOR,"1.0");
        if(x1EventTypeID == null || x2EventTypeID == null) throw new TCKTestErrorException(
            "Couldn't find EventTypeID for one or more of the events. Events type names: "+
            TCKResourceEventX.X1+","+TCKResourceEventX.X2);

        // set up the resource listener
        resourceListener = new QueuingResourceListener(utils());
        setResourceListener(resourceListener);
    }

    public TCKTestResult run() throws Exception {

        // send first message
        TCKActivityID activityID = utils().getResourceInterface().createActivity("IESInitialValuesTest-Activity-1");
        sendMessageAndWait(activityID, TCKResourceEventX.X1, x1EventTypeID, "1",
                        new Address(AddressPlan.IP,"1.0.0.1"));

        // send second message
        activityID = utils().getResourceInterface().createActivity("IESInitialValuesTest-Activity-2");
        sendMessageAndWait(activityID, TCKResourceEventX.X2, x2EventTypeID, "2", null);

        // no Exception was thrown, therefore we passed
        return TCKTestResult.passed();
    }

    /**
     * Sends a message according to given parameters, then waits for a response.
     * If the Sbb detects no incorrect values and no Exception is thrown, this method
     * returns silently.
     * If an incorrect value is detected, this method throws a TCKTestFailureException.
     */
    private void sendMessageAndWait(TCKActivityID activityID, String eventTypeName,
                        EventTypeID eventTypeID, final String eventID, Address address)
                                throws TCKTestFailureException, TCKTestErrorException, RemoteException {

        // fire the event with a message containing some of the expected InitialEventSelector values
        Object eventMessage = new IESInitTestEventMessageData(eventID,eventTypeID,activityID,address).toExported();
        utils().getResourceInterface().fireEvent(eventTypeName,eventMessage,activityID,address);

        // wait for ACK or Exception
        resourceListener.nextMessage(new TCKSbbMessageFilter() {
            public boolean accept(TCKSbbMessage received) {
                return received.getMessage().equals(eventID);
            }
        });
    }

    private EventTypeID x1EventTypeID;
    private EventTypeID x2EventTypeID;
    private QueuingResourceListener resourceListener;

}