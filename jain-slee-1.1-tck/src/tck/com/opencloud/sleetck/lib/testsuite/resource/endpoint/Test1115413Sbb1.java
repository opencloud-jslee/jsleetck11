/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.resource.endpoint;

import java.util.HashMap;

import javax.slee.ActivityContextInterface;
import javax.slee.EventTypeID;
import javax.slee.resource.ActivityFlags;
import javax.slee.resource.FireableEventType;
import javax.slee.resource.SleeEndpoint;

import com.opencloud.sleetck.lib.infra.SleeTCKComponentConstants;
import com.opencloud.sleetck.lib.rautils.UOID;
import com.opencloud.sleetck.lib.resource.adaptor11.TCKActivityHandleImpl;
import com.opencloud.sleetck.lib.testsuite.resource.BaseResourceSbb;
import com.opencloud.sleetck.lib.testsuite.resource.RAMethods;
import com.opencloud.sleetck.lib.testsuite.resource.SimpleEvent;

/**
 * This SBB creates a new activity (via the RA), suspends it, fires an event on it, sleeps for 10 seconds, and then
 * detaches.
 *
 */
public abstract class Test1115413Sbb1 extends BaseResourceSbb {
    private static final EventTypeID eventTypeID = new EventTypeID(SimpleEvent.SIMPLE_EVENT_NAME, SleeTCKComponentConstants.TCK_VENDOR, "1.1");

    // Event handler
    public void onSimpleEvent(SimpleEvent event, ActivityContextInterface aci) {
        HashMap results = new HashMap();
        int sequenceID = event.getSequenceID();
        FireableEventType fireableEventType = null;

        // Ignore events not fired by the RA and detach - we'll get duplicates because of laziness with event types.
        if (!"from RA".equals(event.getPayload())) {
            tracer.info("Ignoring event on SBB 1: " + event);
            detach(aci);
            return;
        }

        SleeEndpoint endpoint = null;
        try {
            endpoint = getSbbInterface().getResourceAdaptorContext().getSleeEndpoint();
            fireableEventType = getSbbInterface().getResourceAdaptorContext().getEventLookupFacility().getFireableEventType(eventTypeID);
        } catch (Exception e) {
            tracer.severe("Failure during ResourceAdaptorContext access from SBB.", e);
            results.put("result-sbb1", e);
            sendSbbMessage(sbbUID, sequenceID, RAMethods.suspendActivity, results);
            detach(aci);
            return;
        }

        try {
            tracer.info("(SBB 1) onSimpleEvent() event handler called with event: " + event);

            TCKActivityHandleImpl handle = new TCKActivityHandleImpl(System.currentTimeMillis(), sequenceID);

            tracer.info("Starting new activity.");
            endpoint.startActivity(handle, handle, ActivityFlags.REQUEST_ACTIVITY_UNREFERENCED_CALLBACK);

            tracer.info("Getting ACI for new activity from ACI factory.");
            ActivityContextInterface newAci = getACIFactory().getActivityContextInterface(handle);

            tracer.info("Attaching this SBB to new activity.");
            newAci.attach(getSbbContext().getSbbLocalObject());

            tracer.info("Calling suspendActivity() on new activity.");
            endpoint.suspendActivity(handle);

            tracer.info("Firing new event on new activity.");
            SimpleEvent newEvent = new SimpleEvent(sequenceID, "from SBB");
            endpoint.fireEvent(handle, fireableEventType, newEvent, null, null);

            tracer.info("Sleeping for 10 seconds.");
            Thread.sleep(10000);
            tracer.info("SBB processing resuming.");

            // Tell the TCK that activity has been suspended and we've fired something on it.

            tracer.info("Sending notification of event firing to TCK.");
            HashMap token = new HashMap();
            token.put("result-token", Boolean.TRUE);
            sendSbbMessage(sbbUID, sequenceID, RAMethods.suspendActivity, token);

            results.put("result-sbb1", Boolean.TRUE);
        } catch (Exception e) {
            tracer.severe("An exception occured during SBB event handler.", e);
            results.put("result-sbb1", e);
        }

        sendSbbMessage(sbbUID, sequenceID, RAMethods.suspendActivity, results);
        detach(aci);
    }

    private static final UOID sbbUID = UOID.createUOID();
}
