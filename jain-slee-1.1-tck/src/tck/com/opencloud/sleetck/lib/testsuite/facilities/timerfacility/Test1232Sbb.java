/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.facilities.timerfacility;

import com.opencloud.sleetck.lib.resource.events.TCKResourceEventX;
import com.opencloud.sleetck.lib.sbbutils.BaseTCKSbb;
import com.opencloud.sleetck.lib.sbbutils.TCKSbbUtils;

import javax.naming.InitialContext;
import javax.slee.ActivityContextInterface;
import javax.slee.facilities.TimerFacility;
import javax.slee.facilities.TimerID;
import javax.slee.facilities.TimerOptions;
import java.util.HashMap;

public abstract class Test1232Sbb extends BaseTCKSbb {

    private static final long DEFAULT_PERIOD = 100000;
    private static final String JNDI_TIMERFACILITY_NAME = "java:comp/env/slee/facilities/timer";

    // Initial event
    public void onTCKResourceEventX1(TCKResourceEventX ev, ActivityContextInterface aci) {

        try {
            HashMap map = new HashMap();

            TimerOptions options = new TimerOptions();
            options.setTimeout(DEFAULT_PERIOD * 2);

            TimerFacility facility = (TimerFacility) new InitialContext().lookup(JNDI_TIMERFACILITY_NAME);
            
            TimerID timerID = null;
            try {
                timerID = facility.setTimer(aci, null, System.currentTimeMillis(), DEFAULT_PERIOD, 1, options);
            } catch (IllegalArgumentException e) {
                map.put("Result", new Boolean(true));
                map.put("Message", "Ok");
                TCKSbbUtils.getResourceInterface().sendSbbMessage(map);
                return;
            }

            try {
                facility.cancelTimer(timerID);
            } catch (Exception e) {
            }

            map.put("Result", new Boolean(false));
            map.put("Message", "The specified TimerOptions timeout value was greater than the period specified in setTimer, but IllegalArgumentException was not thrown.");
            TCKSbbUtils.getResourceInterface().sendSbbMessage(map);            
        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }

    }

}
