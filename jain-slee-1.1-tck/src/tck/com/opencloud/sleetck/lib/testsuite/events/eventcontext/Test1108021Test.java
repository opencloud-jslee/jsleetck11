/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.events.eventcontext;

import java.rmi.RemoteException;
import java.util.Map;

import com.opencloud.sleetck.lib.AbstractSleeTCKTest;
import com.opencloud.sleetck.lib.TCKTestFailureException;
import com.opencloud.sleetck.lib.resource.TCKActivityID;
import com.opencloud.sleetck.lib.resource.TCKSbbMessage;
import com.opencloud.sleetck.lib.resource.events.TCKResourceEventX;
import com.opencloud.sleetck.lib.resource.testapi.TCKResourceListener;
import com.opencloud.sleetck.lib.resource.testapi.TCKResourceTestInterface;
import com.opencloud.sleetck.lib.testutils.Assert;
import com.opencloud.sleetck.lib.testutils.BaseTCKResourceListener;
import com.opencloud.sleetck.lib.testutils.FutureResult;

/**
 * AssertionID(1108021): Test The timeout period is measured from the time the
 * suspendDelivery method is invoked.
 */

public class Test1108021Test extends AbstractSleeTCKTest {

    public static final String SERVICE_DU_PATH_PARAM = "serviceDUPath";

    public static final String activityNameA = "Test1108021Test-ActivityA";

    public static final String activityNameB = "Test1108021Test-ActivityB";

    /**
     * Perform the actual test.
     */

    public void run(FutureResult result) throws Exception {
        this.result = result;

        TCKResourceTestInterface resource = utils().getResourceInterface();
        TCKActivityID activityA = resource.createActivity(activityNameA);
        TCKActivityID activityB = resource.createActivity(activityNameB);

        // TCK test starts activityA and fires event TCKResourceEventX1
        getLog().fine("Firing TCKResourceEventX1 on Test1108021Test-ActivityA.");
        resource.fireEvent(TCKResourceEventX.X1, testName, activityA, null);

        synchronized (this) {
            wait(1000);
        }

        // TCK test starts activityB and fires event TCKResourceEventX2
        getLog().fine("Firing TCKResourceEventX2 on Test1108021Test-ActivityB.");
        resource.fireEvent(TCKResourceEventX.X2, testName, activityB, null);

        synchronized (this) {
            wait(Test1108021Sbb.waitPeriodMs - 1000);
        }

        getLog().fine("Firing TCKResourceEventX3 on Test1108021Test-ActivityA.");
        resource.fireEvent(TCKResourceEventX.X3, testName, activityA, null);
    }

    /**
     * Do all the pre-run configuration of the test.
     */

    public void setUp() throws Exception {
        getLog().fine("Connecting to resource");
        resourceListener = new TCKResourceListenerImpl();
        setResourceListener(resourceListener);
        // testName = utils().getTestParams().getProperty("testName");
        getLog().fine("Installing and activating service");

        setupService(SERVICE_DU_PATH_PARAM, true);
    }

    /**
     * Clean up after the test.
     */

    public void tearDown() throws Exception {
        super.tearDown();
    }

    private class TCKResourceListenerImpl extends BaseTCKResourceListener {
        // TCKResourceListener methods

        public synchronized void onSbbMessage(TCKSbbMessage message, TCKActivityID calledActivity)
                throws RemoteException {

            Map sbbData = (Map) message.getMessage();
            String sbbTestName = "Test1108021Test";
            String sbbTestResult = (String) sbbData.get("result");
            String sbbTestMessage = (String) sbbData.get("message");
            int assertionID = ((Integer) sbbData.get("id")).intValue();
            getLog().info(
                    "Received message from SBB: testname=" + sbbTestName + ", result=" + sbbTestResult + ", message="
                            + sbbTestMessage + ", id=" + assertionID);
            try {
                Assert.assertEquals(assertionID, "Test " + sbbTestName + " failed.", "pass", sbbTestResult);
                result.setPassed();
            } catch (TCKTestFailureException ex) {
                result.setFailed(assertionID, sbbTestMessage);
            }
        }

        public void onException(Exception exception) throws RemoteException {
            getLog().warning("Received Exception from SBB or resource:");
            getLog().warning(exception);
            result.setError(exception);
        }
    }

    private TCKResourceListener resourceListener;

    private FutureResult result;

    private final static String testName = "Test1108021";;
}
