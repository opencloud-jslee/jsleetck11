/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.facilities.profilefacility;

public interface ProfileFacilityTestProfileCMP {
    public int getAttr1();
    public void setAttr1(int attr1);

    public int[] getAttr2();
    public void setAttr2(int[] attr2);

    public String getAttr3();
    public void setAttr3(String attr3);

    
    
}
