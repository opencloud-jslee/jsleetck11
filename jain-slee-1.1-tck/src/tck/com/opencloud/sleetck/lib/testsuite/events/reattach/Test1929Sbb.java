/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.events.reattach;

import com.opencloud.sleetck.lib.resource.events.TCKResourceEventX;
import com.opencloud.sleetck.lib.sbbutils.BaseTCKSbb;
import com.opencloud.sleetck.lib.sbbutils.TCKSbbUtils;

import javax.slee.ActivityContextInterface;
import javax.slee.Address;
import javax.slee.ChildRelation;
import javax.slee.facilities.Level;
import java.util.HashMap;

public abstract class Test1929Sbb extends BaseTCKSbb {

    // Initial event method.
    public void onTCKResourceEventX1(TCKResourceEventX event, ActivityContextInterface aci) {
        try {
            Test1929ChildSbbLocalObject child = (Test1929ChildSbbLocalObject) getChildRelation().create();
            aci.attach(child);
            child.setParentSbbLocalObject(getSbbContext().getSbbLocalObject());

            Test1929SecondChildSbbLocalObject secondChild = (Test1929SecondChildSbbLocalObject) getSecondChildRelation().create();
            aci.attach(secondChild);
            secondChild.setParentSbbLocalObject(getSbbContext().getSbbLocalObject());
            
            fireTest1929Event(new Test1929Event(), aci, null);
            fireTest1929SecondEvent(new Test1929SecondEvent(), aci, null);
        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }
    }

    public void onTest1929Event(Test1929Event event, ActivityContextInterface aci) {
        setCounter(getCounter() + 1);

        try {
            TCKSbbUtils.createTrace(getSbbID(), Level.FINE, "In onTest1929Event in parent SBB.", null);
        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }

    }

    public void onTest1929SecondEvent(Test1929SecondEvent event, ActivityContextInterface aci) {
        try {
            HashMap map = new HashMap();
            if (getCounter() == 2) {
                map.put("Result", new Boolean(true));
                map.put("Message", "Ok");
            } else {
                map.put("Result", new Boolean(false));
                map.put("Message", "Reattached SBB didn't receive fired event a second time (counter = " + getCounter() + ").");
            }
            TCKSbbUtils.getResourceInterface().sendSbbMessage(map);
        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }


    }


    public abstract void fireTest1929Event(Test1929Event event, ActivityContextInterface aci, Address address);
    public abstract void fireTest1929SecondEvent(Test1929SecondEvent event, ActivityContextInterface aci, Address address);

    public abstract void setCounter(int counter);
    public abstract int getCounter();

    public abstract ChildRelation getChildRelation();
    public abstract ChildRelation getSecondChildRelation();
}
