/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.events.maskreadystate;

import com.opencloud.sleetck.lib.SleeTCKTest;
import com.opencloud.sleetck.lib.SleeTCKTestUtils;
import com.opencloud.sleetck.lib.TCKTestResult;
import com.opencloud.sleetck.lib.infra.SleeTCKComponentConstants;
import com.opencloud.sleetck.lib.resource.TCKActivityID;
import com.opencloud.sleetck.lib.resource.TCKSbbMessage;
import com.opencloud.sleetck.lib.resource.events.TCKResourceEventX;
import com.opencloud.sleetck.lib.resource.testapi.TCKResourceListener;
import com.opencloud.sleetck.lib.resource.testapi.TCKResourceTestInterface;
import com.opencloud.sleetck.lib.testutils.BaseTCKResourceListener;
import com.opencloud.sleetck.lib.testutils.ComponentIDLookup;
import com.opencloud.sleetck.lib.testutils.FutureResult;
import com.opencloud.sleetck.lib.testutils.jmx.ProfileProvisioningMBeanProxy;

import javax.management.ObjectName;
import javax.slee.management.DeployableUnitID;
import javax.slee.profile.ProfileSpecificationID;
import java.rmi.RemoteException;

public class Test implements SleeTCKTest {

    // Test parameter keys
    private static final String SERVICE_DU_PATH_PARAM = "serviceDUPath";
    private static final String PROFILE_SPEC_DU_PATH_PARAM = "profileSpecDUPath";

    // SleeTCKTest methods

    public void init(SleeTCKTestUtils utils) { this.utils=utils; }

    public TCKTestResult run() throws Exception {
        futureResult = new FutureResult(utils.getLog());
        String activityName = "MaskEventReadyStateTestActivity";
        TCKResourceTestInterface resource = utils.getResourceInterface();
        utils.getLog().fine("Creating activity");
        TCKActivityID activityID = resource.createActivity(activityName);
        utils.getLog().info("Firing event");
        // send an event to the SBB
        resource.fireEvent(TCKResourceEventX.X1,TCKResourceEventX.X1,activityID,null);
        utils.getLog().fine("Test thread is waiting for test result");
        return futureResult.waitForResultOrFail(utils.getTestTimeout(),"Event must be delivered", 713);
    }

    public void setUp() throws Exception {
        utils.getLog().fine("Connecting to resource");
        resourceListener = new TCKResourceListenerImpl();
        utils.getResourceInterface().setResourceListener(resourceListener);
        utils.getLog().fine("Installing and activating service");
        String serviceDUPath = utils.getTestParams().getProperty(SERVICE_DU_PATH_PARAM);
        DeployableUnitID duID = utils.install(serviceDUPath);
        utils.activateServices(duID,true);
        utils.getLog().fine("Installing profile spec");
        String profileSpecDUPath = utils.getTestParams().getProperty(PROFILE_SPEC_DU_PATH_PARAM);
        utils.install(profileSpecDUPath);
        utils.getLog().fine("Creating a profile table, to create a profile table activity");
        ObjectName profileProvisioningName = utils.getSleeManagementMBeanProxy().getProfileProvisioningMBean();
        profileProvisioningMBeanProxy = utils.getMBeanProxyFactory().createProfileProvisioningMBeanProxy(profileProvisioningName);
        ProfileSpecificationID profileSpecID = new ComponentIDLookup(utils).lookupProfileSpecificationID(
                "SimpleProfile", SleeTCKComponentConstants.TCK_VENDOR, "1.0");
        utils.getLog().info("Creating profile table: " + AssertionSbb.PROFILE_TABLE_NAME);
        profileProvisioningMBeanProxy.createProfileTable(profileSpecID, AssertionSbb.PROFILE_TABLE_NAME);
        tableCreated = true;
    }

    public void tearDown() throws Exception {
        if (profileProvisioningMBeanProxy != null && tableCreated) {
            try {
                utils.getLog().fine("Removing profile table");
                profileProvisioningMBeanProxy.removeProfileTable(AssertionSbb.PROFILE_TABLE_NAME);
            } catch (Exception e) {
                utils.getLog().warning(e);
            }
        }
        utils.getLog().fine("Ending and purging activities");
        utils.getResourceInterface().clearActivities();
        utils.getLog().fine("Deactivating and uninstalling service and profile spec");
        utils.deactivateAllServices();
        utils.uninstallAll();
        utils.getLog().fine("Disconnecting from resource");
        utils.getResourceInterface().removeResourceListener();
    }

    // Private classes

    private class TCKResourceListenerImpl extends BaseTCKResourceListener {

        // TCKResourceListener methods

        public synchronized void onSbbMessage(TCKSbbMessage message, TCKActivityID calledActivity) throws RemoteException {
            utils.getLog().info("Received result from SBB");
            futureResult.setResult(TCKTestResult.fromExported(message.getMessage()));
        }

        public void onException(Exception exception) throws RemoteException {
            utils.getLog().warning("Received Exception from SBB or resource:");
            utils.getLog().warning(exception);
            futureResult.setError(exception);
        }

    }

    // Private state

    private SleeTCKTestUtils utils;
    private TCKResourceListener resourceListener;
    private FutureResult futureResult;
    private ProfileProvisioningMBeanProxy profileProvisioningMBeanProxy;
    private boolean tableCreated;

}
