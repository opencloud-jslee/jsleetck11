/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.usage.types;

import com.opencloud.sleetck.lib.TCKTestFailureException;
import com.opencloud.sleetck.lib.TCKTestResult;
import com.opencloud.sleetck.lib.testsuite.usage.common.GenericUsageTest;
import com.opencloud.sleetck.lib.testutils.Assert;

import javax.management.MBeanAttributeInfo;
import javax.management.MBeanInfo;
import javax.management.MBeanOperationInfo;
import javax.slee.usage.SampleStatistics;

/**
 * Declare counter-type parameters FirstCount and SecondCount, and sample-type parameters
 * TimeBetweenNewConnections and TimeBetweenErrors in the usage parameter interface.
 * Deploy the Sbb containing the usage parameters, then acces the Sbb usage Mbean.
 * Check that the return types of getFirstCount() and getSecondCount() are long, and that the return types
 * of getTimeBetweenNewConnections() and getTimeBetweenErrors() are SampleStatistics.
 */
public class Test2216Test extends GenericUsageTest {

    public TCKTestResult run() throws Exception {
        // inspect the SBB usage MBean
        String firstCountType = null;
        String secondCountType = null;
        String timeBetweenNewConnectionsType = null;
        String timeBetweenErrorsType = null;

        MBeanInfo mBeanInfo = utils().getMBeanFacade().getMBeanInfo(getGenericUsageMBeanLookup().getUnnamedSbbUsageMBeanName());
        MBeanAttributeInfo[] attributeInfos = mBeanInfo.getAttributes();
        getLog().finest("Searching MBean attributes...");
        for (int i = 0; i < attributeInfos.length; i++) {
            MBeanAttributeInfo attributeInfo = attributeInfos[i];
            if(attributeInfo.getName().equals("firstCount")) firstCountType = attributeInfo.getType();
            if(attributeInfo.getName().equals("secondCount")) secondCountType = attributeInfo.getType();
            if(attributeInfo.getName().equals("timeBetweenNewConnections")) timeBetweenNewConnectionsType = attributeInfo.getType();
            if(attributeInfo.getName().equals("timeBetweenErrors")) timeBetweenErrorsType = attributeInfo.getType();
        }
        getLog().finest("Searching MBean operations...");
        if(firstCountType == null || timeBetweenNewConnectionsType == null) {
            MBeanOperationInfo[] mBeanOperationInfos = mBeanInfo.getOperations();
            for (int i = 0; i < mBeanOperationInfos.length; i++) {
                MBeanOperationInfo mBeanOperationInfo = mBeanOperationInfos[i];
                if(mBeanOperationInfo.getName().equals("getFirstCount")) firstCountType = mBeanOperationInfo.getReturnType();
                if(mBeanOperationInfo.getName().equals("getSecondCount")) secondCountType = mBeanOperationInfo.getReturnType();
                if(mBeanOperationInfo.getName().equals("getTimeBetweenNewConnections")) timeBetweenNewConnectionsType = mBeanOperationInfo.getReturnType();
                if(mBeanOperationInfo.getName().equals("getTimeBetweenErrors")) timeBetweenErrorsType = mBeanOperationInfo.getReturnType();
            }
        }
        if(firstCountType == null) throw new TCKTestFailureException(2241,"Counldn't find attribute type for firstCount parameter");
        if(secondCountType == null) throw new TCKTestFailureException(2241,"Counldn't find attribute type for secondCount parameter");
        if(timeBetweenNewConnectionsType == null) throw new TCKTestFailureException(2241,"Counldn't find attribute type for timeBetweenNewConnections parameter");
        if(timeBetweenErrorsType == null) throw new TCKTestFailureException(2241,"Counldn't find attribute type for timeBetweenErrors parameter");

        getLog().info("MBean features detected. Validating...");
        Assert.assertEquals(2323,"Counter-type parameter not recognized. Invalid return type from MBean.","long", firstCountType);
        Assert.assertEquals(2323,"Counter-type parameter not recognized. Invalid return type from MBean.","long", secondCountType);
        Assert.assertEquals(2324,"Sample-type parameter not recognized. Invalid return type from MBean.",SampleStatistics.class.getName(), timeBetweenNewConnectionsType);
        Assert.assertEquals(2324,"Sample-type parameter not recognized. Invalid return type from MBean.",SampleStatistics.class.getName(), timeBetweenErrorsType);
        getLog().info("MBean features valid.");

        return TCKTestResult.passed();
    }

}
