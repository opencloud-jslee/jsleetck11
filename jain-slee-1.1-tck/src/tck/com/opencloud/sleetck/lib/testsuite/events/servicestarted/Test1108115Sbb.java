/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.events.servicestarted;

import java.util.HashMap;

import javax.naming.InitialContext;
import javax.slee.ActivityContextInterface;
import javax.slee.EventContext;
import javax.slee.InitialEventSelector;
import javax.slee.ServiceID;
import javax.slee.facilities.Tracer;
import javax.slee.serviceactivity.ServiceActivityContextInterfaceFactory;
import javax.slee.serviceactivity.ServiceActivityFactory;
import javax.slee.serviceactivity.ServiceStartedEvent;

import com.opencloud.sleetck.lib.sbbutils.BaseTCKSbb;
import com.opencloud.sleetck.lib.sbbutils.TCKSbbUtils;

/**
 * AssertionID(1108115): Test of the getService method returns the Service
 * identifier of the ServiceActivity object’s Service.
 * 
 * AssertionID(1108119): Test of the getService method returns the Service
 * identifier of the Service that has started.
 * 
 * AssertionID(1108121): Test of The JNDI_NAME constant. This constant specifies
 * the JNDI location where a ServiceActivityFactory object may be located by an
 * SBB component in its component environment.
 * 
 * AssertionID(1108123): Test of The JNDI_NAME constant. This constant specifies
 * the JNDI location where a ServiceActivityContextInterface- Factory object may
 * be located by an SBB component in its component environement.
 * 
 */
public abstract class Test1108115Sbb extends BaseTCKSbb {

    public InitialEventSelector initialEventSelectorMethod(InitialEventSelector ies) {
        // set the custom name variable to true to test that this value is not
        // held accross invocations
        ies.setCustomName("test");
        return ies;
    }

    public void onServiceStartedEvent(ServiceStartedEvent event, ActivityContextInterface aci, EventContext context) {
        try {
            tracer = getSbbContext().getTracer("com.test");
            tracer.info("Sbb: Received a TCK event " + event);

            ServiceID serviceID = new ServiceID("Test1108115Service", "jain.slee.tck", "1.1");
            
            // 1108115
            try {
                ServiceActivityFactory factory = (ServiceActivityFactory) new InitialContext().lookup(ServiceActivityFactory.JNDI_NAME);

                // 1108121
                    try {

                        if (!context.getService().equals(serviceID)) {
                            sendResultToTCK(1108115,
                                    "ServiceActivity.getService() did not match the correct ServiceID object by EventContext.", "Test1108115Test", false);
                            return;
                        }
                    } catch (Exception e) {
                        sendResultToTCK(1108115,
                                "ServiceActivity.getService() did not return the correct ServiceID object.", "Test1108115Test", false);
                        return;
                    }
            } catch (Exception e) {
                sendResultToTCK(1108121, "The ServiceActivityFactory was not available at the SLEE-defined location: "
                        + ServiceActivityFactory.JNDI_NAME, "Test1108115Test", false);
                return;
            }

            // 1108119
            try {
                serviceID = event.getService();
                if (!context.getService().equals(serviceID)) {
                    sendResultToTCK(1108119,
                            "ServiceStartedEvent.getService() did not match the correct ServiceID object by EventContext.", "Test1108115Test", false);
                    return;
                }
            } catch (Exception e) {
                sendResultToTCK(1108119,
                        "ServiceStartedEvent.getService() did not return the correct ServiceID object.", "Test1108115Test", false);
                return;
            }

            // 1108123
            try {
                ServiceActivityContextInterfaceFactory aciFactory = (ServiceActivityContextInterfaceFactory) new InitialContext()
                        .lookup(ServiceActivityContextInterfaceFactory.JNDI_NAME);
            } catch (Exception e) {
                sendResultToTCK(1108123, "The ServiceActivityFactory was not available at the SLEE-defined location: "
                        + ServiceActivityContextInterfaceFactory.JNDI_NAME, "Test1108115Test", false);
                return;
            }

            sendResultToTCK(1108115, "Test of Test1108115 Passed.", "Test1108115Test", true);

        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }
    }

    private void sendResultToTCK(int failedAssertionID, String message, String testName, boolean result) throws Exception {
        HashMap sbbData = new HashMap();
        sbbData.put("testname", testName);
        sbbData.put("result", result ? "pass" : "fail");
        sbbData.put("message", message);
        sbbData.put("id", new Integer(failedAssertionID));
        TCKSbbUtils.getResourceInterface().sendSbbMessage(sbbData);
    }

    private Tracer tracer;

}
