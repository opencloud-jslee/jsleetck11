/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.resource.events;

import java.util.HashMap;

import javax.slee.management.SleeState;
import javax.slee.resource.ConfigProperties;
import javax.slee.resource.ResourceAdaptorID;

import com.opencloud.sleetck.lib.AbstractSleeTCKTest;
import com.opencloud.sleetck.lib.TCKTestFailureException;
import com.opencloud.sleetck.lib.TCKTestResult;
import com.opencloud.sleetck.lib.infra.SleeTCKComponentConstants;
import com.opencloud.sleetck.lib.profileutils.BaseMessageAdapter;
import com.opencloud.sleetck.lib.rautils.MessageHandlerRegistry;
import com.opencloud.sleetck.lib.rautils.RMIObjectChannel;
import com.opencloud.sleetck.lib.testutils.FutureResult;
import com.opencloud.sleetck.lib.testutils.jmx.ResourceManagementMBeanProxy;

/**
  * 1115042: If the SLEE is in the Stopping state, resource adaptor objects associated
  * with the resource adaptor entity cannot start new activities, but may submit events
  * on existing activities and end activities.
  */
public class Test1115042Test extends AbstractSleeTCKTest {

    private static final String RA_NAME = "Test1115042RA";
    private static final String RA_VENDOR = SleeTCKComponentConstants.TCK_VENDOR;
    private static final String RA_VERSION = "1.1";

    private static final String RESOURCE_DU_PATH_PARAM = "RADUPath";

    public TCKTestResult run() throws Exception {
        HashMap map;
        TCKTestResult res;

        String raEntityName = RA_NAME+"_Entity";
        futureResult = new FutureResult(getLog());
        ResourceManagementMBeanProxy resourceMBean = utils().getResourceManagementMBeanProxy();

        // Create the RA Entity.
        getLog().debug("Create RA Entity...");
        ResourceAdaptorID raID = new ResourceAdaptorID("Test1115042RA", RA_VENDOR, RA_VERSION);
        ConfigProperties configProperties = new ConfigProperties();
        resourceMBean.createResourceAdaptorEntity(raID, raEntityName, configProperties);

        getLog().debug("Activate the RA Entity...");
        // Activate the RA Entity
        resourceMBean.activateResourceAdaptorEntity(raEntityName);

        //Send msg to the RA Entity to create and register an activity
        map = new HashMap();
        map.put("Type", new Integer(Test1115042MessageListener.CREATE_VALID_ACTIVITY));
        out.sendMessage(map);
        res = futureResult.waitForResult(utils().getTestTimeout());
        if (!res.isPassed())
            return res;

        futureResult = new FutureResult(getLog());

        // stop the slee
        getLog().debug("Stop the SLEE...");
        utils().getSleeManagementMBeanProxy().stop();

        // wait for 2s
        Thread.sleep(5000);

        // check that SLEE state is STOPPING
        SleeState state = utils().getSleeManagementMBeanProxy().getState();
        if (!SleeState.STOPPING.equals(state))
            return TCKTestResult.error("SLEE should have transitioned into the STOPPING state but was found to " +
                    "reside in the state "+state);
        getLog().debug("SLEE is now in the STOPPING state.");

        //send message to RA entity which starts doing the tests.
        map = new HashMap();
        map.put("Type", new Integer(Test1115042MessageListener.CHECK_ASSERTION));
        out.sendMessage(map);

        return futureResult.waitForResult(utils().getTestTimeout());
    }

    public void setUp() throws Exception {

        setupService(RESOURCE_DU_PATH_PARAM);

        in = utils().getRMIObjectChannel();
        in.setMessageHandler(new BaseMessageAdapter(getLog()) {
            public void onSetPassed(int assertionID, String msg) { setPassed(assertionID, msg); }
            public void onSetFailed(int assertionID, String msg, Exception e) { setFailed(assertionID, msg, e); }
            public void onLogCall(String msg) { getLog().fine(msg); }
            public void onSetError(String msg, Exception e) { setError(msg, e); }
        });

        out = utils().getMessageHandlerRegistry();
    }

    public void tearDown() throws Exception {
        try {
            in.clearQueue();
            utils().removeRAEntities();
        } finally {
            super.tearDown();
        }
    }

    protected void setPassed(int assertionID, String msg) {
        getLog().fine(assertionID+": "+msg);
        if (futureResult!=null && !futureResult.isSet())
            futureResult.setPassed();
    }

    protected void setFailed(int assertionID, String msg, Exception e) {
        if (e==null) {
            getLog().fine("FAILURE: "+assertionID+":"+msg);
            if (futureResult!=null && !futureResult.isSet())
                futureResult.setFailed(assertionID, msg);
        }
        else {
            getLog().fine("FAILURE: "+assertionID+":"+msg);
            getLog().fine(e);
            if (futureResult!=null && !futureResult.isSet())
                futureResult.setFailed(new TCKTestFailureException(assertionID, msg, e));
        }
    }

    protected void setError(String msg, Exception e) {
        if (e==null) {
            getLog().warning(msg);
            if (futureResult!=null && !futureResult.isSet())
                futureResult.setError(msg);
        }
        else {
            getLog().warning(msg);
            getLog().warning(e);
            if (futureResult!=null && !futureResult.isSet())
                futureResult.setError(msg, e);
        }
    }

    private MessageHandlerRegistry out;
    private RMIObjectChannel in;
    private FutureResult futureResult;
}
