/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.sbb.abstractclass;

import java.rmi.RemoteException;
import java.util.HashMap;

import com.opencloud.sleetck.lib.AbstractSleeTCKTest;
import com.opencloud.sleetck.lib.TCKTestResult;
import com.opencloud.sleetck.lib.resource.TCKActivityID;
import com.opencloud.sleetck.lib.resource.TCKSbbMessage;
import com.opencloud.sleetck.lib.resource.events.TCKResourceEventX;
import com.opencloud.sleetck.lib.resource.testapi.TCKResourceListener;
import com.opencloud.sleetck.lib.resource.testapi.TCKResourceTestInterface;
import com.opencloud.sleetck.lib.testutils.BaseTCKResourceListener;
import com.opencloud.sleetck.lib.testutils.FutureResult;

public class Test2121Test extends AbstractSleeTCKTest {

    private static final String SERVICE_DU_PATH_PARAM = "DUPath";
    private static final int TEST_ID = 2121;

    /**
     * Perform the actual test.
     */
    public TCKTestResult run() throws Exception {
        result = new FutureResult(getLog());

        TCKResourceTestInterface resource = utils().getResourceInterface();
        TCKActivityID activityID = resource.createActivity("Test2121InitialActivity");
        resource.fireEvent(TCKResourceEventX.X1, TCKResourceEventX.X1, activityID, null);

        return result.waitForResultOrFail(utils().getTestTimeout(), "Timeout waiting for test result.", TEST_ID);
    }

    /**
     * Do all the pre-run configuration of the test.
     */
    public void setUp() throws Exception {

        resourceListener = new TCKResourceListenerImpl();
        setResourceListener(resourceListener);

        setupService(SERVICE_DU_PATH_PARAM);
    }

    private class TCKResourceListenerImpl extends BaseTCKResourceListener {
        public synchronized void onSbbMessage(TCKSbbMessage message, TCKActivityID calledActivity) throws RemoteException {

            HashMap map = (HashMap) message.getMessage();

            String type = (String) map.get("Type");
            if (type.equals("InitialTXN"))
            initTXN = map.get("TXN");

            if (type.equals("RollbackTXN")) {
            Object rollbackTXN = map.get("TXN");

            if (initTXN == null)
                result.setFailed(TEST_ID, "Did not receive initial TXN id from event handler.");

            if (rollbackTXN.equals(initTXN))
                result.setFailed(TEST_ID, "sbbRolledBack() was called with the same TXN as that which rolled back.");
            else
                result.setPassed();
            }
        }

        public void onException(Exception e) throws RemoteException {
            getLog().warning("Received exception from SBB");
            getLog().warning(e);
            result.setError(e);
        }
    }

    private Object initTXN = null;

    private TCKResourceListener resourceListener;
    private FutureResult result;
}
