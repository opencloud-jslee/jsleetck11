/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.usage.scope;

import com.opencloud.sleetck.lib.AbstractSleeTCKTest;
import com.opencloud.sleetck.lib.TCKTestResult;
import com.opencloud.sleetck.lib.resource.TCKActivityID;
import com.opencloud.sleetck.lib.resource.events.TCKResourceEventX;
import com.opencloud.sleetck.lib.resource.events.TCKResourceEventY;
import com.opencloud.sleetck.lib.testsuite.usage.common.GenericUsageSbbInstructions;
import com.opencloud.sleetck.lib.testsuite.usage.common.UsageMBeanLookup;
import com.opencloud.sleetck.lib.testutils.QueuingNotificationListener;
import com.opencloud.sleetck.lib.testutils.QueuingResourceListener;

/**
 * Makes updates to parameters of the same name and same parameter set name,
 * in the same SBB, in different Services.
 */
public class Test2244Test extends AbstractSleeTCKTest {

    private static final int TEST_ID = 2244;
    private static final String PARAMETER_SET_NAME = "Test2244Test-ParameterSet";

    public TCKTestResult run() throws Exception {

        TCKActivityID activityID = utils().getResourceInterface().createActivity("token activity");

        GenericUsageSbbInstructions sbbInstructionsA = new GenericUsageSbbInstructions(PARAMETER_SET_NAME);
        sbbInstructionsA.addFirstCountIncrement(1);
        sbbInstructionsA.addTimeBetweenNewConnectionsSamples(2);
        getLog().info("firing event to SBB in service A");
        utils().getResourceInterface().fireEvent(TCKResourceEventX.X1,sbbInstructionsA.toExported(),activityID,null);

        GenericUsageSbbInstructions sbbInstructionsB = new GenericUsageSbbInstructions(PARAMETER_SET_NAME);
        sbbInstructionsB.addFirstCountIncrement(3);
        sbbInstructionsB.addTimeBetweenNewConnectionsSamples(4);
        getLog().info("firing event to SBB in service B");
        utils().getResourceInterface().fireEvent(TCKResourceEventY.Y1,sbbInstructionsB.toExported(),activityID,null);

        getLog().info("waiting for replies from both SBBs");
        resourceListener.nextMessage();
        resourceListener.nextMessage();
        getLog().info("received replies");

        getLog().info("waiting for 2 usage notifications for each SBB");
        for (int i = 0; i < 2; i++) {
              notificationListenerServiceA.nextNotification();
        }
        for (int i = 0; i < 2; i++) {
              notificationListenerServiceB.nextNotification();
        }
        getLog().info("received all 4 usage notifications");

        getLog().info("checking that parameter updates to both SBBs were independent");

        long firstCountForServiceA  = mBeanLookupServiceA.getNamedSbbUsageMBeanProxy(PARAMETER_SET_NAME).getCounterParameter("firstCount",false);
        long firstCountForServiceB  = mBeanLookupServiceB.getNamedSbbUsageMBeanProxy(PARAMETER_SET_NAME).getCounterParameter("firstCount",false);
        double timeBetweenNewConnectionsMeanForServiceA =
            mBeanLookupServiceA.getNamedSbbUsageMBeanProxy(PARAMETER_SET_NAME).getSampleParameter("timeBetweenNewConnections",false).getMean();
        double timeBetweenNewConnectionsMeanForServiceB =
            mBeanLookupServiceB.getNamedSbbUsageMBeanProxy( PARAMETER_SET_NAME).getSampleParameter("timeBetweenNewConnections",false).getMean();

        String expectedValues = "firstCountForServiceA="+1+",firstCountForServiceB="+3+
            "timeBetweenNewConnectionsMeanForServiceA="+2+"timeBetweenNewConnectionsMeanForServiceB="+4;
        String actualValues = "firstCountForServiceA="+firstCountForServiceA+",firstCountForServiceB="+firstCountForServiceB+
            "timeBetweenNewConnectionsMeanForServiceA="+timeBetweenNewConnectionsMeanForServiceA+"timeBetweenNewConnectionsMeanForServiceB="+timeBetweenNewConnectionsMeanForServiceB;

        if(firstCountForServiceA == 4 || firstCountForServiceB == 4)
            return TCKTestResult.failed(TEST_ID,"Updates to firstCount usage parameter in the same parameter set "+
                "in the same in SBB in different services were combined -- the parameter sets should be independent.");
        if(timeBetweenNewConnectionsMeanForServiceA == 3 || timeBetweenNewConnectionsMeanForServiceB == 3)
            return TCKTestResult.failed(TEST_ID,"Updates to timeBetweenNewConnections usage parameter in the same parameter set "+
                "in the same in SBB in different services were combined -- the parameter sets should be independent.");
        if(firstCountForServiceA != 1 || firstCountForServiceB != 3 || timeBetweenNewConnectionsMeanForServiceA != 2 || timeBetweenNewConnectionsMeanForServiceB != 4) {
            return TCKTestResult.failed(TEST_ID,"Usage parameter values were not set as expected. Expected values: "+
                    expectedValues+", actual values: "+actualValues);
        }
        getLog().info("parameter checks ok");
        return TCKTestResult.passed();
    }

    public void setUp() throws Exception {
        super.setUp();

        setResourceListener(resourceListener = new QueuingResourceListener(utils()));

        mBeanLookupServiceA = new UsageMBeanLookup("Test2244ServiceA","GenericUsageSbb",utils());
        mBeanLookupServiceB = new UsageMBeanLookup("Test2244ServiceB","GenericUsageSbb",utils());
        mBeanLookupServiceA.getServiceUsageMBeanProxy().createUsageParameterSet(mBeanLookupServiceA.getSbbID(),PARAMETER_SET_NAME);
        mBeanLookupServiceB.getServiceUsageMBeanProxy().createUsageParameterSet(mBeanLookupServiceB.getSbbID(),PARAMETER_SET_NAME);
        notificationListenerServiceA = new QueuingNotificationListener(utils());
        notificationListenerServiceB  = new QueuingNotificationListener(utils());
        mBeanLookupServiceA.getNamedSbbUsageMBeanProxy(PARAMETER_SET_NAME).addNotificationListener(notificationListenerServiceA,null,null);
        mBeanLookupServiceB.getNamedSbbUsageMBeanProxy(PARAMETER_SET_NAME).addNotificationListener(notificationListenerServiceB,null,null);
    }

    public void tearDown() throws Exception {
        if(mBeanLookupServiceA != null && notificationListenerServiceA != null) {
            mBeanLookupServiceA.getNamedSbbUsageMBeanProxy(PARAMETER_SET_NAME).removeNotificationListener(notificationListenerServiceA);
        }
        if(mBeanLookupServiceB != null && notificationListenerServiceB != null) {
            mBeanLookupServiceB.getNamedSbbUsageMBeanProxy( PARAMETER_SET_NAME).removeNotificationListener(notificationListenerServiceB);
        }
        if(mBeanLookupServiceA != null)mBeanLookupServiceA.closeAllMBeans();
        if(mBeanLookupServiceB  != null)mBeanLookupServiceB.closeAllMBeans();
        super.tearDown();
    }

    private UsageMBeanLookup mBeanLookupServiceA;
    private UsageMBeanLookup mBeanLookupServiceB;
    private QueuingResourceListener resourceListener;
    private QueuingNotificationListener notificationListenerServiceA;
    private QueuingNotificationListener notificationListenerServiceB;

}
