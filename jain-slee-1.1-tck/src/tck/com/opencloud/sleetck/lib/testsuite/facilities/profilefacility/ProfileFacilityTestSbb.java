/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.facilities.profilefacility;

import com.opencloud.sleetck.lib.resource.events.TCKResourceEventX;
import com.opencloud.sleetck.lib.sbbutils.BaseTCKSbb;
import com.opencloud.sleetck.lib.sbbutils.TCKSbbUtils;

import javax.naming.InitialContext;
import javax.slee.ActivityContextInterface;
import javax.slee.Address;
import javax.slee.facilities.Level;
import javax.slee.profile.*;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;

/**
 * Test ProfileFacility functions from spec s12.7.
 */
public abstract class ProfileFacilityTestSbb extends BaseTCKSbb {

    public void onTCKResourceEventX1(TCKResourceEventX event, ActivityContextInterface aci) {

        try {
            setTestName((String)event.getMessage());
            TCKSbbUtils.createTrace(getSbbID(),Level.INFO,"Received "+getTestName()+" message",null);

            if (getTestName().equals("jndi")) doJNDITest();
            else if (getTestName().equals("exception")) doExceptionTest();
            else if (getTestName().equals("getProfiles")) doGetProfilesTest();
            else if (getTestName().equals("getProfile")) doGetProfileTest();
            else if (getTestName().equals("getProfilesIterator")) doGetProfilesIteratorTest();

            // send result response
            fireSendResultEvent(new SendResultEvent(), aci, null);

        } catch(Exception e) {
            TCKSbbUtils.handleException(e);
        }
    }

    public void onSendResultEvent(SendResultEvent event, ActivityContextInterface aci) {
        try {
            sendResultToTCK();
        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }
    }

    private void doJNDITest() throws Exception {
        ProfileFacility pf = getProfileFacility();
        if (pf != null) setResultPassed("Found ProfileFacility object in JNDI at " + JNDI_PROFILEFACILITY_NAME);
        else setResultFailed(1414, "Could not find ProfileFacility object in JNDI at " + JNDI_PROFILEFACILITY_NAME);
    }

    private void sendResultToTCK() throws Exception {
        HashMap sbbData = new HashMap();
        sbbData.put("testname", getTestName());
        sbbData.put("result", getResult() ?"pass":"fail");
        sbbData.put("message", getMessage());
        sbbData.put("id", new Integer(getFailedAssertionID()));
        TCKSbbUtils.getResourceInterface().sendSbbMessage(sbbData);
    }

    private void setResultFailed(int assertionID, String msg) throws Exception {
        setResult(false);
        setFailedAssertionID(assertionID);
        setMessage("Assertion " + assertionID + " failed: " + msg);
        TCKSbbUtils.createTrace(getSbbID(), Level.WARNING, getMessage(), null);
    }

    private void setResultPassed(String msg) throws Exception {
        setResult(true);
        setMessage("Success: " + msg);
        TCKSbbUtils.createTrace(getSbbID(), Level.INFO, getMessage(), null);
    }


    private void doExceptionTest() throws Exception {
        ProfileFacility pf = getProfileFacility();

        // test for NullPointerException if any argument is null - Assertion 1393
        try {
            pf.getProfilesByIndexedAttribute(null, "attr1", new Integer(0));
            setResultFailed(1393, "getProfilesByIndexedAttribute with null profileTableName did not throw NullPointerException");
            return;
        } catch (NullPointerException npe) {
            TCKSbbUtils.createTrace(getSbbID(),Level.INFO, "got expected NullPointerException", null);
        } catch (Exception e) {
            setResultFailed(1393, "getProfilesByIndexedAttribute with null profileTableName threw unexpected exception: " + e);
            return;
        }
        try {
            pf.getProfilesByIndexedAttribute("ProfileFacilityTestTable", null, new Integer(0));
            setResultFailed(1393, "getProfilesByIndexedAttribute with null attributeName did not throw NullPointerException");
            return;
        } catch (NullPointerException npe) {
            TCKSbbUtils.createTrace(getSbbID(),Level.INFO, "got expected NullPointerException", null);
        } catch (Exception e) {
            setResultFailed(1393, "getProfilesByIndexedAttribute with null attributeName threw unexpected exception: " + e);
            return;
        }
        try {
            pf.getProfilesByIndexedAttribute("ProfileFacilityTestTable", "attr1", null);
            setResultFailed(1393, "getProfilesByIndexedAttribute with null attributeValue did not throw NullPointerException");
            return;
        } catch (NullPointerException npe) {
            TCKSbbUtils.createTrace(getSbbID(),Level.INFO, "got expected NullPointerException", null);
        } catch (Exception e) {
            setResultFailed(1393, "getProfilesByIndexedAttribute with null attributeValue threw unexpected exception: " + e);
            return;
        }
        try {
            pf.getProfileByIndexedAttribute(null, "attr1", new Integer(0));
            setResultFailed(1393, "getProfileByIndexedAttribute with null profileTableName did not throw NullPointerException");
            return;
        } catch (NullPointerException npe) {
            TCKSbbUtils.createTrace(getSbbID(),Level.INFO, "got expected NullPointerException", null);
        } catch (Exception e) {
            setResultFailed(1393, "getProfileByIndexedAttribute with null profileTableName threw unexpected exception: " + e);
            return;
        }
        try {
            pf.getProfileByIndexedAttribute("ProfileFacilityTestTable", null, new Integer(0));
            setResultFailed(1393, "getProfileByIndexedAttribute with null attributeName did not throw NullPointerException");
            return;
        } catch (NullPointerException npe) {
            TCKSbbUtils.createTrace(getSbbID(),Level.INFO, "got expected NullPointerException", null);
        } catch (Exception e) {
            setResultFailed(1393, "getProfileByIndexedAttribute with null attributeName threw unexpected exception: " + e);
            return;
        }
        try {
            pf.getProfileByIndexedAttribute("ProfileFacilityTestTable", "attr1", null);
            setResultFailed(1393, "getProfileByIndexedAttribute with null attributeValue did not throw NullPointerException");
            return;
        } catch (NullPointerException npe) {
            TCKSbbUtils.createTrace(getSbbID(),Level.INFO, "got expected NullPointerException", null);
        } catch (Exception e) {
            setResultFailed(1393, "getProfileByIndexedAttribute with null attributeValue threw unexpected exception: " + e);
            return;
        }

        // Check for UnrecognizedProfileTableNameException - Assertion 1390
        try {
            pf.getProfilesByIndexedAttribute("ThisTableDoesNotExist", "attr1", new Integer(0));
            setResultFailed(1390, "getProfilesByIndexedAttribute with non-existent profileTableName did not throw UnrecognizedProfileTableNameException");
            return;
        } catch (UnrecognizedProfileTableNameException uptne) {
            TCKSbbUtils.createTrace(getSbbID(),Level.INFO, "got expected UnrecognizedProfileTableNameException", null);
        } catch (Exception e) {
            setResultFailed(1390, "getProfilesByIndexedAttribute with non-existent profileTableName threw unexpected exception: " + e);
            return;
        }
        try {
            pf.getProfileByIndexedAttribute("ThisTableDoesNotExist", "attr1", new Integer(0));
            setResultFailed(1390, "getProfileByIndexedAttribute with non-existent profileTableName did not throw UnrecognizedProfileTableNameException");
            return;
        } catch (UnrecognizedProfileTableNameException uptne) {
            TCKSbbUtils.createTrace(getSbbID(),Level.INFO, "got expected UnrecognizedProfileTableNameException", null);
        } catch (Exception e) {
            setResultFailed(1390, "getProfileByIndexedAttribute with non-existent profileTableName threw unexpected exception: " + e);
            return;
        }

        // Check for UnrecognizedAttributeException - Assertion 1391
        try {
            pf.getProfilesByIndexedAttribute("ProfileFacilityTestTable", "thisAttrDoesNotExist", "ProfileFacilityTest");
            setResultFailed(1391, "getProfilesByIndexedAttribute with non-existent attributeName did not throw UnrecognizedProfileTableNameException");
            return;
        } catch (UnrecognizedAttributeException uae) {
            TCKSbbUtils.createTrace(getSbbID(),Level.INFO, "got expected UnrecognizedAttributeException", null);
        } catch (Exception e) {
            setResultFailed(1391, "getProfilesByIndexedAttribute with non-existent attributeName threw unexpected exception: " + e);
            return;
        }
        try {
            pf.getProfileByIndexedAttribute("ProfileFacilityTestTable", "thisAttrDoesNotExist", "ProfileFacilityTest");
            setResultFailed(1391, "getProfileByIndexedAttribute with non-existent attributeName did not throw UnrecognizedProfileTableNameException");
            return;
        } catch (UnrecognizedAttributeException uae) {
            TCKSbbUtils.createTrace(getSbbID(),Level.INFO, "got expected UnrecognizedProfileTableNameException", null);
        } catch (Exception e) {
            setResultFailed(1391, "getProfileByIndexedAttribute with non-existent attributeName threw unexpected exception: " + e);
            return;
        }

        // Check for AttributeNotIndexedException - Assertion 1392
        // attr3 (a String) in our test profile table is not indexed.
        try {
            pf.getProfilesByIndexedAttribute("ProfileFacilityTestTable", "attr3", "ProfileFacilityTest");
            setResultFailed(1392, "getProfilesByIndexedAttribute with non-indexed attribute did not throw AttributeNotIndexedException");
            return;
        } catch (AttributeNotIndexedException anie) {
            TCKSbbUtils.createTrace(getSbbID(),Level.INFO, "got expected AttributeNotIndexedException", null);
        } catch (Exception e) {
            setResultFailed(1392, "getProfilesByIndexedAttribute with non-indexed attribute threw unexpected exception: " + e);
            return;
        }
        try {
            pf.getProfileByIndexedAttribute("ProfileFacilityTestTable", "attr3", "ProfileFacilityTest");
            setResultFailed(1392, "getProfileByIndexedAttribute with non-indexed attribute did not throw AttributeNotIndexedException");
            return;
        } catch (AttributeNotIndexedException anie) {
            TCKSbbUtils.createTrace(getSbbID(),Level.INFO, "got expected AttributeNotIndexedException", null);
        } catch (Exception e) {
            setResultFailed(1392, "getProfileByIndexedAttribute with non-indexed attribute threw unexpected exception: " + e);
            return;
        }

        // Check for IllegalArgumentException - Assertion 1389
        // attr1 in our test profile table in an int(eger). If we search for a String value we should see an
        // InvalidArgumentException.
        try {
            pf.getProfilesByIndexedAttribute("ProfileFacilityTestTable", "attr1", "foo");
            setResultFailed(1389, "getProfilesByIndexedAttribute with inappropriate attributeValue type did not throw InvalidArgumentException");
            return;
        } catch (AttributeTypeMismatchException iae) {
            TCKSbbUtils.createTrace(getSbbID(),Level.INFO, "got expected InvalidArgumentException", null);
        } catch (Exception e) {
            setResultFailed(1389, "getProfilesByIndexedAttribute with inappropriate attributeValue type threw unexpected exception: " + e);
            return;
        }
        try {
            pf.getProfileByIndexedAttribute("ProfileFacilityTestTable", "attr1", "foo");
            setResultFailed(1389, "getProfileByIndexedAttribute with inappropriate attributeValue type did not throw InvalidArgumentException");
            return;
        } catch (AttributeTypeMismatchException iae) {
            TCKSbbUtils.createTrace(getSbbID(),Level.INFO, "got expected InvalidArgumentException", null);
        } catch (Exception e) {
            setResultFailed(1389, "getProfileByIndexedAttribute with inappropriate attributeValue type threw unexpected exception: " + e);
            return;
        }

/*
        // Check for immutable iterator - Assertion 1394
        Iterator it = pf.getProfilesByIndexedAttribute("ProfileFacilityTestTable", "attr1", new Integer(1)); // should return 1 profile
        ProfileID profileID = (ProfileID)it.next();
        try {
            it.remove(); // should throw UnsupportedOperationException
            setResultFailed(1394, "Iterator returned by getProfilesByIndexedAttribute did not throw UnsupportedOperationException on remove()");
            return;
        } catch (UnsupportedOperationException uoe) {
            TCKSbbUtils.createTrace(getSbbID(),Level.INFO, "got expected UnsupportedOperationException", null);
        } catch (Exception e) {
            setResultFailed(1394, "Iterator returned by getProfilesByIndexedAttribute threw unexpected exception on remove(): " + e);
            return;
        }
 */

        // +4448-4456
        // Attempt to remove a profile via the iterator -- remove() method should throw UnupportedOperationException

        Collection collection = pf.getProfiles("ProfileFacilityTestTable");
        Iterator it = collection.iterator();
        ProfileID id = (ProfileID) it.next();
        try {
            it.remove();
            setResultFailed(4452, "Iterator returned by getProfiles did not throw UnsupportedOperationException on remove()");
            return;
        } catch (UnsupportedOperationException e) {
            TCKSbbUtils.createTrace(getSbbID(),Level.INFO, "got expected UnsupportedOperationException", null);
        } catch (Exception e) {
            setResultFailed(4452, "Iterator returned by getProfiles threw unexpected exception on remove(): " + e);
            return;
        }

        try {
            getProfileFacilityTestProfile(id);
        } catch (UnrecognizedProfileNameException e) {
            setResultFailed(4452, "Iterator returned by getProfiles did not throw UnsupportedOperationException on " +
                                  "remove(), and removed the profile");
        }

        // Check for NullPointerException -- assertion 4453
        try {
            pf.getProfiles(null);
            setResultFailed(4453, "getProfiles with null profile table name did not throw null pointer exception");
        } catch (NullPointerException e) {
            TCKSbbUtils.createTrace(getSbbID(),Level.INFO, "got expected NullPointerException", null);
        } catch (Exception e) {
            setResultFailed(4453, "getProfiles with null profile table name threw unexpected exception: " + e);
        }

        // Check for UnrecognizedProfileTableNameException -- assertion 4454
        try {
            pf.getProfiles("ProfileThatDoesNotExist");
            setResultFailed(4454, "getProfiles with non existant table name did not throw UnrecognizedProfileTableNameException");
        } catch (UnrecognizedProfileTableNameException e) {
            TCKSbbUtils.createTrace(getSbbID(),Level.INFO, "got expected UnrecognizedProfileTableNameException", null);
        } catch (Exception e) {
            setResultFailed(4454, "getProfiles with non existant table name threw unexpected exception: " + e);
        }

        // FIXME: check for required transactional behaviour - TransactionRolledbackLocalException

        setResultPassed("ProfileFacility exception tests passed");

    }

    private void doGetProfilesIteratorTest() throws Exception {
        ProfileFacility pf = getProfileFacility();
        Iterator it = pf.getProfiles("ProfileFacilityTestTable2").iterator();
        int count = 0;
        while (it.hasNext()) {
            try {
                ProfileID id = (ProfileID) it.next();
                if (!id.getProfileTableName().equals("ProfileFacilityTestTable2")) {
                    setResultFailed(4448, "getProfiles() returned a profile from another profile table");
                }
                count ++;
            } catch (ClassCastException e) {
                setResultFailed(4451, "Iterator returned by getProfiles() contained elements of type other than ProfileID");
                return;
            }
        }

        if (count < 2) {
            setResultFailed(4448, "Iterator returned by getProfiles did not contain all profiles in the profile table" +
                                  ", expected: 2, got " + count);
            return;
        } else if (count > 2) {
            setResultFailed(4448, "Iterator returned by getProfiles returned too many profiles" +
                                  ", expected: 2, got " + count);
            return;
        }

        it = pf.getProfiles("ProfileFacilityTestTable3").iterator();
        if(it.hasNext()) {
            setResultFailed(4448, "Iterator returned by getProfiles() on a profile table with no profles was not empty");
            return;
        }

        setResultPassed("Profile facility getProfiles() Iterator tests passed");
    }

    private void doGetProfilesTest() throws Exception {

        ProfileFacility pf = getProfileFacility();

        // Test search using non-array attribute
        // This should find exactly one profile
        Iterator it = pf.getProfilesByIndexedAttribute("ProfileFacilityTestTable", "attr1", new Integer(1)).iterator();
        boolean found = false;
        while (it.hasNext()) {

            ProfileID profileID = (ProfileID)it.next();
            ProfileFacilityTestProfileCMP profile = getProfileFacilityTestProfile(profileID);
            // make sure attributes match
            if (1 != profile.getAttr1()) {
                setResultFailed(1383, "returned profile did not match search criteria (non-array attribute)");
                return;
            }

            TCKSbbUtils.createTrace(getSbbID(),Level.INFO,"getProfilesByIndexedAttribute found profile (non-array attribute)",null);
            found = true;
        }
        if (!found) {
            setResultFailed(1383, "getProfilesByIndexedAttribute did not find profile (non-array attribute)");
            return;
        }

        // Test search using array attribute
        // This should find exactly one profile - array attr2 contains [2, 3, 4] - should match with 2
        it = pf.getProfilesByIndexedAttribute("ProfileFacilityTestTable", "attr2", new Integer(2)).iterator();
        found = false;
        while (it.hasNext()) {

            ProfileID profileID = (ProfileID)it.next();
            ProfileFacilityTestProfileCMP profile = getProfileFacilityTestProfile(profileID);
            // make sure attributes match
            int[] attr2 = profile.getAttr2();
            for (int i = 0; i < attr2.length; i++) {
                if (2 == attr2[i]) {
                    found = true;
                    TCKSbbUtils.createTrace(getSbbID(),Level.INFO,"getProfilesByIndexedAttribute found profile (array attribute)",null);
                    break;
                }
            }
            if (!found) {
                setResultFailed(1386, "returned profile did not match search criteria (array attribute)");
                return;
            } else break;
        }
        if (!found) {
            setResultFailed(1386, "getProfilesByIndexedAttribute did not find profile (array attribute)");
            return;
        }

        setResultPassed("getProfilesByIndexedAttribute tests passed");

    }

    private void doGetProfileTest() throws Exception {

        ProfileFacility pf = getProfileFacility();

        // Test search using non-array attribute
        ProfileID profileID = pf.getProfileByIndexedAttribute("ProfileFacilityTestTable", "attr1", new Integer(1));
        if (profileID == null) {
            setResultFailed(1383, "getProfileByIndexedAttribute returned no profiles (non-array attribute)");
            return;
        }

        ProfileFacilityTestProfileCMP profile = getProfileFacilityTestProfile(profileID);
        // make sure attributes match
        if (1 != profile.getAttr1()) {
            setResultFailed(1383, "returned profile did not match search criteria (non-array attribute)");
            return;
        }
        TCKSbbUtils.createTrace(getSbbID(),Level.INFO,"getProfileByIndexedAttribute found profile (non-array attribute)",null);

        // Test search using array attribute
        // This should find exactly one profile - array attr2 contains [2, 3, 4] - should match with 2
        profileID = pf.getProfileByIndexedAttribute("ProfileFacilityTestTable", "attr1", new Integer(1));
        if (profileID == null) {
            setResultFailed(1386, "getProfileByIndexedAttribute returned no profiles (array attribute)");
            return;
        }

        profile = getProfileFacilityTestProfile(profileID);
        // make sure attributes match
        int[] attr2 = profile.getAttr2();
        boolean found = false;
        for (int i = 0; i < attr2.length; i++) {
            if (2 == attr2[i]) {
                found = true;
                TCKSbbUtils.createTrace(getSbbID(),Level.INFO,"getProfileByIndexedAttribute found profile (array attribute)",null);
                break;
            }
        }
        if (!found) {
            setResultFailed(1386, "returned profile did not match search criteria (array attribute)");
            return;
        }

        // Test that failed search returns null
        ProfileID nullProfileID = pf.getProfileByIndexedAttribute("ProfileFacilityTestTable", "attr1", new Integer(999));
        if (nullProfileID != null) {
            setResultFailed(1396, "getProfileByIndexedAttribute did not return null for failed search");
            return;
        }

        setResultPassed("getProfileByIndexedAttribute tests passed");
    }

    private ProfileFacility getProfileFacility() throws Exception {
        ProfileFacility pf = null;
        try {
            pf = (ProfileFacility) new InitialContext().lookup(JNDI_PROFILEFACILITY_NAME);
        } catch (Exception e) {
            TCKSbbUtils.createTrace(getSbbID(),Level.WARNING,"got unexpected Exception: " + e,null);
        }
        return pf;
    }


    // profile CMP
    public abstract ProfileFacilityTestProfileCMP getProfileFacilityTestProfile(ProfileID profileID)
        throws UnrecognizedProfileTableNameException, UnrecognizedProfileNameException;    
      
    public abstract void fireSendResultEvent(SendResultEvent event, ActivityContextInterface aci, Address address);


    // CMP fields

    public abstract boolean getResult();
    public abstract void setResult(boolean result);

    public abstract String getMessage();
    public abstract void setMessage(String message);

    public abstract String getTestName();
    public abstract void setTestName(String testName);

    public abstract int getFailedAssertionID();
    public abstract void setFailedAssertionID(int failedAssertionID);

    // Constants

    private static final String JNDI_PROFILEFACILITY_NAME = "java:comp/env/slee/facilities/profile";


}
