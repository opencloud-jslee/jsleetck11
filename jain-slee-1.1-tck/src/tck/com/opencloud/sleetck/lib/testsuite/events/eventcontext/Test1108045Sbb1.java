/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.events.eventcontext;

import javax.slee.ActivityContextInterface;
import javax.slee.InitialEventSelector;
import javax.slee.EventContext;
import javax.slee.facilities.Tracer;

import com.opencloud.sleetck.lib.sbbutils.BaseTCKSbb;
import com.opencloud.sleetck.lib.sbbutils.TCKSbbUtils;
import com.opencloud.sleetck.lib.resource.events.TCKResourceEventX;

import java.util.HashMap;

/**
 * AssertionID(1108045): Test The javax.slee.IllegalStateException is thrown 
 * if the Event Context is not in “suspended state”, e.g. event delivery had 
 * not been suspended or event delivery has already been resumed.
 */
public abstract class Test1108045Sbb1 extends BaseTCKSbb {
    public static final String TRACE_MESSAGE_TCKResourceEventX1 = "Test1108045Sbb1:I got TCKResourceEventX1 on ActivityA";

    public static final String TRACE_MESSAGE_TCKResourceEventX2 = "Test1108045Sbb1:I got TCKResourceEventX2 on ActivityB";

    public InitialEventSelector initialEventSelector(InitialEventSelector ies) {
        ies.setCustomName("test");
        return ies;
    }

    public void onTCKResourceEventX1(TCKResourceEventX event, ActivityContextInterface aci, EventContext context) {
        try {
            //send message to TCK: I got TCKResourceEventX1 on ActivityA
            tracer = getSbbContext().getTracer("com.test");
            tracer.info(TRACE_MESSAGE_TCKResourceEventX1);

            //store context in CMP field
            setEventContext(context);

            try {
                //call suspendDelivery() method now, we assumed the default timeout 
                //on the SLEE will be more than 10000ms to complete this test.
                context.suspendDelivery();
                
            } catch (Exception e) {
                tracer.severe("Exception on calling EventContext.suspendDelivery(): "+e);
            }

        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }
    }

    public void onTCKResourceEventX2(TCKResourceEventX event, ActivityContextInterface aci, EventContext context) {
        try {
            //send message to TCK: I got TCKResourceEventX2 on ActivityB
            tracer = getSbbContext().getTracer("com.test");
            tracer.info(TRACE_MESSAGE_TCKResourceEventX2);

            //try to test the case of the event delivery had not been suspended
            boolean passed = false;
            try {
                context.resumeDelivery();
            } catch (java.lang.IllegalStateException e) {
                tracer.info("got expected IllegalStateException if the event delivery had not been suspended.");
                passed = true;
            }

            if (!passed) {
                sendResultToTCK("Test1108045Test", false, "SBB1:onTCKResourceEventX2-ERROR: EventContext.resumeDelivery() "
                        + "should have thrown java.lang.IllegalStateException "
                        + "if the event delivery had not been suspended.", 1108045);
                return;
            }

            //get EventContext ec from CMP field
            EventContext ec = getEventContext();

            //assert ec.isSuspended(), check ec has been suspended or not 
            if (!ec.isSuspended()) {
                tracer.warning("ERROR: The event delivery has been suspended, the "
                        + "EventContext.isSuspended() returned true!");
                return;
            }
            
            try {
                //call resumeDelivery() method now 
                ec.resumeDelivery();
                
            } catch (Exception e) {
                tracer.severe("Exception on calling EventContext.resumeDelivery(): "+e);
            }


            //try to test the case of the event delivery has already been resumed
            passed = false;
            try {
                ec.resumeDelivery();
            } catch (java.lang.IllegalStateException e) {
                tracer.info("got expected IllegalStateException is thrown if the Event Context is not "
                        + "in suspended state, e.g.event delivery has already been resumed.");
                passed = true;
            }

            if (!passed) {
                sendResultToTCK("Test1108045Test", false, "SBB1:onTCKResourceEventX2-ERROR: EventContext.resumeDelivery() "
                        + "should have thrown java.lang.IllegalStateException "
                        + "if the event delivery has already been resumed.", 1108045);
                return;
            }

            sendResultToTCK("Test1108045Test", true, "Test The javax.slee.IllegalStateException is thrown if the Event Context "
                    + "is not in “suspended state”, e.g. event delivery had not been suspended or event "
                    + "delivery has already been resumed.", 1108045);

        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }
    }

    private void sendResultToTCK(String testName, boolean result, String message, int failedAssertionID) throws Exception {
        HashMap sbbData = new HashMap();
        sbbData.put("testname", testName);
        sbbData.put("result", result ? "pass" : "fail");
        sbbData.put("message", message);
        sbbData.put("id", new Integer(failedAssertionID));
        TCKSbbUtils.getResourceInterface().sendSbbMessage(sbbData);
    }

    public abstract void setEventContext(EventContext eventContext);
    public abstract EventContext getEventContext();

    private Tracer tracer = null;

}
