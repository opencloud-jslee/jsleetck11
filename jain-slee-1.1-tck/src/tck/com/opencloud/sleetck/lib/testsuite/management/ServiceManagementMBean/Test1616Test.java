/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.management.ServiceManagementMBean;

import javax.slee.management.*;
import javax.slee.ComponentID;
import javax.slee.ServiceID;

import com.opencloud.sleetck.lib.*;
import com.opencloud.sleetck.lib.testutils.*;
import com.opencloud.sleetck.lib.resource.TCKSbbMessage;
import com.opencloud.sleetck.lib.resource.testapi.TCKResourceListener;
import com.opencloud.sleetck.lib.resource.TCKActivityID;
import com.opencloud.sleetck.lib.resource.testapi.TCKResourceTestInterface;
import com.opencloud.sleetck.lib.resource.events.TCKResourceEventX;

import com.opencloud.sleetck.lib.testutils.jmx.DeploymentMBeanProxy;
import com.opencloud.sleetck.lib.testutils.jmx.ServiceManagementMBeanProxy;

import com.opencloud.logging.Logable;

import java.rmi.RemoteException;
import java.util.HashMap;

public class Test1616Test implements SleeTCKTest {

    private static final String SERVICE_DU_PATH_PARAM = "DUPath";
    private static final int TEST_ID = 1616;

    public void init(SleeTCKTestUtils utils) {
    this.utils = utils;
    }

    /**
     * Perform the actual test.
     */

    public TCKTestResult run() throws Exception {

    DeploymentMBeanProxy duProxy = utils.getDeploymentMBeanProxy();
    ServiceManagementMBeanProxy serviceProxy = utils.getMBeanProxyFactory().createServiceManagementMBeanProxy(utils.getSleeManagementMBeanProxy().getServiceManagementMBean());
    DeployableUnitDescriptor duDesc = duProxy.getDescriptor(duID);
    ComponentID components[] = duDesc.getComponents();

    for (int i = 0; i < components.length; i++) {
        if (components[i] instanceof ServiceID) {
        ServiceID service = (ServiceID) components[i];

        // Verify that the Service is in the INACTIVE state.
        if (!serviceProxy.getState(service).isInactive()) {
            continue; // Look for the next available service.
        }

        try {
            serviceProxy.activate(service);
        } catch (javax.slee.InvalidStateException e) {
            return TCKTestResult.failed(TEST_ID, "Failed to activate a service in the inactive state.");
        } catch (Exception e) {
            utils.getLog().fine("Exception thrown: " + e);
            return TCKTestResult.error(e);
        }

        // Check the status of the newly activated service.
        if (!serviceProxy.getState(service).isActive()) {
            return TCKTestResult.failed(1617, "activate(ServiceID) returned ok, but service was not in the active state afterwards.");
        }

        // Try to Activate the Service again and ensure that InvalidStateException is thrown.
        try {
            serviceProxy.activate(service);
        } catch (javax.slee.InvalidStateException e) {
            return TCKTestResult.passed();
        } catch (Exception e) {
            utils.getLog().fine("Exception thrown: " + e);
            return TCKTestResult.error(e);
        }

        return TCKTestResult.failed(1618, "InvalidStateException should have been thrown when activating an already active service.");
        }
    }

    return TCKTestResult.error("Failed to find any services to activate.");
    }

    /**
     * Do all the pre-run configuration of the test.
     */

    public void setUp() throws Exception {

    utils.getLog().fine("Connecting to resource");
    resourceListener = new TCKResourceListenerImpl();
    utils.getResourceInterface().setResourceListener(resourceListener);
    utils.getLog().fine("Installing and activating service");

    // Install the Deployable Units
    String duPath = utils.getTestParams().getProperty(SERVICE_DU_PATH_PARAM);
    duID = utils.install(duPath);
    }

    /**
     * Clean up after the test.
     */

    public void tearDown() throws Exception {
    utils.getLog().fine("Disconnecting from resource");
        utils.getResourceInterface().clearActivities();
        utils.getResourceInterface().removeResourceListener();
        utils.getLog().fine("Deactivating and uninstalling service");
        utils.deactivateAllServices();
        utils.uninstallAll();
    }

    private class TCKResourceListenerImpl extends BaseTCKResourceListener {
    public synchronized void onSbbMessage(TCKSbbMessage message, TCKActivityID calledActivity) throws RemoteException {
        utils.getLog().info("Received message from SBB");

        HashMap map = (HashMap) message.getMessage();
        Boolean passed = (Boolean) map.get("Result");
        String msgString = (String) map.get("Message");

        if (passed.booleanValue() == true)
        result.setPassed();
        else
        result.setFailed(TEST_ID, msgString);
    }

    public void onException(Exception e) throws RemoteException {
        utils.getLog().warning("Received exception from SBB");
        utils.getLog().warning(e);
        result.setError(e);
    }
    }

    private SleeTCKTestUtils utils;
    private TCKResourceListener resourceListener;
    private FutureResult result;
    private DeployableUnitID duID;
}
