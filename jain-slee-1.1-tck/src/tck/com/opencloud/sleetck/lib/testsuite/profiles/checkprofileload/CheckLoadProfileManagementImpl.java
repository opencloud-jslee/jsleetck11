/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.profiles.checkprofileload;

import javax.slee.profile.ProfileManagement;
import javax.slee.profile.ProfileVerificationException;

/**
 *
 */
public abstract class CheckLoadProfileManagementImpl
        implements ProfileManagement, CheckLoadProfileCMP, CheckLoadProfileManagement {

    // ProfileManagement methods

    public void profileInitialize() {
        setValue(CheckLoadProfileCMP.INITIAL_VALUE);
    }

    public void profileLoad() {
        wasProfileLoadCalled = true;
        try {
            String value = getValue();
            if(value != null) {
                wasCMPReadSuccessful = true;
            } else {
                failureReason = "CMP getter did not throw an Exception, but returned null";
            }
        } catch (Exception e) {
            failureReason = "CMP getter threw the following Exception: "+e;
        }
    }

    public void profileStore() {
    }

    public void profileVerify() throws ProfileVerificationException {
    }

    // CheckLoadProfileManagement methods

    public boolean wasProfileLoadCalled() {
        return wasProfileLoadCalled;
    }

    public boolean wasCMPReadSuccessful() {
        return wasCMPReadSuccessful;
    }

    public String getFailureReason() {
        return failureReason;
    }

    private boolean wasProfileLoadCalled;
    private boolean wasCMPReadSuccessful;
    private String failureReason;
}
