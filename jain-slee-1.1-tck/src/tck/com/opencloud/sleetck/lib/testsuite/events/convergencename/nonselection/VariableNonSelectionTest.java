/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.events.convergencename.nonselection;

import com.opencloud.sleetck.lib.TCKTestResult;
import com.opencloud.sleetck.lib.resource.TCKActivityID;
import com.opencloud.sleetck.lib.resource.events.TCKResourceEventX;
import com.opencloud.sleetck.lib.testsuite.events.convergencename.AbstractConvergenceNameTest;
import com.opencloud.sleetck.lib.testsuite.events.convergencename.InitialEventSelectorParameters;
import com.opencloud.sleetck.lib.testutils.ProfileUtils;
import com.opencloud.sleetck.lib.testutils.jmx.AddressProfileProxy;
import com.opencloud.sleetck.lib.testutils.jmx.impl.AddressProfileProxyImpl;

import javax.management.ObjectName;
import javax.slee.Address;
import javax.slee.AddressPlan;

/**
 * A generic class for testing variation of non selected convergence name variables.  The selected variable is defined
 * by the test description XML file, and the test will vary non selected variables checking that the convergence name
 * remains the same.
 */
public class VariableNonSelectionTest extends AbstractConvergenceNameTest {
    // -- Constants -- //

    // Profile names                                     -- initially
    private static final String PROFILE_1 = "PROFILE_1"; // a1 a2
    private static final String PROFILE_2 = "PROFILE_2"; //       a3 a4

    private static final String ADDRESS_PROFILE_TABLE = "tck.AddressAndProfileTestProfile";

    private static final Address ADDRESS_1 = new Address(AddressPlan.IP, "1.0.0.1");
    private static final Address ADDRESS_2 = new Address(AddressPlan.IP, "1.0.0.2");
    private static final Address ADDRESS_3 = new Address(AddressPlan.IP, "1.0.0.3");
    private static final Address ADDRESS_4 = new Address(AddressPlan.IP, "1.0.0.4");

    // Two address sets (used for varying the address profile)
    private static final Address[] ADDRESS_SET_A = {ADDRESS_1, ADDRESS_2};
    private static final Address[] ADDRESS_SET_B = {ADDRESS_3, ADDRESS_4};

    private ProfileUtils profileUtils;

    public TCKTestResult run() throws Exception {
        String iesVariable = utils().getTestParams().getProperty("fixedIESVariable");
        performVariableNonSelectionTest(iesVariable);

        return TCKTestResult.passed();
    }

    private void performVariableNonSelectionTest(String constant) throws Exception {
        getLog().info("Performing constant non selection test for selected variable: " + constant);
        TCKActivityID mainActivityID = utils().getResourceInterface().createActivity(ACTIVITY_ID_PREFIX + (currentActivityIDSuffix++));
        getLog().info("Created activity: " + mainActivityID.getName());
        InitialEventSelectorParameters iesParams;
        if (!constant.equals(CUSTOM_NAME)) {
            iesParams = createIESParamsForVariable(constant, null);
        } else {
            iesParams = createIESParamsForVariable(constant, "custom_name_A");
        }

        String initialEvent = String.valueOf(currentEventID++);
        sendEventAndWait(TCKResourceEventX.X1, initialEvent, mainActivityID, ADDRESS_1, initialEvent, iesParams);

        for (int i = 0; i < selected().size(); i++) {
            String variable = (String) selected().get(i);
            // We don't vary the constant we're testing or custom name
            if (variable.equals(constant) || variable.equals(CUSTOM_NAME))
                continue;

            // set defaults
            TCKActivityID activityID = mainActivityID;
            Address address = ADDRESS_1;
            if (profileSwap) {
                // empty PROFILE_2 first so we don't get uniqueness errors
                replaceAddressProfile(PROFILE_2, new Address[0]);
                replaceAddressProfile(PROFILE_1, ADDRESS_SET_A);
                replaceAddressProfile(PROFILE_2, ADDRESS_SET_B);
                profileSwap = false;
            }
            String eventType = TCKResourceEventX.X1;

            getLog().info("Varying variable " + variable);

            // vary the current variable in the loop
            if (variable.equals(ADDRESS)) {
                address = ADDRESS_2;
            } else if (variable.equals(ADDRESS_PROFILE)) {
                // empty PROFILE_2 first so we don't get uniqueness errors
                replaceAddressProfile(PROFILE_2, new Address[0]);
                replaceAddressProfile(PROFILE_1, ADDRESS_SET_B);
                replaceAddressProfile(PROFILE_2, ADDRESS_SET_A);
                profileSwap = true;
            } else if (variable.equals(ACTIVITY_CONTEXT)) {
                activityID = utils().getResourceInterface().createActivity(ACTIVITY_ID_PREFIX + (currentActivityIDSuffix++));;
            } else if (variable.equals(EVENT_TYPE)) {
                eventType = TCKResourceEventX.X2;
            } else if (variable.equals(EVENT_OBJECT)) {
                // nothing required here -- the TCK always sends a different event object by default
            }

            sendEventAndWait(eventType, String.valueOf(currentEventID++), activityID, address, initialEvent, iesParams);
        }
    }

    private void replaceAddressProfile(String profileName, Address[] addresses) throws Exception {
        ObjectName objectName = profileUtils.getProfileProvisioningProxy().getProfile(ADDRESS_PROFILE_TABLE, profileName);
        setAddressProfile(objectName, addresses);
    }

    private void setAddressProfile(ObjectName objectName, Address[] addresses) throws Exception {
        AddressProfileProxy addressProfileProxy = new AddressProfileProxyImpl(objectName, utils().getMBeanFacade());
        addressProfileProxy.editProfile();
        addressProfileProxy.setAddresses(addresses);
        addressProfileProxy.commitProfile();
    }

    public void setUp() throws Exception {
        // setup the address profile table before installing the service
        profileUtils = new ProfileUtils(utils());
        profileUtils.createStandardAddressProfileTable(ADDRESS_PROFILE_TABLE);
        setupAddressProfile(ADDRESS_PROFILE_TABLE, PROFILE_1, ADDRESS_SET_A);
        setupAddressProfile(ADDRESS_PROFILE_TABLE, PROFILE_2, ADDRESS_SET_B);
        super.setUp();
    }

    private void setupAddressProfile(String tableName, String profileName, Address[] addresses) throws Exception {
        ObjectName objectName = profileUtils.getProfileProvisioningProxy().createProfile(tableName, profileName);
        setAddressProfile(objectName, addresses);
    }

    public void tearDown() throws Exception {
        super.tearDown();
        if (profileUtils != null) {
            profileUtils.getProfileProvisioningProxy().removeProfile(ADDRESS_PROFILE_TABLE, PROFILE_1);
            profileUtils.getProfileProvisioningProxy().removeProfile(ADDRESS_PROFILE_TABLE, PROFILE_2);
            profileUtils.removeProfileTable(ADDRESS_PROFILE_TABLE);
        }
    }

    /**
     * Creates and returns an InitialEventSelectorParameters object
     * based on the selected convergence name variables, and the given
     * custom name.
     */
    private InitialEventSelectorParameters createIESParamsForVariable(String variable, String customName) {
        return new InitialEventSelectorParameters(
            variable.equals(ACTIVITY_CONTEXT),
            variable.equals(ADDRESS),
            variable.equals(ADDRESS_PROFILE),
            false, // event object
            variable.equals(EVENT_TYPE),
            customName,
            false, false, // IES initial event flag
            false, null); // don't change the address in the IES
    }

    private int currentActivityIDSuffix = 0;
    private int currentEventID = 0;
    private boolean profileSwap = false;
}
