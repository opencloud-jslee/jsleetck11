/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.activities.activitycontext;

import com.opencloud.sleetck.lib.sbbutils.BaseTCKSbb;
import com.opencloud.sleetck.lib.sbbutils.TCKSbbUtils;
import com.opencloud.sleetck.lib.sbbutils.TCKSbbConstants;
import com.opencloud.sleetck.lib.resource.events.TCKResourceEventX;
import com.opencloud.sleetck.lib.resource.sbbapi.TCKResourceSbbInterface;
import com.opencloud.sleetck.lib.resource.sbbapi.TCKActivityContextInterfaceFactory;
import com.opencloud.sleetck.lib.resource.sbbapi.TCKActivity;
import com.opencloud.sleetck.lib.resource.TCKActivityID;

import javax.slee.ActivityContextInterface;
import javax.slee.SbbContext;
import javax.slee.Address;
import javax.slee.facilities.Level;
import javax.naming.InitialContext;
import java.util.HashMap;

/**
 * Throws null pointer exception if sbb passed to ACI.attach() is null.
 */

public abstract class ExplicitAttachSbb extends BaseTCKSbb {

    // Declare the fireTCKResourceEventX2 method.  SLEE completes this.
    public abstract void fireExplicitAttachEvent(ExplicitAttachEvent event, ActivityContextInterface aci, Address address);

    public void onTCKResourceEventX1(TCKResourceEventX event, ActivityContextInterface aci) {

        try {

            HashMap map = new HashMap();

            String activityName = "ExplicitAttachSecondActivity";

            TCKResourceSbbInterface resource = TCKSbbUtils.getResourceInterface();
            TCKActivityID activityID = resource.createActivity(activityName);

            // Lookup the newly created Activity's ACI.
            TCKActivityContextInterfaceFactory factory = (TCKActivityContextInterfaceFactory) TCKSbbUtils.getSbbEnvironment().lookup(TCKSbbConstants.TCK_ACI_FACTORY_LOCATION);

            if (factory == null) {
                map.put("Result", new Boolean(false));
                map.put("Message", "Unable to lookup TCKActivityContextInterfaceFactory.");
                resource.sendSbbMessage(map);
                return;
            }

            ActivityContextInterface myAci = factory.getActivityContextInterface(resource.getActivity(activityID));

            // Attach to the ACI.
            myAci.attach(getSbbContext().getSbbLocalObject());

            // Fire an event on the new ActivityContextInterface.

            ExplicitAttachEvent fireEvent = new ExplicitAttachEvent();

            fireExplicitAttachEvent(fireEvent, myAci, null);

        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }

    }

    /**
     * If we receive this event we have been successfully attached to the custom activity.
     */

    public void onExplicitAttachEvent(ExplicitAttachEvent event, ActivityContextInterface aci) {

        try {
            HashMap map = new HashMap();
            TCKResourceSbbInterface resource = TCKSbbUtils.getResourceInterface();

            // Detach the SBB from the ACI so the SBB can become inactive.
            aci.detach(getSbbContext().getSbbLocalObject());

            // End the activity.
            resource.endActivity(((TCKActivity) aci.getActivity()).getID());
            //            resource.endActivity(activityID);

            // Send the success result to the test.
            map.put("Result", new Boolean(true));
            map.put("Message", "Ok");
            resource.sendSbbMessage(map);

        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }

    }

}
