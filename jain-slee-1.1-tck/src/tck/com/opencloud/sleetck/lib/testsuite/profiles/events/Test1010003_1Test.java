/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.profiles.events;

import javax.management.ObjectName;
import javax.slee.profile.ProfileAddedEvent;
import javax.slee.profile.ProfileSpecificationID;

import com.opencloud.sleetck.lib.AbstractSleeTCKTest;
import com.opencloud.sleetck.lib.DescriptionKeys;
import com.opencloud.sleetck.lib.OperationTimedOutException;
import com.opencloud.sleetck.lib.TCKTestErrorException;
import com.opencloud.sleetck.lib.TCKTestFailureException;
import com.opencloud.sleetck.lib.TCKTestResult;
import com.opencloud.sleetck.lib.infra.SleeTCKComponentConstants;
import com.opencloud.sleetck.lib.resource.TCKSbbMessage;
import com.opencloud.sleetck.lib.testsuite.profiles.ProfileTestConstants;
import com.opencloud.sleetck.lib.testutils.ComponentIDLookup;
import com.opencloud.sleetck.lib.testutils.ProfileUtils;
import com.opencloud.sleetck.lib.testutils.QueuingResourceListener;
import com.opencloud.sleetck.lib.testutils.jmx.ProfileMBeanProxy;
import com.opencloud.sleetck.lib.testutils.jmx.ProfileProvisioningMBeanProxy;

public class Test1010003_1Test extends AbstractSleeTCKTest{

    private static final String TCK_SBB_EVENT_DU_PATH_PARAM = "eventDUPath";

    private static final String PROFILE_TABLE_NAME = "Test1010003_1ProfileTable";
    private static final String PROFILE_NAME = "Test1010003_1Profile";

    /**
     * This test is similar to 'ProfileEventsTest' but operates the profile
     * events as 'initial-events',
     * i.e. new root sbbs are established on add, update, remove operation. An sbb entity is
     * expected to send an ACK message in the form of the full event class back to the test.
     */
    public TCKTestResult run() throws Exception {

        // create the profile tables
        getLog().fine("Creating profile tables");
        ProfileProvisioningMBeanProxy profileProvisioning = profileUtils.getProfileProvisioningProxy();

        ProfileSpecificationID profileSpecID = new ComponentIDLookup(utils()).lookupProfileSpecificationID(
            ProfileTestConstants.SIMPLE_PROFILE_11_SPEC_NAME,SleeTCKComponentConstants.TCK_VENDOR,"1.1");
        profileProvisioning.createProfileTable(profileSpecID,PROFILE_TABLE_NAME);

        // -- Add profile to the profile table and expect the SBB to receive ProfileAddedEvent -- //
        getLog().fine("Adding profile "+PROFILE_NAME+" to the profile table");
        ObjectName profile = profileProvisioning.createProfile(PROFILE_TABLE_NAME,PROFILE_NAME);
        ProfileMBeanProxy profProxy = utils().getMBeanProxyFactory().createProfileMBeanProxy(profile);
        profProxy.commitProfile();
        // wait for a ProfileAddedEvent to be received by the SBB
        try {
            TCKSbbMessage reply = resourceListener.nextMessage();
            if(!ProfileAddedEvent.class.getName().equals(reply.getMessage()))
                throw new TCKTestErrorException("Received unexpected reply from SBB after adding a profile. Message="+reply.getMessage());
        } catch (OperationTimedOutException ex) {
            throw new TCKTestFailureException(1010003,"Timed out waiting for acknowledgement of a ProfileAddedEvent "+
                "following the addition of a profile.",ex);
        }

        return TCKTestResult.passed();
    }

    public void setUp() throws Exception {
        // install profile spec and service
        setupService(ProfileTestConstants.PROFILE_SPEC_DU_PATH_PARAM);
        setupService(TCK_SBB_EVENT_DU_PATH_PARAM);
        setupService(DescriptionKeys.SERVICE_DU_PATH_PARAM);

        // set up the resource listener
        resourceListener = new QueuingResourceListener(utils());
        setResourceListener(resourceListener);
        profileUtils = new ProfileUtils(utils());
    }

    public void tearDown() throws Exception {
        // remove profile tables
        try {
            profileUtils.removeProfileTable(PROFILE_TABLE_NAME);
        } catch(Exception e) {
            getLog().warning("Caught exception while trying to remove profile tables:");
            getLog().warning(e);
        }
        super.tearDown();
    }

    private QueuingResourceListener resourceListener;
    private ProfileUtils profileUtils;

}
