/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.transactions.isolation;

import com.opencloud.sleetck.lib.resource.events.TCKResourceEventY;
import com.opencloud.sleetck.lib.sbbutils.TCKSbbUtils;

import javax.slee.ActivityContextInterface;
import javax.slee.facilities.Level;

public abstract class Test2003Sbb2 extends Test2003BaseSbb {

    /**
     * Sets the ACI value (to TCKResourceEventY1's event type name)
     */
    public void onTCKResourceEventY1(TCKResourceEventY event, ActivityContextInterface aci) {
        try {
            String newValue = TCKResourceEventY.Y1;
            createTraceSafe(Level.INFO,"Test2003Sbb2.onTCKResourceEventY1(): Transaction ID: "+getTransactionId());
            createTraceSafe(Level.INFO,"Test2003Sbb2.onTCKResourceEventY1(): changing ACI value to: "+newValue);
            Test2003ActivityContextInterface myaci = null;
            Exception exceptionCaught = null;
            try {
                myaci = getACI();
                myaci.setValue(newValue);
                createTraceSafe(Level.INFO,"Test2003Sbb2.onTCKResourceEventY1(): changed value to: "+newValue);
            } catch (Exception aciEx) {
                exceptionCaught = aciEx;
                createTraceSafe(Level.INFO,"Test2003Sbb2.onTCKResourceEventY1(): Exception received while trying to "+
                        "access ACI value: "+aciEx);
            }
            sendResponse(TCKResourceEventY.Y1, exceptionCaught);
        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }
    }

    /**
     * Attempts to access the ACI value, then sends either the retrieved value, or the exception caught
     * while trying, to the test.
     */
    public void onTCKResourceEventY2(TCKResourceEventY event, ActivityContextInterface aci) {
        try {
            createTraceSafe(Level.INFO,"Test2003Sbb2.onTCKResourceEventY2(): Transaction ID: "+getTransactionId());
            createTraceSafe(Level.INFO,"Test2003Sbb2.onTCKResourceEventY2(): accessing ACI value");
            Object response = null;
            try {
                response = getACI().getValue();
                createTraceSafe(Level.INFO,"Test2003Sbb1.onTCKResourceEventY2(): ACI value: "+response);
            } catch (Exception aciEx) {
                response = aciEx;
            }
            sendResponse(TCKResourceEventY.Y2, response);
        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }
    }

}
