/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.events.convergencename.iesSetsAddress;

import com.opencloud.sleetck.lib.TCKTestResult;
import com.opencloud.sleetck.lib.resource.TCKActivityID;
import com.opencloud.sleetck.lib.resource.events.TCKResourceEventX;
import com.opencloud.sleetck.lib.testsuite.events.convergencename.AbstractConvergenceNameTest;
import com.opencloud.sleetck.lib.testsuite.events.convergencename.InitialEventSelectorParameters;
import com.opencloud.sleetck.lib.testutils.ProfileUtils;
import com.opencloud.sleetck.lib.testutils.jmx.AddressProfileProxy;
import com.opencloud.sleetck.lib.testutils.jmx.impl.AddressProfileProxyImpl;

import javax.management.ObjectName;
import javax.slee.Address;
import javax.slee.AddressPlan;

/**
 * Test assertion 1925: That the SBB's initial event selector method may change the default address and that this address
 * is used in selection of an AddressProfile
 */
public class IesSetsAddressTest extends AbstractConvergenceNameTest {
    // Profile names                                     -- initially
    private static final String PROFILE_1 = "PROFILE_1"; // a1    a3
    private static final String PROFILE_2 = "PROFILE_2"; //    a2

    private static final String ADDRESS_PROFILE_TABLE = "tck.AddressAndProfileTestProfile";

    private static final Address ADDRESS_1 = new Address(AddressPlan.IP, "1.0.0.1");
    private static final Address ADDRESS_2 = new Address(AddressPlan.IP, "1.0.0.2");
    private static final Address ADDRESS_3 = new Address(AddressPlan.IP, "1.0.0.3");

    private ProfileUtils profileUtils;


    public TCKTestResult run() throws Exception {
        TCKActivityID activityID = utils().getResourceInterface().createActivity("Test1924Activity");
        InitialEventSelectorParameters iesParams;

        // Test preparation: create an SBB ("1") with convergence variable activitycontext:PROFILE_1
        iesParams = new InitialEventSelectorParameters(true, false, true, false, false, null, false, false, false, null);
        sendEventAndWait(TCKResourceEventX.X1, "1", activityID, ADDRESS_1, "1", iesParams);

        // Send event with ADDRESS_2: the IES method will change this to ADDRESS_3, which belongs to PROFILE_1
        //  -> the SLEE should deliver this event to SBB "1" and not create a new entity
        iesParams = new InitialEventSelectorParameters(true, false, true, false, false, null, false, false, true, ADDRESS_3);
        sendEventAndWait(TCKResourceEventX.X1, "2", activityID, ADDRESS_2, "1", iesParams);

        return TCKTestResult.passed();
    }

    public void setUp() throws Exception {
        // setup the address profile table before installing the service
        profileUtils = new ProfileUtils(utils());
        profileUtils.createStandardAddressProfileTable(ADDRESS_PROFILE_TABLE);
        setupAddressProfile(ADDRESS_PROFILE_TABLE, PROFILE_1, new Address[]{ADDRESS_1, ADDRESS_3});
        setupAddressProfile(ADDRESS_PROFILE_TABLE, PROFILE_2, new Address[]{ADDRESS_2});
        super.setUp();
    }

    public void tearDown() throws Exception {
        super.tearDown();
        if (profileUtils != null)
            profileUtils.removeProfileTable(ADDRESS_PROFILE_TABLE);
    }

    private void setupAddressProfile(String tableName, String profileName, Address[] addresses) throws Exception {
        ObjectName objectName = profileUtils.getProfileProvisioningProxy().createProfile(tableName, profileName);
        AddressProfileProxy addressProfileProxy = new AddressProfileProxyImpl(objectName, utils().getMBeanFacade());
        addressProfileProxy.editProfile();
        addressProfileProxy.setAddresses(addresses);
        addressProfileProxy.commitProfile();
    }
}
