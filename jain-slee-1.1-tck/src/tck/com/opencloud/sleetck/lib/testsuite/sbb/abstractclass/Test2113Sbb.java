/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.sbb.abstractclass;

import javax.slee.ActivityEndEvent;
import javax.slee.ActivityContextInterface;
import javax.slee.SbbContext;
import javax.slee.Address;
import javax.slee.SbbLocalObject;
import javax.slee.facilities.Level;
import javax.slee.ChildRelation;
import javax.slee.CreateException;

import com.opencloud.sleetck.lib.sbbutils.BaseTCKSbb;
import com.opencloud.sleetck.lib.sbbutils.TCKSbbUtils;
import com.opencloud.sleetck.lib.sbbutils.TCKSbbConstants;
import com.opencloud.sleetck.lib.resource.events.TCKResourceEventX;
import com.opencloud.sleetck.lib.resource.sbbapi.TCKResourceSbbInterface;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Collection;
import java.util.Vector;

public abstract class Test2113Sbb extends BaseTCKSbb {

    // Initial event
    public void onTCKResourceEventX1(TCKResourceEventX ev, ActivityContextInterface aci) {

        try {

            HashMap map = new HashMap();

            SbbLocalObject children[] = new SbbLocalObject[2];
            for (int i = 0; i < 2; i++)
                children[i] = getChildRelation().create();

            Iterator iter = getChildRelation().iterator();

            // 2113 - next

            SbbLocalObject child = null;
            try {
                child = (SbbLocalObject) iter.next();
            } catch (ClassCastException e) {
                map.put("Result", new Boolean(false));
                map.put("Message", "Object returned by ChildRelation.iterator().next() was not an SbbLocalObject.");
                TCKSbbUtils.getResourceInterface().sendSbbMessage(map);
                return;
            }

            // 2114 - remove - removes the last object returned by next
            iter.remove();

            if (getChildRelation().size() != 1) {
                map.put("Result", new Boolean(false));
                map.put("Message", "Incorrect number of children removed via iterator().remove().");
                map.put("ID", new Integer(2114));
                TCKSbbUtils.getResourceInterface().sendSbbMessage(map);
                return;
            }


            if (getChildRelation().iterator().next().equals(child)) {
                map.put("Result", new Boolean(false));
                map.put("Message", "The wrong child was removed from the child relation via ChildRelation.iterator().remove().");
                map.put("ID", new Integer(2114));
                TCKSbbUtils.getResourceInterface().sendSbbMessage(map);
                return;
            }


            map.put("Result", new Boolean(true));
            map.put("Message", "Ok");
            TCKSbbUtils.getResourceInterface().sendSbbMessage(map);
            return;

            





        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }
    }

    // ChildRelation method for the Child SBB.
    public abstract ChildRelation getChildRelation();

}
