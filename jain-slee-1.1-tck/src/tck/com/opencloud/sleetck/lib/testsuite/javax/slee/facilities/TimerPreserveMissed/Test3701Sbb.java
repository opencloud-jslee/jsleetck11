/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.javax.slee.facilities.TimerPreserveMissed;

import com.opencloud.sleetck.lib.resource.events.TCKResourceEventX;
import com.opencloud.sleetck.lib.sbbutils.BaseTCKSbb;
import com.opencloud.sleetck.lib.sbbutils.TCKSbbUtils;

import javax.slee.ActivityContextInterface;
import javax.slee.facilities.TimerPreserveMissed;
import java.util.HashMap;

public abstract class Test3701Sbb extends BaseTCKSbb {

    // Initial event
    public void onTCKResourceEventX1(TCKResourceEventX ev, ActivityContextInterface aci) {

        try {
            HashMap map = new HashMap();

            if (!TimerPreserveMissed.fromInt(TimerPreserveMissed.PRESERVE_ALL).equals(TimerPreserveMissed.ALL)) {
                map.put("Result", new Boolean(false));
                map.put("Message", "TimerPreserveMissed.fromInt(TimerPreserveMissed.PRESERVE_ALL) returned incorrect object.");
                map.put("ID", new Integer(3701));
                TCKSbbUtils.getResourceInterface().sendSbbMessage(map);
                return;
            }

            if (TimerPreserveMissed.ALL.toInt() != TimerPreserveMissed.PRESERVE_ALL) {
                map.put("Result", new Boolean(false));
                map.put("Message", "TimerPreserveMissed.toInt() returned incorrect value.");
                map.put("ID", new Integer(3704));
                TCKSbbUtils.getResourceInterface().sendSbbMessage(map);
                return;
            }

            if (!TimerPreserveMissed.NONE.isNone() || TimerPreserveMissed.ALL.isNone()) {
                map.put("Result", new Boolean(false));
                map.put("Message", "TimerPreserveMissed.isNone() not functioning correctly.");
                map.put("ID", new Integer(3706));
                TCKSbbUtils.getResourceInterface().sendSbbMessage(map);
                return;
            }

            if (!TimerPreserveMissed.ALL.isAll() || TimerPreserveMissed.NONE.isAll()) {
                map.put("Result", new Boolean(false));
                map.put("Message", "TimerPreserveMissed.isAll() not functioning correctly.");
                map.put("ID", new Integer(3708));
                TCKSbbUtils.getResourceInterface().sendSbbMessage(map);
                return;
            }

            if (!TimerPreserveMissed.LAST.isLast() || TimerPreserveMissed.ALL.isLast()) {
                map.put("Result", new Boolean(false));
                map.put("Message", "TimerPreserveMissed.isLast() not functioning correctly.");
                map.put("ID", new Integer(3710));
                TCKSbbUtils.getResourceInterface().sendSbbMessage(map);
                return;
            }

            if (!TimerPreserveMissed.LAST.equals(TimerPreserveMissed.LAST) || TimerPreserveMissed.LAST.equals(TimerPreserveMissed.ALL)) {
                map.put("Result", new Boolean(false));
                map.put("Message", "TimerPreserveMissed.equals() not functioning correctly.");
                map.put("ID", new Integer(3712));
                TCKSbbUtils.getResourceInterface().sendSbbMessage(map);
                return;
            }

            try {
                TimerPreserveMissed.LAST.hashCode();
            } catch (Exception e) {
                map.put("Result", new Boolean(false));
                map.put("Message", "TimerPreserveMissed.hashCode() threw an exception.");
                map.put("ID", new Integer(3714));
                TCKSbbUtils.getResourceInterface().sendSbbMessage(map);
                return;
            }

            try {
                if (TimerPreserveMissed.LAST.toString() == null) {
                    map.put("Result", new Boolean(false));
                    map.put("Message", "TimerPreserveMissed.toString() returned null.");
                    map.put("ID", new Integer(3716));
                    
                    try {
                        TCKSbbUtils.getResourceInterface().sendSbbMessage(map);
                    } catch (Exception e) {
                        TCKSbbUtils.handleException(e);
                    }
                    return;
                }
            } catch (Exception e) {
                map.put("Result", new Boolean(false));
                map.put("Message", "TimerPreserveMissed.toString() threw an exception.");
                map.put("ID", new Integer(3716));
                TCKSbbUtils.getResourceInterface().sendSbbMessage(map);
                return;
            }
            
            map.put("Result", new Boolean(true));
            map.put("Message", "Ok");
            TCKSbbUtils.getResourceInterface().sendSbbMessage(map);
            return;

        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }

    }

}

