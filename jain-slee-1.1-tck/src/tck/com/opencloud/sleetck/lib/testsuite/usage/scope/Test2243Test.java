/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.usage.scope;

import com.opencloud.sleetck.lib.AbstractSleeTCKTest;
import com.opencloud.sleetck.lib.TCKTestResult;
import com.opencloud.sleetck.lib.resource.TCKActivityID;
import com.opencloud.sleetck.lib.resource.events.TCKResourceEventX;
import com.opencloud.sleetck.lib.testsuite.usage.common.UsageMBeanLookup;
import com.opencloud.sleetck.lib.testutils.QueuingNotificationListener;
import com.opencloud.sleetck.lib.testutils.QueuingResourceListener;

/**
 * Makes updates to parameters of the same name and same parameter set name,
 * in different SBBs, then checks that the updates were independent.
 */
public class Test2243Test extends AbstractSleeTCKTest {

    private static final int TEST_ID = 2243;
    private static final String PARAMETER_SET_NAME = "Test2243Test-ParameterSet";

    public TCKTestResult run() throws Exception {

        TCKActivityID activityID = utils().getResourceInterface().createActivity("token activity");

        getLog().info("firing event to Sbb");
        utils().getResourceInterface().fireEvent(TCKResourceEventX.X1,null,activityID,null);

        getLog().info("waiting for reply");
        resourceListener.nextMessage();
        getLog().info("received reply");

        getLog().info("waiting for 2 usage notifications for each Sbb");
        for (int i = 0; i < 2; i++) {
              notificationListenerParent.nextNotification();
        }
        for (int i = 0; i < 2; i++) {
              notificationListenerChild.nextNotification();
        }
        getLog().info("received all 4 usage notifications");

        getLog().info("checking that parameter updates to both Sbb were independent");

        long fooForParent = mBeanLookupParent.getNamedSbbUsageMBeanProxy(PARAMETER_SET_NAME).getCounterParameter("foo",false);
        long fooForChild  = mBeanLookupChild.getNamedSbbUsageMBeanProxy( PARAMETER_SET_NAME).getCounterParameter("foo",false);
        double barMeanForParent =
            mBeanLookupParent.getNamedSbbUsageMBeanProxy(PARAMETER_SET_NAME).getSampleParameter("bar",false).getMean();
        double barMeanForChild =
            mBeanLookupChild.getNamedSbbUsageMBeanProxy( PARAMETER_SET_NAME).getSampleParameter("bar",false).getMean();

        String expectedValues = "fooForParent="+1+",fooForChild="+3+
            "barMeanForParent="+2+"barMeanForChild="+4;
        String actualValues = "fooForParent="+fooForParent+",fooForChild="+fooForChild+
            "barMeanForParent="+barMeanForParent+"barMeanForChild="+barMeanForChild;

        if(fooForParent == 4 || fooForChild == 4)
            return TCKTestResult.failed(TEST_ID,"Updates to foo usage parameter in different "+
                "SBBs were combined -- the parameter sets should be independent.");
        if(barMeanForParent == 3 || barMeanForChild == 3)
            return TCKTestResult.failed(TEST_ID,"Updates to bar usage parameter in different "+
                "parameter sets were combined -- the parameter sets should be independent.");
        if(fooForParent != 1 || fooForChild != 3 || barMeanForParent != 2 || barMeanForChild != 4) {
            return TCKTestResult.failed(TEST_ID,"Usage parameter values were not set as expected. Expected values: "+
                    expectedValues+", actual values: "+actualValues);
        }
        getLog().info("parameter checks ok");
        return TCKTestResult.passed();
    }

    public void setUp() throws Exception {
        super.setUp();
        setResourceListener(resourceListener = new QueuingResourceListener(utils()));

        mBeanLookupParent = new UsageMBeanLookup("Test2243Service","Test2243Sbb",utils());
        mBeanLookupChild  = new UsageMBeanLookup("Test2243Service","Test2243SbbChild",utils());
        mBeanLookupParent.getServiceUsageMBeanProxy().createUsageParameterSet(mBeanLookupParent.getSbbID(),PARAMETER_SET_NAME);
        mBeanLookupChild.getServiceUsageMBeanProxy().createUsageParameterSet( mBeanLookupChild.getSbbID(), PARAMETER_SET_NAME);
        notificationListenerParent = new QueuingNotificationListener(utils());
        notificationListenerChild  = new QueuingNotificationListener(utils());
        mBeanLookupParent.getNamedSbbUsageMBeanProxy(PARAMETER_SET_NAME).addNotificationListener(notificationListenerParent,null,null);
        mBeanLookupChild.getNamedSbbUsageMBeanProxy( PARAMETER_SET_NAME).addNotificationListener(notificationListenerChild,null,null);
    }

    public void tearDown() throws Exception {
        if(mBeanLookupParent != null && notificationListenerParent != null) {
            mBeanLookupParent.getNamedSbbUsageMBeanProxy(PARAMETER_SET_NAME).removeNotificationListener(notificationListenerParent);
        }
        if(mBeanLookupChild != null && notificationListenerChild != null) {
            mBeanLookupChild.getNamedSbbUsageMBeanProxy( PARAMETER_SET_NAME).removeNotificationListener(notificationListenerChild);
        }
        if(mBeanLookupParent != null)mBeanLookupParent.closeAllMBeans();
        if(mBeanLookupChild  != null)mBeanLookupChild.closeAllMBeans();
        super.tearDown();
    }

    private UsageMBeanLookup mBeanLookupParent;
    private UsageMBeanLookup mBeanLookupChild;
    private QueuingResourceListener resourceListener;
    private QueuingNotificationListener notificationListenerParent;
    private QueuingNotificationListener notificationListenerChild;

}
