/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.resource.endpoint;

import java.util.HashMap;

import com.opencloud.sleetck.lib.TCKTestErrorException;
import com.opencloud.sleetck.lib.TCKTestFailureException;
import com.opencloud.sleetck.lib.TCKTestResult;
import com.opencloud.sleetck.lib.testsuite.resource.BaseResourceTest;
import com.opencloud.sleetck.lib.testsuite.resource.RAMethods;

/**
 * Test to ensure that fireEvent() throws an appropriate exception if called
 * with an activity which is local to the current transaction as an argument.
 * <p>
 * Test assertion: 1115255
 */
public class Test1115255Test extends BaseResourceTest {

    public TCKTestResult run() throws Exception {
        HashMap results = sendMessage(RAMethods.fireEvent, new Integer(1115255));
        if (results == null)
            throw new TCKTestErrorException("Test timed out while waiting for response from test component");
        Object result = results.get("result");
        if (result instanceof Exception)
            throw new TCKTestFailureException(1115255, "Unexpected exception thrown during test:", (Exception) result);
        if (Boolean.FALSE.equals(result))
            throw new TCKTestFailureException(1115255, "fireEvent() failed to throw an UnrecognizedActivityHandleException when called with a transaction-local activity");
        if (!Boolean.TRUE.equals(result))
            throw new TCKTestErrorException("Unexpected result received from test component: " + result);
        return TCKTestResult.passed();
    }

}
