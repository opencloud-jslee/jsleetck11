/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.management.ProfileProvisioningMBean;

import com.opencloud.sleetck.lib.*;
import com.opencloud.sleetck.lib.infra.SleeTCKComponentConstants;
import com.opencloud.sleetck.lib.resource.testapi.TCKResourceListener;
import com.opencloud.sleetck.lib.testutils.BaseTCKResourceListener;
import com.opencloud.sleetck.lib.testutils.ComponentIDLookup;
import com.opencloud.sleetck.lib.testutils.ProfileUtils;
import com.opencloud.sleetck.lib.testutils.jmx.ProfileMBeanProxy;
import com.opencloud.sleetck.lib.testutils.jmx.ProfileProvisioningMBeanProxy;

import javax.management.*;
import javax.slee.management.ManagementException;
import javax.slee.profile.ProfileSpecificationID;
import java.rmi.RemoteException;

/**
 * Tests assertion 4346, that where a SLEE MBean interface defines a get and/or set accessor
 * method that follows the design pattern for a managed attribute,
 * the SLEE MBean class must ensure that the target of the accessor is accessible
 * and/or updateable via both the invoke and getAttribute/setAttribute methods of the MBean Server.
 */
public class Test4346Test implements SleeTCKTest {

    private static final String PROFILE_DU_PATH_PARAM = "profileDUPath";
    private static final int TEST_ID = 4346;
    private static final String ATTRIBUTE_NAME = "AttributeA";
    private static final String PROFILE_SPEC_NAME = "Test4346ProfileCMP";

    public void init(SleeTCKTestUtils utils) {
        this.utils = utils;
    }

    /**
     * Perform the actual test.
     */

    public TCKTestResult run() throws Exception {
        TCKTestResult result;

        ProfileProvisioningMBeanProxy profileProvisioningProxy = profileUtils.getProfileProvisioningProxy();

        ProfileSpecificationID profileSpecID = new ComponentIDLookup(utils).lookupProfileSpecificationID(
                PROFILE_SPEC_NAME,SleeTCKComponentConstants.TCK_VENDOR,"1.0");

        if(profileSpecID == null) throw new TCKTestErrorException("No ProfileSpecification found.");

        ObjectName profileObj = null;
        // Create the profile table.
        try {
            profileProvisioningProxy.createProfileTable(profileSpecID, "Test4346ProfileTable");
            profileObj = profileProvisioningProxy.createProfile("Test4346ProfileTable", "Test4346Profile");
        } catch (Exception e) {
            return TCKTestResult.error("Failed to create profile table or profile.");
        }

        ProfileMBeanProxy profileProxy = utils.getMBeanProxyFactory().createProfileMBeanProxy(profileObj);
        try {
            int attr = getViaGetAttribute(profileObj).intValue();
            switch (attr) {
                case Test4346ProfileManagement.PASS:
                    result = TCKTestResult.passed();
                    break;
                case Test4346ProfileManagement.FAIL:
                    result = TCKTestResult.failed(TEST_ID, "Exception thrown when ProfileManagement.markProfileDirty() was called.");
                    break;
                default:
                    result = TCKTestResult.error("Error during profileInitialize()");                break;
            }

        } finally {
            profileProxy.commitProfile();
        }


        return result;
    }

    /**
     * Do all the pre-run configuration of the test.
     */
    public void setUp() throws Exception {

        utils.getLog().fine("Connecting to resource");
        resourceListener = new TCKResourceListenerImpl();
        utils.getResourceInterface().setResourceListener(resourceListener);

        // Install the Deployable Units
        utils.getLog().fine("Installing the profile spec");
        String duPath = utils.getTestParams().getProperty(PROFILE_DU_PATH_PARAM);
        utils.install(duPath);
        profileUtils = new ProfileUtils(utils);

    }

    /**
     * Clean up after the test.
     */

    public void tearDown() throws Exception {

        if (profileUtils != null) {
            ProfileProvisioningMBeanProxy profileProvisioningProxy = profileUtils.getProfileProvisioningProxy();

            try {
                profileProvisioningProxy.removeProfile("Test4346ProfileTable", "Test4346Profile");
            } catch (Exception e) {
                utils.getLog().warning(e);
            }

            try {
                profileUtils.removeProfileTable("Test4346ProfileTable");
            } catch (Exception e) {
                utils.getLog().warning(e);
            }
        }

        utils.getLog().fine("Disconnecting from resource");
        utils.getResourceInterface().clearActivities();
        utils.getResourceInterface().removeResourceListener();
        utils.getLog().fine("Uninstalling the profile spec");
        utils.uninstallAll();
    }

    private class TCKResourceListenerImpl extends BaseTCKResourceListener {
        public void onException(Exception e) throws RemoteException {
            utils.getLog().warning("Received exception from the TCK resource");
            utils.getLog().warning(e);
        }
    }

    private Integer getViaGetAttribute(ObjectName objName) throws ManagementException, TCKTestErrorException, TCKTestFailureException {
        try {
            return (Integer) utils.getMBeanFacade().getAttribute(objName, ATTRIBUTE_NAME);
        } catch(InstanceNotFoundException ie) {
            throw new TCKTestErrorException("Caught Exception while calling MBeanServer.getAttribute()",ie);
        } catch(ReflectionException re) {
            throw new TCKTestFailureException(TEST_ID,"Caught ReflectionException while calling MBeanServer.getAttribute(\"AttributeA\")",re);
        } catch(AttributeNotFoundException e) {
            throw new TCKTestFailureException(TEST_ID,"Caught AttributeNotFoundException while calling MBeanServer.getAttribute(\"AttributeA\")", e);
        } catch(MBeanException e) {
            Exception enclosed = e.getTargetException();
            if(enclosed instanceof ManagementException) throw (ManagementException)enclosed;
            if(enclosed instanceof RuntimeException) throw (RuntimeException)enclosed;
            throw new TCKTestErrorException("Caught unexpected exception",enclosed);
        }


    }

    private SleeTCKTestUtils utils;
    private TCKResourceListener resourceListener;
    private ProfileUtils profileUtils;

}
