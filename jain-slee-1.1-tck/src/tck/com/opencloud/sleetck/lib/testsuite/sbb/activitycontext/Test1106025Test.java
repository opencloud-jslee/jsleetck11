/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.sbb.activitycontext;

import java.rmi.RemoteException;
import java.util.HashMap;

import javax.management.NotificationListener;
import javax.management.ObjectName;
import javax.slee.ComponentID;
import javax.slee.SbbID;
import javax.slee.ServiceID;
import javax.slee.management.DeployableUnitDescriptor;
import javax.slee.management.DeployableUnitID;
import javax.slee.management.ServiceState;
import javax.slee.resource.ResourceAdaptorID;

import com.opencloud.sleetck.lib.AbstractSleeTCKTest;
import com.opencloud.sleetck.lib.TCKTestErrorException;
import com.opencloud.sleetck.lib.TCKTestResult;
import com.opencloud.sleetck.lib.infra.SleeTCKComponentConstants;
import com.opencloud.sleetck.lib.resource.TCKActivityID;
import com.opencloud.sleetck.lib.resource.TCKSbbMessage;
import com.opencloud.sleetck.lib.resource.events.TCKResourceEventX;
import com.opencloud.sleetck.lib.resource.testapi.TCKResourceListener;
import com.opencloud.sleetck.lib.resource.testapi.TCKResourceTestInterface;
import com.opencloud.sleetck.lib.testutils.BaseTCKResourceListener;
import com.opencloud.sleetck.lib.testutils.FutureResult;
import com.opencloud.sleetck.lib.testutils.jmx.DeploymentMBeanProxy;
import com.opencloud.sleetck.lib.testutils.jmx.ResourceManagementMBeanProxy;
import com.opencloud.sleetck.lib.testutils.jmx.ServiceManagementMBeanProxy;
import com.opencloud.sleetck.lib.testutils.jmx.TraceMBeanProxy;

public class Test1106025Test extends AbstractSleeTCKTest {

    private static final String SERVICE_DU_PATH_PARAM = "DUPath";

    private static final int TEST_ID = 1106025;

    /**
     * Perform the actual test.
     */

    public TCKTestResult run() throws Exception {

        // Create an RA activity
        result = new FutureResult(utils().getLog());
        resource = utils().getResourceInterface();
        TCKActivityID activityID = resource.createActivity("Test1106025InitialActivity");
        resource.fireEvent(TCKResourceEventX.X1, TCKResourceEventX.X1, activityID, null);

        return result.waitForResultOrFail(utils().getTestTimeout(), "Timeout waiting for test result.", TEST_ID);
    }

    /**
     * Do all the pre-run configuration of the test.
     */

    public void setUp() throws Exception {
        DeploymentMBeanProxy deploymentMBean = utils().getDeploymentMBeanProxy();
        ServiceManagementMBeanProxy serviceMBean = utils().getServiceManagementMBeanProxy();

        // Install Service
        String serviceDUPath = utils().getTestParams().getProperty(SERVICE_DU_PATH_PARAM);


        if (serviceDUPath != null) {
            DeployableUnitDescriptor descriptor = null;

            String absolutePath = utils().getDeploymentUnitURL(serviceDUPath);
            utils().getLog().info("Installing service: " + absolutePath);
            sbbDUID = deploymentMBean.install(absolutePath);

            try {
                 descriptor = utils().getDeploymentMBeanProxy().getDescriptor(sbbDUID);
            } catch (Exception e) {
                throw new TCKTestErrorException("An error occured while attempting to find a ServiceID contained in DU: " + sbbDUID, e);
            }

            ComponentID[] components = descriptor.getComponents();
            for (int i = 0; i < components.length; i++) {
                if (components[i] instanceof ServiceID) {
                    utils().getLog().fine("Setting serviceID value.");
                    serviceID = (ServiceID) components[i];
                    continue;
                }

                if (components[i] instanceof SbbID) {
                    utils().getLog().fine("Setting sbbID value.");
                    sbbID = (SbbID) components[i];
                    continue;
                }
            }
        }

        serviceMBean.activate(serviceID);
        utils().getLog().fine("SBB & RA are active");

        resourceListener = new TCKResourceListenerImpl();
        setResourceListener(resourceListener);

    }


    /**
     * Clean up after the test.
     */

    public void tearDown() throws Exception {
        try {
            // Final clean up checks
            utils().getResourceInterface().clearActivities();
            utils().getResourceInterface().removeResourceListener();

            // Deactivate and remove service
            try {
                ServiceState serviceState;
                DeploymentMBeanProxy deploymentMBean = utils().getDeploymentMBeanProxy();
                ServiceManagementMBeanProxy serviceMBean = utils().getServiceManagementMBeanProxy();
                utils().getLog().info("Deactivating Service: " + serviceID);
                serviceMBean.deactivate(serviceID);
                long timeout = System.currentTimeMillis() + utils().getTestTimeout();
                while (System.currentTimeMillis() < timeout) {
                    serviceState = serviceMBean.getState(serviceID);
                    if (serviceState.isInactive())
                        break;
                    Thread.sleep(500);
                }
                utils().getLog().info("Uninstall SBB: " + sbbDUID);
                deploymentMBean.uninstall(sbbDUID);
            } catch (Exception e) {
                utils().getLog().warning(e);
            }

            utils().uninstallAll();

       } catch (Exception e) {
               utils().getLog().warning(e);
               utils().getLog().warning("ERROR: problems in test tear down");
       }
    }

    private class TCKResourceListenerImpl extends BaseTCKResourceListener {

        public synchronized void onSbbMessage(TCKSbbMessage message, TCKActivityID calledActivity) throws RemoteException {
            HashMap map = (HashMap) message.getMessage();
            Boolean passed = (Boolean) map.get("Result");
            String msgString = (String) map.get("Message");

            utils().getLog().info("Received message from SBB.");

            if (passed.booleanValue() == true) {
                  result.setPassed();
                  getLog().info(msgString);
            } else {
                result.setFailed(TEST_ID, msgString);
            }
        }

        public void onActivityEndedBySbb(TCKActivityID activityID) throws RemoteException {
            try {
                getLog().info("Received onActivityEndedBySbb message from SBB.");
                TCKActivityID activityID2 = resource.createActivity("Test1106025ActivityEnded");
                resource.fireEvent(TCKResourceEventX.X2, TCKResourceEventX.X2, activityID2, null);
            } catch (Exception e) {
                utils().getLog().warning("Exception Test1106025ActivityEnded");
                utils().getLog().warning(e);
            }
        }

        public void onException(Exception e) throws RemoteException {
            utils().getLog().warning("Received exception from SBB");
            utils().getLog().warning(e);
            result.setError(e);
        }

        public Object onSbbCall(Object argument) throws Exception {
            getLog().info((String)argument);
            return null;
        }
    }


    private TCKResourceListener resourceListener;
    private FutureResult result;
    private DeployableUnitID duID;
    private NotificationListener listener;
    private ObjectName traceMBeanName;
    private TraceMBeanProxy tracembean;
    private ResourceAdaptorID raID;
    private DeployableUnitID sbbDUID;
    private DeployableUnitID raDUID;
    private String bindLinkName = "slee/resources/tck/simple";
    private String raLinkName;
    private String raEntityName;
    private SbbID sbbID;
    private ServiceID serviceID;
    private ResourceManagementMBeanProxy resourceMBean;
    private TCKResourceTestInterface resource;

}
