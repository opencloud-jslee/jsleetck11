/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.management;

import com.opencloud.sleetck.lib.*;
import com.opencloud.sleetck.lib.resource.testapi.TCKResourceListener;
import com.opencloud.sleetck.lib.testutils.BaseTCKResourceListener;

import javax.management.*;
import javax.slee.management.ManagementException;
import javax.slee.management.SleeState;
import java.rmi.RemoteException;

public class Test2096Test implements SleeTCKTest {

    private static final int TEST_ID = 2096;

    public void init(SleeTCKTestUtils utils) {
        this.utils = utils;
    }

    /**
     * Perform the actual test.
     */
    public TCKTestResult run() throws Exception {

        // Here we want to test that get/set methods in the various MBean
        // objects can be accessed via get/set and invoke.

        // Chosen test case:
        //   SleeManagementMBean.getState()

        // N.B. there are no set cases currently defined in the
        // javax.slee.management.*MBean classes, nor are there any isFoo
        // cases

        ObjectName sleeManagementMBeanName = utils.getSleeManagementMBeanName();

        SleeState sleeState = (SleeState)getViaGetAttribute(sleeManagementMBeanName, "State");
        if(sleeState == null) return TCKTestResult.failed(TEST_ID, "The SleeState object returned from the " +
                "SleeManagementMBean via getAttribute(\"State\") was null");
        utils.getLog().info("SleeManagementMBean returned a non-null SleeState object via getAttribute(\"State\"):"+sleeState);

        try {
            sleeState = (SleeState)getViaGetAttribute(sleeManagementMBeanName, "state");
            return TCKTestResult.failed(TEST_ID, "get/set accessor methods are case-sensitive.  This SLEE allowed a call to " +
                    "getAttribute(\"state\"), when only the 'State' attribute should have been available. The returned object: "+sleeState);
        } catch (TCKTestFailureException e) {
            if (e.getEnclosedException().getClass().equals(AttributeNotFoundException.class)) return TCKTestResult.passed();
            else throw e;
        }
    }

    /**
     * Do all the pre-run configuration of the test.
     */
    public void setUp() throws Exception {
        utils.getLog().fine("Connecting to resource");
        resourceListener = new TCKResourceListenerImpl();
        utils.getResourceInterface().setResourceListener(resourceListener);
    }

    /**
     * Clean up after the test.
     */

    public void tearDown() throws Exception {
        utils.getLog().fine("Disconnecting from resource");
        utils.getResourceInterface().clearActivities();
        utils.getResourceInterface().removeResourceListener();
        utils.getLog().fine("Uninstalling the profile spec");
        utils.uninstallAll();
    }

// Maintenance note: the setViaSetAttribute() is not used, as there are currently no setter methods
//  which follow design pattern for a managed attribute in the SLEE management MBeans.
//  If any are added, the setViaSetAttribute() method should be activated, and the appropriate test cases added.

//    /**
//     * Sets the attribute using MBeanServer.setAttribute()
//     */
//    private void setViaSetAttribute(ObjectName objName, String attribute, String value)
//            throws ManagementException, TCKTestErrorException, TCKTestFailureException {
//        try {
//            utils.getMBeanFacade().setAttribute(objName, new Attribute(attribute, value));
//        } catch(InstanceNotFoundException ie) {
//            throw new TCKTestErrorException("Caught Exception while calling MBeanServer.getAttribute()",ie);
//        } catch(ReflectionException re) {
//            throw new TCKTestFailureException(TEST_ID,"Caught ReflectionException while calling MBeanServer.setAttribute(...,\"AttributeA\",...)",re);
//        } catch(AttributeNotFoundException e) {
//            throw new TCKTestFailureException(TEST_ID,"Caught AttributeNotFoundException while calling MBeanServer.setAttribute(...,\"AttributeA\",...)", e);
//        } catch(InvalidAttributeValueException e) {
//            throw new TCKTestFailureException(TEST_ID,"Caught InvalidAttributeValueException while calling MBeanServer.setAttribute(...,\"AttributeA\",...)", e);
//        } catch(MBeanException e) {
//            Exception enclosed = e.getTargetException();
//            if(enclosed instanceof ManagementException) throw (ManagementException)enclosed;
//            if(enclosed instanceof RuntimeException) throw (RuntimeException)enclosed;
//            throw new TCKTestErrorException("Caught unexpected exception",enclosed);
//        }
//    }

    /**
     * Gets the attribute using MBeanServer.getAttribute()
     */
    private Object getViaGetAttribute(ObjectName objName, String attribute)
            throws ManagementException, TCKTestErrorException, TCKTestFailureException {
        try {
            return utils.getMBeanFacade().getAttribute(objName, attribute);
        } catch(InstanceNotFoundException ie) {
            throw new TCKTestErrorException("Caught Exception while calling MBeanServer.getAttribute()",ie);
        } catch(ReflectionException re) {
            throw new TCKTestFailureException(TEST_ID,"Caught ReflectionException while calling MBeanServer.getAttribute(\"AttributeA\")",re);
        } catch(AttributeNotFoundException e) {
            throw new TCKTestFailureException(TEST_ID,"Caught AttributeNotFoundException while calling MBeanServer.getAttribute(\"AttributeA\")", e);
        } catch(MBeanException e) {
            Exception enclosed = e.getTargetException();
            if(enclosed instanceof ManagementException) throw (ManagementException)enclosed;
            if(enclosed instanceof RuntimeException) throw (RuntimeException)enclosed;
            throw new TCKTestErrorException("Caught unexpected exception",enclosed);
        }
    }

    private class TCKResourceListenerImpl extends BaseTCKResourceListener {
        public void onException(Exception e) throws RemoteException {
            utils.getLog().warning("Received exception from the TCK resource");
            utils.getLog().warning(e);
        }
    }

    private SleeTCKTestUtils utils;
    private TCKResourceListener resourceListener;

}
