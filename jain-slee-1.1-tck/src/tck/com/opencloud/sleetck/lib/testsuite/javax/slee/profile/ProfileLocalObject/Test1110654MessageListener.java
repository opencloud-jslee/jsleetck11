/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.javax.slee.profile.ProfileLocalObject;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;

import javax.slee.NoSuchObjectLocalException;
import javax.slee.TransactionRequiredLocalException;
import javax.slee.TransactionRolledbackLocalException;
import javax.slee.profile.ProfileLocalObject;
import javax.slee.profile.ProfileTable;
import javax.slee.transaction.SleeTransaction;
import javax.slee.transaction.SleeTransactionManager;

import com.opencloud.logging.StdErrLog;
import com.opencloud.sleetck.lib.profileutils.BaseMessageSender;
import com.opencloud.sleetck.lib.rautils.MessageHandler;
import com.opencloud.sleetck.lib.rautils.RMIObjectChannel;
import com.opencloud.sleetck.lib.rautils.TCKRAUtils;

/**
 * Provides callback method for handling messages sent from the TCK test to the RA.
 * The actual test code is located in the 'handleMessage()' method.
 */
public class Test1110654MessageListener extends UnicastRemoteObject implements MessageHandler {

    public static final String PROFILE_TABLE_NAME= "Test1110654ProfileTable";
    public static final String PROFILE_NAME= "Test1110654Profile";
    public static final String PROFILE_NAME2= "Test1110654Profile2";

    private Test1110654ResourceAdaptor ra;

    public Test1110654MessageListener(Test1110654ResourceAdaptor ra) throws RemoteException {
        super();

        try {
            out = TCKRAUtils.lookupRMIObjectChannel();
            msgSender = new BaseMessageSender(out, new StdErrLog());
        }
        catch (Exception e) {
            ra.getLog().warning("Exception occurred when trying to acquire RMIObjectChannel: ",e);
        }

        this.ra = ra;
    }

    public boolean handleMessage(Object message) throws RemoteException {

        //run the tests, if both run through successfully and completely (=both return 'true')
        //send success msg back to TCK test
        if (test_getProfileName() && test_getProfileTable() &&
                test_getProfileTableName() && test_isIdentical() &&
                test_remove())
            msgSender.sendSuccess(1110654, "Test successful.");

        return true;
    }

    private boolean test_remove() {
        SleeTransactionManager txnManager = ra.getResourceAdaptorContext().getSleeTransactionManager();
        SleeTransaction txn=null;

        try {
            ProfileTable profileTable = ra.getResourceAdaptorContext().getProfileTable(PROFILE_TABLE_NAME);
            //start a new TXN as profileTable.find is mandatory transactional
            txn = txnManager.beginSleeTransaction();

            ProfileLocalObject profileLocal2 = profileTable.find(PROFILE_NAME2);

            //1110667: the remove() method attempts to remove a profile (subject to the TXN in which it is called being committed)
            try {
                profileLocal2.remove();
            } catch (Exception e) {
                msgSender.sendFailure(1110667, "Exception occured when calling remove(): "+e);
                return false;
            }

            txn.commit();
            txn = txnManager.beginSleeTransaction();

            if (profileTable.find(PROFILE_NAME2)!=null) {
                msgSender.sendFailure(1110667, "Profile "+PROFILE_NAME2+" was NOT removed successfully by calling ProfileLocalObject.remove().");
                return false;
            } else
                msgSender.sendLogMsg("Profile "+PROFILE_NAME2+" was removed successfully.");

            //1110669: The profile is removed in the context of the calling transaction.
            ProfileLocalObject profileLocal = profileTable.find(PROFILE_NAME);
            profileLocal.remove();

            txn.rollback();
            txn = txnManager.beginSleeTransaction();

            if (profileTable.find(PROFILE_NAME)==null) {
                msgSender.sendFailure(1110669, "Profile "+PROFILE_NAME+" was removed though TXN was rolled back.");
                return false;
            } else
                msgSender.sendLogMsg("Removal of profile "+PROFILE_NAME+" was rolled back as expected.");
            txn.commit();

            //1110668: remove method is a mandatory-transactional method.
            //1110671: javax.slee.TransactionRequiredLocalException. This exception is thrown
            //if there is no transaction associated with the calling thread.
            //it has already been invoked with a TXN context, now invoke it again without one
            //in which case it should fail
            try {
                profileLocal.remove();
                msgSender.sendFailure(1110668, "remove() should fail if invoked without a TXN context as it is supposed " +
                "to be mandatory-transactional.");
                return false;
            } catch (TransactionRequiredLocalException e) {
                msgSender.sendLogMsg("remove() was mandatory-transactional as expected.");
            } catch (Exception e) {
                msgSender.sendFailure(1110668, "Wrong type of exception thrown, expected javax.slee.TransactionRequiredLocalException " +
                        "but caught: "+e);
                return false;
            }

            //11101127: javax.slee.TransactionRolledbackLocalException is thrown when the Profile object returns by throwing an runtime exception.
            txn = txnManager.beginSleeTransaction();
            try {
                if (profileLocal2 != null) {
                    profileLocal2.remove();
                    msgSender.sendFailure(11101127, "remove() should fail if invoked on a no longer existing profile.");
                    return false;
                }
            } catch (TransactionRolledbackLocalException e) {
                msgSender.sendLogMsg("remove() caused rollback exception as expected when invoked on a ProifleLocalObject belonging to a no longer existing profile.");
                //11101128: The runtime exception is the cause of this exception, i.e. it is accessible via java.lang.Throwable.getCause().
                Throwable t = e.getCause();
                if (t==null || !(t instanceof NoSuchObjectLocalException)) {
                    msgSender.sendFailure(11101128, "The exception causing the rollback should have been a javax.slee.NoSuchObjectLocalException but was found to be "+t);
                    return false;
                }
            } catch (Exception e) {
                StringWriter sw = new StringWriter();
                e.printStackTrace(new PrintWriter(sw));
                msgSender.sendFailure(11101127, "Wrong type of exception thrown, expected javax.slee.TransactionRolledbackLocalException " +
                        "but caught: "+e+"Stacktrace: "+sw.getBuffer().toString());
                return false;
            }
            txn.rollback();
            txn = null;

        } catch (Exception e) {
            msgSender.sendException(e);
            return false;
        } finally {
            try {
                if (txn!=null)
                    txn.rollback();
            } catch (Exception ex) {
                ra.getLog().warning("Error occured when trying to rollback RA started TXN.", ex);
                msgSender.sendLogMsg("Exception occurred when trying to safely rollback RA started TXN: "+ex.getMessage());
            }
        }

        return true;
    }

    private boolean test_isIdentical() {
        SleeTransactionManager txnManager = ra.getResourceAdaptorContext().getSleeTransactionManager();
        SleeTransaction txn=null;

        try {
            ProfileTable profileTable = ra.getResourceAdaptorContext().getProfileTable(PROFILE_TABLE_NAME);
            //start a new TXN as profileTable.find is mandatory transactional
            txn = txnManager.beginSleeTransaction();

            ProfileLocalObject profileLocal = profileTable.find(PROFILE_NAME);
            ProfileLocalObject profileLocal2 = profileTable.find(PROFILE_NAME2);
            ProfileLocalObject altProfileLocal = profileTable.find(PROFILE_NAME);

            //1110664: The isIdentical method. This methods returns true if the Profile represented by the argument
            //is the same Profile as the Profile represented by this object.
            try {
                if (!profileLocal.isIdentical(profileLocal) ||
                        !profileLocal.isIdentical(altProfileLocal) ||
                        profileLocal.isIdentical(profileLocal2)) {
                    msgSender.sendFailure(1110664, "isIdentical() did not return the correct values during performed comparisons.");
                    return false;
                } else
                    msgSender.sendLogMsg("isIdentical() worked fine.");

            } catch (Exception e) {
                msgSender.sendFailure(1110664, "Exception occured when trying to invoke isIdentical(): "+e);
                return false;
            }

            txn.commit();

            //1110665: isIdentical method is a non-transactional method.
            //it has already been invoked with a TXN context, now invoke it again without one
            try {
                profileLocal.isIdentical(profileLocal);
                msgSender.sendLogMsg("isIdentical() was non-transactional as expected.");
            } catch (Exception e) {
                msgSender.sendFailure(1110665, "isIdentical() should not fail if invoked without a TXN context as it is supposed " +
                        "to be non-transactional.");
                return false;
            }

            txn = null;

        } catch (Exception e) {
            msgSender.sendException(e);
            return false;
        } finally {
            try {
                if (txn!=null)
                    txn.rollback();
            } catch (Exception ex) {
                ra.getLog().warning("Error occured when trying to rollback RA started TXN.", ex);
                msgSender.sendLogMsg("Exception occurred when trying to safely rollback RA started TXN: "+ex.getMessage());
            }
        }

        return true;
    }

    private boolean test_getProfileTableName() {
        SleeTransactionManager txnManager = ra.getResourceAdaptorContext().getSleeTransactionManager();
        SleeTransaction txn=null;

        try {
            ProfileTable profileTable = ra.getResourceAdaptorContext().getProfileTable(PROFILE_TABLE_NAME);
            //start a new TXN as profileTable.find is mandatory transactional
            txn = txnManager.beginSleeTransaction();

            ProfileLocalObject profileLocal = profileTable.find(PROFILE_NAME);

            //1110661: The getProfileTableName method. This method returns the name of the Profile Table of the Profile represented by this ProfileLocalObject.
            try {
                if (!profileLocal.getProfileTableName().equals(PROFILE_TABLE_NAME)) {
                    msgSender.sendFailure(1110661, "getProfileTableName() did not return the expected profile table name.");
                    return false;
                } else
                    msgSender.sendLogMsg("getProfileTableName() returned "+PROFILE_TABLE_NAME+" as expected.");
            } catch (Exception e) {
                msgSender.sendFailure(1110661, "Exception occured when trying to invoke getProfileTableName(): "+e);
                return false;
            }

            txn.commit();

            //1110662: getProfileTableName method is a non-transactional method.
            //it has already been invoked with a TXN context, now invoke it again without one
            try {
                profileLocal.getProfileTableName();
                msgSender.sendLogMsg("getProfileTableName() be non-transactional as expected.");
            } catch (Exception e) {
                msgSender.sendFailure(1110662, "getProfileTableName() should not fail if invoked without a TXN context as it is supposed " +
                        "to be non-transactional.");
                return false;
            }

            txn = null;

        } catch (Exception e) {
            msgSender.sendException(e);
            return false;
        } finally {
            try {
                if (txn!=null)
                    txn.rollback();
            } catch (Exception ex) {
                ra.getLog().warning("Error occured when trying to rollback RA started TXN.", ex);
                msgSender.sendLogMsg("Exception occurred when trying to safely rollback RA started TXN: "+ex.getMessage());
            }
        }

        return true;
    }

    private boolean test_getProfileTable() {
        SleeTransactionManager txnManager = ra.getResourceAdaptorContext().getSleeTransactionManager();
        SleeTransaction txn=null;

        try {
            ProfileTable profileTable = ra.getResourceAdaptorContext().getProfileTable(PROFILE_TABLE_NAME);
            //start a new TXN as profileTable.find is mandatory transactional
            txn = txnManager.beginSleeTransaction();

            ProfileLocalObject profileLocal = profileTable.find(PROFILE_NAME);

            //1110657: The getProfileTable method. This method returns the Profile Table of the Profile represented by this ProfileLocalObject.
            try {
                ProfileLocalObject internal = profileLocal.getProfileTable().find(PROFILE_NAME);
                if (internal==null || !internal.getProfileName().equals(PROFILE_NAME)) {
                    msgSender.sendFailure(1110657, "getProfileTable() did not return a valid ProfileTable object for the profile's profile table.");
                    return false;
                } else
                    msgSender.sendLogMsg("getProfileTable() returned the Profile's ProfileTable object as expected.");
            } catch (Exception e) {
                msgSender.sendFailure(1110657, "Exception occured when trying to invoke getProfileTable(): "+e);
                return false;
            }

            txn.commit();

            //1110658: The return value is able to be type cast to the specific Profile Table interface defined by the Profile Specification.
            //1110659: getProfileTable method is a non-transactional method.
            try {
                Test1110654ProfileTable castedTable = (Test1110654ProfileTable)profileLocal.getProfileTable();
                msgSender.sendLogMsg("getProfileTable() behaved non-transactional as expected and its return value could be cast into the profile spec's ProfileTable class.");
            } catch (ClassCastException e) {
                msgSender.sendFailure(1110658, "ClassCastException occurred when trying to cast the return value of getProfileTable() " +
                        "into the profile spec's ProfileTable class.");
                return false;
            } catch (Exception e) {
                msgSender.sendFailure(1110659, "getProfileTable() should not fail if invoked without a TXN context as it is supposed " +
                        "to behave non-transactional.");
                return false;
            }

            txn = null;

        } catch (Exception e) {
            msgSender.sendException(e);
            return false;
        } finally {
            try {
                if (txn!=null)
                    txn.rollback();
            } catch (Exception ex) {
                ra.getLog().warning("Error occured when trying to rollback RA started TXN.", ex);
                msgSender.sendLogMsg("Exception occurred when trying to safely rollback RA started TXN: "+ex.getMessage());
            }
        }

        return true;
    }

    private boolean test_getProfileName() {
        SleeTransactionManager txnManager = ra.getResourceAdaptorContext().getSleeTransactionManager();
        SleeTransaction txn=null;

        try {
            ProfileTable profileTable = ra.getResourceAdaptorContext().getProfileTable(PROFILE_TABLE_NAME);
            //start a new TXN as profileTable.find is mandatory transactional
            txn = txnManager.beginSleeTransaction();

            ProfileLocalObject profileLocal = profileTable.find(PROFILE_NAME);

            //1110654: The getProfileName method. This method returns the name of the Profile represented by this ProfileLocalObject.
            try {
                if (!profileLocal.getProfileName().equals(PROFILE_NAME)) {
                    msgSender.sendFailure(1110654, "getProfileName() did not return the expected profile name.");
                    return false;
                } else
                    msgSender.sendLogMsg("getProfileName() returned "+PROFILE_NAME+" as expected.");
            } catch (Exception e) {
                msgSender.sendFailure(1110654, "Exception occured when trying to invoke getProfileName(): "+e);
                return false;
            }

            txn.commit();

            //1110655: getProfileName method is a non-transactional method.
            //it has already been invoked with a TXN context, now invoke it again without one
            try {
                profileLocal.getProfileName();
                msgSender.sendLogMsg("getProfileName() behaved non-transactional as expected.");
            } catch (Exception e) {
                msgSender.sendFailure(1110655, "getProfileName() should not fail if invoked without a TXN context as it is supposed " +
                        "to behave non-transactional.");
                return false;
            }

            txn = null;

        } catch (Exception e) {
            msgSender.sendException(e);
            return false;
        } finally {
            try {
                if (txn!=null)
                    txn.rollback();
            } catch (Exception ex) {
                ra.getLog().warning("Error occured when trying to rollback RA started TXN.", ex);
                msgSender.sendLogMsg("Exception occurred when trying to safely rollback RA started TXN: "+ex.getMessage());
            }
        }

        return true;
    }

    private BaseMessageSender msgSender;
    private RMIObjectChannel out;

}

