/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.sbb.abstractclass;

import com.opencloud.sleetck.lib.sbbutils.TCKSbbUtils;
import com.opencloud.sleetck.lib.resource.events.TCKResourceEventX;

import javax.slee.*;
import javax.slee.facilities.*;

/**
 * Test illegal reentrancy from s6.11
 */
public abstract class NonReentrantSbb extends SendResultsSbb {
    public void onTCKResourceEventX1(TCKResourceEventX event, ActivityContextInterface aci) {
        try {
            try {
                // attempt a reentrant method call
                ((NonReentrantSbbLocal)getSbbContext().getSbbLocalObject()).doSomething();

                setResultFailed(479, "non-reentrant sbb allowed a reentrant local-interface invocation");
            }
            catch (Exception e) {
                if (e.getClass().equals(SLEEException.class)) {
                    // intended result
                    TCKSbbUtils.createTrace(getSbbID(), Level.INFO, "received javax.slee.SLEEException as expected", null);
                }
                else {
                    TCKSbbUtils.createTrace(getSbbID(), Level.INFO, "received unexpected exception: " + e, null);
                    setResultFailed(479, "received unexpected exception during illegal reentrant invocation: " + e);
                }
            }
        }
        catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }
    }

    public void onTCKResourceEventX2(TCKResourceEventX event, ActivityContextInterface aci) {
        try {
            try {
                // attempt a reentrant remove
                getSbbContext().getSbbLocalObject().remove();

                setResultFailed(479, "non-reentrant sbb allowed a reentrant remove invocation");
            }
            catch (Exception e) {
                if (e.getClass().equals(SLEEException.class)) {
                    // intended result
                    TCKSbbUtils.createTrace(getSbbID(), Level.INFO, "received javax.slee.SLEEException as expected", null);
                    setResultPassed("non-reentrancy test passed");
                }
                else {
                    TCKSbbUtils.createTrace(getSbbID(), Level.INFO, "received unexpected exception: " + e, null);
                    setResultFailed(479, "received unexpected exception during illegal reentrant invocation: " + e);
                }
            }
        }
        catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }
    }

    public void doSomething() {
        try {
            TCKSbbUtils.createTrace(getSbbID(), Level.WARNING, "illegally in doSomething() method", null);
        }
        catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }
    }
}

