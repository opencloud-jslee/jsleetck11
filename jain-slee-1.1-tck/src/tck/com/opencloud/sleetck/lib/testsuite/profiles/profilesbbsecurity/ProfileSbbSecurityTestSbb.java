/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.profiles.profilesbbsecurity;

import com.opencloud.sleetck.lib.sbbutils.BaseTCKSbb;
import com.opencloud.sleetck.lib.sbbutils.TCKSbbUtils;
import com.opencloud.sleetck.lib.resource.events.TCKResourceEventX;
import com.opencloud.sleetck.lib.testsuite.profiles.simpleprofile.SimpleProfileCMP;
import com.opencloud.sleetck.lib.TCKTestFailureException;

import javax.slee.ActivityContextInterface;
import javax.slee.profile.UnrecognizedProfileNameException;
import javax.slee.profile.UnrecognizedProfileTableNameException;
import javax.slee.profile.ProfileID;
import javax.slee.facilities.Level;

public abstract class ProfileSbbSecurityTestSbb extends BaseTCKSbb {
    public void onTCKResourceEventX1(TCKResourceEventX event, ActivityContextInterface aci) {
        try {
            if (true == testSbbSecurity()) {
                TCKSbbUtils.createTrace(getSbbID(), Level.INFO, "Security exception received", null);
                handleEvent(event, TCKResourceEventX.X1);
            } else {
                TCKSbbUtils.handleException(new TCKTestFailureException(987, "Profile setter access allowed from SBB"));
            }
        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }
    }

    private void handleEvent(TCKResourceEventX event, String eventTypeName) {
        try {
            TCKSbbUtils.createTrace(getSbbID(), Level.INFO, "Received " + eventTypeName + " event", null);
            // send a reply: the event type received and the event type as stored in the message
            String[] message = new String[]{eventTypeName, (String) event.getMessage()};
            TCKSbbUtils.getResourceInterface().sendSbbMessage(message);
        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }
    }

    public abstract SimpleProfileCMP getCmpSimpleProfile(ProfileID profileID)
            throws UnrecognizedProfileTableNameException,
            UnrecognizedProfileNameException;

    /**
     * Test the assertion that an SBB cannot invoke a profile's setter methods (assertion id 987)
     * @return
     */
    private boolean testSbbSecurity() {
        try {
            SimpleProfileCMP profile = null;
            TCKSbbUtils.createTrace(getSbbID(), Level.INFO, "Setting profile value", null);
            profile = getCmpSimpleProfile(new ProfileID(ProfileSbbSecurityTest.PROFILE_TABLE_NAME,
                    "A"));
            profile.setValue("new value");
            TCKSbbUtils.createTrace(getSbbID(), Level.INFO, "Value set", null);
        } catch (java.lang.UnsupportedOperationException e) {
            return true;
        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }
        return false;
    }
}
