/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.sbb.abstractclass;

import com.opencloud.sleetck.lib.sbbutils.BaseTCKSbb;

import javax.slee.CreateException;

public abstract class Test357ChildSbb extends BaseTCKSbb {

    public static final String CREATE_EXCEPTION_MESSAGE = "Test357ChildSbb CreateException from sbbCreate()";

    public void sbbCreate() throws CreateException {
        throw new CreateException(CREATE_EXCEPTION_MESSAGE);
    }

}
