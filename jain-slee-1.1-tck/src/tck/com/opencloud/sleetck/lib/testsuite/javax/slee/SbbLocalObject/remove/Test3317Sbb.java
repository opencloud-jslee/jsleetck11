/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.javax.slee.SbbLocalObject.remove;

import javax.slee.ActivityEndEvent;
import javax.slee.ActivityContextInterface;
import javax.slee.Address;
import javax.slee.SbbLocalObject;
import javax.slee.facilities.Level;
import javax.slee.ChildRelation;

import com.opencloud.sleetck.lib.sbbutils.BaseTCKSbb;
import com.opencloud.sleetck.lib.sbbutils.TCKSbbUtils;
import com.opencloud.sleetck.lib.sbbutils.TCKSbbConstants;
import com.opencloud.sleetck.lib.resource.events.TCKResourceEventX;
import com.opencloud.sleetck.lib.resource.TCKActivityID;
import com.opencloud.sleetck.lib.resource.sbbapi.TCKActivity;
import com.opencloud.sleetck.lib.resource.sbbapi.TCKResourceSbbInterface;
import com.opencloud.sleetck.lib.resource.sbbapi.TCKActivityContextInterfaceFactory;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

/**
 * Test 3317: SbbLocalObject.remove(SbbLocalObject).
 *
 * Any children of the removed SBB entity will also be removed.
 */

public abstract class Test3317Sbb extends BaseTCKSbb {

    public void onTCKResourceEventX1(TCKResourceEventX event, ActivityContextInterface aci) {

        try {

            // Create the children and attach them to this ACI.
            aci.attach(getChildRelation().create());
            aci.attach(getSecondChildRelation().create());

            // Fire the event that tells the Child to create its Child.
            fireTest3317Event(new Test3317Event(), aci, null);

        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }

    }

    public void onTest3317EventTwo(Test3317EventTwo event, ActivityContextInterface aci) {
        try {
            // Remove the first child (which has a child itself)

            ArrayList copy = new ArrayList();
            // copy list of children to new collection so we don't concurrently modify the iterator
            for (Iterator iter = getChildRelation().iterator(); iter.hasNext(); ) copy.add(iter.next());

            Iterator iter = copy.iterator();
            while (iter.hasNext()) {
                SbbLocalObject childLocalObject = (SbbLocalObject) iter.next();
                childLocalObject.remove();
            }

            // Fire the final test event to determine success or failure.
            fireTest3317EventThree(new Test3317EventThree(), aci, null);
         } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }
   }

    // Event fire methods.
    public abstract void fireTest3317Event(Test3317Event event, ActivityContextInterface aci, Address address);
    public abstract void fireTest3317EventThree(Test3317EventThree event, ActivityContextInterface aci, Address address);

    // Get child relation method.
    public abstract ChildRelation getChildRelation();
    public abstract ChildRelation getSecondChildRelation();

}
