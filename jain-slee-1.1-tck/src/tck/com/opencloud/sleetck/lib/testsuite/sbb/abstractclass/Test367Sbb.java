/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.sbb.abstractclass;

import java.util.HashMap;

import javax.slee.ActivityContextInterface;
import javax.slee.CreateException;

import com.opencloud.sleetck.lib.resource.events.TCKResourceEventX;
import com.opencloud.sleetck.lib.sbbutils.BaseTCKSbb;
import com.opencloud.sleetck.lib.sbbutils.TCKSbbUtils;

public abstract class Test367Sbb extends BaseTCKSbb {

    public void sbbCreate() throws CreateException {

        try {
            Object currentTransaction = TCKSbbUtils.getResourceAdaptorInterface().getTransactionIDAccess().getCurrentTransactionID();
            setTransactionID(currentTransaction);
        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }
    }

    public void sbbPostCreate() throws CreateException {

        try {
            HashMap map = new HashMap();

            Object currentTransaction = TCKSbbUtils.getResourceAdaptorInterface().getTransactionIDAccess().getCurrentTransactionID();
            if (currentTransaction != null && currentTransaction.equals(getTransactionID())) {
                map.put("Result", new Boolean(true));
                map.put("Message", "Ok");
            } else {
                map.put("Result", new Boolean(false));
                map.put("Message", "sbbPostCreate() was executed in a different transaction to sbbCreate()");
            }

            TCKSbbUtils.getResourceInterface().sendSbbMessage(map);
        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }
    }

    // Initial event
    public void onTCKResourceEventX1(TCKResourceEventX ev, ActivityContextInterface aci) {
    }

    public abstract void setTransactionID(Object txnID);
    public abstract Object getTransactionID();

}
