/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.javax.slee.Sbb;

import java.util.HashMap;

import javax.slee.ActivityContextInterface;

import com.opencloud.sleetck.lib.resource.events.TCKResourceEventX;
import com.opencloud.sleetck.lib.sbbutils.BaseTCKSbb;
import com.opencloud.sleetck.lib.sbbutils.TCKSbbUtils;

public abstract class Test3220Sbb extends BaseTCKSbb {

    // Override the BaseTCKSbb's version of this function which reports a test error.
    public void sbbRolledBack(javax.slee.RolledBackContext context) {

        try {
            HashMap map = new HashMap();
            map.put("Result", new Boolean(true));
            map.put("TXN", TCKSbbUtils.getResourceAdaptorInterface().getTransactionIDAccess().getCurrentTransactionID());
            map.put("Message", "Ok");

            TCKSbbUtils.getResourceInterface().sendSbbMessage(map);
        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }
    }

    // Override BaseTCKSbb's sbbExceptionThrown
    public void sbbExceptionThrown(Exception exception, Object event, ActivityContextInterface aci) {
    }

    // Initial event
    public void onTCKResourceEventX1(TCKResourceEventX ev, ActivityContextInterface aci) {

        try {
            HashMap map = new HashMap();
            map.put("TXN", TCKSbbUtils.getResourceAdaptorInterface().getTransactionIDAccess().getCurrentTransactionID());
            TCKSbbUtils.getResourceInterface().sendSbbMessage(map);
        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }

        // Throw an unchecked exception.
        throw new IllegalArgumentException("Test3220IllegalArgumentException");
    }

    public void onTCKResourceEventX2(TCKResourceEventX ev, ActivityContextInterface aci) {

        HashMap map = new HashMap();

        map.put("Result", new Boolean(false));
        map.put("Message", "sbbRolledBack() was not called.");
        map.put("ID", new Integer(3220));

        try {
            TCKSbbUtils.getResourceInterface().sendSbbMessage(map);
        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }
    }
}
