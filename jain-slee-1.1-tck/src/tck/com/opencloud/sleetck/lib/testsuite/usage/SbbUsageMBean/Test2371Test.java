/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.usage.SbbUsageMBean;

import com.opencloud.sleetck.lib.OperationTimedOutException;
import com.opencloud.sleetck.lib.TCKTestResult;
import com.opencloud.sleetck.lib.TCKTestErrorException;
import com.opencloud.sleetck.lib.resource.TCKActivityID;
import com.opencloud.sleetck.lib.resource.events.TCKResourceEventX;
import com.opencloud.sleetck.lib.resource.testapi.TCKResourceTestInterface;
import com.opencloud.sleetck.lib.testsuite.usage.common.GenericUsageMBeanProxy;
import com.opencloud.sleetck.lib.testsuite.usage.common.GenericUsageSbbInstructions;
import com.opencloud.sleetck.lib.testsuite.usage.common.GenericUsageTest;
import com.opencloud.sleetck.lib.testutils.QueuingNotificationListener;
import com.opencloud.sleetck.lib.testutils.QueuingResourceListener;

/**
 * Tests the atomicity of usage parameter resets made via the reset flag in
 * the usage parameter accessor methods of the SbbUsageMBean.
 * Fires a number of events containing update instructions to the SBB.
 * Checks and resets the usage parameter values as it receives the usage
 * notifications, and accumulates the sum of counter values and sample counts
 * before resets.
 * After all notifications are received, it checks that the accumulated counter
 * value and accumulate sample count match the expected values (i.e.) the values
 * as they would be had no resets occurred.
 * A deviation from the expected value indicates that a reset operation was not atomic.
 */
public class Test2371Test extends GenericUsageTest {

    private static final String PAUSE_BETWEEN_SENDS_MS_PARAM = "pauseBetweenSendsMs";

    private static final int TEST_ID = 2371;
    private static final int UPDATE_SETS_COUNT = 200;
    private static final int NOTIFICATIONS_TO_SKIP_PER_STEP = 3;

    public TCKTestResult run() throws Exception {

        GenericUsageSbbInstructions instructions = new GenericUsageSbbInstructions(null);
        instructions.addFirstCountIncrement(1);
        instructions.addFirstCountIncrement(2);
        instructions.addTimeBetweenNewConnectionsSamples(3);
        instructions.addTimeBetweenNewConnectionsSamples(4);

        // set expectations
        totalNotificationsExpected      = instructions.getTotalUpdates() * UPDATE_SETS_COUNT;
        accumulatedCounterValueExpected = (1 + 2) * UPDATE_SETS_COUNT; // increments 1 and 2
        accumulatedSampleCountExpected  = 2       * UPDATE_SETS_COUNT; // samples 3 and 4

        getLog().info("Firing update events to the SBB");
        startFiringUpdateEvents(instructions);

        getLog().info("Receiving usage notifications. Will reset the usage parameters as the notifications are received...");
        GenericUsageMBeanProxy sbbUsageMBeanProxy = getGenericUsageMBeanLookup().getUnnamedGenericUsageMBeanProxy();
        while(notificationsReceived < totalNotificationsExpected && fireEventsException == null) {
            // access and reset the counter value
            long currentCounterValue = sbbUsageMBeanProxy.getFirstCount(true);
            accumulatedCounterValue += currentCounterValue;
            getLog().fine("Accessed and reset counter value for parameter \'firstCount\'. Counter value before reset="+
                    currentCounterValue+"; Accumulated counter value="+accumulatedCounterValue);
            // access and reset the sample value
            long currentSampleCount = sbbUsageMBeanProxy.getTimeBetweenNewConnections(true).getSampleCount();
            accumulatedSampleCount += currentSampleCount;
            getLog().fine("Accessed and reset sample parameter \'timeBetweenNewConnections\'. Sample count before reset="+
                    currentSampleCount+"; Accumulated sample count="+accumulatedSampleCount);
            // skip some notifications
            int remainingNotifications = totalNotificationsExpected - notificationsReceived;
            int notificationsToSkip = Math.min(NOTIFICATIONS_TO_SKIP_PER_STEP, remainingNotifications);
            getLog().finer("Skipping "+notificationsToSkip+" usage notifications");
            for (int i = 0; i < notificationsToSkip; i++) {
                try {
                    notificationListener.nextNotification();
                } catch (OperationTimedOutException e) {
                    // if fireEventsException is set - it should be the reason behind the timeout
                    synchronized(this) {
                        throw fireEventsException != null ? fireEventsException : e;
                    }
                }
            }
            notificationsReceived += notificationsToSkip;
        }
        checkForSendException();

        getLog().info("Received all expected usage notifications");

        // accumulate final values from counter and sampler
        long currentCounterValue = sbbUsageMBeanProxy.getFirstCount(true);
        accumulatedCounterValue += currentCounterValue;
        getLog().fine("Accessed and reset counter value for parameter \'firstCount\'. Counter value before reset="+
                      currentCounterValue+"; Accumulated counter value="+accumulatedCounterValue);
        long currentSampleCount = sbbUsageMBeanProxy.getTimeBetweenNewConnections(true).getSampleCount();
        accumulatedSampleCount += currentSampleCount;
        getLog().fine("Accessed and reset sample parameter \'timeBetweenNewConnections\'. Sample count before reset="+
                      currentSampleCount+"; Accumulated sample count="+accumulatedSampleCount);

        if(accumulatedCounterValue != accumulatedCounterValueExpected) {
            return TCKTestResult.failed(TEST_ID,"Accumulated counter value did not match expected value. "+
                    "This may indicate that some reset operations were not atomic. accumulatedCounterValueExpected="+
                    accumulatedCounterValueExpected+";accumulatedCounterValue="+accumulatedCounterValue);
        } else getLog().info("Accumulated counter value was as expected");
        if(accumulatedSampleCount != accumulatedSampleCountExpected) {
            return TCKTestResult.failed(TEST_ID,"Accumulated sample count did not match expected count. "+
                    "This may indicate that some reset operations were not atomic. accumulatedSampleCountExpected="+
                    accumulatedSampleCountExpected+";accumulatedSampleCount="+accumulatedSampleCount);
        } else getLog().info("Accumulated sample count was as expected");

        getLog().fine("Checking that reply messages were received from the SBB");
        checkForSendException();
        receiveReplies();
        checkForSendException();
        return TCKTestResult.passed();
    }

    /**
     * Fires update events to the SBB in a separate thread.
     */
    private void startFiringUpdateEvents(GenericUsageSbbInstructions instructions) throws Exception {
        TCKResourceTestInterface resource = utils().getResourceInterface();
        TCKActivityID activityID = resource.createActivity("Test2371Test-Activity");
        Object instructionsExported = instructions.toExported();
        new Thread(new FireEventsTask(resource, activityID, instructionsExported)).start();
    }

    private void checkForSendException() throws Exception {
        synchronized(this) {
            if(fireEventsException != null) throw fireEventsException;
        }
    }

    private void receiveReplies() throws Exception {
        for (int i = 0; i < UPDATE_SETS_COUNT; i++) {
            resourceListener.nextMessage();
        }
    }

    public void setUp() throws Exception {
        super.setUp();
        setResourceListener(resourceListener = new QueuingResourceListener(utils()));
        notificationListener = new QueuingNotificationListener(utils());
        getGenericUsageMBeanLookup().getUnnamedGenericUsageMBeanProxy().addNotificationListener(notificationListener, null, null);
        pauseBetweenSendsMs = Integer.parseInt(utils().getTestParams().getProperty(PAUSE_BETWEEN_SENDS_MS_PARAM));
        if(pauseBetweenSendsMs < 0) throw new TCKTestErrorException("Invalid value for 'pauseBetweenSendsMs' parameter (can't be negative): "+pauseBetweenSendsMs);
        fireEventsException = null;
    }

    public void tearDown() throws Exception {
        if(notificationListener!=null) {
            getGenericUsageMBeanLookup().getUnnamedGenericUsageMBeanProxy().removeNotificationListener(notificationListener);
        }
        super.tearDown();
    }

    private class FireEventsTask implements Runnable {

        public FireEventsTask(TCKResourceTestInterface resource, TCKActivityID activityID, Object instructionsExported) {
            this.resource = resource;
            this.activityID = activityID;
            this.instructionsExported = instructionsExported;
        }

        public void run() {
            getLog().info("Started event firing thread.");
            getLog().info("Will fire "+UPDATE_SETS_COUNT+" events with a "+pauseBetweenSendsMs+" ms pause between events.");
            try {
                for (int i = 0; i < UPDATE_SETS_COUNT; i++) {
                    getLog().finest("Firing update event");
                    resource.fireEvent(TCKResourceEventX.X1,instructionsExported,activityID,null);
                    if(pauseBetweenSendsMs > 0) {
                        getLog().finest("Waiting for "+pauseBetweenSendsMs+" milliseconds");
                        synchronized(this) {
                            wait(pauseBetweenSendsMs);
                        }
                    }
                }
            } catch (Exception e) {
                synchronized(Test2371Test.this) {
                    fireEventsException = e;
                }
                getLog().warning("Received Exception in event firing thread:");
                getLog().warning(e);
            }
        }

        private TCKResourceTestInterface resource;
        private TCKActivityID activityID;
        private Object instructionsExported;
    }

    private QueuingResourceListener resourceListener;
    private QueuingNotificationListener notificationListener;

    private int pauseBetweenSendsMs;
    private volatile Exception fireEventsException;

    private int accumulatedCounterValue;
    private int accumulatedCounterValueExpected;
    private int accumulatedSampleCount;
    private int accumulatedSampleCountExpected;
    private int notificationsReceived;
    private int totalNotificationsExpected;

}
