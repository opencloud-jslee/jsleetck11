/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.resource;

import java.io.Serializable;

public class SimpleEvent implements Serializable {

    public static final String SIMPLE_EVENT_NAME = "com.opencloud.sleetck.lib.testsuite.resource.SimpleEvent";

    public SimpleEvent() {
        this.sequenceID = -1;
    }

    public SimpleEvent(int sequenceID) {
        this.sequenceID = sequenceID;
    }

    public SimpleEvent(int sequenceID, Object payload) {
        this.sequenceID = sequenceID;
        this.payload = payload;
    }

    public int getSequenceID() {
        return sequenceID;
    }

    public Object getPayload() {
        return payload;
    }

    public String toString() {
        if (payload != null)
            return "SimpleEvent[id=" + sequenceID + ",payload=" + payload + "]";
        return "SimpleEvent[id=" + sequenceID + "]";
    }

    private int sequenceID = 0;
    private Object payload = null;
}
