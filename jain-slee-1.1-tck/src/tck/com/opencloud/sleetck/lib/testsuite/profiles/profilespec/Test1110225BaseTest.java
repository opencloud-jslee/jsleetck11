/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.profiles.profilespec;

import java.rmi.RemoteException;
import java.util.HashMap;

import javax.management.ObjectName;
import javax.slee.profile.ProfileSpecificationID;

import com.opencloud.sleetck.lib.AbstractSleeTCKTest;
import com.opencloud.sleetck.lib.OperationTimedOutException;
import com.opencloud.sleetck.lib.TCKTestErrorException;
import com.opencloud.sleetck.lib.TCKTestFailureException;
import com.opencloud.sleetck.lib.TCKTestResult;
import com.opencloud.sleetck.lib.infra.SleeTCKComponentConstants;
import com.opencloud.sleetck.lib.resource.TCKActivityID;
import com.opencloud.sleetck.lib.resource.TCKSbbMessage;
import com.opencloud.sleetck.lib.resource.events.TCKResourceEventX;
import com.opencloud.sleetck.lib.resource.testapi.TCKResourceTestInterface;
import com.opencloud.sleetck.lib.sbbutils.events2.SbbBaseMessageConstants;
import com.opencloud.sleetck.lib.testutils.ProfileUtils;
import com.opencloud.sleetck.lib.testutils.QueuingResourceListener;
import com.opencloud.sleetck.lib.testutils.jmx.ProfileMBeanProxy;
import com.opencloud.sleetck.lib.testutils.jmx.ProfileProvisioningMBeanProxy;


public abstract class Test1110225BaseTest extends AbstractSleeTCKTest {

    private static final String SBB_EVENT_DU_PATH = "sbbEventDUPath";
    private static final String DU_PATH_PARAM= "DUPath";

    private static final String SPEC_VERSION = "1.0";

    protected abstract String getProfileSpecName();

    /**
     * Run test expecting allowed reentrant or not allowed reentrant behaviour. The SBB event handler calls a business method
     * via ProfileLocal interface on profile PROFILE_NAME. PROFILE_NAME calls business method on
     * PROFILE_NAME2 and PROFILE_NAME2 calls another method on PROFILE_NAME thus completing
     * the loopback.
     */
    public TCKTestResult run() throws Exception {

        String specName = getProfileSpecName();

        setupTable(specName, Test1110225ProfileLocal.PROFILE_TABLE_NAME);

        sendResourceEvent();

        return TCKTestResult.passed();
    }

    private void setupProfile(String tableName, String profileName) throws Exception {
        //add a profile to the table
        ObjectName profile = profileProvisioning.createProfile(tableName, profileName);
        ProfileMBeanProxy profileProxy = utils().getMBeanProxyFactory().createProfileMBeanProxy(profile);
        getLog().fine("Created profile "+profileName+" for profile table "+tableName);

        //commit and close the Profile
        profileProxy.commitProfile();
        profileProxy.closeProfile();
        getLog().fine("Commit and close profile "+profileName);

    }

    private void setupTable(String specName, String tableName) throws Exception {

      //create a profile table
      ProfileSpecificationID specID = new ProfileSpecificationID(specName, SleeTCKComponentConstants.TCK_VENDOR, SPEC_VERSION);
      profileProvisioning.createProfileTable(specID, tableName);
      getLog().fine("Added profile table "+tableName+" based on profile spec "+specName);

      setupProfile(tableName, Test1110225ProfileLocal.PROFILE_NAME);

      setupProfile(tableName, Test1110225ProfileLocal.PROFILE_NAME2);
    }


    private void sendResourceEvent() throws TCKTestErrorException, RemoteException, TCKTestFailureException {
        // send an initial event to the test
        TCKResourceTestInterface resource = utils().getResourceInterface();
        TCKActivityID activityID = resource.createActivity(getClass().getName());

        resource.fireEvent(TCKResourceEventX.X1, null, activityID, null);

        try {
            TCKSbbMessage reply = resourceListener.nextMessage();
            HashMap map = (HashMap) reply.getMessage();
            int type = ((Integer)map.get(SbbBaseMessageConstants.TYPE)).intValue();

            switch (type) {
            case SbbBaseMessageConstants.TYPE_SET_RESULT:
                String msg = (String) map.get("Message");
                int id = ((Integer)map.get(SbbBaseMessageConstants.ID)).intValue();
                boolean result = ((Boolean)map.get(SbbBaseMessageConstants.RESULT)).booleanValue();
                if (result)
                    getLog().fine(id+": "+msg);
                else {
                    getLog().fine("FAILURE: "+msg);
                    throw new TCKTestFailureException(id, msg);
                }
                break;
            }
        } catch (OperationTimedOutException ex) {
            throw new TCKTestErrorException("Timed out waiting for processing of initial resource event.", ex);
        }
    }

    public void setUp() throws Exception {

        setupService(SBB_EVENT_DU_PATH);

        setupService(DU_PATH_PARAM);

        profileUtils = new ProfileUtils(utils());
        profileProvisioning = profileUtils.getProfileProvisioningProxy();

        resourceListener = new QueuingResourceListener(utils()) {
            public Object onSbbCall(Object argument) throws Exception {
                HashMap map = (HashMap) argument;
                int type = ((Integer)map.get(SbbBaseMessageConstants.TYPE)).intValue();
                switch (type) {
                case SbbBaseMessageConstants.TYPE_LOG_MSG:
                    getLog().fine((String)map.get(SbbBaseMessageConstants.MSG));
                }
                return null;
            }
        };
        setResourceListener(resourceListener);
    }

    public void tearDown() throws Exception {

        try {
            profileUtils.removeProfileTable(Test1110225ProfileLocal.PROFILE_TABLE_NAME);
        }
        catch (Exception e) {
            getLog().warning("Caught exception while trying to remove profile table:");
            getLog().warning(e);
        }

        super.tearDown();
    }

    private ProfileUtils profileUtils;
    private QueuingResourceListener resourceListener;
    private ProfileProvisioningMBeanProxy profileProvisioning;
}
