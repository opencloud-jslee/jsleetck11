/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.transactions;

import com.opencloud.sleetck.lib.AbstractSleeTCKTest;
import com.opencloud.sleetck.lib.TCKTestResult;
import com.opencloud.sleetck.lib.resource.TCKActivityID;
import com.opencloud.sleetck.lib.resource.TCKSbbMessage;
import com.opencloud.sleetck.lib.resource.events.TCKResourceEventX;
import com.opencloud.sleetck.lib.resource.testapi.TCKResourceListener;
import com.opencloud.sleetck.lib.resource.testapi.TCKResourceTestInterface;
import com.opencloud.sleetck.lib.testutils.BaseTCKResourceListener;
import com.opencloud.sleetck.lib.testutils.FutureResult;
import com.opencloud.util.Future;

import java.rmi.RemoteException;

public class Test2508Test extends AbstractSleeTCKTest {

    /**
     * Perform the actual test.
     */

    public TCKTestResult run() throws Exception {
        futureResult = new FutureResult(getLog());

        getLog().info("Firing initial event to SBB");
        TCKResourceTestInterface resource = utils().getResourceInterface();
        TCKActivityID activityID = resource.createActivity("Test2508InitialActivity");
        resource.fireEvent(TCKResourceEventX.X1, TCKResourceEventX.X1, activityID, null);

        try {
            return futureResult.waitForResult(utils().getTestTimeout());
        } catch (Future.TimeoutException e) {
            if(wasChildMethodCalled) {
                return TCKTestResult.failed(2510,"Timed out while waiting for the sbbRolledBack() method to be invoked on the parent SBB, "+
                        "following the invocation of a child sbb's local interface method which threw a RuntimeException");
            } else {
                return TCKTestResult.error("Timed out waiting for a message indicating that the TransactionRolledbackLocalException was caught " +
                        "as a result of the invocation of a child sbb's local interface method throwing a RuntimeException");
            }
        }
    }

    /**
     * Do all the pre-run configuration of the test.
     */

    public void setUp() throws Exception {
        // unset flags
        wasChildMethodCalled = false;

        getLog().fine("Connecting to resource");
        resourceListener = new TCKResourceListenerImpl();
        utils().getResourceInterface().setResourceListener(resourceListener);

        super.setUp();

    }

    private class TCKResourceListenerImpl extends BaseTCKResourceListener {
        public synchronized void onSbbMessage(TCKSbbMessage sbbMessage, TCKActivityID calledActivity) throws RemoteException {
            String message = (String)sbbMessage.getMessage();
            if(Test2508Constants.PARENT_CALLED_CHILD.equals(message)) {
                getLog().info("Received indication that the child's local interface method was invoked, and that the "+
                        "TransactionRolledbackLocalException was caught"); 
                wasChildMethodCalled = true;
            } else if(Test2508Constants.SBB_EXCEPTION_THROWN_CHILD.equals(message)) {
                futureResult.setFailed(2508,"The sbbExceptionThrown() method was invoked on an SBB as a result of one of its "+
                        "local interface methods throwing a RuntimeException");
            } else if(Test2508Constants.SBB_ROLLED_BACK_CHILD.equals(message)) {
                futureResult.setFailed(2509,"The sbbRolledBack() method was invoked on an SBB after of one of its "+
                        "local interface methods threw a RuntimeException");
            } else if(Test2508Constants.SBB_EXCEPTION_THROWN_PARENT.equals(message)) {
                futureResult.setFailed(2508,"The sbbExceptionThrown() method was invoked on a calling SBB as a result of it calling "+
                        "a local interface method on a child SBB, and that method throwing a RuntimeException");
            } else if(Test2508Constants.SBB_ROLLED_BACK_PARENT.equals(message)) {
                if(wasChildMethodCalled) {
                    getLog().info("The sbbRolledBack() method was invoked on the parent SBB as expected.");
                    futureResult.setPassed();
                } else {
                    futureResult.setError("The sbbRolledBack() method was invoked on the parent SBB as expected, but the test never "+
                            "received an indication that the local interface method's RuntimeException was processed. "+
                            "(The test never received the expected message indicating that the TransactionRolledbackLocalException was caught)");
                }
            } else futureResult.setError("Unexpected message received from SBB: "+message);
        }

        public void onException(Exception e) throws RemoteException {
            getLog().warning("Received exception from SBB");
            getLog().warning(e);
            futureResult.setError(e);
        }
    }

    private boolean wasChildMethodCalled;

    private TCKResourceListener resourceListener;
    private FutureResult futureResult;

}
