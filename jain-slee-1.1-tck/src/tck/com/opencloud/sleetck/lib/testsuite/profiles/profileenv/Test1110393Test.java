/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.profiles.profileenv;

import com.opencloud.sleetck.lib.AbstractSleeTCKTest;
import com.opencloud.sleetck.lib.TCKTestResult;

public class Test1110393Test extends AbstractSleeTCKTest {

    private static final String CUSTOM_TYPE_PATH_PARAM= "customTypeDUPath";
    private static final String NO_VALUE_PATH_PARAM= "noValueElementDUPath";

    /**
     * This test trys to deploy some profile specs that violate certain restrictions
     * in the component environment section of their DDs.
     */
    public TCKTestResult run() throws Exception {

        //1110393: ...The permissible Java types are: String, Character, Integer, Boolean, Double,
        //Byte, Short, Long, and Float.
        try {
            setupService(CUSTOM_TYPE_PATH_PARAM);
            return TCKTestResult.failed(1110393, "Defining a profile component environment entry with a custom type" +
                    "(i.e. different to java.lang.String or the primitive type wrapper classes) is actually forbidden but worked fine.");
        }
        catch (Exception e) {
            getLog().fine("Caught deployment exception as expected: "+e);
        }

        //1110685: ... must set the values of those environment entries for which no value has been
        //specified. The description elements provided by the Profile Specification Developer help
        //the Profile Specification Deployer with this task.
        try {
            setupService(NO_VALUE_PATH_PARAM);
            return TCKTestResult.failed(1110685, "Deployment succeeded though environment entry had no value specified.");
        }
        catch (Exception e) {
            getLog().fine("Caught deployment exception as expected: "+e);
        }

        return TCKTestResult.passed();
    }

    public void setUp() throws Exception {

    }

    public void tearDown() throws Exception {

        super.tearDown();
    }

}
