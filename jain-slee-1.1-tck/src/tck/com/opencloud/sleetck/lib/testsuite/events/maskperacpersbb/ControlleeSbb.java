/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.events.maskperacpersbb;

import com.opencloud.sleetck.lib.sbbutils.BaseTCKSbb;
import com.opencloud.sleetck.lib.sbbutils.events.TCKSbbEvent;

import javax.slee.*;

public abstract class ControlleeSbb extends BaseTCKSbb {

    public void sbbCreate() throws CreateException {
        setReceivedFooEvent( false );
        setReceivedBarEvent( false );
    }

    public void installController( Controller controller ){
        setController( controller );
    }

    public void onFooEvent( TCKSbbEvent event, ActivityContextInterface ac ){
        setReceivedFooEvent( true );
        Controller controller = (Controller) getController();
        controller.eventReceivedBy( (Controllee)getSbbContext().getSbbLocalObject() );
    }

    public void onBarEvent( TCKSbbEvent event, ActivityContextInterface ac ){
        setReceivedBarEvent( true );
        Controller controller = (Controller) getController();
        controller.eventReceivedBy( (Controllee)getSbbContext().getSbbLocalObject() );
    }

    public void initialiseMaskFor( javax.slee.ActivityContextInterface aci ) throws UnrecognizedEventException, NotAttachedException{
        String[] events = new String[1];
        events[0] = "FooEvent";
        getSbbContext().maskEvent(events, aci);
    }


    public boolean hasReceivedFooEvent(){
        return getReceivedFooEvent();
    }

    public boolean hasReceivedBarEvent(){
        return getReceivedBarEvent();
    }

    public abstract boolean getReceivedFooEvent();
    public abstract void setReceivedFooEvent( boolean received );

    public abstract boolean getReceivedBarEvent();
    public abstract void setReceivedBarEvent( boolean received );

    public abstract SbbLocalObject getController();
    public abstract void setController( SbbLocalObject controller );

}
