/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.transactions;

import com.opencloud.sleetck.lib.TCKTestErrorException;
import com.opencloud.sleetck.lib.resource.events.TCKResourceEventX;
import com.opencloud.sleetck.lib.sbbutils.BaseTCKSbb;
import com.opencloud.sleetck.lib.sbbutils.TCKSbbUtils;

import javax.slee.ActivityContextInterface;
import javax.slee.ChildRelation;
import javax.slee.TransactionRolledbackLocalException;
import javax.slee.facilities.Level;

public abstract class Test2508Sbb extends BaseTCKSbb {

    public void sbbRolledBack(javax.slee.RolledBackContext context) {
        createTraceSafe(Level.INFO,"Test2508Sbb:sbbRolledBack()");
        sendSbbMessageSafe(Test2508Constants.SBB_ROLLED_BACK_PARENT);
    }

    public void sbbExceptionThrown(Exception exception, Object event, ActivityContextInterface aci) {
        createTraceSafe(Level.INFO,"Test2508Sbb:sbbRolledBack().exception:"+exception);
        if (exception instanceof java.lang.RuntimeException && exception.getMessage().equals(Test2508ChildSbb.EXCEPTION_MESSAGE)) {
            createTraceSafe(Level.INFO, "sbbExceptionThrown was invoked on a parent SBB with the "+
                        "RuntimeException thrown from a local interface method invoked on a child SBB");
            sendSbbMessageSafe(Test2508Constants.SBB_EXCEPTION_THROWN_PARENT);
        } else {
            TCKSbbUtils.handleException(new TCKTestErrorException("sbbExceptionThrown() invoked for an unexpected exception. event="+event+";aci="+aci,exception));
        }
    }

    // Initial event
    public void onTCKResourceEventX1(TCKResourceEventX ev, ActivityContextInterface aci) {
        createTraceSafe(Level.INFO,"Test2508Sbb:onTCKResourceEventX1()");

        Test2508ChildSbbLocalObject child = null;
        try {
            createTraceSafe(Level.FINE,"Test2508Sbb:onTCKResourceEventX1():creating child relation");
            child = (Test2508ChildSbbLocalObject) getChildRelation().create();
        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
            return;
        }

        try {
            createTraceSafe(Level.FINE,"Test2508Sbb:onTCKResourceEventX1():calling local interface method throwRuntimeException() on child");
            child.throwRuntimeException();
            TCKSbbUtils.handleException(new TCKTestErrorException("An invocation of an SBB local interface method on a child SBB "+
                    "failed to throw the expected TransactionRolledbackLocalException"));
            return;
        } catch (TransactionRolledbackLocalException e) {
            createTraceSafe(Level.INFO,"Caught expected TransactionRolledbackLocalException from invocation of child SBB local interface method.");
            sendSbbMessageSafe(Test2508Constants.PARENT_CALLED_CHILD);
        }

    }

    private void sendSbbMessageSafe(String message) {
        try {
            TCKSbbUtils.getResourceInterface().sendSbbMessage(message);
        } catch (Exception ex) {
            TCKSbbUtils.handleException(ex);
        }
    }

    public abstract ChildRelation getChildRelation();

}
