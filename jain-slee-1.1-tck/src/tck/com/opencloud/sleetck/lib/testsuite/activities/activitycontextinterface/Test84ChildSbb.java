/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.activities.activitycontextinterface;

import javax.slee.ActivityEndEvent;
import javax.slee.ActivityContextInterface;
import javax.slee.SbbContext;
import javax.slee.ChildRelation;

import com.opencloud.sleetck.lib.sbbutils.BaseTCKSbb;
import com.opencloud.sleetck.lib.sbbutils.TCKSbbUtils;
import com.opencloud.sleetck.lib.sbbutils.TCKSbbConstants;
import com.opencloud.sleetck.lib.resource.events.TCKResourceEventX;
import com.opencloud.sleetck.lib.resource.TCKActivityID;
import com.opencloud.sleetck.lib.resource.sbbapi.TCKResourceSbbInterface;
import com.opencloud.sleetck.lib.resource.sbbapi.TCKActivityContextInterfaceFactory;

import java.util.HashMap;

public abstract class Test84ChildSbb extends BaseTCKSbb {

    public void onTest84Event(Test84Event event, ActivityContextInterface aci) {
        try {
            
            if (getChildNumber() == 0) { // Child with default priority - gets event last.
                HashMap map = new HashMap();
                if (asSbbActivityContextInterface(aci).getValue() == 42) {
                    map.put("Result", new Boolean(true));
                    map.put("Message", "Ok");
                } else {
                    map.put("Result", new Boolean(false));
                    map.put("Message", "The 'value' attribute was not shared between SBB entities of the same SBB component.");
                }
                TCKSbbUtils.getResourceInterface().sendSbbMessage(map);
            } else { // Child with high priority - gets event first
                asSbbActivityContextInterface(aci).setValue(42);
            }
        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }
    }
    
    public void setChildNumber(int child) {
        setChild(child);
    }

    public int getChildNumber() {
        return getChild();
    }

    public abstract void setChild(int child);
    public abstract int getChild();
        
    public abstract Test84ChildSbbActivityContextInterface asSbbActivityContextInterface(ActivityContextInterface aci);

}
