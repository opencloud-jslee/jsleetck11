/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.profiles.querystatic;

import java.util.Collection;
import java.util.Hashtable;

import javax.slee.profile.ProfileTable;

public interface Test1110417_12ProfileTable extends ProfileTable {
    public Collection queryRangeMatchMap(Hashtable from_param, Hashtable to_param);
    public Collection queryRangeMatchMyInt(MyInteger from_param, MyInteger to_param);
}
