/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.facilities.servicelookupfacility;

import java.util.HashMap;

import javax.slee.resource.ConfigProperties;
import javax.slee.resource.ResourceAdaptorID;

import com.opencloud.sleetck.lib.AbstractSleeTCKTest;
import com.opencloud.sleetck.lib.TCKTestFailureException;
import com.opencloud.sleetck.lib.TCKTestResult;
import com.opencloud.sleetck.lib.infra.SleeTCKComponentConstants;
import com.opencloud.sleetck.lib.profileutils.BaseMessageAdapter;
import com.opencloud.sleetck.lib.rautils.MessageHandlerRegistry;
import com.opencloud.sleetck.lib.rautils.RMIObjectChannel;
import com.opencloud.sleetck.lib.testutils.FutureResult;
import com.opencloud.sleetck.lib.testutils.jmx.ResourceManagementMBeanProxy;

/**
 * AssertionID(1113671): Test This getService method returns the Service component 
 * identifier of the Service described by the ReceivableService object.
 * 
 * AssertionID(1113672): Test This getReceivableEvents method returns an array of 
 * javax.slee.resource.ReceivableService.Receivable-Event objects.
 * 
 * AssertionID(1113673): Test Each ReceivableEvent object identifies an event type 
 * that one or more SBBs in the Service has an event handler method for.
 * 
 * AssertionID(1113678): Test The equals implementation class of a ReceivableService 
 * object must override the Object.equals method.
 * 
 * AssertionID(1113679): Test Two ReceivableService objects must be considered equals 
 * if the Service component identifiers returned by their respective getService methods
 *  are equal.
 * 
 * AssertionID(1113680): Test A ReceivableService.ReceivableEvent object contains 
 * information about a single event type that can be received by a Service.
 * 
 * AssertionID(1113683): Test This getEventType method returns the event type identifier 
 * of the event type described by the ReceivableService.ReceivableEvent object.
 * 
 * AssertionID(1113684): Test This getResourceOption method returns the resource option 
 * specified in the deployment descriptor of the SBB receiving the event type. If the 
 * SBB did not specify a resource option then this method returns null.
 * 
 * AssertionID(1113685): Test This isInitialEvent method returns true if and only if 
 * the event type is received by the root SBB of the Service and the root SBB has flagged 
 * the event type as an initial event type in its deployment descriptor.
 * 
 * AssertionID(1113676): Test The returned array will only include information on the event 
 * types that the Resource Adaptor may fire (as determined by the resource adaptor types it 
 * implements) unless event type checking has been disabled for the Resource Adaptor 
 * (see Section 15.10).
 **/

public class Test1113671Test extends AbstractSleeTCKTest {
    private static final String RA_NAME = "Test1113671RA";
    private static final String RA_ENTITY_NAME = RA_NAME+ "_Entity";
    private static final String RA_VENDOR = SleeTCKComponentConstants.TCK_VENDOR;
    private static final String RA_VERSION = "1.1";

    private static final String RESOURCE_DU_PATH_PARAM = "RADUPath";
    private static final String SERVICE_DU_PATH_PARAM = "serviceDUPath";
    private static final String RESOURCE_LINK_NAME_PARAM = "resourceLinkName";

    public TCKTestResult run() throws Exception {

        futureResult = new FutureResult(getLog());

        resourceMBean = utils().getResourceManagementMBeanProxy();

        // Create the RA Entity.
        ResourceAdaptorID raID = new ResourceAdaptorID(RA_NAME, RA_VENDOR, RA_VERSION);
        ConfigProperties configProperties = new ConfigProperties();
        resourceMBean.createResourceAdaptorEntity(raID, RA_ENTITY_NAME, configProperties);

        // Activate the RA Entity
        resourceMBean.activateResourceAdaptorEntity(RA_ENTITY_NAME);
        
        // Bind RA link name
        raLinkName = utils().getTestParams().getProperty(RESOURCE_LINK_NAME_PARAM);
        
        if (raLinkName != null) {
            getLog().info("Binding RA Entity '" + RA_ENTITY_NAME + "' to link name '" + raLinkName + "'");
            resourceMBean.bindLinkName(RA_ENTITY_NAME, raLinkName);
        }

        // Deploy and active the service
        setupService(SERVICE_DU_PATH_PARAM);
        
        int assertionID = 1;
        if (getTestName().equals("IgnoreCheckFalse")) {
            assertionID = Test1113671MessageListener.CHECK_IGNORE_RA_TYPE_EVENT_TYPE_FALSE;
        }            
        else if (getTestName().equals("IgnoreCheckTrue")) {
            assertionID = Test1113671MessageListener.CHECK_IGNORE_RA_TYPE_EVENT_TYPE_TRUE;
        }
        else {
            getLog().error("Unexpected test name encountered during test run: " + getTestName());
        }
        
        HashMap map = new HashMap();
        map.put("Type", new Integer(assertionID));

        //send message to RA entity which starts doing the TXN tests.
        out.sendMessage(map);

        return futureResult.waitForResult(utils().getTestTimeout());
    }

    public void tearDown() throws Exception {
        try {
            in.clearQueue();
            utils().deactivateAllServices();
            resourceMBean.unbindLinkName(raLinkName);
            utils().removeRAEntities();
        } finally {
            super.tearDown();
        }

    }

    protected void setPassed(int assertionID, String msg) {
        getLog().fine(assertionID+": "+msg);
        if (futureResult!=null && !futureResult.isSet())
            futureResult.setPassed();
    }

    protected void setFailed(int assertionID, String msg, Exception e) {
        if (e==null) {
            getLog().fine("FAILURE: "+assertionID+":"+msg);
            if (futureResult!=null && !futureResult.isSet())
                futureResult.setFailed(assertionID, msg);
        }
        else {
            getLog().fine("FAILURE: "+assertionID+":"+msg);
            getLog().fine(e);
            if (futureResult!=null && !futureResult.isSet())
                futureResult.setFailed(new TCKTestFailureException(assertionID, msg, e));
        }
    }

    protected void setError(String msg, Exception e) {
        if (e==null) {
            getLog().warning(msg);
            if (futureResult!=null && !futureResult.isSet())
                futureResult.setError(msg);
        }
        else {
            getLog().warning(msg);
            getLog().warning(e);
            if (futureResult!=null && !futureResult.isSet())
                futureResult.setError(msg, e);
        }
    }


    public void setUp() throws Exception {
        setupService(RESOURCE_DU_PATH_PARAM);

        in = utils().getRMIObjectChannel();
        in.setMessageHandler(new BaseMessageAdapter(getLog()) {
            public void onSetPassed(int assertionID, String msg) { setPassed(assertionID, msg); }
            public void onSetFailed(int assertionID, String msg, Exception e) { setFailed(assertionID, msg, e); }
            public void onLogCall(String msg) { getLog().fine(msg); }
            public void onSetError(String msg, Exception e) { setError(msg, e); }
        });

        out = utils().getMessageHandlerRegistry();

    }
    
    private String getTestName() {
        String testName = "testName";
        return utils().getTestParams().getProperty(testName);
    }

    private MessageHandlerRegistry out;
    private RMIObjectChannel in;
    private FutureResult futureResult;
    private ResourceManagementMBeanProxy resourceMBean;
    private String raLinkName; 
}
