/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.management.ServiceManagementMBean;

import com.opencloud.sleetck.lib.AbstractSleeTCKTest;
import com.opencloud.sleetck.lib.TCKTestErrorException;
import com.opencloud.sleetck.lib.TCKTestResult;
import com.opencloud.sleetck.lib.infra.SleeTCKComponentConstants;
import com.opencloud.sleetck.lib.testutils.ComponentIDLookup;
import com.opencloud.sleetck.lib.testutils.jmx.ServiceUsageMBeanProxy;

import javax.management.ObjectName;
import javax.slee.ServiceID;
import javax.slee.management.ServiceUsageMBean;

/**
 * Checks that a ServiceUsageMBean returned by getServiceUsageMBean()
 * extends the ServiceUsageMBean interface, and that it represents
 * the correct service.
 */
public class Test3971Test extends AbstractSleeTCKTest {

    public static final String SERVICE_NAME = "GenericUsageService";
    public static final int TEST_ID = 3971;

    public TCKTestResult run() throws Exception {
        ObjectName serviceUsageMBeanName = null;
        ServiceUsageMBeanProxy serviceUsageMBeanProxy = null;
        try {
            ComponentIDLookup idLookup = new ComponentIDLookup(utils());
            ServiceID serviceID = idLookup.lookupServiceID(SERVICE_NAME,SleeTCKComponentConstants.TCK_VENDOR,null);
            if(serviceID == null) throw new TCKTestErrorException("ServiceID not found for "+SERVICE_NAME);
            serviceUsageMBeanName = utils().getServiceManagementMBeanProxy().getServiceUsageMBean(serviceID);
            serviceUsageMBeanProxy = utils().getMBeanProxyFactory().createServiceUsageMBeanProxy(serviceUsageMBeanName);

            // check the interface type
            String expectedMBeanClassName = ServiceUsageMBean.class.getName();
            utils().getLog().info("Checking that MBean "+serviceUsageMBeanName+
                    " returned by getServiceUsageMBean() is a "+expectedMBeanClassName);
            if(!utils().getMBeanFacade().isInstanceOf(serviceUsageMBeanName,expectedMBeanClassName))
                return TCKTestResult.failed(TEST_ID, "MBean "+serviceUsageMBeanName+
                        " returned by getServiceUsageMBean() is not an instance of "+expectedMBeanClassName);

            // check that it represents the correct service
            if(!serviceUsageMBeanProxy.getService().equals(serviceID))
                return TCKTestResult.failed(TEST_ID, "MBean "+serviceUsageMBeanName+
                        " returned by getServiceUsageMBean() does not represent the service passed to getServiceUsageMBean()");

        } finally {
            // close the service usage MBean
            if(serviceUsageMBeanProxy != null) {
                serviceUsageMBeanProxy.close();
            }
        }

        return TCKTestResult.passed();
    }


}
