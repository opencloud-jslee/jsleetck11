/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.runtime.security;

import java.util.PropertyPermission;

import com.opencloud.sleetck.lib.TCKTestFailureException;
import com.opencloud.sleetck.lib.TCKTestResult;
import com.opencloud.sleetck.lib.sbbutils.TCKSbbUtils;


public abstract class Test1112015ProfileAbstract extends BaseSecurityProfile implements Test1112015ProfileCMP, Test1112015ProfileManagement {
    private final static int TEST_ID = 1112015;
    
    public TCKTestResult manage() {
        try {
            trace.info("Running profile.manage()");
            // Test 1112016: This has been added in the DU descriptor.
            checkPermissions(TEST_ID);
            checkGrant(new PropertyPermission("tcktest.profile", "write"), 1112016);
        } catch (TCKTestFailureException e) {
            return TCKTestResult.failed(e);
        } catch (Exception e) {
            trace.warning("Got exception:", e);
            TCKSbbUtils.handleException(e);
        }
        return TCKTestResult.passed();
    }
}
