/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.resource.endpoint;

import com.opencloud.sleetck.lib.TCKTestErrorException;
import com.opencloud.sleetck.lib.TCKTestResult;
import com.opencloud.sleetck.lib.testsuite.resource.BaseResourceTest;
import com.opencloud.sleetck.lib.testsuite.resource.RAMethods;

/**
 * Test that endActivity() on an activity which is already ending doesn't throw
 * an exception, and doesn't change the state of the AC.
 * <p>
 * Test assertion: 1115278
 */
public class Test1115278Test extends BaseResourceTest {

    private final static int ASSERTION_ID = 1115278;

    public TCKTestResult run() throws Exception {
        int sequenceID = nextMessageID();

        MultiResponseListener listener = new MultiResponseListener(sequenceID);
        listener.addExpectedResult("result-ra1");
        listener.addExpectedResult("result-ra2");

        listener.addExpectedResult("result-sbb1");
        listener.addExpectedResult("result-sbb2");

        sendMessage(RAMethods.endActivity, new Integer(ASSERTION_ID), listener, sequenceID);

        Object result1 = listener.getResult("result-ra1");
        if (result1 == null)
            throw new TCKTestErrorException("Test timed out while waiting for response from test RA");
        checkResult(result1, ASSERTION_ID);

        Object result2 = listener.getResult("result-ra2");
        if (result2 == null)
            throw new TCKTestErrorException("Test timed out while waiting for response from test RA (from sbb event handler call)");
        checkResult(result2, ASSERTION_ID);

        Object result3 = listener.getResult("result-sbb1");
        if (result3 == null)
            throw new TCKTestErrorException("Test timed out while waiting for response from test sbb");
        checkResult(result3, ASSERTION_ID, "Activity Context was not in ending state after call to endActivity()");

        return TCKTestResult.passed();
    }
}
