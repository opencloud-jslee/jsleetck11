/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.sbb.abstractclass;

import javax.slee.facilities.Level;
import javax.slee.*;

import com.opencloud.sleetck.lib.sbbutils.BaseTCKSbb;
import com.opencloud.sleetck.lib.sbbutils.TCKSbbUtils;
import com.opencloud.sleetck.lib.sbbutils.TCKSbbConstants;
import com.opencloud.sleetck.lib.resource.events.TCKResourceEventX;
import com.opencloud.sleetck.lib.resource.sbbapi.TCKResourceSbbInterface;

import java.util.HashMap;
import java.util.Iterator;

public abstract class Test432Sbb extends BaseTCKSbb {

    // Initial event
    public void onTCKResourceEventX1(TCKResourceEventX ev, ActivityContextInterface aci) {

        try {
            HashMap map = new HashMap();
            ChildRelation childRelation = getChildRelation();
            SbbLocalObject childSbbLocalObject = childRelation.create();
            childSbbLocalObject.remove();
            
            boolean passed = false;
            try {
                childSbbLocalObject.getSbbPriority();
            } catch (javax.slee.TransactionRolledbackLocalException e) {
                passed = true;
            }

            if (passed == false) {
                map.put("Result", new Boolean(false));
                map.put("Message", "Didn't receive TransactionRolledbackLocalException when accessing invalid SbbLocalObject.");
                TCKSbbUtils.getResourceInterface().sendSbbMessage(map);
                return;
            }
                
            if (getSbbContext().getRollbackOnly() == true) {
                map.put("Result", new Boolean(true));
                map.put("Message", "Ok");
            } else {
                map.put("Result", new Boolean(false));
                map.put("Message", "Transaction is not marked for rollback.");

            }                

            TCKSbbUtils.getResourceInterface().sendSbbMessage(map);


        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }

    }

    // Override sbbRolledBack to write a trace message rather than throw an Exception, as we expect to receive the call
    public void sbbRolledBack(RolledBackContext context) {
        createTraceSafe(Level.INFO,"Received expected sbbRolledBack() call");
    }
    
    // ChildRelation method for the Child SBB.
    public abstract ChildRelation getChildRelation();

}
