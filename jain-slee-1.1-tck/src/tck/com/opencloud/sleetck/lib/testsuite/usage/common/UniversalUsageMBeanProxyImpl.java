/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.usage.common;

import com.opencloud.sleetck.lib.TCKTestErrorException;
import com.opencloud.sleetck.lib.testutils.jmx.impl.SbbUsageMBeanProxyImpl;
import com.opencloud.sleetck.lib.testutils.jmx.MBeanFacade;

import javax.slee.management.ManagementException;
import javax.slee.usage.SampleStatistics;
import javax.management.*;

/**
 * Implementation of UniversalUsageMBeanProxy
 */
public class UniversalUsageMBeanProxyImpl extends SbbUsageMBeanProxyImpl implements UniversalUsageMBeanProxy {

    public UniversalUsageMBeanProxyImpl(ObjectName objName, MBeanFacade facade) {
        super(objName, facade);
        this.objName = objName;
        this.facade = facade;
    }

    public long getCounterParameter(String parameterName, boolean reset) throws ManagementException, TCKTestErrorException {
        try {
           Long rValue = (Long)facade.invoke(objName,parameterNameToGetter(parameterName),new Object[]{new Boolean(reset)},new String[]{"boolean"});
           return rValue.longValue();
        } catch(InstanceNotFoundException ie) {
           throw new TCKTestErrorException("Caught Exception while calling MBeanServer.invoke()",ie);
        } catch(ReflectionException re) {
           throw new TCKTestErrorException("Caught Exception while calling MBeanServer.invoke()",re);
        } catch(RuntimeMBeanException rmbe) {
            throw rmbe.getTargetException();
        } catch(MBeanException e) {
           Exception enclosed = e.getTargetException();
           if(enclosed instanceof ManagementException) throw (ManagementException)enclosed;
           if(enclosed instanceof RuntimeException) throw (RuntimeException)enclosed;
           throw new TCKTestErrorException("Caught undeclared exception",enclosed);
        }
    }

    public SampleStatistics getSampleParameter(String parameterName, boolean reset) throws ManagementException, TCKTestErrorException {
        try {
           return (SampleStatistics)facade.invoke(objName,parameterNameToGetter(parameterName),new Object[]{new Boolean(reset)},new String[]{"boolean"});
        } catch(InstanceNotFoundException ie) {
           throw new TCKTestErrorException("Caught Exception while calling MBeanServer.invoke()",ie);
        } catch(ReflectionException re) {
           throw new TCKTestErrorException("Caught Exception while calling MBeanServer.invoke()",re);
        } catch(RuntimeMBeanException rmbe) {
            throw rmbe.getTargetException();
        } catch(MBeanException e) {
           Exception enclosed = e.getTargetException();
           if(enclosed instanceof ManagementException) throw (ManagementException)enclosed;
           if(enclosed instanceof RuntimeException) throw (RuntimeException)enclosed;
           throw new TCKTestErrorException("Caught undeclared exception",enclosed);
        }
    }

    /**
     * @return the name of the getter method for the given parameter name,
     *  e.g. returns 'getFoo' for 'foo'.
     */
    private String parameterNameToGetter(String parameterName) {
        return "get"+Character.toUpperCase(parameterName.charAt(0)) + parameterName.substring(1);
    }

    private ObjectName objName;
    private MBeanFacade facade;

}
