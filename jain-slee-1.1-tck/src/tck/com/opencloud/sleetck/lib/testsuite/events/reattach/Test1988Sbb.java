/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.events.reattach;

import com.opencloud.sleetck.lib.resource.events.TCKResourceEventX;
import com.opencloud.sleetck.lib.sbbutils.BaseTCKSbb;
import com.opencloud.sleetck.lib.sbbutils.TCKSbbUtils;

import javax.slee.ActivityContextInterface;
import javax.slee.Address;
import javax.slee.ChildRelation;
import javax.slee.SbbLocalObject;
import java.util.HashMap;

public abstract class Test1988Sbb extends BaseTCKSbb {

    // Initial event method.
    public void onTCKResourceEventX1(TCKResourceEventX event, ActivityContextInterface aci) {
        try {
            aci.attach(getChildRelation().create());

            fireTest1988Event(new Test1988Event(), aci, null);
            fireTest1988SecondEvent(new Test1988SecondEvent(), aci, null);
        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }
    }

    public void onTest1988Event(Test1988Event event, ActivityContextInterface aci) {
        try {
            SbbLocalObject child = (SbbLocalObject) getChildRelation().iterator().next();
            aci.detach(child);
        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }
    }


    public void onTest1988SecondEvent(Test1988SecondEvent event, ActivityContextInterface aci) {
        try {
            HashMap map = new HashMap();
            map.put("Result", new Boolean(true));
            map.put("Message", "Ok");
            TCKSbbUtils.getResourceInterface().sendSbbMessage(map);
        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }
    }

    public abstract void fireTest1988Event(Test1988Event event, ActivityContextInterface aci, Address address);
    public abstract void fireTest1988SecondEvent(Test1988SecondEvent event, ActivityContextInterface aci, Address address);

    public abstract ChildRelation getChildRelation();
}
