/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.transactions.isolation;

import com.opencloud.sleetck.lib.AbstractSleeTCKTest;
import com.opencloud.sleetck.lib.TCKTestResult;
import com.opencloud.sleetck.lib.SleeTCKTestUtils;
import com.opencloud.sleetck.lib.TCKTestErrorException;
import com.opencloud.sleetck.lib.testutils.QueuingResourceListener;
import com.opencloud.sleetck.lib.resource.TCKActivityID;
import com.opencloud.sleetck.lib.resource.events.TCKResourceEventX;
import com.opencloud.sleetck.lib.resource.events.TCKResourceEventY;

import java.util.Map;
import java.util.Date;
import java.rmi.RemoteException;
import java.text.DateFormat;

/**
 * Test asserttion 2002: transactional isolation.  This test aims to prove that transactional changes performed by an
 * SBB are not visible to any other concurrently executing transactions.
 * <p>Note that if the test timesout while waiting for notification of a concurrently executing transaction this means the
 * SLEE is not allowing the transactions to execute concurrently, so transactional isolation is held, so the test is
 * passed automatically.</p>
 */
public class Test2002Test extends AbstractSleeTCKTest {
    private static final String SERVICE_DU_PATH_PARAM = "serviceDUPath";
    private static final String WAIT_PERIOD_MS_PARAM = "waitPeriodMs";

    private static final String INITIAL_VALUE = "Test2002InitialValue";
    private static final String NEW_VALUE = "Test2002NewValue";
    private static final int EXPECTED_Y2_RESPONSES = 2;

    public TCKTestResult run() throws Exception {
        testResult = TCKTestResult.passed();
        activityA = utils().getResourceInterface().createActivity("Test2002ActivityA");
        activityB = utils().getResourceInterface().createActivity("Test2002ActivityB");
        expectedValue = INITIAL_VALUE;

        listener = new ResourceListenerImpl(utils());
        setResourceListener(listener);

        // Fire an X1 event to set the initial value for the test CMP field
        utils().getResourceInterface().fireEvent(TCKResourceEventX.X1, INITIAL_VALUE, activityA, null);
        // Fire an X2 event to change the CMP field value and initiate the rest of the test
        utils().getResourceInterface().fireEvent(TCKResourceEventX.X2, NEW_VALUE, activityA, null);

        synchronized (testLock) {
            testLock.wait(testTimeout * 2);
        }

        return testResult;
    }

    public void setUp() throws Exception {
        testTimeout = 
                Integer.parseInt(utils().getTestParams().getProperty(WAIT_PERIOD_MS_PARAM));
        setupService(SERVICE_DU_PATH_PARAM, true);
    }

    // Private classes
    private class ResourceListenerImpl extends QueuingResourceListener {
        public ResourceListenerImpl(SleeTCKTestUtils utils) {
            super(utils);
        }

        /**
         * Test callback method.  Determines which event handler made the callback and invokes the appropriate
         * handler method.
         * @param argument
         * @return
         * @throws Exception
         */
        public Object onSbbCall(Object argument) throws Exception {
            Map args = (Map) argument;

            String handlerName = (String) args.get(IsolationTestConstants.SBB_EVENT_HANDLER_FIELD);
            String value = (String) args.get(IsolationTestConstants.VALUE_FIELD);
            info("CALLTEST: "+handlerName + " : " + value);

            if (handlerName.equals("X1")) {
                x1Handler();
            } else if (handlerName.equals("X2")) {
                x2Handler();
            } else if (handlerName.equals("Y1")) {
                y1Handler();
            } else {
                y2Handler(value);
            }
            info("CALLTEST-exit: " + handlerName);
            return null;
        }

        private void x1Handler() {
            info("Sbb CMP value initialized");
        }

        /**
         * This callback is received after X2 updates the CMP field, while the X2 transaction is still alive
         * @throws InterruptedException
         * @throws RemoteException
         * @throws TCKTestErrorException
         */
        private void x2Handler() throws InterruptedException, RemoteException, TCKTestErrorException {
            try {
                // Fire Y2 event to check the current CMP value
                info("Firing Y2 event on activityB...");
                utils().getResourceInterface().fireEvent(TCKResourceEventY.Y2, null, activityB, null);

                // Block to give the SLEE a chance to execute the Y2 transaction concurrently
                synchronized (sbbLock) {
                    sbbLock.wait(testTimeout);
                }

                // If no Y2 responses received assume that the SLEE is not executing these transactions
                // concurrently.  Just mark the test as passed.
                if (receivedY2Responses == 0) {
                    concurrentTest = false;
                } else {
                    concurrentTest = true;
                }

                info("The SLEE is allowing concurrent execution of the SBB entities: " + concurrentTest);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        private void y1Handler() {
            info("In Y1 Handler waiting to notifyAll()");
            synchronized (sbbLock) {
                sbbLock.notifyAll();
            }
        }

        /**
         * This callback is called by the Y2 event handler to pass seen values of the CMP field back to the test.
         * <p>It will unblock the X2 event handler, check the value received against the expected value (possibly failing
         * the test) and then either mark the test as completed if the expected number of responses is reached or fire
         * a Y1 event on activityA to block until the X2 transaction has committed.</p>
         * @param value
         * @throws InterruptedException
         * @throws TCKTestErrorException
         * @throws RemoteException
         */
        private void y2Handler(String value) throws InterruptedException, TCKTestErrorException, RemoteException {

            info("In Y2 handler, attempting to notifyAll()");
            synchronized (sbbLock) {
                sbbLock.notifyAll();
            }

            if (!concurrentTest) {
                info("SLEE is not allowing concurrent execution");
                testCompleted = true;
                return;
            }

            if (!value.equals(expectedValue)) {
                setTestResult(TCKTestResult.failed(2002, "Expected value: " + expectedValue +
                                                         " actual value: " + value));
                testCompleted = true;
            }
            receivedY2Responses++;

            if (receivedY2Responses == EXPECTED_Y2_RESPONSES) {
                testCompleted = true;
            } else {
                info("Firing Y1 event on activityA...") ;
                utils().getResourceInterface().fireEvent(TCKResourceEventY.Y1, null, activityA, null);
                synchronized (sbbLock) {
                    sbbLock.wait(testTimeout);
                }
            }

        }
    }

    private void info(String message) {
        getLog().info(message);
    }

    private void setTestResult(TCKTestResult result) {
        this.testResult = result;
    }

    private TCKActivityID activityA;
    private TCKActivityID activityB;
    private QueuingResourceListener listener;
    private Object testLock = new Object();
    private Object sbbLock = new Object();
    private int receivedY2Responses;
    private String expectedValue;
    private boolean testCompleted;
    private TCKTestResult testResult = null;
    private boolean concurrentTest;
    private long testTimeout;
}
