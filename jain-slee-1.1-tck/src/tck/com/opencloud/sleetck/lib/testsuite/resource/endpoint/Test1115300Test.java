/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.resource.endpoint;

import java.util.HashMap;

import com.opencloud.sleetck.lib.TCKTestResult;
import com.opencloud.sleetck.lib.testsuite.resource.BaseResourceTest;
import com.opencloud.sleetck.lib.testsuite.resource.RAMethods;

/**
 * Test to ensure that fireEvent() is throwing IllegalEventException correctly.
 * <p>
 * Test assertion: 1115300
 */
public class Test1115300Test extends BaseResourceTest {

    private final static int ASSERTION_ID = 1115300;

    public TCKTestResult run() throws Exception {
        HashMap results = sendMessage(RAMethods.fireEvent, new Integer(ASSERTION_ID));

        checkResult(results, "result1", ASSERTION_ID, "fireEvent(handle, eventID, event, address, service) failed to throw an IllegalEventException when given an illegal FireableEventType");
        checkResult(results, "result2", ASSERTION_ID, "fireEvent(handle, eventID, event, address, service, flags) failed to throw an IllegalEventException when given an illegal FireableEventType");

        return TCKTestResult.passed();
    }
}
