/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.facilities.tracefacility.tracer;

import java.util.HashMap;

import javax.slee.ActivityContextInterface;
import javax.slee.facilities.TraceLevel;
import javax.slee.facilities.Tracer;

import com.opencloud.sleetck.lib.resource.events.TCKResourceEventX;
import com.opencloud.sleetck.lib.sbbutils.BaseTCKSbb;
import com.opencloud.sleetck.lib.sbbutils.TCKSbbUtils;

/*
 * AssertionID (1113127): Test The trace level of a particular tracer is equal 
 * to the trace level assigned to it by an Administrator using a TraceMBean 
 * object (see Section 14.15.1).
 * 
 * AssertionID (1113128): Test If a tracer has no assigned trace level, then 
 * the trace level of the tracer is inherited from its closest ancestor that 
 * does have a trace level assigned to it.
 * 
 * AssertionID (1113130): Test this isTraceable method determines if the trace 
 * level currently set for the tracer and notification source represented by this 
 * Tracer object notification source is sufficient to cause a trace notification 
 * to be generated if a trace message was emitted at the trace level specified 
 * by the traceLevel argument.
 * 
 * AssertionID (1113132): Test this.isTraceable(fooLevel) method equivalent 
 * to !this.getTraceLevel().ishigherLevel( fooLevel).
 * 
 * AssertionID (1113347): Test it throws a java.lang.NullPointerException 
 * - if level is null.
 * 
 */
public abstract class Test1113127Sbb extends BaseTCKSbb {

    // Initial event
    public void onTCKResourceEventX1(TCKResourceEventX event, ActivityContextInterface aci) {

        try {
            sbbTracer = getSbbContext().getTracer("com.test");
            sbbTracer.info("Received " + event + " message", null);

            doTest1113127Test();
        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }
    }

    private void sendResultToTCK(String testName, boolean result, int failedAssertionID,  String message) throws Exception {
        HashMap sbbData = new HashMap();
        sbbData.put("testname", testName);
        sbbData.put("result", result ? "pass" : "fail");
        sbbData.put("message", message);
        sbbData.put("id", new Integer(failedAssertionID));
        TCKSbbUtils.getResourceInterface().sendSbbMessage(sbbData);
    }

    private void doTest1113127Test() throws Exception {
        sbbTracer.info("Testing Assertion Number 1113127...");
        Tracer tracer = null;

        TraceLevel traceLevel = TraceLevel.FINEST;

        Tracer fooTracer = null;

        TraceLevel fooTraceLevel = TraceLevel.FINEST;

        TraceLevel infoTraceLevel = TraceLevel.INFO;

        TraceLevel finestTraceLevel = TraceLevel.FINEST;

        // 1113127
        try {
            tracer = getSbbContext().getTracer("com.test");
            traceLevel = tracer.getTraceLevel();
            if (traceLevel != TraceLevel.FINE) {
                sendResultToTCK("Test1113127Test", false, 1113127, "This getTraceLevel method didn't return "
                        + "the trace level assigned to it by an Administrator " + "using a TraceMBean object.");
                return;
            }
            sbbTracer.info("got expected trace level of the tracer: " + traceLevel, null);
        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }

        sbbTracer.info("Testing Assertion Number 1113128...");
        // 1113128
        Tracer unassignedTracer = null;
        TraceLevel unassignedTraceLevel = TraceLevel.FINEST;
        try {
            tracer = getSbbContext().getTracer("com.test");
            traceLevel = tracer.getTraceLevel();
            unassignedTracer = getSbbContext().getTracer("com.test.unassigned");
            unassignedTraceLevel = unassignedTracer.getTraceLevel();
            if (traceLevel != TraceLevel.FINE || unassignedTraceLevel != traceLevel) {
                sendResultToTCK("Test1113127Test", false, 1113128, "This getTraceLevel method didn't return "
                        + "its closest ancestor that does have a trace level " + "assigned to it.");
                return;
            }
            sbbTracer.info("got expected trace level of the tracer: " + traceLevel, null);
        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }

        sbbTracer.info("Testing Assertion Number 1113130...");
        // 1113130

        try {
            fooTracer = getSbbContext().getTracer("com.test");
            // fooTracerLevel should return TraceLevel.FINE here
            fooTraceLevel = fooTracer.getTraceLevel();
            if (fooTraceLevel != TraceLevel.FINE) {
                sendResultToTCK("Test1113127Test", false, 1113130, "This fooTraceLevel argument didn't return " + "TraceLevel.FINE");
                return;
            }

            // try to compare fooTraceLevel with TraceLevel.INFO firstly,
            // returns true.
            if (!fooTracer.isTraceable(infoTraceLevel)) {
                sendResultToTCK("Test1113127Test", false, 1113130, "This isTraceable method didn't return "
                        + "the correct value (true) due to TraceLevel.INFO has " + "higher value than TraceLevel.FINE.");
                return;
            }
            // try to compare fooTraceLevel with TraceLevel.FINEST secondly,
            // returns false.
            if (fooTracer.isTraceable(finestTraceLevel)) {
                sendResultToTCK("Test1113127Test", false, 1113130, "This isTraceable method didn't return "
                        + "the correct value (false) due to TraceLevel.INFO has "
                        + "lower value than TraceLevel.FINEST.");
                return;
            }

            sbbTracer.info("got expected value of the isTraceable method.", null);
        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }
    
        sbbTracer.info("Testing Assertion Number 1113132...");
        // 1113132
        try {
            fooTracer = getSbbContext().getTracer("com.test");
            // fooTracerLevel should return TraceLevel.FINE here
            fooTraceLevel = fooTracer.getTraceLevel();
            if (fooTraceLevel != TraceLevel.FINE) {
                sendResultToTCK("Test1113127Test", false, 1113132, "This fooTraceLevel argument didn't return " + "TraceLevel.FINE");
                return;
            }

            if (fooTracer.isTraceable(infoTraceLevel) != !fooTracer.getTraceLevel().isHigherLevel(infoTraceLevel)) {
                sendResultToTCK("Test1113127Test", false, 1113132, "This isTraceable method didn't equivalent to "
                        + "!this.getTraceLevel().ishigherLevel method");
                return;
            }
            if (fooTracer.isTraceable(finestTraceLevel) != !fooTracer.getTraceLevel().isHigherLevel(finestTraceLevel)) {
                sendResultToTCK("Test1113127Test", false, 1113132, "This isTraceable method didn't equivalent to "
                        + "!this.getTraceLevel().ishigherLevel method");
                return;
            }

            sbbTracer.info("got expected value of the isTraceable method which is equivalent to "
                    + "!this.getTraceLevel().ishigherLevel method.", null);
        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }

        sbbTracer.info("Testing Assertion Number 1113347...");
        // 1113347
        boolean passed = false;

        try {
            fooTracer = getSbbContext().getTracer("com.test");
            fooTracer.isTraceable(null);
        } catch (java.lang.NullPointerException e) {
            sbbTracer.info("got expected NullPointerException", null);
            passed = true;
        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }

        if (!passed) {
            sendResultToTCK("Test1113127Test", false, 1113347, "Tracer.isTraceable(null) should have thrown java.lang.NullPointerException.");
            return;
        }

        sendResultToTCK("Test1113127Test", true, 1113347, "This Tracer.isTraceable tests passed!");
    }


    private Tracer sbbTracer;
}
