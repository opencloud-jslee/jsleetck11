/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.usage.profiles;

import java.util.HashMap;

import javax.naming.InitialContext;
import javax.slee.ActivityContextInterface;
import javax.slee.profile.ProfileFacility;
import javax.slee.profile.ProfileTable;

import com.opencloud.sleetck.lib.resource.events.TCKResourceEventX;
import com.opencloud.sleetck.lib.sbbutils.BaseTCKSbb;
import com.opencloud.sleetck.lib.sbbutils.TCKSbbUtils;
import com.opencloud.sleetck.lib.sbbutils.events2.SbbBaseMessageConstants;

public abstract class Test1111008Sbb extends BaseTCKSbb {

    public void onTCKResourceEventX1(TCKResourceEventX event, ActivityContextInterface aci) {
        HashMap map = new HashMap();
        ProfileFacility facility;
        ProfileTable pt;
        Test1111008ProfileLocal addedProfile;

        try {
            facility = (ProfileFacility) new InitialContext().lookup(ProfileFacility.JNDI_NAME);
            pt = facility.getProfileTable(Test1111008Test.PROFILE_TABLE_NAME);
            addedProfile = (Test1111008ProfileLocal)pt.find(Test1111008Test.PROFILE_NAME);

            if (null != addedProfile) {
                // 1111008: Verify that this method exists by calling it. then check the value is updated
                long initialValue = addedProfile.getNamedParameterSet();
                addedProfile.incrementNamedParameterSet();
                long finalValue = addedProfile.getNamedParameterSet();

                if (finalValue == initialValue+1) {
                    map.put(SbbBaseMessageConstants.MSG, "Counter usage param has been correctly incremented and retrieved");
                    map.put(SbbBaseMessageConstants.TYPE, new Integer(SbbBaseMessageConstants.TYPE_SET_RESULT));
                    map.put(SbbBaseMessageConstants.RESULT, new Boolean(true));
                    TCKSbbUtils.getResourceInterface().sendSbbMessage(map);
                } else {
                    map.put(SbbBaseMessageConstants.MSG, "Counter usage param incorrect");
                    map.put(SbbBaseMessageConstants.TYPE, new Integer(SbbBaseMessageConstants.TYPE_SET_RESULT));
                    map.put(SbbBaseMessageConstants.RESULT, new Boolean(false));
                    TCKSbbUtils.getResourceInterface().sendSbbMessage(map);
                }
            } else {
                // addedProfile was not found.
                map.put(SbbBaseMessageConstants.MSG, "Could not find profile for this test.");
                map.put(SbbBaseMessageConstants.RESULT, new Boolean(false));
                TCKSbbUtils.getResourceInterface().sendSbbMessage(map);
            }

        } catch(Exception e) {
            TCKSbbUtils.handleException(e);
        }
    }
}
