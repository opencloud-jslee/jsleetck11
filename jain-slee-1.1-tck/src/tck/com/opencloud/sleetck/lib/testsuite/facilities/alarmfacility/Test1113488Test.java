/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.facilities.alarmfacility;

import java.rmi.RemoteException;
import java.util.Map;

import javax.management.Notification;
import javax.management.NotificationListener;
import javax.slee.ComponentID;
import javax.slee.SbbID;
import javax.slee.ServiceID;
import javax.slee.management.AlarmNotification;
import javax.slee.management.DeployableUnitDescriptor;
import javax.slee.management.DeployableUnitID;
import javax.slee.management.SbbNotification;

import com.opencloud.sleetck.lib.AbstractSleeTCKTest;
import com.opencloud.sleetck.lib.DescriptionKeys;
import com.opencloud.sleetck.lib.TCKTestErrorException;
import com.opencloud.sleetck.lib.TCKTestFailureException;
import com.opencloud.sleetck.lib.resource.TCKActivityID;
import com.opencloud.sleetck.lib.resource.TCKSbbMessage;
import com.opencloud.sleetck.lib.resource.events.TCKResourceEventX;
import com.opencloud.sleetck.lib.resource.testapi.TCKResourceListener;
import com.opencloud.sleetck.lib.resource.testapi.TCKResourceTestInterface;
import com.opencloud.sleetck.lib.testutils.Assert;
import com.opencloud.sleetck.lib.testutils.BaseTCKResourceListener;
import com.opencloud.sleetck.lib.testutils.FutureResult;
import com.opencloud.sleetck.lib.testutils.jmx.AlarmMBeanProxy;
import com.opencloud.sleetck.lib.testutils.jmx.DeploymentMBeanProxy;

/*  
 *  AssertionID(1113488): Test The raiseAlarm method returns 
 *  a String value which is used to identify the raised alarm.
 *    
 *  AssertionID(1113489): Test If an alarm with the same identifying 
 *  attributes (notification source, alarm type, instanceID) is already 
 *  raised then this method has no effect, and the identifier of the 
 *  existing raised alarm is returned.
 *  
 *  AssertionID(1113490): Test If no such alarm is raised, then a 
 *  new alarm is raised, the SLEE’s AlarmMBean object emits an 
 *  alarm notification, and a SLEE generated alarm identifier for the 
 *  new alarm is returned.
 *  
 */

public class Test1113488Test extends AbstractSleeTCKTest {

    /**
     * Perform the actual test.
     */
    public void run(FutureResult result) throws Exception {
        this.result = result;

        if (sbbID == null)
            throw new TCKTestErrorException("sbbID not found for " + DescriptionKeys.SERVICE_DU_PATH_PARAM);
        if (serviceID == null)
            throw new TCKTestErrorException("serviceID not found for " + DescriptionKeys.SERVICE_DU_PATH_PARAM);

        SbbNotification sbbNotification = new SbbNotification(serviceID, sbbID);
        try {
            getLog().fine(
                    "Starting to get all existing unique alarm identifiers for stateful alarms "
                            + "currently active in the SLEE that were raised by the specified notification "
                            + "source.");
            alarmIDs = alarmMBeanProxy.getAlarms(sbbNotification);
            for (int i = 0; i < alarmIDs.length; i++) {
                getLog().info("Returned Alarm IDs: " + alarmIDs[i]);
            }
        } catch (Exception e) {
            getLog().warning(e);
            result.setError("ERROR!", e);
        }

        String activityName = "Test1113488Test";
        TCKResourceTestInterface resource = utils().getResourceInterface();
        TCKActivityID activityID = resource.createActivity(activityName);

        // kickoff the first event
        getLog().info("Firing event: " + TCKResourceEventX.X1);
        resource.fireEvent(TCKResourceEventX.X1, testName, activityID, null);

        // kickoff the second event
        getLog().info("Firing event: " + TCKResourceEventX.X2);
        resource.fireEvent(TCKResourceEventX.X2, testName, activityID, null);
    }

    /**
     * Do all the pre-run configuration of the test.
     */

    public void setUp() throws Exception {

        getLog().fine("Connecting to resource");
        resourceListener = new TCKResourceListenerImpl();
        setResourceListener(resourceListener);
        getLog().fine("Installing and activating service");

        // Install the Deployable Units
        String duPath = utils().getTestParams().getProperty(DescriptionKeys.SERVICE_DU_PATH_PARAM);
        duID = utils().install(duPath);

        // Activate the DU
        utils().activateServices(duID, true);

        alarmMBeanProxy = utils().getAlarmMBeanProxy();
        listener = new AlarmNotificationListenerImpl();
        alarmMBeanProxy.addNotificationListener(listener, null, null);

        DeploymentMBeanProxy duProxy = utils().getDeploymentMBeanProxy();
        DeployableUnitDescriptor duDesc = duProxy.getDescriptor(duID);
        ComponentID components[] = duDesc.getComponents();
        // Get the SbbID from the components.
        for (int i = 0; i < components.length; i++) {
            if (components[i] instanceof ServiceID) {
                getLog().fine("Setting serviceID value.");
                serviceID = (ServiceID) components[i];
                continue;
            }

            if (components[i] instanceof SbbID) {
                getLog().fine("Setting sbbID value.");
                sbbID = (SbbID) components[i];
                continue;
            }
        }

    }

    /**
     * Clean up after the test.
     */

    public void tearDown() throws Exception {

        String[] listActiveAlarms = alarmMBeanProxy.getAlarms();
        if (alarmMBeanProxy.getAlarms().length > 0)
            for (int i = 0; i < listActiveAlarms.length; i++)
                alarmMBeanProxy.clearAlarm(listActiveAlarms[i]);

        if (null != alarmMBeanProxy)
            alarmMBeanProxy.removeNotificationListener(listener);

        // cleanup
        super.tearDown();
    }

    private class TCKResourceListenerImpl extends BaseTCKResourceListener {
        // TCKResourceListener methods

        public synchronized void onSbbMessage(TCKSbbMessage message, TCKActivityID calledActivity)
                throws RemoteException {

            Map sbbData = (Map) message.getMessage();
            String sbbTestName = "Test1113488Test";
            String sbbTestResult = (String) sbbData.get("result");
            String sbbTestMessage = (String) sbbData.get("message");
            int assertionID = ((Integer) sbbData.get("id")).intValue();
            getLog().info(
                    "Received message from SBB: testname=" + sbbTestName + ", result=" + sbbTestResult + ", message="
                            + sbbTestMessage + ", id=" + assertionID);
            try {
                Assert.assertEquals(assertionID, "Test " + sbbTestName + " failed.", "pass", sbbTestResult);
                result.setPassed();
            } catch (TCKTestFailureException ex) {
                result.setFailed(assertionID, sbbTestMessage);
            }
        }

        public void onException(Exception exception) throws RemoteException {
            getLog().warning("Received Exception from SBB or resource:");
            getLog().warning(exception);
            result.setError(exception);
        }
    }

    public class AlarmNotificationListenerImpl implements NotificationListener {
        public final void handleNotification(Notification notification, Object handback) {

            if (notification instanceof AlarmNotification) {
                AlarmNotification alarmNotification = (AlarmNotification) notification;

                // 1113488
                String newAlarmID = alarmNotification.getAlarmID();
                getLog().info("New Alarm ID =" + newAlarmID);
                for (int i = 0; i < alarmIDs.length; i++) {
                    if (alarmIDs[i].equals(newAlarmID)) {
                        result.setFailed(1113488, "The raiseAlarm method returns a String value, "
                                + "but this returned alarm ID has arelady been existed in an "
                                + "array containing the alarm identifiers of all the alarms "
                                + "currently active in the SLEE!");

                    }
                }

                if (alarmNotification.getMessage().equals(Test1113488Sbb.ALARM_MESSAGE)) {
                    receivedCount++;
                    getLog().info("Received " + alarmNotification);
                } else if (alarmNotification.getMessage().equals(Test1113488Sbb.NEW_ALARM_MESSAGE)) {
                    receivedCount++;
                    getLog().info("Received " + alarmNotification);
                    if (receivedCount == expectedCount)
                        result.setPassed();
                    else
                        result.setFailed(1113488, "The raiseAlarm method returns a String value which "
                                + "is used to identify the raised alarm. There should be " + expectedCount
                                + " AlarmIDs returns here, but it only returned " + receivedCount);
                }

                return;
            }

            getLog().info("Notification received: " + notification);
            return;
        }
    }

    private TCKResourceListener resourceListener;

    private NotificationListener listener;

    private DeployableUnitID duID;

    private AlarmMBeanProxy alarmMBeanProxy;

    private FutureResult result;

    private SbbID sbbID;

    private ServiceID serviceID;

    private String[] alarmIDs;

    private int expectedCount = 2;

    private int receivedCount = 0;
    
    private String testName = "Test1113488";
}
