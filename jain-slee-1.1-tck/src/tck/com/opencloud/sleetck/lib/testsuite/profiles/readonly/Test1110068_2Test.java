/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.profiles.readonly;

import javax.management.ObjectName;

import com.opencloud.sleetck.lib.TCKTestResult;
import com.opencloud.sleetck.lib.testsuite.profiles.ProfileRAInteractionBaseTest;
import com.opencloud.sleetck.lib.testutils.ProfileUtils;
import com.opencloud.sleetck.lib.testutils.jmx.ProfileMBeanProxy;
import com.opencloud.sleetck.lib.testutils.jmx.ProfileProvisioningMBeanProxy;

/**
  * This test checks profile behaviour if the profile spec's 'profile-read-only' attribute is set
  * to 'true'. The test operations are performed via RA.
  */
public class Test1110068_2Test extends ProfileRAInteractionBaseTest {

    private static final String RA_NAME = "Test1110068_2RA";
    private static final String SPEC_NAME = "Test1110068_2Profile";

    public String getRAName() {
        return RA_NAME;
    }

    public String[] getProfileNames(String profileTableName) {
        return new String[]{ReadOnlyIsTrueTestsMessageListener.PROFILE_NAME};
    }

    public String[] getProfileTableNames() {
        return new String[]{ReadOnlyIsTrueTestsMessageListener.PROFILE_TABLE_NAME};
    }

    public String getSpecName() {
        return SPEC_NAME;
    }

    public TCKTestResult run() throws Exception {
        //Set '42' as value for profile before starting the assertion tests
        ProfileUtils profileUtils = new ProfileUtils(utils());
        ProfileProvisioningMBeanProxy profileProvisioning = profileUtils.getProfileProvisioningProxy();

        getLog().fine("Set CMP field 'value' to '42'");
        ObjectName profile = profileProvisioning.getProfile(ReadOnlyIsTrueTestsMessageListener.PROFILE_TABLE_NAME, ReadOnlyIsTrueTestsMessageListener.PROFILE_NAME);
        ProfileMBeanProxy profileProxy = utils().getMBeanProxyFactory().createProfileMBeanProxy(profile);
        profileProxy.editProfile();
        utils().getMBeanFacade().invoke(profile, "setValue", new Object[]{"42"}, new String[]{"java.lang.String"});
        getLog().fine("Called set accessor via management interface.");
        //commit and close the Profile
        profileProxy.commitProfile();
        profileProxy.closeProfile();
        getLog().fine("Commit and close profile "+ReadOnlyIsTrueTestsMessageListener.PROFILE_NAME);

        //execute test as outlined in super class
        return super.run();
    }

}
