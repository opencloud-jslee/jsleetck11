/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.profiles.lifecycle;

import java.util.HashMap;

public interface Test1110227ProfileCMP {
    public int getValue();
    public void setValue(int value);
    public int getValue2();
    public void setValue2(int value);

    public long getLongValue();
    public void setLongValue(long value);

    public String getStringValue();
    public void setStringValue(String value);

    public boolean getBoolValue();
    public void setBoolValue(boolean value);

    public HashMap getObjValue();
    public void setObjValue(HashMap value);
}
