/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.profiles.lifecycle;

import javax.management.Attribute;
import javax.management.ObjectName;
import javax.slee.profile.ProfileSpecificationID;
import javax.slee.profile.UnrecognizedProfileNameException;

import com.opencloud.sleetck.lib.AbstractSleeTCKTest;
import com.opencloud.sleetck.lib.TCKTestFailureException;
import com.opencloud.sleetck.lib.TCKTestResult;
import com.opencloud.sleetck.lib.infra.SleeTCKComponentConstants;
import com.opencloud.sleetck.lib.profileutils.BaseMessageAdapter;
import com.opencloud.sleetck.lib.rautils.RMIObjectChannel;
import com.opencloud.sleetck.lib.resource.TCKActivityID;
import com.opencloud.sleetck.lib.resource.events.TCKResourceEventX;
import com.opencloud.sleetck.lib.resource.testapi.TCKResourceTestInterface;
import com.opencloud.sleetck.lib.testutils.FutureResult;
import com.opencloud.sleetck.lib.testutils.ProfileUtils;
import com.opencloud.sleetck.lib.testutils.jmx.ProfileMBeanProxy;
import com.opencloud.sleetck.lib.testutils.jmx.ProfileProvisioningMBeanProxy;


public class Test1110305Test extends AbstractSleeTCKTest {

    private static final String DU_PATH_PARAM= "DUPath";

    private static final String SPEC_NAME = "Test1110305Profile";
    private static final String SPEC_VERSION = "1.0";

    /**
     *
     */
    public TCKTestResult run() throws Exception {

        //create a profile table
        ProfileSpecificationID specID = new ProfileSpecificationID(SPEC_NAME, SleeTCKComponentConstants.TCK_VENDOR, SPEC_VERSION);
        profileProvisioning.createProfileTable(specID, Test1110305Sbb.PROFILE_TABLE_NAME);
        getLog().fine("Added profile table "+Test1110305Sbb.PROFILE_TABLE_NAME+" based on profile spec "+SPEC_NAME);

        ObjectName profile;
        ProfileMBeanProxy profileProxy;
        TCKTestResult res;

        //add profile BUSINESS_PROFILE to the table
        addProfile(Test1110305Sbb.BUSINESS_PROFILE, Test1110305Profile.THROW_EXCEPTION);

        //add profile MANAGEMENT_PROFILE to the table
        addProfile(Test1110305Sbb.MANAGEMENT_PROFILE, Test1110305Profile.THROW_EXCEPTION);

        //add profile LIFECYCLE_PROFILE to the table
        addProfile(Test1110305Sbb.LIFECYCLE_PROFILE, Test1110305Profile.INIT);

        //Profile object transitions into DOESNT_EXIST state if Runtime-Exception
        //is thrown by business method, management method, or lifecycle callback
        //1110306: if this happens the SLEE does not invoke any further methods on the profile object where the Excepton occured
        //1110307: The associated Profile continues to exist after the failure

        //check whether failure behaviour occurs for calling business method
        getLog().fine("Cause Sbb to call a business method...");
        futureResult =  new FutureResult(getLog());
        TCKResourceTestInterface resource = utils().getResourceInterface();
        TCKActivityID activityID = resource.createActivity(getClass().getName());
        resource.fireEvent(TCKResourceEventX.X1, null, activityID, null);
        res = futureResult.waitForResult(utils().getTestTimeout());
        if (!res.isPassed())
            return res;

        //check that BUSINESS_PROFILE is still there and CMP state can be queried
        getLog().fine("Check whether profile "+Test1110305Sbb.BUSINESS_PROFILE+" still exists.");
        try {
            profile = profileProvisioning.getProfile(Test1110305Sbb.PROFILE_TABLE_NAME, Test1110305Sbb.BUSINESS_PROFILE);
            utils().getMBeanFacade().getAttribute(profile, "Value");
            getLog().fine(Test1110305Sbb.BUSINESS_PROFILE+" still exists.");
        }
        catch (UnrecognizedProfileNameException e) {
            return TCKTestResult.failed(1110307, Test1110305Sbb.BUSINESS_PROFILE+" should still exist after its previously associated profile object has been invalidated.");
        }
        catch (Exception e) {
            return TCKTestResult.error("Unexpected behaviour when looking for profile "+Test1110305Sbb.BUSINESS_PROFILE, e);
        }


        //check that appropriate behaviour occurs for calling management method
        getLog().fine("Call management method...");
        profile = profileProvisioning.getProfile(Test1110305Sbb.PROFILE_TABLE_NAME, Test1110305Sbb.MANAGEMENT_PROFILE);
        try {
            utils().getMBeanFacade().invoke(profile, "manage", new Object[0], new String[0]);
            return TCKTestResult.error("Call to management method should have caused an exception as management method throws an unchecked exception.");
        }
        catch (Exception e) {
            getLog().fine("Call to management method failed as expected."+e);
        }
        getLog().fine("Check whether profile "+Test1110305Sbb.MANAGEMENT_PROFILE+" still exists.");
        try {
            profile = profileProvisioning.getProfile(Test1110305Sbb.PROFILE_TABLE_NAME, Test1110305Sbb.MANAGEMENT_PROFILE);
            utils().getMBeanFacade().getAttribute(profile, "Value");
            getLog().fine(Test1110305Sbb.MANAGEMENT_PROFILE+" still exists.");
        }
        catch (UnrecognizedProfileNameException e) {
            return TCKTestResult.failed(1110307, Test1110305Sbb.MANAGEMENT_PROFILE+" should still exist after its previously associated profile object has been invalidated.");
        }
        catch (Exception e) {
            return TCKTestResult.error("Unexpected behaviour when looking for profile "+Test1110305Sbb.MANAGEMENT_PROFILE, e);
        }


        //check that appropriate behaviour occurs for causing call to lifecycle method profileVerify
        getLog().fine("Start TXN to modify profile "+Test1110305Sbb.LIFECYCLE_PROFILE);
        profile = profileProvisioning.getProfile(Test1110305Sbb.PROFILE_TABLE_NAME, Test1110305Sbb.LIFECYCLE_PROFILE);
        profileProxy = utils().getMBeanProxyFactory().createProfileMBeanProxy(profile);
        profileProxy.editProfile();
        getLog().fine("Set CMP field to "+Test1110305Profile.THROW_EXCEPTION);
        utils().getMBeanFacade().setAttribute(profile, new Attribute("Value", Test1110305Profile.THROW_EXCEPTION));

        getLog().fine("Cause call to lifecycle method profileVerify...");
        try {
            profileProxy.commitProfile();
            return TCKTestResult.error("Committing profile should have caused an exception due to profileVerify throwing a RuntimeException.");
        }
        catch (Exception e) {
            getLog().fine("Committing profile "+Test1110305Sbb.LIFECYCLE_PROFILE+" failed as expected: "+e);
        }

        if (profileProxy.isProfileWriteable()) {
            getLog().fine("Restore profile.");
            profileProxy.restoreProfile();
        }


        getLog().fine("Check whether profile "+Test1110305Sbb.LIFECYCLE_PROFILE+" still exists.");
        try {
            profile = profileProvisioning.getProfile(Test1110305Sbb.PROFILE_TABLE_NAME, Test1110305Sbb.LIFECYCLE_PROFILE);
            getLog().fine("Obtain CMP field value from profile.");
            utils().getMBeanFacade().getAttribute(profile, "Value");
        }
        catch (UnrecognizedProfileNameException e) {
            return TCKTestResult.failed(1110307, Test1110305Sbb.LIFECYCLE_PROFILE+" should still exist after its previously associated profile object has been invalidated.");
        }
        catch (Exception e) {
            return TCKTestResult.error("Unexpected behaviour when looking for profile "+Test1110305Sbb.LIFECYCLE_PROFILE, e);
        }

        //finally check whether any failures or errors arrived while no FutureResult was waiting to be set.
        if (exResult!=null)
            return exResult;
        return TCKTestResult.passed();
    }

    private void addProfile(String profileName, String initValue) throws Exception {
        ObjectName profile = profileProvisioning.createProfile(Test1110305Sbb.PROFILE_TABLE_NAME, profileName);
        ProfileMBeanProxy profileProxy = utils().getMBeanProxyFactory().createProfileMBeanProxy(profile);
        getLog().fine("Created profile "+profileName+" for profile table "+Test1110305Sbb.PROFILE_TABLE_NAME);
        //commit and close the Profile
        utils().getMBeanFacade().setAttribute(profile, new Attribute("Value", initValue));
        getLog().fine("Set CMP value for "+profileName+" to "+initValue);
        profileProxy.commitProfile();
        profileProxy.closeProfile();
    }

    public void setPassed(int assertionID, String msg) {
        getLog().fine(assertionID+": "+msg);
        if (futureResult!=null && !futureResult.isSet())
            futureResult.setPassed();
    }

    public void setFailed(int assertionID, String msg, Exception e) {
        if (e==null) {
            getLog().fine("FAILURE: "+assertionID+":"+msg);
            if (futureResult!=null && !futureResult.isSet())
                futureResult.setFailed(assertionID, msg);

            //in case the currently active futureResult has already been set to passed() before the failure
            //comes in, we have to provide a mechanism to still detect this failure
            setIfEmpty(TCKTestResult.failed(assertionID, msg));
        }
        else {
            getLog().fine("FAILURE: "+assertionID+":"+msg);
            getLog().fine(e);
            if (futureResult!=null && !futureResult.isSet())
                futureResult.setFailed(new TCKTestFailureException(assertionID, msg, e));

            //in case the currently active futureResult has already been set to passed() before the failure
            //comes in, we have to provide a mechanism to still detect this failure
            setIfEmpty(TCKTestResult.failed(new TCKTestFailureException(assertionID, msg, e)));
        }
    }

    public void setError(String msg, Exception e) {
        if (e==null) {
            getLog().warning(msg);
            if (futureResult!=null && !futureResult.isSet())
                futureResult.setError(msg);

            //in case the currently active futureResult has already been set to passed() before the error
            //comes in, we have to provide a mechanism to still detect this error
            setIfEmpty(TCKTestResult.error(msg));
        }
        else {
            getLog().warning(msg);
            getLog().warning(e);
            if (futureResult!=null && !futureResult.isSet())
                futureResult.setError(msg, e);

            //in case the currently active futureResult has already been set to passed() before the error
            //comes in, we have to provide a mechanism to still detect this failure
            setIfEmpty(TCKTestResult.error(msg, e));
        }
    }


    public void setUp() throws Exception {

        setupService(DU_PATH_PARAM);

        in = utils().getRMIObjectChannel();
        in.setMessageHandler(new BaseMessageAdapter(getLog()) {
            public void onSetPassed(int assertionID, String msg) { setPassed(assertionID, msg); }
            public void onSetFailed(int assertionID, String msg, Exception e) { setFailed(assertionID, msg, e); }
            public void onLogCall(String msg) { getLog().fine(msg); }
            public void onSetError(String msg, Exception e) { setError(msg, e); }

        });

        profileUtils = new ProfileUtils(utils());
        profileProvisioning = profileUtils.getProfileProvisioningProxy();
    }

    public void tearDown() throws Exception {

        try {
            profileUtils.removeProfileTable(Test1110305Sbb.PROFILE_TABLE_NAME);
        }
        catch (Exception e) {
            getLog().warning("Caught exception while trying to remove profile table:");
            getLog().warning(e);
        }

        super.tearDown();
    }

    private void setIfEmpty(TCKTestResult exResult) {
        if (this.exResult==null)
            this.exResult = exResult;
    }

    private ProfileUtils profileUtils;
    private ProfileProvisioningMBeanProxy profileProvisioning;

    private RMIObjectChannel in;
    private FutureResult futureResult;
    private TCKTestResult exResult;
}
