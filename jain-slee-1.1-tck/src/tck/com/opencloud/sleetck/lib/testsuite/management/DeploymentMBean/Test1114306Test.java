/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.management.DeploymentMBean;

import com.opencloud.sleetck.lib.AbstractSleeTCKTest;
import com.opencloud.sleetck.lib.TCKTestErrorException;
import com.opencloud.sleetck.lib.TCKTestResult;

import javax.slee.management.DeployableUnitID;
import com.opencloud.sleetck.lib.testutils.jmx.DeploymentMBeanProxy;

/*
 * Test1114306Test
 * A deployable unit cannot be installed successfully into the SLEE when
 * 1114306 - A deployable unit with the same URL is already installed in the SLEE
 * 1114307 - The deployable unit contains components of the same type with the same name, vendor, and version.
 */

public class Test1114306Test extends AbstractSleeTCKTest {

    private static final String SERVICE_DU_PATH_PARAM = "serviceDUPath";
    private static final String INVALID_SERVICE_DU_PATH_PARAM = "serviceInvalidDUPath";
    private static final int TEST_ID = 1114306;

    /**
     * Perform the actual test.
     */
    public TCKTestResult run() throws Exception {

        DeployableUnitID duID1 = null;
        DeployableUnitID duID1fail = null;
        DeployableUnitID duID2 = null;
        DeploymentMBeanProxy duProxy = null;
        String duPath = null;

        try{

            // Install the Deployable Unit.
            try {
                utils().getLog().fine("Installing the deployable unit...");
                duPath = utils().getTestParams().getProperty(SERVICE_DU_PATH_PARAM);
                String fullURL = utils().getDeploymentUnitURL(duPath);
                duProxy = utils().getDeploymentMBeanProxy();
                duID1 = duProxy.install(fullURL);
                utils().getLog().fine("Succeeded in installing the deployable unit");
            } catch (Exception e) {
                return TCKTestResult.failed(TEST_ID, "Failed to install deployable unit.");
            }

            boolean passed = false;

            try {
                duID1fail = utils().install(duPath);
            } catch (TCKTestErrorException e) {
                if (e.getEnclosedException().getClass().equals(javax.slee.management.AlreadyDeployedException.class)) {
                    utils().getLog().fine("1114306 - Same deployable unit cannot be installed");
                    passed = true;
                } else {
                    getLog().warning(e);
                    return TCKTestResult.failed(1114306, "DeploymentMBean has thrown Exception: " + e.getClass().toString());
                }
            } catch (Exception e) {
                getLog().warning(e);
                return TCKTestResult.error("ERROR installing DU", e);
            }

            if (!passed) return TCKTestResult.failed(1114306, "Succeeded in installing two components of the same type " +
                    "with the same identity simultaneously. DeployableUnitID:"+duID1);

            passed = false;
            try {
                duID2 = utils().install(utils().getTestParams().getProperty(INVALID_SERVICE_DU_PATH_PARAM));
            } catch (TCKTestErrorException e) {
                if (e.getEnclosedException().getClass().equals(javax.slee.management.AlreadyDeployedException.class)) {
                    utils().getLog().fine("1114307 - Invalid deployable unit cannot be installed");
                    passed = true;
                } else {
                    getLog().warning(e);
                    return TCKTestResult.failed(1114307, "DeploymentMBean has thrown Exception: " + e.getClass().toString());
                }
            } catch (Exception e) {
                getLog().warning(e);
                return TCKTestResult.error("ERROR installing DU", e);
            }
            if (!passed) return TCKTestResult.failed(1114307, "Succeeded in installing a DU with components of the same identity simultaneously.");

        } finally {
            utils().getLog().fine("Deactivating and uninstalling services...");
            if (duID1 != null)
                duProxy.uninstall(duID1);
            if (duID2 != null)
                duProxy.uninstall(duID2);
        }

        return TCKTestResult.passed();
    }

    // Empty setup
    public void setUp() throws Exception {
    }

    public void tearDown() throws Exception {
        utils().getLog().fine("Disconnecting from resource");
        utils().getResourceInterface().clearActivities();
        utils().getLog().fine("Deactivating and uninstalling service");
        utils().deactivateAllServices();
        utils().uninstallAll();
    }

}


