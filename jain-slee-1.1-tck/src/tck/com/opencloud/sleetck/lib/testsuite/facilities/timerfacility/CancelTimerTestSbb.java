/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.facilities.timerfacility;

import com.opencloud.sleetck.lib.sbbutils.BaseTCKSbb;
import com.opencloud.sleetck.lib.sbbutils.TCKSbbUtils;
import com.opencloud.sleetck.lib.resource.events.TCKResourceEventX;
import com.opencloud.sleetck.lib.resource.events.TCKResourceEventY;
import com.opencloud.sleetck.lib.resource.sbbapi.TCKActivity;

import javax.naming.Context;
import javax.naming.InitialContext;

import java.util.HashMap;

import javax.slee.*;
import javax.slee.facilities.*;

/**
 * Test TimerFacility functions from spec s12.1.
 */
public abstract class CancelTimerTestSbb extends BaseTCKSbb {

    // this event starts the test
    public void onTCKResourceEventX1(TCKResourceEventX event, ActivityContextInterface aci) {

        try {
            setTestName((String)event.getMessage());
            TCKSbbUtils.createTrace(getSbbID(),Level.INFO,"Received start message",null);

            TimerFacility tf = getTimerFacility();

            TimerOptions preserveAll = new TimerOptions();
            preserveAll.setPreserveMissed(TimerPreserveMissed.ALL);

            // set an infinitely repeating timer - we will cancel it after its first
            // event, and count the number of times it fires to test that it was
            // cancelled properly
            TimerID timer = tf.setTimer(aci, null, 0, 2000, 0, preserveAll);

            // save timer in CMP
            setTimer(timer);
        } catch(Exception e) {
            TCKSbbUtils.handleException(e);
        }
    }


    // this event stops the test - should be received 10s or so after test start, to
    // give timer a chance to fire.
    public void onTCKResourceEventY1(TCKResourceEventY event, ActivityContextInterface aci) {

        try {
            TCKSbbUtils.createTrace(getSbbID(),Level.INFO,"Received stop message",null);

            if (getTimerCancelled()) {
                // timer was cancelled, now see if any timer events fired after it was cancelled.
                if (getLastTimerCount() == getTimerFiredCount()) {
                    // success - no more events fired after timer cancelled
                    setResultPassed("Timer cancelled successfully");
                }
                else {
                    setResultFailed(1203, "TimerEvents received after timer cancelled!");
                }
            }
            else {
                setResultFailed(1203, "Error: cancelTimer() never called");
            }

            sendResultToTCK();

        } catch(Exception e) {
            TCKSbbUtils.handleException(e);
        }
    }


    public void onTimerEvent(TimerEvent event, ActivityContextInterface aci) {
        try {
            TCKSbbUtils.createTrace(getSbbID(),Level.INFO,"Received timer event",null);
            // increment count
            setTimerFiredCount(getTimerFiredCount() + 1);

            // now cancel timer - it should not fire again
            if (!getTimerCancelled()) {
                TimerFacility tf = getTimerFacility();
                tf.cancelTimer(getTimer());
                // so we know cancelTimer was called
                setTimerCancelled(true);
                // so we can tell if any more events were received after timer cancelled
                setLastTimerCount(getTimerFiredCount());
            }

        } catch(Exception e) {
            TCKSbbUtils.handleException(e);
        }
    }

    private void sendResultToTCK() throws Exception {
        HashMap sbbData = new HashMap();
        sbbData.put("testname", getTestName());
        sbbData.put("result", result?"pass":"fail");
        sbbData.put("message", message);
        sbbData.put("id", new Integer(failedAssertionID));
        TCKSbbUtils.getResourceInterface().sendSbbMessage(sbbData);
    }

    private void setResultFailed(int assertionID, String msg) throws Exception {
        result = false;
        failedAssertionID = assertionID;
        message =  "Assertion " + assertionID + " failed: " + msg;
        TCKSbbUtils.createTrace(getSbbID(), Level.WARNING, message, null);
    }

    private void setResultPassed(String msg) throws Exception {
        result = true;
        message = "Success: " + msg;
        TCKSbbUtils.createTrace(getSbbID(), Level.INFO, message, null);
    }


    private TimerFacility getTimerFacility () throws Exception {
        TimerFacility tf = null;
        try {
            tf = (TimerFacility) new InitialContext().lookup(JNDI_TIMERFACILITY_NAME);
        } catch (Exception e) {
            TCKSbbUtils.createTrace(getSbbID(),Level.WARNING,"got unexpected Exception: " + e,null);
        }
        return tf;
    }

    // CMP fields
    public abstract TimerID getTimer();
    public abstract void setTimer(TimerID timer);

    public abstract int getTimerFiredCount();
    public abstract void setTimerFiredCount(int count);

    public abstract int getLastTimerCount();
    public abstract void setLastTimerCount(int count);

    public abstract boolean getTimerCancelled();
    public abstract void setTimerCancelled(boolean cancelled);

    public abstract String getTestName();
    public abstract void setTestName(String testName);

    private boolean result = false;
    private String message;
    private static final String JNDI_TIMERFACILITY_NAME = "java:comp/env/slee/facilities/timer";
    private int failedAssertionID = -1;


}
