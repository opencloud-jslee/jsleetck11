/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.profiles.creation;

import javax.slee.profile.ProfileID;
import javax.slee.profile.ProfileSpecificationID;
import javax.slee.profile.ProfileAlreadyExistsException;
import javax.management.ObjectName;
import com.opencloud.sleetck.lib.AbstractSleeTCKTest;
import com.opencloud.sleetck.lib.DescriptionKeys;
import com.opencloud.sleetck.lib.TCKTestResult;
import com.opencloud.sleetck.lib.testsuite.profiles.ProfileTestConstants;
import com.opencloud.sleetck.lib.testutils.ProfileUtils;
import com.opencloud.sleetck.lib.testutils.ComponentIDLookup;
import com.opencloud.sleetck.lib.testutils.jmx.ProfileProvisioningMBeanProxy;
import com.opencloud.sleetck.lib.infra.SleeTCKComponentConstants;
import com.opencloud.sleetck.lib.testutils.jmx.ProfileMBeanProxy;

/**
 * Tests assertion 913, that many profiles can exist in a single table.
 */
public class CreateManyProfilesTest extends AbstractSleeTCKTest {

    private static final String PROFILE_TABLE_NAME = "tck.CreateManyProfilesTest.table";

    public TCKTestResult run() throws Exception {
        // create the profile table
        ProfileProvisioningMBeanProxy profileProvisioning = profileUtils.getProfileProvisioningProxy();
        ProfileSpecificationID profileSpecID = new ComponentIDLookup(utils()).lookupProfileSpecificationID(
            ProfileTestConstants.SIMPLE_PROFILE_SPEC_NAME,SleeTCKComponentConstants.TCK_VENDOR,"1.0");
        getLog().info("Creating profile table: "+PROFILE_TABLE_NAME);
        profileProvisioning.createProfileTable(profileSpecID,PROFILE_TABLE_NAME);
        tableCreated = true;

        // add many profiles
        String[] profileNames = {"profile1","profile2","profile3"};
        int profilesCount = profileNames.length;
        for (int i = 0 ; i < profilesCount; i++) {
            try {
                getLog().info("Creating profile number "+(i+1)+": "+profileNames[i]);
                javax.management.ObjectName profile = profileProvisioning.createProfile(PROFILE_TABLE_NAME,profileNames[i]);
                ProfileMBeanProxy profProxy = utils().getMBeanProxyFactory().createProfileMBeanProxy(profile);
                profProxy.commitProfile();
            } catch (ProfileAlreadyExistsException ex) {
                return TCKTestResult.failed(913,"ProfileAlreadyExistsException caught during creation of profile number "+(i+1)+" in table");
            }
        }

        // check that all the added profiles still exist
        java.util.Collection profiles = profileProvisioning.getProfiles(PROFILE_TABLE_NAME);
        getLog().info("Checking profiles in table");
        if (profiles.size() != profilesCount) return  TCKTestResult.failed(913,
            "Profile claims to have "+profiles.size()+" profiles, after creating "+profilesCount+" profiles");
        for (int i = 0; i < profilesCount; i++) {
            if(!profiles.contains(new ProfileID(PROFILE_TABLE_NAME, profileNames[i]))) {
                return TCKTestResult.failed(913,"Profile "+profileNames[i]+
                    " did not remain in the profile table after adding a total of "+profilesCount+" profiles");
            }
        }
        return TCKTestResult.passed();
    }

    public void setUp() throws Exception {
        String duPath = utils().getTestParams().getProperty(ProfileTestConstants.PROFILE_SPEC_DU_PATH_PARAM);
        getLog().fine("Installing profile specification");
        utils().install(duPath);
        profileUtils = new ProfileUtils(utils());
    }

    public void tearDown() throws Exception {
        try {
            if(profileUtils != null && tableCreated) {
                getLog().fine("Removing profile table");
                profileUtils.removeProfileTable(PROFILE_TABLE_NAME);
            }
        } catch(Exception e) {
            getLog().warning("Caught exception while trying to remove profile table:");
            getLog().warning(e);
        }
        super.tearDown();
    }

    private ProfileUtils profileUtils;
    private boolean tableCreated = false;

}
