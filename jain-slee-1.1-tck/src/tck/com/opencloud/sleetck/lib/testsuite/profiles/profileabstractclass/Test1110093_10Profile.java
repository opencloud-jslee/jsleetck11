/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.profiles.profileabstractclass;

import javax.slee.CreateException;
import javax.slee.profile.Profile;
import javax.slee.profile.ProfileContext;
import javax.slee.profile.ProfileVerificationException;

/**
 * Business methods from ProfileLocal interface dont have implementations with matching signatures.
 */
public abstract class Test1110093_10Profile implements ProfileAbstractClassTestsProfileCMP, Profile  {

    public void business1(int dummy) {
    }

    public int business2(int dummy, String dummy2) {
        return -1;
    }

    public String business3() {
        return "";
    }

    public void setProfileContext(ProfileContext context) {
    }

    public void unsetProfileContext() {
    }

    public void profileInitialize() {
    }

    public void profilePostCreate() throws CreateException {
    }

    public void profileActivate() {
    }

    public void profilePassivate() {
    }

    public void profileLoad() {
    }

    public void profileStore() {
    }

    public void profileRemove() {
    }

    public void profileVerify() throws ProfileVerificationException {
    }
}
