/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.transactions.isolation;

import java.rmi.RemoteException;
import java.util.HashMap;
import java.util.Map;

import javax.naming.Context;
import javax.naming.NamingException;
import javax.slee.ActivityContextInterface;
import javax.slee.RolledBackContext;
import javax.slee.UnrecognizedActivityException;
import javax.slee.facilities.ActivityContextNamingFacility;
import javax.slee.facilities.Level;
import javax.slee.facilities.NameAlreadyBoundException;
import javax.slee.facilities.NameNotBoundException;
import javax.slee.nullactivity.NullActivity;
import javax.slee.nullactivity.NullActivityContextInterfaceFactory;
import javax.slee.nullactivity.NullActivityFactory;

import com.opencloud.sleetck.lib.TCKTestErrorException;
import com.opencloud.sleetck.lib.resource.TCKTestCallException;
import com.opencloud.sleetck.lib.resource.events.TCKResourceEvent;
import com.opencloud.sleetck.lib.sbbutils.BaseTCKSbb;
import com.opencloud.sleetck.lib.sbbutils.TCKSbbUtils;

/**
 * Base SBB abstract class used by some of the transaction isolation tests.
 */
public abstract class Test2003BaseSbb extends BaseTCKSbb {

    public static final String ACI_NAME = "Test2003NullActivity";

    public abstract Test2003ActivityContextInterface asSbbActivityContextInterface(ActivityContextInterface aci);

    public void sbbRolledBack(RolledBackContext context) {
        if(context.getEvent() instanceof TCKResourceEvent) {
            try {
                TCKResourceEvent event = (TCKResourceEvent)context.getEvent();
                String eventTypeName = event.getEventTypeName();
                Map args = new HashMap();
                args.put(IsolationTestConstants.SBB_EVENT_HANDLER_FIELD, eventTypeName);
                args.put(IsolationTestConstants.SBB_ROLLED_BACK,null); // use this key as a flag to indicate SBB_ROLLED_BACK
                createTraceSafe(Level.WARNING,"sbbRolledBack() called for "+eventTypeName+ "event. Calling test");
                TCKSbbUtils.getResourceInterface().callTest(args);
            } catch (Exception e) {
                TCKSbbUtils.handleException(e);
            }

        } else super.sbbRolledBack(context);
    }

    /**
     * Creates a NullActivity, gets its NullActivityContext and uses the ActivityContextNamingFacility to bind the
     * ActivityContextInterface to a name known by the test.
     */
    protected void createSharedActivityContext() throws NamingException, UnrecognizedActivityException, NameAlreadyBoundException {
        Context env = TCKSbbUtils.getSbbEnvironment();
        NullActivityFactory naf = (NullActivityFactory) env.lookup("slee/nullactivity/factory");
        NullActivityContextInterfaceFactory nif =
                (NullActivityContextInterfaceFactory) env.lookup("slee/nullactivity/activitycontextinterfacefactory");
        ActivityContextNamingFacility naming =
                (ActivityContextNamingFacility)env.lookup("slee/facilities/activitycontextnaming");
        NullActivity activity = naf.createNullActivity();
        ActivityContextInterface aci = nif.getActivityContextInterface(activity);
        naming.bind(aci, ACI_NAME);
    }

    /**
     * Unbinds the ActivityContextInterface used by the test and ends the NullActivity
     */
    protected void removeSharedActivityContext() throws NamingException, NameNotBoundException {
        ActivityContextNamingFacility naming =
                (ActivityContextNamingFacility)TCKSbbUtils.getSbbEnvironment().lookup("slee/facilities/activitycontextnaming");
        ActivityContextInterface genericACI = naming.lookup(ACI_NAME);
        NullActivity activity = (NullActivity)genericACI.getActivity();
        activity.endActivity();
        naming.unbind(ACI_NAME);
    }

    /**
     * Retrieves the ActivityContextInterface used by the test by performing a lookup using ActivityContextNamingFacility
     */
    protected Test2003ActivityContextInterface getACI() throws NamingException {
        ActivityContextNamingFacility naming =
                (ActivityContextNamingFacility)TCKSbbUtils.getSbbEnvironment().lookup("slee/facilities/activitycontextnaming");
        ActivityContextInterface genericACI = naming.lookup(ACI_NAME);
        return asSbbActivityContextInterface(genericACI);
    }

    /**
     * Send a response with event handler name and optional value back to the test.  Depending on the event handler
     * and context the test may block before returning.
     * @param handlerName the name of the event handler, which is the event type name of the event
     * @param argument the optional argument to pass to the test
     */
    protected Object sendResponse(String handlerName, Object argument) throws NamingException, TCKTestErrorException, TCKTestCallException, RemoteException {
        Map args = new HashMap();
        args.put(IsolationTestConstants.SBB_EVENT_HANDLER_FIELD, handlerName);
        args.put(IsolationTestConstants.VALUE_FIELD, argument);
        createTraceSafe(Level.INFO,"Sending response for "+handlerName+" event handler. Argument:"+argument);
        return TCKSbbUtils.getResourceInterface().callTest(args);
    }


    /**
     * Returns a unique ID identifying the transaction.
     */
    protected String getTransactionId() throws NamingException, TCKTestErrorException {
        return TCKSbbUtils.getResourceAdaptorInterface().getTransactionIDAccess().getCurrentTransactionID().toString();
    }
}
