/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.resource.endpoint;

import java.util.HashMap;

import com.opencloud.sleetck.lib.TCKTestErrorException;
import com.opencloud.sleetck.lib.TCKTestFailureException;
import com.opencloud.sleetck.lib.TCKTestResult;
import com.opencloud.sleetck.lib.testsuite.resource.BaseResourceTest;
import com.opencloud.sleetck.lib.testsuite.resource.RAMethods;

/**
 * Test to ensure that startActivity() is behaving in a non-transactional way.
 * <p>
 * Test assertion: 1115211
 */
public class Test1115211Test extends BaseResourceTest {

    private static final int ASSERTION_ID = 1115211;

    public TCKTestResult run() throws Exception {
        /*
         * 1115211 - This method is non-transactional method. This also implies
         * that the Activity does not get enrolled in any transaction.
         */

        /*
         * 1115212 - The SLEE considers the Activity started regardless of
         * whether the transaction associated with the current thread (if it
         * exists) commits or rolls back.
         */

        HashMap results = sendMessage(RAMethods.startActivity, new Integer(ASSERTION_ID));
        if (results == null)
            throw new TCKTestFailureException(ASSERTION_ID, "Test timed out while waiting for response from test component");

        Object result = results.get("result");
        if (result instanceof Exception)
            throw new TCKTestFailureException(ASSERTION_ID, "Unexpected exception thrown during test:", (Exception) result);
        if (Boolean.FALSE.equals(result))
            throw new TCKTestFailureException(ASSERTION_ID, "startActivity() appears to be behaving transactionally");
        if (!Boolean.TRUE.equals(result))
            throw new TCKTestErrorException("Unexpected result received from test component: " + result);

        return TCKTestResult.passed();
    }

}
