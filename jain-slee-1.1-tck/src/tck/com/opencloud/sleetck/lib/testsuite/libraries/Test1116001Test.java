/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.libraries;

import java.rmi.RemoteException;

import javax.slee.ServiceID;
import javax.slee.management.DependencyException;
import javax.slee.management.ManagementException;
import javax.slee.management.DeployableUnitID;

import com.opencloud.logging.Logable;
import com.opencloud.sleetck.lib.AbstractSleeTCKTest;
import com.opencloud.sleetck.lib.DescriptionKeys;
import com.opencloud.sleetck.lib.TCKTestErrorException;
import com.opencloud.sleetck.lib.TCKTestResult;
import com.opencloud.sleetck.lib.resource.TCKActivityID;
import com.opencloud.sleetck.lib.resource.TCKSbbMessage;
import com.opencloud.sleetck.lib.resource.events.TCKResourceEventX;
import com.opencloud.sleetck.lib.resource.testapi.TCKResourceTestInterface;
import com.opencloud.sleetck.lib.testutils.BaseTCKResourceListener;
import com.opencloud.sleetck.lib.testutils.FutureResult;
import com.opencloud.sleetck.lib.testutils.jmx.DeploymentMBeanProxy;

public class Test1116001Test extends AbstractSleeTCKTest {

    private static final int TEST_ID = 1116001;

    public TCKTestResult run() throws Exception {
        result = new FutureResult(utils().getLog());
        TCKTestResult tckresult;

        // Create a new activity.
        utils().getLog().fine("Create Activity and fire starting event");
        TCKResourceTestInterface resource = utils().getResourceInterface();
        TCKActivityID activityID = resource.createActivity(getClass().getName());
        resource.fireEvent(TCKResourceEventX.X1, null, activityID, null);

        tckresult = result.waitForResult(5000);

        resource.clearActivities();

        try {
            utils().getLog().fine("1114409 - uninstall library DU");
            deploymentMBean.uninstall(libraryDuID);
            tckresult = TCKTestResult.failed(1114409, "Library could be deployed when there were dependencies on it.");
        } catch (DependencyException de) {
            // good.
            utils().getLog().info("Check for assertion 1114409 OK");
            tckresult = TCKTestResult.passed();
        } catch (Exception e) {
            tckresult =  TCKTestResult.failed(1114409, "Library deployment failed with exception other than DependencyException: " + e);
        }
        return tckresult;
    }

    public void setUp() throws Exception {
        utils().getLog().fine("Connecting to resource");

        resourceListener = new TCKResourceListenerImpl();
        utils().getResourceInterface().setResourceListener(resourceListener);
        deploymentMBean = utils().getDeploymentMBeanProxy();

        try {
            libraryDUPath = utils().getTestParams().getProperty("libraryDUPath");
            utils().getLog().fine("Installing library: " +utils().getDeploymentUnitURL(libraryDUPath));
            libraryDuID = deploymentMBean.install(utils().getDeploymentUnitURL(libraryDUPath));
        } catch (Exception e) {
            utils().getLog().fine("Exception raised trying to install library" + e);
            throw new TCKTestErrorException("Failed to Install Library DU");
        }

        serviceDUPath = utils().getTestParams().getProperty("serviceDUPath");
        serviceDuID = deploymentMBean.install(utils().getDeploymentUnitURL(serviceDUPath));

        serviceID = new ServiceID("Test1116001TestService", "jain.slee.tck", "1.1");
        utils().getServiceManagementMBeanProxy().activate(serviceID);

        utils().getLog().fine("Test set up complete");
    }

    public void tearDown() throws Exception {
        try {
            utils().getResourceInterface().clearActivities();
            utils().getResourceInterface().removeResourceListener();

            if (serviceDuID != null) {
                utils().deactivateService(serviceID);
                deploymentMBean.uninstall(serviceDuID);
            } else {
                getLog().fine("serviceDuID is null");
            }

            if (libraryDuID != null) {
                deploymentMBean.uninstall(libraryDuID);
            } else {
                getLog().fine("libraryDuID is null");
            }

            super.tearDown();
            utils().getLog().info("This test has finished.");
        } catch (Exception e) {
            utils().getLog().fine("Exception raised in tearDown()" + e);
        }
    }

    private class TCKResourceListenerImpl extends BaseTCKResourceListener {

        public synchronized void onSbbMessage(TCKSbbMessage message, TCKActivityID activity) throws RemoteException {
            utils().getLog().info("Received message from SBB: " + message.getMessage());
            result.setPassed();
        }

        public void onException(Exception e) throws RemoteException {
            utils().getLog().warning("Received exception from SBB.");
            utils().getLog().warning(e);
            result.setError(e);
        }

    }

    private TCKResourceListenerImpl resourceListener;
    private FutureResult result;
    private DeployableUnitID libraryDuID, serviceDuID;
    private DeploymentMBeanProxy deploymentMBean;
    private String libraryDUPath;
    private String serviceDUPath;
    private ServiceID serviceID;
}
