/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.events.sbbabstractclass;

import java.util.HashMap;

import javax.slee.ActivityContextInterface;
import javax.slee.Address;
import javax.slee.EventContext;
import javax.slee.ServiceID;
import javax.slee.facilities.Tracer;

import com.opencloud.sleetck.lib.resource.events.TCKResourceEventX;
import com.opencloud.sleetck.lib.sbbutils.BaseTCKSbb;
import com.opencloud.sleetck.lib.sbbutils.TCKSbbUtils;

/**
 * AssertionID(1108059): Test If the service argument is null then SBB 
 * entities belonging to all Services are eligible to receive the event 
 * and new root SBB entities belonging to all Services are eligible to 
 * be initiated by the event.
 */
public abstract class Test1108059Sbb1 extends BaseTCKSbb {

    // There are sbb1a and sbb1b which represent runtime instances of sbb1
    // Initial event method.
    public void onTCKResourceEventX1(TCKResourceEventX event, ActivityContextInterface aci, EventContext context) {
        try {
            //Sbb1a is created because the event from the TCK is an initial event for it
            tracer = getSbbContext().getTracer("com.test");
            tracer.info("Sbb1: Received a TCK event " + event);

            Test1108059Event testEvent = new Test1108059Event();

            fireTest1108059Event(testEvent, aci, null, null);

        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }
    }

    public void onTest1108059Event(Test1108059Event event, ActivityContextInterface aci, EventContext context) {
        try {
            //Sbb1b is created because the custom event fired by 1a is also an initial event for it
            tracer = getSbbContext().getTracer("com.test");
            tracer.info("Sbb1: Received the first custom event " + event);
            sendResultToTCK(1108059, "Sbb1 on Test1108059Event: If the service argument is null then new root "
                    + "SBB entities belonging to all Services are eligible to be initiated by the event.", "Test1108059Test", true);
        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }

    }

    public void onTCKResourceEventX2(TCKResourceEventX event, ActivityContextInterface aci, EventContext context) {
        try {
            tracer = getSbbContext().getTracer("com.test");
            tracer.info("Sbb1: Received a TCK event " + event);
            Test1108059SecondEvent testEvent = new Test1108059SecondEvent();

            fireTest1108059SecondEvent(testEvent, aci, null, null);

        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }
    }

    public void onTest1108059SecondEvent(Test1108059SecondEvent event, ActivityContextInterface aci,
            EventContext context) {
        try {
            tracer = getSbbContext().getTracer("com.test");
            tracer.info("Sbb1: Received the second custom event " + event);
            sendResultToTCK(1108059, "Sbb1 on Test1108059SecondEvent: If the service argument is null then new root "
                    + "SBB entities belonging to all Services are eligible to be initiated by the event.", "Test1108059Test", true);
        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }

    }

    public abstract void fireTest1108059Event(Test1108059Event event, ActivityContextInterface aci, Address address,
            ServiceID serviceID);

    public abstract void fireTest1108059SecondEvent(Test1108059SecondEvent event, ActivityContextInterface aci,
            Address address, ServiceID serviceID);

    private void sendResultToTCK(int failedAssertionID, String message, String testName, boolean result) throws Exception {
        HashMap sbbData = new HashMap();
        sbbData.put("testname", testName);
        sbbData.put("result", result ? "pass" : "fail");
        sbbData.put("message", message);
        sbbData.put("id", new Integer(failedAssertionID));
        TCKSbbUtils.getResourceInterface().sendSbbMessage(sbbData);
    }

    private Tracer tracer;
}
