/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.transactions;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;

import javax.naming.NamingException;
import javax.transaction.RollbackException;
import javax.transaction.Status;
import javax.transaction.Transaction;
import javax.transaction.TransactionManager;

import com.opencloud.logging.StdErrLog;
import com.opencloud.sleetck.lib.profileutils.BaseMessageSender;
import com.opencloud.sleetck.lib.rautils.MessageHandler;
import com.opencloud.sleetck.lib.rautils.RMIObjectChannel;
import com.opencloud.sleetck.lib.rautils.TCKRAUtils;

/**
 * Provides callback method for handling messages sent from the TCK test to the RA.
 * The actual test code is located in the 'handleMessage()' method.
 */
public class Test1109270MessageListener extends UnicastRemoteObject implements MessageHandler {

    public Test1109270MessageListener(Test1109270ResourceAdaptor ra) throws RemoteException {
        super();
        try {
            out = TCKRAUtils.lookupRMIObjectChannel();
            msgSender = new BaseMessageSender(out, new StdErrLog());
        }
        catch (Exception e) {
            ra.getLog().warning("Exception occurred when trying to acquire RMIObjectChannel: ",e);
        }
        this.ra = ra;
    }

    public boolean handleMessage(Object message) throws RemoteException {
        TransactionManager txManager = null;
        Transaction txn;

        try {
            try {
                txManager = ra.getTransactionManager();
            } catch (NamingException e) {
                msgSender.sendError("Could not look up transaction manager.", e);
                return true;
            }

            //1109286: Get the transaction object that represents the transaction context of the calling thread.
            try {
                txn = txManager.getTransaction();
            } catch (Exception e) {
                msgSender.sendFailure(1109286, "Exception when calling txManager.getTransaction().", e);
                return true;
            }
            if (txn != null) {
                msgSender.sendFailure(1109286, "The current thread was not yet associated with any TXN thus " +
                        "TXNManager.getTransaction() should have returned 'null'.");
                return true;
            }

            // 1109270: Create a new transaction and associate it with the current thread.
            try {
                txManager.begin();
            } catch (Exception e) {
                msgSender.sendFailure(1109270, "Exception occurred when calling TXNManager.begin() to create a new TXN.");
                return true;
            }


            //1109286: Get the transaction object that represents the transaction context of the calling thread.
            try {
                txn = txManager.getTransaction();
            } catch (Exception e) {
                msgSender.sendFailure(1109286, "Exception occured while calling getTransaction().", e);
                return true;
            }
            if (txn == null) {
                msgSender.sendFailure(1109286, "TXNManager.getTransaction() returned 'null' though previously " +
                        "a new TXN had been started with TXNManager.begin().");
            }
            msgSender.sendLogMsg("TXNManager.getTransaction() returned current TXN as expected.");


            // 1109282: Obtain the status of the transaction associated with the current thread.
            int status;
            try {
                status = txManager.getStatus();
            } catch (Exception e) {
                msgSender.sendFailure(1109282, "Exception occurred when calling getStatus().", e);
                return true;
            }
            if (StatusUtil.INVALID.equals(StatusUtil.statusToString(status))) {
                msgSender.sendFailure(1109282, "TXNManager.getStatus() returned a value different to the constants defined in the javax.transaction.Status interface.");
                return true;
            }
            msgSender.sendLogMsg("TXNManager.getStatus() returned current status of TXN: "+status);

            //1109274: Complete the transaction associated with the current thread.
            try {
                txManager.commit();
                msgSender.sendLogMsg("commit() returned successfully.");
            } catch (Exception e) {
                msgSender.sendFailure(1109274, "Exception occurred when calling TXNManager.commit().", e);
                return true;
            }

            // 1109348: When commit() completes, the thread is no longer associated with a transaction.
            if (null != txManager.getTransaction()) {
                msgSender.sendFailure(1109348, "There is still a transaction associated with this thread.");
                return true;
            }


            // 1109301: setRollbackOnly() throws IllegalStateException if the current thread is not associated with a transaction.
            try {
                txManager.setRollbackOnly();
                msgSender.sendFailure(1109301, "txManager.setRollbackOnly() did not throw an IllegalStateException");
                return true;
            } catch (IllegalStateException e) {
                msgSender.sendLogMsg("txManager.setRollbackOnly() threw IllegalStateException as expected.");
            }

            // 1109279: commit() throws IllegalStateException if the current thread is not associated with a transaction.
            try {
                txn.commit();
                msgSender.sendFailure(1109279, "txManager.commit() did not throw an IllegalStateException");
                return true;
            } catch (IllegalStateException e) {
                msgSender.sendLogMsg("txManager.commit() threw IllegalStateException as expected.");
            }


            // 1109275: commit() throws RollbackException to indicate that the TXN has been rolled back rather than committed.
            txManager.begin();
            txn = txManager.getTransaction();
            // 1109300: Modify the transaction associated with the current thread such that the only possible outcome
            //of the transaction is to roll back the transaction.
            try {
                txManager.setRollbackOnly();
            } catch (Exception e) {
                msgSender.sendFailure(1109300, "Exception occured when calling setRollbackOnly() with a valid TXN context.", e);
                return true;
            }
            try {
                txManager.commit();
                msgSender.sendFailure(1109275, "txManager.commit() did not throw a RollbackException though the " +
                        "TXN previously has been marked for rollback.");
                return true;
            } catch (RollbackException e) {
                msgSender.sendLogMsg("txManager.commit() threw RollbackException as expected.");
            }

            // 1109283: If no transaction is associated with the current thread, this method returns the Status.NoTransaction value.
            if (Status.STATUS_NO_TRANSACTION != txManager.getStatus()) {
                msgSender.sendFailure(1109283, "txManager.getStatus() did not return Status.STATUS_NO_TRANSACTION.");
                return true;
            }

            // 1109309: If the calling thread is not associated with a transaction, suspend() returns a null object reference.
            if (null != txManager.suspend()) {
                msgSender.sendFailure(1109309, "txManager.suspend() with no active transaction should have returned 'null'.");
                return true;
            }


            // 1109308: Suspend the TXN currently associated with the calling thread and
            //return a TXN object that represents the TXN context being suspended.
            txManager.begin();
            txn = txManager.getTransaction();
            if (!txn.equals(txManager.suspend())) {
                msgSender.sendFailure(1109308, "txManager.suspend returned a different transaction " +
                        "(determined via the TXN's equals() method) than the one previously started via begin().");
                return true;
            } else {
                msgSender.sendLogMsg("suspend() returned the current TXN context as expected.");
            }

            //1109310: When suspend() returns, the calling thread is associated with no transaction.
            if (null != txManager.getTransaction()) {
                msgSender.sendFailure(1109310, "There is still a transaction associated with this thread after calling suspend().");
                return true;
            } else {
                msgSender.sendLogMsg("After suspend() returned the calling thread was no longer associated wit a TXN as expected.");
            }

            //get a new TXN different to the suspended one
            txManager.begin();
            Transaction txn2 =  txManager.suspend();

            //1109289: Resume the transaction context association of the calling thread with the
            //transaction represented by the supplied Transaction object.
            //When this method returns, the calling thread is associated with the transaction
            //context specified.
            txManager.resume(txn);
            if (!txn.equals(txManager.getTransaction())) {
                msgSender.sendFailure(1109289, "After resume() returned getTransaction() did not " +
                        "return the same TXN as the resumed one (determined by the TXN's equals() method), so " +
                        "probably the calling thread was not properly associated with the transaction " +
                        "specified for resuming.");
                return true;
            }

            // 1109291: resume() throws IllegalStateException if the thread is already associated with another transaction.
            try {
                txManager.resume(txn2);
                msgSender.sendFailure(1109291, "txManager.resume() did not throw an IllegalStateException.");
                return true;
            } catch (IllegalStateException e) {
                msgSender.sendLogMsg("resume() threw IllegalStateException as expected.");
            }

            try {
                txn2.rollback();
                msgSender.sendLogMsg("Cleanup txn2.");
            } catch (Exception e) {
                msgSender.sendError("Exception occured cleaning up TXN: "+e.getMessage());
                return true;
            }

            // 1109294: Roll back the transaction associated with the current thread.
            try {
                txManager.rollback();
                msgSender.sendLogMsg("rollback() returned successfully.");
            } catch (Exception e) {
                msgSender.sendFailure(1109294, "Exception occurred when calling TXNManager.rollback().", e);
                return true;
            }
            if (txn.getStatus()!=Status.STATUS_ROLLEDBACK) {
                msgSender.sendFailure(1109294, "txManager.rollback() did not rollback the TXN properly. The TXN's status was "+txn.getStatus());
                return true;
            } else {
                msgSender.sendLogMsg("txManager.rollback() successfully rolledback the TXN.");
            }

            // 1109295: When this method completes, the thread is no longer associated with a TXN.
            if (null != txManager.getTransaction()) {
                msgSender.sendFailure(1109295, "There is still a transaction associated with this thread after rolling back.");
                return true;
            }

            // 1109297: rollback() throws IllegalStateException if the current thread is not associated with a transaction.
            try {
                txManager.rollback();
                msgSender.sendFailure(1109297, "txManager.rollback() did not throw an IllegalStateException.");
                return true;
            } catch (IllegalStateException e) {
                msgSender.sendLogMsg("rollback() threw IllegalStateException as expected.");
            }

            txManager.begin();
            txn = txManager.getTransaction();
            //1109300: Modify the transaction associated with the current thread such that the only possible outcome
            //of the transaction is to roll back the transaction.
            txManager.setRollbackOnly();
            try {
                txManager.rollback();
            } catch (Exception e) {
                msgSender.sendFailure(1109300,"Exception occurred when calling rollback() on a TXN that previously has been marked" +
                        "for rollback via setRollbackOnly()");
            }
            msgSender.sendLogMsg("setRollbackOnly() marked the TXN for rollback as expected.");


            msgSender.sendSuccess(1109270, "Test completed successfully.");
        } catch (Exception e) {
            msgSender.sendException(e);
        } finally {
            try {
                if (txManager!=null && txManager.getTransaction()!=null)
                    txManager.getTransaction().rollback();
            } catch (Exception e2) {
                msgSender.sendLogMsg("Exception occurred when trying to safely cleanup still active TXNs. " +
                        "This is however not the immediate cause of a eventual test failure/error:"+e2);
            }
        }
        return true;
    }

    private Test1109270ResourceAdaptor ra;
    private BaseMessageSender msgSender;
    private RMIObjectChannel out;
}

