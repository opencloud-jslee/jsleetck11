/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.events.convergencename;

import com.opencloud.sleetck.lib.sbbutils.BaseTCKSbb;
import com.opencloud.sleetck.lib.sbbutils.TCKSbbUtils;
import com.opencloud.sleetck.lib.resource.events.TCKResourceEventX;

import javax.slee.ActivityContextInterface;
import javax.slee.facilities.Level;
import javax.slee.InitialEventSelector;

/**
 * The ConvergenceNameTestSbb class is used by a number of the convergence
 * name tests.
 *
 * The two event handler methods delegate to a shared event handler which stores the
 * initial event id, and replies with the received event id and the initial event id.
 *
 * The event message object is an EventMessageData in exported form.
 * The reply message object is an EventReplyData in exported form.
 *
 * This class includes an InitialEventSelector method which will be used if defined
 * in the depoyment descriptor via the initial-event-selector-method-name element.
 *
 * The desployment descriptor elements which are expected to vary in the event elements are:
 * <ul><li>
 * the initial-event-select elements</li><il>
 * the initial-event-selector-method-name element</li><il>
 * </li></ul><ul><li>
 */
public abstract class ConvergenceNameTestSbb extends BaseTCKSbb {

    // -- Event Handlers -- //

    public void onTCKResourceEventX1(TCKResourceEventX event, ActivityContextInterface aci) {
        handleEvent(event);
    }

    public void onTCKResourceEventX2(TCKResourceEventX event, ActivityContextInterface aci) {
        handleEvent(event);
    }

    private void handleEvent(TCKResourceEventX event) {
        try {
            TCKSbbUtils.createTrace(getSbbID(), Level.FINE, "Received " + event.getEventTypeName() +
                                                                  "message=" + event.getMessage(), null);

            // extract the received event id from the event
            EventMessageData eventMessage = EventMessageData.fromExported(event.getMessage());
            String receivedEventID = eventMessage.getEventID();

            // set the initial event id if not already set (i.e. if this is the initial event)
            String initialEventID = getInitialEventID();
            if (initialEventID == null) {
                initialEventID = receivedEventID;
                setInitialEventID(receivedEventID);
            }

            // reply to the test with a synchronous call
            Object reply = new EventReplyData(receivedEventID, initialEventID).toExported();
            TCKSbbUtils.getResourceInterface().callTest(reply);

        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }
    }

    // -- Initial event selector method -- //

    public InitialEventSelector initialEventSelectorMethod(InitialEventSelector ies) {
        try {
            TCKResourceEventX event = (TCKResourceEventX) ies.getEvent();
            EventMessageData eventMessage = EventMessageData.fromExported(event.getMessage());
            InitialEventSelectorParameters iesParams = eventMessage.getInitialEventSelectorData();
            // select the chosen variables
            ies.setActivityContextSelected(iesParams.getSelectActivityContextFlag());
            ies.setAddressProfileSelected(iesParams.getSelectAddressProfileFlag());
            ies.setAddressSelected(iesParams.getSelectAddressFlag());
            ies.setEventTypeSelected(iesParams.getSelectEventTypeFlag());
            ies.setEventSelected(iesParams.getSelectEventFlag());
            ies.setCustomName(iesParams.getCustomName());
            if (iesParams.getChangePossibleInitialEventFlag()) {
                ies.setInitialEvent(iesParams.getPossibleInitialEventFlag());
            }
            if (iesParams.getChangeAddressFlag()) {
                ies.setAddress(iesParams.getNewAddress());
            }
        } catch (RuntimeException ex) {
            TCKSbbUtils.handleException(ex);
        }
        return ies;
    }

    // -- CMP fields -- //

    public abstract String getInitialEventID();
    public abstract void setInitialEventID(String id);

}
