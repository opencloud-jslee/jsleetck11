/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.management.ManagementMBean;

import com.opencloud.sleetck.lib.AbstractSleeTCKTest;
import com.opencloud.sleetck.lib.TCKTestResult;
import com.opencloud.sleetck.lib.testutils.jmx.DeploymentMBeanProxy;

import javax.slee.ComponentID;
import javax.slee.management.DeployableUnitDescriptor;
import javax.slee.management.DeployableUnitID;

public class Test1600Test extends AbstractSleeTCKTest {

    private static final String SBB_DU_PATH_PARAM = "sbbDUPath";
    private static final String EVENT_DU_PATH_PARAM = "eventDUPath";
    private static final int TEST_ID = 1600;

    /**
     * Perform the actual test.
     */
    public TCKTestResult run() throws Exception {
        DeploymentMBeanProxy duProxy = utils().getDeploymentMBeanProxy();
        DeployableUnitDescriptor eventDuDesc = duProxy.getDescriptor(eventDuID);

        ComponentID eventComponents[] = eventDuDesc.getComponents();
        if (eventComponents.length == 0)
            return TCKTestResult.failed(TEST_ID, "No components returned by DeployableUnitDescriptor.getComponents() for " +
                    "DeployableUnitID "+eventDuID);

        ComponentID components[] = duProxy.getReferringComponents(eventComponents[0]);
        if (components.length != 1)
            return TCKTestResult.failed(TEST_ID, "There should have been one referring component, instead " + components.length + " were found. " +
                    "eventDuID="+eventDuID+",sbbDuID="+sbbDuID+". The SBB component refers to the event type.");

        return TCKTestResult.passed();
    }

    /**
     * Do all the pre-run configuration of the test.
     */
    public void setUp() throws Exception {
        getLog().fine("Installing and activating service");
        // Install the Deployable Units
        String duPath = utils().getTestParams().getProperty(EVENT_DU_PATH_PARAM);
        eventDuID = utils().install(duPath);
        duPath = utils().getTestParams().getProperty(SBB_DU_PATH_PARAM);
        sbbDuID = utils().install(duPath);
    }

    private DeployableUnitID eventDuID, sbbDuID;
}
