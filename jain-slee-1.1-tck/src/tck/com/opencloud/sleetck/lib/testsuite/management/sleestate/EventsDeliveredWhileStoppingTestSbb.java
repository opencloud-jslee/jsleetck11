/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.management.sleestate;

import com.opencloud.sleetck.lib.resource.events.TCKResourceEventX;
import com.opencloud.sleetck.lib.sbbutils.TCKSbbUtils;
import com.opencloud.sleetck.lib.sbbutils.BaseTCKSbb;

import javax.slee.ActivityContextInterface;
import javax.slee.facilities.Level;

public abstract class EventsDeliveredWhileStoppingTestSbb extends BaseTCKSbb {

    public void onTCKResourceEventX1(TCKResourceEventX event, ActivityContextInterface aci) {
        handleEvent(EventsDeliveredWhileStoppingTestConstants.SBB1_RECEIVED_X1,"TCKResourceEventX1");
    }

    public void onTCKResourceEventX2(TCKResourceEventX event, ActivityContextInterface aci) {
        handleEvent(EventsDeliveredWhileStoppingTestConstants.SBB1_RECEIVED_X2,"TCKResourceEventX2");
    }

    /**
     * Sends a synchronous ACK message to the test, passing an Integer
     * representing the call code as the message payload.
     */
    private void handleEvent(int callCode, String eventDisplayName) {
        // create a trace message
        try {
            TCKSbbUtils.createTrace(getSbbID(),Level.INFO,"Received "+eventDisplayName,null);
        } catch (Exception ex) {
            TCKSbbUtils.handleException(ex);
        }
        // send a synchronous message to the test
        try {
            TCKSbbUtils.getResourceInterface().callTest(new Integer(callCode));
        } catch (Exception ex) {
            TCKSbbUtils.handleException(ex);
        }
    }

}
