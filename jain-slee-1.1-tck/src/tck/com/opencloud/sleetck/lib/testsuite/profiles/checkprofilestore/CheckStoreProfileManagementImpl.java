/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.profiles.checkprofilestore;

import javax.slee.profile.ProfileManagement;
import javax.slee.profile.ProfileVerificationException;

/**
 * 
 */
public abstract class CheckStoreProfileManagementImpl implements ProfileManagement, CheckStoreProfileCMP, CheckStoreProfileManagement {

    private String transientValue;

    public void profileInitialize() {
        transientValue = CheckStoreProfileCMP.INITIAL_VALUE;
    }

    public void profileLoad() {
    }

    public void profileStore() {
        setValue(transientValue);
    }

    public void profileVerify() throws ProfileVerificationException {
        if( getValue().equals(CheckStoreProfileCMP.INVALID_VALUE) ) {
            throw new ProfileVerificationException("Field value is invalid: " + getValue());
        }
    }

    public void setTransientValue(String transientValue) {
        this.transientValue = transientValue;
        this.markProfileDirty();
    }

}
