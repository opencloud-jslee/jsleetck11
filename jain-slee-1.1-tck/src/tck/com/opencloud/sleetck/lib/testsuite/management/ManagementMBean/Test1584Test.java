/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.management.ManagementMBean;

import com.opencloud.sleetck.lib.AbstractSleeTCKTest;
import com.opencloud.sleetck.lib.TCKTestResult;
import com.opencloud.sleetck.lib.testutils.jmx.DeploymentMBeanProxy;

import javax.slee.management.DependencyException;
import javax.slee.management.DeployableUnitID;

public class Test1584Test extends AbstractSleeTCKTest {

    private static final String SBB_DU_PATH_PARAM = "sbbDUPath";
    private static final String PROFILE_DU_PATH_PARAM = "profileDUPath";
    private static final int TEST_ID = 1584;

    /**
     * Perform the actual test.
     */
    public TCKTestResult run() throws Exception {
        DeploymentMBeanProxy duProxy = utils().getDeploymentMBeanProxy();
        try {
            duProxy.uninstall(profileDuID);
        } catch (DependencyException e) {
            return TCKTestResult.passed();
        }
        return TCKTestResult.failed(TEST_ID, "Was able to uninstall a Profile Specification jar while an SBB refers to it." +
                "profileDuID="+profileDuID+",sbbDuID="+sbbDuID);
    }

    /**
     * Do all the pre-run configuration of the test.
     */

    public void setUp() throws Exception {
        getLog().fine("Installing and activating service");
        // Install the Deployable Units
        String duPath = utils().getTestParams().getProperty(PROFILE_DU_PATH_PARAM);
        profileDuID = utils().install(duPath);
        duPath = utils().getTestParams().getProperty(SBB_DU_PATH_PARAM);
        sbbDuID = utils().install(duPath);
    }

    private DeployableUnitID profileDuID, sbbDuID;
}
