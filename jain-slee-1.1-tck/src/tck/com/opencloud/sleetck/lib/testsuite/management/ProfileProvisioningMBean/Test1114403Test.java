/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.management.ProfileProvisioningMBean;

import java.rmi.RemoteException;
import java.util.Collection;
import java.util.Iterator;
import java.util.Vector;

import javax.slee.InvalidArgumentException;
import javax.slee.management.ManagementException;
import javax.slee.profile.ProfileSpecificationID;
import javax.slee.profile.UnrecognizedProfileSpecificationException;

import com.opencloud.sleetck.lib.SleeTCKTest;
import com.opencloud.sleetck.lib.SleeTCKTestUtils;
import com.opencloud.sleetck.lib.TCKTestErrorException;
import com.opencloud.sleetck.lib.TCKTestFailureException;
import com.opencloud.sleetck.lib.TCKTestResult;
import com.opencloud.sleetck.lib.infra.SleeTCKComponentConstants;
import com.opencloud.sleetck.lib.resource.testapi.TCKResourceListener;
import com.opencloud.sleetck.lib.testutils.BaseTCKResourceListener;
import com.opencloud.sleetck.lib.testutils.ComponentIDLookup;
import com.opencloud.sleetck.lib.testutils.ProfileUtils;
import com.opencloud.sleetck.lib.testutils.jmx.ProfileProvisioningMBeanProxy;

/**
 * Tries to create profile tables with valid and invalid names.
 */
public class Test1114403Test implements SleeTCKTest {

    private static final String PROFILE_DU_PATH_PARAM = "profileDUPath";
    private static final int ASSERTION_ID = 1114403;

    public void init(SleeTCKTestUtils utils) {
        this.utils = utils;
    }

    /**
     * Perform the actual test.
     */
    public TCKTestResult run() throws Exception {
        ProfileProvisioningMBeanProxy profileProvisioningProxy = profileUtils.getProfileProvisioningProxy();

        ProfileSpecificationID profileSpecID = new ComponentIDLookup(utils).lookupProfileSpecificationID(
                "SimpleProfile11",SleeTCKComponentConstants.TCK_VENDOR,"1.1");
        if(profileSpecID == null) throw new TCKTestErrorException("No ProfileSpecification found.");

        String validName = "Test1114403Profile";
        try {
            utils.getLog().info("Attempting to create a profile table with the following valid name: "+validName);
            profileProvisioningProxy.createProfileTable(profileSpecID,validName);
            tablesAdded.addElement(validName);
            utils.getLog().info("The createProfileTable() method threw no Exception. Calling getProfileTables() to check whether the "+
                    "profile table was created");
            Collection profileTableNames = profileProvisioningProxy.getProfileTables(profileSpecID);
            if(profileTableNames.size() < 1 || profileTableNames == null || !profileTableNames.contains(validName))
                throw new TCKTestFailureException(ASSERTION_ID,"Failed to create a profile table "+
                    "with the following valid name: "+validName+". createProfileTable() threw no Exception, "+
                    "but the profile table name did not exist in the set of names returned by getProfileTables().");
            utils.getLog().info("The profile table was created successfully");
            utils.getLog().info("profileSpecID is: " + profileSpecID);
            logSuccessfulCheck(1114403);
        } catch (InvalidArgumentException e) {
            throw new TCKTestFailureException(ASSERTION_ID,"Caught unexpected InvalidArgumentException when trying to create a "+
                    "profile table with the following valid name: "+validName,e);
        } catch (Exception e) {
            utils.getLog().warning(e);
        }

        ProfileSpecificationID nullProfileSpecID = null;
        try {
            Collection profileTableNames = profileProvisioningProxy.getProfileTables(nullProfileSpecID);
            return TCKTestResult.failed(1114556, "ProfileProvisioningMBean.getProfileTables has not thrown Exception: NullPointerException");
        } catch (NullPointerException e) {
            logSuccessfulCheck(1114556);
         } catch (Exception e) {
             utils.getLog().warning(e);
            return TCKTestResult.failed(1114556, "ProfileProvisioningMBean.getProfileTables has thrown Exception: " + e.getClass().toString());
         }

         ProfileSpecificationID badProfileSpecID = new ProfileSpecificationID("badProfile", "jain.slee.tck", "1.1");
        try {
            Collection profileTableNames = profileProvisioningProxy.getProfileTables(badProfileSpecID);
            return TCKTestResult.failed(1114557, "ProfileProvisioningMBean.getProfileTables has thrown Exception: UnrecognizedProfileSpecificationException");
         } catch (UnrecognizedProfileSpecificationException e) {
            logSuccessfulCheck(1114557);
         } catch (Exception e) {
            utils.getLog().warning(e);
            return TCKTestResult.failed(1114557, "ProfileProvisioningMBean.getProfileTables has thrown Exception: " + e.getClass().toString());
         }

        return TCKTestResult.passed();
    }

    /**
     * Do all the pre-run configuration of the test.
     */
    public void setUp() throws Exception {
        tablesAdded = new Vector();

        utils.getLog().fine("Connecting to resource");
        resourceListener = new TCKResourceListenerImpl();
        utils.getResourceInterface().setResourceListener(resourceListener);

        // Install the Deployable Units
        String duPath = utils.getTestParams().getProperty(PROFILE_DU_PATH_PARAM);
        utils.getLog().fine("Installing the profile spec: " + duPath);
        utils.install(duPath);
        profileUtils = new ProfileUtils(utils);

    }

    /**
     * Clean up after the test.
     */
    public void tearDown() throws Exception {
        if (profileUtils != null && tablesAdded != null && !tablesAdded.isEmpty()) {
            Iterator tablesAddedIter = tablesAdded.iterator();
            while(tablesAddedIter.hasNext()) {
                try {
                    profileUtils.removeProfileTable((String)tablesAddedIter.next());
                } catch (Exception e) {
                    utils.getLog().warning(e);
                }
            }
        }
        utils.getLog().fine("Uninstalling the profile spec");
        utils.uninstallAll();
        utils.getLog().fine("Disconnecting from resource");
        utils.getResourceInterface().removeResourceListener();
    }

    private class TCKResourceListenerImpl extends BaseTCKResourceListener {
        public void onException(Exception e) throws RemoteException {
            utils.getLog().warning("Received exception from the TCK resource");
            utils.getLog().warning(e);
        }
    }

    private void logSuccessfulCheck(int assertionID) {
        utils.getLog().info("Check for assertion "+assertionID+" OK");
    }

    private SleeTCKTestUtils utils;
    private TCKResourceListener resourceListener;
    private ProfileUtils profileUtils;
    private Vector tablesAdded;

}
