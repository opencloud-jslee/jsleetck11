/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.profiles.readonly;

/**
 * This test checks profile behaviour if the profile spec's 'profile-read-only' attribute is set
 * to 'true'. The test operations are performed via Sbb.
 */
public class Test1110068Test extends ReadOnlyIsTrueBaseTest {

    private static final String PROFILE_SPEC_NAME = "Test1110068Profile";

    public String getSpecName() {

        return PROFILE_SPEC_NAME;
    }

}
