/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.javax.slee.profile.AddressProfileCMP;

import javax.slee.management.*;
import javax.slee.ComponentID;
import javax.slee.ServiceID;
import javax.slee.profile.*;
import javax.slee.Address;
import javax.slee.AddressPlan;

import com.opencloud.sleetck.lib.*;
import com.opencloud.sleetck.lib.testutils.*;
import com.opencloud.sleetck.lib.resource.TCKSbbMessage;
import com.opencloud.sleetck.lib.resource.testapi.TCKResourceListener;
import com.opencloud.sleetck.lib.resource.TCKActivityID;
import com.opencloud.sleetck.lib.resource.testapi.TCKResourceTestInterface;
import com.opencloud.sleetck.lib.resource.events.TCKResourceEventX;

import com.opencloud.sleetck.lib.testutils.jmx.*;
import com.opencloud.sleetck.lib.testutils.jmx.impl.AddressProfileProxyImpl;

import java.rmi.RemoteException;
import java.util.HashMap;
import javax.management.ObjectName;

public class Test4266Test implements SleeTCKTest {

    private static final String SERVICE_DU_PATH_PARAM = "DUPath";
    private static final int TEST_ID = 4266;

    public void init(SleeTCKTestUtils utils) {
        this.utils = utils;
    }

    /**
     * Perform the actual test.
     */

    public TCKTestResult run() throws Exception {


        // Create a Profile Table, and some Profiles.
        ProfileProvisioningMBeanProxy profileProvisioningProxy = new ProfileUtils(utils).getProfileProvisioningProxy();
        DeploymentMBeanProxy duProxy = utils.getDeploymentMBeanProxy();
        DeployableUnitDescriptor duDesc = duProxy.getDescriptor(duID);

        String addressProfileTable = null;
        ProfileSpecificationID profileSpecID = null;

        ComponentID components[] = duDesc.getComponents();
        for (int i = 0; i < components.length; i++) {
            if (components[i] instanceof ServiceID) {
                ServiceDescriptor serviceDesc = (ServiceDescriptor) duProxy.getDescriptor(components[i]);
                addressProfileTable = serviceDesc.getAddressProfileTable();
            }
        }

        // Need the ProfileSpecificationID of the javax.slee.AddressProfileCMP profile.
        ProfileSpecificationID profiles[] = duProxy.getProfileSpecifications();
        for (int i = 0; i < profiles.length; i++) {
            ProfileSpecificationDescriptor desc = (ProfileSpecificationDescriptor) duProxy.getDescriptor(profiles[i]);
            utils.getLog().fine("Current profile spec cmp interface name is " + desc.getCMPInterfaceName());
            if (desc.getCMPInterfaceName().equals("javax.slee.profile.AddressProfileCMP"))
                profileSpecID = profiles[i];
        }

        if (addressProfileTable == null) {
            utils.getLog().fine("No AddressProfileTable found in Service.");
            throw new Exception("No AddressProfileTable found in Service.");
        }

        if (profileSpecID == null) {
            utils.getLog().fine("Unable to find ProfileSpecificationID for Address Profile.");
            throw new Exception("Unable to find ProfileSpecificationID for Address Profile.");
        }

        // Create the Address Profile Table
        profileProvisioningProxy.createProfileTable(profileSpecID, addressProfileTable);

        // Create a profile in the Address Profile Table
        ObjectName profileMBeanName = profileProvisioningProxy.createProfile(addressProfileTable, Test4266Sbb.PROFILE_NAME);
        AddressProfileProxy profileProxy = new AddressProfileProxyImpl(profileMBeanName,utils.getMBeanFacade());

        // Invoke setAddresses
        Address addresses[] = new Address[1];
        addresses[0] = new Address(AddressPlan.IP, "127.0.0.1");
        profileProxy.setAddresses(addresses);
        profileProxy.commitProfile();
        profileProxy.closeProfile();

        result = new FutureResult(utils.getLog());

        TCKResourceTestInterface resource = utils.getResourceInterface();
        activityID = resource.createActivity("Test4266InitialActivity");
        resource.fireEvent(TCKResourceEventX.X1, TCKResourceEventX.X1, activityID, null);

        return result.waitForResultOrFail(utils.getTestTimeout(), "Timeout waiting for test result.", TEST_ID);
    }

    /**
     * Do all the pre-run configuration of the test.
     */

    public void setUp() throws Exception {

        utils.getLog().fine("Connecting to resource");
        resourceListener = new TCKResourceListenerImpl();
        utils.getResourceInterface().setResourceListener(resourceListener);
        utils.getLog().fine("Installing and activating service");

        // Install the Deployable Units
        String duPath = utils.getTestParams().getProperty(SERVICE_DU_PATH_PARAM);
        duID = utils.install(duPath);

        // Activate the DU
        utils.activateServices(duID, true);

    }

    /**
     * Clean up after the test.
     */

    public void tearDown() throws Exception {
        ProfileProvisioningMBeanProxy profileProxy = new ProfileUtils(utils).getProfileProvisioningProxy();
        try {
            DeploymentMBeanProxy duProxy = utils.getDeploymentMBeanProxy();

            DeployableUnitDescriptor duDesc = duProxy.getDescriptor(duID);
            ComponentID components[] = duDesc.getComponents();
            for (int i = 0; i < components.length; i++) {
                if (components[i] instanceof ServiceID) {
                    ServiceDescriptor serviceDesc = (ServiceDescriptor) duProxy.getDescriptor(components[i]);
                    String addressProfileTable = serviceDesc.getAddressProfileTable();
                    profileProxy.removeProfile(addressProfileTable, Test4266Sbb.PROFILE_NAME);
                    profileProxy.removeProfileTable(addressProfileTable);
                }
            }

        } catch (Exception e) {
        }

        utils.getLog().fine("Disconnecting from resource");
        utils.getResourceInterface().clearActivities();
        utils.getResourceInterface().removeResourceListener();
        utils.getLog().fine("Deactivating and uninstalling service");
        utils.deactivateAllServices();
        utils.uninstallAll();
    }

    private class TCKResourceListenerImpl extends BaseTCKResourceListener {
        public synchronized void onSbbMessage(TCKSbbMessage message, TCKActivityID calledActivity) throws RemoteException {

            HashMap map = (HashMap) message.getMessage();
            String type = (String) map.get("Type");
            if (type.equals("Result")) {
                Boolean passed = (Boolean) map.get("Result");
                String msgString = (String) map.get("Message");

                utils.getLog().info("Received message from SBB.");

                if (passed.booleanValue() == true)
                    result.setPassed();
                else
                    result.setFailed(((Integer) map.get("ID")).intValue(), msgString);
            } else {
                utils.getLog().info("Unexpected test message.");
                result.setError("Received unexpected message from SBB.");
            }
        }

        public void onException(Exception e) throws RemoteException {
            utils.getLog().warning("Received exception from SBB");
            utils.getLog().warning(e);
            result.setError(e);
        }
    }

    private SleeTCKTestUtils utils;
    private TCKResourceListener resourceListener;
    private FutureResult result;
    private DeployableUnitID duID;
    private TCKActivityID activityID;
}
