/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.events.eventrouting;

import javax.slee.ActivityContextInterface;
import javax.slee.InitialEventSelector;
import javax.slee.facilities.Tracer;

import com.opencloud.sleetck.lib.resource.events.TCKResourceEventX;
import com.opencloud.sleetck.lib.sbbutils.BaseTCKSbb;
import com.opencloud.sleetck.lib.sbbutils.TCKSbbUtils;

/**
 * AssertionID(1108095): Test The default-priority element of the service 
 * deployment descriptor element of a Service specifies the initial event 
 * delivery priority of the root SBB entities instantiated by the SLEE for 
 * the Service. 
 */
public abstract class Test1108095SecondSbb extends BaseTCKSbb {

    public InitialEventSelector initialEventSelectorMethod(InitialEventSelector ies) {
        // set the custom name variable to true to test that this value is not held accross invocations
        ies.setCustomName("test");
        return ies;
    }

    // Initial event method.
    public void onTCKResourceEventX1(TCKResourceEventX event, ActivityContextInterface aci) {
        try {
            tracer = getSbbContext().getTracer("com.test");
            tracer.info("Sbb2: Received a TCK event " + event);

            TCKSbbUtils.getResourceInterface().sendSbbMessage(new Integer(15));
        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }
    };

    private Tracer tracer;
}
