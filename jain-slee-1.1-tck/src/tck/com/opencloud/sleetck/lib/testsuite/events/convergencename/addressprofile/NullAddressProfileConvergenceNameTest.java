/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.events.convergencename.addressprofile;

import com.opencloud.sleetck.lib.TCKTestResult;
import com.opencloud.sleetck.lib.testsuite.events.convergencename.AbstractConvergenceNameTest;
import com.opencloud.sleetck.lib.testsuite.events.convergencename.InitialEventSelectorParameters;
import com.opencloud.sleetck.lib.resource.TCKActivityID;
import com.opencloud.sleetck.lib.resource.events.TCKResourceEventX;
import com.opencloud.sleetck.lib.resource.testapi.TCKResourceTestInterface;
import com.opencloud.sleetck.lib.testutils.ProfileUtils;
import com.opencloud.sleetck.lib.testutils.jmx.ProfileProvisioningMBeanProxy;

/**
 * Checks that the address profile attribute will be set to null if the address attribute is null.
 */
public class NullAddressProfileConvergenceNameTest extends AbstractConvergenceNameTest {

    // -- Constants -- //

    private static final String ADDRESS_PROFILE_TABLE = "tck.AddressAndProfileTestProfile";

    public TCKTestResult run() throws Exception {
        // Configure IES params for ActivityContext,Address,AddressProfile
        //  (only effective if the test description configures a service that utilises an IES method).
        InitialEventSelectorParameters iesParams = new InitialEventSelectorParameters(true, true, true, false, false,
                                                                                      null, false, false, false, null);
        TCKResourceTestInterface resource = utils().getResourceInterface();

        String eventType = TCKResourceEventX.X1;
        TCKActivityID activityID = resource.createActivity(ACTIVITY_ID_PREFIX + (currentActivityIDSuffix++));

        // Send two events with a null address. This should cause the AddressProfile attribute to be null,
        // and an SBB to be created for only the first of the two events
        sendEventAndWait(eventType, "1", activityID, null, "1", iesParams);
        sendEventAndWait(eventType, "2", activityID, null, "1", iesParams);

        return TCKTestResult.passed();
    }

    public void setUp() throws Exception {
        // setup the address profile table before installing the service
        profileUtils = new ProfileUtils(utils());
        profileProvisioningProxy = profileUtils.getProfileProvisioningProxy();
        profileUtils.createStandardAddressProfileTable(ADDRESS_PROFILE_TABLE);
        super.setUp();
    }

    public void tearDown() throws Exception {
        super.tearDown();
        if (profileUtils != null) {
            try {
                profileProvisioningProxy.removeProfileTable(ADDRESS_PROFILE_TABLE);
            } catch (Exception e) {
                utils().getLog().warning(e);
            }
        }
    }

    private int currentActivityIDSuffix = 0;
    private ProfileProvisioningMBeanProxy profileProvisioningProxy;
    private ProfileUtils profileUtils;

}
