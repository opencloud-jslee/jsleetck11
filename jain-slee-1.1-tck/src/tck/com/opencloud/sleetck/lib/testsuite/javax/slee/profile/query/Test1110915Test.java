/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.javax.slee.profile.query;

import java.util.Locale;

import javax.slee.profile.ProfileSpecificationID;
import javax.slee.profile.query.And;
import javax.slee.profile.query.CompositeQueryExpression;
import javax.slee.profile.query.Equals;
import javax.slee.profile.query.GreaterThan;
import javax.slee.profile.query.GreaterThanOrEquals;
import javax.slee.profile.query.HasPrefix;
import javax.slee.profile.query.LessThan;
import javax.slee.profile.query.LessThanOrEquals;
import javax.slee.profile.query.LongestPrefixMatch;
import javax.slee.profile.query.Not;
import javax.slee.profile.query.NotEquals;
import javax.slee.profile.query.Or;
import javax.slee.profile.query.OrderedQueryExpression;
import javax.slee.profile.query.QueryCollator;
import javax.slee.profile.query.QueryExpression;
import javax.slee.profile.query.RangeMatch;
import javax.slee.profile.query.SimpleQueryExpression;

import com.opencloud.sleetck.lib.AbstractSleeTCKTest;
import com.opencloud.sleetck.lib.TCKTestFailureException;
import com.opencloud.sleetck.lib.TCKTestResult;
import com.opencloud.sleetck.lib.infra.SleeTCKComponentConstants;
import com.opencloud.sleetck.lib.testutils.ProfileUtils;
import com.opencloud.sleetck.lib.testutils.jmx.ProfileProvisioningMBeanProxy;


public class Test1110915Test extends AbstractSleeTCKTest {

    private static final String SERVICE_DU_PATH_PARAM= "serviceDUPath";

    private static final String SPEC_NAME = "Test1110915Profile";
    private static final String SPEC_VERSION = "1.0";
    private static final String PROFILE_TABLE_NAME = "Test1110915ProfileTable";

    private static final int TEST_ID = 1110915;


    /**
     * Javadoc test for javax.slee.profile.query package
     */
    public TCKTestResult run() throws Exception {

        //Create profile tables
        ProfileSpecificationID specID = new ProfileSpecificationID(SPEC_NAME, SleeTCKComponentConstants.TCK_VENDOR, SPEC_VERSION);
        profileProvisioning.createProfileTable(specID, PROFILE_TABLE_NAME);
        getLog().fine("Added profile table "+PROFILE_TABLE_NAME);

        try {

            testSimpleQueryExpression();

            testEquals();

            testNotEquals();

            testOrderedQueryExpression();

            testGreaterThan();

            testGreaterThanOrEquals();

            testLessThan();

            testLessThanOrEquals();

            testHasPrefix();

            testLongestPrefixMatch();

            testRangeMatch();

            testCompositeQuery();

            testAnd();

            testOr();

            testNot();

            testRangeMatch();

        } catch (TCKTestFailureException e) {
            return TCKTestResult.failed(e);
        }


        return TCKTestResult.passed();
    }

    private void testSimpleQueryExpression() throws Exception {

        QueryCollator collator = new QueryCollator(new Locale("en", "NZ"));

        //11101103: constructor
        MySimpleQueryExpression simple;
        MySimpleQueryExpression simpleNullCollator;
        try {
            simple = new MySimpleQueryExpression("stringValue", "42", collator);
            getLog().fine("MySimpleQueryExpression(String, Object, QueryCollator) worked fine.");
        } catch (Exception e) {
            throw new TCKTestFailureException(11101103, "MySimpleQueryExpression(String, Object, QueryCollator) threw exception.",e);
        }

        //11101104: collator may be null
        try {
            simpleNullCollator = new MySimpleQueryExpression("stringValue", "42", null);
            getLog().fine("MySimpleQueryExpression(String, Object, null) worked fine.");
        } catch (Exception e) {
            throw new TCKTestFailureException(11101104, "MySimpleQueryExpression(String, Object, null) threw exception.",e);
        }

        //11101105: NullPointerException if either attrName or attrValue is null
        try {
            new MySimpleQueryExpression(null, "42", collator);
            throw new TCKTestFailureException(11101105, "NullPointerException should have been thrown for 'SimpleQueryExpression(null, String, QueryCollator)'");
        } catch (NullPointerException e) {
            getLog().fine("SimpleQueryExpression(null, String, QueryCollator) threw exception as expected: "+e);
        } catch (Exception e) {
            if (e instanceof TCKTestFailureException)
                throw (TCKTestFailureException)e;
            throw new TCKTestFailureException(11101105, "Wrong type of exception thrown: "+e.getClass().getName()+" Expected: java.lang.NullPointerException",e);
        }
        try {
            new MySimpleQueryExpression("stringValue", null, collator);
            throw new TCKTestFailureException(11101105, "NullPointerException should have been thrown for 'SimpleQueryExpression(String, null, QueryCollator)'");
        } catch (NullPointerException e) {
            getLog().fine("SimpleQueryExpression(String, null, QueryCollator) threw exception as expected: "+e);
        } catch (Exception e) {
            if (e instanceof TCKTestFailureException)
                throw (TCKTestFailureException)e;
            throw new TCKTestFailureException(11101105, "Wrong type of exception thrown: "+e.getClass().getName()+" Expected: java.lang.NullPointerException",e);
        }

        //11101107: getAttributeName
        if (simple.getAttributeName().equals("stringValue"))
            getLog().fine("getAttributeName() worked fine.");
        else
            throw new TCKTestFailureException(11101107, "getAttributeName() returned wrong value: '"+simple.getAttributeName()+"'. Expected 'stringValue'.");

        //11101109: getAttributeValue
        if (simple.getAttributeValue().equals("42"))
            getLog().fine("getAttributeValue() worked fine.");
        else
            throw new TCKTestFailureException(11101109, "getAttributeValue() returned wrong value: '"+simple.getAttributeValue()+"'. Expected '42'.");

        //11101111: getCollator returns the collator or null if none has been set
        if (simple.getCollator()!= null && simple.getCollator().equals(collator) && simpleNullCollator.getCollator()==null)
            getLog().fine("getCollator() worked fine.");
        else
            throw new TCKTestFailureException(11101111, "getCollator() returned unexpected values. simple.getCollator():"+simple.getCollator()+". simpleNullCollator.getCollator():"+simpleNullCollator.getCollator());

    }

    private void testOrderedQueryExpression() throws Exception {

        QueryCollator collator = new QueryCollator(new Locale("en", "NZ"));

        //11101041: constructor use
        MyOrderedQueryExpression ordered;
        try {
            ordered = new MyOrderedQueryExpression("stringValue", "42", collator);
            getLog().fine("OrderedQueryExpression(String, Object, QueryCollator) worked fine.");
        } catch (Exception e) {
            throw new TCKTestFailureException(11101041, "OrderedQueryExpression(String, Object, QueryCollator) threw exception.",e);
        }
        try {
            new MyOrderedQueryExpression("stringValue", "42", null);
            getLog().fine("OrderedQueryExpression(String, Object, null) worked fine.");
        } catch (Exception e) {
            throw new TCKTestFailureException(11101041, "OrderedQueryExpression(String, Object, null) threw exception.",e);
        }

        //11101042: NullPointerException if either attrName or attrValue is null
        try {
            new MyOrderedQueryExpression(null, "42", collator);
            throw new TCKTestFailureException(11101042, "NullPointerException should have been thrown for 'OrderedQueryExpression(null, String, QueryCollator)'");
        } catch (NullPointerException e) {
            getLog().fine("OrderedQueryExpression(null, String, QueryCollator) threw exception as expected: "+e);
        } catch (Exception e) {
            if (e instanceof TCKTestFailureException)
                throw (TCKTestFailureException)e;
            throw new TCKTestFailureException(11101042, "Wrong type of exception thrown: "+e.getClass().getName()+" Expected: java.lang.NullPointerException",e);
        }
        try {
            new MyOrderedQueryExpression("stringValue", null, collator);
            throw new TCKTestFailureException(11101042, "NullPointerException should have been thrown for 'OrderedQueryExpression(String, null, QueryCollator)'");
        } catch (NullPointerException e) {
            getLog().fine("OrderedQueryExpression(String, null, QueryCollator) threw exception as expected: "+e);
        } catch (Exception e) {
            if (e instanceof TCKTestFailureException)
                throw (TCKTestFailureException)e;
            throw new TCKTestFailureException(11101042, "Wrong type of exception thrown: "+e.getClass().getName()+" Expected: java.lang.NullPointerException",e);
        }

        //11101043: IllegalArgumentException if attrValue does not implement Comparable
        try {
            new MyOrderedQueryExpression("myIntValue", new MyInteger(42), collator);
            throw new TCKTestFailureException(11101043, "IllegalArgumentException should have been thrown for 'OrderedQueryExpression(String, MyInteger, QueryCollator)'");
        } catch (IllegalArgumentException e) {
            getLog().fine("OrderedQueryExpression(String, MyInteger, QueryCollator) threw exception as expected: "+e);
        } catch (Exception e) {
            if (e instanceof TCKTestFailureException)
                throw (TCKTestFailureException)e;
            throw new TCKTestFailureException(11101043, "Wrong type of exception thrown: "+e.getClass().getName()+" Expected: java.lang.IllegalArgumentException",e);
        }

    }

    private void testCompositeQuery() throws Exception{

        QueryExpression ex1 = new Equals("stringValue", "42");
        QueryExpression ex2 = new Equals("intValue", new Integer(42));
        QueryExpression ex3 = new Equals("boolValue", new Boolean(true));

        //1110928: No args constructor
        MyCompositeQueryExpression composite;
        try {
            composite = new MyCompositeQueryExpression();
            getLog().fine("CompositeQueryExpression() worked fine.");
        }
        catch (Exception e) {
            throw new TCKTestFailureException(1110928, "CompositeQueryExpression() threw exception.",e);
        }

        //1110930: Add query expression
        try {
            composite.add2(ex1);
            composite.add2(ex2);
            composite.add2(ex3);
            getLog().fine("add(QueryExpression) worked fine.");
        } catch (Exception e) {
            throw new TCKTestFailureException(1110930, "add(QueryExpression) threw exception.",e);
        }

        //1110931: NullPointerException is thrown if argument is null
        try {
            composite.add2(null);
            throw new TCKTestFailureException(1110931, "NullPointerException should have been thrown for 'CompositeQueryExpression.add(null)'");
        } catch (NullPointerException e) {
            getLog().fine("add(null) threw exception as expected: "+e);
        } catch (Exception e) {
            if (e instanceof TCKTestFailureException)
                throw (TCKTestFailureException)e;
            throw new TCKTestFailureException(1110931, "Wrong type of exception thrown: "+e.getClass().getName()+" Expected: java.lang.NullPointerException",e);
        }

        //1110932: IllegalArgumentException if cyclic expression detected
        try {
            composite.add2(new Not(composite));
            throw new TCKTestFailureException(1110932, "IllegalArgumentException should have been thrown when creating cyclic expression via CompositeQuery.add()");
        }
        catch (IllegalArgumentException e) {
            getLog().fine("Caught exception as expected: "+e);
        }
        catch (Exception e) {
            if (e instanceof TCKTestFailureException)
                throw (TCKTestFailureException)e;
            throw new TCKTestFailureException(1110932, "Wrong type of exception thrown: "+e.getClass().getName()+" Expected: java.lang.IllegalArgumentException");
        }

        //1110934: returns the query expressions that were added to this CompositeQuery
        try {
            QueryExpression[] expr = composite.getExpressions();
            if (expr.length==3 &&
                    (expr[0]==ex1 || expr[0]==ex2 || expr[0]==ex3) &&
                    (expr[1]==ex1 || expr[1]==ex2 || expr[1]==ex3) &&
                    (expr[2]==ex1 || expr[2]==ex2 || expr[2]==ex3)
                    )
                getLog().fine("CompositeQuery.getExpressions worked fine.");
            else
                throw new TCKTestFailureException(1110934, "'CompositeQuery.getExpressions()' did not return the full range of nested queries.");
        }
        catch (Exception e) {
            throw new TCKTestFailureException(1110934, "'CompositeQuery.getExpressions()' threw exception: ",e);
        }

    }

    private void testRangeMatch() throws Exception {

        //11101086: Create RangeMatch query RangeMatch(String, Object, Object)
        RangeMatch rM;
        try {
            rM = new RangeMatch("intValue",new Integer(23), new Integer(42));
            getLog().fine("RangeMatch(String, Object, Object) worked fine.");
        }
        catch (Exception e) {
            throw new TCKTestFailureException(11101086, "RangeMatch(String, Object, Object) threw exception.",e);
        }
        //11101087: NullPointerException if either argument is null
        try {
            new RangeMatch("stringValue", null, "42");
            throw new TCKTestFailureException(11101087, "NullPointerException should have been thrown for 'RangeMatch(String, null, Object)'");
        }
        catch (NullPointerException e) {
            getLog().fine("Caught exception as expected: "+e);
        }
        catch (Exception e) {
            if (e instanceof TCKTestFailureException)
                throw (TCKTestFailureException)e;
            throw new TCKTestFailureException(11101087, "Wrong type of exception thrown: "+e.getClass().getName()+" Expected: java.lang.NullPointerException",e);
        }
        try {
            new RangeMatch("stringValue", "23", null);
            throw new TCKTestFailureException(11101087, "NullPointerException should have been thrown for 'RangeMatch(String, Object, null)'");
        }
        catch (NullPointerException e) {
            getLog().fine("Caught exception as expected: "+e);
        }
        catch (Exception e) {
            if (e instanceof TCKTestFailureException)
                throw (TCKTestFailureException)e;
            throw new TCKTestFailureException(11101087, "Wrong type of exception thrown: "+e.getClass().getName()+" Expected: java.lang.NullPointerException",e);
        }
        try {
            new RangeMatch(null, "23", "42");
            throw new TCKTestFailureException(11101087, "NullPointerException should have been thrown for 'RangeMatch(null, Object, Object)'");
        }
        catch (NullPointerException e) {
            getLog().fine("Caught exception as expected: "+e);
        }
        catch (Exception e) {
            if (e instanceof TCKTestFailureException)
                throw (TCKTestFailureException)e;
            throw new TCKTestFailureException(11101087, "Wrong type of exception thrown: "+e.getClass().getName()+" Expected: java.lang.NullPointerException",e);
        }
        //11101088: java.lang.IllegalArgumentException - if the class of toValue or fromValue
        //does not implement the java.lang.Comparable interface.
        try {
            new RangeMatch("myIntValue", new Integer(23),  new MyInteger(42));
            throw new TCKTestFailureException(11101088, "IllegalArgumentException should have been thrown for 'RangeMatch(\"myIntValue\", Integer, new MyInteger(42))'");
        }
        catch (IllegalArgumentException e) {
            getLog().fine("Caught exception as expected: "+e);
        }
        catch (Exception e) {
            if (e instanceof TCKTestFailureException)
                throw (TCKTestFailureException)e;
            throw new TCKTestFailureException(11101088, "Wrong type of exception thrown: "+e.getClass().getName()+" Expected: java.lang.NullPointerException",e);
        }
        try {
            new RangeMatch("myIntValue", new MyInteger(23),  new Integer(42));
            throw new TCKTestFailureException(11101088, "IllegalArgumentException should have been thrown for 'RangeMatch(\"myIntValue\", new MyInteger(42), Integer)'");
        }
        catch (IllegalArgumentException e) {
            getLog().fine("Caught exception as expected: "+e);
        }
        catch (Exception e) {
            if (e instanceof TCKTestFailureException)
                throw (TCKTestFailureException)e;
            throw new TCKTestFailureException(11101088, "Wrong type of exception thrown: "+e.getClass().getName()+" Expected: java.lang.NullPointerException",e);
        }


        QueryCollator collator = new QueryCollator(new Locale("en", "NZ"));
        //11101090: RangeMatch(String, String, String, QueryCollator)
        try {
            rM = new RangeMatch("stringValue", "23", "42", collator);
            getLog().fine("RangeMatch(String, String, String, QueryCollator) worked fine.");
        }
        catch (Exception e) {
            throw new TCKTestFailureException(11101090, "RangeMatch(String, String, sTring, QueryCollator) threw exception.",e);
        }
        try {
            rM = new RangeMatch("stringValue", "23", "42", null);
            getLog().fine("RangeMatch(String, String, String, null) worked fine.");
        }
        catch (Exception e) {
            throw new TCKTestFailureException(11101090, "RangeMatch(String, String, String, null) threw exception.",e);
        }
        //11101091: NullPointerException if either toValue, fromValue or attrName argument is null
        try {
            new RangeMatch("stringValue", null, "42", collator);
            throw new TCKTestFailureException(11101091, "NullPointerException should have been thrown for 'RangeMatch(String, null, String, QueryCollator)'");
        }
        catch (NullPointerException e) {
            getLog().fine("Caught exception as expected: "+e);
        }
        catch (Exception e) {
            if (e instanceof TCKTestFailureException)
                throw (TCKTestFailureException)e;
            throw new TCKTestFailureException(11101091, "Wrong type of exception thrown: "+e.getClass().getName()+" Expected: java.lang.NullPointerException",e);
        }
        try {
            new RangeMatch("stringValue", "23", null, collator);
            throw new TCKTestFailureException(11101091, "NullPointerException should have been thrown for 'RangeMatch(String, String, null, QueryCollator)'");
        }
        catch (NullPointerException e) {
            getLog().fine("Caught exception as expected: "+e);
        }
        catch (Exception e) {
            if (e instanceof TCKTestFailureException)
                throw (TCKTestFailureException)e;
            throw new TCKTestFailureException(11101091, "Wrong type of exception thrown: "+e.getClass().getName()+" Expected: java.lang.NullPointerException",e);
        }
        try {
            new RangeMatch(null, "23", "42", collator);
            throw new TCKTestFailureException(11101091, "NullPointerException should have been thrown for 'RangeMatch(null, String, String, QueryCollator)'");
        }
        catch (NullPointerException e) {
            getLog().fine("Caught exception as expected: "+e);
        }
        catch (Exception e) {
            if (e instanceof TCKTestFailureException)
                throw (TCKTestFailureException)e;
            throw new TCKTestFailureException(11101091, "Wrong type of exception thrown: "+e.getClass().getName()+" Expected: java.lang.NullPointerException",e);
        }

        //11101094: getAttributeName
        if (rM.getAttributeName().equals("stringValue"))
            getLog().fine("RangeMatch.getAttributeName() worked fine.");
        else
            throw new TCKTestFailureException(11101094, "RangeMatch.getAttributeName() did not return the expected attribute name 'stringValue' but returned: "+rM.getAttributeName());

        //11101096: getFromValue
        Object value;
        value = rM.getFromValue();
        if (value instanceof String && "23".equals(value))
            getLog().fine("RangeMatch.getFromValue() returned the expected value.");
        else
            throw new TCKTestFailureException(11101096, "RangeMatch.getFromValue() did not return the expected value '23' but returned: "+value);

        //11101098: getToValue
        value = rM.getToValue();
        if (value instanceof String && "42".equals(value))
            getLog().fine("RangeMatch.getToValue() returned the expected value.");
        else
            throw new TCKTestFailureException(11101098, "RangeMatch.getToValue() did not return the expected value '42' but returned: "+value);

    }

    private void testLongestPrefixMatch() throws Exception {

        //11101001: Create LongestPrefixMatch query LongestPrefixMatch(String, String)
        LongestPrefixMatch lPM;
        try {
            lPM = new LongestPrefixMatch("stringValue","42");
            getLog().fine("LongestPrefixMatch(String, String) worked fine.");
        }
        catch (Exception e) {
            throw new TCKTestFailureException(11101001, "LongestPrefixMatch(String, String) threw exception.",e);
        }
        //11101002: NullPointerException if either argument is null
        try {
            new LongestPrefixMatch("stringValue", null);
            throw new TCKTestFailureException(11101002, "NullPointerException should have been thrown for 'LongestPrefixMatch(String, null)'");
        }
        catch (NullPointerException e) {
            getLog().fine("Caught exception as expected: "+e);
        }
        catch (Exception e) {
            if (e instanceof TCKTestFailureException)
                throw (TCKTestFailureException)e;
            throw new TCKTestFailureException(11101002, "Wrong type of exception thrown: "+e.getClass().getName()+" Expected: java.lang.NullPointerException",e);
        }
        try {
            new LongestPrefixMatch(null, "42");
            throw new TCKTestFailureException(11101002, "NullPointerException should have been thrown for 'LongestPrefixMatch(null, Object)'");
        }
        catch (NullPointerException e) {
            getLog().fine("Caught exception as expected: "+e);
        }
        catch (Exception e) {
            if (e instanceof TCKTestFailureException)
                throw (TCKTestFailureException)e;
            throw new TCKTestFailureException(11101002, "Wrong type of exception thrown: "+e.getClass().getName()+" Expected: java.lang.NullPointerException",e);
        }

        QueryCollator collator = new QueryCollator(new Locale("en", "NZ"));
        //11101004: LongestPrefixMatch(String, String, QueryCollator)
        try {
            lPM = new LongestPrefixMatch("stringValue","42",collator);
            getLog().fine("LongestPrefixMatch(String, String, QueryCollator) worked fine.");
        }
        catch (Exception e) {
            throw new TCKTestFailureException(11101004, "LongestPrefixMatch(String, String, Collator) threw exception.",e);
        }
        try {
            lPM = new LongestPrefixMatch("stringValue","42", null);
            getLog().fine("LongestPrefixMatch(String, String, null) worked fine.");
        }
        catch (Exception e) {
            throw new TCKTestFailureException(11101004, "LongestPrefixMatch(String, String, null) threw exception.",e);
        }
        //11101005: NullPointerException if either argument is null
        try {
            new LongestPrefixMatch("stringValue", null, collator);
            throw new TCKTestFailureException(11101005, "NullPointerException should have been thrown for 'LongestPrefixMatch(String, null, QueryCollator)'");
        }
        catch (NullPointerException e) {
            getLog().fine("Caught exception as expected: "+e);
        }
        catch (Exception e) {
            if (e instanceof TCKTestFailureException)
                throw (TCKTestFailureException)e;
            throw new TCKTestFailureException(11101005, "Wrong type of exception thrown: "+e.getClass().getName()+" Expected: java.lang.NullPointerException",e);
        }
        try {
            new LongestPrefixMatch(null, "42", collator);
            throw new TCKTestFailureException(11101005, "NullPointerException should have been thrown for 'LongestPrefixMatch(null, String, QueryCollator)'");
        }
        catch (NullPointerException e) {
            getLog().fine("Caught exception as expected: "+e);
        }
        catch (Exception e) {
            if (e instanceof TCKTestFailureException)
                throw (TCKTestFailureException)e;
            throw new TCKTestFailureException(11101005, "Wrong type of exception thrown: "+e.getClass().getName()+" Expected: java.lang.NullPointerException",e);
        }

    }

    private void testHasPrefix() throws Exception {

        //1110969: Create HasPrefix query HasPrefix(String, String)
        HasPrefix hP;
        try {
            hP = new HasPrefix("stringValue","42");
            getLog().fine("HasPrefix(String, String) worked fine.");
        }
        catch (Exception e) {
            throw new TCKTestFailureException(1110969, "HasPrefix(String, String) threw exception.",e);
        }
        //1110970: NullPointerException if either argument is null
        try {
            new HasPrefix("stringValue", null);
            throw new TCKTestFailureException(1110970, "NullPointerException should have been thrown for 'HasPrefix(String, null)'");
        }
        catch (NullPointerException e) {
            getLog().fine("Caught exception as expected: "+e);
        }
        catch (Exception e) {
            if (e instanceof TCKTestFailureException)
                throw (TCKTestFailureException)e;
            throw new TCKTestFailureException(1110970, "Wrong type of exception thrown: "+e.getClass().getName()+" Expected: java.lang.NullPointerException",e);
        }
        try {
            new HasPrefix(null, "42");
            throw new TCKTestFailureException(1110970, "NullPointerException should have been thrown for 'HasPrefix(null, Object)'");
        }
        catch (NullPointerException e) {
            getLog().fine("Caught exception as expected: "+e);
        }
        catch (Exception e) {
            if (e instanceof TCKTestFailureException)
                throw (TCKTestFailureException)e;
            throw new TCKTestFailureException(1110970, "Wrong type of exception thrown: "+e.getClass().getName()+" Expected: java.lang.NullPointerException",e);
        }

        QueryCollator collator = new QueryCollator(new Locale("en", "NZ"));
        //1110972: HasPrefix(String, String, Collator)
        try {
            hP = new HasPrefix("stringValue","42",collator);
            getLog().fine("HasPrefix(String, String, QueryCollator) worked fine.");
        }
        catch (Exception e) {
            throw new TCKTestFailureException(1110972, "HasPrefix(String, String, Collator) threw exception.",e);
        }
        try {
            hP = new HasPrefix("stringValue","42", null);
            getLog().fine("HasPrefix(String, String, null) worked fine.");
        }
        catch (Exception e) {
            throw new TCKTestFailureException(1110972, "HasPrefix(String, String, null) threw exception.",e);
        }
        //1110973: NullPointerException if either argument is null
        try {
            new HasPrefix("stringValue", null, collator);
            throw new TCKTestFailureException(1110973, "NullPointerException should have been thrown for 'HasPrefix(String, null, QueryCollator)'");
        }
        catch (NullPointerException e) {
            getLog().fine("Caught exception as expected: "+e);
        }
        catch (Exception e) {
            if (e instanceof TCKTestFailureException)
                throw (TCKTestFailureException)e;
            throw new TCKTestFailureException(1110973, "Wrong type of exception thrown: "+e.getClass().getName()+" Expected: java.lang.NullPointerException",e);
        }
        try {
            new HasPrefix(null, "42", collator);
            throw new TCKTestFailureException(1110973, "NullPointerException should have been thrown for 'HasPrefix(null, String, QueryCollator)'");
        }
        catch (NullPointerException e) {
            getLog().fine("Caught exception as expected: "+e);
        }
        catch (Exception e) {
            if (e instanceof TCKTestFailureException)
                throw (TCKTestFailureException)e;
            throw new TCKTestFailureException(1110973, "Wrong type of exception thrown: "+e.getClass().getName()+" Expected: java.lang.NullPointerException",e);
        }

    }

    private void testLessThanOrEquals() throws Exception {

        //1110990: Create LessThanOrEquals query LessThanOrEquals(String, Object)
        LessThanOrEquals lT;
        try {
            lT = new LessThanOrEquals("intValue",new Integer(42));
            getLog().fine("LessThanOrEquals(String, Object) worked fine.");
        }
        catch (Exception e) {
            throw new TCKTestFailureException(1110990, "LessThanOrEquals(String, Object) threw exception.",e);
        }
        //1110991: NullPointerException if either argument is null
        try {
            new LessThanOrEquals("stringValue", null);
            throw new TCKTestFailureException(1110991, "NullPointerException should have been thrown for 'LessThanOrEquals(String, null)'");
        }
        catch (NullPointerException e) {
            getLog().fine("Caught exception as expected: "+e);
        }
        catch (Exception e) {
            if (e instanceof TCKTestFailureException)
                throw (TCKTestFailureException)e;
            throw new TCKTestFailureException(1110991, "Wrong type of exception thrown: "+e.getClass().getName()+" Expected: java.lang.NullPointerException",e);
        }
        try {
            new LessThanOrEquals(null, "42");
            throw new TCKTestFailureException(1110991, "NullPointerException should have been thrown for 'LessThanOrEquals(null, Object)'");
        }
        catch (NullPointerException e) {
            getLog().fine("Caught exception as expected: "+e);
        }
        catch (Exception e) {
            if (e instanceof TCKTestFailureException)
                throw (TCKTestFailureException)e;
            throw new TCKTestFailureException(1110991, "Wrong type of exception thrown: "+e.getClass().getName()+" Expected: java.lang.NullPointerException",e);
        }
        //1110992: java.lang.IllegalArgumentException - if the class of attrValue
        //does not implement the java.lang.Comparable interface.
        try {
            new LessThanOrEquals("myIntValue", new MyInteger(42));
            throw new TCKTestFailureException(1110992, "IllegalArgumentException should have been thrown for 'LessThanOrEquals(\"myIntValue\", new MyInteger(42))'");
        }
        catch (IllegalArgumentException e) {
            getLog().fine("Caught exception as expected: "+e);
        }
        catch (Exception e) {
            if (e instanceof TCKTestFailureException)
                throw (TCKTestFailureException)e;
            throw new TCKTestFailureException(1110992, "Wrong type of exception thrown: "+e.getClass().getName()+" Expected: java.lang.NullPointerException",e);
        }


        QueryCollator collator = new QueryCollator(new Locale("en", "NZ"));
        //1110994: LessThanOrEquals(String, String, Collator)
        try {
            lT = new LessThanOrEquals("stringValue","42",collator);
            getLog().fine("LessThanOrEquals(String, String, QueryCollator) worked fine.");
        }
        catch (Exception e) {
            throw new TCKTestFailureException(1110994, "LessThanOrEquals(String, String, QueryCollator) threw exception.",e);
        }
        try {
            lT = new LessThanOrEquals("stringValue","42", null);
            getLog().fine("LessThanOrEquals(String, String, null) worked fine.");
        }
        catch (Exception e) {
            throw new TCKTestFailureException(1110994, "LessThanOrEquals(String, String, null) threw exception.",e);
        }
        //1110995: NullPointerException if either argument is null
        try {
            new LessThanOrEquals("stringValue", null, collator);
            throw new TCKTestFailureException(1110995, "NullPointerException should have been thrown for 'LessThanOrEquals(String, null, QueryCollator)'");
        }
        catch (NullPointerException e) {
            getLog().fine("Caught exception as expected: "+e);
        }
        catch (Exception e) {
            if (e instanceof TCKTestFailureException)
                throw (TCKTestFailureException)e;
            throw new TCKTestFailureException(1110995, "Wrong type of exception thrown: "+e.getClass().getName()+" Expected: java.lang.NullPointerException",e);
        }
        try {
            new LessThanOrEquals(null, "42", collator);
            throw new TCKTestFailureException(1110995, "NullPointerException should have been thrown for 'LessThanOrEquals(null, String, QueryCollator)'");
        }
        catch (NullPointerException e) {
            getLog().fine("Caught exception as expected: "+e);
        }
        catch (Exception e) {
            if (e instanceof TCKTestFailureException)
                throw (TCKTestFailureException)e;
            throw new TCKTestFailureException(1110995, "Wrong type of exception thrown: "+e.getClass().getName()+" Expected: java.lang.NullPointerException",e);
        }

    }


    private void testLessThan() throws Exception {

        //1110979: Create LessThan query LessThan(String, Object)
        LessThan lT;
        try {
            lT = new LessThan("intValue",new Integer(42));
            getLog().fine("LessThan(String, Object) worked fine.");
        }
        catch (Exception e) {
            throw new TCKTestFailureException(1110979, "LessThan(String, Object) threw exception.",e);
        }
        //1110980: NullPointerException if either argument is null
        try {
            new LessThan("stringValue", null);
            throw new TCKTestFailureException(1110980, "NullPointerException should have been thrown for 'LessThan(String, null)'");
        }
        catch (NullPointerException e) {
            getLog().fine("Caught exception as expected: "+e);
        }
        catch (Exception e) {
            if (e instanceof TCKTestFailureException)
                throw (TCKTestFailureException)e;
            throw new TCKTestFailureException(1110980, "Wrong type of exception thrown: "+e.getClass().getName()+" Expected: java.lang.NullPointerException",e);
        }
        try {
            new LessThan(null, "42");
            throw new TCKTestFailureException(1110980, "NullPointerException should have been thrown for 'LessThan(null, Object)'");
        }
        catch (NullPointerException e) {
            getLog().fine("Caught exception as expected: "+e);
        }
        catch (Exception e) {
            if (e instanceof TCKTestFailureException)
                throw (TCKTestFailureException)e;
            throw new TCKTestFailureException(1110980, "Wrong type of exception thrown: "+e.getClass().getName()+" Expected: java.lang.NullPointerException",e);
        }
        //1110981: java.lang.IllegalArgumentException - if the class of attrValue
        //does not implement the java.lang.Comparable interface.
        try {
            new LessThan("myIntValue", new MyInteger(42));
            throw new TCKTestFailureException(1110981, "IllegalArgumentException should have been thrown for 'LessThan(\"myIntValue\", new MyInteger(42))'");
        }
        catch (IllegalArgumentException e) {
            getLog().fine("Caught exception as expected: "+e);
        }
        catch (Exception e) {
            if (e instanceof TCKTestFailureException)
                throw (TCKTestFailureException)e;
            throw new TCKTestFailureException(1110981, "Wrong type of exception thrown: "+e.getClass().getName()+" Expected: java.lang.NullPointerException",e);
        }


        QueryCollator collator = new QueryCollator(new Locale("en", "NZ"));
        //1110983: LessThan(String, String, Collator)
        try {
            lT = new LessThan("stringValue","42",collator);
            getLog().fine("LessThan(String, String, QueryCollator) worked fine.");
        }
        catch (Exception e) {
            throw new TCKTestFailureException(1110983, "LessThan(String, String, QueryCollator) threw exception.",e);
        }
        try {
            lT = new LessThan("stringValue","42", null);
            getLog().fine("LessThan(String, String, null) worked fine.");
        }
        catch (Exception e) {
            throw new TCKTestFailureException(1110983, "LessThan(String, String, null) threw exception.",e);
        }
        //1110984: NullPointerException if either argument is null
        try {
            new LessThan("stringValue", null, collator);
            throw new TCKTestFailureException(1110984, "NullPointerException should have been thrown for 'LessThan(String, null, QueryCollator)'");
        }
        catch (NullPointerException e) {
            getLog().fine("Caught exception as expected: "+e);
        }
        catch (Exception e) {
            if (e instanceof TCKTestFailureException)
                throw (TCKTestFailureException)e;
            throw new TCKTestFailureException(1110984, "Wrong type of exception thrown: "+e.getClass().getName()+" Expected: java.lang.NullPointerException",e);
        }
        try {
            new LessThan(null, "42", collator);
            throw new TCKTestFailureException(1110984, "NullPointerException should have been thrown for 'LessThan(null, String, QueryCollator)'");
        }
        catch (NullPointerException e) {
            getLog().fine("Caught exception as expected: "+e);
        }
        catch (Exception e) {
            if (e instanceof TCKTestFailureException)
                throw (TCKTestFailureException)e;
            throw new TCKTestFailureException(1110984, "Wrong type of exception thrown: "+e.getClass().getName()+" Expected: java.lang.NullPointerException",e);
        }

    }


    private void testGreaterThanOrEquals() throws Exception {

        //1110958: Create GreaterThanOrEquals query GreaterThanOrEquals(String, Object)
        GreaterThanOrEquals gT;
        try {
            gT = new GreaterThanOrEquals("intValue",new Integer(42));
            getLog().fine("GreaterThanOrEquals(String, Object) worked fine.");
        }
        catch (Exception e) {
            throw new TCKTestFailureException(1110958, "GreaterThanOrEquals(String, Object) threw exception.",e);
        }
        //1110959: NullPointerException if either argument is null
        try {
            new GreaterThanOrEquals("stringValue", null);
            throw new TCKTestFailureException(1110959, "NullPointerException should have been thrown for 'GreaterThanOrEquals(String, null)'");
        }
        catch (NullPointerException e) {
            getLog().fine("Caught exception as expected: "+e);
        }
        catch (Exception e) {
            if (e instanceof TCKTestFailureException)
                throw (TCKTestFailureException)e;
            throw new TCKTestFailureException(1110959, "Wrong type of exception thrown: "+e.getClass().getName()+" Expected: java.lang.NullPointerException",e);
        }
        try {
            new GreaterThanOrEquals(null, "42");
            throw new TCKTestFailureException(1110959, "NullPointerException should have been thrown for 'GreaterThanOrEquals(null, Object)'");
        }
        catch (NullPointerException e) {
            getLog().fine("Caught exception as expected: "+e);
        }
        catch (Exception e) {
            if (e instanceof TCKTestFailureException)
                throw (TCKTestFailureException)e;
            throw new TCKTestFailureException(1110959, "Wrong type of exception thrown: "+e.getClass().getName()+" Expected: java.lang.NullPointerException",e);
        }
        //1110960: java.lang.IllegalArgumentException - if the class of attrValue
        //does not implement the java.lang.Comparable interface.
        try {
            new GreaterThanOrEquals("myIntValue", new MyInteger(42));
            throw new TCKTestFailureException(1110960, "IllegalArgumentException should have been thrown for 'GreaterThanOrEquals(\"myIntValue\", new MyInteger(42))'");
        }
        catch (IllegalArgumentException e) {
            getLog().fine("Caught exception as expected: "+e);
        }
        catch (Exception e) {
            if (e instanceof TCKTestFailureException)
                throw (TCKTestFailureException)e;
            throw new TCKTestFailureException(1110960, "Wrong type of exception thrown: "+e.getClass().getName()+" Expected: java.lang.NullPointerException",e);
        }


        QueryCollator collator = new QueryCollator(new Locale("en", "NZ"));
        //1110962: GreaterThanOrEquals(String, String, Collator)
        try {
            gT = new GreaterThanOrEquals("stringValue","42",collator);
            getLog().fine("GreaterThanOrEquals(String, String, QueryCollator) worked fine.");
        }
        catch (Exception e) {
            throw new TCKTestFailureException(1110962, "GreaterThanOrEquals(String, String, QueryCollator) threw exception.",e);
        }
        try {
            gT = new GreaterThanOrEquals("stringValue","42", null);
            getLog().fine("GreaterThanOrEquals(String, String, null) worked fine.");
        }
        catch (Exception e) {
            throw new TCKTestFailureException(1110962, "GreaterThanOrEquals(String, String, null) threw exception.",e);
        }
        //1110963: NullPointerException if either argument is null
        try {
            new GreaterThanOrEquals("stringValue", null, collator);
            throw new TCKTestFailureException(1110963, "NullPointerException should have been thrown for 'GreaterThanOrEquals(String, null, QueryCollator)'");
        }
        catch (NullPointerException e) {
            getLog().fine("Caught exception as expected: "+e);
        }
        catch (Exception e) {
            if (e instanceof TCKTestFailureException)
                throw (TCKTestFailureException)e;
            throw new TCKTestFailureException(1110963, "Wrong type of exception thrown: "+e.getClass().getName()+" Expected: java.lang.NullPointerException",e);
        }
        try {
            new GreaterThanOrEquals(null, "42", collator);
            throw new TCKTestFailureException(1110963, "NullPointerException should have been thrown for 'GreaterThanOrEquals(null, String, QueryCollator)'");
        }
        catch (NullPointerException e) {
            getLog().fine("Caught exception as expected: "+e);
        }
        catch (Exception e) {
            if (e instanceof TCKTestFailureException)
                throw (TCKTestFailureException)e;
            throw new TCKTestFailureException(1110963, "Wrong type of exception thrown: "+e.getClass().getName()+" Expected: java.lang.NullPointerException",e);
        }

    }


    private void testGreaterThan() throws Exception {

        //1110947: Create GreaterThan query GreaterThan(String, Object)
        GreaterThan gT;
        try {
            gT = new GreaterThan("intValue",new Integer(42));
            getLog().fine("GreaterThan(String, Object) worked fine.");
        }
        catch (Exception e) {
            throw new TCKTestFailureException(1110947, "GreaterThan(String, Object) threw exception.",e);
        }
        //1110948: NullPointerException if either argument is null
        try {
            new GreaterThan("stringValue", null);
            throw new TCKTestFailureException(1110948, "NullPointerException should have been thrown for 'GreaterThan(String, null)'");
        }
        catch (NullPointerException e) {
            getLog().fine("Caught exception as expected: "+e);
        }
        catch (Exception e) {
            if (e instanceof TCKTestFailureException)
                throw (TCKTestFailureException)e;
            throw new TCKTestFailureException(1110948, "Wrong type of exception thrown: "+e.getClass().getName()+" Expected: java.lang.NullPointerException",e);
        }
        try {
            new GreaterThan(null, "42");
            throw new TCKTestFailureException(1110948, "NullPointerException should have been thrown for 'GreaterThan(null, Object)'");
        }
        catch (NullPointerException e) {
            getLog().fine("Caught exception as expected: "+e);
        }
        catch (Exception e) {
            if (e instanceof TCKTestFailureException)
                throw (TCKTestFailureException)e;
            throw new TCKTestFailureException(1110948, "Wrong type of exception thrown: "+e.getClass().getName()+" Expected: java.lang.NullPointerException",e);
        }
        //1110949: java.lang.IllegalArgumentException - if the class of attrValue
        //does not implement the java.lang.Comparable interface.
        try {
            new GreaterThan("myIntValue", new MyInteger(42));
            throw new TCKTestFailureException(1110949, "IllegalArgumentException should have been thrown for 'GreaterThan(\"myIntValue\", new MyInteger(42))'");
        }
        catch (IllegalArgumentException e) {
            getLog().fine("Caught exception as expected: "+e);
        }
        catch (Exception e) {
            if (e instanceof TCKTestFailureException)
                throw (TCKTestFailureException)e;
            throw new TCKTestFailureException(1110949, "Wrong type of exception thrown: "+e.getClass().getName()+" Expected: java.lang.NullPointerException",e);
        }


        QueryCollator collator = new QueryCollator(new Locale("en", "NZ"));
        //1110951: GreaterThan(String, String, Collator)
        try {
            gT = new GreaterThan("stringValue","42",collator);
            getLog().fine("GreaterThan(String, String, QueryCollator) worked fine.");
        }
        catch (Exception e) {
            throw new TCKTestFailureException(1110951, "GreaterThan(String, String, QueryCollator) threw exception.",e);
        }
        try {
            gT = new GreaterThan("stringValue","42", null);
            getLog().fine("GreaterThan(String, String, null) worked fine.");
        }
        catch (Exception e) {
            throw new TCKTestFailureException(1110951, "GreaterThan(String, String, null) threw exception.",e);
        }
        //1110952: NullPointerException if either argument is null
        try {
            new GreaterThan("stringValue", null, collator);
            throw new TCKTestFailureException(1110952, "NullPointerException should have been thrown for 'GreaterThan(String, null, QueryCollator)'");
        }
        catch (NullPointerException e) {
            getLog().fine("Caught exception as expected: "+e);
        }
        catch (Exception e) {
            if (e instanceof TCKTestFailureException)
                throw (TCKTestFailureException)e;
            throw new TCKTestFailureException(1110952, "Wrong type of exception thrown: "+e.getClass().getName()+" Expected: java.lang.NullPointerException",e);
        }
        try {
            new GreaterThan(null, "42", collator);
            throw new TCKTestFailureException(1110952, "NullPointerException should have been thrown for 'GreaterThan(null, String, QueryCollator)'");
        }
        catch (NullPointerException e) {
            getLog().fine("Caught exception as expected: "+e);
        }
        catch (Exception e) {
            if (e instanceof TCKTestFailureException)
                throw (TCKTestFailureException)e;
            throw new TCKTestFailureException(1110952, "Wrong type of exception thrown: "+e.getClass().getName()+" Expected: java.lang.NullPointerException",e);
        }

    }


    private void testEquals() throws Exception {

        //1110937: Create Equals query Equals(String, Object)
        Equals equals;
        try {
            equals = new Equals("intValue",new Integer(42));
            getLog().fine("Equals(String, Object) worked fine.");
        }
        catch (Exception e) {
            throw new TCKTestFailureException(1110937, "Equals(String, Object) threw exception.",e);
        }
        //1110938: NullPointerException if either argument is null
        try {
            new Equals("stringValue", null);
            throw new TCKTestFailureException(1110938, "NullPointerException should have been thrown for 'Equals(String, null)'");
        }
        catch (NullPointerException e) {
            getLog().fine("Caught exception as expected: "+e);
        }
        catch (Exception e) {
            if (e instanceof TCKTestFailureException)
                throw (TCKTestFailureException)e;
            throw new TCKTestFailureException(1110938, "Wrong type of exception thrown: "+e.getClass().getName()+" Expected: java.lang.NullPointerException",e);
        }
        try {
            new Equals(null, "42");
            throw new TCKTestFailureException(1110938, "NullPointerException should have been thrown for 'Equals(null, Object)'");
        }
        catch (NullPointerException e) {
            getLog().fine("Caught exception as expected: "+e);
        }
        catch (Exception e) {
            if (e instanceof TCKTestFailureException)
                throw (TCKTestFailureException)e;
            throw new TCKTestFailureException(1110938, "Wrong type of exception thrown: "+e.getClass().getName()+" Expected: java.lang.NullPointerException",e);
        }

        QueryCollator collator = new QueryCollator(new Locale("en", "NZ"));
        //1110940: Equals(String, String, Collator)
        try {
            equals = new Equals("stringValue","42",collator);
            getLog().fine("Equals(String, Object) worked fine.");
        }
        catch (Exception e) {
            throw new TCKTestFailureException(1110940, "Equals(String, String, Collator) threw exception.",e);
        }
        try {
            equals = new Equals("stringValue","42", null);
            getLog().fine("Equals(String, String, null) worked fine.");
        }
        catch (Exception e) {
            throw new TCKTestFailureException(1110940, "Equals(String, Object, null) threw exception.",e);
        }
        //1110941: NullPointerException if either argument is null
        try {
            new Equals("stringValue", null, collator);
            throw new TCKTestFailureException(1110941, "NullPointerException should have been thrown for 'Equals(String, null, QueryCollator)'");
        }
        catch (NullPointerException e) {
            getLog().fine("Caught exception as expected: "+e);
        }
        catch (Exception e) {
            if (e instanceof TCKTestFailureException)
                throw (TCKTestFailureException)e;
            throw new TCKTestFailureException(1110941, "Wrong type of exception thrown: "+e.getClass().getName()+" Expected: java.lang.NullPointerException",e);
        }
        try {
            new Equals(null, "42", collator);
            throw new TCKTestFailureException(1110941, "NullPointerException should have been thrown for 'Equals(null, String, QueryCollator)'");
        }
        catch (NullPointerException e) {
            getLog().fine("Caught exception as expected: "+e);
        }
        catch (Exception e) {
            if (e instanceof TCKTestFailureException)
                throw (TCKTestFailureException)e;
            throw new TCKTestFailureException(1110941, "Wrong type of exception thrown: "+e.getClass().getName()+" Expected: java.lang.NullPointerException",e);
        }

    }

    private void testNotEquals() throws Exception {

        //11101018: Create NotEquals query NotEquals(String, Object)
        NotEquals notEquals;
        try {
            notEquals = new NotEquals("intValue",new Integer(42));
            getLog().fine("NotEquals(String, Object) worked fine.");
        }
        catch (Exception e) {
            throw new TCKTestFailureException(11101018, "NotEquals(String, Object) threw exception.",e);
        }
        //11101019: NullPointerException if either argument is null
        try {
            new NotEquals("stringValue", null);
            throw new TCKTestFailureException(11101019, "NullPointerException should have been thrown for 'NotEquals(String, null)'");
        }
        catch (NullPointerException e) {
            getLog().fine("Caught exception as expected: "+e);
        }
        catch (Exception e) {
            if (e instanceof TCKTestFailureException)
                throw (TCKTestFailureException)e;
            throw new TCKTestFailureException(11101019, "Wrong type of exception thrown: "+e.getClass().getName()+" Expected: java.lang.NullPointerException",e);
        }
        try {
            new NotEquals(null, "42");
            throw new TCKTestFailureException(11101019, "NullPointerException should have been thrown for 'NotEquals(null, Object)'");
        }
        catch (NullPointerException e) {
            getLog().fine("Caught exception as expected: "+e);
        }
        catch (Exception e) {
            if (e instanceof TCKTestFailureException)
                throw (TCKTestFailureException)e;
            throw new TCKTestFailureException(11101019, "Wrong type of exception thrown: "+e.getClass().getName()+" Expected: java.lang.NullPointerException",e);
        }

        QueryCollator collator = new QueryCollator(new Locale("en", "NZ"));
        //11101021: NotEquals(String, String, Collator)
        try {
            notEquals = new NotEquals("stringValue","42",collator);
            getLog().fine("NotEquals(String, Object) worked fine.");
        }
        catch (Exception e) {
            throw new TCKTestFailureException(11101021, "NotEquals(String, String, Collator) threw exception.",e);
        }
        try {
            notEquals = new NotEquals("stringValue","42", null);
            getLog().fine("NotEquals(String, String, null) worked fine.");
        }
        catch (Exception e) {
            throw new TCKTestFailureException(11101021, "NotEquals(String, Object, null) threw exception.",e);
        }
        //11101022: NullPointerException if either argument is null
        try {
            new NotEquals("stringValue", null, collator);
            throw new TCKTestFailureException(11101022, "NullPointerException should have been thrown for 'NotEquals(String, null, QueryCollator)'");
        }
        catch (NullPointerException e) {
            getLog().fine("Caught exception as expected: "+e);
        }
        catch (Exception e) {
            if (e instanceof TCKTestFailureException)
                throw (TCKTestFailureException)e;
            throw new TCKTestFailureException(11101022, "Wrong type of exception thrown: "+e.getClass().getName()+" Expected: java.lang.NullPointerException",e);
        }
        try {
            new NotEquals(null, "42", collator);
            throw new TCKTestFailureException(11101022, "NullPointerException should have been thrown for 'NotEquals(null, String, QueryCollator)'");
        }
        catch (NullPointerException e) {
            getLog().fine("Caught exception as expected: "+e);
        }
        catch (Exception e) {
            if (e instanceof TCKTestFailureException)
                throw (TCKTestFailureException)e;
            throw new TCKTestFailureException(11101022, "Wrong type of exception thrown: "+e.getClass().getName()+" Expected: java.lang.NullPointerException",e);
        }

    }


    private void testAnd() throws TCKTestFailureException {

        QueryExpression ex1 = new Equals("stringValue", "42");
        QueryExpression ex2 = new Equals("intValue", new Integer(42));
        QueryExpression ex3 = new Equals("byteValue", new Byte("42"));

        //1110915: create And query from two other queries
        And and;
        try {
            and = new And(ex1, ex2);
            getLog().fine("'And(QueryExpression, QueryExpression)' works as expected.");
        }
        catch (Exception e) {
            throw new TCKTestFailureException(1110915, "Exception when using 'And(QueryExpression, QueryExpression)': ",e);
        }
        //1110916: Throws: java.lang.NullPointerException - if either argument is null.
        try {
            new And(ex1, null);
            throw new TCKTestFailureException(1110916, "NullPointerException should have been thrown for 'And(equals1, null)'");
        }
        catch (NullPointerException e) {
            getLog().fine("Caught exception as expected: "+e);
        }
        catch (Exception e) {
            if (e instanceof TCKTestFailureException)
                throw (TCKTestFailureException)e;
            throw new TCKTestFailureException(1110916, "Wrong type of exception thrown: "+e.getClass().getName()+" Expected: java.lang.NullPointerException",e);
        }
        try {
            new And(null, ex2);
            throw new TCKTestFailureException(1110916, "NullPointerException should have been thrown for 'And(null, equals2)'");
        }
        catch (NullPointerException e) {
            getLog().fine("Caught exception as expected: "+e);
        }
        catch (Exception e) {
            if (e instanceof TCKTestFailureException)
                throw (TCKTestFailureException)e;
            throw new TCKTestFailureException(1110916, "Wrong type of exception thrown: "+e.getClass().getName()+" Expected: java.lang.NullPointerException",e);
        }


        //1110918: construct And from an array of elements
        try {
            new And(new QueryExpression[]{ex1, ex2, ex3});
            getLog().fine("'And(QueryExpression[])' works as expected.");
        }
        catch (Exception e) {
            throw new TCKTestFailureException(1110918, "Exception when using 'And(QueryExpression[])': ",e);
        }
        //1110919: throws java.lang.NullPointerException if QueryExpression[] is null or contains null elements
        try {
            new And(null);
            throw new TCKTestFailureException(1110919, "NullPointerException should have been thrown for 'And(null)'");
        }
        catch (NullPointerException e) {
            getLog().fine("Caught exception as expected: "+e);
        }
        catch (Exception e) {
            if (e instanceof TCKTestFailureException)
                throw (TCKTestFailureException)e;
            throw new TCKTestFailureException(1110919, "Wrong type of exception thrown: "+e.getClass().getName()+" Expected: java.lang.NullPointerException");
        }
        try {
            new And(new QueryExpression[]{ex1, ex2, null});
            throw new TCKTestFailureException(1110919, "NullPointerException should have been thrown for 'And(null)'");
        }
        catch (NullPointerException e) {
            getLog().fine("Caught exception as expected: "+e);
        }
        catch (Exception e) {
            if (e instanceof TCKTestFailureException)
                throw (TCKTestFailureException)e;
            throw new TCKTestFailureException(1110919, "Wrong type of exception thrown: "+e.getClass().getName()+" Expected: java.lang.NullPointerException");
        }

        //?: IllegalArgumentException if less than two expressions in the array
        try {
            new And(new QueryExpression[]{ex1});
            throw new TCKTestFailureException(1110915, "IllegalArgumentException should have been thrown for 'And(QueryExpression[]{ex1})'");
        } catch (IllegalArgumentException e) {
            getLog().fine("Caught exception as expected: "+e);
        } catch (Exception e) {
            if (e instanceof TCKTestFailureException)
                throw (TCKTestFailureException)e;
            throw new TCKTestFailureException(1110915, "Wrong type of exception thrown: "+e.getClass().getName()+" Expected: java.lang.IllegalArgumentException");
        }


        //1110921: Add a query expression to this And object
        //1110922: Returns a reference to 'this'
        try {
            if (and.and(ex3)!=and)
                throw new TCKTestFailureException(1110922, "and(QueryExpression) should return reference to this.");
            else
                getLog().fine("and(QueryExpression) returned reference to 'this' as expected.");
        }
        catch (Exception e) {
            throw new TCKTestFailureException(1110921, "and(QueryExpression) caused exception: ",e);
        }
        //1110923: NullPointerException if argument is null
        try {
            and.and(null);
            throw new TCKTestFailureException(1110923, "NullPointerException should have been thrown for 'And.and(null)'");
        }
        catch (NullPointerException e) {
            getLog().fine("Caught exception as expected: "+e);
        }
        catch (Exception e) {
            if (e instanceof TCKTestFailureException)
                throw (TCKTestFailureException)e;
            throw new TCKTestFailureException(1110923, "Wrong type of exception thrown: "+e.getClass().getName()+" Expected: java.lang.NullPointerException");
        }
        //1110924:  java.lang.IllegalArgumentException - if adding the query expression
        //to this composite expression would generate a cyclic expression.
        try {
            and.and(new Not(and));
            throw new TCKTestFailureException(1110924, "IllegalArgumentException should have been thrown when creating cyclic expression via And.and()");
        }
        catch (IllegalArgumentException e) {
            getLog().fine("Caught exception as expected: "+e);
        }
        catch (Exception e) {
            if (e instanceof TCKTestFailureException)
                throw (TCKTestFailureException)e;
            throw new TCKTestFailureException(1110924, "Wrong type of exception thrown: "+e.getClass().getName()+" Expected: java.lang.IllegalArgumentException");
        }

        //1110513: The And class extends javax.slee.profile.query.CompositeQueryExpression.
        //this test is actually quite senseless as the hierarchy states anyway that And is a subclass of CompositeQueryExpression
        try {
            CompositeQueryExpression composite = (CompositeQueryExpression)and;
            getLog().fine("Instance of 'And' successfully class-casted into 'CompositeQueryExpression'.");
        }
        catch (ClassCastException e) {
            throw new TCKTestFailureException(1110513, "Unexpected class cast exception when casting instance of 'And' into 'CompositeQueryExpression': "+e);
        }

    }

    private void testOr() throws TCKTestFailureException {

        QueryExpression ex1 = new Equals("stringValue", "42");
        QueryExpression ex2 = new Equals("intValue", new Integer(42));
        QueryExpression ex3 = new Equals("byteValue", new Byte("42"));

        //11101027: create Or query from two other queries
        Or or;
        try {
            or = new Or(ex1, ex2);
            getLog().fine("'Or(QueryExpression, QueryExpression)' works as expected.");
        }
        catch (Exception e) {
            throw new TCKTestFailureException(11101027, "Exception when using 'Or(QueryExpression, QueryExpression)': ",e);
        }
        //11101028: Throws: java.lang.NullPointerException - if either argument is null.
        try {
            new Or(ex1, null);
            throw new TCKTestFailureException(11101028, "NullPointerException should have been thrown for 'Or(equals1, null)'");
        }
        catch (NullPointerException e) {
            getLog().fine("Caught exception as expected: "+e);
        }
        catch (Exception e) {
            if (e instanceof TCKTestFailureException)
                throw (TCKTestFailureException)e;
            throw new TCKTestFailureException(11101028, "Wrong type of exception thrown: "+e.getClass().getName()+" Expected: java.lang.NullPointerException",e);
        }
        try {
            new Or(null, ex2);
            throw new TCKTestFailureException(11101028, "NullPointerException should have been thrown for 'Or(null, equals2)'");
        }
        catch (NullPointerException e) {
            getLog().fine("Caught exception as expected: "+e);
        }
        catch (Exception e) {
            if (e instanceof TCKTestFailureException)
                throw (TCKTestFailureException)e;
            throw new TCKTestFailureException(11101028, "Wrong type of exception thrown: "+e.getClass().getName()+" Expected: java.lang.NullPointerException",e);
        }


        //11101030: construct Or from an array of elements
        try {
            new Or(new QueryExpression[]{ex1, ex2, ex3});
            getLog().fine("'Or(QueryExpression[])' works as expected.");
        }
        catch (Exception e) {
            throw new TCKTestFailureException(11101030, "Exception when using 'Or(QueryExpression[])': ",e);
        }
        //11101031: throws java.lang.NullPointerException if QueryExpression[] is null or contains null elements
        try {
            new Or(null);
            throw new TCKTestFailureException(11101031, "NullPointerException should have been thrown for 'Or(null)'");
        }
        catch (NullPointerException e) {
            getLog().fine("Caught exception as expected: "+e);
        }
        catch (Exception e) {
            if (e instanceof TCKTestFailureException)
                throw (TCKTestFailureException)e;
            throw new TCKTestFailureException(11101031, "Wrong type of exception thrown: "+e.getClass().getName()+" Expected: java.lang.NullPointerException");
        }
        try {
            new Or(new QueryExpression[]{ex1, ex2, null});
            throw new TCKTestFailureException(11101031, "NullPointerException should have been thrown for 'Or(null)'");
        }
        catch (NullPointerException e) {
            getLog().fine("Caught exception as expected: "+e);
        }
        catch (Exception e) {
            if (e instanceof TCKTestFailureException)
                throw (TCKTestFailureException)e;
            throw new TCKTestFailureException(11101031, "Wrong type of exception thrown: "+e.getClass().getName()+" Expected: java.lang.NullPointerException");
        }

        //?: IllegalArgumentException if less than two expressions in the array
        try {
            new Or(new QueryExpression[]{ex1});
            throw new TCKTestFailureException(1110915, "IllegalArgumentException should have been thrown for 'Or(QueryExpression[]{ex1})'");
        } catch (IllegalArgumentException e) {
            getLog().fine("Caught exception as expected: "+e);
        } catch (Exception e) {
            if (e instanceof TCKTestFailureException)
                throw (TCKTestFailureException)e;
            throw new TCKTestFailureException(1110915, "Wrong type of exception thrown: "+e.getClass().getName()+" Expected: java.lang.IllegalArgumentException");
        }


        //11101033: Add a query expression to this Or object
        //11101034: Returns a reference to 'this'
        try {
            if (or.or(ex3)!=or)
                throw new TCKTestFailureException(11101034, "or(QueryExpression) should return reference to this.");
            else
                getLog().fine("and(QueryExpression) returned reference to 'this' as expected.");
        }
        catch (Exception e) {
            throw new TCKTestFailureException(11101033, "or(QueryExpression) caused exception: ",e);
        }
        //11101035: NullPointerException is argument is null
        try {
            or.or(null);
            throw new TCKTestFailureException(11101035, "NullPointerException should have been thrown for 'Or.or(null)'");
        }
        catch (NullPointerException e) {
            getLog().fine("Caught exception as expected: "+e);
        }
        catch (Exception e) {
            if (e instanceof TCKTestFailureException)
                throw (TCKTestFailureException)e;
            throw new TCKTestFailureException(11101035, "Wrong type of exception thrown: "+e.getClass().getName()+" Expected: java.lang.NullPointerException");
        }
        //11101036:  java.lang.IllegalArgumentException - if adding the query expression
        //to this composite expression would generate a cyclic expression.
        try {
            or.or(new Not(or));
            throw new TCKTestFailureException(11101036, "IllegalArgumentException should have been thrown when creating cyclic expression via Or.or()");
        }
        catch (IllegalArgumentException e) {
            getLog().fine("Caught exception as expected: "+e);
        }
        catch (Exception e) {
            if (e instanceof TCKTestFailureException)
                throw (TCKTestFailureException)e;
            throw new TCKTestFailureException(11101036, "Wrong type of exception thrown: "+e.getClass().getName()+" Expected: java.lang.IllegalArgumentException");
        }

        //test the general toString() method
        try {
            or.toString();
            getLog().fine("'Or.toString()' works as expected.");
        }
        catch (Exception e) {
            throw new TCKTestFailureException(1110915, "'Or.toString()' failed",e);
        }

        //1110520: The Or class extends javax.slee.profile.query.CompositeQueryExpression.
        //this test is actually quite senseless as the hierarchy states anyway that Or is a subclass of CompositeQueryExpression
        try {
            CompositeQueryExpression composite = (CompositeQueryExpression)or;
            getLog().fine("Instance of 'Or' successfully class-casted into 'CompositeQueryExpression'.");
        }
        catch (ClassCastException e) {
            throw new TCKTestFailureException(1110520, "Unexpected class cast exception when casting instance of 'Or' into 'CompositeQueryExpression': "+e);
        }

    }



    private void testNot() throws TCKTestFailureException {

        QueryExpression ex = new Equals("intValue", new Integer(42));

        //11101010: create Not query from two other queries
        Not not;
        try {
            not = new Not(ex);
            getLog().fine("'Not(QueryExpression)' works as expected.");
        }
        catch (Exception e) {
            throw new TCKTestFailureException(11101010, "Exception when using 'Not(QueryExpression)': ",e);
        }
        //11101011: Throws: java.lang.NullPointerException - if either argument is null.
        try {
            new Not(null);
            throw new TCKTestFailureException(11101011, "NullPointerException should have been thrown for 'Not(equals1, null)'");
        }
        catch (NullPointerException e) {
            getLog().fine("Caught exception as expected: "+e);
        }
        catch (Exception e) {
            if (e instanceof TCKTestFailureException)
                throw (TCKTestFailureException)e;
            throw new TCKTestFailureException(11101011, "Wrong type of exception thrown: "+e.getClass().getName()+" Expected: java.lang.NullPointerException",e);
        }

        //11101013: getExpression
        try {
            QueryExpression expr = not.getExpression();
            if (expr==ex)
                getLog().fine("Not.getExpression() worked fine.");
            else
                throw new TCKTestFailureException(11101013, "'Not.getExpression()' did not return the nested query.");
        }
        catch (Exception e) {
            throw new TCKTestFailureException(11101013, "'Not.getExpression()' threw exception: ",e);
        }

        //1110527: The Not class extends javax.slee.profile.query.CompositeQueryExpression.
        //this test is actually quite senseless as the hierarchy states anyway that Not is a subclass of QueryExpression
        try {
            QueryExpression composite = (QueryExpression)not;
            getLog().fine("Instance of 'Not' successfully class-casted into 'QueryExpression'.");
        }
        catch (ClassCastException e) {
            throw new TCKTestFailureException(1110527, "Unexpected class cast exception when casting instance of 'Not' into 'QueryExpression': "+e);
        }

    }


    public void setUp() throws Exception {

        setupService(SERVICE_DU_PATH_PARAM);

        profileUtils = new ProfileUtils(utils());
        profileProvisioning = profileUtils.getProfileProvisioningProxy();
    }

    public void tearDown() throws Exception {

        try {
            profileUtils.removeProfileTable(PROFILE_TABLE_NAME);
        }
        catch (Exception e) {
            getLog().warning("Caught exception while trying to remove profile table:");
            getLog().warning(e);
        }

        super.tearDown();
    }

    private class MySimpleQueryExpression extends SimpleQueryExpression {

        public MySimpleQueryExpression(String attrName, Object attrValue, QueryCollator collator) {
            super(attrName, attrValue, collator);
        }

        public String getRelation() {
            return "MySimpleQueryExpression";
        }

    }

    private class MyOrderedQueryExpression extends OrderedQueryExpression {

        public MyOrderedQueryExpression(String attrName, Object attrValue, QueryCollator collator) {
            super(attrName, attrValue, collator);
        }

        protected String getRelation() {
            return "myOrderedQueryExpression";
        }



    }

    private class MyCompositeQueryExpression extends CompositeQueryExpression {

        public MyCompositeQueryExpression() {
            super();
        }

        protected void toString(StringBuffer arg0) {
            //do nothing here
        }

        public void add2(QueryExpression expr) {
            add(expr);
        }

    }

    private ProfileUtils profileUtils;
    private ProfileProvisioningMBeanProxy profileProvisioning;
}
