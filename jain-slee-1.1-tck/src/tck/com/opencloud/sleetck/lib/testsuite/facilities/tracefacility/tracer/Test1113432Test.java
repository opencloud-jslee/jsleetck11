/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.facilities.tracefacility.tracer;

import java.rmi.RemoteException;
import java.util.Map;

import javax.slee.ComponentID;
import javax.slee.SbbID;
import javax.slee.ServiceID;
import javax.slee.facilities.TraceLevel;
import javax.slee.management.DeployableUnitDescriptor;
import javax.slee.management.DeployableUnitID;
import javax.slee.management.SbbNotification;

import com.opencloud.sleetck.lib.AbstractSleeTCKTest;
import com.opencloud.sleetck.lib.DescriptionKeys;
import com.opencloud.sleetck.lib.TCKTestErrorException;
import com.opencloud.sleetck.lib.TCKTestFailureException;
import com.opencloud.sleetck.lib.TCKTestResult;
import com.opencloud.sleetck.lib.resource.TCKActivityID;
import com.opencloud.sleetck.lib.resource.TCKSbbMessage;
import com.opencloud.sleetck.lib.resource.events.TCKResourceEventX;
import com.opencloud.sleetck.lib.resource.testapi.TCKResourceListener;
import com.opencloud.sleetck.lib.resource.testapi.TCKResourceTestInterface;
import com.opencloud.sleetck.lib.testutils.Assert;
import com.opencloud.sleetck.lib.testutils.BaseTCKResourceListener;
import com.opencloud.sleetck.lib.testutils.FutureResult;
import com.opencloud.sleetck.lib.testutils.jmx.DeploymentMBeanProxy;
import com.opencloud.sleetck.lib.testutils.jmx.TraceMBeanProxy;

/*
 * AssertionID (1113432): Test these utility methods are the equivalent 
 * of calling the isTraceable method with the trace level specified in 
 * the method name.
 * 
 * AssertionID (1113193): Test It throws a java.lang.NullPointerException 
 * - if message is null.
 *
 * AssertionID (1113201): Test It throws a java.lang.NullPointerException 
 * - if message is null.
 * 
 * AssertionID (1113215): Test It throws a java.lang.NullPointerException 
 * - if message is null.
 *
 * AssertionID (1113223): Test It throws a java.lang.NullPointerException 
 * - if message is null.
 *
 * AssertionID (1113237): Test It throws a java.lang.NullPointerException 
 * - if message is null.
 *
 * AssertionID (1113245): Test It throws a java.lang.NullPointerException 
 * - if message is null.
 * 
 * AssertionID (1113259): Test It throws a java.lang.NullPointerException 
 * - if message is null.
 *
 * AssertionID (1113267): Test It throws a java.lang.NullPointerException 
 * - if message is null.
 * 
 * AssertionID (1113281): Test It throws a java.lang.NullPointerException 
 * - if message is null.
 * 
 * AssertionID (1113289): Test It throws a java.lang.NullPointerException 
 * - if message is null.
 *
 * AssertionID (1113303): Test It throws a java.lang.NullPointerException 
 * - if message is null.
 *
 * AssertionID (1113311): Test It throws a java.lang.NullPointerException 
 * - if message is null.
 * 
 * AssertionID (1113325): Test It throws a java.lang.NullPointerException 
 * - if message is null.
 *
 * AssertionID (1113333): Test It throws a java.lang.NullPointerException 
 * - if message is null.
 *
 */
public class Test1113432Test extends AbstractSleeTCKTest {

    /**
     * Perform the actual test.
     */
    public void run(FutureResult result) throws Exception {
        this.result = result;
        TraceMBeanProxy traceMBeanProxy = utils().getMBeanProxyFactory().createTraceMBeanProxy(
                utils().getSleeManagementMBeanProxy().getTraceMBean());

        if (sbbID == null)
            throw new TCKTestErrorException("sbbID not found for " + DescriptionKeys.SERVICE_DU_PATH_PARAM);
        if (serviceID == null)
            throw new TCKTestErrorException("serviceID not found for " + DescriptionKeys.SERVICE_DU_PATH_PARAM);

        SbbNotification sbbNotification = new SbbNotification(serviceID, sbbID);
        try {
            getLog().fine(
                    "Starting to test The trace level of a particular tracer "
                            + "is equal to the trace level assigned to it by an Administrator "
                            + "using a TraceMBean object.");
            traceMBeanProxy.setTraceLevel(sbbNotification, "com.test", TraceLevel.FINE);
        } catch (Exception e) {
            getLog().warning(e);
            result.setError("ERROR!", e);
        }

        String activityName = "Test1113432Test";
        TCKResourceTestInterface resource = utils().getResourceInterface();
        TCKActivityID activityID = resource.createActivity(activityName);
        getLog().info("Firing event: " + testName);

        // kickoff the test
        resource.fireEvent(TCKResourceEventX.X1, testName, activityID, null);
    }

    /**
     * Do all the pre-run configuration of the test.
     */

    public void setUp() throws Exception {

        getLog().fine("Connecting to resource");
        getLog().fine("Installing and activating service");

        resourceListener = new TCKResourceListenerImpl();
        setResourceListener(resourceListener);

        // Install the Deployable Units
        String duPath = utils().getTestParams().getProperty(DescriptionKeys.SERVICE_DU_PATH_PARAM);
        duID = utils().install(duPath);

        // Activate the DU
        utils().activateServices(duID, true);

        DeploymentMBeanProxy duProxy = utils().getDeploymentMBeanProxy();
        DeployableUnitDescriptor duDesc = duProxy.getDescriptor(duID);
        ComponentID components[] = duDesc.getComponents();
        // Get the SbbID from the components.
        for (int i = 0; i < components.length; i++) {
            if (components[i] instanceof ServiceID) {
                getLog().fine("Setting serviceID value.");
                serviceID = (ServiceID) components[i];
                continue;
            }

            if (components[i] instanceof SbbID) {
                getLog().fine("Setting sbbID value.");
                sbbID = (SbbID) components[i];
                continue;
            }
        }

    }

    /**
     * Clean up after the test.
     */

    public void tearDown() throws Exception {
        // cleanup
        super.tearDown();
    }

    private class TCKResourceListenerImpl extends BaseTCKResourceListener {
        // TCKResourceListener methods

        public synchronized void onSbbMessage(TCKSbbMessage message, TCKActivityID calledActivity)
                throws RemoteException {

            Map sbbData = (Map) message.getMessage();
            String sbbTestName = "Test1113432Test";
            String sbbTestResult = (String) sbbData.get("result");
            String sbbTestMessage = (String) sbbData.get("message");
            int assertionID = ((Integer) sbbData.get("id")).intValue();
            getLog().info(
                    "Received message from SBB: testname=" + sbbTestName + ", result=" + sbbTestResult + ", message="
                            + sbbTestMessage + ", id=" + assertionID);
            try {
                Assert.assertEquals(assertionID, "Test " + sbbTestName + " failed.", "pass", sbbTestResult);
                result.setPassed();
            } catch (TCKTestFailureException ex) {
                result.setFailed(assertionID, sbbTestMessage);
            }
        }

        public void onException(Exception exception) throws RemoteException {
            getLog().warning("Received Exception from SBB or resource:");
            getLog().warning(exception);
            result.setError(exception);
        }
    }

    // private SleeTCKTestUtils utils;
    private TCKResourceListener resourceListener;

    private FutureResult result;

    private DeployableUnitID duID;

    private String testName = "Test1113432";

    private SbbID sbbID;

    private ServiceID serviceID;

}