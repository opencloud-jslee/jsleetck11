/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.javax.slee.profile.ProfileTableActivityContextInterfaceFactory;

import com.opencloud.sleetck.lib.TCKTestFailureException;
import com.opencloud.sleetck.lib.sbbutils.BaseTCKSbb;
import com.opencloud.sleetck.lib.sbbutils.TCKSbbUtils;
import com.opencloud.sleetck.lib.sbbutils.events.TCKSbbEvent;
import com.opencloud.sleetck.lib.sbbutils.events.TCKSbbEventImpl;

import javax.naming.InitialContext;
import javax.slee.ActivityContextInterface;
import javax.slee.Address;
import javax.slee.profile.ProfileAddedEvent;
import javax.slee.profile.ProfileTableActivity;
import javax.slee.profile.ProfileTableActivityContextInterfaceFactory;

/**
 *
 */
public abstract class Test1110040Sbb extends BaseTCKSbb {

    // Event handler methods
    public void onProfileAddedEvent(ProfileAddedEvent event, ActivityContextInterface aci) {

        try {
            getSbbContext().getTracer("Test1110040Sbb").fine("Received ProfileAddedEvent event:"+event);

            //1110044, 1110045: as the 1.1 SimpleProfile has no ProfileLocal interface defined, a ProfileLocalObject may be safely typecast into
            //javax.slee.profile.ProfileLocalObject or the 1.1 SimpleProfileCMP
            try {
                javax.slee.profile.ProfileLocalObject obj = (javax.slee.profile.ProfileLocalObject) event.getAddedProfileLocal();
            }
            catch (ClassCastException e) {
                throw new TCKTestFailureException(1110044,"Class cast to javax.slee.profile.ProfileLocalObject not successful.",e);
            }
            try {
                com.opencloud.sleetck.lib.testsuite.profiles.simpleprofile11.SimpleProfileCMP obj = (com.opencloud.sleetck.lib.testsuite.profiles.simpleprofile11.SimpleProfileCMP) event.getAddedProfileLocal();
            }
            catch (ClassCastException e) {
                throw new TCKTestFailureException(1110045,"Class cast to CMP-interface com.opencloud.sleetck.lib.testsuite.profiles.simpleprofile11.SimpleProfileCMP not successful.",e);
            }
            getSbbContext().getTracer("Test1110040Sbb").fine("Class casts worked fine.");

            //1110040: obtain ProfileTableActivityContextInterfaceFactory object from the component environment via static constant
            ProfileTableActivityContextInterfaceFactory ptaciFactory = (ProfileTableActivityContextInterfaceFactory) new InitialContext().lookup( ProfileTableActivityContextInterfaceFactory.JNDI_NAME );
            getSbbContext().getTracer("Test1110040Sbb").fine("Obtained ProfileTableActivityContextInterfaceFactory object via lookup.");

            //get an ActivityContextInterface for the profile activity (which we actually already have anyway, but so what)
            ActivityContextInterface ptaci = ptaciFactory.getActivityContextInterface((ProfileTableActivity)aci.getActivity());
            getSbbContext().getTracer("Test1110040Sbb").fine("Obtained ActivityContextInterface for profile activity.");

            // send an Sbb event on the freshly obtained ptaci
            fireTCKSbbEvent(new TCKSbbEventImpl(null),ptaci,null);
            getSbbContext().getTracer("Test1110040Sbb").fine("Fired event on freshly obained activity context interface.");

        } catch(Exception e) {
            TCKSbbUtils.handleException(e);
        }
    }

    public void onTCKSbbEvent(TCKSbbEvent event, ActivityContextInterface aci) {
        try {
            //send an ACK to the test
            TCKSbbUtils.getResourceInterface().sendSbbMessage(TCKSbbEvent.class.getName());
        } catch(Exception e) {
            TCKSbbUtils.handleException(e);
        }
    }

    public abstract void fireTCKSbbEvent(TCKSbbEvent event, ActivityContextInterface aci, Address address);

}
