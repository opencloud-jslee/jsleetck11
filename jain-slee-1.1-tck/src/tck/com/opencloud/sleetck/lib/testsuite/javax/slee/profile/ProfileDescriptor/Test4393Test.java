/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.javax.slee.profile.ProfileDescriptor;

import com.opencloud.sleetck.lib.SleeTCKTest;
import com.opencloud.sleetck.lib.SleeTCKTestUtils;
import com.opencloud.sleetck.lib.TCKTestResult;
import com.opencloud.sleetck.lib.testutils.jmx.DeploymentMBeanProxy;

import javax.slee.ComponentID;
import javax.slee.management.DeployableUnitDescriptor;
import javax.slee.management.DeployableUnitID;
import javax.slee.profile.ProfileSpecificationDescriptor;
import javax.slee.profile.ProfileSpecificationID;

public class Test4393Test implements SleeTCKTest {

    private static final String SERVICE_DU_PATH_PARAM = "DUPath";
    private static final int TEST_ID = 4393;

    public void init(SleeTCKTestUtils utils) {
        this.utils = utils;
    }

    /**
     * Perform the actual test.
     */

    public TCKTestResult run() throws Exception {

        DeploymentMBeanProxy duProxy = utils.getDeploymentMBeanProxy();
        DeployableUnitDescriptor duDesc = duProxy.getDescriptor(duID);
        ComponentID components[] = duDesc.getComponents();

        for (int i = 0; i < components.length; i++) {
            if (components[i] instanceof ProfileSpecificationID) {
                ProfileSpecificationID profileSpecID = (ProfileSpecificationID) components[i];
                ProfileSpecificationDescriptor profileDesc = (ProfileSpecificationDescriptor) duProxy.getDescriptor(profileSpecID);

                String cmpName = profileDesc.getCMPInterfaceName();
                utils.getLog().fine("Got name of " + cmpName);
                if (cmpName.equals("com.opencloud.sleetck.lib.testsuite.javax.slee.profile.ProfileDescriptor.Test4393ProfileCMP"))
                    return TCKTestResult.passed();
                return TCKTestResult.failed(TEST_ID, "Name returned by ProfileSpecificationDescriptor.getCMPInterfaceName() was incorrect.");
            }
        }

        return TCKTestResult.error("Couldn't find a ProfileSpecificationDescriptor to test.");
    }

    /**
     * Do all the pre-run configuration of the test.
     */

    public void setUp() throws Exception {

        utils.getLog().fine("Installing and activating service");

        // Install the Deployable Units
        String duPath = utils.getTestParams().getProperty(SERVICE_DU_PATH_PARAM);
        duID = utils.install(duPath);

        // Activate the DU
        utils.activateServices(duID, true);

    }

    /**
     * Clean up after the test.
     */

    public void tearDown() throws Exception {
        utils.getLog().fine("Disconnecting from resource");
        utils.getResourceInterface().clearActivities();
        utils.getResourceInterface().removeResourceListener();
        utils.getLog().fine("Deactivating and uninstalling service");
        utils.deactivateAllServices();
        utils.uninstallAll();
    }

    private SleeTCKTestUtils utils;
    private DeployableUnitID duID;
}
