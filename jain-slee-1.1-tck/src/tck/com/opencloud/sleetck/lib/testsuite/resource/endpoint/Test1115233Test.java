/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.resource.endpoint;

import javax.slee.ServiceID;
import javax.slee.management.DeployableUnitID;

import com.opencloud.sleetck.lib.DescriptionKeys;
import com.opencloud.sleetck.lib.TCKTestResult;
import com.opencloud.sleetck.lib.testsuite.resource.BaseResourceTest;
import com.opencloud.sleetck.lib.testsuite.resource.RAMethods;
import com.opencloud.sleetck.lib.testutils.jmx.DeploymentMBeanProxy;
import com.opencloud.sleetck.lib.testutils.jmx.ServiceManagementMBeanProxy;

/**
 * Test to ensure that endActivityTransacted() does not transition the
 * Activity's Activity Context to the Ending state if the transaction is rolled
 * back.
 * <p>
 * This test relies on service priority working correctly (it uses multiple
 * sbbs).
 * <p>
 * Test assertion: 1115233
 */
public class Test1115233Test extends BaseResourceTest {

    private static final int ASSERTION_ID = 1115233;

    public TCKTestResult run() throws Exception {

        int sequenceID = nextMessageID();
        MultiResponseListener listener = new MultiResponseListener(sequenceID);

        listener.addExpectedResult("result-ra1");
        listener.addExpectedResult("result-ra2");
        listener.addExpectedResult("result-sbb1a");
        listener.addExpectedResult("result-sbb1b");
        listener.addExpectedResult("result-sbb2");

        sendMessage(RAMethods.endActivityTransacted, new Integer(ASSERTION_ID), listener, sequenceID);

        Object result1 = listener.getResult("result-ra1");
        checkResult(result1, ASSERTION_ID);

        Object result2 = listener.getResult("result-ra2");
        checkResult(result2, ASSERTION_ID, "Exception thrown while attempting to call endActivityTransacted() from an sbb event handler method");

        Object result3 = listener.getResult("result-sbb1a");
        checkResult(result3, ASSERTION_ID, "Activity Context was unexpectedly ending before endActivityTrasacted() was called");

        Object result4 = listener.getResult("result-sbb1b");
        checkResult(result4, ASSERTION_ID, "Activity Context failed to transition into the ending state after endActivityTrasacted() was called");

        Object result5 = listener.getResult("result-sbb2");
        checkResult(result5, ASSERTION_ID,
                "Activity Context transition into ending state in a rolled back transaction was incorrectly visible in a subsequent transaction");

        return TCKTestResult.passed();
    }

    public void setUp() throws Exception {
        super.setUp();

        try {
            DeploymentMBeanProxy deploymentMBean = utils().getDeploymentMBeanProxy();
            ServiceManagementMBeanProxy serviceMBean = utils().getServiceManagementMBeanProxy();

            String serviceDUPath = utils().getTestParams().getProperty(DescriptionKeys.SERVICE_DU_PATH_PARAM + "2");
            String absolutePath = utils().getDeploymentUnitURL(serviceDUPath);

            getLog().info("Installing service: " + absolutePath);

            DeployableUnitID serviceDUID = deploymentMBean.install(absolutePath);
            ServiceID serviceID = getServiceID(serviceDUID);
            serviceMBean.activate(serviceID);
        } catch (Exception e) {
            getLog().error(e);
        }
    }

    public void tearDown() throws Exception {
        try {
            DeploymentMBeanProxy deploymentMBean = utils().getDeploymentMBeanProxy();

            String relativePath = utils().getTestParams().getProperty(DescriptionKeys.SERVICE_DU_PATH_PARAM + "2");
            String serviceDuPath = utils().getDeploymentUnitURL(relativePath);
            getLog().info("Uninstalling service: " + serviceDuPath);

            DeployableUnitID serviceDUID = deploymentMBean.getDeployableUnit(serviceDuPath);
            ServiceID serviceID = getServiceID(serviceDUID);
            utils().deactivateService(serviceID);
            deploymentMBean.uninstall(serviceDUID);
        } catch (Exception e) {
            getLog().error(e);
        }

        super.tearDown();
    }

}
