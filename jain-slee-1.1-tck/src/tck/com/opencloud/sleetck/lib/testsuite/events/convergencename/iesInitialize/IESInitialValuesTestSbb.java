/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.events.convergencename.iesInitialize;

import com.opencloud.sleetck.lib.sbbutils.BaseTCKSbb;
import com.opencloud.sleetck.lib.sbbutils.TCKSbbUtils;
import com.opencloud.sleetck.lib.resource.events.TCKResourceEventX;
import com.opencloud.sleetck.lib.resource.sbbapi.TCKActivity;
import com.opencloud.sleetck.lib.TCKTestErrorException;
import com.opencloud.sleetck.lib.TCKTestFailureException;
import javax.slee.ActivityContextInterface;
import javax.slee.facilities.Level;
import javax.slee.InitialEventSelector;
import javax.naming.Context;
import javax.naming.NamingException;

/**
 * This class expects the initialEventSelector method to be activated in the
 * deployment descriptor for both event types (X1 and X2), and expects the
 * convergence name variables to be selected in the deployment descriptor as follows:<br>
 * For X1: ActivityContext, Address and EventType<br>
 * For X2: AddressProfile and Event <br>
 */
public abstract class IESInitialValuesTestSbb extends BaseTCKSbb {

    // -- Event Handlers -- //

    /**
     * Empty implementation
     */
    public void onTCKResourceEventX1(TCKResourceEventX event, ActivityContextInterface aci) {}

    /**
     * Empty implementation
     */
    public void onTCKResourceEventX2(TCKResourceEventX event, ActivityContextInterface aci) {}

    // -- Initial event selector method -- //

    /**
     * Verifies the contents of the InitialEventSelector object, then
     * sends an ACK to the test (in the form of the event ID String) or sends an Exception
     */
    public InitialEventSelector initialEventSelectorMethod(InitialEventSelector ies) {
        try {
            // check the event
            TCKResourceEventX event = (TCKResourceEventX)ies.getEvent();
            if(event == null) throw new TCKTestFailureException(1922,"The event variable is null");
            IESInitTestEventMessageData eventMessageData = IESInitTestEventMessageData.fromExported(event.getMessage());

            // check event type, address and activity
            assertEqual(757,"Address",ies.getAddress(),eventMessageData.getAddress());

            Object iesActivity = ies.getActivity();
            if(iesActivity == null || !(iesActivity instanceof TCKActivity)) throw new TCKTestFailureException(
                756,"The Activity set in the InitialEventSelector is not the Activity on which the event was fired. "+
                    "InitialEventSelector activity variable: "+iesActivity);
            assertEqual(756,"Activity",((TCKActivity)ies.getActivity()).getID(),eventMessageData.getActivityID());

            if(ies.getCustomName() != null) throw new TCKTestFailureException(758,
                "The custom name variable in the InitialEventSelector was not set to null. "+
                "Custom name value: "+ies.getCustomName());

            if(!ies.isInitialEvent()) throw new TCKTestFailureException(759,
                "The isInitialEvent variable in the InitialEventSelector was set to false");

            // check the is<Variable>Selected flags according to the event type
            if(event.getEventTypeName().equals(TCKResourceEventX.X1)) {
                checkIsVariableSelected("ActivityContext",true,ies.isActivityContextSelected());
                checkIsVariableSelected("Address",true,ies.isAddressSelected());
                //                checkIsVariableSelected("EventType",true,ies.isEventTypeSelected());
                checkIsVariableSelected("AddressProfile",false,ies.isAddressProfileSelected());
                checkIsVariableSelected("Event",false,ies.isEventSelected());
            } else if(event.getEventTypeName().equals(TCKResourceEventX.X2)) {
                checkIsVariableSelected("ActivityContext",false,ies.isActivityContextSelected());
                checkIsVariableSelected("Address",false,ies.isAddressSelected());
                //                checkIsVariableSelected("EventType",false,ies.isEventTypeSelected());
                checkIsVariableSelected("AddressProfile",true,ies.isAddressProfileSelected());
                checkIsVariableSelected("Event",true,ies.isEventSelected());
            } else throw new TCKTestErrorException("Received unexpected event type. Event type name="+event.getEventTypeName());

            // set the custom name variable to true to test that this value is not held accross invocations
            ies.setCustomName("foo");
            // set the isInitialEvent variable to false to test that this value is not held accross invocations
            ies.setInitialEvent(false);

            // send an ACK to the test to confirm that no failures or Exceptions were detected
            TCKSbbUtils.getResourceInterface().sendSbbMessage(eventMessageData.getEventID());
        } catch(Exception ex) {
            TCKSbbUtils.handleException(ex);
        }
        return ies;
    }

    private void assertEqual(int assertionID, String variableName, Object iesValue, Object producerValue)
                                                                    throws TCKTestFailureException{
        boolean equal = iesValue == null ? producerValue == null : iesValue.equals(producerValue);
        if(!equal) throw new TCKTestFailureException(assertionID,variableName+" variable "+iesValue+
            " in InitialEventSelector does not match value given by event producer: "+producerValue);
    }

    /**
     * Checks that the given flag matches the expected value, and throws a TCKTestFailureException
     * if it does not match.
     */
    private void checkIsVariableSelected(String variableName, boolean expectedValue,
                                    boolean iesValue) throws TCKTestFailureException {
        if(expectedValue != iesValue) throw new TCKTestFailureException(754,"The is"+variableName+
            "Selected flag was not set to the expected value in the InitialEventSelector. Expected value: "+
            expectedValue+"; InitialEventSelector value:"+iesValue );
    }

}
