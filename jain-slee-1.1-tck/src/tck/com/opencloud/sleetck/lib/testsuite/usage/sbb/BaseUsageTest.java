/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.usage.sbb;

import javax.slee.ServiceID;
import javax.slee.management.DeployableUnitID;

import com.opencloud.sleetck.lib.AbstractSleeTCKTest;

public abstract class BaseUsageTest extends AbstractSleeTCKTest  {
    private static final String TCK_SBB_EVENT_DU_PATH_PARAM = "eventDUPath";
    private static final String SERVICE_DU_PATH_PARAM = "DUPath";

    public void setUp() throws Exception {

        super.setupService(TCK_SBB_EVENT_DU_PATH_PARAM);

        getLog().fine("Installing and activating service");
        String duPath = utils().getTestParams().getProperty(SERVICE_DU_PATH_PARAM);
        DeployableUnitID duID = utils().install(duPath);

        // Start the DU's service.
        services = utils().activateServices(duID, true);
    }

    protected ServiceID[] services;
}
