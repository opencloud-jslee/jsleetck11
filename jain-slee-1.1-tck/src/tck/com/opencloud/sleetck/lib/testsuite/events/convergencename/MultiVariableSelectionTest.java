/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.events.convergencename;

import com.opencloud.sleetck.lib.TCKTestResult;
import com.opencloud.sleetck.lib.TCKTestErrorException;
import com.opencloud.sleetck.lib.resource.TCKActivityID;
import com.opencloud.sleetck.lib.resource.testapi.TCKResourceTestInterface;
import com.opencloud.sleetck.lib.resource.events.TCKResourceEventX;
import com.opencloud.sleetck.lib.testutils.ProfileUtils;
import com.opencloud.sleetck.lib.testutils.jmx.AddressProfileProxy;
import com.opencloud.sleetck.lib.testutils.jmx.ProfileProvisioningMBeanProxy;
import com.opencloud.sleetck.lib.testutils.jmx.impl.AddressProfileProxyImpl;

import javax.management.ObjectName;
import javax.slee.Address;
import javax.slee.AddressPlan;
import java.util.Vector;
import java.util.Iterator;
import java.rmi.RemoteException;

/**
 * Assertion: 2011 test all combinations of selecting two or more convergence name variables from Address, AddressProfile,
 * ActivityContext, EventType and CustomName.
 */
public class MultiVariableSelectionTest extends AbstractMultiVariableSelectionTest {

    private static final String PAUSE_BETWEEN_SENDS_MS_PARAM = "pauseBetweenSendsMs";

    private static final String PROFILE_1 = "PROFILE_1"; // a1 a2
    private static final String PROFILE_2 = "PROFILE_2"; //       a3 a4

    private static final String ADDRESS_PROFILE_TABLE = "tck.AddressAndProfileTestProfile";

    private static final Address ADDRESS_1 = new Address(AddressPlan.IP, "1.0.0.1");
    private static final Address ADDRESS_2 = new Address(AddressPlan.IP, "1.0.0.2");
    private static final Address ADDRESS_3 = new Address(AddressPlan.IP, "1.0.0.3");
    private static final Address ADDRESS_4 = new Address(AddressPlan.IP, "1.0.0.4");

    // Two address sets (used for varying the address profile)
    private static final Address[] ADDRESS_SET_A = {ADDRESS_1, ADDRESS_2};
    private static final Address[] ADDRESS_SET_B = {ADDRESS_3, ADDRESS_4};

    private ProfileUtils profileUtils;

    /**
     * Performs a convergence name selection test for every combination of convergence name variables
     * @return
     * @throws Exception
     */
    public TCKTestResult run() throws Exception {
        for (int i = 0; i < getCombinations().size(); i++) {
            Vector selectedVariables = (Vector) getCombinations().elementAt(i);
            performVariableNonSelectionTest(selectedVariables);
        }
        return TCKTestResult.passed();
    }

    /**
     * Test a given combination of selectable convergence name variables.  Initially an SBB entity is created using
     * a fixed address, activity context, custom name and event type, then each non-selected variable is changed in turn,
     * firing another event with the same initial event selector parameters.
     * As only non-selected variables are changed only one SBB should be created.
     * @param selectedVariables
     * @throws Exception
     */
    private void performVariableNonSelectionTest(Vector selectedVariables) throws Exception {
        getLog().info("Performing constant non selection test for selected variables: " + selectedVariables);
        InitialEventSelectorParameters iesParams;
        if (!selectedVariables.contains(CUSTOM_NAME)) {
            iesParams = createIESParamsFromSelected(selectedVariables, null);
        } else {
            iesParams = createIESParamsFromSelected(selectedVariables, "custom_name_A");
        }

        String initialEvent = String.valueOf(currentEventID++);
        TCKActivityID mainActivityID = createNextActivity();
        getLog().info("Created activity: " + mainActivityID.getName());
        TCKActivityID activityID = mainActivityID;
        Address address = ADDRESS_1;
        // empty PROFILE_2 first so we don't get uniqueness errors
        replaceAddressProfile(PROFILE_2, new Address[0]);
        replaceAddressProfile(PROFILE_1, ADDRESS_SET_A);
        replaceAddressProfile(PROFILE_2, ADDRESS_SET_B);
        String eventType = TCKResourceEventX.X1;
        sendEventAndWait(eventType, initialEvent, mainActivityID, ADDRESS_1, initialEvent, iesParams);
        pauseBetweenSends();
        sendEventAndWait(eventType, String.valueOf(currentEventID++), mainActivityID, ADDRESS_1, initialEvent, iesParams);
        pauseBetweenSends();

        for (int i = 0; i < VARIABLES.length; i++) {
            String variable = VARIABLES[i];

            // We don't vary the constant we're testing or custom name
            if (selectedVariables.contains(variable) || variable.equals(CUSTOM_NAME))
                continue;

            // set defaults
            getLog().info("Varying variable " + variable);

            // vary the current variable in the loop
            if (variable.equals(ADDRESS)) {
                address = ADDRESS_2;
            } else if (variable.equals(ADDRESS_PROFILE)) {
                // empty PROFILE_2 first so we don't get uniqueness errors
                replaceAddressProfile(PROFILE_2, new Address[0]);
                replaceAddressProfile(PROFILE_1, ADDRESS_SET_B);
                replaceAddressProfile(PROFILE_2, ADDRESS_SET_A);
            } else if (variable.equals(ACTIVITY_CONTEXT)) {
                activityID = createNextActivity();
            } else if (variable.equals(EVENT_TYPE)) {
                eventType = TCKResourceEventX.X2;
            } else if (variable.equals(EVENT_OBJECT)) {
                // nothing required here -- the TCK always sends a different event object by default
            }

            sendEventAndWait(eventType, String.valueOf(currentEventID++), activityID, address, initialEvent, iesParams);
            pauseBetweenSends();
        }
    }

    public void setUp() throws Exception {
        // setup the address profile table before installing the service
        profileUtils = new ProfileUtils(utils());
        profileProxy = profileUtils.getProfileProvisioningProxy();
        profileUtils.createStandardAddressProfileTable(ADDRESS_PROFILE_TABLE);
        setupAddressProfile(ADDRESS_PROFILE_TABLE, PROFILE_1, ADDRESS_SET_A);
        setupAddressProfile(ADDRESS_PROFILE_TABLE, PROFILE_2, ADDRESS_SET_B);
        pauseBetweenSendsMs = Integer.parseInt(utils().getTestParams().getProperty(PAUSE_BETWEEN_SENDS_MS_PARAM));
        if(pauseBetweenSendsMs < 0) throw new TCKTestErrorException("Invalid value for 'pauseBetweenSendsMs' parameter (can't be negative): "+pauseBetweenSendsMs);
        activityIDs = new Vector();
        super.setUp();
    }

    public void tearDown() throws Exception {
        // end the activities one at a time with the specified pause between each, to avoid a burst of end events
        TCKResourceTestInterface resource = utils().getResourceInterface();
        synchronized(activityIDs) {
            Iterator iter = activityIDs.iterator();
            while(iter.hasNext()) {
                resource.endActivity((TCKActivityID)iter.next());
                iter.remove();
                pauseBetweenSends();
            }
        }
        super.tearDown();
        if (profileUtils != null) {
            profileProxy.removeProfile(ADDRESS_PROFILE_TABLE, PROFILE_1);
            profileProxy.removeProfile(ADDRESS_PROFILE_TABLE, PROFILE_2);
            profileProxy.removeProfileTable(ADDRESS_PROFILE_TABLE);
        }
    }

    private void setupAddressProfile(String tableName, String profileName, Address[] addresses) throws Exception {
        ObjectName objectName = profileProxy.createProfile(tableName, profileName);
        setAddressProfile(objectName, addresses);
    }

    private void replaceAddressProfile(String profileName, Address[] addresses) throws Exception {
        ObjectName objectName = profileProxy.getProfile(ADDRESS_PROFILE_TABLE, profileName);
        setAddressProfile(objectName, addresses);
    }

    private void setAddressProfile(ObjectName objectName, Address[] addresses) throws Exception {
        AddressProfileProxy addressProfileProxy = new AddressProfileProxyImpl(objectName, utils().getMBeanFacade());
        addressProfileProxy.editProfile();
        addressProfileProxy.setAddresses(addresses);
        addressProfileProxy.commitProfile();
    }

    private TCKActivityID createNextActivity() throws TCKTestErrorException, RemoteException {
        synchronized(activityIDs) {
            TCKActivityID activityID = utils().getResourceInterface().createActivity(ACTIVITY_ID_PREFIX + (currentActivityIDSuffix++));
            activityIDs.addElement(activityID);
            return activityID;
        }
    }

    private void pauseBetweenSends() throws InterruptedException {
        if(pauseBetweenSendsMs > 0) {
            getLog().finest("Waiting for "+pauseBetweenSendsMs+" milliseconds");
            synchronized(this) {
                wait(pauseBetweenSendsMs);
            }
        }
    }

    private int pauseBetweenSendsMs;
    private int currentActivityIDSuffix = 0;
    private int currentEventID = 0;
    private ProfileProvisioningMBeanProxy profileProxy;
    private Vector activityIDs;
}
