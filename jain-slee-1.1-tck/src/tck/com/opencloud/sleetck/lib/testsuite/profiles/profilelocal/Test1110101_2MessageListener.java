/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.profiles.profilelocal;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.Hashtable;

import javax.slee.TransactionRolledbackLocalException;
import javax.slee.profile.ProfileTable;
import javax.slee.profile.ReadOnlyProfileException;
import javax.slee.transaction.SleeTransaction;
import javax.slee.transaction.SleeTransactionManager;
import javax.transaction.RollbackException;

import com.opencloud.logging.StdErrLog;
import com.opencloud.sleetck.lib.profileutils.BaseMessageSender;
import com.opencloud.sleetck.lib.rautils.MessageHandler;
import com.opencloud.sleetck.lib.rautils.RMIObjectChannel;
import com.opencloud.sleetck.lib.rautils.TCKRAUtils;

/**
 * Provides callback method for handling messages sent from the TCK test to the RA.
 * The actual test code is located in the 'handleMessage()' method.
 */
public class Test1110101_2MessageListener extends UnicastRemoteObject implements MessageHandler {

    public static final String PROFILE_TABLE_NAME= "Test1110101_2ProfileTable";
    public static final String PROFILE_NAME= "Test1110101_2Profile";

    public Test1110101_2MessageListener(Test1110101_2ResourceAdaptor ra) throws RemoteException {
        super();

        try {
            out = TCKRAUtils.lookupRMIObjectChannel();
            msgSender = new BaseMessageSender(out, new StdErrLog());
        }
        catch (Exception e) {
            ra.getLog().warning("Exception occurred when trying to acquire RMIObjectChannel: ",e);
        }

        this.ra = ra;
    }

    public boolean handleMessage(Object message) throws RemoteException {

        //run the tests, if both run through successfully and completely (=both return 'true')
        //send success msg back to TCK test
        if (test_PASS_BY_REF() && test_TRY_CMP_GET_SET() && test_PASS_NON_RMI_IIOP() && test_CHECK_ROLLBACK() && test_CHECK_REMOVE())
            msgSender.sendSuccess(1110101, "Test successful.");

        return true;
    }

    private boolean test_TRY_CMP_GET_SET() {
        SleeTransactionManager txnManager = ra.getResourceAdaptorContext().getSleeTransactionManager();
        SleeTransaction txn=null;

        try {
            ProfileTable profileTable = ra.getResourceAdaptorContext().getProfileTable(PROFILE_TABLE_NAME);
            //start a new TXN as profileTable.find is mandatory transactional
            txn = txnManager.beginSleeTransaction();

            //1110101: If ProfileLocal interface extends ProfileCMP interface all CMP get/set methods are available to Sbb
            Test1110101ProfileLocal profileLocal = (Test1110101ProfileLocal) profileTable.find(PROFILE_NAME);
            profileLocal.setValue("new value");
            profileLocal.getValue();
            profileLocal.setValue2("new value");
            profileLocal.getValue2();
            msgSender.sendLogMsg("Using accessors defined in CMP interface via ProfileLocal interface worked fine.");

            txn.commit();
            txn = null;
        } catch (Exception e) {
            msgSender.sendException(e);
            return false;
        } finally {
            try {
                if (txn!=null)
                    txn.rollback();
            } catch (Exception ex) {
                ra.getLog().warning("Error occured when trying to rollback RA started TXN.", ex);
                msgSender.sendLogMsg("Exception occurred when trying to safely rollback RA started TXN: "+ex.getMessage());
            }
        }

        return true;
    }

    private boolean test_PASS_NON_RMI_IIOP() {
        SleeTransactionManager txnManager = ra.getResourceAdaptorContext().getSleeTransactionManager();
        SleeTransaction txn=null;

        try {
            ProfileTable profileTable = ra.getResourceAdaptorContext().getProfileTable(PROFILE_TABLE_NAME);
            //start a new TXN as profileTable.find is mandatory transactional
            txn = txnManager.beginSleeTransaction();

            //1110106: As pass-by-ref semantics are in place non-RMI-IIOP types can be used as arguments and return types
            Test1110101ProfileLocal profileLocal = (Test1110101ProfileLocal) profileTable.find(PROFILE_NAME);
            MyNonSerializable obj = new MyNonSerializable();
            MyNonSerializable obj_new = profileLocal.businessNonSerial(obj, "new value");
            if (obj==obj_new && obj.value.equals("new value"))
                msgSender.sendLogMsg("Passing non-RMI-IIOP parameter worked fine.");
            else {
                msgSender.sendFailure(1110106, "Passing non-RMI-IIOP parameter failed.");
                return false;
            }

            txn.commit();
            txn = null;
        } catch (Exception e) {
            msgSender.sendException(e);
            return false;
        } finally {
            try {
                if (txn!=null)
                    txn.rollback();
            } catch (Exception ex) {
                ra.getLog().warning("Error occured when trying to rollback RA started TXN.", ex);
                msgSender.sendLogMsg("Exception occurred when trying to safely rollback RA started TXN: "+ex.getMessage());
            }
        }

        return true;
    }

    private boolean test_CHECK_ROLLBACK() {
        SleeTransactionManager txnManager = ra.getResourceAdaptorContext().getSleeTransactionManager();
        SleeTransaction txn=null;

        try {
            ProfileTable profileTable = ra.getResourceAdaptorContext().getProfileTable(PROFILE_TABLE_NAME);
            //start a new TXN as profileTable.find is mandatory transactional
            txn = txnManager.beginSleeTransaction();

            //1110120+1110122: If a business method throws an unchecked exception current transaction is marked
            //for rollback and a TransactionRolledbackLocalException is thrown
            Test1110101ProfileLocal profileLocal = (Test1110101ProfileLocal) profileTable.find(PROFILE_NAME);
            ReadOnlyProfileException rope = new ReadOnlyProfileException("Testexception");
            try {
                profileLocal.raiseException(rope);
                msgSender.sendFailure(1110120, "TransactionRolledbackLocalException should have been thrown.");
                return false;
            }
            catch (TransactionRolledbackLocalException e) {
                msgSender.sendLogMsg("TransactionRolledbackLocalException was received as expected after business method threw an unchecked exception.");
                //11101126: The runtime exception thrown by the Profile object will be the cause of the TransactionRolledbackLocalException i.e. it is accessible via java.lang.Throwable.getCause().
                if (rope.equals(e.getCause()))
                    msgSender.sendLogMsg("Cause of the TransactionRolledbackLocalException was the raised ReadOnlyProfileException as expected.");
                else {
                    msgSender.sendFailure(11101126, "Cause of the TransactionRolledbackLocalException should have been the raised ReadOnlyProfileException but was "+e.getCause());
                    return false;
                }
            }
            catch (Exception e) {
                msgSender.sendFailure(1110120, "Wrong type of exception thrown: "+e.getClass().getName()+
                        " Expected: javax.slee.TransactionRolledbackLocalException");
                return false;
            }

            try {
                txn.commit();
                msgSender.sendFailure(1110122, "TransactionRolledbackLocalException was caught but TXN has not been marked for rollback.");
                return false;
            } catch (RollbackException e) {
                msgSender.sendLogMsg("As expected TXN was marked for rollback after business method threw an unchecked exception.");
            }

            txn = null;
        } catch (Exception e) {
            msgSender.sendException(e);
            return false;
        } finally {
            try {
                if (txn!=null)
                    txn.rollback();
            } catch (Exception ex) {
                ra.getLog().warning("Error occured when trying to rollback RA started TXN.", ex);
                msgSender.sendLogMsg("Exception occurred when trying to safely rollback RA started TXN: "+ex.getMessage());
            }
        }

        return true;
    }

    private boolean test_CHECK_REMOVE() {
        SleeTransactionManager txnManager = ra.getResourceAdaptorContext().getSleeTransactionManager();
        SleeTransaction txn=null;

        try {
            ProfileTable profileTable = ra.getResourceAdaptorContext().getProfileTable(PROFILE_TABLE_NAME);
            //start a new TXN as profileTable.find is mandatory transactional
            txn = txnManager.beginSleeTransaction();

            //1110113: If a ProfileLocal object refers to a Profile which has already been removed
            //a TransactionRolledbackLocalException is thrown (changed in PFD2!! Previously was NoSuchObjectLocalException)
            Test1110101ProfileLocal profileLocal = (Test1110101ProfileLocal) profileTable.find(PROFILE_NAME);
            profileTable.remove(PROFILE_NAME);
            try {
                profileLocal.getValue();
                msgSender.sendFailure(1110113, "TransactionRolledbackLocalException should have been thrown as profile is already removed.");
                return false;
            }
            catch (TransactionRolledbackLocalException e) {
                msgSender.sendLogMsg("TransactionRolledbackLocalException caught as expected when trying to call methods on an invalid ProfileLocal object.");
            }
            catch (Exception e) {
                msgSender.sendFailure(1110113, "Wrong type of exception thrown: "+e.getClass().getName()+
                        " Expected: javax.slee.TransactionRolledbackLocalException");
                return false;
            }

            //try to get rid of the TXN whether it has been marked for rollback or not
            txn.rollback();
            txn = null;

        } catch (Exception e) {
            msgSender.sendException(e);
            return false;
        } finally {
            try {
                if (txn!=null)
                    txn.rollback();
            } catch (Exception ex) {
                ra.getLog().warning("Error occured when trying to rollback RA started TXN.", ex);
                msgSender.sendLogMsg("Exception occurred when trying to safely rollback RA started TXN: "+ex.getMessage());
            }
        }

        return true;
    }

    private boolean test_PASS_BY_REF() {
        SleeTransactionManager txnManager = ra.getResourceAdaptorContext().getSleeTransactionManager();
        SleeTransaction txn=null;

        try {
            ProfileTable profileTable = ra.getResourceAdaptorContext().getProfileTable(PROFILE_TABLE_NAME);
            //start a new TXN as profileTable.find is mandatory transactional
            txn = txnManager.beginSleeTransaction();

            //1110099: Business methods defined in ProfileLocal interface have Pass-by-reference semantics
            Test1110101ProfileLocal profileLocal = (Test1110101ProfileLocal) profileTable.find(PROFILE_NAME);
            Hashtable t = new Hashtable();
            String key = "key";
            String value = "value";
            Hashtable t_new = profileLocal.businessInsert(t, key, value);
            if (t==t_new && t.containsKey(key))
                msgSender.sendLogMsg("Pass-By-Ref worked fine.");
            else {
                msgSender.sendFailure(1110099, "Pass-By-Ref test failed.");
                return false;
            }

            txn.commit();
            txn = null;
        } catch (Exception e) {
            msgSender.sendException(e);
            return false;
        } finally {
            try {
                if (txn!=null)
                    txn.rollback();
            } catch (Exception ex) {
                ra.getLog().warning("Error occured when trying to rollback RA started TXN.", ex);
                msgSender.sendLogMsg("Exception occurred when trying to safely rollback RA started TXN: "+ex.getMessage());
            }
        }

        return true;
    }

    private BaseMessageSender msgSender;
    private RMIObjectChannel out;
    private Test1110101_2ResourceAdaptor ra;
}

