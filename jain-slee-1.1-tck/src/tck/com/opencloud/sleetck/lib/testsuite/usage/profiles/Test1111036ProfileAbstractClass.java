/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.usage.profiles;

import javax.slee.usage.UnrecognizedUsageParameterSetNameException;

public abstract class Test1111036ProfileAbstractClass extends BaseProfileAbstractClass {
    public abstract Test1111036ProfileInterface getUsageParameterSet(String name) throws UnrecognizedUsageParameterSetNameException;
    public abstract Test1111036ProfileInterface getDefaultUsageParameterSet();

    public void incrementDefaultParameterSet() {
        Test1111036ProfileInterface ps;
        ps = getDefaultUsageParameterSet();
        ps.incrementFirstCount(1);
    }        
        
    public void incrementNamedParameterSet() throws javax.slee.usage.UnrecognizedUsageParameterSetNameException {
        Test1111036ProfileInterface ps;
        ps = getUsageParameterSet(PARAM_SET_NAME);
        ps.incrementFirstCount(1);
    }
}
