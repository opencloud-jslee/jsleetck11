/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.resource.endpoint;

import java.util.HashMap;

import com.opencloud.sleetck.lib.TCKTestFailureException;
import com.opencloud.sleetck.lib.TCKTestResult;
import com.opencloud.sleetck.lib.testsuite.resource.BaseResourceTest;
import com.opencloud.sleetck.lib.testsuite.resource.RAMethods;

/**
 * Test to ensure that fireEventTransacted() throws a NullPointerException.
 * <p>
 * Test assertion: 1115312
 */
public class Test1115312Test extends BaseResourceTest {

    private static final int ASSERTION_ID = 1115312;

    public TCKTestResult run() throws Exception {
        HashMap results = sendMessage(RAMethods.fireEventTransacted, new Integer(ASSERTION_ID));
        if (results == null)
            throw new TCKTestFailureException(ASSERTION_ID, "Test timed out while waiting for response from test component");

        Object result1 = results.get("result1");
        checkResult(result1, ASSERTION_ID, "fireEventTransacted(badHandle, eventID, event, null, null) did not throw a UnrecognizeActivityHandleException");

        Object result2 = results.get("result2");
        checkResult(result2, ASSERTION_ID,
                "fireEventTransacted(badHandle, eventID, event, null, null,EventFlags.NO_FLAGS) did not throw a UnrecognizeActivityHandleException");

        return TCKTestResult.passed();
    }
}
