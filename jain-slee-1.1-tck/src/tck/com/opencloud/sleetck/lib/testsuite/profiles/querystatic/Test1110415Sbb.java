/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.profiles.querystatic;

import java.util.Collection;
import java.util.HashMap;

import javax.naming.Context;
import javax.slee.ActivityContextInterface;
import javax.slee.Address;
import javax.slee.AddressPlan;
import javax.slee.profile.ProfileFacility;

import com.opencloud.sleetck.lib.resource.events.TCKResourceEventX;
import com.opencloud.sleetck.lib.sbbutils.BaseTCKSbb;
import com.opencloud.sleetck.lib.sbbutils.TCKSbbUtils;
import com.opencloud.sleetck.lib.sbbutils.events.TCKSbbEvent;
import com.opencloud.sleetck.lib.sbbutils.events.TCKSbbEventImpl;
import com.opencloud.sleetck.lib.sbbutils.events2.SbbBaseMessageComposer;
import com.opencloud.sleetck.lib.sbbutils.events2.SbbBaseMessageConstants;

public abstract class Test1110415Sbb extends BaseTCKSbb {

    public static final int RUN_QUERIES = 0;
    public static final String PROFILE_TABLE_NAME = "Test1110415ProfileTable";
    public static final String PROFILE_NAME0_1 = "Test1110415Profile0_1";
    public static final String PROFILE_NAME0_2 = "Test1110415Profile0_2";
    public static final String PROFILE_NAME1_1 = "Test1110415Profile1_1";
    public static final String PROFILE_NAME1_2 = "Test1110415Profile1_2";
    public static final String PROFILE_NAME2_1 = "Test1110415Profile2_1";
    public static final String PROFILE_NAME2_2 = "Test1110415Profile2_2";
    public static final String PROFILE_NAME3 = "Test1110415Profile3";

    private void sendLogMsgCall (String msg) {
        HashMap map = SbbBaseMessageComposer.getLogMsg(msg);
        try {
            TCKSbbUtils.getResourceInterface().callTest(map);
        }
        catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }
    }

    private void sendResult(boolean result, int id, String msg, ActivityContextInterface aci) {
        HashMap map = SbbBaseMessageComposer.getSetResultMsg(result, id, msg);
        fireTCKSbbEvent(new TCKSbbEventImpl(map), aci, null);
    }

    public void onTCKResourceEventX1(TCKResourceEventX event, ActivityContextInterface aci) {
        try {

            int operationID = ((Integer)event.getMessage()).intValue();
            Context env = TCKSbbUtils.getSbbEnvironment();
            ProfileFacility profileFacility = (ProfileFacility)env.lookup("slee/facilities/profile");
            Test1110415ProfileTable profileTable = (Test1110415ProfileTable)profileFacility.getProfileTable(PROFILE_TABLE_NAME);

            switch (operationID) {
            case RUN_QUERIES:
                do_RUN_QUERIES(profileTable, aci);
                break;
            }

        } catch(Exception e) {
            TCKSbbUtils.handleException(e);
        }
    }

    private void do_RUN_QUERIES(Test1110415ProfileTable profileTable, ActivityContextInterface aci) {

        Collection col;

        //1110419: query options element is accepted if it's there
        //1110420: read-only may be set to 'False'
        //1110424: as a max-match of 1 is specified only 1 profile (instead of 2) should be returned
        col=profileTable.queryAllParamTypesMatches1NotReadOnly("42", new Character('X'), new Integer(42), new Boolean(true), new Double(42), new Byte(42+""), new Short(42+""), new Long(42), new Float(42.0), 'X', 42, true, (double)42, (byte)42, (short)42, (long)42, (float)42.0);
        if (col.size()==1) {
            sendLogMsgCall("Maximum no. of profiles returned by query was reduced as expected.");
        }
        else {
            sendResult(false, 1110424, "Maximum no. of profiles returned by the query should have been reduced to 1 but were "+col.size(), aci);
            return;
        }

        //1110420: read-only may be set to 'True'
        //1110424: as no max-match is specified all matches should be returned
        col=profileTable.queryHasDescriptionNoMatchMaxIsReadOnly();
        if (col.size()==2) {
            sendLogMsgCall("Expected no. of profiles returned by query 'queryHasDescriptionNoMatchMaxIsReadOnly'.");
        }
        else {
            sendResult(false, 1110424, "No. of matching profiles in query 'queryHasDescriptionNoMatchMaxIsReadOnly' was "+col.size()+" but should have been 2", aci);
            return;
        }

        //1110422: read-only can be omitted
        col = profileTable.queryDefaultReadOnlyNoMaxMatches();
        if (col.size()==2) {
            sendLogMsgCall("Expected no. of profiles returned by query 'queryDefaultReadOnlyNoMaxMatches'.");
        }
        else {
            sendResult(false, 1110422, "No. of matching profiles in query 'queryDefaultReadOnlyNoMaxMatches' was "+col.size()+" but should have been 2", aci);
            return;
        }

        //1110419: query-options element can be omitted
        //1110418: description element is optional
        col=profileTable.queryNoDescriptionNoOptions();
        if (col.size()==2) {
            sendLogMsgCall("Expected no. of profiles returned by query 'queryNoDescriptionNoOptions'.");
        }
        else {
            sendResult(false, 1110418, "No. of matching profiles in query 'queryNoDescriptionNoOptions' was "+col.size()+" but should have been 2", aci);
            return;
        }

        //1110433: op is one of the binary operators, thus can be "equals", "not-equals", "less-than
        //"less-than-or-equals", "greater-than", "greater-than-or-equals"
        col=profileTable.queryCompareIntEquals();
        if (col.size()==2) {
            sendLogMsgCall("Expected no. of profiles returned by query 'queryCompareIntEquals'.");
        }
        else {
            sendResult(false, 1110433, "No. of matching profiles in query 'queryCompareIntEquals' was "+col.size()+" but should have been 2", aci);
            return;
        }
        col=profileTable.queryCompareIntNotEquals();
        if (col.size()==5) {
            sendLogMsgCall("Expected no. of profiles returned by query 'queryCompareIntNotEquals'.");
        }
        else {
            sendResult(false, 1110433, "No. of matching profiles in query 'queryCompareIntNotEquals' was "+col.size()+" but should have been 5", aci);
            return;
        }
        col=profileTable.queryCompareIntLessThan();
        if (col.size()==4) {
            sendLogMsgCall("Expected no. of profiles returned by query 'queryCompareIntLessThan'.");
        }
        else {
            sendResult(false, 1110433, "No. of matching profiles in query 'queryCompareIntLessThan' was "+col.size()+" but should have been 4", aci);
            return;
        }
        col=profileTable.queryCompareIntLessThanOrEquals();
        if (col.size()==6) {
            sendLogMsgCall("Expected no. of profiles returned by query 'queryCompareIntLessThanOrEquals'.");
        }
        else {
            sendResult(false, 1110433, "No. of matching profiles in query 'queryCompareIntLessThanOrEquals' was "+col.size()+" but should have been 6", aci);
            return;
        }
        col=profileTable.queryCompareIntGreaterThan();
        if (col.size()==1) {
            sendLogMsgCall("Expected no. of profiles returned by query 'queryCompareIntGreaterThan'.");
        }
        else {
            sendResult(false, 1110433, "No. of matching profiles in query 'queryCompareIntGreaterThan' was "+col.size()+" but should have been 1", aci);
            return;
        }
        col=profileTable.queryCompareIntGreaterThanOrEquals();
        if (col.size()==3) {
            sendLogMsgCall("Expected no. of profiles returned by query 'queryCompareIntGreaterThanOrEquals'.");
        }
        else {
            sendResult(false, 1110433, "No. of matching profiles in query 'queryCompareIntGreaterThanOrEquals' was "+col.size()+" but should have been 3", aci);
            return;
        }


        //1110437: Either "value" or "parameter" element can be used in the "compare" element
        col=profileTable.queryCompareStringValue();
        if (col.size()==2) {
            sendLogMsgCall("Expected no. of profiles returned by query 'queryCompareStringValue'.");
        }
        else {
            sendResult(false, 1110437, "No. of matching profiles in query 'queryCompareStringValue' was "+col.size()+" but should have been 2", aci);
            return;
        }
        col=profileTable.queryCompareStringParam("42");
        if (col.size()==2) {
            sendLogMsgCall("Expected no. of profiles returned by query 'queryCompareStringParam'.");
        }
        else {
            sendResult(false, 1110437, "No. of matching profiles in query 'queryCompareStringParam' was "+col.size()+" but should have been 2", aci);
            return;
        }

        //1110432: "compare" can be used for attributes of type String and primitive types or their wrappers
        col=profileTable.queryCompareIntObj();
        if (col.size()==2) {
            sendLogMsgCall("Expected no. of profiles returned by query 'queryCompareIntObj'.");
        }
        else {
            sendResult(false, 1110432, "No. of matching profiles in query 'queryCompareIntObj' was "+col.size()+" but should have been 2", aci);
            return;
        }
        col=profileTable.queryCompareCharObj();
        if (col.size()==2) {
            sendLogMsgCall("Expected no. of profiles returned by query 'queryCompareCharObj'.");
        }
        else {
            sendResult(false, 1110432, "No. of matching profiles in query 'queryCompareCharObj' was "+col.size()+" but should have been 2", aci);
            return;
        }
        col=profileTable.queryCompareBoolObj();
        if (col.size()==6) {
            sendLogMsgCall("Expected no. of profiles returned by query 'queryCompareBoolObj'.");
        }
        else {
            sendResult(false, 1110432, "No. of matching profiles in query 'queryCompareBoolObj' was "+col.size()+" but should have been 6", aci);
            return;
        }
        col=profileTable.queryCompareDoubleObj();
        if (col.size()==2) {
            sendLogMsgCall("Expected no. of profiles returned by query 'queryCompareDoubleObj'.");
        }
        else {
            sendResult(false, 1110432, "No. of matching profiles in query 'queryCompareDoubleObj' was "+col.size()+" but should have been 2", aci);
            return;
        }
        col=profileTable.queryCompareByteObj();
        if (col.size()==2) {
            sendLogMsgCall("Expected no. of profiles returned by query 'queryCompareByteObj'.");
        }
        else {
            sendResult(false, 1110432, "No. of matching profiles in query 'queryCompareByteObj' was "+col.size()+" but should have been 2", aci);
            return;
        }
        col=profileTable.queryCompareShortObj();
        if (col.size()==2) {
            sendLogMsgCall("Expected no. of profiles returned by query 'queryCompareShortObj'.");
        }
        else {
            sendResult(false, 1110432, "No. of matching profiles in query 'queryCompareShortObj' was "+col.size()+" but should have been 2", aci);
            return;
        }
        col=profileTable.queryCompareLongObj();
        if (col.size()==2) {
            sendLogMsgCall("Expected no. of profiles returned by query 'queryCompareLongObj'.");
        }
        else {
            sendResult(false, 1110432, "No. of matching profiles in query 'queryCompareLongObj' was "+col.size()+" but should have been 2", aci);
            return;
        }
        col=profileTable.queryCompareFloatObj();
        if (col.size()==2) {
            sendLogMsgCall("Expected no. of profiles returned by query 'queryCompareFloatObj'.");
        }
        else {
            sendResult(false, 1110432, "No. of matching profiles in query 'queryCompareFloatObj' was "+col.size()+" but should have been 2", aci);
            return;
        }

        col=profileTable.queryCompareInt();
        if (col.size()==2) {
            sendLogMsgCall("Expected no. of profiles returned by query 'queryCompareInt'.");
        }
        else {
            sendResult(false, 1110432, "No. of matching profiles in query 'queryCompareInt' was "+col.size()+" but should have been 2", aci);
            return;
        }
        col=profileTable.queryCompareChar();
        if (col.size()==2) {
            sendLogMsgCall("Expected no. of profiles returned by query 'queryCompareChar'.");
        }
        else {
            sendResult(false, 1110432, "No. of matching profiles in query 'queryCompareChar' was "+col.size()+" but should have been 2", aci);
            return;
        }
        col=profileTable.queryCompareBool();
        if (col.size()==6) {
            sendLogMsgCall("Expected no. of profiles returned by query 'queryCompareBool'.");
        }
        else {
            sendResult(false, 1110432, "No. of matching profiles in query 'queryCompareBool' was "+col.size()+" but should have been 6", aci);
            return;
        }
        col=profileTable.queryCompareDouble();
        if (col.size()==2) {
            sendLogMsgCall("Expected no. of profiles returned by query 'queryCompareDouble'.");
        }
        else {
            sendResult(false, 1110432, "No. of matching profiles in query 'queryCompareDouble' was "+col.size()+" but should have been 2", aci);
            return;
        }
        col=profileTable.queryCompareByte();
        if (col.size()==2) {
            sendLogMsgCall("Expected no. of profiles returned by query 'queryCompareByte'.");
        }
        else {
            sendResult(false, 1110432, "No. of matching profiles in query 'queryCompareByte' was "+col.size()+" but should have been 2", aci);
            return;
        }
        col=profileTable.queryCompareShort();
        if (col.size()==2) {
            sendLogMsgCall("Expected no. of profiles returned by query 'queryCompareShort'.");
        }
        else {
            sendResult(false, 1110432, "No. of matching profiles in query 'queryCompareShort' was "+col.size()+" but should have been 2", aci);
            return;
        }
        col=profileTable.queryCompareLong();
        if (col.size()==2) {
            sendLogMsgCall("Expected no. of profiles returned by query 'queryCompareLong'.");
        }
        else {
            sendResult(false, 1110432, "No. of matching profiles in query 'queryCompareLong' was "+col.size()+" but should have been 2", aci);
            return;
        }
        col=profileTable.queryCompareFloat();
        if (col.size()==2) {
            sendLogMsgCall("Expected no. of profiles returned by query 'queryCompareFloat'.");
        }
        else {
            sendResult(false, 1110432, "No. of matching profiles in query 'queryCompareFloat' was "+col.size()+" but should have been 2", aci);
            return;
        }
        col=profileTable.queryCompareAddress(new Address(AddressPlan.SLEE_PROFILE, "XY/X"));
        if (col.size()==2) {
            sendLogMsgCall("Expected no. of profiles returned by query 'queryCompareAddress'.");
        }
        else {
            sendResult(false, 1110432, "No. of matching profiles in query 'queryCompareAddress' was "+col.size()+" but should have been 2", aci);
            return;
        }

        //1110443: Either "value" or "parameter" can be used for "longest-prefix-match" element
        col=profileTable.queryLongestPrefixMatchValue();
        if (col.size()==2) {
            sendLogMsgCall("Expected no. of profiles returned by query 'queryLongestPrefixMatchValue'.");
        }
        else {
            sendResult(false, 1110443, "No. of matching profiles in query 'queryLongestPrefixMatchValue' was "+col.size()+" but should have been 2", aci);
            return;
        }
        col=profileTable.queryLongestPrefixMatchParam("42");
        if (col.size()==2) {
            sendLogMsgCall("Expected no. of profiles returned by query 'queryLongestPrefixMatchParam'.");
        }
        else {
            sendResult(false, 1110443, "No. of matching profiles in query 'queryLongestPrefixMatchParam' was "+col.size()+" but should have been 2", aci);
            return;
        }


        //1110449: Either "value" or "parameter" can be used for "has-prefix" element
        col=profileTable.queryHasPrefixValue();
        if (col.size()==2) {
            sendLogMsgCall("Expected no. of profiles returned by query 'queryHasPrefixValue'.");
        }
        else {
            sendResult(false, 1110449, "No. of matching profiles in query 'queryHasPrefixValue' was "+col.size()+" but should have been 2", aci);
            return;
        }
        col=profileTable.queryHasPrefixParam("42");
        if (col.size()==2) {
            sendLogMsgCall("Expected no. of profiles returned by query 'queryHasPrefixParam'.");
        }
        else {
            sendResult(false, 1110449, "No. of matching profiles in query 'queryHasPrefixParam' was "+col.size()+" but should have been 2", aci);
            return;
        }



        //1110457: Either "from-value" or "from-parameter" and "to-value" or "to-parameter" can be used in "range-match"
        col=profileTable.queryRangeMatchStringValue();
        if (col.size()==6) {
            sendLogMsgCall("Expected no. of profiles returned by query 'queryRangeMatchStringValue'.");
        }
        else {
            sendResult(false, 1110457, "No. of matching profiles in query 'queryRangeMatchStringValue' was "+col.size()+" but should have been 6", aci);
            return;
        }
        col=profileTable.queryRangeMatchStringParam("23", "42");
        if (col.size()==6) {
            sendLogMsgCall("Expected no. of profiles returned by query 'queryRangeMatchStringParam'.");
        }
        else {
            sendResult(false, 1110457, "No. of matching profiles in query 'queryRangeMatchStringParam' was "+col.size()+" but should have been 6", aci);
            return;
        }
        col=profileTable.queryRangeMatchStringParamValueMix("23");
        if (col.size()==6) {
            sendLogMsgCall("Expected no. of profiles returned by query 'queryRangeMatchStringParamValueMix'.");
        }
        else {
            sendResult(false, 1110457, "No. of matching profiles in query 'queryRangeMatchStringParamValueMix' was "+col.size()+" but should have been 6", aci);
            return;
        }

        //1110451: profile attributes for range match can be of type String or primitive types or their Wrapper classes
        col=profileTable.queryRangeMatchIntObj();
        if (col.size()==6) {
            sendLogMsgCall("Expected no. of profiles returned by query 'queryRangeMatchIntObj'.");
        }
        else {
            sendResult(false, 1110451, "No. of matching profiles in query 'queryRangeMatchIntObj' was "+col.size()+" but should have been 6", aci);
            return;
        }
        col=profileTable.queryRangeMatchCharObj();
        if (col.size()==6) {
            sendLogMsgCall("Expected no. of profiles returned by query 'queryRangeMatchCharObj'.");
        }
        else {
            sendResult(false, 1110451, "No. of matching profiles in query 'queryRangeMatchCharObj' was "+col.size()+" but should have been 6", aci);
            return;
        }
        col=profileTable.queryRangeMatchBoolObj();
        if (col.size()==7) {
            sendLogMsgCall("Expected no. of profiles returned by query 'queryRangeMatchBoolObj'.");
        }
        else {
            sendResult(false, 1110451, "No. of matching profiles in query 'queryRangeMatchBoolObj' was "+col.size()+" but should have been 7", aci);
            return;
        }
        col=profileTable.queryRangeMatchDoubleObj();
        if (col.size()==6) {
            sendLogMsgCall("Expected no. of profiles returned by query 'queryRangeMatchDoubleObj'.");
        }
        else {
            sendResult(false, 1110451, "No. of matching profiles in query 'queryRangeMatchDoubleObj' was "+col.size()+" but should have been 6", aci);
            return;
        }
        col=profileTable.queryRangeMatchByteObj();
        if (col.size()==6) {
            sendLogMsgCall("Expected no. of profiles returned by query 'queryRangeMatchByteObj'.");
        }
        else {
            sendResult(false, 1110451, "No. of matching profiles in query 'queryRangeMatchByteObj' was "+col.size()+" but should have been 6", aci);
            return;
        }
        col=profileTable.queryRangeMatchShortObj();
        if (col.size()==6) {
            sendLogMsgCall("Expected no. of profiles returned by query 'queryRangeMatchShortObj'.");
        }
        else {
            sendResult(false, 1110451, "No. of matching profiles in query 'queryRangeMatchShortObj' was "+col.size()+" but should have been 6", aci);
            return;
        }
        col=profileTable.queryRangeMatchLongObj();
        if (col.size()==6) {
            sendLogMsgCall("Expected no. of profiles returned by query 'queryRangeMatchLongObj'.");
        }
        else {
            sendResult(false, 1110451, "No. of matching profiles in query 'queryRangeMatchLongObj' was "+col.size()+" but should have been 6", aci);
            return;
        }
        col=profileTable.queryRangeMatchFloatObj();
        if (col.size()==6) {
            sendLogMsgCall("Expected no. of profiles returned by query 'queryRangeMatchFloatObj'.");
        }
        else {
            sendResult(false, 1110451, "No. of matching profiles in query 'queryRangeMatchFloatObj' was "+col.size()+" but should have been 6", aci);
            return;
        }

        col=profileTable.queryRangeMatchInt();
        if (col.size()==6) {
            sendLogMsgCall("Expected no. of profiles returned by query 'queryRangeMatchInt'.");
        }
        else {
            sendResult(false, 1110451, "No. of matching profiles in query 'queryRangeMatchInt' was "+col.size()+" but should have been 6", aci);
            return;
        }
        col=profileTable.queryRangeMatchChar();
        if (col.size()==6) {
            sendLogMsgCall("Expected no. of profiles returned by query 'queryRangeMatchChar'.");
        }
        else {
            sendResult(false, 1110451, "No. of matching profiles in query 'queryRangeMatchChar' was "+col.size()+" but should have been 6", aci);
            return;
        }
        col=profileTable.queryRangeMatchBool();
        if (col.size()==7) {
            sendLogMsgCall("Expected no. of profiles returned by query 'queryRangeMatchBool'.");
        }
        else {
            sendResult(false, 1110451, "No. of matching profiles in query 'queryRangeMatchBool' was "+col.size()+" but should have been 7", aci);
            return;
        }
        col=profileTable.queryRangeMatchDouble();
        if (col.size()==6) {
            sendLogMsgCall("Expected no. of profiles returned by query 'queryRangeMatchDouble'.");
        }
        else {
            sendResult(false, 1110451, "No. of matching profiles in query 'queryRangeMatchDouble' was "+col.size()+" but should have been 6", aci);
            return;
        }
        col=profileTable.queryRangeMatchByte();
        if (col.size()==6) {
            sendLogMsgCall("Expected no. of profiles returned by query 'queryRangeMatchByte'.");
        }
        else {
            sendResult(false, 1110451, "No. of matching profiles in query 'queryRangeMatchByte' was "+col.size()+" but should have been 6", aci);
            return;
        }
        col=profileTable.queryRangeMatchShort();
        if (col.size()==6) {
            sendLogMsgCall("Expected no. of profiles returned by query 'queryRangeMatchShort'.");
        }
        else {
            sendResult(false, 1110451, "No. of matching profiles in query 'queryRangeMatchShort' was "+col.size()+" but should have been 6", aci);
            return;
        }
        col=profileTable.queryRangeMatchLong();
        if (col.size()==6) {
            sendLogMsgCall("Expected no. of profiles returned by query 'queryRangeMatchLong'.");
        }
        else {
            sendResult(false, 1110451, "No. of matching profiles in query 'queryRangeMatchLong' was "+col.size()+" but should have been 6", aci);
            return;
        }
        col=profileTable.queryRangeMatchFloat();
        if (col.size()==6) {
            sendLogMsgCall("Expected no. of profiles returned by query 'queryRangeMatchFloat'.");
        }
        else {
            sendResult(false, 1110451, "No. of matching profiles in query 'queryRangeMatchFloat' was "+col.size()+" but should have been 6", aci);
            return;
        }


        //1110459: and element working with >=2 subelements
        col=profileTable.queryAnd2Compares();
        if (col.size()==2) {
            sendLogMsgCall("Expected no. of profiles returned by query 'queryAnd2Compares'.");
        }
        else {
            sendResult(false, 1110459, "No. of matching profiles in query 'queryAnd2Compares' was "+col.size()+" but should have been 2", aci);
            return;
        }
        col=profileTable.queryAndMoreElements();
        if (col.size()==0) {
            sendLogMsgCall("Expected no. of profiles returned by query 'queryAndMoreElements'.");
        }
        else {
            sendResult(false, 1110459, "No. of matching profiles in query 'queryAndMoreElements' was "+col.size()+" but should have been 0", aci);
            return;
        }


        //1110461: or element working with >=2 subelements
        col=profileTable.queryOr2Compares();
        if (col.size()==2) {
            sendLogMsgCall("Expected no. of profiles returned by query 'queryOr2Compares'.");
        }
        else {
            sendResult(false, 1110461, "No. of matching profiles in query 'queryOr2Compares' was "+col.size()+" but should have been 2", aci);
            return;
        }
        col=profileTable.queryOrMoreElements();
        if (col.size()==7) {
            sendLogMsgCall("Expected no. of profiles returned by query 'queryOrMoreElements'.");
        }
        else {
            sendResult(false, 1110461, "No. of matching profiles in query 'queryOrMoreElements' was "+col.size()+" but should have been 7", aci);
            return;
        }


        //1110463: not element with 1 subelement
        col=profileTable.queryNot1Compare();
        if (col.size()==5) {
            sendLogMsgCall("Expected no. of profiles returned by query 'queryNot1Compare'.");
        }
        else {
            sendResult(false, 1110463, "No. of matching profiles in query 'queryNot1Compare' was "+col.size()+" but should have been 5", aci);
            return;
        }

        sendResult(true, 1110415, "Test successful.", aci);

    }

    public void onTCKSbbEvent(TCKSbbEvent event, ActivityContextInterface aci) {
        try {
            HashMap payload = (HashMap)event.getMessage();
            int type = ((Integer)payload.get("Type")).intValue();

            switch (type) {
            case SbbBaseMessageConstants.TYPE_SET_RESULT:
                TCKSbbUtils.getResourceInterface().sendSbbMessage(payload);
                break;
            case SbbBaseMessageConstants.TYPE_LOG_MSG:
                TCKSbbUtils.getResourceInterface().callTest(payload);
                break;
            }

        } catch(Exception e) {
            TCKSbbUtils.handleException(e);
        }
    }

    public abstract void fireTCKSbbEvent(TCKSbbEvent event, ActivityContextInterface aci, Address address);
}
