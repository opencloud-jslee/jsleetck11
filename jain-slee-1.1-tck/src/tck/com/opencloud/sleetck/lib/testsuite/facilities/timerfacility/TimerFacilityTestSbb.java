/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.facilities.timerfacility;

import com.opencloud.sleetck.lib.sbbutils.BaseTCKSbb;
import com.opencloud.sleetck.lib.sbbutils.TCKSbbUtils;
import com.opencloud.sleetck.lib.resource.events.TCKResourceEventX;
import com.opencloud.sleetck.lib.resource.sbbapi.TCKActivity;

import javax.naming.Context;
import javax.naming.InitialContext;

import java.util.HashMap;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

import javax.slee.*;
import javax.slee.facilities.*;

/**
 * Test TimerFacility functions from spec s12.1.
 */
public abstract class TimerFacilityTestSbb extends BaseTCKSbb {

    public void onTCKResourceEventX1(TCKResourceEventX event, ActivityContextInterface aci) {

        try {
            testName = (String)event.getMessage();
            TCKSbbUtils.createTrace(getSbbID(),Level.INFO,"Received "+testName+" message",null);

            if (testName.equals("jndi")) doJNDITest();
            else if (testName.equals("exception")) doExceptionTest(aci);
            else if (testName.equals("timerID")) doTimerIDTest(aci);

            // send result response
            sendResultToTCK();

        } catch(Exception e) {
            TCKSbbUtils.handleException(e);
        }
    }

    private void sendResultToTCK() throws Exception {
        HashMap sbbData = new HashMap();
        sbbData.put("testname", testName);
        sbbData.put("result", result?"pass":"fail");
        sbbData.put("message", message);
        sbbData.put("id", new Integer(failedAssertionID));
        TCKSbbUtils.getResourceInterface().sendSbbMessage(sbbData);
    }

    private void setResultFailed(int assertionID, String msg) throws Exception {
        result = false;
        failedAssertionID = assertionID;
        message =  "Assertion " + assertionID + " failed: " + msg;
        TCKSbbUtils.createTrace(getSbbID(), Level.WARNING, message, null);
    }

    private void setResultPassed(String msg) throws Exception {
        result = true;
        message = "Success: " + msg;
        TCKSbbUtils.createTrace(getSbbID(), Level.INFO, message, null);
    }

    private void doJNDITest() throws Exception {
        TimerFacility tf = getTimerFacility();
        if (tf != null) setResultPassed("Found TimerFacility object in JNDI at " + JNDI_TIMERFACILITY_NAME);
        else setResultFailed(1409, "Could not find TimerFacility object in JNDI at " + JNDI_TIMERFACILITY_NAME);
    }

    private void doExceptionTest(ActivityContextInterface aci) throws Exception {
        TimerFacility tf = getTimerFacility();

        // check correct exceptions thrown for TimerFacility.setTimer(activity, address, starttime, options)
        try {
            // null activity - throw NullPointerException
            tf.setTimer(null, null, 0, new TimerOptions());
            setResultFailed(1200, "setTimer with null activity did not throw NullPointerException");
            return;
        } catch (NullPointerException npe) {
            TCKSbbUtils.createTrace(getSbbID(),Level.INFO, "got expected NullPointerException", null);
        } catch (Exception e) {
            setResultFailed(1200, "setTimer with null activity threw unexpected exception: " + e);
            return;
        }

        try {
            // null timerOptions - throw NullPointerException
            tf.setTimer(aci, null, 0, null);
            setResultFailed(1200, "setTimer with null timerOptions did not throw NullPointerException");
            return;
        } catch (NullPointerException npe) {
            TCKSbbUtils.createTrace(getSbbID(),Level.INFO, "got expected NullPointerException", null);
        } catch (Exception e) {
            setResultFailed(1200, "setTimer with null timerOptions threw unexpected exception: " + e);
            return;
        }

        try {
            // -ve startTime - throw IllegalArgumentException
            tf.setTimer(aci, null, -1, new TimerOptions());
            setResultFailed(1201, "setTimer with negative startTime did not throw IllegalArgumentException");
            return;
        } catch (IllegalArgumentException iae) {
            TCKSbbUtils.createTrace(getSbbID(),Level.INFO, "got expected IllegalArgumentException", null);
        } catch (Exception e) {
            setResultFailed(1201, "setTimer with negative startTime threw unexpected exception: " + e);
            return;
        }

        // FIXME: check for required transactional behaviour - TransactionRolledbackLocalException

        // check correct exceptions thrown for TimerFacility.setTimer(activity, address, starttime, period, numRepetitions, options)
        try {
            // null activity - throw NullPointerException
            tf.setTimer(null, null, 0, 120000, 0, new TimerOptions());
            setResultFailed(1200, "setTimer with null activity did not throw NullPointerException");
            return;
        } catch (NullPointerException npe) {
            TCKSbbUtils.createTrace(getSbbID(),Level.INFO, "got expected NullPointerException", null);
        } catch (Exception e) {
            setResultFailed(1200, "setTimer with null activity threw unexpected exception: " + e);
            return;
        }

        try {
            // null timerOptions - throw NullPointerException
            tf.setTimer(aci, null, 0, 120000, 0, null);
            setResultFailed(1200, "setTimer with null timerOptions did not throw NullPointerException");
            return;
        } catch (NullPointerException npe) {
            TCKSbbUtils.createTrace(getSbbID(),Level.INFO, "got expected NullPointerException", null);
        } catch (Exception e) {
            setResultFailed(1200, "setTimer with null timerOptions threw unexpected exception: " + e);
            return;
        }

        try {
            // -ve startTime - throw IllegalArgumentException
            tf.setTimer(aci, null, -1, 120000, 0, new TimerOptions());
            setResultFailed(1201, "setTimer with negative startTime did not throw IllegalArgumentException");
            return;
        } catch (IllegalArgumentException iae) {
            TCKSbbUtils.createTrace(getSbbID(),Level.INFO, "got expected IllegalArgumentException", null);
        } catch (Exception e) {
            setResultFailed(1201, "setTimer with negative startTime threw unexpected exception: " + e);
            return;
        }

        try {
            // -ve numRepetitions - throw IllegalArgumentException
            tf.setTimer(aci, null, 0, 120000, -1, new TimerOptions());
            setResultFailed(3557, "setTimer with negative numRepetitions did not throw IllegalArgumentException");
            return;
        } catch (IllegalArgumentException iae) {
            TCKSbbUtils.createTrace(getSbbID(),Level.INFO, "got expected IllegalArgumentException", null);
        } catch (Exception e) {
            setResultFailed(3557, "setTimer with negative numRepetitions threw unexpected exception: " + e);
            return;
        }

        try {
            // -ve period - throw IllegalArgumentException
            tf.setTimer(aci, null, 0, -100, 0, new TimerOptions());
            setResultFailed(1201, "setTimer with negative period did not throw IllegalArgumentException");
            return;
        } catch (IllegalArgumentException iae) {
            TCKSbbUtils.createTrace(getSbbID(),Level.INFO, "got expected IllegalArgumentException", null);
        } catch (Exception e) {
            setResultFailed(1201, "setTimer with negative period threw unexpected exception: " + e);
            return;
        }

        try {
            // zero period - throw IllegalArgumentException
            tf.setTimer(aci, null, 0, 0, 0, new TimerOptions());
            setResultFailed(1201, "setTimer with zero period did not throw IllegalArgumentException");
            return;
        } catch (IllegalArgumentException iae) {
            TCKSbbUtils.createTrace(getSbbID(),Level.INFO, "got expected IllegalArgumentException", null);
        } catch (Exception e) {
            setResultFailed(1201, "setTimer with zero period threw unexpected exception: " + e);
            return;
        }

        try {
            // options.timeout > period - throw IllegalArgumentException
            tf.setTimer(aci, null, 0, 120000, 0, new TimerOptions(false, 120001, TimerPreserveMissed.NONE));
            setResultFailed(1201, "setTimer with options.timeout greater than period did not throw IllegalArgumentException");
            return;
        } catch (IllegalArgumentException iae) {
            TCKSbbUtils.createTrace(getSbbID(),Level.INFO, "got expected IllegalArgumentException", null);
        } catch (Exception e) {
            setResultFailed(1201, "setTimer with options.timeout greater than period threw unexpected exception: " + e);
            return;
        }

        // FIXME: check for required transactional behaviour - TransactionRolledbackLocalException

        // check correct exceptions thrown for TimerFacility.cancelTimer(timerID)
        try {
            // null timerID - throw NullPointerException
            tf.cancelTimer(null);
            setResultFailed(1205, "cancelTimer with null timerID did not throw NullPointerException");
            return;
        } catch (NullPointerException npe) {
            TCKSbbUtils.createTrace(getSbbID(),Level.INFO, "got expected NullPointerException", null);
        } catch (Exception e) {
            setResultFailed(1205, "cancelTimer with null timerID threw unexpected exception: " + e);
            return;
        }

        // FIXME: check for required transactional behaviour - TransactionRolledbackLocalException

        setResultPassed("TimerFacility exception tests passed");

    }

    // test that impl of TimerID meets spec
    private void doTimerIDTest(ActivityContextInterface aci) throws Exception {
        TimerFacility tf = getTimerFacility();

        // first, set a timer to create a new TimerID
        TimerID timerID = tf.setTimer(aci, null, 0, new TimerOptions());
        if (timerID instanceof java.io.Serializable) {
            // test serialization by writing to a byte array
            try {
                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                ObjectOutputStream oos = new ObjectOutputStream(baos);
                oos.writeObject(timerID);
                oos.close();
                byte[] buf = baos.toByteArray();
                ByteArrayInputStream bais = new ByteArrayInputStream(buf);
                ObjectInputStream ois = new ObjectInputStream(bais);
                TimerID testID = (TimerID) ois.readObject();
                ois.close();
                // ensure that IDs are equal
                if (testID.equals(timerID)) {
                    setResultPassed("TimerID implementation serializes/deserializes OK");
                } else {
                    setResultFailed(1175, "TimerID implementation did not deserialize OK");
                }
            } catch (Exception e) {
                setResultFailed(1175, "TimerID implementation is not serializable, got exception: "+ e);
            }
        }
        else {
            setResultFailed(1175, "TimerID implementation is not serializable");
        }
        tf.cancelTimer(timerID);
    }


    private TimerFacility getTimerFacility() throws Exception {
        TimerFacility tf = null;
        try {
            tf = (TimerFacility) new InitialContext().lookup(JNDI_TIMERFACILITY_NAME);
        } catch (Exception e) {
            TCKSbbUtils.createTrace(getSbbID(),Level.WARNING,"got unexpected Exception: " + e,null);
        }
        return tf;
    }

    private boolean result = false;
    private String message;
    private String testName;
    private int failedAssertionID;

    private static final String JNDI_TIMERFACILITY_NAME = "java:comp/env/slee/facilities/timer";


}
