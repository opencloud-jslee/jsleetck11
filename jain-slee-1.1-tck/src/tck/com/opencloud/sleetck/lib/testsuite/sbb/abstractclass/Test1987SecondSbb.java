/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.sbb.abstractclass;

import com.opencloud.sleetck.lib.sbbutils.BaseTCKSbb;
import com.opencloud.sleetck.lib.sbbutils.TCKSbbUtils;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.slee.ActivityContextInterface;

abstract public class Test1987SecondSbb extends BaseTCKSbb {

    // Initial event
    public void onTest1987Event(Test1987Event event, ActivityContextInterface aci) {

        setTestResult(false);
        Context context = null;
        try {
            context = (Context) new InitialContext().lookup("java:comp/env");
        } catch (NamingException e) {
            TCKSbbUtils.handleException(e);
            return;
        }

        try {
            context.lookup("slee/resources/tck/resource");
        } catch (NamingException e) {
            setTestResult(true);
        }
    }

    public boolean getResult() {
        return getTestResult();
    }

    public abstract void setTestResult(boolean result);
    public abstract boolean getTestResult();

}
