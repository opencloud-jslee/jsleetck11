/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.resource.context;

import java.util.HashMap;

import com.opencloud.sleetck.lib.TCKTestErrorException;
import com.opencloud.sleetck.lib.TCKTestFailureException;
import com.opencloud.sleetck.lib.TCKTestResult;
import com.opencloud.sleetck.lib.testsuite.resource.BaseResourceTest;
import com.opencloud.sleetck.lib.testsuite.resource.RAMethods;

/**
 * Test to ensure that getTimer() is returning a correct value.
 * <p>
 * Test assertion ID: 1115186, 1115187
 */
public class Test1115186Test extends BaseResourceTest {

    private static final int ASSERTION_ID = 1115186;

    public TCKTestResult run() throws Exception {
        HashMap resultmap = sendMessage(RAMethods.getTimer);
        if (resultmap == null)
            throw new TCKTestFailureException(ASSERTION_ID, "Test timed out while waiting for response to getTimer()");
        Object result = resultmap.get("result1");
        if (result instanceof Exception)
            throw new TCKTestFailureException(ASSERTION_ID, "Exception thrown while invoking getTimer()", (Exception) result);
        if (result instanceof String)
            throw new TCKTestFailureException(ASSERTION_ID, "An error occured while invoking getTimer(): " + result);
        if (!Boolean.TRUE.equals(result))
            throw new TCKTestErrorException("Unexpected result received while invoking getTimer(): " + result);

        result = resultmap.get("result2");
        if (result instanceof Exception)
            throw new TCKTestFailureException(1115187, "Unexpected Exception thrown while invoking Timer.cancel() - Expected UnsupportedOperationException",
                    (Exception) result);
        if (Boolean.FALSE.equals(result))
            throw new TCKTestFailureException(1115187, "ResourceAdaptorContext.getTimer().cancel() failed to throw an UnsupportedOperationException");
        if (!Boolean.TRUE.equals(result))
            throw new TCKTestErrorException("Unexpected result received while invoking getTimer(): " + result);

        return TCKTestResult.passed();
    }
}
