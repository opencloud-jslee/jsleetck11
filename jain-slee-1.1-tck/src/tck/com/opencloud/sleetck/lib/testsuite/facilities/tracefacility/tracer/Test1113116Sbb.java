/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.facilities.tracefacility.tracer;

import java.util.HashMap;

import javax.slee.ActivityContextInterface;
import javax.slee.facilities.Tracer;

import com.opencloud.sleetck.lib.resource.events.TCKResourceEventX;
import com.opencloud.sleetck.lib.sbbutils.BaseTCKSbb;
import com.opencloud.sleetck.lib.sbbutils.TCKSbbUtils;

/*
 * AssertionID (1113116): Test A Tracer object can be obtained by SBBs via the 
 * SbbContext interface.
 */
public abstract class Test1113116Sbb extends BaseTCKSbb {

    // Initial event
    public void onTCKResourceEventX1(TCKResourceEventX event, ActivityContextInterface aci) {

        try {
            sbbTracer = getSbbContext().getTracer("com.test");
            sbbTracer.info("Received " + event + " message", null);

            doTest1113116Test();
        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }
    }

    private void sendResultToTCK(String testName, boolean result, int failedAssertionID,  String message) throws Exception {
        HashMap sbbData = new HashMap();
        sbbData.put("testname", testName);
        sbbData.put("result", result ? "pass" : "fail");
        sbbData.put("message", message);
        sbbData.put("id", new Integer(failedAssertionID));
        TCKSbbUtils.getResourceInterface().sendSbbMessage(sbbData);
    }


    private void doTest1113116Test() throws Exception {
        Tracer tracer = null;
        String tracerName = "";
        // 1113116
        try {
            tracer = getSbbContext().getTracer(tracerName);
            if (tracer == null) {
                sendResultToTCK("Test1113116Test", false, 1113116, "This Tracer object cannot be obtained by SBBs via the SbbContext interface.");
                return;
            }
            sbbTracer.info("got expected Tracer", null);
        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }

        sendResultToTCK("Test1113116Test", true, 1113116,"This Tracer object can be obtained by SBBs via the SbbContext interface.");
    }

    private Tracer sbbTracer;
}
