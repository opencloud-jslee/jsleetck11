/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.usage.common;

import com.opencloud.sleetck.lib.SleeTCKTestUtils;
import com.opencloud.sleetck.lib.TCKTestErrorException;
import com.opencloud.sleetck.lib.infra.SleeTCKComponentConstants;
import com.opencloud.sleetck.lib.testutils.ComponentIDLookup;
import com.opencloud.sleetck.lib.testutils.jmx.ServiceUsageMBeanProxy;

import javax.management.ObjectName;
import javax.slee.*;
import javax.slee.management.ManagementException;
import javax.slee.usage.UnrecognizedUsageParameterSetNameException;
import java.util.HashMap;
import java.util.Iterator;

/**
 * Utility class for looking up usage MBean names and interfaces for TCK SBBs.
 */
public class UsageMBeanLookup {

    public UsageMBeanLookup(String serviceName, String sbbName, SleeTCKTestUtils testUtils)
                throws UnrecognizedComponentException, ManagementException, TCKTestErrorException, InvalidArgumentException {
        this.testUtils = testUtils;
        namedSbbUsageMBeanProxies = new HashMap();

        // lookup component ids
        ComponentIDLookup idLookup = new ComponentIDLookup(testUtils);
        serviceID = idLookup.lookupServiceID(serviceName,SleeTCKComponentConstants.TCK_VENDOR,null);
        if(serviceID == null) throw new TCKTestErrorException("ServiceID not found for "+serviceName);
        sbbID = idLookup.lookupSbbID(sbbName,SleeTCKComponentConstants.TCK_VENDOR,null);
        if(sbbID == null) throw new TCKTestErrorException("SbbID not found for "+sbbName);
        // lookup MBean names and interfaces
        serviceUsageMBeanName = testUtils.getServiceManagementMBeanProxy().getServiceUsageMBean(serviceID);
        serviceUsageMBeanProxy = testUtils.getMBeanProxyFactory().createServiceUsageMBeanProxy(serviceUsageMBeanName);
        unnamedSbbUsageMBeanName = serviceUsageMBeanProxy.getSbbUsageMBean(sbbID);
        unnamedSbbUsageMBeanProxy = new UniversalUsageMBeanProxyImpl(unnamedSbbUsageMBeanName,testUtils.getMBeanFacade());
    }

    // -- Accessor methods -- //

    public ServiceID getServiceID() {
        return serviceID;
    }

    public SbbID getSbbID() {
        return sbbID;
    }

    public ObjectName getServiceUsageMBeanName() {
        return serviceUsageMBeanName;
    }

    public ObjectName getUnnamedSbbUsageMBeanName() {
        return unnamedSbbUsageMBeanName;
    }

    public ServiceUsageMBeanProxy getServiceUsageMBeanProxy() {
        return serviceUsageMBeanProxy;
    }

    public UniversalUsageMBeanProxy getUnnamedSbbUsageMBeanProxy() {
        return unnamedSbbUsageMBeanProxy;
    }

    // -- Misc methods -- //

    public UniversalUsageMBeanProxy getNamedSbbUsageMBeanProxy(String name)
                throws TCKTestErrorException, ManagementException, InvalidArgumentException,
                        UnrecognizedSbbException, UnrecognizedUsageParameterSetNameException {
        synchronized(namedSbbUsageMBeanProxies) {
            UniversalUsageMBeanProxy rProxy = (UniversalUsageMBeanProxy)namedSbbUsageMBeanProxies.get(name);
            if(rProxy == null) {
                ObjectName namedSbbUsageMBeanName = serviceUsageMBeanProxy.getSbbUsageMBean(sbbID,name);
                rProxy = new UniversalUsageMBeanProxyImpl(namedSbbUsageMBeanName,getUtils().getMBeanFacade());
                namedSbbUsageMBeanProxies.put(namedSbbUsageMBeanName,rProxy);
            }
            return rProxy;
        }
    }

    public void closeAllMBeans() {
        synchronized(namedSbbUsageMBeanProxies) {
            Iterator toCloseIter = namedSbbUsageMBeanProxies.values().iterator();
            while (toCloseIter.hasNext()) {
                UniversalUsageMBeanProxy toClose = (UniversalUsageMBeanProxy) toCloseIter.next();
                try { toClose.close(); } catch (Exception e) { /* no-op */ }
            }
        }
        try { serviceUsageMBeanProxy.close(); } catch (Exception e) { /* no-op */ }
        try { unnamedSbbUsageMBeanProxy.close(); } catch (Exception e) { /* no-op */ }
    }

    protected SleeTCKTestUtils getUtils() {
        return testUtils;
    }

    // -- Usage MBean names and interfaces -- //

    private ServiceID serviceID;
    private SbbID sbbID;
    private ObjectName serviceUsageMBeanName;
    private ObjectName unnamedSbbUsageMBeanName;
    private ServiceUsageMBeanProxy serviceUsageMBeanProxy;
    private UniversalUsageMBeanProxy unnamedSbbUsageMBeanProxy;

    private SleeTCKTestUtils testUtils;
    private HashMap namedSbbUsageMBeanProxies;

}
