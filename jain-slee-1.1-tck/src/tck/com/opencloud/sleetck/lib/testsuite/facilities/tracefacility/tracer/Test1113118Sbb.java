/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.facilities.tracefacility.tracer;

import javax.slee.ActivityContextInterface;
import javax.slee.SbbID;
import javax.slee.ServiceID;
import javax.slee.facilities.TraceLevel;
import javax.slee.facilities.Tracer;
import javax.slee.management.SbbNotification;

import com.opencloud.sleetck.lib.resource.events.TCKResourceEventX;
import com.opencloud.sleetck.lib.sbbutils.BaseTCKSbb;
import com.opencloud.sleetck.lib.sbbutils.TCKSbbUtils;

/**
 * Assertion ID (1113118): Test The effects of operations invokedon 
 * this facility occur regardless of the state or outcome of any enclosing 
 * transaction.
 */
public abstract class Test1113118Sbb extends BaseTCKSbb {
    public void onTCKResourceEventX1(TCKResourceEventX event, ActivityContextInterface aci) {
        Tracer rootTracer = null;
        String tracerName = "";
        SbbID sbbID = null;
        ServiceID serviceID = null;
        SbbNotification sbbNotification = null;
        try {
            sbbID = getSbbContext().getSbb();
            serviceID = getSbbContext().getService();
            sbbNotification = new SbbNotification(serviceID, sbbID);
            tracerName = sbbNotification.getTraceNotificationType();

            rootTracer = getSbbContext().getTracer(tracerName);
            rootTracer.trace(TraceLevel.INFO, "onTCKResourceEventX1- enter", null);
            getSbbContext().setRollbackOnly();
            rootTracer.trace(TraceLevel.INFO, "onTCKResourceEventX1- exit", null);
        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }
    }

    public void sbbRolledBack(javax.slee.RolledBackContext context) {
    }

}
