/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.runtime.security;

import java.security.AccessControlException;
import java.security.AccessController;
import java.security.Permission;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.PropertyPermission;

import javax.slee.ActivityContextInterface;
import javax.slee.ChildRelation;

import com.opencloud.sleetck.lib.TCKTestErrorException;
import com.opencloud.sleetck.lib.TCKTestFailureException;
import com.opencloud.sleetck.lib.resource.events.TCKResourceEventX;
import com.opencloud.sleetck.lib.sbbutils.BaseTCKSbb;
import com.opencloud.sleetck.lib.sbbutils.TCKSbbUtils;

/**
 * Tests an Sbb's security access using AccessController.checkPermission()
 */
public abstract class Test1112012Sbb extends BaseTCKSbb {

    public void onTCKResourceEventX1(TCKResourceEventX event, ActivityContextInterface aci) {
        try {
            checkPermissions();
            
            ((Test1112012SbbChildLocal)getChildRelation().create()).checkPermissions();
            sendResponse();
        } catch(Exception e) { // catch failures and other Exceptions
            TCKSbbUtils.handleException(e); // send to the test
        }
    }
    
    private void checkPermissions() throws TCKTestErrorException {
        checkGrant(new PropertyPermission("tcktest.parent", "write"), 1112012);
        checkGrant(new PropertyPermission("tcktest.child", "write"), 1112012);
    }
    

    protected void sendResponse() throws Exception {
        if (failedResults.isEmpty()) {
            // send a positive ACK to the test
            TCKSbbUtils.getResourceInterface().sendSbbMessage("All tested permissions granted/denied as appropriate");
        } else {
            StringBuffer buf = new StringBuffer();
            buf.append("Some assertions failed:");
            for (Iterator i=failedResults.iterator(); i.hasNext(); )
                buf.append('\n').append(i.next());
            throw new TCKTestFailureException(firstFailedAssertion, buf.toString());
        }
    }

    /**
     * Checks that the SLEE will grant the Sbb the given permission
     */
    protected void checkGrant(Permission permission, int assertionID) throws TCKTestErrorException {
        try {
            AccessController.checkPermission(permission);
            getSbbContext().getTracer("Test1112012Sbb").info("The SLEE granted the SBB permission " + permission + " as expected");
        } catch (AccessControlException ace) {
            failedResults.add("The SLEE denied the SBB a permission which it was expected to grant: " + permission + " (assertion " + assertionID + ")");
            if(firstFailedAssertion == -1) firstFailedAssertion = assertionID;
        } catch (Exception e) {
            throw new TCKTestErrorException("Caught unexpected Exception from AccessController.checkPermission(). "+
                    "Permission="+permission,e);
        }
    }

    /**
     * Checks that the SLEE will deny the Sbb the given permission
     */
    protected void checkDeny(Permission permission, int assertionID) throws TCKTestErrorException {
        try {
            AccessController.checkPermission(permission);
            failedResults.add("The SLEE granted the Sbb a permission which it was expected to deny: " + permission + " (assertion " + assertionID + ")");
            if(firstFailedAssertion == -1) firstFailedAssertion = assertionID;
        } catch (AccessControlException ace) {
            getSbbContext().getTracer("Test1112012Sbb").info("The SLEE denied the SBB permission " + permission + " as expected");
        } catch (Exception e) {
            throw new TCKTestErrorException("Caught unexpected Exception from AccessController.checkPermission(). "+
                    "Permission="+permission,e);
        }
    }


    protected final ArrayList failedResults = new ArrayList();
    private int firstFailedAssertion = -1;
    
    public abstract ChildRelation getChildRelation();
}
