/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.javax.slee.AddressScreening;

import com.opencloud.sleetck.lib.SleeTCKTest;
import com.opencloud.sleetck.lib.SleeTCKTestUtils;
import com.opencloud.sleetck.lib.TCKTestResult;

import javax.slee.AddressScreening;

public class Test3425Test implements SleeTCKTest {

    public void init(SleeTCKTestUtils utils) {}

    /**
     * Perform the actual test.
     */

    public TCKTestResult run() throws Exception {

        try {
            AddressScreening.fromInt(AddressScreening.ADDRESS_SCREENING_UNDEFINED);
        } catch (Exception e) {
            return TCKTestResult.failed(3425, "AddressScreening.fromInt(int) threw an exception.");
        }
        
        try {
            if (AddressScreening.fromInt(AddressScreening.ADDRESS_SCREENING_UNDEFINED).toInt() != AddressScreening.ADDRESS_SCREENING_UNDEFINED)
                return TCKTestResult.failed(3429, "AddressScreening.fromInt() returned incorrect value.");
        } catch (Exception e) {
            return TCKTestResult.failed(3428, "AddressScreening.fromInt() threw an exception.");
        }

        try {
            if (!AddressScreening.fromInt(AddressScreening.ADDRESS_SCREENING_UNDEFINED).isUndefined())
                return TCKTestResult.failed(3431, "AddressScreening.isUndefined() returned false for UNDEFINED type.");
            if (AddressScreening.fromInt(AddressScreening.ADDRESS_SCREENING_USER_NOT_VERIFIED).isUndefined())
                return TCKTestResult.failed(3431, "AddressScreening.isUndefined() returned true for incorrect type.");
        } catch (Exception e) {
            return TCKTestResult.failed(3430, "AddressScreening.isUndefined() threw an exception.");
        }

        try {
            if (!AddressScreening.fromInt(AddressScreening.ADDRESS_SCREENING_USER_NOT_VERIFIED).isUserNotVerified())
                return TCKTestResult.failed(3433, "AddressScreening.isUserNotVerified() returned false for USER_NOT_VERIFIED type.");
            if (AddressScreening.fromInt(AddressScreening.ADDRESS_SCREENING_UNDEFINED).isUserNotVerified())
                return TCKTestResult.failed(3433, "AddressScreening.isUserNotVerified() returned true for incorrect type.");
        } catch (Exception e) {
            return TCKTestResult.failed(3432, "AddressScreening.isUserNotVerified() threw an exception.");
        }

        try {
            if (!AddressScreening.fromInt(AddressScreening.ADDRESS_SCREENING_USER_VERIFIED_PASSED).isUserVerifiedPassed())
                return TCKTestResult.failed(3435, "AddressScreening.isUserVerifiedPassed() returned false for USER_VERIFIED_PASSED type.");
            if (AddressScreening.fromInt(AddressScreening.ADDRESS_SCREENING_USER_NOT_VERIFIED).isUserVerifiedPassed())
                return TCKTestResult.failed(3435, "AddressScreening.isUserVerifiedPassed() returned true for incorrect type.");
        } catch (Exception e) {
            return TCKTestResult.failed(3434, "AddressScreening.isUserVerifiedPassed() threw an exception.");
        }

        try {
            if (!AddressScreening.fromInt(AddressScreening.ADDRESS_SCREENING_NETWORK).isNetwork())
                return TCKTestResult.failed(3437, "AddressScreening.isNetwork() returned false for NETWORK type.");
            if (AddressScreening.fromInt(AddressScreening.ADDRESS_SCREENING_USER_NOT_VERIFIED).isNetwork())
                return TCKTestResult.failed(3437, "AddressScreening.isNetwork() returned true for incorrect type.");
        } catch (Exception e) {
            return TCKTestResult.failed(3436, "AddressScreening.isNetwork() threw an exception.");
        }

        try {
            AddressScreening addressPres = AddressScreening.fromInt(AddressScreening.ADDRESS_SCREENING_UNDEFINED);
            if (!addressPres.equals(addressPres))
                return TCKTestResult.failed(3439, "AddressScreening.equals(AddressScreening) returned false for equal objects.");
            
            if (addressPres.equals(AddressScreening.fromInt(AddressScreening.ADDRESS_SCREENING_USER_VERIFIED_PASSED)))
                return TCKTestResult.failed(3439, "AddressScreening.equals(AddressScreening) returned true for non-equal objects.");
        } catch (Exception e) {
            return TCKTestResult.failed(3438, "AddressScreening.equals() threw an exception.");
        }

        try {
            AddressScreening.fromInt(AddressScreening.ADDRESS_SCREENING_UNDEFINED).hashCode();
        } catch (Exception e) {
            return TCKTestResult.failed(3440, "AddressScreening.hashCode() threw an exception.");
        }

        try {
            AddressScreening.fromInt(AddressScreening.ADDRESS_SCREENING_UNDEFINED).toString();
        } catch (Exception e) {
            return TCKTestResult.failed(3442, "AddressScreening.toString() threw an exception.");
        }

        return TCKTestResult.passed();
    }


    /**
     * Do all the pre-run configuration of the test.
     */

    public void setUp() throws Exception {
    }

    /**
     * Clean up after the test.
     */

    public void tearDown() throws Exception {
    }

}
