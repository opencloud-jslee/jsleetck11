/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.profiles.checkprofileload;

import com.opencloud.sleetck.lib.AbstractSleeTCKTest;
import com.opencloud.sleetck.lib.TCKTestResult;
import com.opencloud.sleetck.lib.infra.SleeTCKComponentConstants;
import com.opencloud.sleetck.lib.testsuite.profiles.ProfileTestConstants;
import com.opencloud.sleetck.lib.testutils.ComponentIDLookup;
import com.opencloud.sleetck.lib.testutils.ProfileUtils;
import com.opencloud.sleetck.lib.testutils.jmx.ProfileProvisioningMBeanProxy;

import javax.management.ObjectName;
import javax.slee.profile.ProfileSpecificationID;
import java.util.Iterator;
import java.util.Vector;

/**
 * Run tests on the profileLoad() callback
 */
public class CheckProfileLoadTest extends AbstractSleeTCKTest {
    private static final String TABLE_NAME = "tck.CheckLLoadProfileTest.table";
    private static final String PROFILE_SPEC_NAME = "CheckLoadProfile";
    private static final int ASSERTION_ID = 4337;

    public TCKTestResult run() throws Exception {
        // create the profile table
        ProfileProvisioningMBeanProxy profileProvisioning = profileUtils.getProfileProvisioningProxy();
        ProfileSpecificationID profileSpecID = new ComponentIDLookup(utils()).lookupProfileSpecificationID(
            PROFILE_SPEC_NAME,SleeTCKComponentConstants.TCK_VENDOR,"1.0");
        getLog().info("Creating profile table: "+TABLE_NAME);
        profileProvisioning.createProfileTable(profileSpecID,TABLE_NAME);
        isTableCreated = true;

        // check that the profileLoad() has been called on the default profile,
        // with an active transaction context
        ObjectName defaultProfileName = profileProvisioning.getDefaultProfile(TABLE_NAME);
        CheckLoadProfileProxy proxy = getProfileProxy(defaultProfileName);
        proxy.editProfile();

        if (proxy.wasProfileLoadCalled()) {
            if (proxy.wasCMPReadSuccessful()) {
                getLog().info("The profile management object reported that profileLoad() was called, and that a CMP read was successful, indicating an " +
                        "active transaction context");
                return TCKTestResult.passed();
            } else {
                return TCKTestResult.failed(ASSERTION_ID, "profileLoad() should have been called in a transactional context, but failed to read a CMP " +
                        "field value from profileLoad(). Failure reason as reported by the profile management implementation: " + proxy.getFailureReason());
            }
        } else {
            return TCKTestResult.failed(ASSERTION_ID, "profileLoad() wasn't called on the profile management object " +
                    "anytime before an invocation of a custom management method (wasProfileLoadCalled()). profileLoad() " +
                    "was expected to be called, and within an active transaction context.");
        }
    }

    public void setUp() throws Exception {
        String duPath = utils().getTestParams().getProperty(ProfileTestConstants.PROFILE_SPEC_DU_PATH_PARAM);
        getLog().fine("Installing profile specification");
        utils().install(duPath);
        profileUtils = new ProfileUtils(utils());
        activeProxies = new Vector();
    }

    public void tearDown() throws Exception {
        // close profiles
        if(activeProxies != null) {
            getLog().fine("Closing profiles");
            Iterator activeProxiesIter = activeProxies.iterator();
            while(activeProxiesIter.hasNext()) {
                CheckLoadProfileProxy aProxy = (CheckLoadProfileProxy)activeProxiesIter.next();
                try {
                    if(aProxy != null) {
                        if(aProxy.isProfileWriteable()) aProxy.restoreProfile();
                        aProxy.closeProfile();
                    }
                } catch (Exception ex) {
                    getLog().warning("Exception caught while trying to close profiles:");
                    getLog().warning(ex);
                }
            }
        }
        // delete profile table
        try {
            if(profileUtils != null && isTableCreated) {
                getLog().fine("Removing profile table");
                profileUtils.removeProfileTable(TABLE_NAME);
            }
        } catch(Exception e) {
            getLog().warning("Caught exception while trying to remove profile table:");
            getLog().warning(e);
        }
        profileUtils = null;
        isTableCreated = false;
        activeProxies = null;
        super.tearDown();
    }

    private CheckLoadProfileProxy getProfileProxy(ObjectName mbeanName) {
        CheckLoadProfileProxy rProxy = new CheckLoadProfileProxy(mbeanName,utils().getMBeanFacade());
        activeProxies.addElement(rProxy);
        return rProxy;
    }

    private ProfileUtils profileUtils;
    private Vector activeProxies;
    private boolean isTableCreated;
}























