/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.javax.slee.profile.ProfileContext;

import javax.naming.Context;
import javax.slee.ActivityContextInterface;
import javax.slee.Address;
import javax.slee.RolledBackContext;
import javax.slee.SbbContext;
import javax.slee.profile.ProfileFacility;
import javax.slee.profile.ProfileTable;

import com.opencloud.logging.StdErrLog;
import com.opencloud.sleetck.lib.profileutils.BaseMessageSender;
import com.opencloud.sleetck.lib.rautils.RMIObjectChannel;
import com.opencloud.sleetck.lib.rautils.TCKRAUtils;
import com.opencloud.sleetck.lib.resource.events.TCKResourceEventX;
import com.opencloud.sleetck.lib.sbbutils.BaseTCKSbb;
import com.opencloud.sleetck.lib.sbbutils.TCKSbbUtils;
import com.opencloud.sleetck.lib.sbbutils.events.TCKSbbEvent;
import com.opencloud.sleetck.lib.sbbutils.events.TCKSbbEventImpl;

public abstract class Test1110346Sbb extends BaseTCKSbb {

    public static int MODE_BUSINESS = 1;
    public static int MODE_BUSINESS_ROLLBACK = 2;
    public static final String PROFILE_TABLE_NAME = "Test1110346ProfileTable";
    public static final String PROFILE_NAME = "Test1110346Profile";
    public static final int BUSINESS = 1;
    public static final int BUSINESS_ROLLBACK = 2;
    public static final int SET_BUSINESS = 3;
    public static final int SET_BUSINESS_ROLLBACK = 4;


    //Sbb lifecycle method. Acquire RMIObjectChannel output stream
    public void setSbbContext(SbbContext context) {
        super.setSbbContext(context);

        try {
            out = TCKRAUtils.lookupRMIObjectChannel();
            msgSender = new BaseMessageSender(out, new StdErrLog());
        }
        catch (Exception e) {
            context.getTracer(getSbbID().getName()).warning("An error occured creating an RMIObjectChannel:", e);
        }

    }

    public void onTCKResourceEventX1(TCKResourceEventX event, ActivityContextInterface aci) {
        try {

            int operationID = ((Integer)event.getMessage()).intValue();
            Context env;
            ProfileFacility profileFacility;
            ProfileTable profileTable;
            ProfileContextTestsProfileLocal profileLocal;

            switch (operationID) {
            case SET_BUSINESS_ROLLBACK:
                setMode(MODE_BUSINESS_ROLLBACK);
                fireTCKSbbEvent(new TCKSbbEventImpl("Set mode to BUSINESS_ROLLBACK"), aci, null);
                break;
            case SET_BUSINESS:
                setMode(MODE_BUSINESS);
                fireTCKSbbEvent(new TCKSbbEventImpl("Set mode to BUSINESS"), aci, null);
                break;
            case BUSINESS:

                env = TCKSbbUtils.getSbbEnvironment();
                profileFacility = (ProfileFacility)env.lookup("slee/facilities/profile");
                msgSender.sendLogMsg("Obtained profileFacility object.");
                profileTable = profileFacility.getProfileTable(PROFILE_TABLE_NAME);
                msgSender.sendLogMsg("Obtained profileTable object.");

                profileLocal = (ProfileContextTestsProfileLocal) profileTable.find(PROFILE_NAME);
                msgSender.sendLogMsg("Obtained profileLocal object.");

                profileLocal.business();

                fireTCKSbbEvent(new TCKSbbEventImpl("Business method executed without rollback."), aci, null);
                break;
            case BUSINESS_ROLLBACK:

                env = TCKSbbUtils.getSbbEnvironment();
                profileFacility = (ProfileFacility)env.lookup("slee/facilities/profile");
                msgSender.sendLogMsg("Obtained profileFacility object.");
                profileTable = profileFacility.getProfileTable(PROFILE_TABLE_NAME);
                msgSender.sendLogMsg("Obtained profileTable object.");

                profileLocal = (ProfileContextTestsProfileLocal) profileTable.find(PROFILE_NAME);
                msgSender.sendLogMsg("Obtained profileLocal object.");

                getSbbContext().setRollbackOnly();
                profileLocal.getRollbackTest();
                break;
            }

        } catch(Exception e) {
            msgSender.sendException(e);
        }
    }

    public void onTCKSbbEvent(TCKSbbEvent event, ActivityContextInterface aci) {
        try {

            msgSender.sendSuccess(1110346, (String)event.getMessage());

        } catch(Exception e) {
            TCKSbbUtils.handleException(e);
        }
    }


    public void sbbRolledBack(RolledBackContext context) {
        //send ACK to the TCK test and indicate that the previous transaction was rolled back
        if (getMode()==MODE_BUSINESS)
            msgSender.sendFailure(1110346, "Business method should have completed without exception.");
        else if (getMode()==MODE_BUSINESS_ROLLBACK)
            msgSender.sendSuccess(1110346, "Transaction rolled back as expected.");
    }

    public abstract void fireTCKSbbEvent(TCKSbbEvent event, ActivityContextInterface aci, Address address);

    public abstract void setMode(int value);
    public abstract int getMode();

    private RMIObjectChannel out;
    private BaseMessageSender msgSender;

}
