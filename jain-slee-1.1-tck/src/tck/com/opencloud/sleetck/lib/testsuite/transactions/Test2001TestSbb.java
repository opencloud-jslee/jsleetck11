/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.transactions;

import com.opencloud.sleetck.lib.sbbutils.BaseTCKSbb;
import com.opencloud.sleetck.lib.sbbutils.TCKSbbUtils;
import com.opencloud.sleetck.lib.resource.events.TCKResourceEventX;
import com.opencloud.sleetck.lib.resource.sbbapi.TCKActivity;
import javax.slee.ActivityContextInterface;
import javax.slee.facilities.Level;
import com.opencloud.sleetck.lib.TCKTestFailureException;

/**
 * Sbb class for Test 2001
 */
public abstract class Test2001TestSbb extends BaseTCKSbb {

    /**
     * Sends an ACK to the test, then sets the CMP field to a non-null value
     */
    public void onTCKResourceEventX1(TCKResourceEventX event, ActivityContextInterface aci) {
        try {
            TCKSbbUtils.createTrace(getSbbID(),Level.INFO,"Received TCKResourceEventX1 event",null);
            TCKSbbUtils.getResourceInterface().sendSbbMessage(TCKResourceEventX.X1);
            setValue("foo");
            TCKSbbUtils.createTrace(getSbbID(),Level.INFO,"Set CMP field to a non-null value",null);
        } catch(Exception e) {
            TCKSbbUtils.handleException(e);
        }
    }

    /**
     * Sends a positive ACK to the test if the CMP field is set to a non-null value.
     * If set to null, sends a TCKTestFailureException instead.
     */
    public void onTCKResourceEventX2(TCKResourceEventX event, ActivityContextInterface aci) {
        try {
            TCKSbbUtils.createTrace(getSbbID(),Level.INFO,"Received TCKResourceEventX2 event",null);
            String value = getValue();
            if(value == null) throw new TCKTestFailureException(2001,"The CMP field was null, i.e. the changes made "+
                "by the first event handler were not persisted");
            TCKSbbUtils.getResourceInterface().sendSbbMessage(TCKResourceEventX.X2);
        } catch(Exception e) {
            TCKSbbUtils.handleException(e);
        }
    }

    // The CMP field
    public abstract void setValue(String value);
    public abstract String getValue();

}