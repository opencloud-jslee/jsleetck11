/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.sbb.abstractclass;

import com.opencloud.sleetck.lib.AbstractSleeTCKTest;
import com.opencloud.sleetck.lib.TCKTestResult;
import com.opencloud.sleetck.lib.testutils.QueuingResourceListener;
import com.opencloud.sleetck.lib.testutils.Assert;
import com.opencloud.sleetck.lib.resource.TCKActivityID;
import com.opencloud.sleetck.lib.resource.events.TCKResourceEventX;

/**
 * Test assertion 2013: that no more methods are invoked on an SBB object after an uncaught exception is thrown.
 */
public class Test2013Test extends AbstractSleeTCKTest {
    public TCKTestResult run() throws Exception {
        final QueuingResourceListener listener = new QueuingResourceListener(utils());
        final TCKActivityID activity = utils().getResourceInterface().createActivity("Test2013Activity");
        setResourceListener(listener);

        utils().getResourceInterface().fireEvent(TCKResourceEventX.X1, "Test2013Sbb_A", activity, null);
        utils().getResourceInterface().fireEvent(TCKResourceEventX.X2, null, activity, null);
        String response = (String) listener.nextMessage().getMessage();
        Assert.assertEquals(2013, "Wrong message received from SBB, expected unsetSbbContext() to be called while Sbb " +
                                  "object is moved to the does not exist state.", "NewSbb", response);

        return TCKTestResult.passed();
    }
}
