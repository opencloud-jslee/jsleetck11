/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.activities.activitycontext;

import javax.slee.ActivityEndEvent;
import javax.slee.ActivityContextInterface;
import javax.slee.SbbContext;

import com.opencloud.sleetck.lib.sbbutils.BaseTCKSbb;
import com.opencloud.sleetck.lib.sbbutils.TCKSbbUtils;
import com.opencloud.sleetck.lib.sbbutils.TCKSbbConstants;
import com.opencloud.sleetck.lib.resource.events.TCKResourceEventX;
import com.opencloud.sleetck.lib.resource.TCKActivityID;
import com.opencloud.sleetck.lib.resource.sbbapi.TCKResourceSbbInterface;
import com.opencloud.sleetck.lib.resource.sbbapi.TCKActivityContextInterfaceFactory;

import java.util.HashMap;

public abstract class Test588Sbb extends BaseTCKSbb {

    public void onTCKResourceEventX1(TCKResourceEventX event, ActivityContextInterface aci) {

        try {

            HashMap map = new HashMap();

            // Write-only attribute, should be able to write, but not read.
            asSbbActivityContextInterface(aci).setValueA(42);

            // Read-only attribute, should be able to read, but not write.
            asSbbActivityContextInterface(aci).getValueB();

            // Read-write attribute, should be able to read and write.
            asSbbActivityContextInterface(aci).setValueC(42);
            if (asSbbActivityContextInterface(aci).getValueC() != 42) {
                map.put("Result", new Boolean(false));
                map.put("Message", "Failed to set/get activity context interface attribute correctly.");
            } else {
                map.put("Result", new Boolean(true));
                map.put("Message", "Ok");
            }

            TCKSbbUtils.getResourceInterface().sendSbbMessage(map);
        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }

    }

    public abstract Test588SbbActivityContextInterface asSbbActivityContextInterface(ActivityContextInterface aci);

}
