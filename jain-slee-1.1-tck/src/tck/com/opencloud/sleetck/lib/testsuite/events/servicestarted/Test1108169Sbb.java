/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.events.servicestarted;

import java.rmi.RemoteException;
import java.security.AccessController;
import java.security.PrivilegedAction;
import java.util.HashMap;

import javax.slee.ActivityContextInterface;
import javax.slee.EventContext;
import javax.slee.InitialEventSelector;
import javax.slee.facilities.Tracer;
import javax.slee.serviceactivity.ServiceStartedEvent;

import com.opencloud.sleetck.lib.rautils.RMIObjectChannel;
import com.opencloud.sleetck.lib.rautils.TCKRAUtils;
import com.opencloud.sleetck.lib.sbbutils.BaseTCKSbb;
import com.opencloud.sleetck.lib.sbbutils.TCKSbbUtils;

/**
 * AssertionID(1108169): Test of A Service starts when the persistent state of the 
 * Service says that the Service should be active, and a previously stopped SLEE 
 * transitions to the Running state.
 *
 */
public abstract class Test1108169Sbb extends BaseTCKSbb {

    public InitialEventSelector initialEventSelectorMethod(InitialEventSelector ies) {
        // set the custom name variable to true to test that this value is not held accross invocations
        ies.setCustomName("test");
        return ies;
    }

    public void onServiceStartedEvent(ServiceStartedEvent event, ActivityContextInterface aci, EventContext context) {
        try {
            tracer = getSbbContext().getTracer("com.test");
            tracer.info("Sbb: Received a TCK event " + event);

            try {
                out = TCKRAUtils.lookupRMIObjectChannel();
            } catch (Exception e) {
                out = null;
                tracer.severe("An error occured creating the RMIObjectChannel for RA Object: " + e);
            }

            sendResultToTCK(1108169, "Test of Test1108169 Passed.", "Test1108169Test", true);

        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }
    }

    private void sendResultToTCK(int failedAssertionID, String message, String testName, boolean result) throws Exception {
        HashMap sbbData = new HashMap();
        sbbData.put("testname", testName);
        sbbData.put("result", result ? "pass" : "fail");
        sbbData.put("message", message);
        sbbData.put("id", new Integer(failedAssertionID));
        sendMessage(sbbData);
    }

    protected void sendMessage(final Object obj) {
        AccessController.doPrivileged(new PrivilegedAction() {
            public Object run() {
                if (out == null) {
                    tracer.severe("Attempted to write to the RMIObjectChannel before it was initialized: , message="
                            + obj);
                    return null;
                }
                try {
                    out.writeObject(obj);
                } catch (RemoteException e) {
                    tracer.severe("An error occured writing to the RMIObjectChannel for RA Object: " + e);
                }
                return null;
            }
        });
    }

    private Tracer tracer;

    private RMIObjectChannel out;
}
