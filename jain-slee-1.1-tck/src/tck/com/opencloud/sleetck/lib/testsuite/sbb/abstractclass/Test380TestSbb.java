/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.sbb.abstractclass;

import com.opencloud.sleetck.lib.sbbutils.BaseTCKSbb;
import com.opencloud.sleetck.lib.sbbutils.TCKSbbUtils;
import com.opencloud.sleetck.lib.resource.events.TCKResourceEventX;
import com.opencloud.sleetck.lib.resource.sbbapi.TCKActivity;
import javax.slee.ActivityContextInterface;
import javax.slee.facilities.Level;

/**
 * Implements an event handler and sbbStore(), both of which send an ACK to the test.
 */
public abstract class Test380TestSbb extends BaseTCKSbb {

    public void onTCKResourceEventX1(TCKResourceEventX event, ActivityContextInterface aci) {
        try {
            TCKSbbUtils.createTrace(getSbbID(),Level.INFO,"Received the TCKResourceEventX1 event",null);
            TCKSbbUtils.getResourceInterface().sendSbbMessage(Test380TestConstants.RECEIVED_EVENT);
        } catch(Exception e) {
            TCKSbbUtils.handleException(e);
        }
    }

    public void sbbStore() {
        try {
            TCKSbbUtils.createTrace(getSbbID(),Level.INFO,"Received sbbStore() call",null);
            Object txnID = TCKSbbUtils.getResourceAdaptorInterface().getTransactionIDAccess().getCurrentTransactionID();
            String message = txnID != null ? Test380TestConstants.SBB_STORE_CALLED_WITH_TXN :
                                             Test380TestConstants.SBB_STORE_CALLED_NO_TXN;
            TCKSbbUtils.getResourceInterface().sendSbbMessage(message);
        } catch(Exception e) {
            TCKSbbUtils.handleException(e);
        }
    }

    // A token CMP field to prevent the SLEE from detecting that there is no
    // persistent state to synchronize
    public abstract void setValue(String value);
    public abstract String getValue();

}