/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.management.ProfileProvisioningMBean;

import com.opencloud.sleetck.lib.*;
import com.opencloud.sleetck.lib.infra.SleeTCKComponentConstants;
import com.opencloud.sleetck.lib.resource.testapi.TCKResourceListener;
import com.opencloud.sleetck.lib.testutils.BaseTCKResourceListener;
import com.opencloud.sleetck.lib.testutils.ComponentIDLookup;
import com.opencloud.sleetck.lib.testutils.ProfileUtils;
import com.opencloud.sleetck.lib.testutils.jmx.ProfileProvisioningMBeanProxy;

import javax.slee.InvalidArgumentException;
import javax.slee.profile.ProfileSpecificationID;
import java.rmi.RemoteException;
import java.util.Collection;
import java.util.Iterator;
import java.util.Vector;

/**
 * Tries to create profile tables with valid and invalid names.
 */
public class ProfileTableNamesTest implements SleeTCKTest {

    private static final String PROFILE_DU_PATH_PARAM = "profileDUPath";
    private static final int ASSERTION_ID = 2426;

    public void init(SleeTCKTestUtils utils) {
        this.utils = utils;
    }

    /**
     * Perform the actual test.
     */
    public TCKTestResult run() throws Exception {
        ProfileProvisioningMBeanProxy profileProvisioningProxy = profileUtils.getProfileProvisioningProxy();

        ProfileSpecificationID profileSpecID = new ComponentIDLookup(utils).lookupProfileSpecificationID(
                "SimpleProfile",SleeTCKComponentConstants.TCK_VENDOR,"1.0");
        if(profileSpecID == null) throw new TCKTestErrorException("No ProfileSpecification found.");

        // create a single name using the well-known valid characters: 0x0020-0x007e (except 0x002f: '/')
        StringBuffer validNameBuf = new StringBuffer();
        for (char validChar = 0x0020; validChar < 0x002e; validChar++) {
            validNameBuf.append(validChar);
        }
        for (char validChar = 0x0030; validChar < 0x007e; validChar++) {
            validNameBuf.append(validChar);
        }
        String validName = validNameBuf.toString();
        try {
            utils.getLog().info("Attempting to create a profile table with the following valid name: "+validName);
            profileProvisioningProxy.createProfileTable(profileSpecID,validName);
            tablesAdded.addElement(validName);
            utils.getLog().info("The createProfileTable() method threw no Exception. Calling getProfileTables() to check whether the "+
                    "profile table was created");
            Collection profileTableNames = profileProvisioningProxy.getProfileTables();
            if(profileTableNames == null || !profileTableNames.contains(validName))
                throw new TCKTestFailureException(ASSERTION_ID,"Failed to create a profile table "+
                    "with the following valid name: "+validName+". createProfileTable() threw no Exception, "+
                    "but the profile table name did not exist in the set of names returned by getProfileTables().");
            utils.getLog().info("The profile table was created successfully");
        } catch (InvalidArgumentException e) {
            throw new TCKTestFailureException(ASSERTION_ID,"Caught unexpected InvalidArgumentException when trying to create a "+
                    "profile table with the following valid name: "+validName,e);
        }

        // build a list of invalid characters: the first 32 unicode characters, the '/' character, and the delete character
        Vector invalidCharacters = new Vector();
        for (char invalidChar = 0x0000; invalidChar < 0x001f; invalidChar++) {
            invalidCharacters.addElement(new Character(invalidChar));
        }
        invalidCharacters.addElement(new Character('/'));
        invalidCharacters.addElement(new Character('\u007f')); // the delete character

        Iterator invalidCharactersIter = invalidCharacters.iterator();
        while(invalidCharactersIter.hasNext()) {
            Character invalidCharacter = (Character)invalidCharactersIter.next();
            int unicodeValue = (int)invalidCharacter.charValue();
            try {
                profileProvisioningProxy.createProfileTable(profileSpecID,invalidCharacter.toString());
                tablesAdded.addElement(invalidCharacter.toString());
                return TCKTestResult.failed(ASSERTION_ID, "The SLEE did not throw the expected InvalidArgumentException when trying "+
                        "to create a profile table with an invalid name. "+
                        "The invalid name was a single character of unicode value "+unicodeValue);
            } catch (InvalidArgumentException e) {
                utils.getLog().info("Caught expected InvalidArgumentException for an invalid profile table name with a character of "+
                        "unicode value "+unicodeValue+". Error message: "+e.getMessage());
            }
        }

        return TCKTestResult.passed();
    }

    /**
     * Do all the pre-run configuration of the test.
     */
    public void setUp() throws Exception {
        tablesAdded = new Vector();

        utils.getLog().fine("Connecting to resource");
        resourceListener = new TCKResourceListenerImpl();
        utils.getResourceInterface().setResourceListener(resourceListener);

        // Install the Deployable Units
        utils.getLog().fine("Installing the profile spec");
        String duPath = utils.getTestParams().getProperty(PROFILE_DU_PATH_PARAM);
        utils.install(duPath);
        profileUtils = new ProfileUtils(utils);

    }

    /**
     * Clean up after the test.
     */
    public void tearDown() throws Exception {
        if (profileUtils != null && tablesAdded != null && !tablesAdded.isEmpty()) {
            Iterator tablesAddedIter = tablesAdded.iterator();
            while(tablesAddedIter.hasNext()) {
                try {
                    profileUtils.removeProfileTable((String)tablesAddedIter.next());
                } catch (Exception e) {
                    utils.getLog().warning(e);
                }
            }
        }
        utils.getLog().fine("Uninstalling the profile spec");
        utils.uninstallAll();
        utils.getLog().fine("Disconnecting from resource");
        utils.getResourceInterface().removeResourceListener();
    }

    private class TCKResourceListenerImpl extends BaseTCKResourceListener {
        public void onException(Exception e) throws RemoteException {
            utils.getLog().warning("Received exception from the TCK resource");
            utils.getLog().warning(e);
        }
    }

    private SleeTCKTestUtils utils;
    private TCKResourceListener resourceListener;
    private ProfileUtils profileUtils;
    private Vector tablesAdded;

}
