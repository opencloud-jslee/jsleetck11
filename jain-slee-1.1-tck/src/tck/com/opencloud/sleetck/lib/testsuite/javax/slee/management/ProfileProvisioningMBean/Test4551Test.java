/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.javax.slee.management.ProfileProvisioningMBean;

import javax.slee.management.*;
import javax.slee.ComponentID;
import javax.slee.ServiceID;
import javax.slee.profile.ProfileSpecificationID;
import javax.slee.profile.UnrecognizedProfileNameException;
import javax.management.ObjectName;

import com.opencloud.sleetck.lib.*;
import com.opencloud.sleetck.lib.testutils.*;
import com.opencloud.sleetck.lib.resource.TCKSbbMessage;
import com.opencloud.sleetck.lib.resource.testapi.TCKResourceListener;
import com.opencloud.sleetck.lib.resource.TCKActivityID;
import com.opencloud.sleetck.lib.resource.testapi.TCKResourceTestInterface;
import com.opencloud.sleetck.lib.resource.events.TCKResourceEventX;
import com.opencloud.sleetck.lib.testutils.ProfileUtils;

import com.opencloud.sleetck.lib.testutils.jmx.DeploymentMBeanProxy;
import com.opencloud.sleetck.lib.testutils.jmx.ServiceManagementMBeanProxy;
import com.opencloud.sleetck.lib.testutils.jmx.ProfileProvisioningMBeanProxy;
import com.opencloud.sleetck.lib.testutils.jmx.ProfileMBeanProxy;

import com.opencloud.logging.Logable;

import java.rmi.RemoteException;
import java.util.HashMap;

public class Test4551Test implements SleeTCKTest {

    private static final String SERVICE_DU_PATH_PARAM = "DUPath";
    private static final int TEST_ID = 4551;

    public void init(SleeTCKTestUtils utils) {
        this.utils = utils;
    }

    /**
     * Perform the actual test.
     */

    public TCKTestResult run() throws Exception {

        result = new FutureResult(utils.getLog());

        // Create the Profile but don't commit it.
        profileProxy = profileUtils.getProfileProvisioningProxy();
        DeploymentMBeanProxy duProxy = utils.getDeploymentMBeanProxy();
        DeployableUnitDescriptor duDesc = duProxy.getDescriptor(duID);
        ComponentID components[] = duDesc.getComponents();

        for (int i = 0; i < components.length; i++) {
            if (components[i] instanceof ProfileSpecificationID) {
                ProfileSpecificationID profileSpecID = (ProfileSpecificationID) components[i];

                // Create the profile table.
                try {
                    profileProxy.createProfileTable(profileSpecID, "Test4551ProfileTable");
                } catch (Exception e) {
                    return TCKTestResult.error("Failed to create profile table.");
                }

                profileObjectName = profileProxy.createProfile(Test4551Sbb.PROFILE_TABLE_NAME, Test4551Sbb.PROFILE_NAME);
                utils.getLog().fine("Created profile 'Test4551ProfileTable/Test4551Profile'");
                break;
            }
        }

        // Fire the first event for the SBB to receive.
        TCKResourceTestInterface resource = utils.getResourceInterface();
        TCKActivityID activityID = resource.createActivity("Test4551InitialActivity");
        resource.fireEvent(TCKResourceEventX.X1, TCKResourceEventX.X1, activityID, null);

        return result.waitForResultOrFail(utils.getTestTimeout(), "Timeout waiting for test result.", TEST_ID);

    }

    /**
     * Do all the pre-run configuration of the test.
     */

    public void setUp() throws Exception {

        utils.getLog().fine("Connecting to resource");
        resourceListener = new TCKResourceListenerImpl();
        utils.getResourceInterface().setResourceListener(resourceListener);
        utils.getLog().fine("Installing and activating service");

        profileUtils = new ProfileUtils(utils);

        // Install the Deployable Units
        String duPath = utils.getTestParams().getProperty(SERVICE_DU_PATH_PARAM);
        duID = utils.install(duPath);
        utils.activateServices(duID, true);

    }

    /**
     * Clean up after the test.
     */

    public void tearDown() throws Exception {

        if (profileProxy != null) {

            try {
                ProfileMBeanProxy profProxy = utils.getMBeanProxyFactory().createProfileMBeanProxy(profileObjectName);
                profProxy.commitProfile();
                profProxy.closeProfile();
            } catch (Exception e) {
            }

            try {
                profileProxy.removeProfile(Test4551Sbb.PROFILE_TABLE_NAME, Test4551Sbb.PROFILE_NAME);
            } catch (Exception e) {
            }
            try {
                profileProxy.removeProfileTable(Test4551Sbb.PROFILE_TABLE_NAME);
            } catch (Exception e) {
            }
        }
        utils.getLog().fine("Disconnecting from resource");
        utils.getResourceInterface().clearActivities();
        utils.getResourceInterface().removeResourceListener();
        utils.getLog().fine("Deactivating and uninstalling service");
        utils.deactivateAllServices();
        utils.uninstallAll();
    }

    private class TCKResourceListenerImpl extends BaseTCKResourceListener {
        public synchronized void onSbbMessage(TCKSbbMessage message, TCKActivityID calledActivity) throws RemoteException {
            utils.getLog().info("Received message from SBB");
            HashMap map = (HashMap) message.getMessage();

            Integer stage = (Integer) map.get("Stage");
            boolean passed = ((Boolean) map.get("Result")).booleanValue();
            String msgString = (String) map.get("Message");

            utils.getLog().info("Stage: " + stage + ", Passed: " + passed + ", Message: " + msgString);

            if (stage.intValue() == 1) {
                if (!passed) {
                    result.setFailed(TEST_ID, msgString);
                    return;
                } else {
                    try {

                        // 2356 - Before the profile commits, it cannot be accessed via mgmt iface.
                        passed = false;
                        try {
                            profileProxy.getProfile(Test4551Sbb.PROFILE_TABLE_NAME, Test4551Sbb.PROFILE_NAME);
                        } catch (javax.slee.profile.UnrecognizedProfileTableNameException e) {
                            passed = true;
                        } catch (javax.slee.profile.UnrecognizedProfileNameException e) {
                            passed = true;
                        } catch (Exception e) {
                            utils.getLog().fine("Exception thrown in getProfile: " + e);
                        }

                        if (!passed) {
                            utils.getLog().fine("Was incorrectly able to retrieve uncommitted profile via MGMT iface (2356)");
                            result.setFailed(2356, "Profile created via ProfileProvisioningMBean.createProfile(...) was retrievable through ProfileProvisioningMBean.getProfile(...) before it had been committed.");
                            return;
                        }


                        // Commit the created profile.
                        ProfileMBeanProxy profProxy = utils.getMBeanProxyFactory().createProfileMBeanProxy(profileObjectName);
                        profProxy.commitProfile();
                        profProxy.closeProfile();

                        utils.getLog().fine("Committed profile 'Test4551ProfileTable/Test4551Profile'");

                        // Fire the second event for the SBB to received.
                        TCKResourceTestInterface resource = utils.getResourceInterface();
                        TCKActivityID activityID = resource.createActivity("Test4551InitialActivity2");
                        resource.fireEvent(TCKResourceEventX.X2, TCKResourceEventX.X2, activityID, null);                    

                    } catch (Exception e) {
                        onException(e);
                    }

                    return;
                }
            } else {
                if (!passed) {
                    result.setFailed(TEST_ID, msgString);
                    return;
                } else {
                    result.setPassed();
                    return;
                }
            }

        }

        public void onException(Exception e) throws RemoteException {
            utils.getLog().warning("Received exception from SBB");
            utils.getLog().warning(e);
            result.setError(e);
        }
    }

    private ProfileProvisioningMBeanProxy profileProxy;
    private SleeTCKTestUtils utils;
    private TCKResourceListener resourceListener;
    private FutureResult result;
    private DeployableUnitID duID;
    private ProfileUtils profileUtils;
    private ObjectName profileObjectName;
}
