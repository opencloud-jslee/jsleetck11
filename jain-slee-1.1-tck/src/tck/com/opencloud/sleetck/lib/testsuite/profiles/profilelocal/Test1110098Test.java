/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.profiles.profilelocal;

import javax.slee.profile.ProfileLocalObject;

import com.opencloud.sleetck.lib.AbstractSleeTCKTest;
import com.opencloud.sleetck.lib.TCKTestFailureException;
import com.opencloud.sleetck.lib.TCKTestResult;

interface Test1110098_1ProfileLocal extends ProfileLocalObject {

}

public class Test1110098Test extends AbstractSleeTCKTest {

    private static final String PROFILE_LOCAL_PCK_PRIVATE_DU_PATH_PARAM= "ProfileLocalPackagePrivateDUPath";
    private static final String PROFILE_LOCAL_NO_EXTEND_DU_PATH_PARAM= "ProfileLocalNoExtendDUPath";
    private static final String SIGNATURE_DIFF_DU_PATH_PARAM= "SignatureDiffDUPath";
    private static final String START_WITH_EJB_DU_PATH_PARAM= "StartWithEJBDUPath";
    private static final String START_WITH_PROFILE_DU_PATH_PARAM= "StartWithProfileDUPath";
    private static final String EQUALS_MBEAN_DU_PATH_PARAM= "EqualsMBeanDUPath";
    private static final String EQUALS_PROFILE_MNGMT_DU_PATH_PARAM= "EqualsProfileManagementDUPath";
    private static final String EQUALS_PROFILE_DU_PATH_PARAM= "EqualsProfileDUPath";
    private static final String SAME_SIGNATURE_AS_CMP_DU_PATH_PARAM= "SameSignatureAsCMPDUPath";
    private static final String COMBINED_POSITIVE_DU_PATH_PARAM= "CombinedPositiveDUPath";
    private static final String THROWS_RMI_EX_DU_PATH_PARAM= "ThrowsRMIExDUPath";
    private static final String INVALID_SUPER_DU_PATH_PARAM= "InvalidSuperDUPath";

    /**
     * This test trys to deploy a number of profile specification scenarios with ProfileLocal interface that break the requirements as outlined
     * in the SLEE1.1 specification (Section 10.8 ProfileLocal interface)(Public Review Draft).
     * The test fails if one of the theoretically invalid profile specs is deployed successfully into the SLEE.
     * There is one valid profile specification (spec 10) which combines the cases in which the deployment should be successful while the ProfileLocal interface configuration
     * however is so unusual (superinterfaces, methods throws clauses etc) that it is not very likely covered in other tests.
     */
    public TCKTestResult run() throws Exception {

        //Spec 1: ProfileLocal interface is not declared as public
        getLog().fine("Deploying profile spec.");
        try {
            setupService(PROFILE_LOCAL_PCK_PRIVATE_DU_PATH_PARAM);
            return TCKTestResult.failed(1110097,"Deployment of Profile spec with ProfileLocal interface not declared public should fail but was successful.");
        }
        catch (Exception e) {
            getLog().fine("Caught expected exception when trying to deploy profile spec. Exception: "+e.getMessage());
        }

        //Spec 2: ProfileLocal interface does not extend javax.slee.profile.ProfileLocalObject
        getLog().fine("Deploying profile spec.");
        try {
            setupService(PROFILE_LOCAL_NO_EXTEND_DU_PATH_PARAM);
            return TCKTestResult.failed(1110098,"Deployment of Profile spec with ProfileLocal interface not extending javax.slee.profile.ProfileLocalObject should fail but was successful.");
        }
        catch (Exception e) {
            getLog().fine("Caught expected exception when trying to deploy profile spec. Exception: "+e.getMessage());
        }

        //Spec 3: get/set accessors on ProfileLocal interface differ in signature from the ones in the CMP interface
        getLog().fine("Deploying profile spec.");
        try {
            setupService(SIGNATURE_DIFF_DU_PATH_PARAM);
            return TCKTestResult.failed(1110102,"Deployment of Profile spec with get/set accessors in the ProfileLocal interface that differ in their signature from the ones in the CMP interface" +
                    " should fail but was successful.");
        }
        catch (Exception e) {
            getLog().fine("Caught expected exception when trying to deploy profile spec. Exception: "+e.getMessage());
        }

        //Spec 4: name of business method is starting with 'ejb'
        getLog().fine("Deploying profile spec.");
        try {
            setupService(START_WITH_EJB_DU_PATH_PARAM);
            return TCKTestResult.failed(1110105,"Deployment of Profile spec containing business method which has a name starting with 'ejb'" +
                    " should fail but was successful.");
        }
        catch (Exception e) {
            getLog().fine("Caught expected exception when trying to deploy profile spec. Exception: "+e.getMessage());
        }

        //Spec 5: name of business method is starting with 'profile'
        getLog().fine("Deploying profile spec.");
        try {
            setupService(START_WITH_PROFILE_DU_PATH_PARAM);
            return TCKTestResult.failed(1110105,"Deployment of Profile spec containing business method which has a name starting with 'profile'" +
                    " should fail but was successful.");
        }
        catch (Exception e) {
            getLog().fine("Caught expected exception when trying to deploy profile spec. Exception: "+e.getMessage());
        }

        //Spec 6: name of business method is the same as one of the methods defined in javax.slee.profile.ProfileMBean
        getLog().fine("Deploying profile spec.");
        try {
            setupService(EQUALS_MBEAN_DU_PATH_PARAM);
            return TCKTestResult.failed(1110105,"Deployment of Profile spec containing business method which has a name equal to one of the methods in javax.slee.profile.ProfileMBean" +
                    " should fail but was successful.");
        }
        catch (Exception e) {
            getLog().fine("Caught expected exception when trying to deploy profile spec. Exception: "+e.getMessage());
        }

        //Spec 7: name of business method is the same as one of the methods defined in javax.slee.profile.ProfileManagement
        getLog().fine("Deploying profile spec.");
        try {
            setupService(EQUALS_PROFILE_MNGMT_DU_PATH_PARAM);
            return TCKTestResult.failed(1110105,"Deployment of Profile spec containing business method which has a name equal to one of the methods in javax.slee.profile.ProfileManagement" +
                    " should fail but was successful.");
        }
        catch (Exception e) {
            getLog().fine("Caught expected exception when trying to deploy profile spec. Exception: "+e.getMessage());
        }

        //Spec 8: name of business method is the same as one of the methods defined in javax.slee.profile.Profile
        getLog().fine("Deploying profile spec.");
        try {
            setupService(EQUALS_PROFILE_DU_PATH_PARAM);
            return TCKTestResult.failed(1110105,"Deployment of Profile spec containing business method which has a name equal to one of the methods in javax.slee.profile.Profile" +
                    " should fail but was successful.");
        }
        catch (Exception e) {
            getLog().fine("Caught expected exception when trying to deploy profile spec. Exception: "+e.getMessage());
        }

        //Spec 9: name and arguments of a business method are the same as one of the CMP accessor methods
        getLog().fine("Deploying profile spec.");
        try {
            setupService(SAME_SIGNATURE_AS_CMP_DU_PATH_PARAM);
            return TCKTestResult.failed(1110107,"Deployment of Profile spec containing business method which has same name AND arguments as a CMP accessor method" +
              " should fail but was successful.");
        }
        catch (Exception e) {
            getLog().fine("Caught expected exception when trying to deploy profile spec. Exception: "+e.getMessage());
        }

        //Spec 10:     - name but NOT arguments of a business method are the same as one of the CMP accessor methods
        //            - business method throws exceptions != java.rmi.RemoteException
        //            - ProfileLocal interface has superinterface which adheres to ProfileLocal interface rules
        //combined positive case
        getLog().fine("Deploying profile spec.");
        try {
            setupService(COMBINED_POSITIVE_DU_PATH_PARAM);
            getLog().fine("Deployment of Profile spec containing a business method which has the same name but NOT arguments as a CMP accessor method, a business method that defines throws statements for " +
                    " exceptions different to java.rmi.RemoteException and has got a superinterface which fulfilles all requirements of a ProfileLocal interface was fine.");
        }
        catch (Exception e) {
            return TCKTestResult.failed(new TCKTestFailureException(1110107,"Deployment of Profile spec containing a business method which has the same name but NOT arguments as a CMP accessor method, a business method that defines throws statements for " +
                    " exceptions different to java.rmi.RemoteException and has got a superinterface which fulfilles all requirements of a ProfileLocal interface" +
            " should succeed but failed.", e));
        }

        //Spec 12: Business method throws java.rmi.RemoteException
        getLog().fine("Deploying profile spec.");
        try {
            setupService(THROWS_RMI_EX_DU_PATH_PARAM);
            return TCKTestResult.failed(1110108,"Deployment of Profile spec containing business method which throws java.rmi.RemoteException" +
              " should fail but was successful.");
        }
        catch (Exception e) {
            getLog().fine("Caught expected exception when trying to deploy profile spec. Exception: "+e.getMessage());
        }

        //Spec 13: ProfileLocal interface has superinterface which breaks ProfileLocal interface rules.
        getLog().fine("Deploying profile spec.");
        try {
            setupService(INVALID_SUPER_DU_PATH_PARAM);
            return TCKTestResult.failed(1110114,"Deployment of Profile spec with superinterface breaking the ProfileLocal interface rules" +
              " should fail but was successful.");
        }
        catch (Exception e) {
            getLog().fine("Caught expected exception when trying to deploy profile spec. Exception: "+e.getMessage());
        }

        return TCKTestResult.passed();
    }


    public void setUp() throws Exception {

    }

    public void tearDown() throws Exception {


        super.tearDown();
    }

}
