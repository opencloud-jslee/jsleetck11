/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.facilities.alarmfacility;

import java.util.HashMap;

import javax.naming.InitialContext;
import javax.slee.ActivityContextInterface;
import javax.slee.facilities.AlarmFacility;
import javax.slee.facilities.AlarmLevel;
import javax.slee.facilities.Tracer;

import com.opencloud.sleetck.lib.resource.events.TCKResourceEventY;
import com.opencloud.sleetck.lib.sbbutils.BaseTCKSbb;
import com.opencloud.sleetck.lib.sbbutils.TCKSbbUtils;

/*  
 * AssertionID(1113506): Test This method only clears alarms 
 * associated with the notification source of the alarm facility 
 * object.
 * 
 */
public abstract class Test1113506Sbb1 extends BaseTCKSbb {

    public static final String ALARM_MESSAGE = "Test1113506AlarmMessageSbb1";

    public static final String ALARM_INSTANCEID = "Test1113506AlarmInstanceIDSbb1";

    public static final String ALARM_TYPE = "javax.slee.management.Alarm";

    // Initial event
    public void onTCKResourceEventY1(TCKResourceEventY event, ActivityContextInterface aci) {

        try {
            tracer = this.getSbbContext().getTracer("com.test");
            tracer.info("Received " + event + " message", null);
            AlarmFacility facility = getAlarmFacility();

            String[] alarmIDs = new String[5];
            // raise an alarm
            alarmIDs[0] = facility.raiseAlarm(ALARM_TYPE, "Test1113506AlarmInstanceID1", AlarmLevel.CRITICAL, ALARM_MESSAGE);
            alarmIDs[1] = facility.raiseAlarm(ALARM_TYPE, "Test1113506AlarmInstanceID2", AlarmLevel.MAJOR,
                    ALARM_MESSAGE);
            alarmIDs[2] = facility.raiseAlarm(ALARM_TYPE, "Test1113506AlarmInstanceID3", AlarmLevel.WARNING,
                    ALARM_MESSAGE);
            alarmIDs[3] = facility.raiseAlarm(ALARM_TYPE, "Test1113506AlarmInstanceID4", AlarmLevel.INDETERMINATE,
                    ALARM_MESSAGE);
            alarmIDs[4] = facility.raiseAlarm(ALARM_TYPE, "Test1113506AlarmInstanceID5", AlarmLevel.MINOR,
                    ALARM_MESSAGE);
            setAlarmIDs(alarmIDs);

        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }

    }

    // Third event
    public void onTCKResourceEventY2(TCKResourceEventY event, ActivityContextInterface aci) {

        try {
            tracer = this.getSbbContext().getTracer("com.test");
            tracer.info("Received " + event + " message again", null);
            AlarmFacility facility = getAlarmFacility();

            if (facility.clearAlarms(ALARM_TYPE) != getAlarmIDs().length) {
                sendResultToTCK("Test1113506Test", false, 1113506, "The return value of this facility.clearAlarms(alarmType) method "
                        + "didn't match the number of alarms that supposed to be cleared!"
                        + "AlarmFacility.clearAlarms(Alarm_type)= " + facility.clearAlarms(ALARM_TYPE)
                        + "vs Total alarms= " + getAlarmIDs().length * 2);
            return;
            } 
            
        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }

    }

    private void sendResultToTCK(String testName, boolean result, int failedAssertionID,  String message) throws Exception {
        HashMap sbbData = new HashMap();
        sbbData.put("testname", testName);
        sbbData.put("result", result ? "pass" : "fail");
        sbbData.put("message", message);
        sbbData.put("id", new Integer(failedAssertionID));
        TCKSbbUtils.getResourceInterface().sendSbbMessage(sbbData);
    }

    private AlarmFacility getAlarmFacility() throws Exception {
        AlarmFacility facility = null;
        String JNDI_ALARMFACILITY_NAME = AlarmFacility.JNDI_NAME;
        try {
            facility = (AlarmFacility) new InitialContext().lookup(JNDI_ALARMFACILITY_NAME);
        } catch (Exception e) {
            tracer.warning("got unexpected Exception: " + e, null);
        }
        return facility;
    }

    private Tracer tracer;

    public abstract void setAlarmIDs(String[] alarmIDs);
    public abstract String[] getAlarmIDs();

}
