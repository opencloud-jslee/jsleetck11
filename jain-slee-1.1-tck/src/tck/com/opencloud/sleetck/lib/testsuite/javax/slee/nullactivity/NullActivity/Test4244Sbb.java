/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.javax.slee.nullactivity.NullActivity;

import javax.slee.ActivityEndEvent;
import javax.slee.ActivityContextInterface;
import javax.slee.Address;
import javax.slee.SbbLocalObject;
import javax.slee.facilities.Level;
import javax.slee.ChildRelation;
import javax.slee.nullactivity.NullActivity;
import javax.slee.nullactivity.NullActivityFactory;
import javax.slee.nullactivity.NullActivityContextInterfaceFactory;
import javax.slee.facilities.ActivityContextNamingFacility;

import com.opencloud.sleetck.lib.sbbutils.BaseTCKSbb;
import com.opencloud.sleetck.lib.sbbutils.TCKSbbUtils;
import com.opencloud.sleetck.lib.sbbutils.TCKSbbConstants;
import com.opencloud.sleetck.lib.resource.events.TCKResourceEventX;
import com.opencloud.sleetck.lib.resource.TCKActivityID;
import com.opencloud.sleetck.lib.resource.sbbapi.TCKActivity;
import com.opencloud.sleetck.lib.resource.sbbapi.TCKResourceSbbInterface;
import com.opencloud.sleetck.lib.resource.sbbapi.TCKActivityContextInterfaceFactory;



import java.util.HashMap;

/**
 * SbbLocalObject.setSbbPriority(SbbLocalObject)
 */

public abstract class Test4244Sbb extends BaseTCKSbb {

    public void sbbRolledBack(javax.slee.RolledBackContext context) {
    }

    public void sbbExceptionThrown(Exception exception, Object event, ActivityContextInterface aci) {
    }

    public void onTCKResourceEventX1(TCKResourceEventX event, ActivityContextInterface aci) {

        try {

            // Create a NullActivity
            NullActivityFactory factory = (NullActivityFactory) TCKSbbUtils.getSbbEnvironment().lookup("slee/nullactivity/factory");
            NullActivity nullActivity = factory.createNullActivity();

            // Get its ActivityContextInterface
            NullActivityContextInterfaceFactory aciFactory = (NullActivityContextInterfaceFactory) TCKSbbUtils.getSbbEnvironment().lookup("slee/nullactivity/activitycontextinterfacefactory");
            ActivityContextInterface nullACI = null;

            try { // Test 4254
                nullACI = aciFactory.getActivityContextInterface(nullActivity);
                // Attach this SBB to the NullActivity
                nullACI.attach(getSbbContext().getSbbLocalObject());
            } catch (Exception e) {
                HashMap map = new HashMap();
                map.put("Result", new Boolean(false));
                map.put("Message", "Caught exception in NullActivityContextInterfaceFactory.getActivityContextInterface().");
                TCKSbbUtils.getResourceInterface().sendSbbMessage(map);
                return;
            }

            // Test 4244 - end the NullActivity, then fire event on it.
            nullActivity.endActivity();
            boolean passed = false;
            try {
                fireTest4244Event(new Test4244Event(), nullACI, null);
            } catch (java.lang.IllegalStateException e) {
                passed = true;
            }
            
            if (passed == false) {
                HashMap map = new HashMap();
                map.put("Result", new Boolean(false));
                map.put("Message", "Was able to fire event on activity that had been ended, did NullActivity.endActivity() really end the activity?");
                map.put("ID", new Integer(4244));
                TCKSbbUtils.getResourceInterface().sendSbbMessage(map);
                return;
            }

            // Create a new nullActivity and get its ACI
            nullActivity = factory.createNullActivity();
            nullACI = aciFactory.getActivityContextInterface(nullActivity);
            nullACI.attach(getSbbContext().getSbbLocalObject());

            // Fire two events on the new NullActivity
            fireTest4244Event(new Test4244Event(), nullACI, null);
            fireTest4244SecondEvent(new Test4244SecondEvent(), nullACI, null);

        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }

    }

    public void onTest4244Event(Test4244Event event, ActivityContextInterface aci) {

        try {

            // End the activity
            NullActivity nullActivity = (NullActivity) aci.getActivity();
            nullActivity.endActivity();

            // Mark TXN for rollback
            getSbbContext().setRollbackOnly();

        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }

    }

    public void onTest4244SecondEvent(Test4244SecondEvent event, ActivityContextInterface aci) {

        try {

            HashMap map = new HashMap();
            
            try {
                
                fireTest4244ThirdEvent(new Test4244ThirdEvent(), aci, null);
            } catch (java.lang.IllegalStateException e) {
                // failed
                map.put("Result", new Boolean(false));
                map.put("Message", "Was unable to fire event on a NullActivity that had its endActivity() call rolledback.");
                map.put("ID", new Integer(4247));
                aci.detach(getSbbContext().getSbbLocalObject());
                TCKSbbUtils.getResourceInterface().sendSbbMessage(map);
                return;
            }

            map.put("Result", new Boolean(true));
            map.put("Message", "Ok");
            aci.detach(getSbbContext().getSbbLocalObject());            
            TCKSbbUtils.getResourceInterface().sendSbbMessage(map);
            //passed
        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }
    }

    public void onTest4244ThirdEvent(Test4244ThirdEvent event, ActivityContextInterface aci) {
    }

    public abstract void fireTest4244Event(Test4244Event event, ActivityContextInterface aci, Address address);
    public abstract void fireTest4244SecondEvent(Test4244SecondEvent event, ActivityContextInterface aci, Address address);
    public abstract void fireTest4244ThirdEvent(Test4244ThirdEvent event, ActivityContextInterface aci, Address address);

}
