/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.profiles.profilecmp;

import java.util.HashMap;

import javax.slee.CreateException;
import javax.slee.profile.Profile;
import javax.slee.profile.ProfileContext;
import javax.slee.profile.ProfileVerificationException;

import com.opencloud.logging.Logable;
import com.opencloud.logging.StdErrLog;
import com.opencloud.sleetck.lib.profileutils.BaseMessageSender;
import com.opencloud.sleetck.lib.rautils.RMIObjectChannel;
import com.opencloud.sleetck.lib.rautils.TCKRAUtils;

/**
 * Tests that dependent value classes can be set as CMP fields and that get/set accessors
 * return copies of dependent value class objects.
 */
public abstract class Test1110629Profile implements Test1110629ProfileCMP, Profile  {
    public static final String PROFILE_TABLE_NAME = "Test1110629ProfileTable";

    public Test1110629Profile() {
        Logable log = new StdErrLog();

        try {
            out = TCKRAUtils.lookupRMIObjectChannel();
            msgSender = new BaseMessageSender(out, log);
        }
        catch (Exception e) {
            log.error("An error occured creating an RMIObjectChannel:");
            log.error(e);
        }
    }

    public void setProfileContext(ProfileContext context) {
        context.getTracer("setProfileContext").info("setProfileContext called");

        this.context = context;
    }

    public void unsetProfileContext() {
        context.getTracer("unsetProfileContext").info("unsetProfileContext called");

        context = null;
    }

    public void profileInitialize() {
        context.getTracer("profileInitialize").info("profileInitialize called");
    }

    public void profilePostCreate() throws CreateException {
        context.getTracer("profilePostCreate").info("profilePostCreate called");

        //1110631: The assignment of a dependent value class value to a CMP field using the set accessor
        //method causes the value to be copied to the target CMP field.
        HashMap oriMap = new HashMap();
        this.setMap(oriMap);
        oriMap.put("Key", "Value");
        HashMap checkMap = this.getMap();

        if (checkMap==oriMap || checkMap.containsKey("Key")) {
            msgSender.sendFailure(1110631, "CMP field set accessor method did not store a COPY of the provided object but rather a reference.");
            return;
        }
        else
            msgSender.sendLogMsg("CMP field set accessor correctly stored a copy of the provided object as the CMP field's value.");


        //1110630: The get accessor method for a CMP field that corresponds to a dependent value class
        //returns a copy of the dependent value class instance.
        oriMap = this.getMap();
        oriMap.put("Key2", "Value2");
        checkMap = this.getMap();
        if (oriMap == checkMap || checkMap.containsKey("Key2")) {
            msgSender.sendFailure(1110630, "CMP field get accessor method did not return a COPY of the stored CMP object but rather a reference.");
            return;
        }
        else
            msgSender.sendLogMsg("CMP field get accessor correctly returned a copy of the stored object as the CMP field's value.");

        msgSender.sendSuccess(1110629, "Test successful.");

    }

    public void profileActivate() {
        context.getTracer("profileActivate").info("profileActivate called");
    }

    public void profilePassivate() {
        context.getTracer("profilePassivate").info("profilePassivate called");
    }

    public void profileLoad() {
        context.getTracer("profileLoad").info("profileLoad called");
    }

    public void profileStore() {
        context.getTracer("profileStore").info("profileStore called");
    }

    public void profileRemove() {
        context.getTracer("profileRemove").info("profileRemove called");
    }

    public void profileVerify() throws ProfileVerificationException {
        context.getTracer("profileVerify").info("profileVerify called");
    }

    private ProfileContext context;

    private RMIObjectChannel out;
    private BaseMessageSender msgSender;
}
