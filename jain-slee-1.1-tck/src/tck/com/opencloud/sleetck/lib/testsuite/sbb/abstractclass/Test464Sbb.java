/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.sbb.abstractclass;

import javax.slee.ActivityEndEvent;
import javax.slee.ActivityContextInterface;
import javax.slee.SbbContext;
import javax.slee.Address;
import javax.slee.SbbLocalObject;
import javax.slee.facilities.Level;
import javax.slee.ChildRelation;
import javax.slee.CreateException;

import com.opencloud.sleetck.lib.sbbutils.BaseTCKSbb;
import com.opencloud.sleetck.lib.sbbutils.TCKSbbUtils;
import com.opencloud.sleetck.lib.sbbutils.TCKSbbConstants;
import com.opencloud.sleetck.lib.resource.events.TCKResourceEventX;
import com.opencloud.sleetck.lib.resource.TCKActivityID;
import com.opencloud.sleetck.lib.resource.sbbapi.TCKActivity;
import com.opencloud.sleetck.lib.resource.sbbapi.TCKResourceSbbInterface;
import com.opencloud.sleetck.lib.resource.sbbapi.TCKActivityContextInterfaceFactory;

import java.util.HashMap;
import java.util.Iterator;

public abstract class Test464Sbb extends BaseTCKSbb {


    private static final String MESSAGE = "Test464RuntimeException";

    public void sbbExceptionThrown(Exception exception, Object event, ActivityContextInterface aci) {
        if (exception instanceof RuntimeException && exception.getMessage().equals(MESSAGE)) {
            try {
                HashMap map = new HashMap();

                Object currentTransaction = TCKSbbUtils.getResourceAdaptorInterface().getTransactionIDAccess().getCurrentTransactionID();
                if (currentTransaction != null) {
                    map.put("Result", new Boolean(true));
                    map.put("Message", "Ok");
                } else {
                    map.put("Result", new Boolean(false));
                    map.put("Message", "sbbCreate() was not executed with a valid transaction ID");
                }

                TCKSbbUtils.getResourceInterface().sendSbbMessage(map);
            } catch (Exception e) {
                TCKSbbUtils.handleException(e);
            }
            return;
        }

        super.sbbExceptionThrown(exception, event, aci);
    }

    public void sbbRolledBack(javax.slee.RolledBackContext context) {
    }

    // Initial event
    public void onTCKResourceEventX1(TCKResourceEventX ev, ActivityContextInterface aci) {
        throw new RuntimeException(MESSAGE);
    }

}
