/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.usage.common;

/**
 * Generic Usage Parameter interface for usage tests.
 *
 * The arbitrary parameter names are taken from the example in the
 * SLEE specification.
 */
public interface GenericUsageParameterInterface {

    public void incrementFirstCount(long value);

    public void incrementSecondCount(long value);

    public void sampleTimeBetweenNewConnections(long value);

    public void sampleTimeBetweenErrors(long value);

}
