/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.javax.slee.AddressPlan;

import com.opencloud.sleetck.lib.SleeTCKTest;
import com.opencloud.sleetck.lib.SleeTCKTestUtils;
import com.opencloud.sleetck.lib.TCKTestResult;

import javax.slee.AddressPlan;

public class Test3356Test implements SleeTCKTest {

    public void init(SleeTCKTestUtils utils) {}

    /**
     * Perform the actual test.
     */

    public TCKTestResult run() throws Exception {

        AddressPlan addressPlan = AddressPlan.IP;

        try {
            AddressPlan.fromInt(AddressPlan.ADDRESS_PLAN_IP);
        } catch (Exception e) {
            return TCKTestResult.failed(3356, "AddressPlan.fromInt(AddressPlan.ADDRESS_PLAN_IP) threw an exception.");
        }
        
        try {
            if (addressPlan.toInt() != AddressPlan.ADDRESS_PLAN_IP)
                return TCKTestResult.failed(3360, "AddressPlan.toInt() returned invalid value.");
        } catch (Exception e) {
            return TCKTestResult.failed(3360, "AddressPlan.toInt() threw an exception.");
        }

        try {
            if (addressPlan.isNotPresent() == true)
                return TCKTestResult.failed(3363, "AddressPlan.isNotPresent() returned true for address plan of different type.");

            if (AddressPlan.fromInt(AddressPlan.ADDRESS_PLAN_NOT_PRESENT).isNotPresent() == false)
                return TCKTestResult.failed(3363, "AddressPlan.isNotPresent() returned false for address plan of that type.");
        } catch (Exception e) {
            return TCKTestResult.failed(3361, "AddressPlan.isNotPresent() threw an exception.");
        }

        try {
            if (addressPlan.isUndefined() == true)
                return TCKTestResult.failed(3365, "AddressPlan.isUndefined() returned true for address plan of different type.");

            if (AddressPlan.fromInt(AddressPlan.ADDRESS_PLAN_UNDEFINED).isUndefined() == false)
                return TCKTestResult.failed(3365, "AddressPlan.isUndefined() returned false for address plan of that type.");
        } catch (Exception e) {
            return TCKTestResult.failed(3364, "AddressPlan.isUndefined() threw an exception.");
        }

        try {
            if (AddressPlan.fromInt(AddressPlan.ADDRESS_PLAN_E164).isIP() == true)
                return TCKTestResult.failed(3367, "AddressPlan.isIP() returned true for address plan of different type.");

            if (AddressPlan.fromInt(AddressPlan.ADDRESS_PLAN_IP).isIP() == false)
                return TCKTestResult.failed(3367, "AddressPlan.isIP() returned false for address plan of that type.");
        } catch (Exception e) {
            return TCKTestResult.failed(3366, "AddressPlan.isIP() threw an exception.");
        }

        try {
            if (addressPlan.isMulticast() == true)
                return TCKTestResult.failed(3369, "AddressPlan.isMulticast() returned true for address plan of different type.");

            if (AddressPlan.fromInt(AddressPlan.ADDRESS_PLAN_MULTICAST).isMulticast() == false)
                return TCKTestResult.failed(3369, "AddressPlan.isMulticast() returned false for address plan of that type.");
        } catch (Exception e) {
            return TCKTestResult.failed(3368, "AddressPlan.isMulticast() threw an exception.");
        }

        try {
            if (addressPlan.isUnicast() == true)
                return TCKTestResult.failed(3371, "AddressPlan.isUnicast() returned true for address plan of different type.");

            if (AddressPlan.fromInt(AddressPlan.ADDRESS_PLAN_UNICAST).isUnicast() == false)
                return TCKTestResult.failed(3371, "AddressPlan.isUnicast() returned false for address plan of that type.");
        } catch (Exception e) {
            return TCKTestResult.failed(3370, "AddressPlan.isUnicast() threw an exception.");
        }

        try {
            if (addressPlan.isE164() == true)
                return TCKTestResult.failed(3373, "AddressPlan.isE164() returned true for address plan of different type.");

            if (AddressPlan.fromInt(AddressPlan.ADDRESS_PLAN_E164).isE164() == false)
                return TCKTestResult.failed(3373, "AddressPlan.isE164() returned false for address plan of that type.");
        } catch (Exception e) {
            return TCKTestResult.failed(3372, "AddressPlan.isE164() threw an exception.");
        }

        try {
            if (addressPlan.isAESA() == true)
                return TCKTestResult.failed(3375, "AddressPlan.isAESA() returned true for address plan of different type.");

            if (AddressPlan.fromInt(AddressPlan.ADDRESS_PLAN_AESA).isAESA() == false)
                return TCKTestResult.failed(3375, "AddressPlan.isAESA() returned false for address plan of that type.");
        } catch (Exception e) {
            return TCKTestResult.failed(3374, "AddressPlan.isAESA() threw an exception.");
        }

        try {
            if (addressPlan.isURI() == true)
                return TCKTestResult.failed(3377, "AddressPlan.isURI() returned true for address plan of different type.");

            if (AddressPlan.fromInt(AddressPlan.ADDRESS_PLAN_URI).isURI() == false)
                return TCKTestResult.failed(3377, "AddressPlan.isURI() returned false for address plan of that type.");
        } catch (Exception e) {
            return TCKTestResult.failed(3376, "AddressPlan.isURI() threw an exception.");
        }

        try {
            if (addressPlan.isNSAP() == true)
                return TCKTestResult.failed(3379, "AddressPlan.isNSAP() returned true for address plan of different type.");

            if (AddressPlan.fromInt(AddressPlan.ADDRESS_PLAN_NSAP).isNSAP() == false)
                return TCKTestResult.failed(3379, "AddressPlan.isNSAP() returned false for address plan of that type.");
        } catch (Exception e) {
            return TCKTestResult.failed(3378, "AddressPlan.isNSAP() threw an exception.");
        }

        try {
            if (addressPlan.isSMTP() == true)
                return TCKTestResult.failed(3381, "AddressPlan.isSMTP() returned true for address plan of different type.");

            if (AddressPlan.fromInt(AddressPlan.ADDRESS_PLAN_SMTP).isSMTP() == false)
                return TCKTestResult.failed(3381, "AddressPlan.isSMTP() returned false for address plan of that type.");
        } catch (Exception e) {
            return TCKTestResult.failed(3380, "AddressPlan.isSMTP() threw an exception.");
        }

        try {
            if (addressPlan.isX400() == true)
                return TCKTestResult.failed(3383, "AddressPlan.isX400() returned true for address plan of different type.");

            if (AddressPlan.fromInt(AddressPlan.ADDRESS_PLAN_X400).isX400() == false)
                return TCKTestResult.failed(3383, "AddressPlan.isX400() returned false for address plan of that type.");
        } catch (Exception e) {
            return TCKTestResult.failed(3382, "AddressPlan.isX400() threw an exception.");
        }

        try {
            if (addressPlan.isSIP() == true)
                return TCKTestResult.failed(3385, "AddressPlan.isSIP() returned true for address plan of different type.");

            if (AddressPlan.fromInt(AddressPlan.ADDRESS_PLAN_SIP).isSIP() == false)
                return TCKTestResult.failed(3385, "AddressPlan.isSIP() returned false for address plan of that type.");
        } catch (Exception e) {
            return TCKTestResult.failed(3384, "AddressPlan.isSIP() threw an exception.");
        }

        try {
            if (addressPlan.isE164Mobile() == true)
                return TCKTestResult.failed(3387, "AddressPlan.isE164Mobile() returned true for address plan of different type.");

            if (AddressPlan.fromInt(AddressPlan.ADDRESS_PLAN_E164_MOBILE).isE164Mobile() == false)
                return TCKTestResult.failed(3387, "AddressPlan.isE164Mobile() returned false for address plan of that type.");
        } catch (Exception e) {
            return TCKTestResult.failed(3386, "AddressPlan.isE164Mobile() threw an exception.");
        }

        try {
            if (addressPlan.isH323() == true)
                return TCKTestResult.failed(3389, "AddressPlan.isH323() returned true for address plan of different type.");

            if (AddressPlan.fromInt(AddressPlan.ADDRESS_PLAN_H323).isH323() == false)
                return TCKTestResult.failed(3389, "AddressPlan.isH323() returned false for address plan of that type.");
        } catch (Exception e) {
            return TCKTestResult.failed(3388, "AddressPlan.isH323() threw an exception.");
        }

        try {
            if (addressPlan.isGT() == true)
                return TCKTestResult.failed(3391, "AddressPlan.isGT() returned true for address plan of different type.");

            if (AddressPlan.fromInt(AddressPlan.ADDRESS_PLAN_GT).isGT() == false)
                return TCKTestResult.failed(3391, "AddressPlan.isGT() returned false for address plan of that type.");
        } catch (Exception e) {
            return TCKTestResult.failed(3390, "AddressPlan.isGT() threw an exception.");
        }

        try {
            if (addressPlan.isSSN() == true)
                return TCKTestResult.failed(3393, "AddressPlan.isSSN() returned true for address plan of different type.");

            if (AddressPlan.fromInt(AddressPlan.ADDRESS_PLAN_SSN).isSSN() == false)
                return TCKTestResult.failed(3393, "AddressPlan.isSSN() returned false for address plan of that type.");
        } catch (Exception e) {
            return TCKTestResult.failed(3392, "AddressPlan.isSSN() threw an exception.");
        }

        try {
            if (addressPlan.isSleeProfileTable() == true)
                return TCKTestResult.failed(3395, "AddressPlan.isSleeProfileTable() returned true for address plan of different type.");

            if (AddressPlan.fromInt(AddressPlan.ADDRESS_PLAN_SLEE_PROFILE_TABLE).isSleeProfileTable() == false)
                return TCKTestResult.failed(3395, "AddressPlan.isSleeProfileTable() returned false for address plan of that type.");
        } catch (Exception e) {
            return TCKTestResult.failed(3394, "AddressPlan.isSleeProfileTable() threw an exception.");
        }

        try {
            if (addressPlan.isSleeProfile() == true)
                return TCKTestResult.failed(3397, "AddressPlan.isSleeProfile() returned true for address plan of different type.");

            if (AddressPlan.fromInt(AddressPlan.ADDRESS_PLAN_SLEE_PROFILE).isSleeProfile() == false)
                return TCKTestResult.failed(3397, "AddressPlan.isSleeProfile() returned false for address plan of that type.");
        } catch (Exception e) {
            return TCKTestResult.failed(3396, "AddressPlan.isSleeProfile() threw an exception.");
        }

        try {
            if (!addressPlan.equals(addressPlan))
                return TCKTestResult.failed(3399, "AddressPlan.equals(AddressPlan) returned false for identical AddressPlan objects.");
            if (addressPlan.equals(AddressPlan.fromInt(AddressPlan.ADDRESS_PLAN_SLEE_PROFILE)))
                return TCKTestResult.failed(3399, "AddressPlain.equals(AddressPlan) returned true for non-identical AddressPlan objects.");
        } catch (Exception e) {
            return TCKTestResult.failed(3398, "AddressPlan.equals(AddressPlan) threw an exception.");
        }

        try {
            addressPlan.hashCode();
        } catch (Exception e) {
            return TCKTestResult.failed(3401, "AddressPlan.hashCode() threw an exception.");
        }

        try {
            addressPlan.toString();
        } catch (Exception e) {
            return TCKTestResult.failed(3403, "AddressPlan.toString() threw an exception.");
        }


        return TCKTestResult.passed();
    }


    /**
     * Do all the pre-run configuration of the test.
     */

    public void setUp() throws Exception {
    }

    /**
     * Clean up after the test.
     */

    public void tearDown() throws Exception {
    }

}
