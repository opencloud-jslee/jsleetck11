/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.facilities.alarmfacility;

import java.util.HashMap;

import javax.naming.InitialContext;
import javax.slee.ActivityContextInterface;
import javax.slee.facilities.AlarmFacility;
import javax.slee.facilities.AlarmLevel;
import javax.slee.facilities.Tracer;

import com.opencloud.sleetck.lib.resource.events.TCKResourceEventX;
import com.opencloud.sleetck.lib.sbbutils.BaseTCKSbb;
import com.opencloud.sleetck.lib.sbbutils.TCKSbbUtils;

/*  
 * AssertionID(1113504): Test If no alarms of this type are 
 * active this method returns silently.
 * 
 * AssertionID(1113505): Test The return value of this method 
 * is the number of alarms that were cleared.
 *  
 */
public abstract class Test1113504Sbb extends BaseTCKSbb {

    private static final String JNDI_ALARMFACILITY_NAME = AlarmFacility.JNDI_NAME;

    public static final String ALARM_MESSAGE = "Test1113504AlarmMessage";

    public static final String ALARM_TYPE = "javax.slee.management.Alarm";

    // Initial event
    public void onTCKResourceEventX1(TCKResourceEventX event, ActivityContextInterface aci) {

        try {
            tracer = this.getSbbContext().getTracer("com.test");
            tracer.info("Received " + event + " message", null);

            AlarmFacility facility = (AlarmFacility) new InitialContext().lookup(JNDI_ALARMFACILITY_NAME);
            //raise an alarm
            String[] alarmIDs = {
                    facility.raiseAlarm(ALARM_TYPE, "Test1113504AlarmInstanceID1", AlarmLevel.CRITICAL, ALARM_MESSAGE),
                    facility.raiseAlarm(ALARM_TYPE, "Test1113504AlarmInstanceID2", AlarmLevel.MAJOR, ALARM_MESSAGE),
                    facility.raiseAlarm(ALARM_TYPE, "Test1113504AlarmInstanceID3", AlarmLevel.WARNING, ALARM_MESSAGE),
                    facility.raiseAlarm(ALARM_TYPE, "Test1113504AlarmInstanceID4", AlarmLevel.INDETERMINATE,
                            ALARM_MESSAGE),
                    facility.raiseAlarm(ALARM_TYPE, "Test1113504AlarmInstanceID5", AlarmLevel.MINOR, ALARM_MESSAGE) };

            //clear the alarm
            if (facility.clearAlarms(ALARM_TYPE) != alarmIDs.length) {
                sendResultToTCK("Test1113504Test", false, 1113505, "The return value of this facility.clearAlarms(alarmType) method "
                        + "didn't match the number of alarms that were cleared!");
                return;
            }

            try {
                //try to clear the alarm again, should fail because of if there are no
                // alarms of this type are active
                if (facility.clearAlarms(ALARM_TYPE) >= 1) {
                    sendResultToTCK("Test1113504Test", false, 1113504, "There should be no alarms of this type are active, but "
                            + "facility.clearAlarms(alarmType) returned a number of active alarms which"
                            + "need to be cleared.");
                    return;
                }
            }
            catch (Exception e) {
                tracer.severe("ERROR: Exception " + e + " caught calling AlarmFacility.clearAlarms() method.");
            }

            //try to clearAlarm(Unknown_Alarm_Type), should return silently.
            //we assumed that there are no alarms of this type(javax.slee.management.UnknownAlarm)
            String unknownedAlarmType = "javax.slee.management.UnknownAlarm";
            if (facility.clearAlarms(unknownedAlarmType) >= 1) {
                sendResultToTCK("Test1113504Test", false, 1113504, "There should be no alarms of this type needs to be cleared!");
                return;
            }

            sendResultToTCK("Test1113504Test", true, 1113504,"AlarmFacility.clearAlarms(alarmType) method tests passed!");

        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }

    }

    private void sendResultToTCK(String testName, boolean result, int failedAssertionID,  String message) throws Exception {
        HashMap sbbData = new HashMap();
        sbbData.put("testname", testName);
        sbbData.put("result", result ? "pass" : "fail");
        sbbData.put("message", message);
        sbbData.put("id", new Integer(failedAssertionID));
        TCKSbbUtils.getResourceInterface().sendSbbMessage(sbbData);
    }

    private Tracer tracer;
}
