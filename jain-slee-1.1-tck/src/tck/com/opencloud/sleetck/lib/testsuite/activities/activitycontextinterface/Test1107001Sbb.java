/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.activities.activitycontextinterface;

import java.util.HashMap;

import javax.slee.ActivityContextInterface;

import com.opencloud.sleetck.lib.TCKTestFailureException;
import com.opencloud.sleetck.lib.resource.TCKActivityID;
import com.opencloud.sleetck.lib.resource.events.TCKResourceEventX;
import com.opencloud.sleetck.lib.resource.sbbapi.TCKActivityContextInterfaceFactory;
import com.opencloud.sleetck.lib.resource.sbbapi.TCKResourceSbbInterface;
import com.opencloud.sleetck.lib.sbbutils.BaseTCKSbb;
import com.opencloud.sleetck.lib.sbbutils.TCKSbbConstants;
import com.opencloud.sleetck.lib.sbbutils.TCKSbbUtils;
import com.opencloud.sleetck.lib.testsuite.activities.activitycontext.Test579SbbActivityContextInterface;

public abstract class Test1107001Sbb extends BaseTCKSbb {

    private boolean passed = true;
    private int failedAssertion = 1107004;
    private String failureReason = "SBB not run";

    public void onTCKResourceEventX1(TCKResourceEventX event, ActivityContextInterface aci) {
        try {

            // Create a new Activity.
            TCKActivityID activityID = TCKSbbUtils.getResourceInterface().createActivity("Test1107001SecondActivity");
            TCKResourceSbbInterface resource = TCKSbbUtils.getResourceInterface();
            TCKActivityContextInterfaceFactory factory = (TCKActivityContextInterfaceFactory) TCKSbbUtils.getSbbEnvironment().lookup(TCKSbbConstants.TCK_ACI_FACTORY_LOCATION);
            ActivityContextInterface myAci = factory.getActivityContextInterface(resource.getActivity(activityID));
            ActivityContextInterface myAci2 = factory.getActivityContextInterface(resource.getActivity(activityID));

            // Attach the SBB to the Activity Context.
            myAci.attach(getSbbContext().getSbbLocalObject());

            // Check that it is attached
            try {
                boolean isAttached = myAci.isAttached(getSbbContext().getSbbLocalObject());
                if (!isAttached) {
                    passed = false;
                    failedAssertion = 1107004;
                }
            } catch (Exception e) {
                reportTCKException(1107004,"myAci.isAttached() raised exception");
            }

            if (passed) {
                // Check that null SBB throws java.lang.NullPointerException
                try {
                    boolean isAttached = myAci.isAttached(null);
                    passed = false;
                    failedAssertion = 1107003;
                    failureReason = "ACI is not attached";
                } catch (NullPointerException npe) {
                    passed = true;
                } catch (Exception e) {
                    reportTCKException(1107003,"myAci.isAttached() raised exception");
                }
            }

            if (passed) {
                // Check that aci's are equal
                try {
                    boolean areEqual = myAci.equals(myAci2);
                    if (!areEqual){
                        passed = false;
                        failedAssertion = 1107010;
                        failureReason = "ACI's are not equal";
                    }
                } catch (Exception e) {
                    reportTCKException(1107010,"myAci.equals() raised exception");
                }
            }

            if (passed) {
                // Check that aci's hash codes are equal
                try {
                    int hashCode1 = myAci.hashCode();
                    int hashCode2 = myAci2.hashCode();
                    if (hashCode1 != hashCode2){
                        passed = false;
                        failedAssertion = 1107011;
                        failureReason = "ACI hashCodes are not equal";
                    }
                } catch (Exception e) {
                    reportTCKException(1107011,"myAci.hashCode() raised exception");
                }
            }

            // Detach the SBB from the Activity Context.
            myAci.detach(getSbbContext().getSbbLocalObject());

            // Check that it is no longer attached
            try {
                boolean isAttached = myAci.isAttached(getSbbContext().getSbbLocalObject());
                if (isAttached) {
                    passed = false;
                    failedAssertion = 1107004;
                    failureReason = "ACI is still attached";
                }
            } catch (Exception e) {
                reportTCKException(1107004,"myAci.isAttached() raised exception");
            }

            sendSBBResulttoTest();

        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }

    }

    private void sendSBBResulttoTest() {
        try {
            // Send the result back to the test
            HashMap map = new HashMap();
            if (passed) {
                map.put("Result", new Boolean(true));
                map.put("Message", "Ok");
            } else {
                map.put("Result", new Boolean(false));
                map.put("ID", new Integer(failedAssertion));
                map.put("Message", failureReason);
            }

            TCKSbbUtils.getResourceInterface().sendSbbMessage(map);
        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }
    }

    private void reportTCKException(int assertionId, String message) {
        TCKSbbUtils.handleException(new TCKTestFailureException(assertionId, message));
    }


    public abstract Test1107001SbbActivityContextInterface asSbbActivityContextInterface(ActivityContextInterface aci);

}
