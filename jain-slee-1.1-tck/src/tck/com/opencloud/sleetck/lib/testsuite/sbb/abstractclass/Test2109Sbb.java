/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.sbb.abstractclass;

import javax.slee.ActivityEndEvent;
import javax.slee.ActivityContextInterface;
import javax.slee.SbbContext;
import javax.slee.Address;
import javax.slee.SbbLocalObject;
import javax.slee.facilities.Level;
import javax.slee.ChildRelation;
import javax.slee.CreateException;

import com.opencloud.sleetck.lib.sbbutils.BaseTCKSbb;
import com.opencloud.sleetck.lib.sbbutils.TCKSbbUtils;
import com.opencloud.sleetck.lib.sbbutils.TCKSbbConstants;
import com.opencloud.sleetck.lib.resource.events.TCKResourceEventX;
import com.opencloud.sleetck.lib.resource.sbbapi.TCKResourceSbbInterface;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Collection;

public abstract class Test2109Sbb extends BaseTCKSbb {

    // Initial event
    public void onTCKResourceEventX1(TCKResourceEventX ev, ActivityContextInterface aci) {

        try {

            Collection children = getChildRelation();
            if (children.size() != 0) {
                HashMap map = new HashMap();
                map.put("Message", "SBB initially had some children, it shouldn't have had any.");
                map.put("Result", new Boolean(false));
                TCKSbbUtils.getResourceInterface().sendSbbMessage(map);
                return;
            }

            getChildRelation().create();

            children = getChildRelation();
            if (children.size() != 1) {
                HashMap map = new HashMap();
                map.put("Message", "SBB should now have 1 child.");
                map.put("Result", new Boolean(false));
                TCKSbbUtils.getResourceInterface().sendSbbMessage(map);
                return;
            }

            HashMap map = new HashMap();
            map.put("Message", "Ok");
            map.put("Result", new Boolean(true));
            TCKSbbUtils.getResourceInterface().sendSbbMessage(map);
            return;

        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }
    }

    // ChildRelation method for the Child SBB.
    public abstract ChildRelation getChildRelation();

}
