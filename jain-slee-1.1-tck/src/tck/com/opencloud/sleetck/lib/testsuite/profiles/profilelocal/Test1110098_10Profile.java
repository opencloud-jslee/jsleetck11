/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.profiles.profilelocal;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.SQLException;

import javax.slee.CreateException;
import javax.slee.profile.Profile;
import javax.slee.profile.ProfileContext;
import javax.slee.profile.ProfileVerificationException;


public abstract class Test1110098_10Profile implements Profile,  ProfileLocalTestsProfileCMP {
    public void setProfileContext(ProfileContext context) {}

    public void unsetProfileContext() {}

    public void profileInitialize() {}

    public void profilePostCreate() throws CreateException {}

    public void profileActivate() {}

    public void profilePassivate() {}

    public void profileLoad() {}

    public void profileStore() {}

    public void profileRemove() {}

    public void profileVerify() throws ProfileVerificationException {}

    public String getValue(int dummy) {
        return null;
    }

    public void setValue(String value, int dummy) {}

    public void business(String exToThrow) throws FileNotFoundException, IOException, SQLException {

        if (exToThrow.equals(FileNotFoundException.class.getName())) {
            throw new FileNotFoundException("Testexception.");
        }
        else if (exToThrow.equals(IOException.class.getName())) {
            throw new IOException("Testexception.");
        }
        else if (exToThrow.equals(SQLException.class.getName())) {
            throw new SQLException("Testexception.");
        }

    }

}
