/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.javax.slee.facilities.Level;

import com.opencloud.sleetck.lib.SleeTCKTest;
import com.opencloud.sleetck.lib.SleeTCKTestUtils;
import com.opencloud.sleetck.lib.TCKTestResult;

import javax.slee.facilities.Level;

public class Test3629Test implements SleeTCKTest {

    public void init(SleeTCKTestUtils utils) {}

    /**
     * Perform the actual test.
     */

    public TCKTestResult run() throws Exception {

        if (!Level.fromInt(Level.LEVEL_FINE).isFine())
            return TCKTestResult.failed(3629, "Level.fromInt(Level.LEVEL_FINE) returned incorrect Level object.");

        if (Level.fromInt(Level.LEVEL_FINE).toInt() != Level.LEVEL_FINE)
            return TCKTestResult.failed(3632, "Level.toInt() returned incorrect value.");



        if (Level.fromInt(Level.LEVEL_FINE).isOff())
            return TCKTestResult.failed(3634, "Level.isOff(Level.LEVEL_FINE) returned true.");

        if (!Level.fromInt(Level.LEVEL_OFF).isOff())
            return TCKTestResult.failed(3634, "Level.isOff(Level.LEVEL_OFF) returned false.");


        if (Level.fromInt(Level.LEVEL_FINE).isSevere())
            return TCKTestResult.failed(3636, "Level.isSevere(Level.LEVEL_FINE) returned true.");

        if (!Level.fromInt(Level.LEVEL_SEVERE).isSevere())
            return TCKTestResult.failed(3636, "Level.isSevere(Level.LEVEL_SEVERE) returned false.");


        if (Level.fromInt(Level.LEVEL_FINE).isWarning())
            return TCKTestResult.failed(3638, "Level.isWarning(Level.LEVEL_FINE) returned true.");

        if (!Level.fromInt(Level.LEVEL_WARNING).isWarning())
            return TCKTestResult.failed(3638, "Level.isWarning(Level.LEVEL_WARNING) returned false.");


        if (Level.fromInt(Level.LEVEL_FINE).isMinor())
            return TCKTestResult.failed(3640, "Level.isMinor(Level.LEVEL_FINE) returned true.");

        if (!Level.fromInt(Level.LEVEL_INFO).isMinor())
            return TCKTestResult.failed(3640, "Level.isMinor(Level.LEVEL_INFO) returned false.");


        if (Level.fromInt(Level.LEVEL_FINE).isConfig())
            return TCKTestResult.failed(3642, "Level.isConfig(Level.LEVEL_FINE) returned true.");

        if (!Level.fromInt(Level.LEVEL_CONFIG).isConfig())
            return TCKTestResult.failed(3642, "Level.isConfig(Level.LEVEL_CONFIG) returned false.");


        if (Level.fromInt(Level.LEVEL_SEVERE).isFine())
            return TCKTestResult.failed(3644, "Level.isFine(Level.LEVEL_SEVERE) returned true.");

        if (!Level.fromInt(Level.LEVEL_FINE).isFine())
            return TCKTestResult.failed(3644, "Level.isFine(Level.LEVEL_FINE) returned false.");


        if (Level.fromInt(Level.LEVEL_FINE).isFiner())
            return TCKTestResult.failed(3646, "Level.isFiner(Level.LEVEL_FINE) returned true.");

        if (!Level.fromInt(Level.LEVEL_FINER).isFiner())
            return TCKTestResult.failed(3646, "Level.isFiner(Level.LEVEL_FINER) returned false.");


        if (Level.fromInt(Level.LEVEL_FINE).isFinest())
            return TCKTestResult.failed(3648, "Level.isFinest(Level.LEVEL_FINE) returned true.");

        if (!Level.fromInt(Level.LEVEL_FINEST).isFinest())
            return TCKTestResult.failed(3648, "Level.isFinest(Level.LEVEL_FINEST) returned false.");


        if (!Level.fromInt(Level.LEVEL_FINE).isHigherLevel(Level.fromInt(Level.LEVEL_FINEST)))
            return TCKTestResult.failed(3650, "Level.isHigherLevel(Level) returned false when it should have returned true.");

        if (Level.fromInt(Level.LEVEL_FINEST).isHigherLevel(Level.fromInt(Level.LEVEL_FINE)))
            return TCKTestResult.failed(3650, "Level.isHigherLevel(Level) returned true when it should have returned false.");


        if (Level.fromInt(Level.LEVEL_FINEST).equals(Level.fromInt(Level.LEVEL_FINE)))
            return TCKTestResult.failed(3653, "Level.equals(Level) returned true when it should have returned false.");

        if (!Level.fromInt(Level.LEVEL_FINE).equals(Level.fromInt(Level.LEVEL_FINE)))
            return TCKTestResult.failed(3653, "Level.equals(Level) returned false when it should have returned true.");

        try {
            Level.fromInt(Level.LEVEL_FINE).hashCode();
        } catch (Exception e) {
            return TCKTestResult.failed(3655, "Level.hashCode() threw an exception.");
        }

        if (Level.fromInt(Level.LEVEL_FINE).toString() == null)
            return TCKTestResult.failed(3657, "Level.toString() returned null.");

        return TCKTestResult.passed();
    }


    /**
     * Do all the pre-run configuration of the test.
     */

    public void setUp() throws Exception {
    }

    /**
     * Clean up after the test.
     */

    public void tearDown() throws Exception {
    }
}
