/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.management.ManagementMBean;

import com.opencloud.sleetck.lib.AbstractSleeTCKTest;
import com.opencloud.sleetck.lib.TCKTestResult;
import com.opencloud.sleetck.lib.testutils.jmx.DeploymentMBeanProxy;

import javax.slee.ComponentID;
import javax.slee.management.DeployableUnitDescriptor;
import javax.slee.management.DeployableUnitID;

public class Test1609Test extends AbstractSleeTCKTest {

    private static final String SERVICE_DU_PATH_PARAM = "DUPath";
    private static final int TEST_ID = 1609;

    /**
     * Perform the actual test.
     */
    public TCKTestResult run() throws Exception {
        DeploymentMBeanProxy duProxy = utils().getDeploymentMBeanProxy();
        DeployableUnitDescriptor duDesc = duProxy.getDescriptor(duID);
        ComponentID components[] = duDesc.getComponents();
        if (components.length == 0) {
            return TCKTestResult.error("Installed deployable unit had no components");
        }
        getLog().info("Calling DeploymentMBean.isInstalled(ComponentID)");
        if (!duProxy.isInstalled(components[0])) {
            return TCKTestResult.failed(TEST_ID, "isInstalled() returned false for installed Component");
        }
        getLog().info("Uninstalling the Component");
        duProxy.uninstall(duID);
        getLog().info("Calling DeploymentMBean.isInstalled(ComponentID)");
        if (duProxy.isInstalled(components[0])) {
            return TCKTestResult.failed(TEST_ID, "isInstalled() returned true for uninstalled Component");
        }
        return TCKTestResult.passed();

    }

    /**
     * Do all the pre-run configuration of the test.
     */
    public void setUp() throws Exception {
        getLog().fine("Installing and activating service");
        // Install the Deployable Units
        String duPath = utils().getTestParams().getProperty(SERVICE_DU_PATH_PARAM);
        duID = utils().install(duPath);
    }

    private DeployableUnitID duID;
}
