/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.transactions.isolation;

import com.opencloud.sleetck.lib.AbstractSleeTCKTest;
import com.opencloud.sleetck.lib.OperationTimedOutException;
import com.opencloud.sleetck.lib.TCKTestErrorException;
import com.opencloud.sleetck.lib.TCKTestFailureException;
import com.opencloud.sleetck.lib.TCKTestResult;
import com.opencloud.sleetck.lib.resource.TCKActivityID;
import com.opencloud.sleetck.lib.resource.events.TCKResourceEventX;
import com.opencloud.sleetck.lib.resource.events.TCKResourceEventY;
import com.opencloud.sleetck.lib.resource.testapi.TCKResourceTestInterface;
import com.opencloud.sleetck.lib.testutils.BaseTCKResourceListener;

import java.rmi.RemoteException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;

/**
 * Test asserttion 2003, re transactional isolation of ActivityContextInterface fields.
 * This test checks that transactional changes made by an SBB to ACI fields
 * are not visible to other concurrently executing transactions.
 */
public class Test2003Test extends AbstractSleeTCKTest {

    // MAINTENANCE NOTES:
    // Threading model: the test is driven from the test thread - all events are fired
    // from the test thread.
    // State updates and accesses are sychronized on stateLock.
    // The event handlers only set flags and other state, block until flags are set, and
    // notify threads waiting on stateLock's monitor.

    private static final String SERVICE1_DU_PATH_PARAM = "service1DUPath";
    private static final String SERVICE2_DU_PATH_PARAM = "service2DUPath";

    private static final String X2_FIRST_ACK_RECEIVED = "X2_FIRST_ACK_RECEIVED";
    private static final String X2_SECOND_ACK_RECEIVED = "X2_SECOND_ACK_RECEIVED";

    private static final String EVENT_ACK_PREFIX = "ACK_RECEIVED_FROM_";
    private static final String ROLLED_BACK_FLAG_PREFIX = "ROLLED_BACK_FOR_";
    private static final String STOP_BLOCKING_PREFIX = "STOP_BLOCKING_FOR_";

    // -- SleeTCKTest methods --

    public void setUp() throws Exception {
        setResourceListener(new ResourceListenerImpl());
        setupService(SERVICE1_DU_PATH_PARAM, true);
        setupService(SERVICE2_DU_PATH_PARAM, true);

        stateLock = new Object();
        flags = new HashSet();
        eventObjectIDs = new HashMap();
        processedEvents = new HashSet();
        sbbOrResourceException = null;
        aciValueOrExceptionFromX2 = null;
        aciValueOrExceptionFromY2 = null;
    }

    public TCKTestResult run() throws Exception {
        TCKResourceTestInterface resource = utils().getResourceInterface();
        activityA = resource.createActivity("Test2003ActivityA");
        activityB = resource.createActivity("Test2003ActivityB");
        try {
            initialiseResources();
            testCase1();
            testCase2();
        } finally {
            freeResources();
        }
        return TCKTestResult.passed();
    }

    // -- Private methods --

    private void initialiseResources() throws Exception {
        getLog().fine("firing X1 on activity A");
        fireEvent(TCKResourceEventX.X1,activityA);
        getLog().fine("waiting for X1 processing to finish");
        waitForEventProcessing(TCKResourceEventX.X1);
        // check that the X1 ACK was received
        if(isRolledBack(TCKResourceEventX.X1)) new TCKTestErrorException("The X1 event handler rolled back. The initial value for "+
                "ACI will not have been committed, and the test cannot continue");
        if(!isACKReceived(TCKResourceEventX.X1)) throw new TCKTestErrorException(
                "Indication of X1 completing processing before the ACK was received from the event handler");
    }

    private void testCase1() throws Exception {
        getLog().info("Testing case 1: Access transactional state which was changed and committed in another "+
                "transaction after the current transaction performed a read on the resource.");

        getLog().fine("firing X2 on activity A");
        fireEvent(TCKResourceEventX.X2,activityA);
        getLog().fine("waiting for first X2 call (request to block)");
        waitForACK(TCKResourceEventX.X2);

        getLog().fine("firing Y1 on activity B while the X2 handler is blocked");
        fireEvent(TCKResourceEventY.Y1,activityB);
        getLog().fine("waiting for Y1 processing to finish");
        try {
            waitForEventProcessing(TCKResourceEventY.Y1);
        } catch (OperationTimedOutException toe) {
            getLog().warning("Timed out while waiting for Y1 to complete processing. This is acceptable, "+
                    "as the SLEE may be using single threaded event delivery, or pessimistic locking. Continuing...");
        }

        getLog().fine("unblocking the X2 handler");
        stopBlockingFor(TCKResourceEventX.X2);
        getLog().fine("waiting for X2 processing to finish");
        waitForEventProcessing(TCKResourceEventX.X2);

        diagnoseCase1Result();

        getLog().finer("waiting for Y1 to complete processing");
        try {
            waitForEventProcessing(TCKResourceEventY.Y1);
        } catch (OperationTimedOutException toe) {
            throw new TCKTestErrorException("Timed out while waiting for Y1 to complete processing, after "+
                    "X2 processing was completed. As the Y1 event handler may still have locks for transaction "+
                    "resources, the test cannot continue.");
        }

        // check than ACK or rolled back indication was received for Y1
        if(!isACKReceived(TCKResourceEventY.Y1) && !isRolledBack(TCKResourceEventY.Y1)) throw new TCKTestErrorException(
                "Indication of Y1 completing processing before an ACK or rolled back indication was received for the event handler");
    }

    private void diagnoseCase1Result() throws Exception {
        // diagnose the call received from X2 (if any)
        if(getFlag(X2_SECOND_ACK_RECEIVED)) {
            if(aciValueOrExceptionFromX2 instanceof Exception) {
                getLog().warning("The attempt to access the ACI or it's value in the X2 event handler resulted in an Exception "+
                        "being thrown. Allow this, as the SLEE may be using pessimistic locking on the ACI. Exception caught:");
                getLog().warning((Exception)aciValueOrExceptionFromX2);
                getLog().fine("Continuing...");
            } else {
                String aciValue = (String)aciValueOrExceptionFromX2;
                if(aciValue == null) {
                    if(isRolledBack(TCKResourceEventX.X2)) {
                        getLog().info("The ACI value retrieved by the X2 handler was null, but the transaction was "+
                                "rolled back. This is an accepted result.");
                    } else {
                        throw new TCKTestFailureException(2003,"The ACI value retrieved by the X2 handler "+
                            "was null, even though the ACI was initialised to a non-null value in an event handler for a "+
                            "TCKResourceEventX1 event which has completed processing. X2's transaction was not rolled back.");
                    }
                } else if(aciValue.equals(TCKResourceEventX.X1)) {
                    getLog().info("The ACI value retrieved by the X2 handler matched its initial value; the changes made "+
                            "by the Y1 event handler (in a concurrent transaction) were not visible. This is the "+
                            "expected result. Continuing...");
                } else if(aciValue.equals(TCKResourceEventY.Y1)) {
                    throw new TCKTestFailureException(2003,"The ACI value retrieved by the X2 handler was the value set "+
                            "by the Y1 event handler (in a concurrent transaction). This indicates that "+
                            "transaction isolation was broken: after transaction A performed a read on the resource, "+
                            "changes were made in transaction B, and those changes were visible from transaction A");
                }
            }
        } else if(isRolledBack(TCKResourceEventX.X2)) {
            getLog().info("The second ACK was not received from the X2 event handler, and the transaction was "+
                    "rolled back. This is an accepted result. Contining...");
        } else {
            throw new TCKTestErrorException(
                "Indication of X2 completing processing before a second ACK was received from the event handler, "+
                "or an sbbRolledBack() call");
        }
    }

    private void testCase2() throws Exception {
        getLog().info("Testing case 2: Access transactional state which was changed but has not yet been committed "+
                "in another transaction");

        getLog().fine("firing X3 on activity A");
        fireEvent(TCKResourceEventX.X3,activityA);
        getLog().fine("waiting for X3 call (request to block)");
        waitForACK(TCKResourceEventX.X3);

        getLog().fine("firing Y2 on activity B while blocking the X3 event handler");
        fireEvent(TCKResourceEventY.Y2,activityB);
        getLog().fine("waiting for Y2 processing to finish");

        try {
            waitForEventProcessing(TCKResourceEventY.Y2);
        } catch (OperationTimedOutException toe) {
            getLog().warning("Timed out while waiting for Y2 to complete processing. This is acceptable, "+
                    "as the SLEE may be using single threaded event delivery, or pessimistic locking. Continuing...");
        }

        diagnoseCase2Result();

        getLog().fine("unblocking the X3 handler");
        stopBlockingFor(TCKResourceEventX.X3);

        getLog().fine("waiting for X3 processing to finish");
        waitForEventProcessing(TCKResourceEventX.X3);
    }

    private void diagnoseCase2Result() throws Exception {
        // diagnose the call received from Y2 (if any)
        if(isEventProcessingFinished(TCKResourceEventY.Y2)) {
            if(isACKReceived(TCKResourceEventY.Y2)) {
                if(aciValueOrExceptionFromY2 instanceof Exception) {
                    getLog().warning("The attempt to access the ACI or it's value in the Y2 event handler resulted in an Exception "+
                            "being thrown. Allow this, as the SLEE may be using pessimistic locking on the ACI. Exception caught:");
                    getLog().warning((Exception)aciValueOrExceptionFromY2);
                    getLog().fine("Continuing...");
                } else {
                    String aciValue = (String)aciValueOrExceptionFromY2;
                    if(aciValue == null) {
                        if(isRolledBack(TCKResourceEventY.Y2)) {
                            getLog().info("The ACI value retrieved by the Y2 handler was null, but the transaction was "+
                                    "rolled back. This is an accepted result.");
                        } else {
                            throw new TCKTestFailureException(2003,"The ACI value retrieved by the Y2 handler "+
                                "was null, even though the ACI was initialised to a non-null value in an event handler for a "+
                                "TCKResourceEventX1 event which has completed processing. Y2's transaction was not rolled back.");
                        }
                    } else if(aciValue.equals(TCKResourceEventY.Y1) || aciValue.equals(TCKResourceEventX.X1)) {
                        getLog().info("The ACI value retrieved by the Y2 handler was not the value set by "+
                                "the X3 event handler (in a concurrent transaction). This is the "+
                                "expected result. Continuing...");
                    } else if(aciValue.equals(TCKResourceEventX.X3)) {
                        throw new TCKTestFailureException(2003,"The ACI value retrieved by the Y2 handler was the value set "+
                                "by the X3 event handler (in a concurrent transaction). This indicates that "+
                                "transaction isolation was broken: uncommitted changes to an ActivityContextInterface "+
                                "in one transaction were visible to other transactions.");
                    }
                }
            } else if(isRolledBack(TCKResourceEventY.Y2)) {
                getLog().info("The second ACK was not received from the Y2 event handler, and the transaction was "+
                        "rolled back. This is an accepted result. Contining...");
            } else {
                throw new TCKTestErrorException(
                    "Indication of Y2 completing processing before an ACK or rolled back indication was received for the event handler");
            }
        }
    }

    private void freeResources() throws Exception {
        getLog().fine("Performing clean up tasks");

        getLog().fine("release all blocking methods");
        stopBlockingFor(TCKResourceEventX.X2);
        stopBlockingFor(TCKResourceEventX.X3);

        getLog().fine("fire Y3 on activity A");
        fireEvent(TCKResourceEventY.Y3,activityA);
        getLog().fine("wait for Y3 processing to finish");
        waitForEventProcessing(TCKResourceEventY.Y3);
    }

    private void fireEvent(String eventType, TCKActivityID activityID) throws TCKTestErrorException, RemoteException {
        getLog().info("Firing an "+eventType+" on activity "+activityID);
        long eventObjectID = utils().getResourceInterface().fireEvent(eventType, null, activityID, null);
        synchronized(stateLock) {
            eventObjectIDs.put(eventType,new Long(eventObjectID));
            stateLock.notifyAll();
        }
    }

    // -- Specific event handlers. All synchronized on stateLock --

    private void handleX1() {
        // no-op
    }

    private void handleX2(Object argument) {
        if(getFlag(X2_FIRST_ACK_RECEIVED)) {
            // this is the second call from X2
            setFlag(X2_SECOND_ACK_RECEIVED);
            aciValueOrExceptionFromX2 = argument;
        } else {
            // this is the first call from X2
            setFlag(X2_FIRST_ACK_RECEIVED);
            getLog().info("X2 handler read ACI value before blocking: "+argument);
            blockEventHandler(TCKResourceEventX.X2);
        }
    }

    private void handleX3() {
        blockEventHandler(TCKResourceEventX.X3);
    }

    private void handleY1(Object argument) {
        if(argument != null) {
            Exception exceptionCaught = (Exception)argument;
            getLog().info("The Y1 event handler caught Exception which trying to access the ACI, "+
                "or update its state:");
            getLog().info(exceptionCaught);
            getLog().info("Presumably this is because the SLEE chose to disallow access to the ACI resource after "+
                    "read access from the X2 handler in another transaction.");
        }
    }

    private void handleY2(Object argument) {
        if(argument instanceof Exception) {
            getLog().info("Y2 handler caught exception while trying to read the ACI value: "+argument);
        } else {
            getLog().info("Y2 handler read ACI value: "+argument);
        }
        aciValueOrExceptionFromY2 = argument;
    }

    private void handleY3() {
        // no-op
    }

    // -- State flag related methods

    private void setFlag(String flagName) {
        synchronized(stateLock) {
            if(!getFlag(flagName)) {
                getLog().fine("Setting flag: "+flagName);
                flags.add(flagName);
            }
            stateLock.notifyAll();
        }
    }

    private boolean getFlag(String flagName) {
        synchronized(stateLock) {
            return flags.contains(flagName);
        }
    }

    /**
     * Waits until one of the following occurs: <ul>
     * <li>(1) The given flag is set </li>
     * <li>(2) An Exception is received via TCKResourceListener.onException() </li>
     * <li>(3) The timeout is reached </li>
     * </ul>
     *
     * In case (1), this method returns silently.
     * In case (2), the Exception is rethrown, wrapped in a TCKTestErrorException
     * In case (3), an OperationTimedOutException is thrown
     *
     * @param flagName the name of the flag to wait for
     * @throws OperationTimedOutException thrown when the test timeout is reached while waiting for the flag to be set
     * @throws TCKTestErrorException if an Exception is received via TCKResourceListener.onException() while waiting
     *  for the flag to be set, it is wrapped in a TCKTestErrorException and rethrown
     */
    private void waitForFlag(String flagName) throws OperationTimedOutException, TCKTestErrorException {
        synchronized(stateLock) {
            long now = System.currentTimeMillis();
            long timeoutAt = now + utils().getTestTimeout();
            while(now < timeoutAt && !getFlag(flagName) && sbbOrResourceException == null) {
                try {
                    stateLock.wait(timeoutAt - now);
                } catch(InterruptedException ie) { /* no-op */ }
                now = System.currentTimeMillis();
            }
            if(sbbOrResourceException != null) throw new TCKTestErrorException(
                    "Received an Exception from an SBB or TCK resource",sbbOrResourceException);
            if(!getFlag(flagName)) throw new OperationTimedOutException("Timed out waiting for flag to be set:"+flagName);
        }
    }

    private void setACKReceived(String eventHandlerName) {
        setFlag(EVENT_ACK_PREFIX+eventHandlerName);
    }

    private boolean isACKReceived(String eventHandlerName) {
        return getFlag(EVENT_ACK_PREFIX+eventHandlerName);
    }

    private boolean isRolledBack(String eventHandlerName) {
        return getFlag(ROLLED_BACK_FLAG_PREFIX+eventHandlerName);
    }

    private void waitForACK(String eventHandlerName) throws OperationTimedOutException, TCKTestErrorException {
        waitForFlag(EVENT_ACK_PREFIX+eventHandlerName);
    }

    /**
     * Returns true if the method can confirm that the given method has completed processing,
     * false otherwise.
     */
    private boolean isEventProcessingFinished(String eventType) {
        synchronized(stateLock) {
            Long eventObjectID = (Long)eventObjectIDs.get(eventType);
            if(eventObjectID != null) {
                return processedEvents.contains(eventObjectID);
            } else {
                // if eventObjectID was null, else expect a (handled) race condition between
                // the event firing thread and the event delivery thread. the condition should
                // be rechecked when the event object ID is set, or an event processing successful/failed call is made
                return false;
            }
        }
    }

    private void waitForEventProcessing(String eventType) throws OperationTimedOutException, TCKTestErrorException {
        synchronized(stateLock) {
            long now = System.currentTimeMillis();
            long timeoutAt = now + utils().getTestTimeout();
            while(now < timeoutAt && !isEventProcessingFinished(eventType) && sbbOrResourceException == null) {
                try {
                    stateLock.wait(timeoutAt - now);
                } catch(InterruptedException ie) { /* no-op */ }
                now = System.currentTimeMillis();
            }
            if(sbbOrResourceException != null) throw new TCKTestErrorException(
                    "Received an Exception from an SBB or TCK resource",sbbOrResourceException);
            if(isEventProcessingFinished(eventType)) {
                getLog().fine("Event processing has finished for "+eventType+ " event");
            } else throw new OperationTimedOutException("Timed out waiting for event to be processed:"+eventType);
        }
    }

    /**
     * Blocks the calling thread until one of the following occurs: <ul>
     * <li>(1) A request to stop blocking is made</li>
     * <li>(2) An Exception is received via TCKResourceListener.onException() </li>
     * </ul>
     *
     * In case either case, this method returns silently.
     *
     * @param eventHandlerName the name of the event handler method being blocked
     */
    private void blockEventHandler(String eventHandlerName) {
        synchronized(stateLock) {
            while(!getFlag(STOP_BLOCKING_PREFIX+eventHandlerName) && sbbOrResourceException == null) {
                try {
                    stateLock.wait();
                } catch(InterruptedException ie) { /* no-op */ }
            }
        }
    }

    private void stopBlockingFor(String eventHandlerName) {
        setFlag(STOP_BLOCKING_PREFIX+eventHandlerName);
    }

    // -- Private classes --

    private class ResourceListenerImpl extends BaseTCKResourceListener {
        public Object onSbbCall(Object argument) throws Exception {
            synchronized(stateLock) {
                Map args = (Map) argument;
                String eventHandlerName = (String) args.get(IsolationTestConstants.SBB_EVENT_HANDLER_FIELD);
                if(args.containsKey(IsolationTestConstants.SBB_ROLLED_BACK)) {
                    setFlag(ROLLED_BACK_FLAG_PREFIX+eventHandlerName);
                } else {
                    setACKReceived(eventHandlerName);
                    Object value = args.get(IsolationTestConstants.VALUE_FIELD);
                    getLog().fine("CALLTEST: "+ eventHandlerName + " entering callback");
                    if (eventHandlerName.equals(TCKResourceEventX.X1)) {
                        handleX1();
                    } else if (eventHandlerName.equals(TCKResourceEventX.X2)) {
                        handleX2(value);
                    } else if (eventHandlerName.equals(TCKResourceEventX.X3)) {
                        handleX3();
                    } else if (eventHandlerName.equals(TCKResourceEventY.Y1)) {
                        handleY1(value);
                    } else if (eventHandlerName.equals(TCKResourceEventY.Y2)) {
                        handleY2(value);
                    } else if (eventHandlerName.equals(TCKResourceEventY.Y3)) {
                        handleY3();
                    }
                    getLog().fine("CALLTEST: " + eventHandlerName + " exiting callback");
                }
                stateLock.notifyAll();
                return null;
            }
        }

        public void onEventProcessingSuccessful(long eventObjectID) throws RemoteException {
            getLog().fine("onEventProcessingSuccessful(): eventObjectID="+eventObjectID);
            synchronized(stateLock) {
                processedEvents.add(new Long(eventObjectID));
                stateLock.notifyAll();
            }
        }

        public void onEventProcessingFailed(long eventObjectID, String message, Exception exception) throws RemoteException {
            getLog().warning("onEventProcessingFailed(): eventObjectID="+eventObjectID+"; message="+message);
            getLog().warning(exception);
            synchronized(stateLock) {
                processedEvents.add(new Long(eventObjectID));
                StringBuffer buf = new StringBuffer("Received onEventProcessingFailed() callback for TCKResourceEvent with object ID="+eventObjectID);
                if(message != null) buf.append(message);
                onException(new TCKTestErrorException(buf.toString(),exception));
            }
        }

        public void onException(Exception exception) throws RemoteException {
            getLog().warning("Received an Exception via TCKResourceListener.onException():");
            getLog().warning(exception);
            synchronized(stateLock) {
                sbbOrResourceException = exception;
                stateLock.notifyAll();
            }
        }

    }

    // Private state

    private TCKActivityID activityA;
    private TCKActivityID activityB;

    private Object stateLock; // state updates and accesses are synchronized on stateLock

    private HashSet flags; // a set of flags. if the flag name is in the set it is considered set
    private Exception sbbOrResourceException; // an Exception received via TCKResourceListener.onException()
    private HashMap eventObjectIDs;
    private HashSet processedEvents;

    private Object aciValueOrExceptionFromX2;
    private Object aciValueOrExceptionFromY2;

}
