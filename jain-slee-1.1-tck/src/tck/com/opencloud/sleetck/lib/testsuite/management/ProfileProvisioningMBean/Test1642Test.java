/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.management.ProfileProvisioningMBean;

import javax.slee.management.*;
import javax.slee.ComponentID;
import javax.slee.ServiceID;
import javax.slee.profile.ProfileSpecificationID;
import javax.management.ObjectName;

import com.opencloud.sleetck.lib.*;
import com.opencloud.sleetck.lib.testutils.*;
import com.opencloud.sleetck.lib.resource.TCKSbbMessage;
import com.opencloud.sleetck.lib.resource.testapi.TCKResourceListener;
import com.opencloud.sleetck.lib.resource.TCKActivityID;
import com.opencloud.sleetck.lib.resource.testapi.TCKResourceTestInterface;
import com.opencloud.sleetck.lib.resource.events.TCKResourceEventX;
import com.opencloud.sleetck.lib.testutils.ProfileUtils;

import com.opencloud.sleetck.lib.testutils.jmx.DeploymentMBeanProxy;
import com.opencloud.sleetck.lib.testutils.jmx.ServiceManagementMBeanProxy;
import com.opencloud.sleetck.lib.testutils.jmx.ProfileProvisioningMBeanProxy;

import com.opencloud.logging.Logable;

import java.rmi.RemoteException;
import java.util.HashMap;

public class Test1642Test implements SleeTCKTest {

    private static final String SERVICE_DU_PATH_PARAM = "DUPath";
    private static final int TEST_ID = 1642;

    public void init(SleeTCKTestUtils utils) {
        this.utils = utils;
    }

    /**
     * Perform the actual test.
     */

    public TCKTestResult run() throws Exception {

        profileUtils = new ProfileUtils(utils);
        ProfileProvisioningMBeanProxy profileProxy = profileUtils.getProfileProvisioningProxy();
        DeploymentMBeanProxy duProxy = utils.getDeploymentMBeanProxy();
        DeployableUnitDescriptor duDesc = duProxy.getDescriptor(duID);
        ComponentID components[] = duDesc.getComponents();

        for (int i = 0; i < components.length; i++) {
            if (components[i] instanceof ProfileSpecificationID) {
                ProfileSpecificationID profileSpecID = (ProfileSpecificationID) components[i];

                // Create the profile table.
                try {
                    profileProxy.createProfileTable(profileSpecID, "Test1642ProfileTable");
                } catch (Exception e) {
                    return TCKTestResult.error("Failed to create profile table.");
                }

                // Get the JMX object name of the default profile.
                ObjectName jmxObjectName = profileProxy.getDefaultProfile("Test1642ProfileTable");

                if (jmxObjectName == null) {
                    return TCKTestResult.failed(TEST_ID, "ProfileProvisioningMBean.getDefaultProfile(String) didn't returned a null ObjectName object.");
                }

                return TCKTestResult.passed();
            }
        }

        return TCKTestResult.error("No ProfileSpecification found.");
    }

    /**
     * Do all the pre-run configuration of the test.
     */

    public void setUp() throws Exception {

        utils.getLog().fine("Connecting to resource");
        resourceListener = new TCKResourceListenerImpl();
        utils.getResourceInterface().setResourceListener(resourceListener);
        utils.getLog().fine("Installing and activating service");

        // Install the Deployable Units
        String duPath = utils.getTestParams().getProperty(SERVICE_DU_PATH_PARAM);
        duID = utils.install(duPath);
        utils.activateServices(duID, true);

    }

    /**
     * Clean up after the test.
     */

    public void tearDown() throws Exception {

        try {
            profileUtils.removeProfileTable("Test1642ProfileTable");
        } catch (Exception e) {
        }

        utils.getLog().fine("Disconnecting from resource");
        utils.getResourceInterface().clearActivities();
        utils.getResourceInterface().removeResourceListener();
        utils.getLog().fine("Deactivating and uninstalling service");
        utils.deactivateAllServices();
        utils.uninstallAll();
    }

    private class TCKResourceListenerImpl extends BaseTCKResourceListener {
        public synchronized void onSbbMessage(TCKSbbMessage message, TCKActivityID calledActivity) throws RemoteException {
            utils.getLog().info("Received message from SBB");

            HashMap map = (HashMap) message.getMessage();
            Boolean passed = (Boolean) map.get("Result");
            String msgString = (String) map.get("Message");

            if (passed.booleanValue() == true)
                result.setPassed();
            else
                result.setFailed(TEST_ID, msgString);
        }

        public void onException(Exception e) throws RemoteException {
            utils.getLog().warning("Received exception from SBB");
            utils.getLog().warning(e);
            result.setError(e);
        }
    }

    private SleeTCKTestUtils utils;
    private TCKResourceListener resourceListener;
    private FutureResult result;
    private DeployableUnitID duID;
    private ProfileUtils profileUtils;
}
