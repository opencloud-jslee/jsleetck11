/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.usage.sets;

import com.opencloud.sleetck.lib.resource.events.TCKResourceEventX;
import com.opencloud.sleetck.lib.sbbutils.BaseTCKSbb;
import com.opencloud.sleetck.lib.sbbutils.TCKSbbUtils;
import com.opencloud.sleetck.lib.testsuite.usage.common.GenericUsageParameterInterface;
import com.opencloud.sleetck.lib.TCKTestResult;

import javax.slee.ActivityContextInterface;
import javax.slee.facilities.Level;
import javax.slee.usage.UnrecognizedUsageParameterSetNameException;

/**
 * Tries to access the given invalid parameter set, then send back
 * a test result to the test.
 */
public abstract class Test2258Sbb extends BaseTCKSbb {

    private static final int TEST_ID = 2258;

    public void onTCKResourceEventX1(TCKResourceEventX event, ActivityContextInterface aci) {
        try {
            String parameterSetName = (String)event.getMessage();
            TCKSbbUtils.createTrace(getSbbID(),Level.INFO,"onTCKResourceEventX1(): parameterSetName="+parameterSetName,null);
            TCKTestResult result;
            // try to access the usage parameter interface
            try {
                getSbbUsageParameterSet(parameterSetName);
                result = TCKTestResult.failed(TEST_ID,"The expected UnrecognizedUsageParameterSetNameException was not thrown "+
                        "when trying to access a usage parameter interface with an invalid name -- no set by the name \""+
                        parameterSetName+"\" was created by the test");
            } catch (UnrecognizedUsageParameterSetNameException e) {
                TCKSbbUtils.createTrace(getSbbID(),Level.INFO,"onTCKResourceEventX1(): Caught expected "+
                        "UnrecognizedUsageParameterSetNameException for unused parameter name \""+parameterSetName+
                        "\". Exception message: "+e.getMessage(),null);
                result = TCKTestResult.passed();
            }
            // send the test result to the test
            TCKSbbUtils.getResourceInterface().sendSbbMessage(result.toExported());
        } catch(Exception e) {
            TCKSbbUtils.handleException(e);
        }
    }

    /**
     * Accessor for the un-named usage parameter interface.
     */
    public abstract GenericUsageParameterInterface getDefaultSbbUsageParameterSet();

    /**
     * Accessor for the named usage parameter interfaces.
     */
    public abstract GenericUsageParameterInterface getSbbUsageParameterSet(String name)
                        throws UnrecognizedUsageParameterSetNameException;

}
