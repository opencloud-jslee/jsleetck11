/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.javax.slee.profile.ProfileID;

import com.opencloud.sleetck.lib.SleeTCKTest;
import com.opencloud.sleetck.lib.SleeTCKTestUtils;
import com.opencloud.sleetck.lib.TCKTestResult;
import com.opencloud.sleetck.lib.testutils.ProfileUtils;
import com.opencloud.sleetck.lib.testutils.jmx.DeploymentMBeanProxy;
import com.opencloud.sleetck.lib.testutils.jmx.ProfileMBeanProxy;
import com.opencloud.sleetck.lib.testutils.jmx.ProfileProvisioningMBeanProxy;

import javax.slee.Address;
import javax.slee.ComponentID;
import javax.slee.management.DeployableUnitDescriptor;
import javax.slee.management.DeployableUnitID;
import javax.slee.profile.ProfileID;
import javax.slee.profile.ProfileSpecificationID;

public class Test4432Test implements SleeTCKTest {

    private static final String SERVICE_DU_PATH_PARAM = "DUPath";

    public void init(SleeTCKTestUtils utils) {
        this.utils = utils;
    }

    /**
     * Perform the actual test.
     */

    public TCKTestResult run() throws Exception {

        ProfileID firstProfile = new ProfileID(Test4432Sbb.PROFILE_TABLE_NAME, Test4432Sbb.PROFILE_NAME);
        ProfileID secondProfile = new ProfileID(Test4432Sbb.PROFILE_TABLE_NAME, Test4432Sbb.SECOND_PROFILE_NAME);

        Address address = firstProfile.toAddress();
        if (!address.getAddressPlan().isSleeProfile())
            return TCKTestResult.failed(4432, "ProfileID.toAddress() returned freaky address.");

        if (firstProfile.equals(secondProfile))
            return TCKTestResult.failed(4434, "ProfileID.equals(different ProfileID) returned true.");

        if (!firstProfile.equals(firstProfile))
            return TCKTestResult.failed(4434, "ProfileID.equals(self) returned false.");

        try {
            firstProfile.hashCode();
        } catch (Exception e) {
            return TCKTestResult.failed(4436, "ProfileID.hashCode() threw an exception.");
        }

        try {
            firstProfile.toString();
        } catch (Exception e) {
            return TCKTestResult.failed(4438, "ProfileID.toString() threw an exception.");
        }

        if (firstProfile.toString() == null)
            return TCKTestResult.failed(4438, "ProfileID.toString() returned null.");

        return TCKTestResult.passed();
    }

    /**
     * Do all the pre-run configuration of the test.
     */

    public void setUp() throws Exception {

        utils.getLog().fine("Installing and activating service");

        // Install the Deployable Units
        String duPath = utils.getTestParams().getProperty(SERVICE_DU_PATH_PARAM);
        duID = utils.install(duPath);

        // Activate the DU
        utils.activateServices(duID, true);

        // Create a Profile Table, and some Profiles.
        ProfileProvisioningMBeanProxy profileProxy = new ProfileUtils(utils).getProfileProvisioningProxy();
        DeploymentMBeanProxy duProxy = utils.getDeploymentMBeanProxy();
        DeployableUnitDescriptor duDesc = duProxy.getDescriptor(duID);
        ComponentID components[] = duDesc.getComponents();

        for (int i = 0; i < components.length; i++) {
            if (components[i] instanceof ProfileSpecificationID) {
                ProfileSpecificationID profileSpecID = (ProfileSpecificationID) components[i];

                profileProxy.createProfileTable(profileSpecID, Test4432Sbb.PROFILE_TABLE_NAME);

                javax.management.ObjectName profile = profileProxy.createProfile(Test4432Sbb.PROFILE_TABLE_NAME, Test4432Sbb.PROFILE_NAME);
                ProfileMBeanProxy profProxy = utils.getMBeanProxyFactory().createProfileMBeanProxy(profile);
                profProxy.commitProfile();

                profile = profileProxy.createProfile(Test4432Sbb.PROFILE_TABLE_NAME, Test4432Sbb.SECOND_PROFILE_NAME);
                profProxy = utils.getMBeanProxyFactory().createProfileMBeanProxy(profile);
                profProxy.commitProfile();
            }
        }

    }

    /**
     * Clean up after the test.
     */

    public void tearDown() throws Exception {
        ProfileProvisioningMBeanProxy profileProxy = new ProfileUtils(utils).getProfileProvisioningProxy();
        try {
            profileProxy.removeProfile(Test4432Sbb.PROFILE_TABLE_NAME, Test4432Sbb.PROFILE_NAME);
        } catch (Exception e) {
        }
        try {
            profileProxy.removeProfile(Test4432Sbb.PROFILE_TABLE_NAME, Test4432Sbb.SECOND_PROFILE_NAME);
        } catch (Exception e) {
        }
        try {
            profileProxy.removeProfileTable(Test4432Sbb.PROFILE_TABLE_NAME);
        } catch (Exception e) {
        }

        utils.getLog().fine("Disconnecting from resource");
        utils.getResourceInterface().clearActivities();
        utils.getResourceInterface().removeResourceListener();
        utils.getLog().fine("Deactivating and uninstalling service");
        utils.deactivateAllServices();
        utils.uninstallAll();
    }

    private SleeTCKTestUtils utils;
    private DeployableUnitID duID;
}