/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.facilities.alarmfacility;

import javax.naming.InitialContext;
import javax.slee.ActivityContextInterface;
import javax.slee.facilities.AlarmFacility;
import javax.slee.facilities.AlarmLevel;
import javax.slee.facilities.Tracer;

import com.opencloud.sleetck.lib.resource.events.TCKResourceEventX;
import com.opencloud.sleetck.lib.sbbutils.BaseTCKSbb;
import com.opencloud.sleetck.lib.sbbutils.TCKSbbUtils;

/*  
 * AssertionID(1113506): Test This method only clears alarms 
 * associated with the notification source of the alarm facility 
 * object.
 * 
 */
public abstract class Test1113506Sbb2 extends BaseTCKSbb {

    public static final String ALARM_MESSAGE = "Test1113506AlarmMessageSbb2";

    public static final String ALARM_INSTANCEID = "Test1113506AlarmInstanceIDSbb2";

    // Initial event
    public void onTCKResourceEventX1(TCKResourceEventX event, ActivityContextInterface aci) {
        try {
            tracer = this.getSbbContext().getTracer("com.test");
            tracer.info("Received " + event + " message", null);
            AlarmFacility facility = getAlarmFacility();

            // raise an alarm
            String[] alarmIDs = {
                    facility.raiseAlarm("javax.slee.management.Alarm6", "Test1113506AlarmInstanceID6",
                            AlarmLevel.CRITICAL, ALARM_MESSAGE),
                    facility.raiseAlarm("javax.slee.management.Alarm7", "Test1113506AlarmInstanceID7",
                            AlarmLevel.MAJOR, ALARM_MESSAGE),
                    facility.raiseAlarm("javax.slee.management.Alarm8", "Test1113506AlarmInstanceID8",
                            AlarmLevel.WARNING, ALARM_MESSAGE),
                    facility.raiseAlarm("javax.slee.management.Alarm9", "Test1113506AlarmInstanceID9",
                            AlarmLevel.INDETERMINATE, ALARM_MESSAGE),
                    facility.raiseAlarm("javax.slee.management.Alarm10", "Test1113506AlarmInstanceID10",
                            AlarmLevel.MINOR, ALARM_MESSAGE) };

        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }

    }

    private AlarmFacility getAlarmFacility() throws Exception {
        AlarmFacility facility = null;
        String JNDI_ALARMFACILITY_NAME = AlarmFacility.JNDI_NAME;
        try {
            facility = (AlarmFacility) new InitialContext().lookup(JNDI_ALARMFACILITY_NAME);
        } catch (Exception e) {
            tracer.warning("got unexpected Exception: " + e, null);
        }
        return facility;
    }

    private Tracer tracer;
}
