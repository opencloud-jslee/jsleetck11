/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.facilities.servicelookupfacility;

import java.util.HashMap;

import javax.slee.ActivityContextInterface;
import javax.slee.Address;
import javax.slee.EventContext;
import javax.slee.ServiceID;
import javax.slee.resource.ResourceAdaptorContext;
import javax.slee.serviceactivity.ServiceStartedEvent;

import com.opencloud.sleetck.lib.rautils.UOID;
import com.opencloud.sleetck.lib.testsuite.resource.BaseResourceSbb;
import com.opencloud.sleetck.lib.testsuite.resource.RAMethods;
import com.opencloud.sleetck.lib.testsuite.resource.SimpleEvent;
import com.opencloud.sleetck.lib.testsuite.resource.SimpleSbbInterface;

public abstract class Test1113671Sbb2 extends BaseResourceSbb {

    // Event handler
    public void onServiceStartedEvent(ServiceStartedEvent event, ActivityContextInterface aci, EventContext context) {
        tracer.info("onServiceStartedEvent() event handler called with event: " + event);
        try {
            Test1113671Event testEvent = new Test1113671Event();
            fireTest1113671Event(testEvent, aci, null, null);
            
        } catch (Exception e) {
            tracer.severe("Exception caught while trying to send message from SBB to TCK", e);
        }
    }
    
    public void onTest1113671Event(Test1113671Event event, ActivityContextInterface aci, EventContext context) {
        try {
            tracer.info("Test1113671Sbb2:I got Test1113671Event on Activity");
        } catch (Exception e) {
            tracer.severe("Exception caught while trying to send message from SBB to TCK", e);
        }

    }
    
    public abstract void fireTest1113671Event(Test1113671Event event, ActivityContextInterface aci, Address address,
            ServiceID serviceID);


    private static final UOID sbbUID = UOID.createUOID();
}
