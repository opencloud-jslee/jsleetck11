/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.events.maskEvent;

import javax.slee.ActivityEndEvent;
import javax.slee.ActivityContextInterface;
import javax.slee.SbbContext;
import javax.slee.Address;
import javax.slee.SbbLocalObject;
import javax.slee.facilities.Level;
import javax.slee.ChildRelation;
import javax.slee.RolledBackContext;

import javax.slee.nullactivity.NullActivityFactory;
import javax.slee.nullactivity.NullActivity;
import javax.slee.nullactivity.NullActivityContextInterfaceFactory;
import javax.slee.facilities.ActivityContextNamingFacility;

import com.opencloud.sleetck.lib.sbbutils.BaseTCKSbb;
import com.opencloud.sleetck.lib.sbbutils.TCKSbbUtils;
import com.opencloud.sleetck.lib.sbbutils.TCKSbbConstants;
import com.opencloud.sleetck.lib.resource.events.TCKResourceEventX;
import com.opencloud.sleetck.lib.resource.events.TCKResourceEventY;
import com.opencloud.sleetck.lib.resource.TCKActivityID;
import com.opencloud.sleetck.lib.resource.sbbapi.TCKActivity;
import com.opencloud.sleetck.lib.resource.sbbapi.TCKResourceSbbInterface;
import com.opencloud.sleetck.lib.resource.sbbapi.TCKActivityContextInterfaceFactory;

import java.util.HashMap;
import java.util.Iterator;

public abstract class Test720Sbb extends BaseTCKSbb {

    public void sbbRolledBack(RolledBackContext context) {
    }

    // Initial event method.
    public void onTCKResourceEventX1(TCKResourceEventX ev, ActivityContextInterface aci) {

        try {
            getSbbContext().maskEvent(new String [] { "Test720Event" }, aci);

            fireTest720Event(new Test720Event(), aci, null);
            fireTest720SecondEvent(new Test720SecondEvent(), aci, null);
        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }
    }

    public void onTCKResourceEventX2(TCKResourceEventX ev, ActivityContextInterface aci) {

        try {
            String masked[] = getSbbContext().getEventMask(aci);
            if (!isMasked(masked, "Test720Event")) {
                // failure
                HashMap map = new HashMap();
                map.put("Result", new Boolean(false));
                map.put("Message", "Masked event was not visibly masked after the TXN had committed.");
                TCKSbbUtils.getResourceInterface().sendSbbMessage(map);
                return;
            }

            getSbbContext().maskEvent(new String [] { "Test720SecondEvent" }, aci);
            getSbbContext().setRollbackOnly();

        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }

    }

    public void onTCKResourceEventY1(TCKResourceEventY ev, ActivityContextInterface aci) {

        try {

            HashMap map = new HashMap();

            String masked [] = getSbbContext().getEventMask(aci);
            if (isMasked(masked, "Test720SecondEvent")) {
                // failure
                map.put("Result", new Boolean(false));
                map.put("Message", "Masked event was visible after TXN was rolled back.");
            } else {
                // success
                map.put("Result", new Boolean(true));
                map.put("Message", "Ok");
            }

            TCKSbbUtils.getResourceInterface().sendSbbMessage(map);
                
        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }


    }

    public boolean isMasked(String mask[], String event) {
        for (int i = 0; i < mask.length; i++) {
            if (mask[i].equals(event))
                return true;
        }
        
        return false;
    }

    public void onTest720Event(Test720Event event, ActivityContextInterface aci) {
    }

    public void onTest720SecondEvent(Test720SecondEvent event, ActivityContextInterface aci) {
    }

    public abstract void fireTest720Event(Test720Event event, ActivityContextInterface aci, Address address);
    public abstract void fireTest720SecondEvent(Test720SecondEvent event, ActivityContextInterface aci, Address address);

}
