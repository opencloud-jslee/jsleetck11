/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.facilities.alarmfacility;

import java.util.HashMap;

import javax.naming.InitialContext;
import javax.slee.ActivityContextInterface;
import javax.slee.facilities.AlarmFacility;
import javax.slee.facilities.AlarmLevel;
import javax.slee.facilities.Tracer;

import com.opencloud.sleetck.lib.resource.events.TCKResourceEventX;
import com.opencloud.sleetck.lib.sbbutils.BaseTCKSbb;
import com.opencloud.sleetck.lib.sbbutils.TCKSbbUtils;

/*  
 * AssertionID(1113495): Test If the identified alarm is not 
 * active this method returns false.
 * 
 * AssertionID(1113496): Test If the identified alarm is active 
 * the alarm is cleared and the method returns true.
 *  
 */
public abstract class Test1113495Sbb extends BaseTCKSbb {

    private static final String JNDI_ALARMFACILITY_NAME = AlarmFacility.JNDI_NAME;

    public static final String ALARM_MESSAGE = "Test1113495AlarmMessage";

    public static final String ALARM_INSTANCEID = "Test1113495AlarmInstanceID";

    // Initial event
    public void onTCKResourceEventX1(TCKResourceEventX event, ActivityContextInterface aci) {

        try {
            tracer = this.getSbbContext().getTracer("com.test");
            tracer.info("Received " + event + " message", null);

            AlarmFacility facility = (AlarmFacility) new InitialContext().lookup(JNDI_ALARMFACILITY_NAME);
            //raise an alarm
            setFirstAlarm(facility.raiseAlarm("javax.slee.management.Alarm", ALARM_INSTANCEID, AlarmLevel.MAJOR,
                    ALARM_MESSAGE));

            //clear the alarm
            if (!facility.clearAlarm(getFirstAlarm())) {
                sendResultToTCK("Test1113495Test", false, 1113495, "The alarm which has been raised cannot be cleared!");
                return;
            }

            //try to clear the alarm again, should fail because of if the identified alarm 
            //is not active this method should return false.
            if (facility.clearAlarm(getFirstAlarm())) {
                sendResultToTCK("Test1113495Test", false, 1113495, "The alarm which is not active, so that it cannot be cleared again. The"
                        + "method shuold return false.");
                return;
            }
        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }

    }

    // Second event
    public void onTCKResourceEventX2(TCKResourceEventX event, ActivityContextInterface aci) {

        try {
            tracer = this.getSbbContext().getTracer("com.test");
            tracer.info("Received " + event + " message again", null);
            AlarmFacility facility = (AlarmFacility) new InitialContext().lookup(JNDI_ALARMFACILITY_NAME);
            //raise an alarm
            setFirstAlarm(facility.raiseAlarm("javax.slee.management.Alarm", ALARM_INSTANCEID, AlarmLevel.MAJOR,
                    ALARM_MESSAGE));
            //try to clear the alarm which has been raised. If the identified alarm is 
            //active the alarm is cleared and the method should return true.
            if (!facility.clearAlarm(getFirstAlarm())) {
                sendResultToTCK("Test1113495Test", false, 1113496, "The alarm which has been raised cannot be cleared. The method should"
                        + "return true!");
                return;
            }

            sendResultToTCK("Test1113495Test", true, 1113495,"AlarmFacility.clearAlarm() method tests passed!");
        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }

    }

    private void sendResultToTCK(String testName, boolean result, int failedAssertionID,  String message) throws Exception {
        HashMap sbbData = new HashMap();
        sbbData.put("testname", testName);
        sbbData.put("result", result ? "pass" : "fail");
        sbbData.put("message", message);
        sbbData.put("id", new Integer(failedAssertionID));
        TCKSbbUtils.getResourceInterface().sendSbbMessage(sbbData);
    }

    public abstract void setFirstAlarm(String alarmID);

    public abstract String getFirstAlarm();

    private Tracer tracer;
}
