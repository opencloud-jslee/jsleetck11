/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.sbb.abstractclass;

import com.opencloud.sleetck.lib.sbbutils.TCKSbbUtils;
import com.opencloud.sleetck.lib.resource.events.TCKResourceEventX;

import javax.slee.*;

/**
 * Test sbbRolledBack() method from spec s6.12.1
 */
public abstract class SbbRolledBackNewTransactionSbb extends SendResultsSbb {
    public void onTCKResourceEventX1(TCKResourceEventX event, ActivityContextInterface aci) {
        // set transactional test value
        setTestValue(3);
        // mark for rollback
        getSbbContext().setRollbackOnly();
    }

    public void sbbRolledBack(javax.slee.RolledBackContext context) {

        try {
            if (context.getEvent() == null) {
                setResultFailed(467, "event argument to sbbRolledBack is null");
                return;
            }

            if (context.getActivityContextInterface() == null) {
                setResultFailed(467, "aci argument to sbbRolledBack is null");
                return;
            }

            // check transactional state
            if (getTestValue() != 0) {
                setResultFailed(468, "sbbRolledBack has transactional state from the previous transaction");
                return;
            }

            setTestValue(5);
        }
        catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }

        // ensure this transaction will roll back
        getSbbContext().setRollbackOnly();
        throw new RuntimeException("");
    }


    public void sbbExceptionThrown(Exception exception, Object event, ActivityContextInterface aci) {
        try {
            if (event == null) {
                setResultFailed(467, "event argument to sbbExceptionThrown is null");
                return;
            }

            if (aci == null) {
                setResultFailed(467, "aci argument to sbbExceptionThrown is null");
                return;
            }

            // check transactional state
            if (getTestValue() != 5) {
                setResultFailed(469, "sbbExceptionThrown not invoked from failed sbbRolledBack in same transaction");
                return;
            }

            setResultPassed("sbbRolledBack for new transaction and sbbExceptionThrown passed");
        }
        catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }
    }

    public abstract void setTestValue(int value);
    public abstract int getTestValue();

}

