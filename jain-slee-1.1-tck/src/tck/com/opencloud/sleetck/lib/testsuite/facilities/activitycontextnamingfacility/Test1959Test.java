/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.facilities.activitycontextnamingfacility;

import java.rmi.RemoteException;
import java.util.HashMap;

import com.opencloud.sleetck.lib.AbstractSleeTCKTest;
import com.opencloud.sleetck.lib.TCKTestErrorException;
import com.opencloud.sleetck.lib.TCKTestResult;
import com.opencloud.sleetck.lib.resource.TCKActivityID;
import com.opencloud.sleetck.lib.resource.TCKSbbMessage;
import com.opencloud.sleetck.lib.resource.events.TCKResourceEventX;
import com.opencloud.sleetck.lib.resource.testapi.TCKResourceListener;
import com.opencloud.sleetck.lib.resource.testapi.TCKResourceTestInterface;
import com.opencloud.sleetck.lib.testutils.BaseTCKResourceListener;
import com.opencloud.sleetck.lib.testutils.FutureResult;

public class Test1959Test extends AbstractSleeTCKTest {

    private static final String SERVICE_DU_PATH_PARAM = "DUPath";
    private static final int TEST_ID = 1959;

    /**
     * Perform the actual test.
     */
    public TCKTestResult run() throws Exception {
        result = new FutureResult(getLog());

        TCKResourceTestInterface resource = utils().getResourceInterface();
        activityID1 = resource.createActivity("Test1959InitialActivity");
        resource.fireEvent(TCKResourceEventX.X1, TCKResourceEventX.X1, activityID1, null);

        return result.waitForResultOrFail(utils().getTestTimeout(), "Timeout waiting for test result.", TEST_ID);
    }

    /**
     * Do all the pre-run configuration of the test.
     */
    public void setUp() throws Exception {

        TCKResourceListener resourceListener = new TCKResourceListenerImpl();
        setResourceListener(resourceListener);

        setupService(SERVICE_DU_PATH_PARAM);
    }

    private class TCKResourceListenerImpl extends BaseTCKResourceListener {
        public synchronized void onSbbMessage(TCKSbbMessage message, TCKActivityID calledActivity) throws RemoteException {

            HashMap map = (HashMap) message.getMessage();
            Boolean passed = (Boolean) map.get("Result");
            String msgString = (String) map.get("Message");

            getLog().info("Received message from SBB.");

            if (passed.booleanValue())
                result.setPassed();
            else
                result.setFailed(TEST_ID, msgString);
        }

        public void onActivityCreatedBySbb(TCKActivityID activityID) throws RemoteException {
            getLog().fine("onActivityCreatedBySbb():activityID=" + activityID);
            sbbCreatedActivityID = activityID;
        }

        public void onActivityContextInvalid(TCKActivityID activityID) throws RemoteException {
            if (!receivedOnActivityContextInvalidCall) {
                getLog().fine("onActivityContextInvalid():activityID=" + activityID + " (1st call)");
                receivedOnActivityContextInvalidCall = true;
                if (sbbCreatedActivityID == null) {
                    result.setError("Received onActivityContextInvalid() call before onActivityCreatedBySbb() call");
                } else if (!activityID.equals(sbbCreatedActivityID)) {
                    result.setError("Received onActivityContextInvalid() call after onActivityCreatedBySbb() call, but with a non-matching activity handle");
                } else {
                    try {
                        utils().getResourceInterface().fireEvent(TCKResourceEventX.X2, TCKResourceEventX.X2, activityID1, null);
                    } catch (TCKTestErrorException e) {
                        result.setError("Caught TCKTestErrorException while sending second event", e);
                    }
                }
            } else {
                getLog().fine("onActivityContextInvalid():activityID=" + activityID + " (not 1st call)");
            }

        }

        public void onException(Exception e) throws RemoteException {
            getLog().warning("Received exception from SBB");
            getLog().warning(e);
            result.setError(e);
        }

        private TCKActivityID sbbCreatedActivityID;
        private boolean receivedOnActivityContextInvalidCall;
    }

    private FutureResult result;
    private TCKActivityID activityID1;
}
