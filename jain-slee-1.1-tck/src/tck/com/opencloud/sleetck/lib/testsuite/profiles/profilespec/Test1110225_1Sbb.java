/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.profiles.profilespec;

import java.util.HashMap;

import javax.naming.Context;
import javax.slee.ActivityContextInterface;
import javax.slee.Address;
import javax.slee.RolledBackContext;
import javax.slee.TransactionRolledbackLocalException;
import javax.slee.profile.ProfileFacility;
import javax.slee.profile.ProfileTable;

import com.opencloud.sleetck.lib.resource.events.TCKResourceEventX;
import com.opencloud.sleetck.lib.sbbutils.BaseTCKSbb;
import com.opencloud.sleetck.lib.sbbutils.TCKSbbUtils;
import com.opencloud.sleetck.lib.sbbutils.events.TCKSbbEvent;
import com.opencloud.sleetck.lib.sbbutils.events.TCKSbbEventImpl;
import com.opencloud.sleetck.lib.sbbutils.events2.SbbBaseMessageComposer;

public abstract class Test1110225_1Sbb extends BaseTCKSbb {

    private void sendLogMsgCall (String msg) {
        HashMap map = SbbBaseMessageComposer.getLogMsg(msg);
        try {
            TCKSbbUtils.getResourceInterface().callTest(map);
        }
        catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }
    }

    private void sendResult(boolean result, int id, String msg, ActivityContextInterface aci) {
        HashMap map = SbbBaseMessageComposer.getSetResultMsg(result, id, msg);
        fireTCKSbbEvent(new TCKSbbEventImpl(map), aci, null);
    }

    public void onTCKResourceEventX1(TCKResourceEventX event, ActivityContextInterface aci) {
        try {
            Context env = TCKSbbUtils.getSbbEnvironment();
            ProfileFacility profileFacility = (ProfileFacility)env.lookup("slee/facilities/profile");
            ProfileTable profileTable = profileFacility.getProfileTable(Test1110225ProfileLocal.PROFILE_TABLE_NAME);
            Test1110225ProfileLocal profileLocal = (Test1110225ProfileLocal)profileTable.find(Test1110225ProfileLocal.PROFILE_NAME);

            try {
                sendLogMsgCall("Calling 'callFromTestSbb()' on profile "+Test1110225ProfileLocal.PROFILE_NAME);
                profileLocal.callFromTestSbb();
                sendLogMsgCall("Successfully called 'callFromTestSbb()' on profile "+Test1110225ProfileLocal.PROFILE_NAME);
                sendResult(true, 1110225, "Reentrant behaviour as expected", aci);
            }
            catch (TransactionRolledbackLocalException e) {
                sendLogMsgCall("Profile should have shown valid reentrant behaviour but threw exception instead: ");
            }
            catch (Exception e) {
                //make sure that whatever exception occured, the TXN is marked for rollback
                //thus sbbRolledBack is called which finally sends an ACK to the test
                getSbbContext().setRollbackOnly();
                sendLogMsgCall("No exception expected and if reentrant behaviour is faulty rather a " +
                        "TransactionRolledbackLocalException should have been thrown, however we received the following exception: "+e);
            }

        } catch(Exception e) {
            TCKSbbUtils.handleException(e);
        }
    }

    public void onTCKSbbEvent(TCKSbbEvent event, ActivityContextInterface aci) {
        try {
            HashMap payload = (HashMap)event.getMessage();
            TCKSbbUtils.getResourceInterface().sendSbbMessage(payload);

        } catch(Exception e) {
            TCKSbbUtils.handleException(e);
        }
    }

    public void sbbRolledBack(RolledBackContext context) {
        //send ACK to the TCK test and indicate that the previous transaction was rolled back
        sendResult(false, 1110225, "Transaction rolled back.", context.getActivityContextInterface());
    }

    public abstract void fireTCKSbbEvent(TCKSbbEvent event, ActivityContextInterface aci, Address address);
}
