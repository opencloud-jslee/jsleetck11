/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.runtime.security;

import com.opencloud.sleetck.lib.AbstractSleeTCKTest;
import com.opencloud.sleetck.lib.TCKTestResult;
import com.opencloud.sleetck.lib.resource.TCKActivityID;
import com.opencloud.sleetck.lib.resource.events.TCKResourceEventX;
import com.opencloud.sleetck.lib.resource.testapi.TCKResourceTestInterface;
import com.opencloud.sleetck.lib.testutils.QueuingResourceListener;

/**
 * Tests an Sbb's security access.
 */
public class SecurityPermissionsTest extends AbstractSleeTCKTest {

    public TCKTestResult run() throws Exception {
        QueuingResourceListener listener = new QueuingResourceListener(utils());
        setResourceListener(listener);
        TCKResourceTestInterface resource = utils().getResourceInterface();

        getLog().fine("Creaing activity");
        TCKActivityID activityID = resource.createActivity(getClass().getName());
        getLog().info("Firing an event");
        resource.fireEvent(TCKResourceEventX.X1,null,activityID,null);

        // Expect a message to indicate a test pass, or a TCKTestFailureException
        // to indicate a failure. nextMessage() will rethrow the failure exception if received
        getLog().fine("Waiting for a reply from the Sbb");
        Object ackMessage = listener.nextMessage().getMessage();
        getLog().info("Received ACK message from Sbb to indicate a test pass: "+ackMessage);

        return TCKTestResult.passed();
    }

}
