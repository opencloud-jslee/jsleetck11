/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.javax.slee.Sbb;

import javax.slee.ActivityEndEvent;
import javax.slee.ActivityContextInterface;
import javax.slee.SbbContext;
import javax.slee.Address;
import javax.slee.SbbLocalObject;
import javax.slee.facilities.Level;
import javax.slee.ChildRelation;
import javax.slee.CreateException;

import com.opencloud.sleetck.lib.sbbutils.BaseTCKSbb;
import com.opencloud.sleetck.lib.sbbutils.TCKSbbUtils;
import com.opencloud.sleetck.lib.sbbutils.TCKSbbConstants;
import com.opencloud.sleetck.lib.resource.sbbapi.TCKActivity;
import com.opencloud.sleetck.lib.resource.events.TCKResourceEventX;
import com.opencloud.sleetck.lib.resource.sbbapi.TCKResourceSbbInterface;

import java.util.HashMap;
import java.util.Iterator;

public abstract class Test3215Sbb extends BaseTCKSbb {

    // Override the BaseTCKSbb's version of this function which reports a test error.
    public void sbbRolledBack(javax.slee.RolledBackContext context) {
    }

    public void sbbExceptionThrown(Exception exception, Object event, ActivityContextInterface aci) {

        HashMap map = new HashMap();

        try {

            if (!exception.getClass().equals(java.lang.NullPointerException.class) || !exception.getMessage().equals("Test3215Exception")) {
                map.put("Result", new Boolean(false));
                map.put("Message", "Exception passed to sbbExceptionThrown was not the exception thrown.");
                map.put("ID", new Integer(4446));
                TCKSbbUtils.getResourceInterface().sendSbbMessage(map);
                return;
            }

            if (!getTxnID().equals(TCKSbbUtils.getResourceAdaptorInterface().getTransactionIDAccess().getCurrentTransactionID())) {
                map.put("Result", new Boolean(false));
                map.put("Message", "sbbExceptionThrown executed in different transaction to that the exception was thrown in.");
                map.put("ID", new Integer(3216));
                TCKSbbUtils.getResourceInterface().sendSbbMessage(map);
                return;
            }

            // Is the TXN marked for rollback.
            if (!getSbbContext().getRollbackOnly()) {
                map.put("Result", new Boolean(false));
                map.put("Message", "sbbExceptionThrown's transaction wasn't marked for rollback.");
                map.put("ID", new Integer(3216));
                TCKSbbUtils.getResourceInterface().sendSbbMessage(map);
                return;
            }

            if (event == null) {
                map.put("Result", new Boolean(false));
                map.put("Message", "null 'event' argument passed to sbbExceptionThrown");
                map.put("ID", new Integer(3217));
                TCKSbbUtils.getResourceInterface().sendSbbMessage(map);
                return;
            }

            if (aci == null) {
                map.put("Result", new Boolean(false));
                map.put("Message", "null 'aci' argument passed to sbbExceptionThrown");
                map.put("ID", new Integer(3218));
                TCKSbbUtils.getResourceInterface().sendSbbMessage(map);
                return;
            }

            /* Removed - Issues with ActivityContextInterface.getActivity() in sbbExceptionThrown found during PFD3 */
            /*
            if (aci.getActivity() == null || !(aci.getActivity() instanceof TCKActivity)) {
            map.put("Result", new Boolean(false));
            map.put("Message", "the aci passed to sbbExceptionThrown did not represent the "+
                        "activity on which the event was fired");
            map.put("ID", new Integer(3218));
            TCKSbbUtils.getResourceInterface().sendSbbMessage(map);
            return;
            }
            */

            // 3217 and 3218 - event and ACI data
            map.put("Type", "Data2");
            map.put("Event", event);
            map.put("Exception", exception);
            TCKSbbUtils.getResourceInterface().sendSbbMessage(map);

            // Test pass.
            map = new HashMap();
            map.put("Result", new Boolean(true));
            map.put("Message", "Ok");
            TCKSbbUtils.getResourceInterface().sendSbbMessage(map);

        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }
    }

    // Initial event
    public void onTCKResourceEventX1(TCKResourceEventX ev, ActivityContextInterface aci) {

        NullPointerException exception = new NullPointerException("Test3215Exception");

        try {
            setTxnID(TCKSbbUtils.getResourceAdaptorInterface().getTransactionIDAccess().getCurrentTransactionID());

            // Tell the test class about the event object and the activity context interface object.
            HashMap map = new HashMap();
            map.put("Type", "Data");
            map.put("Event", ev);
            map.put("Exception", exception);
            TCKSbbUtils.getResourceInterface().sendSbbMessage(map);

        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }

        throw exception;
    }

    public void onTCKResourceEventX2(TCKResourceEventX ev, ActivityContextInterface aci) {

        HashMap map = new HashMap();

        map.put("Result", new Boolean(false));
        map.put("Message", "sbbExceptionThrown was not called after an unchecked exception was thrown in event handler");

        try {
            TCKSbbUtils.getResourceInterface().sendSbbMessage(map);
        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }
    }

    public abstract void setValue(boolean val);
    public abstract boolean getValue();

    public abstract void setTxnID(Object txnID);
    public abstract Object getTxnID();

}
