/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.javax.slee.management.SleeState;

import com.opencloud.sleetck.lib.SleeTCKTest;
import com.opencloud.sleetck.lib.SleeTCKTestUtils;
import com.opencloud.sleetck.lib.TCKTestResult;

import javax.slee.management.DeployableUnitID;
import javax.slee.management.SleeState;

public class Test4144Test implements SleeTCKTest {

    private static final String SERVICE_DU_PATH_PARAM = "DUPath";

    public void init(SleeTCKTestUtils utils) {
        this.utils = utils;
    }

    /**
     * Perform the actual test.
     */

    public TCKTestResult run() throws Exception {

        if (SleeState.fromInt(SleeState.SLEE_STARTING).toInt() != SleeState.SLEE_STARTING)
            return TCKTestResult.failed(4146, "SleeState.to/fromInt() not working correctly.");

        if (SleeState.fromInt(SleeState.SLEE_STARTING).equals(SleeState.fromInt(SleeState.SLEE_STOPPING)))
            return TCKTestResult.failed(4157, "SleeState.equals(SleeState) returned true for different states.");

        if (!SleeState.fromInt(SleeState.SLEE_STARTING).equals(SleeState.fromInt(SleeState.SLEE_STARTING)))
            return TCKTestResult.failed(4157, "SleeState.equals(SleeState) returned false for the same states.");

        try {
            SleeState.STARTING.hashCode();
        } catch (Exception e) {
            return TCKTestResult.failed(4159, "SleeState.hashCode() threw an exception.");
        }

        try {
            SleeState.STARTING.toString();
        } catch (Exception e) {
            return TCKTestResult.failed(4161, "SleeState.toString() threw an exception.");
        }
        
        if (SleeState.STARTING.toString() == null)
            return TCKTestResult.failed(4161, "SleeState.toString() returned null.");


        return TCKTestResult.passed();
    }

    /**
     * Do all the pre-run configuration of the test.
     */

    public void setUp() throws Exception {

        // Install the Deployable Units
        String duPath = utils.getTestParams().getProperty(SERVICE_DU_PATH_PARAM);
        duID = utils.install(duPath);

        // Activate the DU
        utils.activateServices(duID, true);

    }

    /**
     * Clean up after the test.
     */

    public void tearDown() throws Exception {
        utils.getLog().fine("Disconnecting from resource");
        utils.getResourceInterface().clearActivities();
        utils.getResourceInterface().removeResourceListener();
        utils.getLog().fine("Deactivating and uninstalling service");
        utils.deactivateAllServices();
        utils.uninstallAll();
    }

    private SleeTCKTestUtils utils;
    private DeployableUnitID duID;
}
