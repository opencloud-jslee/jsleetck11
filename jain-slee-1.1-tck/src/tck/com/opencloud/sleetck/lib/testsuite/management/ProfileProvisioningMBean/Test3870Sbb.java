/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.management.ProfileProvisioningMBean;

import com.opencloud.sleetck.lib.resource.events.TCKResourceEventX;
import com.opencloud.sleetck.lib.sbbutils.BaseTCKSbb;

import javax.slee.ActivityContextInterface;

public abstract class Test3870Sbb extends BaseTCKSbb {

    public void onTCKResourceEventX1(TCKResourceEventX ev, ActivityContextInterface aci) {
    }

    public abstract Test3870ProfileCMP getProfileCMP(javax.slee.profile.ProfileID id) throws javax.slee.profile.UnrecognizedProfileTableNameException, javax.slee.profile.UnrecognizedProfileNameException;  

}
