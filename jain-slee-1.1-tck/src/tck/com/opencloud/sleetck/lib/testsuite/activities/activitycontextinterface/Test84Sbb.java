/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.activities.activitycontextinterface;

import javax.slee.ActivityEndEvent;
import javax.slee.ActivityContextInterface;
import javax.slee.SbbContext;
import javax.slee.ChildRelation;
import javax.slee.Address;
import javax.slee.SbbLocalObject;

import com.opencloud.sleetck.lib.sbbutils.BaseTCKSbb;
import com.opencloud.sleetck.lib.sbbutils.TCKSbbUtils;
import com.opencloud.sleetck.lib.sbbutils.TCKSbbConstants;
import com.opencloud.sleetck.lib.resource.events.TCKResourceEventX;
import com.opencloud.sleetck.lib.resource.TCKActivityID;
import com.opencloud.sleetck.lib.resource.sbbapi.TCKResourceSbbInterface;
import com.opencloud.sleetck.lib.resource.sbbapi.TCKActivityContextInterfaceFactory;

import java.util.HashMap;

public abstract class Test84Sbb extends BaseTCKSbb {

    public void onTCKResourceEventX1(TCKResourceEventX event, ActivityContextInterface aci) {
        try {

            Test84ChildSbbLocalObject firstChild = (Test84ChildSbbLocalObject) getChildRelation().create();
            Test84ChildSbbLocalObject secondChild = (Test84ChildSbbLocalObject) getChildRelation().create();

            firstChild.setSbbPriority((byte) 0); // Standard priority
            secondChild.setSbbPriority((byte) 127); // Maximum priority

            firstChild.setChildNumber(0);
            secondChild.setChildNumber(1);

            aci.attach(firstChild);
            aci.attach(secondChild);

            fireTest84Event(new Test84Event(), aci, null);

        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }
    }

    // Event fire method.
    public abstract void fireTest84Event(Test84Event event, ActivityContextInterface aci, Address address);

    // getChildRelation method.
    public abstract ChildRelation getChildRelation();

}
