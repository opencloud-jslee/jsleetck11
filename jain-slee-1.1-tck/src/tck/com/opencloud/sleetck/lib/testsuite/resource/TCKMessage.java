/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.resource;

import java.io.Serializable;

import com.opencloud.sleetck.lib.rautils.UOID;

/**
 * Base message class used for sending information between the TCK and a
 * deployed test component.
 */
public class TCKMessage implements Serializable {
    public TCKMessage(UOID uid, int sequenceID, int methodID, Object argument) {
        this.uid = uid;
        this.methodID = methodID;
        this.sequenceID = sequenceID;
        this.argument = argument;
    }

    public TCKMessage(UOID uid, int methodID, Object argument) {
        this(uid, -1, methodID, argument);
    }

    //
    // Getters
    //

    public final UOID getUID() {
        return uid;
    }

    public final int getSequenceID() {
        return sequenceID;
    }

    public final int getMethod() {
        return methodID;
    }

    public final Object getArgument() {
        return argument;
    }

    // Convenience method

    public String getMethodName() {
        return RAMethods.getMethodName(methodID);
    }

    //
    // Object
    //

    public String toString() {
        StringBuffer buf = new StringBuffer("TCKMessage[");
        buf.append("uid=").append(uid).append(",");
        buf.append("sequenceID=").append(sequenceID).append(",");
        buf.append("method=").append(getMethodName()).append(",");
        buf.append("argument=").append(argument);
        buf.append("]");
        return buf.toString();
    }

    //
    // Private
    //

    private final UOID uid;
    private final int sequenceID;
    private final int methodID;
    private final Object argument;
}
