/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.management.ServiceManagementMBean;

import com.opencloud.sleetck.lib.AbstractSleeTCKTest;
import com.opencloud.sleetck.lib.DescriptionKeys;
import com.opencloud.sleetck.lib.OperationTimedOutException;
import com.opencloud.sleetck.lib.TCKTestResult;
import com.opencloud.sleetck.lib.testutils.jmx.DeploymentMBeanProxy;
import com.opencloud.sleetck.lib.testutils.jmx.ServiceManagementMBeanProxy;

import javax.slee.ComponentID;
import javax.slee.InvalidArgumentException;
import javax.slee.InvalidStateException;
import javax.slee.ServiceID;
import javax.slee.management.DeployableUnitDescriptor;
import javax.slee.management.DeployableUnitID;
import javax.slee.management.ServiceState;

public class Test2270Test extends AbstractSleeTCKTest {

    private static final int TEST_ID = 2270;

    /**
     * Perform the actual test.
     */
    public TCKTestResult run() throws Exception {

        DeploymentMBeanProxy duProxy = utils().getDeploymentMBeanProxy();
        serviceProxy = utils().getMBeanProxyFactory().createServiceManagementMBeanProxy(utils().getSleeManagementMBeanProxy().getServiceManagementMBean());
        DeployableUnitDescriptor duDesc = duProxy.getDescriptor(duID);
        ComponentID components[] = duDesc.getComponents();

        services = new ServiceID[2];
        int index = 0;
        for (int i = 0; i < components.length; i++) {
            if (components[i] instanceof ServiceID) {
                services[index++] = (ServiceID) components[i];
            }
        }

        getLog().fine("Setting up test.");

        for (int i = 0; i < services.length; i++) {
            // Verify that the Service is in the INACTIVE state.
            if (!serviceProxy.getState(services[i]).isInactive()) {
                return TCKTestResult.failed(TEST_ID, "Services are not in the inactive state; cannot continue with test.");
            }
        }

        try {
            serviceProxy.activate(services[0]);
        } catch (InvalidStateException e) {
            return TCKTestResult.error(e);
        } catch (Exception e) {
            utils().getLog().fine("Exception thrown: " + e);
            return TCKTestResult.error(e);
        }


        if (!serviceProxy.getState(services[0]).isActive()) {
            return TCKTestResult.error("Failed to activate service to start test, cannot proceed.");
        }
        if (serviceProxy.getState(services[1]).isActive()) {
            return TCKTestResult.error("Test start conditions were not met.");
        }

        getLog().fine("Testing assertion 2270 - deactivateAndActivate(valid services in the right states.");

        // deactivateAndActivate (ServiceID []) - 2270
        try {
            serviceProxy.deactivateAndActivate(new ServiceID[] { services[0] }, new ServiceID[] { services[1] });
        } catch (Exception e) {
            return TCKTestResult.error(e);
        }

        if (!serviceProxy.getState(services[1]).isActive()) {
            return TCKTestResult.failed(2270, "Service not in the ACTIVE state after deactivateAndActivate().");
        }
        if (serviceProxy.getState(services[0]).isActive()) {
            return TCKTestResult.failed(2270, "Service not in the INACTIVE/STOPPING state after deactivateAndActivate().");
        }

        getLog().fine("Verifying that services[0] is in the INACTIVE state before proceeding.");

        // Wait for this service to enter the INACTIVE (vs STOPPING) state.
        waitForStateChange(services[0], ServiceState.INACTIVE);

        getLog().fine("Calling deactivateAndActivate() with valid services.");

        // deactivateAndActivate (ServiceID) - 2270
        try {
            serviceProxy.deactivateAndActivate(services[1], services[0]);
        } catch (Exception e) {
            return TCKTestResult.error(e);
        }

        if (!serviceProxy.getState(services[0]).isActive()) {
            return TCKTestResult.failed(2270, "Service not in the ACTIVE state after deactivateAndActivate().");
        }
        if (serviceProxy.getState(services[1]).isActive()) {
            return TCKTestResult.failed(2270, "Service not in the INACTIVE/STOPPING state after deactivateAndActivate().");
        }

        getLog().fine("Waiting for services[1] to enter the INACTIVE state.");

        // Wait for this service to enter the INACTIVE (vs STOPPING) state.
        waitForStateChange(services[1], ServiceState.INACTIVE);

        getLog().fine("Calling deactivateAndActivate(services[1], services[0])");

        // 2271 - deactivateAndActivate() with services in the wrong state - 6 tests

        boolean passed = false;
        try {
            serviceProxy.deactivateAndActivate(services[1], services[0]);
        } catch (InvalidStateException e) {
            passed = true;
        } catch (Exception e) {
            utils().getLog().fine("Exception thrown: " + e);
            return TCKTestResult.error(e);
        }

        if (!passed)
            return TCKTestResult.failed(2273, "ServiceManagementMBean.deactivateAndActivate() should have thrown InvalidStateException when arg0 and arg1 correspond to services in the wrong state.");


        getLog().fine("Calling deactivateAndActivate(services[1], services[1])");

        passed = false;
        try {
            serviceProxy.deactivateAndActivate(services[1], services[1]);
        } catch (InvalidArgumentException e) {
            passed = true;
        } catch (InvalidStateException e) {
            passed = true;
        } catch (Exception e) {
            utils().getLog().fine("Exception thrown: " + e);
            return TCKTestResult.error(e);
        }

        if (!passed)
            return TCKTestResult.failed(2478, "ServiceManagementMBean.deactivateAndActivate() should have thrown InvalidStateException when arg0 corresponds to a service in the wrong state, or InvalidArgumentException should have been thrown as the same service was requested to be deactivated and activated.");


        getLog().fine("Calling deactivateAndActivate(services[0], services[0])");

        passed = false;
        try {
            serviceProxy.deactivateAndActivate(services[0], services[0]);
        } catch (InvalidArgumentException e) {
            passed = true;
        } catch (InvalidStateException e) {
            passed = true;
        } catch (Exception e) {
            utils().getLog().fine("Exception thrown: " + e);
            return TCKTestResult.error(e);
        }

        if (!passed)
            return TCKTestResult.failed(2478, "ServiceManagementMBean.deactivateAndActivate() should have thrown InvalidStateException when arg1 corresponds to a service in the wrong state, or InvalidArgumentException should have been thrown as the same service was requested to be deactivated and activated.");

        getLog().fine("Calling deactivateAndActivate({services[1]}, {services[0]})");

        passed = false;
        try {
            serviceProxy.deactivateAndActivate(new ServiceID [] {services[1]}, new ServiceID[] {services[0]});
        } catch (InvalidStateException e) {
            passed = true;
        } catch (Exception e) {
            utils().getLog().fine("Exception thrown: " + e);
            return TCKTestResult.error(e);
        }

        if (!passed)
            return TCKTestResult.failed(2273, "ServiceManagementMBean.deactivateAndActivate() should have thrown InvalidStateException when arg0 and arg1 correspond to services in the wrong state.");

        getLog().fine("Calling deactivateAndActivate({services[1]}, {services[1]})");

        passed = false;
        try {
            serviceProxy.deactivateAndActivate(new ServiceID [] {services[1]}, new ServiceID[] {services[1]});
        } catch (InvalidArgumentException e) {
            passed = true;
        } catch (InvalidStateException e) {
            passed = true;
        } catch (Exception e) {
            utils().getLog().fine("Exception thrown: " + e);
            return TCKTestResult.error(e);
        }

        if (!passed)
            return TCKTestResult.failed(2273, "ServiceManagementMBean.deactivateAndActivate() should have thrown InvalidStateException when arg0 corresponds to a service in the wrong state, or InvalidArgumentException should have been thrown as the same service was requested to be deactivated and activated.");

        getLog().fine("Calling deactivateAndActivate({services[0]}, {services[0]})");
        passed = false;
        try {
            serviceProxy.deactivateAndActivate(new ServiceID [] {services[0]}, new ServiceID[] {services[0]});
        } catch (InvalidArgumentException e) {
            passed = true;
        } catch (InvalidStateException e) {
            passed = true;
        } catch (Exception e) {
            utils().getLog().fine("Exception thrown: " + e);
            return TCKTestResult.error(e);
        }

        if (!passed)
            return TCKTestResult.failed(2273, "ServiceManagementMBean.deactivateAndActivate() should have thrown InvalidStateException when arg1 corresponds to a service in the wrong state, or InvalidArgumentException should have been thrown as the same service was requested to be deactivated and activated.");


        getLog().fine("Calling deactivateAndActivate({}, {services[1]})");

        passed = false;
        try {
            serviceProxy.deactivateAndActivate(new ServiceID [] {}, new ServiceID [] { services[1]});
        } catch (InvalidArgumentException e) {
            passed = true;
        } catch (Exception e) {
            utils().getLog().fine("Exception thrown: " + e);
            return TCKTestResult.error(e);
        }

        if (!passed)
            return TCKTestResult.failed(2473, "ServiceManagementMBean.deactivateAndActivate(new ServiceID[] {}, new ServiceID[] {service}) should have thrown InvalidArgumentException.");


        getLog().fine("Calling deactivateAndActivate({services[0]}, {})");
        passed = false;
        try {
            serviceProxy.deactivateAndActivate(new ServiceID [] {services[0]}, new ServiceID [] {});
        } catch (InvalidArgumentException e) {
            passed = true;
        } catch (Exception e) {
            utils().getLog().fine("Exception thrown: " + e);
            return TCKTestResult.error(e);
        }

        if (!passed)
            return TCKTestResult.failed(2473, "ServiceManagementMBean.deactivateAndActivate(new ServiceID[] {service}, new ServiceID[] {}) should have thrown InvalidArgumentException.");


        getLog().fine("Calling deactivateAndActivate({null}, {services[1]})");
        passed = false;
        try {
            serviceProxy.deactivateAndActivate(new ServiceID [] {null}, new ServiceID [] { services[1]});
        } catch (InvalidArgumentException e) {
            passed = true;
        } catch (Exception e) {
            utils().getLog().fine("Exception thrown: " + e);
            return TCKTestResult.error(e);
        }

        if (!passed)
            return TCKTestResult.failed(2473, "ServiceManagementMBean.deactivateAndActivate(new ServiceID[] {null}, new ServiceID[] {service}) should have thrown InvalidArgumentException.");

        getLog().fine("Calling deactivateAndActivate({services[0]}, {null})");
        passed = false;
        try {
            serviceProxy.deactivateAndActivate(new ServiceID [] {services[0]}, new ServiceID [] {null});
        } catch (InvalidArgumentException e) {
            passed = true;
        } catch (Exception e) {
            utils().getLog().fine("Exception thrown: " + e);
            return TCKTestResult.error(e);
        }

        if (!passed)
            return TCKTestResult.failed(2473, "ServiceManagementMBean.deactivateAndActivate(new ServiceID[] {service}, new ServiceID[] {null}) should have thrown InvalidArgumentException.");


        getLog().fine("Calling deactivateAndActivate({services[0], services[0]}, {services[1]})");

        passed = false;
        try {
            serviceProxy.deactivateAndActivate(new ServiceID [] {services[0], services[0]}, new ServiceID [] { services[1]});
        } catch (InvalidArgumentException e) {
            passed = true;
        } catch (Exception e) {
            utils().getLog().fine("Exception thrown: " + e);
            return TCKTestResult.error(e);
        }

        if (!passed)
            return TCKTestResult.failed(2473, "ServiceManagementMBean.deactivateAndActivate(new ServiceID[] {service2, service2}, new ServiceID[] {service}) should have thrown InvalidArgumentException.");


        getLog().fine("Calling deactivateAndActivate({services[0]}, {services[1], services[1]})");
        passed = false;
        try {
            serviceProxy.deactivateAndActivate(new ServiceID [] {services[0]}, new ServiceID [] {services[1], services[1]});
        } catch (InvalidArgumentException e) {
            passed = true;
        } catch (Exception e) {
            utils().getLog().fine("Exception thrown: " + e);
            return TCKTestResult.error(e);
        }

        if (!passed)
            return TCKTestResult.failed(2473, "ServiceManagementMBean.deactivateAndActivate(new ServiceID[] {service}, new ServiceID[] {service2, service2}) should have thrown InvalidArgumentException.");


        return TCKTestResult.passed();
    }

    private void waitForStateChange(ServiceID serviceID, ServiceState expectedState) throws Exception {

        int count = 0;

        while (true) {
            if (serviceProxy.getState(serviceID).equals(expectedState))
                return;

            if (count > 10)
                throw new OperationTimedOutException("Timeout waiting for service " + serviceID + " to enter state " + expectedState);

            // Don't catch the InterruptedException here - let it propagate.
            Thread.sleep(500);
            count++;
        }
    }

    public void setUp() throws Exception {
        getLog().fine("Installing the service");
        String duPath = utils().getTestParams().getProperty(DescriptionKeys.SERVICE_DU_PATH_PARAM);
        duID = utils().install(duPath);
    }

    public void tearDown() throws Exception {
        getLog().fine("Deactivating services");
        try {
            if (serviceProxy.getState(services[0]).isActive())
                serviceProxy.deactivate(services[0]);
        } catch (Exception e) {}
        try {
            if (serviceProxy.getState(services[1]).isActive())
                serviceProxy.deactivate(services[1]);
        } catch (Exception e) {}
        super.tearDown();
    }

    private ServiceManagementMBeanProxy serviceProxy;
    private DeployableUnitID duID;
    private ServiceID services[];

}
