/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.profiles.profileabstractclass;

import java.util.Enumeration;
import java.util.Vector;

import javax.slee.management.ManagementException;
import javax.slee.profile.ProfileSpecificationID;
import javax.slee.profile.UnrecognizedProfileTableNameException;
import javax.transaction.RollbackException;

import com.opencloud.sleetck.lib.AbstractSleeTCKTest;
import com.opencloud.sleetck.lib.TCKTestResult;
import com.opencloud.sleetck.lib.infra.SleeTCKComponentConstants;
import com.opencloud.sleetck.lib.testutils.ProfileUtils;
import com.opencloud.sleetck.lib.testutils.jmx.ProfileProvisioningMBeanProxy;


public class Test1110246Test extends AbstractSleeTCKTest {

    private static final String DU_PATH_PARAM= "DUPath";

    private static final String SPEC_NAME = "Test1110246Profile";
    private static final String SPEC_VERSION = "1.0";

    private static final String PROFILE_TABLE_NAME = "Test1110246ProfileTable";

    /**
     *
     */
    public TCKTestResult run() throws Exception {

        //create a profile table
        ProfileSpecificationID specID = new ProfileSpecificationID(SPEC_NAME, SleeTCKComponentConstants.TCK_VENDOR, SPEC_VERSION);

        //1110246: The SLEE invokes the profileInitialize() method with a transaction context.
        try {
            profileProvisioning.createProfileTable(specID, PROFILE_TABLE_NAME);
            tablesCreated.add(PROFILE_TABLE_NAME);
            return TCKTestResult.failed(1110246, "profileInitialize() method should be called in a transactional context." +
                    " The implementation trys to mark this transaction for rollback thus causing a " +
                    "RollbackException to be thrown to the management client.");
        }
        catch (ManagementException e) {
            if (e.getCause() instanceof RollbackException)
                getLog().fine("Caught expected exception: javax.slee.management.ManagementException with cause: javax.transaction.RollbackException");
            else
                return TCKTestResult.error("Received Management exception but cause is of wrong type: "+e.getCause().getClass().getName()
                        +" javax.transaction.RollbackException expected");
        }
        catch (Exception e) {
            return TCKTestResult.error("Wrong type of exception received: "+e.getClass().getName()
                    +" profileInitialize() trys to mark the transaction it is called in for rollback thus causing a " +
                    "RollbackException to be thrown to the management client.");
        }

        //11101121: The default Profile is created in the same transaction context as
        //the profileInitialize method.
        try {
            profileProvisioning.getDefaultProfile(PROFILE_TABLE_NAME);
            return TCKTestResult.failed(11101121, "Default profile obtained successfully though" +
                    " it should not exist as profileInitialize() TXN context was rolled back.");
        }
        catch (Exception e) {
            getLog().fine("Caught expected exception: "+e);
        }



        return TCKTestResult.passed();
    }

    public void setUp() throws Exception {

        tablesCreated = new Vector();

        setupService(DU_PATH_PARAM);

        profileUtils = new ProfileUtils(utils());
        profileProvisioning = profileUtils.getProfileProvisioningProxy();

    }

    public void tearDown() throws Exception {

        try {
            Enumeration en = tablesCreated.elements();
            while (en.hasMoreElements()) {
                profileUtils.removeProfileTable((String)en.nextElement());
            }
        }
        catch (Exception e) {
            getLog().warning("Caught exception while trying to remove profile table:");
            getLog().warning(e);
        }

        super.tearDown();
    }

    private ProfileUtils profileUtils;
    private Vector tablesCreated;
    private ProfileProvisioningMBeanProxy profileProvisioning;
}
