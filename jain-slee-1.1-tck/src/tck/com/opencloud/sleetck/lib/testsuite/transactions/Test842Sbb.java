/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.transactions;

import javax.slee.ActivityEndEvent;
import javax.slee.ActivityContextInterface;
import javax.slee.SbbContext;
import javax.slee.Address;
import javax.slee.SbbLocalObject;
import javax.slee.facilities.Level;
import javax.slee.ChildRelation;
import javax.slee.CreateException;

import com.opencloud.sleetck.lib.TCKTestErrorException;
import com.opencloud.sleetck.lib.sbbutils.BaseTCKSbb;
import com.opencloud.sleetck.lib.sbbutils.TCKSbbUtils;
import com.opencloud.sleetck.lib.sbbutils.TCKSbbConstants;
import com.opencloud.sleetck.lib.resource.events.TCKResourceEventX;
import com.opencloud.sleetck.lib.resource.events.TCKResourceEventY;
import com.opencloud.sleetck.lib.resource.sbbapi.TCKResourceSbbInterface;

import java.util.HashMap;
import java.util.Iterator;

public abstract class Test842Sbb extends BaseTCKSbb {

    // Initial event
    public void onTCKResourceEventX1(TCKResourceEventX ev, ActivityContextInterface aci) {
        try {
            // Create child and attach to the ACI.
            aci.attach(getChildRelation().create());
        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }

        fireTest842Event(new Test842Event(), aci, null);
        fireTest842SecondEvent(new Test842SecondEvent(), aci, null);
    }

    public void onTest842Event(Test842Event event, ActivityContextInterface aci) {
        try {
            Object currentTransaction = TCKSbbUtils.getResourceAdaptorInterface().getTransactionIDAccess().getCurrentTransactionID();
            Test842ChildSbbLocalObject child = (Test842ChildSbbLocalObject) getChildRelation().iterator().next();
            child.addTxnID(currentTransaction);
        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }

    }

    public void onTest842SecondEvent(Test842SecondEvent event, ActivityContextInterface aci) {
        try {
            Object currentTransaction = TCKSbbUtils.getResourceAdaptorInterface().getTransactionIDAccess().getCurrentTransactionID();
            Test842ChildSbbLocalObject child = (Test842ChildSbbLocalObject) getChildRelation().iterator().next();
            child.addTxnID(currentTransaction);
        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }
    }

    public abstract void fireTest842Event(Test842Event event, ActivityContextInterface aci, Address address);
    public abstract void fireTest842SecondEvent(Test842SecondEvent event, ActivityContextInterface aci, Address address);

    public abstract ChildRelation getChildRelation();

}
