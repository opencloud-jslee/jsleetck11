/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.facilities.alarmfacility;

import java.util.HashMap;

import javax.naming.InitialContext;
import javax.slee.ActivityContextInterface;
import javax.slee.facilities.AlarmFacility;
import javax.slee.facilities.AlarmLevel;
import javax.slee.facilities.Tracer;

import com.opencloud.sleetck.lib.resource.events.TCKResourceEventY;
import com.opencloud.sleetck.lib.sbbutils.BaseTCKSbb;
import com.opencloud.sleetck.lib.sbbutils.TCKSbbUtils;

/*  
 *  AssertionID(1113514): Test If one or more alarms are cleared by 
 *  this method, the SLEE’s AlarmMBean object emits an alarm notification 
 *  for each cleared alarm with the alarm level set to AlarmLevel.CLEAR.
 *    
 */

public abstract class Test1113514Sbb2 extends BaseTCKSbb {

    public static final String ALARM_MESSAGE_CRITICAL = "Sbb2:CRITICAL:Test111514AlarmMessage";

    public static final String ALARM_MESSAGE_MAJOR = "Sbb2:MAJOR:Test111514AlarmMessage";

    public static final String ALARM_MESSAGE_WARNING = "Sbb2:WARNING:Test111514AlarmMessage";

    public static final String ALARM_MESSAGE_INDETERMINATE = "Sbb2:INDETERMINATE:Test111514AlarmMessage";

    public static final String ALARM_MESSAGE_MINOR = "Sbb2:MINOR:Test111514AlarmMessage";

    public static final String ALARM_INSTANCEID = "Sbb2:Test1113514AlarmInstanceID";

    public static final AlarmLevel ALARM_LEVEL = AlarmLevel.MAJOR;

    // Initial event
    public void onTCKResourceEventY1(TCKResourceEventY ev, ActivityContextInterface aci) {

        try {
            tracer = this.getSbbContext().getTracer("com.test");
            tracer.info("Received " + ev + " message", null);

            doTest1113514Test();
            
        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }
    }

    private void doTest1113514Test() throws Exception {

        AlarmFacility facility = getAlarmFacility();

        // 1113514
        try {
            // raise an alarm
            String[] alarmIDs = {
                    facility.raiseAlarm("javax.slee.management.Alarm6", "Test1113514AlarmInstanceID6",
                            AlarmLevel.CRITICAL, ALARM_MESSAGE_CRITICAL),
                    facility.raiseAlarm("javax.slee.management.Alarm7", "Test1113514AlarmInstanceID7",
                            AlarmLevel.MAJOR, ALARM_MESSAGE_MAJOR),
                    facility.raiseAlarm("javax.slee.management.Alarm8", "Test1113514AlarmInstanceID8",
                            AlarmLevel.WARNING, ALARM_MESSAGE_WARNING),
                    facility.raiseAlarm("javax.slee.management.Alarm9", "Test1113514AlarmInstanceID9",
                            AlarmLevel.INDETERMINATE, ALARM_MESSAGE_INDETERMINATE),
                    facility.raiseAlarm("javax.slee.management.Alarm10", "Test1113514AlarmInstanceID10",
                            AlarmLevel.MINOR, ALARM_MESSAGE_MINOR) };
            // clear the alarm has been raised
            if (facility.clearAlarms() != alarmIDs.length) {
                sendResultToTCK("Test1113514Test", false, 1113514, "Sbb2: The return value of this facility.clearAlarms() method "
                        + "didn't match the number of alarms that supposed to be cleared!"
                        + "AlarmFacility.clearAlarms()= " + facility.clearAlarms() + "vs Total alarms= "
                        + alarmIDs.length);
                return;
            }
            
        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }

    }

    private void sendResultToTCK(String testName, boolean result, int failedAssertionID,  String message) throws Exception {
        HashMap sbbData = new HashMap();
        sbbData.put("testname", testName);
        sbbData.put("result", result ? "pass" : "fail");
        sbbData.put("message", message);
        sbbData.put("id", new Integer(failedAssertionID));
        TCKSbbUtils.getResourceInterface().sendSbbMessage(sbbData);
    }


    private AlarmFacility getAlarmFacility() throws Exception {
        AlarmFacility facility = null;
        String JNDI_ALARMFACILITY_NAME = AlarmFacility.JNDI_NAME;
        try {
            facility = (AlarmFacility) new InitialContext().lookup(JNDI_ALARMFACILITY_NAME);
        } catch (Exception e) {
            tracer.warning("got unexpected Exception: " + e, null);
        }
        return facility;
    }

    private Tracer tracer;

}
