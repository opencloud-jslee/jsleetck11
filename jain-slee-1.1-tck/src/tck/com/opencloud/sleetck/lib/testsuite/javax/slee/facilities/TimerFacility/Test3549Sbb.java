/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.javax.slee.facilities.TimerFacility;

import com.opencloud.sleetck.lib.resource.events.TCKResourceEventX;
import com.opencloud.sleetck.lib.sbbutils.BaseTCKSbb;
import com.opencloud.sleetck.lib.sbbutils.TCKSbbUtils;

import javax.naming.InitialContext;
import javax.slee.ActivityContextInterface;
import javax.slee.facilities.*;
import java.util.HashMap;

public abstract class Test3549Sbb extends BaseTCKSbb {

    private static final String JNDI_TIMERFACILITY_NAME = "java:comp/env/slee/facilities/timer";
    private static final int TIMEOUT = 1000;

    public void sbbRolledBack(javax.slee.RolledBackContext context) {
    }

    // Initial event
    public void onTCKResourceEventX1(TCKResourceEventX ev, ActivityContextInterface aci) {

        long currentTime = System.currentTimeMillis();

        try {
            TimerOptions options = new TimerOptions();
            options.setTimeout(TIMEOUT);
            options.setPreserveMissed(TimerPreserveMissed.ALL);

            TimerFacility facility = (TimerFacility) new InitialContext().lookup(JNDI_TIMERFACILITY_NAME);

            setFirstTimerID(facility.setTimer(aci, null, currentTime, options));
            setSecondTimerID(facility.setTimer(aci, null, System.currentTimeMillis(), TIMEOUT + 1, 0, options));

            // Rollback the TXN
            getSbbContext().setRollbackOnly();

            try {
                HashMap map = new HashMap();
                map.put("Type", "RolledBack");
                TCKSbbUtils.getResourceInterface().sendSbbMessage(map);
                return;
            } catch (Exception e) {
                TCKSbbUtils.handleException(e);
            }


        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }

    }

    public void onTimerEvent(TimerEvent event, ActivityContextInterface aci) {

        try {
            HashMap map = new HashMap();
            map.put("Type", "TimerSeen");

            if (event.getTimerID().equals(getFirstTimerID()))
                map.put("ID", new Integer(3549));
            else
                map.put("ID", new Integer(3536));

            TCKSbbUtils.getResourceInterface().sendSbbMessage(map);
            return;
        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }
    }

    public abstract void setFirstTimerID(TimerID timerID);
    public abstract void setSecondTimerID(TimerID timerID);

    public abstract TimerID getFirstTimerID();
    public abstract TimerID getSecondTimerID();

}

