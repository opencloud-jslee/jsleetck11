/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.resource.endpoint;

import com.opencloud.sleetck.lib.TCKTestErrorException;
import com.opencloud.sleetck.lib.TCKTestResult;
import com.opencloud.sleetck.lib.testsuite.resource.BaseResourceTest;
import com.opencloud.sleetck.lib.testsuite.resource.RAMethods;

/**
 * Test to ensure that fireEventTransacted() is throwing ActivityIsEndingException
 * correctly.
 * <p>
 * Test assertion: 1115316
 */
public class Test1115316Test extends BaseResourceTest {

    private final static int ASSERTION_ID = 1115316;

    public TCKTestResult run() throws Exception {
        int sequenceID = nextMessageID();

        MultiResponseListener listener = new MultiResponseListener(sequenceID);
        listener.addExpectedResult("result1");
        listener.addExpectedResult("result2");
        listener.addExpectedResult("result3");

        sendMessage(RAMethods.fireEventTransacted, new Integer(ASSERTION_ID), listener, sequenceID);

        Object result1 = listener.getResult("result1");
        Object result2 = listener.getResult("result2");
        Object result3 = listener.getResult("result3");

        if (result1 == null)
            throw new TCKTestErrorException("Test timed out while waiting for initial response from test resource adaptor component");
        if (result2 == null)
            throw new TCKTestErrorException("Test timed out while waiting for response from test sbb component");
        if (result3 == null)
            throw new TCKTestErrorException("Test timed out while waiting for final response from test resource adaptor component");

        checkResult(result1, ASSERTION_ID);
        checkResult(result2, ASSERTION_ID);
        checkResult(result3, ASSERTION_ID);

        return TCKTestResult.passed();
    }
}
