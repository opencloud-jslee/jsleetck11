/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.javax.slee.profile.ProfileContext;

import javax.slee.CreateException;
import javax.slee.facilities.Tracer;
import javax.slee.profile.Profile;
import javax.slee.profile.ProfileContext;
import javax.slee.profile.ProfileVerificationException;
import javax.slee.profile.UnrecognizedProfileTableNameException;

import com.opencloud.logging.Logable;
import com.opencloud.logging.StdErrLog;
import com.opencloud.sleetck.lib.profileutils.BaseMessageSender;
import com.opencloud.sleetck.lib.rautils.RMIObjectChannel;
import com.opencloud.sleetck.lib.rautils.TCKRAUtils;

/**
 * The profile spec's Profile abstract class.
 */
public abstract class Test1110310Profile implements ProfileContextTestsProfileCMP, Profile, Test1110310ProfileManagement  {

    public static final String PROFILE_TABLE_NAME = "Test1110310ProfileTable";
    public static final String PROFILE_NAME = "Test1110310Profile";
    public static final String TRACE_SUCCESS = "TraceSuccessMsg";

    public Test1110310Profile() {
        Logable log = new StdErrLog();

        try {
            out = TCKRAUtils.lookupRMIObjectChannel();
            msgSender = new BaseMessageSender(out, log);
        }
        catch (Exception e) {
            log.error("An error occured creating an RMIObjectChannel:");
            log.error(e);
        }

    }

    public void setProfileContext(ProfileContext context) {
        context.getTracer("setProfileContext").info("setProfileContext called");

        //Sort of self-evident but note it down here formally however
        //1110310: The ProfileContext object implements the javax.slee.profile.ProfileContext interface.
        try {
            javax.slee.profile.ProfileContext testcontext = (javax.slee.profile.ProfileContext)context;
            msgSender.sendSuccess(1110310, "ProfileContext object implements javax.slee.profile.ProfileContext interface.");
        }
        catch (ClassCastException e) {
            msgSender.sendFailure( 1110310, "ProfileContext object does NOT implement javax.slee.profile.ProfileContext interface: "+e);
        }

        this.context = context;

        //1110311: The getProfileTableName method returns the name of the Profile Table
        //which the Profile Context is associated with.
        if (!context.getProfileTableName().equals(PROFILE_TABLE_NAME))
            msgSender.sendFailure( 1110311, "getProfileTableName did not return "+PROFILE_TABLE_NAME+" as it should have done.");
        else
            msgSender.sendSuccess( 1110311, "getProfileTableName returned "+PROFILE_TABLE_NAME+" as expected.");

        //1110315: getProfileName() method may only be invoked during a profilePostCreate method
        //invocation, or during any method invocation on a Profile object while the
        //Profile object is in the Ready state. Otherwise a java.lang.IllegalStateException
        //is thrown.
        try {
            context.getProfileName();
            msgSender.sendFailure( 1110315, "Calling getProfileName in setProfileContext should have resulted in an exception being thrown.");
        }
        catch (IllegalStateException e) {
            msgSender.sendSuccess( 1110315, "Calling getProfileName in setProfileContext threw exception as expected: "+e);
        }
        catch (Exception e) {
            msgSender.sendFailure( 1110315, "Wrong type of exception: "+e.getClass().getName()+" Expected: java.lang.IllegalStateException");
        }

        //1110319, 1110320: getProfileTable returns ProfileTable object, this can be cast to the
        //profile specs ProfileTable object type
        try {
            ProfileContextTestsProfileTable profileTable = (ProfileContextTestsProfileTable)context.getProfileTable();
            msgSender.sendSuccess( 1110319, "getProfileTable called in setProfileContext returns object that can be successfully cast into the profile specs ProfileTable type.");
        }
        catch (ClassCastException e) {
            msgSender.sendFailure( 1110319, "getProfileTable called in setProfileContext returned an object that could not be cast into the profile specs ProfileTable type: "+e);
        }
        catch (Exception e) {
            msgSender.sendFailure( 1110319, "Calling getProfileTable in setProfileContext should have succeeded but threw exception: "+e);
        }

        //1110324, 1110325: getProfileTable(String) returns ProfileTable object for a specific profileTable, this can be cast to the
        //corresponding profile specs ProfileTable object type
        try {
            ProfileContextTestsProfileTable profileTable = (ProfileContextTestsProfileTable)context.getProfileTable(PROFILE_TABLE_NAME);
            msgSender.sendSuccess( 1110324, "getProfileTable(String) called in setProfileContext returns object that can be successfully cast into the profile specs ProfileTable type.");
        }
        catch (ClassCastException e) {
            msgSender.sendFailure( 1110324, "getProfileTable(String) called in setProfileContext returned an object that could not be cast into the profile specs ProfileTable type: "+e);
        }
        catch (Exception e) {
            msgSender.sendFailure( 1110324, "Calling getProfileTable(String) in setProfileContext should have succeeded but threw exception: "+e);
        }

        //1110327: The getProfileTable(String profileTableName) method throws a
        //java.lang.NullPointerException if the profileTableName argument is null.
        try {
            context.getProfileTable(null);
            msgSender.sendFailure( 1110327, "Calling getProfileTable(null) should have resulted in an exception being thrown.");
        }
        catch (NullPointerException e) {
            msgSender.sendSuccess( 1110327, "Calling getProfileTable(null) threw exception as expected: "+e);
        }
        catch (Exception e) {
            msgSender.sendFailure( 1110327, "Wrong type of exception: "+e.getClass().getName()+" Expected: java.lang.NullPointerException.");
        }

        //1110328: The getProfileTable(String profileTableName) method throws a
        //javax.slee.profile.UnrecognizedProfileTableNameException if no Profile
        //Table exists with the name specified by the profileTableName argument.
        try {
            context.getProfileTable("XYZ");
            msgSender.sendFailure( 1110328, "Calling getProfileTable(\"XYZ\") should have resulted in an exception being thrown.");
        }
        catch (UnrecognizedProfileTableNameException e) {
            msgSender.sendSuccess( 1110328, "Calling getProfileTable(\"XYZ\") threw exception as expected: "+e);
        }
        catch (Exception e) {
            msgSender.sendFailure( 1110328, "Wrong type of exception: "+e.getClass().getName()+" Expected: javax.slee.profile.UnrecognizedProfileTableNameException.");
        }

        //1110332, 1110333, 1110334, 1110335: getProfileLocalObject returns ProfileLocal object for the profile associated with this profile object
        //which can be typecast into the profile specs ProfileLocal type.
        //it may only be invoked when the profile object is in the READY state
        try {
            ProfileContextTestsProfileLocal profileLocal = (ProfileContextTestsProfileLocal)context.getProfileLocalObject();
            msgSender.sendFailure( 1110334, "getProfileLocalObject called in setProfileContext should have thrown an exception.");
        }
        catch (IllegalStateException e) {
            msgSender.sendSuccess( 1110334, "getProfileLocalObject called in setProfileContext threw an exception as expected: "+e);
        }
        catch (Exception e) {
            msgSender.sendFailure( 1110334, "Wrong type of exception: "+e.getClass().getName()+" Expected: java.lang.IllegalStateException");
        }
    }

    public void unsetProfileContext() {
        context.getTracer("unsetProfileContext").info("unsetProfileContext called");

        //1110315: getProfileName() method may only be invoked during a profilePostCreate method
        //invocation, or during any method invocation on a Profile object while the
        //Profile object is in the Ready state. Otherwise a java.lang.IllegalStateException
        //is thrown.
        try {
            context.getProfileName();
            msgSender.sendFailure( 1110315, "Calling getProfileName in unsetProfileContext should have resulted in an exception being thrown.");
        }
        catch (IllegalStateException e) {
            msgSender.sendSuccess( 1110315, "Calling getProfileName in unsetProfileContext threw exception as expected.");
        }
        catch (Exception e) {
            msgSender.sendFailure( 1110315, "Wrong type of exception: "+e.getClass().getName()+" Expected: java.lang.IllegalStateException");
        }

        //1110319, 1110320: getProfileTable returns ProfileTable object, this can be cast to the
        //profile specs ProfileTable object type
        try {
            ProfileContextTestsProfileTable profileTable = (ProfileContextTestsProfileTable)context.getProfileTable();
            msgSender.sendSuccess( 1110319, "getProfileTable called in unsetProfileContext returns object that can be successfully cast into the profile specs ProfileTable type.");
        }
        catch (ClassCastException e) {
            msgSender.sendFailure( 1110319, "getProfileTable called in unsetProfileContext returned an object that could not be cast into the profile specs ProfileTable type: "+e);
        }
        catch (Exception e) {
            msgSender.sendFailure( 1110319, "Calling getProfileTable in unsetProfileContext should have succeeded but threw exception: "+e);
        }

        //1110324, 1110325: getProfileTable(String) returns ProfileTable object for a specific profileTable, this can be cast to the
        //corresponding profile specs ProfileTable object type
        try {
            ProfileContextTestsProfileTable profileTable = (ProfileContextTestsProfileTable)context.getProfileTable(PROFILE_TABLE_NAME);
            msgSender.sendSuccess( 1110324, "getProfileTable(String) called in unsetProfileContext returns object that can be successfully cast into the profile specs ProfileTable type.");
        }
        catch (ClassCastException e) {
            msgSender.sendFailure( 1110324, "getProfileTable(String) called in unsetProfileContext returned an object that could not be cast into the profile specs ProfileTable type: "+e);
        }
        catch (Exception e) {
            msgSender.sendFailure( 1110324, "Calling getProfileTable(String) in unsetProfileContext should have succeeded but threw exception: "+e);
        }

        //1110332, 1110333, 1110334, 1110335: getProfileLocalObject returns ProfileLocal object for the profile associated with this profile object
        //which can be typecast into the profile specs ProfileLocal type.
        //it may only be invoked when the profile object is in the READY state
        try {
            ProfileContextTestsProfileLocal profileLocal = (ProfileContextTestsProfileLocal)context.getProfileLocalObject();
            msgSender.sendFailure( 1110334, "getProfileLocalObject called in unsetProfileContext should have thrown an exception.");
        }
        catch (IllegalStateException e) {
            msgSender.sendSuccess( 1110334, "getProfileLocalObject called in unsetProfileContext threw an exception as expected: "+e);
        }
        catch (Exception e) {
            msgSender.sendFailure( 1110334, "Wrong type of exception: "+e.getClass().getName()+" Expected: java.lang.IllegalStateException");
        }

        context = null;

    }

    public void profileInitialize() {
        context.getTracer("profileInitialize").info("profileInitialize called");

        //1110315: getProfileName() method may only be invoked during a profilePostCreate method
        //invocation, or during any method invocation on a Profile object while the
        //Profile object is in the Ready state. Otherwise a java.lang.IllegalStateException
        //is thrown.
        try {
            context.getProfileName();
            msgSender.sendFailure( 1110315, "Calling getProfileName in profileInitialize should have resulted in an exception being thrown.");
        }
        catch (IllegalStateException e) {
            msgSender.sendSuccess( 1110315, "Calling getProfileName in profileInitialize threw exception as expected.");
        }
        catch (Exception e) {
            msgSender.sendFailure( 1110315, "Wrong type of exception: "+e.getClass().getName()+" Expected: java.lang.IllegalStateException");
        }

        //1110319, 1110320: getProfileTable returns ProfileTable object, this can be cast to the
        //profile specs ProfileTable object type
        try {
            ProfileContextTestsProfileTable profileTable = (ProfileContextTestsProfileTable)context.getProfileTable();
            msgSender.sendSuccess( 1110319, "getProfileTable called in profileInitialize returns object that can be successfully cast into the profile specs ProfileTable type.");
        }
        catch (ClassCastException e) {
            msgSender.sendFailure( 1110319, "getProfileTable called in profileInitialize returned an object that could not be cast into the profile specs ProfileTable type: "+e);
        }
        catch (Exception e) {
            msgSender.sendFailure( 1110319, "Calling getProfileTable in profileInitialize should have succeeded but threw exception: "+e);
        }

        //1110324, 1110325: getProfileTable(String) returns ProfileTable object for a specific profileTable, this can be cast to the
        //corresponding profile specs ProfileTable object type
        try {
            ProfileContextTestsProfileTable profileTable = (ProfileContextTestsProfileTable)context.getProfileTable(PROFILE_TABLE_NAME);
            msgSender.sendSuccess( 1110324, "getProfileTable(String) called in profileInitialize returns object that can be successfully cast into the profile specs ProfileTable type.");
        }
        catch (ClassCastException e) {
            msgSender.sendFailure( 1110324, "getProfileTable(String) called in profileInitialize returned an object that could not be cast into the profile specs ProfileTable type: "+e);
        }
        catch (Exception e) {
            msgSender.sendFailure( 1110324, "Calling getProfileTable(String) in profileInitialize should have succeeded but threw exception: "+e);
        }

        //1110332, 1110333, 1110334, 1110335: getProfileLocalObject returns ProfileLocal object for the profile associated with this profile object
        //which can be typecast into the profile specs ProfileLocal type.
        //it may only be invoked when the profile object is in the READY state
        try {
            ProfileContextTestsProfileLocal profileLocal = (ProfileContextTestsProfileLocal)context.getProfileLocalObject();
            msgSender.sendFailure( 1110334, "getProfileLocalObject called in profileInitialize should have thrown an exception.");
        }
        catch (IllegalStateException e) {
            msgSender.sendSuccess( 1110334, "getProfileLocalObject called in profileInitialize threw an exception as expected: "+e);
        }
        catch (Exception e) {
            msgSender.sendFailure( 1110334, "Wrong type of exception: "+e.getClass().getName()+" Expected: java.lang.IllegalStateException");
        }
    }

    public void profilePostCreate() throws CreateException {
        context.getTracer("profilePostCreate").info("profilePostCreate called");

        //1110315: getProfileName() method may only be invoked during a profilePostCreate method
        //invocation, or during any method invocation on a Profile object while the
        //Profile object is in the Ready state. Otherwise a java.lang.IllegalStateException
        //is thrown.
        try {
            context.getProfileName();
            msgSender.sendSuccess( 1110315, "Calling getProfileName in profilePostCreate succeeded as expected.");
        }
        catch (Exception e) {
            msgSender.sendFailure( 1110315, "Calling getProfileName in profilePostCreate should have succeeded but threw exception: "+e);
        }

        //1110319, 1110320: getProfileTable returns ProfileTable object, this can be cast to the
        //profile specs ProfileTable object type
        try {
            ProfileContextTestsProfileTable profileTable = (ProfileContextTestsProfileTable)context.getProfileTable();
            msgSender.sendSuccess( 1110319, "getProfileTable called in profilePostCreate returns object that can be successfully cast into the profile specs ProfileTable type.");
        }
        catch (ClassCastException e) {
            msgSender.sendFailure( 1110319, "getProfileTable called in profilePostCreate returned an object that could not be cast into the profile specs ProfileTable type: "+e);
        }
        catch (Exception e) {
            msgSender.sendFailure( 1110319, "Calling getProfileTable in profilePostCreate should have succeeded but threw exception: "+e);
        }

        //1110324, 1110325: getProfileTable(String) returns ProfileTable object for a specific profileTable, this can be cast to the
        //corresponding profile specs ProfileTable object type
        try {
            ProfileContextTestsProfileTable profileTable = (ProfileContextTestsProfileTable)context.getProfileTable(PROFILE_TABLE_NAME);
            msgSender.sendSuccess( 1110324, "getProfileTable(String) called in profilePostCreate returns object that can be successfully cast into the profile specs ProfileTable type.");
        }
        catch (ClassCastException e) {
            msgSender.sendFailure( 1110324, "getProfileTable(String) called in profilePostCreate returned an object that could not be cast into the profile specs ProfileTable type: "+e);
        }
        catch (Exception e) {
            msgSender.sendFailure( 1110324, "Calling getProfileTable(String) in profilePostCreate should have succeeded but threw exception: "+e);
        }

        //1110332, 1110333, 1110334, 1110335: getProfileLocalObject returns ProfileLocal object for the profile associated with this profile object
        //which can be typecast into the profile specs ProfileLocal type.
        //the method should fail if profilePostCreate is called for the default profile
        try {
            ProfileContextTestsProfileLocal profileLocal = (ProfileContextTestsProfileLocal)context.getProfileLocalObject();
            if (context.getProfileName()==null)
                msgSender.sendFailure( 1110335, "getProfileLocalObject called in profilePostCreate should have thrown an exception as associated profile is the default profile");
            else
                msgSender.sendSuccess( 1110332, "getProfileLocalObject called in profilePostCreate returns object that can be successfully cast into the profile specs ProfileLocal interface type");
        }
        catch (IllegalStateException e) {
            if (context.getProfileName()!=null)
                msgSender.sendFailure( 1110332, "getProfileLocalObject called in profilePostCreate threw an exception though associated profile is not the default profile: "+e);
            else
                msgSender.sendSuccess( 1110335, "getProfileLocalObject called in profilePostCreate correctly threw an exception as associated profile is the default profile: "+e);
        }
        catch (Exception e) {
            msgSender.sendFailure( 1110332, "Wrong type of exception: "+e.getClass().getName()+" Expected: java.lang.IllegalStateException");
        }
    }

    public void profileActivate() {
        context.getTracer("profileActivate").info("profileActivate called");

        //1110315: getProfileName() method may only be invoked during a profilePostCreate method
        //invocation, or during any method invocation on a Profile object while the
        //Profile object is in the Ready state. Otherwise a java.lang.IllegalStateException
        //is thrown.
        try {
            context.getProfileName();
            msgSender.sendFailure( 1110315, "Calling getProfileName in profileActivate should have resulted in an exception being thrown.");
        }
        catch (IllegalStateException e) {
            msgSender.sendSuccess( 1110315, "Calling getProfileName in profileActivate threw exception as expected.");
        }
        catch (Exception e) {
            msgSender.sendFailure( 1110315, "Wrong type of exception: "+e.getClass().getName()+" Expected: java.lang.IllegalStateException");
        }

        //1110319, 1110320: getProfileTable returns ProfileTable object, this can be cast to the
        //profile specs ProfileTable object type
        try {
            ProfileContextTestsProfileTable profileTable = (ProfileContextTestsProfileTable)context.getProfileTable();
            msgSender.sendSuccess( 1110319, "getProfileTable called in profileActivate returns object that can be successfully cast into the profile specs ProfileTable type.");
        }
        catch (ClassCastException e) {
            msgSender.sendFailure( 1110319, "getProfileTable called in profileActivate returned an object that could not be cast into the profile specs ProfileTable type: "+e);
        }
        catch (Exception e) {
            msgSender.sendFailure( 1110319, "Calling getProfileTable in profileActivate should have succeeded but threw exception: "+e);
        }

        //1110324, 1110325: getProfileTable(String) returns ProfileTable object for a specific profileTable, this can be cast to the
        //corresponding profile specs ProfileTable object type
        try {
            ProfileContextTestsProfileTable profileTable = (ProfileContextTestsProfileTable)context.getProfileTable(PROFILE_TABLE_NAME);
            msgSender.sendSuccess( 1110324, "getProfileTable(String) called in profileActivate returns object that can be successfully cast into the profile specs ProfileTable type.");
        }
        catch (ClassCastException e) {
            msgSender.sendFailure( 1110324, "getProfileTable(String) called in profileActivate returned an object that could not be cast into the profile specs ProfileTable type: "+e);
        }
        catch (Exception e) {
            msgSender.sendFailure( 1110324, "Calling getProfileTable(String) in profileActivate should have succeeded but threw exception: "+e);
        }

        //1110332, 1110333, 1110334, 1110335: getProfileLocalObject returns ProfileLocal object for the profile associated with this profile object
        //which can be typecast into the profile specs ProfileLocal type.
        //it may only be invoked when the profile object is in the READY state
        try {
            ProfileContextTestsProfileLocal profileLocal = (ProfileContextTestsProfileLocal)context.getProfileLocalObject();
            msgSender.sendFailure( 1110334, "getProfileLocalObject called in profileActivate should have thrown an exception.");
        }
        catch (IllegalStateException e) {
            msgSender.sendSuccess( 1110334, "getProfileLocalObject called in profileActivate threw an exception as expected: "+e);
        }
        catch (Exception e) {
            msgSender.sendFailure( 1110334, "Wrong type of exception: "+e.getClass().getName()+" Expected: java.lang.IllegalStateException");
        }
    }

    public void profilePassivate() {
        context.getTracer("profilePassivate").info("profilePassivate called");

        //1110315: getProfileName() method may only be invoked during a profilePostCreate method
        //invocation, or during any method invocation on a Profile object while the
        //Profile object is in the Ready state. Otherwise a java.lang.IllegalStateException
        //is thrown.
        try {
            context.getProfileName();
            msgSender.sendSuccess( 1110315, "Calling getProfileName in profilePassivate succeeded as expected.");
        }
        catch (Exception e) {
            msgSender.sendFailure( 1110315, "Calling getProfileName in profilePassivate should have succeeded but threw exception: "+e);
        }

        //1110319, 1110320: getProfileTable returns ProfileTable object, this can be cast to the
        //profile specs ProfileTable object type
        try {
            ProfileContextTestsProfileTable profileTable = (ProfileContextTestsProfileTable)context.getProfileTable();
            msgSender.sendSuccess( 1110319, "getProfileTable called in profilePassivate returns object that can be successfully cast into the profile specs ProfileTable type.");
        }
        catch (ClassCastException e) {
            msgSender.sendFailure( 1110319, "getProfileTable called in profilePassivate returned an object that could not be cast into the profile specs ProfileTable type: "+e);
        }
        catch (Exception e) {
            msgSender.sendFailure( 1110319, "Calling getProfileTable in profilePassivate should have succeeded but threw exception: "+e);
        }

        //1110324, 1110325: getProfileTable(String) returns ProfileTable object for a specific profileTable, this can be cast to the
        //corresponding profile specs ProfileTable object type
        try {
            ProfileContextTestsProfileTable profileTable = (ProfileContextTestsProfileTable)context.getProfileTable(PROFILE_TABLE_NAME);
            msgSender.sendSuccess( 1110324, "getProfileTable(String) called in profilePassivate returns object that can be successfully cast into the profile specs ProfileTable type.");
        }
        catch (ClassCastException e) {
            msgSender.sendFailure( 1110324, "getProfileTable(String) called in profilePassivate returned an object that could not be cast into the profile specs ProfileTable type: "+e);
        }
        catch (Exception e) {
            msgSender.sendFailure( 1110324, "Calling getProfileTable(String) in profilePassivate should have succeeded but threw exception: "+e);
        }

        //1110332, 1110333, 1110334, 1110335: getProfileLocalObject returns ProfileLocal object for the profile associated with this profile object
        //which can be typecast into the profile specs ProfileLocal type.
        //the method should fail if profilePassivate is called for the default profile
        try {
            ProfileContextTestsProfileLocal profileLocal = (ProfileContextTestsProfileLocal)context.getProfileLocalObject();
            if (context.getProfileName()==null)
                msgSender.sendFailure( 1110335, "getProfileLocalObject called in profilePassivate should have thrown an exception as associated profile is the default profile");
            else
                msgSender.sendSuccess( 1110332, "getProfileLocalObject called in profilePassivate returns object that can be successfully cast into the profile specs ProfileLocal interface type");
        }
        catch (IllegalStateException e) {
            if (context.getProfileName()!=null)
                msgSender.sendFailure( 1110332, "getProfileLocalObject called in profilePassivate threw an exception though associated profile is not the default profile: "+e);
            else
                msgSender.sendSuccess( 1110335, "getProfileLocalObject called in profilePassivate correctly threw an exception as associated profile is the default profile: "+e);
        }
        catch (Exception e) {
            msgSender.sendFailure( 1110332, "Wrong type of exception: "+e.getClass().getName()+" Expected: java.lang.IllegalStateException");
        }
    }

    public void profileLoad() {
        context.getTracer("profileLoad").info("profileLoad called");

        //1110315: getProfileName() method may only be invoked during a profilePostCreate method
        //invocation, or during any method invocation on a Profile object while the
        //Profile object is in the Ready state. Otherwise a java.lang.IllegalStateException
        //is thrown.
        try {
            context.getProfileName();
            msgSender.sendSuccess( 1110315, "Calling getProfileName in profileLoad succeeded as expected.");
        }
        catch (Exception e) {
            msgSender.sendFailure( 1110315, "Calling getProfileName in profileLoad should have succeeded but threw exception: "+e);
        }

        //1110319, 1110320: getProfileTable returns ProfileTable object, this can be cast to the
        //profile specs ProfileTable object type
        try {
            ProfileContextTestsProfileTable profileTable = (ProfileContextTestsProfileTable)context.getProfileTable();
            msgSender.sendSuccess( 1110319, "getProfileTable called in profileLoad returns object that can be successfully cast into the profile specs ProfileTable type.");
        }
        catch (ClassCastException e) {
            msgSender.sendFailure( 1110319, "getProfileTable called in profileLoad returned an object that could not be cast into the profile specs ProfileTable type: "+e);
        }
        catch (Exception e) {
            msgSender.sendFailure( 1110319, "Calling getProfileTable in profileLoad should have succeeded but threw exception: "+e);
        }

        //1110324, 1110325: getProfileTable(String) returns ProfileTable object for a specific profileTable, this can be cast to the
        //corresponding profile specs ProfileTable object type
        try {
            ProfileContextTestsProfileTable profileTable = (ProfileContextTestsProfileTable)context.getProfileTable(PROFILE_TABLE_NAME);
            msgSender.sendSuccess( 1110324, "getProfileTable(String) called in profileLoad returns object that can be successfully cast into the profile specs ProfileTable type.");
        }
        catch (ClassCastException e) {
            msgSender.sendFailure( 1110324, "getProfileTable(String) called in profileLoad returned an object that could not be cast into the profile specs ProfileTable type: "+e);
        }
        catch (Exception e) {
            msgSender.sendFailure( 1110324, "Calling getProfileTable(String) in profileLoad should have succeeded but threw exception: "+e);
        }

        //1110332, 1110333, 1110334, 1110335: getProfileLocalObject returns ProfileLocal object for the profile associated with this profile object
        //which can be typecast into the profile specs ProfileLocal type.
        //the method should fail if profilePassivate is called for the default profile
        try {
            ProfileContextTestsProfileLocal profileLocal = (ProfileContextTestsProfileLocal)context.getProfileLocalObject();
            if (context.getProfileName()==null)
                msgSender.sendFailure( 1110335, "getProfileLocalObject called in profileLoad should have thrown an exception as associated profile is the default profile");
            else
                msgSender.sendSuccess( 1110332, "getProfileLocalObject called in profileLoad returns object that can be successfully cast into the profile specs ProfileLocal interface type");
        }
        catch (IllegalStateException e) {
            if (context.getProfileName()!=null)
                msgSender.sendFailure( 1110332, "getProfileLocalObject called in profileLoad threw an exception though associated profile is not the default profile: "+e);
            else
                msgSender.sendSuccess( 1110335, "getProfileLocalObject called in profileLoad correctly threw an exception as associated profile is the default profile: "+e);
        }
        catch (Exception e) {
            msgSender.sendFailure( 1110332, "Wrong type of exception: "+e.getClass().getName()+" Expected: java.lang.IllegalStateException");
        }
    }

    public void profileStore() {
        context.getTracer("profileStore").info("profileStore called");

        //1110315: getProfileName() method may only be invoked during a profilePostCreate method
        //invocation, or during any method invocation on a Profile object while the
        //Profile object is in the Ready state. Otherwise a java.lang.IllegalStateException
        //is thrown.
        try {
            context.getProfileName();
            msgSender.sendSuccess( 1110315, "Calling getProfileName in profileStore succeeded as expected.");
        }
        catch (Exception e) {
            msgSender.sendFailure( 1110315, "Calling getProfileName in profileStore should have succeeded but threw exception: "+e);
        }

        //1110319, 1110320: getProfileTable returns ProfileTable object, this can be cast to the
        //profile specs ProfileTable object type
        try {
            ProfileContextTestsProfileTable profileTable = (ProfileContextTestsProfileTable)context.getProfileTable();
            msgSender.sendSuccess( 1110319, "getProfileTable called in profileStore returns object that can be successfully cast into the profile specs ProfileTable type.");
        }
        catch (ClassCastException e) {
            msgSender.sendFailure( 1110319, "getProfileTable called in profileStore returned an object that could not be cast into the profile specs ProfileTable type: "+e);
        }
        catch (Exception e) {
            msgSender.sendFailure( 1110319, "Calling getProfileTable in profileStore should have succeeded but threw exception: "+e);
        }

        //1110324, 1110325: getProfileTable(String) returns ProfileTable object for a specific profileTable, this can be cast to the
        //corresponding profile specs ProfileTable object type
        try {
            ProfileContextTestsProfileTable profileTable = (ProfileContextTestsProfileTable)context.getProfileTable(PROFILE_TABLE_NAME);
            msgSender.sendSuccess( 1110324, "getProfileTable(String) called in profileStore returns object that can be successfully cast into the profile specs ProfileTable type.");
        }
        catch (ClassCastException e) {
            msgSender.sendFailure( 1110324, "getProfileTable(String) called in profileStore returned an object that could not be cast into the profile specs ProfileTable type: "+e);
        }
        catch (Exception e) {
            msgSender.sendFailure( 1110324, "Calling getProfileTable(String) in profileStore should have succeeded but threw exception: "+e);
        }

        //1110332, 1110333, 1110334, 1110335: getProfileLocalObject returns ProfileLocal object for the profile associated with this profile object
        //which can be typecast into the profile specs ProfileLocal type.
        //the method should fail if profilePassivate is called for the default profile
        try {
            ProfileContextTestsProfileLocal profileLocal = (ProfileContextTestsProfileLocal)context.getProfileLocalObject();
            if (context.getProfileName()==null)
                msgSender.sendFailure( 1110335, "getProfileLocalObject called in profileStore should have thrown an exception as associated profile is the default profile");
            else
                msgSender.sendSuccess( 1110332, "getProfileLocalObject called in profileStore returns object that can be successfully cast into the profile specs ProfileLocal interface type");
        }
        catch (IllegalStateException e) {
            if (context.getProfileName()!=null)
                msgSender.sendFailure( 1110332, "getProfileLocalObject called in profileStore threw an exception though associated profile is not the default profile: "+e);
            else
                msgSender.sendSuccess( 1110335, "getProfileLocalObject called in profileStore correctly threw an exception as associated profile is the default profile: "+e);
        }
        catch (Exception e) {
            msgSender.sendFailure( 1110332, "Wrong type of exception: "+e.getClass().getName()+" Expected: java.lang.IllegalStateException");
        }
    }

    public void profileRemove() {
        context.getTracer("profileRemove").info("profileRemove called");

        //1110315: getProfileName() method may only be invoked during a profilePostCreate method
        //invocation, or during any method invocation on a Profile object while the
        //Profile object is in the Ready state. Otherwise a java.lang.IllegalStateException
        //is thrown.
        try {
            context.getProfileName();
            msgSender.sendSuccess( 1110315, "Calling getProfileName in profileRemove succeeded as expected.");
        }
        catch (Exception e) {
            msgSender.sendFailure( 1110315, "Calling getProfileName in profileRemove should have succeeded but threw exception: "+e);
        }

        //1110319, 1110320: getProfileTable returns ProfileTable object, this can be cast to the
        //profile specs ProfileTable object type
        try {
            ProfileContextTestsProfileTable profileTable = (ProfileContextTestsProfileTable)context.getProfileTable();
            msgSender.sendSuccess( 1110319, "getProfileTable called in profileRemove returns object that can be successfully cast into the profile specs ProfileTable type.");
        }
        catch (ClassCastException e) {
            msgSender.sendFailure( 1110319, "getProfileTable called in profileRemove returned an object that could not be cast into the profile specs ProfileTable type: "+e);
        }
        catch (Exception e) {
            msgSender.sendFailure( 1110319, "Calling getProfileTable in profileRemove should have succeeded but threw exception: "+e);
        }

        //1110324, 1110325: getProfileTable(String) returns ProfileTable object for a specific profileTable, this can be cast to the
        //corresponding profile specs ProfileTable object type
        try {
            ProfileContextTestsProfileTable profileTable = (ProfileContextTestsProfileTable)context.getProfileTable(PROFILE_TABLE_NAME);
            msgSender.sendSuccess( 1110324, "getProfileTable(String) called in profileRemove returns object that can be successfully cast into the profile specs ProfileTable type.");
        }
        catch (ClassCastException e) {
            msgSender.sendFailure( 1110324, "getProfileTable(String) called in profileRemove returned an object that could not be cast into the profile specs ProfileTable type: "+e);
        }
        catch (Exception e) {
            msgSender.sendFailure( 1110324, "Calling getProfileTable(String) in profileRemove should have succeeded but threw exception: "+e);
        }

        //1110332, 1110333, 1110334, 1110335: getProfileLocalObject returns ProfileLocal object for the profile associated with this profile object
        //which can be typecast into the profile specs ProfileLocal type.
        //the method should fail if profilePassivate is called for the default profile
        try {
            ProfileContextTestsProfileLocal profileLocal = (ProfileContextTestsProfileLocal)context.getProfileLocalObject();
            if (context.getProfileName()==null)
                msgSender.sendFailure( 1110335, "getProfileLocalObject called in profileRemove should have thrown an exception as associated profile is the default profile");
            else
                msgSender.sendSuccess( 1110332, "getProfileLocalObject called in profileRemove returns object that can be successfully cast into the profile specs ProfileLocal interface type");
        }
        catch (IllegalStateException e) {
            if (context.getProfileName()!=null)
                msgSender.sendFailure( 1110332, "getProfileLocalObject called in profileRemove threw an exception though associated profile is not the default profile: "+e);
            else
                msgSender.sendSuccess( 1110335, "getProfileLocalObject called in profileRemove correctly threw an exception as associated profile is the default profile: "+e);
        }
        catch (Exception e) {
            msgSender.sendFailure( 1110332, "Wrong type of exception: "+e.getClass().getName()+" Expected: java.lang.IllegalStateException");
        }
    }

    public void profileVerify() throws ProfileVerificationException {
        context.getTracer("profileVerify").info("profileVerify called");

        //1110315: getProfileName() method may only be invoked during a profilePostCreate method
        //invocation, or during any method invocation on a Profile object while the
        //Profile object is in the Ready state. Otherwise a java.lang.IllegalStateException
        //is thrown.
        try {
            context.getProfileName();
            msgSender.sendSuccess( 1110315, "Calling getProfileName in profileVerify succeeded as expected.");
        }
        catch (Exception e) {
            msgSender.sendFailure( 1110315, "Calling getProfileName in profileVerify should have succeeded but threw exception: "+e);
        }

        //1110319, 1110320: getProfileTable returns ProfileTable object, this can be cast to the
        //profile specs ProfileTable object type
        try {
            ProfileContextTestsProfileTable profileTable = (ProfileContextTestsProfileTable)context.getProfileTable();
            msgSender.sendSuccess( 1110319, "getProfileTable called in profileVerify returns object that can be successfully cast into the profile specs ProfileTable type.");
        }
        catch (ClassCastException e) {
            msgSender.sendFailure( 1110319, "getProfileTable called in profileVerify returned an object that could not be cast into the profile specs ProfileTable type: "+e);
        }
        catch (Exception e) {
            msgSender.sendFailure( 1110319, "Calling getProfileTable in profileVerify should have succeeded but threw exception: "+e);
        }

        //1110324, 1110325: getProfileTable(String) returns ProfileTable object for a specific profileTable, this can be cast to the
        //corresponding profile specs ProfileTable object type
        try {
            ProfileContextTestsProfileTable profileTable = (ProfileContextTestsProfileTable)context.getProfileTable(PROFILE_TABLE_NAME);
            msgSender.sendSuccess( 1110324, "getProfileTable(String) called in profileVerify returns object that can be successfully cast into the profile specs ProfileTable type.");
        }
        catch (ClassCastException e) {
            msgSender.sendFailure( 1110324, "getProfileTable(String) called in profileVerify returned an object that could not be cast into the profile specs ProfileTable type: "+e);
        }
        catch (Exception e) {
            msgSender.sendFailure( 1110324, "Calling getProfileTable(String) in profileVerify should have succeeded but threw exception: "+e);
        }

        //1110332, 1110333, 1110334, 1110335: getProfileLocalObject returns ProfileLocal object for the profile associated with this profile object
        //which can be typecast into the profile specs ProfileLocal type.
        //the method should fail if profilePassivate is called for the default profile
        try {
            ProfileContextTestsProfileLocal profileLocal = (ProfileContextTestsProfileLocal)context.getProfileLocalObject();
            if (context.getProfileName()==null)
                msgSender.sendFailure( 1110335, "getProfileLocalObject called in profileVerify should have thrown an exception as associated profile is the default profile");
            else
                msgSender.sendSuccess( 1110332, "getProfileLocalObject called in profileVerify returns object that can be successfully cast into the profile specs ProfileLocal interface type");
        }
        catch (IllegalStateException e) {
            if (context.getProfileName()!=null)
                msgSender.sendFailure( 1110332, "getProfileLocalObject called in profileVerify threw an exception though associated profile is not the default profile: "+e);
            else
                msgSender.sendSuccess( 1110335, "getProfileLocalObject called in profileVerify correctly threw an exception as associated profile is the default profile: "+e);
        }
        catch (Exception e) {
            msgSender.sendFailure( 1110332, "Wrong type of exception: "+e.getClass().getName()+" Expected: java.lang.IllegalStateException");
        }
    }

    public void business() {
    }

    public void getRollbackTest() {
    }


    public void manage() {
        context.getTracer("manage").fine("Management method called.");

        //1110315: getProfileName() method may only be invoked during a profilePostCreate method
        //invocation, or during any method invocation on a Profile object while the
        //Profile object is in the Ready state. Otherwise a java.lang.IllegalStateException
        //is thrown.
        try {
            context.getProfileName();
            msgSender.sendSuccess( 1110315, "Calling getProfileName in management method succeeded as expected.");
        }
        catch (Exception e) {
            msgSender.sendFailure( 1110315, "Calling getProfileName in management method should have succeeded but threw exception: "+e);
        }

        //1110319, 1110320: getProfileTable returns ProfileTable object, this can be cast to the
        //profile specs ProfileTable object type
        try {
            ProfileContextTestsProfileTable profileTable = (ProfileContextTestsProfileTable)context.getProfileTable();
            msgSender.sendSuccess( 1110319, "getProfileTable called in management method returns object that can be successfully cast into the profile specs ProfileTable type.");
        }
        catch (ClassCastException e) {
            msgSender.sendFailure( 1110319, "getProfileTable called in management method returned an object that could not be cast into the profile specs ProfileTable type: "+e);
        }
        catch (Exception e) {
            msgSender.sendFailure( 1110319, "Calling getProfileTable in management method should have succeeded but threw exception: "+e);
        }

        //1110324, 1110325: getProfileTable(String) returns ProfileTable object for a specific profileTable, this can be cast to the
        //corresponding profile specs ProfileTable object type
        try {
            ProfileContextTestsProfileTable profileTable = (ProfileContextTestsProfileTable)context.getProfileTable(PROFILE_TABLE_NAME);
            msgSender.sendSuccess( 1110324, "getProfileTable(String) called in management method returns object that can be successfully cast into the profile specs ProfileTable type.");
        }
        catch (ClassCastException e) {
            msgSender.sendFailure( 1110324, "getProfileTable(String) called in management method returned an object that could not be cast into the profile specs ProfileTable type: "+e);
        }
        catch (Exception e) {
            msgSender.sendFailure( 1110324, "Calling getProfileTable(String) in management method should have succeeded but threw exception: "+e);
        }

        //1110332, 1110333, 1110334, 1110335: getProfileLocalObject returns ProfileLocal object for the profile associated with this profile object
        //which can be typecast into the profile specs ProfileLocal type.
        //the method should fail if profilePassivate is called for the default profile
        try {
            ProfileContextTestsProfileLocal profileLocal = (ProfileContextTestsProfileLocal)context.getProfileLocalObject();
            if (context.getProfileName()==null)
                msgSender.sendFailure( 1110335, "getProfileLocalObject called in management method should have thrown an exception as associated profile is the default profile");
            else
                msgSender.sendSuccess( 1110332, "getProfileLocalObject called in management method returns object that can be successfully cast into the profile specs ProfileLocal interface type");
        }
        catch (IllegalStateException e) {
            if (context.getProfileName()!=null)
                msgSender.sendFailure( 1110332, "getProfileLocalObject called in management method threw an exception though associated profile is not the default profile: "+e);
            else
                msgSender.sendSuccess( 1110335, "getProfileLocalObject called in management method correctly threw an exception as associated profile is the default profile: "+e);
        }
        catch (Exception e) {
            msgSender.sendFailure( 1110332, "Wrong type of exception: "+e.getClass().getName()+" Expected: java.lang.IllegalStateException");
        }

        //1110338: The getTracer method returns a tracer for the specified tracer name.
        Tracer tracer = context.getTracer("tracerName");
        if (tracer.getTracerName().equals("tracerName"))
            msgSender.sendSuccess( 1110338, "getTracer returned Tracer for name 'tracerName' as expected.");
        else
            msgSender.sendFailure( 1110338, "getTracer returned Tracer with wrong name: '"+tracer.getTracerName()+"'. Expected 'tracerName'");

        //1110339, 1110340: send special msg to the tests NotificationListener to cause it evaluating the associated NotificationSource and Notification type
        tracer.info(TRACE_SUCCESS);

        //1110341: NPE is thrown if tracerName is null
        try {
            context.getTracer(null);
            msgSender.sendFailure( 1110341, "getTracer(null) should have thrown an exception.");
        }
        catch (NullPointerException e) {
            msgSender.sendSuccess( 1110341, "getTracer(null) threw an exception as expected: "+e);
        }
        catch (Exception e) {
            msgSender.sendFailure( 1110341, "Wrong type of exception received: "+e.getClass().getName()+" Expected: java.lang.NullPointerException");
        }

        //1110342: The getTracer method throws a java.lang.IllegalArgumentException if
        //the tracerName argument is not a valid tracer name.
        try {
            context.getTracer("com..mycompany");
            msgSender.sendFailure( 1110342, "getTracer(\"com..mycompany\") should have thrown an exception.");
        }
        catch (IllegalArgumentException e) {
            msgSender.sendSuccess( 1110342, "getTracer(\"com..mycompany\") threw an exception as expected: "+e);
        }
        catch (Exception e) {
            msgSender.sendFailure( 1110342, "Wrong type of exception received: "+e.getClass().getName()+" Expected: java.lang.IllegalArgumentException");
        }
    }

    public void invokeOnDefaultProfile() {
        context.getTracer("invokeOnDefaultProfile").fine("Management method invoked on Default Profile.");

        //1110756: Returns null if the Profile object is associated with the profile table's default profile.
        if (context.getProfileName()!=null)
            msgSender.sendFailure( 1110756, "getProfileName should return 'null' when called for the Default Profile.");
        else
            msgSender.sendSuccess( 1110756, "getProfileName returned 'null' when called for the Default Profile as expected.");

    }

    private ProfileContext context;

    private RMIObjectChannel out;
    private BaseMessageSender msgSender;

}
