/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.sbb.abstractclass;

import com.opencloud.sleetck.lib.resource.events.TCKResourceEventX;
import com.opencloud.sleetck.lib.sbbutils.BaseTCKSbb;
import com.opencloud.sleetck.lib.sbbutils.TCKSbbUtils;

import javax.slee.ActivityContextInterface;
import javax.slee.SLEEException;
import javax.slee.facilities.Level;
import java.util.HashMap;

public abstract class Test2122Sbb extends BaseTCKSbb {

    private static final String MESSAGE = "Test2122RuntimeException";

    public void sbbPostCreate() throws javax.slee.CreateException {
        createTraceSafe(Level.INFO, "Throwing a RuntimeException to cause the TXN to rollback in sbbPostCreate().");
        throw new SLEEException(MESSAGE);
    }

    public void sbbRolledBack(javax.slee.RolledBackContext context) {
        try {
            HashMap map = new HashMap();
            map.put("Result", new Boolean(false));
            map.put("Message", "sbbRolledBack() was called on a SBB that didn't allow sbbPostCreate() to complete.");
            TCKSbbUtils.getResourceInterface().sendSbbMessage(map);
        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }
    }

    public void sbbExceptionThrown(Exception exception, Object event, ActivityContextInterface aci) {
        createTraceSafe(Level.INFO,"Test2122Sbb:sbbExceptionThrown",null);
    }

    // Initial event
    public void onTCKResourceEventX1(TCKResourceEventX ev, ActivityContextInterface aci) {
        createTraceSafe(Level.INFO,"Test2122Sbb:Received initial event");
    }

}
