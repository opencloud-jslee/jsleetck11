/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.transactions;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.HashMap;

import javax.slee.transaction.CommitListener;
import javax.slee.transaction.RollbackListener;
import javax.slee.transaction.SleeTransaction;
import javax.slee.transaction.SleeTransactionManager;
import javax.transaction.HeuristicMixedException;
import javax.transaction.HeuristicRollbackException;
import javax.transaction.RollbackException;
import javax.transaction.Status;
import javax.transaction.SystemException;

import com.opencloud.logging.StdErrLog;
import com.opencloud.sleetck.lib.profileutils.BaseMessageSender;
import com.opencloud.sleetck.lib.rautils.MessageHandler;
import com.opencloud.sleetck.lib.rautils.RMIObjectChannel;
import com.opencloud.sleetck.lib.rautils.TCKRAUtils;
import com.opencloud.util.Future;
import com.opencloud.util.Future.TimeoutException;

/**
 * Provides callback method for handling messages sent from the TCK test to the RA.
 * The actual test code is located in the 'handleMessage()' method.
 */
public class Test1109203MessageListener extends UnicastRemoteObject implements MessageHandler {

    private Future future;
    private long blockTimeout;
    private Test1109203ResourceAdaptor ra;

    /**
     * Simple CommitListener which set the RA handleMessage's test thread semaphore
     */
    private class MyCommitListener implements CommitListener {

        public void committed() {
            future.setValue("committed");
        }

        public void heuristicMixed(HeuristicMixedException e) {
            future.setValue("heuristicMixed");
        }

        public void heuristicRollback(HeuristicRollbackException e) {
            future.setValue("heuristicRollback");
        }

        public void rolledBack(RollbackException e) {
            future.setValue("rolledBack");
        }

        public void systemException(SystemException e) {
            future.setValue("systemException");
        }
    }

    /**
     * Simple RollbackListener which sets the RA handleMessage's test thread semaphore
     */
    private class MyRollbackListener implements RollbackListener {

        public void rolledBack() {
            future.setValue("rolledBack");
        }

        public void systemException(SystemException e) {
            future.setValue("systemException");
        }

    }

    public Test1109203MessageListener(Test1109203ResourceAdaptor ra) throws RemoteException {
        super();

        try {
            out = TCKRAUtils.lookupRMIObjectChannel();
            msgSender = new BaseMessageSender(out, new StdErrLog());
        }
        catch (Exception e) {
            ra.getLog().warning("Exception occurred when trying to acquire RMIObjectChannel: ",e);
        }

        this.ra = ra;
    }

    public boolean handleMessage(Object message) throws RemoteException {

        HashMap map = (HashMap)message;
        blockTimeout = 2000;
        if (map.get("Timeout") != null)
            blockTimeout = ((Long)map.get("Timeout")).longValue();

        //run the tests, if both run through successfully and completely (=both return 'true')
        //send success msg back to TCK test
        if (testAsyncCommit() && testAsyncRollback())
            msgSender.sendSuccess(1109203, "Test successful.");

        return true;
    }

    private boolean testAsyncRollback() {
        SleeTransactionManager txnManager = ra.getResourceAdaptorContext().getSleeTransactionManager();
        SleeTransaction txn=null;


        //1109341: The calling thread is not required to have this transaction associated with the thread.
        //1109213:  If this argument is null, no callbacks will be made by the SLEE.
        //i.e. the rollbackListener argument may be null (there should be no exception etc.), the second part of the assertion
        //'no callbacks are made' can't be tested.
        try {
            txn = txnManager.beginSleeTransaction();
            txnManager.suspend();
            try {
                txn.asyncRollback(null);
                msgSender.sendLogMsg("Asynchronous rollback could be invoked from thread that is not currently associated with the TXN.");
            } catch (Exception e) {
                try {
                    if (txn!=null)
                        txn.rollback();
                } catch (Exception ex) {
                    ra.getLog().warning("Error occured when trying to rollback RA started TXN.", ex);
                    msgSender.sendLogMsg("Exception occurred when trying to safely rollback RA started TXN: "+ex.getMessage());
                }
                msgSender.sendFailure(1109341, "Asynchronous rollback failed with exception when invoked from thread that is currently not associated with the TXN.", e);
                return false;
            }


            //1109342: If any threads are associated with the transaction then those associations are cleared before this method returns.
            txn = txnManager.beginSleeTransaction();
            txn.asyncRollback(null);
            txn = txnManager.getSleeTransaction();

            //if the thread's associated TXN is not 'null', this is a violation of 1109342
            if (txn !=null) {
                msgSender.sendFailure(1109342, "Association with thread should have been cleared before 'asyncRollback()' returned from its call, thus 'null' should have been returned by TransactionManager.getTransaction().");
                return false;
            }
            else {
                msgSender.sendLogMsg("Association with thread was cleared before 'asyncRollback()' returned from its call as expected.");
            }

            int status;
            txn = txnManager.beginSleeTransaction();
            status = txn.getStatus();
            txn.asyncRollback(null);
            //txn.asyncRollback(new MyRollbackListener());
            //1109210:  The transaction must be currently active (Status.STATUS_ACTIVE) or marked for
            //rollback (Status.STATUS_MARKED_ROLLBACK).
            if (status==Status.STATUS_ACTIVE || status==Status.STATUS_MARKED_ROLLBACK) {
                msgSender.sendLogMsg("TXN asyncRollback() succeeded and status was Status.STATUS_ACTIVE or Status.STATUS_MARKED_ROLLBACK as expected.");
            }
            else {
                msgSender.sendFailure(1109210, "TXN asyncRollback() succeeded but status was different from Status.STATUS_ACTIVE or Status.STATUS_MARKED_ROLLBACK.");
                return false;
            }

            //as guaranteed in one of the previous assertions, the asyncRollback method diassociates
            //its thread from the TXN before the method returns, thus at this point the
            //TXN can't be STATUS_ACTIVE or STATUS_MARKED_ROLLBACK, thus consequently an
            //IllegalStateException should be thrown
            //1109214:  java.lang.IllegalStateException - if the transaction is not in the active or marked for rollback state.
            try {
                txn.asyncRollback(null);
                //txn.asyncRollback(new MyRollbackListener());
                status = txn.getStatus();
                msgSender.sendFailure(1109214, "asyncRollback() succeeded though current status can't have been STATUS_ACTIVE or STATUS_MARKED_ROLLBACK. Current status was: "+StatusUtil.statusToString(status));
                return false;
            } catch (IllegalStateException e) {
                msgSender.sendLogMsg("IllegalStateException thrown as expected: "+e);
            } catch (Exception e) {
                msgSender.sendFailure(1109214, "Wrong type of exception thrown, expected java.lang.IllegalStateException but caught: ", e);
                return false;
            }

            txn = txnManager.beginSleeTransaction();
            future = new Future();
            txn.asyncRollback(new MyRollbackListener());
            //1109339: listener object that will receive callbacks depending on the final outcome of the transaction.
            try {
                String rollbackResult = (String)future.getValue(blockTimeout);
                if ("rolledBack".equals(rollbackResult))
                    msgSender.sendLogMsg("RollbackListener received TXN rolledBack callback as expected.");
                else {
                    msgSender.sendFailure(1109339,"RollbackListener did not receive TXN rolledBack callback but returned '"+rollbackResult+"' instead.");
                    return false;
                }
            } catch (TimeoutException e) {
                msgSender.sendFailure(1109339,"RollbackListener did not receive callback thus the RA test thread timed out.",e);
                return false;
            }

        } catch (Exception e) {
            try {
                if (txn!=null)
                    txn.rollback();
            } catch (Exception ex) {
                ra.getLog().warning("Error occured when trying to rollback RA started TXN.", ex);
                msgSender.sendLogMsg("Exception occurred when trying to safely rollback RA started TXN: "+ex.getMessage());
            }
            msgSender.sendException(e);
            return false;
        }

        return true;
    }

    private boolean testAsyncCommit() {
        SleeTransactionManager txnManager = ra.getResourceAdaptorContext().getSleeTransactionManager();
        SleeTransaction txn=null;

        try {
            txn = txnManager.beginSleeTransaction();
            txnManager.suspend();

            //1109268: The calling thread is not required to have this transaction associated with the thread.
            //1109206: If this argument is null, no callbacks will be made by the SLEE.
            //i.e. the commitListener argument may be null (there should be no exception etc.), the second part of the assertion
            //'no callbacks are made' can't be tested.
            try {
                txn.asyncCommit(null);
                msgSender.sendLogMsg("Asynchronous commit could be invoked from thread that is currently not associated with the TXN as expected.");
            } catch (Exception e) {
                try {
                    if (txn!=null)
                        txn.rollback();
                } catch (Exception ex) {
                    ra.getLog().warning("Error occured when trying to rollback RA started TXN.", ex);
                    msgSender.sendLogMsg("Exception occurred when trying to safely rollback RA started TXN: "+ex.getMessage());
                }
                msgSender.sendFailure(1109268, "Asynchronous commit failed with exception when invoked from thread that is currently not associated with the TXN.", e);
                return false;
            }

            //1109203: If any threads are associated with the transaction then those associations are cleared before this method returns.
            txn = txnManager.beginSleeTransaction();
            txn.asyncCommit(null);
            txn = txnManager.getSleeTransaction();
            //if the threads associated TXN is not 'null', this is a violation of 1109203
            if (txn !=null) {
                msgSender.sendFailure(1109203, "Association with thread should have been cleared before 'asyncCommit()' returned from its call, thus 'null' should have been returned by TransactionManager.getTransaction().");
                return false;
            }
            else {
                msgSender.sendLogMsg("Association with thread was cleared before 'asyncCommit()' returned from its call as expected.");
            }


            int status;
            txn = txnManager.beginSleeTransaction();
            status = txn.getStatus();
            txn.asyncCommit(null);
            //1109202:  The transaction must be currently active (Status.STATUS_ACTIVE) or marked for
            //rollback (Status.STATUS_MARKED_ROLLBACK).
            if (status==Status.STATUS_ACTIVE || status==Status.STATUS_MARKED_ROLLBACK) {
                msgSender.sendLogMsg("TXN asyncCommit() succeeded and status was Status.STATUS_ACTIVE or Status.STATUS_MARKED_ROLLBACK as expected.");
            }
            else {
                msgSender.sendFailure(1109202, "TXN asyncCommit() succeeded but status was different from Status.STATUS_ACTIVE or Status.STATUS_MARKED_ROLLBACK.");
                return false;
            }

            //as guaranteed in one of the previous assertions, the asyncCommit method diassociates
            //its thread from the TXN before the method returns, thus at this point the
            //TXN can't be STATUS_ACTIVE or STATUS_MARKED_ROLLBACK, thus consequently an
            //IllegalStateException should be thrown
            //1109207:  java.lang.IllegalStateException - if the transaction is not in the active or marked for rollback state.
            try {
                txn.asyncCommit(null);
                status = txn.getStatus();
                msgSender.sendFailure(1109207, "asyncCommit() succeeded though current status can't have been STATUS_ACTIVE or STATUS_MARKED_ROLLBACK. Current status was: "+StatusUtil.statusToString(status));
                return false;
            } catch (IllegalStateException e) {
                msgSender.sendLogMsg("IllegalStateException thrown as expected: "+e);
            } catch (Exception e) {
                msgSender.sendFailure(1109207, "Wrong type of exception thrown, expected java.lang.IllegalStateException but caught: ", e);
                return false;
            }

            txn = txnManager.beginSleeTransaction();
            future = new Future();
            txn.asyncCommit(new MyCommitListener());
            //1109340: listener object that will receive callbacks depending on the final outcome of the transaction.
            try {
                String commitResult = (String)future.getValue(blockTimeout);
                if ("committed".equals(commitResult))
                    msgSender.sendLogMsg("CommitListener received TXN commit callback as expected.");
                else {
                    msgSender.sendFailure(1109340,"CommitListener did not receive TXN committed callback but returned '"+commitResult+"' instead.");
                    return false;
                }
            } catch (TimeoutException e) {
                msgSender.sendFailure(1109340,"CommitListener did not receive callback thus the RA test thread timed out.",e);
                return false;
            }


            txn = txnManager.beginSleeTransaction();
            future = new Future();
            txn.setRollbackOnly();
            txn.asyncCommit(new MyCommitListener());
            //1109204: if a TXN has been marked for rollback the outcome of asyncCommit is as a call to asyncRollback (i.e. a CommitListener will receive a 'rolledBack' call)
            try {
                String commitResult = (String)future.getValue(blockTimeout);
                if ("rolledBack".equals(commitResult))
                    msgSender.sendLogMsg("CommitListener received TXN rolledBack callback as expected.");
                else {
                    msgSender.sendFailure(1109204,"CommitListener did not receive TXN rolledBack callback but returned '"+commitResult+"' instead.");
                    return false;
                }
            } catch (TimeoutException e) {
                msgSender.sendFailure(1109204,"CommitListener did not receive callback thus the RA test thread timed out.",e);
                return false;
            }

        } catch (Exception e) {
            try {
                if (txn!=null)
                    txn.rollback();
            } catch (Exception ex) {
                ra.getLog().warning("Error occured when trying to rollback RA started TXN.", ex);
                msgSender.sendLogMsg("Exception occurred when trying to safely rollback RA started TXN: "+ex.getMessage());
            }
            msgSender.sendException(e);
            return false;
        }

        return true;
    }

    private BaseMessageSender msgSender;
    private RMIObjectChannel out;

}

