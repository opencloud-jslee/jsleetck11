/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.sbb.abstractclass;

import com.opencloud.sleetck.lib.resource.events.TCKResourceEventX;
import com.opencloud.sleetck.lib.sbbutils.BaseTCKSbb;
import com.opencloud.sleetck.lib.sbbutils.TCKSbbUtils;

import javax.slee.ActivityContextInterface;
import javax.slee.ChildRelation;
import javax.slee.CreateException;
import java.util.HashMap;

public abstract class Test365Sbb extends BaseTCKSbb {

    // Initial event
    public void onTCKResourceEventX1(TCKResourceEventX ev, ActivityContextInterface aci) {

        try {
            ChildRelation childRelation = getChildRelation();
            try {
                childRelation.create();
                sendResult(false,"No exception was thrown by the Child SBB's sbbPostCreate() method, has the test been altered?");
            } catch (CreateException createException) {
                if (childRelation.size() != 0) {
                    sendResult(false,"Despite CreateException being thrown in the Child SBB's sbbPostCreate() method the child SBB was still created.");
                } else if(!Test365ChildSbb.CREATE_EXCEPTION_MESSAGE.equals(createException.getMessage())) {
                    sendResult(false,"A CreateException was propagated to the caller, but it was not the exception that the "+
                            "SBB throw from sbbPostCreate(), or it was changed. Expected exception message: "+
                            Test365ChildSbb.CREATE_EXCEPTION_MESSAGE+". Found message:"+createException.getMessage());
                } else {
                    sendResult(true,"Ok: the CreateException was propagated unchanged to the caller as expected");
                }
            }
        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }

    }

    private void sendResult(boolean result, String message) throws Exception {
        HashMap map = new HashMap();
        map.put("Result", new Boolean(result));
        map.put("Message", message);
        TCKSbbUtils.getResourceInterface().sendSbbMessage(map);
    }

    // ChildRelation method for the Child SBB.
    public abstract ChildRelation getChildRelation();

}
