/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.events.convergencename.eventisunique;

import com.opencloud.sleetck.lib.resource.events.TCKResourceEventX;
import com.opencloud.sleetck.lib.sbbutils.BaseTCKSbb;
import com.opencloud.sleetck.lib.sbbutils.TCKSbbUtils;

import javax.slee.ActivityContextInterface;
import javax.slee.Address;
import javax.slee.CreateException;
import javax.slee.facilities.Level;


/**
 * SBB for testing assertion 1963.
 */
public abstract class Test1964Sbb extends BaseTCKSbb {

    /**
     * Callback to the test so the test can count the number of SBB entities created
     * @throws CreateException
     */
    public void sbbCreate() throws CreateException {
        callTest("sbbCreate");
    }

    /**
     * Ask the test using the callback if the incoming event should be re-fired.  If yes, use the SBB's fire event method
     * to re-fire the received event.
     * @param event
     * @param aci
     */
    public void onTCKResourceEventX1(TCKResourceEventX event, ActivityContextInterface aci) {
        createTrace("- onTCKResourceEventX1 -");
        Boolean testResponse = (Boolean) callTest("onTCKResourceEventX1");
        if (testResponse.booleanValue() == true) {
            createTrace("- Firing event -");
            fireTCKResourceEventX1(event, aci, null);
        }
    }

    public abstract void fireTCKResourceEventX1(TCKResourceEventX event, ActivityContextInterface aci, Address address);

    private void createTrace(String message) {
        try {
            TCKSbbUtils.createTrace(getSbbID(), Level.INFO, message, null);
        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }
    }

    private Object callTest(Object o) {
        createTrace("Synchronous call to test: " + o);
        try {
            return TCKSbbUtils.getResourceInterface().callTest(o);
        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }
        return null;
    }

    // CMP fields
    public abstract String getInitialEventID();
    public abstract void setInitialEventID(String id);
}
