/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.javax.slee.management.AlarmMBean;

import java.rmi.RemoteException;
import java.util.Map;

import javax.management.Notification;
import javax.management.NotificationListener;
import javax.management.ObjectName;
import javax.slee.ComponentID;
import javax.slee.SbbID;
import javax.slee.ServiceID;
import javax.slee.facilities.AlarmLevel;
import javax.slee.management.Alarm;
import javax.slee.management.AlarmNotification;
import javax.slee.management.DeployableUnitDescriptor;
import javax.slee.management.DeployableUnitID;
import javax.slee.management.NotificationSource;
import javax.slee.management.SbbNotification;
import javax.slee.management.UnrecognizedNotificationSourceException;

import com.opencloud.sleetck.lib.AbstractSleeTCKTest;
import com.opencloud.sleetck.lib.DescriptionKeys;
import com.opencloud.sleetck.lib.SleeTCKTestUtils;
import com.opencloud.sleetck.lib.TCKTestErrorException;
import com.opencloud.sleetck.lib.TCKTestFailureException;
import com.opencloud.sleetck.lib.TCKTestResult;
import com.opencloud.sleetck.lib.resource.TCKActivityID;
import com.opencloud.sleetck.lib.resource.TCKSbbMessage;
import com.opencloud.sleetck.lib.resource.events.TCKResourceEventX;
import com.opencloud.sleetck.lib.resource.testapi.TCKResourceListener;
import com.opencloud.sleetck.lib.resource.testapi.TCKResourceTestInterface;
import com.opencloud.sleetck.lib.testutils.BaseTCKResourceListener;
import com.opencloud.sleetck.lib.testutils.jmx.AlarmMBeanProxy;
import com.opencloud.sleetck.lib.testutils.jmx.DeploymentMBeanProxy;
import com.opencloud.sleetck.lib.testutils.jmx.SleeManagementMBeanProxy;
import com.opencloud.sleetck.lib.testutils.jmx.impl.AlarmMBeanProxyImpl;

/*
 * AssertionID(1114768): Test all methods and exceptions
 * in the AlarmMBean Javadoc
 *
 */

public class Test1114768Test extends AbstractSleeTCKTest {


    /**
     * Perform the actual test.
     */
    public TCKTestResult run() throws Exception {

        if(sbbID == null) throw new TCKTestErrorException("sbbID not found for "+DescriptionKeys.SERVICE_DU_PATH_PARAM);
        if(serviceID == null) throw new TCKTestErrorException("serviceID not found for "+DescriptionKeys.SERVICE_DU_PATH_PARAM);

        String activityName = "Test1114768Test";
        TCKResourceTestInterface resource = utils().getResourceInterface();
        TCKActivityID activityID = resource.createActivity(activityName);

        // kickoff the test
        getLog().info("Firing event: " + TCKResourceEventX.X1 );
        resource.fireEvent(TCKResourceEventX.X1, TCKResourceEventX.X1, activityID, null);

        synchronized (this) {
            wait(utils().getTestTimeout());
        }

        if (!alarmsCreated)
            result.error("Error: Alarm notification not received");

        return result;
    }


    /**
     * Do all the pre-run configuration of the test.
     */

    public void setUp() throws Exception {

        result.error("Test did not receive SBB notification");

        getLog().fine("Connecting to resource");
        resourceListener = new TCKResourceListenerImpl();
        setResourceListener(resourceListener);
        getLog().fine("Installing and activating service");

        // Install the Deployable Units
        String duPath = utils().getTestParams().getProperty(DescriptionKeys.SERVICE_DU_PATH_PARAM);
        duID = utils().install(duPath);

        // Activate the DU
        utils().activateServices(duID, true);

        SleeManagementMBeanProxy proxy = utils().getSleeManagementMBeanProxy();
        alarmMBeanName = proxy.getAlarmMBean();
        listener = new AlarmNotificationListenerImpl();
        alarmMBeanProxy = new AlarmMBeanProxyImpl(alarmMBeanName, utils().getMBeanFacade());
        alarmMBeanProxy.addNotificationListener(listener, null, null);

        DeploymentMBeanProxy duProxy = utils().getDeploymentMBeanProxy();
        DeployableUnitDescriptor duDesc = duProxy.getDescriptor(duID);
        ComponentID components[] = duDesc.getComponents();
        // Get the SbbID from the components.
        for (int i = 0; i < components.length; i++) {
            if (components[i] instanceof ServiceID) {
                getLog().fine("Setting serviceID value.");
                serviceID = (ServiceID) components[i];
                continue;
            }

            if (components[i] instanceof SbbID) {
                getLog().fine("Setting sbbID value.");
                sbbID = (SbbID) components[i];
                continue;
            }
        }

    }

    /**
     * Clean up after the test.
     */

    public void tearDown() throws Exception {

        String[] listActiveAlarms = alarmMBeanProxy.getAlarms();
        if (alarmMBeanProxy.getAlarms().length > 0)
            for (int i = 0; i < listActiveAlarms.length; i++)
                alarmMBeanProxy.clearAlarm(listActiveAlarms[i]);

        if (null != alarmMBeanProxy)
            alarmMBeanProxy.removeNotificationListener(listener);
        // cleanup
        super.tearDown();
    }

    private class TCKResourceListenerImpl extends BaseTCKResourceListener {
        // TCKResourceListener methods

        public synchronized void onSbbMessage(TCKSbbMessage message, TCKActivityID calledActivity) throws RemoteException {

            Map sbbData = (Map)message.getMessage();
            String sbbTestName = "Test1114768Test";
            Boolean sbbTestResult = (Boolean)sbbData.get("result");
            String sbbTestMessage = (String)sbbData.get("message");
            int assertionID = ((Integer)sbbData.get("id")).intValue();
            getLog().info("Received message from SBB: testname="+ sbbTestName+", result="+sbbTestResult+", message="+sbbTestMessage+", id="+assertionID);
        }

        public void onException(Exception exception) throws RemoteException {
            getLog().warning("Received Exception from SBB");
            getLog().warning(exception);
            result.error(exception);
        }
    }

    public class AlarmNotificationListenerImpl implements NotificationListener {
        public final void handleNotification(Notification notification, Object handback) {

        if (notification instanceof AlarmNotification) {
            getLog().fine("Received Alarm Notification: " +notification);

            if (!alarmsCreated) {
                AlarmNotification alarmNotification = (AlarmNotification) notification;
                //Create an Alarm object that contains information about an alarm that has
                //been raised in the SLEE.
                String alarmID = alarmNotification.getAlarmID();
                sbbNotification = (SbbNotification)alarmNotification.getNotificationSource();
                alarmType = alarmNotification.getAlarmType();
                String instanceID = alarmNotification.getInstanceID();
                AlarmLevel alarmLevel = alarmNotification.getAlarmLevel();
                String message = alarmNotification.getMessage();
                Throwable cause = alarmNotification.getCause();
                long timestamp = alarmNotification.getTimeStamp();

                getLog().fine("Creating Alarm from notification...");
                Alarm alarm = new Alarm(alarmID, sbbNotification, alarmType, instanceID, alarmLevel, message, cause, timestamp);

                try {
                    result =  doAlarmMBeanCheck(alarmMBeanProxy);
                 } catch (Exception e) {
                    getLog().warning(e);
                    result.error("Error in Alarm notification");
                }
                alarmsCreated = true;
            }
        }

        return;
        }
    }

    private TCKTestResult doAlarmMBeanCheck(AlarmMBeanProxy alarmMBeanProxy) {

        // Debug
        utils().getLog().fine("sbbNotification = " +sbbNotification.toString());
        utils().getLog().fine("Alarm Type for sbbNotification = " +alarmType);

        //getAlarms()
        try {
            getLog().fine("1114775: Starting to get all existing unique alarm identifiers for stateful alarms ");
            alarmIDs = alarmMBeanProxy.getAlarms();
            utils().getLog().info("Number of Alarm IDs in SLEE = " +alarmIDs.length);
            if (alarmIDs.length > 0)
                logSuccessfulCheck(1114775);
            else
                return TCKTestResult.failed(1114775, "alarmMBeanProxy.getAlarms failed to return any Alarms");
        } catch (Exception e) {
            getLog().warning(e);
            return TCKTestResult.failed(1114775, "alarmMBeanProxy.getAlarms failed");
        }

        // getAlarms(notificationSource)
        try {
            String[] sbbAlarmIDs = alarmMBeanProxy.getAlarms(sbbNotification);
            utils().getLog().fine("Number of Alarm IDs for sbbNotification = " +sbbAlarmIDs.length);
            // THere will be (at least) 3 alarms
            if (sbbAlarmIDs.length >= 3)
                logSuccessfulCheck(1114768);
            else
                return TCKTestResult.failed(1114768, "alarmMBeanProxy.getAlarms(sbbNotification) failed to return any Alarms");
        } catch (Exception e) {
            getLog().warning(e);
            return TCKTestResult.failed(1114768, "alarmMBeanProxy.getAlarms(sbbNotification) failed");
        }

        SbbNotification nullSbbNotification = null;
        try {
            String[] sbbAlarmIDs = alarmMBeanProxy.getAlarms(nullSbbNotification);
            return TCKTestResult.failed(1114769, "alarmMBeanProxy.getAlarms(null) failed to throw NullPointerException");
        } catch (NullPointerException e) {
            logSuccessfulCheck(1114769);
        } catch (Exception e) {
            getLog().warning(e);
            return TCKTestResult.failed(1114769, "alarmMBeanProxy.getAlarms(null) failed");
        }

        ServiceID badServiceID = new ServiceID("bad", "jain.slee.tck", "1.0");
        SbbID badSbbID = new SbbID("bad", "jain.slee.tck", "1.0");
        SbbNotification badSbbNotification1 = new SbbNotification(badServiceID, sbbID);
        SbbNotification badSbbNotification2 = new SbbNotification(serviceID, badSbbID);
        try {
            String[] sbbAlarmIDs = alarmMBeanProxy.getAlarms(badSbbNotification1);
            return TCKTestResult.failed(1114981, "alarmMBeanProxy.getAlarms failed to throw UnrecognizedNotificationSourceException");
        } catch (UnrecognizedNotificationSourceException e) {
            // Try 2nd case
            try {
                String[] sbbAlarmIDs = alarmMBeanProxy.getAlarms(badSbbNotification2);
                return TCKTestResult.failed(1114981, "alarmMBeanProxy.getAlarms failed to throw UnrecognizedNotificationSourceException");
            } catch (UnrecognizedNotificationSourceException e2) {
                logSuccessfulCheck(1114981);
            } catch (Exception e2) {
                getLog().warning(e2);
                return TCKTestResult.failed(1114981, "alarmMBeanProxy.getAlarms(badSbbNotification2) failed");
            }
        } catch (Exception e) {
            getLog().warning(e);
            return TCKTestResult.failed(1114981, "alarmMBeanProxy.getAlarms(badSbbNotification1) failed");
        }

         // getDescriptor()
        try {
            Alarm alarm = alarmMBeanProxy.getDescriptor(alarmIDs[0]);
            logSuccessfulCheck(1114983);
        } catch (Exception e) {
            getLog().warning(e);
            return TCKTestResult.failed(1114983, "alarmMBeanProxy.getDescriptor failed");
        }

        String nullAlarmID = null;
        try {
            Alarm alarm = alarmMBeanProxy.getDescriptor(nullAlarmID);
            return TCKTestResult.failed(1114984, "alarmMBeanProxy.getDescriptor(null) failed to throw NullPointerException");
        } catch (NullPointerException e) {
            logSuccessfulCheck(1114984);
        } catch (Exception e) {
            getLog().warning(e);
            return TCKTestResult.failed(1114984, "alarmMBeanProxy.getDescriptor(null) failed");
        }

        //getDescriptors
        try {
            Alarm[] alarms = alarmMBeanProxy.getDescriptors(alarmIDs);
            if (alarms.length > 0)
                logSuccessfulCheck(1114987);
            else
                return TCKTestResult.failed(1114987, "alarmMBeanProxy.getDescriptors failed to return alarms");
        } catch (Exception e) {
            getLog().warning(e);
            return TCKTestResult.failed(1114987, "alarmMBeanProxy.getDescriptors failed");
        }

        String[] nullAlarmIDs = null;
        try {
            Alarm[] alarms = alarmMBeanProxy.getDescriptors(nullAlarmIDs);
            return TCKTestResult.failed(1114988, "alarmMBeanProxy.getDescriptor(null) failed to throw NullPointerException");
        } catch (NullPointerException e) {
            logSuccessfulCheck(1114988);
        } catch (Exception e) {
            getLog().warning(e);
            return TCKTestResult.failed(1114988, "alarmMBeanProxy.getDescriptor(null) failed");
        }

        // clearAlarm(alarmID) - first alarm
        try {
            boolean cleared = alarmMBeanProxy.clearAlarm(alarmIDs[0]);
            if (cleared)
                logSuccessfulCheck(1114760);
            else
                return TCKTestResult.failed(1114760, "alarmMBeanProxy.clearAlarm failed to clear Alarm");
        } catch (Exception e) {
            getLog().warning(e);
            return TCKTestResult.failed(1114760, "alarmMBeanProxy.clearAlarm failed");
        }

        try {
            boolean cleared = alarmMBeanProxy.clearAlarm(nullAlarmID);
            return TCKTestResult.failed(1114761, "alarmMBeanProxy.clearAlarms(null) failed");
        } catch (NullPointerException e) {
            logSuccessfulCheck(1114761);
        } catch (Exception e) {
            getLog().warning(e);
            return TCKTestResult.failed(1114761, "alarmMBeanProxy.clearAlarms(null) failed");
        }


        // clearAlarms(notificationSource, AlarmID) 2nd Alarm
        try {
            String[] sbbAlarmIDs = alarmMBeanProxy.getAlarms(sbbNotification);
            utils().getLog().fine("Number of Alarm IDs for sbbNotification = " +sbbAlarmIDs.length);
            utils().getLog().fine("Alarm Type for sbbNotification = " +alarmType);
            int numberCleared = alarmMBeanProxy.clearAlarms(sbbNotification, alarmType);
            // Should clear the remaining 2 alarms of this alarmType
            if (numberCleared == 2)
                logSuccessfulCheck(1114772);
            else
                return TCKTestResult.failed(1114772, "alarmMBeanProxy.clearAlarms failed to clear Alarm");
        } catch (Exception e) {
            getLog().warning(e);
            return TCKTestResult.failed(1114772, "alarmMBeanProxy.clearAlarms failed");
        }

        String nullAlarmType = null;
        try {
            int numberCleared = alarmMBeanProxy.clearAlarms(nullSbbNotification, alarmType);
            return TCKTestResult.failed(1114773, "alarmMBeanProxy.clearAlarms failed to throw NullPointerException");
        } catch (NullPointerException e) {
            try {
                int numberCleared = alarmMBeanProxy.clearAlarms(sbbNotification, nullAlarmType);
                return TCKTestResult.failed(1114773, "alarmMBeanProxy.clearAlarms failed to throw NullPointerException");
            } catch (NullPointerException e2) {
                logSuccessfulCheck(1114773);
            } catch (Exception e2) {
                getLog().warning(e2);
                return TCKTestResult.failed(1114773, "alarmMBeanProxy.clearAlarms failed");
            }
        } catch (Exception e) {
            getLog().warning(e);
            return TCKTestResult.failed(1114773, "alarmMBeanProxy.clearAlarms failed");
        }

        try {
            int numberCleared = alarmMBeanProxy.clearAlarms(badSbbNotification1, alarmType);
            return TCKTestResult.failed(1114980, "alarmMBeanProxy.clearAlarms failed to throw UnrecognizedNotificationSourceException");
        } catch (UnrecognizedNotificationSourceException e) {
            logSuccessfulCheck(1114980);
        } catch (Exception e) {
            getLog().warning(e);
            return TCKTestResult.failed(1114980, "alarmMBeanProxy.clearAlarms failed");
        }

        // clearAlarms(notificationSource) - remaining alarms
        try {
               String[] sbbAlarmIDs = alarmMBeanProxy.getAlarms(sbbNotification);
            utils().getLog().fine("Number of Alarm IDs for sbbNotification = " +sbbAlarmIDs.length);
            int numberCleared = alarmMBeanProxy.clearAlarms(sbbNotification);
            // THere should be no alarms left to clear
            if (numberCleared == 0)
                logSuccessfulCheck(1114764);
            else
                return TCKTestResult.failed(1114764, "alarmMBeanProxy.clearAlarms failed to clear Alarms");
        } catch (Exception e) {
            getLog().warning(e);
            return TCKTestResult.failed(1114764, "alarmMBeanProxy.clearAlarms failed");
        }

        try {
            int numberCleared = alarmMBeanProxy.clearAlarms(nullSbbNotification);
            return TCKTestResult.failed(1114765, "alarmMBeanProxy.clearAlarms(null) failed to throw NullPointerException");
        } catch (NullPointerException e) {
            logSuccessfulCheck(1114765);
        } catch (Exception e) {
            getLog().warning(e);
            return TCKTestResult.failed(1114765, "alarmMBeanProxy.clearAlarms(null) failed");
        }

        try {
            int numberCleared = alarmMBeanProxy.clearAlarms(badSbbNotification1);
            return TCKTestResult.failed(1114769, "alarmMBeanProxy.clearAlarms failed to throw UnrecognizedNotificationSourceException");
        } catch (UnrecognizedNotificationSourceException e) {
            logSuccessfulCheck(1114769);
        } catch (Exception e) {
            getLog().warning(e);
            return TCKTestResult.failed(1114769, "alarmMBeanProxy.clearAlarms failed");
        }


        return TCKTestResult.passed();

    }

    private void logSuccessfulCheck(int assertionID) {
        utils().getLog().info("Check for assertion "+assertionID+" OK");
    }

    private TCKResourceListener resourceListener;
    private NotificationListener listener;
    private DeployableUnitID duID;
    private AlarmMBeanProxy alarmMBeanProxy;
    private ObjectName alarmMBeanName;
    private TCKTestResult result;
    private SbbID sbbID;
    private ServiceID serviceID;
    private SbbNotification sbbNotification;
    private int AlarmInstance;
    private String[] alarmIDs;
    private String alarmType;
    private boolean alarmsCreated = false;


}
