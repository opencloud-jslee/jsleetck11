/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.sbb.abstractclass;

import java.util.HashMap;

import javax.slee.ActivityContextInterface;
import javax.slee.RolledBackContext;

import com.opencloud.sleetck.lib.resource.events.TCKResourceEventX;
import com.opencloud.sleetck.lib.sbbutils.BaseTCKSbb;
import com.opencloud.sleetck.lib.sbbutils.TCKSbbUtils;

public abstract class Test2121Sbb extends BaseTCKSbb {

    public void sbbRolledBack(RolledBackContext context) {

        try {
            HashMap map = new HashMap();
            map.put("Type", "RollbackTXN");
            map.put("TXN", TCKSbbUtils.getResourceAdaptorInterface().getTransactionIDAccess().getCurrentTransactionID());
            TCKSbbUtils.getResourceInterface().sendSbbMessage(map);
        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }
    }

    // Initial event
    public void onTCKResourceEventX1(TCKResourceEventX ev, ActivityContextInterface aci) {

        try {
            HashMap map = new HashMap();
            map.put("Type", "InitialTXN");
            map.put("TXN", TCKSbbUtils.getResourceAdaptorInterface().getTransactionIDAccess().getCurrentTransactionID());
            TCKSbbUtils.getResourceInterface().sendSbbMessage(map);
            getSbbContext().setRollbackOnly();
        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }
    }

}
