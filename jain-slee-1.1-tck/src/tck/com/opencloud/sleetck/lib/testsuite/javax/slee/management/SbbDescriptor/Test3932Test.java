/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.javax.slee.management.SbbDescriptor;

import com.opencloud.sleetck.lib.SleeTCKTest;
import com.opencloud.sleetck.lib.SleeTCKTestUtils;
import com.opencloud.sleetck.lib.TCKTestResult;
import com.opencloud.sleetck.lib.testutils.jmx.DeploymentMBeanProxy;

import javax.slee.ComponentID;
import javax.slee.SbbID;
import javax.slee.management.ComponentDescriptor;
import javax.slee.management.DeployableUnitDescriptor;
import javax.slee.management.DeployableUnitID;
import javax.slee.management.SbbDescriptor;

public class Test3932Test implements SleeTCKTest {

    private static final String SERVICE_DU_PATH_PARAM = "DUPath";

    public void init(SleeTCKTestUtils utils) {
        this.utils = utils;
    }

    /**
     * Perform the actual test.
     */

    public TCKTestResult run() throws Exception {

        DeploymentMBeanProxy duProxy = utils.getDeploymentMBeanProxy();

        // Get the array of ComponentID objects.
        DeployableUnitDescriptor duDesc = duProxy.getDescriptor(duID);
        ComponentID components[] = duDesc.getComponents();
        ComponentDescriptor descriptors[] = duProxy.getDescriptors(components);

        for (int i = 0; i < descriptors.length; i++) {
            if (descriptors[i] instanceof SbbDescriptor) {

                SbbDescriptor sbbDesc = (SbbDescriptor) descriptors[i];
                if (sbbDesc.getName().equals("Test3932Test")) {
                    SbbID children[] = sbbDesc.getSbbs();
                    if (children.length != 1)
                        return TCKTestResult.failed(4759, "SbbDescriptor.getSbbs() returned incorrect number of children.");

                    try {
                        sbbDesc.getEventTypes();
                    } catch (Exception e) {
                        return TCKTestResult.failed(3934, "SbbDescriptor.getEventTypes() threw an exception.");
                    }

                    try {
                        sbbDesc.getProfileSpecifications();
                    } catch (Exception e) {
                        return TCKTestResult.failed(3936, "SbbDescriptor.getProfileSpecifications() threw an exception.");
                    }

                    try {
                        sbbDesc.getResourceAdaptorTypes();
                    } catch (Exception e) {
                        return TCKTestResult.failed(3938, "SbbDescriptor.getResourceAdaptorTypes() threw an exception.");
                    }
                    
                    return TCKTestResult.passed();
                    
                }
            }
        }

        return TCKTestResult.error("Failed to find the SbbDescriptor for the installed root SBB.");
    }

    /**
     * Do all the pre-run configuration of the test.
     */

    public void setUp() throws Exception {

        // Install the Deployable Units
        String duPath = utils.getTestParams().getProperty(SERVICE_DU_PATH_PARAM);
        duID = utils.install(duPath);

        // Activate the DU
        utils.activateServices(duID, true);

    }

    /**
     * Clean up after the test.
     */

    public void tearDown() throws Exception {
        utils.getLog().fine("Disconnecting from resource");
        utils.getResourceInterface().clearActivities();
        utils.getResourceInterface().removeResourceListener();
        utils.getLog().fine("Deactivating and uninstalling service");
        utils.deactivateAllServices();
        utils.uninstallAll();
    }

    private SleeTCKTestUtils utils;
    private DeployableUnitID duID;
}
