/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.profiles.profileabstractclass;

import javax.slee.profile.ProfileLocalObject;

/**
 * 3 business methods defined. Implementation in Profile abstract class has methods with matching signatures
 */

public interface Test1110093_9ProfileLocal extends ProfileLocalObject {
    public void business1();
    public int business2(int dummy);
    public String business3(int dummy, String dummy2);
}
