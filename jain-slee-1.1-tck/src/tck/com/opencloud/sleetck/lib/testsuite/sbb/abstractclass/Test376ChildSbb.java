/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.sbb.abstractclass;

import java.util.HashMap;

import com.opencloud.sleetck.lib.sbbutils.BaseTCKSbb;
import com.opencloud.sleetck.lib.sbbutils.TCKSbbUtils;

public abstract class Test376ChildSbb extends BaseTCKSbb {

    public void sbbRemove() {

    try {
            HashMap map = new HashMap();

            Object currentTransaction = TCKSbbUtils.getResourceAdaptorInterface().getTransactionIDAccess().getCurrentTransactionID();
            if (currentTransaction != null) {
                if (currentTransaction.equals(getTxnID())) {
                    map.put("Result", new Boolean(true));
                    map.put("Message", "Ok");
                } else {
                    map.put("Result", new Boolean(false));
                    map.put("Message", "sbbRemove() was not executed in the same TXN as the parent's method that called child.remove()");
                    map.put("ID", new Integer(3209));
                }
            } else {
                map.put("Result", new Boolean(false));
                map.put("Message", "sbbCreate() was not executed with a valid transaction ID");
                map.put("ID", new Integer(376));
            }

            TCKSbbUtils.getResourceInterface().sendSbbMessage(map);
        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }

    }

    public void setParentTxnID(Object txnID) {
        setTxnID(txnID);
    }

    public abstract void setTxnID(Object txnID);
    public abstract Object getTxnID();

}
