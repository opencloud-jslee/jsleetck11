/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.facilities.alarmfacility;

import java.util.HashMap;

import javax.naming.InitialContext;
import javax.slee.ActivityContextInterface;
import javax.slee.facilities.AlarmFacility;
import javax.slee.facilities.AlarmLevel;
import javax.slee.facilities.Tracer;

import com.opencloud.sleetck.lib.resource.events.TCKResourceEventX;
import com.opencloud.sleetck.lib.sbbutils.BaseTCKSbb;
import com.opencloud.sleetck.lib.sbbutils.TCKSbbUtils;

/*  AssertionID(1113438): Test Requests to raise an alarm that is already
 * raised or to clear an alarm that is not currently raised cause no further
 * action in the SLEE, ie. notifications are not generated in this case.
 *
 */
public abstract class Test1113438Sbb extends BaseTCKSbb {

    private static final String JNDI_ALARMFACILITY_NAME = AlarmFacility.JNDI_NAME;

    public static final String ALARM_MESSAGE = "Test1113438AlarmMessage";

    public static final String ALARM_INSTANCEID = "Test1113438AlarmInstanceID";

    // Initial event
    public void onTCKResourceEventX1(TCKResourceEventX event, ActivityContextInterface aci) {

        try {
            sbbTracer = getSbbContext().getTracer("com.test");
            sbbTracer.info("Received " + event + " message.");
            AlarmFacility facility = (AlarmFacility) new InitialContext().lookup(JNDI_ALARMFACILITY_NAME);
            setFirstAlarm(facility.raiseAlarm("javax.slee.management.Alarm", ALARM_INSTANCEID, AlarmLevel.MAJOR,
                    ALARM_MESSAGE));

        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }

    }

    // Second event
    public void onTCKResourceEventX2(TCKResourceEventX event, ActivityContextInterface aci) {

        try {
            sbbTracer = getSbbContext().getTracer("com.test");
            sbbTracer.info("Received " + event + " message.");
            AlarmFacility facility = (AlarmFacility) new InitialContext().lookup(JNDI_ALARMFACILITY_NAME);
            String oldAlarmID = getFirstAlarm();
            String newAlarmID = facility.raiseAlarm("javax.slee.management.Alarm", ALARM_INSTANCEID,
                    AlarmLevel.CRITICAL, ALARM_MESSAGE);
            if (!newAlarmID.equals(oldAlarmID))
                sendResultToTCK("Test1113438Test", false,
                        1113438,
                        "If an alarm with the same identifying attributes (notification source, alarm type, and instance ID) "
                                + "is already active in the SLEE, this method has no effect and the alarm identifier of the existing active alarm "
                                + "is returned. But it didn't return the alarm identifier of the existing active alarm.");

            // We need to clear this alarm first, so that the duplicated clear
            // will fail later.
            if (!facility.clearAlarm(getFirstAlarm()))
                sendResultToTCK("Test1113438Test", false, 1113438, "The alarm which was raised cannot be cleared!");

        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }

    }

    // Third event
    public void onTCKResourceEventX3(TCKResourceEventX event, ActivityContextInterface aci) {

        try {
            sbbTracer = getSbbContext().getTracer("com.test");
            sbbTracer.info("Received " + event + " message.");
            AlarmFacility facility = (AlarmFacility) new InitialContext().lookup(JNDI_ALARMFACILITY_NAME);

            if (facility.clearAlarm(getFirstAlarm()))
                sendResultToTCK("Test1113438Test", false, 1113438, "This alarm has been already cleared previously, there is no point"
                        + "to clear it again!");

        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }

    }

    private void sendResultToTCK(String testName, boolean result, int failedAssertionID,  String message) throws Exception {
        HashMap sbbData = new HashMap();
        sbbData.put("testname", testName);
        sbbData.put("result", result ? "pass" : "fail");
        sbbData.put("message", message);
        sbbData.put("id", new Integer(failedAssertionID));
        TCKSbbUtils.getResourceInterface().sendSbbMessage(sbbData);
    }

    public abstract void setFirstAlarm(String alarmID);

    public abstract String getFirstAlarm();

    private Tracer sbbTracer;
}
