/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.usage.SbbUsageMBean;

import com.opencloud.sleetck.lib.TCKTestErrorException;

import javax.slee.SbbLocalObject;
import javax.slee.usage.UnrecognizedUsageParameterSetNameException;
import javax.naming.NamingException;

public interface Test2365SbbChildLocal extends SbbLocalObject {

     public void doUpdates() throws NamingException, TCKTestErrorException, UnrecognizedUsageParameterSetNameException;

}
