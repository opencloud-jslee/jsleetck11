/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.sbb.activitycontext;

import java.util.HashMap;

import javax.slee.ActivityContextInterface;
import javax.slee.SLEEException;
import javax.slee.SbbLocalObject;
import javax.slee.TransactionRequiredLocalException;
import javax.slee.TransactionRolledbackLocalException;

import com.opencloud.sleetck.lib.resource.events.TCKResourceEventX;
import com.opencloud.sleetck.lib.sbbutils.BaseTCKSbb;
import com.opencloud.sleetck.lib.sbbutils.TCKSbbUtils;

public abstract class Test1106029Sbb extends BaseTCKSbb {
    private final static String nullACIName = "java:comp/env/slee/nullactivity/factory";
    private final static String nullACIFactoryName = "java:comp/env/slee/nullactivity/activitycontextinterfacefactory";

    /*
     * This is the initial event.
     * 
     * Test whether an activity context stored in a CMP field is null
     * after the activity has ended. 
     */
    public void onTCKResourceEventX1(TCKResourceEventX ev, ActivityContextInterface aci) {
        HashMap map = new HashMap();

        try {
            // Try the null case.
            setMyActivityContext(null);
            if (null != getMyActivityContext()) {
                // test failed
                map.put("Result", new Boolean(false));
                map.put("ID", new Integer(1106029));
                map.put("Message", "Null ActivityContext stored in CMP field was not null.");
                TCKSbbUtils.getResourceInterface().sendSbbMessage(map);
                return;
            }

            // Try making a fake AC. 
            MyActivityContext myAC;
            myAC = new MyActivityContext();
            try {
                setMyActivityContext(myAC);
                // test failed
                map.put("Result", new Boolean(false));
                map.put("ID", new Integer(1106029));
                map.put("Message", "Setting activity context CMP field with invalid activity did not throw exception.");
                TCKSbbUtils.getResourceInterface().sendSbbMessage(map);
                return;
            } catch (IllegalArgumentException e) {
                // good.
            }                

            // Try positive case.
            try {
                setMyActivityContext(asSbbActivityContextInterface(aci));
            } catch (IllegalArgumentException e) {
                map.put("Result", new Boolean(false));
                map.put("ID", new Integer(1106029));
                map.put("Message", "Setting valid activity context threw a IllegalArgumentException.");
                TCKSbbUtils.getResourceInterface().sendSbbMessage(map);
                return;
            }
            
            // test passed.
            map.put("Result", new Boolean(true));
            map.put("ID", new Integer(1106029));
            map.put("Message", "Test passed.");
            TCKSbbUtils.getResourceInterface().sendSbbMessage(map);
            return;

        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }
    }


    /*
     * This class exists only to make an activity context which isn't
     * an activity context.
     */
    private class MyActivityContext implements Test1106029ActivityContextInterface {
        public String getValue() {
            // return a value.
            return "hello world";
        }

        public void setValue(String newValue) {
            // do Nothing...
            // This is a dummy method.
        }

        // Methods from
        // javax.slee.ActivityContextInterface:
        public Object getActivity()
        throws TransactionRequiredLocalException, SLEEException
        {
            return null;
        }

        public void attach(SbbLocalObject sbb)
        throws NullPointerException, 
        TransactionRequiredLocalException, 
        TransactionRolledbackLocalException,
        SLEEException {
            return;
        }

        public void detach(SbbLocalObject sbb)
        throws NullPointerException, 
        TransactionRequiredLocalException, 
        TransactionRolledbackLocalException,
        SLEEException {
            return;
        }

        public boolean isAttached(SbbLocalObject sbb)
        throws NullPointerException, 
        TransactionRequiredLocalException, 
        TransactionRolledbackLocalException,
        SLEEException {
            return false;
        }

        public boolean isEnding()
        throws TransactionRequiredLocalException, 
        SLEEException {
            return false;
        }
    }


    public abstract void setMyActivityContext(Test1106029ActivityContextInterface object);
    public abstract Test1106029ActivityContextInterface getMyActivityContext();

    public abstract Test1106029ActivityContextInterface 
        asSbbActivityContextInterface(ActivityContextInterface aci);
}

