/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.facilities.timerfacility;

import com.opencloud.sleetck.lib.resource.events.TCKResourceEventX;
import com.opencloud.sleetck.lib.sbbutils.BaseTCKSbb;
import com.opencloud.sleetck.lib.sbbutils.TCKSbbUtils;

import javax.slee.ActivityContextInterface;
import javax.slee.facilities.TimerOptions;
import javax.slee.facilities.TimerPreserveMissed;
import java.util.HashMap;

public abstract class Test1235Sbb extends BaseTCKSbb {

    // Initial event
    public void onTCKResourceEventX1(TCKResourceEventX ev, ActivityContextInterface aci) {

        try {
            HashMap map = new HashMap();

            boolean passed = false;
            TimerOptions options = new TimerOptions();
            try {
                options.setTimeout(-1);
            } catch (java.lang.IllegalArgumentException e) {
                passed = true;
            }
            
            if (!passed) {
                map.put("Result", new Boolean(false));
                map.put("Message", "Passing a negative value to TimerOptions.setTimeout() didn't cause IllegalArgumentException to be thrown.");
                TCKSbbUtils.getResourceInterface().sendSbbMessage(map);
                return;
            }

            passed = false;
            try {
                options = new TimerOptions(false, -1, TimerPreserveMissed.ALL);
            } catch (java.lang.IllegalArgumentException e) {
                passed = true;
            }            

            if (!passed) {
                map.put("Result", new Boolean(false));
                map.put("Message", "Passing a negative value to TimerOptions() didn't cause IllegalArgumentException to be thrown.");
                TCKSbbUtils.getResourceInterface().sendSbbMessage(map);
                return;
            }

            map.put("Result", new Boolean(true));
            map.put("Message", "Ok");
            TCKSbbUtils.getResourceInterface().sendSbbMessage(map);

        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }

    }

}
