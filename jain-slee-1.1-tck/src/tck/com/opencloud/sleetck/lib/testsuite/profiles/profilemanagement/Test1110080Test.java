/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.profiles.profilemanagement;

import javax.management.ObjectName;
import javax.management.ReflectionException;
import javax.slee.InvalidStateException;
import javax.slee.management.ManagementException;
import javax.slee.profile.ProfileSpecificationID;
import javax.slee.profile.ProfileVerificationException;
import javax.slee.profile.UnrecognizedProfileTableNameException;

import com.opencloud.sleetck.lib.AbstractSleeTCKTest;
import com.opencloud.sleetck.lib.TCKTestErrorException;
import com.opencloud.sleetck.lib.TCKTestResult;
import com.opencloud.sleetck.lib.infra.SleeTCKComponentConstants;
import com.opencloud.sleetck.lib.testutils.Assert;
import com.opencloud.sleetck.lib.testutils.ProfileUtils;
import com.opencloud.sleetck.lib.testutils.jmx.ProfileMBeanProxy;
import com.opencloud.sleetck.lib.testutils.jmx.ProfileProvisioningMBeanProxy;


public class Test1110080Test extends AbstractSleeTCKTest {

    private static final String MNGMT_DEFINED_DU_PATH_PARAM= "MngmtDefinedDUPath";
    private static final String MNGMT_EQUALS_CMP_DU_PATH_PARAM= "MngmtEqualsCMPDUPath";
    private static final String NO_MNGMT_DEFINED_DU_PATH_PARAM= "NoMngmtDefinedDUPath";

    private static final String SPEC_NAME_1 = "Test1110080_1Profile";
    private static final String SPEC_NAME_2 = "Test1110080_2Profile";
    private static final String SPEC_NAME_3 = "Test1110080_3Profile";
    private static final String SPEC_VERSION = "1.0";

    private static final String PROFILE_TABLE_NAME = "Test1110080ProfileTable";
    private static final String PROFILE_NAME = "Test1110080Profile";

    private static final int TEST_ID = 1110080;

    private void setupServiceAndProfile(String specName, String DUPathParam) throws Exception {
        //Deploy profile spec
        setupService(DUPathParam);

        //Create a profile table
        ProfileSpecificationID specID = new ProfileSpecificationID(specName, SleeTCKComponentConstants.TCK_VENDOR, SPEC_VERSION);
        profileProvisioning.createProfileTable(specID, PROFILE_TABLE_NAME);
        getLog().fine("Added profile table "+PROFILE_TABLE_NAME);

        //Create a profile via management view
        profile = profileProvisioning.createProfile(PROFILE_TABLE_NAME, PROFILE_NAME);
        profileProxy = utils().getMBeanProxyFactory().createProfileMBeanProxy(profile);
        getLog().fine("Created profile "+PROFILE_NAME+" for profile table "+PROFILE_TABLE_NAME);
    }

    private void removeServiceAndProfile() throws TCKTestErrorException,
                                                    InvalidStateException,
                                                    ProfileVerificationException,
                                                    ManagementException,
                                                    NullPointerException,
                                                    UnrecognizedProfileTableNameException {
        //commit and close the Profile
        profileProxy.commitProfile();
        profileProxy.closeProfile();
        getLog().fine("Commit and close profile "+PROFILE_NAME);

        //remove profile table
        profileProvisioning.removeProfileTable(PROFILE_TABLE_NAME);

        utils().uninstallAll();
    }

    /**
     *
     */
    public TCKTestResult run() throws Exception {

        //Run tests for spec with distinct ProfileManagement interface
        setupServiceAndProfile(SPEC_NAME_1, MNGMT_DEFINED_DU_PATH_PARAM);
        getLog().fine("Run tests for profile spec "+SPEC_NAME_1);
        try {
            //invoke set value method declared in ProfileManagement interface
            utils().getMBeanFacade().invoke(profile, "setValue", new Object[]{"42"}, new String[]{"java.lang.String"});
            utils().getMBeanFacade().invoke(profile, "getValue", new Object[]{}, new String[]{});
            getLog().fine("Called get/set accessors on ProfileManagement interface.");
        }
        catch (ReflectionException e) {
            Assert.fail(1110080, "Method not found when invoking accessor method declared in ProfileManagement interface.");
        }

        try {
            //invoke management method declared in ProfileManagement interface
            utils().getMBeanFacade().invoke(profile, "manage", new Object[]{}, new String[]{});
            getLog().fine("Called management method on ProfileManagement interface.");
        }
        catch (ReflectionException e) {
            Assert.fail(1110080, "Method not found when invoking management method declared in ProfileManagement interface.");
        }

        try {
            //try to invoke set method NOT defined in ProfileManagement interface but defined in CMP interface
            utils().getMBeanFacade().invoke(profile, "setValue2", new Object[]{"42"}, new String[]{"java.lang.String"});
            Assert.fail(1110080, "Method should NOT have been found when invoking profile accessor method which is NOT declared in ProfileManagement interface but defined in ProfileCMP interface.");
        }
        catch (ReflectionException e) {
            getLog().fine("Call to CMP accessor method which is not declared in ProfileManagement interface failed as expected: "+e.getClass().getName());
        }
        removeServiceAndProfile();



        //Run tests for spec with ProfileManagement interface is identical to ProfileCMP interface.
        setupServiceAndProfile(SPEC_NAME_2, MNGMT_EQUALS_CMP_DU_PATH_PARAM);
        getLog().fine("Run tests for profile spec "+SPEC_NAME_2);
        try {
            //invoke set value method declared in ProfileManagement interface
            utils().getMBeanFacade().invoke(profile, "setValue", new Object[]{"42"}, new String[]{"java.lang.String"});
            utils().getMBeanFacade().invoke(profile, "getValue", new Object[]{}, new String[]{});
            getLog().fine("Called get/set accessors on ProfileManagement interface that is identical to ProfileCMP interface.");
        }
        catch (ReflectionException e) {
            Assert.fail(1110081, "Method not found when invoking accessor method declared in ProfileManagement interface that is identical to ProfileCMP interface.");
        }

        try {
            //invoke further set method defined in ProfileManagement interface but defined in CMP interface
            utils().getMBeanFacade().invoke(profile, "setValue2", new Object[]{"42"}, new String[]{"java.lang.String"});
            utils().getMBeanFacade().invoke(profile, "getValue2", new Object[]{}, new String[]{});
            getLog().fine("Called get/set accessors on ProfileManagement interface that is identical to ProfileCMP interface.");
        }
        catch (ReflectionException e) {
            Assert.fail(1110081, "Method not found when invoking accessor method declared in ProfileManagement interface that is identical to ProfileCMP interface.");
        }
        removeServiceAndProfile();



        //Run tests for spec with ProfileManagement interface not defined at all.
        setupServiceAndProfile(SPEC_NAME_3, NO_MNGMT_DEFINED_DU_PATH_PARAM);
        getLog().fine("Run tests for profile spec "+SPEC_NAME_3);
        try {
            //invoke set value method declared in ProfileManagement interface
            utils().getMBeanFacade().invoke(profile, "setValue", new Object[]{"42"}, new String[]{"java.lang.String"});
            utils().getMBeanFacade().invoke(profile, "getValue", new Object[]{}, new String[]{});
            getLog().fine("Called get/set accessors for profile spec which does not define a custom ProfileManagement interface.");
        }
        catch (ReflectionException e) {
            Assert.fail(1110081, "Method not found when invoking accessor methods for profile spec which does not define a custom ProfileManagement interface.");
        }

        try {
            //invoke further set method defined in ProfileManagement interface but defined in CMP interface
            utils().getMBeanFacade().invoke(profile, "setValue2", new Object[]{"42"}, new String[]{"java.lang.String"});
            utils().getMBeanFacade().invoke(profile, "getValue2", new Object[]{}, new String[]{});
            getLog().fine("Called get/set accessors for profile spec which does not define a custom ProfileManagement interface.");
        }
        catch (ReflectionException e) {
            Assert.fail(1110081, "Method not found when invoking accessor methods for profile spec which does not define a custom ProfileManagement interface.");
        }
        removeServiceAndProfile();


        return TCKTestResult.passed();
    }


    public void setUp() throws Exception {

        profileUtils = new ProfileUtils(utils());
        profileProvisioning = profileUtils.getProfileProvisioningProxy();

    }

    public void tearDown() throws Exception {


        super.tearDown();
    }

    private ProfileUtils profileUtils;
    private ProfileProvisioningMBeanProxy profileProvisioning;
    private ObjectName profile;
    private ProfileMBeanProxy profileProxy;
}
