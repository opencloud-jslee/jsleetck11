/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.transactions;

import com.opencloud.sleetck.lib.AbstractSleeTCKTest;
import com.opencloud.sleetck.lib.TCKTestResult;
import com.opencloud.sleetck.lib.OperationTimedOutException;
import com.opencloud.sleetck.lib.testutils.QueuingResourceListener;
import com.opencloud.sleetck.lib.testutils.Assert;
import com.opencloud.sleetck.lib.resource.TCKActivityID;
import com.opencloud.sleetck.lib.resource.events.TCKResourceEventX;

/**
 * Test assertion 1993: that because fire event methods are transactional an event fired in a transaction that is
 * rolled back will never be delivered.
 * The test fires event X1 to the Sbb, the Sbb's onX1 fires X2 and rollsback.  The Sbb's X2 sends a response to the
 * test if X2 is delivered.  The test passes only if this response is never received (within the test timeout).
 */
public class Test1993Test extends AbstractSleeTCKTest {
    public TCKTestResult run() throws Exception {
        QueuingResourceListener listener = new QueuingResourceListener(utils());
        setResourceListener(listener);

        TCKActivityID activity = utils().getResourceInterface().createActivity("Test1993Activity");
        utils().getResourceInterface().fireEvent(TCKResourceEventX.X1, null, activity, null);

        try {
            String message = (String) listener.nextMessage().getMessage();
            getLog().info("Got unexpected result from Sbb: " + message);
            Assert.fail(1993, "An event fired by invoking a fire event method from a transaction that was rolled back " +
                              "should not be delivered");
        } catch (OperationTimedOutException e) {
            getLog().info("Got expected timeout exception waiting for result from Sbb.");
        }
        return TCKTestResult.passed();

    }
}
