/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.javax.slee.facilities.TimerOptions;

import java.util.HashMap;

import javax.slee.ActivityContextInterface;
import javax.slee.facilities.TimerOptions;
import javax.slee.facilities.Tracer;

import com.opencloud.sleetck.lib.resource.events.TCKResourceEventX;
import com.opencloud.sleetck.lib.sbbutils.BaseTCKSbb;
import com.opencloud.sleetck.lib.sbbutils.TCKSbbUtils;

/*
 * AssertionID(1113098): Test java.lang.NullPointerException 
 * - if preserveMissed is null.
 */
public abstract class Test1113098Sbb extends BaseTCKSbb {

    // Initial event
    public void onTCKResourceEventX1(TCKResourceEventX event, ActivityContextInterface aci) {

        try {
            tracer = this.getSbbContext().getTracer("com.test");
            tracer.info("Received " + event + " message", null);
            doTest1113098Test();

        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }
    }

    private void sendResultToTCK(String testName, boolean result, int failedAssertionID,  String message) throws Exception {
        HashMap sbbData = new HashMap();
        sbbData.put("testname", testName);
        sbbData.put("result", result ? "pass" : "fail");
        sbbData.put("message", message);
        sbbData.put("id", new Integer(failedAssertionID));
        TCKSbbUtils.getResourceInterface().sendSbbMessage(sbbData);
    }


    private void doTest1113098Test() throws Exception {
        TimerOptions timerOptions = null;
        boolean passed = false;
        //1113098
        try {
            timerOptions = new TimerOptions(1000, null);
        } catch (NullPointerException iae) {
            passed = true;
        } catch (Exception e) {
            tracer.info("got unexpected Exception: " + e, null);
            TCKSbbUtils.handleException(e);
        }

        if (passed == true) {
            tracer.info("got expected NullPointerException: ", null);
            sendResultToTCK("Test1113098Test", true, 1113098, "Got expected IllegalArgumentException if timeout is less than zero.");
        } else {
            tracer.info("got unexpected error", null);
            sendResultToTCK("Test1113098Test", false, 1113098, "Invalid error returned for preserveMissed is null");
        }
    }

    private Tracer tracer;
}
