/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.sbb.activitycontext;

import java.util.HashMap;

import javax.slee.ActivityContextInterface;
import javax.slee.EventContext;

import com.opencloud.sleetck.lib.resource.events.TCKResourceEventX;
import com.opencloud.sleetck.lib.sbbutils.BaseTCKSbb;
import com.opencloud.sleetck.lib.sbbutils.TCKSbbUtils;

public abstract class Test1106030Sbb extends BaseTCKSbb {

    /*
     * This is the initial event.
     * 
     * Test whether an activity context stored in a CMP field is null
     * after the activity has ended. 
     */
    public void onTCKResourceEventX1(TCKResourceEventX ev, ActivityContextInterface aci, EventContext eventContext) {
        HashMap map;
        map = new HashMap();

        // Try the null case.
        setMyEventContext(null);
        if (null != getMyEventContext()) {
            // test failed
            map.put("Result", new Boolean(false));
            map.put("ID", new Integer(1106030));
            map.put("Message", "Null ActivityContext stored in CMP field was not null.");
            sendSbbMessage(map);
            return;
        }

        // Set the Event context. 
        setMyEventContext(eventContext);
        map.put("Result", new Boolean(true));
        map.put("Message", "Event Context now in CMP field.");
        sendSbbMessage(map);
        return;
    }


    public void onTCKResourceEventX2(TCKResourceEventX ev, ActivityContextInterface aci, EventContext eventContext) {
        HashMap map;
        EventContext oldEventContext;
        map = new HashMap();

        oldEventContext = getMyEventContext();
        if (null != oldEventContext) {
            // test failed
            map.put("Result", new Boolean(false));
            map.put("ID", new Integer(1106030));
            map.put("Message", "Null ActivityContext stored in CMP field was not null.");
            sendSbbMessage(map);
            return;
        }

        // Test has passed.
        map.put("Result", new Boolean(true));
        map.put("ID", new Integer(1106030));
        map.put("Message", "Test passed.");

        sendSbbMessage(map);
        return;
    }

    private void sendSbbMessage(HashMap map) {
        try {
            TCKSbbUtils.getResourceInterface().sendSbbMessage(map);
        } catch(Exception e) {
            TCKSbbUtils.handleException(e);
        }
    }
    
    public abstract void setMyEventContext(EventContext object);
    public abstract EventContext getMyEventContext();

}

