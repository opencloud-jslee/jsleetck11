/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.profiles.profileenv;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.slee.CreateException;
import javax.slee.facilities.AlarmFacility;
import javax.slee.profile.Profile;
import javax.slee.profile.ProfileContext;
import javax.slee.profile.ProfileTable;
import javax.slee.profile.ProfileVerificationException;

import com.opencloud.logging.Logable;
import com.opencloud.logging.StdErrLog;
import com.opencloud.sleetck.lib.profileutils.BaseMessageSender;
import com.opencloud.sleetck.lib.rautils.RMIObjectChannel;
import com.opencloud.sleetck.lib.rautils.TCKRAUtils;

/**
 *
 */
public abstract class Test1110374Profile implements ProfileEnvTestsProfileCMP, Profile, Test1110374ProfileManagement  {

    public Test1110374Profile() {
        Logable log = new StdErrLog();

        try {
            out = TCKRAUtils.lookupRMIObjectChannel();
            msgSender = new BaseMessageSender(out, log);
        }
        catch (Exception e) {
            log.error("An error occured creating an RMIObjectChannel:");
            log.error(e);
        }
    }

    public void setProfileContext(ProfileContext context) {
        context.getTracer("setProfileContext").info("setProfileContext called");

        this.context = context;
    }

    public void unsetProfileContext() {
        context.getTracer("unsetProfileContext").info("unsetProfileContext called");

        context = null;
    }

    public void profileInitialize() {
        context.getTracer("profileInitialize").info("profileInitialize called");
    }

    public void profilePostCreate() throws CreateException {
        context.getTracer("profilePostCreate").info("profilePostCreate called");
    }

    public void profileActivate() {
        context.getTracer("profileActivate").info("profileActivate called");
    }

    public void profilePassivate() {
        context.getTracer("profilePassivate").info("profilePassivate called");
    }

    public void profileLoad() {
        context.getTracer("profileLoad").info("profileLoad called");
    }

    public void profileStore() {
        context.getTracer("profileStore").info("profileStore called");
    }

    public void profileRemove() {
        context.getTracer("profileRemove").info("profileRemove called");
    }

    public void profileVerify() throws ProfileVerificationException {
        context.getTracer("profileVerify").info("profileVerify called");
    }

    public void manage() {
        context.getTracer("manage").fine("Management method called.");

        doJNDIEnvTest();
    }

    public void doJNDIEnvTest() {
        Context initCtx;

        //1110384: Profile objects locate the environment naming context using the JNDI interfaces. A Profile object creates a
        //javax.naming.InitialContext object by using the constructor with no arguments...
        try {
            initCtx = new InitialContext();
            msgSender.sendSuccess(1110384, "Retrieved Naming context.");
        } catch (NamingException e) {
            msgSender.sendFailure( 1110384, "Error when retrieving a new InitialContext via no-args constructor: "+e);
            return;
        }

        //1110384: ...looks up the environment naming via the InitialContext under the name java:comp/env.
        Context myEnv;
        try {
            myEnv = (Context)initCtx.lookup("java:comp/env");
            msgSender.sendSuccess( 1110384, "Retrieved Profile Component environment.");
        } catch (NamingException e) {
            msgSender.sendFailure( 1110384, "Error when trying to retrieve Profile Component environment: "+e);
            return;
        }

        //1110377: The SLEE provides the following environment entries: An entry for each SLEE Facility exposed to the Profile abstract class.
        try {
            AlarmFacility alarmFacility = (AlarmFacility)myEnv.lookup("slee/facilities/alarm");
            if (alarmFacility==null) {
                msgSender.sendFailure( 1110377, "Received null when trying to retrieve AlarmFacility.");
                return;
            }
            msgSender.sendSuccess( 1110377, "AlarmFacility retrieved as expected.");
        } catch (NamingException e) {
            msgSender.sendFailure( 1110377, "Exception occured when trying to retrieve AlarmFacility: "+e);
            return;
        }

        //1110388: The entry value type is the type declared in the DD
        try {
            String profileTableName = (String)myEnv.lookup("tableName");
            ProfileTable profileTable = context.getProfileTable(profileTableName);
            msgSender.sendSuccess( 1110388, "Retrieved ProfileTable interface through environment lookup.");
        } catch (Exception e) {
            msgSender.sendFailure( 1110388, "Exception occurred when trying to retrieve ProfileTable interface: "+e);
            return;
        }

        //1110393: The permissible Java types are: String, Character, Integer, Boolean, Double, Byte, Short, Long, and Float.
        try {
            Character Ch = (Character)myEnv.lookup("charValue");
            Integer Int = (Integer)myEnv.lookup("intValue");
            Boolean Bool = (Boolean)myEnv.lookup("boolValue");
            Double D = (Double)myEnv.lookup("doubleValue");
            Byte B = (Byte)myEnv.lookup("byteValue");
            Short Sh = (Short)myEnv.lookup("shortValue");
            Long Lo = (Long)myEnv.lookup("longValue");
            Float Fl = (Float)myEnv.lookup("floatValue");

            msgSender.sendSuccess( 1110393, "Retrieved values for all permissible types.");
        } catch (Exception e) {
            msgSender.sendFailure( 1110393, "Exception occurred when trying to retrieve entry value: "+e);
            return;
        }

        //1110395: An environment entry is scoped to the Profile Specification component whose declaration contains
        //the env-entry element. This means that the environment entry is inaccessible from other Profile Specification
        //components at runtime,...
        try {
            String profileTableName = (String)myEnv.lookup("tableName2");
            msgSender.sendFailure( 1110395, "Lookup for entry of a different profile spec should have failed but was successful.");
            return;
        } catch (Exception e) {
            msgSender.sendSuccess( 1110395, "Looking up value of entry in a different scope failed as expected: "+e);
        }

        //1110381: Instances of the classes of the Profile Specification component are not allowed to modify
        //the Profile Specification component environment at runtime.
        try {
            myEnv.addToEnvironment("XYZ", new Integer(42));
            msgSender.sendFailure( 1110381, "Adding property to component environment succeeded though modifications a prohibited.");
        }
        catch (NamingException e) {
            msgSender.sendSuccess( 1110381, "Adding property to component environment caused exception as expected: "+e);
        }
        try {
            myEnv.addToEnvironment("tableName", "XYZ");
            msgSender.sendFailure( 1110381, "Modifying property value succeeded though modifications a prohibited.");
        }
        catch (NamingException e) {
            msgSender.sendSuccess( 1110381, "Modifying property value caused exception as expected: "+e);
        }
    }

    private ProfileContext context;

    private RMIObjectChannel out;
    private BaseMessageSender msgSender;
}
