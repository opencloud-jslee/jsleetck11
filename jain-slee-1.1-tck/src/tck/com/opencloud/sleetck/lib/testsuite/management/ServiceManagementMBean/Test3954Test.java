/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.management.ServiceManagementMBean;

import com.opencloud.sleetck.lib.SleeTCKTest;
import com.opencloud.sleetck.lib.SleeTCKTestUtils;
import com.opencloud.sleetck.lib.TCKTestResult;
import com.opencloud.sleetck.lib.resource.TCKActivityID;
import com.opencloud.sleetck.lib.resource.TCKSbbMessage;
import com.opencloud.sleetck.lib.resource.testapi.TCKResourceListener;
import com.opencloud.sleetck.lib.testutils.BaseTCKResourceListener;
import com.opencloud.sleetck.lib.testutils.FutureResult;
import com.opencloud.sleetck.lib.testutils.jmx.DeploymentMBeanProxy;
import com.opencloud.sleetck.lib.testutils.jmx.ServiceManagementMBeanProxy;

import javax.slee.ComponentID;
import javax.slee.ServiceID;
import javax.slee.management.DeployableUnitDescriptor;
import javax.slee.management.DeployableUnitID;
import javax.slee.management.ServiceState;
import java.rmi.RemoteException;
import java.util.HashMap;

public class Test3954Test implements SleeTCKTest {

    private static final String SERVICE_DU_PATH_PARAM = "DUPath";
    private static final int TEST_ID = 3954;

    public void init(SleeTCKTestUtils utils) {
        this.utils = utils;
    }

    /**
     * Perform the actual test.
     */

    public TCKTestResult run() throws Exception {

        DeploymentMBeanProxy duProxy = utils.getDeploymentMBeanProxy();
        ServiceManagementMBeanProxy serviceProxy = utils.getMBeanProxyFactory().createServiceManagementMBeanProxy(utils.getSleeManagementMBeanProxy().getServiceManagementMBean());
        DeployableUnitDescriptor duDesc = duProxy.getDescriptor(duID);
        ComponentID components[] = duDesc.getComponents();

        ServiceID firstService = null;
        ServiceID secondService = null;

        for (int i = 0; i < components.length; i++) {
            if (components[i] instanceof ServiceID) {
                ServiceID service = (ServiceID) components[i];

                if (firstService == null) {
                    firstService = service;
                    continue;
                }

                if (secondService == null) {
                    secondService = service;
                    break;
                }
            }
        }

        if (firstService == null || secondService == null) {
            return TCKTestResult.error("Failed to find both test services.");
        }

        // Activate the first service.
        serviceProxy.activate(firstService);

        ServiceID services[] = serviceProxy.getServices(ServiceState.ACTIVE);
        if (services.length != 1 || !services[0].equals(firstService))
            return TCKTestResult.failed(TEST_ID, "Failed to find one active service: found " + services.length + " instead.");

        services = serviceProxy.getServices(ServiceState.INACTIVE);
        if (services.length != 1 || !services[0].equals(secondService))
            return TCKTestResult.failed(TEST_ID, "Failed to find single inactive service.");

        try {
            serviceProxy.getServices(null);
        } catch (NullPointerException e) {
            return TCKTestResult.passed();
        }

        return TCKTestResult.failed(3955, "Should have thrown NullPointerException when passed a NULL state.");
    }

    /**
     * Do all the pre-run configuration of the test.
     */

    public void setUp() throws Exception {

        utils.getLog().fine("Connecting to resource");
        resourceListener = new TCKResourceListenerImpl();
        utils.getResourceInterface().setResourceListener(resourceListener);
        utils.getLog().fine("Installing and activating service");

        // Install the Deployable Units
        String duPath = utils.getTestParams().getProperty(SERVICE_DU_PATH_PARAM);
        duID = utils.install(duPath);
    }

    /**
     * Clean up after the test.
     */

    public void tearDown() throws Exception {
        utils.getLog().fine("Disconnecting from resource");
        utils.getResourceInterface().clearActivities();
        utils.getResourceInterface().removeResourceListener();
        utils.getLog().fine("Deactivating and uninstalling service");
        utils.deactivateAllServices();
        utils.uninstallAll();
    }

    private class TCKResourceListenerImpl extends BaseTCKResourceListener {
        public synchronized void onSbbMessage(TCKSbbMessage message, TCKActivityID calledActivity) throws RemoteException {
            utils.getLog().info("Received message from SBB");

            HashMap map = (HashMap) message.getMessage();
            Boolean passed = (Boolean) map.get("Result");
            String msgString = (String) map.get("Message");

            if (passed.booleanValue() == true)
                result.setPassed();
            else
                result.setFailed(TEST_ID, msgString);
        }

        public void onException(Exception e) throws RemoteException {
            utils.getLog().warning("Received exception from SBB");
            utils.getLog().warning(e);
            result.setError(e);
        }
    }

    private SleeTCKTestUtils utils;
    private TCKResourceListener resourceListener;
    private FutureResult result;
    private DeployableUnitID duID;
}
