/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.events.concurrency;

import com.opencloud.sleetck.lib.sbbutils.BaseTCKSbb;
import com.opencloud.sleetck.lib.sbbutils.TCKSbbUtils;
import javax.slee.facilities.Level;

/**
 * Base Sbb class for the SbbActivityConcurrencyTest.
 */
public abstract class BaseSbbActivityConcurrencyTestSbb extends BaseTCKSbb {

    /**
     * Makes a synchronous ACK call to the test, which will block as appropriate.
     * It passes an Integer representing the call code as the argument to test method.
     */
    protected void callTest(int callCode, String eventDisplayName) {
        createTraceSafe(Level.INFO,"Received "+eventDisplayName);
        try {
            TCKSbbUtils.getResourceInterface().callTest(new Integer(callCode));
        } catch (Exception ex) {
            TCKSbbUtils.handleException(ex);
        }
    }

}