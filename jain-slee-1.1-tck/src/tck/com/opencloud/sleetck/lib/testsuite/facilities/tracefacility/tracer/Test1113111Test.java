/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.facilities.tracefacility.tracer;

import javax.slee.ComponentID;
import javax.slee.SbbID;
import javax.slee.ServiceID;
import javax.slee.facilities.TraceLevel;
import javax.slee.management.DeployableUnitDescriptor;
import javax.slee.management.DeployableUnitID;
import javax.slee.management.SbbNotification;

import com.opencloud.sleetck.lib.AbstractSleeTCKTest;
import com.opencloud.sleetck.lib.TCKTestErrorException;
import com.opencloud.sleetck.lib.testutils.FutureResult;
import com.opencloud.sleetck.lib.testutils.jmx.DeploymentMBeanProxy;
import com.opencloud.sleetck.lib.testutils.jmx.TraceMBeanProxy;

/*
 * AssertionID (1113111): Test Tracer name components must be at least one character 
 * in length. This means that the tracer name “com.foo” is a legal name,
 * 
 * AssertionID (1113428): Test whereas “com..foo” is not, as the middle name component 
 * of this name is zero-length.
 * 
 * AssertionID (1113112): Test The empty string, “”, denotes the root tracer. The root 
 * tracer sits at the top of a tracer hierarchy and is the ancestor of all tracers. 
 * The root tracer always exists and always has an assigned trace level. The default 
 * trace level for all root tracers is TraceLevel.OFF.
 * 
 * AssertionID (1113168): Test The Administrator uses the TraceMBean to change the trace 
 * level for a tracer. 
 *
 */
public class Test1113111Test extends AbstractSleeTCKTest {

    private static final String SERVICE_DU_PATH_PARAM = "serviceDUPath";

    /**
     * Perform the actual test.
     */

    public void run(FutureResult result) throws Exception {
        this.result = result;
        TraceMBeanProxy traceMBeanProxy = utils().getMBeanProxyFactory().createTraceMBeanProxy(
                utils().getSleeManagementMBeanProxy().getTraceMBean());

        if (sbbID == null)
            throw new TCKTestErrorException("sbbID not found for " + SERVICE_DU_PATH_PARAM);
        if (serviceID == null)
            throw new TCKTestErrorException("serviceID not found for " + SERVICE_DU_PATH_PARAM);

        SbbNotification sbbNotification = new SbbNotification(serviceID, sbbID);
        //1113111
        try {
            getLog().fine("Starting to test a valid length of the tracer name components.");
            traceMBeanProxy.setTraceLevel(sbbNotification, "com.foo", TraceLevel.FINE);
        } catch (UnsupportedOperationException uoe) {
            getLog().warning(uoe);
            result.setError("ERROR!", uoe);
        } catch (Exception e) {
            getLog().warning(e);
            result.setFailed(1113111, "Tracer name components must be at least "
                    + "one character in length, that means that the tracer name com.foo" + "is a legal name.");
        }

        //1113428
        boolean passed = false;
        try {
            getLog().fine("Starting to test an invalid length of the tracer name components.");
            traceMBeanProxy.setTraceLevel(sbbNotification, "com..foo", TraceLevel.FINE);
        } catch (UnsupportedOperationException uoe) {
            getLog().warning(uoe);
            result.setError("ERROR!", uoe);
        } catch (javax.slee.InvalidArgumentException iae) {
            passed = true;
        } catch (Exception e) {
            getLog().warning(e);
            result.setError("ERROR!", e);
        }

        if (!passed) {
            result.setFailed(1113428, "Tracer name components must be at least "
                    + "one character in length, that means that “com..foo” is not a legal name "
                    + "because the middle name component of this name is zero-length.");
        }

        //1113112
        try {
            // In spec 1.1-pfd the default trace level of a Tracer with the root tracername is INFO,
            // in spec 1.1-pr, the default trace level of a Tracer with the root tracername is OFF.
            getLog().fine(
                    "Starting to test the default trace level for all root tracers "
                            + "is TraceLevel.OFF or TraceLevel.INFO.");
            if (traceMBeanProxy.getTraceLevel(sbbNotification, "") != TraceLevel.INFO)
                result.setFailed(1113112, "The default trace level for all root tracers "
                        + "must set to TraceLevel.OFF, but it returned "
                        + traceMBeanProxy.getTraceLevel(sbbNotification, "").toString());
        } catch (Exception e) {
            getLog().warning(e);
            result.setError("ERROR!", e);
        }

        //1113168
        try {
            getLog().fine(
                    "Starting to test the Administrator uses the TraceMBean to "
                            + "change the trace level for a tracer. ");
            traceMBeanProxy.setTraceLevel(sbbNotification, "com.foo", TraceLevel.FINE);
            if (traceMBeanProxy.getTraceLevel(sbbNotification, "com.foo") != TraceLevel.FINE)
                result.setFailed(1113168, "The old TraceLevel for the tracername com.foo did not "
                        + "return TraceLevel.FINE.");
            traceMBeanProxy.setTraceLevel(sbbNotification, "com.foo", TraceLevel.CONFIG);
            if (traceMBeanProxy.getTraceLevel(sbbNotification, "com.foo") != TraceLevel.CONFIG)
                result.setFailed(1113168, "The new TraceLevel for the tracername com.foo did not "
                        + "return TraceLevel.CONFIG.");
        } catch (UnsupportedOperationException uoe) {
            getLog().warning(uoe);
            result.setError("ERROR!", uoe);
        } catch (Exception e) {
            getLog().warning(e);
            result.setFailed(1113168, "There is an error when the Administrator uses "
                    + "the TraceMBean to change the trace level for a tracer. ");
        }

        result.setPassed();
    }

    /**
     * Do all the pre-run configuration of the test.
     */

    public void setUp() throws Exception {

        getLog().fine("Connecting to resource");
        getLog().fine("Installing and activating service");

        // Install the Deployable Units
        String duPath = utils().getTestParams().getProperty(SERVICE_DU_PATH_PARAM);
        duID = utils().install(duPath);

        // Activate the DU
        utils().activateServices(duID, true);

        DeploymentMBeanProxy duProxy = utils().getDeploymentMBeanProxy();
        DeployableUnitDescriptor duDesc = duProxy.getDescriptor(duID);
        ComponentID components[] = duDesc.getComponents();
        // Get the SbbID from the components.
        for (int i = 0; i < components.length; i++) {
            if (components[i] instanceof ServiceID) {
                getLog().fine("Setting serviceID value.");
                serviceID = (ServiceID) components[i];
                continue;
            }

            if (components[i] instanceof SbbID) {
                getLog().fine("Setting sbbID value.");
                sbbID = (SbbID) components[i];
                continue;
            }
        }

    }

    /**
     * Clean up after the test.
     */

    public void tearDown() throws Exception {
        super.tearDown();
    }

    private DeployableUnitID duID;

    private FutureResult result;

    private SbbID sbbID;

    private ServiceID serviceID;
}
