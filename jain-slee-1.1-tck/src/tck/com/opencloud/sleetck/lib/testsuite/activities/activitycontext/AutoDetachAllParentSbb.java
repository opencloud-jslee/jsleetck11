/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.activities.activitycontext;

import com.opencloud.sleetck.lib.sbbutils.BaseTCKSbb;
import com.opencloud.sleetck.lib.sbbutils.TCKSbbUtils;
import com.opencloud.sleetck.lib.sbbutils.TCKSbbConstants;
import com.opencloud.sleetck.lib.resource.sbbapi.TCKActivityContextInterfaceFactory;
import com.opencloud.sleetck.lib.resource.events.TCKResourceEventX;
import com.opencloud.sleetck.lib.resource.events.TCKResourceEventY;
import com.opencloud.sleetck.lib.resource.TCKActivityID;

import javax.slee.ActivityContextInterface;
import javax.slee.facilities.Level;
import javax.slee.facilities.ActivityContextNamingFacility;
import javax.slee.Sbb;
import javax.slee.SbbLocalObject;
import javax.slee.ChildRelation;
import javax.slee.Address;

import java.util.HashMap;
import java.util.Iterator;

public abstract class AutoDetachAllParentSbb extends BaseTCKSbb {

    public static final int N_ACTIVITIES = 3;

    /**
     * Creates the child SBB and attaches it to a number of newly created activities,
     * then ends the child.
     */

            // @@ Make sure parent's priority on the fireChildEvent is less than that of the child.

    public void onTCKResourceEventX1(TCKResourceEventX event, ActivityContextInterface aci) {

        SbbLocalObject childLocalObject = null;
        SbbLocalObject secondChildLocalObject = null;

        try {
            // Create the child SBBs.
            childLocalObject = getChildRelation().create();
            secondChildLocalObject = getSecondChildRelation().create();
            setSecondChildLocalObject(secondChildLocalObject);

            // Get the ACNF
            ActivityContextNamingFacility facility = (ActivityContextNamingFacility) TCKSbbUtils.getSbbEnvironment().lookup("slee/facilities/activitycontextnaming");
            // Bind this "default" activity to it, for use by children later.
            facility.bind(aci, "AutoDetachAllTest");

            // Get the ActivityContextInterface factory for TCK Activity objects.
            TCKActivityContextInterfaceFactory factory = (TCKActivityContextInterfaceFactory) TCKSbbUtils.getSbbEnvironment().lookup(TCKSbbConstants.TCK_ACI_FACTORY_LOCATION);

            // Create N_ACTIVITIES activities, and attach the child to them.
            TCKActivityID activities[] = new TCKActivityID[N_ACTIVITIES];
            for (int i = 0; i < N_ACTIVITIES; i++) {
                activities[i] = TCKSbbUtils.getResourceInterface().createActivity("AutoDetachAll_" + i);
                ActivityContextInterface myAci = factory.getActivityContextInterface(TCKSbbUtils.getResourceInterface().getActivity(activities[i]));

                // Attach the children to the ACI.
                myAci.attach(childLocalObject);
                myAci.attach(secondChildLocalObject);
            }

            // Remove the first Child SBB.
            childLocalObject.remove();

            for (int i = 0; i < N_ACTIVITIES; i++) {
                ActivityContextInterface myAci = factory.getActivityContextInterface(TCKSbbUtils.getResourceInterface().getActivity(activities[i]));
                fireChildEvent(new AutoDetachAllEvent(), myAci, null);
            }

            // Set the starting index to 0.
            setIndex(0);

            // Fire an event on the first activity.
            // ActivityContextInterface myAci = facility.lookup("AutoDetachAll_" + getIndex());
            // fireChildEvent(new AutoDetachAllEvent(), myAci, null);

        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }
    }

    public void onFailedEvent(AutoDetachAllFailedEvent event, ActivityContextInterface aci) {
        setFailed(true);
    }

    public void onPassedEvent(AutoDetachAllPassedEvent event, ActivityContextInterface aci) {

        try {

            setIndex(getIndex() + 1);
            if (getIndex() >= N_ACTIVITIES) { // Events have been returned.
                // Remove the second Child SBB local object.
                getSecondChildLocalObject().remove();

                // If a failure was reported, return test failure.
                if (getFailed() == true) {
                    HashMap map = new HashMap();
                    map.put("Result", new Boolean(false));
                    map.put("Message", "Removed Child SBB received an Event from an Activity Context it was still attached to.");
                    TCKSbbUtils.getResourceInterface().sendSbbMessage(map);
                    return;
                }

                HashMap map = new HashMap();
                map.put("Result", new Boolean(true));
                map.put("Message", "Ok");
                TCKSbbUtils.getResourceInterface().sendSbbMessage(map);
                return;
            }
        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }
    }

    public abstract ChildRelation getChildRelation();
    public abstract ChildRelation getSecondChildRelation();

    public abstract void setSecondChildLocalObject(SbbLocalObject sbbLocalObject);
    public abstract SbbLocalObject getSecondChildLocalObject();

    public abstract void setFailed(boolean failed);
    public abstract boolean getFailed();

    public abstract void setIndex(int index);
    public abstract int getIndex();

    public abstract void fireChildEvent(AutoDetachAllEvent event, ActivityContextInterface aci, Address address);

}
