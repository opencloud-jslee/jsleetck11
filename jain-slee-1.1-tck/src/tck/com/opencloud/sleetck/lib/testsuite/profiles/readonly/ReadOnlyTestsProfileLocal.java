/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.profiles.readonly;

import javax.slee.profile.ProfileLocalObject;

public interface ReadOnlyTestsProfileLocal extends ProfileLocalObject {
    public void localSetValue(String value);
    public void setValue(String value);
    public String getValue();
}
