/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.events.servicestarted;

import com.opencloud.sleetck.lib.sbbutils.BaseTCKSbb;
import com.opencloud.sleetck.lib.sbbutils.TCKSbbUtils;

import javax.naming.InitialContext;
import javax.slee.ActivityContextInterface;
import javax.slee.serviceactivity.ServiceActivityContextInterfaceFactory;
import javax.slee.serviceactivity.ServiceStartedEvent;
import java.util.HashMap;

public abstract class Test4798Sbb extends BaseTCKSbb {

    private static final String JNDI_SERVICEACTIVITYACI_FACTORY = "java:comp/env/slee/serviceactivity/activitycontextinterfacefactory";

    // Initial event
    public void onServiceStartedEvent(ServiceStartedEvent event, ActivityContextInterface aci) {

        HashMap map = new HashMap();

        try {
            ServiceActivityContextInterfaceFactory aciFactory = (ServiceActivityContextInterfaceFactory) new InitialContext().lookup(JNDI_SERVICEACTIVITYACI_FACTORY);

            try {
                aciFactory.getActivityContextInterface(null);
            } catch (NullPointerException e) {
                map.put("Result", Boolean.TRUE);
                TCKSbbUtils.getResourceInterface().sendSbbMessage(map);
                return;
            }

            map.put("Result", Boolean.FALSE);
            map.put("ID", new Integer(4798));
            map.put("Message", "ServiceActivityContextInterfaceFactory.getActivityContextInterface(null) should have thrown NullPointerException");
            TCKSbbUtils.getResourceInterface().sendSbbMessage(map);
            return;
        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }
    }
}
