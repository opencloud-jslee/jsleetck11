/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.activities.activitycontext;

import javax.slee.ActivityContextInterface;

public interface Test588SbbActivityContextInterface extends ActivityContextInterface {

    // Write-only attribute
    public void setValueA(int foo);

    // Read-only attribute
    public int getValueB();

    // Read-write attribute
    public void setValueC(int foo);
    public int getValueC();

}
