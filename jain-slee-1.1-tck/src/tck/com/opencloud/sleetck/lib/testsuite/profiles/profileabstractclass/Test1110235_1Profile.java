/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.profiles.profileabstractclass;

import javax.slee.CreateException;
import javax.slee.profile.Profile;
import javax.slee.profile.ProfileContext;
import javax.slee.profile.ProfileVerificationException;

/**
 * Lifecycle methods are abstract.
 */

public abstract class Test1110235_1Profile implements ProfileAbstractClassTestsProfileCMP, Profile  {

    public Test1110235_1Profile() {

    }

    public abstract void setProfileContext(ProfileContext context) ;

    public abstract void unsetProfileContext() ;

    public abstract void profileInitialize() ;

    public abstract void profilePostCreate() throws CreateException ;

    public abstract void profileActivate() ;

    public abstract void profilePassivate() ;

    public abstract void profileLoad() ;

    public abstract void profileStore() ;

    public abstract void profileRemove() ;

    public abstract void profileVerify() throws ProfileVerificationException ;

    private ProfileContext context;

}
