/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.events.eventrouting;

import java.rmi.RemoteException;
import java.util.Map;

import javax.management.ObjectName;
import javax.slee.Address;
import javax.slee.UnrecognizedComponentException;
import javax.slee.management.ManagementException;
import javax.slee.profile.ProfileSpecificationID;

import com.opencloud.sleetck.lib.AbstractSleeTCKTest;
import com.opencloud.sleetck.lib.TCKTestErrorException;
import com.opencloud.sleetck.lib.TCKTestFailureException;
import com.opencloud.sleetck.lib.resource.TCKActivityID;
import com.opencloud.sleetck.lib.resource.TCKSbbMessage;
import com.opencloud.sleetck.lib.resource.events.TCKResourceEventX;
import com.opencloud.sleetck.lib.resource.testapi.TCKResourceListener;
import com.opencloud.sleetck.lib.resource.testapi.TCKResourceTestInterface;
import com.opencloud.sleetck.lib.testutils.Assert;
import com.opencloud.sleetck.lib.testutils.BaseTCKResourceListener;
import com.opencloud.sleetck.lib.testutils.ComponentIDLookup;
import com.opencloud.sleetck.lib.testutils.FutureResult;
import com.opencloud.sleetck.lib.testutils.ProfileUtils;
import com.opencloud.sleetck.lib.testutils.jmx.AddressProfile11CMPProxy;
import com.opencloud.sleetck.lib.testutils.jmx.ProfileProvisioningMBeanProxy;
import com.opencloud.sleetck.lib.testutils.jmx.impl.AddressProfile11CMPProxyImpl;

/**
 * AssertionID(1108094): Test If the addressProfileSelected variable is set to true 
 * and the address is not null but does not locate any Address Profile in the Address 
 * Profile Table of the Service, then no convergence name is created, i.e. 
 * same as setting the initialEvent attribute to false.
 */
public class Test1108094Test extends AbstractSleeTCKTest {

    public static final String SERVICE_DU_PATH_PARAM = "serviceDUPath";

    private static final String ADDRESS_PROFILE_TABLE = "tck.Test1108094.AddressAndProfileTestProfile";

    private static final String PROFILE_1 = "PROFILE_1";

    private static final String PROFILE_2 = "PROFILE_2";
    
    private static final int defaultTimeout = 25000;

    /**
     * Perform the actual test.
     */

    public void run(FutureResult result) throws Exception {
        this.result = result;

        TCKResourceTestInterface resource = utils().getResourceInterface();
        TCKActivityID activityID = resource.createActivity("Test1108094InitialActivity");

        getLog().fine("Firing TCKResourceEventX1 on Test1108094InitialActivity with Address3.");
        resource.fireEvent(TCKResourceEventX.X1, testName, activityID, Test1108094Sbb.ADDRESS_3);

        getLog().fine("Firing TCKResourceEventX1 on Test1108094InitialActivity with Address4.");
        resource.fireEvent(TCKResourceEventX.X1, testName, activityID, Test1108094Sbb.ADDRESS_4);

        synchronized (this) {
            wait(defaultTimeout);
        }

        if (receivedCount == expectedCount)
            result.setPassed();
        else
            result.setFailed(1108094, "Expected number of successful messages not received, messages were"
                    + "not delivered by the SBBs (expected " + expectedCount + ", received " + receivedCount + ")");
    }

    /**
     * Do all the pre-run configuration of the test.
     */

    public void setUp() throws Exception {
        getLog().fine("Connecting to resource");
        resourceListener = new TCKResourceListenerImpl();
        setResourceListener(resourceListener);
        getLog().fine("Installing and activating service");

        setupService(SERVICE_DU_PATH_PARAM, true);

        // setup the address profile table before installing the service
        profileProxy = new ProfileUtils(utils()).getProfileProvisioningProxy();
        profileProxy.createProfileTable(getStdAddressProfileSpecID(), ADDRESS_PROFILE_TABLE);
        setupAddressProfile(ADDRESS_PROFILE_TABLE, PROFILE_1, Test1108094Sbb.ADDRESS_1 );
        setupAddressProfile(ADDRESS_PROFILE_TABLE, PROFILE_2, Test1108094Sbb.ADDRESS_2 );
        tableCreated = true;
    }

    public String getTestName() {
        String testName = "testName";
        return utils().getTestParams().getProperty(testName);
    }

    /**
     * Clean up after the test.
     */

    public void tearDown() throws Exception {

        if (profileProxy != null && tableCreated) {
            try {
                getLog().fine("Removing profile table");
                profileProxy.removeProfileTable(ADDRESS_PROFILE_TABLE);
            } catch (Exception e) {
                getLog().warning(e);
            }
        }

        // cleanup
        super.tearDown();
    }

    private void setupAddressProfile(String tableName, String profileName, Address address) throws Exception {
        ObjectName objectName = profileProxy.createProfile(tableName, profileName);
        AddressProfile11CMPProxy addressProfileProxy = new AddressProfile11CMPProxyImpl(objectName, utils().getMBeanFacade());
        addressProfileProxy.editProfile();
        addressProfileProxy.setAddress(address);
        addressProfileProxy.commitProfile();
    }

    public ProfileSpecificationID getStdAddressProfileSpecID() throws TCKTestErrorException, ManagementException,
            UnrecognizedComponentException {
        if (stdAddressProfileSpecID == null) {
            stdAddressProfileSpecID = new ComponentIDLookup(utils()).lookupProfileSpecificationID("AddressProfileSpec",
                    "javax.slee", "1.1");
            if (stdAddressProfileSpecID == null)
                throw new TCKTestErrorException("Standard address profile specification not found");
        }
        return stdAddressProfileSpecID;
    }

    private class TCKResourceListenerImpl extends BaseTCKResourceListener {
        // TCKResourceListener methods

        public synchronized void onSbbMessage(TCKSbbMessage message, TCKActivityID calledActivity)
                throws RemoteException {

            Map sbbData = (Map) message.getMessage();
            String sbbTestName = "Test1108094Test";
            String sbbTestResult = (String) sbbData.get("result");
            String sbbTestMessage = (String) sbbData.get("message");
            int assertionID = ((Integer) sbbData.get("id")).intValue();
            getLog().info(
                    "Received message from SBB: testname=" + sbbTestName + ", result=" + sbbTestResult + ", message="
                            + sbbTestMessage + ", id=" + assertionID);
            try {
                Assert.assertEquals(assertionID, "Test " + sbbTestName + " failed.", "pass", sbbTestResult);
                receivedCount++;
            } catch (TCKTestFailureException ex) {
                result.setFailed(assertionID, sbbTestMessage);
            }
        }

        public void onException(Exception exception) throws RemoteException {
            getLog().warning("Received Exception from SBB or resource:");
            getLog().warning(exception);
            result.setError(exception);
        }
    }

    private TCKResourceListener resourceListener;

    private FutureResult result;

    private static final int expectedCount = 0;

    private int receivedCount = 0;

    private final static String testName = "Test1108094";

    private ProfileSpecificationID stdAddressProfileSpecID;

    private boolean tableCreated = false;

    private ProfileProvisioningMBeanProxy profileProxy;
}