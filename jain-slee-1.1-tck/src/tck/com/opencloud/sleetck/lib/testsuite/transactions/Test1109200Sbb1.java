/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.transactions;

import javax.slee.ActivityContextInterface;
import javax.slee.CreateException;

import com.opencloud.sleetck.lib.resource.events.TCKResourceEventX;
import com.opencloud.sleetck.lib.sbbutils.BaseTCKSbb;
import com.opencloud.sleetck.lib.sbbutils.TCKSbbUtils;

/**
 * Define an Sbb to test the SLEE's rollback of fire event methods
 */
public abstract class Test1109200Sbb1 extends BaseTCKSbb {

    public void sbbCreate() throws CreateException {
        throw new CreateException("Testing sbbCreate...");
    }

    public void onTCKResourceEventX1(TCKResourceEventX event, ActivityContextInterface aci) {
        getSbbContext().getTracer("Test1109200Sbb1").fine("SBB got TCKResourceEventX1.");

        try {
            TCKSbbUtils.getResourceInterface().sendSbbMessage(null);
        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }
    }
}
