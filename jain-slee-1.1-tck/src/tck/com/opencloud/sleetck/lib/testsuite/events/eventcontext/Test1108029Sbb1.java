/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.events.eventcontext;

import javax.slee.ActivityContextInterface;
import javax.slee.InitialEventSelector;
import javax.slee.EventContext;
import javax.slee.facilities.Tracer;

import com.opencloud.sleetck.lib.sbbutils.BaseTCKSbb;
import com.opencloud.sleetck.lib.sbbutils.TCKSbbUtils;
import com.opencloud.sleetck.lib.resource.events.TCKResourceEventX;

import java.util.HashMap;

/**
 * AssertionID(1108029): Test The javax.slee.IllegalStateException is thrown 
 * if the event delivery has already been suspended or suspendDelivery is 
 * invoked on an Event Context object that is not the Event Context object 
 * provided by the SLEE to the event handler method invoked by the SLEE.
 */
public abstract class Test1108029Sbb1 extends BaseTCKSbb {
    public static final String TRACE_MESSAGE_TCKResourceEventX1 = "Test1108029Sbb1:I got TCKResourceEventX1 on ActivityA";

    public static final String TRACE_MESSAGE_TCKResourceEventX2 = "Test1108029Sbb1:I got TCKResourceEventX2 on ActivityB";
    
    public static final String TRACE_MESSAGE_TCKResourceEventX3 = "Test1108029Sbb1:I got TCKResourceEventX3 on ActivityB";

    public InitialEventSelector initialEventSelector(InitialEventSelector ies) {
        ies.setCustomName("test");
        return ies;
    }

    public void onTCKResourceEventX1(TCKResourceEventX event, ActivityContextInterface aci, EventContext context) {
        try {
            //send message to TCK: I got TCKResourceEventX1 on ActivityA
            tracer = getSbbContext().getTracer("com.test");
            tracer.info(TRACE_MESSAGE_TCKResourceEventX1);

            setTestName((String) event.getMessage());

            //store context in CMP field
            setEventContext(context);

            if (getTestName().equals("TIMEOUT")) {
                //call suspendDelivery(10000ms) method now 
                context.suspendDelivery(10000);
            }
            else if (getTestName().equals("NOTIMEOUT")) {
                //call suspendDelivery() method now, we assumed the default timeout 
                //on the SLEE will be more than 10000ms to complete this test.
                context.suspendDelivery();
            }
            else {
                tracer.severe("Unexpected test name encountered during X1 event handler: " + getTestName());
                return;
            }

            boolean passed = false;
            try {
                context.suspendDelivery();
            } catch (java.lang.IllegalStateException e) {
                tracer.info("got expected IllegalStateException if the event delivery has already been suspended.");
                passed = true;
            }

            if (!passed) {
                sendResultToTCK("Test1108029Test", false, "SBB1:onTCKResourceEventX1-ERROR: EventContext.suspendDelivery() "
                        + "should have thrown java.lang.IllegalStateException "
                        + "if the event delivery has already been suspended.", 1108029);
                return;
            }

        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }
    }

    public void onTCKResourceEventX2(TCKResourceEventX event, ActivityContextInterface aci, EventContext context) {
        try {
            // send message to TCK: I got TCKResourceEventX2 on ActivityB
            tracer = getSbbContext().getTracer("com.test");
            tracer.info(TRACE_MESSAGE_TCKResourceEventX2);
            // get EventContext ec from CMP field
            EventContext ec = getEventContext();
            // assert ec.isSuspended(), check ec has been suspended or not
            if (!ec.isSuspended()) {
                sendResultToTCK("Test1108029Test", false, "SBB1:onTCKResourceEventX2-ERROR: ERROR onTCKResourceEventX2, the event delivery hasn't been suspended, the "
                        + "EventContext.isSuspended() returned false!", 1108029);
                return;
            }

            boolean passed = false;
            try {
                ec.suspendDelivery();
            } catch (java.lang.IllegalStateException e) {
                tracer.info("got expected IllegalStateException if the event delivery has already been suspended.");
                passed = true;
            }

            if (!passed) {
                sendResultToTCK("Test1108029Test", false, "SBB1:onTCKResourceEventX2-ERROR: EventContext.suspendDelivery() "
                        + "should have thrown java.lang.IllegalStateException "
                        + "if the event delivery has already been suspended.", 1108029);
                return;
            }
            
            sendResultToTCK("Test1108029Test", true, "Test The javax.slee.IllegalStateException is thrown if the event delivery "
                    + "has already been suspended or suspendDelivery is invoked on an Event Context "
                    + "object that is not the Event Context object provided by the SLEE to the event "
                    + "handler method invoked by the SLEE passed.", 1108029);

        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }
    }
    
    public void onTCKResourceEventX3(TCKResourceEventX event, ActivityContextInterface aci, EventContext context) {
        try {
            // send message to TCK: I got TCKResourceEventX3 on ActivityB
            tracer = getSbbContext().getTracer("com.test");
            tracer.info(TRACE_MESSAGE_TCKResourceEventX3);
            EventContext ec = getEventContext();
            try {
                if (ec != null && ec.isSuspended())
                    ec.resumeDelivery();
            } catch (Exception e) {
                tracer.severe("Failed to resume the suspended event delivery.");
            }
        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }
    }

    private void sendResultToTCK(String testName, boolean result, String message, int failedAssertionID) throws Exception {
        HashMap sbbData = new HashMap();
        sbbData.put("testname", testName);
        sbbData.put("result", result ? "pass" : "fail");
        sbbData.put("message", message);
        sbbData.put("id", new Integer(failedAssertionID));
        TCKSbbUtils.getResourceInterface().sendSbbMessage(sbbData);
    }

    public abstract void setEventContext(EventContext eventContext);
    public abstract EventContext getEventContext();

    public abstract void setTestName(String testName);
    public abstract String getTestName();

    private Tracer tracer = null;

}
