/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.management.AlarmMBean;

import javax.naming.InitialContext;
import javax.slee.ActivityContextInterface;
import javax.slee.facilities.AlarmFacility;
import javax.slee.facilities.AlarmLevel;

import com.opencloud.sleetck.lib.resource.events.TCKResourceEventX;
import com.opencloud.sleetck.lib.sbbutils.BaseTCKSbb;
import com.opencloud.sleetck.lib.sbbutils.TCKSbbUtils;

public abstract class Test1114148Sbb extends BaseTCKSbb {

    private static final String JNDI_ALARMFACILITY_NAME = AlarmFacility.JNDI_NAME;
    public static final String ALARM_MESSAGE = "Test1114768AlarmMessage";
    public static final String ALARM_INSTANCEID = "Test1114768AlarmInstanceID";

    // Initial event
    public void onTCKResourceEventX1(TCKResourceEventX ev, ActivityContextInterface aci) {

        try {
            AlarmFacility facility = (AlarmFacility) new InitialContext().lookup(JNDI_ALARMFACILITY_NAME);
            String AlarmID = facility.raiseAlarm("javax.slee.management.Alarm", ALARM_INSTANCEID, AlarmLevel.MAJOR, ALARM_MESSAGE);

        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }

    }

}

