/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.javax.slee.management.ServiceUsageMBean;

import com.opencloud.sleetck.lib.AbstractSleeTCKTest;
import com.opencloud.sleetck.lib.TCKTestErrorException;
import com.opencloud.sleetck.lib.TCKTestResult;
import com.opencloud.sleetck.lib.TCKTestFailureException;
import com.opencloud.sleetck.lib.infra.SleeTCKComponentConstants;
import com.opencloud.sleetck.lib.testsuite.usage.common.UsageMBeanLookup;
import com.opencloud.sleetck.lib.testutils.ComponentIDLookup;
import com.opencloud.sleetck.lib.testutils.jmx.ServiceUsageMBeanProxy;

import javax.slee.SbbID;
import javax.slee.UnrecognizedSbbException;
import javax.slee.management.DeployableUnitID;
import javax.slee.InvalidArgumentException;
import javax.slee.usage.UnrecognizedUsageParameterSetNameException;

/**
 * Checks that the correct Exceptions are thrown from ServiceUsageMBean methods.
 *
 * Calls createUsageParameterSet() with the SbbID of an uninstalled SBB.
 * Calls createUsageParameterSet() with the SbbID of an SBB not in the service
 * represented by the ServiceUsageMBean.
 *
 * Calls removeUsageParameterSet() with null arguments.
 * Calls removeUsageParameterSet() with id of an uninstalled SBB.
 * Calls removeUsageParameterSet() with the id of an SBB not in the
 * service represented by the ServiceUsageMBean.
 * Calls removeUsageParameterSet() with the id of an SBB in the service represented
 * by the ServiceUsageMBean, with no usage parameters interface.
 * Calls removeUsageParameterSet() with a name that does not identify
 * a usage parameter set that has been created for the SBB.
 *
 * Calls getUsageParameterSets() with a null SBB id argument.
 * Calls getUsageParameterSets() with the id of an SBB in the service represented
 * by the ServiceUsageMBean, with no usage parameters interface.
 * Calls getUsageParameterSets() with the id of an uninstalled SBB.
 * Calls getUsageParameterSets() with the id of an SBB not in the service
 * represented by the ServiceUsageMBean.
 *
 * Calls getSbbUsageMBean(SbbID) with a null SBB id argument.
 * Calls getSbbUsageMBean(SbbID) with the id of an SBB in the service represented
 * by the ServiceUsageMBean, with no usage parameters interface.
 * Calls getSbbUsageMBean(SbbID) with the id of an uninstalled SBB.
 * Calls getSbbUsageMBean(SbbID) with the id of an SBB not in the service represented by the ServiceUsageMBean.
 *
 * Calls getSbbUsageMBean(SbbID,String) with null arguments.
 * Calls getSbbUsageMBean(SbbID,String) with id of an uninstalled SBB.
 * Calls getSbbUsageMBean(SbbID,String) with the id of an SBB not in the
 * service represented by the ServiceUsageMBean.
 * Calls getSbbUsageMBean(SbbID,String) with the id of an SBB in the service represented
 * by the ServiceUsageMBean, with no usage parameters interface.
 * Calls getSbbUsageMBean(SbbID,String) with a name that does not identify
 * a usage parameter set that has been created for the SBB.
 *
 * Calls resetAllUsageParameters(SbbID) with a null SBB id argument.
 * Calls resetAllUsageParameters(SbbID) with the id of an SBB in the service represented
 * by the ServiceUsageMBean, with no usage parameters interface.
 * Calls resetAllUsageParameters(SbbID) with the id of an uninstalled SBB.
 * Calls resetAllUsageParameters(SbbID) with the id of an SBB not in the service represented by the ServiceUsageMBean.
 */
public class ServiceUsageMBeanExceptionsTest extends AbstractSleeTCKTest {

    private static final String TEMPORARY_SERVICE_DU_PATH = "serviceDUPath-Temp";
    private static final String CREATED_PARAMETER_SET = "CREATED_PARAMETER_SET";

    public TCKTestResult run() throws Exception {

        testCreateUsageParameterSet();
        testRemoveUsageParameterSet();
        testGetUsageParameterSets();
        testGetSbbUsageMBean1Arg();
        testGetSbbUsageMBean2Args();
        testResetAllUsageParameters1Arg();

        return TCKTestResult.passed();
    }

    /**
     * Tests for createUsageParameterSet()
     */
    private void testCreateUsageParameterSet() throws Exception {
        try {
            serviceUsageMBeanProxy.createUsageParameterSet(chosenSbb,CREATED_PARAMETER_SET);
            getLog().info("Call to createUsageParameterSet() with valid arguments threw no Exception");
        } catch (UnrecognizedSbbException e) {
            throw new TCKTestErrorException("Call to createUsageParameterSet() with valid arguments "+
                    "threw an unexpected UnrecognizedSbbException:",e);
        }

        try {
            serviceUsageMBeanProxy.createUsageParameterSet(uninstalledSbb,"invalid-attempt-1");
            throw new TCKTestFailureException(4559,"createUsageParameterSet() failed to throw the expected "+
                    "UnrecognizedSbbException when called with the id of an uninstalled SBB");
        } catch (UnrecognizedSbbException e) {
            getLog().info("Caught excpected UnrecognizedSbbException from call to createUsageParameterSet() "+
                    "with the id of an uninstalled SBB");
        }

        try {
            serviceUsageMBeanProxy.createUsageParameterSet(sbbInOtherService,"invalid-attempt-2");
            throw new TCKTestFailureException(4559,"createUsageParameterSet() failed to throw the expected "+
                    "UnrecognizedSbbException when called with the id of an SBB not in the service represented "+
                    "by the ServiceUsageMBean");
        } catch (UnrecognizedSbbException e) {
            getLog().info("Caught excpected UnrecognizedSbbException from call to createUsageParameterSet() "+
                    "with the id of an SBB not in the service represented by the ServiceUsageMBean");
        }
    }

    /**
     * Tests for removeUsageParameterSet()
     */
    private void testRemoveUsageParameterSet() throws Exception {
        try {
            serviceUsageMBeanProxy.removeUsageParameterSet(null,CREATED_PARAMETER_SET);
            throw new TCKTestFailureException(4567,"removeUsageParameterSet() failed to throw the expected "+
                    "NullPointerException when called with a null SBB ID");
        } catch (NullPointerException e) {
            getLog().info("Caught expected NullPointerException from call to removeUsageParameterSet "+
                    "when called with a null Sbb ID");
        }
        try {
            serviceUsageMBeanProxy.removeUsageParameterSet(chosenSbb,null);
            throw new TCKTestFailureException(4567,"removeUsageParameterSet() failed to throw the expected "+
                    "NullPointerException when called with a null parameter set name");
        } catch (NullPointerException e) {
            getLog().info("Caught expected NullPointerException from call to removeUsageParameterSet "+
                    "when called with a null parameter set name");
        }
        try {
            serviceUsageMBeanProxy.removeUsageParameterSet(null,null);
            throw new TCKTestFailureException(4567,"removeUsageParameterSet() failed to throw the expected "+
                    "NullPointerException when called with a null SBB ID and parameter set name");
        } catch (NullPointerException e) {
            getLog().info("Caught expected NullPointerException from call to removeUsageParameterSet "+
                    "when called with a null SBB ID and parameter set name");
        }

        try {
            serviceUsageMBeanProxy.removeUsageParameterSet(uninstalledSbb,CREATED_PARAMETER_SET);
            throw new TCKTestFailureException(4568,"removeUsageParameterSet() failed to throw the expected "+
                    "UnrecognizedSbbException when called with the id of an uninstalled SBB");
        } catch (UnrecognizedSbbException e) {
            getLog().info("Caught expected UnrecognizedSbbException from call to removeUsageParameterSet "+
                    "when called with the id of an uninstalled SBB");
        }

        try {
            serviceUsageMBeanProxy.removeUsageParameterSet(sbbInOtherService,CREATED_PARAMETER_SET);
            throw new TCKTestFailureException(4568,"removeUsageParameterSet() failed to throw the expected "+
                    "UnrecognizedSbbException when called with the id of an SBB not in the service represented "+
                    "by the ServiceUsageMBean");
        } catch (UnrecognizedSbbException e) {
            getLog().info("Caught expected UnrecognizedSbbException from call to removeUsageParameterSet "+
                    "when called with the id of an SBB not in the service represented by the ServiceUsageMBean");
        }

        try {
            serviceUsageMBeanProxy.removeUsageParameterSet(sbbNoUsage,CREATED_PARAMETER_SET);
            throw new TCKTestFailureException(4569,"removeUsageParameterSet() failed to throw the expected "+
                    "InvalidArgumentException when called with the id of an SBB in the service represented "+
                    "by the ServiceUsageMBean, with no usage parameters interface");
        } catch (InvalidArgumentException e) {
            getLog().info("Caught expected InvalidArgumentException from call to removeUsageParameterSet "+
                    "when called with the id of an SBB in the service represented "+
                    "by the ServiceUsageMBean, with no usage parameters interface");
        }

        try {
            serviceUsageMBeanProxy.removeUsageParameterSet(chosenSbb,"unused-parameter-set-name");
            throw new TCKTestFailureException(4570,"removeUsageParameterSet() failed to throw the expected "+
                    "UnrecognizedUsageParameterSetNameException when called with a name that does not "+
                    "identify a usage parameter set that has been created for the SBB");
        } catch (UnrecognizedUsageParameterSetNameException e) {
            getLog().info("Caught expected UnrecognizedUsageParameterSetNameException from call to removeUsageParameterSet "+
                    "when called with a name that does not identify a usage parameter set that has been created for the SBB");
        }
    }

    /**
     * Tests for getUsageParameterSets()
     */
    private void testGetUsageParameterSets() throws Exception {
        try {
            serviceUsageMBeanProxy.getUsageParameterSets(null);
            throw new TCKTestFailureException(4576,"getUsageParameterSets() failed to throw the expected "+
                    "NullPointerException when called with a null SBB ID");
        } catch (NullPointerException e) {
            getLog().info("Caught expected NullPointerException from call to getUsageParameterSets() "+
                    "when called with a null Sbb ID");
        }

        try {
            serviceUsageMBeanProxy.getUsageParameterSets(sbbNoUsage);
            throw new TCKTestFailureException(4577,"getUsageParameterSets() failed to throw the expected "+
                    "InvalidArgumentException when called with the id of an SBB in the service represented "+
                    "by the ServiceUsageMBean, with no usage parameters interface");
        } catch (InvalidArgumentException e) {
            getLog().info("Caught expected InvalidArgumentException from call to getUsageParameterSets "+
                    "when called with the id of an SBB in the service represented "+
                    "by the ServiceUsageMBean, with no usage parameters interface");
        }

        try {
            serviceUsageMBeanProxy.getUsageParameterSets(uninstalledSbb);
            throw new TCKTestFailureException(4578,"getUsageParameterSets() failed to throw the expected "+
                    "UnrecognizedSbbException when called with the id of an uninstalled SBB");
        } catch (UnrecognizedSbbException e) {
            getLog().info("Caught expected UnrecognizedSbbException from call to getUsageParameterSets "+
                    "when called with the id of an uninstalled SBB");
        }

        try {
            serviceUsageMBeanProxy.getUsageParameterSets(sbbInOtherService);
            throw new TCKTestFailureException(4578,"getUsageParameterSets() failed to throw the expected "+
                    "UnrecognizedSbbException when called with the id of an SBB not in the service represented "+
                    "by the ServiceUsageMBean");
        } catch (UnrecognizedSbbException e) {
            getLog().info("Caught expected UnrecognizedSbbException from call to getUsageParameterSets "+
                    "when called with the id of an SBB not in the service represented by the ServiceUsageMBean");
        }
    }

    /**
     * Tests for getSbbUsageMBean(SbbID)
     */
    private void testGetSbbUsageMBean1Arg() throws Exception {
        try {
            serviceUsageMBeanProxy.getSbbUsageMBean(null);
            throw new TCKTestFailureException(4584,"getSbbUsageMBean(SbbID) failed to throw the expected "+
                    "NullPointerException when called with a null SBB ID");
        } catch (NullPointerException e) {
            getLog().info("Caught expected NullPointerException from call to getSbbUsageMBean(SbbID) "+
                    "when called with a null Sbb ID");
        }

        try {
            serviceUsageMBeanProxy.getSbbUsageMBean(sbbNoUsage);
            throw new TCKTestFailureException(4651,"getSbbUsageMBean(SbbID) failed to throw the expected "+
                    "InvalidArgumentException when called with the id of an SBB in the service represented "+
                    "by the ServiceUsageMBean, with no usage parameters interface");
        } catch (InvalidArgumentException e) {
            getLog().info("Caught expected InvalidArgumentException from call to getSbbUsageMBean(SbbID) "+
                    "when called with the id of an SBB in the service represented "+
                    "by the ServiceUsageMBean, with no usage parameters interface");
        }

        try {
            serviceUsageMBeanProxy.getSbbUsageMBean(uninstalledSbb);
            throw new TCKTestFailureException(4585,"getSbbUsageMBean(SbbID) failed to throw the expected "+
                    "UnrecognizedSbbException when called with the id of an uninstalled SBB");
        } catch (UnrecognizedSbbException e) {
            getLog().info("Caught expected UnrecognizedSbbException from call to getSbbUsageMBean(SbbID) "+
                    "when called with the id of an uninstalled SBB");
        }

        try {
            serviceUsageMBeanProxy.getSbbUsageMBean(sbbInOtherService);
            throw new TCKTestFailureException(4585,"getSbbUsageMBean(SbbID) failed to throw the expected "+
                    "UnrecognizedSbbException when called with the id of an SBB not in the service represented "+
                    "by the ServiceUsageMBean");
        } catch (UnrecognizedSbbException e) {
            getLog().info("Caught expected UnrecognizedSbbException from call to getSbbUsageMBean(SbbID) "+
                    "when called with the id of an SBB not in the service represented by the ServiceUsageMBean");
        }
    }

    /**
     * Tests for getSbbUsageMBean(SbbID,String)
     */
    private void testGetSbbUsageMBean2Args() throws Exception {
        try {
            serviceUsageMBeanProxy.getSbbUsageMBean(null,CREATED_PARAMETER_SET);
            throw new TCKTestFailureException(4591,"getSbbUsageMBean(SbbID,String) failed to throw the expected "+
                    "NullPointerException when called with a null SBB ID");
        } catch (NullPointerException e) {
            getLog().info("Caught expected NullPointerException from call to getSbbUsageMBean(SbbID,String) "+
                    "when called with a null Sbb ID");
        }
        try {
            serviceUsageMBeanProxy.getSbbUsageMBean(chosenSbb,null);
            throw new TCKTestFailureException(4591,"getSbbUsageMBean(SbbID,String) failed to throw the expected "+
                    "NullPointerException when called with a null parameter set name");
        } catch (NullPointerException e) {
            getLog().info("Caught expected NullPointerException from call to getSbbUsageMBean(SbbID,String) "+
                    "when called with a null parameter set name");
        }
        try {
            serviceUsageMBeanProxy.getSbbUsageMBean(null,null);
            throw new TCKTestFailureException(4591,"getSbbUsageMBean(SbbID,String) failed to throw the expected "+
                    "NullPointerException when called with a null SBB id and parameter set name");
        } catch (NullPointerException e) {
            getLog().info("Caught expected NullPointerException from call to getSbbUsageMBean(SbbID,String) "+
                    "when called with a null SBB id and parameter set name");
        }

        try {
            serviceUsageMBeanProxy.getSbbUsageMBean(sbbNoUsage,CREATED_PARAMETER_SET);
            throw new TCKTestFailureException(4592,"getSbbUsageMBean(SbbID,String) failed to throw the expected "+
                    "InvalidArgumentException when called with the id of an SBB in the service represented "+
                    "by the ServiceUsageMBean, with no usage parameters interface");
        } catch (InvalidArgumentException e) {
            getLog().info("Caught expected InvalidArgumentException from call to getSbbUsageMBean(SbbID,String) "+
                    "when called with the id of an SBB in the service represented "+
                    "by the ServiceUsageMBean, with no usage parameters interface");
        }

        try {
            serviceUsageMBeanProxy.getSbbUsageMBean(uninstalledSbb,CREATED_PARAMETER_SET);
            throw new TCKTestFailureException(4654,"getSbbUsageMBean(SbbID,String) failed to throw the expected "+
                    "UnrecognizedSbbException when called with the id of an uninstalled SBB");
        } catch (UnrecognizedSbbException e) {
            getLog().info("Caught expected UnrecognizedSbbException from call to getSbbUsageMBean(SbbID,String) "+
                    "when called with the id of an uninstalled SBB");
        }

        try {
            serviceUsageMBeanProxy.getSbbUsageMBean(sbbInOtherService,CREATED_PARAMETER_SET);
            throw new TCKTestFailureException(4654,"getSbbUsageMBean(SbbID,String) failed to throw the expected "+
                    "UnrecognizedSbbException when called with the id of an SBB not in the service represented "+
                    "by the ServiceUsageMBean");
        } catch (UnrecognizedSbbException e) {
            getLog().info("Caught expected UnrecognizedSbbException from call to getSbbUsageMBean(SbbID,String) "+
                    "when called with the id of an SBB not in the service represented by the ServiceUsageMBean");
        }

        try {
            serviceUsageMBeanProxy.getSbbUsageMBean(chosenSbb,"unused-parameter-set-name");
            throw new TCKTestFailureException(4655,"getSbbUsageMBean(SbbID,String) failed to throw the expected "+
                    "UnrecognizedUsageParameterSetNameException when called with a name that does not "+
                    "identify a usage parameter set that has been created for the SBB");
        } catch (UnrecognizedUsageParameterSetNameException e) {
            getLog().info("Caught expected UnrecognizedUsageParameterSetNameException from call to getSbbUsageMBean(SbbID,String) "+
                    "when called with a name that does not identify a usage parameter set that has been created for the SBB");
        }
    }

    /**
     * Tests for resetAllUsageParameters(SbbID)
     */
    private void testResetAllUsageParameters1Arg() throws Exception {
        try {
            serviceUsageMBeanProxy.resetAllUsageParameters(null);
            throw new TCKTestFailureException(4622,"resetAllUsageParameters(SbbID) failed to throw the expected "+
                    "NullPointerException when called with a null SBB ID");
        } catch (NullPointerException e) {
            getLog().info("Caught expected NullPointerException from call to resetAllUsageParameters(SbbID) "+
                    "when called with a null Sbb ID");
        }

        try {
            serviceUsageMBeanProxy.resetAllUsageParameters(sbbNoUsage);
            throw new TCKTestFailureException(4624,"resetAllUsageParameters(SbbID) failed to throw the expected "+
                    "InvalidArgumentException when called with the id of an SBB in the service represented "+
                    "by the ServiceUsageMBean, with no usage parameters interface");
        } catch (InvalidArgumentException e) {
            getLog().info("Caught expected InvalidArgumentException from call to resetAllUsageParameters(SbbID) "+
                    "when called with the id of an SBB in the service represented "+
                    "by the ServiceUsageMBean, with no usage parameters interface");
        }

        try {
            serviceUsageMBeanProxy.resetAllUsageParameters(uninstalledSbb);
            throw new TCKTestFailureException(4623,"resetAllUsageParameters(SbbID) failed to throw the expected "+
                    "UnrecognizedSbbException when called with the id of an uninstalled SBB");
        } catch (UnrecognizedSbbException e) {
            getLog().info("Caught expected UnrecognizedSbbException from call to resetAllUsageParameters(SbbID) "+
                    "when called with the id of an uninstalled SBB");
        }

        try {
            serviceUsageMBeanProxy.resetAllUsageParameters(sbbInOtherService);
            throw new TCKTestFailureException(4623,"resetAllUsageParameters(SbbID) failed to throw the expected "+
                    "UnrecognizedSbbException when called with the id of an SBB not in the service represented "+
                    "by the ServiceUsageMBean");
        } catch (UnrecognizedSbbException e) {
            getLog().info("Caught expected UnrecognizedSbbException from call to resetAllUsageParameters(SbbID) "+
                    "when called with the id of an SBB not in the service represented by the ServiceUsageMBean");
        }
    }

    public void setUp() throws Exception {
        super.setUp();
        // install then uninstall
        DeployableUnitID duID = null;
        ComponentIDLookup idLookup = new ComponentIDLookup(utils());
        try {
            String relativePath = utils().getTestParams().getProperty(TEMPORARY_SERVICE_DU_PATH);
            String fullURL = utils().getDeploymentUnitURL(relativePath);
            getLog().fine("Installing deployable unit "+fullURL);
            duID = utils().getDeploymentMBeanProxy().install(fullURL);
            uninstalledSbb = idLookup.lookupSbbID("GenericUsageSbb",SleeTCKComponentConstants.TCK_VENDOR,null);
        } finally {
            if(duID != null) {
                try {
                    utils().getDeploymentMBeanProxy().uninstall(duID);
                } catch (Exception e) {
                    getLog().warning(e);
                }
            }
        }
        usageMBeanLookupA = new UsageMBeanLookup(
                "ServiceUsageMBeanExceptionsTestServiceA","ServiceUsageMBeanExceptionsChildSbbA",utils());
        usageMBeanLookupB = new UsageMBeanLookup(
                "ServiceUsageMBeanExceptionsTestServiceB","ServiceUsageMBeanExceptionsChildSbbB",utils());
        serviceUsageMBeanProxy = usageMBeanLookupA.getServiceUsageMBeanProxy();
        chosenSbb = usageMBeanLookupA.getSbbID();
        sbbInOtherService = usageMBeanLookupB.getSbbID();
        sbbNoUsage = idLookup.lookupSbbID("ServiceUsageMBeanExceptionsParentSbbA",SleeTCKComponentConstants.TCK_VENDOR, null);
    }

    public void tearDown() throws Exception {
        if(usageMBeanLookupA != null) usageMBeanLookupA.closeAllMBeans();
        if(usageMBeanLookupB != null) usageMBeanLookupB.closeAllMBeans();
        super.tearDown();
    }

    private UsageMBeanLookup usageMBeanLookupA;
    private UsageMBeanLookup usageMBeanLookupB;
    private ServiceUsageMBeanProxy serviceUsageMBeanProxy;
    private SbbID chosenSbb;
    private SbbID sbbInOtherService;
    private SbbID sbbNoUsage;
    private SbbID uninstalledSbb;

}
