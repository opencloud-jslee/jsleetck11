/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.resource.endpoint;

import java.util.HashMap;

import javax.slee.ActivityContextInterface;

import com.opencloud.sleetck.lib.rautils.UOID;
import com.opencloud.sleetck.lib.testsuite.resource.BaseResourceSbb;
import com.opencloud.sleetck.lib.testsuite.resource.RAMethods;
import com.opencloud.sleetck.lib.testsuite.resource.SimpleEvent;
import com.opencloud.sleetck.lib.testsuite.resource.SimpleSbbInterface;

public abstract class Test1115233Sbb2 extends BaseResourceSbb {

    // Event handler
    public void onSimpleEvent(SimpleEvent event, ActivityContextInterface aci) {
        tracer.info("onSimpleEvent() event handler called with event: " + event);
        try {
            int sequenceID = event.getSequenceID();
            SimpleSbbInterface sbbInterface = getSbbInterface();
            HashMap results = new HashMap();
            tracer.info("Checking activity state (in subsequent transaction).");
            if (!aci.isEnding()) {
                tracer.info("Activity Context is correctly not ending.");
                results.put("result-sbb2", Boolean.TRUE);
            } else {
                tracer.severe("Error: Activity Context was in ending state after a call to endActivityTrasacted() which was rolled back");
                results.put("result-sbb2", Boolean.FALSE);
            }

            tracer.info("Calling endActivityTransacted() for test activity cleanup");
            try {
                sbbInterface.executeTestLogic(new Integer(1115233));
            } catch (Exception e) {
                tracer.info("An error occured during test activity cleanup", e);
            }
            sendSbbMessage(sbbUID, sequenceID, RAMethods.endActivityTransacted, results);
        } catch (Exception e) {
            tracer.severe("Exception caught while trying to execute test logic in RA", e);
        }
    }

    private static final UOID sbbUID = UOID.createUOID();
}
