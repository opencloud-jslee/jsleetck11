/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.events.convergencename;

/**
 * Encapsulates the data send by the ConvergenceNameTestSbb in reply
 * to an event handler invocation.
 */
public class EventReplyData {

    public EventReplyData(String receivedEventID, String initialEventID) {
        this.receivedEventID=receivedEventID;
        this.initialEventID=initialEventID;
    }

    /**
     * Exports the Object into an Object or array of primitive types,
     * J2SE types or SLEE types.
     */
    public Object toExported() {
        return new String[] {receivedEventID, initialEventID};
    }

    /**
     * Returns an EventReplyData Object from the
     * given object in exported form
     */
    public static EventReplyData fromExported(Object exported) {
        String[] stringArray = (String[])exported;
        return new EventReplyData(stringArray[0],stringArray[1]);
    }

    // Accessor methods

    public String getReceivedEventID() {
        return receivedEventID;
    }

    public String getInitialEventID() {
        return initialEventID;
    }

    // Private state

    private String receivedEventID;
    private String initialEventID;

}