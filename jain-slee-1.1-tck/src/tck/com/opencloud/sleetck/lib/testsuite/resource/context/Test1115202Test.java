/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.resource.context;

import java.util.HashMap;

import javax.slee.usage.NoUsageParametersInterfaceDefinedException;

import com.opencloud.sleetck.lib.TCKTestFailureException;
import com.opencloud.sleetck.lib.TCKTestResult;
import com.opencloud.sleetck.lib.testsuite.resource.BaseResourceTest;
import com.opencloud.sleetck.lib.testsuite.resource.RAMethods;

/**
 * Test to ensure that getUsageParameterSet() is throwing a
 * NoUsageParametersInterfaceDefinedException exception correctly.
 * <p>
 * Test assertion ID: 1115202
 */
public class Test1115202Test extends BaseResourceTest {

    private static final int ASSERTION_ID = 1115202;

    public TCKTestResult run() throws Exception {
        HashMap resultmap = sendMessage(RAMethods.getUsageParameterSet);
        if (resultmap == null)
            throw new TCKTestFailureException(ASSERTION_ID, "Test timed out while waiting for response to getUsageParameterSet()");

        Object result = resultmap.get("result1");
        if (result instanceof NoUsageParametersInterfaceDefinedException)
            return TCKTestResult.passed();
        if (result instanceof Exception)
            throw new TCKTestFailureException(ASSERTION_ID, "Unexpected exception thrown while invoking getUsageParameterSet()", (Exception) result);
        if (result instanceof String)
            throw new TCKTestFailureException(ASSERTION_ID, "An error occured while invoking getUsageParameterSet(): " + result);
        if (Boolean.TRUE.equals(result))
            throw new TCKTestFailureException(ASSERTION_ID,
                    "Expected NoUsageParametersInterfaceDefinedException was not thrown while invoking getUsageParameterSet()");
        return TCKTestResult.error("Unexpected result received while invoking getUsageParameterSet(): " + result);
    }
}
