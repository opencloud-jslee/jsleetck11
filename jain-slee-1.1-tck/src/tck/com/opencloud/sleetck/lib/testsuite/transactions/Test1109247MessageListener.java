/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.transactions;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;

import javax.naming.NamingException;
import javax.slee.transaction.RollbackListener;
import javax.slee.transaction.SleeTransaction;
import javax.slee.transaction.SleeTransactionManager;
import javax.transaction.Status;
import javax.transaction.SystemException;

import com.opencloud.logging.StdErrLog;
import com.opencloud.sleetck.lib.profileutils.BaseMessageSender;
import com.opencloud.sleetck.lib.rautils.MessageHandler;
import com.opencloud.sleetck.lib.rautils.RMIObjectChannel;
import com.opencloud.sleetck.lib.rautils.TCKRAUtils;
import com.opencloud.util.Future;
import com.opencloud.util.Future.TimeoutException;

/**
 * Provides callback method for handling messages sent from the TCK test to the RA.
 * The actual test code is located in the 'handleMessage()' method.
 */
public class Test1109247MessageListener extends UnicastRemoteObject implements MessageHandler {

    public Test1109247MessageListener(Test1109247ResourceAdaptor ra) throws RemoteException {
        super();
        try {
            out = TCKRAUtils.lookupRMIObjectChannel();
            msgSender = new BaseMessageSender(out, new StdErrLog());
        }
        catch (Exception e) {
            ra.getLog().warning("Exception occurred when trying to acquire RMIObjectChannel: ",e);
        }
        this.ra = ra;
    }

    public boolean handleMessage(Object message) throws RemoteException {
        SleeTransactionManager txManager = null;
        SleeTransaction txn;
        RollbackListener listener;
        String result;
        Future future;

        try {
            future = new Future();

            try {
                txManager = ra.getTransactionManager();
            } catch (NamingException e) {
                msgSender.sendError("Could not look up transaction manager.", e);
                return true;
            }

            txn = txManager.beginSleeTransaction();
            //1109347: If the asyncRollback() method's argument is null, no callbacks will be made by the SLEE.
            //can only test here that 'null' does not cause any exceptions when provided as a parameter
            try {
                txManager.asyncRollback(null);
                msgSender.sendLogMsg("asyncRollback(null) did not cause any exceptions as expected.");
            } catch (Exception e) {
                msgSender.sendFailure(1109347, "Exception occured when calling TXNManager.asyncRollback(null).");
                return true;
            }

            txn = txManager.beginSleeTransaction();
            listener = new MyRollbackListener(future);
            txManager.asyncRollback(listener);

            // any inserted code here could run before the CommitListener is
            // used, but we cannot test for this - a valid scenario
            // is where the transactions commits immediately.

            // 1109248: after asyncRollback, there is no transaction associated with the current thread.
            if (null != txManager.getSleeTransaction()) {
                msgSender.sendFailure(1109248, "There is still a transaction associated with this thread.");
                return true;
            }

            // 1109251: java.lang.IllegalStateException if a transaction is not currently associated with the calling thread.
            try {
                txManager.asyncRollback(null);
                msgSender.sendFailure(1109251, "SleeTransactionManager.asyncRollback() did not throw IllegalStateException");
                return true;
            } catch (IllegalStateException e) {
                msgSender.sendLogMsg("SleeTransactionManager.asyncRollback() threw IllegalStateException as expected.");
            }

            //1109250: listener object that will receive callbacks depending on the final outcome of the transaction.
            //wait for the RollbackListener callbacks to be called
            try {
                result = (String)future.getValue(5000);
            } catch (TimeoutException e) {
                msgSender.sendFailure(1109250, "Timout occured while waiting for the RollbackListener callbacks to be called.",e);
                return true;
            }
            if ("rolledBack".equals(result))
                msgSender.sendLogMsg("RollbackListener.rolledBack() was called as expected.");
            else {
                msgSender.sendFailure(1109250, "The RollbackListener's rolledBack() method shoud have been called " +
                        "but instead the "+result+"() method was called.");
                return true;
            }


            //1109249:  At some point after calling asyncRollback(), the transaction state will
            //become Status.STATUS_ROLLEDBACK.
            if (txn.getStatus()!=Status.STATUS_ROLLEDBACK) {
                msgSender.sendFailure(1109249, "Transaction status is not Status.STATUS_ROLLEDBACK.");
                return true;
            } else
                msgSender.sendLogMsg("At some point after calling asyncRollback() the TXN status was " +
                        "Status.STATUS_ROLLEDBACK as expected.");

            msgSender.sendSuccess(1109247, "Test completed successfully.");

        } catch (Exception e) {
            msgSender.sendException(e);
            try {
                if (null != txManager.getSleeTransaction())
                    txManager.getSleeTransaction().rollback();
            } catch (SystemException e2) {
                msgSender.sendLogMsg("Exception occured when trying to safely rollback leftover TXN, this is however not the immediate cause for" +
                        " an eventual failure of the test: "+e2);
            }
        }
        return true;
    }

    public class MyRollbackListener implements RollbackListener {

        public MyRollbackListener(Future future) {
            this.future = future;
        }

        public void rolledBack() {
            future.setValue("rolledBack");
        }

        public void systemException(SystemException se) {
            future.setValue("systemException");
        }

        Future future;
    }

    private Test1109247ResourceAdaptor ra;
    private BaseMessageSender msgSender;
    private RMIObjectChannel out;
}

