/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.management.ProfileProvisioningMBean;

import com.opencloud.sleetck.lib.AbstractSleeTCKTest;
import com.opencloud.sleetck.lib.TCKTestResult;
import com.opencloud.sleetck.lib.testutils.ProfileUtils;
import com.opencloud.sleetck.lib.testutils.jmx.DeploymentMBeanProxy;
import com.opencloud.sleetck.lib.testutils.jmx.ProfileProvisioningMBeanProxy;

import javax.slee.ComponentID;
import javax.slee.management.DeployableUnitDescriptor;
import javax.slee.management.DeployableUnitID;
import javax.slee.profile.ProfileSpecificationID;
import javax.slee.profile.UnrecognizedProfileTableNameException;

public class Test3870Test extends AbstractSleeTCKTest {

    private static final String SERVICE_DU_PATH_PARAM = "DUPath";
    private static final String PROFILE_TABLE_NAME = "Test3870ProfileTable";

    /**
     * Perform the actual test.
     */
    public TCKTestResult run() throws Exception {

        profileUtils = new ProfileUtils(utils());
        ProfileProvisioningMBeanProxy profileProxy = profileUtils.getProfileProvisioningProxy();
        DeploymentMBeanProxy duProxy = utils().getDeploymentMBeanProxy();
        DeployableUnitDescriptor duDesc = duProxy.getDescriptor(duID);
        ComponentID components[] = duDesc.getComponents();

        ProfileSpecificationID profileSpecID = null;
        for (int i = 0; i < components.length; i++) {
            if (components[i] instanceof ProfileSpecificationID) {
                profileSpecID = (ProfileSpecificationID) components[i];

                profileProxy.createProfileTable(profileSpecID, PROFILE_TABLE_NAME);
                if(!profileProxy.getProfileTables().contains(PROFILE_TABLE_NAME))
                    return TCKTestResult.error("The profile table "+PROFILE_TABLE_NAME+" did not appear to be created "+
                            "following a call to createProfileTable()");
                try {
                    getLog().info("Calling removeProfileTable with a null table name");
                    profileProxy.removeProfileTable(null);
                    return TCKTestResult.failed(3871,
                            "Failed to throw NullPointerException when given a null ProfileTableName");
                } catch (NullPointerException e) {
                    getLog().info("Caught the expected NullPointerException after passing a null ProfileTableName " +
                            "to removeProfileTable()");
                }

                try {
                    getLog().info("Calling removeProfileTable with the name of an existing profile table: "+PROFILE_TABLE_NAME);
                    profileProxy.removeProfileTable(PROFILE_TABLE_NAME);
                } catch(Exception e) {
                    return TCKTestResult.failed(3870,"removeProfileTable() threw an unexpected Exception when invoked " +
                            "with the name of an existing profile table "+PROFILE_TABLE_NAME+". Exception:"+e);
                }

                if(profileProxy.getProfileTables().contains(PROFILE_TABLE_NAME))
                    return TCKTestResult.failed(3870,"The profile table "+PROFILE_TABLE_NAME+" did not appear to be removed "+
                            "following a call to removeProfileTable(). It was in the set returned by getProfileTables()");

                try {
                    String invalidName = "Test3870ThisProfileDoesNotExist";
                    getLog().info("Calling removeProfileTable with the name that does not reference an existing profile table: "+invalidName);
                    profileProxy.removeProfileTable(invalidName);
                    return TCKTestResult.failed(3872,
                            "Failed to throw UnrecognizedProfileTableNameException when given a non-existent ProfileTableName");
                } catch (UnrecognizedProfileTableNameException e) {
                    getLog().info("Caught the expected UnrecognizedProfileTableNameException after passing a non-existent " +
                            "ProfileTableName to removeProfileTable()");
                }

                return TCKTestResult.passed();
            }
        }

        return TCKTestResult.error("Unable to find any ProfileSpecifications to work with.");

    }

    /**
     * Do all the pre-run configuration of the test.
     */

    public void setUp() throws Exception {
        getLog().fine("Installing and activating service");
        // Install the Deployable Units
        String duPath = utils().getTestParams().getProperty(SERVICE_DU_PATH_PARAM);
        duID = utils().install(duPath);

    }

    /**
     * Clean up after the test.
     */
    public void tearDown() throws Exception {
        if (profileUtils != null) {
            ProfileProvisioningMBeanProxy profileProxy = profileUtils.getProfileProvisioningProxy();
            try {
                profileProxy.removeProfileTable("Test3870ProfileTable");
            } catch (Exception e) {
            }
        }
        super.tearDown();
    }

    private DeployableUnitID duID;
    private ProfileUtils profileUtils;
}
