/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.javax.slee.profile.ProfileContext;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;

import javax.slee.TransactionRequiredLocalException;
import javax.slee.profile.ProfileContext;
import javax.slee.profile.ProfileTable;
import javax.slee.transaction.SleeTransaction;
import javax.slee.transaction.SleeTransactionManager;

import com.opencloud.logging.StdErrLog;
import com.opencloud.sleetck.lib.profileutils.BaseMessageSender;
import com.opencloud.sleetck.lib.rautils.MessageHandler;
import com.opencloud.sleetck.lib.rautils.RMIObjectChannel;
import com.opencloud.sleetck.lib.rautils.TCKRAUtils;

/**
 * Provides callback method for handling messages sent from the TCK test to the RA.
 * The actual test code is located in the 'handleMessage()' method.
 */
public class Test1110349MessageListener extends UnicastRemoteObject implements MessageHandler {

    public static final String PROFILE_TABLE_NAME = "Test1110349ProfileTable";
    public static final String PROFILE_NAME = "Test1110349Profile";

    private Test1110349ResourceAdaptor ra;

    public Test1110349MessageListener(Test1110349ResourceAdaptor ra) throws RemoteException {
        super();

        try {
            out = TCKRAUtils.lookupRMIObjectChannel();
            msgSender = new BaseMessageSender(out, new StdErrLog());
        }
        catch (Exception e) {
            ra.getLog().warning("Exception occurred when trying to acquire RMIObjectChannel: ",e);
        }

        this.ra = ra;
    }

    public boolean handleMessage(Object message) throws RemoteException {

        //run the tests
        if (test_MANDATORY_TXN_BEHAVIOUR() ) {

            msgSender.sendSuccess(1110349, "Test successful.");
        }
        return true;
    }

    private boolean test_MANDATORY_TXN_BEHAVIOUR() {
        SleeTransactionManager txnManager = ra.getResourceAdaptorContext().getSleeTransactionManager();
        SleeTransaction txn=null;

        try {
            ProfileTable profileTable = ra.getResourceAdaptorContext().getProfileTable(PROFILE_TABLE_NAME);
            //start a new TXN as profileTable.find is mandatory transactional
            txn = txnManager.beginSleeTransaction();

            Test1110349ProfileLocal profileLocal = (Test1110349ProfileLocal)profileTable.find(PROFILE_NAME);
            ProfileContext profileContext = profileLocal.getProfileContext();

            txn.commit();


            //1110349: The getRollbackOnly method is a mandatory transactional method.
            try {
                profileContext.getRollbackOnly();
                msgSender.sendFailure(1110349, "getRollbackOnly() was supposed to throw TransactionRequiredLocalException as it was invoked without a TXN context.");
                return false;
            } catch (TransactionRequiredLocalException e) {
                msgSender.sendLogMsg("getRollbackOnly() threw TransactionRequiredLocalException as expected.");
            } catch (Exception e) {
                msgSender.sendFailure(1110349, "getRollbackOnly() threw wrong type of exception. javax.slee.TransactionRequiredLocalException expected but caught "+e.getClass().getName());
                return false;
            }


            //1110353: The setRollbackOnly method is a mandatory transactional method.
            try {
                profileContext.setRollbackOnly();
                msgSender.sendFailure(1110353, "setRollbackOnly() was supposed to throw TransactionRequiredLocalException as it was invoked without a TXN context.");
                return false;
            } catch (TransactionRequiredLocalException e) {
                msgSender.sendLogMsg("setRollbackOnly() threw TransactionRequiredLocalException as expected.");
            } catch (Exception e) {
                msgSender.sendFailure(1110353, "setRollbackOnly() threw wrong type of exception. javax.slee.TransactionRequiredLocalException expected but caught "+e.getClass().getName());
                return false;
            }

            txn = null;

        } catch (Exception e) {
            msgSender.sendException(e);
            return false;
        } finally {
            try {
                if (txn!=null)
                    txn.rollback();
            } catch (Exception ex) {
                ra.getLog().warning("Error occured when trying to rollback RA started TXN.", ex);
                msgSender.sendLogMsg("Exception occurred when trying to safely rollback RA started TXN: "+ex.getMessage());
            }
        }

        return true;
    }

    private BaseMessageSender msgSender;
    private RMIObjectChannel out;

}

