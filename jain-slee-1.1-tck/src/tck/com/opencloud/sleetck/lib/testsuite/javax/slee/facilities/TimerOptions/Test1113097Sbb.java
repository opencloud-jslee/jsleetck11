/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.javax.slee.facilities.TimerOptions;

import java.util.HashMap;

import javax.slee.ActivityContextInterface;
import javax.slee.facilities.TimerOptions;
import javax.slee.facilities.TimerPreserveMissed;
import javax.slee.facilities.Tracer;

import com.opencloud.sleetck.lib.resource.events.TCKResourceEventX;
import com.opencloud.sleetck.lib.sbbutils.BaseTCKSbb;
import com.opencloud.sleetck.lib.sbbutils.TCKSbbUtils;

/*
 * AssertionID(1113097): Test creating a TimerOptions object for 
 * the specified timer behavior
 */
public abstract class Test1113097Sbb extends BaseTCKSbb {

    // Initial event
    public void onTCKResourceEventX1(TCKResourceEventX event, ActivityContextInterface aci) {

        try {
            tracer = this.getSbbContext().getTracer("com.test");
            tracer.info("Received " + event + " message", null);

            doTest1113097Test();
        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }
    }

    private void sendResultToTCK(String testName, boolean result, int failedAssertionID,  String message) throws Exception {
        HashMap sbbData = new HashMap();
        sbbData.put("testname", testName);
        sbbData.put("result", result ? "pass" : "fail");
        sbbData.put("message", message);
        sbbData.put("id", new Integer(failedAssertionID));
        TCKSbbUtils.getResourceInterface().sendSbbMessage(sbbData);
    }

    private void doTest1113097Test() throws Exception {
        TimerOptions timerOptions = null;
        // 1113097
        try {
            timerOptions = new TimerOptions(1000, TimerPreserveMissed.ALL);
            if (timerOptions != null) {
                tracer.info("got expected TimerOptions object", null);
                sendResultToTCK("Test1113097Test", true, 1113097,"The constructor TimerOptions(long timeout, TimerPreserveMissed preserveMissed) test passed");
            } else {
                tracer.info("got unexpected TimerOptions object", null);
                sendResultToTCK("Test1113097Test", false, 1113097, "Invalid TimerOptions returned for the specified timer behavior");
            }
        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }
    }

    private Tracer tracer;
}
