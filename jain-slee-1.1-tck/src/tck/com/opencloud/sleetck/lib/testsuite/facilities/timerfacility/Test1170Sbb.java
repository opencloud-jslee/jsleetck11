/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.facilities.timerfacility;

import com.opencloud.sleetck.lib.resource.events.TCKResourceEventX;
import com.opencloud.sleetck.lib.sbbutils.BaseTCKSbb;
import com.opencloud.sleetck.lib.sbbutils.TCKSbbUtils;

import javax.naming.InitialContext;
import javax.slee.ActivityContextInterface;
import javax.slee.facilities.TimerEvent;
import javax.slee.facilities.TimerFacility;
import javax.slee.facilities.TimerID;
import javax.slee.facilities.TimerOptions;
import java.util.HashMap;

public abstract class Test1170Sbb extends BaseTCKSbb {

    private static final String JNDI_TIMERFACILITY_NAME = "java:comp/env/slee/facilities/timer";

    // Initial event
    public void onTCKResourceEventX1(TCKResourceEventX ev, ActivityContextInterface aci) {

        try {
            TimerOptions options = new TimerOptions();
            options.setTimeout(200000);

            TimerFacility facility = (TimerFacility) new InitialContext().lookup(JNDI_TIMERFACILITY_NAME);

            setFirstTimer(facility.setTimer(aci, null, System.currentTimeMillis(), options));
            setSecondTimer(facility.setTimer(aci, null, System.currentTimeMillis(), Long.MAX_VALUE, 1, options));

            facility.cancelTimer(getFirstTimer());

        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }

    }

    public void onTimerEvent(TimerEvent event, ActivityContextInterface aci) {

        try {

            HashMap map = new HashMap();

            if (event.getTimerID().equals(getFirstTimer())) {
                map.put("Result", new Boolean(false));
                map.put("Message", "First timer received even though it was cancelled.");
                TCKSbbUtils.getResourceInterface().sendSbbMessage(map);
                return;
            }


            if (event.getTimerID().equals(getSecondTimer())) {
                map.put("Result", new Boolean(true));
                map.put("Message", "Ok");
                TCKSbbUtils.getResourceInterface().sendSbbMessage(map);
                return;
            }

            map.put("Result", new Boolean(false));
            map.put("Message", "Unexpected timer event received.");
            TCKSbbUtils.getResourceInterface().sendSbbMessage(map);
            return;


        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }

    }

    public abstract void setFirstTimer(TimerID timerID);
    public abstract TimerID getFirstTimer();

    public abstract void setSecondTimer(TimerID timerID);
    public abstract TimerID getSecondTimer();

}

