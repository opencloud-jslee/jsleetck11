/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.events.sbbabstractclass;

import javax.slee.ActivityContextInterface;
import javax.slee.Address;
import javax.slee.EventContext;
import javax.slee.ServiceID;
import javax.slee.facilities.Tracer;

import com.opencloud.sleetck.lib.resource.events.TCKResourceEventX;
import com.opencloud.sleetck.lib.sbbutils.BaseTCKSbb;
import com.opencloud.sleetck.lib.sbbutils.TCKSbbUtils;

/**
 * AssertionID(1108066): Test An SBB can only have one event handler method 
 * for each event type.
 */
public abstract class Test1108066Sbb2 extends BaseTCKSbb {

    // Initial event method.
    public void onTCKResourceEventX1(TCKResourceEventX event, ActivityContextInterface aci, EventContext context) {
        try {
            tracer = getSbbContext().getTracer("com.test");
            tracer.info("Received event TCKResourceEventX");
            Test1108066Event testEvent = new Test1108066Event();

            ServiceID serviceID = context.getService();
            Address address = context.getAddress();
            ActivityContextInterface activityContextInterface = context.getActivityContextInterface();

            fireTest1108066Event(testEvent, activityContextInterface, address, serviceID);

        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }
    }

    public void onTest1108066Event(Test1108066Event event, ActivityContextInterface aci, EventContext context) {
        try {
            tracer = getSbbContext().getTracer("com.test");
            tracer.info("Received the first event " + event + " with EventConext.");

        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }

    }

    public void onTest1108066SecondEvent(Test1108066Event event, ActivityContextInterface aci, EventContext context) {
        try {
            tracer = getSbbContext().getTracer("com.test");
            tracer.info("Received the second event " + event + " with EventConext.");

        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }

    }

    public abstract void fireTest1108066Event(Test1108066Event event, ActivityContextInterface aci, Address address,
            ServiceID serviceID);

    private Tracer tracer;
}
