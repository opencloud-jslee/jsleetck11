/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.events.sbbabstractclass;

import java.util.HashMap;

import javax.slee.ActivityContextInterface;
import javax.slee.Address;
import javax.slee.EventContext;
import javax.slee.facilities.Tracer;
import javax.slee.ServiceID;

import com.opencloud.sleetck.lib.resource.events.TCKResourceEventX;
import com.opencloud.sleetck.lib.sbbutils.BaseTCKSbb;
import com.opencloud.sleetck.lib.sbbutils.TCKSbbUtils;

/**
 * AssertionID(1108053): Test An additional fire event method has been 
 * added. This method allows the SBB to fire an event to a particular 
 * target Service on an Activity Context.
 */
public abstract class Test1108053Sbb1 extends BaseTCKSbb {

    // Initial event method.
    public void onTCKResourceEventX1(TCKResourceEventX event, ActivityContextInterface aci, EventContext context) {
        try {
            tracer = getSbbContext().getTracer("com.test");
            tracer.info("Sbb1: Received a TCK event " + event);

            Test1108053Event testEvent = new Test1108053Event();
            ServiceID serviceID = context.getService();

            //1108053:event_class==null;
            boolean passed1108053 = false;
            try {
                fireTest1108053Event(null, aci, null, serviceID);
            } catch (java.lang.NullPointerException e) {
                tracer.info("got expected NullPointerException when the event class is null", null);
                passed1108053 = true;
            }

            if (!passed1108053) {
                sendResultToTCK(1108053, "fireEventName(null, ActivityContextInterface, Address, ServiceID) "
                        + "should have thrown java.lang.NullPointerException.", "Test1108053Test", false);
                return;
            }

            //1108053:ActivityContextInterface==null;
            passed1108053 = false;
            try {
                fireTest1108053Event(testEvent, null, null, serviceID);
            } catch (java.lang.NullPointerException e) {
                tracer.info("got expected NullPointerException when the ActivityContextInterface is null", null);
                passed1108053 = true;
            }

            if (!passed1108053) {
                sendResultToTCK(1108053, "fireEventName(EventClass, null, Address, ServiceID) "
                        + "should have thrown java.lang.NullPointerException.", "Test1108053Test", false);
                return;
            }

            fireTest1108053Event(testEvent, aci, null, serviceID);

        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }
    }

    public void onTest1108053Event(Test1108053Event event, ActivityContextInterface aci, EventContext context) {
        try {
            tracer = getSbbContext().getTracer("com.test");
            tracer.info("Sbb1: Received a custom event " + event);
            sendResultToTCK(1108053, "Sbb1: SBB entities belonging to all Services are eligible to receive the event."
                    + "Sbb1 received the custom event " + event, "Test1108053Test", true);

        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }

    }

    public abstract void fireTest1108053Event(Test1108053Event event, ActivityContextInterface aci, Address address,
            ServiceID serviceID);

    private void sendResultToTCK(int failedAssertionID, String message, String testName, boolean result) throws Exception {
        HashMap sbbData = new HashMap();
        sbbData.put("testname", testName);
        sbbData.put("result", result ? "pass" : "fail");
        sbbData.put("message", message);
        sbbData.put("id", new Integer(failedAssertionID));
        TCKSbbUtils.getResourceInterface().sendSbbMessage(sbbData);
    }

    private Tracer tracer;
}
