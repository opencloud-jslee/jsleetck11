/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.resource.context;

import java.util.HashMap;

import com.opencloud.sleetck.lib.TCKTestErrorException;
import com.opencloud.sleetck.lib.TCKTestFailureException;
import com.opencloud.sleetck.lib.TCKTestResult;
import com.opencloud.sleetck.lib.testsuite.resource.BaseResourceTest;
import com.opencloud.sleetck.lib.testsuite.resource.RAMethods;

/**
 * Test to ensure that getTracer() is returning a correct value.
 * <p>
 * Test assertion ID: 1115180, 1115181, 1115182
 */
public class Test1115180Test extends BaseResourceTest {

    private static final int ASSERTION_ID = 1115180;

    public TCKTestResult run() throws Exception {
        HashMap resultmap = sendMessage(RAMethods.getTracer);
        if (resultmap == null)
            throw new TCKTestFailureException(ASSERTION_ID, "Test timed out while waiting for response to getTracer()");
        Object result = resultmap.get("result1");
        if (result instanceof Exception)
            throw new TCKTestFailureException(ASSERTION_ID, "Exception thrown while invoking getTracer()", (Exception) result);
        if (result instanceof String)
            throw new TCKTestFailureException(ASSERTION_ID, "An error occured while invoking getTracer(): " + result);
        if (!Boolean.TRUE.equals(result))
            throw new TCKTestErrorException("Unexpected result received while invoking getTracer(): " + result);

        result = resultmap.get("result2");
        if (!(result instanceof NullPointerException))
            throw new TCKTestFailureException(1115181, "getTracer() failed to throw a NullPointerException when given a null traceName");

        result = resultmap.get("result3");
        if (!(result instanceof IllegalArgumentException))
            throw new TCKTestFailureException(1115182,
                    "getTracer() failed to throw an IllegalArgumentException when given an invalid tracer name (\"com..mycompany\")");

        return TCKTestResult.passed();
    }
}
