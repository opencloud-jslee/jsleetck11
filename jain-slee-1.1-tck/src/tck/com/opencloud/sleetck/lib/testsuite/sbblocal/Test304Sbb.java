/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.sbblocal;

import javax.slee.ActivityEndEvent;
import javax.slee.ActivityContextInterface;
import javax.slee.SbbContext;
import javax.slee.Address;
import javax.slee.SbbLocalObject;
import javax.slee.facilities.Level;
import javax.slee.ChildRelation;

import javax.slee.nullactivity.NullActivityFactory;
import javax.slee.nullactivity.NullActivity;
import javax.slee.nullactivity.NullActivityContextInterfaceFactory;
import javax.slee.facilities.ActivityContextNamingFacility;

import com.opencloud.sleetck.lib.sbbutils.BaseTCKSbb;
import com.opencloud.sleetck.lib.sbbutils.TCKSbbUtils;
import com.opencloud.sleetck.lib.sbbutils.TCKSbbConstants;
import com.opencloud.sleetck.lib.resource.events.TCKResourceEventX;
import com.opencloud.sleetck.lib.resource.TCKActivityID;
import com.opencloud.sleetck.lib.resource.sbbapi.TCKActivity;
import com.opencloud.sleetck.lib.resource.sbbapi.TCKResourceSbbInterface;
import com.opencloud.sleetck.lib.resource.sbbapi.TCKActivityContextInterfaceFactory;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;


/**
 * If an SBB does not define an SBB specific local interface, then
 * the SBB's local interface is SBBLocalObject.
 */

public abstract class Test304Sbb extends BaseTCKSbb {

    public void sbbCreate() throws javax.slee.CreateException {
        setChildCount(0);
        setParentEnded(false);
    }

    public void sbbPostCreate() throws javax.slee.CreateException {
        try {

            NullActivityFactory factory = (NullActivityFactory) TCKSbbUtils.getSbbEnvironment().lookup("slee/nullactivity/factory");
            NullActivity nullActivity = factory.createNullActivity();

            NullActivityContextInterfaceFactory aciFactory = (NullActivityContextInterfaceFactory) TCKSbbUtils.getSbbEnvironment().lookup("slee/nullactivity/activitycontextinterfacefactory");
            ActivityContextInterface nullACI = aciFactory.getActivityContextInterface(nullActivity);


            // Register this nullACI with the naming facitily.
            ActivityContextNamingFacility namingFacility = (ActivityContextNamingFacility) TCKSbbUtils.getSbbEnvironment().lookup("slee/facilities/activitycontextnaming");
            namingFacility.bind(nullACI, "Test304NullActivity");

            // Create the child and attach to the NullActivity.
            nullACI.attach(getChild().create());

            // Attach this SBB to the NullActivity.
            nullACI.attach(getSbbContext().getSbbLocalObject());

        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }
    }

    public void onTCKResourceEventX1(TCKResourceEventX event, ActivityContextInterface aci) {
        try {
            ArrayList copy = new ArrayList();
            // copy list of children to new collection so we don't concurrently modify the iterator
            for (Iterator iter = getChild().iterator(); iter.hasNext(); ) copy.add(iter.next());

            Iterator iter = copy.iterator();
            while (iter.hasNext()) {
                SbbLocalObject child = (SbbLocalObject) iter.next();
                child.remove();
            }
        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }
    }

    public void onSbbEndingEvent(Test304Event event, ActivityContextInterface aci) {
        try {

            // Must receive Parent event, then two child events.
            if (event.isChild() == false) {
                if (getChildCount() > 0) { // Seen a child event before this event?
                    HashMap map = new HashMap();
                    map.put("Result", new Boolean(false));
                    map.put("Message", "A Child SBB was destroyed before its parent.");
                    TCKSbbUtils.getResourceInterface().sendSbbMessage(map);
                    return;
                }
                setParentEnded(true);
                return;
            }

            // If this is a child sending the event, increase the child count.
            // If all children have responded, check the test result.
            if (event.isChild()) {
                setChildCount(getChildCount() + 1);

                if (getChildCount() == 2) {
                    // Unbind the NullActivity - and perhaps also end it.
                    ActivityContextNamingFacility namingFacility = (ActivityContextNamingFacility) TCKSbbUtils.getSbbEnvironment().lookup("slee/facilities/activitycontextnaming");
                    ActivityContextInterface nullACI = namingFacility.lookup("Test304NullActivity");
                    namingFacility.unbind("Test304NullActivity");

                    NullActivity activity = (NullActivity) nullACI.getActivity();
                    activity.endActivity();

                    // If this is the parent of the two children, check the child count.
                    HashMap map = new HashMap();
                    if (getParentEnded() == false) {
                        map.put("Result", new Boolean(false));
                        map.put("Message", "The Parent SBB did not end before the child SBBs.");
                    } else {
                        map.put("Result", new Boolean(true));
                        map.put("Message", "Ok");
                    }
                        TCKSbbUtils.getResourceInterface().sendSbbMessage(map);
                }
            }

        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }
    }


    public abstract void setChildCount(int count);
    public abstract int getChildCount();

    public abstract void setParentEnded(boolean ended);
    public abstract boolean getParentEnded();

    public abstract ChildRelation getChild();

}
