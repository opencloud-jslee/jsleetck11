/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.runtime.security;

import javax.management.ObjectName;
import javax.management.ReflectionException;
import javax.slee.profile.ProfileSpecificationID;

import com.opencloud.sleetck.lib.AbstractSleeTCKTest;
import com.opencloud.sleetck.lib.TCKTestErrorException;
import com.opencloud.sleetck.lib.TCKTestResult;
import com.opencloud.sleetck.lib.infra.SleeTCKComponentConstants;
import com.opencloud.sleetck.lib.testutils.Assert;
import com.opencloud.sleetck.lib.testutils.ProfileUtils;
import com.opencloud.sleetck.lib.testutils.jmx.ProfileMBeanProxy;
import com.opencloud.sleetck.lib.testutils.jmx.ProfileProvisioningMBeanProxy;


public class Test1112015Test extends AbstractSleeTCKTest {
    public static final String DU_PATH_PARAM= "serviceDUPath";
    public static final String SPEC_NAME = "Test1112015Profile";
    public static final String SPEC_VERSION = "1.1";
    public static final String PROFILE_TABLE_NAME = "Test1112015ProfileTable";
    public static final String PROFILE_NAME = "Test1112015Profile";
    public static final int    TEST_ID = 1112015;

    private void setupServiceAndProfile(String specName, String DUPathParam) throws Exception {
        //Deploy profile spec
        if (null == setupService(DUPathParam)) {
            throw new TCKTestErrorException("Could not deploy Test1112015Profile service.");
        }
        
        //Create a profile table
        ProfileSpecificationID specID = new ProfileSpecificationID(specName, SleeTCKComponentConstants.TCK_VENDOR, SPEC_VERSION);
        profileProvisioning.createProfileTable(specID, PROFILE_TABLE_NAME);
        getLog().fine("Added profile table "+PROFILE_TABLE_NAME);

        //Create a profile via management view
        profile = profileProvisioning.createProfile(PROFILE_TABLE_NAME, PROFILE_NAME);
        profileProxy = utils().getMBeanProxyFactory().createProfileMBeanProxy(profile);
        if (null == profileProxy) {
            throw new TCKTestErrorException("Could not create profile MBean proxy.");
        }
        profileProxy.commitProfile();
        getLog().fine("Created profile "+PROFILE_NAME+" for profile table "+PROFILE_TABLE_NAME);
    }

    public TCKTestResult run() throws Exception {
        TCKTestResult t=null;
        getLog().fine("Run tests for profile spec "+SPEC_NAME);
        
        try {
            //invoke management method declared in ProfileManagement interface
            t = (TCKTestResult)utils().getMBeanFacade().invoke(profile, "manage", new Object[]{}, new String[]{});
            getLog().fine("Called management method on ProfileManagement interface.");
        } catch (ReflectionException e) {
            Assert.fail(TEST_ID, "Method not found when invoking management method declared in ProfileManagement interface.");
        }

        return (null!=t) ? t : TCKTestResult.failed(TEST_ID, "Test result is null");
    }

    public void setUp() throws Exception {
        profileUtils = new ProfileUtils(utils());
        profileProvisioning = profileUtils.getProfileProvisioningProxy();
        //Run tests for spec with distinct ProfileManagement interface
        setupServiceAndProfile(SPEC_NAME, DU_PATH_PARAM);
    }

    public void tearDown() throws Exception {
        try {
            profileProxy.closeProfile();
        } finally {
            try {
                profileProvisioning.removeProfileTable(PROFILE_TABLE_NAME);
            } finally {
                super.tearDown();
            }
        }
    }

    private ProfileUtils profileUtils;
    private ProfileProvisioningMBeanProxy profileProvisioning;
    private ObjectName profile;
    private ProfileMBeanProxy profileProxy;
}
