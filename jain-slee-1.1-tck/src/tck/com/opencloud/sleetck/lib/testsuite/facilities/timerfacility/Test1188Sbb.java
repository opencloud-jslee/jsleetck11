/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.facilities.timerfacility;

import com.opencloud.sleetck.lib.resource.events.TCKResourceEventX;
import com.opencloud.sleetck.lib.sbbutils.BaseTCKSbb;
import com.opencloud.sleetck.lib.sbbutils.TCKSbbUtils;

import javax.naming.InitialContext;
import javax.slee.ActivityContextInterface;
import javax.slee.Address;
import javax.slee.AddressPlan;
import javax.slee.ActivityEndEvent;
import javax.slee.facilities.Level;
import javax.slee.facilities.TimerEvent;
import javax.slee.facilities.TimerFacility;
import javax.slee.facilities.TimerOptions;
import javax.slee.nullactivity.NullActivity;
import javax.slee.nullactivity.NullActivityContextInterfaceFactory;
import javax.slee.nullactivity.NullActivityFactory;

/**
 * 
 */
public abstract class Test1188Sbb extends BaseTCKSbb {

    public void onTCKResourceEventX1(TCKResourceEventX event, ActivityContextInterface aci) {
        try {
            createTrace("onTCKResourceEventX1 - event received");
            NullActivityFactory nullActivityFactory = (NullActivityFactory)
                    new InitialContext().lookup("java:comp/env/slee/nullactivity/factory");
            NullActivity activity = nullActivityFactory.createNullActivity();
            NullActivityContextInterfaceFactory nullACIFactory = (NullActivityContextInterfaceFactory)
                    new InitialContext().lookup("java:comp/env/slee/nullactivity/activitycontextinterfacefactory");
            ActivityContextInterface nullACI = nullACIFactory.getActivityContextInterface(activity);

            TimerFacility timerFacility = (TimerFacility)
                    new InitialContext().lookup("java:comp/env/slee/facilities/timer");

            timerFacility.setTimer(nullACI, new Address(AddressPlan.IP,  "1.0.0.1"),
                                   System.currentTimeMillis() + 2000, 1000, 1, new TimerOptions());

        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }
    }

    public void onTimerEvent(TimerEvent event, ActivityContextInterface aci) {
        createTrace("onTimerEvent: enter");
        try {
            TCKSbbUtils.getResourceInterface().callTest(new Object[] {"onTimerEvent", Boolean.valueOf(aci.isEnding())});
            // we only want to get the timer event once
            TimerFacility timerFacility = (TimerFacility)
                    new InitialContext().lookup("java:comp/env/slee/facilities/timer");
            timerFacility.cancelTimer(event.getTimerID());
            aci.detach(getSbbContext().getSbbLocalObject());
        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }
        createTrace("onTimerEvent: exit");
    }

    public void onActivityEndEvent(ActivityEndEvent event, ActivityContextInterface aci) {

        if (aci.getActivity() instanceof javax.slee.nullactivity.NullActivity) {
            createTrace("onActivityEndEvent: enter");
            createTrace("ActivityEndEvent received on activity " + aci.getActivity());
            
            try {
                TCKSbbUtils.getResourceInterface().callTest(new Object[] {"onActivityEndEvent"});
            } catch (Exception e) {
                TCKSbbUtils.handleException(e);
            }
            createTrace("onActivityEndEvent: exit");
        }
    }

    private void createTrace(String message) {
        try {
            TCKSbbUtils.createTrace(getSbbID(), Level.INFO, message, null);
        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }
    }
}
