/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.profiles.profilespec;

import com.opencloud.sleetck.lib.AbstractSleeTCKTest;
import com.opencloud.sleetck.lib.TCKTestErrorException;
import com.opencloud.sleetck.lib.TCKTestFailureException;
import com.opencloud.sleetck.lib.TCKTestResult;


public class Test1110047Test extends AbstractSleeTCKTest {

    private static final String IMPL_PROF_LOCAL= "implLocalDUPath";
    private static final String NO_BUS_METH_DU_PATH_PARAM= "noBusMethDUPath";
    private static final String NO_MNGMT_METH_DU_PATH_PARAM= "noMngmtMethDUPath";
    private static final String NO_LIFE_CYCLE_METH_DU_PATH_PARAM= "noLifeCycleMethDUPath";
    private static final String ALL_OK_DU_PATH_PARAM= "allOKDUPath";

    /**
     * This test trys to deploy various profile specs that are invalid due to omitted
     * management, business, or lifecycle callback method implementations.
     * The test will fail if one of the invalid specs is deployed into the SLEE successfully.
     */
    public TCKTestResult run() throws Exception {

        //Profile abstract class does implement the spec's Profile Local interface
        getLog().fine("Deploying profile spec.");
        try {
            setupService(IMPL_PROF_LOCAL);
            return TCKTestResult.failed(1110047,"Deployment of Profile spec with Profile abstract class implementing Profile Local interface directly should fail but was successful.");
        }
        catch (TCKTestErrorException e) {
            getLog().fine("Caught expected exception when trying to deploy profile spec. Exception: "+e.getMessage());
        }

        //Profile abstract class does not implement one of the Profile Local interface's business methods
        getLog().fine("Deploying profile spec.");
        try {
            setupService(NO_BUS_METH_DU_PATH_PARAM);
            return TCKTestResult.failed(1110047,"Deployment of Profile spec with Profile abstract class not implementing Profile Local interface's business methods should fail but was successful.");
        }
        catch (TCKTestErrorException e) {
            getLog().fine("Caught expected exception when trying to deploy profile spec. Exception: "+e.getMessage());
        }

        //Profile abstract class does not implement one of the Profile Management interface's management methods
        getLog().fine("Deploying profile spec.");
        try {
            setupService(NO_MNGMT_METH_DU_PATH_PARAM);
            return TCKTestResult.failed(1110047,"Deployment of Profile spec with Profile abstract class not implementing Profile Management interface's management methods should fail but was successful.");
        }
        catch (TCKTestErrorException e) {
            getLog().fine("Caught expected exception when trying to deploy profile spec. Exception: "+e.getMessage());
        }

        //Profile abstract class does not implement life cycle callbacks
        getLog().fine("Deploying profile spec.");
        try {
            setupService(NO_LIFE_CYCLE_METH_DU_PATH_PARAM);
            return TCKTestResult.failed(1110047,"Deployment of Profile spec with Profile abstract class not implementing life cycle callback methods should fail but was successful.");
        }
        catch (TCKTestErrorException e) {
            getLog().fine("Caught expected exception when trying to deploy profile spec. Exception: "+e.getMessage());
        }

        //Make sure that the correct Profile abstract class deploys fine.
        getLog().fine("Deploying profile spec.");
        try {
            setupService(ALL_OK_DU_PATH_PARAM);
            getLog().fine("Correct Profile abstract class deploys fine.");
        }
        catch (TCKTestErrorException e) {
            return TCKTestResult.failed(new TCKTestFailureException(1110047,"Deployment of valid profile spec failed.",e));
        }

        return TCKTestResult.passed();
    }

    public void setUp() throws Exception {

    }

    public void tearDown() throws Exception {

        super.tearDown();
    }
}
