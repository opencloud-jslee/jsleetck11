/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.facilities.tracefacility.tracelevel;

import javax.slee.facilities.TraceLevel;

import com.opencloud.sleetck.lib.AbstractSleeTCKTest;
import com.opencloud.sleetck.lib.TCKTestResult;

/*
 * AssertionID(1113155): Test Each of the is<Level> methods determines if this TraceLevel object 
 * represents the <LEVEL> level, and is equivalent to (this == <LEVEL>).
 * 
 * AssertionID(1113379): Test This method is effectively equivalent to the conditional test: 
 * (this == OFF), ie. the code: if (traceLevel.isOff()) ... is interchangeable with the code:  
 * if (traceLevel == TraceLevel.OFF) ... 
 * 
 * AssertionID(1113383): Test This method is effectively equivalent to the conditional test: 
 * (this == SEVERE), ie. the code: if (traceLevel.isSevere()) ...  is interchangeable with 
 * the code: if (traceLevel == TraceLevel.SEVERE) ... 
 *
 * AssertionID(1113387): Test This method is effectively equivalent to the conditional test: 
 * (this == WARNING), ie. the code: if (traceLevel.isWarning()) ... is interchangeable with 
 * the code: if (traceLevel == TraceLevel.WARNING) ... 
 *
 * AssertionID(1113391): Test This method is effectively equivalent to the conditional test: 
 * (this == INFO), ie. the code: if (traceLevel.isMinor()) ... is interchangeable with the 
 * code: if (traceLevel == TraceLevel.INFO) ... 
 * 
 * AssertionID(1113395): Test This method is effectively equivalent to the conditional test: 
 * (this == CONFIG), ie. the code: if (traceLevel.isConfig()) ... is interchangeable with the 
 * code: if (traceLevel == TraceLevel.CONFIG) ... 
 *
 * AssertionID(1113399): Test This method is effectively equivalent to the conditional test: 
 * (this == FINE), ie. the code: if (traceLevel.isFine()) ... is interchangeable with the code:
 *  if (traceLevel == TraceLevel.FINE) ... 
 *
 * AssertionID(1113403): Test This method is effectively equivalent to the conditional test: 
 * (this == FINER), ie. the code: if (traceLevel.isFiner()) ... is interchangeable with the code: 
 * if (traceLevel == TraceLevel.FINER) ... 
 *
 * AssertionID(1113407): Test This method is effectively equivalent to the conditional test: 
 * (this == FINEST), ie. the code: if (traceLevel.isFinest()) ... is interchangeable with the code:
 *  if (traceLevel == TraceLevel.FINEST) ... 
 *
 */
public class Test1113155Test extends AbstractSleeTCKTest {

    /**
     * Perform the actual test.
     */

    public TCKTestResult run() throws Exception {
        TraceLevel traceLevel = null;

        // 1113155
        traceLevel = TraceLevel.OFF;
        try {
            if (!traceLevel.isOff()) {
                return TCKTestResult.failed(1113155, "This traceLevel.isOff() method didn't return true");
            }
            if (traceLevel != TraceLevel.OFF) {
                return TCKTestResult.failed(1113155, "This traceLevel argument didn't return TraceLevel.OFF");
            }
            getLog().info(
                    "Test Each of the isOff() methods determines if this TraceLevel object "
                            + "represents the TraceLevel.OFF level, and is equivalent to this == TraceLevel.OFF");
        } catch (Exception e) {
            return TCKTestResult.failed(1113155, "traceLevel.isOff() threw an exception.");
        }

        // 1113379
        try {
            if (traceLevel.isOff() != (traceLevel == TraceLevel.OFF)) {
                return TCKTestResult.failed(1113379, "This isOff() method didn't equivalent to "
                        + "the conditional test: (this == OFF)");
            }
            getLog().info("This isOff() method is effectively equivalent to the conditional " + "test: this == OFF");
        } catch (Exception e) {
            return TCKTestResult.failed(1113379, "traceLevel.isOff() equal test threw an exception.");
        }

        // 1113155
        traceLevel = TraceLevel.SEVERE;
        try {
            if (!traceLevel.isSevere()) {
                return TCKTestResult.failed(1113155, "This traceLevel.isSevere() method didn't return true");
            }
            if (traceLevel != TraceLevel.SEVERE) {
                return TCKTestResult.failed(1113155, "This traceLevel argument didn't return TraceLevel.SEVERE");
            }
            getLog().info(
                    "Test Each of the isSevere() methods determines if this TraceLevel object "
                            + "represents the TraceLevel.SEVERE level, and is equivalent to this == TraceLevel.SEVERE");
        } catch (Exception e) {
            return TCKTestResult.failed(1113155, "traceLevel.isSevere() threw an exception.");
        }

        // 1113383
        try {
            if (traceLevel.isSevere() != (traceLevel == TraceLevel.SEVERE)) {
                return TCKTestResult.failed(1113383, "This isSevere() method didn't equivalent to "
                        + "the conditional test: (this == SEVERE)");
            }
            getLog().info(
                    "This isSevere() method is effectively equivalent to the conditional " + "test: this == SEVERE");
        } catch (Exception e) {
            return TCKTestResult.failed(1113383, "traceLevel.isSevere() equal test threw an exception.");
        }

        // 1113155
        traceLevel = TraceLevel.WARNING;
        try {
            if (!traceLevel.isWarning()) {
                return TCKTestResult.failed(1113155, "This traceLevel.isWarning() method didn't return true");
            }
            if (traceLevel != TraceLevel.WARNING) {
                return TCKTestResult.failed(1113155, "This traceLevel argument didn't return TraceLevel.WARNING");
            }
            getLog()
                    .info(
                            "Test Each of the isWarning() methods determines if this TraceLevel object "
                                    + "represents the TraceLevel.WARNING level, and is equivalent to this == TraceLevel.WARNING");
        } catch (Exception e) {
            return TCKTestResult.failed(1113155, "traceLevel.isWarning() threw an exception.");
        }

        // 1113387
        try {
            if (traceLevel.isWarning() != (traceLevel == TraceLevel.WARNING)) {
                return TCKTestResult.failed(1113387, "This isWarning() method didn't equivalent to "
                        + "the conditional test: (this == WARNING)");
            }
            getLog().info(
                    "This isWarning() method is effectively equivalent to the conditional " + "test: this == WARNING");
        } catch (Exception e) {
            return TCKTestResult.failed(1113387, "traceLevel.isWarning() equal test threw an exception.");
        }

        // 1113155
        traceLevel = TraceLevel.INFO;
        try {
            if (!traceLevel.isInfo()) {
                return TCKTestResult.failed(1113155, "This traceLevel.isInfo() method didn't return true");
            }
            if (traceLevel != TraceLevel.INFO) {
                return TCKTestResult.failed(1113155, "This traceLevel argument didn't return TraceLevel.INFO");
            }
            getLog().info(
                    "Test Each of the isInfo() methods determines if this TraceLevel object "
                            + "represents the TraceLevel.INFO level, and is equivalent to this == TraceLevel.INFO");
        } catch (Exception e) {
            return TCKTestResult.failed(1113155, "traceLevel.isInfo() threw an exception.");
        }

        // 1113391
        try {
            if (traceLevel.isInfo() != (traceLevel == TraceLevel.INFO)) {
                return TCKTestResult.failed(1113391, "This isInfo() method didn't equivalent to "
                        + "the conditional test: (this == INFO)");
            }
            getLog().info("This isInfo() method is effectively equivalent to the conditional " + "test: this == INFO");
        } catch (Exception e) {
            return TCKTestResult.failed(1113391, "traceLevel.isInfo() equal test threw an exception.");
        }

        // 1113155
        traceLevel = TraceLevel.CONFIG;
        try {
            if (!traceLevel.isConfig()) {
                return TCKTestResult.failed(1113155, "This traceLevel.isConfig() method didn't return true");
            }
            if (traceLevel != TraceLevel.CONFIG) {
                return TCKTestResult.failed(1113155, "This traceLevel argument didn't return TraceLevel.CONFIG");
            }
            getLog().info(
                    "Test Each of the isConfig() methods determines if this TraceLevel object "
                            + "represents the TraceLevel.CONFIG level, and is equivalent to this == TraceLevel.CONFIG");
        } catch (Exception e) {
            return TCKTestResult.failed(1113155, "traceLevel.isConfig() threw an exception.");
        }

        // 1113395
        try {
            if (traceLevel.isConfig() != (traceLevel == TraceLevel.CONFIG)) {
                return TCKTestResult.failed(1113395, "This isConfig() method didn't equivalent to "
                        + "the conditional test: (this == CONFIG)");
            }
            getLog().info(
                    "This isConfig() method is effectively equivalent to the conditional " + "test: this == CONFIG");
        } catch (Exception e) {
            return TCKTestResult.failed(1113395, "traceLevel.isConfig() equal test threw an exception.");
        }

        // 1113155
        traceLevel = TraceLevel.FINE;
        try {
            if (!traceLevel.isFine()) {
                return TCKTestResult.failed(1113155, "This traceLevel.isFine() method didn't return true");
            }
            if (traceLevel != TraceLevel.FINE) {
                return TCKTestResult.failed(1113155, "This traceLevel argument didn't return TraceLevel.FINE");
            }
            getLog().info(
                    "Test Each of the isFine() methods determines if this TraceLevel object "
                            + "represents the TraceLevel.FINE level, and is equivalent to this == TraceLevel.FINE");
        } catch (Exception e) {
            return TCKTestResult.failed(1113155, "traceLevel.isFine() threw an exception.");
        }

        // 1113399
        try {
            if (traceLevel.isFine() != (traceLevel == TraceLevel.FINE)) {
                return TCKTestResult.failed(1113399, "This isFine() method didn't equivalent to "
                        + "the conditional test: (this == FINE)");
            }
            getLog().info("This isFine() method is effectively equivalent to the conditional " + "test: this == FINE");
        } catch (Exception e) {
            return TCKTestResult.failed(1113399, "traceLevel.isFine() equal test threw an exception.");
        }

        // 1113155
        traceLevel = TraceLevel.FINER;
        try {
            if (!traceLevel.isFiner()) {
                return TCKTestResult.failed(1113155, "This traceLevel.isFiner() method didn't return true");
            }
            if (traceLevel != TraceLevel.FINER) {
                return TCKTestResult.failed(1113155, "This traceLevel argument didn't return TraceLevel.FINER");
            }
            getLog().info(
                    "Test Each of the isFiner() methods determines if this TraceLevel object "
                            + "represents the TraceLevel.FINER level, and is equivalent to this == TraceLevel.FINER");
        } catch (Exception e) {
            return TCKTestResult.failed(1113155, "traceLevel.isFiner() threw an exception.");
        }

        // 1113403
        try {
            if (traceLevel.isFiner() != (traceLevel == TraceLevel.FINER)) {
                return TCKTestResult.failed(1113403, "This isFiner() method didn't equivalent to "
                        + "the conditional test: (this == FINER)");
            }
            getLog()
                    .info("This isFiner() method is effectively equivalent to the conditional " + "test: this == FINER");
        } catch (Exception e) {
            return TCKTestResult.failed(1113403, "traceLevel.isFiner() equal test threw an exception.");
        }

        // 1113155
        traceLevel = TraceLevel.FINEST;
        try {
            if (!traceLevel.isFinest()) {
                return TCKTestResult.failed(1113155, "This traceLevel.isFinest() method didn't return true");
            }
            if (traceLevel != TraceLevel.FINEST) {
                return TCKTestResult.failed(1113155, "This traceLevel argument didn't return TraceLevel.FINEST");
            }
            getLog().info(
                    "Test Each of the isFinest() methods determines if this TraceLevel object "
                            + "represents the TraceLevel.FINEST level, and is equivalent to this == TraceLevel.FINEST");
        } catch (Exception e) {
            return TCKTestResult.failed(1113155, "traceLevel.isFinest() threw an exception.");
        }

        // 1113407
        try {
            if (traceLevel.isFinest() != (traceLevel == TraceLevel.FINEST)) {
                return TCKTestResult.failed(1113407, "This isFinest() method didn't equivalent to "
                        + "the conditional test: (this == FINEST)");
            }
            getLog().info(
                    "This isFinest() method is effectively equivalent to the conditional " + "test: this == FINEST");
        } catch (Exception e) {
            return TCKTestResult.failed(1113407, "traceLevel.isFinest() equal test threw an exception.");
        }

        return TCKTestResult.passed();
    }

    /**
     * Do all the pre-run configuration of the test.
     */

    public void setUp() throws Exception {

    }

    /**
     * Clean up after the test.
     */

    public void tearDown() throws Exception {
    }
}
