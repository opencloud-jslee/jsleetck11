/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.management.ManagementMBean;

import com.opencloud.sleetck.lib.AbstractSleeTCKTest;
import com.opencloud.sleetck.lib.TCKTestErrorException;
import com.opencloud.sleetck.lib.TCKTestResult;

import javax.slee.management.DeploymentException;

public class Test1576Test extends AbstractSleeTCKTest {

    private static final String RA_DU_PATH_PARAM = "raDUPath";
    private static final int TEST_ID = 1576;

    /**
     * Perform the actual test.
     */
    public TCKTestResult run() throws Exception {
        try {
            String duPath = utils().getTestParams().getProperty(RA_DU_PATH_PARAM);
            utils().install(duPath);
        } catch (TCKTestErrorException e) {
            if (e.getEnclosedException().getClass().equals(DeploymentException.class)) {
                return TCKTestResult.passed();
            }
        }
        return TCKTestResult.failed(TEST_ID, "Was able to install a Resource Adaptor Type dependent on an event not yet installed.");
    }

    /**
     * Do all the pre-run configuration of the test.
     */
    public void setUp() throws Exception {}

}
