/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.profiles.profiletable;

import java.io.Serializable;

public class MyCMPType implements Serializable {

    private static final long serialVersionUID = 8806686860398806967L;

    private int value;

    public MyCMPType () {
        this(-1);
    }

    public MyCMPType(int value) {
        this.value = value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    public int getValue() {
        return this.value;
    }

    public boolean equals(Object obj) {
        return ((obj instanceof MyCMPType) && (((MyCMPType)obj).getValue() == this.getValue()));
    }

}
