/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.profiles.readonly;

import com.opencloud.sleetck.lib.TCKTestErrorException;
import com.opencloud.sleetck.lib.resource.events.TCKResourceEventX;
import com.opencloud.sleetck.lib.sbbutils.BaseTCKSbb;
import com.opencloud.sleetck.lib.sbbutils.TCKSbbUtils;
import com.opencloud.sleetck.lib.sbbutils.events.TCKSbbEvent;
import com.opencloud.sleetck.lib.sbbutils.events.TCKSbbEventImpl;
import com.opencloud.sleetck.lib.sbbutils.events2.SbbBaseMessageComposer;
import com.opencloud.sleetck.lib.sbbutils.events2.SendResultEvent;

import javax.naming.Context;
import javax.slee.ActivityContextInterface;
import javax.slee.Address;
import javax.slee.profile.ProfileFacility;
import javax.slee.profile.ProfileID;
import javax.slee.profile.ProfileTable;
import javax.slee.profile.UnrecognizedProfileNameException;
import javax.slee.profile.UnrecognizedProfileTableNameException;

import java.util.HashMap;

public abstract class Test1110069Sbb extends BaseTCKSbb {

    public static final String PROFILE_TABLE_NAME = "Test1110069ProfileTable";
    public static final String PROFILE_NAME = "Test1110069Profile";


    private void sendLogMsgCall (String msg) {
        HashMap map = SbbBaseMessageComposer.getLogMsg(msg);
        try {
            TCKSbbUtils.getResourceInterface().callTest(map);
        }
        catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }
    }

    private void sendResult(boolean result, int id, String msg, ActivityContextInterface aci) {
        HashMap map = SbbBaseMessageComposer.getSetResultMsg(result, id, msg);
        setResult(map);
        fireSendResultEvent(new SendResultEvent(), aci, null);
    }

    public void onSendResultEvent(SendResultEvent event, ActivityContextInterface aci) {

        try {
            TCKSbbUtils.getResourceInterface().sendSbbMessage(getResult());

        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }
    }

    public void onTCKResourceEventX1(TCKResourceEventX event, ActivityContextInterface aci) {
        try {

            sendLogMsgCall("Received TCKResourceEventX1.");

            fireTCKSbbEvent(new TCKSbbEventImpl(event.getMessage()),aci,null);

        } catch(Exception e) {
            TCKSbbUtils.handleException(e);
        }
    }

    public void onTCKSbbEvent(TCKSbbEvent event, ActivityContextInterface aci) {
        try {
            Context env = TCKSbbUtils.getSbbEnvironment();
            ProfileFacility profileFacility = (ProfileFacility)env.lookup("slee/facilities/profile");
            sendLogMsgCall("Obtained ProfileFacility from naming context.");

            //use profileFacility to get a profile table interface (since JAIN SLEE 1.1)
            ProfileTable profileTable = profileFacility.getProfileTable(PROFILE_TABLE_NAME);
            sendLogMsgCall("Obtained ProfileTable object through Profile Facility.");

            String msg = (String)event.getMessage();
            if (msg.equals("Create")) {
                ReadOnlyTestsProfileCMP profileCMP = (ReadOnlyTestsProfileCMP)profileTable.create(PROFILE_NAME);
                if (profileCMP == null) {
                    TCKSbbUtils.handleException(new TCKTestErrorException("profileTable.create(PROFILE_NAME) returned null when it shouldn't have."));
                    return;
                }
                sendLogMsgCall("Created profile "+PROFILE_NAME);
                fireTCKSbbEvent(new TCKSbbEventImpl("Update"), aci, null);

            }
            else if (msg.equals("Update")) {
                ReadOnlyTestsProfileCMP profileCMP = (ReadOnlyTestsProfileCMP)profileTable.find(PROFILE_NAME);
                profileCMP.setValue("newValue");
                sendLogMsgCall("Set new value for profile attribute.");
                fireTCKSbbEvent(new TCKSbbEventImpl("Check"), aci, null);

            }
            else if (msg.equals("Check")) {
                ReadOnlyTestsProfileCMP profileCMP = (ReadOnlyTestsProfileCMP)profileTable.find(PROFILE_NAME);
                if (!profileCMP.getValue().equals("newValue")) {
                    sendResult(false, 1110069, "Failed to use set/get accessor methods properly.", aci);
                    return;
                }
                sendLogMsgCall("Checked that new value has been successfully set and could be obtained by 'get' accessor.");
                fireTCKSbbEvent(new TCKSbbEventImpl("UpdateViaCMP"), aci, null);

            }
            else if (msg.equals("UpdateViaCMP")) {
                ReadOnlyTestsProfileCMP profileCMP = getProfileCMP(new ProfileID(PROFILE_TABLE_NAME, PROFILE_NAME));
                profileCMP.setValue("newValue2");
                sendLogMsgCall("Set new value for profile attribute via CMP interface.");
                fireTCKSbbEvent(new TCKSbbEventImpl("CheckViaCMP"), aci, null);

            }
            else if (msg.equals("CheckViaCMP")) {
                ReadOnlyTestsProfileCMP profileCMP = getProfileCMP(new ProfileID(PROFILE_TABLE_NAME, PROFILE_NAME));
                if (!profileCMP.getValue().equals("newValue2")) {
                    sendResult(false, 1110086, "Failed to use set/get accessor methods via ProfileCMP object properly.", aci);
                    return;
                }
                sendLogMsgCall("Checked that new value has been successfully set and could be obtained by 'get' accessor via ProfileCMP interface.");
                fireTCKSbbEvent(new TCKSbbEventImpl("Remove"), aci, null);

            }
            else if (msg.equals("Remove")) {
                boolean res = profileTable.remove(PROFILE_NAME);
                if (!res) {
                    sendResult(false, 1110069, "Sbb failed to remove profile "+PROFILE_NAME, aci);
                    return;
                }
                sendLogMsgCall("ProfileTable object reports successful removal of profile table "+PROFILE_NAME);
                fireTCKSbbEvent(new TCKSbbEventImpl("CheckRemove"), aci, null);

            }
            else if (msg.equals("CheckRemove")) {
                ReadOnlyTestsProfileCMP profileCMP = (ReadOnlyTestsProfileCMP)profileTable.find(PROFILE_NAME);
                if (profileCMP != null) {
                    sendResult(false, 1110069, "Profile "+PROFILE_NAME+ " was not removed correctly.", aci);
                    return;
                }
                sendLogMsgCall("Check whether profile was really removed after TXN committed was successful.");
                sendResult(true, 1110069, "Test successful", aci);

            }
        } catch(Exception e) {
            TCKSbbUtils.handleException(e);
        }
    }

    public abstract void fireTCKSbbEvent(TCKSbbEvent event, ActivityContextInterface aci, Address address);

    public abstract void fireSendResultEvent(SendResultEvent event, ActivityContextInterface aci, Address address);

    public abstract void setResult(HashMap map);
    public abstract HashMap getResult();

    public abstract ReadOnlyTestsProfileCMP getProfileCMP(ProfileID profileID) throws UnrecognizedProfileTableNameException, UnrecognizedProfileNameException;

}
