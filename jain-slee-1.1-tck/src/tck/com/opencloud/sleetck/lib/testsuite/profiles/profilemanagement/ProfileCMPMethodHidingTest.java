/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.profiles.profilemanagement;

import com.opencloud.sleetck.lib.AbstractSleeTCKTest;
import com.opencloud.sleetck.lib.TCKTestResult;
import com.opencloud.sleetck.lib.infra.SleeTCKComponentConstants;
import com.opencloud.sleetck.lib.testsuite.profiles.ProfileTestConstants;
import com.opencloud.sleetck.lib.testutils.Assert;
import com.opencloud.sleetck.lib.testutils.ComponentIDLookup;
import com.opencloud.sleetck.lib.testutils.ProfileUtils;
import com.opencloud.sleetck.lib.testutils.jmx.ProfileProvisioningMBeanProxy;
import com.opencloud.sleetck.lib.testutils.jmx.impl.EmptyArrays;

import javax.management.ObjectName;
import javax.management.ReflectionException;
import javax.slee.profile.ProfileSpecificationID;

/**
 * Tests assertion 971: that if the profile developer defines a Profile Management interface then this interface
 * determines the profile specification specific management methods that are made available to management clients.
 */
public class ProfileCMPMethodHidingTest extends AbstractSleeTCKTest {
    protected static final String PROFILE_TABLE_NAME = "tck.ProfileManagementTest.table";
    public static final String PROFILE_SPEC_NAME = "PMProfile";

    /**
     * Runs some tests on javax.slee.profile.ProfileManagement methods <code>markProfileDirty</code> and
     * <code>isProfileValid</code> and wrapping of exceptions thrown by management class methods.
     * @return
     * @throws Exception
     */
    public TCKTestResult run() throws Exception {
        // create the profile table
        ProfileProvisioningMBeanProxy profileProvisioning = profileUtils.getProfileProvisioningProxy();
        ProfileSpecificationID profileSpecID = new ComponentIDLookup(utils()).lookupProfileSpecificationID(
                PROFILE_SPEC_NAME, SleeTCKComponentConstants.TCK_VENDOR, "1.0");

        getLog().info("Creating profile table: " + PROFILE_TABLE_NAME);

        profileProvisioning.createProfileTable(profileSpecID, PROFILE_TABLE_NAME);
        tableCreated = true;

        // add profile
        final String name = "A";
        getLog().info("Creating profile " + name);
        ObjectName jmxObjectName = profileProvisioning.createProfile(PROFILE_TABLE_NAME, name);

        // Verify that the CMP methods that are present in the profile management interface are visible
        // Test getter
        utils().getMBeanFacade().invoke(jmxObjectName, "getValue",
                                        EmptyArrays.EMPTY_OBJECT_ARRAY, EmptyArrays.EMPTY_STRING_ARRAY);

        utils().getMBeanFacade().invoke(jmxObjectName, "editProfile",
                                        EmptyArrays.EMPTY_OBJECT_ARRAY, EmptyArrays.EMPTY_STRING_ARRAY);
        // Setter
        utils().getMBeanFacade().invoke(jmxObjectName, "setValue",
                                        new Object[]{"new value"}, new String[]{"java.lang.String"});

        // Test invoking a CMP accessor method that is not defined in the profile management interface
        // Should get a ReflectionException from the MBean server
        // Try a CMP getter that is not declared in the profile management interfacec
        try {
            utils().getMBeanFacade().invoke(jmxObjectName, "getValue2",
                                            EmptyArrays.EMPTY_OBJECT_ARRAY, EmptyArrays.EMPTY_STRING_ARRAY);
            Assert.fail(971, "Was able to invoke a CMP method that should be hidden from management clients");
        } catch (ReflectionException e) {
            getLog().info("Got expected exception: " + e);
        } catch (Exception e) {
            Assert.fail(971, "Got unexpected exception calling a CMP method that should be hidden from " +
                             "management clients: " + e);
        }

        // Try a CMP setter that is not declared in the profile management interface
        try {
            utils().getMBeanFacade().invoke(jmxObjectName, "setValue2",
                                            new Object[]{"new value"}, new String[]{"java.lang.String"});
            Assert.fail(971, "Was able to invoke a CMP method that should be hidden from management clients");
        } catch (ReflectionException e) {
            getLog().info("Got expected exception: " + e);
        } catch (Exception e) {
            Assert.fail(971, "Got unexpected exception calling a CMP method that should be hidden from " +
                             "management clients: " + e);
        }

        utils().getMBeanFacade().invoke(jmxObjectName, "commitProfile",
                                        EmptyArrays.EMPTY_OBJECT_ARRAY, EmptyArrays.EMPTY_STRING_ARRAY);

        return TCKTestResult.passed();
    }

    public void setUp() throws Exception {
        String duPath = utils().getTestParams().getProperty(ProfileTestConstants.PROFILE_SPEC_DU_PATH_PARAM);
        getLog().fine("Installing profile specification");
        utils().install(duPath);
        profileUtils = new ProfileUtils(utils());
    }

    public void tearDown() throws Exception {
        // delete profile tables
        try {
            if (profileUtils != null && tableCreated) {
                getLog().fine("Removing profile table");
                profileUtils.removeProfileTable(PROFILE_TABLE_NAME);
            }
        } catch (Exception e) {
            getLog().warning("Caught exception while trying to remove profile table:");
            getLog().warning(e);
        }
        super.tearDown();
    }

    private ProfileUtils profileUtils;
    private boolean tableCreated = false;
}
