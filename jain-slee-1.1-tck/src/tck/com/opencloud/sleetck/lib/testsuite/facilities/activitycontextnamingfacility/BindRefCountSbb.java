/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.facilities.activitycontextnamingfacility;

import com.opencloud.sleetck.lib.resource.events.TCKResourceEventX;
import com.opencloud.sleetck.lib.sbbutils.BaseTCKSbb;
import com.opencloud.sleetck.lib.sbbutils.TCKSbbUtils;
import com.opencloud.sleetck.lib.sbbutils.events.TCKSbbEvent;
import com.opencloud.sleetck.lib.sbbutils.events.TCKSbbEventImpl;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.slee.*;
import javax.slee.facilities.ActivityContextNamingFacility;
import javax.slee.facilities.Level;
import javax.slee.nullactivity.NullActivity;
import javax.slee.nullactivity.NullActivityContextInterfaceFactory;
import javax.slee.nullactivity.NullActivityFactory;
import java.util.HashMap;

/**
 * Test ActivityContextNamingFacility functions from spec s12.6.
 */
public abstract class BindRefCountSbb extends BaseTCKSbb {

    public void setSbbContext(SbbContext context) {
        super.setSbbContext(context);
        try {
            Context myEnv = (Context) new InitialContext().lookup("java:comp/env");

            nullActivityFactory = (NullActivityFactory) myEnv.lookup("slee/nullactivity/factory");
            nullACIFactory = (NullActivityContextInterfaceFactory) myEnv.lookup("slee/nullactivity/activitycontextinterfacefactory");
            acNamingFacility = (ActivityContextNamingFacility) myEnv.lookup("slee/facilities/activitycontextnaming");
        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }
    }

    // test starts here...
    public void onTCKResourceEventX1(TCKResourceEventX event, ActivityContextInterface aci) {
        try {
            TCKSbbUtils.createTrace(getSbbID(),Level.INFO,"Received start message",null);
            setTestName((String)event.getMessage());

            // create a null aci
            NullActivity nobody = nullActivityFactory.createNullActivity();
            ActivityContextInterface nobodyACI = nullACIFactory.getActivityContextInterface(nobody);

            // bind aci to a name but don't attach - this ensures only the facility has
            // a reference to the aci
            acNamingFacility.bind(nobodyACI, "bind-test");

            // fire an event - if the SLEE correctly increments the aci ref count, we should
            // receive this event before the aci's ActivityEndEvent
            fireCustomEvent(new TCKSbbEventImpl(new Integer(0)), nobodyACI, null);

        } catch(Exception e) {
            TCKSbbUtils.handleException(e);
        }
    }

    public void onCustomEvent(TCKSbbEvent event, ActivityContextInterface aci) {
        try {
            TCKSbbUtils.createTrace(getSbbID(),Level.INFO,"Received custom event",null);

            if (getReceivedEndEvent()) {
                // FAILED - we should have got this event before the end event
                setResultFailed(1978, "bind() did not increment aci ref count");
            } else {
                // PASSED
                setResultPassed("bind() incremented aci ref count correctly");
            }

            // detach from the null aci so it ends implicitly
            aci.detach(getSbbContext().getSbbLocalObject());

            // unbind the name
            acNamingFacility.unbind("bind-test");

            // we are done
            sendResultToTCK();

        } catch(Exception e) {
            TCKSbbUtils.handleException(e);
        }
    }

    public void onActivityEndEvent(ActivityEndEvent event, ActivityContextInterface aci) {
        try {
            TCKSbbUtils.createTrace(getSbbID(),Level.INFO,"Received activity end event",null);

            if (aci.getActivity() instanceof NullActivity) {
                setReceivedEndEvent(true);
            }

        } catch(Exception e) {
            TCKSbbUtils.handleException(e);
        }
    }

    // return a static string so that this SBB entity gets selected every time
    public InitialEventSelector getStaticConvergenceName(InitialEventSelector ies) {
        ies.setCustomName("foobar");
        return ies;
    }

    public abstract void fireCustomEvent(TCKSbbEvent event, ActivityContextInterface aci, Address address);

    // receivedEndEvent flag
    public abstract void setReceivedEndEvent(boolean received);
    public abstract boolean getReceivedEndEvent();

    public abstract void setTestName(String testName);
    public abstract String getTestName();

    private void sendResultToTCK() throws Exception {
        HashMap sbbData = new HashMap();
        sbbData.put("testname", getTestName());
        sbbData.put("result", result?"pass":"fail");
        sbbData.put("message", message);
        sbbData.put("id", new Integer(failedAssertionID));
        TCKSbbUtils.getResourceInterface().sendSbbMessage(sbbData);
    }

    private void setResultFailed(int assertionID, String msg) throws Exception {
        result = false;
        failedAssertionID = assertionID;
        message =  "Assertion " + assertionID + " failed: " + msg;
        TCKSbbUtils.createTrace(getSbbID(), Level.WARNING, message, null);
    }

    private void setResultPassed(String msg) throws Exception {
        result = true;
        message = "Success: " + msg;
        TCKSbbUtils.createTrace(getSbbID(), Level.INFO, message, null);
    }

    private NullActivityFactory nullActivityFactory;
    private NullActivityContextInterfaceFactory nullACIFactory;
    private ActivityContextNamingFacility acNamingFacility;

    private boolean result = false;
    private String message;
    private int failedAssertionID = -1;

}