/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.profiles.profilecmp;

/*
 * CMP fields may be primitive types, any serializable type
 * or arrays of these types
 */
public interface Test1110627_3ProfileCMP {

    //Arrays:
    //custom serializable type
    public MyInteger[] getMyIntegerArr();
    public void setMyIntegerArr(MyInteger[] array);

    //String
    public String[] getStringValueArr();
    public void setStringValueArr(String[] array);

    //primitive types
    public int[] getIntValueArr();
    public void setIntValueArr(int[] array);

    public char[] getCharValueArr();
    public void setCharValueArr(char[] array);

    public boolean[] getBoolValueArr();
    public void setBoolValueArr(boolean[] array);

    public double[] getDoubleValueArr();
    public void setDoubleValueArr(double[] array);

    public byte[] getByteValueArr();
    public void setByteValueArr(byte[] array);

    public short[] getShortValueArr();
    public void setShortValueArr(short[] array);

    public long[] getLongValueArr();
    public void setLongValueArr(long[] array);

    public float[] getFloatValueArr();
    public void setFloatValueArr(float[] array);


    //primitive type wrappers (are all serializable)
    public Integer[] getIntObjValueArr();
    public void setIntObjValueArr(Integer[] array);

    public Character[] getCharObjValueArr();
    public void setCharObjValueArr(Character[] array);

    public Boolean[] getBoolObjValueArr();
    public void setBoolObjValueArr(Boolean[] array);

    public Double[] getDoubleObjValueArr();
    public void setDoubleObjValueArr(Double[] array);

    public Byte[] getByteObjValueArr();
    public void setByteObjValueArr(Byte[] array);

    public Short[] getShortObjValueArr();
    public void setShortObjValueArr(Short[] array);

    public Long[] getLongObjValueArr();
    public void setLongObjValueArr(Long[] array);

    public Float[] getFloatObjValueArr();
    public void setFloatObjValueArr(Float[] array);


    //single values:
    //custom serializable type
    public MyInteger getMyInteger();
    public void setMyInteger(MyInteger value);

    //String
    public String getStringValue();
    public void setStringValue(String value);

    //primitive types
    public int getIntValue();
    public void setIntValue(int value);

    public char getCharValue();
    public void setCharValue(char value);

    public boolean getBoolValue();
    public void setBoolValue(boolean value);

    public double getDoubleValue();
    public void setDoubleValue(double value);

    public byte getByteValue();
    public void setByteValue(byte value);

    public short getShortValue();
    public void setShortValue(short value);

    public long getLongValue();
    public void setLongValue(long value);

    public float getFloatValue();
    public void setFloatValue(float value);


    //primitive type wrappers (are all serializable)
    public Integer getIntObjValue();
    public void setIntObjValue(Integer value);

    public Character getCharObjValue();
    public void setCharObjValue(Character value);

    public Boolean getBoolObjValue();
    public void setBoolObjValue(Boolean value);

    public Double getDoubleObjValue();
    public void setDoubleObjValue(Double value);

    public Byte getByteObjValue();
    public void setByteObjValue(Byte value);

    public Short getShortObjValue();
    public void setShortObjValue(Short value);

    public Long getLongObjValue();
    public void setLongObjValue(Long value);

    public Float getFloatObjValue();
    public void setFloatObjValue(Float value);

}
