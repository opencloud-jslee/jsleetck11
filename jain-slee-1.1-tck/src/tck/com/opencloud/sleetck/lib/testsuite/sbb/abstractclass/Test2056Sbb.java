/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.sbb.abstractclass;

import com.opencloud.sleetck.lib.resource.events.TCKResourceEventX;
import com.opencloud.sleetck.lib.sbbutils.BaseTCKSbb;
import com.opencloud.sleetck.lib.sbbutils.TCKSbbUtils;
import com.opencloud.sleetck.lib.TCKTestErrorException;

import javax.slee.ActivityContextInterface;
import javax.slee.CreateException;
import javax.slee.RolledBackContext;
import javax.slee.SLEEException;
import javax.slee.facilities.Level;

public abstract class Test2056Sbb extends BaseTCKSbb {

    public static final String EXCEPTION_FROM_SBB_CREATE = "Test2056:SLEEException from sbbCreate()";
    public static final String EXCEPTION_FROM_SBB_POST_CREATE = "Test2056:SLEEException from sbbPostCreate()";

    public void sbbCreate() throws CreateException {
        try {
            TCKSbbUtils.createTrace(getSbbID(),Level.INFO,"Test2056Sbb:sbbCreate()",null);
        } catch (TCKTestErrorException e) {
            TCKSbbUtils.handleException(e);
        }
        throw new SLEEException(EXCEPTION_FROM_SBB_CREATE);
    }

    public void sbbPostCreate() throws CreateException {
        try {
            TCKSbbUtils.createTrace(getSbbID(),Level.INFO,"Test2056Sbb:sbbPostCreate()",null);
        } catch (TCKTestErrorException e) {
            TCKSbbUtils.handleException(e);
        }
        throw new SLEEException(EXCEPTION_FROM_SBB_POST_CREATE);
    }

    public void sbbExceptionThrown(Exception ex, Object ev, ActivityContextInterface aci) {
        try {
            TCKSbbUtils.createTrace(getSbbID(),Level.INFO,"Test2056Sbb:sbbExceptionThrown(). Exception message:"+ex.getMessage(),null);
        } catch (TCKTestErrorException e) {
            TCKSbbUtils.handleException(e);
        }
        try {
            if(ex instanceof SLEEException) {
                String message = ex.getMessage();
                if(EXCEPTION_FROM_SBB_CREATE.equals(message) ||
                        EXCEPTION_FROM_SBB_POST_CREATE.equals(message))
                    TCKSbbUtils.getResourceInterface().sendSbbMessage(message);
                return;
            }
            // not one of the expected Exceptions
            throw ex;
        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }
    }

    public void sbbRolledBack(RolledBackContext context) {
    }

    // Initial event
    public void onTCKResourceEventX1(TCKResourceEventX ev, ActivityContextInterface aci) {
        try {
            TCKSbbUtils.createTrace(getSbbID(),Level.INFO,"Test2056Sbb:onTCKResourceEventX1",null);
        } catch (TCKTestErrorException e) {
            TCKSbbUtils.handleException(e);
        }
    }
}
