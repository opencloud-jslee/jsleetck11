/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.facilities.alarmfacility;

import java.util.HashMap;

import javax.naming.InitialContext;
import javax.slee.ActivityContextInterface;
import javax.slee.facilities.AlarmFacility;
import javax.slee.facilities.AlarmLevel;
import javax.slee.facilities.Tracer;

import com.opencloud.sleetck.lib.resource.events.TCKResourceEventX;
import com.opencloud.sleetck.lib.sbbutils.BaseTCKSbb;
import com.opencloud.sleetck.lib.sbbutils.TCKSbbUtils;

/*  
 *
 *  AssertionID(1113491): Test The raiseAlarm method throws 
 *  a java.lang.NullPointerException if any of the arguments, 
 *  except cause, are null.
 *  
 *  AssertionID(1113492): Test It throws a java.lang.IllegalArgumentException 
 *  if the specified alarm level is AlarmLevel.CLEAR.
 *  
 */

public abstract class Test1113491Sbb extends BaseTCKSbb {

    public static final String ALARM_MESSAGE = "Test1113491AlarmMessage";

    public static final String ALARM_INSTANCEID = "Test1113491AlarmInstanceID";

    public static final String ALARM_TYPE = "javax.slee.management.Alarm";

    public static final AlarmLevel ALARM_LEVEL = AlarmLevel.MAJOR;

    // Initial event
    public void onTCKResourceEventX1(TCKResourceEventX event, ActivityContextInterface aci) {

        try {
            tracer = getSbbContext().getTracer("com.test");
            tracer.info("Received " + event + " message", null);

            doTest1113491Test();
        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }
    }

    private void doTest1113491Test() throws Exception {
        tracer.info("Testing Assertion Number 1113491...");
        boolean passed = false;

        AlarmFacility facility = getAlarmFacility();
        String alarmID = null;

        //1113491:ALARM_TYPE==null;
        try {
            alarmID = facility.raiseAlarm(null, ALARM_INSTANCEID, ALARM_LEVEL, ALARM_MESSAGE);
        } catch (java.lang.NullPointerException e) {
            tracer.info("got expected NullPointerException when ALARM_TYPE is null", null);
            passed = true;
        }

        if (!passed) {
            sendResultToTCK("Test1113491Test", false, 1113491, "AlarmFacility.raiseAlarm(null,ALARM_INSTANCEID, ALARM_LEVEL, ALARM_MESSAGE) "
                    + "should have thrown java.lang.NullPointerException.");
            return;
        }

        //1113491:ALARM_INSTANCEID==null;
        passed = false;
        try {
            alarmID = facility.raiseAlarm(ALARM_TYPE, null, ALARM_LEVEL, ALARM_MESSAGE);
        } catch (java.lang.NullPointerException e) {
            tracer.info("got expected NullPointerException when ALARM_INSTANCEID is null", null);
            passed = true;
        }

        if (!passed) {
            sendResultToTCK("Test1113491Test", false, 1113491, "AlarmFacility.raiseAlarm(ALARM_TYPE, null, ALARM_LEVEL, ALARM_MESSAGE) "
                    + "should have thrown java.lang.NullPointerException.");
            return;
        }

        //1113491:ALARM_LEVEL==null;
        passed = false;
        try {
            alarmID = facility.raiseAlarm(ALARM_TYPE, ALARM_INSTANCEID, null, ALARM_MESSAGE);
        } catch (java.lang.NullPointerException e) {
            tracer.info("got expected NullPointerException when ALARM_LEVEL is null", null);
            passed = true;
        }

        if (!passed) {
            sendResultToTCK("Test1113491Test", false, 1113491, "AlarmFacility.raiseAlarm(ALARM_TYPE, ALARM_INSTANCEID, null, ALARM_MESSAGE) "
                    + "should have thrown java.lang.NullPointerException.");
            return;
        }

        //1113491:ALARM_MESSAGE==null;
        passed = false;
        try {
            alarmID = facility.raiseAlarm(ALARM_TYPE, ALARM_INSTANCEID, ALARM_LEVEL, null);
        } catch (java.lang.NullPointerException e) {
            tracer.info("got expected NullPointerException when ALARM_MESSAGE is null", null);
            passed = true;
        }

        if (!passed) {
            sendResultToTCK("Test1113491Test", false, 1113491, "AlarmFacility.raiseAlarm(ALARM_TYPE, ALARM_INSTANCEID, ALARM_LEVEL, null) "
                    + "should have thrown java.lang.NullPointerException.");
            return;
        }

        tracer.info("Testing Assertion Number 1113492...");
        
        //1113492
        passed = false;
        try {
            alarmID = facility.raiseAlarm(ALARM_TYPE, ALARM_INSTANCEID, AlarmLevel.CLEAR, ALARM_MESSAGE);
        } catch (java.lang.IllegalArgumentException e) {
            tracer.info("got expected IllegalArgumentException when alarmLevel is AlarmLevel.CLEAR", null);
            passed = true;
        }

        if (!passed) {
            sendResultToTCK("Test1113491Test", false, 1113492,
                    "AlarmFacility.raiseAlarm(ALARM_TYPE, ALARM_INSTANCEID, AlarmLevel.CLEAR, ALARM_MESSAGE) "
                            + "should have thrown java.lang.IllegalArgumentException.");
            return;
        }

        sendResultToTCK("Test1113491Test", true, 1113491,"AlarmFacility.raiseAlarm() tests passed");
    }

    private void sendResultToTCK(String testName, boolean result, int failedAssertionID, String message)
            throws Exception {
        HashMap sbbData = new HashMap();
        sbbData.put("testname", testName);
        sbbData.put("result", result ? "pass" : "fail");
        sbbData.put("message", message);
        sbbData.put("id", new Integer(failedAssertionID));
        TCKSbbUtils.getResourceInterface().sendSbbMessage(sbbData);
    }

    private AlarmFacility getAlarmFacility() throws Exception {
        AlarmFacility facility = null;
        String JNDI_ALARMFACILITY_NAME = AlarmFacility.JNDI_NAME;
        try {
            facility = (AlarmFacility) new InitialContext().lookup(JNDI_ALARMFACILITY_NAME);
        } catch (Exception e) {
            tracer.warning("got unexpected Exception: " + e, null);
        }
        return facility;
    }

    private Tracer tracer;
}
