/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.profiles.querystatic;

import com.opencloud.sleetck.lib.AbstractSleeTCKTest;
import com.opencloud.sleetck.lib.TCKTestResult;


public class Test1110417Test extends AbstractSleeTCKTest {

    private static final String STRANGE_VALID_DU_PATH= "strangeValidDUPath";
    private static final String UNMATCHING_READONLY_DU_PATH= "unmatchingReadOnlyDUPath";
    private static final String PARAM_ILLEGAL_DU_PATH= "parameterIllegalTypesDUPath";
    private static final String ELEMENT_COUNT_DU_PATH= "elementCountDUPath";
    private static final String COMPARE_TYPES_WRONG_DU_PATH= "compareTypesWrongDUPath";
    private static final String PARAM_TYPES_WRONG_DU_PATH= "paramTypesWrongDUPath";
    private static final String COMPARE_VALANDPARAM_DU_PATH= "compareValueAndParamDUPath";
    private static final String LPM_WRONG_TYPE_DU_PATH= "longestPrefixMatchWrongTypeDUPath";
    private static final String LPM_VALANDPARAM_DU_PATH= "longestPrefixMatchValueAndParamDUPath";
    private static final String HP_WRONG_TYPE_DU_PATH= "hasPrefixWrongTypeDUPath";
    private static final String HP_VALANDPARAM_DU_PATH= "hasPrefixValueAndParamDUPath";
    private static final String RM_TYPES_WRONG_DU_PATH= "rangeMatchTypesWrongDUPath";
    private static final String AND_DU_PATH= "and1CompareDUPath";
    private static final String OR_DU_PATH= "or1CompareDUPath";
    private static final String NOT_DU_PATH= "not2CompareDUPath";
    private static final String QUERY_NAME_ATTRIB= "nameAttribDUPath";

    public static final int TEST_ID = 1110417;

    /**
     * Try to deploy some profile spec's defining unusual or illegal static queries. The illegal
     * queries should cause deployment failures otherwise the test will fail.
     */
    public TCKTestResult run() throws Exception {

        //spec1: 1110417: query name must be a valid java identifier. You can't try to damage much here
        //without getting compilation errors(!?) so just try to use some unusual char as
        //first char of the query name and ensure that uppercasing etc. is working OK.
        try {
            setupService(STRANGE_VALID_DU_PATH);
            getLog().fine("Deployment succeeded as expected.");
        }
        catch (Exception e) {
            return TCKTestResult.failed(1110417, "Query method starts with \"_\". " +
                    "This is legal and deployment should have succeeded but threw exception: "+e);
        }

        //spec2: 1110423: It is a deployment error to specify false for
        //the query-option's read-only element if the value of the
        //profileread-only attribute of the enclosing profile-spec element is true.
        try {
            setupService(UNMATCHING_READONLY_DU_PATH);
            return TCKTestResult.failed(1110423, "Deployment should have failed as " +
                    "the query-option's read-only element ist 'false' while the " +
                    "profileread-only attribute of the enclosing profile-spec element is true");
        }
        catch (Exception e) {
            getLog().fine("Caught exception as expected: "+e);
        }

        //spec3: 1110429: query-parameter type may only be String and primitive type or their wrappers
        try {
            setupService(PARAM_ILLEGAL_DU_PATH);
            return TCKTestResult.failed(1110429, "Deployment should have failed as " +
                    "the query parameter types are illegal.");
        }
        catch (Exception e) {
            getLog().fine("Caught exception as expected: "+e);
        }

        //spec4: 1110430: query element contains exactly one subelement (compare, has-prefix, and, etc.)
        try {
            setupService(ELEMENT_COUNT_DU_PATH);
            return TCKTestResult.failed(1110430, "Deployment should have failed as " +
                    "query element did not contain exactly one evaluatable subelement (i.e. compare, has-prefix, and, or ...).");
        }
        catch (Exception e) {
            getLog().fine("Caught exception as expected: "+e);
        }

        //spec5: 1110432: The Java type of the identified Profile attribute must be
        //a Java primitive type or its equivalent object wrapper, or java.lang.String.
        try {
            setupService(COMPARE_TYPES_WRONG_DU_PATH);
            return TCKTestResult.failed(1110432, "Deployment should have failed as " +
                    "attribute type was not String or a Java primitive type or its object wrapper.");
        }
        catch (Exception e) {
            getLog().fine("Caught exception as expected: "+e);
        }

        //spec6: 1110435: the identified query parameter must be the same type as
        //the identified Profile attribute that will be compared.
        try {
            setupService(PARAM_TYPES_WRONG_DU_PATH);
            return TCKTestResult.failed(1110435, "Deployment should have failed as " +
                    "attribute type and parameter type don't match.");
        }
        catch (Exception e) {
            getLog().fine("Caught exception as expected: "+e);
        }

        //spec7: 1110437: Either the value attribute or the parameter attribute,
        //but not both, must be specified for this element.
        try {
            setupService(COMPARE_VALANDPARAM_DU_PATH);
            return TCKTestResult.failed(1110437, "Deployment should have failed as " +
                    "the test queries either don't have a value nor a parameter defined or define both elements in a single compare element.");
        }
        catch (Exception e) {
            getLog().fine("Caught exception as expected: "+e);
        }

        //spec8: 1110439: longest-prefix-match queries may only be defined for attributes of type String
        try {
            setupService(LPM_WRONG_TYPE_DU_PATH);
            return TCKTestResult.failed(1110439, "Deployment should have failed as " +
                    "longest-prefix-match query was defined for a type different to java.lang.String.");
        }
        catch (Exception e) {
            getLog().fine("Caught exception as expected: "+e);
        }

        //spec9: 1110443: Either the value attribute or the parameter attribute,
        //but not both, must be specified for this element.
        try {
            setupService(LPM_VALANDPARAM_DU_PATH);
            return TCKTestResult.failed(1110443, "Deployment should have failed as " +
                "the test queries either don't have a value nor a parameter defined or define both elements in a single longest-prefix-match element.");
        }
        catch (Exception e) {
            getLog().fine("Caught exception as expected: "+e);
        }

        //spec10: 1110445: has-prefix queries may only be defined for attributes of type String
        try {
            setupService(HP_WRONG_TYPE_DU_PATH);
            return TCKTestResult.failed(1110445, "Deployment should have failed as " +
            "has-prefix query was defined for a type different to java.lang.String.");
        }
        catch (Exception e) {
            getLog().fine("Caught exception as expected: "+e);
        }

        //spec11: 1110449: Either the value attribute or the parameter attribute,
        //but not both, must be specified for this element.
        try {
            setupService(HP_VALANDPARAM_DU_PATH);
            return TCKTestResult.failed(1110449, "Deployment should have failed as " +
                "the test queries either don't have a value nor a parameter defined or define both elements in a single has-prefix element.");
        }
        catch (Exception e) {
            getLog().fine("Caught exception as expected: "+e);
        }

        //spec12: 1110451: The Java type of the identified Profile attribute must be
        //a Java primitive type or its equivalent object wrapper, or java.lang.String.
        try {
            setupService(RM_TYPES_WRONG_DU_PATH);
            return TCKTestResult.failed(1110451, "Deployment should have failed as " +
                "attribute type was not String or a Java primitive type or its object wrapper.");
        }
        catch (Exception e) {
            getLog().fine("Caught exception as expected: "+e);
        }

        //spec13: 1110459: And must have two or more subelements
        try {
            setupService(AND_DU_PATH);
            return TCKTestResult.failed(1110459, "Deployment should have failed as " +
                "and element must have at least two evaluatable subelements.");
        }
        catch (Exception e) {
            getLog().fine("Caught exception as expected: "+e);
        }

        //spec14: 1110461: Or must have two or more subelements
        try {
            setupService(OR_DU_PATH);
            return TCKTestResult.failed(1110461, "Deployment should have failed as " +
                "or element must have at least two evaluatable subelements.");
        }
        catch (Exception e) {
            getLog().fine("Caught exception as expected: "+e);
        }

        //spec15: 1110463: Not must have exactly one subelement
        try {
            setupService(NOT_DU_PATH);
            return TCKTestResult.failed(1110463, "Deployment should have failed as " +
                "not element must have exactly one evaluatable subelement.");
        }
        catch (Exception e) {
            getLog().fine("Caught exception as expected: "+e);
        }

        //spec16: 1110415: query element has to contain a 'name' attribute
        try {
            setupService(QUERY_NAME_ATTRIB);
            return TCKTestResult.failed(1110415, "Deployment should have failed as " +
                "query element must have a 'name' attribute.");
        }
        catch (Exception e) {
            getLog().fine("Caught exception as expected: "+e);
        }

        return TCKTestResult.passed();
    }

    public void setUp() throws Exception {
        //overrides std behaviour of superclass
    }
}
