/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.activities.nullactivity;

import com.opencloud.sleetck.lib.AbstractSleeTCKTest;
import com.opencloud.sleetck.lib.TCKTestResult;
import com.opencloud.sleetck.lib.resource.TCKActivityID;
import com.opencloud.sleetck.lib.resource.events.TCKResourceEventX;
import com.opencloud.sleetck.lib.testutils.BaseTCKResourceListener;
import com.opencloud.sleetck.lib.testutils.FutureResult;
import com.opencloud.util.Future;

import java.rmi.RemoteException;

/**
 * Test asserttion 4464: When a transaction that creates a NullActivity is rolled back the NullActivity is not
 * created so no ActivityEndEvent should be delivered for it.
 */
public class Test4464Test extends AbstractSleeTCKTest {

    public TCKTestResult run() throws Exception {
        testResult = new FutureResult(getLog());
        activityA = utils().getResourceInterface().createActivity("Test4464ActivityA");

        setResourceListener(new ResourceListenerImpl());

        // Fire an X1 event.  The X1 handler will create a NullActivity and then rollback
        utils().getResourceInterface().fireEvent(TCKResourceEventX.X1, null, activityA, null);

        // Wait and see if the SBB tells us it received an ActivityEndEvent for a NullActivity
        try {
            return testResult.waitForResult(utils().getTestTimeout());
        } catch (Future.TimeoutException e) {
            // if the ActivityEndEvent was delivered, the result would have been set to a fail, i.e. the test passed
            getLog().info("Timed out waiting for the ActivityEndEvent which should not be delivered. " +
                    "This indicates a pass result.");
            return TCKTestResult.passed();
        }
    }

    /**
     * This resource listener will receive a callback from the SBB if an ActivityEndEvent is received for a NullActivity
     */
    private class ResourceListenerImpl extends BaseTCKResourceListener {
        public synchronized Object onSbbCall(Object argument) throws Exception {
            getLog().info("Callback: " + argument);
            testResult.setFailed(4464, "An ActivityEndEvent was delivered for a NullActivity that should not have "+
                    "been created (was during a transaction that was rolled back)");
            return null;
        }

        public synchronized void onException(Exception exception) throws RemoteException {
            testResult.setError("An Exception was received from the SBB or the TCK resource",exception);
        }
    }

    private TCKActivityID activityA;
    private FutureResult testResult;

}
