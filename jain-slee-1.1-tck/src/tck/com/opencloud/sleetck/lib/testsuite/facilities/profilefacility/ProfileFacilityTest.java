/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.facilities.profilefacility;

import com.opencloud.sleetck.lib.AbstractSleeTCKTest;
import com.opencloud.sleetck.lib.DescriptionKeys;
import com.opencloud.sleetck.lib.TCKTestFailureException;
import com.opencloud.sleetck.lib.infra.SleeTCKComponentConstants;
import com.opencloud.sleetck.lib.resource.TCKActivityID;
import com.opencloud.sleetck.lib.resource.TCKSbbMessage;
import com.opencloud.sleetck.lib.resource.events.TCKResourceEventX;
import com.opencloud.sleetck.lib.resource.testapi.TCKResourceListener;
import com.opencloud.sleetck.lib.resource.testapi.TCKResourceTestInterface;
import com.opencloud.sleetck.lib.testutils.*;
import com.opencloud.sleetck.lib.testutils.jmx.ProfileMBeanProxy;
import com.opencloud.sleetck.lib.testutils.jmx.ProfileProvisioningMBeanProxy;

import javax.slee.profile.ProfileSpecificationID;
import java.rmi.RemoteException;
import java.util.Map;

/**
 * Tests methods of the ProfileFacility interface.<p>
 * Use the <code>testName</code> parameter in test description files to
 * test different TimerFacility features.<p>
 */
public class ProfileFacilityTest extends AbstractSleeTCKTest {

    private static final String PROFILE_TABLE_NAME = "ProfileFacilityTestTable";
    private static final String PROFILE_SPEC_NAME = "ProfileFacilityTestProfile";
    private static final String PROFILE_NAME = "testProfile";
    private static final String PROFILE_SPEC_DU_PATH_PARAM = "profileDUPath";

    // +4448 - 4456
    private static final String PROFILE_TABLE_NAME_2 = "ProfileFacilityTestTable2";
    private static final String PROFILE_NAME_2 = "testProfile2";
    private static final String PROFILE_TABLE_NAME_3 = "ProfileFacilityTestTable3";
    /////

    public void run(FutureResult result) throws Exception {
        this.result = result;
        String activityName = "ProfileFacilityTest";
        TCKResourceTestInterface resource = utils().getResourceInterface();
        TCKActivityID activityID = resource.createActivity(activityName);
        getLog().info("Firing event: " + testName );

        // kickoff the test
        resource.fireEvent(TCKResourceEventX.X1, testName, activityID, null);
    }

    public void setUp() throws Exception {
        getLog().fine("Connecting to resource");
        resourceListener = new TCKResourceListenerImpl();
        setResourceListener(resourceListener);
        this.testName = utils().getTestParams().getProperty("testName");
        profileUtils = new ProfileUtils(utils());

        // install our profile specification DU
        String profileDUPath = utils().getTestParams().getProperty(PROFILE_SPEC_DU_PATH_PARAM);
        if (profileDUPath != null) {
            getLog().info("installing profile specification");
            utils().install(profileDUPath);

            profileProvisioningMBean = profileUtils.getProfileProvisioningProxy();

            ProfileSpecificationID profileSpecID = new ComponentIDLookup(utils()).lookupProfileSpecificationID(PROFILE_SPEC_NAME, SleeTCKComponentConstants.TCK_VENDOR,"1.0");

            getLog().info("creating profile table " + PROFILE_TABLE_NAME);
            profileProvisioningMBean.createProfileTable(profileSpecID, PROFILE_TABLE_NAME);
            getLog().info("creating profile " + PROFILE_TABLE_NAME + "/" + PROFILE_NAME);
            javax.management.ObjectName profile = profileProvisioningMBean.createProfile(PROFILE_TABLE_NAME, PROFILE_NAME);
        ProfileMBeanProxy profProxy = utils().getMBeanProxyFactory().createProfileMBeanProxy(profile);
        profProxy.commitProfile();
        profProxy.closeProfile();

            // +4448-4456
            // Create a second profile table with multiple profiles
            getLog().info("creating profile table " + PROFILE_TABLE_NAME_2);
            profileProvisioningMBean.createProfileTable(profileSpecID, PROFILE_TABLE_NAME_2);

            getLog().info("creating profile " + PROFILE_TABLE_NAME_2 + "/" + PROFILE_NAME);
            profile = profileProvisioningMBean.createProfile(PROFILE_TABLE_NAME_2, PROFILE_NAME);
        profProxy = utils().getMBeanProxyFactory().createProfileMBeanProxy(profile);
        profProxy.commitProfile();
        profProxy.closeProfile();

            getLog().info("creating profile " + PROFILE_TABLE_NAME_2 + "/" + PROFILE_NAME_2);
            profile = profileProvisioningMBean.createProfile(PROFILE_TABLE_NAME_2, PROFILE_NAME_2);
        profProxy = utils().getMBeanProxyFactory().createProfileMBeanProxy(profile);
        profProxy.commitProfile();
        profProxy.closeProfile();

            // Create a third profile table, with no profiles
            getLog().info("creating profile table " + PROFILE_TABLE_NAME_3);
            profileProvisioningMBean.createProfileTable(profileSpecID, PROFILE_TABLE_NAME_3);
            /////
        }

        // this wil install the service DU
        setupService(DescriptionKeys.SERVICE_DU_PATH_PARAM, true);

    }

    public void tearDown() throws Exception {

        // uninstall profile & table
        String profileDUPath = utils().getTestParams().getProperty(PROFILE_SPEC_DU_PATH_PARAM);
        if (profileDUPath != null && profileProvisioningMBean != null) {
            getLog().info("removing profile " + PROFILE_TABLE_NAME + "/" + PROFILE_NAME);
            profileProvisioningMBean.removeProfile(PROFILE_TABLE_NAME, PROFILE_NAME);
            getLog().info("removing profile table " + PROFILE_TABLE_NAME);
            profileProvisioningMBean.removeProfileTable(PROFILE_TABLE_NAME);

            // +4448-4456
            // Remove second profile table
            getLog().info("removing profile " + PROFILE_TABLE_NAME_2 + "/" + PROFILE_NAME_2);
            profileProvisioningMBean.removeProfile(PROFILE_TABLE_NAME_2, PROFILE_NAME_2);
            getLog().info("removing profile " + PROFILE_TABLE_NAME_2 + "/" + PROFILE_NAME);
            profileProvisioningMBean.removeProfile(PROFILE_TABLE_NAME_2, PROFILE_NAME);
            getLog().info("removing profile table " + PROFILE_TABLE_NAME_2);
            profileProvisioningMBean.removeProfileTable(PROFILE_TABLE_NAME_2);

            // Remove third profile table
            getLog().info("removing profile table " + PROFILE_TABLE_NAME_3);
            profileProvisioningMBean.removeProfileTable(PROFILE_TABLE_NAME_3);
            /////
        }
        // cleanup
        super.tearDown();
    }

    // Private classes

    private class TCKResourceListenerImpl extends BaseTCKResourceListener {

        // TCKResourceListener methods

        public synchronized void onSbbMessage(TCKSbbMessage message, TCKActivityID calledActivity) throws RemoteException {
            Map sbbData = (Map)message.getMessage();
            String sbbTestName = (String)sbbData.get("testname");
            String sbbTestResult = (String)sbbData.get("result");
            String sbbTestMessage = (String)sbbData.get("message");
            int assertionID = ((Integer)sbbData.get("id")).intValue();
            getLog().info("Received message from SBB: testname="+ sbbTestName+", result="+sbbTestResult+", message="+sbbTestMessage+", id="+assertionID);
            try {
                if(sbbTestName.equals(testName)) {

                    Assert.assertEquals(assertionID,"Test " + testName + " failed: " + sbbTestMessage, "pass", sbbTestResult);
                    result.setPassed();

                } else result.setError("Invalid response sent by SBB: "+sbbTestName);
            } catch (TCKTestFailureException ex) {
                result.setFailed(ex);
            }
        }

        public void onException(Exception exception) throws RemoteException {
            getLog().warning("Received Exception from SBB or resource:");
            getLog().warning(exception);
            result.setError(exception);
        }

    }

    // Private state

    private TCKResourceListener resourceListener;
    private FutureResult result;
    private String testName;
    private ProfileUtils profileUtils;

    private ProfileProvisioningMBeanProxy profileProvisioningMBean;

}
