/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.transactions.isolation;

import com.opencloud.sleetck.lib.resource.events.TCKResourceEventX;
import com.opencloud.sleetck.lib.resource.events.TCKResourceEventY;
import com.opencloud.sleetck.lib.sbbutils.TCKSbbUtils;

import javax.slee.ActivityContextInterface;
import javax.slee.facilities.Level;

/**
 * SBB to test transaction isolation for updates to activity context interface fields
 */
public abstract class Test2003Sbb1 extends Test2003BaseSbb {

    /**
     * Creates a NullActivity and binds its activity context to the name used by the test,
     * then sets an initial value for the ACI value (TCKResourceEventX1's event type name)
     */
    public void onTCKResourceEventX1(TCKResourceEventX event, ActivityContextInterface aci) {
        try {
            createTraceSafe(Level.INFO,"Test2003Sbb1.onTCKResourceEventX1(): Transaction ID: "+getTransactionId());
            createTraceSafe(Level.INFO,"Test2003Sbb1.onTCKResourceEventX1(): Creating null activity");
            createSharedActivityContext();
            String initialValue = TCKResourceEventX.X1;
            Test2003ActivityContextInterface myaci = null;
            myaci = getACI();
            createTraceSafe(Level.INFO,"Test2003Sbb1.onTCKResourceEventX1(): got ActivityContextInterface: " + myaci);
            createTraceSafe(Level.INFO,"Test2003Sbb1.onTCKResourceEventX1(): start value: " + myaci.getValue());
            createTraceSafe(Level.INFO,"Test2003Sbb1.onTCKResourceEventX1(): setting value to "+initialValue);
            myaci.setValue(initialValue);
            createTraceSafe(Level.INFO,"Test2003Sbb1.onTCKResourceEventX1(): initialized value to: " + myaci.getValue());
            sendResponse(TCKResourceEventX.X1, null);
        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }
    }

    /**
     * Access the ACI value, then calls the test to block (passing the ACI value), then reads the value again,
     * and passes the read value (or the Exception caught while trying to read) back to the test.
     */
    public void onTCKResourceEventX2(TCKResourceEventX event, ActivityContextInterface aci) {
        try {
            createTraceSafe(Level.INFO,"Test2003Sbb1.onTCKResourceEventX2(): Transaction ID: "+getTransactionId());
            String aciValue = getACI().getValue();
            createTraceSafe(Level.INFO,"Test2003Sbb1.onTCKResourceEventX2(): read ACI value: "+aciValue);
            createTraceSafe(Level.INFO,"Test2003Sbb1.onTCKResourceEventX2(): calling test to block");
            sendResponse(TCKResourceEventX.X2, aciValue);
            createTraceSafe(Level.INFO,"Test2003Sbb1.onTCKResourceEventX2(): finished blocking. accessing ACI state...");
            aciValue = null;
            Object response = null;
            try {
                response = getACI().getValue();
                createTraceSafe(Level.INFO,"Test2003Sbb1.onTCKResourceEventX2(): ACI value: "+aciValue);
            } catch (Exception aciEx) {
                response = aciEx;
                createTraceSafe(Level.INFO,"Test2003Sbb1.onTCKResourceEventX2(): Exception received while trying to "+
                        "access ACI value: "+aciEx);
            }
            sendResponse(TCKResourceEventX.X2, response);
        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }
    }

    /**
     * Sets the ACI value (to TCKResourceEventX3's event type name), then calls the test to block
     */
    public void onTCKResourceEventX3(TCKResourceEventX event, ActivityContextInterface aci) {
        try {
            String newValue = TCKResourceEventX.X3;
            createTraceSafe(Level.INFO,"Test2003Sbb1.onTCKResourceEventX3(): Transaction ID: "+getTransactionId());
            createTraceSafe(Level.INFO,"Test2003Sbb1.onTCKResourceEventX3(): changing ACI value to: "+newValue);
            Test2003ActivityContextInterface myaci = null;
            myaci = getACI();
            myaci.setValue(newValue);
            createTraceSafe(Level.INFO,"Test2003Sbb1.onTCKResourceEventX3(): changed value to: " + newValue+". calling test to block");
            sendResponse(TCKResourceEventX.X3, null);
            createTraceSafe(Level.INFO,"Test2003Sbb1.onTCKResourceEventX3(): finished blocking");
        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }
    }

    /**
     * Performs clean up: unbinds the activity  
     */
    public void onTCKResourceEventY3(TCKResourceEventY event, ActivityContextInterface aci) {
        try {
            createTraceSafe(Level.INFO,"Test2003Sbb1.onTCKResourceEventY3(): Transaction ID: "+getTransactionId());
            createTraceSafe(Level.INFO,"Test2003Sbb1.onTCKResourceEventY3(): removing null activity binding");
            removeSharedActivityContext();
        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
            return;
        }
    }

}
