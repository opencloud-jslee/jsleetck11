/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.profiles.profilecmp;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.HashMap;

import javax.slee.TransactionRequiredLocalException;
import javax.slee.profile.ProfileTable;
import javax.slee.transaction.SleeTransaction;
import javax.slee.transaction.SleeTransactionManager;

import com.opencloud.logging.StdErrLog;
import com.opencloud.sleetck.lib.profileutils.BaseMessageSender;
import com.opencloud.sleetck.lib.rautils.MessageHandler;
import com.opencloud.sleetck.lib.rautils.RMIObjectChannel;
import com.opencloud.sleetck.lib.rautils.TCKRAUtils;

/**
 * Provides callback method for handling messages sent from the TCK test to the RA.
 * The actual test code is located in the 'handleMessage()' method.
 */
public class Test1110633MessageListener extends UnicastRemoteObject implements MessageHandler {

    public static final String PROFILE_TABLE_NAME= "Test1110633ProfileTable";
    public static final String PROFILE_NAME= "Test1110633Profile";

    private Test1110633ResourceAdaptor ra;

    public Test1110633MessageListener(Test1110633ResourceAdaptor ra) throws RemoteException {
        super();

        try {
            out = TCKRAUtils.lookupRMIObjectChannel();
            msgSender = new BaseMessageSender(out, new StdErrLog());
        }
        catch (Exception e) {
            ra.getLog().warning("Exception occurred when trying to acquire RMIObjectChannel: ",e);
        }

        this.ra = ra;
    }

    public boolean handleMessage(Object message) throws RemoteException {

        //run the tests, if both run through successfully and completely (=both return 'true')
        //send success msg back to TCK test
        if (test_GetSetCopy() && test_GetSetMandatoryTXN())
            msgSender.sendSuccess(1110633, "Test successful.");

        return true;
    }

    private boolean test_GetSetMandatoryTXN() {
        SleeTransactionManager txnManager = ra.getResourceAdaptorContext().getSleeTransactionManager();
        SleeTransaction txn=null;

        try {
            ProfileTable profileTable = ra.getResourceAdaptorContext().getProfileTable(PROFILE_TABLE_NAME);
            //start a new TXN as profileTable.find is mandatory transactional
            txn = txnManager.beginSleeTransaction();

            Test1110633ProfileCMP profileCMP = (Test1110633ProfileCMP)profileTable.find(PROFILE_NAME);

            txn.commit();

            //1110633: The CMP field accessor methods are mandatory transactional methods
            //They throw a javax.slee.TransactionRequiredLocalException if
            //they are invoked without a valid transaction context.
            try {
                profileCMP.setMap(new HashMap());
                msgSender.sendFailure(1110633, "CMP field set accessor method should be mandatory transactional.");
                return false;
            } catch (TransactionRequiredLocalException e) {
                msgSender.sendLogMsg("CMP field set accessor threw exception as expected if called without a valid TXN context: "+e);
            }

            try {
                profileCMP.getMap();
                msgSender.sendFailure(1110633, "CMP field get accessor method should be mandatory transactional.");
                return false;
            } catch (TransactionRequiredLocalException e) {
                msgSender.sendLogMsg("CMP field get accessor threw exception as expected if called without a valid TXN context: "+e);
            }

            txn = null;

        } catch (Exception e) {
            msgSender.sendException(e);
            return false;
        } finally {
            try {
                if (txn!=null)
                    txn.rollback();
            } catch (Exception ex) {
                ra.getLog().warning("Error occured when trying to rollback RA started TXN.", ex);
                msgSender.sendLogMsg("Exception occurred when trying to safely rollback RA started TXN: "+ex.getMessage());
            }
        }

        return true;
    }

    private boolean test_GetSetCopy() {
        SleeTransactionManager txnManager = ra.getResourceAdaptorContext().getSleeTransactionManager();
        SleeTransaction txn=null;

        try {
            ProfileTable profileTable = ra.getResourceAdaptorContext().getProfileTable(PROFILE_TABLE_NAME);
            //start a new TXN as profileTable.find is mandatory transactional
            txn = txnManager.beginSleeTransaction();

            Test1110633ProfileCMP profileCMP = (Test1110633ProfileCMP)profileTable.find(PROFILE_NAME);

            //1110631: The assignment of a dependent value class value to a CMP field using the set accessor
            //method causes the value to be copied to the target CMP field.
            HashMap oriMap = new HashMap();
            profileCMP.setMap(oriMap);

            txn.commit();
            txn = txnManager.beginSleeTransaction();

            oriMap.put("Key", "Value");
            
            profileCMP = (Test1110633ProfileCMP)profileTable.find(PROFILE_NAME);
            HashMap checkMap = profileCMP.getMap();

            if (checkMap==oriMap || checkMap.containsKey("Key")) {
                msgSender.sendFailure(1110631, "CMP field set accessor method did not store a COPY of the provided object but rather a reference.");
                return false;
            }
            else
                msgSender.sendLogMsg("CMP field set accessor correctly stored a copy of the provided object as the CMP field's value.");

            txn.commit();
            txn = txnManager.beginSleeTransaction();

            //1110630: The get accessor method for a CMP field that corresponds to a dependent value class
            //returns a copy of the dependent value class instance.
            profileCMP = (Test1110633ProfileCMP)profileTable.find(PROFILE_NAME);
            oriMap = profileCMP.getMap();

            txn.commit();
            txn = txnManager.beginSleeTransaction();

            oriMap.put("Key2", "Value2");
            
            profileCMP = (Test1110633ProfileCMP)profileTable.find(PROFILE_NAME);
            checkMap = profileCMP.getMap();
            if (oriMap == checkMap || checkMap.containsKey("Key2")) {
                msgSender.sendFailure(1110630, "CMP field get accessor method did not return a COPY of the stored CMP object but rather a reference.");
                return false;
            }
            else
                msgSender.sendLogMsg("CMP field get accessor correctly returned a copy of the stored object as the CMP field's value.");


            txn.commit();
            txn = null;

        } catch (Exception e) {
            msgSender.sendException(e);
            return false;
        } finally {
            try {
                if (txn!=null)
                    txn.rollback();
            } catch (Exception ex) {
                ra.getLog().warning("Error occured when trying to rollback RA started TXN.", ex);
                msgSender.sendLogMsg("Exception occurred when trying to safely rollback RA started TXN: "+ex.getMessage());
            }
        }

        return true;
    }

    private BaseMessageSender msgSender;
    private RMIObjectChannel out;

}

