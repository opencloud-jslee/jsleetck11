/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.usage.types;

import com.opencloud.sleetck.lib.TCKTestFailureException;
import com.opencloud.sleetck.lib.TCKTestResult;
import com.opencloud.sleetck.lib.resource.TCKActivityID;
import com.opencloud.sleetck.lib.resource.events.TCKResourceEventX;
import com.opencloud.sleetck.lib.testsuite.usage.common.GenericUsageMBeanProxy;
import com.opencloud.sleetck.lib.testsuite.usage.common.GenericUsageSbbInstructions;
import com.opencloud.sleetck.lib.testsuite.usage.common.GenericUsageTest;
import com.opencloud.sleetck.lib.testutils.QueuingNotificationListener;
import com.opencloud.sleetck.lib.testutils.QueuingResourceListener;

import javax.management.Notification;

/**
 * Checks the return value of a counter type usage parameter accessor with various
 * increments and decrements.
 */
public class Test2154Test extends GenericUsageTest {

    public TCKTestResult run() throws Exception {
        activityID = utils().getResourceInterface().createActivity("Test2154Test-Activity");
        expectedCounterValue = 0;
        /*
          Cases covered:
              increments/decrements: Long.MIN_VALUE,Long.MIN_VALUE+1,-10,-1,0,1,10,Long.MAX_VALUE-1,Long.MAX_VALUE
              counter changes:
                  up from 0;down from 0;cross 0 upwards;cross 0 downwards;
                  upwards from positive;downwards from positive;
                  upwards from negative;downwards from negative
        */
        testIncrements(new long[]{Long.MIN_VALUE});
        testIncrements(new long[]{Long.MIN_VALUE+1,Long.MAX_VALUE-1});
        testIncrements(new long[]{Long.MAX_VALUE});
        testIncrements(new long[]{1,10,-10,0,-1});
        testIncrements(new long[]{4});
        testIncrements(new long[]{-4});
        testIncrements(new long[]{-1,3});
        testIncrements(new long[]{1,-3});
        testIncrements(new long[]{10,5});
        testIncrements(new long[]{10,-5});
        testIncrements(new long[]{-10,5});
        testIncrements(new long[]{-10,-5});
        getLog().info("All checks passed");
        return TCKTestResult.passed();
    }

    private void testIncrements(long[] increments) throws Exception {
        resetCounter();
        auditCounterValue();
        for (int i = 0; i < increments.length; i++) {
            long increment = increments[i];
            doUpdateAndWait(increment);
            auditCounterValue();
        }
    }

    private void resetCounter() throws Exception {
        sbbUsageMBeanProxy.getFirstCount(true);
        expectedCounterValue = 0;
    }

    private void doUpdateAndWait(long increment) throws Exception {
        // do the update
        getLog().fine("Sending update instructions to the SBB via an event. Increment value: "+increment);
        GenericUsageSbbInstructions instructions = new GenericUsageSbbInstructions(null);
        instructions.addFirstCountIncrement(increment);
        utils().getResourceInterface().fireEvent(TCKResourceEventX.X1,instructions.toExported(),activityID, null);
        // wait for indication of completion
        getLog().fine("Waiting for a reply from the SBB");
        resourceListener.nextMessage();
        getLog().fine("Received reply from SBB. Waiting for notification of update");
        Notification usageNotification = notificationListener.nextNotification();
        getLog().fine("Received notification of update: "+usageNotification);
        // update our expected counter value
        expectedCounterValue += increment;
    }

    private void auditCounterValue() throws Exception {
        long actualCounterValue = sbbUsageMBeanProxy.getFirstCount(false); // don't reset
        getLog().info("Auditing counter value. Expected value:"+expectedCounterValue);
        if(actualCounterValue != expectedCounterValue) {
            throw new TCKTestFailureException(2154,"SbbUsageMBean.getFirstCount(false) returned an invalid counter value. "+
                    "Expected="+expectedCounterValue+"; counter value returned="+actualCounterValue);
        }
    }

    public void setUp() throws Exception {
        super.setUp();
        setResourceListener(resourceListener = new QueuingResourceListener(utils()));
        sbbUsageMBeanProxy = getGenericUsageMBeanLookup().getUnnamedGenericUsageMBeanProxy();
        notificationListener = new QueuingNotificationListener(utils());
        sbbUsageMBeanProxy.addNotificationListener(notificationListener, null, null);
    }

    public void tearDown() throws Exception {
        if(notificationListener != null) {
            try {
                sbbUsageMBeanProxy.removeNotificationListener(notificationListener);
            } catch (Exception e) { getLog().warning(e); }
        }
        sbbUsageMBeanProxy = null;
        super.tearDown();
    }

    private TCKActivityID activityID;
    private QueuingResourceListener resourceListener;
    private QueuingNotificationListener notificationListener;
    private GenericUsageMBeanProxy sbbUsageMBeanProxy;

    private long expectedCounterValue;

}
