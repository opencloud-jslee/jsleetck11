/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.events.eventcontext;

import java.util.HashMap;

import javax.slee.ActivityContextInterface;
import javax.slee.EventContext;
import javax.slee.InitialEventSelector;
import javax.slee.facilities.Tracer;

import com.opencloud.sleetck.lib.resource.TCKActivityID;
import com.opencloud.sleetck.lib.resource.events.TCKResourceEventX;
import com.opencloud.sleetck.lib.resource.sbbapi.TCKActivity;
import com.opencloud.sleetck.lib.sbbutils.BaseTCKSbb;
import com.opencloud.sleetck.lib.sbbutils.TCKSbbUtils;

/**
 * AssertionID(1108193): From the perspective of event delivery, 
 * if the delivery is suspended, the event is not delivered to a 
 * subsequent SBB entity, i.e. the event is considered to be “being 
 * delivered” to the SBB entity that suspended delivery.
 * 
 * AssertionID(1108194): Test Once the event delivery is resumed 
 * it may be delivered to other SBB entities, i.e. once resumed 
 * the event is considered to have been processed by the SBB entity 
 * that suspended delivery. 
 */

public abstract class Test1108009Sbb1 extends BaseTCKSbb {
    public static final String TRACE_MESSAGE_TCKResourceEventX1 = "Test1108009Sbb1:I got TCKResourceEventX1 on ActivityA";

    public static final String TRACE_MESSAGE_TCKResourceEventX2 = "Test1108009Sbb1:I got TCKResourceEventX2 on ActivityB";

    public static final String TRACE_MESSAGE_TCKResourceEventX3A = "Test1108009Sbb1:I got TCKResourceEventX3 on ActivityA";

    public static final String TRACE_MESSAGE_TCKResourceEventX3B = "Test1108009Sbb1:I got TCKResourceEventX3 on ActivityB";

    public InitialEventSelector initialEventSelector(InitialEventSelector ies) {
        ies.setCustomName("test");
        return ies;
    }

    public void onTCKResourceEventX1(TCKResourceEventX event, ActivityContextInterface aci, EventContext context) {
        try {
            //send message to TCK: I got TCKResourceEventX1 on ActivityA
            tracer = getSbbContext().getTracer("com.test");
            tracer.info(TRACE_MESSAGE_TCKResourceEventX1);
            //store context in CMP field
            setEventContext(context);
            //call suspendDelivery() method now, we assumed the default timeout 
            //on the SLEE will be more than 10000ms to complete this test.
            context.suspendDelivery();
            sendResultToTCK("Test1108009Test", true, "Sbb1 received TCKResourceEventX1 on ActivityA", 1108009);
        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }
    }

    public void onTCKResourceEventX2(TCKResourceEventX event, ActivityContextInterface aci, EventContext context) {
        try {
            //send message to TCK: I got TCKResourceEventX2 on ActivityB
            tracer = getSbbContext().getTracer("com.test");
            tracer.info(TRACE_MESSAGE_TCKResourceEventX2);
            //get EventContext ec from CMP field
            EventContext ec = getEventContext();
            //assert ec.isSuspended(), check ec has been suspended or not 
            if (!ec.isSuspended()) {
                sendResultToTCK("Test1108009Test", false, "SBB1:onTCKResourceEventX2-ERROR: The event delivery hasn't been " +
                                "suspended, the EventContext.isSuspended() returned false!", 1108009);
                return;
            }
            sendResultToTCK("Test1108009Test", true, "Sbb1 received TCKResourceEventX2 on ActivityB", 1108009);

        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }
    }

    public void onTCKResourceEventX3(TCKResourceEventX event, ActivityContextInterface aci, EventContext context) {
        try {
            TCKActivity activity = (TCKActivity) context.getActivityContextInterface().getActivity();
            TCKActivityID activityID = activity.getID();

            if (activityID.getName().equals(Test1108009Test.activityNameB)) {
                //send message to TCK: I got TCKResourceEventX3 on ActivityB
                tracer = getSbbContext().getTracer("com.test");
                tracer.info(TRACE_MESSAGE_TCKResourceEventX3B);
                //get EventContext ec from CMP field
                EventContext ec = getEventContext();
                //assert ec.isSuspended(), check ec has been suspended or not 
                if (!ec.isSuspended()) {
                    sendResultToTCK("Test1108009Test", false, "SBB1:onTCKResourceEventX3-ERROR: The event delivery hasn't been " +
                            "suspended, the EventContext.isSuspended() returned false!", 1108009);
                    return;
                }
                //call resumeDelivery method now
                ec.resumeDelivery();
                sendResultToTCK("Test1108009Test", true, "Sbb1 received TCKResourceEventX3 on ActivityB", 1108009);
            } else if (activityID.getName().equals(Test1108009Test.activityNameA)) {
                //send message to TCK: I got TCKResourceEventX3 on ActivityA
                tracer = getSbbContext().getTracer("com.test");
                tracer.info(TRACE_MESSAGE_TCKResourceEventX3A);
                sendResultToTCK("Test1108009Test", true, "Sbb1 received TCKResourceEventX3 on ActivityA", 1108009);
            }

        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }
    }

    private void sendResultToTCK(String testName, boolean result, String message, int failedAssertionID) throws Exception {
        HashMap sbbData = new HashMap();
        sbbData.put("testname", testName);
        sbbData.put("result", result ? "pass" : "fail");
        sbbData.put("message", message);
        sbbData.put("id", new Integer(failedAssertionID));
        TCKSbbUtils.getResourceInterface().sendSbbMessage(sbbData);
    }

    //cmp fields
    public abstract void setEventContext(EventContext eventContext);
    public abstract EventContext getEventContext();
    
    private Tracer tracer = null;

}
