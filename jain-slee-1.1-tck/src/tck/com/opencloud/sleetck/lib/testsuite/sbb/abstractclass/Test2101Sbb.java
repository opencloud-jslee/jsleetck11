/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.sbb.abstractclass;

import com.opencloud.sleetck.lib.resource.events.TCKResourceEventX;
import com.opencloud.sleetck.lib.sbbutils.BaseTCKSbb;
import com.opencloud.sleetck.lib.sbbutils.TCKSbbUtils;

import javax.slee.ActivityContextInterface;
import javax.slee.ChildRelation;
import javax.slee.SbbLocalObject;
import java.util.Collection;
import java.util.HashMap;
import java.util.Vector;

public abstract class Test2101Sbb extends BaseTCKSbb {

    // Initial event
    public void onTCKResourceEventX1(TCKResourceEventX ev, ActivityContextInterface aci) {

        HashMap map = new HashMap();

        try {

            // Create the child SBB.
            SbbLocalObject child = getChildRelation().create();
            SbbLocalObject secondChild = getChildRelation().create();

            try {
                Collection children = getChildRelation();

                if (children.size() != 2) {
                    map.put("Result", new Boolean(false));
                    map.put("Message", "ChildRelation Collection.size() returned incorrect number of children.");
                    map.put("ID", new Integer(2102));
                    TCKSbbUtils.getResourceInterface().sendSbbMessage(map);
                    return;
                }

                if (children.isEmpty()) {
                    map.put("Result", new Boolean(false));
                    map.put("Message", "ChildRelation Collection.isEmpty() returned true when the Collection contained children.");
                    map.put("ID", new Integer(2103));
                    TCKSbbUtils.getResourceInterface().sendSbbMessage(map);
                    return;
                }

                if (!children.contains(child)) {
                    map.put("Result", new Boolean(false));
                    map.put("Message", "ChildRelation Collection.contains(SbbLocalObject) returned false for valid child.");
                    map.put("ID", new Integer(2104));
                    TCKSbbUtils.getResourceInterface().sendSbbMessage(map);
                    return;
                }

                Vector v = new Vector();
                v.add(child);
                v.add(secondChild);
                if (!children.containsAll(v)) {
                    map.put("Result", new Boolean(false));
                    map.put("Message", "ChildRelation Collection.containsAll(Vector) returned false for valid collection.");
                    map.put("ID", new Integer(2105));
                    TCKSbbUtils.getResourceInterface().sendSbbMessage(map);
                    return;
                }

                Object [] array = children.toArray();
                if (array.length != 2) {
                    map.put("Result", new Boolean(false));
                    map.put("Message", "ChildRelation Collection.toArray() returned incorrect array.");
                    map.put("ID", new Integer(2106));
                    TCKSbbUtils.getResourceInterface().sendSbbMessage(map);
                    return;
                }

                if (!((array[0].equals(child) && array[1].equals(secondChild))
                        || (array[0].equals(secondChild) && array[1].equals(child)))) {
                    map.put("Result", new Boolean(false));
                    map.put("Message", "ChildRelation Collection.toArray() returned incorrect array - didn't contain the required elements.");
                    map.put("ID", new Integer(2106));
                    TCKSbbUtils.getResourceInterface().sendSbbMessage(map);
                    return;
                }

                boolean passed = false;
                try {
                    children.add(getSbbContext().getSbbLocalObject());
                } catch (java.lang.UnsupportedOperationException e) {
                    passed = true;
                }

                if (passed == false) {
                    map.put("Result", new Boolean(false));
                    map.put("Message", "ChildRelation Collection.add(SbbLocalObject) didn't throw UnsupportedOperationException as it should have done.");
                    map.put("ID", new Integer(2107));
                    TCKSbbUtils.getResourceInterface().sendSbbMessage(map);
                    return;
                }

            } catch (ClassCastException e) {
                map.put("Result", new Boolean(false));
                map.put("Message", "ChildRelation object was not a Collection");
                TCKSbbUtils.getResourceInterface().sendSbbMessage(map);
                return;
            }

            map.put("Result", new Boolean(true));
            map.put("Message", "Ok - all checks passed");
            TCKSbbUtils.getResourceInterface().sendSbbMessage(map);


        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }
    }

    public abstract ChildRelation getChildRelation();

}
