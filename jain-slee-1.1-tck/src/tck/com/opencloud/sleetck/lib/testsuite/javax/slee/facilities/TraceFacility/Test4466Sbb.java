/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.javax.slee.facilities.TraceFacility;

import com.opencloud.sleetck.lib.resource.events.TCKResourceEventX;
import com.opencloud.sleetck.lib.sbbutils.BaseTCKSbb;
import com.opencloud.sleetck.lib.sbbutils.TCKSbbUtils;

import javax.naming.InitialContext;
import javax.slee.ActivityContextInterface;
import javax.slee.facilities.Level;
import javax.slee.facilities.TraceFacility;


public abstract class Test4466Sbb extends BaseTCKSbb {

    private static final String JNDI_TRACEFACILITY_NAME = "java:comp/env/slee/facilities/trace";
    public static final String TRACE_MESSAGE = "Test4466TraceMessage";

    // Initial event
    public void onTCKResourceEventX1(TCKResourceEventX ev, ActivityContextInterface aci) {

        try {
            TraceFacility facility = (TraceFacility) new InitialContext().lookup(JNDI_TRACEFACILITY_NAME);
            facility.createTrace(getSbbID(), Level.INFO, "javax.slee.management.trace", TRACE_MESSAGE, System.currentTimeMillis());

        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }

    }

}
