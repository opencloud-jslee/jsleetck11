/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.profiles.profiletable;

import java.util.Collection;
import javax.slee.profile.ProfileTable;
/**
 * 'query' prefix omitted.
 */
public interface Test1110130_5ProfileTable extends ProfileTable {
    public Collection isAnswerToLife();
    public Collection queryEqualsValue(String value);
    public Collection queryNotEqualsValue(String value);
    public Collection queryNotEqualsValues(String value, int intValue);
}
