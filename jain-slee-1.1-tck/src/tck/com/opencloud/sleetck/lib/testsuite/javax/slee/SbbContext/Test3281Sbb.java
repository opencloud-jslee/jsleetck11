/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.javax.slee.SbbContext;

import javax.slee.ActivityEndEvent;
import javax.slee.ActivityContextInterface;
import javax.slee.SbbContext;
import javax.slee.Address;
import javax.slee.SbbLocalObject;
import javax.slee.facilities.Level;
import javax.slee.ChildRelation;
import javax.slee.CreateException;

import com.opencloud.sleetck.lib.sbbutils.BaseTCKSbb;
import com.opencloud.sleetck.lib.sbbutils.TCKSbbUtils;
import com.opencloud.sleetck.lib.sbbutils.TCKSbbConstants;
import com.opencloud.sleetck.lib.resource.events.TCKResourceEventX;
import com.opencloud.sleetck.lib.resource.sbbapi.TCKResourceSbbInterface;

import java.util.HashMap;
import java.util.Iterator;

public abstract class Test3281Sbb extends BaseTCKSbb {

    // Override the BaseTCKSbb's version of this function which reports a test error.
    public void sbbRolledBack(javax.slee.RolledBackContext context) {
    }

    // Initial event
    public void onTCKResourceEventX1(TCKResourceEventX ev, ActivityContextInterface aci) {

        try {
            setValue(true);
            getSbbContext().setRollbackOnly();

            if (getSbbContext().getRollbackOnly() == false) {
                HashMap map = new HashMap();
                map.put("Result", new Boolean(false));
                map.put("Message", "SbbContext.getRollbackOnly() returned false on a marked for rollback transaction.");
                map.put("ID", new Integer(3289));
                TCKSbbUtils.getResourceInterface().sendSbbMessage(map);
            }

        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }
    }

    public void onTCKResourceEventX2(TCKResourceEventX ev, ActivityContextInterface aci) {

        HashMap map = new HashMap();

        if (getValue() == true) {
            map.put("Result", new Boolean(false));
            map.put("Message", "onTCKResourceEventX1 was marked for rollback, yet the CMP value change in it stayed");
            map.put("ID", new Integer(3281));
        } else {
            map.put("Result", new Boolean(true));
            map.put("Message", "Ok");
        }

        try {
            TCKSbbUtils.getResourceInterface().sendSbbMessage(map);
        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }
    }

    public abstract void setValue(boolean val);
    public abstract boolean getValue();

}
