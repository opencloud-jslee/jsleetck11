/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.management.sleestate;

import com.opencloud.sleetck.lib.AbstractSleeTCKTest;
import com.opencloud.sleetck.lib.OperationTimedOutException;
import com.opencloud.sleetck.lib.TCKTestErrorException;
import com.opencloud.sleetck.lib.TCKTestResult;
import com.opencloud.sleetck.lib.resource.TCKActivityID;
import com.opencloud.sleetck.lib.resource.events.TCKResourceEventX;
import com.opencloud.sleetck.lib.testutils.Assert;
import com.opencloud.sleetck.lib.testutils.BaseTCKResourceListener;
import com.opencloud.sleetck.lib.testutils.jmx.MBeanFacade;
import com.opencloud.sleetck.lib.testutils.jmx.SleeManagementMBeanProxy;

import javax.management.ObjectName;
import javax.slee.management.SleeState;
import javax.slee.management.SleeStateChangeNotification;
import java.rmi.RemoteException;

/**
 * Test assertion 1479: that all SLEE MBean types are available in all SLEE states.
 */
public class Test1479Test extends AbstractSleeTCKTest {
    public TCKTestResult run() throws Exception {
        ObjectName[] mbeanNames = { management.getAlarmMBean(),
                                    management.getDeploymentMBean(),
                                    management.getProfileProvisioningMBean(),
                                    management.getServiceManagementMBean(),
                                    management.getTraceMBean() };
        MBeanFacade facade = utils().getMBeanFacade();

        // Create an activity and fire an initial event on it to an SBB
        //  -  to keep the SBB in the stopping state long enough for the test
        TCKActivityID activity = utils().getResourceInterface().createActivity("Test1479Activity");
        utils().getResourceInterface().fireEvent(TCKResourceEventX.X1, null, activity, null);

        management.stop();
        waitForStateChange(SleeState.STOPPING);
        checkMBeansAvailable(facade, mbeanNames);

        // End the activity -- releases the SLEE from the stopping state.
        utils().getResourceInterface().endActivity(activity);

        waitForStateChange(SleeState.STOPPED);
        checkMBeansAvailable(facade, mbeanNames);

        management.start();
        waitForStateChange(SleeState.STARTING);
        checkMBeansAvailable(facade, mbeanNames);
        waitForStateChange(SleeState.RUNNING);
        checkMBeansAvailable(facade, mbeanNames);

        return TCKTestResult.passed();
    }

    public void setUp() throws Exception {
        super.setUp();
        management = utils().getSleeManagementMBeanProxy();
        stateListener = new QueuingSleeStateListener(utils());
        management.addNotificationListener(stateListener, null, null);
        setResourceListener(new ResourceListenerImpl());
    }

    public void tearDown() throws Exception {
        if (null != management)
            management.removeNotificationListener(stateListener);
        super.tearDown();
    }

    private void checkMBeansAvailable(MBeanFacade facade, ObjectName[] jmxNames) throws Exception {
        for (int i = 0; i < jmxNames.length; i++) {
            ObjectName jmxName = jmxNames[i];
            if (!facade.isRegistered(jmxName)) {
                Assert.fail(1479, "MBean " + jmxName + " is not registered in the " + management.getState() + " state");
            }
        }
    }

    private void waitForStateChange(SleeState expectedState) throws Exception {
        // wait for the state change
        getLog().info("state: " + management.getState());
        SleeStateChangeNotification stateChange = null;
        try {
            getLog().info("Waiting to move to the " + expectedState + " state");
            stateChange = stateListener.nextNotification();
            getLog().info("Found state change: " + stateChange);
        } catch (OperationTimedOutException toe) {
            throw new TCKTestErrorException("Timed out waiting for SLEE to change to the " + expectedState + " state");
        }
    }

    public class ResourceListenerImpl extends BaseTCKResourceListener {
        public void onException(Exception exception) throws RemoteException {
            utils().getLog().warning("An Exception was received from the SBB or the TCK resource:");
            utils().getLog().warning(exception);
        }
    }

    private QueuingSleeStateListener stateListener;
    private SleeManagementMBeanProxy management;
}
