/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.events.convergencename;

import com.opencloud.sleetck.lib.AbstractSleeTCKTest;
import com.opencloud.sleetck.lib.TCKTestErrorException;
import com.opencloud.sleetck.lib.TCKTestFailureException;
import com.opencloud.sleetck.lib.resource.TCKActivityID;
import com.opencloud.sleetck.lib.testutils.BaseTCKResourceListener;

import javax.slee.Address;
import java.rmi.RemoteException;
import java.util.*;

/**
 * The AbstractConvergenceNameTest provides a base class for many of
 * the convergence name tests.
 *
 * The implementation class must provide the implementation of the run()
 * method to make calls to sendEventAndWait() as approriate to the test.
 *
 * The sendEventAndWait() method sends an event as described, then checks that
 * SBB instances are created as appropriate, and that the correct set of
 * SBB instances receive the events.
 *
 * Note: each SBB instance is identified by the application level id of its initial event
 * (i.e. from the eventID parameter passed to the sendEventAndWait() method).
 */
public class AbstractConvergenceNameTest extends AbstractSleeTCKTest {

    // -- Constants -- //

    // Test parameter keys

    public static final String SELECTED_VARIABLES    = "selectedVariables";
    public static final String MAIN_ASSERTION_ID     = "mainAssertionID";

    // Convergence name variables

    public static final String ACTIVITY_CONTEXT  = "ActivityContext";
    public static final String ADDRESS_PROFILE   = "AddressProfile";
    public static final String ADDRESS           = "Address";
    public static final String EVENT_TYPE        = "EventType";
    public static final String EVENT_OBJECT      = "Event";
    public static final String CUSTOM_NAME       = "CustomName";

    // Other constants

    protected static final String ADDRESS_PREFIX = "1.0.0.";
    protected static final String ACTIVITY_ID_PREFIX = "Activity";

    // -- Public methods -- //

    // SleeTCKTest methods

    public void setUp() throws Exception {
        super.setUp();
        setResourceListener(new TCKResourceListenerImpl());
        mainAssertionID = Integer.parseInt(utils().getTestParams().getProperty(MAIN_ASSERTION_ID));
        isEventDeliveryFinished = false;
        // read and validate the list of selected convergence name variables
        selected = elementsFromTokens(utils().getTestParams().getProperty(SELECTED_VARIABLES));
        if(selected != null) {
            String[] validVariables = {ACTIVITY_CONTEXT, ADDRESS_PROFILE, ADDRESS,
                                        EVENT_TYPE, EVENT_OBJECT, CUSTOM_NAME};
            Arrays.sort(validVariables);
            Iterator selectedIter = selected.iterator();
            while(selectedIter.hasNext()) {
                String aVariable = (String)selectedIter.next();
                if(Arrays.binarySearch(validVariables,aVariable) < 0) throw new TCKTestErrorException(
                    "Invalid convergence name variable specified in the "+SELECTED_VARIABLES+" parameter: "+aVariable);
            }
        }
    }

    // -- Protected methods -- //

    /**
     * Sends the given event with the given parameters,
     * then waits until any of the following occur:
     * (1) The event is claimed to be processed successfully,
     * (2) A failure or error is detected,
     * (3) The timeout is reached.
     * An Exception is thrown in cases (2) and (3).
     *
     * @param eventTypeName the name of the event type, eg TCKResourceEventX.X1
     * @param eventID An arbitrary unique ID for the event. If the event causes
     *  a new SBB instance to be created, then this ID is used to identify the SBB instance.
     * @param activityID the ID of the activity on which to fire the event
     * @param address the address to deliver with the event
     * @param expectedConvergenceNameSbb The ID of the SBB instance with the convergence name
     *  matching this event. Should be the same as eventID if the event is expected to cause
     *  a new SBB instance to be created.
     *  If null, this indicates that no SBB should be created for the event,
     *  e.g. because the event is not an initial event for the SBB, or the InitialEvent attribute will be set to false,
     *  or no convergence name will be created for the event.
     * @param iesParams parameters for the initial event selector method
     *  (may be null if the initial event selector method is not used)
     */
    protected void sendEventAndWait(String eventTypeName, String eventID, TCKActivityID activityID,
                            Address address, String expectedConvergenceNameSbb,
                            InitialEventSelectorParameters iesParams) throws Exception {

        synchronized (stateLock) {
            if(eventTypeName == null) throw new NullPointerException("eventTypeName may not be null");
            if(eventID == null) throw new NullPointerException("eventID may not be null");
            if(activityID == null) throw new NullPointerException("activityID may not be null");

            currentEventID = eventID;
            this.expectedConvergenceNameSbb = expectedConvergenceNameSbb;

            // each event ID may only be used once
            if(previousEvents.contains(eventID)) throw new TCKTestErrorException("Programmer error: eventID "+eventID+" has already been used");

            if(eventID.equals(expectedConvergenceNameSbb)) {
                // we are expecting the SLEE to create a new SBB instance and attach it to the activity
                Vector willAttachTo = new Vector();
                willAttachTo.addElement(activityID);
                instancesToActivities.put(eventID,willAttachTo);
            } else if(expectedConvergenceNameSbb != null) {
                // check that the expectedConvergenceNameSbb references a known SBB instance
                if(!instancesToActivities.containsKey(expectedConvergenceNameSbb)) throw new TCKTestErrorException(
                    "Programmer error: expectedConvergenceNameSbb parameter references an unknown SBB instance. "+
                    "Initial event of expectedConvergenceNameSbb=: "+expectedConvergenceNameSbb);
                // expect the Sbb to become attached to the activity
                Vector attachedTo = (Vector)instancesToActivities.get(expectedConvergenceNameSbb);
                if(!attachedTo.contains(activityID)) attachedTo.addElement(activityID);
            }

            // expect the event to be delivered to all SBB instances attached to the activity
            Iterator instancesToActivitiesIter = instancesToActivities.keySet().iterator();
            while(instancesToActivitiesIter.hasNext()) {
                Object instance = instancesToActivitiesIter.next();
                Vector attachedTo = (Vector)instancesToActivities.get(instance);
                if(attachedTo != null) {
                    Iterator attachedToIter = attachedTo.iterator();
                    while(attachedToIter.hasNext()) {
                        Object attachedActivity = attachedToIter.next();
                        if(activityID.equals(attachedActivity)) {
                            expectedEventReceivers.addElement(instance);
                        }
                    }
                }
            }
        }

        // create the event message and fire the event
        Object eventMessage = new EventMessageData(eventID,iesParams).toExported();
        utils().getResourceInterface().fireEvent(eventTypeName, eventMessage, activityID, address);

        synchronized (stateLock) {
            eventHistory.addElement("eventTypeName="+eventTypeName+", eventID="+eventID+", activityID="+activityID+
                            ",address="+address+", expectedConvergenceNameSbb="+expectedConvergenceNameSbb+
                            ",iesParams=["+iesParams+"]");
            // wait for success, failure, error, or timeout
            waitForResponse();
            // clear state
            expectedEventReceivers.clear();
            eventReceivers.clear();
            previousEvents.addElement(currentEventID);
            currentEventID = null;
            expectedConvergenceNameSbb = null;
            isEventDeliveryFinished = false;
        }
    }

    /**
     * Returns the list of selected convergence name variables
     */
    protected List selected() {
        return selected;
    }

    /**
     * Set the ID of the main assertion being tested
     * @param mainAssertionID
     */
    protected void setMainAssertionID(int mainAssertionID) {
        this.mainAssertionID = mainAssertionID;
    }

    // -- Private methods -- //

    /**
     * Waits until any of the following occur:
     * (1) The event is claimed to be processed successfully,
     * (2) A failure or error is detected,
     * (3) The timeout is reached.
     * An Exception is thrown in cases (2) and (3).
     * When all events have been processed successfully, this method waits for pauseBetweenEvents ms,
     * then rechecks (to detect subsequent unexpected events/errors).
     */
    private void waitForResponse() throws Exception {
        long now = System.currentTimeMillis();
        long timeoutAt = now + utils().getTestTimeout();
        synchronized (stateLock) {
            while(now < timeoutAt && !isEventDeliveryFinished && failureOrError == null) {
                try {
                    stateLock.wait(timeoutAt - now);
                } catch(InterruptedException ie) {/*no-op*/}
                now = System.currentTimeMillis();
            }
            if(failureOrError != null) throw failureOrError;
            if(!isEventDeliveryFinished) throw new TCKTestErrorException("Timed out waiting waiting for an "+
                    "onEventProcessingSuccessful() or onEventProcessingFailed() callback for the event. "+
                    "Context info:"+getContextInfo());
            if(!isEventDeliverySuccessful()) {
                diagnoseMissedDelivery(); // will throw a TCKTestFailureException
            }
        }
    }

    /**
     * Returns true if the current event has been delivered to all expected SBB instances;
     * false otherwise.
     */
    private boolean isEventDeliverySuccessful() {
        synchronized (stateLock) {
            return expectedEventReceivers.size() == eventReceivers.size();
        }
    }

    /**
     * Called when onEventProcessingSuccessful() or onEventProcessingFailed() is called for the event,
     * but one or more SBBs unexpectedly missed the event.
     * Diagnoses the failure and throws the appropriate TCKTestFailureException.
     */
    private void diagnoseMissedDelivery() throws TCKTestFailureException {
        synchronized (stateLock) {
            if(expectedConvergenceNameSbb != null && !eventReceivers.contains(expectedConvergenceNameSbb)) {
                if(expectedConvergenceNameSbb.equals(currentEventID)) {
                    // the expected new instance was not created
                    throw new TCKTestFailureException(mainAssertionID,"The expected new Sbb instance was not created "+
                     "for the event, which implies a failure in the convergence name algorithm. Context info:"+getContextInfo());
                } else {
                    // the expected existing instance did not receive the event
                    throw new TCKTestFailureException(mainAssertionID,"The existing Sbb instance which matched the "+
                     "convergence name of the event did not receive the event. "+
                     "This implies a failure in the convergence name algorithm. Context info:"+getContextInfo());
                }
            } else {
                // find the set of expected receivers which did not receive the event
                Vector missers = new Vector(expectedEventReceivers);
                missers.removeAll(eventReceivers);
                throw new TCKTestFailureException(769,
                    "Existing Sbb(s) which were expected to be attached to activity did not receive the event. "+
                    formatList(missers,"Sbb instances which missed the event",new StringBuffer()).toString()+". Context info:"+getContextInfo());
            }
        }
    }

    /**
     * Returns a String containing test context information.
     */
    private String getContextInfo() {
        StringBuffer rBuf = new StringBuffer("\n-- Context info: --\n");
        synchronized (stateLock) {
            formatList(selected(),"Selected convergence name variables",rBuf);
            formatList(eventHistory,"Event history list",rBuf);
            rBuf.append("currentEventID="+currentEventID+"\n");
            if(expectedConvergenceNameSbb != null)rBuf.append("expected initial event of convergence name SBB="+expectedConvergenceNameSbb+"\n");
            else rBuf.append("no convergence name expected, or not an initial event\n");
            formatList(eventReceivers,"Event receivers list",rBuf);
            formatList(expectedEventReceivers,"Expected event receivers list",rBuf);
            rBuf.append("Expected SBB instance and their activities:\n");
            Iterator instancesIter = instancesToActivities.keySet().iterator();
            while(instancesIter.hasNext()) {
                Object instanceID = instancesIter.next();
                rBuf.append("instanceID/initialEventID="+instanceID+';');
                rBuf.append("activityIDs={");
                Vector attachedTo = (Vector)instancesToActivities.get(instanceID);
                Iterator attachedToIter = attachedTo.iterator();
                while(attachedToIter.hasNext()) rBuf.append(attachedToIter.next()+",");
                rBuf.append("};\n");
            }
        }
        rBuf.append("(Context info ends)");
        return rBuf.toString();
    }

    private StringBuffer formatList(List list, String name, StringBuffer buf) {
        buf.append(name+":\n");
        Iterator listIter = list.iterator();
        while(listIter.hasNext()) buf.append(listIter.next()).append('\n');
        return buf.append("("+name+" ends)\n");
    }

    /**
     * Returns a Vector containing the elements in the given comma separated list.
     */
    private Vector elementsFromTokens(String commaSeparatedList) {
        Vector elements = null;
        if(commaSeparatedList != null) {
            StringTokenizer tokens = new StringTokenizer(commaSeparatedList,",");
            elements = new Vector();
            while(tokens.hasMoreTokens()) elements.addElement(tokens.nextToken());
        }
        return elements;
    }

    // Private classes

    private class TCKResourceListenerImpl extends BaseTCKResourceListener {

        // TCKResourceListener methods

        public synchronized Object onSbbCall(Object argument) throws Exception {
            try {
                EventReplyData replyData = EventReplyData.fromExported(argument);
                String receivedEventID = replyData.getReceivedEventID();
                String initialEventID = replyData.getInitialEventID();
                getLog().info("Received message from SBB. receivedEventID="+receivedEventID+",initialEventID="+initialEventID);
                // validate arguments
                if(receivedEventID == null) throw new TCKTestErrorException("Received response containing a null receivedEventID. Context info:"+getContextInfo());
                if(initialEventID == null) throw new TCKTestErrorException("Received response containing a null initialEventID. Context info:"+getContextInfo());

                synchronized (stateLock) {
                    // we only process one event at a time
                    if(!receivedEventID.equals(currentEventID)) {
                        if(receivedEventID.equals(initialEventID)) {
                            // An Sbb was created unexpectedly for the last event - i.e. the convergence name handling failed
                            throw new TCKTestFailureException(mainAssertionID,"Received event "+
                                receivedEventID+" on a new Sbb instance after it was delivered to all expected receivers, "+
                                "which implies a failure in the convergence name algorithm. Context info: "+getContextInfo());
                        } else {
                            throw new TCKTestFailureException(mainAssertionID,"Received event "+
                                receivedEventID+" after it was delivered to all expected receivers. Illegal receiver's initial event="+
                                initialEventID+". This Sbb instance was not the expected convergence name target, and was not "+
                                "expected to be previously attached to the activity: this implies a failure in the "+
                                "convergence name algorithm. Context info:"+getContextInfo());
                        }
                    }

                    // check that the event is received by the SBB instance only once
                    if(eventReceivers.contains(initialEventID)) throw new TCKTestFailureException(1984,
                        "Event "+receivedEventID+" was received twice by SBB instance with initial event "+initialEventID+
                        ". Context info:"+getContextInfo());
                    eventReceivers.addElement(initialEventID);

                    // check that the SBB is expected to receive the event
                    if(!expectedEventReceivers.contains(initialEventID)) {
                        if(initialEventID.equals(receivedEventID)) {
                            throw new TCKTestFailureException(mainAssertionID,"The SLEE created an SBB instance "+
                                "when not expected to: this implies a failure in the "+
                                "convergence name algorithm. Context info:"+getContextInfo());
                        } else {
                            throw new TCKTestFailureException(mainAssertionID,"The SLEE delivered the event to an SBB "+
                                "instance not attached to the activity, with a different convergence name from the event. "+
                                "this implies a failure in the convergence name algorithm. Context info: "+getContextInfo());
                        }
                    }

                    // logging
                    if(initialEventID.equals(expectedConvergenceNameSbb)) {
                        // the event was delivered to the SBB with the convergence name
                        if(initialEventID.equals(receivedEventID)) {
                            getLog().fine("The SLEE created a new SBB instance for the event");
                        } else {
                            getLog().fine("The SLEE delivered the event to an existing SBB instance with a matching convergence name");
                        }
                    } else getLog().fine("The SLEE delivered the event to an SBB instance which was attached to the event's activity");

                    // notify the test thread if the event has been received by all expected receivers
                    if(expectedEventReceivers.size() == eventReceivers.size()) stateLock.notifyAll();
                }
            } catch(Exception e) {
                synchronized (stateLock) {
                    failureOrError = e;
                    stateLock.notifyAll();
                }
            }
            return null;
        }

        public void onEventProcessingSuccessful(long eventObjectID) throws RemoteException {
            synchronized(stateLock) {
                isEventDeliveryFinished = true;
                stateLock.notifyAll();
            }
        }

        public void onEventProcessingFailed(long eventObjectID, String message, Exception exception) throws RemoteException {
            synchronized(stateLock) {
                isEventDeliveryFinished = true;
                // this callback indicates a test error
                StringBuffer buf = new StringBuffer("Received onEventProcessingFailed() callback for TCKResourceEvent with object ID="+eventObjectID);
                if(message != null) buf.append(message);
                onException(new TCKTestErrorException(buf.toString(),exception));
            }
        }

        public void onException(Exception exception) throws RemoteException {
            getLog().warning("Received Exception from SBB or resource:");
            getLog().warning(exception);
            synchronized (stateLock) {
                failureOrError = exception;
                stateLock.notifyAll();
            }
        }

    }

    // Private state

    // all the variables and data sets below are synchronized on stateLock
    private Object stateLock = new Object();
    // the selected convergence name variables
    private Vector selected;
    // the ID of the main assertion being tested
    private int mainAssertionID;
    // the application level event id of the event being tested
    private String currentEventID;
    // The SBB instance (by initial event id) which is expected to match the convergence name
    // of the event. This will be the currentEventID if the event is expected to create a new instance.
    // A null value indicates that no SBB should be created for the event.
    private String expectedConvergenceNameSbb;

    // whether onEventProcessingSuccessful() or onEventProcessingFailed() has been called for the event
    private boolean isEventDeliveryFinished;
    // set when a failure or error is detected in the resource listener
    private Exception failureOrError;

    // a map of expected SBB instances (by initial event id) to the activities we expect them to be attached to
    private final HashMap instancesToActivities = new HashMap();
    // the expected receivers of the current event
    private final Vector expectedEventReceivers = new Vector();
    // the set of SBB instances which have received the current event
    private final Vector eventReceivers = new Vector();
    // events previously tested in this test run
    private final Vector previousEvents = new Vector();
    // contains one human readable String of event related information for each events sent during the test run
    private final Vector eventHistory = new Vector();

}
