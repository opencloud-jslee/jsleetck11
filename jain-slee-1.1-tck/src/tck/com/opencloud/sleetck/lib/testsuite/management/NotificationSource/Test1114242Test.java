/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.management.NotificationSource;

import java.util.HashMap;

import javax.management.Notification;
import javax.management.NotificationListener;
import javax.slee.management.NotificationSource;
import javax.slee.management.AlarmNotification;
import javax.slee.management.ResourceAdaptorEntityNotification;

import com.opencloud.sleetck.lib.TCKTestErrorException;
import com.opencloud.sleetck.lib.TCKTestFailureException;
import com.opencloud.sleetck.lib.TCKTestResult;
import com.opencloud.sleetck.lib.testsuite.resource.BaseResourceTest;
import com.opencloud.sleetck.lib.testsuite.resource.RAMethods;
import com.opencloud.sleetck.lib.testutils.FutureResult;
import com.opencloud.sleetck.lib.testutils.jmx.AlarmMBeanProxy;
import com.opencloud.util.Future;
import com.opencloud.util.Future.TimeoutException;

/*
 * AssertionID (1114242):
 * The ResourceAdaptorEntityNotification class identifies a resource adaptor entity as the
 * source of a notification.
 *
 */

public class Test1114242Test extends BaseResourceTest {

    private static final int ASSERTION_ID1 = 1114242;

    public void run(FutureResult result) throws Exception {
        this.result = result;
        Future future = new Future();
        utils().getAlarmMBeanProxy().addNotificationListener(new AlarmNotificationListenerImpl(future), null, null);

        // Trigger RA to send Alarm()
        HashMap resultmap = sendMessage(RAMethods.getAlarmFacility);
        if (resultmap == null)
            throw new TCKTestFailureException(ASSERTION_ID1, "Test timed out while waiting for response to getAlarmFacility()");
        Object mapResult = resultmap.get("result");
        if (mapResult instanceof Exception)
            throw new TCKTestFailureException(ASSERTION_ID1, "Exception thrown while invoking getAlarmFacility()", (Exception) mapResult);
        if (Boolean.FALSE.equals(mapResult))
            throw new TCKTestFailureException(ASSERTION_ID1, "ResourceAdaptorContext.getAlarmFacility() returned null");
        if (!Boolean.TRUE.equals(mapResult))
            throw new TCKTestErrorException("Unexpected result received while invoking getAlarmFacility(): " + result);

        synchronized (this) {
            wait(utils().getTestTimeout());
        }

        if (passed) {
            result.setPassed();
        } else {
            if (alarmProcessed)
                throw new TCKTestFailureException(ASSERTION_ID1, "Unexpected alarm notification type received in notification. " );
            else
                throw new TCKTestFailureException(ASSERTION_ID1, "Did not receive alarm notification within the timeout period " );
        }
    }

    public class AlarmNotificationListenerImpl implements NotificationListener {

        public AlarmNotificationListenerImpl(Future future) {
            this.future = future;
        }

        public final void handleNotification(Notification notification, Object handback) {
            getLog().info("Notification received: " + notification);
            if (notification instanceof AlarmNotification) {
                getLog().fine("Received Alarm Notification: " +notification);
                AlarmNotification alarmNotification = (AlarmNotification) notification;
                if ((!alarmProcessed) && (alarmNotification.getType().equals(ResourceAdaptorEntityNotification.ALARM_NOTIFICATION_TYPE))) {
                    NotificationSource notificationSource =  alarmNotification.getNotificationSource();
                    ResourceAdaptorEntityNotification resourceNotification = (ResourceAdaptorEntityNotification) notificationSource;
                    passed = doNotficationSourcesCheck(resourceNotification);
                    alarmProcessed = true;
                    if (passed == true)
                        logSuccessfulCheck(1114242);
                }
           }
        }
       private Future future;
    }

    public void tearDown() throws Exception {
        try {
            // Clear any alarms generated during the test.
            AlarmMBeanProxy alarmMBean = utils().getAlarmMBeanProxy();
            String[] alarms = alarmMBean.getAlarms();
            for (int i = 0; i < alarms.length; i++)
                alarmMBean.clearAlarm(alarms[i]);
        } finally {
            super.tearDown();
        }
    }

    private boolean doNotficationSourcesCheck(ResourceAdaptorEntityNotification theNotification) {
        ResourceAdaptorEntityNotification expectedRANotification = null;
        boolean passed = true;
        String entityName = null;
        String notification;
        final String expectedRAentity = "TCK_Context_Test_RA_Entity";

        //1114240
        try {
            entityName = theNotification.getEntityName();
            getLog().fine("1114240: getEntityName : " +entityName);
            if (entityName.equals(expectedRAentity)) {
                logSuccessfulCheck(1114240);
            } else {
                result.setFailed(1114240, "Incorrect RA Entity Name");
                passed = false;
            }
        }
        catch (Exception e) {
            getLog().warning(e);
            result.setFailed(1114240, "Incorrect RA Entity Name");
            return false;
        }

        //1114243
        try {
            notification = theNotification.toString();
            getLog().fine("1114243: toString() : " +notification);
            logSuccessfulCheck(1114243);
        }
        catch (Exception e) {
            getLog().warning(e);
            result.setFailed(1114243, "Incorrect Notification Name");
            return false;
        }

        //1114246
        try {
            theNotification.hashCode();
            logSuccessfulCheck(1114246);
        } catch (Exception e) {
            result.setFailed(1114246, ".hashCode() failed");
            return false;
        }

        // 1114223
        expectedRANotification = new ResourceAdaptorEntityNotification(expectedRAentity);
        ResourceAdaptorEntityNotification notExpectedRANotification =new ResourceAdaptorEntityNotification("TCK_Context_Test_RA_Entity2");
        try {
            getLog().fine("1114223: theNotification.equals : "+theNotification + ", " +expectedRANotification);
            if (theNotification.equals(expectedRANotification)) {
                logSuccessfulCheck(1114223);
            } else {
                result.setFailed(1114223, ".equals() failed");
                passed = false;
            }
        }
        catch (Exception e) {
            getLog().warning(e);
            result.setFailed(1114223, ".equals() failed");
            return false;
        }

        // 1114224
        try {
            int areCompared;
            areCompared = theNotification.compareTo(expectedRANotification);
            if (areCompared == 0) {
                areCompared = theNotification.compareTo(notExpectedRANotification);
                getLog().fine("1114224: notExpectedProfileNotification = : " +notExpectedRANotification);
                if (areCompared != 0)
                      logSuccessfulCheck(1114224);
                else
                    result.setFailed(1114224, ".compareTo() failed non-equal test");
            } else {
                result.setFailed(1114224, ".compareTo() failed");
                passed = false;
            }
        }
        catch (Exception e) {
            getLog().warning(e);
            result.setFailed(1114224, ".compareTo() failed");
            return false;
        }

        //1114247
        try {
            String alarmNotify = theNotification.getAlarmNotificationType();
            getLog().fine("1114247: getAlarmNotificationType = "+alarmNotify);
            if (alarmNotify.equals(ResourceAdaptorEntityNotification.ALARM_NOTIFICATION_TYPE)) {
                logSuccessfulCheck(1114247);
            } else {
                result.setFailed(1114247, "getAlarmNotificationType() failed");
                passed = false;
            }
        }
        catch (Exception e) {
            getLog().warning(e);
            result.setFailed(1114247, "getAlarmNotificationType() failed");
            return false;
        }

        //1114239
        try {
            String alarmNotify = theNotification.getTraceNotificationType();
            getLog().fine("1114239: getTraceNotificationType = "+alarmNotify);
            if (alarmNotify.equals(ResourceAdaptorEntityNotification.TRACE_NOTIFICATION_TYPE)) {
                logSuccessfulCheck(1114239);
            } else {
                result.setFailed(1114239, "getTraceNotificationType() failed");
                passed = false;
            }
        }
        catch (Exception e) {
            getLog().warning(e);
            result.setFailed(1114239, "getTraceNotificationType() failed");
            return false;
        }

        //1114245
        try {
            String raNotify = theNotification.getUsageNotificationType();
            getLog().fine("1114245: getUsageNotificationType = "+raNotify);
            if (raNotify.equals(ResourceAdaptorEntityNotification.USAGE_NOTIFICATION_TYPE)) {
                logSuccessfulCheck(1114245);
            } else {
                result.setFailed(1114245, "getUsageNotificationType() failed");
                passed = false;
            }
        }
        catch (Exception e) {
            getLog().warning(e);
            result.setFailed(1114245, "getUsageNotificationType() failed");
            return false;
        }

        //1114244
        try {
            String usageMBeanProperties = theNotification.getUsageMBeanProperties();
            getLog().fine("1114244: getUsageMBeanProperties = "+usageMBeanProperties);
            if (usageMBeanProperties.contains(expectedRAentity)) {
                logSuccessfulCheck(1114244);
            } else {
                result.setFailed(1114244, "getUsageNotificationType() failed");
                passed = false;
            }
        }
        catch (Exception e) {
            getLog().warning(e);
            result.setFailed(1114244, "getUsageNotificationType() failed");
            return false;
        }

        //1114413
        try {
            String usageMBeanProperties = theNotification.getUsageMBeanProperties(entityName);
            getLog().fine("1114413: getUsageMBeanProperties = "+usageMBeanProperties);
            if (usageMBeanProperties.contains(expectedRAentity)) {
                logSuccessfulCheck(1114413);
            } else {
                result.setFailed(1114413, "getUsageNotificationType() failed");
                passed = false;
            }
        }
        catch (Exception e) {
            getLog().warning(e);
            result.setFailed(1114413, "getUsageNotificationType() failed");
            return false;
        }

        return passed;
    }

     protected void logSuccessfulCheck(int assertionID) {
        utils().getLog().info("Check for assertion "+assertionID+" OK");
    }

    //
    // Private
    //

    private boolean passed = false;
    private FutureResult result;
    private boolean alarmProcessed = false;

}
