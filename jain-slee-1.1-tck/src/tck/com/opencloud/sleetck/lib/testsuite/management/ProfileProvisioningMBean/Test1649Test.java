/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.management.ProfileProvisioningMBean;

import com.opencloud.sleetck.lib.SleeTCKTest;
import com.opencloud.sleetck.lib.SleeTCKTestUtils;
import com.opencloud.sleetck.lib.TCKTestResult;
import com.opencloud.sleetck.lib.resource.TCKActivityID;
import com.opencloud.sleetck.lib.resource.TCKSbbMessage;
import com.opencloud.sleetck.lib.resource.testapi.TCKResourceListener;
import com.opencloud.sleetck.lib.testutils.BaseTCKResourceListener;
import com.opencloud.sleetck.lib.testutils.FutureResult;
import com.opencloud.sleetck.lib.testutils.ProfileUtils;
import com.opencloud.sleetck.lib.testutils.jmx.DeploymentMBeanProxy;
import com.opencloud.sleetck.lib.testutils.jmx.ProfileMBeanProxy;
import com.opencloud.sleetck.lib.testutils.jmx.ProfileProvisioningMBeanProxy;

import javax.management.ObjectName;
import javax.slee.ComponentID;
import javax.slee.management.DeployableUnitDescriptor;
import javax.slee.management.DeployableUnitID;
import javax.slee.profile.AttributeNotIndexedException;
import javax.slee.profile.AttributeTypeMismatchException;
import javax.slee.profile.ProfileSpecificationID;
import javax.slee.profile.UnrecognizedAttributeException;
import java.rmi.RemoteException;
import java.util.Collection;
import java.util.HashMap;

public class Test1649Test implements SleeTCKTest {

    private static final String SERVICE_DU_PATH_PARAM = "DUPath";
    private static final int TEST_ID = 1649;

    public void init(SleeTCKTestUtils utils) {
        this.utils = utils;
    }

    /**
     * Perform the actual test.
     */

    public TCKTestResult run() throws Exception {

        profileUtils = new ProfileUtils(utils);
        ProfileProvisioningMBeanProxy profileProxy = profileUtils.getProfileProvisioningProxy();
        DeploymentMBeanProxy duProxy = utils.getDeploymentMBeanProxy();
        DeployableUnitDescriptor duDesc = duProxy.getDescriptor(duID);
        ComponentID components[] = duDesc.getComponents();

        for (int i = 0; i < components.length; i++) {
            if (components[i] instanceof ProfileSpecificationID) {
                ProfileSpecificationID profileSpecID = (ProfileSpecificationID) components[i];

                // Create the profile table.
                try {
                    profileProxy.createProfileTable(profileSpecID, "Test1649ProfileTable");
                    ObjectName profile = profileProxy.createProfile("Test1649ProfileTable", "Test1649Profile");
                    // Commit the created profile.
                    ProfileMBeanProxy proxy = utils.getMBeanProxyFactory().createProfileMBeanProxy(profile);
                    proxy.commitProfile();
                    proxy.closeProfile();
                } catch (Exception e) {
                    return TCKTestResult.error("Failed to create profile table or profile.");
                }

                Collection profiles = profileProxy.getProfilesByIndexedAttribute("Test1649ProfileTable", "attributeC", "Foo");

                // Assertion 1650
                if (profiles.size() != 1) {
                    return TCKTestResult.failed(1650, "Search for attributeC of 'Foo' failed");
                }

                // Assertion 1651
                boolean passed = false;
                try {
                    profileProxy.getProfilesByIndexedAttribute("Test1649ProfileTable", "attributeC", new Boolean(true));
                } catch (AttributeTypeMismatchException e) {
                    passed = true;
                }

                if (passed == false) {
                    return TCKTestResult.failed(1651, "attributeC is of type java.lang.String, but searching for java.lang.Boolean didn't throw AttributeTypeMismatchException.");
                }

                // Assertion 1652
                try {
                    profileProxy.getProfilesByIndexedAttribute("Test1649ProfileTable", "attributeA", new Boolean(true));
                } catch (Exception e) {
                    return TCKTestResult.failed(1652, "AttributeValue object was appropriate for attributeA but an Exception was thrown.");
                }

                passed = false;
                try {
                    profileProxy.getProfilesByIndexedAttribute("Test1649ProfileTable", "attributeA", new Integer(1));
                } catch (AttributeTypeMismatchException e) {
                    passed = true;
                }

                if (passed == false) {
                    return TCKTestResult.failed(1652, "AttributeValue object was inappropriate for attribute expecting boolean type, yet AttributeTypeMismatchException was not thrown.");
                }

                // Assertion 1653
                profiles = profileProxy.getProfilesByIndexedAttribute("Test1649ProfileTable", "attributeB", "Bar");
                if (profiles.size() != 1) {
                    return TCKTestResult.failed(1653, "Expected one match, got " + profiles.size());
                }

                profiles = profileProxy.getProfilesByIndexedAttribute("Test1649ProfileTable", "attributeB", "NonExistant");
                if (profiles.size() != 0) {
                    return TCKTestResult.failed(1653, "Expected zero matches, got " + profiles.size());
                }

                // Assertion 1654
                passed = false;
                try {
                    profileProxy.getProfilesByIndexedAttribute("Test1649ProfileTable", "attributeB", new Integer(1));
                } catch (AttributeTypeMismatchException e) {
                    passed = true;
                }

                if (passed == false) {
                    return TCKTestResult.failed(1654, "AttributeValue object was inappropriate for attribute expecting String type, yet AttributeTypeMismatchException was not thrown.");
                }

                // Assertion 1655 and 1656
                passed = false;
                try {
                    profileProxy.getProfilesByIndexedAttribute("Test1649ProfileTable", "attributeD", new Integer(42));
                } catch (AttributeTypeMismatchException e) {
                    passed = true;
                }

                if (passed == false) {
                    return TCKTestResult.failed(1656, "AttributeValue object was inappropriate for attribute expecting boolean type, yet AttributeTypeMismatchException was not thrown.");
                }

                passed = false;
                try {
                    profileProxy.getProfilesByIndexedAttribute("Test1649ProfileTable", "attributeF", new Boolean(true));
                } catch (UnrecognizedAttributeException e) {
                    passed = true;
                }

                if (passed == false) {
                    return TCKTestResult.failed(1662, "Should have thrown UnrecognizedAttributeException when non-existent attribute was used.");
                }

                try {
                    profileProxy.getProfilesByIndexedAttribute("Test1649ProfileTable", "attributeE", new Boolean(true));
                } catch (AttributeNotIndexedException e) {
                    return TCKTestResult.passed();
                }

                return TCKTestResult.failed(1665, "Should have thrown AttributeNotIndexedException when non-indexed attribute was used.");
            }
        }

        return TCKTestResult.error("No ProfileSpecification found.");
    }

    /**
     * Do all the pre-run configuration of the test.
     */

    public void setUp() throws Exception {

        utils.getLog().fine("Connecting to resource");
        resourceListener = new TCKResourceListenerImpl();
        utils.getResourceInterface().setResourceListener(resourceListener);
        utils.getLog().fine("Installing and activating service");

        // Install the Deployable Units
        String duPath = utils.getTestParams().getProperty(SERVICE_DU_PATH_PARAM);
        duID = utils.install(duPath);
        utils.activateServices(duID, true);

    }

    /**
     * Clean up after the test.
     */

    public void tearDown() throws Exception {

        if (profileUtils != null) {
            ProfileProvisioningMBeanProxy profileProxy = profileUtils.getProfileProvisioningProxy();

            try {
                profileProxy.removeProfile("Test1649ProfileTable", "Test1649Profile");
            } catch (Exception e) {
            }

            try {
                profileUtils.removeProfileTable("Test1649ProfileTable");
            } catch (Exception e) {
            }
        }

        utils.getLog().fine("Disconnecting from resource");
        utils.getResourceInterface().clearActivities();
        utils.getResourceInterface().removeResourceListener();
        utils.getLog().fine("Deactivating and uninstalling service");
        utils.deactivateAllServices();
        utils.uninstallAll();
    }

    private class TCKResourceListenerImpl extends BaseTCKResourceListener {
        public synchronized void onSbbMessage(TCKSbbMessage message, TCKActivityID calledActivity) throws RemoteException {
            utils.getLog().info("Received message from SBB");

            HashMap map = (HashMap) message.getMessage();
            Boolean passed = (Boolean) map.get("Result");
            String msgString = (String) map.get("Message");

            if (passed.booleanValue() == true)
                result.setPassed();
            else
                result.setFailed(TEST_ID, msgString);
        }

        public void onException(Exception e) throws RemoteException {
            utils.getLog().warning("Received exception from SBB");
            utils.getLog().warning(e);
            result.setError(e);
        }
    }

    private SleeTCKTestUtils utils;
    private TCKResourceListener resourceListener;
    private FutureResult result;
    private DeployableUnitID duID;
    private ProfileUtils profileUtils;
}
