/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.javax.slee.management.ServiceDescriptor;

import javax.slee.management.*;
import javax.slee.ComponentID;
import javax.slee.ServiceID;
import javax.slee.SbbID;

import com.opencloud.sleetck.lib.*;
import com.opencloud.sleetck.lib.testutils.*;
import com.opencloud.sleetck.lib.resource.TCKSbbMessage;
import com.opencloud.sleetck.lib.resource.testapi.TCKResourceListener;
import com.opencloud.sleetck.lib.resource.TCKActivityID;
import com.opencloud.sleetck.lib.resource.testapi.TCKResourceTestInterface;
import com.opencloud.sleetck.lib.resource.events.TCKResourceEventX;

import com.opencloud.sleetck.lib.testutils.jmx.DeploymentMBeanProxy;
import com.opencloud.sleetck.lib.testutils.jmx.ServiceManagementMBeanProxy;

import com.opencloud.logging.Logable;

import java.rmi.RemoteException;
import java.util.HashMap;

public class Test3940Test implements SleeTCKTest {

    private static final String SERVICE_DU_PATH_PARAM = "DUPath";
    private static final int TEST_ID = 3940;

    public void init(SleeTCKTestUtils utils) {
        this.utils = utils;
    }

    /**
     * Perform the actual test.
     */

    public TCKTestResult run() throws Exception {

        DeploymentMBeanProxy duProxy = utils.getDeploymentMBeanProxy();

        // Get the array of ComponentID objects.
        DeployableUnitDescriptor duDesc = duProxy.getDescriptor(duID);
        ComponentID components[] = duDesc.getComponents();
        ComponentDescriptor descriptors[] = duProxy.getDescriptors(components);

        for (int i = 0; i < descriptors.length; i++) {
            if (descriptors[i] instanceof ServiceDescriptor) {

                ServiceDescriptor serviceDesc = (ServiceDescriptor) descriptors[i];

                try {
                    if (serviceDesc.getRootSbb() == null)
                        return TCKTestResult.failed(TEST_ID, "ServiceDescriptor.getRootSbb() didn't return the root SBB.");
                } catch (Exception e) {
                    return TCKTestResult.failed(TEST_ID, "ServiceDescriptor.getRootSbb() threw an exception.");
                }

                try {
                    if (serviceDesc.getAddressProfileTable() != null)
                        return TCKTestResult.failed(3943, "ServiceDescriptor.getAddressProfileTable() didn't return null.");
                } catch (Exception e) {
                    return TCKTestResult.failed(3943, "ServiceDescriptor.getAddressProfileTable() threw an exception.");
                }

                try {
                    if (serviceDesc.getResourceInfoProfileTable() != null)
                        return TCKTestResult.failed(3945, "ServiceDescriptor.getResourceInfoProfileTable() didn't return null.");
                } catch (Exception e) {
                    return TCKTestResult.failed(3945, "ServiceDescriptor.getResourceInfoProfileTable() threw an exception.");
                }

                return TCKTestResult.passed();
            }
        }

        return TCKTestResult.error("Failed to find the ServiceDescriptor for the installed root SBB.");
    }

    /**
     * Do all the pre-run configuration of the test.
     */

    public void setUp() throws Exception {

        // Install the Deployable Units
        String duPath = utils.getTestParams().getProperty(SERVICE_DU_PATH_PARAM);
        duID = utils.install(duPath);

        // Activate the DU
        utils.activateServices(duID, true);

    }

    /**
     * Clean up after the test.
     */

    public void tearDown() throws Exception {
        utils.getLog().fine("Disconnecting from resource");
        utils.getResourceInterface().clearActivities();
        utils.getResourceInterface().removeResourceListener();
        utils.getLog().fine("Deactivating and uninstalling service");
        utils.deactivateAllServices();
        utils.uninstallAll();
    }

    private SleeTCKTestUtils utils;
    private DeployableUnitID duID;
}
