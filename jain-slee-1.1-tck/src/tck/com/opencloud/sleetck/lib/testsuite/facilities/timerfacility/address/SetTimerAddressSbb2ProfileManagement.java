/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.facilities.timerfacility.address;

import javax.slee.Address;
import javax.slee.AddressPlan;
import javax.slee.profile.ProfileManagement;
import javax.slee.profile.ProfileVerificationException;

public abstract class SetTimerAddressSbb2ProfileManagement implements SetTimerAddressSbb2ProfileCMP, ProfileManagement {

    public static final Address[] defaultAddresses = { new Address(AddressPlan.E164, "111") };

    public void profileInitialize() {
        setAddresses(defaultAddresses);
    }
    
    public void profileLoad() {}
    public void profileStore() {}
    public void profileVerify() throws ProfileVerificationException {}

}
