/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.profiles.profileinitialstate;

import javax.slee.profile.ProfileManagement;
import javax.slee.profile.ProfileVerificationException;

/**
 * This profile management class will setup value in profileInitialize then copy value in profileStore to value2.
 * profileVerify will check that both values correspond to the valid value.
 */
public abstract class Test4333ProfileManagementImpl implements ProfileManagement, Test4333ProfileCMP {

    private static final String VALID_VALUE = "Test4333ValidProfile";

    public void profileInitialize() {
        setValue(VALID_VALUE);
    }

    public void profileLoad() {
    }

    public void profileStore() {
        setValue2(getValue());
    }

    public void profileVerify() throws ProfileVerificationException {
        if( !getValue().equals(VALID_VALUE) ) {
            throw new ProfileVerificationException("Field value is invalid: " + getValue());
        }
        if( !getValue2().equals(VALID_VALUE) ) {
            throw new ProfileVerificationException("Field value2 is invalid: " + getValue());
        }
    }
}
