/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.runtime.security;

import javax.naming.Context;
import javax.slee.ActivityContextInterface;
import javax.slee.profile.ProfileFacility;
import javax.slee.profile.ProfileTable;

import com.opencloud.sleetck.lib.resource.events.TCKResourceEventX;
import com.opencloud.sleetck.lib.sbbutils.BaseTCKSbb;
import com.opencloud.sleetck.lib.sbbutils.TCKSbbUtils;


public abstract class Test1112018Sbb extends BaseTCKSbb {

    public static final String PROFILE_TABLE_NAME = "Test1112018ProfileTable";
    public static final String PROFILE_NAME = "Test1112018Profile";
    
    public void onTCKResourceEventX1(TCKResourceEventX event, ActivityContextInterface aci) {
        try {
            getSbbContext().getTracer("Test1112017Sbb").fine("Received event.");
            Context env = TCKSbbUtils.getSbbEnvironment();
            ProfileFacility profileFacility = (ProfileFacility)env.lookup("slee/facilities/profile");
            ProfileTable profileTable = profileFacility.getProfileTable(PROFILE_TABLE_NAME);
            Test1112018ProfileLocal profileLocal = (Test1112018ProfileLocal)profileTable.find(PROFILE_NAME);

            profileLocal.businessMethod();
            TCKSbbUtils.getResourceInterface().sendSbbMessage("All tested permissions granted/denied as appropriate");
        } catch(Exception e) { // catch failures and other Exceptions
            TCKSbbUtils.handleException(e); // send to the test
        }
    }
}
