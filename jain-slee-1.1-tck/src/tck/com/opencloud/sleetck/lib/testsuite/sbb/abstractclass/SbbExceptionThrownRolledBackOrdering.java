/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.sbb.abstractclass;

import com.opencloud.sleetck.lib.*;
import com.opencloud.sleetck.lib.testutils.ExceptionsUtil;
import com.opencloud.sleetck.lib.testutils.BaseTCKResourceListener;
import com.opencloud.sleetck.lib.resource.TCKActivityID;
import com.opencloud.sleetck.lib.resource.testapi.TCKResourceTestInterface;
import com.opencloud.sleetck.lib.resource.testapi.TCKResourceListener;
import com.opencloud.sleetck.lib.resource.events.TCKResourceEventX;
import com.opencloud.sleetck.lib.testutils.*;
import com.opencloud.sleetck.lib.testutils.jmx.*;
import com.opencloud.sleetck.lib.resource.TCKSbbMessage;
import java.rmi.RemoteException;
import java.util.Map;

/**
 * Test that sbbRolledBack is invoked after sbbExceptionThrown.
 */

public class SbbExceptionThrownRolledBackOrdering extends AbstractSleeTCKTest {

    public void run(FutureResult result) throws Exception {
        this.result = result;

        // create an activity object
        TCKResourceTestInterface resource = utils().getResourceInterface();
        String activityName = "SbbExceptionThrownRolledBackOrdering";
        TCKActivityID activityID = resource.createActivity(activityName);

        // fire TCKResourceEventX.X1 on this activity
        utils().getLog().fine("Firing TCKResourceEventX.X1 on activity " + activityName);
        resource.fireEvent(TCKResourceEventX.X1, TCKResourceEventX.X1, activityID, null);
    }

    public void setUp() throws Exception {
        getLog().fine("Connecting to resource");
        TCKResourceListener resourceListener = new TCKResourceListenerImpl();
        setResourceListener(resourceListener);

        setupService(DescriptionKeys.SERVICE_DU_PATH_PARAM, true);
    }


    private class TCKResourceListenerImpl extends BaseTCKResourceListener {
        // TCKResourceListener methods
        public synchronized void onSbbMessage(TCKSbbMessage message, TCKActivityID calledActivity) throws RemoteException {
            Map sbbData = (Map)message.getMessage();
            if (sbbData.containsKey("sbbExceptionThrown")) {
                if (sbbExceptionThrown) {
                    result.setFailed(470, "expected only one sbbExceptionThrown invocation");
                    return;
                }
                sbbExceptionThrown = true;
            }
            else if (sbbData.containsKey("sbbRolledBack")) {
                if (sbbExceptionThrown) {
                    result.setPassed();
                }
                else {
                    result.setFailed(470, "expected sbbRolledBack to be invoked before sbbExceptionThrown");
                }
            }
        }

        public void onException(Exception exception) throws RemoteException {
            getLog().warning("Received exception from SBB or resource:");
            getLog().warning(exception);
            result.setError(exception);
        }
    }


    private FutureResult result;
    private boolean sbbExceptionThrown = false;
}
