/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.facilities.profilefacility;

import java.util.Date;

public interface Test1113032ProfileCMP {
    
    public void setX(int x);
    public int getX();

    public void setY(String t);
    public String getY();

    public void setZ(Date date);
    public Date getZ();


}
