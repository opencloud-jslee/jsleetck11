/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.management.sleestate;

import com.opencloud.sleetck.lib.TCKTestErrorException;
import com.opencloud.sleetck.lib.resource.events.TCKResourceEventX;
import com.opencloud.sleetck.lib.resource.events.TCKResourceEventY;
import com.opencloud.sleetck.lib.sbbutils.BaseTCKSbb;
import com.opencloud.sleetck.lib.sbbutils.TCKSbbUtils;

import javax.slee.ActivityContextInterface;
import javax.slee.TransactionRequiredLocalException;
import javax.slee.TransactionRolledbackLocalException;
import javax.slee.RolledBackContext;
import javax.slee.facilities.Level;
import javax.slee.nullactivity.NullActivity;
import javax.slee.nullactivity.NullActivityFactory;
import javax.slee.profile.ProfileFacility;
import javax.slee.profile.ProfileTableActivity;
import javax.slee.profile.UnrecognizedProfileTableNameException;

public abstract class CreateActivityWhileStoppingTestSbb extends BaseTCKSbb {

    /**
     * Fired to attach the Sbb to an activity, to stall the SLEE in the STOPPING state.
     */
    public void onTCKResourceEventX1(TCKResourceEventX event, ActivityContextInterface aci) {
        createTraceSafe(Level.INFO,"Received TCKResourceEventX1");
        callTest(CreateActivityWhileStoppingTestConstants.RECEIVED_X1);
    }

    /**
     * Fired to get the Sbb to attempt to create a NullActivity
     */
    public void onTCKResourceEventX2(TCKResourceEventX event, ActivityContextInterface aci) {
        createTraceSafe(Level.INFO,"Received TCKResourceEventX2");
        NullActivityFactory nullACFactory = null;
        try {
            nullACFactory = (NullActivityFactory)TCKSbbUtils.getSbbEnvironment().lookup("slee/nullactivity/factory");
            NullActivity nullAC = nullACFactory.createNullActivity();
            if(nullAC != null) {
                callTest(CreateActivityWhileStoppingTestConstants.NULL_ACTIVITY_CREATE_SUCCEEDED);
            } else {
                createTraceSafe(Level.INFO,"nullACFactory.createNullActivity() returned null (the test allows this)");
                callTest(CreateActivityWhileStoppingTestConstants.NULL_ACTIVITY_CREATED_FAILED);
            }
        } catch (TransactionRequiredLocalException trle) {
            TCKSbbUtils.handleException(new TCKTestErrorException("Caught unexpected Exception while trying to create a NullActivity in an event handler method",trle));
        } catch (Exception e) {
            createTraceSafe(Level.INFO,"Caught Exception while trying to create a NullActivity (the test allows this):"+e);
            callTest(CreateActivityWhileStoppingTestConstants.NULL_ACTIVITY_CREATED_FAILED);
        }
    }

    /**
     * Fired to get the Sbb to attempt to locate a ProfileTableActivity.
     * The event payload should be the name of the profile table.
     */
    public void onTCKResourceEventY1(TCKResourceEventY event, ActivityContextInterface aci) {
        try {
            createTraceSafe(Level.INFO,"Received TCKResourceEventY1");
            Object payload = event.getMessage();
            if(payload == null) throw new NullPointerException("The event payload of the TCKResourceEventY1 was null");
            ProfileFacility profileFacility = (ProfileFacility)TCKSbbUtils.getSbbEnvironment().lookup("slee/facilities/profile");
            // lookup the ProfileTableActivity
            String profileTableName = (String)payload;
            try {
                ProfileTableActivity tableActivity = profileFacility.getProfileTableActivity(profileTableName);
                if(tableActivity != null) {
                    callTest(CreateActivityWhileStoppingTestConstants.PROFILE_TABLE_ACTIVITY_FOUND);
                } else {
                    createTraceSafe(Level.INFO,"profileFacility.getProfileTableActivity() returned null (the test allows this)");
                    callTest(CreateActivityWhileStoppingTestConstants.PROFILE_TABLE_ACTIVITY_NOT_FOUND);
                }
            } catch (TransactionRolledbackLocalException trle) {
                TCKSbbUtils.handleException(new TCKTestErrorException("Caught unexpected Exception while trying to get a ProfileTableActivity in an event handler method",trle));
            } catch (UnrecognizedProfileTableNameException uptne) {
                TCKSbbUtils.handleException(new TCKTestErrorException("Caught unexpected Exception while trying to get a ProfileTableActivity in an event handler method",uptne));
            } catch (Exception ex) {
                createTraceSafe(Level.INFO,"Caught Exception while trying to get a ProfileTableActivity (the test allows this):"+ex);
                callTest(CreateActivityWhileStoppingTestConstants.PROFILE_TABLE_ACTIVITY_NOT_FOUND);
            }
        } catch (Exception ex) {
            TCKSbbUtils.handleException(ex);
        }
    }

    /**
     * Fired to test whether an event fired on an activity will be delivered to an Sbb
     * if fired on an activity created in the STOPPING state.
     */
    public void onTCKResourceEventY2(TCKResourceEventY event, ActivityContextInterface aci) {
        createTraceSafe(Level.INFO,"Received TCKResourceEventY2");
        callTest(CreateActivityWhileStoppingTestConstants.RECEIVED_Y2);
    }

    /**
     * Sends a synchronous message to the test, containing an Integer representing the call code.
     */
    private void callTest(int callCode) {
        try {
            TCKSbbUtils.getResourceInterface().callTest(new Integer(callCode));
        } catch (Exception ex) {
            TCKSbbUtils.handleException(ex);
        }
    }

}
