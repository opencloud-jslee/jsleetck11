/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.runtime.security;

import javax.management.ObjectName;
import javax.management.ReflectionException;
import javax.slee.profile.ProfileSpecificationID;

import com.opencloud.sleetck.lib.AbstractSleeTCKTest;
import com.opencloud.sleetck.lib.TCKTestErrorException;
import com.opencloud.sleetck.lib.TCKTestFailureException;
import com.opencloud.sleetck.lib.TCKTestResult;
import com.opencloud.sleetck.lib.infra.SleeTCKComponentConstants;
import com.opencloud.sleetck.lib.profileutils.BaseMessageAdapter;
import com.opencloud.sleetck.lib.rautils.RMIObjectChannel;
import com.opencloud.sleetck.lib.resource.TCKActivityID;
import com.opencloud.sleetck.lib.resource.events.TCKResourceEventX;
import com.opencloud.sleetck.lib.resource.testapi.TCKResourceTestInterface;
import com.opencloud.sleetck.lib.testutils.Assert;
import com.opencloud.sleetck.lib.testutils.FutureResult;
import com.opencloud.sleetck.lib.testutils.ProfileUtils;
import com.opencloud.sleetck.lib.testutils.jmx.ProfileMBeanProxy;
import com.opencloud.sleetck.lib.testutils.jmx.ProfileProvisioningMBeanProxy;


public class Test1112019Test extends AbstractSleeTCKTest {
    public static final String DU_PATH_PARAM= "serviceDUPath";
    public static final String SPEC_NAME = "Test1112019Profile";
    public static final String SPEC_VERSION = "1.1";


    public TCKTestResult run() throws Exception {
        futureResult = new FutureResult(getLog());
        getLog().fine("Run tests for profile spec "+SPEC_NAME);

        //Create a profile table
        ProfileSpecificationID specID = new ProfileSpecificationID(SPEC_NAME, SleeTCKComponentConstants.TCK_VENDOR, SPEC_VERSION);
        profileProvisioning.createProfileTable(specID, Test1112019Sbb.PROFILE_TABLE_NAME);
        getLog().fine("Added profile table "+Test1112019Sbb.PROFILE_TABLE_NAME);

        //Create a profile via management view
        profile = profileProvisioning.createProfile(Test1112019Sbb.PROFILE_TABLE_NAME, Test1112019Sbb.PROFILE_NAME);
        profileProxy = utils().getMBeanProxyFactory().createProfileMBeanProxy(profile);
        if (null == profileProxy) {
            throw new TCKTestErrorException("Could not create profile.");
        }
        profileProxy.commitProfile();
        getLog().fine("Created profile "+Test1112019Sbb.PROFILE_NAME+" for profile table "+Test1112019Sbb.PROFILE_TABLE_NAME);


        try {
            // Invoke a management method directly.
            getLog().info("Invoking profile management method directly.");
            try {
                utils().getMBeanFacade().invoke(profile, "manage", new Object[]{}, new String[]{});
                getLog().fine("Called management method on ProfileManagement interface.");
            } catch (ReflectionException e) {
                Assert.fail(Test1112019ProfileAbstract.TEST_ID, "Method not found when invoking management method declared in ProfileManagement interface.");
            }

            // Ask the SBB to invoke the management method.
            TCKResourceTestInterface resource = utils().getResourceInterface();

            getLog().fine("Creating activity");
            TCKActivityID activityID = resource.createActivity(getClass().getName());
            getLog().fine("Firing an event");
            resource.fireEvent(TCKResourceEventX.X1, null, activityID, null);

            TCKTestResult res = futureResult.waitForResult(utils().getTestTimeout());
            if (!res.isPassed())
                return res;

        } finally {
            futureResult = new FutureResult(getLog());
            profileProxy.closeProfile();
            profileProvisioning.removeProfile(Test1112019Sbb.PROFILE_TABLE_NAME, Test1112019Sbb.PROFILE_NAME);
            profileProvisioning.removeProfileTable(Test1112019Sbb.PROFILE_TABLE_NAME);
            if (futureResult.isSet())
                return futureResult.waitForResult(utils().getTestTimeout());
        }

        //finally check whether any failures or errors arrived while no FutureResult was waiting to be set.
        if (exResult!=null)
            return exResult;
        return TCKTestResult.passed();
    }

    public void setPassed(int assertionID, String msg) {
        getLog().fine(assertionID+": "+msg);
        if (futureResult!=null && !futureResult.isSet())
            futureResult.setPassed();
    }

    public void setFailed(int assertionID, String msg, Exception e) {
        if (e==null) {
            getLog().fine("FAILURE: "+assertionID+":"+msg);
            if (futureResult!=null && !futureResult.isSet())
                futureResult.setFailed(assertionID, msg);

            //in case the currently active futureResult has already been set to passed() before the failure
            //comes in, we have to provide a mechanism to still detect this failure
            setIfEmpty(TCKTestResult.failed(assertionID, msg));
        }
        else {
            getLog().fine("FAILURE: "+assertionID+":"+msg);
            getLog().fine(e);
            if (futureResult!=null && !futureResult.isSet())
                futureResult.setFailed(new TCKTestFailureException(assertionID, msg, e));

            //in case the currently active futureResult has already been set to passed() before the failure
            //comes in, we have to provide a mechanism to still detect this failure
            setIfEmpty(TCKTestResult.failed(new TCKTestFailureException(assertionID, msg, e)));
        }
    }

    public void setError(String msg, Exception e) {
        if (e==null) {
            getLog().warning(msg);
            if (futureResult!=null && !futureResult.isSet())
                futureResult.setError(msg);

            //in case the currently active futureResult has already been set to passed() before the error
            //comes in, we have to provide a mechanism to still detect this error
            setIfEmpty(TCKTestResult.error(msg));
        }
        else {
            getLog().warning(msg);
            getLog().warning(e);
            if (futureResult!=null && !futureResult.isSet())
                futureResult.setError(msg, e);

            //in case the currently active futureResult has already been set to passed() before the error
            //comes in, we have to provide a mechanism to still detect this failure
            setIfEmpty(TCKTestResult.error(msg, e));
        }
    }

    public void tearDown() throws Exception {
        in.clearQueue();

        super.tearDown();
    }

    public void setUp() throws Exception {
        profileUtils = new ProfileUtils(utils());
        profileProvisioning = profileUtils.getProfileProvisioningProxy();

        in = utils().getRMIObjectChannel();
        in.setMessageHandler(new BaseMessageAdapter(getLog()) {
            public void onSetPassed(int assertionID, String msg) { setPassed(assertionID, msg); }
            public void onSetFailed(int assertionID, String msg, Exception e) { setFailed(assertionID, msg, e); }
            public void onLogCall(String msg) { getLog().fine(msg); }
            public void onSetError(String msg, Exception e) { setError(msg, e); }

        });

        setupService(DU_PATH_PARAM);
    }

    private void setIfEmpty(TCKTestResult exResult) {
        if (this.exResult==null)
            this.exResult = exResult;
    }

    private ProfileUtils profileUtils;
    private ProfileProvisioningMBeanProxy profileProvisioning;
    private ObjectName profile;
    private ProfileMBeanProxy profileProxy;
    private RMIObjectChannel in;
    private FutureResult futureResult;
    private TCKTestResult exResult;

}
