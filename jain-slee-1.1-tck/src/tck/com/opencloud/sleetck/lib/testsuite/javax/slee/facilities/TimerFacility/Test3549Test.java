/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.javax.slee.facilities.TimerFacility;

import com.opencloud.sleetck.lib.OperationTimedOutException;
import com.opencloud.sleetck.lib.SleeTCKTest;
import com.opencloud.sleetck.lib.SleeTCKTestUtils;
import com.opencloud.sleetck.lib.TCKTestResult;
import com.opencloud.sleetck.lib.resource.TCKActivityID;
import com.opencloud.sleetck.lib.resource.events.TCKResourceEventX;
import com.opencloud.sleetck.lib.resource.testapi.TCKResourceTestInterface;
import com.opencloud.sleetck.lib.testutils.QueuingResourceListener;

import javax.slee.management.DeployableUnitID;
import java.util.HashMap;

public class Test3549Test implements SleeTCKTest {

    private static final String SERVICE_DU_PATH_PARAM = "DUPath";

    public void init(SleeTCKTestUtils utils) {
        this.utils = utils;
    }

    /**
     * Perform the actual test.
     */

    public TCKTestResult run() throws Exception {
        QueuingResourceListener listener = new QueuingResourceListener(utils);
        utils.getResourceInterface().setResourceListener(listener);

        TCKResourceTestInterface resource = utils.getResourceInterface();
        TCKActivityID activityID = resource.createActivity("Test3549InitialActivity");
        resource.fireEvent(TCKResourceEventX.X1, TCKResourceEventX.X1, activityID, null);

        Integer id = null;
        Object sbbMessage = null;
        while (true) {
            try {
                sbbMessage = listener.nextMessage().getMessage();
            } catch (OperationTimedOutException e) {
                break;
            }

            HashMap map = (HashMap) sbbMessage;
            if ((map.get("Type")).equals("TimerSeen")) {
                timerSeen++;
                id = (Integer) map.get("ID");
            }

            if ((map.get("Type")).equals("RolledBack"))
                rolledBack = true;

        }

        if (timerSeen == 0 && rolledBack == true)
            return TCKTestResult.passed();

        if (rolledBack == false)
            return TCKTestResult.error("Was unable to setup the required event and rollback.");

        return TCKTestResult.failed(id.intValue(), "Expected timer event not to be fired, but it was fired " + timerSeen + " times.");
    }

    /**
     * Do all the pre-run configuration of the test.
     */

    public void setUp() throws Exception {

        utils.getLog().fine("Installing and activating service");

        // Install the Deployable Units
        String duPath = utils.getTestParams().getProperty(SERVICE_DU_PATH_PARAM);
        duID = utils.install(duPath);

        // Activate the DU
        utils.activateServices(duID, true);

    }

    /**
     * Clean up after the test.
     */

    public void tearDown() throws Exception {
        utils.getLog().fine("Disconnecting from resource");
        utils.getResourceInterface().clearActivities();
        utils.getResourceInterface().removeResourceListener();
        utils.getLog().fine("Deactivating and uninstalling service");
        utils.deactivateAllServices();
        utils.uninstallAll();
    }

    private SleeTCKTestUtils utils;
    private DeployableUnitID duID;

    private boolean rolledBack = true;
    private int timerSeen = 0;
}
