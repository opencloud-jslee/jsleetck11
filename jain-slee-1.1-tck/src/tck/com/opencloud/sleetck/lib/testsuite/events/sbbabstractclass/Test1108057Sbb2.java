/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.events.sbbabstractclass;

import java.util.HashMap;

import javax.slee.ActivityContextInterface;
import javax.slee.EventContext;
import javax.slee.facilities.Tracer;

import com.opencloud.sleetck.lib.resource.events.TCKResourceEventX;
import com.opencloud.sleetck.lib.sbbutils.BaseTCKSbb;
import com.opencloud.sleetck.lib.sbbutils.TCKSbbUtils;

/**
 * AssertionID(1108057): Test If the service argument is not null then only new root 
 * SBB entities belonging to the specified Service are eligible to be initiated by 
 * the event.
 */
public abstract class Test1108057Sbb2 extends BaseTCKSbb {

    // Initial event method.
    public void onTCKResourceEventX1(TCKResourceEventX event, ActivityContextInterface aci, EventContext context) {
        try {
            tracer = getSbbContext().getTracer("com.test");
            tracer.info("Sbb2: Received a TCK event " + event);
        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }
    }

    public void onTest1108057Event(Test1108057Event event, ActivityContextInterface aci, EventContext context) {
        try {
            tracer = getSbbContext().getTracer("com.test");
            tracer.info("Sbb2: Received a first custom event " + event);
            sendResultToTCK(1108057, "Sbb2 fired an event to a particular target Service1 only, "
                    + "hence there should be no event receives in this Service2. But Sbb2 received the "
                    + "first custom event " + event + " fired by Sbb1!", "Test1108057Test", false);
            return;
        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }
    }

    public void onTCKResourceEventX2(TCKResourceEventX event, ActivityContextInterface aci, EventContext context) {
        try {
            tracer = getSbbContext().getTracer("com.test");
            tracer.info("Sbb2: Received a TCK event " + event);
        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }
    }

    public void onTest1108057SecondEvent(Test1108057SecondEvent event, ActivityContextInterface aci,
            EventContext context) {
        try {
            tracer = getSbbContext().getTracer("com.test");
            tracer.info("Sbb2: Received a second custom event " + event);
            sendResultToTCK(1108057, "Sbb2 fired an event to a particular target Service1 only, "
                    + "hence there should be no event receives in this Service2. But Sbb2 received the "
                    + "second custom event " + event + " fired by Sbb1!", "Test1108057Test", false);
            return;
        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }
    }

    private void sendResultToTCK(int failedAssertionID, String message, String testName, boolean result) throws Exception {
        HashMap sbbData = new HashMap();
        sbbData.put("testname", testName);
        sbbData.put("result", result ? "pass" : "fail");
        sbbData.put("message", message);
        sbbData.put("id", new Integer(failedAssertionID));
        TCKSbbUtils.getResourceInterface().sendSbbMessage(sbbData);
    }

    private Tracer tracer;
}
