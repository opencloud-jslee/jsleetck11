/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.facilities.timerfacility;

import java.util.HashMap;

import javax.naming.InitialContext;
import javax.slee.ActivityContextInterface;
import javax.slee.facilities.TimerFacility;
import javax.slee.facilities.Tracer;

import com.opencloud.sleetck.lib.resource.events.TCKResourceEventX;
import com.opencloud.sleetck.lib.sbbutils.BaseTCKSbb;
import com.opencloud.sleetck.lib.sbbutils.TCKSbbUtils;

/**
 * AssertionID (1113003): Test The JNDI_NAME constant. This constant specifies
 * the JNDI location where a TimerFacility object may be located by an SBB
 * component in its component environment.
 * 
 * AssertionID (1113094): Test the value of this constant is 
 * "java:comp/env/slee/facilities/timer".
 */
public abstract class Test1113003Sbb extends BaseTCKSbb {

    public void onTCKResourceEventX1(TCKResourceEventX event, ActivityContextInterface aci) {

        try {
            tracer = getSbbContext().getTracer("com.test");
            tracer.info("Sbb: Received a TCK event " + event);

            doJNDITest();
            } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }
    }

    private void sendResultToTCK(String testName, boolean result, int failedAssertionID,  String message) throws Exception {
        HashMap sbbData = new HashMap();
        sbbData.put("testname", testName);
        sbbData.put("result", result ? "pass" : "fail");
        sbbData.put("message", message);
        sbbData.put("id", new Integer(failedAssertionID));
        TCKSbbUtils.getResourceInterface().sendSbbMessage(sbbData);
    }

    private void doJNDITest() throws Exception {
        TimerFacility tf = getTimerFacility();
        if (tf == null) {
            sendResultToTCK("Test1113003Test", false, 1113003, "Could not find TimerFacility object in JNDI at " + TimerFacility.JNDI_NAME);
            return;
        }
        tracer.info("got expected TimerFacility object in JNDI at " + TimerFacility.JNDI_NAME, null);

        ///1113094
        String JNDI_ACNF_NAME = TimerFacility.JNDI_NAME;
        if (JNDI_ACNF_NAME != "java:comp/env/slee/facilities/timer") {
            sendResultToTCK("Test1113003Test", false, 1113094, "The value of this JNDI_NAME is not equal to java:comp/env/slee/facilities/timer");
            return;
        }
        tracer.info("got expected value of this TimerFacility.JNDI_NAME", null);

        sendResultToTCK("Test1113003Test", true, 1113003,"TimerFacility.JNDI_NAME tests passed");
    }

    private TimerFacility getTimerFacility() throws Exception {
        TimerFacility tf = null;
        String JNDI_TIMERFACILITY_NAME = TimerFacility.JNDI_NAME;
        try {
            tf = (TimerFacility) new InitialContext().lookup(JNDI_TIMERFACILITY_NAME);
        } catch (Exception e) {
            tracer.warning("got unexpected Exception: " + e, null);
        }
        return tf;
    }

    private Tracer tracer;
}
