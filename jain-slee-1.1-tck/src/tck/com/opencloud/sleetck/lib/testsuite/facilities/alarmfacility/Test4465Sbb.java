/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.facilities.alarmfacility;

import com.opencloud.sleetck.lib.resource.events.TCKResourceEventX;
import com.opencloud.sleetck.lib.sbbutils.BaseTCKSbb;
import com.opencloud.sleetck.lib.sbbutils.TCKSbbUtils;

import javax.naming.InitialContext;
import javax.slee.ActivityContextInterface;
import javax.slee.facilities.AlarmFacility;
import javax.slee.facilities.Level;


public abstract class Test4465Sbb extends BaseTCKSbb {

    private static final String JNDI_ALARMFACILITY_NAME = "java:comp/env/slee/facilities/alarm";
    public static final String ALARM_MESSAGE = "Test4465AlarmMessage";

    // Initial event
    public void onTCKResourceEventX1(TCKResourceEventX ev, ActivityContextInterface aci) {

        try {
            AlarmFacility facility = (AlarmFacility) new InitialContext().lookup(JNDI_ALARMFACILITY_NAME);
            facility.createAlarm(getSbbID(), Level.INFO, "javax.slee.management.alarm", ALARM_MESSAGE, System.currentTimeMillis());

        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }

    }

}
