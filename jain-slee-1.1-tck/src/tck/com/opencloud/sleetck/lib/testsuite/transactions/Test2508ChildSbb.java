/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.transactions;

import com.opencloud.sleetck.lib.TCKTestErrorException;
import com.opencloud.sleetck.lib.sbbutils.BaseTCKSbb;
import com.opencloud.sleetck.lib.sbbutils.TCKSbbUtils;

import javax.slee.ActivityContextInterface;
import javax.slee.facilities.Level;

public abstract class Test2508ChildSbb extends BaseTCKSbb {

    public static final String EXCEPTION_MESSAGE = "Test2508RuntimeException";

    public void sbbExceptionThrown(Exception exception, Object event, ActivityContextInterface aci) {
        createTraceSafe(Level.INFO,"Test2508ChildSbb:sbbExceptionThrown()");

        if (exception instanceof java.lang.RuntimeException && exception.getMessage().equals(EXCEPTION_MESSAGE)) {
            createTraceSafe(Level.INFO, "sbbExceptionThrown was invoked on the child SBB with the "+
                        "RuntimeException thrown from a local interface method");
            sendSbbMessageSafe(Test2508Constants.SBB_EXCEPTION_THROWN_CHILD);
        } else {
            TCKSbbUtils.handleException(new TCKTestErrorException("sbbExceptionThrown() invoked for an unexpected exception. event="+event+";aci="+aci,exception));
        }
    }

    public void sbbRolledBack(javax.slee.RolledBackContext context) {
        createTraceSafe(Level.INFO,"Test2508ChildSbb:sbbRolledBack()");
        sendSbbMessageSafe(Test2508Constants.SBB_ROLLED_BACK_CHILD);
    }

    public void throwRuntimeException() {
        throw new RuntimeException(EXCEPTION_MESSAGE);
    }

    private void sendSbbMessageSafe(String message) {
        try {
            TCKSbbUtils.getResourceInterface().sendSbbMessage(message);
        } catch (Exception ex) {
            TCKSbbUtils.handleException(ex);
        }
    }

}
