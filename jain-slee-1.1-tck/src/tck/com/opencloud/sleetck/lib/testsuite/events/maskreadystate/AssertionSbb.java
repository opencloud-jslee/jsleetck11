/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.events.maskreadystate;

import com.opencloud.sleetck.lib.TCKTestResult;
import com.opencloud.sleetck.lib.resource.events.TCKResourceEventX;
import com.opencloud.sleetck.lib.sbbutils.BaseTCKSbb;
import com.opencloud.sleetck.lib.sbbutils.TCKSbbUtils;

import javax.slee.ActivityContextInterface;
import javax.slee.CreateException;
import javax.slee.facilities.Level;
import javax.slee.profile.ProfileAddedEvent;
import javax.slee.profile.ProfileFacility;
import javax.slee.profile.ProfileTableActivity;
import javax.slee.profile.ProfileTableActivityContextInterfaceFactory;

public abstract class AssertionSbb extends BaseTCKSbb{

    public static final String PROFILE_TABLE_NAME = "MaskReadyStateTest-ProfileTable";
    private static final int TEST_ID = 713;

    public void sbbCreate() throws CreateException {
        createTraceSafe(Level.INFO,"AssertionSbb:sbbCreate()");
        try{
            ProfileFacility profileFacility = (ProfileFacility)TCKSbbUtils.getSbbEnvironment().lookup("slee/facilities/profile");
            ProfileTableActivityContextInterfaceFactory profileTableACIFactory =
                    (ProfileTableActivityContextInterfaceFactory)TCKSbbUtils.getSbbEnvironment().lookup("slee/facilities/profiletableactivitycontextinterfacefactory");
            ProfileTableActivity profileTableActivity = profileFacility.getProfileTableActivity(PROFILE_TABLE_NAME);
            ActivityContextInterface profileTableACI = profileTableACIFactory.getActivityContextInterface(profileTableActivity);
            createTraceSafe(Level.INFO,"AssertionSbb:calling maskEvent() from the sbbCreate() method. (Expecting an IllegalStateException)");
            getSbbContext().maskEvent(new String[]{"ProfileAddedEvent"}, profileTableACI);

            String failureReason = "SLEE did not throw IllegalStateException when an SBB with no assigned entity invoked maskEvent on its own SbbContext";
            createTraceSafe(Level.INFO,"Test failed: "+failureReason);
            sendTestResult(TCKTestResult.failed(TEST_ID,failureReason));

        } catch( IllegalStateException ise ){
            createTraceSafe(Level.INFO,"Test passed");
            sendTestResult(TCKTestResult.passed());
        } catch( Exception e ){
            TCKSbbUtils.handleException( e );
        }
    }

    public void sbbExceptionThrown( Exception exception, Object event, ActivityContextInterface aci ){
    }

    public void sbbRolledBack(javax.slee.RolledBackContext context) {
    }

    public void onTCKResourceEventX1(TCKResourceEventX event, ActivityContextInterface aci) {
        // no-op -- implemented in name only
    }

    public void onProfileAddedEvent(ProfileAddedEvent event, ActivityContextInterface aci) {
        // no-op -- implemented in name only
    }

    private void sendTestResult(TCKTestResult result) {
        try{
            TCKSbbUtils.getResourceInterface().sendSbbMessage(result.toExported());
        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }
    }

}
