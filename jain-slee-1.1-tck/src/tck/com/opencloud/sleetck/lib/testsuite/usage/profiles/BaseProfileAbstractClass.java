/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.usage.profiles;

import javax.slee.CreateException;
import javax.slee.profile.ProfileVerificationException;

public abstract class BaseProfileAbstractClass implements BaseProfileCmp, javax.slee.profile.Profile {
    // PARAM_SET_NAME also defined in BaseProfileTest.
    public static final String PARAM_SET_NAME = "FooParameterSet";
    
    public void profileStore() {}
    public void profilePostCreate() throws CreateException {}
    public void profileActivate() {}
    public void profileInitialize() {}
    public void profileVerify() throws ProfileVerificationException {}
    public void setProfileContext(javax.slee.profile.ProfileContext p) {}
    public void profileRemove() {}
    public void profilePassivate() {}
    public void profileLoad() {}
    public void unsetProfileContext() {}
}
