/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.sbb.abstractclass;

import javax.slee.ActivityEndEvent;
import javax.slee.ActivityContextInterface;
import javax.slee.SbbContext;
import javax.slee.ChildRelation;
import javax.slee.Address;

import com.opencloud.sleetck.lib.sbbutils.BaseTCKSbb;
import com.opencloud.sleetck.lib.sbbutils.TCKSbbUtils;
import com.opencloud.sleetck.lib.sbbutils.TCKSbbConstants;
import com.opencloud.sleetck.lib.resource.events.TCKResourceEventX;
import com.opencloud.sleetck.lib.resource.TCKActivityID;
import com.opencloud.sleetck.lib.resource.sbbapi.TCKResourceSbbInterface;
import com.opencloud.sleetck.lib.resource.sbbapi.TCKActivityContextInterfaceFactory;

import java.util.HashMap;

public abstract class Test2424ChildSbb extends BaseTCKSbb {

    public void onTest2424Event(Test2424Event event, ActivityContextInterface aci) {
        try {
            HashMap map = new HashMap();

            if (!getMode()) {
                asSbbActivityContextInterface(aci).setValue(42);
                map.put("Type", "FireEvent");
            }
            else {
                if (asSbbActivityContextInterface(aci).getValue() != 42) {
                    map.put("Result", Boolean.FALSE);
                    map.put("Message", "State stored in an Activity Context was not persistent for the lifetime of the Activity Context.");
                } else {
                    map.put("Result", Boolean.TRUE);
                }
            }

            TCKSbbUtils.getResourceInterface().sendSbbMessage(map);

        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }

    }


    public abstract Test2424ChildSbbActivityContextInterface asSbbActivityContextInterface(ActivityContextInterface aci);

    public void setChildMode(boolean mode) {
        setMode(mode);
    }


    public abstract void setMode(boolean mode);
    public abstract boolean getMode();

}
