/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.management.ServiceManagementMBean;

import javax.slee.management.*;
import javax.slee.ComponentID;
import javax.slee.ServiceID;
import javax.slee.InvalidArgumentException;

import com.opencloud.sleetck.lib.*;
import com.opencloud.sleetck.lib.testutils.*;
import com.opencloud.sleetck.lib.resource.TCKSbbMessage;
import com.opencloud.sleetck.lib.resource.testapi.TCKResourceListener;
import com.opencloud.sleetck.lib.resource.TCKActivityID;
import com.opencloud.sleetck.lib.resource.testapi.TCKResourceTestInterface;
import com.opencloud.sleetck.lib.resource.events.TCKResourceEventX;

import com.opencloud.sleetck.lib.testutils.jmx.DeploymentMBeanProxy;
import com.opencloud.sleetck.lib.testutils.jmx.ServiceManagementMBeanProxy;

import com.opencloud.logging.Logable;

import java.rmi.RemoteException;
import java.util.HashMap;

public class Test2470Test implements SleeTCKTest {

    private static final String SERVICE_DU_PATH_PARAM = "DUPath";
    private static final int TEST_ID = 2470;

    public void init(SleeTCKTestUtils utils) {
    this.utils = utils;
    }

    /**
     * Perform the actual test.
     */

    public TCKTestResult run() throws Exception {

    DeploymentMBeanProxy duProxy = utils.getDeploymentMBeanProxy();
    serviceProxy = utils.getMBeanProxyFactory().createServiceManagementMBeanProxy(utils.getSleeManagementMBeanProxy().getServiceManagementMBean());
    DeployableUnitDescriptor duDesc = duProxy.getDescriptor(duID);
    ComponentID components[] = duDesc.getComponents();

    // Get an array of the valid Services
    int index = 0;
    for (int i = 0; i < components.length; i++) {
        if (components[i] instanceof ServiceID) {
        services[index++] = (ServiceID) components[i];
        }
    }

    utils.getLog().fine("Checking service states.");

    // Check that the services are not active
    for (int i = 0; i < services.length; i++) {
        if (!serviceProxy.getState(services[i]).isInactive()) {
        return TCKTestResult.failed(TEST_ID, "Services are already active - cannot continue test.");
        }
    }

    utils.getLog().fine("Activating via array method.");

    // Activate via array method
    try {
        serviceProxy.activate(services);
    } catch (Exception e) {
        return TCKTestResult.error(e);
    }

    utils.getLog().fine("Checking service states.");

    // Check that the services are active
    for (int i = 0; i < services.length; i++) {
        if (serviceProxy.getState(services[i]).isInactive()) {
        return TCKTestResult.failed(1617, "Failed to correctly activate services.");
        }
    }

    utils.getLog().fine("Activating via array method (should fail)");

    // Activate via array method - should fail
    boolean passed = false;
    try {
        serviceProxy.activate(services);
    } catch (javax.slee.InvalidStateException e) {
        passed = true;
    } catch (Exception e) {
        utils.getLog().fine("Exception thrown: " + e);
        return TCKTestResult.error(e);
    }

    if (passed == false)
        return TCKTestResult.failed(1618, "ServiceManagementMBean.activate() was permitted for active services");

    utils.getLog().fine("Deactivating services for next batch of tests.");

    serviceProxy.deactivate(services[0]);
    serviceProxy.deactivate(services[1]);

    // Wait until the services are in the INACTIVE state.
    waitForStateChange(services[0], ServiceState.INACTIVE);
    waitForStateChange(services[1], ServiceState.INACTIVE);

    utils.getLog().fine("Trying to activate invalid services.");

    passed = false;
    try {
        serviceProxy.activate(new ServiceID[] {});
    } catch (InvalidArgumentException e) {
        passed = true;
    } catch (Exception e) {
        utils.getLog().fine("Exception thrown: " + e);
        return TCKTestResult.error(e);
    }

    if (!passed)
        return TCKTestResult.failed(TEST_ID, "ServiceManagementMBean.activate(new ServiceID[] {}) should have thrown InvalidArgumentException, it didn't.");

    utils.getLog().fine("Trying to activate services with null in.");

    passed = false;
    try {
        serviceProxy.activate(new ServiceID[] {services[0], null});
    } catch (InvalidArgumentException e) {
        passed = true;
    } catch (Exception e) {
        utils.getLog().fine("Exception thrown: " + e);
        return TCKTestResult.error(e);
    }
    if (!passed)
        return TCKTestResult.failed(TEST_ID, "ServiceManagementMBean.activate(new ServiceID[] {ServiceID, null}) should have thrown InvalidArgumentException, it didn't.");

    utils.getLog().fine("Trying to activate duplicate services.");

    passed = false;
    try {
        serviceProxy.activate(new ServiceID[] {services[0], services[1], services[0]});
    } catch (InvalidArgumentException e) {
        passed = true;
    } catch (Exception e) {
        utils.getLog().fine("Exception thrown: " + e);
        return TCKTestResult.error(e);
    }
    if (!passed)
        return TCKTestResult.failed(TEST_ID, "ServiceManagementMBean.activate(new ServiceID[] {ServiceID_1, ServiceID_2, ServiceID_1}) should have thrown InvalidArgumentException, it didn't.");

    utils.getLog().fine("Returning test pass.");

    return TCKTestResult.passed();
    }

    private void waitForStateChange(ServiceID serviceID, ServiceState expectedState) throws Exception {

    int count = 0;

    while (true) {
        if (serviceProxy.getState(serviceID).equals(expectedState))
        return;

        if (count > 10)
        throw new OperationTimedOutException("Timeout waiting for service " + serviceID + " to enter state " + expectedState);

        // Don't catch the InterruptedException here - let it propagate.
        Thread.sleep(500);
        count++;
    }

    }

    /**
     * Do all the pre-run configuration of the test.
     */

    public void setUp() throws Exception {

    utils.getLog().fine("Connecting to resource");
    resourceListener = new TCKResourceListenerImpl();
    utils.getResourceInterface().setResourceListener(resourceListener);
    utils.getLog().fine("Installing and activating service");

    // Install the Deployable Units
    String duPath = utils.getTestParams().getProperty(SERVICE_DU_PATH_PARAM);
    duID = utils.install(duPath);
    }

    /**
     * Clean up after the test.
     */

    public void tearDown() throws Exception {
    utils.getLog().fine("Disconnecting from resource");
        utils.getResourceInterface().clearActivities();
        utils.getResourceInterface().removeResourceListener();
        utils.getLog().fine("Deactivating and uninstalling service");

    // Manually deactivate, since deactivateAllServices only deactivates
    // those that activateAllServices() activated.
    try {
        if (serviceProxy.getState(services[0]).isActive())
        serviceProxy.deactivate(services[0]);
    } catch (Exception e) {
    }
    try {
        if (serviceProxy.getState(services[1]).isActive())
        serviceProxy.deactivate(services[1]);
    } catch (Exception e) {
    }
        utils.deactivateAllServices();
        utils.uninstallAll();
    }

    private class TCKResourceListenerImpl extends BaseTCKResourceListener {
    public synchronized void onSbbMessage(TCKSbbMessage message, TCKActivityID calledActivity) throws RemoteException {
        utils.getLog().info("Received message from SBB");

        HashMap map = (HashMap) message.getMessage();
        Boolean passed = (Boolean) map.get("Result");
        String msgString = (String) map.get("Message");

        if (passed.booleanValue() == true)
        result.setPassed();
        else
        result.setFailed(TEST_ID, msgString);
    }

    public void onException(Exception e) throws RemoteException {
        utils.getLog().warning("Received exception from SBB");
        utils.getLog().warning(e);
        result.setError(e);
    }
    }

    private ServiceManagementMBeanProxy serviceProxy;
    private SleeTCKTestUtils utils;
    private TCKResourceListener resourceListener;
    private FutureResult result;
    private DeployableUnitID duID;
    private ServiceID services[] = new ServiceID[2];
}
