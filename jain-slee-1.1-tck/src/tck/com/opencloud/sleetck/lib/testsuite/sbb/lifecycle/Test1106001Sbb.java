/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.sbb.lifecycle;

import java.util.HashMap;

import javax.slee.ActivityContextInterface;
import javax.slee.CreateException;
import javax.slee.RolledBackContext;
import javax.slee.SbbContext;
import javax.slee.SbbID;
import javax.slee.ServiceID;

import com.opencloud.sleetck.lib.TCKTestErrorException;
import com.opencloud.sleetck.lib.TCKTestResult;
import com.opencloud.sleetck.lib.resource.events.TCKResourceEventX;
import com.opencloud.sleetck.lib.sbbutils.BaseTCKSbb;
import com.opencloud.sleetck.lib.sbbutils.TCKSbbUtils;

public abstract class Test1106001Sbb extends BaseTCKSbb {

    /*
     ***************************************************
     * Lifecycle methods.
     ***************************************************
     *
     *
     * Some method invocations here are commented out because 
     * the listener is no longer valid when they are invoked.
     * A later revision of this test will use an RMI object
     * channel instead.
     */

    /**
     * This implementation sends the exception to the test via the TCK resource
     */
    public void sbbExceptionThrown(Exception exception, Object event, ActivityContextInterface aci) {
        TCKSbbUtils.handleException(new TCKTestErrorException("sbbExceptionThrown() called. event="+event+";aci="+aci,exception));
    }

    /**
     * This implementation sends an exception to the test via the TCK resource
     */
    public void sbbRolledBack(RolledBackContext context) {
        TCKSbbUtils.handleException(new TCKTestErrorException("Unexpected roll back. event="+context.getEvent()+";aci="+context.getActivityContextInterface()));
    }

    /**
     * 
     * setSbbContext is called when the SBB does the transition:
     * Does not exist  -->  Pooled
     *     
     * This implementation sets the sbb context instance variable,
     * which can be accessed via getSbbContext().
     * 
     * 
     */
    public void setSbbContext(SbbContext context) {
        this.context=context;

        // Assertion 1106006
        mySbbId = context.getSbb();
        myServiceId = context.getService();
        
        checkSbbAndService("setSbbContext");
    }

    /**
     * 
     * unsetSbbContext() is called when the SBB does the transition:
     * Pooled --> Does not exist.
     * 
     * 
     * This implementation unsets the sbb context instance variable.
     */
    public void unsetSbbContext()  {
        //checkSbbAndService("unsetSbbContext");
        context = null;
    }

    /**
     * 
     * sbbCreate is called when the SBB does the transition:
     * Pooled   -->   Ready
     *  
     */
    public void sbbCreate() throws CreateException  {
        checkSbbAndService("sbbCreate");
    }

    /**
     * checkSbbAndService()
     * sbbPostCreate is called immediately after sbbCreate
     *  
     */
    public void sbbPostCreate() throws CreateException  {
        checkSbbAndService("sbbPostCreate");
    }

    /**
     * 
     * sbbActivate is called when the SBB does the transition:
     * Pooled   -->   Ready
     *  
     */
    public void sbbActivate()  {
        checkSbbAndService("sbbActivate");
    }

    /**
     * 
     * sbbPassivate is called when the SBB does the transition:
     * Ready --> Pooled
     *  
     */
    public void sbbPassivate() {
        checkSbbAndService("sbbPassivate");
    }


    /**
     * 
     * sbbLoad

 is called when the SBB does the transition:
     * Ready   -->   Ready
     *  
     */
    public void sbbLoad() {
        checkSbbAndService("sbbLoad");
    }

    /**
     * 
     * setStore is called when the SBB does the transition:
     * Ready   -->   Ready
     *  
     */
    public void sbbStore() {
        //checkSbbAndService("sbbStore");
    }


    /**
     * 
     * setRemove is called when the SBB does the transition:
     * Ready  -->  Pooled
     *  
     */
    public void sbbRemove() {
        //checkSbbAndService("sbbRemove");
    }


    /*    
     ***************************************************
     * Event handlers
     ***************************************************
     */
    public void onTCKResourceEventX1(TCKResourceEventX ev, ActivityContextInterface aci) {
        try {
            // do nothing.
            TCKSbbUtils.getResourceInterface().sendSbbMessage(null);

        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }

    }
    
    /*
     ***************************************************
     * Utility methods
     ***************************************************
     */
    
    private void checkSbbAndService(String methodName) {
        TCKTestResult result = null;
        HashMap message = new HashMap();
        if (mySbbId != context.getSbb()) {
            result = TCKTestResult.failed(1106006, "SBB Context returned a different SBB!");
        }
        if (myServiceId != context.getService()) {
            result = TCKTestResult.failed(1106006, "SBB Context returned a different Service!");
        }
        message.put("TCKResult", result); // may be null.
        message.put("Method", methodName);
        
        try {
            TCKSbbUtils.getResourceInterface().sendSbbMessage(message);
        } catch (Exception e) {
            TCKSbbUtils.handleException(e);  // probably won't work?
        }
    }
        
    
    private SbbID mySbbId;
    private ServiceID myServiceId;
    private SbbContext context;
}
