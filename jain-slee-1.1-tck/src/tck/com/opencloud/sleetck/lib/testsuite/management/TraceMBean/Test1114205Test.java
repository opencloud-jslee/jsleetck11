/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.management.TraceMBean;

import java.rmi.RemoteException;
import java.util.Map;

import javax.management.Notification;
import javax.management.NotificationListener;
import javax.management.ObjectName;
import javax.slee.ComponentID;
import javax.slee.SbbID;
import javax.slee.ServiceID;
import javax.slee.facilities.TraceLevel;
import javax.slee.management.DeployableUnitDescriptor;
import javax.slee.management.DeployableUnitID;
import javax.slee.management.SbbNotification;
import javax.slee.management.TraceNotification;

import com.opencloud.sleetck.lib.AbstractSleeTCKTest;
import com.opencloud.sleetck.lib.DescriptionKeys;
import com.opencloud.sleetck.lib.TCKTestErrorException;
import com.opencloud.sleetck.lib.TCKTestFailureException;
import com.opencloud.sleetck.lib.TCKTestResult;
import com.opencloud.sleetck.lib.resource.TCKActivityID;
import com.opencloud.sleetck.lib.resource.TCKSbbMessage;
import com.opencloud.sleetck.lib.resource.events.TCKResourceEventX;
import com.opencloud.sleetck.lib.resource.testapi.TCKResourceListener;
import com.opencloud.sleetck.lib.resource.testapi.TCKResourceTestInterface;
import com.opencloud.sleetck.lib.testutils.Assert;
import com.opencloud.sleetck.lib.testutils.BaseTCKResourceListener;
import com.opencloud.sleetck.lib.testutils.FutureResult;
import com.opencloud.sleetck.lib.testutils.jmx.DeploymentMBeanProxy;
import com.opencloud.sleetck.lib.testutils.jmx.SleeManagementMBeanProxy;
import com.opencloud.sleetck.lib.testutils.jmx.TraceMBeanProxy;
import com.opencloud.sleetck.lib.testutils.jmx.impl.TraceMBeanProxyImpl;

/*
 * AssertionID (1114205): Test if the specified trace level is equal
 * or above the trace filter level set, the Trace Facility will accept
 * the trace message and propagate the trace message by emitting a
 * TraceNotification object from a TraceMBean object.
 *
 */
public class Test1114205Test extends  AbstractSleeTCKTest {

    /**
     * Perform the actual test.
     */
    public void run(FutureResult result) throws Exception {
        this.result = result;

        if (getTestName().equals("SEVERE")) {
            expectedTraceNotifications = 1;
            receivedTraceNotifications = 0;
            doTest1114205Test(TraceLevel.SEVERE);
        }

        else if (getTestName().equals("WARNING")) {
            expectedTraceNotifications = 3;
            receivedTraceNotifications = 0;
            doTest1114205Test(TraceLevel.WARNING);
        }
        else if (getTestName().equals("INFO")) {
            expectedTraceNotifications = 6;
            receivedTraceNotifications = 0;
            doTest1114205Test(TraceLevel.INFO);
        }
        else if (getTestName().equals("CONFIG")) {
            expectedTraceNotifications = 10;
            receivedTraceNotifications = 0;
            doTest1114205Test(TraceLevel.CONFIG);
        }
        else if (getTestName().equals("FINE")) {
            expectedTraceNotifications = 15;
            receivedTraceNotifications = 0;
            doTest1114205Test(TraceLevel.FINE);
        }
        else if (getTestName().equals("FINER")) {
            expectedTraceNotifications = 21;
            receivedTraceNotifications = 0;
            doTest1114205Test(TraceLevel.FINER);
        }
        else if (getTestName().equals("FINEST")) {
            expectedTraceNotifications = 28;
            receivedTraceNotifications = 0;
            doTest1114205Test(TraceLevel.FINEST);
        }

        synchronized (this) {
            wait(utils().getTestTimeout());
        }

        if (expectedTraceNotifications != receivedTraceNotifications)
            result.setFailed(1114205, "Expected number of trace messages not received, traces were not delivered by " +
                    "the TraceFacility (expected " + expectedTraceNotifications + ", received " + receivedTraceNotifications + ")");
    }

    public void doTest1114205Test(TraceLevel traceLevel) throws Exception {

        if(sbbID == null) throw new TCKTestErrorException("sbbID not found for "+DescriptionKeys.SERVICE_DU_PATH_PARAM);
        if(serviceID == null) throw new TCKTestErrorException("serviceID not found for "+DescriptionKeys.SERVICE_DU_PATH_PARAM);

        TraceMBeanProxy traceMBeanProxy = utils().getMBeanProxyFactory().createTraceMBeanProxy(utils().getSleeManagementMBeanProxy().getTraceMBean());
        sbbNotification = new SbbNotification(serviceID, sbbID);

        try {
            getLog().fine("Starting to test The trace level of a particular tracer " +
                        "is equal to the trace level assigned to it by an Administrator " +
                        "using a TraceMBean object.");
            traceMBeanProxy.setTraceLevel(sbbNotification, "com.foo", traceLevel);
        }
        catch (Exception e) {
            getLog().warning(e);
            result.setError("ERROR setTraceLevel ", e);
        }

        String activityName = "Test1114205Test";
        TCKResourceTestInterface resource = utils().getResourceInterface();
        TCKActivityID activityID = resource.createActivity(activityName);
        getLog().info("Firing event: " + testName );

        // kickoff the test
        resource.fireEvent(TCKResourceEventX.X1, testName, activityID, null);
    }


    /**
     * Do all the pre-run configuration of the test.
     */

    public void setUp() throws Exception {

        getLog().fine("Connecting to resource");
        getLog().fine("Installing and activating service");

        resourceListener = new TCKResourceListenerImpl();
        setResourceListener(resourceListener);

        // Install the Deployable Units
        String duPath = utils().getTestParams().getProperty(DescriptionKeys.SERVICE_DU_PATH_PARAM);
        duID = utils().install(duPath);

        // Activate the DU
        utils().activateServices(duID, true);

        // Setup TraceNotificationListener
        SleeManagementMBeanProxy proxy = utils().getSleeManagementMBeanProxy();
        traceMBeanName = proxy.getTraceMBean();
        listener = new TraceNotificationListenerImpl();

        tracembean = new TraceMBeanProxyImpl(traceMBeanName, utils().getMBeanFacade());
        tracembean.addNotificationListener(listener, null, null);

        DeploymentMBeanProxy duProxy = utils().getDeploymentMBeanProxy();
        DeployableUnitDescriptor duDesc = duProxy.getDescriptor(duID);
        ComponentID components[] = duDesc.getComponents();
        // Get the SbbID from the components.
        for (int i = 0; i < components.length; i++) {
            if (components[i] instanceof ServiceID) {
                getLog().fine("Setting serviceID value.");
                serviceID = (ServiceID) components[i];
                continue;
            }

            if (components[i] instanceof SbbID) {
                getLog().fine("Setting sbbID value.");
                sbbID = (SbbID) components[i];
                continue;
            }
        }

    }
    /**
     * Clean up after the test.
     */

    public void tearDown() throws Exception {
        if (null != tracembean)
            tracembean.removeNotificationListener(listener);
        // cleanup
        super.tearDown();
    }

    public String getTestName() {
        String testName = "testName";
        return utils().getTestParams().getProperty(testName);
    }

    private class TCKResourceListenerImpl extends BaseTCKResourceListener {
        // TCKResourceListener methods

        public synchronized void onSbbMessage(TCKSbbMessage message, TCKActivityID calledActivity) throws RemoteException {

            Map sbbData = (Map)message.getMessage();
            String sbbTestName = "Test1114205Test";
            String sbbTestResult = (String)sbbData.get("result");
            String sbbTestMessage = (String)sbbData.get("message");
            int assertionID = ((Integer)sbbData.get("id")).intValue();
            getLog().info("Received message from SBB: testname="+ sbbTestName+", result="+sbbTestResult+", message="+sbbTestMessage+", id="+assertionID);
            try {
                Assert.assertEquals(assertionID,"Test " + sbbTestName + " setFailed.", "pass", sbbTestResult);
                result.setPassed();
            } catch (TCKTestFailureException ex) {
                result.setFailed(ex);
            }
        }

        public void onException(Exception exception) throws RemoteException {
            getLog().warning("Received Exception from SBB or resource:");
            getLog().warning(exception);
            result.setError(exception);
        }
    }

    public class TraceNotificationListenerImpl implements NotificationListener {
        public final void handleNotification(Notification notification, Object handback) {

        if (notification instanceof TraceNotification)  {
            TraceNotification traceNotification = (TraceNotification) notification;

            if (!traceNotification.getNotificationSource().equals(sbbNotification))
                return;

            getLog().fine("Received Trace Notification from: " + traceNotification);

            if (traceNotification.getMessage().equals(Test1114205Sbb.TRACE_MESSAGE_SEVERE)) {
                receivedTraceNotifications = receivedTraceNotifications + TraceLevel.LEVEL_SEVERE;
            }
            else if (traceNotification.getMessage().equals(Test1114205Sbb.TRACE_MESSAGE_WARNING)) {
                receivedTraceNotifications = receivedTraceNotifications + TraceLevel.LEVEL_WARNING;
            }
            else if (traceNotification.getMessage().equals(Test1114205Sbb.TRACE_MESSAGE_INFO)) {
                receivedTraceNotifications = receivedTraceNotifications + TraceLevel.LEVEL_INFO;
                logSuccessfulCheck(1114205);

                if (traceNotification.getNotificationSource().equals(sbbNotification) ) {
                    logSuccessfulCheck(1114210);
                } else {
                    result.setFailed(1114210, "NotificationSource does not equal sbbNotification.");
                }

                String traceNotifyStr = "SbbNotification[service=ServiceID[name=Test1114205Service,vendor=jain.slee.tck,version=1.1],sbb=SbbID[name=Test1114205Sbb,vendor=jain.slee.tck,version=1.1]]";
                String traceStrVal = traceNotification.getNotificationSource().toString();
                utils().getLog().fine("" + traceStrVal);
                getLog().fine("Trace Notification is: " + traceStrVal);
                if (traceStrVal.equals(traceNotifyStr)) {
                    logSuccessfulCheck(1114203);
                } else {
                    result.setFailed(1114203, "toString() returned incorrect NotificationSource");
                }

                if (traceNotification.getTracerName().equals("com.foo")) {
                    logSuccessfulCheck(1114208);
                }else{
                    result.setFailed(1114208, "Received erroneous Trace Name: " + traceNotification.getTracerName());
                }

                if (traceNotification.getTraceLevel() == TraceLevel.INFO) {
                    logSuccessfulCheck(1114202);
                }else{
                    result.setFailed(1114202, "Received erroneous Trace Level: " + traceNotification.getTraceLevel());
                }

                if (traceNotification.getMessage().equals("INFO:Test1114205TraceMessage")) {
                    logSuccessfulCheck(1114206);
                }else{
                    result.setFailed(1114206, "Received erroneous Trace Message: " + traceNotification.getMessage());
                }
                if (traceNotification.getCause() == null) {
                    logSuccessfulCheck(1114200);
                }else{
                    result.setFailed(1114200, "Received erroneous Trace Cause: " + traceNotification.getCause());
                 }
            }
            else if (traceNotification.getMessage().equals(Test1114205Sbb.TRACE_MESSAGE_CONFIG)) {
                receivedTraceNotifications = receivedTraceNotifications + TraceLevel.LEVEL_CONFIG;
                result.setFailed(1114205, "setFailed. Received erroneous Trace message");
            }
            else if (traceNotification.getMessage().equals(Test1114205Sbb.TRACE_MESSAGE_FINE)) {
                receivedTraceNotifications = receivedTraceNotifications + TraceLevel.LEVEL_FINE;
                result.setFailed(1114205, "setFailed. Received erroneous Trace message");
            }
            else if (traceNotification.getMessage().equals(Test1114205Sbb.TRACE_MESSAGE_FINER)) {
                receivedTraceNotifications = receivedTraceNotifications + TraceLevel.LEVEL_FINER;
                result.setFailed(1114205, "setFailed. Received erroneous Trace message");
            }
            else if (traceNotification.getMessage().equals(Test1114205Sbb.TRACE_MESSAGE_FINEST)) {
                receivedTraceNotifications = receivedTraceNotifications + TraceLevel.LEVEL_FINEST;
                result.setFailed(1114205, "setFailed. Received erroneous Trace message");
            }

            if (!result.isSet())
                    result.setPassed();

            return;
        }

        return;
        }

   }

    private void logSuccessfulCheck(int assertionID) {
        utils().getLog().info("Check for assertion "+assertionID+" OK");
    }

    //private SleeTCKTestUtils utils;
    private TCKResourceListener resourceListener;
    private NotificationListener listener;
    private FutureResult result;
    private DeployableUnitID duID;
    private String testName = "Test1114205";
    private int assertionID = 1114205;
    private SbbID sbbID;
    private ServiceID serviceID;
    private ObjectName traceMBeanName;
    private TraceMBeanProxy tracembean;
    private int receivedTraceNotifications;
    private int expectedTraceNotifications;
    private SbbNotification sbbNotification;
}
