/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.events.deploymentdescriptor;

import java.io.Serializable;
import java.util.Random;
                                                                                
public final class Test663SecondEvent implements Serializable {
    public Test663SecondEvent(boolean isChild) {
        // generate random id
        id = new Random().nextLong() ^ System.currentTimeMillis();

        this.isChild = isChild;
    }
                                                                                
    public boolean equals(Object o) {
        if (o == this) return true;
        return (o instanceof Test663SecondEvent)
            && ((Test663SecondEvent)o).id == id;
    }
                                                                                
    public int hashCode() {
        return (int)id;
    }
                                                                                
    public String toString() {
        return "Test663SecondEvent[" + id + "]";
    }
                                          
    public boolean isChild() {
        return this.isChild;
    }
                                      
    private boolean isChild = false;
    private final long id;
}
