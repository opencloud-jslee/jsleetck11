/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.activities.activitycontextinterface;

import com.opencloud.sleetck.lib.AbstractSleeTCKTest;
import com.opencloud.sleetck.lib.TCKTestResult;
import com.opencloud.sleetck.lib.resource.TCKActivityID;
import com.opencloud.sleetck.lib.resource.events.TCKResourceEventX;
import com.opencloud.sleetck.lib.testutils.QueuingResourceListener;

/**
 * Test assertsions 4506 and 4507.
 * Assertion 4506 tests that an event cannot be fired on an activity that is in ending state.
 * Assertion 4507 tests the ActivityContextInterface.isEnding() method.
 */
public class Test4506Test extends AbstractSleeTCKTest {
    public TCKTestResult run() throws Exception {
        activity = utils().getResourceInterface().createActivity("Test4506.activity");

        utils().getResourceInterface().fireEvent(TCKResourceEventX.X1, Boolean.FALSE, activity, null);

        // Should receive two responses from the test.
        Integer response1, response2;
        response1 = (Integer) listener.nextMessage().getMessage();
        getLog().info("Received response from Sbb: " + response1);
        response2 = (Integer) listener.nextMessage().getMessage();
        getLog().info("Received response from Sbb: " + response2);

        return TCKTestResult.passed();
    }

    public void setUp() throws Exception {
        super.setUp();
        setResourceListener(listener = new QueuingResourceListener(utils()));
    }

    TCKActivityID activity;
    QueuingResourceListener listener;
}
