/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.profiles.profilelocal;

import java.util.Hashtable;

import javax.slee.profile.ProfileLocalObject;

/**
 * Test pass by reference semantics
 */
public interface Test1110101ProfileLocal extends ProfileLocalObject, ProfileLocalTestsProfileCMP {
    public Hashtable businessInsert(Hashtable t, Object key, Object value);
    public String getValue();
    public void setValue(String value);
    public MyNonSerializable businessNonSerial(MyNonSerializable obj, String newValue);
    public void raiseException(RuntimeException rte) ;
    public ProfileLocalObject businessGetProfileLocal();
    public boolean businessCompareProfileLocal(ProfileLocalObject profileLocal);
}
