/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.profiles.checkprofilestore;

/**
 * Define some CMP fields for the CheckProfileStore profile
 */
public interface CheckStoreProfileCMP {
    public static final String INITIAL_VALUE = "INITIALIZED";
    public static final String CHANGED_VALUE_1 = "VALUE1";
    public static final String CHANGED_VALUE_2 = "VALUE2";
    public static final String INVALID_VALUE = "INVALID";

    public String getValue();
    public void setValue(String value);
}
