/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.usage.sets;

import com.opencloud.sleetck.lib.TCKTestErrorException;
import com.opencloud.sleetck.lib.TCKTestFailureException;
import com.opencloud.sleetck.lib.TCKTestResult;
import com.opencloud.sleetck.lib.testsuite.usage.common.GenericUsageMBeanLookup;
import com.opencloud.sleetck.lib.testsuite.usage.common.GenericUsageTest;
import com.opencloud.sleetck.lib.testutils.jmx.ServiceUsageMBeanProxy;

import javax.management.ObjectName;
import javax.slee.InvalidArgumentException;
import javax.slee.SbbID;
import javax.slee.usage.UnrecognizedUsageParameterSetNameException;
import java.util.Iterator;
import java.util.Vector;

/**
 * Tries to create usage parameter sets with valid and invalid names.
 */
public class UsageParameterSetNamesTest extends GenericUsageTest {

    public static final int TEST_ID = 2486;

    public TCKTestResult run() throws Exception {
        GenericUsageMBeanLookup mBeanLookup = getGenericUsageMBeanLookup();
        SbbID sbbID = mBeanLookup.getSbbID();
        ServiceUsageMBeanProxy serviceUsageMBeanProxy = mBeanLookup.getServiceUsageMBeanProxy();

        // first try to create a set with an obviously valid name, to confirm that the SbbID is valid
        // (an invalid SbbID would also cause an InvalidArgumentException)
        try {
            serviceUsageMBeanProxy.createUsageParameterSet(sbbID,"foo");
        } catch (InvalidArgumentException e) {
            throw new TCKTestErrorException("Caught unexpected InvalidArgumentException when trying to create a "+
                    "usage parameter with a valid name. This may indicate an invalid SbbID or a error in the SLEE.",e);
        }

        // create a single name using the well-known valid characters
        StringBuffer validNameBuf = new StringBuffer();
        for (char validChar = 0x0020; validChar < 0x007e; validChar++) {
            validNameBuf.append(validChar);
        }
        String validName = validNameBuf.toString();
        try {
            getLog().info("Attempting to create a usage parameter set name with the following valid name: "+validName);
            serviceUsageMBeanProxy.createUsageParameterSet(sbbID,validName);
            getLog().info("Attempting to access the SbbUsageMBean for the parameter set via getSbbUsageMBean() to test whether the creation was successful");
            ObjectName sbbUsageMBeanName = null;
            try {
                sbbUsageMBeanName = serviceUsageMBeanProxy.getSbbUsageMBean(sbbID,validName);
            } catch (UnrecognizedUsageParameterSetNameException e) {
                throw new TCKTestFailureException(TEST_ID,"Failed to create a usage parameter set "+
                    "with the following valid name: "+validName+". createUsageParameterSet() threw no Exception, "+
                    "but getSbbUsageMBean() threw an UnrecognizedUsageParameterSetNameException for the named usage parameter set.");
            }
            if(sbbUsageMBeanName == null) throw new TCKTestFailureException(TEST_ID,"Failed to create a usage parameter set "+
                    "with the following valid name: "+validName+". createUsageParameterSet() threw no Exception, "+
                    "but getSbbUsageMBean() returned a null ObjectName for the named usage parameter set.");
            getLog().info("getSbbUsageMBean() returned a non-null ObjectName: "+sbbUsageMBeanName);
        } catch (InvalidArgumentException e) {
            throw new TCKTestFailureException(TEST_ID,"Caught unexpected InvalidArgumentException when trying to create a "+
                    "usage parameter with the following valid name: "+validName,e);
        }

        // build a list of invalid characters: the first 32 unicode characters, and the delete character
        Vector invalidCharacters = new Vector();
        for (char invalidChar = 0x0000; invalidChar < 0x001f; invalidChar++) {
            invalidCharacters.addElement(new Character(invalidChar));
        }
        invalidCharacters.addElement(new Character('\u007f')); // the delete character

        Iterator invalidCharactersIter = invalidCharacters.iterator();
        while(invalidCharactersIter.hasNext()) {
            Character invalidCharacter = (Character)invalidCharactersIter.next();
            int unicodeValue = (int)invalidCharacter.charValue();
            try {
                serviceUsageMBeanProxy.createUsageParameterSet(sbbID,invalidCharacter.toString());
                return TCKTestResult.failed(TEST_ID,"The SLEE did not throw the expected InvalidArgumentException when trying "+
                        "to create a usage parameter set with an invalid name. "+
                        "The invalid name was a single character of unicode value "+unicodeValue);
            } catch (InvalidArgumentException e) {
                getLog().info("Caught expected InvalidArgumentException for an invalid parameter set name with a character of "+
                        "unicode value "+unicodeValue+". Error message: "+e.getMessage());
            }
        }

        return TCKTestResult.passed();
    }

}
