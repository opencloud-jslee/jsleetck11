/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.management.SleeManagementMBean;

import javax.management.ObjectName;
import javax.slee.InvalidArgumentException;
import javax.slee.management.DeployableUnitID;
import javax.slee.management.ManagementException;
import javax.slee.management.UnrecognizedSubsystemException;

import com.opencloud.sleetck.lib.TCKTestResult;
import com.opencloud.sleetck.lib.testsuite.usage.common.GenericUsageTest;
import com.opencloud.sleetck.lib.testutils.jmx.SleeManagementMBeanProxy;

/*
 * The SleeManagementMBean interface defines the central management interface for the SLEE.
 * Use this MBean interface to access subsystem details.
 * Note: If the SLEE does not have a subsystem with Usage, there's nothing this test can do
 * about it
 */

public class Test1114391Test extends GenericUsageTest {

    private static final String SERVICE_DU_PATH_PARAM = "serviceDUPath";
    private static final int TEST_ID = 1114391;

    /**
     * Perform the actual test.
     */
    public TCKTestResult run() throws Exception {
        boolean subsystemWithUsageExists = false;
        boolean subsystemWithoutUsageExists = false;
        String usageSubsystem = null;
        String noUsageSubsystem = null;
        SleeManagementMBeanProxy sleeProxy;
        TCKTestResult testResult = null;

       //1114301
        try {
            sleeProxy = utils().getSleeManagementMBeanProxy();
            String strObjectName = utils().getSleeManagementMBeanName().toString();
            getLog().fine("1114301: SleeManagementMBean = "+strObjectName);
            if (strObjectName.equals("javax.slee.management:name=SleeManagement")) {
                logSuccessfulCheck(1114301);
            } else {
                getLog().warning("1114301, SleeManagementMBean returned: " +strObjectName);
                return TCKTestResult.failed(1114301, "Failed to create SleeManagementMBean");
            }
        }
        catch (Exception e) {
            getLog().warning(e);
            return TCKTestResult.error("Failed to create SleeManagementMBean");
        }

        try{

            try {
                 String[] installedSubsystems = sleeProxy.getSubsystems();
                 logSuccessfulCheck(1114391);

                 /* Check the subsystems in the SLEE and assign last one found for both Usage and no Usage
                  * Note: If the SLEE does not have a subsystem with Usage, there's nothing this test can do
                  * about it
                  * */
                 for (int i=0; i<installedSubsystems.length; i++) {
                     utils().getLog().fine("Installed Subsystem is: " + installedSubsystems[i]);
                     try{
                         boolean usage = sleeProxy.hasUsage(installedSubsystems[i]);
                         utils().getLog().fine("Installed Subsystem hasUsage = " +usage);
                         if (usage) {
                             usageSubsystem = installedSubsystems[i];  // Usage Subsystem
                             subsystemWithUsageExists = true;
                         } else {
                             noUsageSubsystem = installedSubsystems[i];    // No Usage Subsystem
                             subsystemWithoutUsageExists = true;
                         }
                     } catch (Exception e) {
                         return TCKTestResult.failed(1114392, "SleeManagementMBeanProxy.hasUsage() has thrown Exception: " + e.getClass().toString());
                     }
                 }

            } catch (ManagementException e) {
                 return TCKTestResult.failed(1114391, "The Installed Subsystems name(s) could not obtained due to a system-level failure");
            } catch (Exception e) {
                 return TCKTestResult.failed(1114391, "SleeManagementMBeanProxy.getSubsystems() has thrown Exception: " + e.getClass().toString());
            }

            // Test the Usage and No Usage cases by calling the test with each subsystem
            if (subsystemWithoutUsageExists) {
                utils().getLog().fine("Checking SleeManagementMBean for subsystem without Usage...");
                testResult = checkSleeSubsystems(sleeProxy, noUsageSubsystem, false);
            }

            if (subsystemWithUsageExists && (testResult == null || testResult == TCKTestResult.passed())) {
                utils().getLog().fine("Now checking SleeManagementMBean for subsystem with Usage...");
                testResult = checkSleeSubsystems(sleeProxy, usageSubsystem, true);
            }

        } finally {
            utils().getLog().fine("Test Complete");
        }

        return testResult;

    }


    public TCKTestResult checkSleeSubsystems(SleeManagementMBeanProxy sleeProxy, String installedSubsystem, boolean hasUsage) throws Exception {
        String usageParam = "NoUsageParams";
        String noUsageSubsystem = null;
        String[] notrecognizedusageParams = null;
        String notrecognizedSubsystem = "Test1114391Test";
        String nullSubsystem = null;

        // 1114392 Usage condition
        try {
            boolean usage = sleeProxy.hasUsage(installedSubsystem);
            utils().getLog().fine("1114392 - hasUsage = " +usage);
            logSuccessfulCheck(1114392);
        } catch (UnrecognizedSubsystemException e) {
            return TCKTestResult.failed(1114392, "The Installed Subsystems name(s) was not recognised");
        } catch (ManagementException e) {
            return TCKTestResult.failed(1114392, "The Installed Subsystems name(s) could not obtained due to a system-level failure");
        } catch (Exception e) {
            return TCKTestResult.failed(1114392, "SleeManagementMBeanProxy.hasUsage() has thrown Exception: " + e.getClass().toString());
        }

        // Following tests are only applicable to subsystems with Usage, but we will test them anyway.
        // If the Subsystem has no Usage it throws InvalidArgumentException
        try {
            String[] usageParams = sleeProxy.getUsageParameterSets(installedSubsystem);
            logSuccessfulCheck(1114393);
            usageParam = usageParams[0];
        } catch (NullPointerException e) {
            return TCKTestResult.failed(1114393, "The Installed Subsystems name(s) was null");
        } catch (UnrecognizedSubsystemException e) {
            return TCKTestResult.failed(1114393, "The Installed Subsystems name(s) was not recognised");
        } catch (InvalidArgumentException e) {
            // Can be that this subsystem has no Usage
            if (!hasUsage) {
                logSuccessfulCheck(1114393);
            } else {
                return TCKTestResult.failed(1114393, "The Installed Subsystems name(s) was not valid");
            }
        } catch (ManagementException e) {
            return TCKTestResult.failed(1114393, "The Installed Subsystems name(s) could not obtained due to a system-level failure");
        } catch (Exception e) {
            return TCKTestResult.failed(1114393, "SleeManagementMBeanProxy.getUsageParameterSets() has thrown Exception: " + e.getClass().toString());
        }

        try {
            ObjectName objectName = sleeProxy.getUsageMBean(installedSubsystem);
            logSuccessfulCheck(1114394);
        } catch (NullPointerException e) {
            return TCKTestResult.failed(1114394, "The Installed Subsystems name(s) was null");
        } catch (UnrecognizedSubsystemException e) {
            return TCKTestResult.failed(1114394, "The Installed Subsystems name(s) was not recognised");
        } catch (InvalidArgumentException e) {
            // Can be that this subsystem has no Usage
            if (!hasUsage) {
                logSuccessfulCheck(1114394);
            } else {
                return TCKTestResult.failed(1114394, "The Installed Subsystems name(s) was not valid");
            }
        } catch (ManagementException e) {
            return TCKTestResult.failed(1114394, "The Installed Subsystems name(s) could not obtained due to a system-level failure");
        } catch (Exception e) {
            return TCKTestResult.failed(1114394, "SleeManagementMBeanProxy.getSubsystems() has thrown Exception: " + e.getClass().toString());
        }

        try {
            ObjectName objectName = sleeProxy.getUsageMBean(installedSubsystem, usageParam);
            logSuccessfulCheck(1114395);
        } catch (NullPointerException e) {
            return TCKTestResult.failed(1114395, "The Installed Subsystems name(s) was null");
        } catch (UnrecognizedSubsystemException e) {
            return TCKTestResult.failed(1114395, "The Installed Subsystems name(s) was not recognised");
        } catch (InvalidArgumentException e) {
            // Can be that this subsystem has no Usage
            if (!hasUsage) {
                logSuccessfulCheck(1114395);
            } else {
                return TCKTestResult.failed(1114395, "The Installed Subsystems name(s) was not valid");
            }
        } catch (ManagementException e) {
            return TCKTestResult.failed(1114395, "The Installed Subsystems name(s) could not obtained due to a system-level failure");
        } catch (Exception e) {
            return TCKTestResult.failed(1114395, "SleeManagementMBeanProxy.getSubsystems() has thrown Exception: " + e.getClass().toString());
        }

        try {
            ObjectName objectName = sleeProxy.getUsageNotificationManagerMBean(installedSubsystem);
            logSuccessfulCheck(1114396);
        } catch (NullPointerException e) {
            return TCKTestResult.failed(1114396, "The Installed Subsystems name(s) was null");
        } catch (UnrecognizedSubsystemException e) {
            return TCKTestResult.failed(1114396, "The Installed Subsystems name(s) was not recognised");
        } catch (InvalidArgumentException e) {
            // Can be that this subsystem has no Usage
            if (!hasUsage) {
                logSuccessfulCheck(1114396);
            } else {
                return TCKTestResult.failed(1114396, "The Installed Subsystems name(s) was not valid");
            }
        } catch (ManagementException e) {
            return TCKTestResult.failed(1114396, "The Installed Subsystems name(s) could not obtained due to a system-level failure");
        } catch (Exception e) {
            return TCKTestResult.failed(1114396, "SleeManagementMBeanProxy.getUsageNotificationManagerMBean() has thrown Exception: " + e.getClass().toString());
        }

        return TCKTestResult.passed();
    }

    /**
     * Do all the pre-run configuration of the test.
     */
    public void setUp() throws Exception {
        // Install the Deployable Units
        try {
            String duPath = utils().getTestParams().getProperty(SERVICE_DU_PATH_PARAM);
            utils().getLog().fine("Installing " + duPath);
            duID = utils().install(duPath);
            utils().activateServices(duID, true);
            getLog().fine("1114391: Deployed Service: "+duPath);
         } catch (Exception e) {
            getLog().warning(e);
            getLog().warning("Failed to install deployable unit.");
        }
    }

    /**
     * Clean up after the test.
     */
    public void tearDown() throws Exception {
        utils().getLog().fine("Disconnecting from resource");
        utils().getResourceInterface().clearActivities();
        utils().getLog().fine("Deactivating and uninstalling service");
        utils().deactivateAllServices();
        utils().uninstallAll();
   }

    private void logSuccessfulCheck(int assertionID) {
        utils().getLog().info("Check for assertion "+assertionID+" OK");
    }

    private DeployableUnitID duID;

}
