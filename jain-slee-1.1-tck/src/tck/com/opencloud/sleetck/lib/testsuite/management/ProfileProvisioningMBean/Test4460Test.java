/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.management.ProfileProvisioningMBean;

import javax.slee.management.*;
import javax.slee.ComponentID;
import javax.slee.ServiceID;
import javax.slee.profile.ProfileID;
import javax.slee.profile.ProfileSpecificationID;
import javax.slee.InvalidArgumentException;
import javax.slee.profile.AttributeNotIndexedException;
import javax.slee.profile.UnrecognizedAttributeException;
import javax.management.ObjectName;

import com.opencloud.sleetck.lib.*;
import com.opencloud.sleetck.lib.testutils.*;
import com.opencloud.sleetck.lib.resource.TCKSbbMessage;
import com.opencloud.sleetck.lib.resource.testapi.TCKResourceListener;
import com.opencloud.sleetck.lib.resource.TCKActivityID;
import com.opencloud.sleetck.lib.resource.testapi.TCKResourceTestInterface;
import com.opencloud.sleetck.lib.resource.events.TCKResourceEventX;
import com.opencloud.sleetck.lib.testutils.ProfileUtils;

import com.opencloud.sleetck.lib.testutils.jmx.DeploymentMBeanProxy;
import com.opencloud.sleetck.lib.testutils.jmx.ServiceManagementMBeanProxy;
import com.opencloud.sleetck.lib.testutils.jmx.ProfileProvisioningMBeanProxy;
import com.opencloud.sleetck.lib.testutils.jmx.ProfileMBeanProxy;

import com.opencloud.logging.Logable;

import java.rmi.RemoteException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Collection;

public class Test4460Test implements SleeTCKTest {

    private static final String SERVICE_DU_PATH_PARAM = "DUPath";
    private static final int TEST_ID = 4460;

    private static final String PROFILE_TABLE_NAME = "Test4460ProfileTable";
    private static final String PROFILE_NAME = "Test4460Profile";

    public void init(SleeTCKTestUtils utils) {
        this.utils = utils;
    }

    /**
     * Perform the actual test.
     */

    public TCKTestResult run() throws Exception {

        profileUtils = new ProfileUtils(utils);
        profileProxy = profileUtils.getProfileProvisioningProxy();
        DeploymentMBeanProxy duProxy = utils.getDeploymentMBeanProxy();
        DeployableUnitDescriptor duDesc = duProxy.getDescriptor(duID);
        ComponentID components[] = duDesc.getComponents();

        ProfileSpecificationID profileSpecID = null;
        ObjectName profileObject = null;
        for (int i = 0; i < components.length; i++) {
            if (components[i] instanceof ProfileSpecificationID) {
                profileSpecID = (ProfileSpecificationID) components[i];

                // Create the profile table.
                try {
                    profileProxy.createProfileTable(profileSpecID, PROFILE_TABLE_NAME);
                    profileObject = profileProxy.createProfile(PROFILE_TABLE_NAME, PROFILE_NAME);
                    ProfileMBeanProxy proxy = utils.getMBeanProxyFactory().createProfileMBeanProxy(profileObject);
                    proxy.commitProfile();
                    proxy.closeProfile();


                } catch (Exception e) {
                    return TCKTestResult.error("Failed to create profile table or profile.");
                }
                break;
            }
        }

        if (profileSpecID == null)
            return TCKTestResult.error("Could not find Profile Specification.");


        // Assertion 4460
        // Call profileProxy.getProfiles(PROFILE_TABLE_NAME) and make sure that
        // only the non-default table is returned.

        Collection profiles = profileProxy.getProfiles(PROFILE_TABLE_NAME);
        Iterator iter = profiles.iterator();
        while (iter.hasNext()) {
            ProfileID profileID = (ProfileID) iter.next();
            if (!profileID.getProfileName().equals(PROFILE_NAME)) {
                return TCKTestResult.failed(4460, "Returned set of profiles contained default profile.");
            }
        }

        // Assertion 4461
        // Call profileProxy.getProfilesByIndexedAttribute(PROFILE_TABLE_NAME, "value", new Integer(0)
        profiles = profileProxy.getProfilesByIndexedAttribute(PROFILE_TABLE_NAME, "value", new Integer(0));
        iter = profiles.iterator();
        while (iter.hasNext()) {
            ProfileID profileID = (ProfileID) iter.next();
            if (!profileID.getProfileName().equals(PROFILE_NAME)) {
                return TCKTestResult.failed(4461, "Returned set of profiles contained default profile.");
            }
        }

        return TCKTestResult.passed();
    }

    /**
     * Do all the pre-run configuration of the test.
     */

    public void setUp() throws Exception {

        utils.getLog().fine("Connecting to resource");
        resourceListener = new TCKResourceListenerImpl();
        utils.getResourceInterface().setResourceListener(resourceListener);
        utils.getLog().fine("Installing and activating service");

        // Install the Deployable Units
        String duPath = utils.getTestParams().getProperty(SERVICE_DU_PATH_PARAM);
        duID = utils.install(duPath);
        utils.activateServices(duID, true);

    }

    /**
     * Clean up after the test.
     */

    public void tearDown() throws Exception {
        
        if (profileProxy != null) {
            try {
                profileProxy.removeProfile(PROFILE_TABLE_NAME, PROFILE_NAME);
            } catch (Exception e) {
            }
            
            try {
                profileUtils.removeProfileTable(PROFILE_TABLE_NAME);
            } catch (Exception e) {
            }
        }


        utils.getLog().fine("Disconnecting from resource");
        utils.getResourceInterface().clearActivities();
        utils.getResourceInterface().removeResourceListener();
        utils.getLog().fine("Deactivating and uninstalling service");
        utils.deactivateAllServices();
        utils.uninstallAll();
    }

    private class TCKResourceListenerImpl extends BaseTCKResourceListener {
        public synchronized void onSbbMessage(TCKSbbMessage message, TCKActivityID calledActivity) throws RemoteException {
            utils.getLog().info("Received message from SBB");

            HashMap map = (HashMap) message.getMessage();
            Boolean passed = (Boolean) map.get("Result");
            String msgString = (String) map.get("Message");

            if (passed.booleanValue() == true)
                result.setPassed();
            else
                result.setFailed(TEST_ID, msgString);
        }

        public void onException(Exception e) throws RemoteException {
            utils.getLog().warning("Received exception from SBB");
            utils.getLog().warning(e);
            result.setError(e);
        }
    }

    private ProfileProvisioningMBeanProxy profileProxy;
    private SleeTCKTestUtils utils;
    private TCKResourceListener resourceListener;
    private FutureResult result;
    private DeployableUnitID duID;
    private ProfileUtils profileUtils;
}
