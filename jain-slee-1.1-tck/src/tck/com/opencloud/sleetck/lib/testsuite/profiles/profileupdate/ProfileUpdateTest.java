/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.profiles.profileupdate;

import com.opencloud.sleetck.lib.*;
import com.opencloud.sleetck.lib.testutils.jmx.*;
import com.opencloud.sleetck.lib.testsuite.profiles.ProfileTestConstants;
import com.opencloud.sleetck.lib.testsuite.profiles.simpleprofile.SimpleProfileProxy;
import com.opencloud.sleetck.lib.testutils.ProfileUtils;
import com.opencloud.sleetck.lib.testutils.ComponentIDLookup;
import com.opencloud.sleetck.lib.testutils.Assert;
import com.opencloud.sleetck.lib.testutils.jmx.ProfileMBeanProxy;

import javax.slee.profile.*;

import com.opencloud.sleetck.lib.infra.SleeTCKComponentConstants;

import javax.management.ObjectName;
import javax.slee.management.ManagementException;
import java.util.Vector;
import java.util.Iterator;


/**
 * Edit the profile via it's management interface.  Tests the SLEE's implementation of the profile CMP interface's
 * getter and setter methods and the lifecycle methods of the profile mbean interface
 */
public class ProfileUpdateTest extends AbstractSleeTCKTest {

    protected static final String PROFILE_TABLE_NAME = "tck.ProfileUpdateTest.table";
    public static final String PROFILE_SPEC_NAME = "SimpleProfile";

    public static final String VALUE_1 = "value1";
    public static final String VALUE_2 = "value2";

    /**
     * Test modification of profile CMP fields via management methods and the ProfileMBean lifecycle.
     * @return
     * @throws Exception
     */
    public TCKTestResult run() throws Exception {
        // create the profile table
        ProfileProvisioningMBeanProxy profileProvisioning = profileUtils.getProfileProvisioningProxy();
        ProfileSpecificationID profileSpecID = new ComponentIDLookup(utils()).lookupProfileSpecificationID(
                PROFILE_SPEC_NAME, SleeTCKComponentConstants.TCK_VENDOR, "1.0");

        getLog().info("Creating profile table: " + PROFILE_TABLE_NAME);
        profileProvisioning.createProfileTable(profileSpecID, PROFILE_TABLE_NAME);
        tableCreated = true;

        // add profile
        final String name = "A";
        getLog().info("Creating profile " + name);
        ObjectName jmxObjectName = profileProvisioning.createProfile(PROFILE_TABLE_NAME, name);

        SimpleProfileProxy proxy = getProfileProxy(jmxObjectName);
        String startValue = proxy.getValue();

        getLog().info("INITIAL VALUE: " + startValue);
        proxy.editProfile();
        proxy.setValue(VALUE_1);

        // Assertion 4362: Calling editProfile() when the profile is already in the writable state will have no effect and return silently
        // (will not throw an exception)
        try {
            proxy.editProfile();
        } catch (ManagementException e) {
            Assert.fail(4362, "Caught exception, editProfile() should return silently if called when the " +
                              "profile is already in a writable state: " + e);
        }
        Assert.assertEquals(4362, "editProfile() should return silently if called when the profile is already in a " +
                                  "writable state.  No exception was thrown but the transaction context has changed",
                            proxy.getValue(), VALUE_1);

        // Assertion 4362: editProfile() will set the profile into a writable state
        Assert.assertEquals(4362, "Profile should be made writable after calling editProfile()",
                            VALUE_1, proxy.getValue());

        proxy.commitProfile();

        // Assertion 1948: if during commitProfile() the profileVerify() callback doesn't throw an exception the changes
        // are committed


        proxy.editProfile();
        proxy.setValue(VALUE_2);
        proxy.restoreProfile();
        // Assertion 4371: Changes made to the profile should be discarded by the SLEE
        Assert.assertEquals(4371, "Changes made in previous update of the profile should have been discarded",
                            VALUE_1, proxy.getValue());

        // Assertion 4373: restoreProfile() should put the profile into read only mode
        Assert.assertTrue(4373, "Profile should have been restored to read only state by restoreProfile()",
                          !proxy.isProfileWriteable());

        proxy.closeProfile();

        return TCKTestResult.passed();
    }

    public void setUp() throws Exception {
        String duPath = utils().getTestParams().getProperty(ProfileTestConstants.PROFILE_SPEC_DU_PATH_PARAM);
        getLog().fine("Installing profile specification");
        utils().install(duPath);
        profileUtils = new ProfileUtils(utils());
        activeProxies = new Vector();
    }

    public void tearDown() throws Exception {
        // close profiles
        if (activeProxies != null) {
            getLog().fine("Closing profiles");
            Iterator activeProxiesIter = activeProxies.iterator();
            while (activeProxiesIter.hasNext()) {
                SimpleProfileProxy aProxy = (SimpleProfileProxy) activeProxiesIter.next();
                try {
                    if (aProxy != null) {
                        if (aProxy.isProfileWriteable()) aProxy.restoreProfile();
                        aProxy.closeProfile();
                    }
                } catch (Exception ex) {
                    getLog().warning("Exception caught while trying to close profiles:");
                    getLog().warning(ex);
                }
            }
        }
        // delete profile tables
        try {
            if (profileUtils != null && tableCreated) {
                getLog().fine("Removing profile table");
                profileUtils.removeProfileTable(PROFILE_TABLE_NAME);
            }
        } catch (Exception e) {
            getLog().warning("Caught exception while trying to remove profile table:");
            getLog().warning(e);
        }
        super.tearDown();
    }

    private SimpleProfileProxy getProfileProxy(ObjectName mbeanName) {
        SimpleProfileProxy rProxy = new SimpleProfileProxy(mbeanName, utils().getMBeanFacade());
        activeProxies.addElement(rProxy);
        return rProxy;
    }

    private ProfileUtils profileUtils;
    private boolean tableCreated = false;
    private Vector activeProxies;

}
