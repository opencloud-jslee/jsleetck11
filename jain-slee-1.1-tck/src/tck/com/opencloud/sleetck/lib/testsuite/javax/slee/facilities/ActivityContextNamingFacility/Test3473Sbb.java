/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.javax.slee.facilities.ActivityContextNamingFacility;

import javax.slee.ActivityEndEvent;
import javax.slee.ActivityContextInterface;
import javax.slee.Address;
import javax.slee.SbbLocalObject;
import javax.slee.facilities.Level;
import javax.slee.ChildRelation;
import javax.slee.nullactivity.NullActivity;
import javax.slee.nullactivity.NullActivityFactory;
import javax.slee.nullactivity.NullActivityContextInterfaceFactory;
import javax.slee.facilities.ActivityContextNamingFacility;

import com.opencloud.sleetck.lib.sbbutils.BaseTCKSbb;
import com.opencloud.sleetck.lib.sbbutils.TCKSbbUtils;
import com.opencloud.sleetck.lib.sbbutils.TCKSbbConstants;
import com.opencloud.sleetck.lib.resource.events.TCKResourceEventX;
import com.opencloud.sleetck.lib.resource.TCKActivityID;
import com.opencloud.sleetck.lib.resource.sbbapi.TCKActivity;
import com.opencloud.sleetck.lib.resource.sbbapi.TCKResourceSbbInterface;
import com.opencloud.sleetck.lib.resource.sbbapi.TCKActivityContextInterfaceFactory;



import java.util.HashMap;

/**
 * SbbLocalObject.setSbbPriority(SbbLocalObject)
 */

public abstract class Test3473Sbb extends BaseTCKSbb {

    public void sbbRolledBack(javax.slee.RolledBackContext context) {
    }

    public void sbbExceptionThrown(Exception exception, Object event, ActivityContextInterface aci) {
    }

    public void onTCKResourceEventX1(TCKResourceEventX event, ActivityContextInterface aci) {

        try {

            // Create a NullActivity
            NullActivityFactory factory = (NullActivityFactory) TCKSbbUtils.getSbbEnvironment().lookup("slee/nullactivity/factory");
            NullActivity nullActivity = factory.createNullActivity();

            // Get its ActivityContextInterface
            NullActivityContextInterfaceFactory aciFactory = (NullActivityContextInterfaceFactory) TCKSbbUtils.getSbbEnvironment().lookup("slee/nullactivity/activitycontextinterfacefactory");
            ActivityContextInterface nullACI = aciFactory.getActivityContextInterface(nullActivity);
            
            // Attach this SBB to the NullActivity
            nullACI.attach(getSbbContext().getSbbLocalObject());
            
            // Fire two events on the new Activity.
            fireTest3473Event(new Test3473Event(), nullACI, null);
            fireTest3473SecondEvent(new Test3473SecondEvent(), nullACI, null);
        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }

    }

    public void onTest3473Event(Test3473Event event, ActivityContextInterface aci) {
        try {
            ActivityContextNamingFacility facility = (ActivityContextNamingFacility) TCKSbbUtils.getSbbEnvironment().lookup("slee/facilities/activitycontextnaming");
            
            // Register this Null ACI with the ActivityContextNaming facility.
            facility.bind(aci, "Test3473NullActivity");
            
            // Mark this transaction for rollback.
            getSbbContext().setRollbackOnly();
        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }
    }

    public void onTest3473SecondEvent(Test3473SecondEvent event, ActivityContextInterface aci) {

        try {
            HashMap map = new HashMap();

            ActivityContextNamingFacility facility = (ActivityContextNamingFacility) TCKSbbUtils.getSbbEnvironment().lookup("slee/facilities/activitycontextnaming");
            
            ActivityContextInterface nullACI = facility.lookup("Test3473NullActivity");
            if (nullACI == null) {
                map.put("Result", new Boolean(true));
                map.put("Message", "Ok");
            } else {
                map.put("Result", new Boolean(false));
                map.put("Message", "ActivityContextNamingFacility stored name even though transaction name was stored in was rolled back");
            }
            
            // End the NullActivity
            NullActivity nullActivity = (NullActivity) aci.getActivity();
            nullActivity.endActivity();

            TCKSbbUtils.getResourceInterface().sendSbbMessage(map);
        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }

    }


    public abstract void fireTest3473Event(Test3473Event event, ActivityContextInterface aci, Address address);
    public abstract void fireTest3473SecondEvent(Test3473SecondEvent event, ActivityContextInterface aci, Address address);

}
