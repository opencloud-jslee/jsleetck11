/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.management.sleestate;

import com.opencloud.sleetck.lib.sbbutils.BaseTCKSbb;
import com.opencloud.sleetck.lib.sbbutils.TCKSbbUtils;
import com.opencloud.sleetck.lib.resource.events.TCKResourceEventX;
import com.opencloud.sleetck.lib.TCKTestErrorException;

import javax.slee.ActivityContextInterface;
import javax.slee.facilities.Level;

/**
 * 
 */
public abstract class Test1479Sbb extends BaseTCKSbb {
    public void onTCKResourceEventX1(TCKResourceEventX event, ActivityContextInterface aci) {
        try {
            TCKSbbUtils.createTrace(getSbbID(), Level.INFO, "onTCKResourceEventX1", null);
            aci.attach(getSbbContext().getSbbLocalObject());
        } catch (TCKTestErrorException e) {
            TCKSbbUtils.handleException(e);
        }
    }
}
