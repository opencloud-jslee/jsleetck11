/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.sbb.abstractclass;

import javax.slee.ActivityEndEvent;
import javax.slee.ActivityContextInterface;
import javax.slee.SbbContext;
import javax.slee.Address;
import javax.slee.SbbLocalObject;
import javax.slee.facilities.Level;
import javax.slee.ChildRelation;
import javax.slee.CreateException;

import com.opencloud.sleetck.lib.sbbutils.BaseTCKSbb;
import com.opencloud.sleetck.lib.sbbutils.TCKSbbUtils;
import com.opencloud.sleetck.lib.sbbutils.TCKSbbConstants;
import com.opencloud.sleetck.lib.resource.events.TCKResourceEventX;
import com.opencloud.sleetck.lib.resource.TCKActivityID;
import com.opencloud.sleetck.lib.resource.sbbapi.TCKActivity;
import com.opencloud.sleetck.lib.resource.sbbapi.TCKResourceSbbInterface;
import com.opencloud.sleetck.lib.resource.sbbapi.TCKActivityContextInterfaceFactory;

import java.util.HashMap;
import java.util.Iterator;

public abstract class Test360Sbb extends BaseTCKSbb {

    public void sbbCreate() throws CreateException {

        HashMap map = new HashMap();

        try {
            
            if (getIntVal() != 0) {
                map.put("Result", new Boolean(false));
                map.put("Message", "Default value for CMP field with int value is not Java default value.");
                TCKSbbUtils.getResourceInterface().sendSbbMessage(map);
                return;
            }

            if (getBooleanVal() != false) {
                map.put("Result", new Boolean(false));
                map.put("Message", "Default value for CMP field with boolean value is not Java default value.");
                TCKSbbUtils.getResourceInterface().sendSbbMessage(map);
                return;
            }
            
            if (getStringVal() != null) {
                map.put("Result", new Boolean(false));
                map.put("Message", "Default value for CMP field with String(Object) value is not Java default value.");
                TCKSbbUtils.getResourceInterface().sendSbbMessage(map);
                return;
            }
            
            if (getDoubleVal() != 0.0) {
                map.put("Result", new Boolean(false));
                map.put("Message", "Default value for CMP field with double value is not Java default value.");
                TCKSbbUtils.getResourceInterface().sendSbbMessage(map);
                return;
            }
            
            map.put("Result", new Boolean(true));
            map.put("Message", "Ok");
            TCKSbbUtils.getResourceInterface().sendSbbMessage(map);
            
        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }
    }

    // Initial event
    public void onTCKResourceEventX1(TCKResourceEventX ev, ActivityContextInterface aci) {
    }

    public abstract void setIntVal(int foo);
    public abstract int getIntVal();

    public abstract void setBooleanVal(boolean foo);
    public abstract boolean getBooleanVal();

    public abstract void setStringVal(String foo);
    public abstract String getStringVal();

    public abstract void setDoubleVal(double foo);
    public abstract double getDoubleVal();
}
