/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.sbb.abstractclass;

import java.util.HashMap;

import javax.slee.ActivityContextInterface;
import javax.slee.facilities.Tracer;

import com.opencloud.sleetck.lib.resource.events.TCKResourceEventX;
import com.opencloud.sleetck.lib.sbbutils.BaseTCKSbb;
import com.opencloud.sleetck.lib.sbbutils.TCKSbbUtils;

public abstract class Test1106023Sbb extends BaseTCKSbb {

    // Initial event
    public void onTCKResourceEventX1(TCKResourceEventX ev, ActivityContextInterface aci) {
        try {
            boolean testPassed = true;
            String tracerName;
            Tracer tracer = null;
            HashMap map = new HashMap();

            // Test for null tracer.
            try {
                tracer = getSbbContext().getTracer(null);
                map.put("TestOne", "getTracer(null) did not throw NullPointerException.");
                testPassed = false;
            } catch (NullPointerException e) {
                // good.
            }

            // Test for invalid tracer name.
            tracerName = "com..mycompany";  
            try {
                tracer = getSbbContext().getTracer(tracerName);
                map.put("TestTwo", "getTracer() with an invalid name did not throw exception.");
                testPassed = false;
            } catch (IllegalArgumentException e) {
                // good.
            }

            // Test getting the root tracer.
            tracerName = ""; 
            try {
                tracer = getSbbContext().getTracer(tracerName);
            } catch (Exception e) {
                map.put("TestThree", "getTracer(\"\") did not return root tracer.");
                testPassed = false;
            }

            // Test getting a good tracer.
            tracerName = "Test1106023.goodTracer"; 
            try {
                tracer = getSbbContext().getTracer(tracerName);
                tracer.info("Normal tracer works.");
            } catch (Exception e) {
                map.put("TestFour", "getTracer(\"Test1106023.goodTracer\") did not return a valid tracer.");
                testPassed = false;
            }
            
            if (testPassed == true)
                map.put("TestResult", "passed");
            else
                map.put("TestResult", "failed");
            TCKSbbUtils.getResourceInterface().sendSbbMessage(map);

        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }

    }

}
