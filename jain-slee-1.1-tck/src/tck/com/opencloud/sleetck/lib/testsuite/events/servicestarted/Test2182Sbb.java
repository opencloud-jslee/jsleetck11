/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.events.servicestarted;

import com.opencloud.sleetck.lib.sbbutils.BaseTCKSbb;
import com.opencloud.sleetck.lib.sbbutils.TCKSbbUtils;

import javax.naming.InitialContext;
import javax.slee.ActivityContextInterface;
import javax.slee.facilities.Level;
import javax.slee.serviceactivity.ServiceActivity;
import javax.slee.serviceactivity.ServiceActivityContextInterfaceFactory;
import javax.slee.serviceactivity.ServiceActivityFactory;
import javax.slee.serviceactivity.ServiceStartedEvent;
import java.util.HashMap;

public abstract class Test2182Sbb extends BaseTCKSbb {

    private static final String JNDI_SERVICEACTIVITY_FACTORY = "java:comp/env/slee/serviceactivity/factory";
    private static final String JNDI_SERVICEACTIVITYACI_FACTORY = "java:comp/env/slee/serviceactivity/activitycontextinterfacefactory";

    // Initial event
    public void onServiceStartedEvent(ServiceStartedEvent event, ActivityContextInterface aci) {

        try {
            HashMap map = new HashMap();

            try {
                ServiceActivityFactory factory = (ServiceActivityFactory) new InitialContext().lookup(JNDI_SERVICEACTIVITY_FACTORY);

                try {
                    ServiceActivity activity = factory.getActivity();

                    if (!aci.getActivity().equals(activity)) {
                        map.put("Result", Boolean.FALSE);
                        map.put("ID", new Integer(2404));
                        map.put("Message", "ServiceActivityFactory.getActivity() did not return the correct ServiceActivity object.");
                        TCKSbbUtils.getResourceInterface().sendSbbMessage(map);
                        return;
                    }

                } catch (Exception e) {
                    map.put("Result", Boolean.FALSE);
                    map.put("ID", new Integer(2404));
                    map.put("Message", "ServiceActivityFactory.getActivity() did not return the correct ServiceActivity object.");
                    TCKSbbUtils.getResourceInterface().sendSbbMessage(map);
                    return;
                }

            } catch (Exception e) {
                map.put("Result", new Boolean(false));
                map.put("ID", new Integer(2199));
                map.put("Message", "The ServiceActivityFactory was not available at the SLEE-defined location: " + JNDI_SERVICEACTIVITY_FACTORY);
                TCKSbbUtils.getResourceInterface().sendSbbMessage(map);
                return;
            }

            try {
                ServiceActivityContextInterfaceFactory aciFactory = (ServiceActivityContextInterfaceFactory) new InitialContext().lookup(JNDI_SERVICEACTIVITYACI_FACTORY);
                try {
                    aciFactory.getActivityContextInterface((ServiceActivity) aci.getActivity());
                } catch (Exception e) {
                    map.put("Result", new Boolean(false));
                    map.put("ID", new Integer(2203));
                    map.put("Message", "The ServiceActivityContextInterfaceFactory didn't return an ActivityContextInterface.");
                    TCKSbbUtils.getResourceInterface().sendSbbMessage(map);
                    return;
                }                
            } catch (Exception e) {
                map.put("Result", new Boolean(false));
                map.put("ID", new Integer(2204));
                map.put("Message", "The ServiceActivityContextInterfaceFactory was not available at the SLEE-defined location: " + JNDI_SERVICEACTIVITYACI_FACTORY);
                TCKSbbUtils.getResourceInterface().sendSbbMessage(map);
                return;
            }


            TCKSbbUtils.createTrace(getSbbID(), Level.INFO, "Received ServiceStartedEvent", null);

            map.put("Result", new Boolean(true));            
            TCKSbbUtils.getResourceInterface().sendSbbMessage(map);
        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }
    }

}
