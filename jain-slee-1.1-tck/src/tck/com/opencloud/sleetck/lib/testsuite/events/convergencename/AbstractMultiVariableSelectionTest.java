/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.events.convergencename;

import java.util.Vector;

/**
 * An abstract base class for convergence name tests where all combinations of initial event selector variables will be
 * tested.
 */
public abstract class AbstractMultiVariableSelectionTest extends AbstractConvergenceNameTest {

    protected static final String[] VARIABLES = {
        "ActivityContext", "Address", "AddressProfile", "EventType", "CustomName"
    };

    private static final short[] FLAGS = { 0x1, 0x2, 0x4, 0x8, 0x10 };

    private Vector combinations;

    /**
     * Generate all possible combinations of the convergence variables defined in <code>VARIABLES</code>.
     * Uses a binary progression to come up with all the combos...
     */
    private void prepareVariableCombinations() {
        combinations = new Vector();
        for (int i = 0; i < Math.round(Math.pow(2.0, VARIABLES.length)); i++) {
            Vector v = new Vector();
            for (int j = 0; j < FLAGS.length; j++) {
                if ((i & FLAGS[j]) != 0) {
                    v.add(VARIABLES[j]);
                }
            }
            if (v.size() >= 1) {
                combinations.add(v);
            }

        }
    }

    public void setUp() throws Exception {
        super.setUp();
        prepareVariableCombinations();
    }

    /**
     * Returns a vector of all possible combinations of initial event selector variables.
     * @return
     */
    public Vector getCombinations() {
        return combinations;
    }

    /**
     * Creates and returns an InitialEventSelectorParameters object
     * based on the selected convergence name variables, and the given
     * custom name.
     */
    public InitialEventSelectorParameters createIESParamsFromSelected(Vector selectedVariables, String customName) {
        return new InitialEventSelectorParameters(
            selectedVariables.contains(ACTIVITY_CONTEXT),
            selectedVariables.contains(ADDRESS),
            selectedVariables.contains(ADDRESS_PROFILE), // address profile
            false, // event object
            selectedVariables.contains(EVENT_TYPE),
            selectedVariables.contains(CUSTOM_NAME) ? customName : null,
            false, false, // IES initial event flag
            false, null); // don't change the address in the IES
    }

}
