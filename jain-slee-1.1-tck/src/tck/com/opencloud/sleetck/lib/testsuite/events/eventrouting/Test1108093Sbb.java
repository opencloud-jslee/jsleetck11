/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.events.eventrouting;

import javax.slee.ActivityContextInterface;
import javax.slee.InitialEventSelector;
import javax.slee.facilities.Tracer;

import com.opencloud.sleetck.lib.resource.events.TCKResourceEventX;
import com.opencloud.sleetck.lib.sbbutils.BaseTCKSbb;
import com.opencloud.sleetck.lib.sbbutils.TCKSbbUtils;
import com.opencloud.sleetck.lib.testsuite.events.convergencename.EventMessageData;
import com.opencloud.sleetck.lib.testsuite.events.convergencename.EventReplyData;
import com.opencloud.sleetck.lib.testsuite.events.convergencename.InitialEventSelectorParameters;

/**
 * AssertionID(1108093): Test If the initialEvent attribute is set to false,
 * then the event is not an initial event for this Service and no further
 * convergence name and initial event processing is performed for this Service,
 * i.e. the SLEE will not create any new root SBB entity for this Service to
 * process this event.
 * 
 */
public abstract class Test1108093Sbb extends BaseTCKSbb {

    // -- Event Handlers -- //

    public void onTCKResourceEventX1(TCKResourceEventX event, ActivityContextInterface aci) {
        handleEvent(event);
    }

    public void onTCKResourceEventX2(TCKResourceEventX event, ActivityContextInterface aci) {
        handleEvent(event);
    }

    private void handleEvent(TCKResourceEventX event) {
        try {
            tracer = getSbbContext().getTracer("com.test");
            tracer.info("Sbb: Received a TCK event " + event);

            // extract the received event id from the event
            EventMessageData eventMessage = EventMessageData.fromExported(event.getMessage());
            String receivedEventID = eventMessage.getEventID();

            // set the initial event id if not already set (i.e. if this is the
            // initial event)
            String initialEventID = getInitialEventID();
            if (initialEventID == null) {
                initialEventID = receivedEventID;
                setInitialEventID(receivedEventID);
            }

            // reply to the test with a synchronous call
            Object reply = new EventReplyData(receivedEventID, initialEventID).toExported();
            TCKSbbUtils.getResourceInterface().callTest(reply);

        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }
    }

    // -- Initial event selector method -- //

    public InitialEventSelector initialEventSelectorMethod(InitialEventSelector ies) {
        try {
            TCKResourceEventX event = (TCKResourceEventX) ies.getEvent();
            EventMessageData eventMessage = EventMessageData.fromExported(event.getMessage());
            InitialEventSelectorParameters iesParams = eventMessage.getInitialEventSelectorData();
            // select the chosen variables
            ies.setActivityContextSelected(iesParams.getSelectActivityContextFlag());
            ies.setAddressProfileSelected(iesParams.getSelectAddressProfileFlag());
            ies.setAddressSelected(iesParams.getSelectAddressFlag());
            ies.setEventTypeSelected(iesParams.getSelectEventTypeFlag());
            ies.setEventSelected(iesParams.getSelectEventFlag());
            ies.setCustomName(iesParams.getCustomName());
            if (iesParams.getChangePossibleInitialEventFlag()) {
                ies.setInitialEvent(iesParams.getPossibleInitialEventFlag());
            }
            if (iesParams.getChangeAddressFlag()) {
                ies.setAddress(iesParams.getNewAddress());
            }
        } catch (RuntimeException ex) {
            TCKSbbUtils.handleException(ex);
        }
        return ies;
    }

    // -- CMP fields -- //

    public abstract String getInitialEventID();

    public abstract void setInitialEventID(String id);

    private Tracer tracer;
}