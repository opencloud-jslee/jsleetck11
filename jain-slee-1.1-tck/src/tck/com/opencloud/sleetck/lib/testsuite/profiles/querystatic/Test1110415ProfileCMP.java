/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.profiles.querystatic;

import javax.slee.Address;

public interface Test1110415ProfileCMP {

    //Singular values:
    public String getStringValue();
    public void setStringValue(String value);

    public int getIntValue();
    public void setIntValue(int value);

    public char getCharValue();
    public void setCharValue(char value);

    public boolean getBoolValue();
    public void setBoolValue(boolean value);

    public double getDoubleValue();
    public void setDoubleValue(double value);

    public byte getByteValue();
    public void setByteValue(byte value);

    public short getShortValue();
    public void setShortValue(short value);

    public long getLongValue();
    public void setLongValue(long value);

    public float getFloatValue();
    public void setFloatValue(float value);

    public Integer getIntObjValue();
    public void setIntObjValue(Integer value);

    public Character getCharObjValue();
    public void setCharObjValue(Character value);

    public Boolean getBoolObjValue();
    public void setBoolObjValue(Boolean value);

    public Double getDoubleObjValue();
    public void setDoubleObjValue(Double value);

    public Byte getByteObjValue();
    public void setByteObjValue(Byte value);

    public Short getShortObjValue();
    public void setShortObjValue(Short value);

    public Long getLongObjValue();
    public void setLongObjValue(Long value);

    public Float getFloatObjValue();
    public void setFloatObjValue(Float value);

    public Address getAddress();
    public void setAddress(Address value);

}
