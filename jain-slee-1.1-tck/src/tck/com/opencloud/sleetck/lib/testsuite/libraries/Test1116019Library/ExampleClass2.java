/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.libraries.Test1116019Library;

import java.awt.AWTPermission;
import java.io.FilePermission;
import java.io.SerializablePermission;
import java.lang.reflect.ReflectPermission;
import java.net.NetPermission;
import java.net.SocketPermission;
import java.rmi.RemoteException;
import java.security.AccessControlException;
import java.security.AccessController;
import java.security.AllPermission;
import java.security.Permission;
import java.security.PrivilegedActionException;
import java.security.PrivilegedExceptionAction;
import java.security.SecurityPermission;
import java.util.PropertyPermission;

import com.opencloud.sleetck.lib.TCKTestErrorException;
import com.opencloud.sleetck.lib.TCKTestFailureException;
import com.opencloud.sleetck.lib.TCKTestResult;
import com.opencloud.sleetck.lib.sbbutils.TCKSbbUtils;


/* 
 * This gets packaged up into a library 
 * 
 * I get put directly in the library component.
 * 
 */
public class ExampleClass2 {
    private static final int SECURITY_TEST_ID = 1116029;

    /**
     * Checks that the SLEE will grant the Sbb the given permission
     * 
     */
    protected void checkGrant(Permission permission, int assertionID) throws TCKTestErrorException, TCKTestFailureException {
        try {
            AccessController.checkPermission(permission);
            //
        } catch (AccessControlException ace) {
            throw new TCKTestFailureException(assertionID, "The SLEE denied this library a permission which it was expected to grant: ", ace);
        }
    }

    /**
     * Checks that the SLEE will deny the Sbb the given permission
     * 
     */
    protected void checkDeny(Permission permission, int assertionID) throws TCKTestErrorException, TCKTestFailureException {
        try {
            AccessController.checkPermission(permission);
            throw new TCKTestFailureException(assertionID, "The SLEE granted this library a permission which it was expected to deny: ");
        } catch (AccessControlException ace) {
            //
        }
    }
    
    /*
     * Check for extra permissions.
     * 
     * In this case, we check for permissions
     * for classes directly in the library
     * component.
     */
    public void checkExtraPermissions() throws TCKTestErrorException, TCKTestFailureException {
        try {
            AccessController.doPrivileged(new PrivilegedExceptionAction() {
                public Object run() throws TCKTestFailureException, TCKTestErrorException {
                    checkDeny(new PropertyPermission("tcktest.library.inthejar", "write"), 1116029);
                    checkGrant(new PropertyPermission("tcktest.library.direct", "write"), 1116018);
                    return null;
                }
            } );
        } catch (PrivilegedActionException e) {
            throw new TCKTestFailureException(1116018, "permissions were incorrect", e);
        }
    }
    
    
    /**
     * Checks the various permissions, with target names and actions introduced in Java 1.3 or lower
     * 
     */
    public void checkPermissions() throws TCKTestErrorException, TCKTestFailureException {
        checkDeny(new AllPermission(),SECURITY_TEST_ID);

        checkDeny(new AWTPermission("*"),SECURITY_TEST_ID);
        checkDeny(new AWTPermission("accessClipboard"),SECURITY_TEST_ID);
        checkDeny(new AWTPermission("accessEventQueue"),SECURITY_TEST_ID);
        checkDeny(new AWTPermission("listenToAllAWTEvents"),SECURITY_TEST_ID);
        checkDeny(new AWTPermission("showWindowWithoutWarningBanner"),SECURITY_TEST_ID);
        checkDeny(new AWTPermission("readDisplayPixels"),SECURITY_TEST_ID);
        checkDeny(new AWTPermission("createRobot"),SECURITY_TEST_ID);

        checkDeny(new FilePermission("*","read"),SECURITY_TEST_ID);
        checkDeny(new FilePermission("*","write"),SECURITY_TEST_ID);
        checkDeny(new FilePermission("*","execute"),SECURITY_TEST_ID);
        checkDeny(new FilePermission("*","delete"),SECURITY_TEST_ID);

        checkDeny(new NetPermission("setDefaultAuthenticator"),SECURITY_TEST_ID);
        checkDeny(new NetPermission("requestPasswordAuthentication"),SECURITY_TEST_ID);
        checkDeny(new NetPermission("specifyStreamHandler"),SECURITY_TEST_ID);

        checkGrant(new PropertyPermission("*","read"),SECURITY_TEST_ID);
        checkDeny(new PropertyPermission("*","write"),SECURITY_TEST_ID);

        checkDeny(new ReflectPermission("suppressAccessChecks"),SECURITY_TEST_ID);

        // queuePrintJob no longer allowed
        checkDeny(new RuntimePermission("queuePrintJob"), SECURITY_TEST_ID);
        checkDeny(new RuntimePermission("createClassLoader"),SECURITY_TEST_ID);
        checkDeny(new RuntimePermission("getClassLoader"),SECURITY_TEST_ID);
        checkDeny(new RuntimePermission("setContextClassLoader"),SECURITY_TEST_ID);
        checkDeny(new RuntimePermission("setSecurityManager"),SECURITY_TEST_ID);
        checkDeny(new RuntimePermission("createSecurityManager"),SECURITY_TEST_ID);
        checkDeny(new RuntimePermission("exitVM"),SECURITY_TEST_ID);
        checkDeny(new RuntimePermission("shutdownHooks"),SECURITY_TEST_ID);
        checkDeny(new RuntimePermission("setFactory"),SECURITY_TEST_ID);

        checkDeny(new RuntimePermission("setIO"),SECURITY_TEST_ID);
        checkDeny(new RuntimePermission("modifyThread"),SECURITY_TEST_ID);
        checkDeny(new RuntimePermission("stopThread"),SECURITY_TEST_ID);
        checkDeny(new RuntimePermission("modifyThreadGroup"),SECURITY_TEST_ID);
        checkDeny(new RuntimePermission("getProtectionDomain"),SECURITY_TEST_ID);
        checkDeny(new RuntimePermission("readFileDescriptor"),SECURITY_TEST_ID);
        checkDeny(new RuntimePermission("writeFileDescriptor"),SECURITY_TEST_ID);
        checkDeny(new RuntimePermission("loadLibrary.*"),SECURITY_TEST_ID);
        checkDeny(new RuntimePermission("accessClassInPackage.*"),SECURITY_TEST_ID);
        checkDeny(new RuntimePermission("defineClassInPackage.*"),SECURITY_TEST_ID);
        checkDeny(new RuntimePermission("accessDeclaredMembers"),SECURITY_TEST_ID);
        checkDeny(new RuntimePermission("*"),SECURITY_TEST_ID);

        checkDeny(new SecurityPermission("*"),SECURITY_TEST_ID);
        checkDeny(new SecurityPermission("createAccessControlContext"),SECURITY_TEST_ID);
        checkDeny(new SecurityPermission("getDomainCombiner"),SECURITY_TEST_ID);
        checkDeny(new SecurityPermission("getPolicy"),SECURITY_TEST_ID);
        checkDeny(new SecurityPermission("setPolicy"),SECURITY_TEST_ID);
        checkDeny(new SecurityPermission("getProperty.*"),SECURITY_TEST_ID);
        checkDeny(new SecurityPermission("setProperty.*"),SECURITY_TEST_ID);
        checkDeny(new SecurityPermission("insertProvider.*"),SECURITY_TEST_ID);
        checkDeny(new SecurityPermission("removeProvider.*"),SECURITY_TEST_ID);
        checkDeny(new SecurityPermission("setSystemScope"),SECURITY_TEST_ID);
        checkDeny(new SecurityPermission("setIdentityPublicKey"),SECURITY_TEST_ID);
        checkDeny(new SecurityPermission("setIdentityInfo"),SECURITY_TEST_ID);
        checkDeny(new SecurityPermission("addIdentityCertificate"),SECURITY_TEST_ID);
        checkDeny(new SecurityPermission("removeIdentityCertificate"),SECURITY_TEST_ID);
        checkDeny(new SecurityPermission("printIdentity"),SECURITY_TEST_ID);
        checkDeny(new SecurityPermission("clearProviderProperties.*"),SECURITY_TEST_ID);
        checkDeny(new SecurityPermission("putProviderProperty.*"),SECURITY_TEST_ID);
        checkDeny(new SecurityPermission("removeProviderProperty.*"),SECURITY_TEST_ID);
        checkDeny(new SecurityPermission("getSignerPrivateKey"),SECURITY_TEST_ID);
        checkDeny(new SecurityPermission("setSignerKeyPair"),SECURITY_TEST_ID);

        checkDeny(new SerializablePermission("enableSubclassImplementation"),SECURITY_TEST_ID);
        checkDeny(new SerializablePermission("enableSubstitution"),SECURITY_TEST_ID);

        checkDeny(new SocketPermission("*","accept"),SECURITY_TEST_ID);
        checkDeny(new SocketPermission("*","listen"),SECURITY_TEST_ID);

    }
}


