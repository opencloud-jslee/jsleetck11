/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.usage.sbb;

import java.rmi.RemoteException;

import javax.management.Notification;
import javax.management.ObjectName;
import javax.slee.SbbID;
import javax.slee.ServiceID;
import javax.slee.management.SbbNotification;
import javax.slee.usage.SampleStatistics;

import com.opencloud.sleetck.lib.OperationTimedOutException;
import com.opencloud.sleetck.lib.TCKTestErrorException;
import com.opencloud.sleetck.lib.TCKTestFailureException;
import com.opencloud.sleetck.lib.TCKTestResult;
import com.opencloud.sleetck.lib.resource.TCKActivityID;
import com.opencloud.sleetck.lib.resource.TCKSbbMessage;
import com.opencloud.sleetck.lib.resource.events.TCKResourceEventX;
import com.opencloud.sleetck.lib.testsuite.usage.common.GenericUsageSbbInstructions;
import com.opencloud.sleetck.lib.testsuite.usage.common.UsageMBeanProxy;
import com.opencloud.sleetck.lib.testsuite.usage.common.UsageMBeanProxyImpl;
import com.opencloud.sleetck.lib.testutils.BaseTCKResourceListener;
import com.opencloud.sleetck.lib.testutils.FutureResult;
import com.opencloud.sleetck.lib.testutils.QueuingNotificationListener;
import com.opencloud.sleetck.lib.testutils.jmx.MBeanProxyFactory;
import com.opencloud.sleetck.lib.testutils.jmx.ServiceUsageMBeanProxy;

public class Test1111056Test extends BaseUsageTest {


    public static final int TEST_ID = 1111055;
    public static final String PARAM_SET_NAME = "Param set name";

    public TCKTestResult run() throws Exception {
        TCKActivityID activityID = utils().getResourceInterface().createActivity("token activity");
        UsageMBeanProxy usage;
        long stats;
        SampleStatistics sample;
        Notification ntnA;
        Notification ntnB;

        // Test on a default parameter set.
        GenericUsageSbbInstructions instr = new GenericUsageSbbInstructions(null);
        instr.addSecondCountIncrement(1); // enabled-notifications="True"
        instr.addTimeBetweenNewConnectionsSamples(36);

        getLog().info("firing event to Sbb");
        utils().getResourceInterface().fireEvent(TCKResourceEventX.X1,instr.toExported(),activityID,null);

        // 1111054 - We should get two notifications back.
        try {
            ntnA = notificationListenerA.nextNotification();
            getLog().fine("Got notification: "+ntnA.toString());
            // Test 1111200
            if (!SbbNotification.USAGE_NOTIFICATION_TYPE.equals(ntnA.getType()))
                return TCKTestResult.failed(1111200, "Notification type is not a SbbNotification.");
        } catch (OperationTimedOutException e) {
            return TCKTestResult.failed(1111054, "Did not receive notification A in time");
        }
        try { // Use a separate try...catch... clause to make exceptions pretty.
            ntnB = notificationListenerB.nextNotification();
            getLog().fine("Got notification: "+ntnB.toString());
        } catch (OperationTimedOutException e) {
            return  TCKTestResult.failed(1111054, "Did not receive notification B in time");
        }

        // Now check the results.
        try {
            if (ntnA.equals(ntnB))
                 return  TCKTestResult.failed(1111054, "2 notifications from different Services are equal");
        } catch (Exception e) {
            utils().getLog().error(e);
            return TCKTestResult.failed(new TCKTestFailureException(TEST_ID, "Exception occurred comparing Notifications", e));
        }

        try {
            // Check the default usage parameters
            for (int i=0; i<services.length; i++) {
                usage = getSbbUsageMBean(services[i]);
                // Cut and paste from 1111025.
                stats = usage.getFirstCount(false);
                if (stats != 0) return TCKTestResult.failed(TEST_ID, "Usage parameter 'firstCount' had unexpected value.");

                stats = usage.getSecondCount(false);
                if (stats != 1) return TCKTestResult.failed(TEST_ID, "Usage parameter 'secondCount' had unexpected value.");

                sample = usage.getTimeBetweenNewConnections(false);

                getLog().fine("timeBetweenNewConnections is: "+sample.getSampleCount());
                getLog().fine("timeBetweenNewConnections is: "+sample.getSampleCount());
                getLog().fine("timeBetweenNewConnections is: "+sample.getSampleCount());
                if (sample.getSampleCount() != 1) return TCKTestResult.failed(TEST_ID, "Usage parameter 'timeBetweenConnections' had unexpected value.");

                sample = usage.getTimeBetweenErrors(false);
                getLog().fine("timeBetweenErrors is: "+sample.getSampleCount());
                if (sample.getSampleCount() != 0) return TCKTestResult.failed(TEST_ID, "Usage parameter 'timeBetweenErrors' had unexpected value.");
            }
        } catch (Exception e) {
            utils().getLog().error(e);
            return TCKTestResult.failed(new TCKTestFailureException(TEST_ID, "Exception occurred checking default usage parameters", e));
        }
        return TCKTestResult.passed();
    }

    public void setUp() throws Exception {
        super.setUp();

        sbbid = new SbbID("GenericUsageSbb", "jain.slee.tck", "1.1");

        utils().getLog().fine("Connecting to resource");
        resourceListener = new TCKResourceListenerImpl();
        utils().getResourceInterface().setResourceListener(resourceListener);

        // short-hand:
        proxyFactory = utils().getMBeanProxyFactory();

        // Set up the notifications.
        notificationListenerA = new QueuingNotificationListener(utils());
        notificationListenerB  = new QueuingNotificationListener(utils());

        if (2 != services.length) throw new TCKTestErrorException("There should be 2 services deployed.");
        getSbbUsageMBean(services[0]).addNotificationListener(notificationListenerA, null, null);
        getSbbUsageMBean(services[1]).addNotificationListener(notificationListenerB, null, null);
    }

    public void tearDown() throws Exception {
        UsageMBeanProxy u;
        u = getSbbUsageMBean(services[0]);
        u.removeNotificationListener(notificationListenerA);
        u.close();
        u = getSbbUsageMBean(services[1]);
        u.removeNotificationListener(notificationListenerB);
        u.close();
        super.tearDown();
    }

    private UsageMBeanProxy getSbbUsageMBean(ServiceID serviceID) throws TCKTestErrorException {
        ObjectName n;
        ServiceUsageMBeanProxy serviceUsage;
        try {
            n = utils().getServiceManagementMBeanProxy().getServiceUsageMBean(serviceID);
            serviceUsage = proxyFactory.createServiceUsageMBeanProxy(n);
            n = serviceUsage.getSbbUsageMBean(sbbid); // Get the default usage parameter set.
            return new UsageMBeanProxyImpl(n, utils().getMBeanFacade());
        } catch(Exception e) {
            throw new TCKTestErrorException("Failed to get SBB usage proxy.", e);
        }
    }

    private class TCKResourceListenerImpl extends BaseTCKResourceListener {

        public synchronized void onSbbMessage(TCKSbbMessage message, TCKActivityID activity) throws RemoteException {
            utils().getLog().fine("Received message from SBB: " + message.getMessage().toString());
        }

        public void onException(Exception e) throws RemoteException {
            utils().getLog().warning("Received exception from SBB.");
            utils().getLog().warning(e);
            result.setError(e);
        }
    }

    private TCKResourceListenerImpl resourceListener;
    private SbbID sbbid;
    private MBeanProxyFactory proxyFactory;
    private FutureResult result;
    private QueuingNotificationListener notificationListenerA, notificationListenerB;
}