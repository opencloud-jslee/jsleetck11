/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.sbb.abstractclass;

import javax.naming.*;

/**
 * Test scoping of environment entries
 */
public abstract class EnvEntryScopeChild2Sbb extends SendResultsSbb {
    public boolean checkEnvEntries() throws Exception {
        Context context = (Context)new InitialContext().lookup("java:comp/env");

        try {
            context.lookup("teststring");
            setResultFailed(496, "teststring environment variable present when it shouldn't be");
            return false;
        }
        catch (NameNotFoundException nnfe) {}

        Boolean b = (Boolean)context.lookup("testboolean");
        if (b.booleanValue()) {
            setResultFailed(496, "testboolean environment variable does not contain expected value 'false', instead it has 'true'");
            return false;
        }

        return true;
    }
}

