/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.events.FireEventNotReady;

import com.opencloud.sleetck.lib.sbbutils.BaseTCKSbb;
import com.opencloud.sleetck.lib.sbbutils.TCKSbbUtils;
import com.opencloud.sleetck.lib.resource.events.TCKResourceEventX;
import com.opencloud.sleetck.lib.resource.sbbapi.TCKActivity;
import javax.slee.ActivityContextInterface;
import javax.slee.facilities.Level;
import javax.slee.ChildRelation;
import javax.slee.facilities.ActivityContextNamingFacility;
import javax.slee.*;
import javax.naming.InitialContext;

public abstract class SetupSbb extends BaseTCKSbb {

    public void onTCKResourceEventX1(TCKResourceEventX event, ActivityContextInterface aci) {

          try{
              InitialContext ic = new InitialContext();
              ActivityContextNamingFacility acNaming = (ActivityContextNamingFacility)ic.lookup("java:comp/env/slee/facilities/activitycontextnaming");

              acNaming.bind( aci, "name" );
              getChildRelation().create();
          }catch( javax.slee.facilities.NameAlreadyBoundException nabe ){
              throw new RuntimeException("the activity context is already bound to the name 'name', it should not be");
          }catch( javax.naming.NamingException ne ){
              throw new RuntimeException(ne.getMessage());
          }catch( javax.slee.CreateException ce ){
              throw new RuntimeException( ce.getMessage() );
          }

    }

    private void sendTestSuccess(){
        sendBooleanMessage( true );
    }

    private void sendTestFail(){
        sendBooleanMessage(false);
    }

    private void sendBooleanMessage(boolean bool) {
        try {
            TCKSbbUtils.createTrace(getSbbID(),Level.FINE,"sending a boolean message back with value: " + bool,null);
            // send a reply: the value of the boolean message
            Boolean message = new Boolean( bool );
            TCKSbbUtils.getResourceInterface().sendSbbMessage(message);
        } catch(Exception e) {
            TCKSbbUtils.handleException(e);
        }
    }

    public abstract ChildRelation getChildRelation();
}
