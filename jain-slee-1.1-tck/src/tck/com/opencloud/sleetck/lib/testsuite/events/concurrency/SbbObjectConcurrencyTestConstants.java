/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.events.concurrency;

/**
 * Defines constants used by the SbbObjectConcurrencyTest
 */
public interface SbbObjectConcurrencyTestConstants {

    // -- Codes used to indicate the nature of a sbb -> test call -- //

    /**
     * Used to request an Sbb Object ID.
     * The test will return a unique ID as a java.lang.Integer
     */
    public static final int REQUEST_SBB_OID     = 1;

    /**
     * Used when an X1 event is received.
     * The test will block for some length of time before returning.
     */
    public static final int RECEIVED_X1         = 2;

    /**
     * Used when a Y1 event is received.
     */
    public static final int RECEIVED_Y1         = 3;

    /**
     * Used when a Y2 event is received.
     */
    public static final int RECEIVED_Y2         = 4;

    /**
     * Used when a Y3 event is received.
     */
    public static final int RECEIVED_Y3         = 5;

    /**
     * Used when the sbbRolledBack method is called with the X1 event.
     * The test will block for some length of time before returning.
     */
    public static final int SBB_ROLLED_BACK_X1     = 6;

    /**
     * Used when the sbbRolledBack method is called with the Y1 event.
     */
    public static final int SBB_ROLLED_BACK_Y1     = 7;

    /**
     * Used when the sbbRolledBack method is called with the Y2 event.
     */
    public static final int SBB_ROLLED_BACK_Y2     = 8;

    /**
     * Used when the sbbRolledBack method is called with the Y3 event.
     */
    public static final int SBB_ROLLED_BACK_Y3     = 9;

    /**
     * Used when the sbbExceptionThrown method is called with the X1 event.
     * The test will block for some length of time before returning.
     */
    public static final int SBB_EXCEPTION_THROWN= 10;

}