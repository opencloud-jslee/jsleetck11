/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.profiles.profileadded;

import java.util.HashMap;

import javax.management.Attribute;
import javax.management.ObjectName;
import javax.slee.profile.ProfileSpecificationID;

import com.opencloud.logging.Logable;
import com.opencloud.sleetck.lib.AbstractSleeTCKTest;
import com.opencloud.sleetck.lib.DescriptionKeys;
import com.opencloud.sleetck.lib.TCKTestResult;
import com.opencloud.sleetck.lib.infra.SleeTCKComponentConstants;
import com.opencloud.sleetck.lib.sbbutils.events2.SbbBaseMessageConstants;
import com.opencloud.sleetck.lib.sbbutils.events2.SbbMessageAdapter;
import com.opencloud.sleetck.lib.testsuite.profiles.ProfileTestConstants;
import com.opencloud.sleetck.lib.testutils.FutureResult;
import com.opencloud.sleetck.lib.testutils.ProfileUtils;
import com.opencloud.sleetck.lib.testutils.jmx.ProfileMBeanProxy;
import com.opencloud.sleetck.lib.testutils.jmx.ProfileProvisioningMBeanProxy;

public class Test1110024Test extends AbstractSleeTCKTest {

    private static final String TCK_SBB_EVENT_DU_PATH_PARAM = "eventDUPath";
    private static final String SEND_RESULT_EVENT_DU_PATH_PARAM = "resultEventDUPath";

    private static final String PROFILE_TABLE_NAME = "Test1110024ProfileTable";
    private static final String PROFILE_NAME = "Test1110024Profile";
    private static final int TEST_ID = 1110024;

    private static final String PROFILE_SPEC_NAME = "ProfileEventsTestsProfile";
    private static final String PROFILE_SPEC_VERSION = "1.0";

    /**
     * Perform the actual test.
     */
    public TCKTestResult run() throws Exception {
        Logable log =  utils().getLog();
        result = new FutureResult(utils().getLog());

        // Create the profile table.
        ProfileSpecificationID profSpecID = new ProfileSpecificationID(PROFILE_SPEC_NAME, SleeTCKComponentConstants.TCK_VENDOR, PROFILE_SPEC_VERSION);
        profileProvisioning.createProfileTable(profSpecID, PROFILE_TABLE_NAME);
        log.fine("Created profile table "+PROFILE_TABLE_NAME+ " for profile specification "+profSpecID.getName()+" "+profSpecID.getVersion()+", "+profSpecID.getVendor());

        // Create a profile, set some values, then commit it.
        ObjectName profile = profileProvisioning.createProfile(PROFILE_TABLE_NAME, PROFILE_NAME);
        ProfileMBeanProxy proxy = utils().getMBeanProxyFactory().createProfileMBeanProxy(profile);
        log.fine("Created profile "+PROFILE_NAME+" for profile table "+PROFILE_TABLE_NAME);

        // Set 'value' to '0'
        utils().getMBeanFacade().setAttribute(profile, new Attribute("Value", new Integer(0)));
        log.fine("Initially set 'value' to '0'");

        // Commit the profile, this should generate a ProfileAddedEvent
        proxy.commitProfile();
        proxy.closeProfile();
        log.fine("Commit profile. This should create ProfileAddedEvent");

        profile = profileProvisioning.getProfile(PROFILE_TABLE_NAME, PROFILE_NAME);
        proxy = utils().getMBeanProxyFactory().createProfileMBeanProxy(profile);
        //Change value to new value '42'
        proxy.editProfile();
        utils().getMBeanFacade().setAttribute(profile, new Attribute("Value", new Integer(42)));
        log.fine("Set 'value' to '42'");

        //Commit the profile, this should generate a ProfileUpdatedEvent
        proxy.commitProfile();
        proxy.closeProfile();
        log.fine("Commit and close profile. This should create ProfileUpdatedEvent");

        log.info("Waiting for Sbb's response to 'ProfileUpdatedEvent'");
        return result.waitForResultOrFail(utils().getTestTimeout(), "Timeout waiting for test result", TEST_ID);
    }

    private void onUpdateProfileRequest(Integer newValue) throws Exception {
        Logable log = utils().getLog();
        ObjectName profile = profileProvisioning.getProfile(PROFILE_TABLE_NAME, PROFILE_NAME);
        ProfileMBeanProxy proxy = utils().getMBeanProxyFactory().createProfileMBeanProxy(profile);
        log.fine("Obtained MBean proxy for profile "+PROFILE_TABLE_NAME+"/"+PROFILE_NAME);

        // Set new value
        proxy.editProfile();
        utils().getMBeanFacade().setAttribute(profile, new Attribute("Value", newValue));
        log.fine("Set 'value' to '"+newValue.intValue()+"'");

        // Commit the profile
        proxy.commitProfile();
        proxy.closeProfile();
        log.fine("Commit changed value");
    }

    public void setPassed() {
        result.setPassed();
    }

    public void setFailed(int id, String msg) {
        result.setFailed(id, msg);
    }

    public void setError(Exception e) {
        result.setError(e);
    }

    /**
     * Do all the pre-run configuration of the test.
     */

    public void setUp() throws Exception {

        setupService(TCK_SBB_EVENT_DU_PATH_PARAM);

        setupService(SEND_RESULT_EVENT_DU_PATH_PARAM);

        setupService(ProfileTestConstants.PROFILE_SPEC_DU_PATH_PARAM);

        setupService(DescriptionKeys.SERVICE_DU_PATH_PARAM);

        // Create profileUtils and profileProvisioning proxy
        profileProvisioning = new ProfileUtils(utils()).getProfileProvisioningProxy();

        // Create and register a resource listener for this test
        setResourceListener(new SbbMessageAdapter() {
            public Logable getLog() {
                return utils().getLog();
            }
            public void onSetPassed(int id, String msg) { setPassed(); }
            public void onSetFailed(int id, String msg) { setFailed(id, msg); }
            public void onSetException(Exception e) { setError(e); }
            public void onHandleRequest(int type, HashMap msgLoad) throws Exception {
                switch (type) {
                case Test1110024Sbb.TYPE_UPDATE_PROFILE:
                    Integer id = (Integer)msgLoad.get(SbbBaseMessageConstants.ID);
                    Integer newValue = (Integer)msgLoad.get("NewValue");
                    onUpdateProfileRequest(newValue);
                    break;
                }
            }
        });
}

    /**
     * Clean up after the test.
     */

    public void tearDown() throws Exception {
        try {
            profileProvisioning.removeProfileTable(PROFILE_TABLE_NAME);
        } catch (Exception e) {
            getLog().warning("Exception occured while trying to remove profile table: ");
            getLog().warning(e);
        }

        super.tearDown();
    }

    private FutureResult result;
    private ProfileProvisioningMBeanProxy profileProvisioning;
}
