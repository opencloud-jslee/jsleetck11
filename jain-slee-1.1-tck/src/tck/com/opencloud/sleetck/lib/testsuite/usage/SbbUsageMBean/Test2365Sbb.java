/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.usage.SbbUsageMBean;

import com.opencloud.sleetck.lib.resource.events.TCKResourceEventX;
import com.opencloud.sleetck.lib.sbbutils.BaseTCKSbb;
import com.opencloud.sleetck.lib.sbbutils.TCKSbbUtils;

import javax.slee.ActivityContextInterface;
import javax.slee.ChildRelation;
import javax.slee.facilities.Level;

public abstract class Test2365Sbb extends BaseTCKSbb {

    public static final String PARAMETER_SET_NAME = "Test2365Test-ParameterSet";

    // Initial event
    public void onTCKResourceEventX1(TCKResourceEventX event, ActivityContextInterface aci) {
        try {
            // delegate to the child SBBs to do updates
            TCKSbbUtils.createTrace(getSbbID(),Level.INFO,"onTCKResourceEventX1(): asking the child SBB to do usage updates",null);
            ((Test2365SbbChildLocal)getChildRelationA().create()).doUpdates();
            ((Test2365SbbChildLocal)getChildRelationB().create()).doUpdates();
            // send a message back to the test to confirm the updates
            TCKSbbUtils.createTrace(getSbbID(),Level.INFO,"onTCKResourceEventX1(): replying to test",null);
            TCKSbbUtils.getResourceInterface().sendSbbMessage(null);
        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }
    }

    public abstract ChildRelation getChildRelationA();
    public abstract ChildRelation getChildRelationB();

}
