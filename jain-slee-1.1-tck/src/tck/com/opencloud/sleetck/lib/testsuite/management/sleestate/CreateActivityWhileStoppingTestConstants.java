/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.management.sleestate;

/**
 * Constants used by the CreateActivityWhileStoppingTest
 */
public interface CreateActivityWhileStoppingTestConstants {

    // -- Sbb -> Test call codes -- //

    // Sent by X1 event handler
    public static final int RECEIVED_X1 = 1;

    // Sent by X2 event handler
    public static final int NULL_ACTIVITY_CREATED_FAILED = 2;
    public static final int NULL_ACTIVITY_CREATE_SUCCEEDED = 3;

    // Sent by Y1 event handler
    public static final int PROFILE_TABLE_ACTIVITY_FOUND = 4;
    public static final int PROFILE_TABLE_ACTIVITY_NOT_FOUND = 5;

    // Sent by Y2 event handler
    public static final int RECEIVED_Y2 = 8;

}