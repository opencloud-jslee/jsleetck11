/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.profiles.profilesbbsecurity;

import com.opencloud.sleetck.lib.*;
import com.opencloud.sleetck.lib.testutils.QueuingResourceListener;
import com.opencloud.sleetck.lib.resource.TCKActivityID;
import com.opencloud.sleetck.lib.resource.testapi.TCKResourceTestInterface;
import com.opencloud.sleetck.lib.resource.events.TCKResourceEventX;
import com.opencloud.sleetck.lib.testutils.*;
import com.opencloud.sleetck.lib.testutils.jmx.*;
import com.opencloud.sleetck.lib.testsuite.profiles.ProfileTestConstants;
import com.opencloud.sleetck.lib.testutils.ProfileUtils;
import com.opencloud.sleetck.lib.testutils.ComponentIDLookup;
import com.opencloud.sleetck.lib.testutils.jmx.ProfileProvisioningMBeanProxy;
import com.opencloud.sleetck.lib.testutils.jmx.ProfileMBeanProxy;

import javax.slee.profile.ProfileSpecificationID;

import com.opencloud.sleetck.lib.infra.SleeTCKComponentConstants;

import java.util.Vector;
import java.util.Iterator;

import com.opencloud.sleetck.lib.testsuite.profiles.simpleprofile.SimpleProfileProxy;

/**
 * Test assertion 987, that an SBB will receive a security exception when trying to invoke the setter method of a
 * profile's CMP interface
 */
public class ProfileSbbSecurityTest extends AbstractSleeTCKTest {

    protected static final String PROFILE_TABLE_NAME = "tck.ProfilesSbbSecurityTest.table";

    /**
     * Create a profile table and fires an event to ProfileSecurityTestSbb to test that the profile setter returns
     * the correct exception.
     */
    public TCKTestResult run() throws Exception {
        // create the profile table
        ProfileProvisioningMBeanProxy profileProvisioning = profileUtils.getProfileProvisioningProxy();
        ProfileSpecificationID profileSpecID = new ComponentIDLookup(utils()).lookupProfileSpecificationID(
                ProfileTestConstants.SIMPLE_PROFILE_SPEC_NAME, SleeTCKComponentConstants.TCK_VENDOR, "1.0");
        getLog().info("Creating profile table: " + PROFILE_TABLE_NAME);
        profileProvisioning.createProfileTable(profileSpecID, PROFILE_TABLE_NAME);
        tableCreated = true;

        // add profile
        final String name = "A";
        getLog().info("Creating profile " + name);
        javax.management.ObjectName profile = profileProvisioning.createProfile(PROFILE_TABLE_NAME, name);
        ProfileMBeanProxy profProxy = utils().getMBeanProxyFactory().createProfileMBeanProxy(profile);
        profProxy.commitProfile();

        // install resource listener
        QueuingResourceListener listener = new QueuingResourceListener(utils());
        setResourceListener(listener);

        TCKResourceTestInterface resource = utils().getResourceInterface();

        getLog().fine("Creaing activity");
        TCKActivityID activityID = resource.createActivity(getClass().getName());

        getLog().info("Firing event");

        // Send an event
        resource.fireEvent(TCKResourceEventX.X1, TCKResourceEventX.X1, activityID, null);

        // Check for reply to X1
        listener.nextMessage().getMessage();
        return TCKTestResult.passed();
    }

    public void setUp() throws Exception {
        String duPath = utils().getTestParams().getProperty(ProfileTestConstants.PROFILE_SPEC_DU_PATH_PARAM);
        getLog().fine("Installing profile specification");
        utils().install(duPath);
        profileUtils = new ProfileUtils(utils());
        activeProxies = new Vector();
        super.setUp();
    }

    public void tearDown() throws Exception {
        // close profiles
        if (activeProxies != null) {
            getLog().fine("Closing profiles");
            Iterator activeProxiesIter = activeProxies.iterator();
            while (activeProxiesIter.hasNext()) {
                SimpleProfileProxy aProxy = (SimpleProfileProxy) activeProxiesIter.next();
                try {
                    if (aProxy != null) {
                        if (aProxy.isProfileWriteable()) aProxy.restoreProfile();
                        aProxy.closeProfile();
                    }
                } catch (Exception ex) {
                    getLog().warning("Exception caught while trying to close profiles:");
                    getLog().warning(ex);
                }
            }
        }
        // delete profile tables
        try {
            if (profileUtils != null && tableCreated) {
                getLog().fine("Removing profile table");
                profileUtils.removeProfileTable(PROFILE_TABLE_NAME);
            }
        } catch (Exception e) {
            getLog().warning("Caught exception while trying to remove profile table:");
            getLog().warning(e);
        }
        super.tearDown();
    }

    private ProfileUtils profileUtils;
    private boolean tableCreated = false;
    private Vector activeProxies;

}
