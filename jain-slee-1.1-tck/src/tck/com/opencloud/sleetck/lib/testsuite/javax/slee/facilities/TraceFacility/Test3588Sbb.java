/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.javax.slee.facilities.TraceFacility;

import com.opencloud.sleetck.lib.resource.events.TCKResourceEventX;
import com.opencloud.sleetck.lib.sbbutils.BaseTCKSbb;
import com.opencloud.sleetck.lib.sbbutils.TCKSbbUtils;

import javax.naming.InitialContext;
import javax.slee.ActivityContextInterface;
import javax.slee.facilities.TraceFacility;
import java.util.HashMap;

public abstract class Test3588Sbb extends BaseTCKSbb {

    private static final String JNDI_TIMERFACILITY_NAME = "java:comp/env/slee/facilities/trace";

    // Initial event
    public void onTCKResourceEventX1(TCKResourceEventX ev, ActivityContextInterface aci) {

        try {
            HashMap map = new HashMap();
            
            boolean passed = false;
            TraceFacility facility = (TraceFacility) new InitialContext().lookup(JNDI_TIMERFACILITY_NAME);
            try {
                facility.getTraceLevel(null);
            } catch (NullPointerException e) {
                passed = true;
            }

            map.put("Result", new Boolean(passed));
            if (passed == false) {
                map.put("Message", "TraceFacility.getTraceLevel(ComponentID) with null component ID didn't throw exception.");
                map.put("ID", new Integer(3588));
            }

            TCKSbbUtils.getResourceInterface().sendSbbMessage(map);
            return;

        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }

    }
}

