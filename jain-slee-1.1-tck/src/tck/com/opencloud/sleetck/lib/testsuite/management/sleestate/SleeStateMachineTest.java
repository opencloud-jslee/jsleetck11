/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.management.sleestate;

import com.opencloud.sleetck.lib.*;
import com.opencloud.sleetck.lib.resource.TCKActivityID;
import com.opencloud.sleetck.lib.resource.TCKSbbMessage;
import com.opencloud.sleetck.lib.resource.events.TCKResourceEventX;
import com.opencloud.sleetck.lib.resource.testapi.TCKResourceTestInterface;
import com.opencloud.sleetck.lib.testutils.BaseTCKResourceListener;
import com.opencloud.sleetck.lib.testutils.jmx.SleeManagementMBeanProxy;
import com.opencloud.sleetck.lib.testutils.statetracking.DirectionalGraph;
import com.opencloud.util.SleepUtil;

import javax.slee.InvalidStateException;
import javax.slee.management.ManagementException;
import javax.slee.management.SleeState;
import javax.slee.management.SleeStateChangeNotification;
import java.rmi.RemoteException;
import java.util.Arrays;

/**
 * Tests assertions related to the SLEE state machine:
 * 1469,1486,1489,1983,1883,1887,1483,1487,1490,4169,4171,4022, and 4028
 */
public class SleeStateMachineTest extends AbstractSleeTCKTest {

    // -- Test Parameters -- //

    private static final String WAIT_DURING_STOPPING_PARAM = "waitDuringStopping";

    // -- Implementation of SleeTCKTest methods -- //

    public TCKTestResult run() throws Exception {
        // confirm that we're in the RUNNING state
        SleeState currentState = getCurrentState();
        if(!currentState.isRunning()) throw new TCKTestErrorException(
            "Expected SLEE to be in RUNNING state as a pre-condition of TCK tests. Current state:"+currentState);
        getLog().info("In the RUNNING state");

        // create activity A and attach an Sbb to it by sending it an initial event on A
        getLog().info("Firing an event on activity A to attach an Sbb to A");
        TCKResourceTestInterface resource = utils().getResourceInterface();
        TCKActivityID activityID = resource.createActivity("tck.SleeStateMachineTest.ActivityA");
        resource.fireEvent(TCKResourceEventX.X1,null,activityID,null);
        waitForSbbAttach();
        getLog().info("The Sbb is attached to the activity");

        attemptInvalidStart();

        // call stop()
        try {
            getLog().info("Calling stop()");
            management.stop(); 
        } catch (InvalidStateException ise) {
            throw new TCKTestFailureException(4028,
                "SleeManagementMBean.stop() threw an InvalidStateException from the RUNNING state",ise);
        }

        // the stop() method should not block. test that we have not shifted to the STOPPED state already - i.e. 
        // check that stop() did not block
        if(getCurrentState().isStopped()) {
            if(resourceListener.hasActivityEnded()) getLog().warning(
                "The SLEE automatically detached the activity, presumably as a result of a timeout, and appeared to do so while "+
                "during the stop() method. If the time taken in stop() was large, this indicated an illegally blocking implementation "+
                "of stop(). If small, this indicates an overly small timeout for ending activities.");
            else throw new TCKTestFailureException(1886,"The SLEE moved to the STOPPED state before all activities were ended");
        }

        // wait for the STOPPING state
        waitForStateChange(1490,SleeState.STOPPING,false);
        if(getCurrentState().isRunning()) throw new TCKTestFailureException(4166,"The SLEE fired a state change notification for the "+
          "STOPPING state before leaving the RUNNING state.");
        getLog().info("Waiting "+waitDuringStopping+" ms in the STOPPING state to ensure the SLEE doesn't "+
            "move to the STOPPED state before the activity has ended");
        SleepUtil.sleepFor(waitDuringStopping);
        currentState = getCurrentState();
        getLog().info("Stopped waiting. Now in the "+currentState+" state");
        if(!currentState.isStopping()) {
            if(currentState.isStopped()) {
                if(resourceListener.hasActivityEnded()) getLog().warning(
                    "The SLEE automatically detached the activity, presumably as a result of a timeout");
                else throw new TCKTestFailureException(1886,"The SLEE moved to the STOPPED state before all activities were ended");
            } else throw new TCKTestErrorException("The SLEE unexpected changed to the "+currentState+" state (sometime after entering the STOPPED state)");
        }

        if(!currentState.isStopped()) {
            attemptInvalidStart();
            attemptInvalidStop();
        }

        getLog().info("Ending the activity to trigger a change to the STOPPED state (if not changed already)");
        if(resource.isLive(activityID)) {
            try {
                resource.endActivity(activityID);
            } catch (Exception ex) {
                getLog().warning("Caught Exception while trying to end the activity:");
                getLog().warning(ex);
                // allow for the race condition where another thread (i.e. the SLEE) could end the activity between
                // our calls to isLive and endActivity: only rethow if the activity is still live
                if(resource.isLive(activityID)) throw ex;
            }
        }

        // wait for the STOPPED state
        waitForStateChange(1887,SleeState.STOPPED,true);
        currentState = getCurrentState();
        if(currentState.isRunning() || currentState.isStopping()) throw new TCKTestFailureException(4166,
            "The SLEE fired a state change notification for the STOPPED state before entering that state.");
        attemptInvalidStop();

        // call start()
        try {
            getLog().info("Calling start() from the STOPPED state");
            management.start();
        } catch (InvalidStateException ise) {
            throw new TCKTestFailureException(4022,"SleeManagementMBean.start() threw an InvalidStateException from the STOPPED state",ise);
        }

        // wait for the STARTING state
        waitForStateChange(1487,SleeState.STARTING,false);
        currentState = getCurrentState();
        if(currentState.isStopped()) throw new TCKTestFailureException(4166,"The SLEE fired a state change notification for the "+
          "STARTING state before leaving the STOPPED state.");
        if(!currentState.isStarting() && !currentState.isRunning()) throw new TCKTestFailureException(1487,
            "The SLEE was in an unexpected state after receiving a notification of change to the STARTING state:"+currentState);
        attemptInvalidStart();

        // wait for the RUNNING state
        waitForStateChange(1883,SleeState.RUNNING,true);
        currentState = getCurrentState();
        if(currentState.isStopped() || currentState.isStarting()) throw new TCKTestFailureException(4166,
            "The SLEE fired a state change notification for the RUNNING state before entering that state.");
        getLog().info("Back in the RUNNING state");
        if(exceptionFromResource != null) return TCKTestResult.error("Exception received from TCK resource",exceptionFromResource);
        return TCKTestResult.passed();
    }

    public void setUp() throws Exception {
        buildStateMachineModel();
        resourceListener = new ResourceListenerImpl();
        setResourceListener(resourceListener);
        management = utils().getSleeManagementMBeanProxy();
        stateListener = new QueuingSleeStateListener(utils());
        management.addNotificationListener(stateListener, null, null);
        waitDuringStopping = Long.parseLong(utils().getTestParams().getProperty(WAIT_DURING_STOPPING_PARAM));
        super.setUp();
    }

    public void tearDown() throws Exception {
        if(management != null) management.removeNotificationListener(stateListener);
        stateListener = null;
        resourceListener = null;
        management = null;
        super.tearDown();
    }

    // Private methods

    /**
     * Waits until the next SleeStateChangeNotification is received, then validates the transition.
     * If checkNoFurtherTransitions, this method also confirms that no further state change nofications
     * have been received, and that the SLEE in the new state specified by the notification.
     * @param assertionReArrival the assertion ID with which to fail if the SLEE fails to shift states
     * @param expectedState the expected state to shift to
     * @param checkNoFurtherTransitions whether to check for, and fail on, further state transitions
     */
    private void waitForStateChange(int assertionReArrival, SleeState expectedState,
            boolean checkNoFurtherTransitions) throws TCKTestFailureException, TCKTestErrorException, ManagementException {
        // wait for the state change
        SleeStateChangeNotification stateChange = null;
        try {
            getLog().info("Waiting to move to the "+expectedState+" state");
            stateChange = stateListener.nextNotification();
            getLog().info("Found state change: "+stateChange);
        } catch(OperationTimedOutException toe) {
            SleeState currentState = getCurrentState();
            // if the state has changed since our last notification, we have missed a notification
            if(previousNewState != null && !currentState.equals(previousNewState))
                throw new TCKTestFailureException(1469,"The SLEE moved from the "+previousNewState+
                " state to the "+currentState+" state without delivering a SleeStateChangeNotification");
            // otherwise we simply timed out waiting for the notification
            else throw new TCKTestFailureException(assertionReArrival,"Timed out waiting for the "+expectedState+" state");
        }
        // validate the state change
        checkTransition(stateChange);
        if(!stateChange.getNewState().equals(expectedState)) throw new TCKTestErrorException(
            "Received a legal but unexpected state change. Expected state: "+expectedState+". State change received: "+stateChange);

        // Verify that stateChange.toString() is valid
        try {
            stateChange.toString();
        } catch (Exception e) {
            throw new TCKTestFailureException(4173, "SleeStateChangeNotification.toString() threw exception.");
        }

        if (stateChange.toString() == null)
            throw new TCKTestFailureException(4173, "SleeStateChangeNotification.toString() returned null.");

        // further checks if checkNoFurtherTransitions is true
        if(checkNoFurtherTransitions) {
            if(stateListener.hasNotifications()) {
                // we received an unallowed further state change
                stateChange = stateListener.nextNotification();
                checkTransition(stateChange);
                throw new TCKTestErrorException("The SLEE transitioned unexpectedly from the "+stateChange.getOldState()+
                    " to the "+stateChange.getNewState()+" state");
            } else {
                // check that the current state matches the new state of the notification
                SleeState currentState = getCurrentState();
                if(!stateChange.getNewState().equals(currentState)) throw new TCKTestFailureException(4169,
                    "SleeManagementMBean.getState() returns "+currentState+", but last received state Notification was to the "+previousNewState+" state");
            }
        }
    }

    private void checkTransition(SleeStateChangeNotification stateChange) throws TCKTestFailureException {
        SleeState oldState = stateChange.getOldState();
        SleeState newState = stateChange.getNewState();
        if(oldState == null) throw new TCKTestFailureException(4169,"SleeStateChangeNotification.getOldState() returned null. Notification:"+stateChange);
        if(newState == null) throw new TCKTestFailureException(4171,"SleeStateChangeNotification.getNewState() returned null. Notification:"+stateChange);
        if(!validChangesGraph.isEdge(oldState.toInt(),newState.toInt())) throw new TCKTestFailureException(
            1983,"The SLEE sent a notification which describes an invalid state change from the "+oldState+
                " state to the "+newState+" state");
        if(previousNewState != null && !oldState.equals(previousNewState)) {
            throw new TCKTestFailureException(4171,"SleeStateChangeNotification.getOldState() returned "+oldState+
                ", which does not match the state returned by the getNewState() from the previous notification: "+previousNewState);
        }
        previousNewState = newState;
    }

    /**
     * Checks:
     * (a) Assertion 4022, that start() throws an InvalidStateException when invoked
     * from the STARTING, RUNNING and STOPPING states, and
     * (b) Assertion 1486, that start() has no effect on the SLEE state when invoked from
     * the STARTING, RUNNING or STOPPING states.
     *
     * Pre-conditions: the SLEE is in the STARTING, RUNNING or STOPPING state
     */
    private void attemptInvalidStart() throws Exception {
        SleeState preCallState = getCurrentState();
        try {
            if(preCallState.isStopped()) throw new TCKTestErrorException("attemptInvalidStart() may not be called from the STOPPED state");
            getLog().info("Attempting invalid call to start() from the "+preCallState+" state");
            management.start();
            throw new TCKTestFailureException(4022,"ManagementMBean.start() did not throw an InvalidStateException when invoked from the "+preCallState+" state");
        } catch (InvalidStateException e) {
            getLog().info("Caught expected InvalidStateException from SleeManagementMBean.start() in "+preCallState+" state");
            // check that the state hasn't changed as a result of our call
            SleeState postCallState = getCurrentState();
            if( !postCallState.equals(preCallState) &&
                !(preCallState.isStarting() && postCallState.isRunning()) && // a valid, spontaneous transition
                !(preCallState.isStopping() && postCallState.isStopped()) ) {// a valid, spontaneous transition
                throw new TCKTestFailureException(1486,"SleeManagementMBean.start() threw the expected InvalidStateException, but "+
                 "resulted in an unexpected state change from the "+preCallState+" state to the "+postCallState+ " state");
            }
        }
    }

    /**
     * Checks:
     * (a) Assertion 4028, that stop() throws an InvalidStateException when invoked
     * from the STOPPED and STOPPING states, and
     * (b) Assertion 1489, that stop() has no effect on the SLEE state when invoked from the STOPPED or STOPPING state
     *
     * Pre-conditions: the SLEE is in the STOPPED or STOPPING state
     */
    private void attemptInvalidStop() throws Exception {
        SleeState preCallState = getCurrentState();
        try {
            if(!preCallState.isStopped() && !preCallState.isStopping()) throw new TCKTestErrorException(
                "attemptInvalidStop() may only be called from the STOPPED or STOPPING state");
            getLog().info("Attempting invalid call to stop() from the "+preCallState+" state");
            management.stop();
            throw new TCKTestFailureException(4028,"ManagementMBean.stop() did not throw an InvalidStateException when invoked from the "+preCallState+" state");
        } catch (InvalidStateException e) {
            getLog().info("Caught expected InvalidStateException from SleeManagementMBean.stop() in "+preCallState+" state");
            // check that the state hasn't changed as a result of our call
            SleeState postCallState = getCurrentState();
            if( !postCallState.equals(preCallState) &&
                !(preCallState.isStopping() && postCallState.isStopped()) ) {// a valid, spontaneous transition
                throw new TCKTestFailureException(1489,"SleeManagementMBean.stop() threw the expected InvalidStateException, but "+
                 "resulted in an unexpected state change from the "+preCallState+" state to the "+postCallState+ " state");
            }
        }
    }

    /**
     * Waits for the Sbb to receive the event (and therfore become attached to the activity)
     */
    private void waitForSbbAttach() throws OperationTimedOutException {
        synchronized (resourceListener) {
            long now = System.currentTimeMillis();
            long timeoutAt = now + utils().getTestTimeout();
            while(timeoutAt > now && !resourceListener.hasSbbAttached()) {
                try {
                    resourceListener.wait(timeoutAt - now);
                    now = System.currentTimeMillis();
                } catch (InterruptedException ie) { /* no-op */ }
            }
            if(!resourceListener.hasSbbAttached()) throw new OperationTimedOutException(
                "Timed out while waiting for the Sbb to receive an event");
        }
    }

    /**
     * Retrives, checks and returns the current SLEE state
     */
    private SleeState getCurrentState() throws TCKTestFailureException, TCKTestErrorException, ManagementException {
        SleeState currentState = management.getState();
        if(currentState == null) throw new TCKTestFailureException(1483,"SleeManagementMBean.getState() returned null");
        return currentState;
    }

    /**
     * Builds the model of the slee provider's state machine
     * in the state tracker by adding all valid state transitions.
     */
    private void buildStateMachineModel() {
        int[] allStates = {SleeState.SLEE_STOPPED,SleeState.SLEE_STARTING,SleeState.SLEE_RUNNING,SleeState.SLEE_STOPPING};
        Arrays.sort(allStates);
        validChangesGraph = new DirectionalGraph(allStates);
        validChangesGraph.addEdge(SleeState.SLEE_STOPPED,SleeState.SLEE_STARTING);
        validChangesGraph.addEdge(SleeState.SLEE_STARTING,SleeState.SLEE_RUNNING);
        validChangesGraph.addEdge(SleeState.SLEE_STARTING,SleeState.SLEE_STOPPING);
        validChangesGraph.addEdge(SleeState.SLEE_RUNNING,SleeState.SLEE_STOPPING);
        validChangesGraph.addEdge(SleeState.SLEE_STOPPING,SleeState.SLEE_STOPPED);
    }

    // -- Private classes -- //

    private class ResourceListenerImpl extends BaseTCKResourceListener {

        public synchronized void onSbbMessage(TCKSbbMessage message, TCKActivityID calledActivity) throws RemoteException {
            hasSbbAttached = true;
            getLog().info("ResourceListenerImpl: received message from Sbb");
            notifyAll();
        }

        public synchronized void onActivityContextInvalid(TCKActivityID activityID) throws RemoteException {
            hasActivityEnded = true;
            getLog().info("ResourceListenerImpl: received notification of activity end");
            notifyAll();
        }

        public void onException(Exception e) {
            exceptionFromResource = e;
            utils().getLog().warning("Received Exception from resource:");
            utils().getLog().warning(e);
        }

        /**
         * Returns true if and only if the listener has received a message from the Sbb
         * (i.e. the Sbb has been attached to the activity)
         */
        public synchronized boolean hasSbbAttached() {
            return hasSbbAttached;
        }

        /**
         * Returns true if and only if the listener has received notification that the TCK activity
         * has ended
         */
        public synchronized boolean hasActivityEnded() {
            return hasActivityEnded;
        }

        private boolean hasSbbAttached;
        private boolean hasActivityEnded;

    }

    // -- Private state -- //

    private ResourceListenerImpl resourceListener;
    private QueuingSleeStateListener stateListener;
    private SleeManagementMBeanProxy management;
    private DirectionalGraph validChangesGraph;
    private long waitDuringStopping;
    private Exception exceptionFromResource;
    private SleeState previousNewState;

}
