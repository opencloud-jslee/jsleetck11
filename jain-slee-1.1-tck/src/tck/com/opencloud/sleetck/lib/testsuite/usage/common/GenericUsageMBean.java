/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.usage.common;

import javax.slee.usage.SbbUsageMBean;
import javax.slee.usage.SampleStatistics;
import javax.slee.management.ManagementException;

/**
 * This interface is a coded representation of the MBean interface
 * which the SLEE should generate for GenericUsageParameterInterface.
 * This file is used by the TCK tools in the generation
 * of the proxy class for that interface.
 */
public interface GenericUsageMBean extends SbbUsageMBean {

    public long getFirstCount(boolean reset) throws ManagementException;

    public long getSecondCount(boolean reset) throws ManagementException;

    public SampleStatistics getTimeBetweenNewConnections(boolean reset) throws ManagementException;

    public SampleStatistics getTimeBetweenErrors(boolean reset) throws ManagementException;

}
