/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.resource;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.slee.ActivityContextInterface;
import javax.slee.CreateException;
import javax.slee.RolledBackContext;
import javax.slee.Sbb;
import javax.slee.SbbContext;
import javax.slee.facilities.Tracer;

import com.opencloud.sleetck.lib.rautils.TCKRAConstants;

public abstract class SimpleSbb implements Sbb {

    // Event handler
    public void onSimpleEvent(SimpleEvent event, ActivityContextInterface aci) {
        tracer.info("onSimpleEvent() event handler called.");
        aci.getActivity();
        try {
            SimpleSbbInterface sbbInterface = getSbbInterface();
            sbbInterface.logTestString("Test method invocation to force getResourceAdaptorInterface() to be called");
        } catch (Exception e) {
            tracer.severe("Exception caught while looking up SBB Interface from JNDI", e);
        }
    }

    public void sbbActivate() {
    }

    public void sbbCreate() throws CreateException {
    }

    public void sbbExceptionThrown(Exception exception, Object event, ActivityContextInterface aci) {
    }

    public void sbbLoad() {
    }

    public void sbbPassivate() {
    }

    public void sbbPostCreate() throws CreateException {
    }

    public void sbbRemove() {
    }

    public void sbbRolledBack(RolledBackContext context) {
    }

    public void sbbStore() {
    }

    public void setSbbContext(SbbContext context) {
        this.tracer = context.getTracer("SimpleSBB");
    }

    private SimpleSbbInterface getSbbInterface() throws NamingException {
        return (SimpleSbbInterface) getSbbEnvironment().lookup(TCKRAConstants.SIMPLE_RESOURCE_ADAPTOR_LOCATION);
    }

    private Context getSbbEnvironment() throws NamingException {
        Context initCtx = new InitialContext();
        return (Context) initCtx.lookup("java:comp/env");
    }

    public void unsetSbbContext() {
        tracer = null;
    }

    private Tracer tracer;

}
