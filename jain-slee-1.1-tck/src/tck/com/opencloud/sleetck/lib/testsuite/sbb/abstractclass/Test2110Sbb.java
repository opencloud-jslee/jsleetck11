/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.sbb.abstractclass;

import javax.slee.ActivityEndEvent;
import javax.slee.ActivityContextInterface;
import javax.slee.SbbContext;
import javax.slee.Address;
import javax.slee.SbbLocalObject;
import javax.slee.facilities.Level;
import javax.slee.ChildRelation;
import javax.slee.CreateException;

import com.opencloud.sleetck.lib.sbbutils.BaseTCKSbb;
import com.opencloud.sleetck.lib.sbbutils.TCKSbbUtils;
import com.opencloud.sleetck.lib.sbbutils.TCKSbbConstants;
import com.opencloud.sleetck.lib.resource.events.TCKResourceEventX;
import com.opencloud.sleetck.lib.resource.sbbapi.TCKResourceSbbInterface;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Collection;
import java.util.Vector;

public abstract class Test2110Sbb extends BaseTCKSbb {

    // Initial event
    public void onTCKResourceEventX1(TCKResourceEventX ev, ActivityContextInterface aci) {

        try {

            HashMap map = new HashMap();

            SbbLocalObject children[] = new SbbLocalObject[10];
            for (int i = 0; i < 10; i++)
                children[i] = getChildRelation().create();

            // clear(), remove(Object), removeAll(Collection), retainAll(Collection)


            Collection kids = getChildRelation();

            kids.remove(children[9]);
            if (kids.size() != 9) {
                map.put("Result", new Boolean(false));
                map.put("Message", "ChildRelation().remove(SbbLocalObject) non-functional");
                TCKSbbUtils.getResourceInterface().sendSbbMessage(map);
                return;
            }

            Vector v = new Vector();
            v.add(children[8]);
            v.add(children[7]);
            kids.removeAll(v);
            if (kids.size() != 7) {
                map.put("Result", new Boolean(false));
                map.put("Message", "ChildRelation().removeAll(Collection) non-functional");
                TCKSbbUtils.getResourceInterface().sendSbbMessage(map);
                return;
            }

            v = new Vector();
            v.add(children[0]);
            v.add(children[1]);
            kids.retainAll(v);
            if (kids.size() != 2) {
                map.put("Result", new Boolean(false));
                map.put("Message", "ChildRelation().retainAll(Collection) non-functional");
                TCKSbbUtils.getResourceInterface().sendSbbMessage(map);
                return;
            }

            kids.clear();
            if (kids.size() != 0) {
                map.put("Result", new Boolean(false));
                map.put("Message", "ChildRelation().clear() non-functional");
                TCKSbbUtils.getResourceInterface().sendSbbMessage(map);
                return;
            }

            map.put("Result", new Boolean(true));
            map.put("Message", "Ok");
            TCKSbbUtils.getResourceInterface().sendSbbMessage(map);
            return;

        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }
    }

    // ChildRelation method for the Child SBB.
    public abstract ChildRelation getChildRelation();

}
