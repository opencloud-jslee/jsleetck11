/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.facilities.profilefacility;

public interface Test1397ProfileCMP {

    public void setStringVal(String value);
    public String getStringVal();

    public void setIntVal(int value);
    public int getIntVal();

    public void setStringArray(String [] value);
    public String [] getStringArray();

    public void setIntArray(int [] value);
    public int [] getIntArray();

}
