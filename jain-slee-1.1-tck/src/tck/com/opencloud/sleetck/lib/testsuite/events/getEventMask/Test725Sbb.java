/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.events.getEventMask;

import java.util.HashMap;

import javax.slee.ActivityContextInterface;

import com.opencloud.sleetck.lib.resource.events.TCKResourceEventX;
import com.opencloud.sleetck.lib.sbbutils.BaseTCKSbb;
import com.opencloud.sleetck.lib.sbbutils.TCKSbbUtils;

public abstract class Test725Sbb extends BaseTCKSbb {

    // Initial event method.
    public void onTCKResourceEventX1(TCKResourceEventX ev, ActivityContextInterface aci) {

        try {

            HashMap map = new HashMap();

            // Fetch the set of masked events.
            String[] maskedEvents = getSbbContext().getEventMask(aci);

            if (maskedEvents.length == 1 && maskedEvents[0].equals("Test725Event")) {
                map.put("Result", new Boolean(true));
                map.put("Message", "Ok");
                TCKSbbUtils.getResourceInterface().sendSbbMessage(map);
                return;
            }

            map.put("Result", new Boolean(false));
            if (maskedEvents.length == 0)
                map.put("Message","There were no masked events, Test725Event should be masked.");

            if (maskedEvents.length >= 2)
                map.put("Message","There were " + maskedEvents.length + "masked events, when there should have been only one: Test725Event.");

            if (maskedEvents.length == 1)
                map.put("Message", "The masked event was " + maskedEvents[0] + ", it should have been Test725Event.");

            TCKSbbUtils.getResourceInterface().sendSbbMessage(map);
        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }
    }

    public void onTCKResourceEventX2(TCKResourceEventX event, ActivityContextInterface aci) {
    }

    public void onTest725Event(Test725Event event, ActivityContextInterface aci) {
    }

}
