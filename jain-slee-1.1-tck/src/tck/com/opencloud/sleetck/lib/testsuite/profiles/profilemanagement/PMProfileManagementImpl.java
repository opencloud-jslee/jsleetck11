/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.profiles.profilemanagement;

import javax.slee.profile.ProfileManagement;
import javax.slee.profile.ProfileVerificationException;
import javax.slee.profile.ProfileID;

/**
 * Define a profile management abstract class for the PM profile
 */
public abstract class PMProfileManagementImpl
        implements PMProfileCMP, ProfileManagement, PMProfileManagement {

    public void profileInitialize() {
        setValue(PMProfileCMP.INITIALIZED_VALUE);
    }

    public void profileLoad() { }

    public void profileStore() { }

    public void profileVerify() throws ProfileVerificationException { }

    public void testExceptionWrapper() {
        throw new RuntimeException("test exception");
    }

    public void testMarkDirty() {
        this.markProfileDirty();
    }

    public boolean testIsProfileValid(ProfileID profileID) {
        return this.isProfileValid(profileID);
    }

    public void setValueIndirectly(String value) {
        setValue(value);
    }
}
