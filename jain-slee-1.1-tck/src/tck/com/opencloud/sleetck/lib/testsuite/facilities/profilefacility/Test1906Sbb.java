/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.facilities.profilefacility;

import com.opencloud.sleetck.lib.resource.events.TCKResourceEventX;
import com.opencloud.sleetck.lib.sbbutils.BaseTCKSbb;
import com.opencloud.sleetck.lib.sbbutils.TCKSbbUtils;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.slee.ActivityContextInterface;
import javax.slee.ActivityEndEvent;
import javax.slee.Address;
import javax.slee.facilities.Level;
import javax.slee.profile.*;
import java.util.HashMap;

public abstract class Test1906Sbb extends BaseTCKSbb {

    private static final String JNDI_PROFILEFACILITY_NAME = "java:comp/env/slee/facilities/profile";
    public static final String PROFILE_TABLE_NAME = "Test1906ProfileTable";

    // Initial event
    public void onTCKResourceEventX1(TCKResourceEventX ev, ActivityContextInterface aci) {

        try {
            Context env = TCKSbbUtils.getSbbEnvironment();

            ProfileFacility facility = (ProfileFacility) new InitialContext().lookup(JNDI_PROFILEFACILITY_NAME);
            ProfileTableActivityContextInterfaceFactory factory = (ProfileTableActivityContextInterfaceFactory) env.lookup("slee/facilities/profiletableactivitycontextinterfacefactory");

            ProfileTableActivity activity = facility.getProfileTableActivity(PROFILE_TABLE_NAME);
            ActivityContextInterface profileTableACI = factory.getActivityContextInterface(activity);
            profileTableACI.attach(getSbbContext().getSbbLocalObject());
            
            TCKSbbUtils.createTrace(getSbbID(), Level.FINE, "Attached to Profile Table ACI.", null);

            fireTest1906Event(new Test1906Event(), aci, null);
        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }
    }

    public void onTest1906Event(Test1906Event event, ActivityContextInterface aci) {
        
        try {
            HashMap map = new HashMap();
            map.put("Type", "RemoveProfile");

            TCKSbbUtils.getResourceInterface().sendSbbMessage(map);
        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }

    }

    public void onActivityEndEvent(ActivityEndEvent event, ActivityContextInterface aci) {
        try {
            TCKSbbUtils.createTrace(getSbbID(), Level.FINE, "In onActivityEndEvent.", null);
            HashMap map = new HashMap();
            map.put("Type", "Result");
            map.put("Result", Boolean.TRUE);
            TCKSbbUtils.getResourceInterface().sendSbbMessage(map);
        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }
    }

    public void onProfileAddedEvent(ProfileAddedEvent event, ActivityContextInterface aci) {
        try {
            TCKSbbUtils.createTrace(getSbbID(), Level.FINE, "In onProfileAddedEvent.", null);
        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }
    }

    public void onProfileRemovedEvent(ProfileRemovedEvent event, ActivityContextInterface aci) {
        try {
            TCKSbbUtils.createTrace(getSbbID(), Level.FINE, "In onProfileRemovedEvent.", null);
        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }
    }

    public abstract Test1906ProfileCMP getProfileCMP(ProfileID profileID) throws UnrecognizedProfileTableNameException, UnrecognizedProfileNameException;
    public abstract void fireTest1906Event(Test1906Event event, ActivityContextInterface aci, Address address);
}
