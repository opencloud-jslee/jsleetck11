/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.profiles.profileadded;

import com.opencloud.sleetck.lib.SleeTCKTest;
import com.opencloud.sleetck.lib.SleeTCKTestUtils;
import com.opencloud.sleetck.lib.TCKTestResult;
import com.opencloud.sleetck.lib.resource.TCKActivityID;
import com.opencloud.sleetck.lib.resource.TCKSbbMessage;
import com.opencloud.sleetck.lib.resource.testapi.TCKResourceListener;
import com.opencloud.sleetck.lib.testutils.BaseTCKResourceListener;
import com.opencloud.sleetck.lib.testutils.FutureResult;
import com.opencloud.sleetck.lib.testutils.ProfileUtils;
import com.opencloud.sleetck.lib.testutils.jmx.DeploymentMBeanProxy;
import com.opencloud.sleetck.lib.testutils.jmx.ProfileMBeanProxy;
import com.opencloud.sleetck.lib.testutils.jmx.ProfileProvisioningMBeanProxy;

import javax.management.Attribute;
import javax.slee.ComponentID;
import javax.slee.management.DeployableUnitDescriptor;
import javax.slee.management.DeployableUnitID;
import javax.slee.profile.ProfileSpecificationID;
import java.rmi.RemoteException;
import java.util.HashMap;

public class Test2073Test implements SleeTCKTest {

    private static final String SERVICE_DU_PATH_PARAM = "DUPath";
    private static final int TEST_ID = 2073;

    public void init(SleeTCKTestUtils utils) {
        this.utils = utils;
    }

    /**
     * Perform the actual test.
     */

    public TCKTestResult run() throws Exception {
        result = new FutureResult(utils.getLog());

        // Fire an initial event.
        /*
        TCKResourceTestInterface resource = utils.getResourceInterface();
        TCKActivityID activityID = resource.createActivity("Test2073InitialActivity");
        resource.fireEvent(TCKResourceEventX.X1, TCKResourceEventX.X1, activityID, null);
        */

        // Create the profile table.
        profileProxy = new ProfileUtils(utils).getProfileProvisioningProxy();
        DeploymentMBeanProxy duProxy = utils.getDeploymentMBeanProxy();
        DeployableUnitDescriptor duDesc = duProxy.getDescriptor(duID);
        ComponentID components[] = duDesc.getComponents();

        for (int i = 0; i < components.length; i++) {
            if (components[i] instanceof ProfileSpecificationID) {
                profileProxy.createProfileTable((ProfileSpecificationID) components[i], "Test2073ProfileTable");
            }
        }

        // Create a profile, set some values, then commit it.
        javax.management.ObjectName profile = profileProxy.createProfile("Test2073ProfileTable", "Test2073Profile");
        ProfileMBeanProxy proxy = utils.getMBeanProxyFactory().createProfileMBeanProxy(profile);

        // Set 'value' to '42'
        utils.getMBeanFacade().setAttribute(profile, new Attribute("Value", new Integer(42)));

        // Commit the profile, this should generate a ProfileAddedEvent
        proxy.commitProfile();

        return result.waitForResultOrFail(utils.getTestTimeout(), "Timeout waiting for test result", TEST_ID);
    }

    /**
     * Do all the pre-run configuration of the test.
     */

    public void setUp() throws Exception {

        utils.getLog().fine("Connecting to resource");
        resourceListener = new TCKResourceListenerImpl();
        utils.getResourceInterface().setResourceListener(resourceListener);

        utils.getLog().fine("Installing and activating service");

        // Install the Deployable Units
        String duPath = utils.getTestParams().getProperty(SERVICE_DU_PATH_PARAM);
        duID = utils.install(duPath);

        // Start the DU's service.
        utils.activateServices(duID, true);
    }

    /**
     * Clean up after the test.
     */

    public void tearDown() throws Exception {

        try {
            profileProxy.removeProfileTable("Test2073ProfileTable");
        } catch (Exception e) {
        }

        utils.getLog().fine("Disconnecting from resource");
        utils.getResourceInterface().clearActivities();
        utils.getResourceInterface().removeResourceListener();
        utils.getLog().fine("Deactivating and uninstalling service");
        utils.deactivateAllServices();
        utils.uninstallAll();
    }

    private class TCKResourceListenerImpl extends BaseTCKResourceListener {
        public synchronized void onSbbMessage(TCKSbbMessage message, TCKActivityID activity) throws RemoteException {
            utils.getLog().info("Received message from SBB: " + message.getMessage());

            HashMap map = (HashMap) message.getMessage();

            String type = (String) map.get("Type");
            if (type != null) {
                try {
                    profileProxy.removeProfile("Test2073ProfileTable", "Test2073Profile");
                } catch (Exception e) {
                    onException(e);
                }

            } else {
                Boolean passed = (Boolean) map.get("Result");
                String msgString = (String) map.get("Message");
                Integer id = (Integer) map.get("ID");
                
                if (passed.booleanValue() == true)
                    result.setPassed();
                else
                    result.setFailed(id.intValue(), msgString);
            }
        }

        public void onException(Exception e) throws RemoteException {
            utils.getLog().warning("Received exception from SBB.");
            utils.getLog().warning(e);
        result.setError(e);
        }

    }

    private SleeTCKTestUtils utils;
    private TCKResourceListener resourceListener;
    private FutureResult result;
    private DeployableUnitID duID;
    private ProfileProvisioningMBeanProxy profileProxy;
}
