/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.sbb.abstractclass;

import com.opencloud.sleetck.lib.*;
import com.opencloud.sleetck.lib.testutils.jmx.MBeanFacade;
import com.opencloud.sleetck.lib.testutils.ExceptionsUtil;
import com.opencloud.sleetck.lib.testutils.BaseTCKResourceListener;
import com.opencloud.sleetck.lib.testutils.QueuingResourceListener;
import com.opencloud.sleetck.lib.resource.TCKActivityID;
import com.opencloud.sleetck.lib.resource.testapi.TCKResourceTestInterface;
import com.opencloud.sleetck.lib.resource.testapi.TCKResourceListener;
import com.opencloud.sleetck.lib.resource.events.TCKResourceEventX;
import com.opencloud.sleetck.lib.testutils.*;
import com.opencloud.sleetck.lib.testutils.jmx.*;
import com.opencloud.sleetck.lib.resource.TCKSbbMessage;
import java.util.Properties;
import java.rmi.RemoteException;
import javax.management.ObjectName;
import com.opencloud.logging.Logable;
import javax.slee.management.DeployableUnitID;

/**
 * Tests assertion 379, that sbbStore() is invoked when the persistent state of the
 * Sbb entity needs to be synchronized with the state of the Sbb object,
 * and tests assertion 380, that sbbStore() is invoked with
 * a valid transaction context.
 */
public class Test380Test extends AbstractSleeTCKTest {

    public TCKTestResult run() throws Exception {

        QueuingResourceListener listener = new QueuingResourceListener(utils());
        setResourceListener(listener);
        TCKResourceTestInterface resource = utils().getResourceInterface();

        getLog().fine("Creaing an activity");
        TCKActivityID activityID = resource.createActivity(getClass().getName());
        getLog().fine("Sending an event");
        resource.fireEvent(TCKResourceEventX.X1,null,activityID,null);

        Object sbbMessage = null;
        do {
            getLog().fine("Waiting for Sbb to confirm event delivery");
            sbbMessage = listener.nextMessage().getMessage();
            getLog().fine("Received response from Sbb: "+sbbMessage);
        // allow the SLEE to make sbbStore() invocations before invoking the event handler
        } while(!Test380TestConstants.RECEIVED_EVENT.equals(sbbMessage));

        getLog().fine("Waiting for Sbb to confirm call to sbbStore()");
        try {
            sbbMessage = listener.nextMessage().getMessage();
        } catch(OperationTimedOutException otoe) {
            throw new TCKTestFailureException(379,"Timed out waiting for sbbStore() to be called "+
                "following an event handler invocation");
        }

        if(Test380TestConstants.SBB_STORE_CALLED_WITH_TXN.equals(sbbMessage)) {
            getLog().info("sbbStore() was invoked with a valid transaction context");
            return TCKTestResult.passed();
        } else if(Test380TestConstants.SBB_STORE_CALLED_NO_TXN.equals(sbbMessage)) {
            return TCKTestResult.failed(380,"sbbStore() was invoked with no transaction context");
        } else {
            return TCKTestResult.error("Received an unexcpected message from the Sbb while waiting "+
                "for sbbStore() to be called: "+sbbMessage);
        }

    }

}
