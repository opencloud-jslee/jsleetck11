/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.events.convergencename.eventisunique;

import com.opencloud.sleetck.lib.AbstractSleeTCKTest;
import com.opencloud.sleetck.lib.TCKTestResult;
import com.opencloud.sleetck.lib.resource.TCKActivityID;
import com.opencloud.sleetck.lib.resource.events.TCKResourceEventX;
import com.opencloud.sleetck.lib.testutils.BaseTCKResourceListener;
import com.opencloud.sleetck.lib.testutils.FutureResult;

import java.rmi.RemoteException;

/**
 * Test assertion 1963: that all events are unique.  the SBB will create an event and fire it twice with the same activity.
 * Because Event is selected as an initial event selector two SBB entitie's will be created to handle the event.
 * This means a total of 3 SBB entities will be created for the test (including the entity that creates and fires the event).
 */
public class Test1963Test extends AbstractSleeTCKTest {

    public static final int EXPECTED_SBBS_CREATED = 3;

    public TCKTestResult run() throws Exception {
        ResourceListener listener = new ResourceListener();
        testResult = new FutureResult(getLog());
        setResourceListener(listener);
        sbbsCreated = 0;
        TCKActivityID activityID = utils().getResourceInterface().createActivity("Test1963Activity");

        utils().getResourceInterface().fireEvent(TCKResourceEventX.X1, null, activityID, null);

        return testResult.waitForResultOrFail(utils().getTestTimeout(),"The expected number of SBB entities was not created. " +
                "This implies that multiple firings of the same event were not treated as unique events by the SLEE",1963);
    }

    public class ResourceListener extends BaseTCKResourceListener {
        public synchronized Object onSbbCall(Object args) {
            getLog().info("onSbbCall: An SBB was created");
            if(++sbbsCreated == EXPECTED_SBBS_CREATED) {
                getLog().info("The expected number of SBBs were created.");
                testResult.setPassed();
            }
            return null;
        }

        public synchronized void onException(Exception exception) throws RemoteException {
            testResult.setError("An Exception was received from the SBB or the TCK resource",exception);
        }

    }

    private int sbbsCreated;
    private FutureResult testResult;

}
