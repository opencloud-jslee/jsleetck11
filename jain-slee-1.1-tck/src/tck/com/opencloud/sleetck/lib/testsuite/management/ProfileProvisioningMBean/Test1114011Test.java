/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.management.ProfileProvisioningMBean;

import java.util.Collection;

import javax.management.Attribute;
import javax.management.ObjectName;
import javax.slee.profile.ProfileID;
import javax.slee.profile.ProfileSpecificationID;

import com.opencloud.sleetck.lib.AbstractSleeTCKTest;
import com.opencloud.sleetck.lib.TCKTestResult;
import com.opencloud.sleetck.lib.infra.SleeTCKComponentConstants;
import com.opencloud.sleetck.lib.testutils.ProfileUtils;
import com.opencloud.sleetck.lib.testutils.jmx.ProfileMBeanProxy;
import com.opencloud.sleetck.lib.testutils.jmx.ProfileProvisioningMBeanProxy;

/*
 * Test the getProfilesByStaticQuery method of the ProfileProvisioningMBean interface
 * A Collection of ProfileID objects that identify all the Profiles in the Profile Table
 * that satisfy the search criteria of the static query identified by the queryName argument are expected.
 */

public class Test1114011Test extends AbstractSleeTCKTest {

    public static final String SERVICE_DU_PATH_PARAM= "serviceDUPath";

    public static final String SPEC_NAME = "Test1114011Profile";
    public static final String SPEC_VERSION = "1.1";
    public static final String PROFILE_TABLE_NAME = "Test1114011ProfileTable";
    public static final String QUERY_NAME = "compareStringParam";
    public static final String QUERY_PARAM = "Test1114011Query";
    public static final String PROFILE_NAME  = "Test1114011Profile_1";

    public static final int TEST_ID = 1114011;


    /**
     * 1114011 test for static query
     */
    public TCKTestResult run() throws Exception {
        ProfileID expectedProfile = new ProfileID(PROFILE_TABLE_NAME, PROFILE_NAME);

        //Create profile table
        ProfileSpecificationID specID = new ProfileSpecificationID(SPEC_NAME, SleeTCKComponentConstants.TCK_VENDOR, SPEC_VERSION);
        profileProvisioning.createProfileTable(specID, PROFILE_TABLE_NAME);
        getLog().fine("1114011: Added profile table "+PROFILE_TABLE_NAME);

        // Set the attribute value
        ObjectName profile = profileProvisioning.createProfile(PROFILE_TABLE_NAME, PROFILE_NAME);
        ProfileMBeanProxy proxy = utils().getMBeanProxyFactory().createProfileMBeanProxy(profile);
        utils().getMBeanFacade().setAttribute(profile, new Attribute("StringValue", QUERY_PARAM));
        getLog().fine("1114011: Set Attribute in profile table to: "+QUERY_PARAM);

        // Commit the profile,
        proxy.commitProfile();
        proxy.closeProfile();

        try {
            Collection profileCollections;
            Object[] queryParameters = {new String(QUERY_PARAM)};
            boolean found = false;

             profileCollections = profileProvisioning.getProfilesByStaticQuery(PROFILE_TABLE_NAME, QUERY_NAME, queryParameters);
             if (!profileCollections.isEmpty()) {
                 for (int i=0; i < profileCollections.size(); i++) {
                     getLog().fine("1114011: query result = " + profileCollections.toArray()[i].toString());
                     if (profileCollections.contains(expectedProfile)) {
                         found = true;
                     } else {
                         return TCKTestResult.failed(1114011, "Profile does not contain Attribute for Query");
                     }
                 }
                 if (found)
                     logSuccessfulCheck(1114011);
             } else {
                 return TCKTestResult.failed(1114011, "ProfileTable contains no Attributes for Query");
             }

             if (!found)
                 return TCKTestResult.failed(1114011, "getProfilesByStaticQuery did not return correct value");

        } catch (Exception e) {
            getLog().warning(e);
            return TCKTestResult.failed(1114011, "ProfileProvisioningMBean did not perform getProfilesByStaticQuery");
        }


        return TCKTestResult.passed();
    }



    public void setUp() throws Exception {

        setupService(SERVICE_DU_PATH_PARAM);

        getLog().fine("1114011: setUp: Create ProfileProvisioningProxy ");
        profileUtils = new ProfileUtils(utils());
        profileProvisioning = profileUtils.getProfileProvisioningProxy();
    }

    public void tearDown() throws Exception {

        try {
            profileUtils.removeProfileTable(PROFILE_TABLE_NAME);
        }
        catch (Exception e) {
            getLog().warning("Caught exception while trying to remove profile table:");
            getLog().warning(e);
        }

        super.tearDown();
    }

    private void logSuccessfulCheck(int assertionID) {
        utils().getLog().info("Check for assertion "+assertionID+" OK");
    }


    private ProfileUtils profileUtils;
    private ProfileProvisioningMBeanProxy profileProvisioning;
}
