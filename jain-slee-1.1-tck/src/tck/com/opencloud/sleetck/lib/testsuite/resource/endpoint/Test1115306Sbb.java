/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.resource.endpoint;

import java.util.HashMap;

import javax.slee.ActivityContextInterface;

import com.opencloud.sleetck.lib.rautils.UOID;
import com.opencloud.sleetck.lib.testsuite.resource.BaseResourceSbb;
import com.opencloud.sleetck.lib.testsuite.resource.RAMethods;
import com.opencloud.sleetck.lib.testsuite.resource.SimpleEvent;
import com.opencloud.sleetck.lib.testsuite.resource.SimpleSbbInterface;
import com.opencloud.sleetck.lib.testsuite.resource.TCKMessage;

public abstract class Test1115306Sbb extends BaseResourceSbb {

    // Event handler
    public void onSimpleEvent(SimpleEvent event, ActivityContextInterface aci) {
        try {
            tracer.info("onSimpleEvent() event handler called with event: " + event);
            HashMap results = new HashMap();

            SimpleSbbInterface sbbInterface = getSbbInterface();

            int sequenceID = event.getSequenceID();
            Object payload = event.getPayload();

            if ("A".equals(payload)) {
                results.put("result-sbba", Boolean.TRUE);
            } else if ("B".equals(payload)) {
                results.put("result-sbbb", Boolean.FALSE);
            } else {
                tracer.severe("Unexpected SimpleEvent received: " + event);
                return;
            }
            sbbInterface.sendSbbMessage(new TCKMessage(sbbUID, sequenceID, RAMethods.fireEventTransacted, results));

        } catch (Exception e) {
            tracer.severe("Exception caught while trying to execute test logic in RA", e);
        }
    }

    private static final UOID sbbUID = UOID.createUOID();
}
