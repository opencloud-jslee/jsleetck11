/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.transactions.isolation;

import javax.slee.ActivityContextInterface;

/**
 * Defines an SBB Activity Context interface with a single attribute 'value', which
 * is aliased and shared by Test2003Sbb1 and Test2003Sbb2
 */
public interface Test2003ActivityContextInterface extends ActivityContextInterface {
    public String getValue();
    public void setValue(String value);
}
