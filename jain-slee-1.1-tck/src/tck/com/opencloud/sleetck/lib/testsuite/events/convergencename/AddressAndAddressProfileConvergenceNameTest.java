/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.events.convergencename;

import com.opencloud.sleetck.lib.TCKTestResult;
import com.opencloud.sleetck.lib.resource.TCKActivityID;
import com.opencloud.sleetck.lib.resource.events.TCKResourceEventX;
import com.opencloud.sleetck.lib.testutils.ProfileUtils;
import com.opencloud.sleetck.lib.testutils.jmx.AddressProfileProxy;
import com.opencloud.sleetck.lib.testutils.jmx.ProfileMBeanProxy;
import com.opencloud.sleetck.lib.testutils.jmx.ProfileProvisioningMBeanProxy;
import com.opencloud.sleetck.lib.testutils.jmx.impl.AddressProfileProxyImpl;

import javax.management.ObjectName;
import javax.slee.Address;
import javax.slee.AddressPlan;

/**
 * Tests assertion 2005 : If both the address and profile and address variables are selected,
 * variation of either value will result in a different convergence name.
 * If only one of the variables is selected, the value of the other variable
 * is ignored.
 */
public class AddressAndAddressProfileConvergenceNameTest extends AbstractConvergenceNameTest {

    // -- Constants -- //

    // Profile names                                     -- initially
    private static final String PROFILE_1 = "PROFILE_1"; // a1 a2
    private static final String PROFILE_2 = "PROFILE_2"; //       a3 a4

    private static final String ADDRESS_PROFILE_TABLE = "tck.AddressAndProfileTestProfile";

    private static final Address ADDRESS_1 = new Address(AddressPlan.IP, "1.0.0.1");
    private static final Address ADDRESS_2 = new Address(AddressPlan.IP, "1.0.0.2");
    private static final Address ADDRESS_3 = new Address(AddressPlan.IP, "1.0.0.3");
    private static final Address ADDRESS_4 = new Address(AddressPlan.IP, "1.0.0.4");

    private static final Address[] ADDRESS_SET_A = {ADDRESS_1, ADDRESS_2};
    private static final Address[] ADDRESS_SET_B = {ADDRESS_3, ADDRESS_4};

    private ProfileUtils profileUtils;

    /*---------------------------------------------------------------------------------------------*
    4 test cases:

    |                                         |                                         |
    |       AddressProfile not selected       |       AddressProfile selected           |
    ----------+-----------------------------------------+-----------------------------------------+
    |  TestCase #0                            |  TestCase #1                            |
    |  0. Create control SBB                  |  0. Create control SBB                  |
    Address  |  1. Vary Address does not create new    |  1. Vary Address does not create new    |
    not      |      SBB                                |      SBB                                |
    selected |  2. Vary AddressProfile does not create |  2. Vary AddressProfile creates new SBB |
    |      new SBB                            |                                         |
    |                                         |                                         |
    ----------+-----------------------------------------+-----------------------------------------+
    |  TestCase #2                            |  TestCase #3                            |
    |  0. Create control SBB                  |  0. Create control SBB                  |
    Address  |  1. Vary Address creates new SBB        |  1. Vary Address creates new SBB        |
    selected |  2. Vary AddressProfile does not create |  2. Vary AddressProfile creates new     |
    |     new SBB                             |     SBB                                 |
    |                                         |                                         |
    |                                         |                                         |
    ----------+-----------------------------------------+-----------------------------------------+
    */
    /**
     * Define InitialEventSelectorParameters for each test case
     * Value-pairs, index 0 = address selected, index 1 = addressprofile selected
     */
    private final boolean[][] testCaseConfig = {
        {false, false},  // TestCase 0
        {false, true},   // TestCase 1
        {true,  false},  // TestCase 2
        {true,  true}
    };

    /**
     * Run a test case
     * For each test case the following scenaarios are tested:
     * 0. Fire event to create an SBB
     * 1. Fire event with a different address.  Check if a new SBB is or is not created according to the expected results
     * 2. Swap the address profiles and fire an event with the original address.  Check if a new SBB is or is not created
     * according to the expected results (a new SBB should only be created when varying a variable selected by the test case).
     * <p>For InitialEventSelectorParameters, Address and or AddressProfile are selected according to the test case requirements.
     * ActivityContext is always selected.
     * Events for each test case are fired with a different activity to prevent results from previous tests interfering.</p>
     * @param testCaseNumber
     * @throws Exception
     */
    private void runTestCase(int testCaseNumber) throws Exception {
        InitialEventSelectorParameters iesParams = new InitialEventSelectorParameters(true,
                testCaseConfig[testCaseNumber][0],
                testCaseConfig[testCaseNumber][1],
                false, false, null, false, false, false, null);
        getLog().info("Test Case: " + testCaseNumber +
                ", Address selected=" + testCaseConfig[testCaseNumber][0] +
                ", AddressProfile selected=" + testCaseConfig[testCaseNumber][1]);

        // empty PROFILE_2 first so we don't get uniqueness errors
        setAddressProfile(PROFILE_2, new Address[0]);
        setAddressProfile(PROFILE_1, ADDRESS_SET_A);
        setAddressProfile(PROFILE_2, ADDRESS_SET_B);

        // Create a new activity -- run all tests cases on a separate activity
        TCKActivityID activityID = utils().getResourceInterface().createActivity(ACTIVITY_ID_PREFIX + (currentActivityIDSuffix++));

        // Create an event id for the initial 'control' SBB
        String controlSBBID = String.valueOf(currentEventID++);
        String expectedRespondingSBBID = controlSBBID;

        // Create initial SBB (Test 0)
        sendEventAndWait(TCKResourceEventX.X1, controlSBBID, activityID, ADDRESS_1, expectedRespondingSBBID, iesParams);

        // Test 1 -- vary the address.
        fireTestEvent(iesParams, activityID, ADDRESS_2, controlSBBID, testCaseConfig[testCaseNumber][0]);

        // Test 2 -- vary the address profile
        // empty PROFILE_2 first so we don't get uniqueness errors
        setAddressProfile(PROFILE_2, new Address[0]);
        setAddressProfile(PROFILE_1, ADDRESS_SET_B);
        setAddressProfile(PROFILE_2, ADDRESS_SET_A);
        fireTestEvent(iesParams, activityID, ADDRESS_1, controlSBBID, testCaseConfig[testCaseNumber][1]);
    }

    private void fireTestEvent(InitialEventSelectorParameters iesParams,
                               TCKActivityID activityID,
                               Address address,
                               String initialSBBEventID,
                               boolean expectNewSBB) throws Exception {
        String newEventID = String.valueOf(currentEventID++);
        sendEventAndWait(TCKResourceEventX.X1, newEventID, activityID, address,
                expectNewSBB ? newEventID : initialSBBEventID,
                iesParams);
    }

    /**
     * Main test loop, iterates through all test cases and invoke the test (<code>runTestCase</code>) for each.
     * @return
     * @throws Exception
     */
    public TCKTestResult run() throws Exception {
        for (int i = 0; i < testCaseConfig.length; i++) {
            runTestCase(i);
        }
        return TCKTestResult.passed();
    }

    private void setAddressProfile(String profileName, Address[] addresses) throws Exception {
        ObjectName objectName = profileProxy.getProfile(ADDRESS_PROFILE_TABLE, profileName);
        setAddressProfile(objectName, addresses);
    }

    private void setAddressProfile(ObjectName objectName, Address[] addresses) throws Exception {
        AddressProfileProxy addressProfileProxy = new AddressProfileProxyImpl(objectName,utils().getMBeanFacade());
        addressProfileProxy.editProfile();
        addressProfileProxy.setAddresses(addresses);
        addressProfileProxy.commitProfile();
    }

    public void setUp() throws Exception {
        // setup the address profile table before installing the service
        profileUtils = new ProfileUtils(utils());
        profileUtils.createStandardAddressProfileTable(ADDRESS_PROFILE_TABLE);
        profileProxy = profileUtils.getProfileProvisioningProxy();

        ObjectName profile = profileProxy.createProfile(ADDRESS_PROFILE_TABLE, PROFILE_1);
        ProfileMBeanProxy profProxy = utils().getMBeanProxyFactory().createProfileMBeanProxy(profile);
        profProxy.commitProfile();

        profile = profileProxy.createProfile(ADDRESS_PROFILE_TABLE, PROFILE_2);
        profProxy = utils().getMBeanProxyFactory().createProfileMBeanProxy(profile);
        profProxy.commitProfile();
        super.setUp();
    }

    public void tearDown() throws Exception {
        super.tearDown();
        if (profileUtils != null) {
            profileProxy.removeProfile(ADDRESS_PROFILE_TABLE, PROFILE_1);
            profileProxy.removeProfile(ADDRESS_PROFILE_TABLE, PROFILE_2);
            profileUtils.removeProfileTable(ADDRESS_PROFILE_TABLE);
        }
    }

    private int currentActivityIDSuffix = 0;
    private int currentEventID = 0;
    private ProfileProvisioningMBeanProxy profileProxy;
}
