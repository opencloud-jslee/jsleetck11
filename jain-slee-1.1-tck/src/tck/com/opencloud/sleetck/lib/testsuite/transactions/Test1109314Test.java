/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.transactions;

import javax.management.ObjectName;
import javax.slee.profile.ProfileSpecificationID;

import com.opencloud.sleetck.lib.TCKTestResult;
import com.opencloud.sleetck.lib.infra.SleeTCKComponentConstants;
import com.opencloud.sleetck.lib.testutils.ProfileUtils;
import com.opencloud.sleetck.lib.testutils.jmx.ProfileMBeanProxy;
import com.opencloud.sleetck.lib.testutils.jmx.ProfileProvisioningMBeanProxy;

/**
 * This test check the implementation of the JTA javax.transation.Transaction interface
 */
public class Test1109314Test extends TransactionsBaseTest {

    private static final String RA_NAME = "Test1109314RA";

    private static final String SPEC_NAME = "Test1109314Profile";
    private static final String SPEC_VERSION = "1.0";

    private static final String PROFILE_SPEC_DU_PATH_PARAM = "ProfileSpecDUPath";

    public String getRAName() {
        return RA_NAME;
    }

    public TCKTestResult run() throws Exception {

        //Create a profile table
        ProfileSpecificationID specID = new ProfileSpecificationID(SPEC_NAME, SleeTCKComponentConstants.TCK_VENDOR, SPEC_VERSION);
        profileProvisioning.createProfileTable(specID, Test1109314MessageListener.PROFILE_TABLE_NAME);
        getLog().fine("Added profile table "+Test1109314MessageListener.PROFILE_TABLE_NAME);

        //Create a profile via management view
        ObjectName profile = profileProvisioning.createProfile(Test1109314MessageListener.PROFILE_TABLE_NAME, Test1109314MessageListener.PROFILE_NAME);
        ProfileMBeanProxy profileProxy = utils().getMBeanProxyFactory().createProfileMBeanProxy(profile);
        getLog().fine("Created profile "+Test1109314MessageListener.PROFILE_NAME+" for profile table "+Test1109314MessageListener.PROFILE_TABLE_NAME);

        //commit and close the Profile
        profileProxy.commitProfile();
        profileProxy.closeProfile();
        getLog().fine("Commit and close profile "+Test1109314MessageListener.PROFILE_NAME);

        return super.run();
    }

    public void tearDown() throws Exception {
        try {
            profileUtils.removeProfileTable(Test1109314MessageListener.PROFILE_TABLE_NAME);

        } finally {
            super.tearDown();
        }

    }

    public void setUp() throws Exception {
        setupService(PROFILE_SPEC_DU_PATH_PARAM);
        profileUtils = new ProfileUtils(utils());
        profileProvisioning = profileUtils.getProfileProvisioningProxy();

        super.setUp();
    }

    private ProfileUtils profileUtils;
    private ProfileProvisioningMBeanProxy profileProvisioning;

}
