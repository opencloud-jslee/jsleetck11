/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.facilities.tracefacility;

import javax.management.Notification;
import javax.management.NotificationListener;
import javax.management.ObjectName;
import javax.slee.management.ResourceAdaptorEntityNotification;
import javax.slee.management.TraceNotification;

import com.opencloud.sleetck.lib.AbstractSleeTCKTest;
import com.opencloud.sleetck.lib.TCKTestResult;
import com.opencloud.sleetck.lib.resource.TCKActivityID;
import com.opencloud.sleetck.lib.resource.events.TCKResourceEventX;
import com.opencloud.sleetck.lib.testutils.Assert;
import com.opencloud.sleetck.lib.testutils.jmx.SleeManagementMBeanProxy;
import com.opencloud.sleetck.lib.testutils.jmx.TraceMBeanProxy;
import com.opencloud.sleetck.lib.testutils.jmx.impl.TraceMBeanProxyImpl;

/**
 *
 */
public final class Test4484Test extends AbstractSleeTCKTest {
    public TCKTestResult run() throws Exception {
        TCKActivityID activity = utils().getResourceInterface().createActivity("Test4484Activity");

        receivedTraceNotifications = 0;
        expectedTraceNotifications = 4;   // Each event will produce two traces

        // fire 2 events
        utils().getResourceInterface().fireEvent(TCKResourceEventX.X1, null, activity, null);
        utils().getResourceInterface().fireEvent(TCKResourceEventX.X1, null, activity, null);

        synchronized (this) {
            wait(utils().getTestTimeout());
        }

        Assert.assertTrue(4484, "Expected number of trace messages not received, traces were not delivered by " +
                                "the TraceFacility (expected " + expectedTraceNotifications + ", received " + receivedTraceNotifications + ")",
                            expectedTraceNotifications == receivedTraceNotifications);

        return TCKTestResult.passed();
    }

    public void setUp() throws Exception {
        super.setUp();
        SleeManagementMBeanProxy proxy = utils().getSleeManagementMBeanProxy();
        traceMBeanName = proxy.getTraceMBean();
        listener = new TraceNotificationListenerImpl();

        tracembean = new TraceMBeanProxyImpl(traceMBeanName, utils().getMBeanFacade());
        tracembean.addNotificationListener(listener, null, null);
    }

    public void tearDown() throws Exception {
        if (null != tracembean)
            tracembean.removeNotificationListener(listener);
        super.tearDown();
    }

    public class TraceNotificationListenerImpl implements NotificationListener {
        public final void handleNotification(Notification notification, Object o) {
            if (notification instanceof TraceNotification) {
                TraceNotification n = (TraceNotification) notification;
                getLog().info("Type= "+n.getType());
                //filter out those notifications that come from the TCK-RA (since 1.1)
                if (!ResourceAdaptorEntityNotification.TRACE_NOTIFICATION_TYPE.equals(n.getType())) {
                    getLog().info("Got Trace: " + n);
                    receivedTraceNotifications++;
                }
            }
        }
    }

    private ObjectName traceMBeanName;
    private NotificationListener listener;
    private TraceMBeanProxy tracembean;
    private int expectedTraceNotifications;
    private int receivedTraceNotifications;
}
