/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.profiles.profileabstractclass;

import javax.slee.CreateException;
import javax.slee.profile.Profile;
import javax.slee.profile.ProfileContext;
import javax.slee.profile.ProfileVerificationException;

/**
 * Lifecycle methods are final.
 */

public abstract class Test1110235_2Profile implements ProfileAbstractClassTestsProfileCMP, Profile  {

    public Test1110235_2Profile() {

    }

    public final void setProfileContext(ProfileContext context)  {
        this.context = context;
    }

    public final void unsetProfileContext() {
    }

    public final void profileInitialize() {
    }

    public final void profilePostCreate() throws CreateException {
    }

    public final void profileActivate() {
    }

    public final void profilePassivate() {
    }

    public final void profileLoad() {
    }

    public final void profileStore() {
    }

    public final void profileRemove() {
    }

    public final void profileVerify() throws ProfileVerificationException {
    }

    private ProfileContext context;

}
