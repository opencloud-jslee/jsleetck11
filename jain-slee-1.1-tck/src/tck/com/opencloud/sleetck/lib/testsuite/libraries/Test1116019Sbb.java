/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.libraries;

import javax.slee.ActivityContextInterface;

import com.opencloud.sleetck.lib.resource.events.TCKResourceEventX;
import com.opencloud.sleetck.lib.sbbutils.BaseTCKSbb;
import com.opencloud.sleetck.lib.sbbutils.TCKSbbUtils;
import com.opencloud.sleetck.lib.testsuite.libraries.Test1116019Library.ExampleClass;
import com.opencloud.sleetck.lib.testsuite.libraries.Test1116019Library.ExampleClass2;

/*
 * Test that the library has the permissions defined.
 */

public abstract class Test1116019Sbb extends BaseTCKSbb {

    public void onTCKResourceEventX1(TCKResourceEventX event, ActivityContextInterface aci) {
        ExampleClass ec;
        ExampleClass2 ec2;
        try {
            ec = new ExampleClass();
            ec.checkExtraPermissions(); 
            ec.checkPermissions();
            
            ec2 = new ExampleClass2();
            ec2.checkExtraPermissions();
            ec2.checkPermissions();
            
            TCKSbbUtils.getResourceInterface().sendSbbMessage("Tests passed");
        } catch(Exception e) {
            TCKSbbUtils.handleException(e);
        }
    }
}
