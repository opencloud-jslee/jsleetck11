/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.activities.activitycontext;

import javax.slee.ActivityEndEvent;
import javax.slee.ActivityContextInterface;
import javax.slee.SbbContext;

import com.opencloud.sleetck.lib.sbbutils.BaseTCKSbb;
import com.opencloud.sleetck.lib.sbbutils.TCKSbbUtils;
import com.opencloud.sleetck.lib.sbbutils.TCKSbbConstants;
import com.opencloud.sleetck.lib.resource.events.TCKResourceEventX;
import com.opencloud.sleetck.lib.resource.TCKActivityID;
import com.opencloud.sleetck.lib.resource.sbbapi.TCKResourceSbbInterface;
import com.opencloud.sleetck.lib.resource.sbbapi.TCKActivityContextInterfaceFactory;

import java.util.HashMap;

public abstract class Test556Sbb extends BaseTCKSbb {

    public void onActivityEndEvent(ActivityEndEvent event, ActivityContextInterface aci) {
        try {
            HashMap map = new HashMap();
            map.put("Result", new Boolean(true));
            map.put("Message", "Ok");
            TCKSbbUtils.getResourceInterface().sendSbbMessage(map);
        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }
    }

    public void onTCKResourceEventX1(TCKResourceEventX event, ActivityContextInterface aci) {

        try {
            // Create an activity, attach this SBB to it.

            TCKActivityID activityID = TCKSbbUtils.getResourceInterface().createActivity("Test556SecondActivity");
            TCKResourceSbbInterface resource = TCKSbbUtils.getResourceInterface();
            TCKActivityContextInterfaceFactory factory = (TCKActivityContextInterfaceFactory) TCKSbbUtils.getSbbEnvironment().lookup(TCKSbbConstants.TCK_ACI_FACTORY_LOCATION);
            ActivityContextInterface myAci = factory.getActivityContextInterface(resource.getActivity(activityID));

            myAci.attach(getSbbContext().getSbbLocalObject());

            // End that activity.
            resource.endActivity(activityID);

            // Verify that this SBB receives the ActivityEndEvent.
        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }

    }

}
