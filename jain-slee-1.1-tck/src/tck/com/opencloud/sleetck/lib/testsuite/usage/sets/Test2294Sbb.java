/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.usage.sets;

import com.opencloud.sleetck.lib.sbbutils.BaseTCKSbb;
import com.opencloud.sleetck.lib.resource.events.TCKResourceEventX;

import javax.slee.ActivityContextInterface;

/**
 * This is a do nothing SBB which does not define a get usage parameters method.
 * Its SbbID is passed to createUsageParameterSet() to trigger an InvalidArgumentException.
 */
public abstract class Test2294Sbb extends BaseTCKSbb {

    public void onTCKResourceEventX1(TCKResourceEventX event, ActivityContextInterface aci) {
        // no-op
    }

}
