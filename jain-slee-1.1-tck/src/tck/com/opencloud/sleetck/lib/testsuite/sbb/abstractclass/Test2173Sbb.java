/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.sbb.abstractclass;

import javax.slee.ActivityEndEvent;
import javax.slee.ActivityContextInterface;
import javax.slee.SbbContext;
import javax.slee.Address;
import javax.slee.SbbLocalObject;
import javax.slee.facilities.Level;
import javax.slee.ChildRelation;
import javax.slee.CreateException;

import com.opencloud.sleetck.lib.sbbutils.BaseTCKSbb;
import com.opencloud.sleetck.lib.sbbutils.TCKSbbUtils;
import com.opencloud.sleetck.lib.sbbutils.TCKSbbConstants;
import com.opencloud.sleetck.lib.resource.events.TCKResourceEventX;
import com.opencloud.sleetck.lib.resource.sbbapi.TCKResourceSbbInterface;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Collection;
import java.util.Vector;

public abstract class Test2173Sbb extends BaseTCKSbb {

    // Initial event
    public void onTCKResourceEventX1(TCKResourceEventX ev, ActivityContextInterface aci) {

        HashMap map = new HashMap();

        try {

            // Create the child SBB.
            SbbLocalObject child = getChildRelation().create();

            setChild(child);

            child.remove();

            if (getChild() == null)
                map.put("Result", Boolean.TRUE);
            else {
                map.put("Result", Boolean.FALSE);
                map.put("Message", "The get accessor for a CMP field storing a removed child returned non-null.  It should have returned null.");
            }

            TCKSbbUtils.getResourceInterface().sendSbbMessage(map);
        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }
    }

    public abstract ChildRelation getChildRelation();

    public abstract void setChild(SbbLocalObject child);
    public abstract SbbLocalObject getChild();

}
