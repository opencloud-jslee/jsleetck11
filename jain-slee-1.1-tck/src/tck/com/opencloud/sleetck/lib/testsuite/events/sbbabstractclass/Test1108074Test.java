/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.events.sbbabstractclass;

import java.rmi.RemoteException;
import java.util.Map;

import javax.management.ObjectName;
import javax.slee.profile.ProfileSpecificationID;

import com.opencloud.sleetck.lib.AbstractSleeTCKTest;
import com.opencloud.sleetck.lib.TCKTestFailureException;
import com.opencloud.sleetck.lib.infra.SleeTCKComponentConstants;
import com.opencloud.sleetck.lib.resource.TCKActivityID;
import com.opencloud.sleetck.lib.resource.TCKSbbMessage;
import com.opencloud.sleetck.lib.resource.events.TCKResourceEventX;
import com.opencloud.sleetck.lib.resource.testapi.TCKResourceListener;
import com.opencloud.sleetck.lib.resource.testapi.TCKResourceTestInterface;
import com.opencloud.sleetck.lib.testutils.Assert;
import com.opencloud.sleetck.lib.testutils.BaseTCKResourceListener;
import com.opencloud.sleetck.lib.testutils.ComponentIDLookup;
import com.opencloud.sleetck.lib.testutils.FutureResult;
import com.opencloud.sleetck.lib.testutils.jmx.ProfileMBeanProxy;
import com.opencloud.sleetck.lib.testutils.jmx.ProfileProvisioningMBeanProxy;

/**
 * AssertionID(1108074): Test The SBB object of the SbbContext object must 
 * be in the Ready state, i.e. it must have an assignedSBB entity, when it 
 * invokes this method. Otherwise, this method throws a java.lang.IllegalStateException.
 */
public class Test1108074Test extends AbstractSleeTCKTest {

    private static final String SERVICE_DU_PATH_PARAM = "serviceDUPath";

    private static final String PROFILE_SPEC_DU_PATH_PARAM = "profileSpecDUPath";

    /**
     * Perform the actual test.
     */

    public void run(FutureResult result) throws Exception {
        this.result = result;

        TCKResourceTestInterface resource = utils().getResourceInterface();
        TCKActivityID activityID = resource.createActivity("Test1108074InitialActivity");

        getLog().fine("Firing TCKResourceEventX1 on Test1108074InitialActivity.");
        resource.fireEvent(TCKResourceEventX.X1, testName, activityID, null);
    }

    /**
     * Do all the pre-run configuration of the test.
     */

    public void setUp() throws Exception {
        getLog().fine("Connecting to resource");
        resourceListener = new TCKResourceListenerImpl();
        setResourceListener(resourceListener);
        getLog().fine("Installing and activating service");
        setupService(SERVICE_DU_PATH_PARAM, true);

        getLog().fine("Installing profile spec");

        String profileSpecDUPath = utils().getTestParams().getProperty(PROFILE_SPEC_DU_PATH_PARAM);
        utils().install(profileSpecDUPath);
        getLog().fine("Creating a profile table, to create a profile table activity");

        ObjectName profileProvisioningName = utils().getSleeManagementMBeanProxy().getProfileProvisioningMBean();
        profileProvisioningMBeanProxy = utils().getMBeanProxyFactory().createProfileProvisioningMBeanProxy(
                profileProvisioningName);
        ProfileSpecificationID profileSpecID = new ComponentIDLookup(utils()).lookupProfileSpecificationID(
                "SimpleProfile11", SleeTCKComponentConstants.TCK_VENDOR, "1.1");
        getLog().info("Creating profile table: " + Test1108074Sbb.PROFILE_TABLE_NAME);
        profileProvisioningMBeanProxy.createProfileTable(profileSpecID, Test1108074Sbb.PROFILE_TABLE_NAME);
        ObjectName profile = profileProvisioningMBeanProxy.createProfile(Test1108074Sbb.PROFILE_TABLE_NAME,
                Test1108074Sbb.PROFILE_NAME);
        ProfileMBeanProxy profProxy = utils().getMBeanProxyFactory().createProfileMBeanProxy(profile);
        profProxy.commitProfile();
        tableCreated = true;
    }

    /**
     * Clean up after the test.
     */

    public void tearDown() throws Exception {
        if (profileProvisioningMBeanProxy != null && tableCreated) {
            try {
                getLog().fine("Removing profile table");
                profileProvisioningMBeanProxy.removeProfileTable(Test1108074Sbb.PROFILE_TABLE_NAME);
            } catch (Exception e) {
                getLog().warning(e);
            }
        }

        // cleanup
        super.tearDown();
    }

    private class TCKResourceListenerImpl extends BaseTCKResourceListener {
        // TCKResourceListener methods

        public synchronized void onSbbMessage(TCKSbbMessage message, TCKActivityID calledActivity)
                throws RemoteException {

            Map sbbData = (Map) message.getMessage();
            String sbbTestName = (String) sbbData.get("testname");
            String sbbTestResult = (String) sbbData.get("result");
            String sbbTestMessage = (String) sbbData.get("message");
            int assertionID = ((Integer) sbbData.get("id")).intValue();
            getLog().info(
                    "Received message from SBB: testname=" + sbbTestName + ", result=" + sbbTestResult + ", message="
                            + sbbTestMessage + ", id=" + assertionID);
            try {
                Assert.assertEquals(assertionID, "Test " + sbbTestName + " failed.", "pass", sbbTestResult);
                result.setPassed();
            } catch (TCKTestFailureException ex) {
                result.setFailed(1108074,
                        "The SBB fired an event in the pooled state and did not receive an IllegalStateException");
            }
        }

        public void onException(Exception exception) throws RemoteException {
            getLog().warning("Received Exception from SBB or resource:");
            getLog().warning(exception);
            result.setError(exception);
        }
    }

    private TCKResourceListener resourceListener;

    private ProfileProvisioningMBeanProxy profileProvisioningMBeanProxy;

    private FutureResult result;

    private final static String testName = "Test1108074";

    private boolean tableCreated = false;
}