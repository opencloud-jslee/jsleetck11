/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.events.eventcontext;

import javax.slee.ActivityContextInterface;
import javax.slee.InitialEventSelector;
import javax.slee.EventContext;
import javax.slee.facilities.Tracer;

import com.opencloud.sleetck.lib.sbbutils.BaseTCKSbb;
import com.opencloud.sleetck.lib.sbbutils.TCKSbbUtils;
import com.opencloud.sleetck.lib.resource.events.TCKResourceEventX;
import com.opencloud.sleetck.lib.resource.sbbapi.TCKActivity;
import com.opencloud.sleetck.lib.resource.TCKActivityID;

import java.util.HashMap;

/**
 * AssertionID(1108040): Test The SBB entity to which the event was 
 * delivered that was not added to the SLEE delivered set when the 
 * event was suspended is now added to the event’s delivered set.
 */
public abstract class Test1108040Sbb extends BaseTCKSbb {
    public static final String TRACE_MESSAGE_TCKResourceEventX1 = "Test1108040Sbb:I got TCKResourceEventX1 on ActivityA";

    public static final String TRACE_MESSAGE_TCKResourceEventX2A = "Test1108040Sbb:I got TCKResourceEventX2 on ActivityA";

    public static final String TRACE_MESSAGE_TCKResourceEventX2B = "Test1108040Sbb:I got TCKResourceEventX2 on ActivityB";

    public static final String TRACE_MESSAGE_TCKResourceEventX3 = "Test1108040Sbb:I got TCKResourceEventX3 on ActivityA";

    public InitialEventSelector initialEventSelector(InitialEventSelector ies) {
        ies.setCustomName("test");
        return ies;
    }

    public void onTCKResourceEventX1(TCKResourceEventX event, ActivityContextInterface aci, EventContext context) {
        try {
            //send message to TCK: I got TCKResourceEventX1 on ActivityA
            tracer = getSbbContext().getTracer("com.test");
            tracer.info("Prior to call EventContext.suspendDelivery() " + TRACE_MESSAGE_TCKResourceEventX1);
            setPassedX1(false);
            setPassedX2(false);
            //store context in CMP field
            setEventContext(context);
            //call suspendDelivery() method now, we assumed the default timeout 
            //on the SLEE will be more than 10000ms to complete this test.
            context.suspendDelivery();
            tracer.info("After called EventContext.suspendDelivery() " + TRACE_MESSAGE_TCKResourceEventX1);
        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }
    }

    public void onTCKResourceEventX2(TCKResourceEventX event, ActivityContextInterface aci, EventContext context) {
        try {
            TCKActivity activity = (TCKActivity) context.getActivityContextInterface().getActivity();
            TCKActivityID activityID = activity.getID();
            tracer = getSbbContext().getTracer("com.test");
            tracer.info("Sbb1:Entering onTCKResourceEventX2");
            
            if (activityID.getName().equals(Test1108040Test.activityNameB)) {
                //send message to TCK: I got TCKResourceEventX2 on ActivityB
                tracer.info(TRACE_MESSAGE_TCKResourceEventX2B);
                //get EventContext ec from CMP field
                EventContext ec = getEventContext();
                
                try {
                //assert ec.isSuspended(), check ec has been suspended or not 
                if (!ec.isSuspended()) {
                    sendResultToTCK("Test1108040Test", false, "SBB:onTCKResourceEventX2-ERROR: The event delivery hasn't been suspended, the "
                            + "EventContext.isSuspended() returned false!", 1108040);
                    return;
                }
                //call resumeDelivery method now
                ec.resumeDelivery();
                } 
                catch (Exception e) {
                    tracer.severe("ERROR onTCKResourceEventX2, the event delivery failed to be resumed" + e.getMessage());
                    return;
                }
                
                setPassedX2(true);
            } else if (activityID.getName().equals(Test1108040Test.activityNameA)) {
                //send message to TCK: I got TCKResourceEventX2 on ActivityA
                tracer.info(TRACE_MESSAGE_TCKResourceEventX2A);
                if (getPassedX2()) {
                    setPassedX1(true);
                }
            }
            tracer.info("Sbb1:Exiting onTCKResourceEventX2");
            return;

        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }
    }

    public void onTCKResourceEventX3(TCKResourceEventX event, ActivityContextInterface aci, EventContext context) {
        try {
            tracer = getSbbContext().getTracer("com.test");
            tracer.info(TRACE_MESSAGE_TCKResourceEventX3);
            if (getPassedX1())
                sendResultToTCK("Test1108040Test", true, "The SBB entity to which the event was delivered that was "
                        + "not added to the SLEE delivered set when the event was suspended "
                        + "is now added to the event’s delivered set.", 1108040);
            else
                sendResultToTCK("Test1108040Test", false, "Sbb did not receive TCKResourceEventX3 on ActivityA, this means the resumeDelivery "
                                + "method still does not resume the delivery of the event associated with this Event Context.", 1108040);
            return;
        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }
    }

    private void sendResultToTCK(String testName, boolean result, String message, int failedAssertionID) throws Exception {
        HashMap sbbData = new HashMap();
        sbbData.put("testname", testName);
        sbbData.put("result", result ? "pass" : "fail");
        sbbData.put("message", message);
        sbbData.put("id", new Integer(failedAssertionID));
        TCKSbbUtils.getResourceInterface().sendSbbMessage(sbbData);
    }

    public abstract void setEventContext(EventContext eventContext);
    public abstract EventContext getEventContext();
    
    public abstract void setPassedX1(boolean passed);
    public abstract boolean getPassedX1();
    
    public abstract void setPassedX2(boolean passed);
    public abstract boolean getPassedX2();

    private Tracer tracer = null;
}
