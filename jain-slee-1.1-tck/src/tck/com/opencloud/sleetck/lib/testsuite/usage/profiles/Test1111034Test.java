/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.usage.profiles;

import java.rmi.RemoteException;

import javax.management.Notification;
import javax.management.ObjectName;
import javax.slee.management.ProfileTableNotification;

import com.opencloud.sleetck.lib.OperationTimedOutException;
import com.opencloud.sleetck.lib.TCKTestResult;
import com.opencloud.sleetck.lib.resource.TCKActivityID;
import com.opencloud.sleetck.lib.resource.TCKSbbMessage;
import com.opencloud.sleetck.lib.resource.events.TCKResourceEventX;
import com.opencloud.sleetck.lib.resource.testapi.TCKResourceTestInterface;
import com.opencloud.sleetck.lib.testsuite.usage.common.UsageMBeanProxy;
import com.opencloud.sleetck.lib.testsuite.usage.common.UsageMBeanProxyImpl;
import com.opencloud.sleetck.lib.testutils.BaseTCKResourceListener;
import com.opencloud.sleetck.lib.testutils.QueuingNotificationListener;
import com.opencloud.sleetck.lib.testutils.jmx.ProfileTableUsageMBeanProxy;

public class Test1111034Test extends BaseProfileTest {

    public static final String PROFILE_TABLE_NAME = "Test1111034ProfileTable";
    public static final String PROFILE_NAME = "Test1111034Profile";
    private static final int TEST_ID = 1111034;


    /**
     * Perform the actual test.
     */

    public TCKTestResult run() throws Exception {
        TCKTestResult result=null;

        // Look up the UsageMBean.
        ObjectName profileTableUsageMBeanName = profileProxy.getProfileTableUsageMBean(PROFILE_TABLE_NAME);
        ProfileTableUsageMBeanProxy p = utils().getMBeanProxyFactory().createProfileTableUsageMBeanProxy(profileTableUsageMBeanName);

        String setName = p.getUsageParameterSets()[0];
        ObjectName usageMBeanName = p.getUsageMBean(setName);
        usageMBeanProxy = new UsageMBeanProxyImpl(usageMBeanName, utils().getMBeanFacade());
        p.close();

        // Listen to notifications from that proxy.

        QueuingNotificationListener notificationListener = new QueuingNotificationListener(utils());
        usageMBeanProxy.addNotificationListener(notificationListener,null,null);

        // Request that the default usage parameter gets updated.
        TCKResourceTestInterface resource = utils().getResourceInterface();
        TCKActivityID activityID = resource.createActivity(getClass().getName());
        getLog().fine("About to fire named usage parameter update request to SBB");
        resource.fireEvent(TCKResourceEventX.X1, null, activityID, null);

        try {
            Notification n = notificationListener.nextNotification();
            if (!ProfileTableNotification.USAGE_NOTIFICATION_TYPE.equals(n.getType()))
                result = TCKTestResult.failed(TEST_ID, "Notification was not a ProfileTableNotification.");
            else
                result = TCKTestResult.passed();

        } catch (OperationTimedOutException e) {
            result = TCKTestResult.failed(TEST_ID, "Did not receive notification before timeout.");
        }
        usageMBeanProxy.removeNotificationListener(notificationListener);
        usageMBeanProxy.close();
        return result;
    }

    /**
     * Do all the pre-run configuration of the test.
     */

    public void setUp() throws Exception {
        super.setUp();

        createProfileTable(PROFILE_TABLE_NAME);
        createNamedParameterSet(PROFILE_TABLE_NAME);
        createProfile(PROFILE_TABLE_NAME, PROFILE_NAME);

        setResourceListener(new TCKResourceListenerImpl());
    }

    /**
     * Clean up after the test.
     */

    public void tearDown() throws Exception {
        removeNamedParameterSet(PROFILE_TABLE_NAME);
        removeProfile(PROFILE_TABLE_NAME, PROFILE_NAME);
        removeProfileTable(PROFILE_TABLE_NAME);

        super.tearDown();
    }

    //private resource listener implementation
    private class TCKResourceListenerImpl extends BaseTCKResourceListener {

        public synchronized void onSbbMessage(TCKSbbMessage message, TCKActivityID activity) throws RemoteException {
            utils().getLog().fine("Received message from SBB: " + message.getMessage());
        }

        public void onException(Exception e) throws RemoteException {
            utils().getLog().warning("Received exception from SBB.");
            utils().getLog().warning(e);
        }

    }

    private UsageMBeanProxy usageMBeanProxy;
}
