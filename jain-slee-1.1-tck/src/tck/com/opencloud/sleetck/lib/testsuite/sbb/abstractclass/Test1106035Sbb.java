/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.sbb.abstractclass;

import java.util.HashMap;

import javax.slee.ActivityContextInterface;
import javax.slee.ChildRelation;

import com.opencloud.sleetck.lib.resource.events.TCKResourceEventX;
import com.opencloud.sleetck.lib.sbbutils.BaseTCKSbb;
import com.opencloud.sleetck.lib.sbbutils.TCKSbbUtils;

public abstract class Test1106035Sbb extends BaseTCKSbb {

    // Initial event
    public void onTCKResourceEventX1(TCKResourceEventX ev, ActivityContextInterface aci) {
        HashMap map = new HashMap();

        try {
            /* The SLEE is likely to catch this error at deploy time, so this code may never get executed. */
            try {
                setLocalObject((Test1106035SbbLocalObject) getSbbContext().getSbbLocalObject());
            } catch (IllegalArgumentException e) {
                map.put("Result", new Boolean(true));
                map.put("Message", "Ok");
                TCKSbbUtils.getResourceInterface().sendSbbMessage(map);
                return;
            }
            map.put("Result", new Boolean(false));
            map.put("ID", new Integer(1106035));
            map.put("Message", "Expected IllegalArgumentException to be thrown when storing Test1106035SbbLocalObject into a CMP field defined to hold Test1106035ChildSbbLocalObject.");
            TCKSbbUtils.getResourceInterface().sendSbbMessage(map);
            return;
        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }
    }


    public abstract void setLocalObject(Test1106035SbbLocalObject object);
    public abstract Test1106035SbbLocalObject getLocalObject();

    public abstract ChildRelation getChildRelation();

}
