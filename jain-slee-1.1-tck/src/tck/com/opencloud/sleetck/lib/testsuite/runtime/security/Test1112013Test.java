/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.runtime.security;

import com.opencloud.sleetck.lib.AbstractSleeTCKTest;
import com.opencloud.sleetck.lib.TCKTestResult;
import com.opencloud.sleetck.lib.resource.TCKActivityID;
import com.opencloud.sleetck.lib.resource.events.TCKResourceEventX;
import com.opencloud.sleetck.lib.resource.testapi.TCKResourceTestInterface;
import com.opencloud.sleetck.lib.testutils.QueuingResourceListener;

/**
 * If the “isolate-security-permissions” attribute of the sbb-local-interface
 * element in the SBB’s deployment descriptor is “False”, then invoked business
 * methods on that SBB's local interface run with an access control context
 * including the protections domains of the SBB with the protection domains of
 * other classes (methods?) on the call stack.
 */

public class Test1112013Test extends AbstractSleeTCKTest {

    public TCKTestResult run() throws Exception {
        QueuingResourceListener listener = new QueuingResourceListener(utils());
        Object ackMessage;
        setResourceListener(listener);
        TCKResourceTestInterface resource = utils().getResourceInterface();

        getLog().fine("Creaing activity");
        TCKActivityID activityID = resource
                .createActivity(getClass().getName());
        getLog().info("Firing an event");
        resource.fireEvent(TCKResourceEventX.X1, null, activityID, null);

        // Expect a message to indicate a test pass, or a
        // TCKTestFailureException
        // to indicate a failure. nextMessage() will rethrow the failure
        // exception if received
        getLog().fine("Waiting for a reply from the Sbb");
        ackMessage = listener.nextMessage().getMessage();
        getLog().info(
                "Received First ACK message from Sbb to indicate a test pass: "
                        + ackMessage);
        ackMessage = listener.nextMessage().getMessage();
        getLog().info(
                "Received Second ACK message from Sbb to indicate a test pass: "
                        + ackMessage);
        
        return TCKTestResult.passed();
    }

}
