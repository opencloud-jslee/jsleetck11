/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.events.eventcontext;

import java.util.HashMap;

import javax.slee.ActivityContextInterface;
import javax.slee.Address;
import javax.slee.EventContext;
import javax.slee.ServiceID;
import javax.slee.facilities.Tracer;

import com.opencloud.sleetck.lib.resource.events.TCKResourceEventX;
import com.opencloud.sleetck.lib.sbbutils.BaseTCKSbb;
import com.opencloud.sleetck.lib.sbbutils.TCKSbbUtils;

/**
 * AssertionID(1108177): Test This will be null if the address provided was null.
 *
 */
public abstract class Test1108177Sbb1 extends BaseTCKSbb {

    public void onTCKResourceEventX1(TCKResourceEventX event, ActivityContextInterface aci, EventContext context) {
        try {
            tracer = getSbbContext().getTracer("com.test");
            tracer.info("Test1108177Sbb1:I got TCKResourceEventX1 on ActivityA");
            Test1108177Event testEvent = new Test1108177Event();
            ServiceID serviceID = context.getService();

            // If the provided ServiceID is null
            Address nullAddress = null;
            fireTest1108177Event(testEvent, aci, nullAddress, serviceID);

        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }
    }

    public void onTest1108177Event(Test1108177Event event, ActivityContextInterface aci, EventContext context) {
        try {
            tracer = getSbbContext().getTracer("com.test");
            tracer.info("Test1108177Sbb1:I got Test1108177Event on ActivityA");
            if (context.getAddress() != null)
                sendResultToTCK("Test1108177Test", false, "SBB1:onTest1108177Event-ERROR: This EventContext.getAddress() method didn't return null "
                        + "if the address provided was null.", 1108177);
            else
                sendResultToTCK("Test1108177Test", true, "This EventContext.getService() method returned null if the address "
                        + "provided was null.", 1108177);
        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }

    }

    public abstract void fireTest1108177Event(Test1108177Event event, ActivityContextInterface aci, Address address,
            ServiceID serviceID);

    private void sendResultToTCK(String testName, boolean result, String message, int failedAssertionID) throws Exception {
        HashMap sbbData = new HashMap();
        sbbData.put("testname", testName);
        sbbData.put("result", result ? "pass" : "fail");
        sbbData.put("message", message);
        sbbData.put("id", new Integer(failedAssertionID));
        TCKSbbUtils.getResourceInterface().sendSbbMessage(sbbData);
    }

    private Tracer tracer;
}
