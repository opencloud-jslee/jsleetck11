/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.javax.slee.SbbContext;

import javax.slee.ActivityEndEvent;
import javax.slee.ActivityContextInterface;
import javax.slee.SbbContext;
import javax.slee.Address;
import javax.slee.SbbLocalObject;
import javax.slee.facilities.Level;
import javax.slee.ChildRelation;
import javax.slee.CreateException;
import javax.slee.ServiceID;

import javax.slee.nullactivity.NullActivity;
import javax.slee.nullactivity.NullActivityFactory;
import javax.slee.nullactivity.NullActivityContextInterfaceFactory;

import com.opencloud.sleetck.lib.sbbutils.BaseTCKSbb;
import com.opencloud.sleetck.lib.sbbutils.TCKSbbUtils;
import com.opencloud.sleetck.lib.sbbutils.TCKSbbConstants;
import com.opencloud.sleetck.lib.resource.events.TCKResourceEventX;
import com.opencloud.sleetck.lib.resource.sbbapi.TCKResourceSbbInterface;

import java.util.HashMap;
import java.util.Iterator;

public abstract class Test3248Sbb extends BaseTCKSbb {

    // Initial event
    public void onTCKResourceEventX1(TCKResourceEventX ev, ActivityContextInterface aci) {

        HashMap map = new HashMap();

        int nActivities = 5;
        NullActivity nullActivities[] = new NullActivity[nActivities];
        try {

            // Get the NullActivityFactory
            NullActivityFactory factory = (NullActivityFactory) TCKSbbUtils.getSbbEnvironment().lookup("slee/nullactivity/factory");

            // Get the ActivityContextInterfaceFactory
            NullActivityContextInterfaceFactory aciFactory = (NullActivityContextInterfaceFactory) TCKSbbUtils.getSbbEnvironment().lookup("slee/nullactivity/activitycontextinterfacefactory");

            // Create the null activities and attach this SBB to them.
            for (int i = 0; i < nActivities; i++) {
                nullActivities[i] = factory.createNullActivity();
            
                ActivityContextInterface nullAci = aciFactory.getActivityContextInterface(nullActivities[i]);
                nullAci.attach(getSbbContext().getSbbLocalObject());
            }

            ActivityContextInterface acis[];

            acis = getSbbContext().getActivities();
            if (acis.length != nActivities + 1) { // +1 for the initial activity
                map.put("Result", new Boolean(false));
                map.put("Message", "Number of activities returned by SbbContext.getActivities() was " + acis.length + " instead of the expected " + (nActivities + 1));
                map.put("ID", new Integer(3248));
                TCKSbbUtils.getResourceInterface().sendSbbMessage(map);
                removeActivities(nullActivities);
                return;
            }

            map.put("Result", new Boolean(true));
            map.put("Message", "Ok");
            TCKSbbUtils.getResourceInterface().sendSbbMessage(map);
            removeActivities(nullActivities);
            return;
        } catch (Exception e) {
            removeActivities(nullActivities);
            TCKSbbUtils.handleException(e);
        }

    }

    public void removeActivities(NullActivity [] nullActivities) {
        for (int i = 0; i < nullActivities.length; i++) {
            try {
                nullActivities[i].endActivity();
            } catch (Exception e) {
            }
        }
    }

}
