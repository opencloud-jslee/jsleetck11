/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.javax.slee.management.AlarmNotification;

import com.opencloud.sleetck.lib.SleeTCKTest;
import com.opencloud.sleetck.lib.SleeTCKTestUtils;
import com.opencloud.sleetck.lib.TCKTestResult;
import com.opencloud.sleetck.lib.resource.TCKActivityID;
import com.opencloud.sleetck.lib.resource.TCKSbbMessage;
import com.opencloud.sleetck.lib.resource.events.TCKResourceEventX;
import com.opencloud.sleetck.lib.resource.testapi.TCKResourceListener;
import com.opencloud.sleetck.lib.resource.testapi.TCKResourceTestInterface;
import com.opencloud.sleetck.lib.testutils.BaseTCKResourceListener;
import com.opencloud.sleetck.lib.testutils.FutureResult;
import com.opencloud.sleetck.lib.testutils.jmx.AlarmMBeanProxy;
import com.opencloud.sleetck.lib.testutils.jmx.DeploymentMBeanProxy;

import javax.management.Notification;
import javax.slee.ComponentID;
import javax.slee.SbbID;
import javax.slee.facilities.Level;
import javax.slee.management.AlarmNotification;
import javax.slee.management.DeployableUnitDescriptor;
import javax.slee.management.DeployableUnitID;
import java.rmi.RemoteException;
import java.util.HashMap;

public class Test4085Test implements SleeTCKTest, javax.management.NotificationListener {

    private static final String SERVICE_DU_PATH_PARAM = "DUPath";
    private static final int TEST_ID = 4085;

    public void init(SleeTCKTestUtils utils) {
        this.utils = utils;
    }

    /**
     * Perform the actual test.
     */

    public TCKTestResult run() throws Exception {
        result = new FutureResult(utils.getLog());

        TCKResourceTestInterface resource = utils.getResourceInterface();
        TCKActivityID activityID = resource.createActivity("Test4085InitialActivity");
        resource.fireEvent(TCKResourceEventX.X1, TCKResourceEventX.X1, activityID, null);

        return result.waitForResultOrFail(utils.getTestTimeout(), "Timeout waiting for test result.", TEST_ID);
    }

    /**
     * Do all the pre-run configuration of the test.
     */

    public void setUp() throws Exception {

        utils.getLog().fine("Connecting to resource");
        resourceListener = new TCKResourceListenerImpl();
        utils.getResourceInterface().setResourceListener(resourceListener);
        utils.getLog().fine("Installing and activating service");

        AlarmMBeanProxy alarmMBeanProxy = utils.getMBeanProxyFactory().createAlarmMBeanProxy(utils.getSleeManagementMBeanProxy().getAlarmMBean());
        alarmMBeanProxy.addNotificationListener(this, null, null);

        // Install the Deployable Units
        String duPath = utils.getTestParams().getProperty(SERVICE_DU_PATH_PARAM);
        duID = utils.install(duPath);

        // Activate the DU
        utils.activateServices(duID, true);

        // Retrieve the SbbID for later on.
        DeploymentMBeanProxy duProxy = utils.getDeploymentMBeanProxy();
        DeployableUnitDescriptor duDesc = duProxy.getDescriptor(duID);
        ComponentID components[] = duDesc.getComponents();
        // Get the SbbID from the components.
        for (int i = 0; i < components.length; i++) {
            if (components[i] instanceof SbbID) {
                utils.getLog().fine("Setting sbbID value.");
                sbbID = (SbbID) components[i];
                continue;
            }
        }
    }

    /**
     * Clean up after the test.
     */

    public void tearDown() throws Exception {
        utils.getLog().fine("Disconnecting from resource");
        utils.getResourceInterface().clearActivities();
        utils.getResourceInterface().removeResourceListener();
        utils.getLog().fine("Deactivating and uninstalling service");
        utils.deactivateAllServices();
        utils.uninstallAll();
    }

    private class TCKResourceListenerImpl extends BaseTCKResourceListener {
        public synchronized void onSbbMessage(TCKSbbMessage message, TCKActivityID calledActivity) throws RemoteException {

            HashMap map = (HashMap) message.getMessage();
            Boolean passed = (Boolean) map.get("Result");
            String msgString = (String) map.get("Message");

            utils.getLog().info("Received message from SBB.");

            if (passed.booleanValue() == true)
                result.setPassed();
            else
                result.setFailed(TEST_ID, msgString);
        }

        public void onException(Exception e) throws RemoteException {
            utils.getLog().warning("Received exception from SBB");
            utils.getLog().warning(e);
            result.setError(e);
        }
    }

    public synchronized void handleNotification(Notification notification, Object handback) {

        if (notification instanceof AlarmNotification) {

            AlarmNotification alarmNotification = (AlarmNotification) notification;

            if (firstAlarm == null) {
                firstAlarm = alarmNotification;
                return;
            }

            if (!alarmNotification.getAlarmType().equals("javax.slee.management.alarm")) {
                result.setFailed(4085, "AlarmNotification.getType() returned incorrect type.");
                return;
            }

            if (!alarmNotification.getAlarmSource().equals(sbbID)) {
                result.setFailed(4087, "AlarmNotification.getSource() returned incorrect source.");
                return;
            }

            if (!alarmNotification.getLevel().equals(Level.INFO)) {
                result.setFailed(4089, "AlarmNotification.getLevel() returned incorrect level.");
                return;
            }

            if (alarmNotification.getCause() != null) {
                result.setFailed(4091, "AlarmNotification.getCause() returned a cause, it should have been null.");
                return;
            }

            try {
                alarmNotification.hashCode();
            } catch (Exception e) {
                result.setFailed(4095, "AlarmNotification.hashCode() threw an exception.");
                return;
            }

            try {
                alarmNotification.toString();
            } catch (Exception e) {
                result.setFailed(4097, "AlarmNotification.toString() threw an exception.");
                return;
            }

            if (alarmNotification.toString() == null) {
                result.setFailed(4097, "AlarmNotification.toString() returned null.");
                return;
            }

            if (firstAlarm.equals(alarmNotification)) {
                result.setFailed(4093, "AlarmNotification.equals(AlarmNotification) returned true for non-identical alarms.");
                return;
            }

            if (!alarmNotification.equals(alarmNotification)) {
                result.setFailed(4093, "AlarmNotification.equals(AlarmNotification) returned false when comparing an alarm with itself.");
                return;
            }

            result.setPassed();
            return;
        }

        return;

    }

    private SleeTCKTestUtils utils;
    private TCKResourceListener resourceListener;
    private FutureResult result;
    private DeployableUnitID duID;
    private SbbID sbbID;
    private AlarmNotification firstAlarm;
}
