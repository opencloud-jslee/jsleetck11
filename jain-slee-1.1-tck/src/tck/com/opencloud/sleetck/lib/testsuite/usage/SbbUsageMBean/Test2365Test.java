/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.usage.SbbUsageMBean;

import com.opencloud.sleetck.lib.AbstractSleeTCKTest;
import com.opencloud.sleetck.lib.TCKTestResult;
import com.opencloud.sleetck.lib.testsuite.usage.common.UsageMBeanLookup;

import javax.slee.management.ManagementException;

/**
 * Deploys a service containing one parent SBB and two child SBBs.
 * The child SBBs define separate usage parameter interfaces, with
 * attributes: (one names a counter 'Foo', one names a sample parameter 'Foo').
 * The test accesses the current parameter values to check access.
 * Successfully accessing both MBeans proves that 2 interfaces were created.
 */
public class Test2365Test extends AbstractSleeTCKTest {

    private static final int TEST_ID = 2365;
    private static final String SHARED_PARAMETER_NAME = "foo";

    public TCKTestResult run() throws Exception {

        try {
            mBeanLookupSbbA.getUnnamedSbbUsageMBeanProxy().getCounterParameter(SHARED_PARAMETER_NAME,false);
            mBeanLookupSbbB.getUnnamedSbbUsageMBeanProxy().getSampleParameter(SHARED_PARAMETER_NAME, false);
        } catch(ManagementException e) {
            getLog().warning("Caught unexpected ManagementException while trying to access one of the usage parameter values:");
            getLog().warning(e);
            return TCKTestResult.failed(TEST_ID,"Failed to access one of the Foo parameter values due to a ManagementException. "+
                    "If the ManagementException is related to a missing attribute, or an invalid return type, this may indicate "+
                    "a failure to create 2 SbbUsageMBean interfaces for the 2 usage parameter interfaces. "+
                    "The ManagementException caught:"+e);
        }
        getLog().info("Successfully accessed the Foo parameter as a counter in one interface, and "+
                "as a sample in the other. This proves that 2 SbbUsageMBean interfaces were created.");
        return TCKTestResult.passed();
    }

    public void setUp() throws Exception {
        super.setUp();
        mBeanLookupSbbA = new UsageMBeanLookup("Test2365Service","Test2365SbbChildA",utils());
        mBeanLookupSbbB = new UsageMBeanLookup("Test2365Service","Test2365SbbChildB",utils());
    }

    public void tearDown() throws Exception {
        if(mBeanLookupSbbA != null)mBeanLookupSbbA.closeAllMBeans();
        if(mBeanLookupSbbB != null)mBeanLookupSbbB.closeAllMBeans();
        super.tearDown();
    }

    private UsageMBeanLookup mBeanLookupSbbA;
    private UsageMBeanLookup mBeanLookupSbbB;

}
