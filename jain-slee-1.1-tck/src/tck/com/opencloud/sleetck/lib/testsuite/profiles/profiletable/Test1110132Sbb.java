/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.profiles.profiletable;

import java.util.Collection;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Iterator;

import javax.naming.Context;
import javax.slee.ActivityContextInterface;
import javax.slee.Address;
import javax.slee.AddressPlan;
import javax.slee.CreateException;
import javax.slee.RolledBackContext;
import javax.slee.profile.ProfileAlreadyExistsException;
import javax.slee.profile.ProfileFacility;
import javax.slee.profile.ProfileTable;

import com.opencloud.sleetck.lib.resource.events.TCKResourceEventX;
import com.opencloud.sleetck.lib.sbbutils.BaseTCKSbb;
import com.opencloud.sleetck.lib.sbbutils.TCKSbbUtils;
import com.opencloud.sleetck.lib.sbbutils.events.TCKSbbEvent;
import com.opencloud.sleetck.lib.sbbutils.events.TCKSbbEventImpl;
import com.opencloud.sleetck.lib.sbbutils.events2.SbbBaseMessageComposer;
import com.opencloud.sleetck.lib.sbbutils.events2.SbbBaseMessageConstants;

public abstract class Test1110132Sbb extends BaseTCKSbb {

    public static final String PROFILE_TABLE_NAME = "Test1110132ProfileTable";
    public static final String PROFILE_TABLE_NAME2 = "Test1110132ProfileTable2";
    public static final String PROFILE_NAME = "Test1110132Profile";
    public static final String PROFILE_NAME2 = "Test1110132Profile2";
    public static final String PROFILE_NAME3 = "Test1110132Profile3";

    public static final int CREATE_COMMIT_PROFILE = 0;
    public static final int CREATE_ROLLBACK_PROFILE = 1;
    public static final int CHECK_ROLLBACK_PROFILE = 2;
    public static final int CHECK_COMMIT_PROFILE = 3;
    public static final int FIND_PROFILE = 4;
    public static final int FIND_PROFILE_INDEXED = 5;
    public static final int FIND_PROFILES_INDEXED = 6;
    public static final int REMOVE_COMMIT_PROFILE = 7;
    public static final int REMOVE_ROLLBACK_PROFILE  =8;
    public static final int CHECK_REMOVE_ROLLBACK = 9;
    public static final int CHECK_REMOVE_COMMIT = 10;
    public static final int SEND_TEST_MSG = 11;
    public static final int STATIC_QUERY = 12;



    private void sendLogMsgCall (String msg) {
        HashMap map = SbbBaseMessageComposer.getLogMsg(msg);
        try {
            TCKSbbUtils.getResourceInterface().callTest(map);
        }
        catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }
    }

    private void sendResult(boolean result, int id, String msg, ActivityContextInterface aci) {
        HashMap map = SbbBaseMessageComposer.getSetResultMsg(result, id, msg);
        fireTCKSbbEvent(new TCKSbbEventImpl(map), aci, null);
    }

    private void do_CREATE_COMMIT_PROFILE(Test1110132ProfileTable profileTable, ActivityContextInterface aci) throws Exception {
        //1110137:create throws NPE if invoked with null as argument
        try {
            profileTable.create(null);
            sendResult(false, 1110137, "create(null) should have thrown a NullPointerException.", aci);
            return;
        }
        catch (NullPointerException e) {
            sendLogMsgCall("create(null) threw NullPointerException as expected.");
        }
        catch (Exception e) {
            sendResult(false, 1110137, "Wrong type of exception thrown: "+ e.getClass().getName() +" Expected: NullPointerException", aci);
            return;
        }

        //1110138:create throws IllegalArgument exception if ProfileName contains illegal characters (outside of the range 0x0020-0x007e)
        try {
            Character ch = new Character('\u0019');
            profileTable.create(ch.toString());
            sendResult(false, 1110138, "create('"+ch.toString()+"') should have thrown an IllegalArgumentException.", aci);
            return;
        }
        catch (IllegalArgumentException e) {
            sendLogMsgCall("create('\u0019') threw IllegalArgumentException as expected.");
        }
        catch (Exception e) {
            sendResult(false, 1110138, "Wrong type of exception thrown: "+ e.getClass().getName() +" Expected: IllegalArgumentException", aci);
            return;
        }
        try {
            Character ch = new Character('\u007f');
            profileTable.create(ch.toString());
            sendResult(false,1110138 , "create('"+ch.toString()+"') should have thrown an IllegalArgumentException.", aci);
            return;
        }
        catch (IllegalArgumentException e) {
            sendLogMsgCall("create('\u007f') threw IllegalArgumentException as expected.");
        }
        catch (Exception e) {
            sendResult(false, 1110138, "Wrong type of exception thrown: "+ e.getClass().getName() +" Expected: IllegalArgumentException", aci);
            return;
        }

        //1110141: create propagates CreateException from profile object's profilePostCreate() method
        try {
            profileTable.create(Test1110132Profile.CREATE_EXCEPTION_PROFILE);
            sendResult(false, 1110141, "create(\""+Test1110132Profile.CREATE_EXCEPTION_PROFILE+"\") should have thrown a CreateException.", aci);
            return;
        }
        catch (CreateException e) {
            sendLogMsgCall("create(\""+Test1110132Profile.CREATE_EXCEPTION_PROFILE+"\") threw CreateException " +
                    "propagated from profile abstract class's profilePostCreate() method as expected.");
        }
        catch (Exception e) {
            sendResult(false, 1110141, "Wrong type of exception thrown: "+ e.getClass().getName() +" Expected: CreateException.", aci);
            return;
        }


        //1110132:create a profile
        //1110133:the object returned by create() can be typecast into the spec's ProfileLocal interface
        Test1110132ProfileLocal profileLocal = (Test1110132ProfileLocal)profileTable.create(PROFILE_NAME);

        //1110135: allow current transaction to commit, then check that profile is really there
        sendResult(true, 1110132, "Testsequence CREATE_COMMIT_PROFILE successful.", aci);
    }

    private void do_CHECK_COMMIT_PROFILE(Test1110132ProfileTable profileTable, ActivityContextInterface aci) {
        //1110132: check that create was successful
        //1110143: find() returns an object that can be typecast into profile specs ProfileLocal interface
        Test1110132ProfileLocal profileLocal = (Test1110132ProfileLocal)profileTable.find(PROFILE_NAME);
        if (profileLocal!=null)
            sendResult(true, 1110132, "Creating and committing profile "+PROFILE_NAME+" worked fine.", aci);
        else
            sendResult(false, 1110132, "Creating and committing profile "+
                            PROFILE_NAME+" failed.", aci);
    }

    private void do_CREATE_ROLLBACK_PROFILE(Test1110132ProfileTable profileTable, ActivityContextInterface aci) throws Exception {
        //1110140: Try to create a profile which already exists.
        try {
            profileTable.create(PROFILE_NAME);
            sendResult(false, 1110140, "create(\""+PROFILE_NAME+"\") should have thrown a ProfileAlreadyExistsException.", aci);
            return;
        }
        catch (ProfileAlreadyExistsException e) {
            sendLogMsgCall("create(\""+PROFILE_NAME+"\") threw ProfileAlreadyExistsException as expected.");
        }
        catch (Exception e) {
            sendResult(false, 1110140, "Wrong type of exception thrown: "+ e.getClass().getName() +" Expected: ProfileAlreadyExistsException", aci);
            return;
        }

        //1110135: allow current transaction to rollback, then check that profile is not created
        profileTable.create(PROFILE_NAME2);

        getSbbContext().setRollbackOnly();

        sendLogMsgCall("Creating profile "+PROFILE_NAME2+" then marking transaction for rollback.");
        //allow transaction to rollback, sbbRolledBack handler sends OK msg causing the TCK test to continue.
    }

    private void do_CHECK_ROLLBACK_PROFILE(Test1110132ProfileTable profileTable, ActivityContextInterface aci) {
        //1110135: check that 2nd profile has NOT been created
        //1110145: find() returns null if profile does not exist
        if (profileTable.find(PROFILE_NAME2)==null)
            sendResult(true, 1110135, "Creating profile "+PROFILE_NAME2+" followed by rollback worked fine.", aci);
        else
            sendResult(false, 1110135, "Profile "+PROFILE_NAME2+
                    " was created though transaction was rolled back.", aci);
    }

    private void do_FIND_PROFILE(Test1110132ProfileTable profileTable, ActivityContextInterface aci) throws Exception {
        //1110147: find() throws NPE if invoked with null argument
        try {
            profileTable.find(null);
            sendResult(false, 1110147, "find(null) should have thrown a NullPointerException.", aci);
            return;
        }
        catch (NullPointerException e) {
            sendLogMsgCall("find(null) threw NullPointerException as expected.");
        }
        catch (Exception e) {
            sendResult(false, 1110147, "Wrong type of exception thrown: "+ e.getClass().getName() +" Expected: NullPointerException", aci);
            return;
        }

        //1110148: findAll() returns all profiles other than the default profile of the profile table
        //1110149: returned object is a collection with a ProfileLocalObject for each profile found
        //1110150: each object in the collection may be typecast into the spec's ProfileLocal type
        Test1110132ProfileLocal profileLocal3 = (Test1110132ProfileLocal)profileTable.create(PROFILE_NAME3);
        Collection col = profileTable.findAll();
        Iterator iter = col.iterator();
        Hashtable tab = new Hashtable();
        while (iter.hasNext()) {
            Test1110132ProfileLocal prof = (Test1110132ProfileLocal)iter.next();
            tab.put(prof.getProfileName(), prof);
        }
        if (col.size()!=2 || !tab.containsKey(PROFILE_NAME) || !tab.containsKey(PROFILE_NAME3)) {
            sendResult(false, 1110148, "Wrong number of profiles or some profiles were missing from the collection returned by findAll()", aci);
            return;
        }
        else {
            sendLogMsgCall("findAll() returned the correct list of profiles.");
        }

        //1110151: collection object is immutable, any attempt to modify it results in an UnsupportedOperationException
        try {
            col.add("1");
            sendResult(false, 1110151, "Modification attempt on collection object should have caused a UnsupportedOperationException.", aci);
            return;
        }
        catch (UnsupportedOperationException e) {
            sendLogMsgCall("Modification attempt on collection object caused UnsupportedOperationException as expected.");
        }
        catch (Exception e) {
            sendResult(false, 1110151, "Wrong type of exception thrown: "+ e.getClass().getName() +" Expected: UnsupportedOperationException", aci);
            return;
        }
        try {
            col.clear();
            sendResult(false, 1110151, "Modification attempt on collection object should have caused a UnsupportedOperationException.", aci);
            return;
        }
        catch (UnsupportedOperationException e) {
            sendLogMsgCall("Modification attempt on collection object caused UnsupportedOperationException as expected.");
        }
        catch (Exception e) {
            sendResult(false, 1110151, "Wrong type of exception thrown: "+ e.getClass().getName() +" Expected: UnsupportedOperationException", aci);
            return;
        }
        try {
            col.remove("1");
            sendResult(false, 1110151, "Modification attempt on collection object should have caused a UnsupportedOperationException.", aci);
            return;
        }
        catch (UnsupportedOperationException e) {
            sendLogMsgCall("Modification attempt on collection object caused UnsupportedOperationException as expected.");
        }
        catch (Exception e) {
            sendResult(false, 1110151, "Wrong type of exception thrown: "+ e.getClass().getName() +" Expected: UnsupportedOperationException", aci);
            return;
        }

        //1110152: if no profiles exist in the table then an empty collection is returned
        Context env = TCKSbbUtils.getSbbEnvironment();
        ProfileFacility profileFacility = (ProfileFacility)env.lookup("slee/facilities/profile");
        ProfileTable profileTable2 = profileFacility.getProfileTable(PROFILE_TABLE_NAME2);
        Collection col2 = profileTable2.findAll();
        if (col2.size()!=0) {
            sendResult(false, 1110152, "Collection returned by findAll() should be empty as profile table "+PROFILE_TABLE_NAME2+" does not contain any profiles.", aci);
            return;
        }

        //sendLogMsgCall("Rollback?: "+this.getSbbContext().getRollbackOnly());

        sendResult(true,1110132,"Testsequence FIND_PROFILE successful.",aci);
    }

    private void do_FIND_PROFILE_INDEXED(Test1110132ProfileTable profileTable, ActivityContextInterface aci) throws Exception {

        //1110676: This method can only be used on attributes whose Java type is a
        //primitive type, an Object wrapper of primitive type (e.g. java.lang.Integer),
        //java.lang.String, javax.slee.Address, or an array of one of these types.
        //1110154: Equality for attributes of primitive types is specified by direct
        //value comparison.    Equality for object types is evaluated by
        //java.lang.Object.equals(Object)
        Test1110132ProfileLocal profileLocal = (Test1110132ProfileLocal)profileTable.find(PROFILE_NAME);
        Test1110132ProfileLocal profileLocal3 = (Test1110132ProfileLocal)profileTable.find(PROFILE_NAME3);
        profileLocal3.setStringValue("42");
        profileLocal3.setIntValue(42);
        profileLocal3.setIntObjValue(new Integer(42));
        profileLocal3.setCharValue('X');
        profileLocal3.setCharObjValue(new Character('X'));
        profileLocal3.setBoolValue(true);
        profileLocal3.setBoolObjValue(new Boolean(true));
        profileLocal3.setAddress(new Address(AddressPlan.SLEE_PROFILE, "XY/A"));

        profileLocal.setStringValue("41");
        profileLocal.setIntValue(41);
        profileLocal.setIntObjValue(new Integer(41));
        profileLocal.setCharValue('Y');
        profileLocal.setCharObjValue(new Character('Y'));
        profileLocal.setBoolValue(false);
        profileLocal.setBoolObjValue(new Boolean(false));
        profileLocal.setAddress(new Address(AddressPlan.SLEE_PROFILE, "XY/B"));

        Test1110132ProfileLocal result = null;
        //1110158: The returned object may be typecast to the spec's ProfileLocal type
        //find for String attribute
        result = (Test1110132ProfileLocal) profileTable.findProfileByAttribute("stringValue", "42");
        if (result==null || !result.getProfileName().equals(PROFILE_NAME3)) {
            sendResult(false, 1110154, "Wrong profile or null returned by findProfileByAttribute() on attribute 'stringValue'", aci);
            return;
        }
        else {
            sendLogMsgCall("Correct profile with 'stringValue=\"42\"' found.");
        }
        //find for int and its object wrapper attribute
        result = (Test1110132ProfileLocal) profileTable.findProfileByAttribute("intValue", new Integer(42));
        if (result==null || !result.getProfileName().equals(PROFILE_NAME3)) {
            sendResult(false, 1110154, "Wrong profile or null returned by findProfileByAttribute() on attribute 'intValue'", aci);
            return;
        }
        else {
            sendLogMsgCall("Correct profile with 'intValue=42' found.");
        }
        result = (Test1110132ProfileLocal) profileTable.findProfileByAttribute("intObjValue", new Integer(42));
        if (result==null || !result.getProfileName().equals(PROFILE_NAME3)) {
            sendResult(false, 1110154, "Wrong profile or null returned by findProfileByAttribute() on attribute 'intObjValue'", aci);
            return;
        }
        else {
            sendLogMsgCall("Correct profile with 'intObjValue=42' found.");
        }
        //find for char and its object wrapper attribute
        result = (Test1110132ProfileLocal) profileTable.findProfileByAttribute("charValue", new Character('X'));
        if (result==null || !result.getProfileName().equals(PROFILE_NAME3)) {
            sendResult(false, 1110154, "Wrong profile or null returned by findProfileByAttribute() on attribute 'charValue'", aci);
            return;
        }
        else {
            sendLogMsgCall("Correct profile with 'charValue='X'' found.");
        }
        result = (Test1110132ProfileLocal) profileTable.findProfileByAttribute("charObjValue", new Character('X'));
        if (result==null || !result.getProfileName().equals(PROFILE_NAME3)) {
            sendResult(false, 1110154, "Wrong profile or null returned by findProfileByAttribute() on attribute 'charObjValue'", aci);
            return;
        }
        else {
            sendLogMsgCall("Correct profile with 'charObjValue='X'' found.");
        }
        //find for boolean and its object wrapper attribute
        result = (Test1110132ProfileLocal) profileTable.findProfileByAttribute("boolValue", new Boolean(true));
        if (result==null || !result.getProfileName().equals(PROFILE_NAME3)) {
            sendResult(false, 1110154, "Wrong profile or null returned by findProfileByAttribute() on attribute 'boolValue'", aci);
            return;
        }
        else {
            sendLogMsgCall("Correct profile with 'boolValue=true' found.");
        }
        result = (Test1110132ProfileLocal) profileTable.findProfileByAttribute("boolObjValue", new Boolean(true));
        if (result==null || !result.getProfileName().equals(PROFILE_NAME3)) {
            sendResult(false, 1110154, "Wrong profile or null returned by findProfileByAttribute() on attribute 'boolObjValue'", aci);
            return;
        }
        else {
            sendLogMsgCall("Correct profile with 'boolObjValue=true' found.");
        }
        //find for Address attribute
        result = (Test1110132ProfileLocal) profileTable.findProfileByAttribute("address", new Address(AddressPlan.SLEE_PROFILE, "XY/A"));
        if (result==null || !result.getProfileName().equals(PROFILE_NAME3)) {
            sendResult(false, 1110154, "Wrong profile or null returned by findProfileByAttribute() on attribute 'address'", aci);
            return;
        }
        else {
            sendLogMsgCall("Correct profile with 'address=(AddressPlan.SLEE_PROFILE, \"XY/A\")' found.");
        }

        //1110156: the default profile is not considered when determining a match
        //1110157: If no matching profiles are found the method returns null
        if     (profileTable.findProfileByAttribute("intValue", new Integer(0))!= null ||
                profileTable.findProfileByAttribute("stringValue", "0")!= null ||
                profileTable.findProfileByAttribute("address", new Address(AddressPlan.SLEE_PROFILE,"XY/0"))!= null) {
            sendResult(false, 1110156, "findProfileByAttribute(...) should not consider the default profile when determining matching profiles.", aci);
            return;
        }
        else {
            sendLogMsgCall("findProfileByAttribute(...) correctly ignored the default profile and returned null for each query.");
        }

        //1110160:findProfileByIndexedAttribute throws NPE if invoked with null as arguments
        try {
            profileTable.findProfileByAttribute(null, "42");
            sendResult(false, 1110160,"findProfileByAttribute(null, \"42\") should have thrown a NullPointerException.", aci);
            return;
        }
        catch (NullPointerException e) {
            sendLogMsgCall("findProfileByAttribute(null, \"42\") threw NullPointerException as expected.");
        }
        catch (Exception e) {
            sendResult(false, 1110160,"Wrong type of exception thrown: "+ e.getClass().getName() +" Expected: NullPointerException", aci);
            return;
        }
        try {
            profileTable.findProfileByAttribute("stringValue", null);
            sendResult(false, 1110160,"findProfileByAttribute(\"stringValue\", null) should have thrown a NullPointerException.",aci);
            return;
        }
        catch (NullPointerException e) {
            sendLogMsgCall("findProfileByAttribute(\"stringValue\", null) threw NullPointerException as expected.");
        }
        catch (Exception e) {
            sendResult(false, 1110160,"Wrong type of exception thrown: "+ e.getClass().getName() +" Expected: NullPointerException",aci);
            return;
        }

        //1110678: java.lang.IllegalArgumentException: This exception is thrown in the following scenarios:
        //if an attribute with the specified name is not defined in the Profile Specification of the specified Profile Table,
        //if the type of the attributeValue argument is different to the type of the Profile attribute,
        //if the type of the attribute is not one of the allowed types.
        try {
            profileTable.findProfileByAttribute("xyz", "42");
            sendResult(false, 1110678, "findProfileByAttribute(\"xyz\", \"42\") should have thrown a IllegalArgumentException.", aci);
        } catch(IllegalArgumentException e) {
            sendLogMsgCall("findProfileByAttribute(\"xyz\", \"42\") threw IllegalArgumentException as expected.");
        } catch(Exception e) {
            sendResult(false, 1110678,"Wrong type of exception thrown: "+ e.getClass().getName() +" Expected: IllegalArgumentException",aci);
            return;
        }
        try {
            profileTable.findProfileByAttribute("intValue", "42");
            sendResult(false, 1110678, "findProfileByAttribute(\"intValue\", \"42\") should have thrown a IllegalArgumentException.", aci);
        } catch(IllegalArgumentException e) {
            sendLogMsgCall("findProfileByAttribute(\"intValue\", \"42\") threw IllegalArgumentException as expected.");
        } catch(Exception e) {
            sendResult(false, 1110678,"Wrong type of exception thrown: "+ e.getClass().getName() +" Expected: IllegalArgumentException",aci);
            return;
        }
        try {
            profileTable.findProfileByAttribute("myCMPValue", new MyCMPType());
            sendResult(false, 1110678, "findProfileByAttribute(\"myCMPValue\",...) should have thrown a IllegalArgumentException.", aci);
        } catch(IllegalArgumentException e) {
            sendLogMsgCall("findProfileByAttribute(\"myCMPValue\", ...) threw IllegalArgumentException as expected.");
        } catch(Exception e) {
            sendResult(false, 1110678,"Wrong type of exception thrown: "+ e.getClass().getName() +" Expected: IllegalArgumentException",aci);
            return;
        }

        sendResult(true, 1110132,"Testsequence FIND_PROFILE_INDEXED successful." ,aci);
    }

    private void do_FIND_PROFILES_INDEXED(Test1110132ProfileTable profileTable, ActivityContextInterface aci) throws Exception {

        //1110681: This method can only be used on attributes whose Java type is a
        //primitive type, an Object wrapper of primitive type (e.g. java.lang.Integer),
        //java.lang.String, javax.slee.Address, or an array of one of these types.
        //1110165: Equality for attributes of primitive types is specified by direct
        //value comparison.    Equality for object types is evaluated by
        //java.lang.Object.equals(Object)
        Test1110132ProfileLocal profileLocal = (Test1110132ProfileLocal)profileTable.find(PROFILE_NAME);
        Test1110132ProfileLocal profileLocal3 = (Test1110132ProfileLocal)profileTable.find(PROFILE_NAME3);
        profileLocal3.setStringValue("42");
        profileLocal3.setIntValue(42);
        profileLocal3.setIntObjValue(new Integer(42));
        profileLocal3.setCharValue('X');
        profileLocal3.setCharObjValue(new Character('X'));
        profileLocal3.setBoolValue(true);
        profileLocal3.setBoolObjValue(new Boolean(true));
        profileLocal3.setAddress(new Address(AddressPlan.SLEE_PROFILE, "XY/A"));

        profileLocal.setStringValue("42");
        profileLocal.setIntValue(41);
        profileLocal.setIntObjValue(new Integer(41));
        profileLocal.setCharValue('Y');
        profileLocal.setCharObjValue(new Character('Y'));
        profileLocal.setBoolValue(false);
        profileLocal.setBoolObjValue(new Boolean(false));
        profileLocal.setAddress(new Address(AddressPlan.SLEE_PROFILE, "XY/A"));

        Collection profiles = null;
        Iterator profileIter = null;
        Hashtable profileNamesTab = null;

        //stringValue attribute
        profileNamesTab = new Hashtable();
        profiles= profileTable.findProfilesByAttribute("stringValue", "42");
        profileIter = profiles.iterator();
        while (profileIter.hasNext()) {
            Test1110132ProfileLocal pL = (Test1110132ProfileLocal)profileIter.next();
            profileNamesTab.put(pL.getProfileName(), pL);
        }
        if (profiles.size()!=2 || !profileNamesTab.containsKey(PROFILE_NAME) || !profileNamesTab.containsKey(PROFILE_NAME3) ) {
            sendResult(false, 1110167, "Wrong profiles returned by findProfilesByAttribute() on attribute 'stringValue'", aci);
            return;
        }
        else {
            sendLogMsgCall("Correct profiles with 'stringValue=\"42\"' found.");
        }
        //intValue attribute and Object wrapper
        profileNamesTab = new Hashtable();
        //1110167: The method returns a Collection with one ProfileLocal object for each matching profile
        //1110168: Each of the ProfileLocal objects may be typecast into the spec's ProfileLocal interface type
        profiles= profileTable.findProfilesByAttribute("intValue", new Integer(42));
        profileIter = profiles.iterator();
        while (profileIter.hasNext()) {
            Test1110132ProfileLocal pL = (Test1110132ProfileLocal)profileIter.next();
            profileNamesTab.put(pL.getProfileName(), pL);
        }
        if (profiles.size()!=1 || !profileNamesTab.containsKey(PROFILE_NAME3)) {
            sendResult(false, 1110167, "Wrong profiles returned by findProfilesByAttribute() on attribute 'intValue'", aci);
            return;
        }
        else {
            sendLogMsgCall("Correct profiles with 'intValue=42' found.");
        }
        profileNamesTab = new Hashtable();
        //1110167: The method returns a Collection with one ProfileLocal object for each matching profile
        //1110168: Each of the ProfileLocal objects may be typecast into the spec's ProfileLocal interface type
        profiles= profileTable.findProfilesByAttribute("intObjValue", new Integer(42));
        profileIter = profiles.iterator();
        while (profileIter.hasNext()) {
            Test1110132ProfileLocal pL = (Test1110132ProfileLocal)profileIter.next();
            profileNamesTab.put(pL.getProfileName(), pL);
        }
        if (profiles.size()!=1 || !profileNamesTab.containsKey(PROFILE_NAME3)) {
            sendResult(false, 1110167, "Wrong profiles returned by findProfilesByAttribute() on attribute 'intObjValue'", aci);
            return;
        }
        else {
            sendLogMsgCall("Correct profiles with 'intObjValue=Integer(42)' found.");
        }
        //charValue attribute and Object wrapper
        profileNamesTab = new Hashtable();
        //1110167: The method returns a Collection with one ProfileLocal object for each matching profile
        //1110168: Each of the ProfileLocal objects may be typecast charo the spec's ProfileLocal charerface type
        profiles= profileTable.findProfilesByAttribute("charValue", new Character('X'));
        profileIter = profiles.iterator();
        while (profileIter.hasNext()) {
            Test1110132ProfileLocal pL = (Test1110132ProfileLocal)profileIter.next();
            profileNamesTab.put(pL.getProfileName(), pL);
        }
        if (profiles.size()!=1 || !profileNamesTab.containsKey(PROFILE_NAME3)) {
            sendResult(false, 1110167, "Wrong profiles returned by findProfilesByAttribute() on attribute 'charValue'", aci);
            return;
        }
        else {
            sendLogMsgCall("Correct profiles with 'charValue='X'' found.");
        }
        profileNamesTab = new Hashtable();
        //1110167: The method returns a Collection with one ProfileLocal object for each matching profile
        //1110168: Each of the ProfileLocal objects may be typecast charo the spec's ProfileLocal charerface type
        profiles= profileTable.findProfilesByAttribute("charObjValue", new Character('X'));
        profileIter = profiles.iterator();
        while (profileIter.hasNext()) {
            Test1110132ProfileLocal pL = (Test1110132ProfileLocal)profileIter.next();
            profileNamesTab.put(pL.getProfileName(), pL);
        }
        if (profiles.size()!=1 || !profileNamesTab.containsKey(PROFILE_NAME3)) {
            sendResult(false, 1110167, "Wrong profiles returned by findProfilesByAttribute() on attribute 'charObjValue'", aci);
            return;
        }
        else {
            sendLogMsgCall("Correct profiles with 'charObjValue=Character('X')' found.");
        }
        //boolValue attribute and Object wrapper
        profileNamesTab = new Hashtable();
        //1110167: The method returns a Collection with one ProfileLocal object for each matching profile
        //1110168: Each of the ProfileLocal objects may be typecast boolo the spec's ProfileLocal boolerface type
        profiles= profileTable.findProfilesByAttribute("boolValue", new Boolean(true));
        profileIter = profiles.iterator();
        while (profileIter.hasNext()) {
            Test1110132ProfileLocal pL = (Test1110132ProfileLocal)profileIter.next();
            profileNamesTab.put(pL.getProfileName(), pL);
        }
        if (profiles.size()!=1 || !profileNamesTab.containsKey(PROFILE_NAME3)) {
            sendResult(false, 1110167, "Wrong profiles returned by findProfilesByAttribute() on attribute 'boolValue'", aci);
            return;
        }
        else {
            sendLogMsgCall("Correct profiles with 'boolValue=true' found.");
        }
        profileNamesTab = new Hashtable();
        //1110167: The method returns a Collection with one ProfileLocal object for each matching profile
        //1110168: Each of the ProfileLocal objects may be typecast boolo the spec's ProfileLocal boolerface type
        profiles= profileTable.findProfilesByAttribute("boolObjValue", new Boolean(true));
        profileIter = profiles.iterator();
        while (profileIter.hasNext()) {
            Test1110132ProfileLocal pL = (Test1110132ProfileLocal)profileIter.next();
            profileNamesTab.put(pL.getProfileName(), pL);
        }
        if (profiles.size()!=1 || !profileNamesTab.containsKey(PROFILE_NAME3)) {
            sendResult(false, 1110167, "Wrong profiles returned by findProfilesByAttribute() on attribute 'boolObjValue'", aci);
            return;
        }
        else {
            sendLogMsgCall("Correct profiles with 'boolObjValue=Boolean(true)' found.");
        }
        //address attribute
        profileNamesTab = new Hashtable();
        profiles= profileTable.findProfilesByAttribute("address", new Address(AddressPlan.SLEE_PROFILE, "XY/A"));
        profileIter = profiles.iterator();
        while (profileIter.hasNext()) {
            Test1110132ProfileLocal pL = (Test1110132ProfileLocal)profileIter.next();
            profileNamesTab.put(pL.getProfileName(), pL);
        }
        if (profiles.size()!=2 || !profileNamesTab.containsKey(PROFILE_NAME3) || !profileNamesTab.containsKey(PROFILE_NAME) ) {
            sendResult(false,1110167, "Wrong profiles returned by findProfilesByIndexedAttribute() on attribute 'address'", aci );
            return;
        }
        else {
            sendLogMsgCall("Correct profiles with 'address=(AddressPlan.SLEE_PROFILE, \"XY/A\")' found.");
        }

        //1110166: the default profile is not considered when determining a match
        //1110170: If no matching profiles are found the method returns null
        if     (profileTable.findProfilesByAttribute("intValue", new Integer(0)).size()!= 0 ||
                profileTable.findProfilesByAttribute("stringValue", "0").size()!= 0 ||
                profileTable.findProfilesByAttribute("address", new Address(AddressPlan.SLEE_PROFILE, "XY/0")).size()!= 0) {
            sendResult(false,1110166, "findProfilesByAttribute() should not consider the default profile when determining matching profiles.", aci );
            return;
        }
        else {
            sendLogMsgCall("findProfilesByAttribute() correctly ignored the default profile and returned empty collections for each query.");
        }

        //1110169: collection object is immutable, any attempt to modify it results in an UnsupportedOperationException
        try {
            profiles.add("1");
            sendResult(false,1110169,"Modification attempt on collection object should have caused a UnsupportedOperationException.",aci);
            return;
        }
        catch (UnsupportedOperationException e) {
            sendLogMsgCall("Modification attempt on collection object caused UnsupportedOperationException as expected.");
        }
        catch (Exception e) {
            sendResult(false, 1110169,"Wrong type of exception thrown: "+ e.getClass().getName() +" Expected: UnsupportedOperationException", aci);
            return;
        }
        try {
            profiles.clear();
            sendResult(false,1110169,"Modification attempt on collection object should have caused a UnsupportedOperationException.",aci );
            return;
        }
        catch (UnsupportedOperationException e) {
            sendLogMsgCall("Modification attempt on collection object caused UnsupportedOperationException as expected.");
        }
        catch (Exception e) {
            sendResult(false, 1110169,"Wrong type of exception thrown: "+ e.getClass().getName() +" Expected: UnsupportedOperationException",aci);
            return;
        }
        try {
            profiles.remove("1");
            sendResult(false, 1110169, "Modification attempt on collection object should have caused a UnsupportedOperationException.", aci);
            return;
        }
        catch (UnsupportedOperationException e) {
            sendLogMsgCall("Modification attempt on collection object caused UnsupportedOperationException as expected.");
        }
        catch (Exception e) {
            sendResult(false, 1110169, "Wrong type of exception thrown: "+ e.getClass().getName() +" Expected: UnsupportedOperationException", aci);
            return;
        }

        //1110172:findProfilesByIndexedAttribute throws NPE if invoked with null as arguments
        try {
            profileTable.findProfilesByAttribute(null, "42");
            sendResult(false, 1110172,"findProfilesByAttribute(null, \"42\") should have thrown a NullPointerException.",aci);
            return;
        }
        catch (NullPointerException e) {
            sendLogMsgCall("findProfilesByAttribute(null, \"42\") threw NullPointerException as expected.");
        }
        catch (Exception e) {
            sendResult(false, 1110172,"Wrong type of exception thrown: "+ e.getClass().getName() +" Expected: NullPointerException",aci);
            return;
        }
        try {
            profileTable.findProfilesByAttribute("stringValue", null);
            sendResult(false,1110172,"findProfilesByAttribute(\"stringValue\", null) should have thrown a NullPointerException.",aci );
            return;
        }
        catch (NullPointerException e) {
            sendLogMsgCall("findProfilesByAttribute(\"stringValue\", null) threw NullPointerException as expected.");
        }
        catch (Exception e) {
            sendResult(false,1110172,"Wrong type of exception thrown: "+ e.getClass().getName() +" Expected: NullPointerException",aci);
            return;
        }

        //1110683: java.lang.IllegalArgumentException: This exception is thrown in the following scenarios:
        //if an attribute with the specified name is not defined in the Profile Specification of the specified Profile Table,
        //if the type of the attributeValue argument is different to the type of the Profile attribute,
        //if the type of the attribute is not one of the allowed types.
        try {
            profileTable.findProfileByAttribute("xyz", "42");
            sendResult(false, 1110683, "findProfilesByAttribute(\"xyz\", \"42\") should have thrown a IllegalArgumentException.", aci);
        } catch(IllegalArgumentException e) {
            sendLogMsgCall("findProfilesByAttribute(\"xyz\", \"42\") threw IllegalArgumentException as expected.");
        } catch(Exception e) {
            sendResult(false, 1110683,"Wrong type of exception thrown: "+ e.getClass().getName() +" Expected: IllegalArgumentException",aci);
            return;
        }
        try {
            profileTable.findProfileByAttribute("intValue", "42");
            sendResult(false, 1110683, "findProfilesByAttribute(\"intValue\", \"42\") should have thrown a IllegalArgumentException.", aci);
        } catch(IllegalArgumentException e) {
            sendLogMsgCall("findProfilesByAttribute(\"intValue\", \"42\") threw IllegalArgumentException as expected.");
        } catch(Exception e) {
            sendResult(false, 1110683,"Wrong type of exception thrown: "+ e.getClass().getName() +" Expected: IllegalArgumentException",aci);
            return;
        }
        try {
            profileTable.findProfileByAttribute("myCMPValue", new MyCMPType());
            sendResult(false, 1110683, "findProfilesByAttribute(\"myCMPValue\",...) should have thrown a IllegalArgumentException.", aci);
        } catch(IllegalArgumentException e) {
            sendLogMsgCall("findProfilesByAttribute(\"myCMPValue\", ...) threw IllegalArgumentException as expected.");
        } catch(Exception e) {
            sendResult(false, 1110683,"Wrong type of exception thrown: "+ e.getClass().getName() +" Expected: IllegalArgumentException",aci);
            return;
        }

        sendResult(true, 1110132, "Testsequence FIND_PROFILES_INDEXED successful.", aci);
    }

    private void do_REMOVE_ROLLBACK_PROFILE(Test1110132ProfileTable profileTable, ActivityContextInterface aci) {
        //1110178: allow current transaction to rollback, then check that profile is not removed
        profileTable.remove(PROFILE_NAME);

        getSbbContext().setRollbackOnly();

        sendLogMsgCall("Removing profile "+PROFILE_NAME+" then marking transaction for rollback.");
        //allow transaction to rollback, sbbRolledBack handler sends OK msg causing the TCK test to continue.
    }

    private void do_CHECK_REMOVE_ROLLBACK(Test1110132ProfileTable profileTable, ActivityContextInterface aci) {
        //1110178: check that profile has NOT been removed
        if (profileTable.find(PROFILE_NAME)!=null)
            sendResult(true, 1110178,"Removing profile "+PROFILE_NAME+" then rolling back worked fine, profile still exists.", aci );
        else
            sendResult(false, 1110178,"Profile "+
                PROFILE_NAME+" was removed though transaction was rolled back.",aci );
    }

    private void do_REMOVE_COMMIT_PROFILE(Test1110132ProfileTable profileTable, ActivityContextInterface aci) {
        //1110177: remove() returns false if no profile with the specified name existed in the profile table
        //but true if a profile with this name was found and removed
        if (profileTable.remove("XYZ")!=false) {
            sendResult(false, 1110177,"remove(\"XYZ\") should have returned 'false' as profile "+
                    " 'XYZ' does not exist in the profile table", aci);
            return;
        }
        else {
            sendLogMsgCall("remove(\"XYZ\") correctly returned false as profile 'XYZ' does not exist.");
        }
        if (profileTable.remove(PROFILE_NAME)==false) {
            sendResult(false,1110177,"remove() should have returned 'true' as profile "+
                    PROFILE_NAME+" exists in the profile table.",aci );
            return;
        }
        else {
            sendLogMsgCall("remove() correctly returned true and deleted profile "+PROFILE_NAME);
        }

        //1110180: remove() throws NPE if invoked with null as argument
        try {
            profileTable.remove(null);
            sendResult(false, 1110180,"remove(null) should have thrown a NullPointerException.",aci);
            return;
        }
        catch (NullPointerException e) {
            sendLogMsgCall("remove(null) threw NullPointerException as expected.");
        }
        catch (Exception e) {
            sendResult(false, 1110180,"Wrong type of exception thrown: "+ e.getClass().getName() +" Expected: NullPointerException",aci);
            return;
        }

        //allow current transaction to commit, then check that profile is really there
        sendResult(true, 1110132, "Testsequence REMOVE_COMMIT_PROFILE successful.", aci);
    }

    private void do_CHECK_REMOVE_COMMIT(Test1110132ProfileTable profileTable, ActivityContextInterface aci) {
        //1110178: check that removal was successful
        if (profileTable.find(PROFILE_NAME)==null)
            sendResult(true, 1110178,"Removing and committing profile "+PROFILE_NAME+" worked fine.",aci);
        else
            sendResult(false, 1110178,"Removing and committing profile "+
                    PROFILE_NAME+" failed.",aci);
    }

    private void do_STATIC_QUERY(Test1110132ProfileTable profileTable, ActivityContextInterface aci) {
        //1110188: each query returns a Collection with a ProfileLocal objects for each profile that satisfies the query
        //1110189: each of the ProfileLocal objects may be typecast into the Profile specifications ProfileLocal type.
        Test1110132ProfileLocal profileLocal = (Test1110132ProfileLocal)profileTable.find(PROFILE_NAME);
        Test1110132ProfileLocal profileLocal3 = (Test1110132ProfileLocal)profileTable.find(PROFILE_NAME3);
        profileLocal3.setStringValue("42");
        profileLocal3.setIntValue(42);
        profileLocal3.setAddress(new Address(AddressPlan.SLEE_PROFILE, "XY/A"));
        profileLocal.setStringValue("41");
        profileLocal.setIntValue(42);
        profileLocal.setAddress(new Address(AddressPlan.SLEE_PROFILE, "XY/B"));

        Collection col = null;

        //test 'queryIsAnswerToLife'
        col = profileTable.queryIsAnswerToLife();
        if (col.size()!=1 || !((Test1110132ProfileLocal)col.iterator().next()).getProfileName().equals(PROFILE_NAME3)) {
            sendResult(false, 1110188, "Wrong profile found for profile query 'queryIsAnswerToLife'" , aci);
            return;
        }
        else
            sendLogMsgCall("Correct profile was returned for profile query 'queryIsAnswerToLife'");

        //test 'queryEqualsValue'
        col = profileTable.queryEqualsValue("42");
        if (col.size()!=1 || !((Test1110132ProfileLocal)col.iterator().next()).getProfileName().equals(PROFILE_NAME3)) {
            sendResult(false, 1110188, "Wrong profile found for profile query 'queryEqualsValue(\"42\")'" , aci);
            return;
        }
        else
            sendLogMsgCall("Correct profile was returned for profile query 'queryEqualsValue(\"42\")'");

        //test 'queryNotEqualsValue'
        col = profileTable.queryNotEqualsValue("41");
        if (col.size()!=1 || !((Test1110132ProfileLocal)col.iterator().next()).getProfileName().equals(PROFILE_NAME3)) {
            sendResult(false, 1110188, "Wrong profile found for profile query 'queryNotEqualsValue(\"41\")'" , aci);
            return;
        }
        else
            sendLogMsgCall("Correct profile was returned for profile query 'queryNotEqualsValue(\"41\")'");

        //test 'queryNotEqualsValues'
        col = profileTable.queryNotEqualsValues("41", 41);
        if (col.size()!=1 || !((Test1110132ProfileLocal)col.iterator().next()).getProfileName().equals(PROFILE_NAME3)) {
            sendResult(false, 1110188, "Wrong profile found for profile query 'queryNotEqualsValues(\"41\", 41)'" , aci);
            return;
        }
        else
            sendLogMsgCall("Correct profile was returned for profile query 'queryNotEqualsValues(\"41\", 41)'");

        sendResult(true, 1110188,"Testsequence STATIC_QUERY successful.",aci);
    }

    public void onTCKResourceEventX1(TCKResourceEventX event, ActivityContextInterface aci) {
        try {

            int operationID = ((Integer)event.getMessage()).intValue();
            Context env = TCKSbbUtils.getSbbEnvironment();
            ProfileFacility profileFacility = (ProfileFacility)env.lookup("slee/facilities/profile");
            Test1110132ProfileTable profileTable = (Test1110132ProfileTable)profileFacility.getProfileTable(PROFILE_TABLE_NAME);

            switch (operationID) {
            case CREATE_COMMIT_PROFILE:
                do_CREATE_COMMIT_PROFILE(profileTable, aci);
                break;
            case CHECK_COMMIT_PROFILE:
                do_CHECK_COMMIT_PROFILE(profileTable, aci);
                break;
            case CREATE_ROLLBACK_PROFILE:
                do_CREATE_ROLLBACK_PROFILE(profileTable, aci);
                break;
            case CHECK_ROLLBACK_PROFILE:
                do_CHECK_ROLLBACK_PROFILE(profileTable, aci);
                break;
            case FIND_PROFILE:
                do_FIND_PROFILE(profileTable, aci);
                break;
            case FIND_PROFILE_INDEXED:
                do_FIND_PROFILE_INDEXED(profileTable, aci);
                break;
            case FIND_PROFILES_INDEXED:
                do_FIND_PROFILES_INDEXED(profileTable, aci);
                break;
            case REMOVE_ROLLBACK_PROFILE:
                do_REMOVE_ROLLBACK_PROFILE(profileTable, aci);
                break;
            case CHECK_REMOVE_ROLLBACK:
                do_CHECK_REMOVE_ROLLBACK(profileTable, aci);
                break;
            case REMOVE_COMMIT_PROFILE:
                do_REMOVE_COMMIT_PROFILE(profileTable, aci);
                break;
            case CHECK_REMOVE_COMMIT:
                do_CHECK_REMOVE_COMMIT(profileTable, aci);
                break;
            case STATIC_QUERY:
                do_STATIC_QUERY(profileTable, aci);
                break;
            }

        } catch(Exception e) {
            TCKSbbUtils.handleException(e);
        }
    }

    public void onTCKSbbEvent(TCKSbbEvent event, ActivityContextInterface aci) {
        try {
            HashMap payload = (HashMap)event.getMessage();
            int type = ((Integer)payload.get("Type")).intValue();

            switch (type) {
            case SbbBaseMessageConstants.TYPE_SET_RESULT:
                TCKSbbUtils.getResourceInterface().sendSbbMessage(payload);
                break;
            case SbbBaseMessageConstants.TYPE_LOG_MSG:
                TCKSbbUtils.getResourceInterface().callTest(payload);
                break;
            }

        } catch(Exception e) {
            TCKSbbUtils.handleException(e);
        }
    }

    public void sbbRolledBack(RolledBackContext context) {
        //send an ACK to the TCK test that the previous transaction (whatever that was) was successfully rolled back
        sendResult(true, 1110132, "Transaction rolled back.", context.getActivityContextInterface());
    }

    public abstract void fireTCKSbbEvent(TCKSbbEvent event, ActivityContextInterface aci, Address address);
}
