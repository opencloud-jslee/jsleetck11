/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.javax.slee.ChildRelation;

import javax.slee.ActivityEndEvent;
import javax.slee.ActivityContextInterface;
import javax.slee.Address;
import javax.slee.SbbLocalObject;
import javax.slee.facilities.Level;
import javax.slee.ChildRelation;

import com.opencloud.sleetck.lib.sbbutils.BaseTCKSbb;
import com.opencloud.sleetck.lib.sbbutils.TCKSbbUtils;
import com.opencloud.sleetck.lib.sbbutils.TCKSbbConstants;
import com.opencloud.sleetck.lib.resource.events.TCKResourceEventX;
import com.opencloud.sleetck.lib.resource.TCKActivityID;
import com.opencloud.sleetck.lib.resource.sbbapi.TCKActivity;
import com.opencloud.sleetck.lib.resource.sbbapi.TCKResourceSbbInterface;
import com.opencloud.sleetck.lib.resource.sbbapi.TCKActivityContextInterfaceFactory;

import java.util.HashMap;
import java.util.Iterator;

public abstract class Test3044Sbb extends BaseTCKSbb {

    public void onTCKResourceEventX1(TCKResourceEventX event, ActivityContextInterface aci) {

        try {
            HashMap map = new HashMap();

            SbbLocalObject firstChild = getChildRelation().create();
            SbbLocalObject secondChild = getChildRelation().create();

            Iterator iter = getChildRelation().iterator();
            int count = 0;
            while (iter.hasNext()) {
                iter.next();
                count++;
            }

            if (count == 2) {
                if (getChildRelation().size() == 2) { // Assertion 3050
                    if (getChildRelation().contains(firstChild) && getChildRelation().contains(secondChild)) { // Assertion 3056
                        map.put("Result", new Boolean(true));
                        map.put("Message", "Ok");
                    } else {
                        map.put("Result", new Boolean(false));
                        map.put("Message", "ChildRelation.contains(SbbLocalObject) returned false for child in that relation.");
                    }                        
                } else {
                    map.put("Result", new Boolean(false));
                    map.put("Message", "ChildRelation.size() returned incorrect size.");
                }
            } else {
                map.put("Result", new Boolean(false));
                map.put("Message", "ChildRelation.iterator() had an incorrect number of SbbLocalObjects in it.");
            }
            TCKSbbUtils.getResourceInterface().sendSbbMessage(map);
            return;

        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }

    }

    public abstract ChildRelation getChildRelation();

}
