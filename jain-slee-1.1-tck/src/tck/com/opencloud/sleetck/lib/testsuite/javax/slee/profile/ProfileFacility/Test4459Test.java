/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.javax.slee.profile.ProfileFacility;

import javax.slee.management.*;
import javax.slee.ComponentID;
import javax.slee.ServiceID;
import javax.slee.profile.*;

import com.opencloud.sleetck.lib.*;
import com.opencloud.sleetck.lib.testutils.*;
import com.opencloud.sleetck.lib.resource.TCKSbbMessage;
import com.opencloud.sleetck.lib.resource.testapi.TCKResourceListener;
import com.opencloud.sleetck.lib.resource.TCKActivityID;
import com.opencloud.sleetck.lib.resource.testapi.TCKResourceTestInterface;
import com.opencloud.sleetck.lib.resource.events.TCKResourceEventX;
import com.opencloud.sleetck.lib.resource.events.TCKResourceEventY;

import com.opencloud.sleetck.lib.testutils.jmx.DeploymentMBeanProxy;
import com.opencloud.sleetck.lib.testutils.jmx.ServiceManagementMBeanProxy;
import com.opencloud.sleetck.lib.testutils.jmx.ProfileProvisioningMBeanProxy;
import com.opencloud.sleetck.lib.testutils.jmx.ProfileMBeanProxy;

import com.opencloud.logging.Logable;

import java.rmi.RemoteException;
import javax.management.Attribute;
import javax.management.ObjectName;
import java.util.HashMap;

public class Test4459Test implements SleeTCKTest {

    private static final String SERVICE_DU_PATH_PARAM = "DUPath";
    private static final int TEST_ID = 4459;

    public void init(SleeTCKTestUtils utils) {
        this.utils = utils;
    }

    /**
     * Perform the actual test.
     */

    public TCKTestResult run() throws Exception {
        result = new FutureResult(utils.getLog());

        TCKResourceTestInterface resource = utils.getResourceInterface();
        TCKActivityID activityID = resource.createActivity("Test4459InitialActivity");
        resource.fireEvent(TCKResourceEventX.X1, TCKResourceEventX.X1, activityID, null);

        return result.waitForResultOrFail(utils.getTestTimeout(), "Timeout waiting for test result.", TEST_ID);
    }

    /**
     * Do all the pre-run configuration of the test.
     */

    public void setUp() throws Exception {

        utils.getLog().fine("Connecting to resource");
        resourceListener = new TCKResourceListenerImpl();
        utils.getResourceInterface().setResourceListener(resourceListener);
        utils.getLog().fine("Installing and activating service");

        // Install the Deployable Units
        String duPath = utils.getTestParams().getProperty(SERVICE_DU_PATH_PARAM);
        duID = utils.install(duPath);

        // Activate the DU
        utils.activateServices(duID, true);

        // Create a Profile Table, and some Profiles.
        ProfileProvisioningMBeanProxy profileProxy = new ProfileUtils(utils).getProfileProvisioningProxy();
        DeploymentMBeanProxy duProxy = utils.getDeploymentMBeanProxy();
        DeployableUnitDescriptor duDesc = duProxy.getDescriptor(duID);
        ComponentID components[] = duDesc.getComponents();


        for (int i = 0; i < components.length; i++) {
            if (components[i] instanceof ProfileSpecificationID) {
                ProfileSpecificationID profileSpecID = (ProfileSpecificationID) components[i];

                profileProxy.createProfileTable(profileSpecID, Test4459Sbb.PROFILE_TABLE_NAME);

                ObjectName profile = profileProxy.createProfile(Test4459Sbb.PROFILE_TABLE_NAME, Test4459Sbb.PROFILE_NAME);
                ProfileMBeanProxy profileMBeanProxy = utils.getMBeanProxyFactory().createProfileMBeanProxy(profile);

                // Modify the created profile so that it's different to the default profile.
                try {
                    utils.getMBeanFacade().setAttribute(profile, new Attribute("IntVal", new Integer(42)));
                } finally {
                    if (profileMBeanProxy.isProfileWriteable())
                        profileMBeanProxy.commitProfile();
                    profileMBeanProxy.closeProfile();
                }

                break;
            }
        }
    }

    /**
     * Clean up after the test.
     */

    public void tearDown() throws Exception {
        ProfileProvisioningMBeanProxy profileProxy = new ProfileUtils(utils).getProfileProvisioningProxy();
        try {
            profileProxy.removeProfile(Test4459Sbb.PROFILE_TABLE_NAME, Test4459Sbb.PROFILE_NAME);
        } catch (Exception e) {
        }
        try {
            profileProxy.removeProfileTable(Test4459Sbb.PROFILE_TABLE_NAME);
        } catch (Exception e) {
        }

        utils.getLog().fine("Disconnecting from resource");
        utils.getResourceInterface().clearActivities();
        utils.getResourceInterface().removeResourceListener();
        utils.getLog().fine("Deactivating and uninstalling service");
        utils.deactivateAllServices();
        utils.uninstallAll();
    }

    private class TCKResourceListenerImpl extends BaseTCKResourceListener {
        public synchronized void onSbbMessage(TCKSbbMessage message, TCKActivityID calledActivity) throws RemoteException {

            HashMap map = (HashMap) message.getMessage();
            String type = (String) map.get("Type");
            if (type.equals("ProfileID")) {
                ProfileID profileID = (ProfileID) map.get("ProfileID");
                if (profileID.getProfileTableName().equals(Test4459Sbb.PROFILE_TABLE_NAME)
                    && profileID.getProfileName().equals(Test4459Sbb.PROFILE_NAME))
                    result.setPassed();
                else
                    result.setFailed(TEST_ID, "Profile Table's default table returned from ProfileFacility.getProfileByIndexedAttribute.");
            }
        }

        public void onException(Exception e) throws RemoteException {
            utils.getLog().warning("Received exception from SBB");
            utils.getLog().warning(e);
            result.setError(e);
        }
    }

    private SleeTCKTestUtils utils;
    private TCKResourceListener resourceListener;
    private FutureResult result;
    private DeployableUnitID duID;
}
