/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.profiles;

import com.opencloud.sleetck.lib.AbstractSleeTCKTest;
import com.opencloud.sleetck.lib.TCKTestResult;
import com.opencloud.sleetck.lib.infra.SleeTCKComponentConstants;
import com.opencloud.sleetck.lib.testsuite.profiles.ProfileTestConstants;
import com.opencloud.sleetck.lib.testutils.Assert;
import com.opencloud.sleetck.lib.testutils.ComponentIDLookup;
import com.opencloud.sleetck.lib.testutils.ProfileUtils;
import com.opencloud.sleetck.lib.testutils.jmx.ProfileProvisioningMBeanProxy;
import com.opencloud.sleetck.lib.testutils.jmx.ProfileMBeanProxy;

import javax.slee.management.ManagementException;
import javax.slee.profile.ProfileSpecificationID;
import javax.slee.profile.ProfileVerificationException;
import javax.management.ObjectName;

/**
 * Test assertion UniqueAttribute: that when a profile table is creatted the SLEE calls profileInitialize, profileStore
 * and profileVerify in the same transaction.
 */
public class TestUniqueAttributeTest extends AbstractSleeTCKTest {
    private static final String TABLE_NAME = "tck.TestUniqueAttributeTest.table";
    private static final String PROFILE_SPEC_NAME = "TestUniqueAttributeProfile";

    private static final int TEST_ID = 9999;

    public TCKTestResult run() throws Exception {
        // create the profile table
        ProfileProvisioningMBeanProxy profileProvisioning = profileUtils.getProfileProvisioningProxy();
        ProfileSpecificationID profileSpecID = new ComponentIDLookup(utils()).lookupProfileSpecificationID(
            PROFILE_SPEC_NAME,SleeTCKComponentConstants.TCK_VENDOR,"1.0");

        profileProvisioning.createProfileTable(profileSpecID, TABLE_NAME);

        // Create profile, commit  - should succeed.
        // Create profile, commit  - should fail.

        ObjectName profiles[] = new ObjectName[2];

        // Now starts the test proper.
        try {
            profiles[0] = profileProvisioning.createProfile(TABLE_NAME, "Foo");
            // Commit the first profile
            ProfileMBeanProxy proxy = utils().getMBeanProxyFactory().createProfileMBeanProxy(profiles[0]);
            proxy.commitProfile();
        } catch (Exception e) {
            return TCKTestResult.failed(TEST_ID, "Failed to create profile with uniquely indexed attribute - perhaps the SLEE erroneously included the default profile when deciding whether or not a profile with this attribute value could be legally created.");
        }
        
        try {
            profiles[1] = profileProvisioning.createProfile(TABLE_NAME, "Bar");
            ProfileMBeanProxy proxy = utils().getMBeanProxyFactory().createProfileMBeanProxy(profiles[1]);
            proxy.commitProfile();
        }
        catch (ProfileVerificationException pve) {
            return TCKTestResult.passed();
        }
        catch (Exception e) {
            return TCKTestResult.error(e);
        }

        return TCKTestResult.failed(TEST_ID, "Was able to create two profiles with a uniquely indexed attribute that had its value set in profileInitialize().");
    }

    public void setUp() throws Exception {
        String duPath = utils().getTestParams().getProperty(ProfileTestConstants.PROFILE_SPEC_DU_PATH_PARAM);
        getLog().fine("Installing profile specification");
        utils().install(duPath);
        profileUtils = new ProfileUtils(utils());
    }

    public void tearDown() throws Exception {
        if (profileUtils != null) {
            ProfileProvisioningMBeanProxy profileProvisioning = profileUtils.getProfileProvisioningProxy();
            
            try {
                profileProvisioning.removeProfile(TABLE_NAME, "Bar");
            } catch (Exception e) {
            }
            
            try {
                profileProvisioning.removeProfile(TABLE_NAME, "Foo");
            } catch (Exception e) {
            }
            

            // delete profile table
            try {
                getLog().fine("Removing profile table");
                profileUtils.removeProfileTable(TABLE_NAME);
            } catch(Exception e) {
                getLog().warning("Caught exception while trying to remove profile table:");
                getLog().warning(e);
            }
        }
        profileUtils = null;
        super.tearDown();
    }

    private ProfileUtils profileUtils;
}























