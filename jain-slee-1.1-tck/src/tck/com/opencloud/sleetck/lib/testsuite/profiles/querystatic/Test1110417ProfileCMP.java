/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.profiles.querystatic;

import java.util.Hashtable;

public interface Test1110417ProfileCMP {
    public String getStringValue();
    public void setStringValue(String value);

    public int getIntValue();
    public void setIntValue(int value);

    public MyInteger getMyIntValue();
    public void setMyIntValue(MyInteger value);

    public Hashtable getMapValue();
    public void setMapValue(Hashtable value);
}
