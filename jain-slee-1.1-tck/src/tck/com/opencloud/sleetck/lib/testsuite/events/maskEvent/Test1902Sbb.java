/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.events.maskEvent;

import javax.slee.ActivityEndEvent;
import javax.slee.ActivityContextInterface;
import javax.slee.SbbContext;
import javax.slee.Address;
import javax.slee.SbbLocalObject;
import javax.slee.facilities.Level;
import javax.slee.ChildRelation;

import javax.slee.nullactivity.NullActivityFactory;
import javax.slee.nullactivity.NullActivity;
import javax.slee.nullactivity.NullActivityContextInterfaceFactory;
import javax.slee.facilities.ActivityContextNamingFacility;

import com.opencloud.sleetck.lib.sbbutils.BaseTCKSbb;
import com.opencloud.sleetck.lib.sbbutils.TCKSbbUtils;
import com.opencloud.sleetck.lib.sbbutils.TCKSbbConstants;
import com.opencloud.sleetck.lib.resource.events.TCKResourceEventX;
import com.opencloud.sleetck.lib.resource.TCKActivityID;
import com.opencloud.sleetck.lib.resource.sbbapi.TCKActivity;
import com.opencloud.sleetck.lib.resource.sbbapi.TCKResourceSbbInterface;
import com.opencloud.sleetck.lib.resource.sbbapi.TCKActivityContextInterfaceFactory;

import java.util.HashMap;
import java.util.Iterator;

public abstract class Test1902Sbb extends BaseTCKSbb {

    // Initial event method.
    public void onTCKResourceEventX1(TCKResourceEventX ev, ActivityContextInterface aci) {

        try {
            HashMap map = new HashMap();

            // Valid set of names.
            try {
                getSbbContext().maskEvent( new String [] { "Test1902Event", "TCKResourceEventX2" }, aci);
            } catch (javax.slee.UnrecognizedEventException e) {
                map.put("Result", new Boolean(false));
                map.put("Message", "SbbContext.maskEvent() failed when given valid set of event names.");
                TCKSbbUtils.getResourceInterface().sendSbbMessage(map);
                return;
            }

            boolean passed = false;
            try {            
                // Set of invalid (gobbledigook) names.
                getSbbContext().maskEvent( new String [] { "FredEvent" }, aci);
            } catch (javax.slee.UnrecognizedEventException e) {
                passed = true;
            }

            // If the exception wasn't thrown...
            if (passed == false) {
                map.put("Result", new Boolean(false));
                map.put("Message", "SbbContext.maskEvent() didn't throw exception when given a non-existent event.");
                TCKSbbUtils.getResourceInterface().sendSbbMessage(map);
                return;
            }
        
            // Reset the pass/fail marker.
            passed = false;
        
            try {            
                // Otherwise valid set containing event with direction fire (invalid)
                getSbbContext().maskEvent( new String [] { "Test1902SecondEvent" }, aci);
            } catch (javax.slee.UnrecognizedEventException e) {
                passed = true;
            }
        
            // If the exception wasn't thrown...
            if (passed == false) {
                map.put("Result", new Boolean(false));
                map.put("Message", "SbbContext.maskEvent() didn't throw exception when given an event with direction of fire.");
                TCKSbbUtils.getResourceInterface().sendSbbMessage(map);
                return;
            }

            // Mark the test as passed.
            map.put("Result", new Boolean(true));
            map.put("Message", "Ok");
            TCKSbbUtils.getResourceInterface().sendSbbMessage(map);

        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }


    }

    // Fire and receive event.
    public abstract void fireTest1902Event(Test1902Event event, ActivityContextInterface aci, Address address);
    public void onTest1902Event(Test1902Event event, ActivityContextInterface aci) {
        try {
            HashMap map = new HashMap();
            map.put("Result", new Boolean(false));
            map.put("Message", "SBB was able to fire event when it was not in the Ready state.");
            TCKSbbUtils.getResourceInterface().sendSbbMessage(map);
        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }
    }

    // Receive-only event.
    public void onTCKResourceEventX2(TCKResourceEventX event, ActivityContextInterface aci) {
    }

    // Fire-only event.
    public abstract void fireTest1902SecondEvent(Test1902SecondEvent event, ActivityContextInterface aci, Address address);

}
