/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.events.eventrouting;

import java.rmi.RemoteException;

import com.opencloud.sleetck.lib.AbstractSleeTCKTest;
import com.opencloud.sleetck.lib.resource.TCKActivityID;
import com.opencloud.sleetck.lib.resource.TCKSbbMessage;
import com.opencloud.sleetck.lib.resource.events.TCKResourceEventX;
import com.opencloud.sleetck.lib.resource.events.TCKResourceEventY;
import com.opencloud.sleetck.lib.resource.testapi.TCKResourceListener;
import com.opencloud.sleetck.lib.resource.testapi.TCKResourceTestInterface;
import com.opencloud.sleetck.lib.testutils.BaseTCKResourceListener;
import com.opencloud.sleetck.lib.testutils.FutureResult;

/**
 * AssertionID(1108096): Test An SBB can modify the event delivery priority 
 * of a root SBB entity at runtime by invoking the setSbbPriority method on 
 * an SBB local object that represents the root SBB entity.
 *
 */
public class Test1108096Test extends AbstractSleeTCKTest {

    public static final String SERVICE1_DU_PATH_PARAM = "service1DUPath";

    public static final String SERVICE2_DU_PATH_PARAM = "service2DUPath";

    /**
     * Perform the actual test.
     */

    public void run(FutureResult result) throws Exception {
        this.result = result;

        TCKResourceTestInterface resource = utils().getResourceInterface();
        TCKActivityID activityID = resource.createActivity("Test1108096InitialActivity");

        getLog().fine("Firing TCKResourceEventX1 on Test1108096InitialActivity");
        resource.fireEvent(TCKResourceEventX.X1, testName, activityID, null);
        synchronized (this) {
            wait(1000);
        }

        getLog().fine("Firing TCKResourceEventY1 on Test1108096InitialActivity");
        resource.fireEvent(TCKResourceEventY.Y1, testName, activityID, null);
        synchronized (this) {
            wait(1000);
        }

        getLog().fine("Firing TCKResourceEventX2 on Test1108096InitialActivity");
        resource.fireEvent(TCKResourceEventX.X2, testName, activityID, null);

        synchronized (this) {
            wait(1000);
        }

        getLog().fine("Firing TCKResourceEventY2 on Test1108096InitialActivity");
        resource.fireEvent(TCKResourceEventY.Y2, testName, activityID, null);

        synchronized (this) {
            wait(1000);
        }

        getLog().fine("Firing TCKResourceEventX3 on Test1108096InitialActivity");
        resource.fireEvent(TCKResourceEventX.X3, testName, activityID, null);

        synchronized (this) {
            wait(1000);
        }

        getLog().fine("Firing TCKResourceEventY3 on Test1108096InitialActivity");
        resource.fireEvent(TCKResourceEventY.Y3, testName, activityID, null);

    }

    /**
     * Do all the pre-run configuration of the test.
     */

    public void setUp() throws Exception {
        getLog().fine("Connecting to resource");
        resourceListener = new TCKResourceListenerImpl();
        setResourceListener(resourceListener);
        getLog().fine("Installing and activating service");

        setupService(SERVICE1_DU_PATH_PARAM, true);
        setupService(SERVICE2_DU_PATH_PARAM, true);
    }

    /**
     * Clean up after the test.
     */

    public void tearDown() throws Exception {
        // cleanup
        super.tearDown();
    }

    private class TCKResourceListenerImpl extends BaseTCKResourceListener {
        public synchronized void onSbbMessage(TCKSbbMessage message, TCKActivityID calledActivity)
                throws RemoteException {
            int seq = ((Integer) message.getMessage()).intValue();
            getLog().info("Received message from SBB:" + seq);

            switch (seq) {
            case 1:
            case 2:
            case 3:
            case 4:
                // Messages 1 to 4 should be received in order.
                // Guaranteed by sbb priority settings.
                if (seq != seqCount) {
                    result.setFailed(1108096,"The received message is not the same as the expected message.");
                    return;
                }
                break;
            case 5:
            case 6:
                // Messages 5 and 6 can be received in any order since the sbb priorities
                // are both set to 0 at this point.
                if (seqCount == MAX_SEQ) {
                    result.setPassed();
                    return;
                }
                break;
            default:
                result.setFailed(1108096,"Received an unexpected message.");
                return;
            }
            
            seqCount++;
        }

        public void onException(Exception e) throws RemoteException {
            getLog().warning("Received exception from SBB");
            getLog().warning(e);
            result.setError(e);
        }
    }

    private TCKResourceListener resourceListener;

    private FutureResult result;

    private final static String testName = "Test1108096";

    private static final int MAX_SEQ = 6;
    
    private int seqCount = 1;
}