/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.profiles.profileabstractclass;

import java.util.HashMap;

import javax.naming.Context;
import javax.slee.ActivityContextInterface;
import javax.slee.RolledBackContext;
import javax.slee.profile.ProfileFacility;
import javax.slee.profile.ProfileTable;

import com.opencloud.sleetck.lib.resource.events.TCKResourceEventX;
import com.opencloud.sleetck.lib.sbbutils.BaseTCKSbb;
import com.opencloud.sleetck.lib.sbbutils.TCKSbbUtils;
import com.opencloud.sleetck.lib.sbbutils.events2.SbbBaseMessageComposer;

public abstract class Test1110251Sbb extends BaseTCKSbb {

    public static final String PROFILE_TABLE_NAME = "Test1110251ProfileTable";
    public static final String PROFILE_NAME = "Test1110251Profile";
    public static final String RAISE_EXCEPTION_PROFILE = "ExceptionProfile";
    public static final String ROLLBACK_REMOVE_PROFILE = "RemoveProfile";
    public static final String ROLLBACK_STORE_PROFILE = "StoreProfile";
    public static final String VERIFY_EXCEPTION_PROFILE = "VerifyExceptionProfile";

    public static final int POSTCREATE_ROLLBACK = 1;
    public static final int POSTCREATE_EXCEPTION = 2;
    public static final int REMOVE_ROLLBACK = 3;

    private void sendLogMsgCall (String msg) {
        HashMap map = SbbBaseMessageComposer.getLogMsg(msg);
        try {
            TCKSbbUtils.getResourceInterface().callTest(map);
        }
        catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }
    }

    private void sendResult(boolean result, int id, String msg) {
        HashMap map = SbbBaseMessageComposer.getSetResultMsg(result, id, msg);
        try {
            TCKSbbUtils.getResourceInterface().sendSbbMessage(map);
        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }
    }


    public void onTCKResourceEventX1(TCKResourceEventX event, ActivityContextInterface aci) {
        try {
            Context env = TCKSbbUtils.getSbbEnvironment();
            ProfileFacility profileFacility = (ProfileFacility)env.lookup("slee/facilities/profile");
            ProfileTable profileTable = profileFacility.getProfileTable(PROFILE_TABLE_NAME);
            sendLogMsgCall("Obtained ProfileTable from ProfileFacility.");


            Test1110251ProfileLocal profileLocal = null;

            int operationID = ((Integer)event.getMessage()).intValue();
            switch(operationID) {
            case POSTCREATE_ROLLBACK:
                //1110251: the profilePostCreate method should be invoked in the same TXN context as
                //this handler method, so even if profilePostCreate marks TXN context as invalid it
                //should be completing the create method without exception...
                profileLocal = (Test1110251ProfileLocal)profileTable.create(PROFILE_NAME);

                //...the current TXN context should be marked for rollback...
                if (getSbbContext().getRollbackOnly())
                    sendLogMsgCall("TXN context has been marked for rollback as expected.");
                else {
                    sendResult(false, 1110251, "Sbb event handler TXN context should have been marked for rollback after profileTable.create() has been called.");
                    return;
                }

                //...the change to the CMP field done in profilePostCreate() should be visible here...
                if (!"42".equals(profileLocal.getValue())) {
                    sendResult(false, 1110251, "Modified CMP field should have been visible to Sbb event handler method.");
                    return;
                }
                else
                    sendLogMsgCall("Modified CMP field was visible to Sbb event handler method as expected.");

                //1110252: The Profile object enters the Ready state after profilePostCreate returns normally (i.e. without throwing an exception)
                //It's in the Ready state that business methods can be invoked
                try {
                    profileLocal.business2();
                    sendLogMsgCall("Profile object must have correctly entered the Ready state after " +
                            "profilePostCreate returned normally as business methods could be invoked without problems.");
                } catch (Exception e) {
                    sendResult(false, 1110252, "ProfileLocal object should have executed a business method as it should have been in the" +
                            "Ready state after profilePostCreate returned normally.");
                    return;
                }

                //1110134: The ProfileLocalObject returned by the create() method allows an SBB Developer or
                //Resource Adaptor Developer to interact with the Profile within the same transaction
                //context as it was created.
                try {
                    if (profileLocal.business())
                        sendLogMsgCall("ProfileLocal object correctly allowed interaction within same TXN context as profile was created.");
                    else {
                        sendResult(false, 1110134, "ProfileLocal object should have executed a business method with same TXN context as profile was created.");
                        return;
                    }
                } catch (Exception e) {
                    sendResult(false, 1110134, "Exception occurred when trying to interact with profile in the same TXN context as it was created."+e);
                    return;
                }


                sendResult(true, 1110251, "POSTCREATE_ROLLBACK completed.");
                //allow the TXN to rollback, thus sbbRolledBack() is called by the SLEE
                break;

            case REMOVE_ROLLBACK:
                //1110266: The profileRemove callback should be invoked in the same TXN context as
                //this handler method, so even if profileRemove marks TXN context as invalid it
                //should be completing the remove method without exception...
                profileLocal = (Test1110251ProfileLocal)profileTable.find(ROLLBACK_REMOVE_PROFILE);
                profileTable.remove(ROLLBACK_REMOVE_PROFILE);

                //...the current TXN context should be marked for rollback...
                if (getSbbContext().getRollbackOnly())
                    sendLogMsgCall("TXN context has been marked for rollback as expected.");
                else {
                    sendResult(false,1110266, "Sbb event handler TXN context should have been marked for rollback after profileTable.remove() was called.");
                    return;
                }

                //1110263:...calling a business method should fail as object is not in the READY state
                try {
                    profileLocal.business();
                    sendResult(false, 1110263, "Profile object is supposed to be in POOLED state after 'profileRemove' callback has returned.");
                    return;
                }
                catch (Exception e) {
                    sendLogMsgCall("Calling business method failed as expected as profile object is not in READY state after 'profileRemove' has returned.");
                }

                sendResult(true, 1110251, "REMOVE_ROLLBACK completed.");
                break;
            }


        } catch(Exception e) {
            TCKSbbUtils.handleException(e);
        }
    }

    public void sbbRolledBack(RolledBackContext context) {
        //override behaviour from parent class which reports rollbacks as errors/failures to the TCK Test
    }

}
