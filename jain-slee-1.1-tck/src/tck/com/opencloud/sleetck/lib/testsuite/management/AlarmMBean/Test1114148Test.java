/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.management.AlarmMBean;

import javax.management.Notification;
import javax.management.NotificationListener;
import javax.management.ObjectName;
import javax.slee.ComponentID;
import javax.slee.SbbID;
import javax.slee.ServiceID;
import javax.slee.facilities.AlarmLevel;
import javax.slee.management.Alarm;
import javax.slee.management.AlarmNotification;
import javax.slee.management.DeployableUnitDescriptor;
import javax.slee.management.DeployableUnitID;
import javax.slee.management.NotificationSource;
import javax.slee.management.SbbNotification;

import com.opencloud.sleetck.lib.AbstractSleeTCKTest;
import com.opencloud.sleetck.lib.DescriptionKeys;
import com.opencloud.sleetck.lib.SleeTCKTestUtils;
import com.opencloud.sleetck.lib.TCKTestErrorException;
import com.opencloud.sleetck.lib.TCKTestResult;
import com.opencloud.sleetck.lib.resource.TCKActivityID;
import com.opencloud.sleetck.lib.resource.events.TCKResourceEventX;
import com.opencloud.sleetck.lib.resource.testapi.TCKResourceListener;
import com.opencloud.sleetck.lib.resource.testapi.TCKResourceTestInterface;
import com.opencloud.sleetck.lib.testutils.jmx.AlarmMBeanProxy;
import com.opencloud.sleetck.lib.testutils.jmx.DeploymentMBeanProxy;
import com.opencloud.sleetck.lib.testutils.jmx.SleeManagementMBeanProxy;
import com.opencloud.sleetck.lib.testutils.jmx.impl.AlarmMBeanProxyImpl;

/*
 * Test methods and exceptions in the AlarmMBean interface together with the alarm constructor
 *
 */

public class Test1114148Test extends AbstractSleeTCKTest {

    /**
     * Perform the actual test.
     */
    public TCKTestResult run() throws Exception {

        if(sbbID == null) throw new TCKTestErrorException("sbbID not found for "+DescriptionKeys.SERVICE_DU_PATH_PARAM);
        if(serviceID == null) throw new TCKTestErrorException("serviceID not found for "+DescriptionKeys.SERVICE_DU_PATH_PARAM);

        String activityName = "Test1114148Test";
        TCKResourceTestInterface resource = utils().getResourceInterface();
        TCKActivityID activityID = resource.createActivity(activityName);

        // kickoff the test
        getLog().info("Firing event: " + TCKResourceEventX.X1 );
        resource.fireEvent(TCKResourceEventX.X1, TCKResourceEventX.X1, activityID, null);

        synchronized (this) {
            wait(utils().getTestTimeout());
        }

        return result;
    }


    /**
     * Do all the pre-run configuration of the test.
     */

    public void setUp() throws Exception {

        getLog().fine("Installing and activating service");

        // Install the Deployable Units
        String duPath = utils().getTestParams().getProperty(DescriptionKeys.SERVICE_DU_PATH_PARAM);
        duID = utils().install(duPath);

        // Activate the DU
        utils().activateServices(duID, true);

        SleeManagementMBeanProxy proxy = utils().getSleeManagementMBeanProxy();
        alarmMBean = proxy.getAlarmMBean();
        alarmMBeanName = alarmMBean.toString();
        getLog().info("1114148: Object Name of an AlarmMBean is: " +alarmMBeanName);
        if (alarmMBeanName.equals("javax.slee.management:name=Alarm"))
            logSuccessfulCheck(1114148);

        listener = new AlarmNotificationListenerImpl();
        alarmMBeanProxy = new AlarmMBeanProxyImpl(alarmMBean, utils().getMBeanFacade());
        alarmMBeanProxy.addNotificationListener(listener, null, null);

        DeploymentMBeanProxy duProxy = utils().getDeploymentMBeanProxy();
        DeployableUnitDescriptor duDesc = duProxy.getDescriptor(duID);
        ComponentID components[] = duDesc.getComponents();
        // Get the SbbID from the components.
        for (int i = 0; i < components.length; i++) {
            if (components[i] instanceof ServiceID) {
                getLog().fine("Setting serviceID value.");
                serviceID = (ServiceID) components[i];
                continue;
            }

            if (components[i] instanceof SbbID) {
                getLog().fine("Setting sbbID value.");
                sbbID = (SbbID) components[i];
                continue;
            }
        }

    }

    /**
     * Clean up after the test.
     */

    public void tearDown() throws Exception {
        String[] listActiveAlarms = alarmMBeanProxy.getAlarms();
        if (alarmMBeanProxy.getAlarms().length > 0)
            for (int i = 0; i < listActiveAlarms.length; i++)
                alarmMBeanProxy.clearAlarm(listActiveAlarms[i]);

        if (null != alarmMBeanProxy)
            alarmMBeanProxy.removeNotificationListener(listener);
        // cleanup
        super.tearDown();
    }


    public class AlarmNotificationListenerImpl implements NotificationListener {
        public final void handleNotification(Notification notification, Object handback) {

        if (notification instanceof AlarmNotification) {
            getLog().fine("Received Alarm Notification: " +notification);
            result =  doAlarmMBeanCheck(notification);
        }

        return;
        }
    }

    private TCKTestResult doAlarmMBeanCheck(Notification notification) {

        if (notification instanceof AlarmNotification) {

            try {
                AlarmNotification alarmNotification = (AlarmNotification) notification;
                //Create an Alarm object that contains information about an alarm that has
                //been raised in the SLEE.
                String alarmID = alarmNotification.getAlarmID();
                //NotificationSource notificationSource = alarmNotification.getNotificationSource();
                SbbNotification notificationSource = (SbbNotification)alarmNotification.getNotificationSource();
                String alarmType = alarmNotification.getAlarmType();
                String instanceID = alarmNotification.getInstanceID();
                AlarmLevel alarmLevel = alarmNotification.getAlarmLevel();
                String message = alarmNotification.getMessage();
                Throwable cause = alarmNotification.getCause();
                long timestamp = alarmNotification.getTimeStamp();

                getLog().fine("Creating Alarm from notification...");
                Alarm alarm = new Alarm(alarmID, notificationSource, alarmType, instanceID, alarmLevel, message, cause, timestamp);

                if (alarm == null) {
                    return TCKTestResult.failed(1114166, "The Alarm object returned null");
                } else {
                    logSuccessfulCheck(1114166);
                }

                if (alarm.getAlarmID() == alarmID) {
                    logSuccessfulCheck(1114418);
                } else {
                    return TCKTestResult.failed(1114418, "Alarm.getAlarmID() returned incorrect ID.");
                }

                NotificationSource newAlarmNotification = alarm.getNotificationSource();
                SbbNotification newNotificationSource = (SbbNotification)newAlarmNotification;
                if (newNotificationSource.equals(notificationSource)) {
                    logSuccessfulCheck(1114419);
                } else {
                    getLog().warning("Error in getNotificationSource: obtained " +newAlarmNotification);
                    return TCKTestResult.failed(1114419, "Alarm.getNotificationSource() returned incorrect notification source.");
                }

                if (alarm.getAlarmType() == alarmType) {
                    logSuccessfulCheck(1114420);
                } else {
                    return TCKTestResult.failed(1114420, "Alarm.getAlarmType() returned incorrect Alarm Type.");
                }

                if (alarm.getInstanceID() == instanceID) {
                    logSuccessfulCheck(1114421);
                } else {
                    return TCKTestResult.failed(1114421, "Alarm.getInstanceID() returned incorrect Instance ID.");
                }

                if (alarm.getAlarmLevel() == alarmLevel) {
                    logSuccessfulCheck(1114167);
                } else {
                    return TCKTestResult.failed(1114167, "Alarm.getAlarmLevel() returned incorrect Alarm Level.");
                }

                if (alarm.getMessage() == message) {
                    logSuccessfulCheck(1114170);
                } else {
                    return TCKTestResult.failed(1114170, "Alarm.getMessage() returned incorrect message.");
                }

                if (alarm.getCause() == cause) {
                    logSuccessfulCheck(1114169);
                } else {
                    return TCKTestResult.failed(1114169, "Alarm.getCause() returned incorrect cause.");
                }

                if (alarm.getTimestamp() == timestamp) {
                    logSuccessfulCheck(1114175);
                } else {
                    return TCKTestResult.failed(1114175, "Alarm.getTimestamp() returned incorrect timestamp.");
                }

                // Create an equal alarm
                Alarm alarm2 = alarm;

                // Create an non-equal alarm
                String ALARM_MESSAGE = "Test1114148DummyAlarmMessage";
                String ALARM_INSTANCEID = "Test1114148DummylarmInstanceID";
                String dummyalarmID = "Test1114148AlarmId";
                String dummyalarmType = "TestAlarm";
                String dummyinstanceID = "Test1114148AlarmInstanceID";
                AlarmLevel dummyalarmLevel = javax.slee.facilities.AlarmLevel.MAJOR;
                String dummymessage = "Test1114148Alarm";
                Throwable dummycause = null;
                long dummytimestamp = 1042725;

                Alarm dummyAlarm = new Alarm(dummyalarmID, notificationSource, dummyalarmType, dummyinstanceID, dummyalarmLevel, dummymessage, dummycause, dummytimestamp);
                String dummyAlarmID = dummyAlarm.getAlarmID();
                getLog().fine("Created Dummy Alarm: " +dummyAlarmID);
                if (alarm.equals(alarm2)) {
                    if (!alarm.equals(dummyAlarm))
                        logSuccessfulCheck(1114174);
                    else
                        return TCKTestResult.failed(1114174, "Alarm.equals() returned incorrect comparisonon non-equals Alarm");
                } else {
                    return TCKTestResult.failed(1114174, "Alarm.equals() returned incorrect comparison on equals Alarm.");
                }

                try {
                    int hashCode = alarm.hashCode();
                    logSuccessfulCheck(1114168);
                } catch (Exception e) {
                    getLog().warning(e);
                    return TCKTestResult.failed(1114168, "hashCode() raised exception: ");
                }

                try {
                    String alarmString = alarm.toString();
                    logSuccessfulCheck(1114171);
                } catch (Exception e) {
                    getLog().warning(e);
                    return TCKTestResult.failed(1114171, "hashCode() raised exception: ");
                }

                return TCKTestResult.passed();

            } catch (Exception e) {
                getLog().warning(e);
                return TCKTestResult.error("ERROR creating Alarm", e);
            }

        }else {
            return TCKTestResult.error("ERROR creating Alarm");
        }

    }

    private void logSuccessfulCheck(int assertionID) {
        utils().getLog().info("Check for assertion "+assertionID+" OK");
    }

    private SleeTCKTestUtils utils;
    private TCKResourceListener resourceListener;
    private NotificationListener listener;
    private DeployableUnitID duID;
    private AlarmMBeanProxy alarmMBeanProxy;
    private ObjectName alarmMBean;
    private String alarmMBeanName;
    private TCKTestResult result;
    private SbbID sbbID;
    private ServiceID serviceID;
    private SbbNotification sbbNotification;

}
