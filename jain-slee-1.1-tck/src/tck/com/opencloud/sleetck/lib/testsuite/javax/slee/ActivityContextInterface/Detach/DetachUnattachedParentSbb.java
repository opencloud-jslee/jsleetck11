/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.javax.slee.ActivityContextInterface.Detach;

import com.opencloud.sleetck.lib.sbbutils.BaseTCKSbb;
import com.opencloud.sleetck.lib.sbbutils.TCKSbbUtils;
import com.opencloud.sleetck.lib.resource.events.TCKResourceEventX;

import javax.slee.ActivityContextInterface;
import javax.slee.facilities.Level;
import javax.slee.Sbb;
import javax.slee.SbbLocalObject;
import javax.slee.ChildRelation;

import java.util.HashMap;
import java.util.Iterator;

/**
 * Tries to detach an unattached child from an ACI.
 */

public abstract class DetachUnattachedParentSbb extends BaseTCKSbb {

    public abstract ChildRelation getChildRelation();
 
    public void onTCKResourceEventX1(TCKResourceEventX event, ActivityContextInterface aci) {

        try {

            HashMap map = new HashMap();
            ChildRelation childRelation = getChildRelation();
            SbbLocalObject childLocalObject = null;

            // Create the child SBB and detach it.
            childLocalObject = childRelation.create();
            aci.detach(childLocalObject);

            // Assume success - detaching a child should
            // have done _nothing_ - no exceptions thrown.

            map.put("Result", new Boolean(true));
            map.put("Message", "Ok");
            TCKSbbUtils.getResourceInterface().sendSbbMessage(map);
            return;

        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }

    }
}
