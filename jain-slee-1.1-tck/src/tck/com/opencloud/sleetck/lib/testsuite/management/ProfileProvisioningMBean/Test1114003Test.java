/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.management.ProfileProvisioningMBean;

import java.rmi.RemoteException;
import java.util.Collection;
import java.util.Iterator;
import java.util.Vector;

import javax.management.ObjectName;
import javax.slee.management.ManagementException;
import javax.slee.profile.ProfileSpecificationID;
import javax.slee.profile.UnrecognizedProfileTableNameException;
import javax.slee.InvalidArgumentException;

import com.opencloud.sleetck.lib.SleeTCKTest;
import com.opencloud.sleetck.lib.SleeTCKTestUtils;
import com.opencloud.sleetck.lib.TCKTestErrorException;
import com.opencloud.sleetck.lib.TCKTestResult;
import com.opencloud.sleetck.lib.infra.SleeTCKComponentConstants;
import com.opencloud.sleetck.lib.resource.testapi.TCKResourceListener;
import com.opencloud.sleetck.lib.testutils.BaseTCKResourceListener;
import com.opencloud.sleetck.lib.testutils.ComponentIDLookup;
import com.opencloud.sleetck.lib.testutils.ProfileUtils;
import com.opencloud.sleetck.lib.testutils.jmx.ProfileProvisioningMBeanProxy;

/**
 * Test the profileProvisioningProxy method getProfileTableUsageMBean()
 */
public class Test1114003Test implements SleeTCKTest {

    private static final String PROFILE_DU_PATH_PARAM = "profileDUPath";
    private static final int ASSERTION_ID = 1114003;

    public void init(SleeTCKTestUtils utils) {
        this.utils = utils;
    }

    /**
     * Perform the actual test.
     */
    public TCKTestResult run() throws Exception {
        ProfileProvisioningMBeanProxy profileProvisioningProxy = profileUtils.getProfileProvisioningProxy();

        ProfileSpecificationID profileSpecID = new ComponentIDLookup(utils).lookupProfileSpecificationID(
                "Test1114003Profile",SleeTCKComponentConstants.TCK_VENDOR,"1.1");
        if(profileSpecID == null) throw new TCKTestErrorException("No ProfileSpecification found.");

        String validName = "Test1114003Profile";
        try {
            utils.getLog().info("Attempting to create a profile table with the following valid name: "+validName);
            profileProvisioningProxy.createProfileTable(profileSpecID,validName);
            tablesAdded.addElement(validName);
            utils.getLog().info("The createProfileTable() method threw no Exception. Calling getProfileTables() to check whether the "+
                    "profile table was created");

            Collection profileTableNames = profileProvisioningProxy.getProfileTables(profileSpecID);
            if(profileTableNames.size() < 1)
                return TCKTestResult.failed(1114003, "No profile tables exist as returned by getProfileTables()");
        } catch (Exception e) {
            utils.getLog().warning(e);
            return TCKTestResult.failed(1114003, "ProfileProvisioningMBean has error creating ProfileTables: " + e.getClass().toString());
        }

        try {
            ObjectName object = profileProvisioningProxy.getProfileTableUsageMBean(validName);
            utils.getLog().fine ("ProfileTableUsageMBean is: " +object.toString());
            if (object.toString().equals("javax.slee.management.usage:type=ProfileTableUsage,profileTableName=\"Test1114003Profile\""))
                logSuccessfulCheck(1114003);
            else
                return TCKTestResult.failed(1114003, "ProfileProvisioningMBean.getProfileTableUsageMBean has returned incorrect object name: " +object.toString());

         } catch (Exception e) {
            utils.getLog().warning(e);
            return TCKTestResult.failed(1114003, "ProfileProvisioningMBean.getProfileTableUsageMBean has thrown Exception: " + e.getClass().toString());
         }

        String nullName = null;
        try {
            ObjectName object = profileProvisioningProxy.getProfileTableUsageMBean(nullName);
            return TCKTestResult.failed(1114575, "ProfileProvisioningMBean.getProfileTableUsageMBean has not thrown Exception: NullPointerException" );
        } catch (NullPointerException e) {
            logSuccessfulCheck(1114575);
         } catch (Exception e) {
            utils.getLog().warning(e);
            return TCKTestResult.failed(1114575, "ProfileProvisioningMBean.getProfileTableUsageMBean has thrown Exception: " + e.getClass().toString());
         }

        String badName = "BadProfile";
        try {
            ObjectName object = profileProvisioningProxy.getProfileTableUsageMBean(badName);
            return TCKTestResult.failed(1114576, "ProfileProvisioningMBean.getProfileTableUsageMBean has thrown Exception: UnrecognizedProfileTableNameException" );
         } catch (UnrecognizedProfileTableNameException e) {
            logSuccessfulCheck(1114576);
         } catch (Exception e) {
            utils.getLog().warning(e);
            return TCKTestResult.failed(1114576, "ProfileProvisioningMBean.getProfileTableUsageMBean has thrown Exception: " + e.getClass().toString());
         }

         // This is the case where the Profile Table has no Usage
         try {
             ProfileSpecificationID noUsageProfileSpecID = new ComponentIDLookup(utils).lookupProfileSpecificationID(
                     "SimpleProfile11",SleeTCKComponentConstants.TCK_VENDOR,"1.1");
             if(noUsageProfileSpecID == null) throw new TCKTestErrorException("No ProfileSpecification found.");
             profileProvisioningProxy.createProfileTable(noUsageProfileSpecID,"Test1114003NoUsageProfile");
             tablesAdded.addElement("Test1114003NoUsageProfile");
         } catch (Exception e) {
             utils.getLog().warning(e);
             return TCKTestResult.failed(1114003, "ProfileProvisioningMBean has error creating ProfileTables: " + e.getClass().toString());
         }

         try {
             ObjectName object = profileProvisioningProxy.getProfileTableUsageMBean("Test1114003NoUsageProfile");
             return TCKTestResult.failed(1114577, "ProfileProvisioningMBean.getProfileTableUsageMBean has not thrown Exception: InvalidArgumentException");
         } catch (InvalidArgumentException e) {
             logSuccessfulCheck(1114577);
         } catch (Exception e) {
             utils.getLog().warning(e);
             return TCKTestResult.failed(1114577, "ProfileProvisioningMBean.getProfileTableUsageMBean has thrown Exception: " + e.getClass().toString());
         }

        return TCKTestResult.passed();
    }

    /**
     * Do all the pre-run configuration of the test.
     */
    public void setUp() throws Exception {
        tablesAdded = new Vector();

        // Install the Deployable Units
        String duPath = utils.getTestParams().getProperty(PROFILE_DU_PATH_PARAM);
        utils.getLog().fine("Installing the profile spec: " + duPath);
        utils.install(duPath);
        profileUtils = new ProfileUtils(utils);

    }

    /**
     * Clean up after the test.
     */
    public void tearDown() throws Exception {
        if (profileUtils != null && tablesAdded != null && !tablesAdded.isEmpty()) {
            Iterator tablesAddedIter = tablesAdded.iterator();
            while(tablesAddedIter.hasNext()) {
                try {
                    profileUtils.removeProfileTable((String)tablesAddedIter.next());
                } catch (Exception e) {
                    utils.getLog().warning(e);
                }
            }
        }
        utils.getLog().fine("Uninstalling the profile spec");
        utils.uninstallAll();
        utils.getLog().fine("Disconnecting from resource");
        utils.getResourceInterface().removeResourceListener();
    }

    private void logSuccessfulCheck(int assertionID) {
        utils.getLog().info("Check for assertion "+assertionID+" OK");
    }

    private SleeTCKTestUtils utils;
    private TCKResourceListener resourceListener;
    private ProfileUtils profileUtils;
    private Vector tablesAdded;

}
