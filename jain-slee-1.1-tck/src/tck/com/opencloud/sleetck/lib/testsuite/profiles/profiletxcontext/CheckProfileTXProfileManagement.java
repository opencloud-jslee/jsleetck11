/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.profiles.profiletxcontext;

public interface CheckProfileTXProfileManagement {
    String INITIAL_VALUE = "INITIALIZED";
    String CHANGED_VALUE = "CHANGED_VALUE";

    // Subset of CheckProfileTXProfileCMP methods ...

    // Values set from the test client

    public String getValue2();
    public void setValue2(String value2);

    public String getValue3();

    // The values as seen in the management method setValue3ViaManagementMethod()

    public String getValue2InManagementMethod();

    // The values as seen in profileStore()

    public String getValue1InProfileStore();
    public String getValue2InProfileStore();
    public String getValue3InProfileStore();

    // Set to make the commit fail in profileVerify()

    public boolean getValid();
    public void setValid(boolean valid);

    // Non CheckProfileTXProfileCMP methods ...

    public void setValue3ViaManagementMethod(String value3);

}
