/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.profiles.profiletable;

import java.util.Collection;
import javax.slee.profile.ProfileTable;

/**
 * Parameters are in wrong order
 *
 */
public interface Test1110130_9ProfileTable extends ProfileTable {
    public Collection queryIsAnswerToLife();
    public Collection queryEqualsValue(String value);
    public Collection queryNotEqualsValue(String value);
    public Collection queryNotEqualsValues(int intValue, String value);
}
