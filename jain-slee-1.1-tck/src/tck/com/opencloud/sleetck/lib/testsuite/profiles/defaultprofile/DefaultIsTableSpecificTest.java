/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.profiles.defaultprofile;

import javax.slee.profile.ProfileSpecificationID;
import javax.slee.profile.ProfileAlreadyExistsException;
import javax.management.ObjectName;
import com.opencloud.sleetck.lib.AbstractSleeTCKTest;
import com.opencloud.sleetck.lib.TCKTestErrorException;
import com.opencloud.sleetck.lib.TCKTestFailureException;
import com.opencloud.sleetck.lib.DescriptionKeys;
import com.opencloud.sleetck.lib.TCKTestResult;
import com.opencloud.sleetck.lib.testsuite.profiles.ProfileTestConstants;
import com.opencloud.sleetck.lib.testsuite.profiles.simpleprofile.SimpleProfileProxy;
import com.opencloud.sleetck.lib.testutils.ProfileUtils;
import com.opencloud.sleetck.lib.testutils.ComponentIDLookup;
import com.opencloud.sleetck.lib.testutils.jmx.ProfileProvisioningMBeanProxy;
import com.opencloud.sleetck.lib.infra.SleeTCKComponentConstants;
import java.util.Vector;
import java.util.Iterator;

/**
 * Tests assertion 923, that each Profile Table has a
 * default Profile that is specific to the Profile Table.
 */
public class DefaultIsTableSpecificTest extends AbstractSleeTCKTest {

    private static final String PROFILE_TABLE_A_NAME = "tck.DefaultIsTableSpecificTest.tableA";
    private static final String PROFILE_TABLE_B_NAME = "tck.DefaultIsTableSpecificTest.tableB";
    private static final String CHANGED_PROFILE_VALUE = "Changed";

    public TCKTestResult run() throws Exception {
        // create the profile tables
        ProfileProvisioningMBeanProxy profileProvisioning = profileUtils.getProfileProvisioningProxy();
        ProfileSpecificationID profileSpecID = new ComponentIDLookup(utils()).lookupProfileSpecificationID(
            ProfileTestConstants.SIMPLE_PROFILE_SPEC_NAME,SleeTCKComponentConstants.TCK_VENDOR,"1.0");
        String[] TABLE_NAMES = {PROFILE_TABLE_A_NAME,PROFILE_TABLE_B_NAME};
        for (int i = 0; i < TABLE_NAMES.length; i++) {
            getLog().info("Creating profile table: "+TABLE_NAMES[i]);
            profileProvisioning.createProfileTable(profileSpecID,TABLE_NAMES[i]);
            createdTables.addElement(TABLE_NAMES[i]);
        }
        // edit the default profile for table A
        ObjectName defaultAName = profileProvisioning.getDefaultProfile(PROFILE_TABLE_A_NAME);
        SimpleProfileProxy defaultAProxy = new SimpleProfileProxy(defaultAName,utils().getMBeanFacade());
        activeProxies.addElement(defaultAProxy);
        String initialAValue = defaultAProxy.getValue();
        if(initialAValue != null) throw new TCKTestErrorException("Expected simple profile value to be null, but found: "+initialAValue);
        getLog().info("Editing default profile for table "+PROFILE_TABLE_A_NAME);
        defaultAProxy.editProfile();
        defaultAProxy.setValue(CHANGED_PROFILE_VALUE);
        defaultAProxy.commitProfile();

        // check that our changes to table A's default profile do not affect B's default profile
        ObjectName defaultBName = profileProvisioning.getDefaultProfile(PROFILE_TABLE_B_NAME);
        SimpleProfileProxy defaultBProxy = new SimpleProfileProxy(defaultBName,utils().getMBeanFacade());
        activeProxies.addElement(defaultBProxy);
        getLog().info("Querying default profile for table "+PROFILE_TABLE_B_NAME);
        String initialBValue = defaultBProxy.getValue();
        if(initialBValue != null) {
            if(initialBValue.equals(CHANGED_PROFILE_VALUE)) throw new TCKTestFailureException(923,
                "Changes to the default profile of one table affected the default profile of another table");
            else throw new TCKTestErrorException("Expected simple profile value to be null, but found: "+initialBValue);
        }
        return TCKTestResult.passed();
    }

    public void setUp() throws Exception {
        String duPath = utils().getTestParams().getProperty(ProfileTestConstants.PROFILE_SPEC_DU_PATH_PARAM);
        getLog().fine("Installing profile specification");
        utils().install(duPath);
        profileUtils = new ProfileUtils(utils());
        createdTables = new Vector();
        activeProxies = new Vector();
    }

    public void tearDown() throws Exception {
        // close profiles
        if(activeProxies != null) {
            getLog().fine("Closing profiles");
            Iterator activeProxiesIter = activeProxies.iterator();
            while(activeProxiesIter.hasNext()) {
                SimpleProfileProxy aProxy = (SimpleProfileProxy)activeProxiesIter.next();
                try {
                    if(aProxy != null) {
                        if(aProxy.isProfileWriteable()) aProxy.restoreProfile();
                        aProxy.closeProfile();
                    }
                } catch (Exception ex) {
                    getLog().warning("Exception caught while trying to close profile:");
                    getLog().warning(ex);
                }
            }
        }
        // delete profile tables
        try {
            if(profileUtils != null && !createdTables.isEmpty()) {
                getLog().fine("Removing profile tables");
                Iterator createdTablesIter = createdTables.iterator();
                while(createdTablesIter.hasNext()) profileUtils.removeProfileTable((String)createdTablesIter.next());
            }
        } catch(Exception e) {
            getLog().warning("Caught exception while trying to remove profile tables:");
            getLog().warning(e);
        }
        profileUtils = null;
        createdTables = null;
        activeProxies = null;
        super.tearDown();
    }

    private Vector createdTables;
    private Vector activeProxies;
    private ProfileUtils profileUtils;

}
