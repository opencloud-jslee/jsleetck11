/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.profiles.profilecommit;

import com.opencloud.sleetck.lib.sbbutils.BaseTCKSbb;
import com.opencloud.sleetck.lib.sbbutils.TCKSbbUtils;
import com.opencloud.sleetck.lib.resource.events.TCKResourceEventX;
import com.opencloud.sleetck.lib.testsuite.profiles.simpleprofile.SimpleProfileCMP;

import javax.slee.ActivityContextInterface;
import javax.slee.profile.UnrecognizedProfileNameException;
import javax.slee.profile.UnrecognizedProfileTableNameException;
import javax.slee.profile.ProfileID;
import javax.slee.facilities.Level;
import java.util.HashMap;

public abstract class ProfileCommitTestSbb extends BaseTCKSbb {
    public void onTCKResourceEventX1(TCKResourceEventX event, ActivityContextInterface aci) {
        try {
            String expectedValue = (String)event.getMessage();
            String currentValue;

            SimpleProfileCMP profile = getCmpSimpleProfile(new ProfileID("ProfileCommitTestTable", "A"));
            currentValue = profile.getValue();

            TCKSbbUtils.createTrace(getSbbID(), Level.INFO, "(X1)Expecting value: " + expectedValue, null);
            TCKSbbUtils.createTrace(getSbbID(), Level.INFO, "(X1)Actual value 1: " + currentValue, null);
            if (! currentValue.equals(expectedValue)) {
                sendResponse(event, "fail1");
            }

            // Make a synchronous call back to the test to change the profile value
            TCKSbbUtils.createTrace(getSbbID(), Level.INFO, "(X1)Making a synchronous call back to the test", null);
            TCKSbbUtils.getResourceInterface().callTest(null);

            // Retrieve the value again from the profile
            currentValue = profile.getValue();
            TCKSbbUtils.createTrace(getSbbID(), Level.INFO, "(X1)Actual value 2: " + currentValue, null);

            if (! currentValue.equals(expectedValue)) {
                sendResponse(event, "fail2");
            }

            sendResponse(event, "pass");

        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }
    }

    public void onTCKResourceEventX2(TCKResourceEventX event, ActivityContextInterface aci) {
        try {
            String expectedValue = (String)event.getMessage();
            String currentValue;

            SimpleProfileCMP profile = getCmpSimpleProfile(new ProfileID("ProfileCommitTestTable", "A"));
            currentValue = profile.getValue();

            TCKSbbUtils.createTrace(getSbbID(), Level.INFO, "(X2)Expecting value: " + expectedValue, null);
            TCKSbbUtils.createTrace(getSbbID(), Level.INFO, "(X2)Actual value 1: " + currentValue, null);
            if (! currentValue.equals(expectedValue)) {
                sendResponse(event, "fail");
            }

            sendResponse(event, "pass");
        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }
    }

    private void sendResponse(TCKResourceEventX event, String status) {
        try {
            HashMap response = new HashMap();
            response.put("event", event);
            response.put("status", status);
            TCKSbbUtils.getResourceInterface().sendSbbMessage(response);
        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }
    }

    public void sbbRolledBack(javax.slee.RolledBackContext context) {
        try {
            TCKSbbUtils.createTrace(getSbbID(), Level.INFO, "SBB Rolled Back", null);
        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }
    }

    public abstract SimpleProfileCMP getCmpSimpleProfile(ProfileID profileID)
            throws UnrecognizedProfileTableNameException,
            UnrecognizedProfileNameException;
}
