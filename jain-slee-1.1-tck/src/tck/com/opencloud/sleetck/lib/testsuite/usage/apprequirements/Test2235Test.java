/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.usage.apprequirements;

import com.opencloud.sleetck.lib.SleeTCKTest;
import com.opencloud.sleetck.lib.SleeTCKTestUtils;
import com.opencloud.sleetck.lib.TCKTestErrorException;
import com.opencloud.sleetck.lib.TCKTestResult;

import javax.slee.management.DeploymentException;

public class Test2235Test implements SleeTCKTest {

    private static final String SERVICE_DU_PATH_PARAM = "DUPath";
    private static final int TEST_ID = 2235;

    public void init(SleeTCKTestUtils utils) {
            this.utils = utils;
    }

    /**
     * Perform the actual test.
     */

    public TCKTestResult run() throws Exception {

        // Install the Deployable Unit
        String duPath = utils.getTestParams().getProperty(SERVICE_DU_PATH_PARAM);
        try {
            utils.install(duPath);
        } catch (TCKTestErrorException e) {
            Exception enclosedException = e.getEnclosedException();
            if (enclosedException != null && enclosedException instanceof DeploymentException) {
                utils.getLog().info("Caught DeploymentException as expected: "+enclosedException);
                return TCKTestResult.passed();
            } else throw e;
        }
        return TCKTestResult.failed(TEST_ID, "Was able to deploy an service with an Sbb which declared a usage interface, "+
                "without it being defined in the deployment descriptor.");
    }

    /**
     * Do all the pre-run configuration of the test.
     */
    public void setUp() throws Exception {
    }

    /**
     * Clean up after the test.
     */

    public void tearDown() throws Exception {
        utils.uninstallAll();
    }

    private SleeTCKTestUtils utils;

}
