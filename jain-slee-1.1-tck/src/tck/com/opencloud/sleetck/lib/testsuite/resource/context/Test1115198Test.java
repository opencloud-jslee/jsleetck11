/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.resource.context;

import java.util.HashMap;

import com.opencloud.sleetck.lib.TCKTestErrorException;
import com.opencloud.sleetck.lib.TCKTestFailureException;
import com.opencloud.sleetck.lib.TCKTestResult;
import com.opencloud.sleetck.lib.testsuite.resource.BaseResourceTest;
import com.opencloud.sleetck.lib.testsuite.resource.RAMethods;

/**
 * Test to ensure that getUsageParameterSet() is returning the correct result.
 * <p>
 * Test assertion ID: 1115198, 1115200, 1115201, 1115203
 */
public class Test1115198Test extends BaseResourceTest {

    private static final int ASSERTION_ID = 1115198;

    public TCKTestResult run() throws Exception {
        HashMap resultmap = sendMessage(RAMethods.getUsageParameterSet);
        if (resultmap == null)
            throw new TCKTestFailureException(ASSERTION_ID, "Test timed out while waiting for response to getUsageParameterSet()");

        // Check valid UsageParameterSet
        Object result = resultmap.get("result1");
        if (result instanceof Exception)
            throw new TCKTestFailureException(ASSERTION_ID, "Unexpected exception thrown while invoking getUsageParameterSet()", (Exception) result);
        if (result instanceof String)
            throw new TCKTestFailureException(ASSERTION_ID, "An error occured while invoking getUsageParameterSet(): " + result);
        if (!Boolean.TRUE.equals(result))
            throw new TCKTestErrorException("Unexpected result received while invoking getUsageParameterSet(): " + result);

        result = resultmap.get("result2");
        if (result instanceof Exception)
            throw new TCKTestFailureException(1115200,
                    "An exception was thrown while trying to typecast the UsageParamaterSet returned by ResourceAdaptorContext.getUsageParameterSet()",
                    (Exception) result);
        if (!Boolean.TRUE.equals(result))
            throw new TCKTestErrorException("Unexpected result received while trying to typecast the UsageParamaterSet returned by getUsageParameterSet(): "
                    + result);

        result = resultmap.get("result3");
        if (result instanceof Exception)
            throw new TCKTestFailureException(1115201,
                    "An unexpected exception occured while calling ResourceAdaptorContext.getUsageParameterSet(null) - expected NullPointerException",
                    (Exception) result);
        if (Boolean.FALSE.equals(result))
            throw new TCKTestFailureException(1115201, "ResourceAdaptorContext.getUsageParameterSet(null) failed to throw a NullPointerException");

        result = resultmap.get("result4");
        if (result instanceof Exception)
            throw new TCKTestFailureException(
                    1115203,
                    "An unexpected exception occured while calling ResourceAdaptorContext.getUsageParameterSet() - expected UnrecognizedUsageParameterSetNameException",
                    (Exception) result);
        if (Boolean.FALSE.equals(result))
            throw new TCKTestFailureException(1115203,
                    "ResourceAdaptorContext.getUsageParameterSet(null) failed to throw a UnrecognizedUsageParameterSetNameException");

        return TCKTestResult.passed();
    }
}
