/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.usage.apprequirements;

import com.opencloud.sleetck.lib.*;
import javax.slee.management.DeploymentException;

public class UsageParameterNamesTest implements SleeTCKTest {

    private static final String SERVICE_DU_PATH_PARAM_PREFIX = "DUPath-";
    private static final int TEST_ID = 2483;

    public void init(SleeTCKTestUtils utils) {
            this.utils = utils;
    }

    /**
     * Perform the actual test.
     */

    public TCKTestResult run() throws Exception {
        String[] testCases = {"a","b","c","d","e","f"};
        String[] identifiers = {"public","0Foo","true","class","9Bar","false"};
        for (int i = 0; i < testCases.length; i++) {
            testService(testCases[i],identifiers[i]);
        }
        return TCKTestResult.passed();
    }

    private void testService(String testCaseID, String identifier) throws TCKTestFailureException, TCKTestErrorException {
        // Install the Deployable Unit
        String duPath = utils.getTestParams().getProperty(SERVICE_DU_PATH_PARAM_PREFIX+testCaseID);
        try {
            utils.install(duPath);
        } catch (TCKTestErrorException e) {
            Exception enclosedException = e.getEnclosedException();
            if (enclosedException != null && enclosedException instanceof DeploymentException) {
                utils.getLog().info("Caught DeploymentException as expected: "+enclosedException);
                return; // expected
            } else throw e;
        }
        throw new TCKTestFailureException(TEST_ID, "Was able to deploy a service containing "+
                "a usage parameter which is not a valid java identifier: "+identifier);
    }

    /**
     * Do all the pre-run configuration of the test.
     */
    public void setUp() throws Exception {
    }

    /**
     * Clean up after the test.
     */

    public void tearDown() throws Exception {
        utils.uninstallAll();
    }

    private SleeTCKTestUtils utils;

}
