/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.resource.endpoint;

import java.util.HashMap;

import com.opencloud.sleetck.lib.TCKTestErrorException;
import com.opencloud.sleetck.lib.TCKTestFailureException;
import com.opencloud.sleetck.lib.TCKTestResult;
import com.opencloud.sleetck.lib.testsuite.resource.BaseResourceTest;
import com.opencloud.sleetck.lib.testsuite.resource.RAMethods;

/**
 * Test to ensure that endActivityTransacted() is throwing correct exceptions.
 * <p>
 * Test assertion: 1115291, 1115293
 */
public class Test1115291Test extends BaseResourceTest {

    private final static int ASSERTION_ID1 = 1115291;
    private final static int ASSERTION_ID2 = 1115293;

    public TCKTestResult run() throws Exception {
        HashMap results = sendMessage(RAMethods.endActivityTransacted, new Integer(ASSERTION_ID1));

        if (results == null)
            throw new TCKTestErrorException("Test timed out while waiting for response from test component");

        Object result1 = results.get("result1");
        if (result1 instanceof Exception)
            throw new TCKTestFailureException(ASSERTION_ID1, "Unexpected exception thrown during test:", (Exception) result1);
        if (Boolean.FALSE.equals(result1))
            throw new TCKTestFailureException(ASSERTION_ID1, "endActivityTransacted() failed to throw a NullPointerException when required");
        if (!Boolean.TRUE.equals(result1))
            throw new TCKTestErrorException("Unexpected result received from test component: " + result1);

        Object result2 = results.get("result2");
        if (result2 instanceof Exception)
            throw new TCKTestFailureException(ASSERTION_ID2, "Unexpected exception thrown during test:", (Exception) result2);
        if (Boolean.FALSE.equals(result2))
            throw new TCKTestFailureException(ASSERTION_ID2, "endActivityTransacted() failed to throw a UnrecognizedActivityHandleException when required");
        if (!Boolean.TRUE.equals(result2))
            throw new TCKTestErrorException("Unexpected result received from test component: " + result2);

        return TCKTestResult.passed();
    }
}
