/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.usage.SbbUsageMBean;

import com.opencloud.sleetck.lib.TCKTestResult;
import com.opencloud.sleetck.lib.TCKTestFailureException;
import com.opencloud.sleetck.lib.testsuite.usage.common.GenericUsageTest;
import com.opencloud.sleetck.lib.testutils.jmx.ServiceUsageMBeanProxy;

import javax.management.ObjectName;
import javax.management.NotificationBroadcaster;
import javax.slee.SbbID;
import javax.slee.usage.SbbUsageMBean;

/**
 * Checks that a SBB usage MBean generated for a given usage parameter interface
 * extends the SbbUsageMBean interface and the NotificationBroadcaster interface.
 */
public class Test2366Test extends GenericUsageTest {

    public TCKTestResult run() throws Exception {
        SbbID genericSbbID = getGenericUsageMBeanLookup().getSbbID();
        ServiceUsageMBeanProxy serviceUsageMBeanProxy = getGenericUsageMBeanLookup().getServiceUsageMBeanProxy();
        ObjectName usageMBeanName = serviceUsageMBeanProxy.getSbbUsageMBean(genericSbbID);
        checkUsageMBean(2334,usageMBeanName,NotificationBroadcaster.class.getName());
        return TCKTestResult.passed();
    }

    private void checkUsageMBean(int assertionID, ObjectName usageMBeanName, String interfaceName) throws Exception {
        utils().getLog().info("Checking that MBean "+usageMBeanName+
                " returned by getSbbUsageMBean() is an instance of "+interfaceName);
        if(!utils().getMBeanFacade().isInstanceOf(usageMBeanName,interfaceName))
            throw new TCKTestFailureException(assertionID, "MBean "+usageMBeanName+
                    " returned by getSbbUsageMBean() is not an instance of "+interfaceName);
    }

}
