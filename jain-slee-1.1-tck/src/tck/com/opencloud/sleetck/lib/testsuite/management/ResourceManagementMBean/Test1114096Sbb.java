/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.management.ResourceManagementMBean;

import java.util.HashMap;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.slee.ActivityContextInterface;
import javax.slee.SbbContext;
import javax.slee.ServiceID;
import javax.slee.facilities.Tracer;
import javax.slee.resource.ResourceAdaptorContext;

import com.opencloud.sleetck.lib.rautils.TCKRAConstants;
import com.opencloud.sleetck.lib.sbbutils.BaseTCKSbb;
import com.opencloud.sleetck.lib.testsuite.resource.RAMethods;
import com.opencloud.sleetck.lib.testsuite.resource.SimpleEvent;
import com.opencloud.sleetck.lib.testsuite.resource.SimpleSbbInterface;
import com.opencloud.sleetck.lib.testsuite.resource.TCKMessage;

public abstract class Test1114096Sbb extends BaseTCKSbb {

    // Event handler
    public void onSimpleEvent(SimpleEvent event, ActivityContextInterface aci) {
        tracer.info("onSimpleEvent() event handler called.");
        try {
            SimpleSbbInterface sbbInterface = getSbbInterface();
            ResourceAdaptorContext raContext = sbbInterface.getResourceAdaptorContext();
            HashMap results = new HashMap();
            try {
                ServiceID invokingService = raContext.getInvokingService();
                results.put("result2", invokingService);
            } catch (Exception e) {
                tracer.severe("An error occured while trying to call getInvokingService() on the TCK_Context_Test_RA from an Sbb:", e);
                results.put("result2", e);
            }
            sbbInterface.sendSbbMessage(new TCKMessage(null, RAMethods.getInvokingService, results));
        } catch (Exception e) {
            tracer.severe("Exception caught while trying to send message from SBB to TCK", e);
        }
    }

    public void setSbbContext(SbbContext context) {
        this.tracer = context.getTracer("SimpleSBB");
    }

    private SimpleSbbInterface getSbbInterface() throws NamingException {
        return (SimpleSbbInterface) getSbbEnvironment().lookup(TCKRAConstants.SIMPLE_RESOURCE_ADAPTOR_LOCATION);
    }

    private Context getSbbEnvironment() throws NamingException {
        Context initCtx = new InitialContext();
        return (Context) initCtx.lookup("java:comp/env");
    }

    public void unsetSbbContext() {
        tracer = null;
    }

    private Tracer tracer;
}
