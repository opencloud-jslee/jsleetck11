/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.sbb.abstractclass;

import com.opencloud.sleetck.lib.sbbutils.BaseTCKSbb;
import com.opencloud.sleetck.lib.sbbutils.TCKSbbUtils;
import com.opencloud.sleetck.lib.resource.events.TCKResourceEventX;

import javax.slee.ActivityContextInterface;
import javax.slee.facilities.Level;

/**
 * An SBB to test assertion 2049: that no more methods will be invoked on an SBB object after an unchecked exception is
 * caught.
 */
public abstract class Test2013Sbb extends BaseTCKSbb {
    private String state = "NewSbb";

    public void unsetSbbContext() {
        createTrace("unsetSbbContext");
        super.unsetSbbContext();
        try {
            TCKSbbUtils.getResourceInterface().sendSbbMessage("unsetSbbContext");
        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }
    }

    public void onTCKResourceEventX1(TCKResourceEventX event, ActivityContextInterface aci) {
        // Save the state message passed in with this event -- this is used in assertion 2013
        state = (String) event.getMessage();
        // Throw an unchecked exception
        throw new RuntimeException("Test349 exception");
    }

    public void onTCKResourceEventX2(TCKResourceEventX event, ActivityContextInterface aci) {
        // Return the stored state of the SBB.  Use this to assert that the message is being handled by a new SBB.
        try {
            TCKSbbUtils.getResourceInterface().sendSbbMessage(state);
        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }
    }

    public void sbbRolledBack(javax.slee.RolledBackContext context) {
        // no-op, overrides the std behaviour in superclass
    }

    public void sbbExceptionThrown(Exception exception, Object event, ActivityContextInterface aci) {
        // no-op, overrides the std behaviour in superclass
    }

    private void createTrace(String message) {
        try {
            TCKSbbUtils.createTrace(getSbbID(), Level.INFO, message, null);
        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }
    }
}
