/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.facilities.timerfacility;

import com.opencloud.sleetck.lib.sbbutils.BaseTCKSbb;
import com.opencloud.sleetck.lib.sbbutils.TCKSbbUtils;
import com.opencloud.sleetck.lib.resource.events.TCKResourceEventX;
import com.opencloud.sleetck.lib.resource.TCKTestCallException;
import com.opencloud.sleetck.lib.TCKTestErrorException;

import javax.slee.ActivityContextInterface;
import javax.slee.nullactivity.NullActivityFactory;
import javax.slee.nullactivity.NullActivity;
import javax.slee.nullactivity.NullActivityContextInterfaceFactory;
import javax.slee.facilities.TimerEvent;
import javax.slee.facilities.Level;
import javax.slee.facilities.TimerFacility;
import javax.slee.facilities.TimerOptions;
import javax.naming.InitialContext;

/**
 *
 */
public abstract class Test1187Sbb extends BaseTCKSbb {

    public void onTCKResourceEventX1(TCKResourceEventX event, ActivityContextInterface aci) {
        try {
            createTrace("onTCKResourceEventX1 - event received");
            NullActivityFactory nullActivityFactory = (NullActivityFactory)
                    new InitialContext().lookup("java:comp/env/slee/nullactivity/factory");
            NullActivity activity = nullActivityFactory.createNullActivity();
            NullActivityContextInterfaceFactory nullACIFactory = (NullActivityContextInterfaceFactory)
                    new InitialContext().lookup("java:comp/env/slee/nullactivity/activitycontextinterfacefactory");
            ActivityContextInterface nullACI = nullACIFactory.getActivityContextInterface(activity);

            TimerFacility timerFacility = (TimerFacility)
                    new InitialContext().lookup("java:comp/env/slee/facilities/timer");

            nullACI.attach(getSbbContext().getSbbLocalObject());
            timerFacility.setTimer(nullACI, null, System.currentTimeMillis() + 2000, new TimerOptions());
            activity.endActivity();

        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }
    }

    public void onTimerEvent(TimerEvent event, ActivityContextInterface aci) {
        createTrace("onTimerEvent: enter");
        try {
            TCKSbbUtils.getResourceInterface().callTest(null);
        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }
        createTrace("onTimerEvent: exit");
    }

    private void createTrace(String message) {
        try {
            TCKSbbUtils.createTrace(getSbbID(), Level.INFO, message, null);
        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }
    }
}
