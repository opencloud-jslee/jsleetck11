/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.resource.endpoint;

import java.util.HashMap;

import javax.slee.ActivityContextInterface;
import javax.slee.ActivityEndEvent;

import com.opencloud.sleetck.lib.rautils.UOID;
import com.opencloud.sleetck.lib.testsuite.resource.BaseResourceSbb;
import com.opencloud.sleetck.lib.testsuite.resource.RAMethods;
import com.opencloud.sleetck.lib.testsuite.resource.SimpleEvent;

public abstract class Test1115316Sbb extends BaseResourceSbb {

    private static final int ASSERTION_ID = 1115316;

    // Event handlers
    public void onSimpleEvent(SimpleEvent event, ActivityContextInterface aci) {
        tracer.info("onSimpleEvent() event handler called with event: " + event);
        tracer.info("Setting CMP field with initial event");
        setInitialEvent(event);
    }

    public void onActivityEndEvent(ActivityEndEvent event, ActivityContextInterface aci) {
        tracer.info("onActivityEnd() event handler called with event: " + event);

        tracer.info("Getting initial event from CMP field");
        SimpleEvent initialEvent = getInitialEvent();
        int sequenceID = initialEvent.getSequenceID();

        if (sequenceID == -1)
            return;

        HashMap results = new HashMap();
        results.put("result2", Boolean.TRUE);

        tracer.info("Asking test Resource Adaptor to fire another event on this activity");

        try {
            getSbbInterface().executeTestLogic(new Integer(ASSERTION_ID));
        } catch (Exception e) {
            tracer.severe("An error occured invoking test logic on the test Resource Adaptor", e);
            results.put("result2", Boolean.FALSE);
        }

        tracer.info("Sending results from SBB to TCK");
        sendSbbMessage(sbbUID, sequenceID, RAMethods.fireEvent, results);
    }

    // CMP field 'InitialEvent' of type SimpleEvent
    public abstract SimpleEvent getInitialEvent();
    public abstract void setInitialEvent(SimpleEvent event);

    private static final UOID sbbUID = UOID.createUOID();
}
