/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.profiles.events;

import com.opencloud.sleetck.lib.AbstractSleeTCKTest;
import com.opencloud.sleetck.lib.TCKTestResult;
import com.opencloud.sleetck.lib.DescriptionKeys;
import com.opencloud.sleetck.lib.OperationTimedOutException;
import com.opencloud.sleetck.lib.TCKTestErrorException;
import com.opencloud.sleetck.lib.TCKTestFailureException;
import com.opencloud.sleetck.lib.testutils.QueuingResourceListener;
import com.opencloud.sleetck.lib.testutils.ProfileUtils;
import com.opencloud.sleetck.lib.testutils.jmx.ProfileProvisioningMBeanProxy;
import com.opencloud.sleetck.lib.testutils.ComponentIDLookup;
import com.opencloud.sleetck.lib.testsuite.profiles.ProfileTestConstants;
import com.opencloud.sleetck.lib.resource.TCKActivityID;
import com.opencloud.sleetck.lib.resource.testapi.TCKResourceTestInterface;
import com.opencloud.sleetck.lib.resource.events.TCKResourceEventX;
import com.opencloud.sleetck.lib.resource.TCKSbbMessage;
import com.opencloud.sleetck.lib.infra.SleeTCKComponentConstants;
import com.opencloud.sleetck.lib.testsuite.profiles.simpleprofile.SimpleProfileProxy;
import com.opencloud.sleetck.lib.testutils.jmx.ProfileMBeanProxy;
import java.util.Vector;
import java.util.Iterator;
import javax.slee.profile.ProfileSpecificationID;
import javax.slee.profile.ProfileAddedEvent;
import javax.slee.profile.ProfileUpdatedEvent;
import javax.slee.profile.ProfileRemovedEvent;
import javax.management.ObjectName;

public class ProfileEventsTest extends AbstractSleeTCKTest {

    public static final String TCK_SBB_EVENT_DU_PATH_PARAM = "eventDUPath";

    public static final String PROFILE_TABLE_X_NAME = "tck.ProfileEventsTest.table.X";
    public static final String PROFILE_TABLE_Y_NAME = "tck.ProfileEventsTest.table.Y";
    public static final String PROFILE_A_NAME = "tck.ProfileEventsTest.profile.A";
    public static final String PROFILE_B_NAME = "tck.ProfileEventsTest.profile.B";
    public static final String CHANGED_PROFILE_VALUE = "changedValue";

    /**
     * Creates profile tables X and Y.
     * Fires a TCKResourceEventX to the Sbb with configuration data.
     * Adds profile A then profile B to X.
     * Updates and commits B.
     * Removes A, then removes B.
     */
    public TCKTestResult run() throws Exception {

        // create the profile tables
        getLog().fine("Creating profile tables");
        ProfileProvisioningMBeanProxy profileProvisioning = profileUtils.getProfileProvisioningProxy();
        ProfileSpecificationID profileSpecID = new ComponentIDLookup(utils()).lookupProfileSpecificationID(
            ProfileTestConstants.SIMPLE_PROFILE_SPEC_NAME,SleeTCKComponentConstants.TCK_VENDOR,"1.0");
        profileProvisioning.createProfileTable(profileSpecID,PROFILE_TABLE_X_NAME);
        tablesCreated.addElement(PROFILE_TABLE_X_NAME);
        profileProvisioning.createProfileTable(profileSpecID,PROFILE_TABLE_Y_NAME);
        tablesCreated.addElement(PROFILE_TABLE_Y_NAME);

        // send an event to the test containing data to expect from the profile events
        TCKResourceTestInterface resource = utils().getResourceInterface();
        TCKActivityID activityID = resource.createActivity(getClass().getName());
        Object message = new ProfileEventsTestEventData(PROFILE_TABLE_X_NAME,PROFILE_A_NAME,PROFILE_B_NAME).toExported();
        getLog().fine("About to fire TCKResourceEventX.X1 event to the Sbb");
        resource.fireEvent(TCKResourceEventX.X1,message,activityID,null);
        // wait for an ACK from the test
        TCKSbbMessage reply = resourceListener.nextMessage();
        if(!TCKResourceEventX.class.getName().equals(reply.getMessage())) throw new TCKTestErrorException(
            "Received unexpected reply from SBB after sending TCKResourceEventX. Message="+reply.getMessage());

        // -- Add profile A then B to the profile table and expect the SBB to receive both ProfileAddedEvents -- //
        String[] profileNames = {PROFILE_A_NAME,PROFILE_B_NAME};
        for (int i = 0; i < profileNames.length; i++) {
            getLog().fine("Adding profile "+profileNames[i]+" to the profile table");
            javax.management.ObjectName profile = profileProvisioning.createProfile(PROFILE_TABLE_X_NAME,profileNames[i]);
            ProfileMBeanProxy profProxy = utils().getMBeanProxyFactory().createProfileMBeanProxy(profile);
            profProxy.commitProfile();
            // wait for a ProfileAddedEvent to be received by the SBB
            try {
                reply = resourceListener.nextMessage();
                if(!ProfileAddedEvent.class.getName().equals(reply.getMessage())) throw new TCKTestErrorException(
                    "Received unexpected reply from SBB after adding a profile. Message="+reply.getMessage());
            } catch (OperationTimedOutException ex) {
                throw new TCKTestFailureException(935,"Timed out waiting for acknowledgement of a ProfileAddedEvent "+
                    "following the addition of a profile.",ex);
            }
        }

        // -- Update profile B and expect the SBB to receive a ProfileUpdatedEvent -- //
        ObjectName profileBMBeanName = profileProvisioning.getProfile(PROFILE_TABLE_X_NAME,PROFILE_B_NAME);
        SimpleProfileProxy profileProxy = new SimpleProfileProxy(profileBMBeanName,utils().getMBeanFacade());
        profileProxy.editProfile();
        profileProxy.setValue(CHANGED_PROFILE_VALUE);
        profileProxy.commitProfile();
        // wait for a ProfileUpdatedEvent to be received by the SBB
        try {
            reply = resourceListener.nextMessage();
            if(!ProfileUpdatedEvent.class.getName().equals(reply.getMessage())) throw new TCKTestErrorException(
                "Received unexpected reply from SBB after updating a profile. Message="+reply.getMessage());
        } catch (OperationTimedOutException ex) {
            throw new TCKTestFailureException(2409,"Timed out waiting for acknowledgement of a ProfileUpdatedEvent "+
                "following the commit of changes to a profile.",ex);
        }

        // -- Remove profile A then B from the profile table and expect the SBB to receive both ProfileRemovedEvents -- //
        for (int i = 0; i < profileNames.length; i++) {
            getLog().fine("Removing profile "+profileNames[i]+" from the profile table");
            profileProvisioning.removeProfile(PROFILE_TABLE_X_NAME,profileNames[i]);
            // wait for a ProfileAddedEvent to be received by the SBB
            try {
                reply = resourceListener.nextMessage();
                if(!ProfileRemovedEvent.class.getName().equals(reply.getMessage())) throw new TCKTestErrorException(
                    "Received unexpected reply from SBB after removing a profile. Message="+reply.getMessage());
            } catch (OperationTimedOutException ex) {
                throw new TCKTestFailureException(936,"Timed out waiting for acknowledgement of a ProfileRemovedEvent "+
                    "following the removal of a profile.",ex);
            }
        }
        return TCKTestResult.passed();
    }

    public void setUp() throws Exception {
        // install profile spec and service
        getLog().fine("Installing profile specification");
        String profDUPath = utils().getTestParams().getProperty(ProfileTestConstants.PROFILE_SPEC_DU_PATH_PARAM);
        utils().install(profDUPath);

        getLog().fine("Installing TCKSbbEvent deployable unit");
        String eventDUPath = utils().getTestParams().getProperty(TCK_SBB_EVENT_DU_PATH_PARAM);
        utils().install(eventDUPath);

        setupService(DescriptionKeys.SERVICE_DU_PATH_PARAM);
        // set up the resource listener
        resourceListener = new QueuingResourceListener(utils());
        setResourceListener(resourceListener);
        profileUtils = new ProfileUtils(utils());
        tablesCreated = new Vector();
    }

    public void tearDown() throws Exception {
        // remove profile tables
        try {
            if(profileUtils != null && !tablesCreated.isEmpty()) {
                getLog().fine("Removing profile tables");
                Iterator tablesCreatedIter = tablesCreated.iterator();
                while(tablesCreatedIter.hasNext()) {
                    profileUtils.removeProfileTable((String)tablesCreatedIter.next());
                }
            }
        } catch(Exception e) {
            getLog().warning("Caught exception while trying to remove profile tables:");
            getLog().warning(e);
        }
        if (tablesCreated != null)tablesCreated.clear();
        super.tearDown();
    }

    private QueuingResourceListener resourceListener;
    private ProfileUtils profileUtils;
    private Vector tablesCreated;

}
