/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.resource.endpoint;

import java.rmi.RemoteException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Set;

import javax.slee.ServiceID;
import javax.slee.management.DeployableUnitID;

import com.opencloud.sleetck.lib.DescriptionKeys;
import com.opencloud.sleetck.lib.TCKTestErrorException;
import com.opencloud.sleetck.lib.TCKTestFailureException;
import com.opencloud.sleetck.lib.TCKTestResult;
import com.opencloud.sleetck.lib.testsuite.resource.BaseResourceTest;
import com.opencloud.sleetck.lib.testsuite.resource.RAMethods;
import com.opencloud.sleetck.lib.testsuite.resource.TCKMessage;
import com.opencloud.sleetck.lib.testutils.jmx.DeploymentMBeanProxy;
import com.opencloud.sleetck.lib.testutils.jmx.ServiceManagementMBeanProxy;
import com.opencloud.util.Future;

/**
 * startActivitySuspended() - The startActivitySuspended methods perform the function of the equivalent startActivity
 * method and suspendActivity method but combined into a single atomic action.
 * <p>
 * Test assertion: 1115418
 */
public class Test1115418Test extends BaseResourceTest {
    private final static int ASSERTION_ID = 1115418;

    public TCKTestResult run() throws Exception {

        // Set up a handler for returned messages.
        int sequenceID = nextMessageID();
        CustomResponseListener listener = new CustomResponseListener(sequenceID);

        listener.addExpectedResult("result-ra1");
        listener.addExpectedResult("result-sbb1");
        listener.addExpectedResult("result-sbb2");
        listener.addExpectedResult("result-token");

        // Send message to RA
        sendMessage(RAMethods.startActivitySuspended, new Integer(ASSERTION_ID), listener, sequenceID);

        // Check RA received message correctly.
        getLog().info("Checking TCK received result from RA.");
        Object result1 = listener.getResult("result-ra1");
        if (result1 == null)
            throw new TCKTestErrorException("Did not receive confirmation that RA received message from TCK.");
        checkResult(result1, ASSERTION_ID);

        // Check SBB received message correctly.
        getLog().info("Checking TCK received result from SBB 1.");
        Object result2 = listener.getResult("result-sbb1");
        if (result2 == null)
            throw new TCKTestErrorException("Did not receive confirmation that SBB 1 event processing took place.");
        checkResult(result2, ASSERTION_ID);

        // Check SBB sent token back to TCK
        getLog().info("Checking TCK received state marker token from SBB 1.");
        Object result3 = listener.getResult("result-token");
        if (result3 == null)
            throw new TCKTestErrorException("Did not receive token back from initial event processing.");
        checkResult(result3, ASSERTION_ID);

        // Check second SBB received message correctly
        getLog().info("Checking TCK received result from SBB 2.");
        Object result4 = listener.getResult("result-sbb2");
        if (result4 == null)
            throw new TCKTestFailureException(ASSERTION_ID, "Event processing for event fired on suspended activity did not occur.");
        checkResult(result4, ASSERTION_ID);

        // Check token time < sbb 2 time.
        getLog().info("Checking state marker token was received before SBB 2 result.");
        if (!(listener.getTokenTime() <= listener.getSbbTime()))
            throw new TCKTestFailureException(ASSERTION_ID, "Event processing for second activity occured while event delivery on that activity was suspended.");

        return TCKTestResult.passed();
    }

    protected class CustomResponseListener extends MultiResponseListener {

        public CustomResponseListener(int sequenceID) {
            super(sequenceID);
        }

        public boolean handleMessage(Object obj) throws RemoteException {
            getLog().info("Received message from test component: " + obj);

            if (!(obj instanceof TCKMessage)) {
                getLog().error("Unhandled message type: " + obj);
                return false;
            }
            TCKMessage message = (TCKMessage) obj;

            if (message.getSequenceID() != expectedResponse)
                return true;

            HashMap results = (HashMap) message.getArgument();
            if (results == null)
                return true;

            if (results.get("result-token") != null)
                tokenTime = System.currentTimeMillis();

            if (results.get("result-sbb2") != null)
                sbbTime = System.currentTimeMillis();

            Set keys = results.keySet();
            Iterator iter = keys.iterator();
            while (iter.hasNext()) {
                String key = (String) iter.next();
                Future future = getFuture(key);
                if (future != null) {
                    if (future.isSet())
                        continue;
                    future.setValue(results.get(key));
                }
            }

            return true;
        }

        public long getTokenTime() {
            return tokenTime;
        }

        public long getSbbTime() {
            return sbbTime;
        }

        private long tokenTime = 1;
        private long sbbTime = 0;
    }

    public void setUp() throws Exception {
        super.setUp();

        try {
            DeploymentMBeanProxy deploymentMBean = utils().getDeploymentMBeanProxy();
            ServiceManagementMBeanProxy serviceMBean = utils().getServiceManagementMBeanProxy();

            String serviceDUPath = utils().getTestParams().getProperty(DescriptionKeys.SERVICE_DU_PATH_PARAM + "2");
            String absolutePath = utils().getDeploymentUnitURL(serviceDUPath);

            getLog().info("Installing service: " + absolutePath);

            DeployableUnitID serviceDUID = deploymentMBean.install(absolutePath);
            ServiceID serviceID = getServiceID(serviceDUID);
            serviceMBean.activate(serviceID);
        } catch (Exception e) {
            getLog().error(e);
        }
    }

    public void tearDown() throws Exception {
        try {
            DeploymentMBeanProxy deploymentMBean = utils().getDeploymentMBeanProxy();

            String relativePath = utils().getTestParams().getProperty(DescriptionKeys.SERVICE_DU_PATH_PARAM + "2");
            String serviceDuPath = utils().getDeploymentUnitURL(relativePath);
            getLog().info("Uninstalling service: " + serviceDuPath);

            DeployableUnitID serviceDUID = deploymentMBean.getDeployableUnit(serviceDuPath);
            ServiceID serviceID = getServiceID(serviceDUID);
            utils().deactivateService(serviceID);
            deploymentMBean.uninstall(serviceDUID);
        } catch (Exception e) {
            getLog().error(e);
        }

        super.tearDown();
    }
}
