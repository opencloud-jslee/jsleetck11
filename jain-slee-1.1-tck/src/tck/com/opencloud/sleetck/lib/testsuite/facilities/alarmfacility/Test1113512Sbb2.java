/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.facilities.alarmfacility;

import java.util.HashMap;

import javax.naming.InitialContext;
import javax.slee.ActivityContextInterface;
import javax.slee.facilities.AlarmFacility;
import javax.slee.facilities.AlarmLevel;
import javax.slee.facilities.Tracer;

import com.opencloud.sleetck.lib.resource.events.TCKResourceEventY;
import com.opencloud.sleetck.lib.sbbutils.BaseTCKSbb;
import com.opencloud.sleetck.lib.sbbutils.TCKSbbUtils;

/*  
 *  AssertionID(1113512): Test If one or more alarms are cleared by 
 *  this method, the SLEE’s AlarmMBean object emits an alarm notification 
 *  for each cleared alarm with the alarm level set to AlarmLevel.CLEAR.
 *  
 *  AssertionID(1113588): Test This method only affects alarms belonging 
 *  to the notification source associated with the AlarmFacility object instance.
 *
 */

public abstract class Test1113512Sbb2 extends BaseTCKSbb {

    public static final String ALARM_MESSAGE = "Sbb2:Test1113512AlarmMessage";

    public static final int ACTIVE_ALARMS = 5;

    // Initial event
    public void onTCKResourceEventY1(TCKResourceEventY event, ActivityContextInterface aci) {

        try {
            tracer = this.getSbbContext().getTracer("com.test");
            tracer.info("Received " + event + " message", null);

            doTest1113512Test();

            
        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }
    }

    private void doTest1113512Test() throws Exception {

        AlarmFacility facility = getAlarmFacility();

        // 1113512
        try {
            // raise an alarm
            String[] alarmIDs = {
                    facility.raiseAlarm("javax.slee.management.Alarm6", "Test1113512AlarmInstanceID6",
                            AlarmLevel.CRITICAL, ALARM_MESSAGE),
                    facility.raiseAlarm("javax.slee.management.Alarm7", "Test1113512AlarmInstanceID7",
                            AlarmLevel.MAJOR, ALARM_MESSAGE),
                    facility.raiseAlarm("javax.slee.management.Alarm8", "Test1113512AlarmInstanceID8",
                            AlarmLevel.WARNING, ALARM_MESSAGE),
                    facility.raiseAlarm("javax.slee.management.Alarm9", "Test1113512AlarmInstanceID9",
                            AlarmLevel.INDETERMINATE, ALARM_MESSAGE),
                    facility.raiseAlarm("javax.slee.management.Alarm10", "Test1113512AlarmInstanceID10",
                            AlarmLevel.MINOR, ALARM_MESSAGE) };

            if (alarmIDs.length != ACTIVE_ALARMS) {
                sendResultToTCK("Test1113512Test", false, 1113512, "Sbb2: There should be " + alarmIDs.length + " currently active in the SLEE");
                return;
            }
        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }

    }

    private void sendResultToTCK(String testName, boolean result, int failedAssertionID,  String message) throws Exception {
        HashMap sbbData = new HashMap();
        sbbData.put("testname", testName);
        sbbData.put("result", result ? "pass" : "fail");
        sbbData.put("message", message);
        sbbData.put("id", new Integer(failedAssertionID));
        TCKSbbUtils.getResourceInterface().sendSbbMessage(sbbData);
    }

    private AlarmFacility getAlarmFacility() throws Exception {
        AlarmFacility facility = null;
        String JNDI_ALARMFACILITY_NAME = AlarmFacility.JNDI_NAME;
        try {
            facility = (AlarmFacility) new InitialContext().lookup(JNDI_ALARMFACILITY_NAME);
        } catch (Exception e) {
            tracer.warning("got unexpected Exception: " + e, null);
        }
        return facility;
    }

    private Tracer tracer;

}
