/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.events.handlerinvocation;

import com.opencloud.sleetck.lib.*;
import com.opencloud.sleetck.lib.testutils.jmx.MBeanFacade;
import com.opencloud.sleetck.lib.testutils.ExceptionsUtil;
import com.opencloud.sleetck.lib.testutils.BaseTCKResourceListener;
import com.opencloud.sleetck.lib.testutils.QueuingResourceListener;
import com.opencloud.sleetck.lib.resource.TCKActivityID;
import com.opencloud.sleetck.lib.resource.testapi.TCKResourceTestInterface;
import com.opencloud.sleetck.lib.resource.testapi.TCKResourceListener;
import com.opencloud.sleetck.lib.resource.events.TCKResourceEventX;
import com.opencloud.sleetck.lib.testutils.*;
import com.opencloud.sleetck.lib.testutils.jmx.*;
import com.opencloud.sleetck.lib.resource.TCKSbbMessage;
import java.util.Properties;
import java.rmi.RemoteException;
import javax.management.ObjectName;
import com.opencloud.logging.Logable;
import javax.slee.management.DeployableUnitID;

public class EventHandlerInvocationTest extends AbstractSleeTCKTest {

    public TCKTestResult run() throws Exception {
        QueuingResourceListener listener = new QueuingResourceListener(utils());
        setResourceListener(listener);

        TCKResourceTestInterface resource = utils().getResourceInterface();

        getLog().fine("Creating activity");
        TCKActivityID activityID = resource.createActivity(getClass().getName());

        getLog().info("Firing events");

        // Send an event of each type - and send the event type as the event message

        // Send X1

        resource.fireEvent(TCKResourceEventX.X1,TCKResourceEventX.X1,activityID,null);

        // Expect the Sbb to reply with the event type received, and the message stored
        // in the event message (i.e. the event type we fired).

        // Confirm that X1 was delivered to the right handler
        String[] messageData = (String[])listener.nextMessage().getMessage();
        if(!TCKResourceEventX.X1.equals(messageData[1])) throw new TCKTestErrorException(
            "Received unexpected event when waiting reply for "+TCKResourceEventX.X1+". Event message was: "+messageData[1]);
        Assert.assertEquals(635,"Event should have been delivered to the correct handler.",TCKResourceEventX.X1,messageData[0]);

        // Send X2
        resource.fireEvent(TCKResourceEventX.X2,TCKResourceEventX.X2,activityID,null);

        // Confirm that X2 was delivered to the right handler
        messageData = (String[])listener.nextMessage().getMessage();
        if(!TCKResourceEventX.X2.equals(messageData[1])) throw new TCKTestErrorException(
            "Received unexpected event when waiting reply for "+TCKResourceEventX.X2+". Event message was: "+messageData[1]);
        Assert.assertEquals(635,"Event should have been delivered to the correct handler.",TCKResourceEventX.X2,messageData[0]);

        return TCKTestResult.passed();
    }

}
