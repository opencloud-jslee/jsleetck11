/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.profiles.profileadded;

import com.opencloud.sleetck.lib.sbbutils.BaseTCKSbb;
import com.opencloud.sleetck.lib.sbbutils.TCKSbbUtils;
import com.opencloud.sleetck.lib.sbbutils.events.TCKSbbEvent;
import com.opencloud.sleetck.lib.sbbutils.events.TCKSbbEventImpl;
import com.opencloud.sleetck.lib.sbbutils.events2.SbbBaseMessageComposer;
import com.opencloud.sleetck.lib.sbbutils.events2.SbbBaseMessageConstants;
import com.opencloud.sleetck.lib.sbbutils.events2.SendResultEvent;

import javax.slee.profile.*;
import javax.slee.*;

import java.util.HashMap;

public abstract class Test1110016Sbb extends BaseTCKSbb {

    public static final int TYPE_UPDATE_PROFILE = 1024;

    private Object sendUpdateRequestCall (int newValue, int id) throws Exception {
        HashMap map = SbbBaseMessageComposer.getCustomMessage(TYPE_UPDATE_PROFILE,
                new String[]{"NewValue", SbbBaseMessageConstants.ID},
                new Object[]{new Integer(newValue), new Integer(id)});

        return TCKSbbUtils.getResourceInterface().callTest(map);
    }

    private void sendResult(boolean result, int id, String msg, ActivityContextInterface aci) {
        HashMap map = SbbBaseMessageComposer.getSetResultMsg(result, id, msg);
        setResult(map);
        fireSendResultEvent(new SendResultEvent(), aci, null);
    }

    public void onProfileAddedEvent(ProfileAddedEvent event, ActivityContextInterface aci) {

        try {
            ProfileEventsTestsProfileLocal addedProfile;

            //1110016, 1110017: test getAddedProfileLocal and java typecast;
            try {
                addedProfile = (ProfileEventsTestsProfileLocal) event.getAddedProfileLocal();
            } catch (ClassCastException e) {
                sendResult(false, 1110017, "The Profile Local interface returned by ProfileAddedEvent.getAddedProfileLocal() was not of type ProfileEventsTestsProfileLocal", aci);
                return;
            }

            //1110016: access the contents of the added profile
            if (addedProfile.getValue() != 42) {
                sendResult(false, 1110016, "The 'value' field in the ProfileEventsTestsProfileLocal object was not correct.", aci);
                return;
            }

            //send message to test to change profile value
            //block as otherwise transaction in which onProfileAddedEvent was called commits
            sendUpdateRequestCall(2048, 1110018);

            //1110018: check that value is still 42 as ProfileLocalObject is a snapshot of the time when profile was added
            if (addedProfile.getValue() != 42) {
                sendResult(false, 1110018, "The 'value' field in the ProfileEventsTestsProfileLocal object should represent a snapshot of the time when the profile was added and not be subject to changes done to the profile since then.", aci);
                return;
            }

            //store the obtained ProfileLocalObject in CMP field
            //1110640: An SBB component may retrieve a Profile local object from a SBB CMP field.
            try {
                setProfileLocalObject(addedProfile);
            } catch (Exception e) {
                sendResult(false, 1110640,"Attempt to store a ProfileLocal object in an SBB CMP field failed.", aci );
                return;
            }

            //get ProfileLocalObject from CMP field
            //1110640: An SBB component may retrieve a Profile local object from a SBB CMP field.
            try {
                ProfileLocalObject profileLocal = getProfileLocalObject();
                if (!addedProfile.isIdentical(profileLocal)) {
                    sendResult(false, 1110640,"Attempt to retrieve a ProfileLocal object from an SBB CMP field failed. " +
                            "Retrieved and original ProfileLocalObject are not identical (as determined by ProfileObject.isIdentical()).", aci );
                    return;
                }
            } catch (Exception e) {
                sendResult(false, 1110640,"Attempt to retrieve a ProfileLocal object from an SBB CMP field failed with exception: "+e, aci );
                return;
            }

            //sbb fires event to itself so that the current TXN can complete
            fireTCKSbbEvent(new TCKSbbEventImpl(null),aci,null);

        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }

    }

    public void onTCKSbbEvent(TCKSbbEvent event, ActivityContextInterface aci) {

        sendResult(true, 1110016, "Test successful.", aci);

    }

    public void onSendResultEvent(SendResultEvent event, ActivityContextInterface aci) {

        try {
            TCKSbbUtils.getResourceInterface().sendSbbMessage(getResult());

        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }
    }

    public abstract void fireSendResultEvent(SendResultEvent event, ActivityContextInterface aci, Address address);
    public abstract void fireTCKSbbEvent(TCKSbbEvent event, ActivityContextInterface aci, Address address);

    public abstract void setResult(HashMap map);
    public abstract HashMap getResult();

    public abstract ProfileLocalObject getProfileLocalObject();
    public abstract void setProfileLocalObject(ProfileLocalObject value);
}
