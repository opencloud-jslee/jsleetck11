/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.transactions;

import javax.slee.facilities.Level;
import javax.slee.*;

import com.opencloud.sleetck.lib.TCKTestErrorException;
import com.opencloud.sleetck.lib.sbbutils.BaseTCKSbb;
import com.opencloud.sleetck.lib.sbbutils.TCKSbbUtils;
import com.opencloud.sleetck.lib.sbbutils.TCKSbbConstants;
import com.opencloud.sleetck.lib.resource.events.TCKResourceEventX;
import com.opencloud.sleetck.lib.resource.sbbapi.TCKResourceSbbInterface;

import java.util.HashMap;
import java.util.Iterator;

public abstract class Test817Sbb extends BaseTCKSbb {

    // Initial event
    public void onTCKResourceEventX1(TCKResourceEventX ev, ActivityContextInterface aci) {

        getSbbContext().setRollbackOnly();

        HashMap map = new HashMap();
        if (getSbbContext().getRollbackOnly()) {
            map.put("Result", new Boolean(true));
            map.put("Message", "Ok");
        } else {
            map.put("Result", new Boolean(false));
            map.put("Message", "SBB called setRollbackOnly(), but the transaction was not marked for rollback.");
        }
        try {
            TCKSbbUtils.getResourceInterface().sendSbbMessage(map);
        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }

    }

    // Override sbbRolledBack to write a trace message rather than throw an Exception, as we expect to receive the call
    public void sbbRolledBack(RolledBackContext context) {
        createTraceSafe(Level.INFO,"Received expected sbbRolledBack() call");
    }

}
