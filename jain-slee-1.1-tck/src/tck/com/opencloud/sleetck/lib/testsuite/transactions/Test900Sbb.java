/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.transactions;

import java.util.HashMap;

import javax.slee.ActivityContextInterface;

import com.opencloud.sleetck.lib.resource.events.TCKResourceEventX;
import com.opencloud.sleetck.lib.sbbutils.BaseTCKSbb;
import com.opencloud.sleetck.lib.sbbutils.TCKSbbUtils;

public abstract class Test900Sbb extends BaseTCKSbb {

    public void sbbExceptionThrown(Exception e, Object event, ActivityContextInterface aci) {
    }

    public void sbbRolledBack(javax.slee.RolledBackContext context) {

        if (context.getEvent() instanceof TCKResourceEventX) {

            HashMap map = new HashMap();
            map.put("Message", "Ok");
            map.put("Result", new Boolean(true));
            try {
            TCKSbbUtils.getResourceInterface().sendSbbMessage(map);
            } catch (Exception e) {
            TCKSbbUtils.handleException(e);
            }

        } else {
            super.sbbRolledBack(context);
        }

    }

    // Initial event
    public void onTCKResourceEventX1(TCKResourceEventX ev, ActivityContextInterface aci) {
        try {
            checkTransaction();
        } catch (RuntimeException e) {
            throw e;
        }
    }

    public void checkTransaction() {
        throw new RuntimeException("Test900RuntimeException");
    }


}
