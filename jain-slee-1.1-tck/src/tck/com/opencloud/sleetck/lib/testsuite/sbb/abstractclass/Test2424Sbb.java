/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.sbb.abstractclass;

import com.opencloud.sleetck.lib.resource.events.TCKResourceEventX;
import com.opencloud.sleetck.lib.sbbutils.BaseTCKSbb;
import com.opencloud.sleetck.lib.sbbutils.TCKSbbUtils;

import javax.slee.ActivityContextInterface;
import javax.slee.Address;
import javax.slee.ChildRelation;
import javax.slee.SbbLocalObject;
import java.util.Iterator;

public abstract class Test2424Sbb extends BaseTCKSbb {

    public void onTCKResourceEventX1(TCKResourceEventX event, ActivityContextInterface aci) {
        try {
            Test2424ChildSbbLocalObject child = (Test2424ChildSbbLocalObject) getChildRelation().create();
            child.setChildMode(false);
            aci.attach(child);
            fireTest2424Event(new Test2424Event(), aci, null);
        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }
    }

    public void onTCKResourceEventX2(TCKResourceEventX event, ActivityContextInterface aci) {
        try {
            // Remove the first child.
            ChildRelation children = getChildRelation();
            Iterator iter = children.iterator();
            SbbLocalObject removeChild = (SbbLocalObject) iter.next();
            removeChild.remove();

            // Create another child of the same type.
            Test2424ChildSbbLocalObject child = (Test2424ChildSbbLocalObject) getChildRelation().create();
            child.setChildMode(true);
            aci.attach(child);
            fireTest2424Event(new Test2424Event(), aci, null);
        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }
    }

    public abstract void fireTest2424Event(Test2424Event event, ActivityContextInterface aci, Address address);

    public abstract ChildRelation getChildRelation();


}
