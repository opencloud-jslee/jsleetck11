/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.javax.slee.profile.ProfileTableActivityContextInterfaceFactory;

import javax.management.ObjectName;
import javax.slee.profile.ProfileSpecificationID;

import com.opencloud.sleetck.lib.AbstractSleeTCKTest;
import com.opencloud.sleetck.lib.DescriptionKeys;
import com.opencloud.sleetck.lib.OperationTimedOutException;
import com.opencloud.sleetck.lib.TCKTestFailureException;
import com.opencloud.sleetck.lib.TCKTestResult;
import com.opencloud.sleetck.lib.infra.SleeTCKComponentConstants;
import com.opencloud.sleetck.lib.resource.TCKSbbMessage;
import com.opencloud.sleetck.lib.sbbutils.events.TCKSbbEvent;
import com.opencloud.sleetck.lib.testsuite.profiles.ProfileTestConstants;
import com.opencloud.sleetck.lib.testutils.ComponentIDLookup;
import com.opencloud.sleetck.lib.testutils.ProfileUtils;
import com.opencloud.sleetck.lib.testutils.QueuingResourceListener;
import com.opencloud.sleetck.lib.testutils.jmx.ProfileMBeanProxy;
import com.opencloud.sleetck.lib.testutils.jmx.ProfileProvisioningMBeanProxy;

public class Test1110040Test extends AbstractSleeTCKTest{

    private static final String PROFILE_TABLE_NAME = "Test1110040ProfileTable";
    private static final String PROFILE_NAME = "Test1110040Profile";

    private static final String TCK_SBB_EVENT_DU_PATH_PARAM = "eventDUPath";

    /**
     *
     */
    public TCKTestResult run() throws Exception {

        // create the profile tables
        getLog().fine("Creating profile tables");
        ProfileSpecificationID profileSpecID = new ComponentIDLookup(utils()).lookupProfileSpecificationID(
            ProfileTestConstants.SIMPLE_PROFILE_11_SPEC_NAME,SleeTCKComponentConstants.TCK_VENDOR,"1.1");
        profileProvisioning.createProfileTable(profileSpecID,PROFILE_TABLE_NAME);

        // Add profile to the profile table, this should create a ProfileAddedEvent
        getLog().fine("Adding profile "+PROFILE_NAME+" to the profile table");
        ObjectName profile = profileProvisioning.createProfile(PROFILE_TABLE_NAME,PROFILE_NAME);
        ProfileMBeanProxy profProxy = utils().getMBeanProxyFactory().createProfileMBeanProxy(profile);
        profProxy.commitProfile();
        // wait for a ProfileAddedEvent to be received by the SBB, which try's to use lookup to obtain
        // a ProfileTableActivityContextInterfaceFactory object, then fires an Sbb event to itself
        // which in turn should send a msg to the test in form of its full class name
        try {
            TCKSbbMessage reply = resourceListener.nextMessage();
            if(!TCKSbbEvent.class.getName().equals(reply.getMessage()))
                return TCKTestResult.error("Received unexpected reply from SBB. Message="+reply.getMessage());
        } catch (OperationTimedOutException ex) {
            return TCKTestResult.failed(new TCKTestFailureException(1110040,"Timed out waiting for acknowledgement of obtaining a  " +
                    "ProfileTableActivityContextInterfaceFactory object via environment lookup.",ex));
        }

        return TCKTestResult.passed();
    }

    public void setUp() throws Exception {
        setupService(ProfileTestConstants.PROFILE_SPEC_DU_PATH_PARAM);

        setupService(TCK_SBB_EVENT_DU_PATH_PARAM);

        setupService(DescriptionKeys.SERVICE_DU_PATH_PARAM);

        // set up the resource listener
        resourceListener = new QueuingResourceListener(utils());
        setResourceListener(resourceListener);

        profileProvisioning = new ProfileUtils(utils()).getProfileProvisioningProxy();
    }

    public void tearDown() throws Exception {
        // remove profile tables
        try {
            profileProvisioning.removeProfileTable(PROFILE_TABLE_NAME);
        }
        catch (Exception e) {
            getLog().warning("Exception occured when trying to remove profile table: ");
            getLog().warning(e);
        }

        super.tearDown();
    }

    private QueuingResourceListener resourceListener;
    private ProfileProvisioningMBeanProxy profileProvisioning;
}
