/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.javax.slee.Sbb;

import java.rmi.RemoteException;
import java.util.HashMap;

import com.opencloud.sleetck.lib.AbstractSleeTCKTest;
import com.opencloud.sleetck.lib.TCKTestResult;
import com.opencloud.sleetck.lib.resource.TCKActivityID;
import com.opencloud.sleetck.lib.resource.TCKSbbMessage;
import com.opencloud.sleetck.lib.resource.events.TCKResourceEventX;
import com.opencloud.sleetck.lib.resource.testapi.TCKResourceListener;
import com.opencloud.sleetck.lib.resource.testapi.TCKResourceTestInterface;
import com.opencloud.sleetck.lib.testutils.BaseTCKResourceListener;
import com.opencloud.sleetck.lib.testutils.FutureResult;

public class Test3220Test extends AbstractSleeTCKTest {

    private static final String SERVICE_DU_PATH_PARAM = "DUPath";
    private static final int TEST_ID = 3220;

    /**
     * Perform the actual test.
     */
    public TCKTestResult run() throws Exception {
        result = new FutureResult(getLog());

        TCKResourceTestInterface resource = utils().getResourceInterface();
        TCKActivityID activityID = resource.createActivity("Test3220InitialActivity");
        resource.fireEvent(TCKResourceEventX.X1, TCKResourceEventX.X1, activityID, null);
        resource.fireEvent(TCKResourceEventX.X2, TCKResourceEventX.X2, activityID, null);

        return result.waitForResultOrFail(utils().getTestTimeout(), "Timeout waiting for child to indicate test success.", TEST_ID);
    }

    /**
     * Do all the pre-run configuration of the test.
     */
    public void setUp() throws Exception {

        resourceListener = new TCKResourceListenerImpl();
        setResourceListener(resourceListener);

        setupService(SERVICE_DU_PATH_PARAM);
    }

    private class TCKResourceListenerImpl extends BaseTCKResourceListener {
        public synchronized void onSbbMessage(TCKSbbMessage message, TCKActivityID calledActivity) throws RemoteException {

            HashMap map = (HashMap) message.getMessage();
            Boolean passed = (Boolean) map.get("Result");
            String msgString = (String) map.get("Message");

            if (passed == null) {
                txnID = map.get("TXN");
                return;
            }

            getLog().info("Received message from SBB.");

            if (txnID.equals(map.get("TXN"))) {
                result.setFailed(3221, "Transaction used for sbbRolledBack was the same as that rolled back.");
                return;
            }

            if (passed.booleanValue() == true)
                result.setPassed();
            else {
                Integer id = (Integer) map.get("ID");
                if (id == null)
                    result.setFailed(TEST_ID, msgString);
                else
                    result.setFailed(id.intValue(), msgString);
             }
        }

        public void onException(Exception e) throws RemoteException {
            getLog().warning("Received exception from SBB");
            getLog().warning(e);
            result.setError(e);
        }
    }

    private Object txnID = null;

    private TCKResourceListener resourceListener;
    private FutureResult result;
}
