/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.facilities.activitycontextnamingfacility;

import com.opencloud.sleetck.lib.*;
import com.opencloud.sleetck.lib.testutils.jmx.MBeanFacade;
import com.opencloud.sleetck.lib.testutils.ExceptionsUtil;
import com.opencloud.sleetck.lib.testutils.BaseTCKResourceListener;
import com.opencloud.sleetck.lib.resource.TCKActivityID;
import com.opencloud.sleetck.lib.resource.testapi.TCKResourceTestInterface;
import com.opencloud.sleetck.lib.resource.testapi.TCKResourceListener;
import com.opencloud.sleetck.lib.resource.events.TCKResourceEventX;
import com.opencloud.sleetck.lib.testutils.*;
import com.opencloud.sleetck.lib.testutils.jmx.*;
import com.opencloud.sleetck.lib.resource.TCKSbbMessage;
import java.util.Map;
import java.util.Properties;
import java.rmi.RemoteException;
import javax.management.ObjectName;
import com.opencloud.logging.Logable;
import javax.slee.management.DeployableUnitID;

/**
 * Tests methods of the ActivityContextNamingFacility interface.<p>
 * Use the <code>testName</code> parameter in test description files to
 * test different ACNF features.<p>
 * Currently supported values for <code>testname</code> are:
 * <li><code>jndi</code> - tests that an SBB can find the Activity Context Naming
 * Facility in its JNDI component environment.
 * <li><code>bind</code> - tests for correct behaviour of ActivityContextNamingFacility.bind()
 * method, with valid and invalid parameters.
 * <li><code>unbind</code> - tests for correct behaviour of ActivityContextNamingFacility.unbind()
 * method, with valid and invalid parameters.
 * <li><code>lookup</code> - tests for correct behaviour of ActivityContextNamingFacility.lookup()
 * method, with valid and invalid parameters.
 */
public class ActivityContextNamingFacilityTest extends AbstractSleeTCKTest {

    public void run(FutureResult result) throws Exception {
        this.result = result;
        String activityName = "ActivityContextNamingFacilityTest";
        TCKResourceTestInterface resource = utils().getResourceInterface();
        TCKActivityID activityID = resource.createActivity(activityName);
        getLog().info("Firing event: " + testName );

        // kickoff the test
        resource.fireEvent(TCKResourceEventX.X1, testName, activityID, null);
    }

    public void setUp() throws Exception {
        //super.setUp();  // will install service in serviceDuPath
        getLog().fine("Connecting to resource");
        resourceListener = new TCKResourceListenerImpl();
        setResourceListener(resourceListener);
        // testName should be one of: "jndi", "bind", "unbind" or "lookup"
        this.testName = utils().getTestParams().getProperty("testName");

        if (testName.equals("bindRefCount")) {
            // deploy custom events
            String eventDUPath = utils().getTestParams().getProperty("eventDUPath");
            utils().install(eventDUPath);
        }

        setupService(DescriptionKeys.SERVICE_DU_PATH_PARAM, true);

    }

    // Private classes

    private class TCKResourceListenerImpl extends BaseTCKResourceListener {

        // TCKResourceListener methods

        public synchronized void onSbbMessage(TCKSbbMessage message, TCKActivityID calledActivity) throws RemoteException {
            Map sbbData = (Map)message.getMessage();
            String sbbTestName = (String)sbbData.get("testname");
            String sbbTestResult = (String)sbbData.get("result");
            String sbbTestMessage = (String)sbbData.get("message");
            int assertionID = ((Integer)sbbData.get("id")).intValue();
            getLog().info("Received message from SBB: testname="+ sbbTestName+", result="+sbbTestResult+", message="+sbbTestMessage+", id="+assertionID);
            try {
                if(sbbTestName.equals(testName)) {

                    Assert.assertEquals(assertionID, "Test " + testName + " failed.", "pass", sbbTestResult);
                    result.setPassed();

                } else result.setError("Invalid response sent by SBB: "+sbbTestName);
            } catch (TCKTestFailureException ex) {
                result.setFailed(ex);
            }
        }

        public void onException(Exception exception) throws RemoteException {
            getLog().warning("Received Exception from SBB or resource:");
            getLog().warning(exception);
            result.setError(exception);
        }

    }

    // Private state

    private TCKResourceListener resourceListener;
    private FutureResult result;
    private String testName;

}