/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.profiles.checkprofilestore;

import com.opencloud.sleetck.lib.TCKTestResult;
import com.opencloud.sleetck.lib.TCKTestErrorException;
import com.opencloud.sleetck.lib.AbstractSleeTCKTest;
import com.opencloud.sleetck.lib.testsuite.profiles.ProfileTestConstants;
import com.opencloud.sleetck.lib.infra.SleeTCKComponentConstants;
import com.opencloud.sleetck.lib.testutils.jmx.ProfileProvisioningMBeanProxy;
import com.opencloud.sleetck.lib.testutils.ComponentIDLookup;
import com.opencloud.sleetck.lib.testutils.Assert;
import com.opencloud.sleetck.lib.testutils.ProfileUtils;

import javax.slee.profile.ProfileSpecificationID;
import javax.slee.profile.UnrecognizedProfileNameException;
import javax.slee.profile.UnrecognizedProfileTableNameException;
import javax.slee.profile.ProfileVerificationException;
import javax.slee.management.ManagementException;
import javax.management.ObjectName;
import java.util.Vector;
import java.util.Iterator;

/**
 * Run tests on the profileStore() callback
 */
public class CheckProfileStoreTest extends AbstractSleeTCKTest {
    private static final String TABLE_NAME = "tck.CheckStoreProfileTest.table";
    private static final String PROFILE_SPEC_NAME = "CheckStoreProfile";

    public TCKTestResult run() throws Exception {
        // create the profile table
        ProfileProvisioningMBeanProxy profileProvisioning = profileUtils.getProfileProvisioningProxy();
        ProfileSpecificationID profileSpecID = new ComponentIDLookup(utils()).lookupProfileSpecificationID(
            PROFILE_SPEC_NAME,SleeTCKComponentConstants.TCK_VENDOR,"1.0");
        getLog().info("Creating profile table: "+TABLE_NAME);
        profileProvisioning.createProfileTable(profileSpecID,TABLE_NAME);
        isTableCreated = true;

        // check that the default profile has been initialized
        ObjectName defaultProfileName = profileProvisioning.getDefaultProfile(TABLE_NAME);
        CheckStoreProfileProxy defaultProfileProxy = getProfileProxy(defaultProfileName);
        getLog().info("Checking default profile");
        String initialValue = defaultProfileProxy.getValue();
        if(initialValue == null) {
            Assert.fail(1018, "The default profile CMP fields were not initialized by profileStore()");
        }
        Assert.assertEquals(1018, "The default profile was initialized to an unexpected value (" + initialValue +
                                  ") it should have been set to " + CheckStoreProfileCMP.INITIAL_VALUE + " profileStore()",
            CheckStoreProfileCMP.INITIAL_VALUE, initialValue);

        defaultProfileProxy.editProfile();
        defaultProfileProxy.setTransientValue(CheckStoreProfileCMP.CHANGED_VALUE_1);
        defaultProfileProxy.commitProfile();

        Assert.assertEquals(1024, "Transient values were not saved to CMP fields by profileStore()",
            CheckStoreProfileCMP.CHANGED_VALUE_1, defaultProfileProxy.getValue());

        defaultProfileProxy.editProfile();
        defaultProfileProxy.setValue(CheckStoreProfileCMP.INVALID_VALUE);
        defaultProfileProxy.setTransientValue(CheckStoreProfileCMP.CHANGED_VALUE_2);
        try {
            defaultProfileProxy.commitProfile();
        } catch (ProfileVerificationException e) {
            Assert.fail(1029, "profileStore() was not called before profileVerify()");
        }

        return TCKTestResult.passed();
    }

    public void setUp() throws Exception {
        String duPath = utils().getTestParams().getProperty(ProfileTestConstants.PROFILE_SPEC_DU_PATH_PARAM);
        getLog().fine("Installing profile specification");
        utils().install(duPath);
        profileUtils = new ProfileUtils(utils());
        activeProxies = new Vector();
    }

    public void tearDown() throws Exception {
        // close profiles
        if(activeProxies != null) {
            getLog().fine("Closing profiles");
            Iterator activeProxiesIter = activeProxies.iterator();
            while(activeProxiesIter.hasNext()) {
                CheckStoreProfileProxy aProxy = (CheckStoreProfileProxy)activeProxiesIter.next();
                try {
                    if(aProxy != null) {
                        if(aProxy.isProfileWriteable()) aProxy.restoreProfile();
                        aProxy.closeProfile();
                    }
                } catch (Exception ex) {
                    getLog().warning("Exception caught while trying to close profiles:");
                    getLog().warning(ex);
                }
            }
        }
        // delete profile table
        try {
            if(profileUtils != null && isTableCreated) {
                getLog().fine("Removing profile table");
                profileUtils.removeProfileTable(TABLE_NAME);
            }
        } catch(Exception e) {
            getLog().warning("Caught exception while trying to remove profile table:");
            getLog().warning(e);
        }
        profileUtils = null;
        isTableCreated = false;
        activeProxies = null;
        super.tearDown();
    }

    private CheckStoreProfileProxy getProfileProxy(ObjectName mbeanName) throws TCKTestErrorException,
            UnrecognizedProfileNameException, UnrecognizedProfileTableNameException, ManagementException {
        CheckStoreProfileProxy rProxy = new CheckStoreProfileProxy(mbeanName,utils().getMBeanFacade());
        activeProxies.addElement(rProxy);
        return rProxy;
    }

    private ProfileUtils profileUtils;
    private Vector activeProxies;
    private boolean isTableCreated;
}























