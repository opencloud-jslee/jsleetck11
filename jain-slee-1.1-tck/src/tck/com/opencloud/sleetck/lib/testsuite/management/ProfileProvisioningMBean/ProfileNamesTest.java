/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.management.ProfileProvisioningMBean;

import com.opencloud.sleetck.lib.*;
import com.opencloud.sleetck.lib.infra.SleeTCKComponentConstants;
import com.opencloud.sleetck.lib.resource.testapi.TCKResourceListener;
import com.opencloud.sleetck.lib.testutils.BaseTCKResourceListener;
import com.opencloud.sleetck.lib.testutils.ComponentIDLookup;
import com.opencloud.sleetck.lib.testutils.ProfileUtils;
import com.opencloud.sleetck.lib.testutils.jmx.ProfileProvisioningMBeanProxy;

import javax.management.ObjectName;
import javax.slee.InvalidArgumentException;
import javax.slee.profile.ProfileSpecificationID;
import javax.slee.profile.UnrecognizedProfileNameException;
import java.rmi.RemoteException;
import java.util.Iterator;
import java.util.Vector;

/**
 * Tries to create profiles with valid and invalid names.
 */
public class ProfileNamesTest implements SleeTCKTest {

    private static final String PROFILE_DU_PATH_PARAM = "profileDUPath";
    private static final String PROFILE_TABLE_NAME = "ProfileNamesTest_ProfileTable";
    private static final int ASSERTION_ID = 2429;

    public void init(SleeTCKTestUtils utils) {
        this.utils = utils;
    }

    /**
     * Perform the actual test.
     */
    public TCKTestResult run() throws Exception {
        ProfileProvisioningMBeanProxy profileProvisioningProxy = profileUtils.getProfileProvisioningProxy();

        ProfileSpecificationID profileSpecID = new ComponentIDLookup(utils).lookupProfileSpecificationID(
                "SimpleProfile",SleeTCKComponentConstants.TCK_VENDOR,"1.0");
        if(profileSpecID == null) throw new TCKTestErrorException("No ProfileSpecification found.");

        // create a profile table
        profileProvisioningProxy.createProfileTable(profileSpecID,PROFILE_TABLE_NAME);

        // try to create a profile using null parameters
        try {
            profileProvisioningProxy.createProfile(PROFILE_TABLE_NAME,null);
            throw new TCKTestFailureException(3897,"createProfile() failed to throw the excpected NullPointerException "+
                    "with a null profile name argument");
        } catch (NullPointerException e) {
            utils.getLog().info("Caught the expected NullPointerException from createProfile() with a null profile name");
        }
        try {
            profileProvisioningProxy.createProfile(null,"validProfileName");
            throw new TCKTestFailureException(3897,"createProfile() failed to throw the excpected NullPointerException "+
                    "with a null profile table name argument");
        } catch (NullPointerException e) {
            utils.getLog().info("Caught the expected NullPointerException from createProfile() with a null profile table name");
        }

        // try to create a profile with a zero length name
        try {
            profileProvisioningProxy.createProfile(PROFILE_TABLE_NAME,"");
            throw new TCKTestFailureException(3899,"createProfile() failed to throw the excpected InvalidArgumentException "+
                    "with a zero length profile name argument");
        } catch (InvalidArgumentException e) {
            utils.getLog().info("Caught the expected InvalidArgumentException from createProfile() with a zero length profile table name");
        }

        // create a single name using the well-known valid characters: 0x0020-0x007e
        StringBuffer validNameBuf = new StringBuffer();
        for (char validChar = 0x0020; validChar < 0x007e; validChar++) {
            validNameBuf.append(validChar);
        }
        String validName = validNameBuf.toString();
        try {
            utils.getLog().info("Attempting to create a profile with the following valid name: "+validName);
            ObjectName created = profileProvisioningProxy.createProfile(PROFILE_TABLE_NAME,validName);
            utils.getMBeanProxyFactory().createProfileMBeanProxy(created).commitProfile();
            utils.getLog().info("The createProfile() method threw no Exception. Calling getProfile() to check whether the "+
                    "profile was created");
            ObjectName profileMBeanName;
            try {
                profileMBeanName = profileProvisioningProxy.getProfile(PROFILE_TABLE_NAME,validName);
            } catch (UnrecognizedProfileNameException e) {
                throw new TCKTestFailureException(ASSERTION_ID,"Failed to create a profile "+
                    "with the following valid name: "+validName+". createProfile() threw no Exception, "+
                    "but getProfile() threw an UnrecognizedProfileNameException when trying to access the profile");
            }
            if(profileMBeanName == null)
                throw new TCKTestFailureException(ASSERTION_ID,"Failed to create a profile "+
                    "with the following valid name: "+validName+". createProfile() threw no Exception, "+
                    "but getProfile() returned a null ObjectName for the named profile.");
            utils.getLog().info("getProfile() returned a non-null ObjectName: "+profileMBeanName);
        } catch (InvalidArgumentException e) {
            throw new TCKTestFailureException(ASSERTION_ID,"Caught unexpected InvalidArgumentException when trying to create a "+
                    "profile with the following valid name: "+validName,e);
        }

        // build a list of invalid characters: the first 32 unicode characters, and the delete character
        Vector invalidCharacters = new Vector();
        for (char invalidChar = 0x0000; invalidChar < 0x001f; invalidChar++) {
            invalidCharacters.addElement(new Character(invalidChar));
        }
        invalidCharacters.addElement(new Character('\u007f')); // the delete character

        Iterator invalidCharactersIter = invalidCharacters.iterator();
        while(invalidCharactersIter.hasNext()) {
            Character invalidCharacter = (Character)invalidCharactersIter.next();
            int unicodeValue = (int)invalidCharacter.charValue();
            try {
                profileProvisioningProxy.createProfile(PROFILE_TABLE_NAME,invalidCharacter.toString());
                return TCKTestResult.failed(ASSERTION_ID, "The SLEE did not throw the expected InvalidArgumentException when trying "+
                        "to create a profile with an invalid name. "+
                        "The invalid name was a single character of unicode value "+unicodeValue);
            } catch (InvalidArgumentException e) {
                utils.getLog().info("Caught expected InvalidArgumentException for an invalid profile name with a character of "+
                        "unicode value "+unicodeValue+". Error message: "+e.getMessage());
            }
        }
        return TCKTestResult.passed();
    }

    /**
     * Do all the pre-run configuration of the test.
     */
    public void setUp() throws Exception {
        utils.getLog().fine("Connecting to resource");
        resourceListener = new TCKResourceListenerImpl();
        utils.getResourceInterface().setResourceListener(resourceListener);

        // Install the Deployable Units
        utils.getLog().fine("Installing the profile spec");
        String duPath = utils.getTestParams().getProperty(PROFILE_DU_PATH_PARAM);
        utils.install(duPath);
        profileUtils = new ProfileUtils(utils);
    }

    /**
     * Clean up after the test.
     */

    public void tearDown() throws Exception {
        if (profileUtils != null) {
            try {
                profileUtils.getProfileProvisioningProxy().removeProfileTable(PROFILE_TABLE_NAME);
            } catch (Exception e) {
                utils.getLog().warning(e);
            }
        }
        utils.getLog().fine("Uninstalling the profile spec");
        utils.uninstallAll();
        utils.getLog().fine("Disconnecting from resource");
        utils.getResourceInterface().removeResourceListener();
    }

    private class TCKResourceListenerImpl extends BaseTCKResourceListener {
        public void onException(Exception e) throws RemoteException {
            utils.getLog().warning("Received exception from the TCK resource");
            utils.getLog().warning(e);
        }
    }

    private SleeTCKTestUtils utils;
    private TCKResourceListener resourceListener;
    private ProfileUtils profileUtils;

}
