/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.transactions;

import com.opencloud.sleetck.lib.sbbutils.TCKSbbUtils;
import com.opencloud.sleetck.lib.sbbutils.BaseTCKSbb;
import com.opencloud.sleetck.lib.resource.events.TCKResourceEventX;
import com.opencloud.sleetck.lib.TCKTestErrorException;

import javax.slee.ActivityContextInterface;
import javax.slee.Address;
import javax.slee.facilities.Level;
import javax.naming.NamingException;

/**
 * Define an Sbb to test the SLEE's rollback of fire event methods
 */
public abstract class Test1993Sbb extends BaseTCKSbb {

    public void onTCKResourceEventX1(TCKResourceEventX event, ActivityContextInterface aci) {
        createTrace("onTCKResourceEventX1: firing X2 event and setting rollback-only");
        fireTCKResourceEventX2(event, aci, null);
        getSbbContext().setRollbackOnly();
    }

    public void onTCKResourceEventX2(TCKResourceEventX event, ActivityContextInterface aci) {
        try {
            createTrace("onTCKResourceEventX2: received X2 event, sending response to test");
            TCKSbbUtils.getResourceInterface().sendSbbMessage("onTCKResourceEventX2 - event received");
        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }
    }

    public void createTrace(String message) {
        try {
            TCKSbbUtils.createTrace(getSbbID(), Level.INFO, message, null);
        }
        catch (TCKTestErrorException e) {}
    }

    public abstract void fireTCKResourceEventX2(TCKResourceEventX event, ActivityContextInterface aci,
                                                Address address);

    public void sbbRolledBack(javax.slee.RolledBackContext context) {
        // no-op
    }
}
