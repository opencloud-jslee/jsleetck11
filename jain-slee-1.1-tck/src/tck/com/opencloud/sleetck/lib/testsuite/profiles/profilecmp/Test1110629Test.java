/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.profiles.profilecmp;

import javax.slee.profile.ProfileSpecificationID;

import com.opencloud.sleetck.lib.AbstractSleeTCKTest;
import com.opencloud.sleetck.lib.TCKTestFailureException;
import com.opencloud.sleetck.lib.TCKTestResult;
import com.opencloud.sleetck.lib.infra.SleeTCKComponentConstants;
import com.opencloud.sleetck.lib.profileutils.BaseMessageAdapter;
import com.opencloud.sleetck.lib.rautils.RMIObjectChannel;
import com.opencloud.sleetck.lib.testutils.FutureResult;
import com.opencloud.sleetck.lib.testutils.ProfileUtils;
import com.opencloud.sleetck.lib.testutils.jmx.ProfileProvisioningMBeanProxy;

public class Test1110629Test extends AbstractSleeTCKTest {

    private static final String DU_PATH_PARAM= "DUPath";
    private static final String SPEC_NAME = "Test1110629Profile";
    private static final String SPEC_VERSION = "1.0";

    /**
     * The actual test.
     */
    public TCKTestResult run() throws Exception {

        futureResult = new FutureResult(getLog());

        //create a profile table
        //this triggers the creation of the Default profile whose 'profilePostCreate'
        //method is used to do the tests
        ProfileSpecificationID specID = new ProfileSpecificationID(SPEC_NAME, SleeTCKComponentConstants.TCK_VENDOR, SPEC_VERSION);
        profileProvisioning.createProfileTable(specID, Test1110629Profile.PROFILE_TABLE_NAME);
        getLog().fine("Added profile table "+Test1110629Profile.PROFILE_TABLE_NAME+" based on profile spec "+SPEC_NAME);

        profilecreated = true;

        return futureResult.waitForResult(utils().getTestTimeout());
    }

    public void setPassed(int assertionID, String msg) {
        getLog().fine(assertionID+": "+msg);
        if (futureResult!=null && !futureResult.isSet())
            futureResult.setPassed();
    }

    public void setFailed(int assertionID, String msg, Exception e) {
        if (e==null) {
            getLog().fine("FAILURE: "+assertionID+":"+msg);
            if (futureResult!=null && !futureResult.isSet())
                futureResult.setFailed(assertionID, msg);
        }
        else {
            getLog().fine("FAILURE: "+assertionID+":"+msg);
            getLog().fine(e);
            if (futureResult!=null && !futureResult.isSet())
                futureResult.setFailed(new TCKTestFailureException(assertionID, msg, e));
        }
    }

    public void setError(String msg, Exception e) {
        if (e==null) {
            getLog().warning(msg);
            if (futureResult!=null && !futureResult.isSet())
                futureResult.setError(msg);
        }
        else {
            getLog().warning(msg);
            getLog().warning(e);
            if (futureResult!=null && !futureResult.isSet())
                futureResult.setError(msg, e);
        }
    }


    public void setUp() throws Exception {
        setupService(DU_PATH_PARAM);

        in = utils().getRMIObjectChannel();
        in.setMessageHandler(new BaseMessageAdapter(getLog()) {
            public void onSetPassed(int assertionID, String msg) { setPassed(assertionID, msg); }
            public void onSetFailed(int assertionID, String msg, Exception e) { setFailed(assertionID, msg, e); }
            public void onLogCall(String msg) { getLog().fine(msg); }
            public void onSetError(String msg, Exception e) { setError(msg, e); }

        });

        profileUtils = new ProfileUtils(utils());
        profileProvisioning = profileUtils.getProfileProvisioningProxy();

    }

    public void tearDown() throws Exception {

        try {
            if (profilecreated)
                profileProvisioning.removeProfileTable(Test1110629Profile.PROFILE_TABLE_NAME);
        }
        catch (Exception e) {
            getLog().warning("Exception occurred when trying to remove profile tables: ");
            getLog().warning(e);
        }

        super.tearDown();
    }

    private ProfileUtils profileUtils;
    private ProfileProvisioningMBeanProxy profileProvisioning;

    private RMIObjectChannel in;
    private FutureResult futureResult;
    private boolean profilecreated = false;

}
