/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.management.ProfileProvisioningMBean;

public interface Test1649ProfileCMP {

    // Non-array primitive type
    public void setAttributeA(boolean value);
    public boolean getAttributeA();

    // Array non-primitive type
    public void setAttributeB(String [] value);
    public String [] getAttributeB();

    // Non-array non-primitive type
    public void setAttributeC(String value);
    public String getAttributeC();

    // Array primitive type
    public void setAttributeD(boolean [] value);
    public boolean [] getAttributeD();

    // Non-indexed attribute
    public void setAttributeE(boolean t);
    public boolean getAttributeE();

}
