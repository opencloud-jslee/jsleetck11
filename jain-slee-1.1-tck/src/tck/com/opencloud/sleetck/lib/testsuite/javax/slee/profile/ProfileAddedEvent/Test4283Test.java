/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.javax.slee.profile.ProfileAddedEvent;

import com.opencloud.sleetck.lib.SleeTCKTest;
import com.opencloud.sleetck.lib.SleeTCKTestUtils;
import com.opencloud.sleetck.lib.TCKTestResult;
import com.opencloud.sleetck.lib.resource.TCKActivityID;
import com.opencloud.sleetck.lib.resource.TCKSbbMessage;
import com.opencloud.sleetck.lib.resource.events.TCKResourceEventX;
import com.opencloud.sleetck.lib.resource.testapi.TCKResourceListener;
import com.opencloud.sleetck.lib.resource.testapi.TCKResourceTestInterface;
import com.opencloud.sleetck.lib.testutils.BaseTCKResourceListener;
import com.opencloud.sleetck.lib.testutils.FutureResult;
import com.opencloud.sleetck.lib.testutils.ProfileUtils;
import com.opencloud.sleetck.lib.testutils.jmx.DeploymentMBeanProxy;
import com.opencloud.sleetck.lib.testutils.jmx.ProfileMBeanProxy;
import com.opencloud.sleetck.lib.testutils.jmx.ProfileProvisioningMBeanProxy;

import javax.management.ObjectName;
import javax.slee.Address;
import javax.slee.ComponentID;
import javax.slee.management.DeployableUnitDescriptor;
import javax.slee.management.DeployableUnitID;
import javax.slee.profile.ProfileID;
import javax.slee.profile.ProfileSpecificationID;
import java.rmi.RemoteException;
import java.util.HashMap;

public class Test4283Test implements SleeTCKTest {

    private static final String SERVICE_DU_PATH_PARAM = "DUPath";
    private static final String TCK_SBB_EVENT_DU_PATH_PARAM = "eventDUPath";
    private static final int TEST_ID = 4283;

    public void init(SleeTCKTestUtils utils) {
        this.utils = utils;
    }

    /**
     * Perform the actual test.
     */

    public TCKTestResult run() throws Exception {
        result = new FutureResult(utils.getLog());

        TCKResourceTestInterface resource = utils.getResourceInterface();
        TCKActivityID activityID = resource.createActivity("Test4283InitialActivity");
        resource.fireEvent(TCKResourceEventX.X1, TCKResourceEventX.X1, activityID, null);

        return result.waitForResultOrFail(utils.getTestTimeout(), "Timeout waiting for test result.", TEST_ID);
    }

    /**
     * Do all the pre-run configuration of the test.
     */

    public void setUp() throws Exception {

        utils.getLog().fine("Connecting to resource");
        resourceListener = new TCKResourceListenerImpl();
        utils.getResourceInterface().setResourceListener(resourceListener);
        utils.getLog().fine("Installing and activating service");

        // Install the Deployable Units
        utils.getLog().fine("Installing TCKSbbEvent deployable unit");
            String eventDUPath = utils.getTestParams().getProperty(TCK_SBB_EVENT_DU_PATH_PARAM);
        utils.install(eventDUPath);

        String duPath = utils.getTestParams().getProperty(SERVICE_DU_PATH_PARAM);
        duID = utils.install(duPath);

        // Activate the DU
        utils.activateServices(duID, true);

         // Create a Profile Table
        ProfileProvisioningMBeanProxy profileProxy = new ProfileUtils(utils).getProfileProvisioningProxy();
        DeploymentMBeanProxy duProxy = utils.getDeploymentMBeanProxy();
        DeployableUnitDescriptor duDesc = duProxy.getDescriptor(duID);
        ComponentID components[] = duDesc.getComponents();
        
        for (int i = 0; i < components.length; i++) {
            if (components[i] instanceof ProfileSpecificationID) {
                ProfileSpecificationID profileSpecID = (ProfileSpecificationID) components[i];
                
                profileProxy.createProfileTable(profileSpecID, Test4283Sbb.PROFILE_TABLE_NAME);
            }
        }

    }

    /**
     * Clean up after the test.
     */

    public void tearDown() throws Exception {
        ProfileProvisioningMBeanProxy profileProxy = new ProfileUtils(utils).getProfileProvisioningProxy();
        try {
            profileProxy.removeProfile(Test4283Sbb.PROFILE_TABLE_NAME, Test4283Sbb.PROFILE_NAME);
        } catch (Exception e) {
        }
        try {
            profileProxy.removeProfileTable(Test4283Sbb.PROFILE_TABLE_NAME);
        } catch (Exception e) {
        }

        utils.getLog().fine("Disconnecting from resource");
        utils.getResourceInterface().clearActivities();
        utils.getResourceInterface().removeResourceListener();
        utils.getLog().fine("Deactivating and uninstalling service");
        utils.deactivateAllServices();
        utils.uninstallAll();
    }

    private class TCKResourceListenerImpl extends BaseTCKResourceListener {
        public synchronized void onSbbMessage(TCKSbbMessage message, TCKActivityID calledActivity) throws RemoteException {

            try {
                HashMap map = (HashMap) message.getMessage();
                Boolean passed = (Boolean) map.get("Result");
                String msgString = (String) map.get("Message");
            
                utils.getLog().info("Received message from SBB.");

                if ((map.get("Type")).equals("Result")) {
            if (passed.booleanValue() == false)
                result.setFailed(((Integer) map.get("ID")).intValue(), msgString);
            else {
                ProfileProvisioningMBeanProxy profileProxy = new ProfileUtils(utils).getProfileProvisioningProxy();
                ObjectName profile = profileProxy.getProfile(sentProfileID.getProfileTableName(), sentProfileID.getProfileName());
                if (!profile.equals(createdProfile)) {
                    result.setFailed(4286, "ProfileID retrieved from ProfileAddedEvent didn't equal that of the Profile Added.");
                } else {
                    String expectedAddressString = Test4283Sbb.PROFILE_TABLE_NAME + "/" + Test4283Sbb.PROFILE_NAME;
                    if(!sentAddress.getAddressPlan().isSleeProfile()) result.setFailed(4755,"ProfileAddedEvent.getProfileAddress() "+
                            "returned and Address with the wrong address plan:"+sentAddress.getAddressPlan());
                    else if (!expectedAddressString.equals(sentAddress.getAddressString())) result.setFailed(4754,"ProfileAddedEvent.getProfileAddress() "+
                            "returned and Address with the wrong address string. Expected:"+expectedAddressString+". Returned:"+sentAddress.getAddressString());
                    else result.setPassed();
                }
            }
            return;
                }

                if ((map.get("Type")).equals("ProfileID")) {
                    sentProfileID = (ProfileID) map.get("ProfileID");
                    return;
                }

                if ((map.get("Type")).equals("Address")) {
                    sentAddress = (Address) map.get("Address");
                    return;
                }

                // Default -- Create the Profile
                ProfileProvisioningMBeanProxy profileProxy = new ProfileUtils(utils).getProfileProvisioningProxy();
                javax.management.ObjectName profile = createdProfile = profileProxy.createProfile(Test4283Sbb.PROFILE_TABLE_NAME, Test4283Sbb.PROFILE_NAME);
                ProfileMBeanProxy profProxy = utils.getMBeanProxyFactory().createProfileMBeanProxy(profile);
                profProxy.commitProfile();
            } catch (Exception e) {
                result.setError(e);
            }
        }

        public void onException(Exception e) throws RemoteException {
            utils.getLog().warning("Received exception from SBB");
            utils.getLog().warning(e);
            result.setError(e);
        }
    }

    private SleeTCKTestUtils utils;
    private TCKResourceListener resourceListener;
    private FutureResult result;
    private DeployableUnitID duID;
    private ObjectName createdProfile;
    private ProfileID sentProfileID;
    private Address sentAddress;
}
