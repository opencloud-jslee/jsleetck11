/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.usage.sbb;

import java.rmi.RemoteException;

import com.opencloud.sleetck.lib.TCKTestFailureException;
import com.opencloud.sleetck.lib.TCKTestResult;
import com.opencloud.sleetck.lib.resource.TCKActivityID;
import com.opencloud.sleetck.lib.resource.TCKSbbMessage;
import com.opencloud.sleetck.lib.resource.events.TCKResourceEventX;
import com.opencloud.sleetck.lib.testutils.BaseTCKResourceListener;
import com.opencloud.sleetck.lib.testutils.FutureResult;

public class Test1111068Test extends BaseUsageTest {

    public static final int TEST_ID = 1111068;
    public static final String PARAM_SET_NAME = "Param set name";

    public TCKTestResult run() throws Exception {
        result = new FutureResult(getLog());

        TCKActivityID activityID = utils().getResourceInterface().createActivity("token activity");
        utils().getResourceInterface().fireEvent(TCKResourceEventX.X1,null,activityID,null);
        return result.waitForResult(5000);
    }

    public void setUp() throws Exception {
        super.setUp();

        setResourceListener(new TCKResourceListenerImpl());
    }

    public void tearDown() throws Exception {
        super.tearDown();
        utils().getResourceInterface().removeResourceListener();
    }

    private class TCKResourceListenerImpl extends BaseTCKResourceListener {

        public synchronized void onSbbMessage(TCKSbbMessage message, TCKActivityID activity) throws RemoteException {
            utils().getLog().fine("Received message from SBB: " + message.getMessage());
            String sbbMessage = (String) message.getMessage();
            if (sbbMessage.equals("passed")) result.setPassed();
            else result.setFailed(new TCKTestFailureException(TEST_ID, sbbMessage));
        }

        public void onException(Exception e) throws RemoteException {
            utils().getLog().warning("Received exception from SBB.");
            utils().getLog().warning(e);
            result.setError(e);
        }
    }

    private FutureResult result;
}