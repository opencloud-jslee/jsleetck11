/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.management.ProfileProvisioningMBean;

import com.opencloud.sleetck.lib.SleeTCKTest;
import com.opencloud.sleetck.lib.SleeTCKTestUtils;
import com.opencloud.sleetck.lib.TCKTestResult;
import com.opencloud.sleetck.lib.resource.TCKActivityID;
import com.opencloud.sleetck.lib.resource.TCKSbbMessage;
import com.opencloud.sleetck.lib.resource.testapi.TCKResourceListener;
import com.opencloud.sleetck.lib.testutils.BaseTCKResourceListener;
import com.opencloud.sleetck.lib.testutils.FutureResult;
import com.opencloud.sleetck.lib.testutils.ProfileUtils;
import com.opencloud.sleetck.lib.testutils.jmx.DeploymentMBeanProxy;
import com.opencloud.sleetck.lib.testutils.jmx.ProfileMBeanProxy;
import com.opencloud.sleetck.lib.testutils.jmx.ProfileProvisioningMBeanProxy;

import javax.management.ObjectName;
import javax.slee.ComponentID;
import javax.slee.management.DeployableUnitDescriptor;
import javax.slee.management.DeployableUnitID;
import javax.slee.profile.ProfileSpecificationID;
import javax.slee.profile.UnrecognizedProfileTableNameException;
import java.rmi.RemoteException;
import java.util.Collection;
import java.util.HashMap;

public class Test1647Test implements SleeTCKTest {

    private static final String SERVICE_DU_PATH_PARAM = "DUPath";
    private static final int TEST_ID = 1647;

    public void init(SleeTCKTestUtils utils) {
        this.utils = utils;
    }

    /**
     * Perform the actual test.
     */

    public TCKTestResult run() throws Exception {

        ProfileProvisioningMBeanProxy profileProxy = profileUtils.getProfileProvisioningProxy();
        DeploymentMBeanProxy duProxy = utils.getDeploymentMBeanProxy();
        DeployableUnitDescriptor duDesc = duProxy.getDescriptor(duID);
        ComponentID components[] = duDesc.getComponents();


        for (int i = 0; i < components.length; i++) {
            if (components[i] instanceof ProfileSpecificationID) {
                ProfileSpecificationID profileSpecID = (ProfileSpecificationID) components[i];

                // Create the profile table.
                try {
                    profileProxy.createProfileTable(profileSpecID, "Test1647ProfileTable");
                    ObjectName profile = profileProxy.createProfile("Test1647ProfileTable", "Test1647Profile");
                    // Commit the created profile.
                    ProfileMBeanProxy proxy = utils.getMBeanProxyFactory().createProfileMBeanProxy(profile);
                    proxy.commitProfile();

                } catch (Exception e) {
                    return TCKTestResult.error("Failed to create profile table.");
                }

                // Perform Test for assertion 1648.
                Collection profiles = profileProxy.getProfiles("Test1647ProfileTable");
                if (profiles.size() != 1) {
                    return TCKTestResult.failed(1648, "Incorrect number of profiles in Collection object.");
                }

                Collection profileTables = profileProxy.getProfileTables();
                String profileTableName = (String) profileTables.iterator().next();
                if (profileTables.size() != 1 || !profileTableName.equals("Test1647ProfileTable")) {
                    return TCKTestResult.failed(1647, "Incorrect number of profile tables.");
                }

                // Assertion
                try {
                    profileProxy.getProfiles("Test1647Foo");
                } catch (UnrecognizedProfileTableNameException e) {
                    return TCKTestResult.passed();
                }

                return TCKTestResult.failed(1660, "UnrecognizedProfileTableNameException was not thrown as expected by call to ProfileProvisioningMBean.getProfiles(String profileTableName)");
            }
        }

        return TCKTestResult.error("No ProfileSpecification found.");
    }

    /**
     * Do all the pre-run configuration of the test.
     */

    public void setUp() throws Exception {

        utils.getLog().fine("Connecting to resource");
        resourceListener = new TCKResourceListenerImpl();
        utils.getResourceInterface().setResourceListener(resourceListener);
        utils.getLog().fine("Installing and activating service");

        profileUtils = new ProfileUtils(utils);

        // Install the Deployable Units
        String duPath = utils.getTestParams().getProperty(SERVICE_DU_PATH_PARAM);
        duID = utils.install(duPath);
        utils.activateServices(duID, true);

    }

    /**
     * Clean up after the test.
     */

    public void tearDown() throws Exception {

        ProfileProvisioningMBeanProxy profileProxy = profileUtils.getProfileProvisioningProxy();
        try {
            profileProxy.removeProfile("Test1647ProfileTable", "Test1647Profile");
        } catch (Exception e) {
        }
        try {
            profileProxy.removeProfileTable("Test1647ProfileTable");
        } catch (Exception e) {
        }

        utils.getLog().fine("Disconnecting from resource");
        utils.getResourceInterface().clearActivities();
        utils.getResourceInterface().removeResourceListener();
        utils.getLog().fine("Deactivating and uninstalling service");
        utils.deactivateAllServices();
        utils.uninstallAll();
    }

    private class TCKResourceListenerImpl extends BaseTCKResourceListener {
        public synchronized void onSbbMessage(TCKSbbMessage message, TCKActivityID calledActivity) throws RemoteException {
            utils.getLog().info("Received message from SBB");

            HashMap map = (HashMap) message.getMessage();
            Boolean passed = (Boolean) map.get("Result");
            String msgString = (String) map.get("Message");

            if (passed.booleanValue() == true)
                result.setPassed();
            else
                result.setFailed(TEST_ID, msgString);
        }

        public void onException(Exception e) throws RemoteException {
            utils.getLog().warning("Received exception from SBB");
            utils.getLog().warning(e);
            result.setError(e);
        }
    }

    private SleeTCKTestUtils utils;
    private TCKResourceListener resourceListener;
    private FutureResult result;
    private DeployableUnitID duID;
    private ProfileUtils profileUtils;
}
