/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.sbb.activitycontext;

import java.util.HashMap;

import javax.naming.InitialContext;
import javax.slee.ActivityContextInterface;
import javax.slee.EventContext;
import javax.slee.NoSuchObjectLocalException;
import javax.slee.SLEEException;
import javax.slee.TransactionRequiredLocalException;
import javax.slee.profile.AddressProfileLocal;
import javax.slee.profile.ProfileFacility;
import javax.slee.profile.ProfileLocalObject;
import javax.slee.profile.ProfileTable;
import javax.slee.profile.ReadOnlyProfileException;

import com.opencloud.sleetck.lib.resource.events.TCKResourceEventX;
import com.opencloud.sleetck.lib.sbbutils.BaseTCKSbb;
import com.opencloud.sleetck.lib.sbbutils.TCKSbbUtils;

public abstract class Test1106033Sbb extends BaseTCKSbb {

    /*
     * This is the initial event.
     * 
     * Store the value of a profile in my CMP field. 
     */
    public void onTCKResourceEventX1(TCKResourceEventX ev, ActivityContextInterface aci, EventContext eventContext) {
        HashMap map;
        ProfileLocalObject myProfile;

        ProfileFacility facility;
        ProfileTable pt;

        map = new HashMap();

        // Try the positive case.
        try {
            facility = (ProfileFacility) new InitialContext().lookup(ProfileFacility.JNDI_NAME);
            pt = facility.getProfileTable(Test1106032Test.PROFILE_TABLE_NAME);
            myProfile = (AddressProfileLocal)pt.find(Test1106032Test.PROFILE_NAME);

            setMyProfile(myProfile);
            if (!myProfile.equals(getMyProfile())) {
                map.put("Result", new Boolean(false));
                map.put("Message", "Profile set and gotten from CMP field was not equal to itself");
                sendSbbMessage(map);
            }
        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }

        // Try the null case.
        setMyProfile(null);
        if (null != getMyProfile()) {
            map.put("Result", new Boolean(false));
            map.put("Message", "My profile CMP field was not null after setting it to null.");
            sendSbbMessage(map);
            return;
        }            
        
        // Try the illegal argument case.
        myProfile = new MyProfile(); 
        try {
            setMyProfile(myProfile);
        } catch (IllegalArgumentException e) {
            map.put("Result", new Boolean(true));
            map.put("Message", "IllegalArgumentException thrown as expected.");
            sendSbbMessage(map);
            return;
        }

        map.put("Result", new Boolean(false));
        map.put("Message", "IllegalArgumentException was not thrown.");
        sendSbbMessage(map);
        return;

    }


    public abstract void setMyProfile(ProfileLocalObject object);
    public abstract ProfileLocalObject getMyProfile();

    private void sendSbbMessage(HashMap map) {
        try {
            TCKSbbUtils.getResourceInterface().sendSbbMessage(map);
        } catch(Exception e) {
            TCKSbbUtils.handleException(e);
        }
    }
    
    private class MyProfile implements ProfileLocalObject {
        /**
         * Compare this <code>ProfileLocalObject</code> for identity equality with another.
         * <p>
         * This method is a non-transactional method.
         * @param obj the object to compare this with.
         * @return <code>true</code> if <code>obj</code> is a reference to a profile with the
         *        same identity (profile table name and profile name) as the profile referenced
         *        by this <code>ProfileLocalObject</code> object, <code>false</code> otherwise.
         * @throws SLEEException if the equality test could not be completed due to a
         *        system-level failure.
         */
        public boolean isIdentical(ProfileLocalObject obj)
            throws SLEEException {
            return false;
        }

        /**
         * Get the name of the profile table in which the profile referenced by this
         * <code>ProfileLocalObject</code> exists.
         * <p>
         * This method is a non-transactional method.
         * @return the profile table name.
         * @throws SLEEException if the profile table name could not be obtained due to a
         *        system-level failure.
         */
        public String getProfileTableName()
            throws SLEEException {
            return "";
        }

        /**
         * Get the name of the profile referenced by this <code>ProfileLocalObject</code>.
         * <p>
         * This method is a non-transactional method.
         * @return the profile name, or <code>null</code> if the Profile object is associated
         *        with the profile table's default profile.
         * @throws SLEEException if the profile name could not be obtained due to a
         *        system-level failure.
         */
        public String getProfileName()
            throws SLEEException {
            return "";
        }

        /**
         * Get an object that implements the Profile Table Interface of the profile table in
         * which the profile that this <code>ProfileLocalObject</code> references exists.  If
         * the profile specification of the profile table has defined a Profile Table Interface
         * that extends {@link ProfileTable} then the object returned from this method may be
         * safely typecast to the subinterface declared in the Profile Specification.
         * <p>
         * This method is a non-transactional method.
         * @return a Profile Table Interface object.
         * @throws SLEEException if the Profile Table Interface object could not be obtained
         *        due to a system-level failure.
         */
        public ProfileTable getProfileTable()
            throws SLEEException {
            return null;
        }

        /**
         * Remove the profile referenced by this <code>ProfileLocalObject</code>.
         * <p>
         * This method is a mandatory transactional method.
         * @throws TransactionRequiredLocalException if this method is invoked without a valid
         *        transaction context.
         * @throws NoSuchObjectLocalException if the profile referenced by this <code>ProfileLocalObject</code>
         *        is no longer valid.
         * @throws ReadOnlyProfileException if the profile table's profile specification has
         *        enforced a read-only SLEE component view of profiles.
         * @throws SLEEException if the SBB entity could not be removed due to a system-level
         *        failure.
         */
        public void remove()
            throws TransactionRequiredLocalException, NoSuchObjectLocalException,
                   ReadOnlyProfileException, SLEEException {}
    }
    
}











