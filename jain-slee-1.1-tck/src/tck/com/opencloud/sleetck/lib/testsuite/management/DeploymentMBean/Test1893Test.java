/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.management.DeploymentMBean;

import com.opencloud.sleetck.lib.AbstractSleeTCKTest;
import com.opencloud.sleetck.lib.TCKTestResult;
import com.opencloud.sleetck.lib.infra.SleeTCKComponentConstants;
import com.opencloud.sleetck.lib.testutils.Assert;

import javax.slee.management.DeployableUnitID;
import javax.slee.management.UnrecognizedDeployableUnitException;
import javax.slee.management.ComponentDescriptor;

/**
 * Test assertions 1892/1893: That a deployable unit is only installed if all included components install successfully.
 * The test attempts to install a deployable unit that contains some valid components and some invalid components in
 * the same deployable unit.  The SLEE should not install any of the components.
 */
public class Test1893Test extends AbstractSleeTCKTest {

    private static final String SERVICE_DU_PATH_PARAM = "DUPath";

    public TCKTestResult run() throws Exception {
        String duPath = utils().getTestParams().getProperty(SERVICE_DU_PATH_PARAM);
        duPath = utils().getDeploymentUnitURL(duPath);
        try {
            duID = utils().getDeploymentMBeanProxy().install(duPath);
            Assert.fail(1892, "Expected javax.slee.management.DeploymentException not thrown when installing a " +
                              "deployable unit that contains invalid components");
        } catch (javax.slee.management.DeploymentException e) {
            getLog().info("Got expected javax.slee.management.DeploymentException");
        }

        try {
            utils().getDeploymentMBeanProxy().getDeployableUnit(duPath);
            Assert.fail(1892, "Expected javax.slee.management.UnrecognizedDeployableUnitException not thrown when " +
                              "getting DeployableUnitID for a deployable unnit that was not installed");
        } catch (UnrecognizedDeployableUnitException e) {
            getLog().info("Got expected javax.slee.management.UnrecognizedDeployableUnitException");
        }

        // Check that none of the components from the deployable unit remain in the SLEE
        Assert.assertTrue(1893, "Some components from a deployable unit that failed to install correctly remain installed " +
                                "in the SLEE",
                          !isInstalled("Test1893Profile", SleeTCKComponentConstants.TCK_VENDOR, "1.0",
                                       utils().getDeploymentMBeanProxy().getDescriptors(
                                            utils().getDeploymentMBeanProxy().getProfileSpecifications()))
                          &&
                          !isInstalled("com.opencloud.sleetck.lib.management.DeploymentMBean.Test1893Event", SleeTCKComponentConstants.TCK_VENDOR, "1.0",
                                       utils().getDeploymentMBeanProxy().getDescriptors(
                                            utils().getDeploymentMBeanProxy().getEventTypes()))
                          &&
                          !isInstalled("Test1893Service", SleeTCKComponentConstants.TCK_VENDOR, "1.0",
                                       utils().getDeploymentMBeanProxy().getDescriptors(
                                            utils().getDeploymentMBeanProxy().getServices()))
        );

        return TCKTestResult.passed();
    }

    /**
     * Do all the pre-run configuration of the test.
     */
    public void setUp() throws Exception {
    }

    /**
     * Clean up after the test.
     */
    public void tearDown() throws Exception {
        // uninstall the DU if for some reason it did get installed
        if (null != duID) {
            utils().getDeploymentMBeanProxy().uninstall(duID);
        }

    }

    private boolean isInstalled(String name, String vendor, String version, ComponentDescriptor[] components) {
        for (int i = 0; i < components.length; i++) {
            ComponentDescriptor component = components[i];
            if (component.getName().equals(name) && component.getVendor().equals(vendor)
                && component.getVersion().equals(version))
                return true;
        }
        return false;
    }

    private DeployableUnitID duID = null;
}
