/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.facilities.alarmfacility;

import java.rmi.RemoteException;
import java.util.Map;

import javax.management.Notification;
import javax.management.NotificationListener;
import javax.slee.ComponentID;
import javax.slee.SbbID;
import javax.slee.ServiceID;
import javax.slee.management.AlarmNotification;
import javax.slee.management.DeployableUnitDescriptor;
import javax.slee.management.DeployableUnitID;
import javax.slee.management.SbbNotification;

import com.opencloud.sleetck.lib.AbstractSleeTCKTest;
import com.opencloud.sleetck.lib.DescriptionKeys;
import com.opencloud.sleetck.lib.TCKTestErrorException;
import com.opencloud.sleetck.lib.TCKTestFailureException;
import com.opencloud.sleetck.lib.resource.TCKActivityID;
import com.opencloud.sleetck.lib.resource.TCKSbbMessage;
import com.opencloud.sleetck.lib.resource.events.TCKResourceEventX;
import com.opencloud.sleetck.lib.resource.testapi.TCKResourceListener;
import com.opencloud.sleetck.lib.resource.testapi.TCKResourceTestInterface;
import com.opencloud.sleetck.lib.testutils.Assert;
import com.opencloud.sleetck.lib.testutils.BaseTCKResourceListener;
import com.opencloud.sleetck.lib.testutils.FutureResult;
import com.opencloud.sleetck.lib.testutils.jmx.AlarmMBeanProxy;
import com.opencloud.sleetck.lib.testutils.jmx.DeploymentMBeanProxy;

/*  AssertionID(1113474): Test An alarm will be raised if there 
 * is no existing alarm with matching identifying attributes raised.
 * 
 *  AssertionID(1113476): Test When an alarm is raised an alarm 
 *  notification is emitted by the Alarm Mbean.
 *    
 */

public class Test1113474Test extends AbstractSleeTCKTest {

    /**
     * Perform the actual test.
     */
    public void run(FutureResult result) throws Exception {
        this.result = result;
        
        receivedAlarmNotifications = 0;
        expectedAlarmNotifications = 3;

        if (sbbID == null)
            throw new TCKTestErrorException("sbbID not found for " + DescriptionKeys.SERVICE_DU_PATH_PARAM);
        if (serviceID == null)
            throw new TCKTestErrorException("serviceID not found for " + DescriptionKeys.SERVICE_DU_PATH_PARAM);

        SbbNotification sbbNotification = new SbbNotification(serviceID, sbbID);
        try {
            getLog().fine(
                    "Starting to get all existing unique alarm identifiers for stateful alarms "
                            + "currently active in the SLEE that were raised by the specified notification "
                            + "source.");
            alarmIDs = alarmMBeanProxy.getAlarms(sbbNotification);
            if (alarmIDs.length > 0) {
                result.setFailed(1113474, "There are some existing alarms which matching"
                        + "SbbNotification attributes have arelady been raised!");
                return;
            }

        } catch (Exception e) {
            getLog().warning(e);
            result.setError("ERROR!", e);
        }

        String activityName = "Test1113474Test";
        TCKResourceTestInterface resource = utils().getResourceInterface();
        TCKActivityID activityID = resource.createActivity(activityName);
        getLog().info("Firing event: " + TCKResourceEventX.X1);

        // kickoff the test
        resource.fireEvent(TCKResourceEventX.X1, testName, activityID, null);
        
        synchronized (this) {
            wait(utils().getTestTimeout());
        }

        Assert.assertTrue(1113438, "Expected number of Alarm messages not received, Alarms were not delivered by "
                + "the AlarmFacility (expected " + expectedAlarmNotifications + ", received "
                + receivedAlarmNotifications + ")", expectedAlarmNotifications == receivedAlarmNotifications);

        result.setPassed();
    }

    /**
     * Do all the pre-run configuration of the test.
     */

    public void setUp() throws Exception {

        getLog().fine("Connecting to resource");
        resourceListener = new TCKResourceListenerImpl();
        setResourceListener(resourceListener);
        getLog().fine("Installing and activating service");

        // Install the Deployable Units
        String duPath = utils().getTestParams().getProperty(DescriptionKeys.SERVICE_DU_PATH_PARAM);
        duID = utils().install(duPath);

        // Activate the DU
        utils().activateServices(duID, true);

        alarmMBeanProxy = utils().getAlarmMBeanProxy();
        listener = new AlarmNotificationListenerImpl();
        alarmMBeanProxy.addNotificationListener(listener, null, null);

        DeploymentMBeanProxy duProxy = utils().getDeploymentMBeanProxy();
        DeployableUnitDescriptor duDesc = duProxy.getDescriptor(duID);
        ComponentID components[] = duDesc.getComponents();
        // Get the SbbID from the components.
        for (int i = 0; i < components.length; i++) {
            if (components[i] instanceof ServiceID) {
                getLog().fine("Setting serviceID value.");
                serviceID = (ServiceID) components[i];
                continue;
            }

            if (components[i] instanceof SbbID) {
                getLog().fine("Setting sbbID value.");
                sbbID = (SbbID) components[i];
                continue;
            }
        }

    }

    /**
     * Clean up after the test.
     */

    public void tearDown() throws Exception {

        String[] listActiveAlarms = alarmMBeanProxy.getAlarms();
        if (alarmMBeanProxy.getAlarms().length > 0)
            for (int i = 0; i < listActiveAlarms.length; i++)
                alarmMBeanProxy.clearAlarm(listActiveAlarms[i]);

        if (null != alarmMBeanProxy)
            alarmMBeanProxy.removeNotificationListener(listener);
        // cleanup
        super.tearDown();
    }

    private class TCKResourceListenerImpl extends BaseTCKResourceListener {
        // TCKResourceListener methods

        public synchronized void onSbbMessage(TCKSbbMessage message, TCKActivityID calledActivity)
                throws RemoteException {

            Map sbbData = (Map) message.getMessage();
            String sbbTestName = "Test1113474Test";
            String sbbTestResult = (String) sbbData.get("result");
            String sbbTestMessage = (String) sbbData.get("message");
            int assertionID = ((Integer) sbbData.get("id")).intValue();
            getLog().info(
                    "Received message from SBB: testname=" + sbbTestName + ", result=" + sbbTestResult + ", message="
                            + sbbTestMessage + ", id=" + assertionID);
            try {
                Assert.assertEquals(assertionID, "Test " + sbbTestName + " failed.", "pass", sbbTestResult);
            } catch (TCKTestFailureException ex) {
                result.setFailed(assertionID, sbbTestMessage);
            }
        }

        public void onException(Exception exception) throws RemoteException {
            getLog().warning("Received Exception from SBB or resource:");
            getLog().warning(exception);
            result.setError(exception);
        }
    }

    public class AlarmNotificationListenerImpl implements NotificationListener {
        public final void handleNotification(Notification notification, Object handback) {

            if (notification instanceof AlarmNotification) {
                AlarmNotification alarmNotification = (AlarmNotification) notification;
                getLog().debug(alarmNotification.toString());
                
                if (alarmNotification.getMessage().equals(Test1113474Sbb.ALARM_MESSAGE))
                    receivedAlarmNotifications++;
                else
                    result.setFailed(1113476,
                            "When an alarm is raised, an alarm notification didn't emit by the Alarm Mbean.");
                return;
            }

            getLog().info("Notification received: " + notification);
            return;
        }
    }

    private TCKResourceListener resourceListener;

    private NotificationListener listener;

    private DeployableUnitID duID;

    private AlarmMBeanProxy alarmMBeanProxy;

    private FutureResult result;

    private SbbID sbbID;

    private ServiceID serviceID;

    private String[] alarmIDs;

    private String testName = "Test1113474";
    
    private int expectedAlarmNotifications;

    private int receivedAlarmNotifications;
}
