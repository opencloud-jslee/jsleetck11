/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.profiles.readonly;

/**
 * This test is more or less a duplicate of Test1110068, the only difference is
 * that it is using a profile spec with 'profile-read-only' attribute not set
 * at all which means it should default to 'True' and show the same behaviour as
 * Test1110068.
 */
public class Test1110067Test extends ReadOnlyIsTrueBaseTest {

    private static final String PROFILE_SPEC_NAME = "Test1110067Profile";

    public String getSpecName() {

        return PROFILE_SPEC_NAME;
    }

}
