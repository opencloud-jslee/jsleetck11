/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.transactions;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;

import javax.naming.NamingException;
import javax.slee.transaction.CommitListener;
import javax.slee.transaction.SleeTransaction;
import javax.slee.transaction.SleeTransactionManager;
import javax.transaction.HeuristicMixedException;
import javax.transaction.HeuristicRollbackException;
import javax.transaction.RollbackException;
import javax.transaction.Status;
import javax.transaction.SystemException;

import com.opencloud.logging.StdErrLog;
import com.opencloud.sleetck.lib.profileutils.BaseMessageSender;
import com.opencloud.sleetck.lib.rautils.MessageHandler;
import com.opencloud.sleetck.lib.rautils.RMIObjectChannel;
import com.opencloud.sleetck.lib.rautils.TCKRAUtils;
import com.opencloud.util.Future;
import com.opencloud.util.Future.TimeoutException;

/**
 * Provides callback method for handling messages sent from the TCK test to the RA.
 * The actual test code is located in the 'handleMessage()' method.
 */
public class Test1109238MessageListener extends UnicastRemoteObject implements MessageHandler {

    public Test1109238MessageListener(Test1109238ResourceAdaptor ra) throws RemoteException {
        super();
        try {
            out = TCKRAUtils.lookupRMIObjectChannel();
            msgSender = new BaseMessageSender(out, new StdErrLog());
        }
        catch (Exception e) {
            ra.getLog().warning("Exception occurred when trying to acquire RMIObjectChannel: ",e);
        }
        this.ra = ra;
    }

    public boolean handleMessage(Object message) throws RemoteException {
        SleeTransactionManager txManager = null;
        SleeTransaction txn;
        CommitListener listener;
        String result;
        Future future;

        try {
            future = new Future();

            try {
                txManager = ra.getTransactionManager();
            } catch (NamingException e) {
                msgSender.sendError("Could not look up transaction manager.", e);
                return true;
            }

            txn = txManager.beginSleeTransaction();
            //1109345: If the asyncCommit() method's argument is null, no callbacks will be made by the SLEE.
            //can only test here that 'null' does not cause any exceptions when provided as a parameter
            try {
                txManager.asyncCommit(null);
                msgSender.sendLogMsg("asyncCommit(null) did not cause any exceptions as expected.");
            } catch (Exception e) {
                msgSender.sendFailure(1109345, "Exception occured when calling TXNManager.asyncCommit(null).");
                return true;
            }

            txn = txManager.beginSleeTransaction();
            listener = new MyCommitListener(future);
            txn.asyncCommit(listener);

            // any inserted code here could run before the CommitListener is
            // used, but we cannot test for this - a valid scenario
            // is where the transactions commits immediately.

            // 1109239: after asyncCommit, there is no transaction associated with the current thread.
            if (null != txManager.getSleeTransaction()) {
                msgSender.sendFailure(1109239, "There is still a transaction associated with this thread.");
                return false;
            }

            // 1109244: java.lang.IllegalStateException if a transaction is not currently associated with the calling thread.
            try {
                txManager.asyncCommit(null);
                msgSender.sendFailure(1109244, "SleeTransactionManager.asyncCommit() did not throw IllegalStateException");
                return true;
            } catch (IllegalStateException e) {
                msgSender.sendLogMsg("asyncCommit() threw IllegalStateException as expected when called while no TXN is active.");
            }

            //1109243: listener object that will receive callbacks depending on the final outcome of the transaction.
            //wait for the CommitListener callbacks to be called
            try {
                result = (String)future.getValue(5000);
            } catch (TimeoutException e) {
                msgSender.sendFailure(1109243, "Timout occured while waiting for the CommitListner callbacks to be called.",e);
                return true;
            }
            if ("committed".equals(result))
                msgSender.sendLogMsg("CommitListener.committed() was called as expected.");
            else {
                msgSender.sendFailure(1109243, "The CommitListener's committed() method shoud have been called " +
                        "but instead the "+result+"() method was called.");
                return true;
            }


            //1109241: At some point after calling asyncCommit, the transaction state will become either Status.STATUS_COMMITTED or Status.STATUS_ROLLEDBACK.
            //here we assume that 'at some point' is 'after the CommitListener's committed() method has been called
            if (txn.getStatus()!=Status.STATUS_COMMITTED) {
                msgSender.sendFailure(1109241, "Transaction status is not Status.STATUS_COMITTED.");
                return true;
            } else
                msgSender.sendLogMsg("At some point after calling asyncCommit() the TXN status was " +
                        "Status.STATUS_COMITTED as expected.");



            txn = txManager.beginSleeTransaction();
            txManager.setRollbackOnly();
            future = new Future();
            listener = new MyCommitListener(future);
            txManager.asyncCommit(listener);

            //1109243: listener object that will receive callbacks depending on the final outcome of the transaction.
            //wait for the CommitListener callbacks to be called
            try {
                result = (String)future.getValue(5000);
            } catch (TimeoutException e) {
                msgSender.sendFailure(1109243, "Timout occured while waiting for the CommitListner callbacks to be called.",e);
                return true;
            }
            if ("rolledBack".equals(result))
                msgSender.sendLogMsg("CommitListener.rolledBack() was called as expected.");
            else {
                msgSender.sendFailure(1109243, "The CommitListener's rolledBack() method shoud have been called " +
                        "but instead the "+result+"() method was executed.");
                return true;
            }

            //1109241: At some point after calling asyncCommit, the transaction state will become either Status.STATUS_COMMITTED or Status.STATUS_ROLLEDBACK.
            //here we assume that 'at some point' is 'after the CommitListener's committed() method has been called
            //1109240: If the transaction has been marked for rollback, or if execution of
            //any Synchronization.beforeCompletion() callbacks cause it to be marked for rollback,
            //then a rollback will be started (equivalent to calling asyncRollback(javax.slee.transaction.RollbackListener)).
            if (txn.getStatus()!=Status.STATUS_ROLLEDBACK) {
                msgSender.sendFailure(1109240, "Outcome of the TXN commit should have been 'rolledBack'.");
                return true;
            } else
                msgSender.sendLogMsg("Outcome of the TXN commit was 'rolledBack' as expected.");


            msgSender.sendSuccess(1109238, "Test completed successfully.");

        } catch (Exception e) {
            msgSender.sendException(e);
            try {
                if (null != txManager.getSleeTransaction())
                    txManager.getSleeTransaction().rollback();
            } catch (SystemException e2) {
                msgSender.sendLogMsg("Exception occured when trying to safely rollback leftover TXN, this is however not the immediate cause for" +
                        " an eventual failure of the test: "+e2);
            }
        }
        return true;
    }

    public class MyCommitListener implements CommitListener {

        public MyCommitListener(Future future) {
            this.future = future;
        }

        public void committed() {
            future.setValue("committed");
        }

        public void heuristicMixed(HeuristicMixedException hme) {
            future.setValue("heuristicMixed");
        }

        public void heuristicRollback(HeuristicRollbackException hrbe) {
            future.setValue("heuristicRollback");
        }

        public void rolledBack(RollbackException rbe) {
            future.setValue("rolledBack");
        }

        public void systemException(SystemException se) {
            future.setValue("systemException");
        }

        private Future future;
    }

    private Test1109238ResourceAdaptor ra;
    private BaseMessageSender msgSender;
    private RMIObjectChannel out;
}

