/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.facilities.alarmfacility;

import java.rmi.RemoteException;
import java.util.Map;

import javax.management.Notification;
import javax.management.NotificationListener;
import javax.slee.facilities.AlarmLevel;
import javax.slee.management.AlarmNotification;

import com.opencloud.sleetck.lib.AbstractSleeTCKTest;
import com.opencloud.sleetck.lib.TCKTestFailureException;
import com.opencloud.sleetck.lib.resource.TCKActivityID;
import com.opencloud.sleetck.lib.resource.TCKSbbMessage;
import com.opencloud.sleetck.lib.resource.events.TCKResourceEventX;
import com.opencloud.sleetck.lib.resource.events.TCKResourceEventY;
import com.opencloud.sleetck.lib.resource.testapi.TCKResourceListener;
import com.opencloud.sleetck.lib.resource.testapi.TCKResourceTestInterface;
import com.opencloud.sleetck.lib.testutils.Assert;
import com.opencloud.sleetck.lib.testutils.BaseTCKResourceListener;
import com.opencloud.sleetck.lib.testutils.FutureResult;
import com.opencloud.sleetck.lib.testutils.jmx.AlarmMBeanProxy;

/*  
 * AssertionID(1113506): Test This method only clears alarms 
 * associated with the notification source of the alarm facility 
 * object.
 * 
 */
public class Test1113506Test extends AbstractSleeTCKTest {
    public static final String SERVICE1_DU_PATH_PARAM = "service1DUPath";

    public static final String SERVICE2_DU_PATH_PARAM = "service2DUPath";

    /**
     * Perform the actual test.
     */
    public void run(FutureResult result) throws Exception {
        this.result = result;

        receivedAlarmNotifications = 0;
        expectedAlarmNotifications = 15;
        
        String activityName = "Test1113506Test";
        TCKResourceTestInterface resource = utils().getResourceInterface();
        TCKActivityID activityID = resource.createActivity(activityName);

        // Fire the first event towards Sbb2 to raise an alarm in Sbb2
        getLog().info("Firing event: " + TCKResourceEventX.X1);
        resource.fireEvent(TCKResourceEventX.X1, testName, activityID, null);

        // Fire the second event towards Sbb1 to raise an alarm in Sbb1
        getLog().info("Firing event: " + TCKResourceEventY.Y1);
        resource.fireEvent(TCKResourceEventY.Y1, testName, activityID, null);

        // Make sure the above two events have been raised
        synchronized (this) {
            wait(3000);
        }

        // Fire the last event towards Sbb1 to check the alarm in Sbb1 had been
        // cleared,
        // and the alarm in Sbb2 still alive.
        getLog().info("Firing event: " + TCKResourceEventY.Y2);
        resource.fireEvent(TCKResourceEventY.Y2, testName, activityID, null);
        
        synchronized (this) {
            wait(utils().getTestTimeout()-3000);
        }

        Assert.assertTrue(1113506, "Expected number of Alarm messages not received, Alarms were not delivered by "
                + "the AlarmFacility (expected " + expectedAlarmNotifications + ", received "
                + receivedAlarmNotifications + ")", expectedAlarmNotifications == receivedAlarmNotifications);

        result.setPassed();
    }

    /**
     * Do all the pre-run configuration of the test.
     */

    public void setUp() throws Exception {

        getLog().fine("Connecting to resource");
        resourceListener = new TCKResourceListenerImpl();
        setResourceListener(resourceListener);
        getLog().fine("Installing and activating service");

        setupService(SERVICE1_DU_PATH_PARAM, true);
        setupService(SERVICE2_DU_PATH_PARAM, true);
        
        alarmMBeanProxy = utils().getAlarmMBeanProxy();
        listener = new AlarmNotificationListenerImpl();
        alarmMBeanProxy.addNotificationListener(listener, null, null);

    }

    /**
     * Clean up after the test.
     */

    public void tearDown() throws Exception {

        String[] listActiveAlarms = alarmMBeanProxy.getAlarms();
        if (alarmMBeanProxy.getAlarms().length > 0)
            for (int i = 0; i < listActiveAlarms.length; i++)
                alarmMBeanProxy.clearAlarm(listActiveAlarms[i]);

        if (null != alarmMBeanProxy)
            alarmMBeanProxy.removeNotificationListener(listener);
        // cleanup
        super.tearDown();
    }

    private class TCKResourceListenerImpl extends BaseTCKResourceListener {
        // TCKResourceListener methods

        public synchronized void onSbbMessage(TCKSbbMessage message, TCKActivityID calledActivity)
                throws RemoteException {

            Map sbbData = (Map) message.getMessage();
            String sbbTestName = "Test1113506Test";
            String sbbTestResult = (String) sbbData.get("result");
            String sbbTestMessage = (String) sbbData.get("message");
            int assertionID = ((Integer) sbbData.get("id")).intValue();
            getLog().info(
                    "Received message from SBB: testname=" + sbbTestName + ", result=" + sbbTestResult + ", message="
                            + sbbTestMessage + ", id=" + assertionID);
            try {
                Assert.assertEquals(assertionID, "Test " + sbbTestName + " failed.", "pass", sbbTestResult);
            } catch (TCKTestFailureException ex) {
                result.setFailed(assertionID, sbbTestMessage);
            }
        }

        public void onException(Exception exception) throws RemoteException {
            getLog().warning("Received Exception from SBB or resource:");
            getLog().warning(exception);
            result.setError(exception);
        }
    }
    
    public class AlarmNotificationListenerImpl implements NotificationListener {
        public final void handleNotification(Notification notification, Object handback) {

            if (notification instanceof AlarmNotification) {
                AlarmNotification alarmNotification = (AlarmNotification) notification;
                
                if (alarmNotification.getMessage().equals(Test1113506Sbb1.ALARM_MESSAGE))
                    receivedAlarmNotifications++;
                else if (alarmNotification.getMessage().equals(Test1113506Sbb2.ALARM_MESSAGE)) {
                    receivedAlarmNotifications++;
                    if (alarmNotification.getAlarmLevel().equals(AlarmLevel.CLEAR) && receivedAlarmNotifications >= 5) {
                        result.setFailed(1113506, "Received the raised alarm been cleared from Test1113506Sbb2, " +
                                        "which is not expected!"+ receivedAlarmNotifications);
                        return;
                    }
                }
                else
                    result.setFailed(1113506,
                            "Message in AlarmNotification was not that set in AlarmFacility.raiseAlarm()");
                return;
            }
            
            getLog().info("Notification received: " + notification);
            return;
        }
    }

    private AlarmMBeanProxy alarmMBeanProxy;

    private TCKResourceListener resourceListener;
    
    private NotificationListener listener;

    private FutureResult result;
    
    private String testName = "Test1113506";
    
    private int expectedAlarmNotifications;

    private int receivedAlarmNotifications;
}
