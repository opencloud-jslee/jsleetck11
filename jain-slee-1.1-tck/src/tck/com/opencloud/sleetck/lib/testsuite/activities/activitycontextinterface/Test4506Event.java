/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.activities.activitycontextinterface;

import java.io.Serializable;
import java.util.Random;

/**
 * Define an event type that contains a sequence number.  The sequence number is used to direct the handler to perform
 * each stage of the test.
 */
public final class Test4506Event implements Serializable {
    public Test4506Event(int sequenceNumber) {
        this.id = createId();
        this.sequenceNumber = sequenceNumber;
    }

    public boolean equals(Object o) {
        if (o == this) return true;
        return (o instanceof Test4506Event)
            && ((Test4506Event)o).id == id;
    }
                                                                                
    public int hashCode() {
        return (int)id;
    }
                                                                                
    public String toString() {
        return "Test4506Event[" + id + "]";
    }

    public int getSequenceNumber() {
        return sequenceNumber;
    }

    private long createId() {
        return new Random().nextLong() ^ System.currentTimeMillis();
    }

    private final long id;

    private final int sequenceNumber;
}
