/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.activities.activitycontext;

import javax.slee.management.DeployableUnitID;

import com.opencloud.sleetck.lib.*;
import com.opencloud.sleetck.lib.testutils.*;
import com.opencloud.sleetck.lib.resource.TCKSbbMessage;
import com.opencloud.sleetck.lib.resource.testapi.TCKResourceListener;
import com.opencloud.sleetck.lib.resource.TCKActivityID;
import com.opencloud.sleetck.lib.resource.testapi.TCKResourceTestInterface;
import com.opencloud.sleetck.lib.resource.events.TCKResourceEventX;

import com.opencloud.logging.Logable;

import java.rmi.RemoteException;
import java.util.HashMap;

/**
 * This test loads a deployable unit containing a single root SBB.  It fires
 * the initial event of the SBB and checks that it receives it:
 *
 *   The SLEE automatically attaches an SBB entity to an ActivityContext when
 *   the SBB entity receives an initial event delivered on the ActivityContext.
 *
 * If the SBB does not receive the event the test will time out and fail.
 */

public class AutoAttachTest implements SleeTCKTest {

    private static final String SERVICE_DU_PATH_PARAM = "serviceDUPath";
    private static final int TEST_ID = 537;

    public void init(SleeTCKTestUtils utils) {
        this.utils = utils;
    }

    /**
     * Perform the actual test.
     */

    public TCKTestResult run() throws Exception {
        result = new FutureResult(utils.getLog());

        utils.getLog().fine("Firing TCKResourceExentX at SBB.");

        TCKResourceTestInterface resource = utils.getResourceInterface();
        String activityName = "AutoAttachTest";

        TCKActivityID activityID = resource.createActivity(activityName);
        // send an event of each type - and send the event type as the event message
        resource.fireEvent(TCKResourceEventX.X1,TCKResourceEventX.X1,activityID,null);

        utils.getLog().fine("Waiting for test pass indicator from SBB.");

        return result.waitForResultOrFail(utils.getTestTimeout(), "No response from the SBB", TEST_ID);
    }

    /**
     * Do all the pre-run configuration of the test.
     */

    public void setUp() throws Exception {
        utils.getLog().fine("Connecting to resource");
        resourceListener = new TCKResourceListenerImpl();
        utils.getResourceInterface().setResourceListener(resourceListener);
        utils.getLog().fine("Installing and activating service");

        // Install the Deployable Unit.
        String duPath = utils.getTestParams().getProperty(SERVICE_DU_PATH_PARAM);
        DeployableUnitID duID = utils.install(duPath);
        utils.activateServices(duID, true); // Activate the service
    }

    /**
     * Clean up after the test.
     */

    public void tearDown() throws Exception {
        utils.getLog().fine("Disconnecting from resource");
        utils.getResourceInterface().clearActivities();
        utils.getResourceInterface().removeResourceListener();
        utils.getLog().fine("Deactivating and uninstalling service");
        utils.deactivateAllServices();
        utils.uninstallAll();
    }

    private class TCKResourceListenerImpl extends BaseTCKResourceListener {
        public synchronized void onSbbMessage(TCKSbbMessage message, TCKActivityID calledActivity) throws RemoteException {
            utils.getLog().info("Received message from SBB");

            HashMap map = (HashMap) message.getMessage();
            Boolean passed = (Boolean) map.get("Result");
            String msgString = (String) map.get("Message");

            if (passed.booleanValue() == true)
                result.setPassed();
            else
                result.setFailed(TEST_ID, msgString);
        }

        public void onException(Exception e) throws RemoteException {
            utils.getLog().warning("Received exception from SBB");
            utils.getLog().warning(e);
            result.setError(e);
        }
    }

    private SleeTCKTestUtils utils;
    private TCKResourceListener resourceListener;
    private FutureResult result;
}
