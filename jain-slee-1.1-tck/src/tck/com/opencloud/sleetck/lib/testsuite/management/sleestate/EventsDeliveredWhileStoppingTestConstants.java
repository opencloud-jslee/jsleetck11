/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.management.sleestate;

/**
 * Constants used by the EventsDeliveredWhileStoppingTest
 */
public interface EventsDeliveredWhileStoppingTestConstants {

    // -- Sbb -> Test call codes -- //

    public static final int SBB1_RECEIVED_X1 = 1;
    public static final int SBB1_RECEIVED_X2 = 2;
}
