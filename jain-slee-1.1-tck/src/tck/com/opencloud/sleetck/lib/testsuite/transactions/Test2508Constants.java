/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.transactions;

/**
 * Defines message strings passed from the Test2508 SBBs to the test
 */
public interface Test2508Constants {

    public static final String PARENT_CALLED_CHILD          = "PARENT_CALLED_CHILD";
    public static final String SBB_EXCEPTION_THROWN_CHILD   = "SBB_EXCEPTION_THROWN_CHILD";
    public static final String SBB_ROLLED_BACK_CHILD        = "SBB_ROLLED_BACK_CHILD";
    public static final String SBB_EXCEPTION_THROWN_PARENT  = "SBB_EXCEPTION_THROWN_PARENT";
    public static final String SBB_ROLLED_BACK_PARENT       = "SBB_ROLLED_BACK_PARENT";

}
