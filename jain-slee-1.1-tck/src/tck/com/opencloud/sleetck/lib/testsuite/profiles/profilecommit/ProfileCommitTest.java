/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.profiles.profilecommit;

import com.opencloud.sleetck.lib.*;
import com.opencloud.sleetck.lib.testutils.QueuingResourceListener;
import com.opencloud.sleetck.lib.resource.TCKActivityID;
import com.opencloud.sleetck.lib.resource.testapi.TCKResourceTestInterface;
import com.opencloud.sleetck.lib.resource.events.TCKResourceEventX;
import com.opencloud.sleetck.lib.testutils.*;
import com.opencloud.sleetck.lib.testutils.jmx.*;
import com.opencloud.sleetck.lib.testsuite.profiles.ProfileTestConstants;
import com.opencloud.sleetck.lib.testutils.ProfileUtils;
import com.opencloud.sleetck.lib.testutils.ComponentIDLookup;
import com.opencloud.sleetck.lib.testutils.jmx.ProfileProvisioningMBeanProxy;
import com.opencloud.sleetck.lib.testutils.jmx.ProfileMBeanProxy;

import javax.slee.profile.ProfileSpecificationID;
import javax.management.ObjectName;

import com.opencloud.sleetck.lib.infra.SleeTCKComponentConstants;

import java.util.Vector;
import java.util.Iterator;
import java.util.Map;

import com.opencloud.sleetck.lib.testsuite.profiles.simpleprofile.SimpleProfileProxy;

public class ProfileCommitTest extends AbstractSleeTCKTest {

    public static final String PROFILE_TABLE_NAME = "ProfileCommitTestTable";
    public static final String VALUE_1 = "val1";
    public static final String VALUE_2 = "val2";
    public static final String VALUE_3 = "val3";
    public static final String VALUE_4 = "val4";
    public static final String STATUS_PASS = "pass";

    /**
     * Createa a profile table and fires an event to ProfileSecurityTestSbb to test that the profile setter returns
     * the correct exception.
     */
    public TCKTestResult run() throws Exception {
        // create the profile table
        ProfileProvisioningMBeanProxy profileProvisioning = profileUtils.getProfileProvisioningProxy();
        ProfileSpecificationID profileSpecID = new ComponentIDLookup(utils()).lookupProfileSpecificationID(
                ProfileTestConstants.SIMPLE_PROFILE_SPEC_NAME, SleeTCKComponentConstants.TCK_VENDOR, "1.0");
        getLog().info("Creating profile table: " + PROFILE_TABLE_NAME);
        profileProvisioning.createProfileTable(profileSpecID, PROFILE_TABLE_NAME);
        tableCreated = true;

        // add profile
        final String name = "A";
        getLog().info("Creating profile " + name);
        ObjectName jmxObjectName = profileProvisioning.createProfile(PROFILE_TABLE_NAME, name);

        // install resource listener
        listener = new ResourceListenerImpl(utils());
        setResourceListener(listener);
        resource = utils().getResourceInterface();

        getLog().fine("Creaing activity");
        activityID = resource.createActivity(getClass().getName());

        proxy = getProfileProxy(jmxObjectName);

        proxy.editProfile();
        getLog().info("Initialising profile value");
        proxy.setValue(VALUE_1);
        proxy.commitProfile();

        proxy.editProfile();
        proxy.setValue(VALUE_2);

        getLog().info("Verifying that SBB does not see the new value");
        if (! testSbbProfileValue(TCKResourceEventX.X2, VALUE_1)) {
            Assert.fail(1122, "Changes made to a profile were visible to the SBB before beging committed by the " +
                              "administrator");
        }

        proxy.commitProfile();

        if (! testSbbProfileValue(TCKResourceEventX.X2, VALUE_2)) {
            Assert.fail(1064, "The profile get accessor method did not return the last committed value to the SBB(1)");
        }

        proxy.editProfile();
        proxy.setValue(VALUE_3);
        proxy.commitProfile();

        if (! testSbbProfileValue(TCKResourceEventX.X2, VALUE_3)) {
            Assert.fail(1064, "The profile get accessor method did not return the last committed value to the SBB(2)");
        }

        return TCKTestResult.passed();
    }

    public void setUp() throws Exception {
        String duPath = utils().getTestParams().getProperty(ProfileTestConstants.PROFILE_SPEC_DU_PATH_PARAM);
        getLog().fine("Installing profile specification");
        utils().install(duPath);
        profileUtils = new ProfileUtils(utils());
        activeProxies = new Vector();
        super.setUp();
    }

    public void tearDown() throws Exception {
        // close profiles
        if (activeProxies != null) {
            getLog().fine("Closing profiles");
            Iterator activeProxiesIter = activeProxies.iterator();
            while (activeProxiesIter.hasNext()) {
                SimpleProfileProxy aProxy = (SimpleProfileProxy) activeProxiesIter.next();
                try {
                    if (aProxy != null) {
                        if (aProxy.isProfileWriteable()) aProxy.restoreProfile();
                        aProxy.closeProfile();
                    }
                } catch (Exception ex) {
                    getLog().warning("Exception caught while trying to close profiles:");
                    getLog().warning(ex);
                }
            }
        }
        // delete profile tables
        try {
            if (profileUtils != null && tableCreated) {
                getLog().fine("Removing profile table");
                profileUtils.removeProfileTable(PROFILE_TABLE_NAME);
            }
        } catch (Exception e) {
            getLog().warning("Caught exception while trying to remove profile table:");
            getLog().warning(e);
        }
        super.tearDown();
    }

    private boolean testSbbProfileValue(String eventTypeName, String expectedValue) throws Exception {
        Map response;
        String status;
        getLog().info("Firing event");
        // Send an event
        getLog().info("Firing event: SBB's view of the profile should have value: " + expectedValue);
        resource.fireEvent(eventTypeName, expectedValue, activityID, null);
        response = (Map)listener.nextMessage().getMessage();
        status = (String)response.get("status");
        getLog().info("SBB returned status: " + status);
        return status.equals(STATUS_PASS);
    }

    private SimpleProfileProxy getProfileProxy(ObjectName mbeanName) {
        SimpleProfileProxy rProxy = new SimpleProfileProxy(mbeanName, utils().getMBeanFacade());
        activeProxies.addElement(rProxy);
        return rProxy;
    }


    // Private classes
    private class ResourceListenerImpl extends QueuingResourceListener {
        public ResourceListenerImpl(SleeTCKTestUtils utils) {
            super(utils);
        }

        public Object onSbbCall(Object argument) throws Exception {
            getLog().info("Sbb synchronous callback: " + argument);
            proxy.editProfile();
            proxy.setValue(VALUE_4);
            proxy.commitProfile();
            return null;
        }
    }

    private SimpleProfileProxy proxy;
    private TCKActivityID activityID;
    private TCKResourceTestInterface resource;
    private ResourceListenerImpl listener;
    private ProfileUtils profileUtils;
    private boolean tableCreated = false;
    private Vector activeProxies;

}
