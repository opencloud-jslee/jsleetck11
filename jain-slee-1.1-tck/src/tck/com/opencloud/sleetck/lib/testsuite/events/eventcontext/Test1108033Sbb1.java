/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.events.eventcontext;

import java.util.HashMap;

import javax.slee.ActivityContextInterface;
import javax.slee.EventContext;
import javax.slee.InitialEventSelector;
import javax.slee.facilities.Tracer;

import com.opencloud.sleetck.lib.resource.events.TCKResourceEventX;
import com.opencloud.sleetck.lib.sbbutils.BaseTCKSbb;
import com.opencloud.sleetck.lib.sbbutils.TCKSbbUtils;

/**
 * AssertionID(1108033): Test This isSuspended method determines if the 
 * delivery of the event associated with this Event Context is suspended.
 * 
 * AssertionID(1108034): Test This method returns true if the event 
 * delivery is suspended or false otherwise.
 *
 */
public abstract class Test1108033Sbb1 extends BaseTCKSbb {
    public static final String TRACE_MESSAGE_TCKResourceEventX1 = "Test1108033Sbb1:I got TCKResourceEventX1 on ActivityA";

    public static final String TRACE_MESSAGE_TCKResourceEventX2 = "Test1108033Sbb1:I got TCKResourceEventX2 on ActivityB";
    
    public static final String TRACE_MESSAGE_TCKResourceEventX3 = "Test1108033Sbb1:I got TCKResourceEventX3 on ActivityB";

    public InitialEventSelector initialEventSelector(InitialEventSelector ies) {
        ies.setCustomName("test");
        return ies;
    }

    public void onTCKResourceEventX1(TCKResourceEventX event, ActivityContextInterface aci, EventContext context) {
        try {
            //send message to TCK: I got TCKResourceEventX1 on ActivityA
            tracer = getSbbContext().getTracer("com.test");
            tracer.info("Begin: " + TRACE_MESSAGE_TCKResourceEventX1);

            setTestName((String) event.getMessage());

            //store context in CMP field
            setEventContext(context);

            //call suspendDelivery() method now, we assumed the default timeout 
            //on the SLEE will be more than 10000ms to complete this test.
            context.suspendDelivery();
            tracer.info("End: " + TRACE_MESSAGE_TCKResourceEventX1);
        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }
    }

    public void onTCKResourceEventX2(TCKResourceEventX event, ActivityContextInterface aci, EventContext context) {
        try {
            //send message to TCK: I got TCKResourceEventX2 on ActivityB
            tracer = getSbbContext().getTracer("com.test");
            tracer.info("Begin: " + TRACE_MESSAGE_TCKResourceEventX2);
            //get EventContext ec from CMP field
            EventContext ec = getEventContext();
            //assert ec.isSuspended(), check ec has been suspended or not 
            if (!ec.isSuspended()) {
                sendResultToTCK("Test1108033Test", false, "SBB1:onTCKResourceEventX2-ERROR: The event delivery hasn't been suspended, the "
                        + "EventContext.isSuspended() returned false!", 1108034);
                return;
            }
            tracer.info("End: " + TRACE_MESSAGE_TCKResourceEventX2);
        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }
    }
    
    public void onTCKResourceEventX3(TCKResourceEventX event, ActivityContextInterface aci, EventContext context) {
        try {
            //send message to TCK: I got TCKResourceEventX3 on ActivityB
            tracer = getSbbContext().getTracer("com.test");
            tracer.info("Begin: " + TRACE_MESSAGE_TCKResourceEventX3);
            //get EventContext ec from CMP field
            EventContext ec = getEventContext();
            //assert ec.isSuspended(), check ec has been suspended or not
            try {
                if (ec.isSuspended())  
                    ec.resumeDelivery();
            } catch (Exception e) {
                tracer.severe("Failed to resume the suspended event delivery.");
            }
            tracer.info("End: " + TRACE_MESSAGE_TCKResourceEventX3);
        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }
    }

    private void sendResultToTCK(String testName, boolean result, String message, int failedAssertionID) throws Exception {
        HashMap sbbData = new HashMap();
        sbbData.put("testname", testName);
        sbbData.put("result", result ? "pass" : "fail");
        sbbData.put("message", message);
        sbbData.put("id", new Integer(failedAssertionID));
        TCKSbbUtils.getResourceInterface().sendSbbMessage(sbbData);
    }

    public abstract void setEventContext(EventContext eventContext);
    public abstract EventContext getEventContext();

    public abstract void setTestName(String testName);
    public abstract String getTestName();
    
    private Tracer tracer = null;

}
