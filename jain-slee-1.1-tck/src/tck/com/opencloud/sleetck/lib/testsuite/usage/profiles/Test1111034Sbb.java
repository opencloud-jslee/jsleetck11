/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.usage.profiles;

import javax.naming.InitialContext;
import javax.slee.ActivityContextInterface;
import javax.slee.profile.ProfileFacility;
import javax.slee.profile.ProfileTable;

import com.opencloud.sleetck.lib.resource.events.TCKResourceEventX;
import com.opencloud.sleetck.lib.sbbutils.BaseTCKSbb;
import com.opencloud.sleetck.lib.sbbutils.TCKSbbUtils;

public abstract class Test1111034Sbb extends BaseTCKSbb {

    public void onTCKResourceEventX1(TCKResourceEventX event, ActivityContextInterface aci) {
        ProfileFacility facility;
        ProfileTable pt;
        Test1111034ProfileLocal addedProfile;

        try {
            facility = (ProfileFacility) new InitialContext().lookup(ProfileFacility.JNDI_NAME);
            pt = facility.getProfileTable(Test1111034Test.PROFILE_TABLE_NAME);
            addedProfile = (Test1111034ProfileLocal)pt.find(Test1111034Test.PROFILE_NAME);
            
            getSbbContext().getTracer("Test1111034Sbb").fine("Got profile: "+addedProfile.toString());

            addedProfile.incrementNamedParameterSet();

            TCKSbbUtils.getResourceInterface().sendSbbMessage(null);
        } catch(Exception e) {
            TCKSbbUtils.handleException(e);
        }
    }
}
