/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.facilities.profilefacility;

import java.util.Collection;
import javax.slee.profile.ProfileTable;

public interface Test1113032ProfileTable extends ProfileTable {
    public Collection queryXEquals(int x);
    public Collection queryYEquals(String y);
    public Collection queryXGreaterThan(int value);
    public Collection queryXRange(int low, int high);
    public Collection queryYHasPrefix(String prefix);
    public Collection queryYLongestPrefix(String prefix);
}
