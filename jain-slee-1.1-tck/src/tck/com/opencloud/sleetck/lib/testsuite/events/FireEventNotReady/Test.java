/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.events.FireEventNotReady;

import com.opencloud.sleetck.lib.SleeTCKTest;
import com.opencloud.sleetck.lib.SleeTCKTestUtils;
import com.opencloud.sleetck.lib.TCKTestResult;
import com.opencloud.sleetck.lib.resource.TCKActivityID;
import com.opencloud.sleetck.lib.resource.TCKSbbMessage;
import com.opencloud.sleetck.lib.resource.events.TCKResourceEventX;
import com.opencloud.sleetck.lib.resource.testapi.TCKResourceListener;
import com.opencloud.sleetck.lib.resource.testapi.TCKResourceTestInterface;
import com.opencloud.sleetck.lib.testutils.BaseTCKResourceListener;
import com.opencloud.sleetck.lib.testutils.FutureResult;

import javax.slee.management.DeployableUnitID;
import java.rmi.RemoteException;

public class Test implements SleeTCKTest {

    // Test parameter keys
    private static final String SERVICE_DU_PATH_PARAM = "serviceDUPath";
    private static final String EVENT_DU_PATH_PARAM = "eventDUPath";
    // SleeTCKTest methods

    public void init(SleeTCKTestUtils utils) { this.utils=utils; }

    public TCKTestResult run() throws Exception {
        result = new FutureResult(utils.getLog());
        String activityName = "FireEventNotReadyTestActivity";
        TCKResourceTestInterface resource = utils.getResourceInterface();
        utils.getLog().fine("Creating activity");
        TCKActivityID activityID = resource.createActivity(activityName);
        utils.getLog().info("Firing event");
        // send an event of each type - and send the event type as the event message
        resource.fireEvent(TCKResourceEventX.X1,TCKResourceEventX.X1,activityID,null);
        utils.getLog().fine("Test thread is waiting for test result");
        return result.waitForResult(utils.getTestTimeout(),"Event must be delivered");
    }

    public void setUp() throws Exception {
        utils.getLog().fine("Connecting to resource");
        resourceListener = new TCKResourceListenerImpl();
        utils.getResourceInterface().setResourceListener(resourceListener);
        utils.getLog().fine("Installing and activating service");
        String evDuPath = utils.getTestParams().getProperty(EVENT_DU_PATH_PARAM);
        utils.install(evDuPath);
        String duPath = utils.getTestParams().getProperty(SERVICE_DU_PATH_PARAM);
        DeployableUnitID duID = utils.install(duPath);
        utils.activateServices(duID,true);
    }

    public void tearDown() throws Exception {
        utils.getLog().fine("Disconnecting from resource");
        utils.getResourceInterface().clearActivities();
        utils.getResourceInterface().removeResourceListener();
        utils.getLog().fine("Deactivating and uninstalling service");
        utils.deactivateAllServices();
        utils.uninstallAll();
    }

    // Private classes

    private class TCKResourceListenerImpl extends BaseTCKResourceListener {

        // TCKResourceListener methods

        public synchronized void onSbbMessage(TCKSbbMessage message, TCKActivityID calledActivity) throws RemoteException {
            utils.getLog().info("Received message from SBB");
            Boolean messageData = (Boolean)message.getMessage();
            if( messageData.booleanValue() )
                result.setPassed();
            else
                result.setFailed(690, "The SBB fired an event in the pooled state and did not receive an IllegalStateException");
        }

        public void onException(Exception exception) throws RemoteException {
            utils.getLog().warning("Received Exception from SBB or resource:");
            utils.getLog().warning(exception);
            result.setError(exception);
        }

    }

    // Private state

    private SleeTCKTestUtils utils;
    private TCKResourceListener resourceListener;
    private FutureResult result;

}
