/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.javax.slee.management.DeploymentMBean;

import com.opencloud.sleetck.lib.SleeTCKTest;
import com.opencloud.sleetck.lib.SleeTCKTestUtils;
import com.opencloud.sleetck.lib.TCKTestResult;
import com.opencloud.sleetck.lib.testutils.jmx.DeploymentMBeanProxy;

import javax.slee.ComponentID;
import javax.slee.EventTypeID;
import javax.slee.SbbID;
import javax.slee.ServiceID;
import javax.slee.management.DeployableUnitID;
import javax.slee.profile.ProfileSpecificationID;

public class Test3776Test implements SleeTCKTest {

    private static final String SERVICE_DU_PATH_PARAM = "DUPath";
    private static final int TEST_ID = 3776;

    public void init(SleeTCKTestUtils utils) {
        this.utils = utils;
    }

    /**
     * Perform the actual test.
     */
    public TCKTestResult run() throws Exception {

        DeploymentMBeanProxy duProxy = utils.getDeploymentMBeanProxy();

        // Store the initial set of DU IDs for later comparison
        DeployableUnitID[] initialDuIDs = duProxy.getDeployableUnits();

        // Install the Deployable Units
        String duPath = utils.getTestParams().getProperty(SERVICE_DU_PATH_PARAM);
        String duURL = utils.getDeploymentUnitURL(duPath);
        DeployableUnitID duID = duProxy.install(duURL);

        try {
            DeployableUnitID duIDsAfterInstall[] = duProxy.getDeployableUnits();
            if (duIDsAfterInstall.length != initialDuIDs.length + 1)
                return TCKTestResult.failed(TEST_ID, "DeploymentMBean.getDeployableUnits() returned incorrect number of deployable units. " +
                        "Before the installation of the service the number was " + initialDuIDs.length + ", and following the " +
                        "install the number was " + duIDsAfterInstall.length+ " (was expecting an increase of 1)");
            logSuccessfulCheck(TEST_ID);

            SbbID sbbIDs[] = duProxy.getSbbs();
            if (sbbIDs.length != 1)
                return TCKTestResult.failed(3780, "DeploymentMBean.getSbbs() returned incorrect number of SBBs.");
            logSuccessfulCheck(3780);

            EventTypeID eventIDs[] = duProxy.getEventTypes();
            if (eventIDs.length == 0)
                return TCKTestResult.failed(3784, "DeploymentMBean.getEventTypes() returned incorrect number of event types.");
            logSuccessfulCheck(3784);

            Object profileIDs[] = duProxy.getProfileSpecifications();
            for (int i = 0; i < profileIDs.length; i++)
                if (!(profileIDs[i] instanceof ProfileSpecificationID))
                    return TCKTestResult.failed(3788, "DeploymentMBean.getProfileSpecifications() returned an array containing objects other than ProfileSpecificationID.");
            logSuccessfulCheck(3788);

            ServiceID serviceIDs[] = duProxy.getServices();
            if (serviceIDs.length != 1)
                return TCKTestResult.failed(3792, "DeploymentMBean.getServices() returned incorrect number of services.");
            logSuccessfulCheck(3792);

            try {
                duProxy.getResourceAdaptorTypes();
                logSuccessfulCheck(3796);
            } catch (Exception e) {
                return TCKTestResult.failed(3796, "DeploymentMBean.getResourceAdaptorTypes() threw an exception.");
            }

            try {
                duProxy.getResourceAdaptors();
                logSuccessfulCheck(4475);
            } catch (Exception e) {
                return TCKTestResult.failed(4475, "DeploymentMBean.getResourceAdaptors() threw an exception.");
            }

            boolean passed = false;
            try {
                duProxy.getReferringComponents(null);
            } catch (NullPointerException e) {
                passed = true;
                logSuccessfulCheck(3802);
            } catch (Exception e) {
                return TCKTestResult.failed(3802, "DeploymentMBean.getReferringComponents(null) didn't throw NullPointerException but " + e.getClass().toString());
            }

            if (passed == false)
                return TCKTestResult.failed(3802, "DeploymentMBean.getReferringComponents(null) didn't throw NullPointerException.");

            passed = false;
            try {
                duProxy.getDescriptor((DeployableUnitID) null);
            } catch (NullPointerException e) {
                passed = true;
                logSuccessfulCheck(3808);
            } catch (Exception e) {
                return TCKTestResult.failed(3808, "DeploymentMBean.getDescriptor((DeployableUnitID) null) didn't throw NullPointerException but " + e.getClass().toString());
            }

            if (passed == false)
                return TCKTestResult.failed(3808, "DeploymentMBean.getDescriptor((DeployableUnitID) null) didn't throw NullPointerException.");

            passed = false;
            try {
                duProxy.getDescriptors((DeployableUnitID[]) null);
            } catch (NullPointerException e) {
                passed = true;
                logSuccessfulCheck(3814);
            } catch (Exception e) {
                return TCKTestResult.failed(3814, "DeploymentMBean.getDescriptors((DeployableUnitID[]) null) didn't throw NullPointerException but " + e.getClass().toString());
            }

            if (passed == false)
                return TCKTestResult.failed(3814, "DeploymentMBean.getDescriptors((DeployableUnitID[]) null) didn't throw NullPointerException.");

            passed = false;
            try {
                duProxy.getDescriptor((ComponentID) null);
            } catch (NullPointerException e) {
                passed = true;
                logSuccessfulCheck(3819);
            } catch (Exception e) {
                return TCKTestResult.failed(3819, "DeploymentMBean.getDescriptor((ComponentID) null) didn't throw NullPointerException but " + e.getClass().toString());
            }

            if (passed == false)
                return TCKTestResult.failed(3819, "DeploymentMBean.getDescriptor((ComponentID) null) didn't throw NullPointerException.");

            passed = false;
            try {
                duProxy.getDescriptors((ComponentID[]) null);
            } catch (NullPointerException e) {
                passed = true;
                logSuccessfulCheck(3825);
            } catch (Exception e) {
                return TCKTestResult.failed(3825, "DeploymentMBean.getDescriptors((ComponentID[]) null) didn't throw NullPointerException but " + e.getClass().toString());
            }

            if (passed == false)
                return TCKTestResult.failed(3825, "DeploymentMBean.getDescriptors((ComponentID[]) null) didn't throw NullPointerException.");

            passed = false;
            try {
                duProxy.isInstalled((DeployableUnitID) null);
            } catch (NullPointerException e) {
                passed = true;
                logSuccessfulCheck(3829);
            } catch (Exception e) {
                return TCKTestResult.failed(3829, "DeploymentMBean.isInstalled((DeployableUnitID) null) threw " + e.getClass().toString());
            }

            if (passed == false)
                return TCKTestResult.failed(3829, "DeploymentMBean.isInstalled((DeployableUnitID) null) didn't throw NullPointerException.");

            passed = false;
            try {
                duProxy.isInstalled((ComponentID) null);
            } catch (NullPointerException e) {
                passed = true;
                logSuccessfulCheck(3833);
            } catch (Exception e) {
                return TCKTestResult.failed(3833, "DeploymentMBean.isInstalled((ComponentID) null) threw " + e.getClass().toString());
            }

            if (passed == false)
                return TCKTestResult.failed(3833, "DeploymentMBean.isInstalled((ComponentID) null) didn't throw NullPointerException.");


        } finally {
            utils.getLog().fine("Deactivating and uninstalling service");
            duProxy.uninstall(duID);
            DeployableUnitID duIDsAfterUninstall[] = duProxy.getDeployableUnits();
            if (duIDsAfterUninstall.length != initialDuIDs.length)
                return TCKTestResult.failed(TEST_ID, "DeploymentMBean.getDeployableUnits() returned incorrect number of deployable units. " +
                        "Before the installation of the service the number was " + initialDuIDs.length + ", and following the " +
                        "install, then later uninstall of service, the number was " + duIDsAfterUninstall.length);
            logSuccessfulCheck(TEST_ID);
        }

        return TCKTestResult.passed();

    }

    /**
     * Do all the pre-run configuration of the test.
     */
    public void setUp() throws Exception {
        // no-op
    }

    /**
     * Clean up after the test.
     */
    public void tearDown() throws Exception {
        utils.getLog().fine("Disconnecting from resource");
        utils.getResourceInterface().clearActivities();
        utils.getResourceInterface().removeResourceListener();
    }

    private void logSuccessfulCheck(int assertionID) {
        utils.getLog().info("Check for assertion "+assertionID+" OK");
    }

    private SleeTCKTestUtils utils;

}
