/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.javax.slee.management.ServiceUsageMBean;

import com.opencloud.sleetck.lib.resource.events.TCKResourceEventY;
import com.opencloud.sleetck.lib.sbbutils.TCKSbbUtils;
import com.opencloud.sleetck.lib.testsuite.usage.common.GenericUsageSbbInstructions;
import com.opencloud.sleetck.lib.testsuite.usage.common.GenericUsageSbbLocal;
import com.opencloud.sleetck.lib.testsuite.usage.common.GenericUsageSbb;

import javax.slee.ActivityContextInterface;
import javax.slee.ChildRelation;
import javax.slee.facilities.Level;

/**
 * Calls doUpdates() on the child SBB from a TCKResourceEventY1 event handler
 * As this SBB extends GenericUsageSbb, its TCKResourceEventX1 event handler will
  * cause updates to itself.
  */
public abstract class ServiceUsageMBeanExceptionsParentSbbB extends GenericUsageSbb {

    public void onTCKResourceEventY1(TCKResourceEventY event, ActivityContextInterface aci) {
        try {
            // fire an abitrary trace message
            TCKSbbUtils.createTrace(getSbbID(),Level.INFO,"This is an arbitrary trace message generated to cause "+
                    "a TraceNotification which should be filtered by the usage notification filters.",null);
            // delegate to the child SBB to handle the update request
            GenericUsageSbbInstructions instructions = GenericUsageSbbInstructions.fromExported(event.getMessage());
            ((GenericUsageSbbLocal)getChildRelation().create()).doUpdates(instructions);
        } catch(Exception e) {
            TCKSbbUtils.handleException(e);
        }
    }

    public abstract ChildRelation getChildRelation();

}
