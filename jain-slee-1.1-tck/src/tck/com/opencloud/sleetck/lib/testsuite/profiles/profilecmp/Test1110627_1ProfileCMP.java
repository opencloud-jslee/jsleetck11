/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.profiles.profilecmp;

/*
 * CMP field name has to start with an uppercase letter after the initial 'get' or 'set'.
 */
public interface Test1110627_1ProfileCMP {
    public String getvalue();
    public void setvalue(String value);
}
