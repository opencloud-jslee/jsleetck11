/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.javax.slee.ActivityContextInterface.Detach;

import com.opencloud.sleetck.lib.TCKTestResult;
import com.opencloud.sleetck.lib.resource.events.TCKResourceEventX;
import com.opencloud.sleetck.lib.sbbutils.BaseTCKSbb;
import com.opencloud.sleetck.lib.sbbutils.TCKSbbUtils;

import javax.slee.ActivityContextInterface;
import javax.slee.ChildRelation;
import javax.slee.SbbLocalObject;
import javax.slee.facilities.Level;
import javax.slee.RolledBackContext;

/**
 * Defines an initial event handler, which:
 * creates a child, attaches the child to the activity, removes the child,
 * then attempts to detach the now invalid child from the activity
 */
public abstract class DetachInvalidParentSbb extends BaseTCKSbb {

    public void onTCKResourceEventX1(TCKResourceEventX event, ActivityContextInterface aci) {
        try {
            createTraceSafe(Level.INFO,"DetachInvalidParentSbb:onTCKResourceEventX1()");

            SbbLocalObject childLocalObject = null;

            // Create the child sbb local object
            createTraceSafe(Level.INFO,"DetachInvalidParentSbb:onTCKResourceEventX1():creating the child SBB");
            childLocalObject = getChildRelation().create();

            // Attach it to the ActivityContextInterface.
            createTraceSafe(Level.INFO,"DetachInvalidParentSbb:onTCKResourceEventX1():attaching the child SBB to the activity");
            aci.attach(childLocalObject);

            // Remove the child SBB
            createTraceSafe(Level.INFO,"DetachInvalidParentSbb:onTCKResourceEventX1():removing the child SBB");
            childLocalObject.remove();

            // Try to detach it from the ActivityContextInterface.
            try {
                createTraceSafe(Level.INFO,"DetachInvalidParentSbb:onTCKResourceEventX1():attempting to detach the now invalid child SBB from the activity");
                aci.detach(childLocalObject);
                String errorMessage = "TransactionRolledbackLocalException should have been thrown, but no exception was thrown.";
                createTraceSafe(Level.WARNING,errorMessage);
                sendTestResultSafe(TCKTestResult.failed(3027,errorMessage));
            } catch (javax.slee.TransactionRolledbackLocalException trble) {
                createTraceSafe(Level.INFO,"DetachInvalidParentSbb:onTCKResourceEventX1():caught the expected TransactionRolledbackLocalException");
                sendTestResultSafe(TCKTestResult.passed());
            }
        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }
    }

    public void sbbRolledBack(RolledBackContext context) {
        createTraceSafe(Level.INFO,"DetachInvalidParentSbb:sbbRolledBack() invoked as expected");
    }

    private void sendTestResultSafe(TCKTestResult result) {
        try {
            TCKSbbUtils.getResourceInterface().sendSbbMessage(result.toExported());
        } catch (Exception ex) {
            TCKSbbUtils.handleException(ex);
        }
    }

    public abstract ChildRelation getChildRelation();

}
