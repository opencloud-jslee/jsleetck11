/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.management.DeploymentMBean;

import com.opencloud.sleetck.lib.AbstractSleeTCKTest;
import com.opencloud.sleetck.lib.TCKTestResult;

import javax.slee.management.DeployableUnitID;

public class Test1571Test extends AbstractSleeTCKTest {

    private static final String SERVICE_DU_PATH_PARAM = "DUPath";
    private static final int TEST_ID = 1571;

    /**
     * Perform the actual test.
     */
    public TCKTestResult run() throws Exception {
        DeployableUnitID duID;

        // Install the Deployable Units
        String duPath = utils().getTestParams().getProperty(SERVICE_DU_PATH_PARAM);
        try {
            String fullURL = utils().getDeploymentUnitURL(duPath);
            duID = utils().getDeploymentMBeanProxy().install(fullURL);
        } catch (Exception e) {
            return TCKTestResult.failed(TEST_ID, "Failed to install deployable unit containing two different SBBs.");
        }

        // Uninstall the DU.
        utils().getDeploymentMBeanProxy().uninstall(duID);

        return TCKTestResult.passed();
    }

    // Empty implementation
    public void setUp() throws Exception {}

}
