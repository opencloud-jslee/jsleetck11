/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.sbb.abstractclass;

import com.opencloud.sleetck.lib.*;
import com.opencloud.sleetck.lib.testutils.ExceptionsUtil;
import com.opencloud.sleetck.lib.testutils.BaseTCKResourceListener;
import com.opencloud.sleetck.lib.resource.TCKActivityID;
import com.opencloud.sleetck.lib.resource.testapi.TCKResourceTestInterface;
import com.opencloud.sleetck.lib.resource.testapi.TCKResourceListener;
import com.opencloud.sleetck.lib.resource.events.TCKResourceEventX;
import com.opencloud.sleetck.lib.testutils.*;
import com.opencloud.sleetck.lib.testutils.jmx.*;
import com.opencloud.sleetck.lib.resource.TCKSbbMessage;
import java.rmi.RemoteException;
import java.util.Map;

/**
 * Tests reaction of the SLEE to an exception thrown by a non-transaction
 * SBB abstract class method.
 */

public class SbbExceptionThrownRethrow extends AbstractSleeTCKTest {

    public void run(FutureResult result) throws Exception {
        this.result = result;

        // create an activity object
        TCKResourceTestInterface resource = utils().getResourceInterface();
        String activityName = "SbbExceptionThrownRethrow";
        TCKActivityID activityID = resource.createActivity(activityName);

        // fire TCKResourceEventX.X1 on this activity
        utils().getLog().fine("Firing TCKResourceEventX.X1 on activity " + activityName);
        resource.fireEvent(TCKResourceEventX.X1, TCKResourceEventX.X1, activityID, null);

        // fire TCKResourceEventX.X1 on this activity
        utils().getLog().fine("Firing TCKResourceEventX.X2 on activity " + activityName);
        resource.fireEvent(TCKResourceEventX.X2, TCKResourceEventX.X2, activityID, null);
    }

    public void setUp() throws Exception {
        getLog().fine("Connecting to resource");
        TCKResourceListener resourceListener = new TCKResourceListenerImpl();
        setResourceListener(resourceListener);

        setupService(DescriptionKeys.SERVICE_DU_PATH_PARAM, true);
    }


    private class TCKResourceListenerImpl extends BaseTCKResourceListener {
        // TCKResourceListener methods
        public synchronized void onSbbMessage(TCKSbbMessage message, TCKActivityID calledActivity) throws RemoteException {
            Map sbbData = (Map)message.getMessage();
            if (sbbData.containsKey("reinvoked")) reinvoked = true;
            else if (sbbData.containsKey("x2")) {
                if (reinvoked) {
                    result.setFailed(459, "sbbExceptionThrown was reinvoked after rethrowing a runtime exception");
                }
                else {
                    result.setPassed();
                }
            }
        }

        public void onException(Exception exception) throws RemoteException {
            getLog().warning("Received exception from SBB or resource:");
            getLog().warning(exception);
            result.setError(exception);
        }
    }


    private FutureResult result;
    private boolean reinvoked = false;
}
