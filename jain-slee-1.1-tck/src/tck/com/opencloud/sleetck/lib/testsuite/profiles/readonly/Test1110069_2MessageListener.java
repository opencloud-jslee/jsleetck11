/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.profiles.readonly;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;

import javax.slee.profile.ProfileTable;
import javax.slee.transaction.SleeTransaction;
import javax.slee.transaction.SleeTransactionManager;

import com.opencloud.logging.StdErrLog;
import com.opencloud.sleetck.lib.profileutils.BaseMessageSender;
import com.opencloud.sleetck.lib.rautils.MessageHandler;
import com.opencloud.sleetck.lib.rautils.RMIObjectChannel;
import com.opencloud.sleetck.lib.rautils.TCKRAUtils;

/**
 * Provides callback method for handling messages sent from the TCK test to the RA.
 * The actual test code is located in the 'handleMessage()' method.
 */
public class Test1110069_2MessageListener extends UnicastRemoteObject implements MessageHandler {

    public static final String PROFILE_TABLE_NAME = "Test1110069_2ProfileTable";
    public static final String PROFILE_NAME = "Test1110069_2Profile";

    private Test1110069_2ResourceAdaptor ra;

    public Test1110069_2MessageListener(Test1110069_2ResourceAdaptor ra) throws RemoteException {
        super();

        try {
            out = TCKRAUtils.lookupRMIObjectChannel();
            msgSender = new BaseMessageSender(out, new StdErrLog());
        }
        catch (Exception e) {
            ra.getLog().warning("Exception occurred when trying to acquire RMIObjectChannel: ",e);
        }

        this.ra = ra;
    }

    public boolean handleMessage(Object message) throws RemoteException {

        //run the tests, if both run through successfully and completely (=both return 'true')
        //send success msg back to TCK test
        if (test_CREATE() && test_UPDATE() && test_CHECK() &&
            test_UPDATE_VIA_CMP() && test_CHECK_VIA_CMP() &&
            test_REMOVE() && test_CHECK_REMOVE()) {

            msgSender.sendSuccess(1110069, "Test successful.");
        }
        return true;
    }

    private boolean test_CHECK_REMOVE() {
        SleeTransactionManager txnManager = ra.getResourceAdaptorContext().getSleeTransactionManager();
        SleeTransaction txn=null;

        try {
            ProfileTable profileTable = ra.getResourceAdaptorContext().getProfileTable(PROFILE_TABLE_NAME);
            //start a new TXN as profileTable.find is mandatory transactional
            txn = txnManager.beginSleeTransaction();

            ReadOnlyTestsProfileCMP profileCMP = (ReadOnlyTestsProfileCMP)profileTable.find(PROFILE_NAME);
            if (profileCMP != null) {
                msgSender.sendFailure(1110069, "Profile "+PROFILE_NAME+ " was not removed correctly.");
                return false;
            }
            msgSender.sendLogMsg("Check whether profile was really removed after TXN committed was successful.");

            txn.commit();
            txn = null;

        } catch (Exception e) {
            msgSender.sendException(e);
            return false;
        } finally {
            try {
                if (txn!=null)
                    txn.rollback();
            } catch (Exception ex) {
                ra.getLog().warning("Error occured when trying to rollback RA started TXN.", ex);
                msgSender.sendLogMsg("Exception occurred when trying to safely rollback RA started TXN: "+ex.getMessage());
            }
        }

        return true;
    }


    private boolean test_REMOVE() {
        SleeTransactionManager txnManager = ra.getResourceAdaptorContext().getSleeTransactionManager();
        SleeTransaction txn=null;

        try {
            ProfileTable profileTable = ra.getResourceAdaptorContext().getProfileTable(PROFILE_TABLE_NAME);
            //start a new TXN as profileTable.find is mandatory transactional
            txn = txnManager.beginSleeTransaction();

            boolean res = profileTable.remove(PROFILE_NAME);
            if (!res) {
                msgSender.sendFailure(1110069, "Sbb failed to remove profile "+PROFILE_NAME);
                return false;
            }
            msgSender.sendLogMsg("ProfileTable object reports successful removal of profile table "+PROFILE_NAME);

            txn.commit();
            txn = null;

        } catch (Exception e) {
            msgSender.sendException(e);
            return false;
        } finally {
            try {
                if (txn!=null)
                    txn.rollback();
            } catch (Exception ex) {
                ra.getLog().warning("Error occured when trying to rollback RA started TXN.", ex);
                msgSender.sendLogMsg("Exception occurred when trying to safely rollback RA started TXN: "+ex.getMessage());
            }
        }

        return true;
    }

    private boolean test_CHECK_VIA_CMP() {
        SleeTransactionManager txnManager = ra.getResourceAdaptorContext().getSleeTransactionManager();
        SleeTransaction txn=null;

        try {
            ProfileTable profileTable = ra.getResourceAdaptorContext().getProfileTable(PROFILE_TABLE_NAME);
            //start a new TXN as profileTable.find is mandatory transactional
            txn = txnManager.beginSleeTransaction();

            ReadOnlyTestsProfileCMP profileCMP = (ReadOnlyTestsProfileCMP)profileTable.find(PROFILE_NAME);
            if (!profileCMP.getValue().equals("newValue2")) {
                msgSender.sendFailure(1110086, "Failed to use set/get accessor methods via ProfileCMP object properly.");
                return false;
            }
            msgSender.sendLogMsg("Checked that new value has been successfully set and could be obtained by 'get' accessor via ProfileCMP interface.");

            txn.commit();
            txn = null;

        } catch (Exception e) {
            msgSender.sendException(e);
            return false;
        } finally {
            try {
                if (txn!=null)
                    txn.rollback();
            } catch (Exception ex) {
                ra.getLog().warning("Error occured when trying to rollback RA started TXN.", ex);
                msgSender.sendLogMsg("Exception occurred when trying to safely rollback RA started TXN: "+ex.getMessage());
            }
        }

        return true;
    }

    private boolean test_UPDATE_VIA_CMP() {
        SleeTransactionManager txnManager = ra.getResourceAdaptorContext().getSleeTransactionManager();
        SleeTransaction txn=null;

        try {
            ProfileTable profileTable = ra.getResourceAdaptorContext().getProfileTable(PROFILE_TABLE_NAME);
            //start a new TXN as profileTable.find is mandatory transactional
            txn = txnManager.beginSleeTransaction();

            ReadOnlyTestsProfileCMP profileCMP = (ReadOnlyTestsProfileCMP)profileTable.find(PROFILE_NAME);
            profileCMP.setValue("newValue2");
            msgSender.sendLogMsg("Set new value for profile attribute via CMP interface.");

            txn.commit();
            txn = null;

        } catch (Exception e) {
            msgSender.sendException(e);
            return false;
        } finally {
            try {
                if (txn!=null)
                    txn.rollback();
            } catch (Exception ex) {
                ra.getLog().warning("Error occured when trying to rollback RA started TXN.", ex);
                msgSender.sendLogMsg("Exception occurred when trying to safely rollback RA started TXN: "+ex.getMessage());
            }
        }

        return true;
    }

    private boolean test_CHECK() {
        SleeTransactionManager txnManager = ra.getResourceAdaptorContext().getSleeTransactionManager();
        SleeTransaction txn=null;

        try {
            ProfileTable profileTable = ra.getResourceAdaptorContext().getProfileTable(PROFILE_TABLE_NAME);
            //start a new TXN as profileTable.find is mandatory transactional
            txn = txnManager.beginSleeTransaction();

            ReadOnlyTestsProfileCMP profileCMP = (ReadOnlyTestsProfileCMP)profileTable.find(PROFILE_NAME);
            if (!profileCMP.getValue().equals("newValue")) {
                msgSender.sendFailure(1110069, "Failed to use set/get accessor methods properly.");
                return false;
            }
            msgSender.sendLogMsg("Checked that new value has been successfully set and could be obtained by 'get' accessor.");

            txn.commit();
            txn = null;

        } catch (Exception e) {
            msgSender.sendException(e);
            return false;
        } finally {
            try {
                if (txn!=null)
                    txn.rollback();
            } catch (Exception ex) {
                ra.getLog().warning("Error occured when trying to rollback RA started TXN.", ex);
                msgSender.sendLogMsg("Exception occurred when trying to safely rollback RA started TXN: "+ex.getMessage());
            }
        }

        return true;
    }

    private boolean test_UPDATE() {
        SleeTransactionManager txnManager = ra.getResourceAdaptorContext().getSleeTransactionManager();
        SleeTransaction txn=null;

        try {
            ProfileTable profileTable = ra.getResourceAdaptorContext().getProfileTable(PROFILE_TABLE_NAME);
            //start a new TXN as profileTable.find is mandatory transactional
            txn = txnManager.beginSleeTransaction();

            ReadOnlyTestsProfileCMP profileCMP = (ReadOnlyTestsProfileCMP)profileTable.find(PROFILE_NAME);
            profileCMP.setValue("newValue");
            msgSender.sendLogMsg("Set new value for profile attribute.");

            txn.commit();
            txn = null;

        } catch (Exception e) {
            msgSender.sendException(e);
            return false;
        } finally {
            try {
                if (txn!=null)
                    txn.rollback();
            } catch (Exception ex) {
                ra.getLog().warning("Error occured when trying to rollback RA started TXN.", ex);
                msgSender.sendLogMsg("Exception occurred when trying to safely rollback RA started TXN: "+ex.getMessage());
            }
        }

        return true;
    }

    private boolean test_CREATE() {
        SleeTransactionManager txnManager = ra.getResourceAdaptorContext().getSleeTransactionManager();
        SleeTransaction txn=null;

        try {
            ProfileTable profileTable = ra.getResourceAdaptorContext().getProfileTable(PROFILE_TABLE_NAME);
            //start a new TXN as profileTable.find is mandatory transactional
            txn = txnManager.beginSleeTransaction();

            ReadOnlyTestsProfileCMP profileCMP = (ReadOnlyTestsProfileCMP)profileTable.create(PROFILE_NAME);
            profileCMP.getValue(); //Dummy call to raise NPE if profileCMP was set to 'null' by the ProfileTable.create() method
            msgSender.sendLogMsg("Created profile "+PROFILE_NAME);

            txn.commit();
            txn = null;

        } catch (Exception e) {
            msgSender.sendException(e);
            return false;
        } finally {
            try {
                if (txn!=null)
                    txn.rollback();
            } catch (Exception ex) {
                ra.getLog().warning("Error occured when trying to rollback RA started TXN.", ex);
                msgSender.sendLogMsg("Exception occurred when trying to safely rollback RA started TXN: "+ex.getMessage());
            }
        }

        return true;
    }

    private BaseMessageSender msgSender;
    private RMIObjectChannel out;

}

