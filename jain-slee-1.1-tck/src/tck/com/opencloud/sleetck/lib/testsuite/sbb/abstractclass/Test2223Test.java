/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.sbb.abstractclass;

import com.opencloud.sleetck.lib.SleeTCKTest;
import com.opencloud.sleetck.lib.SleeTCKTestUtils;
import com.opencloud.sleetck.lib.TCKTestResult;
import com.opencloud.sleetck.lib.resource.TCKActivityID;
import com.opencloud.sleetck.lib.resource.TCKSbbMessage;
import com.opencloud.sleetck.lib.resource.events.TCKResourceEventX;
import com.opencloud.sleetck.lib.resource.testapi.TCKResourceListener;
import com.opencloud.sleetck.lib.resource.testapi.TCKResourceTestInterface;
import com.opencloud.sleetck.lib.testsuite.usage.common.UsageMBeanLookup;
import com.opencloud.sleetck.lib.testutils.BaseTCKResourceListener;
import com.opencloud.sleetck.lib.testutils.FutureResult;
import com.opencloud.sleetck.lib.testutils.jmx.SbbUsageMBeanProxy;

import javax.management.Notification;
import javax.management.NotificationListener;
import javax.slee.management.DeployableUnitID;
import javax.slee.usage.UsageNotification;
import java.rmi.RemoteException;

public class Test2223Test implements SleeTCKTest {

    private static final String SERVICE_DU_PATH_PARAM = "DUPath";
    private static final int COUNTER_TYPE_ASSERTION = 2223;
    private static final int SAMPLE_TYPE_ASSERTION = 2232;

    public void init(SleeTCKTestUtils utils) {
        this.utils = utils;
    }

    /**
     * Perform the actual test.
     */

    public TCKTestResult run() throws Exception {
        result = new FutureResult(utils.getLog());

        TCKResourceTestInterface resource = utils.getResourceInterface();
        TCKActivityID activityID = resource.createActivity("Test522InitialActivity");
        resource.fireEvent(TCKResourceEventX.X1, TCKResourceEventX.X1, activityID, null);

        return result.waitForResultOrFail(utils.getTestTimeout(), "Timeout waiting for test result.", COUNTER_TYPE_ASSERTION);
    }

    /**
     * Do all the pre-run configuration of the test.
     */

    public void setUp() throws Exception {
        utils.getLog().fine("Connecting to resource");
        resourceListener = new TCKResourceListenerImpl();
        utils.getResourceInterface().setResourceListener(resourceListener);
        utils.getLog().fine("Installing and activating service");

        String duPath = utils.getTestParams().getProperty(SERVICE_DU_PATH_PARAM);
        duID = utils.install(duPath);

        // Sort out listeners AFTER the service has been installed.
        listener = new UsageNotificationListenerImpl();
        mBeanLookup = new UsageMBeanLookup("Test2223TestService","Test2223Test",utils);

        usagembean = mBeanLookup.getUnnamedSbbUsageMBeanProxy();
        usagembean.addNotificationListener(listener, null, null);

        utils.activateServices(duID, true);
    }

    /**
     * Clean up after the test.
     */

    public void tearDown() throws Exception {

        if (usagembean != null) usagembean.removeNotificationListener(listener);
        if(mBeanLookup != null) mBeanLookup.closeAllMBeans();

        utils.getLog().fine("Cleaning up activities");
        utils.getResourceInterface().clearActivities();
        utils.getLog().fine("Deactivating and uninstalling service");
        utils.deactivateAllServices();
        utils.uninstallAll();
        utils.getLog().fine("Disconnecting from resource");
        utils.getResourceInterface().removeResourceListener();
    }

    private class TCKResourceListenerImpl extends BaseTCKResourceListener {
        public synchronized void onSbbMessage(TCKSbbMessage message, TCKActivityID calledActivity) throws RemoteException {
            result.setError("Unexpected sbb message received: "+message);
        }

        public void onException(Exception e) throws RemoteException {
            utils.getLog().warning("Received exception from SBB");
            utils.getLog().warning(e);
            result.setError(e);
        }
    }

    public class UsageNotificationListenerImpl implements NotificationListener {
        public synchronized final void handleNotification(Notification notification, Object o) {
            if (notification instanceof UsageNotification) {
                UsageNotification n = (UsageNotification) notification;

                String parameterName = n.getUsageParameterName();
                if(parameterName.equals("foo")) {
                    if (n.getValue() == 10) receivedCounterNotification = true;
                    else result.setFailed(COUNTER_TYPE_ASSERTION, "Value of 'foo' usage parameter notification was not 10 as it should have been.");
                } else if(parameterName.equals("bar")) {
                    if (n.getValue() == 20) receivedSampleNotification = true;
                    else result.setFailed(SAMPLE_TYPE_ASSERTION, "Value of 'bar' usage parameter notification was not 20 as it should have been.");
                } else result.setError("Received notification for unrecognized usage parameter name: "+parameterName);

                if(receivedCounterNotification && receivedSampleNotification) result.setPassed();
            }
        }
    }

    private NotificationListener listener;
    private SbbUsageMBeanProxy usagembean;
    private SleeTCKTestUtils utils;
    private TCKResourceListener resourceListener;
    private FutureResult result;
    private DeployableUnitID duID;
    private UsageMBeanLookup mBeanLookup;

    private boolean receivedCounterNotification;
    private boolean receivedSampleNotification;

}
