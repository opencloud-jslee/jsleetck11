/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.transactions;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;

import javax.naming.Context;
import javax.naming.NamingException;
import javax.slee.TransactionRequiredLocalException;
import javax.slee.transaction.SleeTransaction;
import javax.slee.transaction.SleeTransactionManager;
import javax.transaction.HeuristicMixedException;
import javax.transaction.HeuristicRollbackException;
import javax.transaction.NotSupportedException;
import javax.transaction.RollbackException;
import javax.transaction.Synchronization;
import javax.transaction.SystemException;
import javax.transaction.Transaction;
import javax.transaction.TransactionManager;
import javax.transaction.xa.XAResource;

import com.opencloud.logging.StdErrLog;
import com.opencloud.sleetck.lib.profileutils.BaseMessageSender;
import com.opencloud.sleetck.lib.rautils.MessageHandler;
import com.opencloud.sleetck.lib.rautils.RMIObjectChannel;
import com.opencloud.sleetck.lib.rautils.TCKRAUtils;

/**
 * Provides callback method for handling messages sent from the TCK test to the RA.
 * The actual test code is located in the 'handleMessage()' method.
 */
public class Test1109218MessageListener extends UnicastRemoteObject implements MessageHandler {

    private Test1109218ResourceAdaptor ra;

    public Test1109218MessageListener(Test1109218ResourceAdaptor ra) throws RemoteException {
        super();
        try {
            out = TCKRAUtils.lookupRMIObjectChannel();
            msgSender = new BaseMessageSender(out, new StdErrLog());
        }
        catch (Exception e) {
            ra.getLog().warning("Exception occurred when trying to acquire RMIObjectChannel: ",e);
        }
        this.ra = ra;
    }

    public boolean handleMessage(Object message) throws RemoteException {
        SleeTransactionManager sTxManager = null;

        try {

            try {
                // 1109218: Test that getSleeTransactionManager is a subclass of javax.transaction.TransactionManager.
                if (ra.getTransactionManager() instanceof TransactionManager)
                    msgSender.sendLogMsg("javax.slee.transaction.SleeTransactionManager extends javax.transaction.TransactionManager as expected.");
                else {
                    msgSender.sendFailure(1109218, "javax.slee.transaction.SleeTransactionManager does not extend javax.transaction.TransactionManager.");
                    return true;
                }
            } catch (NamingException e) {
                msgSender.sendError("Exception occured when trying to obtain TransactionManager via RA's getTransactionManager() method: ",e);
                return true;
            }


            sTxManager = ra.getTransactionManager();
            SleeTransaction sTxn;
            // 1109228: getSleeTransaction() is a non-transactional method; here, check that it can be called outside of TXN context.
            // 1109229: Returns: ... null if the thread is not currently associated with a transaction.
            try {
                sTxn = sTxManager.getSleeTransaction();
                msgSender.sendLogMsg("getSleeTransaction() showed non-transactional behaviour and could be successfully called outside of a TXN context.");
            } catch (TransactionRequiredLocalException e) {
                msgSender.sendFailure(1109228, "getSleeTransaction() is supposed to behave non-transactional but threw a TransactionRequiredLocalException " +
                        "when called without a transactional context.");
                return true;
            }
            if (sTxn != null) {
                msgSender.sendFailure(1109229, "getSleeTransaction() didn't return null though the current thread is not yet associated with a TXN.");
                return true;
            } else {
                msgSender.sendLogMsg("getSleeTransaction() returned 'null' as expected when called while the current thread is not yet associated with a TXN.");
            }

            // 1109222: beginSleeTransaction() is a non-transactional method; here, check that it can be called outside of TXN context
            // 1109223: beginSleeTransaction() returns a new SleeTransaction object representing the new TXN
            try {
                sTxn = sTxManager.beginSleeTransaction();
            } catch (TransactionRequiredLocalException e) {
                msgSender.sendFailure(1109222, "beginSleeTransaction() is supposed to be non-transactional but threw TransactionRequiredLocalException when called outside " +
                        "of a valid TXN.");
                return true;
            } catch (Exception e) {
                //this could be a ClassCastException if type does not match or something
                //else going wrong in beginSleeTransaction()
                msgSender.sendFailure(1109223, "beginSleeTransaction() was supposed to return a SleeTransaction object " +
                        "representing the new TXN but threw exception: ", e);
                return true;
            }

            if (sTxn==null) {
                msgSender.sendFailure(1109223, "beginSleeTransaction() was supposed to return a SleeTransaction object " +
                        "representing the new TXN but returned 'null'.");
                return true;
            }


            // 1109222: beginSleeTransaction() is a non-transactional method;
            // here, check that it can be called inside of TXN context though it may throw a
            // javax.transaction.NotSupportedException
            try {
                sTxManager.beginSleeTransaction();
            } catch (NotSupportedException e) {
                msgSender.sendLogMsg("When calling beginSleeTransaction() with a valid TXN context a javax.transaction.NotSupportedException was thrown as expected.");
            } catch (Exception e) {
                msgSender.sendFailure(1109222, "When calling beginSleeTransaction() with a valid TXN context this should either pass without problems " +
                        "if nested TXNs are supported or cause a javax.transaction.NotSupportedException. Wrong type of exception was thrown: "+e.getClass().getName());
                return true;
            }

            msgSender.sendLogMsg("beginSleeTransaction() correctly behaved non-transactional and returned a SleeTransaction object " +
            "representing the new TXN.");

            // 1109228: getSleeTransaction() is a non-transactional method; here, check that it can be called with valid TXN context.
            SleeTransaction sTxn2;
            try {
                sTxn2 = sTxManager.getSleeTransaction();
            } catch (Exception e) {
                msgSender.sendFailure(1109228, "getSleeTransaction() should behave non-transactional but threw exception when called from within a valid TXN context: ",e);
                return true;
            }

            // 1109344: after beginSleeTransaction() returns a new transaction has become associated with the calling thread.
            if (sTxn2==null) {
                msgSender.sendFailure(1109344, "After beginSleeTransaction() returned, a new TXN should have been associated with the " +
                        "current thread but getSleeTransaction() still returned 'null'");
                return true;
            }
            // 1109229: getSleeTransaction() returns: the SleeTransaction object that represents the transaction context of the calling thread, ...
            //it has already been checked that getSleeTransaction() didnt return 'null' this time so
            //we just have to assure that it really is the same TXN as previously returned by beginSleeTransaction()
            if (!sTxn.equals(sTxn2)) {
                msgSender.sendFailure(1109229, "getSleeTransaction() is supposed to return the TXN associated with the current thread " +
                        "but returned a different TXN (determined via TXN.equals()) than beginSleeTransaction() returned previously.");
                return true;
            } else {
                msgSender.sendLogMsg("getSleeTransaction() returned the TXN associated with the current thread as expected.");
            }

            //get rid of the TXN
            sTxn.commit();

            //1109221: This method is equivalent to: SleeTransactionManager txManager = ...;
            //txManager.begin();       SleeTransaction tx = txManager.getSleeTransaction();
            //all we do here is to check that this procedure can be used to get 'some' new TXN
            //we can't check that it is the same that beginSleeTransaction() would return if called
            sTxManager.begin();
            sTxn = sTxManager.getSleeTransaction();


            //1109055: The SLEE must implement the SleeTransaction object’s equals method to allow comparison between the target object and another SleeTransaction object. The equals method should return
            //true if the target object and the parameter object both refer to the same global transaction
            //sTxn2 still holds a reference to the first TXN that has been committed previously
            try {
                if (!sTxn.equals(sTxn) || !sTxn.equals(sTxManager.getSleeTransaction()) ||
                        sTxn.equals(sTxn2) || sTxn.equals(null)) {
                    msgSender.sendFailure(1109055, "SleeTransaction.equals() is not properly implemented.");
                    return true;
                }
                msgSender.sendLogMsg("SleeTransaction.equals() worked as expected.");
            } catch (Exception e) {
                msgSender.sendFailure(1109055, "Exception occured when testing the implementation of SleeTransaction.equals(): ", e);
                return true;
            }

            //1109136: SLEE TXN must override the toString() method thus that if two SleeTransaction objects are equal their String representations must be equal; conversely if two SleeTransaction objects are not equal their String representations must not be equal.
            try {
                String sTxnStr = sTxn.toString();
                String sTxn2Str = sTxn2.toString();
                if (!sTxnStr.equals(sTxManager.getSleeTransaction().toString()) ||
                        sTxnStr.equals(sTxn2Str)) {
                    msgSender.sendFailure(1109136, "SleeTransaction.toString() is not properly implemented.");
                    return true;
                }
                msgSender.sendLogMsg("SleeTransaction.toString() worked as expected.");
            } catch (Exception e) {
                msgSender.sendFailure(1109136, "Exception occured when testing the implementation of SleeTransaction.toString(): ", e);
                return true;
            }

            //1109056: In addition, the SLEE must implement the SleeTransaction object’s
            //hashCode method so that if two SleeTransaction objects are equal,
            //they have the same hash code. However, the converse is not necessarily true.
            //Two SleeTransaction objects with the same hash code are not necessarily equal.
            try {
                if (sTxn.hashCode()!=sTxn.hashCode() || sTxn.hashCode()!=sTxManager.getSleeTransaction().hashCode()) {
                    msgSender.sendFailure(1109056, "SleeTransaction.hashCode() is not properly implemented.");
                    return true;
                }
                msgSender.sendLogMsg("SleeTransaction.hashCode() worked as expected.");
            } catch (Exception e) {
                msgSender.sendFailure(1109056, "Exception occured when testing the implementation of SleeTransaction.hashCode(): ", e);
                return true;
            }

            //1109233: asSleeTransaction() returns: a SleeTransaction object for the specified transaction.
            try {
                sTxn2 = sTxManager.asSleeTransaction(sTxManager.getTransaction());
                msgSender.sendLogMsg("asSleeTransaction() returned SleeTransaction object for getTransaction().");
            } catch (Exception e) {
                msgSender.sendFailure(1109233, "Exception occurred when trying to return" +
                        " a SleeTransaction object for getTransaction() via asSleeTransaction().");
                return true;
            }

            //1109227: getSleeTransaction() is equivalent to:  SleeTransactionManager txManager = ...;
            //SleeTransaction tx = txManager.asSleeTransaction(txManager.getTransaction());
            if (!sTxn2.equals(sTxManager.getSleeTransaction())) {
                msgSender.sendFailure(1109227, "getSleeTransaction() returned different TXN than asSleeTransaction(getTransaction()) (determined via TXN.equals())");
                return true;
            } else {
                msgSender.sendLogMsg("getSleeTransaction() was equivalent to asSleeTransaction(getTransaction()). According to the TXN's equals() method both " +
                        "returned the same TXN.");
            }

            //1109234: Throws: java.lang.NullPointerException - if tx is null.
            try {
                sTxManager.asSleeTransaction(null);
                msgSender.sendFailure(1109234, "SleeTransactionManager.asSleeTransaction(null) did not throw NullPointerException.");
                return true;
            } catch (NullPointerException e) {
                msgSender.sendLogMsg("asSleeTransaction(null) threw NullPointerException as expected.");
            }

            //1109235: Throws java.lang.IllegalArgumentException - if tx represents a transaction
            //that was not started by this transaction manager.
            try {
                sTxn = sTxManager.asSleeTransaction(new MyTransaction());
                msgSender.sendFailure(1109235, "SleeTransactionManager.asSleeTransaction(MyTransaction()) did not throw IllegalArgumentException.");
                return true;
            } catch (IllegalArgumentException e) {
                msgSender.sendLogMsg("SleeTransactionManager.asSleeTransaction(MyTransaction()) threw IllegalArgumentException as expected.");
            }

            msgSender.sendSuccess(1109218, "Test completed successfully.");
            return true;
        } catch (Exception e) {
            msgSender.sendException(e);
            return true;
        } finally {
            // clean up.
            if (sTxManager!=null) {
                try {
                    sTxManager.getSleeTransaction().commit();
                } catch (Exception e) {
                    msgSender.sendLogMsg("Error occurred when trying to cleanup existing TXNs, this however is not the " +
                            "immediate reason for an eventual test failure/error: "+e.toString());
                }
            }
        }
    }

    private class MyTransaction implements Transaction {

        public void commit() throws RollbackException, HeuristicMixedException, HeuristicRollbackException, SecurityException, SystemException {
        }

        public boolean delistResource(XAResource arg0, int arg1) throws IllegalStateException, SystemException {
            return false;
        }

        public boolean enlistResource(XAResource arg0) throws RollbackException, IllegalStateException, SystemException {
            return false;
        }

        public int getStatus() throws SystemException {
            return 0;
        }

        public void registerSynchronization(Synchronization arg0) throws RollbackException, IllegalStateException, SystemException {
        }

        public void rollback() throws IllegalStateException, SystemException {
        }

        public void setRollbackOnly() throws IllegalStateException, SystemException {
        }

    }

    private BaseMessageSender msgSender;
    private RMIObjectChannel out;

}

