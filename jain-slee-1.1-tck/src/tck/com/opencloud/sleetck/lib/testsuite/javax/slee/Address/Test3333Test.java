/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.javax.slee.Address;

import com.opencloud.sleetck.lib.SleeTCKTest;
import com.opencloud.sleetck.lib.SleeTCKTestUtils;
import com.opencloud.sleetck.lib.TCKTestResult;

import javax.slee.Address;
import javax.slee.AddressPlan;

public class Test3333Test implements SleeTCKTest {

    private static final int TEST_ID = 3333;

    public void init(SleeTCKTestUtils utils) {}

    /**
     * Perform the actual test.
     */

    public TCKTestResult run() throws Exception {

        Address address = null;
        try {
            address = new Address(AddressPlan.IP, "127.0.0.1", null, null, null, null);
        } catch (Exception e) {
            return TCKTestResult.failed(TEST_ID, "new Address(AddressPlan, String, AddressPresentation, AddressScreening, String, String) threw an exception.");
        }

        try {
            address.getAddressPlan();
        } catch (Exception e) {
            return TCKTestResult.failed(3336, "Address.getAddressPlan() threw an exception.");
        }

        try {
            address.getAddressString();
        } catch (Exception e) {
            return TCKTestResult.failed(3338, "Address.getAddressString() threw an exception.");
        }

        try {
            address.getAddressPresentation();
        } catch (Exception e) {
            return TCKTestResult.failed(3340, "Address.getAddressPresentation() threw an exception.");
        }

        try {
            address.getAddressScreening();
        } catch (Exception e) {
            return TCKTestResult.failed(3342, "Address.getAddressScreening() threw an exception.");
        }

        try {
            address.getSubAddressString();
        } catch (Exception e) {
            return TCKTestResult.failed(3344, "Address.getSubAddressString() threw an exception.");
        }

        try {
            address.getAddressName();
        } catch (Exception e) {
            return TCKTestResult.failed(3346, "Address.getAddressName() threw an exception.");
        }

        try {
            address.hashCode();
        } catch (Exception e) {
            return TCKTestResult.failed(3350, "Address.hashCode() threw an exception.");
        }

        try {
            address.toString();
        } catch (Exception e) {
            return TCKTestResult.failed(3352, "Address.toString() threw an exception.");
        }

        if (!address.equals(address))
            return TCKTestResult.failed(3348, "Address.equals(Address) returned false comparing object with itself.");

        Address secondAddress = new Address(AddressPlan.IP, "127.0.0.2");
        if (address.equals(secondAddress))
            return TCKTestResult.failed(3348, "Address.equals(Address) returned true for two different Addresses.");

        return TCKTestResult.passed();
    }


    /**
     * Do all the pre-run configuration of the test.
     */

    public void setUp() throws Exception {
    }

    /**
     * Clean up after the test.
     */

    public void tearDown() throws Exception {
    }

}
