/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.runtime.security;

import java.net.SocketPermission;
import java.security.AccessControlException;
import java.security.AccessController;
import java.security.Permission;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.PropertyPermission;

import com.opencloud.sleetck.lib.TCKTestErrorException;
import com.opencloud.sleetck.lib.TCKTestFailureException;
import com.opencloud.sleetck.lib.sbbutils.BaseTCKSbb;
import com.opencloud.sleetck.lib.sbbutils.TCKSbbUtils;

public abstract class Test1112013SbbChild extends BaseTCKSbb {

    /**
     * If the “isolate-security-permissions” attribute of the sbb-local-interface
     * element in the SBB’s deployment descriptor is “False”, then invoked business
     * methods on that SBB's local interface run with an access control context
     * including the protections domains of the SBB with the protection domains of
     * other classes (methods?) on the call stack.
     * 
     */
    public void checkPermissions() throws Exception {
        getSbbContext().getTracer("Test1112013SbbChild").fine("Child SBB doing checkPermissions()");
        checkDeny(new SocketPermission("*","accept"),1112009);
        checkDeny(new SocketPermission("*","listen"),1112009);

        checkDeny(new PropertyPermission("tcktest.parent", "write"), 1112009);
        checkGrant(new PropertyPermission("tcktest.child", "write"), 1112009);
        checkGrant(new PropertyPermission("tcktest.childonly", "write"), 1112009);
        checkDeny(new PropertyPermission("no.such.property", "write"), 1112009);
        getSbbContext().getTracer("Test1112013SbbChild").fine("Child SBB sending response.");
        sendResponse();
    }

    protected void sendResponse() throws Exception {
        if (failedResults.isEmpty()) {
            // send a positive ACK to the test
            TCKSbbUtils.getResourceInterface().sendSbbMessage("All tested permissions granted/denied as appropriate");
        } else {
            StringBuffer buf = new StringBuffer();
            buf.append("Some assertions failed:");
            for (Iterator i=failedResults.iterator(); i.hasNext(); )
                buf.append('\n').append(i.next());
            throw new TCKTestFailureException(firstFailedAssertion, buf.toString());
        }
    }

    /**
     * Checks that the SLEE will grant the Sbb the given permission
     */
    protected void checkGrant(Permission permission, int assertionID) throws TCKTestErrorException {
        try {
            AccessController.checkPermission(permission);
            getSbbContext().getTracer("Test1112013SbbChild").info("The SLEE granted the SBB permission " + permission + " as expected");
        } catch (AccessControlException ace) {
            failedResults.add("The SLEE denied the SBB a permission which it was expected to grant: " + permission + " (assertion " + assertionID + ")");
            if(firstFailedAssertion == -1) firstFailedAssertion = assertionID;
        } catch (Exception e) {
            throw new TCKTestErrorException("Caught unexpected Exception from AccessController.checkPermission(). "+
                    "Permission="+permission,e);
        }
    }

    /**
     * Checks that the SLEE will deny the Sbb the given permission
     */
    protected void checkDeny(Permission permission, int assertionID) throws TCKTestErrorException {
        try {
            AccessController.checkPermission(permission);
            failedResults.add("The SLEE granted the Sbb a permission which it was expected to deny: " + permission + " (assertion " + assertionID + ")");
            if(firstFailedAssertion == -1) firstFailedAssertion = assertionID;
        } catch (AccessControlException ace) {
            getSbbContext().getTracer("Test1112013SbbChild").info("The SLEE denied the SBB permission " + permission + " as expected");
        } catch (Exception e) {
            throw new TCKTestErrorException("Caught unexpected Exception from AccessController.checkPermission(). "+
                    "Permission="+permission,e);
        }
    }


    protected final ArrayList failedResults = new ArrayList();
    private int firstFailedAssertion = -1;
    
}
