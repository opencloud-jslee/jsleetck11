/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.resource.endpoint;

import com.opencloud.sleetck.lib.TCKTestFailureException;
import com.opencloud.sleetck.lib.TCKTestResult;
import com.opencloud.sleetck.lib.testsuite.resource.BaseResourceTest;
import com.opencloud.sleetck.lib.testsuite.resource.RAMethods;

/**
 * Test to ensure that fireEventTransacted() only delivers events if the
 * containing transaction commits.
 * <p>
 * Test assertion: 1115306, 1115307
 */
public class Test1115306Test extends BaseResourceTest {

    public TCKTestResult run() throws Exception {
        int sequenceID = nextMessageID();

        MultiResponseListener listener = new MultiResponseListener(sequenceID);

        listener.addExpectedResult("result-ra");
        listener.addExpectedResult("result-sbba");
        listener.addExpectedResult("result-sbbb");

        sendMessage(RAMethods.fireEventTransacted, new Integer(1115306), listener, sequenceID);

        getLog().info("Checking for RA result");
        Object result1 = listener.getResult("result-ra");
        checkResult(result1, 1115306);

        getLog().info("Checking for sbb result corresponding to transaction A");
        Object result2 = listener.getResult("result-sbba");
        checkResult(result2, 1115306);

        getLog().info("Checking for sbb result corresponding to transaction B (this should timeout)");
        Object result3 = listener.getResult("result-sbbb");
        if (result3 != null)
            throw new TCKTestFailureException(1115307, "Event delivery still occured when fireEventTransacted() was called in a rolled back transaction");

        return TCKTestResult.passed();
    }
}
