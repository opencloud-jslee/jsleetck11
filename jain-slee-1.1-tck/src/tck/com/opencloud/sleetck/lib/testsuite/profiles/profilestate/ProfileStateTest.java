/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.profiles.profilestate;

import com.opencloud.sleetck.lib.*;
import com.opencloud.sleetck.lib.testutils.jmx.*;
import com.opencloud.sleetck.lib.testsuite.profiles.ProfileTestConstants;
import com.opencloud.sleetck.lib.testsuite.profiles.simpleprofile.SimpleProfileProxy;
import com.opencloud.sleetck.lib.testutils.ProfileUtils;
import com.opencloud.sleetck.lib.testutils.ComponentIDLookup;
import com.opencloud.sleetck.lib.testutils.Assert;
import com.opencloud.sleetck.lib.testutils.jmx.ProfileMBeanProxy;

import javax.slee.profile.*;

import com.opencloud.sleetck.lib.infra.SleeTCKComponentConstants;

import javax.management.ObjectName;
import javax.slee.management.ManagementException;
import javax.slee.InvalidStateException;
import java.util.Vector;
import java.util.Iterator;


/**
 * Test the lifecycle of a profile mbean, and the mbean state interrogation methods isProfileDirty
 * and isProfileWritable
 */
public class ProfileStateTest extends AbstractSleeTCKTest {

    protected static final String PROFILE_TABLE_NAME = "tck.ProfileStateTest.table";
    public static final String PROFILE_SPEC_NAME = "SimpleProfile";
    protected static final String PROFILE_NAME = "A";

    public static final String VALUE_1 = "value1";
    public static final String VALUE_2 = "value2";

    /**
     * Run a series of tests on the profile state methods <code>isProfileDirty</coded> and <code>isProfileWritable</code>.
     * Also tests that <code>javax.slee.InvalidStateException</code> is thrown where appropriate
     * @return
     * @throws Exception
     */
    public TCKTestResult run() throws Exception {
        // create the profile table
        ProfileProvisioningMBeanProxy profileProvisioning = profileUtils.getProfileProvisioningProxy();
        ProfileSpecificationID profileSpecID = new ComponentIDLookup(utils()).lookupProfileSpecificationID(
                PROFILE_SPEC_NAME, SleeTCKComponentConstants.TCK_VENDOR, "1.0");

        getLog().info("Creating profile table: " + PROFILE_TABLE_NAME);
        profileProvisioning.createProfileTable(profileSpecID, PROFILE_TABLE_NAME);
        tableCreated = true;

        // add a profile to work with
        getLog().info("Creating profile " + PROFILE_NAME);
        ObjectName jmxObjectName = profileProvisioning.createProfile(PROFILE_TABLE_NAME, PROFILE_NAME);

        SimpleProfileProxy proxy = getProfileProxy(jmxObjectName);

        // Assertion 1111: When the ProfileMBean is created it should be in the read-write state
        Assert.assertTrue(1111, "After creation the ProfileMBean should be in the read-write state",
                          proxy.isProfileWriteable());

        proxy.commitProfile();

        // Get the profile in the read-only state.
        jmxObjectName = profileProvisioning.getProfile(PROFILE_TABLE_NAME, PROFILE_NAME);
        proxy = getProfileProxy(jmxObjectName);
        
        // Assertion 4384: isWritable() will return false if the profile is not in a writable state
        Assert.assertTrue(4384, "isWritable() should return false when the profile is not in a writable state",
                          !proxy.isProfileWriteable());

        // Assertion 4381: isProfileDirty() will return false if the profile has not been modified
        Assert.assertTrue(4381, "The profile has not been modified so isProfileDirty() should return false",
                          !proxy.isProfileDirty());


        proxy.editProfile();
        // Assertion 4384: isWritable() will return true if the profile is in a writable state
        Assert.assertTrue(4384, "isWritable() should return true when the profile is in a writable state",
                          proxy.isProfileWriteable());

        proxy.setValue(VALUE_1);

        // Assertion 4381: isProfileDirty() will return true if the profile has been modified
        Assert.assertTrue(4381, "The profile has been modified so isProfileDirty() should return true",
                          proxy.isProfileDirty());

        proxy.commitProfile();


        // Assertion 4381: isProfileDirty() will return false if the profile is not editable
        Assert.assertTrue(4381, "The profile changes have been committed so isProfileDirty() should return false",
                          !proxy.isProfileDirty());

        // Assertion 4366: Attempt to modify non-writable profile will throw InvalidStateException
        try {
            proxy.commitProfile();
            // If we get here no exception was thrown
            throw new TCKTestFailureException(4366, "Expected javax.slee.InvalidStateException exception not thrown");
        } catch (InvalidStateException e) { }

        // Assertion 4374: restoreProfile() when the client does not have read/write access
        // to the profile will throw an InvalidStateException
        try {
            proxy.restoreProfile();
            throw new TCKTestFailureException(4374, "Expected javax.slee.InvalidStateException not thrown");
        } catch (InvalidStateException e) { }

        // Assertion 4378: closeProfile should return an InvalidStateException if the profile has uncommitted changes
        proxy.editProfile();
        proxy.setValue(VALUE_2);
        try {
            proxy.closeProfile();
            throw new TCKTestFailureException(4378, "closeProfile() should throw a javax.slee.InvalidStateException " +
                                                    "when the profile has uncommitted changes");
        } catch (InvalidStateException e) { }

        proxy.restoreProfile();

        proxy.closeProfile();

        return TCKTestResult.passed();
    }

    public void setUp() throws Exception {
        String duPath = utils().getTestParams().getProperty(ProfileTestConstants.PROFILE_SPEC_DU_PATH_PARAM);
        getLog().fine("Installing profile specification");
        utils().install(duPath);
        profileUtils = new ProfileUtils(utils());
        activeProxies = new Vector();
    }

    public void tearDown() throws Exception {
        // close profiles
        if (activeProxies != null) {
            getLog().fine("Closing profiles");
            Iterator activeProxiesIter = activeProxies.iterator();
            while (activeProxiesIter.hasNext()) {
                SimpleProfileProxy aProxy = (SimpleProfileProxy) activeProxiesIter.next();
                try {
                    if (aProxy != null) {
                        if (aProxy.isProfileWriteable()) aProxy.restoreProfile();
                        aProxy.closeProfile();
                    }
                } catch (Exception ex) {
                    getLog().warning("Exception caught while trying to close profiles:");
                    getLog().warning(ex);
                }
            }
        }
        // delete profile tables
        try {
            if (profileUtils != null && tableCreated) {

                try {
                    ProfileProvisioningMBeanProxy proxy = profileUtils.getProfileProvisioningProxy();
                    proxy.removeProfile(PROFILE_TABLE_NAME, PROFILE_NAME);
                } catch (Exception e) {
                }

                getLog().fine("Removing profile table");
                profileUtils.removeProfileTable(PROFILE_TABLE_NAME);
            }
        } catch (Exception e) {
            getLog().warning("Caught exception while trying to remove profile table:");
            getLog().warning(e);
        }
        super.tearDown();
    }

    /**
     * Determines whether a profile is read only by attempting to temporary change the value of the CMP field 'value'
     * @param proxy
     * @return
     * @throws TCKTestErrorException
     * @throws ManagementException
     */
    private boolean isProfileReadOnly(SimpleProfileProxy proxy) throws TCKTestErrorException, ManagementException {
        try {
            String oldValue = proxy.getValue();
            proxy.setValue("isProfileReadOnly?");
            proxy.setValue(oldValue);
            return false;  // We were able to write to the profile, so it is not read only
        } catch (InvalidStateException e) {
            return true;   // Profile is read only
        }
    }

    private SimpleProfileProxy getProfileProxy(ObjectName mbeanName) {
        SimpleProfileProxy rProxy = new SimpleProfileProxy(mbeanName, utils().getMBeanFacade());
        activeProxies.addElement(rProxy);
        return rProxy;
    }

    private ProfileUtils profileUtils;
    private boolean tableCreated = false;
    private Vector activeProxies;

}
