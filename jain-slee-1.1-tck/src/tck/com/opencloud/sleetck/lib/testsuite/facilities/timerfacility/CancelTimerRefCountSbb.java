/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.facilities.timerfacility;

import com.opencloud.sleetck.lib.sbbutils.BaseTCKSbb;
import com.opencloud.sleetck.lib.sbbutils.TCKSbbUtils;
import com.opencloud.sleetck.lib.resource.events.TCKResourceEventX;
import com.opencloud.sleetck.lib.resource.sbbapi.TCKActivity;

import javax.naming.Context;
import javax.naming.InitialContext;

import java.util.HashMap;

import javax.slee.*;
import javax.slee.facilities.*;
import javax.slee.nullactivity.*;

/**
 * Test that a cancelled timer releases its references to the aci.
 */
public abstract class CancelTimerRefCountSbb extends BaseTCKSbb {

    public void setSbbContext(SbbContext context) {
        super.setSbbContext(context);
        try {
            Context myEnv = (Context) new InitialContext().lookup("java:comp/env");

            nullActivityFactory = (NullActivityFactory) myEnv.lookup("slee/nullactivity/factory");
            nullACIFactory = (NullActivityContextInterfaceFactory) myEnv.lookup("slee/nullactivity/activitycontextinterfacefactory");
            timerFacility = (TimerFacility) myEnv.lookup("slee/facilities/timer");
        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }
    }

    // test starts here...
    public void onTCKResourceEventX1(TCKResourceEventX event, ActivityContextInterface aci) {
        try {
            TCKSbbUtils.createTrace(getSbbID(),Level.INFO,"Received start message",null);
            setTestName((String)event.getMessage());

            // Create a null aci
            NullActivity nobody = nullActivityFactory.createNullActivity();
            ActivityContextInterface nobodyACI = nullACIFactory.getActivityContextInterface(nobody);

            // set a timer - this assumes setTimer actually works
            TimerID timerID = timerFacility.setTimer(nobodyACI, null, 0, new TimerOptions());

            // cancel the timer - there should be no refences to the null activity so it
            // will end implicitly, and we will receive the end event.
            timerFacility.cancelTimer(timerID);

            // At this point there should be no references to the null aci, so it will
            // implicitly end when this txn commits.

        } catch(Exception e) {
            TCKSbbUtils.handleException(e);
        }
    }

    public void onActivityEndEvent(ActivityEndEvent event, ActivityContextInterface aci) {
        try {
            if (aci.getActivity() instanceof NullActivity) {

                // This is the end event for our null activity. It should have ended implicitly
                // when its ref count dropped to zero after the call to unbind() above.
                TCKSbbUtils.createTrace(getSbbID(),Level.INFO,"Received NullActivity end event",null);

                // PASSED - we received the activity end event first
                setResultPassed("cancelTimer() decremented aci ref count correctly");

                // Note no failed case - implicitly fails if this end event is never received and the
                // test times out.

                // we are done
                sendResultToTCK();
            }

        } catch(Exception e) {
            TCKSbbUtils.handleException(e);
        }
    }

    // return a static string so that this SBB entity gets selected every time
    public InitialEventSelector getStaticConvergenceName(InitialEventSelector ies) {
        ies.setCustomName("foobar");
        return ies;
    }

    public abstract void setTestName(String testName);
    public abstract String getTestName();

    private void sendResultToTCK() throws Exception {
        HashMap sbbData = new HashMap();
        sbbData.put("testname", getTestName());
        sbbData.put("result", result?"pass":"fail");
        sbbData.put("message", message);
        sbbData.put("id", new Integer(failedAssertionID));
        TCKSbbUtils.getResourceInterface().sendSbbMessage(sbbData);
    }

    private void setResultFailed(int assertionID, String msg) throws Exception {
        result = false;
        failedAssertionID = assertionID;
        message =  "Assertion " + assertionID + " failed: " + msg;
        TCKSbbUtils.createTrace(getSbbID(), Level.WARNING, message, null);
    }

    private void setResultPassed(String msg) throws Exception {
        result = true;
        message = "Success: " + msg;
        TCKSbbUtils.createTrace(getSbbID(), Level.INFO, message, null);
    }

    private NullActivityFactory nullActivityFactory;
    private NullActivityContextInterfaceFactory nullACIFactory;
    private TimerFacility timerFacility;

    private boolean result = false;
    private String message;
    private int failedAssertionID = -1;

}