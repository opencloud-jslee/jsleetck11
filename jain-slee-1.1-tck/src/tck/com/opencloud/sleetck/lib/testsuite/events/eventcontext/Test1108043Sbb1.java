/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.events.eventcontext;

import javax.slee.ActivityContextInterface;
import javax.slee.InitialEventSelector;
import javax.slee.EventContext;
import javax.slee.facilities.Tracer;

import com.opencloud.sleetck.lib.sbbutils.BaseTCKSbb;
import com.opencloud.sleetck.lib.sbbutils.TCKSbbUtils;
import com.opencloud.sleetck.lib.resource.TCKActivityID;
import com.opencloud.sleetck.lib.resource.events.TCKResourceEventX;
import com.opencloud.sleetck.lib.resource.sbbapi.TCKActivity;

import java.util.HashMap;

/**
 * AssertionID(1108043): Test If the transaction does not commit then the 
 * Event Context will not be “resumed” and therefore the event will not be 
 * delivered to any other eligible SBB entities.
 */
public abstract class Test1108043Sbb1 extends BaseTCKSbb {
    public static final String TRACE_MESSAGE_TCKResourceEventX1 = "Test1108043Sbb1:I got TCKResourceEventX1 on ActivityA";

    public static final String TRACE_MESSAGE_TCKResourceEventX2A = "Test1108043Sbb1:I got TCKResourceEventX2 on ActivityA";

    public static final String TRACE_MESSAGE_TCKResourceEventX2B = "Test1108043Sbb1:I got TCKResourceEventX2 on ActivityB";

    public static final String TRACE_MESSAGE_TCKResourceEventX3 = "Test1108043Sbb1:I got TCKResourceEventX3 on ActivityB";

    public InitialEventSelector initialEventSelector(InitialEventSelector ies) {
        ies.setCustomName("test");
        return ies;
    }

    public void onTCKResourceEventX1(TCKResourceEventX event, ActivityContextInterface aci, EventContext context) {
        try {
            //send message to TCK: I got TCKResourceEventX1 on ActivityA
            tracer = getSbbContext().getTracer("com.test");
            tracer.info(TRACE_MESSAGE_TCKResourceEventX1);
            // store context in CMP field
            setEventContext(context);
            //call suspendDelivery() method now, we assumed the default timeout 
            //on the SLEE will be more than 10000ms to complete this test.
            context.suspendDelivery();

        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }
    }

    public void onTCKResourceEventX2(TCKResourceEventX event, ActivityContextInterface aci, EventContext context) {
        try {
            tracer = getSbbContext().getTracer("com.test");

            TCKActivity activity = (TCKActivity) context.getActivityContextInterface().getActivity();
            TCKActivityID activityID = activity.getID();
            if (activityID.getName().equals(Test1108043Test.activityNameB)) {

                //send message to TCK: I got TCKResourceEventX2 on ActivityB
               tracer.info(TRACE_MESSAGE_TCKResourceEventX2B);
               
               //get EventContext ec from CMP field
               EventContext ec = getEventContext();
               try {
                   // call resumeDelivery method now
                    ec.resumeDelivery();
                    }
                    catch (Exception e) {
                        tracer.severe("ERROR onTCKResourceEventX2, the event delivery failed to be resumed");
                        sendResultToTCK("Test1108043Test", false, "SBB1:onTCKResourceEventX2-ERROR: The event delivery failed to be resumed", 1108043);
                        return;
                    }

                tracer.info("Test1108043Sbb1: onTCKResourceEventX2- Enter Rollback.");
                getSbbContext().setRollbackOnly();
                tracer.info("Test1108043Sbb1: onTCKResourceEventX2- Exit Rollback.");
                
                if (!getSbbContext().getRollbackOnly()) {
                    try {
                        if (ec.isSuspended())
                            ec.resumeDelivery();
                    } catch (Exception e) {
                        tracer.severe("Failed to resume the suspended event delivery.");
                    }
                }
                
                return;
            } else if (activityID.getName().equals(Test1108043Test.activityNameA)) {
                //send message to TCK: I got TCKResourceEventX2 on ActivityA
                tracer = getSbbContext().getTracer("com.test");
                tracer.info(TRACE_MESSAGE_TCKResourceEventX2A);
                
                if (!getPassed())
                sendResultToTCK("Test1108043Test", false, "SBB1:onTCKResourceEventX2-ERROR: When the transaction does not commit, then the Event Context "
                                + "will not be resumed, and the event should not been delivered to the event's"
                                + "delivered set.", 1108043);
                return;
            }

        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }
    }

    public void onTCKResourceEventX3(TCKResourceEventX event, ActivityContextInterface aci, EventContext context) {
        try {
            //send message to TCK: I got TCKResourceEventX3 on ActivityB
            tracer = getSbbContext().getTracer("com.test");
            tracer.info(TRACE_MESSAGE_TCKResourceEventX3);
            setPassed(true);
            
            //assert context.isSuspended(), check ec has been suspended or not 
            if (context.isSuspended()) {
                sendResultToTCK("Test1108043Test", false, "SBB1:onTCKResourceEventX3-ERROR: The event delivery should not be suspended, the "
                        + "EventContext.isSuspended() returned true!", 1108043);
                return;
            }
        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }
    }

    public void sbbRolledBack(javax.slee.RolledBackContext context) {
    }

    public abstract void setEventContext(EventContext eventContext);
    public abstract EventContext getEventContext();
    
    private void sendResultToTCK(String testName, boolean result, String message, int failedAssertionID) throws Exception {
        HashMap sbbData = new HashMap();
        sbbData.put("testname", testName);
        sbbData.put("result", result ? "pass" : "fail");
        sbbData.put("message", message);
        sbbData.put("id", new Integer(failedAssertionID));
        TCKSbbUtils.getResourceInterface().sendSbbMessage(sbbData);
    }

    private Tracer tracer = null;

    public abstract void setPassed(boolean passed);
    public abstract boolean getPassed();
}
