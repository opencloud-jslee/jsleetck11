/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.profiles.profiletable;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.Collection;
import java.util.Hashtable;
import java.util.Iterator;

import javax.slee.Address;
import javax.slee.AddressPlan;
import javax.slee.CreateException;
import javax.slee.TransactionRequiredLocalException;
import javax.slee.profile.ProfileAlreadyExistsException;
import javax.slee.profile.ProfileTable;
import javax.slee.transaction.SleeTransaction;
import javax.slee.transaction.SleeTransactionManager;

import com.opencloud.logging.StdErrLog;
import com.opencloud.sleetck.lib.profileutils.BaseMessageSender;
import com.opencloud.sleetck.lib.rautils.MessageHandler;
import com.opencloud.sleetck.lib.rautils.RMIObjectChannel;
import com.opencloud.sleetck.lib.rautils.TCKRAUtils;

/**
 * Provides callback method for handling messages sent from the TCK test to the RA.
 * The actual test code is located in the 'handleMessage()' method.
 */
public class Test1110132_2MessageListener extends UnicastRemoteObject implements MessageHandler {

    public static final String PROFILE_TABLE_NAME = "Test1110132_2ProfileTable";
    public static final String PROFILE_TABLE_NAME2 = "Test1110132_2ProfileTable2";
    public static final String PROFILE_NAME = "Test1110132_2Profile";
    public static final String PROFILE_NAME2 = "Test1110132_2Profile2";
    public static final String PROFILE_NAME3 = "Test1110132_2Profile3";

    private Test1110132_2ResourceAdaptor ra;

    public Test1110132_2MessageListener(Test1110132_2ResourceAdaptor ra) throws RemoteException {
        super();

        try {
            out = TCKRAUtils.lookupRMIObjectChannel();
            msgSender = new BaseMessageSender(out, new StdErrLog());
        }
        catch (Exception e) {
            ra.getLog().warning("Exception occurred when trying to acquire RMIObjectChannel: ",e);
        }

        this.ra = ra;
    }

    public boolean handleMessage(Object message) throws RemoteException {

        //run the tests, if both run through successfully and completely (=both return 'true')
        //send success msg back to TCK test
        if (test_MANDATORY_TXN_BEHAVIOUR() &&
                test_CREATE_COMMIT_PROFILE() && test_CHECK_COMMIT_PROFILE() && test_CREATE_ROLLBACK_PROFILE() &&
                test_CHECK_ROLLBACK_PROFILE() && test_FIND_PROFILE() && test_FIND_PROFILE_INDEXED() &&
                test_FIND_PROFILES_INDEXED() && test_STATIC_QUERY() && test_REMOVE_ROLLBACK_PROFILE() &&
                test_CHECK_REMOVE_ROLLBACK() && test_REMOVE_COMMIT_PROFILE() && test_CHECK_REMOVE_COMMIT()) {

            msgSender.sendSuccess(1110132, "Test successful.");
        }
        return true;
    }

    private boolean test_MANDATORY_TXN_BEHAVIOUR() {

        try {
            Test1110132ProfileTable profileTable = (Test1110132ProfileTable)ra.getResourceAdaptorContext().getProfileTable(PROFILE_TABLE_NAME);

            //1110131: All methods on this (the javax.slee.profile.ProfileTable) interface are mandatory transactional methods.
            try {
                profileTable.create("Dummy");
                msgSender.sendFailure(1110131, "create() was supposed to throw TransactionRequiredLocalException as it was invoked without a TXN context.");
                return false;
            } catch (TransactionRequiredLocalException e) {
                msgSender.sendLogMsg("create() threw TransactionRequiredLocalException as expected.");
            } catch (Exception e) {
                msgSender.sendFailure(1110131, "create() threw wrong type of exception. javax.slee.TransactionRequiredLocalException expected but caught "+e.getClass().getName());
                return false;
            }

            try {
                profileTable.find(PROFILE_NAME);
                msgSender.sendFailure(1110131, "find() was supposed to throw TransactionRequiredLocalException as it was invoked without a TXN context.");
                return false;
            } catch (TransactionRequiredLocalException e) {
                msgSender.sendLogMsg("find() threw TransactionRequiredLocalException as expected.");
            } catch (Exception e) {
                msgSender.sendFailure(1110131, "find() threw wrong type of exception. javax.slee.TransactionRequiredLocalException expected but caught "+e.getClass().getName());
                return false;
            }

            try {
                profileTable.findAll();
                msgSender.sendFailure(1110131, "findAll() was supposed to throw TransactionRequiredLocalException as it was invoked without a TXN context.");
                return false;
            } catch (TransactionRequiredLocalException e) {
                msgSender.sendLogMsg("findAll() threw TransactionRequiredLocalException as expected.");
            } catch (Exception e) {
                msgSender.sendFailure(1110131, "findAll() threw wrong type of exception. javax.slee.TransactionRequiredLocalException expected but caught "+e.getClass().getName());
                return false;
            }

            try {
                profileTable.findProfileByAttribute("intValue", new Integer(42));
                msgSender.sendFailure(1110131, "findProfileByAttribute() was supposed to throw TransactionRequiredLocalException as it was invoked without a TXN context.");
                return false;
            } catch (TransactionRequiredLocalException e) {
                msgSender.sendLogMsg("findProfileByAttribute() threw TransactionRequiredLocalException as expected.");
            } catch (Exception e) {
                msgSender.sendFailure(1110131, "findProfileByAttribute() threw wrong type of exception. javax.slee.TransactionRequiredLocalException expected but caught "+e.getClass().getName());
                return false;
            }

            try {
                profileTable.findProfilesByAttribute("intValue", new Integer(42));
                msgSender.sendFailure(1110131, "findProfilesByAttribute() was supposed to throw TransactionRequiredLocalException as it was invoked without a TXN context.");
                return false;
            } catch (TransactionRequiredLocalException e) {
                msgSender.sendLogMsg("findProfilesByAttribute() threw TransactionRequiredLocalException as expected.");
            } catch (Exception e) {
                msgSender.sendFailure(1110131, "findProfilesByAttribute() threw wrong type of exception. javax.slee.TransactionRequiredLocalException expected but caught "+e.getClass().getName());
                return false;
            }

            try {
                profileTable.remove(PROFILE_NAME);
                msgSender.sendFailure(1110131, "remove() was supposed to throw TransactionRequiredLocalException as it was invoked without a TXN context.");
                return false;
            } catch (TransactionRequiredLocalException e) {
                msgSender.sendLogMsg("remove() threw TransactionRequiredLocalException as expected.");
            } catch (Exception e) {
                msgSender.sendFailure(1110131, "remove() threw wrong type of exception. javax.slee.TransactionRequiredLocalException expected but caught "+e.getClass().getName());
                return false;
            }

            //1110191: Static query methods are mandatory transactional
            try {
                profileTable.queryEqualsValue("42");
                msgSender.sendFailure(1110191, "static query was supposed to throw TransactionRequiredLocalException as it was invoked without a TXN context.");
                return false;
            } catch (TransactionRequiredLocalException e) {
                msgSender.sendLogMsg("static query threw TransactionRequiredLocalException as expected.");
            } catch (Exception e) {
                msgSender.sendFailure(1110191, "static query threw wrong type of exception. javax.slee.TransactionRequiredLocalException expected but caught "+e.getClass().getName());
                return false;
            }


        } catch (Exception e) {
            msgSender.sendException(e);
            return false;
        }

        return true;
    }

    private boolean test_STATIC_QUERY() {
        SleeTransactionManager txnManager = ra.getResourceAdaptorContext().getSleeTransactionManager();
        SleeTransaction txn=null;

        try {
            Test1110132ProfileTable profileTable = (Test1110132ProfileTable)ra.getResourceAdaptorContext().getProfileTable(PROFILE_TABLE_NAME);
            //start a new TXN as profileTable.find is mandatory transactional
            txn = txnManager.beginSleeTransaction();

            //1110188: each query returns a Collection with a ProfileLocal objects for each profile that satisfies the query
            //1110189: each of the ProfileLocal objects may be typecast into the Profile specifications ProfileLocal type.
            Test1110132ProfileLocal profileLocal = (Test1110132ProfileLocal)profileTable.find(PROFILE_NAME);
            Test1110132ProfileLocal profileLocal3 = (Test1110132ProfileLocal)profileTable.find(PROFILE_NAME3);
            profileLocal3.setStringValue("42");
            profileLocal3.setIntValue(42);
            profileLocal3.setAddress(new Address(AddressPlan.SLEE_PROFILE, "XY/A"));
            profileLocal.setStringValue("41");
            profileLocal.setIntValue(42);
            profileLocal.setAddress(new Address(AddressPlan.SLEE_PROFILE, "XY/B"));

            Collection col = null;

            //test 'queryIsAnswerToLife'
            col = profileTable.queryIsAnswerToLife();
            if (col.size()!=1 || !((Test1110132ProfileLocal)col.iterator().next()).getProfileName().equals(PROFILE_NAME3)) {
                msgSender.sendFailure(1110188, "Wrong profile found for profile query 'queryIsAnswerToLife'" );
                return false;
            }
            else
                msgSender.sendLogMsg("Correct profile was returned for profile query 'queryIsAnswerToLife'");

            //test 'queryEqualsValue'
            col = profileTable.queryEqualsValue("42");
            if (col.size()!=1 || !((Test1110132ProfileLocal)col.iterator().next()).getProfileName().equals(PROFILE_NAME3)) {
                msgSender.sendFailure( 1110188, "Wrong profile found for profile query 'queryEqualsValue(\"42\")'" );
                return false;
            }
            else
                msgSender.sendLogMsg("Correct profile was returned for profile query 'queryEqualsValue(\"42\")'");

            //test 'queryNotEqualsValue'
            col = profileTable.queryNotEqualsValue("41");
            if (col.size()!=1 || !((Test1110132ProfileLocal)col.iterator().next()).getProfileName().equals(PROFILE_NAME3)) {
                msgSender.sendFailure( 1110188, "Wrong profile found for profile query 'queryNotEqualsValue(\"41\")'" );
                return false;
            }
            else
                msgSender.sendLogMsg("Correct profile was returned for profile query 'queryNotEqualsValue(\"41\")'");

            //test 'queryNotEqualsValues'
            col = profileTable.queryNotEqualsValues("41", 41);
            if (col.size()!=1 || !((Test1110132ProfileLocal)col.iterator().next()).getProfileName().equals(PROFILE_NAME3)) {
                msgSender.sendFailure( 1110188, "Wrong profile found for profile query 'queryNotEqualsValues(\"41\", 41)'" );
                return false;
            }
            else
                msgSender.sendLogMsg("Correct profile was returned for profile query 'queryNotEqualsValues(\"41\", 41)'");

            msgSender.sendLogMsg("Testsequence STATIC_QUERY successful.");

            txn.commit();
            txn = null;

        } catch (Exception e) {
            msgSender.sendException(e);
            return false;
        } finally {
            try {
                if (txn!=null)
                    txn.rollback();
            } catch (Exception ex) {
                ra.getLog().warning("Error occured when trying to rollback RA started TXN.", ex);
                msgSender.sendLogMsg("Exception occurred when trying to safely rollback RA started TXN: "+ex.getMessage());
            }
        }

        return true;
    }

    private boolean test_REMOVE_ROLLBACK_PROFILE() {
        SleeTransactionManager txnManager = ra.getResourceAdaptorContext().getSleeTransactionManager();
        SleeTransaction txn=null;

        try {
            ProfileTable profileTable = ra.getResourceAdaptorContext().getProfileTable(PROFILE_TABLE_NAME);
            //start a new TXN as profileTable.find is mandatory transactional
            txn = txnManager.beginSleeTransaction();

            //1110178: allow current transaction to rollback, then check that profile is not removed
            profileTable.remove(PROFILE_NAME);

            txn.setRollbackOnly();

            msgSender.sendLogMsg("Removing profile "+PROFILE_NAME+" then marking transaction for rollback.");
            //allow transaction to rollback

            txn.rollback();
            txn = null;

        } catch (Exception e) {
            msgSender.sendException(e);
            return false;
        } finally {
            try {
                if (txn!=null)
                    txn.rollback();
            } catch (Exception ex) {
                ra.getLog().warning("Error occured when trying to rollback RA started TXN.", ex);
                msgSender.sendLogMsg("Exception occurred when trying to safely rollback RA started TXN: "+ex.getMessage());
            }
        }

        return true;
    }

    private boolean test_REMOVE_COMMIT_PROFILE() {
        SleeTransactionManager txnManager = ra.getResourceAdaptorContext().getSleeTransactionManager();
        SleeTransaction txn=null;

        try {
            ProfileTable profileTable = ra.getResourceAdaptorContext().getProfileTable(PROFILE_TABLE_NAME);
            //start a new TXN as profileTable.find is mandatory transactional
            txn = txnManager.beginSleeTransaction();

            //1110177: remove() returns false if no profile with the specified name existed in the profile table
            //but true if a profile with this name was found and removed
            if (profileTable.remove("XYZ")!=false) {
                msgSender.sendFailure(1110177,"remove(\"XYZ\") should have returned 'false' as profile "+
                        " 'XYZ' does not exist in the profile table");
                return false;
            }
            else {
                msgSender.sendLogMsg("remove(\"XYZ\") correctly returned false as profile 'XYZ' does not exist.");
            }
            if (profileTable.remove(PROFILE_NAME)==false) {
                msgSender.sendFailure(1110177,"remove() should have returned 'true' as profile "+
                        PROFILE_NAME+" exists in the profile table." );
                return false;
            }
            else {
                msgSender.sendLogMsg("remove() correctly returned true and deleted profile "+PROFILE_NAME);
            }

            //1110180: remove() throws NPE if invoked with null as argument
            try {
                profileTable.remove(null);
                msgSender.sendFailure(1110180,"remove(null) should have thrown a NullPointerException.");
                return false;
            }
            catch (NullPointerException e) {
                msgSender.sendLogMsg("remove(null) threw NullPointerException as expected.");
            }
            catch (Exception e) {
                msgSender.sendFailure(1110180,"Wrong type of exception thrown: "+ e.getClass().getName() +" Expected: NullPointerException");
                return false;
            }

            //allow current transaction to commit, then check that profile is really there
            msgSender.sendLogMsg("Testsequence REMOVE_COMMIT_PROFILE successful.");

            txn.commit();
            txn = null;

        } catch (Exception e) {
            msgSender.sendException(e);
            return false;
        } finally {
            try {
                if (txn!=null)
                    txn.rollback();
            } catch (Exception ex) {
                ra.getLog().warning("Error occured when trying to rollback RA started TXN.", ex);
                msgSender.sendLogMsg("Exception occurred when trying to safely rollback RA started TXN: "+ex.getMessage());
            }
        }

        return true;
    }

    private boolean test_FIND_PROFILES_INDEXED() {
        SleeTransactionManager txnManager = ra.getResourceAdaptorContext().getSleeTransactionManager();
        SleeTransaction txn=null;

        try {
            ProfileTable profileTable = ra.getResourceAdaptorContext().getProfileTable(PROFILE_TABLE_NAME);
            //start a new TXN as profileTable.find is mandatory transactional
            txn = txnManager.beginSleeTransaction();

          //1110681: This method can only be used on attributes whose Java type is a
            //primitive type, an Object wrapper of primitive type (e.g. java.lang.Integer),
            //java.lang.String, javax.slee.Address, or an array of one of these types.
            //1110165: Equality for attributes of primitive types is specified by direct
            //value comparison.    Equality for object types is evaluated by
            //java.lang.Object.equals(Object)
            Test1110132ProfileLocal profileLocal = (Test1110132ProfileLocal)profileTable.find(PROFILE_NAME);
            Test1110132ProfileLocal profileLocal3 = (Test1110132ProfileLocal)profileTable.find(PROFILE_NAME3);
            profileLocal3.setStringValue("42");
            profileLocal3.setIntValue(42);
            profileLocal3.setIntObjValue(new Integer(42));
            profileLocal3.setCharValue('X');
            profileLocal3.setCharObjValue(new Character('X'));
            profileLocal3.setBoolValue(true);
            profileLocal3.setBoolObjValue(new Boolean(true));
            profileLocal3.setAddress(new Address(AddressPlan.SLEE_PROFILE, "XY/A"));

            profileLocal.setStringValue("42");
            profileLocal.setIntValue(41);
            profileLocal.setIntObjValue(new Integer(41));
            profileLocal.setCharValue('Y');
            profileLocal.setCharObjValue(new Character('Y'));
            profileLocal.setBoolValue(false);
            profileLocal.setBoolObjValue(new Boolean(false));
            profileLocal.setAddress(new Address(AddressPlan.SLEE_PROFILE, "XY/A"));

            Collection profiles = null;
            Iterator profileIter = null;
            Hashtable profileNamesTab = null;

            //stringValue attribute
            profileNamesTab = new Hashtable();
            profiles= profileTable.findProfilesByAttribute("stringValue", "42");
            profileIter = profiles.iterator();
            while (profileIter.hasNext()) {
                Test1110132ProfileLocal pL = (Test1110132ProfileLocal)profileIter.next();
                profileNamesTab.put(pL.getProfileName(), pL);
            }
            if (profiles.size()!=2 || !profileNamesTab.containsKey(PROFILE_NAME) || !profileNamesTab.containsKey(PROFILE_NAME3) ) {
                msgSender.sendFailure(1110167, "Wrong profiles returned by findProfilesByAttribute() on attribute 'stringValue'");
                return false;
            }
            else {
                msgSender.sendLogMsg("Correct profiles with 'stringValue=\"42\"' found.");
            }
            //intValue attribute and Object wrapper
            profileNamesTab = new Hashtable();
            //1110167: The method returns a Collection with one ProfileLocal object for each matching profile
            //1110168: Each of the ProfileLocal objects may be typecast into the spec's ProfileLocal interface type
            profiles= profileTable.findProfilesByAttribute("intValue", new Integer(42));
            profileIter = profiles.iterator();
            while (profileIter.hasNext()) {
                Test1110132ProfileLocal pL = (Test1110132ProfileLocal)profileIter.next();
                profileNamesTab.put(pL.getProfileName(), pL);
            }
            if (profiles.size()!=1 || !profileNamesTab.containsKey(PROFILE_NAME3)) {
                msgSender.sendFailure( 1110167, "Wrong profiles returned by findProfilesByAttribute() on attribute 'intValue'");
                return false;
            }
            else {
                msgSender.sendLogMsg("Correct profiles with 'intValue=42' found.");
            }
            profileNamesTab = new Hashtable();
            //1110167: The method returns a Collection with one ProfileLocal object for each matching profile
            //1110168: Each of the ProfileLocal objects may be typecast into the spec's ProfileLocal interface type
            profiles= profileTable.findProfilesByAttribute("intObjValue", new Integer(42));
            profileIter = profiles.iterator();
            while (profileIter.hasNext()) {
                Test1110132ProfileLocal pL = (Test1110132ProfileLocal)profileIter.next();
                profileNamesTab.put(pL.getProfileName(), pL);
            }
            if (profiles.size()!=1 || !profileNamesTab.containsKey(PROFILE_NAME3)) {
                msgSender.sendFailure( 1110167, "Wrong profiles returned by findProfilesByAttribute() on attribute 'intObjValue'");
                return false;
            }
            else {
                msgSender.sendLogMsg("Correct profiles with 'intObjValue=Integer(42)' found.");
            }
            //charValue attribute and Object wrapper
            profileNamesTab = new Hashtable();
            //1110167: The method returns a Collection with one ProfileLocal object for each matching profile
            //1110168: Each of the ProfileLocal objects may be typecast charo the spec's ProfileLocal charerface type
            profiles= profileTable.findProfilesByAttribute("charValue", new Character('X'));
            profileIter = profiles.iterator();
            while (profileIter.hasNext()) {
                Test1110132ProfileLocal pL = (Test1110132ProfileLocal)profileIter.next();
                profileNamesTab.put(pL.getProfileName(), pL);
            }
            if (profiles.size()!=1 || !profileNamesTab.containsKey(PROFILE_NAME3)) {
                msgSender.sendFailure( 1110167, "Wrong profiles returned by findProfilesByAttribute() on attribute 'charValue'");
                return false;
            }
            else {
                msgSender.sendLogMsg("Correct profiles with 'charValue='X'' found.");
            }
            profileNamesTab = new Hashtable();
            //1110167: The method returns a Collection with one ProfileLocal object for each matching profile
            //1110168: Each of the ProfileLocal objects may be typecast charo the spec's ProfileLocal charerface type
            profiles= profileTable.findProfilesByAttribute("charObjValue", new Character('X'));
            profileIter = profiles.iterator();
            while (profileIter.hasNext()) {
                Test1110132ProfileLocal pL = (Test1110132ProfileLocal)profileIter.next();
                profileNamesTab.put(pL.getProfileName(), pL);
            }
            if (profiles.size()!=1 || !profileNamesTab.containsKey(PROFILE_NAME3)) {
                msgSender.sendFailure( 1110167, "Wrong profiles returned by findProfilesByAttribute() on attribute 'charObjValue'");
                return false;
            }
            else {
                msgSender.sendLogMsg("Correct profiles with 'charObjValue=Character('X')' found.");
            }
            //boolValue attribute and Object wrapper
            profileNamesTab = new Hashtable();
            //1110167: The method returns a Collection with one ProfileLocal object for each matching profile
            //1110168: Each of the ProfileLocal objects may be typecast boolo the spec's ProfileLocal boolerface type
            profiles= profileTable.findProfilesByAttribute("boolValue", new Boolean(true));
            profileIter = profiles.iterator();
            while (profileIter.hasNext()) {
                Test1110132ProfileLocal pL = (Test1110132ProfileLocal)profileIter.next();
                profileNamesTab.put(pL.getProfileName(), pL);
            }
            if (profiles.size()!=1 || !profileNamesTab.containsKey(PROFILE_NAME3)) {
                msgSender.sendFailure( 1110167, "Wrong profiles returned by findProfilesByAttribute() on attribute 'boolValue'");
                return false;
            }
            else {
                msgSender.sendLogMsg("Correct profiles with 'boolValue=true' found.");
            }
            profileNamesTab = new Hashtable();
            //1110167: The method returns a Collection with one ProfileLocal object for each matching profile
            //1110168: Each of the ProfileLocal objects may be typecast boolo the spec's ProfileLocal boolerface type
            profiles= profileTable.findProfilesByAttribute("boolObjValue", new Boolean(true));
            profileIter = profiles.iterator();
            while (profileIter.hasNext()) {
                Test1110132ProfileLocal pL = (Test1110132ProfileLocal)profileIter.next();
                profileNamesTab.put(pL.getProfileName(), pL);
            }
            if (profiles.size()!=1 || !profileNamesTab.containsKey(PROFILE_NAME3)) {
                msgSender.sendFailure( 1110167, "Wrong profiles returned by findProfilesByAttribute() on attribute 'boolObjValue'");
                return false;
            }
            else {
                msgSender.sendLogMsg("Correct profiles with 'boolObjValue=Boolean(true)' found.");
            }
            //address attribute
            profileNamesTab = new Hashtable();
            profiles= profileTable.findProfilesByAttribute("address", new Address(AddressPlan.SLEE_PROFILE, "XY/A"));
            profileIter = profiles.iterator();
            while (profileIter.hasNext()) {
                Test1110132ProfileLocal pL = (Test1110132ProfileLocal)profileIter.next();
                profileNamesTab.put(pL.getProfileName(), pL);
            }
            if (profiles.size()!=2 || !profileNamesTab.containsKey(PROFILE_NAME3) || !profileNamesTab.containsKey(PROFILE_NAME) ) {
                msgSender.sendFailure(1110167, "Wrong profiles returned by findProfilesByIndexedAttribute() on attribute 'address'" );
                return false;
            }
            else {
                msgSender.sendLogMsg("Correct profiles with 'address=(AddressPlan.SLEE_PROFILE, \"XY/A\")' found.");
            }

            //1110166: the default profile is not considered when determining a match
            //1110170: If no matching profiles are found the method returns null
            if     (profileTable.findProfilesByAttribute("intValue", new Integer(0)).size()!= 0 ||
                    profileTable.findProfilesByAttribute("stringValue", "0").size()!= 0 ||
                    profileTable.findProfilesByAttribute("address", new Address(AddressPlan.SLEE_PROFILE, "XY/0")).size()!= 0) {
                msgSender.sendFailure(1110166, "findProfilesByAttribute() should not consider the default profile when determining matching profiles." );
                return false;
            }
            else {
                msgSender.sendLogMsg("findProfilesByAttribute() correctly ignored the default profile and returned empty collections for each query.");
            }

            //1110169: collection object is immutable, any attempt to modify it results in an UnsupportedOperationException
            try {
                profiles.add("1");
                msgSender.sendFailure(1110169,"Modification attempt on collection object should have caused a UnsupportedOperationException.");
                return false;
            }
            catch (UnsupportedOperationException e) {
                msgSender.sendLogMsg("Modification attempt on collection object caused UnsupportedOperationException as expected.");
            }
            catch (Exception e) {
                msgSender.sendFailure( 1110169,"Wrong type of exception thrown: "+ e.getClass().getName() +" Expected: UnsupportedOperationException");
                return false;
            }
            try {
                profiles.clear();
                msgSender.sendFailure(1110169,"Modification attempt on collection object should have caused a UnsupportedOperationException." );
                return false;
            }
            catch (UnsupportedOperationException e) {
                msgSender.sendLogMsg("Modification attempt on collection object caused UnsupportedOperationException as expected.");
            }
            catch (Exception e) {
                msgSender.sendFailure( 1110169,"Wrong type of exception thrown: "+ e.getClass().getName() +" Expected: UnsupportedOperationException");
                return false;
            }
            try {
                profiles.remove("1");
                msgSender.sendFailure( 1110169, "Modification attempt on collection object should have caused a UnsupportedOperationException.");
                return false;
            }
            catch (UnsupportedOperationException e) {
                msgSender.sendLogMsg("Modification attempt on collection object caused UnsupportedOperationException as expected.");
            }
            catch (Exception e) {
                msgSender.sendFailure( 1110169, "Wrong type of exception thrown: "+ e.getClass().getName() +" Expected: UnsupportedOperationException");
                return false;
            }

            //1110172:findProfilesByIndexedAttribute throws NPE if invoked with null as arguments
            try {
                profileTable.findProfilesByAttribute(null, "42");
                msgSender.sendFailure( 1110172,"findProfilesByAttribute(null, \"42\") should have thrown a NullPointerException.");
                return false;
            }
            catch (NullPointerException e) {
                msgSender.sendLogMsg("findProfilesByAttribute(null, \"42\") threw NullPointerException as expected.");
            }
            catch (Exception e) {
                msgSender.sendFailure( 1110172,"Wrong type of exception thrown: "+ e.getClass().getName() +" Expected: NullPointerException");
                return false;
            }
            try {
                profileTable.findProfilesByAttribute("stringValue", null);
                msgSender.sendFailure(1110172,"findProfilesByAttribute(\"stringValue\", null) should have thrown a NullPointerException." );
                return false;
            }
            catch (NullPointerException e) {
                msgSender.sendLogMsg("findProfilesByAttribute(\"stringValue\", null) threw NullPointerException as expected.");
            }
            catch (Exception e) {
                msgSender.sendFailure(1110172,"Wrong type of exception thrown: "+ e.getClass().getName() +" Expected: NullPointerException");
                return false;
            }

            //1110683: java.lang.IllegalArgumentException: This exception is thrown in the following scenarios:
            //if an attribute with the specified name is not defined in the Profile Specification of the specified Profile Table,
            //if the type of the attributeValue argument is different to the type of the Profile attribute,
            //if the type of the attribute is not one of the allowed types.
            try {
                profileTable.findProfileByAttribute("xyz", "42");
                msgSender.sendFailure( 1110683, "findProfilesByAttribute(\"xyz\", \"42\") should have thrown a IllegalArgumentException.");
            } catch(IllegalArgumentException e) {
                msgSender.sendLogMsg("findProfilesByAttribute(\"xyz\", \"42\") threw IllegalArgumentException as expected.");
            } catch(Exception e) {
                msgSender.sendFailure( 1110683,"Wrong type of exception thrown: "+ e.getClass().getName() +" Expected: IllegalArgumentException");
                return false;
            }
            try {
                profileTable.findProfileByAttribute("intValue", "42");
                msgSender.sendFailure( 1110683, "findProfilesByAttribute(\"intValue\", \"42\") should have thrown a IllegalArgumentException.");
            } catch(IllegalArgumentException e) {
                msgSender.sendLogMsg("findProfilesByAttribute(\"intValue\", \"42\") threw IllegalArgumentException as expected.");
            } catch(Exception e) {
                msgSender.sendFailure( 1110683,"Wrong type of exception thrown: "+ e.getClass().getName() +" Expected: IllegalArgumentException");
                return false;
            }
            try {
                profileTable.findProfileByAttribute("myCMPValue", new MyCMPType());
                msgSender.sendFailure( 1110683, "findProfilesByAttribute(\"myCMPValue\",...) should have thrown a IllegalArgumentException.");
            } catch(IllegalArgumentException e) {
                msgSender.sendLogMsg("findProfilesByAttribute(\"myCMPValue\", ...) threw IllegalArgumentException as expected.");
            } catch(Exception e) {
                msgSender.sendFailure( 1110683,"Wrong type of exception thrown: "+ e.getClass().getName() +" Expected: IllegalArgumentException");
                return false;
            }

            msgSender.sendLogMsg("Testsequence FIND_PROFILES_INDEXED successful.");

            txn.commit();
            txn = null;

        } catch (Exception e) {
            msgSender.sendException(e);
            return false;
        } finally {
            try {
                if (txn!=null)
                    txn.rollback();
            } catch (Exception ex) {
                ra.getLog().warning("Error occured when trying to rollback RA started TXN.", ex);
                msgSender.sendLogMsg("Exception occurred when trying to safely rollback RA started TXN: "+ex.getMessage());
            }
        }

        return true;
    }

    private boolean test_FIND_PROFILE_INDEXED() {
        SleeTransactionManager txnManager = ra.getResourceAdaptorContext().getSleeTransactionManager();
        SleeTransaction txn=null;

        try {
            ProfileTable profileTable = ra.getResourceAdaptorContext().getProfileTable(PROFILE_TABLE_NAME);
            //start a new TXN as profileTable.find is mandatory transactional
            txn = txnManager.beginSleeTransaction();

            //1110676: This method can only be used on attributes whose Java type is a
            //primitive type, an Object wrapper of primitive type (e.g. java.lang.Integer),
            //java.lang.String, javax.slee.Address, or an array of one of these types.
            //1110154: Equality for attributes of primitive types is specified by direct
            //value comparison.    Equality for object types is evaluated by
            //java.lang.Object.equals(Object)
            Test1110132ProfileLocal profileLocal = (Test1110132ProfileLocal)profileTable.find(PROFILE_NAME);
            Test1110132ProfileLocal profileLocal3 = (Test1110132ProfileLocal)profileTable.find(PROFILE_NAME3);
            profileLocal3.setStringValue("42");
            profileLocal3.setIntValue(42);
            profileLocal3.setIntObjValue(new Integer(42));
            profileLocal3.setCharValue('X');
            profileLocal3.setCharObjValue(new Character('X'));
            profileLocal3.setBoolValue(true);
            profileLocal3.setBoolObjValue(new Boolean(true));
            profileLocal3.setAddress(new Address(AddressPlan.SLEE_PROFILE, "XY/A"));

            profileLocal.setStringValue("41");
            profileLocal.setIntValue(41);
            profileLocal.setIntObjValue(new Integer(41));
            profileLocal.setCharValue('Y');
            profileLocal.setCharObjValue(new Character('Y'));
            profileLocal.setBoolValue(false);
            profileLocal.setBoolObjValue(new Boolean(false));
            profileLocal.setAddress(new Address(AddressPlan.SLEE_PROFILE, "XY/B"));


            Test1110132ProfileLocal result = null;
            //1110158: The returned object may be typecast to the spec's ProfileLocal type
            //find for String attribute
            result = (Test1110132ProfileLocal) profileTable.findProfileByAttribute("stringValue", "42");
            if (result==null || !result.getProfileName().equals(PROFILE_NAME3)) {
                msgSender.sendFailure( 1110154, "Wrong profile or null returned by findProfileByAttribute() on attribute 'stringValue'");
                return false;
            }
            else {
                msgSender.sendLogMsg("Correct profile with 'stringValue=\"42\"' found.");
            }
            //find for int and its object wrapper attribute
            result = (Test1110132ProfileLocal) profileTable.findProfileByAttribute("intValue", new Integer(42));
            if (result==null || !result.getProfileName().equals(PROFILE_NAME3)) {
                msgSender.sendFailure( 1110154, "Wrong profile or null returned by findProfileByAttribute() on attribute 'intValue'");
                return false;
            }
            else {
                msgSender.sendLogMsg("Correct profile with 'intValue=42' found.");
            }
            result = (Test1110132ProfileLocal) profileTable.findProfileByAttribute("intObjValue", new Integer(42));
            if (result==null || !result.getProfileName().equals(PROFILE_NAME3)) {
                msgSender.sendFailure( 1110154, "Wrong profile or null returned by findProfileByAttribute() on attribute 'intObjValue'");
                return false;
            }
            else {
                msgSender.sendLogMsg("Correct profile with 'intObjValue=42' found.");
            }
            //find for char and its object wrapper attribute
            result = (Test1110132ProfileLocal) profileTable.findProfileByAttribute("charValue", new Character('X'));
            if (result==null || !result.getProfileName().equals(PROFILE_NAME3)) {
                msgSender.sendFailure( 1110154, "Wrong profile or null returned by findProfileByAttribute() on attribute 'charValue'");
                return false;
            }
            else {
                msgSender.sendLogMsg("Correct profile with 'charValue='X'' found.");
            }
            result = (Test1110132ProfileLocal) profileTable.findProfileByAttribute("charObjValue", new Character('X'));
            if (result==null || !result.getProfileName().equals(PROFILE_NAME3)) {
                msgSender.sendFailure( 1110154, "Wrong profile or null returned by findProfileByAttribute() on attribute 'charObjValue'");
                return false;
            }
            else {
                msgSender.sendLogMsg("Correct profile with 'charObjValue='X'' found.");
            }
            //find for boolean and its object wrapper attribute
            result = (Test1110132ProfileLocal) profileTable.findProfileByAttribute("boolValue", new Boolean(true));
            if (result==null || !result.getProfileName().equals(PROFILE_NAME3)) {
                msgSender.sendFailure( 1110154, "Wrong profile or null returned by findProfileByAttribute() on attribute 'boolValue'");
                return false;
            }
            else {
                msgSender.sendLogMsg("Correct profile with 'boolValue=true' found.");
            }
            result = (Test1110132ProfileLocal) profileTable.findProfileByAttribute("boolObjValue", new Boolean(true));
            if (result==null || !result.getProfileName().equals(PROFILE_NAME3)) {
                msgSender.sendFailure( 1110154, "Wrong profile or null returned by findProfileByAttribute() on attribute 'boolObjValue'");
                return false;
            }
            else {
                msgSender.sendLogMsg("Correct profile with 'boolObjValue=true' found.");
            }
            //find for Address attribute
            result = (Test1110132ProfileLocal) profileTable.findProfileByAttribute("address", new Address(AddressPlan.SLEE_PROFILE, "XY/A"));
            if (result==null || !result.getProfileName().equals(PROFILE_NAME3)) {
                msgSender.sendFailure( 1110154, "Wrong profile or null returned by findProfileByAttribute() on attribute 'address'");
                return false;
            }
            else {
                msgSender.sendLogMsg("Correct profile with 'address=(AddressPlan.SLEE_PROFILE, \"XY/A\")' found.");
            }

            //1110156: the default profile is not considered when determining a match
            //1110157: If no matching profiles are found the method returns null
            if     (profileTable.findProfileByAttribute("intValue", new Integer(0))!= null ||
                    profileTable.findProfileByAttribute("stringValue", "0")!= null ||
                    profileTable.findProfileByAttribute("address", new Address(AddressPlan.SLEE_PROFILE,"XY/0"))!= null) {
                msgSender.sendFailure( 1110156, "findProfileByAttribute(...) should not consider the default profile when determining matching profiles.");
                return false;
            }
            else {
                msgSender.sendLogMsg("findProfileByAttribute(...) correctly ignored the default profile and returned null for each query.");
            }

            //1110160:findProfileByIndexedAttribute throws NPE if invoked with null as arguments
            try {
                profileTable.findProfileByAttribute(null, "42");
                msgSender.sendFailure( 1110160,"findProfileByAttribute(null, \"42\") should have thrown a NullPointerException.");
                return false;
            }
            catch (NullPointerException e) {
                msgSender.sendLogMsg("findProfileByAttribute(null, \"42\") threw NullPointerException as expected.");
            }
            catch (Exception e) {
                msgSender.sendFailure( 1110160,"Wrong type of exception thrown: "+ e.getClass().getName() +" Expected: NullPointerException");
                return false;
            }
            try {
                profileTable.findProfileByAttribute("stringValue", null);
                msgSender.sendFailure( 1110160,"findProfileByAttribute(\"stringValue\", null) should have thrown a NullPointerException.");
                return false;
            }
            catch (NullPointerException e) {
                msgSender.sendLogMsg("findProfileByAttribute(\"stringValue\", null) threw NullPointerException as expected.");
            }
            catch (Exception e) {
                msgSender.sendFailure( 1110160,"Wrong type of exception thrown: "+ e.getClass().getName() +" Expected: NullPointerException");
                return false;
            }

            //1110678: java.lang.IllegalArgumentException: This exception is thrown in the following scenarios:
            //if an attribute with the specified name is not defined in the Profile Specification of the specified Profile Table,
            //if the type of the attributeValue argument is different to the type of the Profile attribute,
            //if the type of the attribute is not one of the allowed types.
            try {
                profileTable.findProfileByAttribute("xyz", "42");
                msgSender.sendFailure( 1110678, "findProfileByAttribute(\"xyz\", \"42\") should have thrown a IllegalArgumentException.");
            } catch(IllegalArgumentException e) {
                msgSender.sendLogMsg("findProfileByAttribute(\"xyz\", \"42\") threw IllegalArgumentException as expected.");
            } catch(Exception e) {
                msgSender.sendFailure( 1110678,"Wrong type of exception thrown: "+ e.getClass().getName() +" Expected: IllegalArgumentException");
                return false;
            }
            try {
                profileTable.findProfileByAttribute("intValue", "42");
                msgSender.sendFailure( 1110678, "findProfileByAttribute(\"intValue\", \"42\") should have thrown a IllegalArgumentException.");
            } catch(IllegalArgumentException e) {
                msgSender.sendLogMsg("findProfileByAttribute(\"intValue\", \"42\") threw IllegalArgumentException as expected.");
            } catch(Exception e) {
                msgSender.sendFailure( 1110678,"Wrong type of exception thrown: "+ e.getClass().getName() +" Expected: IllegalArgumentException");
                return false;
            }
            try {
                profileTable.findProfileByAttribute("myCMPValue", new MyCMPType());
                msgSender.sendFailure( 1110678, "findProfileByAttribute(\"myCMPValue\",...) should have thrown a IllegalArgumentException.");
            } catch(IllegalArgumentException e) {
                msgSender.sendLogMsg("findProfileByAttribute(\"myCMPValue\", ...) threw IllegalArgumentException as expected.");
            } catch(Exception e) {
                msgSender.sendFailure( 1110678,"Wrong type of exception thrown: "+ e.getClass().getName() +" Expected: IllegalArgumentException");
                return false;
            }

            msgSender.sendLogMsg("Testsequence FIND_PROFILE_INDEXED successful.");

            txn.commit();
            txn = null;

        } catch (Exception e) {
            msgSender.sendException(e);
            return false;
        } finally {
            try {
                if (txn!=null)
                    txn.rollback();
            } catch (Exception ex) {
                ra.getLog().warning("Error occured when trying to rollback RA started TXN.", ex);
                msgSender.sendLogMsg("Exception occurred when trying to safely rollback RA started TXN: "+ex.getMessage());
            }
        }

        return true;
    }

    private boolean test_FIND_PROFILE() {
        SleeTransactionManager txnManager = ra.getResourceAdaptorContext().getSleeTransactionManager();
        SleeTransaction txn=null;

        try {
            ProfileTable profileTable = ra.getResourceAdaptorContext().getProfileTable(PROFILE_TABLE_NAME);
            //start a new TXN as profileTable.find is mandatory transactional
            txn = txnManager.beginSleeTransaction();

            //1110147: find() throws NPE if invoked with null argument
            try {
                profileTable.find(null);
                msgSender.sendFailure( 1110147, "find(null) should have thrown a NullPointerException.");
                return false;
            }
            catch (NullPointerException e) {
                msgSender.sendLogMsg("find(null) threw NullPointerException as expected.");
            }
            catch (Exception e) {
                msgSender.sendFailure( 1110147, "Wrong type of exception thrown: "+ e.getClass().getName() +" Expected: NullPointerException");
                return false;
            }

            //1110148: findAll() returns all profiles other than the default profile of the profile table
            //1110149: returned object is a collection with a ProfileLocalObject for each profile found
            //1110150: each object in the collection may be typecast into the spec's ProfileLocal type
            Test1110132ProfileLocal profileLocal3 = (Test1110132ProfileLocal)profileTable.create(PROFILE_NAME3);
            Collection col = profileTable.findAll();
            Iterator iter = col.iterator();
            Hashtable tab = new Hashtable();
            while (iter.hasNext()) {
                Test1110132ProfileLocal prof = (Test1110132ProfileLocal)iter.next();
                tab.put(prof.getProfileName(), prof);
            }
            if (col.size()!=2 || !tab.containsKey(PROFILE_NAME) || !tab.containsKey(PROFILE_NAME3)) {
                msgSender.sendFailure( 1110148, "Wrong number of profiles or some profiles were missing from the collection returned by findAll()");
                return false;
            }
            else {
                msgSender.sendLogMsg("findAll() returned the correct list of profiles.");
            }

            //1110151: collection object is immutable, any attempt to modify it results in an UnsupportedOperationException
            try {
                col.add("1");
                msgSender.sendFailure( 1110151, "Modification attempt on collection object should have caused a UnsupportedOperationException.");
                return false;
            }
            catch (UnsupportedOperationException e) {
                msgSender.sendLogMsg("Modification attempt on collection object caused UnsupportedOperationException as expected.");
            }
            catch (Exception e) {
                msgSender.sendFailure( 1110151, "Wrong type of exception thrown: "+ e.getClass().getName() +" Expected: UnsupportedOperationException");
                return false;
            }
            try {
                col.clear();
                msgSender.sendFailure( 1110151, "Modification attempt on collection object should have caused a UnsupportedOperationException.");
                return false;
            }
            catch (UnsupportedOperationException e) {
                msgSender.sendLogMsg("Modification attempt on collection object caused UnsupportedOperationException as expected.");
            }
            catch (Exception e) {
                msgSender.sendFailure( 1110151, "Wrong type of exception thrown: "+ e.getClass().getName() +" Expected: UnsupportedOperationException");
                return false;
            }
            try {
                col.remove("1");
                msgSender.sendFailure( 1110151, "Modification attempt on collection object should have caused a UnsupportedOperationException.");
                return false;
            }
            catch (UnsupportedOperationException e) {
                msgSender.sendLogMsg("Modification attempt on collection object caused UnsupportedOperationException as expected.");
            }
            catch (Exception e) {
                msgSender.sendFailure( 1110151, "Wrong type of exception thrown: "+ e.getClass().getName() +" Expected: UnsupportedOperationException");
                return false;
            }

            //1110152: if no profiles exist in the table then an empty collection is returned
            ProfileTable profileTable2 = ra.getResourceAdaptorContext().getProfileTable(PROFILE_TABLE_NAME2);
            Collection col2 = profileTable2.findAll();
            if (col2.size()!=0) {
                msgSender.sendFailure( 1110152, "Collection returned by findAll() should be empty as profile table "+PROFILE_TABLE_NAME2+" does not contain any profiles.");
                return false;
            }

            msgSender.sendLogMsg("Testsequence FIND_PROFILE successful.");


            txn.commit();
            txn = null;

        } catch (Exception e) {
            msgSender.sendException(e);
            return false;
        } finally {
            try {
                if (txn!=null)
                    txn.rollback();
            } catch (Exception ex) {
                ra.getLog().warning("Error occured when trying to rollback RA started TXN.", ex);
                msgSender.sendLogMsg("Exception occurred when trying to safely rollback RA started TXN: "+ex.getMessage());
            }
        }

        return true;
    }

    private boolean test_CREATE_ROLLBACK_PROFILE() {
        SleeTransactionManager txnManager = ra.getResourceAdaptorContext().getSleeTransactionManager();
        SleeTransaction txn=null;

        try {
            ProfileTable profileTable = ra.getResourceAdaptorContext().getProfileTable(PROFILE_TABLE_NAME);
            //start a new TXN as profileTable.find is mandatory transactional
            txn = txnManager.beginSleeTransaction();

            //1110140: Try to create a profile which already exists.
            try {
                profileTable.create(PROFILE_NAME);
                msgSender.sendFailure(1110140, "create(\""+PROFILE_NAME+"\") should have thrown a ProfileAlreadyExistsException.");
                return false;
            }
            catch (ProfileAlreadyExistsException e) {
                msgSender.sendLogMsg("create(\""+PROFILE_NAME+"\") threw NullPointerException as expected.");
            }
            catch (Exception e) {
                msgSender.sendFailure(1110140, "Wrong type of exception thrown: "+ e.getClass().getName() +" Expected: ProfileAlreadyExistsException");
                return false;
            }

            //1110135: allow current transaction to rollback, then check that profile is not created
            profileTable.create(PROFILE_NAME2);

            txn.setRollbackOnly();

            msgSender.sendLogMsg("Creating profile "+PROFILE_NAME2+" then marking transaction for rollback.");
            //allow transaction to rollback
            msgSender.sendLogMsg("Testsequence CREATE_ROLLBACK_PROFILE successful.");

            txn.rollback();
            txn = null;

        } catch (Exception e) {
            msgSender.sendException(e);
            return false;
        } finally {
            try {
                if (txn!=null)
                    txn.rollback();
            } catch (Exception ex) {
                ra.getLog().warning("Error occured when trying to rollback RA started TXN.", ex);
                msgSender.sendLogMsg("Exception occurred when trying to safely rollback RA started TXN: "+ex.getMessage());
            }
        }

        return true;
    }

    private boolean test_CREATE_COMMIT_PROFILE() {
        SleeTransactionManager txnManager = ra.getResourceAdaptorContext().getSleeTransactionManager();
        SleeTransaction txn=null;

        try {
            ProfileTable profileTable = ra.getResourceAdaptorContext().getProfileTable(PROFILE_TABLE_NAME);
            //start a new TXN as profileTable.find is mandatory transactional
            txn = txnManager.beginSleeTransaction();

            //1110137:create throws NPE if invoked with null as argument
            try {
                profileTable.create(null);
                msgSender.sendFailure(1110137, "create(null) should have thrown a NullPointerException.");
                return false;
            }
            catch (NullPointerException e) {
                msgSender.sendLogMsg("create(null) threw NullPointerException as expected.");
            }
            catch (Exception e) {
                msgSender.sendFailure(1110137, "Wrong type of exception thrown: "+ e.getClass().getName() +" Expected: NullPointerException");
                return false;
            }

            //1110138:create throws IllegalArgument exception if ProfileName contains illegal characters (outside of the range 0x0020-0x007e)
            try {
                Character ch = new Character('\u0019');
                profileTable.create(ch.toString());
                msgSender.sendFailure( 1110138, "create('"+ch.toString()+"') should have thrown an IllegalArgumentException.");
                return false;
            }
            catch (IllegalArgumentException e) {
                msgSender.sendLogMsg("create('\u0019') threw IllegalArgumentException as expected.");
            }
            catch (Exception e) {
                msgSender.sendFailure( 1110138, "Wrong type of exception thrown: "+ e.getClass().getName() +" Expected: IllegalArgumentException");
                return false;
            }
            try {
                Character ch = new Character('\u007f');
                profileTable.create(ch.toString());
                msgSender.sendFailure(1110138 , "create('"+ch.toString()+"') should have thrown an IllegalArgumentException.");
                return false;
            }
            catch (IllegalArgumentException e) {
                msgSender.sendLogMsg("create('\u007f') threw IllegalArgumentException as expected.");
            }
            catch (Exception e) {
                msgSender.sendFailure( 1110138, "Wrong type of exception thrown: "+ e.getClass().getName() +" Expected: IllegalArgumentException");
                return false;
            }

            //1110141: create propagates CreateException from profile object's profilePostCreate() method
            try {
                profileTable.create(Test1110132Profile.CREATE_EXCEPTION_PROFILE);
                msgSender.sendFailure( 1110141, "create(\""+Test1110132Profile.CREATE_EXCEPTION_PROFILE+"\") should have thrown a CreateException.");
                return false;
            }
            catch (CreateException e) {
                msgSender.sendLogMsg("create(\""+Test1110132Profile.CREATE_EXCEPTION_PROFILE+"\") threw CreateException " +
                        "propagated from profile abstract class's profilePostCreate() method as expected.");
            }
            catch (Exception e) {
                msgSender.sendFailure( 1110141, "Wrong type of exception thrown: "+ e.getClass().getName() +" Expected: CreateException.");
                return false;
            }

            //1110132:create a profile
            //1110133:the object returned by create() can be typecast into the spec's ProfileLocal interface
            Test1110132ProfileLocal profileLocal = (Test1110132ProfileLocal)profileTable.create(PROFILE_NAME);

            //1110135: allow current transaction to commit, then check that profile is really there
            msgSender.sendLogMsg("Testsequence CREATE_COMMIT_PROFILE successful.");
            txn.commit();
            txn = null;

        } catch (Exception e) {
            msgSender.sendException(e);
            return false;
        } finally {
            try {
                if (txn!=null)
                    txn.rollback();
            } catch (Exception ex) {
                ra.getLog().warning("Error occured when trying to rollback RA started TXN.", ex);
                msgSender.sendLogMsg("Exception occurred when trying to safely rollback RA started TXN: "+ex.getMessage());
            }
        }

        return true;
    }

    private boolean test_CHECK_ROLLBACK_PROFILE() {
        SleeTransactionManager txnManager = ra.getResourceAdaptorContext().getSleeTransactionManager();
        SleeTransaction txn=null;

        try {
            ProfileTable profileTable = ra.getResourceAdaptorContext().getProfileTable(PROFILE_TABLE_NAME);
            //start a new TXN as profileTable.find is mandatory transactional
            txn = txnManager.beginSleeTransaction();

            //1110135: check that 2nd profile has NOT been created
            //1110145: find() returns null if profile does not exist
            if (profileTable.find(PROFILE_NAME2)==null)
                msgSender.sendLogMsg("Creating profile "+PROFILE_NAME2+" followed by rollback worked fine.");
            else {
                msgSender.sendFailure(1110135, "Profile "+PROFILE_NAME2+
                        " was created though transaction was rolled back.");
                return false;
            }

            msgSender.sendLogMsg("Testsequence CHECK_ROLLBACK_PROFILE successful.");

            txn.commit();
            txn = null;

        } catch (Exception e) {
            msgSender.sendException(e);
            return false;
        } finally {
            try {
                if (txn!=null)
                    txn.rollback();
            } catch (Exception ex) {
                ra.getLog().warning("Error occured when trying to rollback RA started TXN.", ex);
                msgSender.sendLogMsg("Exception occurred when trying to safely rollback RA started TXN: "+ex.getMessage());
            }
        }

        return true;
    }

    private boolean test_CHECK_REMOVE_ROLLBACK() {
        SleeTransactionManager txnManager = ra.getResourceAdaptorContext().getSleeTransactionManager();
        SleeTransaction txn=null;

        try {
            ProfileTable profileTable = ra.getResourceAdaptorContext().getProfileTable(PROFILE_TABLE_NAME);
            //start a new TXN as profileTable.find is mandatory transactional
            txn = txnManager.beginSleeTransaction();

            //1110178: check that profile has NOT been removed
            if (profileTable.find(PROFILE_NAME)!=null)
                msgSender.sendLogMsg("Removing profile "+PROFILE_NAME+" then rolling back worked fine, profile still exists." );
            else {
                msgSender.sendFailure(1110178,"Profile "+
                    PROFILE_NAME+" was removed though transaction was rolled back." );
                return false;
            }

            txn.commit();
            txn = null;

        } catch (Exception e) {
            msgSender.sendException(e);
            return false;
        } finally {
            try {
                if (txn!=null)
                    txn.rollback();
            } catch (Exception ex) {
                ra.getLog().warning("Error occured when trying to rollback RA started TXN.", ex);
                msgSender.sendLogMsg("Exception occurred when trying to safely rollback RA started TXN: "+ex.getMessage());
            }
        }

        return true;
    }

    private boolean test_CHECK_REMOVE_COMMIT() {
        SleeTransactionManager txnManager = ra.getResourceAdaptorContext().getSleeTransactionManager();
        SleeTransaction txn=null;

        try {
            ProfileTable profileTable = ra.getResourceAdaptorContext().getProfileTable(PROFILE_TABLE_NAME);
            //start a new TXN as profileTable.find is mandatory transactional
            txn = txnManager.beginSleeTransaction();

          //1110178: check that removal was successful
            if (profileTable.find(PROFILE_NAME)==null)
                msgSender.sendLogMsg("Removing and committing profile "+PROFILE_NAME+" worked fine.");
            else {
                msgSender.sendFailure(1110178,"Removing and committing profile "+
                        PROFILE_NAME+" failed.");
                return false;
            }

            txn.commit();
            txn = null;

        } catch (Exception e) {
            msgSender.sendException(e);
            return false;
        } finally {
            try {
                if (txn!=null)
                    txn.rollback();
            } catch (Exception ex) {
                ra.getLog().warning("Error occured when trying to rollback RA started TXN.", ex);
                msgSender.sendLogMsg("Exception occurred when trying to safely rollback RA started TXN: "+ex.getMessage());
            }
        }

        return true;
    }

    private boolean test_CHECK_COMMIT_PROFILE() {
        SleeTransactionManager txnManager = ra.getResourceAdaptorContext().getSleeTransactionManager();
        SleeTransaction txn=null;

        try {
            ProfileTable profileTable = ra.getResourceAdaptorContext().getProfileTable(PROFILE_TABLE_NAME);
            //start a new TXN as profileTable.find is mandatory transactional
            txn = txnManager.beginSleeTransaction();

            //1110132: check that create was successful
            //1110143: find() returns an object that can be typecast into profile specs ProfileLocal interface
            Test1110132ProfileLocal profileLocal = (Test1110132ProfileLocal)profileTable.find(PROFILE_NAME);
            if (profileLocal!=null)
                msgSender.sendLogMsg("Creating and committing profile "+PROFILE_NAME+" worked fine.");
            else {
                msgSender.sendFailure(1110132, "Creating and committing profile "+
                                PROFILE_NAME+" failed.");
                return false;
            }

            msgSender.sendLogMsg("Testsequence CHECK_COMMIT_PROFILE successful.");

            txn.commit();
            txn = null;

        } catch (Exception e) {
            msgSender.sendException(e);
            return false;
        } finally {
            try {
                if (txn!=null)
                    txn.rollback();
            } catch (Exception ex) {
                ra.getLog().warning("Error occured when trying to rollback RA started TXN.", ex);
                msgSender.sendLogMsg("Exception occurred when trying to safely rollback RA started TXN: "+ex.getMessage());
            }
        }

        return true;
    }

    private BaseMessageSender msgSender;
    private RMIObjectChannel out;

}

