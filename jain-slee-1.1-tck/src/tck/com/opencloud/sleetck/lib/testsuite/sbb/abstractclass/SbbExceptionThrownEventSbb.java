/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.sbb.abstractclass;

import com.opencloud.sleetck.lib.sbbutils.TCKSbbUtils;
import com.opencloud.sleetck.lib.resource.events.TCKResourceEventX;

import javax.slee.*;

/**
 * Test sbbExceptionThrown() method from spec s6.11.1 and s6.11.3
 */
public abstract class SbbExceptionThrownEventSbb extends SendResultsSbb {
    private static final String EXCEPTION_MSG = "Exception Test Message for SbbExceptionThrownSbbNonEvent SBB";

    public void onTCKResourceEventX1(TCKResourceEventX event, ActivityContextInterface aci) {
        // save references to the parameters
        this.event = event;
        this.aci = aci;

        // save transactional state
        setTestValue(3);

        throw new SLEEException(EXCEPTION_MSG, new Exception(EXCEPTION_MSG));
    }

    public void sbbExceptionThrown(Exception exception, Object event, ActivityContextInterface aci) {
        try {
            // check exception
            if (exception == null) {
                setResultFailed(462, "Exception passed to sbbExceptionThrown is null");
                return;
            }

            if (!exception.getClass().equals(SLEEException.class)) {
                setResultFailed(462, "Exception passed to sbbExceptionThrown is a different type to that thrown: " + exception.getClass().getName());
                return;
            }

            if (!exception.getMessage().equals(EXCEPTION_MSG)) {
                setResultFailed(462, "Exception passed to sbbExceptionThrown has different message to that originally given: " + exception.getMessage());
                return;
            }

            // check nested exception
            Throwable nested = ((SLEEException)exception).getCause();
            if (nested == null) {
                setResultFailed(462, "Exception passed to sbbExceptionThrown has lost its nested exception");
                return;
            }

            if (!nested.getClass().equals(Exception.class)) {
                setResultFailed(462, "Exception passed to sbbExceptionThrown has a different nested exception type to that thrown: " + nested.getClass().getName());
                return;
            }

            if (!nested.getMessage().equals(EXCEPTION_MSG)) {
                setResultFailed(462, "Exception passed to sbbExceptionThrown has a nested exception with a different message to that originally given: " + nested.getMessage());
                return;
            }

            // check that this object contains references to the event-handler parameters
            if (this.event == null || this.aci == null) {
                setResultFailed(457, "sbbExceptionThrown not invoked on sbb object that threw the exception");
                return;
            }
            if (!getSbbContext().getRollbackOnly()) {
                setResultFailed(457, "Transaction not marked for rollback in sbbExceptionThrown");
                return;
            }
            if (getTestValue() != 3) {
                setResultFailed(457, "Transactional state set before exception was thrown has changed: possibly not the same transaction");
                return;
            }

            // check parameters for exception thrown from event-handler method
            if (!event.equals(this.event)) {
                setResultFailed(463, "sbbExceptionThrown does not have the event object from event-handler method that threw the exception");
                return;
            }
            if (!aci.equals(this.aci)) {
                setResultFailed(463, "sbbExceptionThrown does not have the ActivityContextInterface object from event-handler method that threw the exception");
                return;
            }


            setResultPassed("sbbExceptionThrown for non-transactional methods passed");
        }
        catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }
        finally {
            // clean up references
            this.event = null;
            this.aci = null;
        }
    }

    public void sbbRolledBack(javax.slee.RolledBackContext context) {
        // do nothing, expected behaviour
    }


    public abstract void setTestValue(int value);
    public abstract int getTestValue();

    private TCKResourceEventX event;
    private ActivityContextInterface aci;
}

