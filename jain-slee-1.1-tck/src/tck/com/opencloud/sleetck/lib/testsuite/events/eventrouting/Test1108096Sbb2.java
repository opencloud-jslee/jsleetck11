/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.events.eventrouting;

import javax.slee.ActivityContextInterface;
import javax.slee.EventContext;
import javax.slee.InitialEventSelector;
import javax.slee.facilities.Tracer;

import com.opencloud.sleetck.lib.resource.events.TCKResourceEventX;
import com.opencloud.sleetck.lib.resource.events.TCKResourceEventY;
import com.opencloud.sleetck.lib.sbbutils.BaseTCKSbb;
import com.opencloud.sleetck.lib.sbbutils.TCKSbbUtils;

/**
 * AssertionID(1108096): Test An SBB can modify the event delivery priority 
 * of a root SBB entity at runtime by invoking the setSbbPriority method on 
 * an SBB local object that represents the root SBB entity.
 *
 */
public abstract class Test1108096Sbb2 extends BaseTCKSbb {

    public InitialEventSelector initialEventSelectorMethod(InitialEventSelector ies) {
        // set the custom name variable to true to test that this value is not held accross invocations
        ies.setCustomName("test");
        return ies;
    }

    // Initial event method.
    public void onTCKResourceEventX1(TCKResourceEventX event, ActivityContextInterface aci, EventContext context) {
        try {
            tracer = getSbbContext().getTracer("com.test");
            tracer.finest("Sbb2: Received a TCK event " + event);

        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }
    };

    public void onTCKResourceEventY1(TCKResourceEventY event, ActivityContextInterface aci, EventContext context) {
        try {
            tracer = getSbbContext().getTracer("com.test");
            tracer.info("Sbb2: Received a TCK event " + event);

            TCKSbbUtils.getResourceInterface().sendSbbMessage(new Integer(2));

        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }
    };

    public void onTCKResourceEventX2(TCKResourceEventX event, ActivityContextInterface aci, EventContext context) {
        try {
            tracer = getSbbContext().getTracer("com.test");
            tracer.finest("Sbb2: Received a TCK event " + event);

        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }
    };

    public void onTCKResourceEventY2(TCKResourceEventY event, ActivityContextInterface aci, EventContext context) {
        try {
            tracer = getSbbContext().getTracer("com.test");
            tracer.info("Sbb2: Received a TCK event " + event);

            TCKSbbUtils.getResourceInterface().sendSbbMessage(new Integer(3));

        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }
    };

    public void onTCKResourceEventX3(TCKResourceEventX event, ActivityContextInterface aci, EventContext context) {
        try {
            tracer = getSbbContext().getTracer("com.test");
            tracer.finest("Sbb2: Received a TCK event " + event);

        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }
    };

    public void onTCKResourceEventY3(TCKResourceEventY event, ActivityContextInterface aci, EventContext context) {
        try {
            tracer = getSbbContext().getTracer("com.test");
            tracer.info("Sbb2: Received a TCK event " + event);

            TCKSbbUtils.getResourceInterface().sendSbbMessage(new Integer(6));

        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }
    };

    private Tracer tracer;
}
