/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.facilities.activitycontextnamingfacility;

import com.opencloud.sleetck.lib.sbbutils.BaseTCKSbb;
import com.opencloud.sleetck.lib.sbbutils.TCKSbbUtils;

import javax.slee.ActivityContextInterface;
import javax.slee.facilities.ActivityContextNamingFacility;
import javax.slee.nullactivity.NullActivity;
import java.util.HashMap;

public abstract class Test1346ChildSbb extends BaseTCKSbb {

    public void onTest1346Event(Test1346Event event, ActivityContextInterface aci) {
         try {
            ActivityContextNamingFacility facility = (ActivityContextNamingFacility) TCKSbbUtils.getSbbEnvironment().lookup("slee/facilities/activitycontextnaming");
            ActivityContextInterface nullACI = facility.lookup(Test1346Sbb.ACTIVITY_NAME);

            HashMap map = new HashMap();
            if (nullACI == null) {
                map.put("Result", new Boolean(false));
                map.put("Message", "Failed to lookup NullActivity registered in the ActivityContextNamingFacility.");
            } else {
                map.put("Result", new Boolean(true));
                map.put("Message", "Ok");
                
                facility.unbind(Test1346Sbb.ACTIVITY_NAME);

                NullActivity activity = (NullActivity) nullACI.getActivity();
                activity.endActivity();
            }
            TCKSbbUtils.getResourceInterface().sendSbbMessage(map);
        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }

    }

}
