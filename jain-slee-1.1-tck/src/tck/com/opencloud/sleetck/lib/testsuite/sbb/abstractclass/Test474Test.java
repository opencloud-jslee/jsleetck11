/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.sbb.abstractclass;

import com.opencloud.sleetck.lib.AbstractSleeTCKTest;
import com.opencloud.sleetck.lib.OperationTimedOutException;
import com.opencloud.sleetck.lib.SleeTCKTestUtils;
import com.opencloud.sleetck.lib.TCKTestResult;
import com.opencloud.sleetck.lib.resource.TCKActivityID;
import com.opencloud.sleetck.lib.resource.events.TCKResourceEventX;
import com.opencloud.sleetck.lib.resource.testapi.TCKResourceTestInterface;
import com.opencloud.sleetck.lib.testutils.FutureResult;
import com.opencloud.sleetck.lib.testutils.QueuingResourceListener;

import javax.slee.management.DeployableUnitID;

public class Test474Test extends AbstractSleeTCKTest {

    private static final String SERVICE_DU_PATH_PARAM = "DUPath";
    private static final int TEST_ID = 474;

    public void init(SleeTCKTestUtils utils) {
        this.utils = utils;
    }

    /**
     * Perform the actual test.
     */

    public TCKTestResult run() throws Exception {

        QueuingResourceListener listener = new QueuingResourceListener(utils);
        utils.getResourceInterface().setResourceListener(listener);

        TCKResourceTestInterface resource = utils.getResourceInterface();
        TCKActivityID activityID = resource.createActivity("Test474InitialActivity");
        resource.fireEvent(TCKResourceEventX.X1, TCKResourceEventX.X1, activityID, null);

        try {
            // wait for the first message
            listener.nextMessage().getMessage();
        } catch (OperationTimedOutException e) {
            return TCKTestResult.failed(TEST_ID, "Timed out waiting for message from SBB.");
        }

        // Look for the second message (timeout should occur)
        try {
            listener.nextMessage().getMessage();
        } catch (OperationTimedOutException e) {
            // Good
            return TCKTestResult.passed();
        }

        return TCKTestResult.failed(TEST_ID, "Received second message from SBB.");

    }

    /**
     * Do all the pre-run configuration of the test.
     */

    public void setUp() throws Exception {

        utils.getLog().fine("Installing and activating service");

        // Install the Deployable Units
        String duPath = utils.getTestParams().getProperty(SERVICE_DU_PATH_PARAM);
        duID = utils.install(duPath);

        // Activate the DU
        utils.activateServices(duID, true);

    }

    /**
     * Clean up after the test.
     */

    public void tearDown() throws Exception {
        utils.getLog().fine("Disconnecting from resource");
        utils.getResourceInterface().clearActivities();
        utils.getResourceInterface().removeResourceListener();
        utils.getLog().fine("Deactivating and uninstalling service");
        utils.deactivateAllServices();
        utils.uninstallAll();
    }

    private SleeTCKTestUtils utils;
    private FutureResult result;
    private DeployableUnitID duID;
}
