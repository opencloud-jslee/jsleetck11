/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.libraries;

import javax.slee.management.DeployableUnitID;
import javax.slee.management.DeploymentException;

import com.opencloud.logging.Logable;
import com.opencloud.sleetck.lib.AbstractSleeTCKTest;
import com.opencloud.sleetck.lib.TCKTestResult;
import com.opencloud.sleetck.lib.testutils.jmx.DeploymentMBeanProxy;

public class Test1116011Test extends AbstractSleeTCKTest {

    private static final int TEST_ID = 1116011;

    public TCKTestResult run() throws Exception {
        Logable log =  utils().getLog();
        DeployableUnitID libraryDuID=null;
        DeploymentMBeanProxy deploymentMBean;
        String libraryDUPath;
        TCKTestResult testResult = null;
        
        deploymentMBean = utils().getDeploymentMBeanProxy();
        
        log.info("Trying to deploy library without name.");
        libraryDUPath = utils().getTestParams().getProperty("libraryDU-noName");
        try {
            libraryDuID = deploymentMBean.install(utils().getDeploymentUnitURL(libraryDUPath));
            log.error("Library should not have deployed because it does not have library-name element.");
            testResult = TCKTestResult.failed(TEST_ID, "Library should not have deployed because it does not have library-name element.");
        } catch (DeploymentException e) {
            // good.
        } finally {
            try {
                deploymentMBean.uninstall(libraryDuID);
            } catch (Exception e) {}
        }
        
        log.info("Trying to deploy library without vendor");
        libraryDUPath = utils().getTestParams().getProperty("libraryDU-noVendor");
        try {
            libraryDuID = deploymentMBean.install(utils().getDeploymentUnitURL(libraryDUPath));
            log.error("Library should not have deployed because it does not have library-vendor element.");
            testResult = TCKTestResult.failed(TEST_ID, "Library should not have deployed because it does not have library-vendor element.");
        } catch (DeploymentException e) {
            // good.
        } finally {
            try {
                deploymentMBean.uninstall(libraryDuID);
            } catch (Exception e) {}
        }

        log.info("Trying to deploy library without version.");
        libraryDUPath = utils().getTestParams().getProperty("libraryDU-noVersion");
        try {
            libraryDuID = deploymentMBean.install(utils().getDeploymentUnitURL(libraryDUPath));
            log.error("Library should not have deployed because it does not have library-version element.");
            testResult = TCKTestResult.failed(TEST_ID, "Library should not have deployed because it does not have library-version element.");
        } catch (DeploymentException e) {
            // good.
        } finally {
            try {
                deploymentMBean.uninstall(libraryDuID);
            } catch (Exception e) {}
        }
        
        return testResult==null ? TCKTestResult.passed() : testResult; 
    }
    
    public void setUp() throws Exception {} // do not allow super.setUp() to be called.
    public void tearDown() throws Exception {} // do not allow super.tearDown() to be called.
}
