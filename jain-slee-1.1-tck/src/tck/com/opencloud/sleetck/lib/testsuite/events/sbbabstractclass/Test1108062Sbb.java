/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.events.sbbabstractclass;

import javax.naming.InitialContext;
import javax.slee.ActivityContextInterface;
import javax.slee.ChildRelation;
import javax.slee.EventContext;
import javax.slee.facilities.ActivityContextNamingFacility;
import javax.slee.facilities.Tracer;

import com.opencloud.sleetck.lib.resource.events.TCKResourceEventX;
import com.opencloud.sleetck.lib.sbbutils.BaseTCKSbb;

/**
 * AssertionID(1108062): Test The SBB object must be in the Ready state, 
 * i.e. it must have an assigned SBB entity, when it invokes this method. 
 * Otherwise, this method throws a java.lang.IllegalStateException.
 */
public abstract class Test1108062Sbb extends BaseTCKSbb {

    // Initial event method.
    public void onTCKResourceEventX1(TCKResourceEventX event, ActivityContextInterface aci, EventContext context) {
        try {
            tracer = getSbbContext().getTracer("com.test");
            tracer.info("Sbb1: Received a TCK event " + event);

            InitialContext ic = new InitialContext();
            ActivityContextNamingFacility acNaming = (ActivityContextNamingFacility) ic
                    .lookup(ActivityContextNamingFacility.JNDI_NAME);

            acNaming.bind(aci, "name");
            getChildRelation().create();

        } catch (javax.slee.facilities.NameAlreadyBoundException nabe) {
            throw new RuntimeException("The activity context is already bound to the name 'name', it should not be");
        } catch (javax.naming.NamingException ne) {
            throw new RuntimeException(ne.getMessage());
        } catch (javax.slee.CreateException ce) {
            throw new RuntimeException(ce.getMessage());
        }
    }

    public abstract ChildRelation getChildRelation();

    private Tracer tracer;
}
