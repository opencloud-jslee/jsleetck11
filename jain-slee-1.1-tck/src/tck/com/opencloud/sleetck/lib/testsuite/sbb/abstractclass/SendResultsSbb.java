/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.sbb.abstractclass;

import com.opencloud.sleetck.lib.sbbutils.BaseTCKSbb;
import com.opencloud.sleetck.lib.sbbutils.TCKSbbUtils;

import java.util.HashMap;

import javax.slee.facilities.Level;

public abstract class SendResultsSbb extends BaseTCKSbb {
    protected void setResultFailed(int assertionID, String msg) throws Exception {
        String tmsg = "Assertion " + assertionID + " failed: " + msg;
        TCKSbbUtils.createTrace(getSbbID(), Level.WARNING, tmsg, null);

        HashMap sbbData = new HashMap();
        sbbData.put("result", Boolean.FALSE);
        sbbData.put("message", msg);
        sbbData.put("id", new Integer(assertionID));
        TCKSbbUtils.getResourceInterface().sendSbbMessage(sbbData);
    }

    protected void setResultPassed(String msg) throws Exception {
        String tmsg = "Success: " + msg;
        TCKSbbUtils.createTrace(getSbbID(), Level.INFO, tmsg, null);

        HashMap sbbData = new HashMap();
        sbbData.put("result", Boolean.TRUE);
        sbbData.put("message", msg);
        TCKSbbUtils.getResourceInterface().sendSbbMessage(sbbData);
    }
}

