/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.facilities.timerfacility;

import com.opencloud.sleetck.lib.sbbutils.BaseTCKSbb;
import com.opencloud.sleetck.lib.sbbutils.TCKSbbUtils;
import com.opencloud.sleetck.lib.resource.events.TCKResourceEventX;
import com.opencloud.sleetck.lib.resource.sbbapi.TCKActivity;

import javax.naming.Context;
import javax.naming.InitialContext;

import java.util.HashMap;

import javax.slee.*;
import javax.slee.facilities.*;
import javax.slee.nullactivity.*;

/**
 * Test TimerFacility.setTimer() single timer method
 */
public abstract class SetTimerSingleSbb extends BaseTCKSbb {

    public void setSbbContext(SbbContext context) {
        super.setSbbContext(context);
        try {
            Context myEnv = (Context) new InitialContext().lookup("java:comp/env");

            timerFacility = (TimerFacility) myEnv.lookup("slee/facilities/timer");
        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }
    }

    // test starts here...
    public void onTCKResourceEventX1(TCKResourceEventX event, ActivityContextInterface aci) {
        try {
            TCKSbbUtils.createTrace(getSbbID(),Level.INFO,"Received start message",null);
            setTestName((String)event.getMessage());

            TimerOptions preserveAll = new TimerOptions();
            preserveAll.setPreserveMissed(TimerPreserveMissed.ALL);

            // Test setTimer single timer method - using either startTime < current (should fire
            // right away) or startTime > current.
            if (getTestName().equals("setTimerSingle1")) {
                timerFacility.setTimer(aci, null, 0, preserveAll);
            } else if (getTestName().equals("setTimerSingle2")) {
                timerFacility.setTimer(aci, null, System.currentTimeMillis() + 5000, preserveAll);
            }

        } catch(Exception e) {
            TCKSbbUtils.handleException(e);
        }
    }

    public void onTimerEvent(TimerEvent event, ActivityContextInterface aci) {
        try {
            TCKSbbUtils.createTrace(getSbbID(),Level.INFO,"Received timer event",null);

            setResultPassed("setTimer() single timer fired correctly");

            // NB: no failed case - if timer never fires then JavaTest will timeout and
            // fail the test.

            // we are done
            sendResultToTCK();

        } catch(Exception e) {
            TCKSbbUtils.handleException(e);
        }
    }

    public abstract void setTestName(String testName);
    public abstract String getTestName();

    private void sendResultToTCK() throws Exception {
        HashMap sbbData = new HashMap();
        sbbData.put("testname", getTestName());
        sbbData.put("result", result?"pass":"fail");
        sbbData.put("message", message);
        sbbData.put("id", new Integer(failedAssertionID));
        TCKSbbUtils.getResourceInterface().sendSbbMessage(sbbData);
    }

    private void setResultFailed(int assertionID, String msg) throws Exception {
        result = false;
        failedAssertionID = assertionID;
        message =  "Assertion " + assertionID + " failed: " + msg;
        TCKSbbUtils.createTrace(getSbbID(), Level.WARNING, message, null);
    }

    private void setResultPassed(String msg) throws Exception {
        result = true;
        message = "Success: " + msg;
        TCKSbbUtils.createTrace(getSbbID(), Level.INFO, message, null);
    }

    private TimerFacility timerFacility;

    private boolean result = false;
    private String message;
    private int failedAssertionID = -1;

}
