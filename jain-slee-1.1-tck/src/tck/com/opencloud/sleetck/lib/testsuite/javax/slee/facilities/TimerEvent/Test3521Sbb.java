/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.javax.slee.facilities.TimerEvent;

import com.opencloud.sleetck.lib.resource.events.TCKResourceEventX;
import com.opencloud.sleetck.lib.sbbutils.BaseTCKSbb;
import com.opencloud.sleetck.lib.sbbutils.TCKSbbUtils;

import javax.naming.InitialContext;
import javax.slee.ActivityContextInterface;
import javax.slee.facilities.TimerEvent;
import javax.slee.facilities.TimerFacility;
import javax.slee.facilities.TimerOptions;
import java.util.HashMap;

public abstract class Test3521Sbb extends BaseTCKSbb {

    private static final String JNDI_TIMERFACILITY_NAME = "java:comp/env/slee/facilities/timer";

    private static final long TIMEOUT = 10000;

    // Initial event
    public void onTCKResourceEventX1(TCKResourceEventX ev, ActivityContextInterface aci) {

        try {
            TimerOptions options = new TimerOptions();
            options.setTimeout(TIMEOUT);

            TimerFacility facility = (TimerFacility) new InitialContext().lookup(JNDI_TIMERFACILITY_NAME);

            // Create a finitely repeating timer of period TIMEOUT * 2
            facility.setTimer(aci, null, System.currentTimeMillis(), TIMEOUT * 2, 2, options);

        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }

    }

    public void onTimerEvent(TimerEvent event, ActivityContextInterface aci) {

        try {

            HashMap map = new HashMap();
            TimerFacility facility = (TimerFacility) new InitialContext().lookup(JNDI_TIMERFACILITY_NAME);

            facility.cancelTimer(event.getTimerID());

            if (event.getRemainingRepetitions() != event.getNumRepetitions() - 1) {
                map.put("Result", new Boolean(false));
                map.put("Message", "TimerEvent.getRemainingRepetitions() returned an incorrect value.");
                map.put("ID", new Integer(3521));
            } else {
                map.put("Result", new Boolean(true));
                map.put("Message", "Ok");
            }

            TCKSbbUtils.getResourceInterface().sendSbbMessage(map);
            return;

        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }

    }
}

