/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.javax.slee.ActivityContextInterface.Attach;

import com.opencloud.sleetck.lib.sbbutils.BaseTCKSbb;
import com.opencloud.sleetck.lib.sbbutils.TCKSbbUtils;
import com.opencloud.sleetck.lib.resource.events.TCKResourceEventX;

import javax.slee.ActivityContextInterface;
import javax.slee.facilities.Level;
import javax.slee.Sbb;
import javax.slee.SbbLocalObject;
import javax.slee.ChildRelation;

import java.util.HashMap;
import java.util.Iterator;

/**
 * TCKResourceEventX1 causes this SBB to attach its child SBB.
 */

public abstract class AttachEventsParentSbb extends BaseTCKSbb {

    public abstract ChildRelation getChildRelation();
 
    public void onTCKResourceEventX1(TCKResourceEventX event, ActivityContextInterface aci) {

        SbbLocalObject childLocalObject = null;

        // Create the child sbb local object, and attach it to the ACI.
        try {
            childLocalObject = getChildRelation().create();
            aci.attach(childLocalObject);
        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }
    }
}
