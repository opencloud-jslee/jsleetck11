/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.events.deploymentdescriptor;

import com.opencloud.sleetck.lib.SleeTCKTest;
import com.opencloud.sleetck.lib.SleeTCKTestUtils;
import com.opencloud.sleetck.lib.TCKTestErrorException;
import com.opencloud.sleetck.lib.TCKTestResult;

/**
 * The value of the event-direction attribute can be "Receive", "Fire" or 
 * "FireAndReceive".
 */
public class Test663Test implements SleeTCKTest {

    private static final String SERVICE_DU_PATH_PARAM = "serviceDUPath";
    private static final String SERVICE_INVALID_DU_PATH_PARAM = "serviceInvalidDUPath";

    private static final int TEST_ID = 663;

    public void init(SleeTCKTestUtils utils) {
        this.utils = utils;
    }

    /**
     * Perform the actual test.
     */

    public TCKTestResult run() throws Exception {

        utils.getLog().fine("Installing first deployable unit.");

        // Load the first DU.
        try {
            String duPath = utils.getTestParams().getProperty(SERVICE_DU_PATH_PARAM);
            utils.install(duPath);
            //            utils.uninstall(duID);
        } catch (TCKTestErrorException e) {
            if (e.getEnclosedException().getClass().equals(javax.slee.management.DeploymentException.class)) {
                // Failure - should have loaded first DU fine.
                return TCKTestResult.failed(TEST_ID, "Failed to install service with valid event directions");
            } else {
                // Other problem.
                throw(e);
            }
        }

        // Deactivating and uninstalling first service.
        utils.getLog().fine("Deactivating and uninstalling first service.");
        utils.deactivateAllServices();
        utils.uninstallAll();

        utils.getLog().fine("Installing second deployable unit.");

        // Load the second DU.
        try {
            String duPath = utils.getTestParams().getProperty(SERVICE_INVALID_DU_PATH_PARAM);
            utils.install(duPath);
            //            utils.uninstall(duID);
        } catch (TCKTestErrorException e) {
            if (e.getEnclosedException().getClass().equals(javax.slee.management.DeploymentException.class)) {
                // Success - should have refused to load second DU.
                return TCKTestResult.passed();
            } else {
                throw(e);
            }
        }

        // Failure - should have thrown an exception with invalid event direction.
        return TCKTestResult.failed(TEST_ID, "Installed service with invalid event direction.");
    }

    /**
     * Do all the pre-run configuration of the test.
     */

    public void setUp() throws Exception {
        // No setup -- all installing is done in run().
    }

    /**
     * Clean up after the test.
     */

    public void tearDown() throws Exception {
        utils.getLog().fine("Disconnecting from resource");
        utils.getResourceInterface().clearActivities();
        utils.getLog().fine("Deactivating and uninstalling service");
        utils.deactivateAllServices();
        utils.uninstallAll();
    }

    private SleeTCKTestUtils utils;
}
