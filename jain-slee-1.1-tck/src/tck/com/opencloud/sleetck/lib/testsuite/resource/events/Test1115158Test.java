/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.resource.events;

import java.util.HashMap;

import javax.slee.ComponentID;
import javax.slee.ServiceID;
import javax.slee.management.DeployableUnitDescriptor;
import javax.slee.management.DeployableUnitID;
import javax.slee.resource.ConfigProperties;
import javax.slee.resource.ResourceAdaptorID;

import com.opencloud.sleetck.lib.AbstractSleeTCKTest;
import com.opencloud.sleetck.lib.TCKTestErrorException;
import com.opencloud.sleetck.lib.TCKTestFailureException;
import com.opencloud.sleetck.lib.TCKTestResult;
import com.opencloud.sleetck.lib.infra.SleeTCKComponentConstants;
import com.opencloud.sleetck.lib.rautils.RMIObjectChannel;
import com.opencloud.sleetck.lib.testsuite.resource.RAMethods;
import com.opencloud.sleetck.lib.testsuite.resource.TCKMessage;
import com.opencloud.sleetck.lib.testsuite.resource.SimpleEvent;
import com.opencloud.sleetck.lib.testutils.jmx.DeploymentMBeanProxy;
import com.opencloud.sleetck.lib.testutils.jmx.ResourceManagementMBeanProxy;
import com.opencloud.sleetck.lib.testutils.jmx.ServiceManagementMBeanProxy;

/**
 * Basic event processing callback flags test.
 * <p>
 * Covers assertions: 1115158, 1115159, 1115166
 * 
 */
public class Test1115158Test extends AbstractSleeTCKTest {

    private static final String RA_NAME = "TCK_Event_Test_RA";
    private static final String RA_ENTITY_NAME = RA_NAME + "_Entity";
    private static final String RA_VENDOR = SleeTCKComponentConstants.TCK_VENDOR;
    private static final String RA_VERSION = "1.1";

    private static final String RA_LINK_NAME = "slee/resources/tck/simple";

    private static final String RESOURCE_DU_PATH_PARAM = "resourceDUPath";
    private static final String SERVICE_DU_PATH_PARAM = "serviceDUPath";

    public TCKTestResult run() throws Exception {
        RMIObjectChannel in = utils().getRMIObjectChannel();
        ResourceManagementMBeanProxy resourceMBean = utils().getResourceManagementMBeanProxy();
        DeploymentMBeanProxy deploymentMBean = utils().getDeploymentMBeanProxy();
        ServiceManagementMBeanProxy serviceMBean = utils().getServiceManagementMBeanProxy();

        ResourceAdaptorID raID = new ResourceAdaptorID(RA_NAME, RA_VENDOR, RA_VERSION);
        resourceMBean.createResourceAdaptorEntity(raID, RA_ENTITY_NAME, new ConfigProperties());
        resourceMBean.bindLinkName(RA_ENTITY_NAME, RA_LINK_NAME);

        // Install service
        String relativePath = utils().getTestParams().getProperty(SERVICE_DU_PATH_PARAM);
        String serviceDuPath = utils().getDeploymentUnitURL(relativePath);

        serviceDUID = deploymentMBean.install(serviceDuPath);
        ServiceID serviceID = getServiceID(serviceDUID);

        // Activate Service
        serviceMBean.activate(serviceID);

        // Activate RA - This will fire events.
        resourceMBean.activateResourceAdaptorEntity(RA_ENTITY_NAME);

        TCKMessage[] messages = castMessages(in.readQueue(utils().getTestTimeout()));
        getLog().info("Received the following messages:");
        for (int i = 0; i < messages.length; i++) {
            getLog().info(" " + messages[i].toString());
        }

        // Check event 1
        // endpoint.fireEvent(handle, eventID, new SimpleEvent(1), null, null);
        updateSuccessFlags(messages, 1);
        if (successCallbackReceived)
            throw new TCKTestFailureException(1115159,
                    "Received eventProcessingSuccessful callback when EventFlags.REQUEST_EVENT_PROCESSING_SUCCESSFUL_CALLBACK was not specified during fireEvent()");
        if (unreferencedCallbackReceived)
            throw new TCKTestFailureException(1115166,
                    "Received eventUnreferenced callback when EventFlags.REQUEST_EVENT_UNREFERENCED_CALLBACK was not specified during fireEvent()");
        if (!sbbEventReceived)
            throw new TCKTestFailureException(1115158, "Event was not delivered to SBB");

        // Check event 2
        // EventFlags.NO_FLAGS
        updateSuccessFlags(messages, 2);
        if (successCallbackReceived)
            throw new TCKTestFailureException(1115159,
                    "Received eventProcessingSuccessful callback when EventFlags.REQUEST_EVENT_PROCESSING_SUCCESSFUL_CALLBACK was not specified during fireEvent()");
        if (unreferencedCallbackReceived)
            throw new TCKTestFailureException(1115166,
                    "Received eventUnreferenced callback when EventFlags.REQUEST_EVENT_UNREFERENCED_CALLBACK was not specified during fireEvent()");
        if (!sbbEventReceived)
            throw new TCKTestFailureException(1115158, "Event was not delivered to SBB");

        // Check event 3
        // EventFlags.REQUEST_EVENT_UNREFERENCED_CALLBACK
        updateSuccessFlags(messages, 3);
        if (successCallbackReceived)
            throw new TCKTestFailureException(1115159,
                    "Received eventProcessingSuccessful callback when EventFlags.REQUEST_EVENT_PROCESSING_SUCCESSFUL_CALLBACK was not specified during fireEvent()");
        if (!unreferencedCallbackReceived)
            throw new TCKTestFailureException(1115166,
                    "Did not receive expected eventUnreferenced callback when EventFlags.REQUEST_EVENT_UNREFERENCED_CALLBACK was specified during fireEvent()");
        if (!sbbEventReceived)
            throw new TCKTestFailureException(1115158, "Event was not delivered to SBB");

        // Check event 4
        // EventFlags.REQUEST_PROCESSING_SUCCESSFUL_CALLBACK
        updateSuccessFlags(messages, 4);
        if (!successCallbackReceived)
            throw new TCKTestFailureException(1115159,
                    "Did not receive expected eventProcessingSuccessful callback when EventFlags.REQUEST_EVENT_PROCESSING_SUCCESSFUL_CALLBACK was specified during fireEvent()");
        if (unreferencedCallbackReceived)
            throw new TCKTestFailureException(1115166,
                    "Received eventUnreferenced callback when EventFlags.REQUEST_EVENT_UNREFERENCED_CALLBACK was not specified during fireEvent()");
        if (!sbbEventReceived)
            throw new TCKTestFailureException(1115158, "Event was not delivered to SBB");

        // Check event 5
        // EventFlags.REQUEST_PROCESSING_SUCCESSFUL_CALLBACK|EventFlags.REQUEST_EVENT_UNREFERENCED_CALLBACK
        updateSuccessFlags(messages, 5);
        if (!successCallbackReceived)
            throw new TCKTestFailureException(1115159,
                    "Did not receive expected eventProcessingSuccessful callback when EventFlags.REQUEST_EVENT_PROCESSING_SUCCESSFUL_CALLBACK and EventFlags.REQUEST_EVENT_UNREFERENCED_CALLBACK were specified during fireEvent()");
        if (!unreferencedCallbackReceived)
            throw new TCKTestFailureException(1115166,
                    "Did not receive expected eventUnreferenced callback when EventFlags.REQUEST_EVENT_PROCESSING_SUCCESSFUL_CALLBACK and EventFlags.REQUEST_EVENT_UNREFERENCED_CALLBACK were specified during fireEvent()");
        if (!sbbEventReceived)
            throw new TCKTestFailureException(1115158, "Event was not delivered to SBB");

        return TCKTestResult.passed();
    }

    public void tearDown() throws Exception {
        try {
            DeploymentMBeanProxy deploymentMBean = utils().getDeploymentMBeanProxy();
            try {
                if (serviceDUID != null) {
                    utils().deactivateService(getServiceID(serviceDUID));
                    deploymentMBean.uninstall(serviceDUID);
                }
            } finally {
                utils().removeRAEntities();
            }
        } finally {
            super.tearDown();
        }
    }

    public void setUp() throws Exception {
        String duPath = utils().getTestParams().getProperty(RESOURCE_DU_PATH_PARAM);
        utils().install(duPath);
    }

    //
    // Private
    //

    private TCKMessage[] castMessages(Object[] objects) {
        TCKMessage[] messages = new TCKMessage[objects.length];
        for (int i = 0; i < objects.length; i++)
            messages[i] = (TCKMessage) objects[i];
        return messages;
    }

    private ServiceID getServiceID(DeployableUnitID duID) throws TCKTestErrorException {
        DeployableUnitDescriptor descriptor;
        try {
            descriptor = utils().getDeploymentMBeanProxy().getDescriptor(duID);
        } catch (Exception e) {
            throw new TCKTestErrorException("An error occured while attempting to determine ServiceID", e);
        }
        ComponentID[] components = descriptor.getComponents();
        for (int i = 0; i < components.length; i++)
            if (components[i] instanceof ServiceID)
                return (ServiceID) components[i];
        return null;
    }

    // This is a very inefficient way of calculating these flags.
    private void updateSuccessFlags(TCKMessage[] messages, int sequenceID) throws TCKTestErrorException {
        successCallbackReceived = false;
        unreferencedCallbackReceived = false;
        sbbEventReceived = false;

        // Check event 1
        for (int i = 0; i < messages.length; i++) {
            TCKMessage message = messages[i];
            
            HashMap results = (HashMap) message.getArgument();
            if (results == null)
                throw new TCKTestErrorException("Null result received in message from test component: " + message);
            boolean fromSbb = false;
            if (Boolean.TRUE.equals(results.get("fromsbb")))
                fromSbb = true;
            SimpleEvent event = (SimpleEvent) results.get("event");
            if (event == null)
                throw new TCKTestErrorException("Null event contained in message from test component: " + message);
            
            if (fromSbb && event.getSequenceID() == sequenceID)
                sbbEventReceived = true;
            else if (!fromSbb && event.getSequenceID() == sequenceID && message.getMethod() == RAMethods.eventProcessingSuccessful)
                successCallbackReceived = true;
            else if (!fromSbb && event.getSequenceID() == sequenceID && message.getMethod() == RAMethods.eventUnreferenced)
                unreferencedCallbackReceived = true;
        }
    }

    private boolean successCallbackReceived;
    private boolean unreferencedCallbackReceived;
    private boolean sbbEventReceived;

    private DeployableUnitID serviceDUID = null;
}
