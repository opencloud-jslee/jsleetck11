/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.events.eventcontext;

import java.util.HashMap;

import javax.slee.ActivityContextInterface;
import javax.slee.EventContext;
import javax.slee.InitialEventSelector;
import javax.slee.facilities.Tracer;

import com.opencloud.sleetck.lib.resource.TCKActivityID;
import com.opencloud.sleetck.lib.resource.events.TCKResourceEventX;
import com.opencloud.sleetck.lib.resource.sbbapi.TCKActivity;
import com.opencloud.sleetck.lib.sbbutils.BaseTCKSbb;
import com.opencloud.sleetck.lib.sbbutils.TCKSbbUtils;

/**
 * AssertionID(1108196): Test In case of a programming error in the SBB, 
 * event delivery is only ever suspended for a time period. This time 
 * period can be provided by the SBB as an argument, or if not provided 
 * it is a platform specific default.
 * 
 * AssertionID(1108197): Test The SLEE automatically resumes the delivery
 * of the event after the period elapses.
 */
public abstract class Test1108196Sbb1 extends BaseTCKSbb {
    public static final String TRACE_MESSAGE_TCKResourceEventX1 = "Test1108196Sbb1:I got TCKResourceEventX1 on ActivityA";

    public static final String TRACE_MESSAGE_TCKResourceEventX2 = "Test1108196Sbb1:I got TCKResourceEventX2 on ActivityB";
    
    public static final String TRACE_MESSAGE_TCKResourceEventX3A = "Test1108196Sbb1:I got TCKResourceEventX3 on ActivityA";

    public static final String TRACE_MESSAGE_TCKResourceEventX3B = "Test1108196Sbb1:I got TCKResourceEventX3 on ActivityB";


    public InitialEventSelector initialEventSelector(InitialEventSelector ies) {
        ies.setCustomName("test");
        return ies;
    }

    public void onTCKResourceEventX1(TCKResourceEventX event, ActivityContextInterface aci, EventContext context) {
        try {
            //send message to TCK: I got TCKResourceEventX1 on ActivityA
            tracer = getSbbContext().getTracer("com.test");
            tracer.info(TRACE_MESSAGE_TCKResourceEventX1);

            setTestName((String) event.getMessage());

            //store context in CMP field
            setEventContext(context);

            if (getTestName().equals("TIMEOUT")) {
                //call suspendDelivery(5000ms) method now 
                context.suspendDelivery(5000);
            }
            else if (getTestName().equals("NOTIMEOUT")) {
                //call suspendDelivery() method now, we assumed the default timeout 
                //on the SLEE will be more than 10000ms to complete this test.
                context.suspendDelivery();
            }
            else {
                tracer.severe("Unexpected test name encountered during X1 event handler: " + getTestName());
                return;
            }
            sendResultToTCK("Test1108196Test", true, "Sbb1 received TCKResourceEventX1 on ActivityA", 1108196);


        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }
    }

    public void onTCKResourceEventX2(TCKResourceEventX event, ActivityContextInterface aci, EventContext context) {
        try {
            //send message to TCK: I got TCKResourceEventX2 on ActivityB
            tracer = getSbbContext().getTracer("com.test");
            tracer.info(TRACE_MESSAGE_TCKResourceEventX2);
            //get EventContext ec from CMP field
            EventContext ec = getEventContext();
            //assert ec.isSuspended(), check ec has been suspended or not
            //we assume there is a programming error here in the SBB. So we
            //failed to resume the Event Delivery.
            try {
                ec.suspendDelivery();
            } catch (java.lang.IllegalStateException e) {
                tracer.info("got expected IllegalStateException if the event delivery has already been suspended.");
            }
            
            if (!ec.isSuspended()) {
                ec.resumeDelivery();
            }
            sendResultToTCK("Test1108196Test", true, "Sbb1 received TCKResourceEventX2 on ActivityB", 1108196);
        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }
    }
    
    public void onTCKResourceEventX3(TCKResourceEventX event, ActivityContextInterface aci, EventContext context) {
        try {
            TCKActivity activity = (TCKActivity) context.getActivityContextInterface().getActivity();
            TCKActivityID activityID = activity.getID();

            if (activityID.getName().equals(Test1108196Test.activityNameB)) {
                //send message to TCK: I got TCKResourceEventX3 on ActivityB
                tracer = getSbbContext().getTracer("com.test");
                tracer.info(TRACE_MESSAGE_TCKResourceEventX3B);
                //get EventContext ec from CMP field
                EventContext ec = getEventContext();
                //assert ec.isSuspended(), check ec has been suspended or not 
                if (ec != null && ec.isSuspended()) {
                    sendResultToTCK("Test1108196Test", false, "SBB1:onTCKResourceEventX3-ERROR: The event delivery should not be " +
                            "suspended, the EventContext.isSuspended() returned false!", 1108196);
                    return;
                }
                sendResultToTCK("Test1108196Test", true, "Sbb1 received TCKResourceEventX3 on ActivityB", 1108196);
            } else if (activityID.getName().equals(Test1108196Test.activityNameA)) {
                //send message to TCK: I got TCKResourceEventX3 on ActivityA
                tracer = getSbbContext().getTracer("com.test");
                tracer.info(TRACE_MESSAGE_TCKResourceEventX3A);
                sendResultToTCK("Test1108196Test", true, "Sbb1 received TCKResourceEventX3 on ActivityA", 1108196);
            }

        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }
    }

    private void sendResultToTCK(String testName, boolean result, String message, int failedAssertionID) throws Exception {
        HashMap sbbData = new HashMap();
        sbbData.put("testname", testName);
        sbbData.put("result", result ? "pass" : "fail");
        sbbData.put("message", message);
        sbbData.put("id", new Integer(failedAssertionID));
        TCKSbbUtils.getResourceInterface().sendSbbMessage(sbbData);
    }

    // receivedEndEvent flag
    public abstract void setEventContext(EventContext eventContext);

    public abstract EventContext getEventContext();

    public abstract void setTestName(String testName);

    public abstract String getTestName();

    private Tracer tracer = null;

}
