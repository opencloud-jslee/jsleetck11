/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.resource.endpoint;

import java.util.HashMap;

import com.opencloud.sleetck.lib.TCKTestErrorException;
import com.opencloud.sleetck.lib.TCKTestFailureException;
import com.opencloud.sleetck.lib.TCKTestResult;
import com.opencloud.sleetck.lib.testsuite.resource.BaseResourceTest;
import com.opencloud.sleetck.lib.testsuite.resource.RAMethods;

/**
 * Test to ensure that visibility of activities started with
 * startActivityTransacted() is transactionally correct.
 * <p>
 * Test assertion: 1115215, 1115216
 */
public class Test1115215Test extends BaseResourceTest {

    private static final int ASSERTION_ID1 = 1115215;
    private static final int ASSERTION_ID2 = 1115216;

    public TCKTestResult run() throws Exception {
        HashMap results = sendMessage(RAMethods.startActivityTransacted, new Integer(1115215));
        if (results == null)
            throw new TCKTestFailureException(ASSERTION_ID1, "Test timed out while waiting for response from test component");

        Object result1 = results.get("result1");
        Object result2 = results.get("result2");
        Object result3 = results.get("result3");

        if (result1 instanceof Exception)
            throw new TCKTestFailureException(ASSERTION_ID1, "Unexpected exception thrown during test:", (Exception) result1);
        if (Boolean.FALSE.equals(result1))
            throw new TCKTestFailureException(ASSERTION_ID1, "Call to startActivityTransacted() does not appear to be transactionally isolated");
        if (!Boolean.TRUE.equals(result1))
            throw new TCKTestErrorException("Unexpected result received from test component: " + result1);

        if (result2 instanceof Exception)
            throw new TCKTestFailureException(ASSERTION_ID2, "Unexpected exception thrown during test:", (Exception) result2);
        if (Boolean.FALSE.equals(result2))
            throw new TCKTestFailureException(ASSERTION_ID2,
                    "Committed transaction did not create activity (subsequent attempts to create the same handle in a non-transactional manner succeeded)");
        if (!Boolean.TRUE.equals(result2))
            throw new TCKTestErrorException("Unexpected result received from test component: " + result2);

        if (result3 instanceof Exception)
            throw new TCKTestFailureException(ASSERTION_ID2, "Unexpected exception thrown during test:", (Exception) result3);
        if (Boolean.FALSE.equals(result3))
            throw new TCKTestFailureException(ASSERTION_ID2,
                    "Committed transaction did not create activity (subsequent attempts to create the same handle in a transactional manner succeeded)");
        if (!Boolean.TRUE.equals(result3))
            throw new TCKTestErrorException("Unexpected result received from test component: " + result3);

        return TCKTestResult.passed();
    }
}
