/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.javax.slee.management.AlarmDuplicateFilter;

import java.rmi.RemoteException;

import javax.management.Notification;
import javax.slee.management.AlarmDuplicateFilter;
import javax.slee.management.AlarmNotification;

import com.opencloud.sleetck.lib.AbstractSleeTCKTest;
import com.opencloud.sleetck.lib.TCKTestResult;
import com.opencloud.sleetck.lib.resource.TCKActivityID;
import com.opencloud.sleetck.lib.resource.events.TCKResourceEventX;
import com.opencloud.sleetck.lib.resource.testapi.TCKResourceListener;
import com.opencloud.sleetck.lib.resource.testapi.TCKResourceTestInterface;
import com.opencloud.sleetck.lib.testutils.BaseTCKResourceListener;
import com.opencloud.sleetck.lib.testutils.FutureResult;
import com.opencloud.sleetck.lib.testutils.jmx.AlarmMBeanProxy;

public class Test4066Test extends AbstractSleeTCKTest implements javax.management.NotificationListener {

    private static final String SERVICE_DU_PATH_PARAM = "DUPath";
    private static final int TEST_ID = 4066;

    /**
     * Perform the actual test.
     */
    public TCKTestResult run() throws Exception {
        result = new FutureResult(getLog());

        TCKResourceTestInterface resource = utils().getResourceInterface();
        TCKActivityID activityID = resource.createActivity("Test4066InitialActivity");
        resource.fireEvent(TCKResourceEventX.X1, TCKResourceEventX.X1,activityID, null);

        return result.waitForResultOrFail(utils().getTestTimeout(),"Timeout waiting for test result.", TEST_ID);
    }

    /**
     * Do all the pre-run configuration of the test.
     */
    public void setUp() throws Exception {

        resourceListener = new TCKResourceListenerImpl();
        setResourceListener(resourceListener);

        AlarmMBeanProxy alarmMBeanProxy = utils().getMBeanProxyFactory().createAlarmMBeanProxy(utils().getSleeManagementMBeanProxy().getAlarmMBean());
        alarmFilter = new AlarmDuplicateFilter(1000);
        alarmMBeanProxy.addNotificationListener(this, alarmFilter, null);

        setupService(SERVICE_DU_PATH_PARAM);
    }

    /**
     * Clean up after the test.
     */
    public void tearDown() throws Exception {
        super.tearDown();
    }

    private class TCKResourceListenerImpl extends BaseTCKResourceListener {

        public synchronized Object onSbbCall(Object argument) throws Exception {
            getLog().fine("Setting timerTriggered to true.");
            timerTriggered = true;
            return null;
        }

        public void onException(Exception e) throws RemoteException {
            getLog().warning("Received exception from SBB");
            getLog().warning(e);
            result.setError(e);
        }
    }

    public synchronized void handleNotification(Notification notification,Object handback) {

        if (notification instanceof AlarmNotification) {

            notifCount++;

            if (timerTriggered) {
                if (notifCount == 2)
                    result.setPassed();
                else
                    result.setFailed(4067,"Incorrect number of notifications received.");
                return;
            }

            return;
        }

        result.setError("Received non-alarm notification.");
        return;
    }

    private TCKResourceListener resourceListener;
    private FutureResult result;
    private AlarmDuplicateFilter alarmFilter;
    private int notifCount;
    private boolean timerTriggered;
}
