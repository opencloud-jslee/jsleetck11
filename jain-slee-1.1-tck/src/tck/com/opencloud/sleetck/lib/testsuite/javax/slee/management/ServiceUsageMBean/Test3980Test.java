/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.javax.slee.management.ServiceUsageMBean;

import com.opencloud.sleetck.lib.SleeTCKTest;
import com.opencloud.sleetck.lib.SleeTCKTestUtils;
import com.opencloud.sleetck.lib.TCKTestResult;
import com.opencloud.sleetck.lib.testutils.jmx.DeploymentMBeanProxy;
import com.opencloud.sleetck.lib.testutils.jmx.ServiceManagementMBeanProxy;
import com.opencloud.sleetck.lib.testutils.jmx.ServiceUsageMBeanProxy;

import javax.management.ObjectName;
import javax.slee.ComponentID;
import javax.slee.ServiceID;
import javax.slee.management.DeployableUnitDescriptor;
import javax.slee.management.DeployableUnitID;

public class Test3980Test implements SleeTCKTest {

    private static final String SERVICE_DU_PATH_PARAM = "DUPath";

    public void init(SleeTCKTestUtils utils) {
        this.utils = utils;
    }

    /**
     * Perform the actual test.
     */

    public TCKTestResult run() throws Exception {

        ServiceManagementMBeanProxy serviceProxy = utils.getMBeanProxyFactory().createServiceManagementMBeanProxy(utils.getSleeManagementMBeanProxy().getServiceManagementMBean());
        ObjectName usageMBeanName = serviceProxy.getServiceUsageMBean(serviceID);

        // Assertion 1430
        if(!utils.getMBeanFacade().isRegistered(usageMBeanName)) {
            return TCKTestResult.failed(1430,"ServiceManagementMBean.getServiceUsageMBean() returned "+
                    "the ObjectName of an MBean which was not registered with the MBeanServer");
        }

        ServiceUsageMBeanProxy serviceUsageMBean = utils.getMBeanProxyFactory().createServiceUsageMBeanProxy(usageMBeanName);
        if (!serviceUsageMBean.getService().equals(serviceID))
            return TCKTestResult.failed(3980, "ServiceUsageMBean.getService() returned an incorrect ServiceID object.");

        try {
            serviceUsageMBean.close();
        } catch (Exception e) {
            return TCKTestResult.failed(4009, "ServiceUsageMBean.close() threw an exception.");
        }

        return TCKTestResult.passed();
    }

    /**
     * Do all the pre-run configuration of the test.
     */

    public void setUp() throws Exception {

        utils.getLog().fine("Installing and activating service");

        // Install the Deployable Units
        String duPath = utils.getTestParams().getProperty(SERVICE_DU_PATH_PARAM);
        duID = utils.install(duPath);

        // Activate the DU
        utils.activateServices(duID, true);

        DeploymentMBeanProxy duProxy = utils.getDeploymentMBeanProxy();
        DeployableUnitDescriptor duDesc = duProxy.getDescriptor(duID);
        ComponentID components[] = duDesc.getComponents();
        // Get the ServiceID from the components.
        for (int i = 0; i < components.length; i++) {
            if (components[i] instanceof ServiceID) {
                utils.getLog().fine("Setting serviceID value.");
                serviceID = (ServiceID) components[i];
                continue;
            }
        }

    }

    /**
     * Clean up after the test.
     */

    public void tearDown() throws Exception {
        utils.getLog().fine("Disconnecting from resource");
        utils.getResourceInterface().clearActivities();
        utils.getLog().fine("Deactivating and uninstalling service");
        utils.deactivateAllServices();
        utils.uninstallAll();
    }

    private SleeTCKTestUtils utils;
    private DeployableUnitID duID;

    private ServiceID serviceID;

}
