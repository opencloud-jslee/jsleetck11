/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.resource.context;

import java.util.HashMap;

import javax.slee.ActivityContextInterface;
import javax.slee.ServiceID;
import javax.slee.resource.ResourceAdaptorContext;

import com.opencloud.sleetck.lib.rautils.UOID;
import com.opencloud.sleetck.lib.testsuite.resource.BaseResourceSbb;
import com.opencloud.sleetck.lib.testsuite.resource.RAMethods;
import com.opencloud.sleetck.lib.testsuite.resource.SimpleEvent;
import com.opencloud.sleetck.lib.testsuite.resource.SimpleSbbInterface;

public abstract class Test1115205Sbb extends BaseResourceSbb {

    // Event handler
    public void onSimpleEvent(SimpleEvent event, ActivityContextInterface aci) {
        tracer.info("onSimpleEvent() event handler called with event: " + event);
        try {
            int sequenceID = event.getSequenceID();
            SimpleSbbInterface sbbInterface = getSbbInterface();
            ResourceAdaptorContext raContext = sbbInterface.getResourceAdaptorContext();

            HashMap results = new HashMap();
            try {
                ServiceID invokingService = raContext.getInvokingService();
                results.put("result2", invokingService);
            } catch (Exception e) {
                tracer.severe("An error occured while trying to call getInvokingService() on the TCK_Context_Test_RA from an Sbb:", e);
                results.put("result2", e);
            }
            sendSbbMessage(sbbUID, sequenceID, RAMethods.getInvokingService, results);
        } catch (Exception e) {
            tracer.severe("Exception caught while trying to send message from SBB to TCK", e);
        }
    }

    private static final UOID sbbUID = UOID.createUOID();
}
