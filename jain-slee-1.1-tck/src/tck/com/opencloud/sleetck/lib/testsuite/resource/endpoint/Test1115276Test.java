/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.resource.endpoint;

import com.opencloud.sleetck.lib.TCKTestErrorException;
import com.opencloud.sleetck.lib.TCKTestFailureException;
import com.opencloud.sleetck.lib.TCKTestResult;
import com.opencloud.sleetck.lib.testsuite.resource.BaseResourceTest;
import com.opencloud.sleetck.lib.testsuite.resource.RAMethods;

/**
 * Test to ensure that endActivity() is non-transactional.
 * <p>
 * Test assertion: 1115276
 */
public class Test1115276Test extends BaseResourceTest {

    private final static int ASSERTION_ID = 1115276;

    public TCKTestResult run() throws Exception {
        int sequenceID = nextMessageID();

        MultiResponseListener listener = new MultiResponseListener(sequenceID);
        listener.addExpectedResult("result1");
        listener.addExpectedResult("result2");

        sendMessage(RAMethods.endActivity, new Integer(ASSERTION_ID), listener, sequenceID);

        Object result1 = listener.getResult("result1"); // Reply from RA
        Object result2 = listener.getResult("result2"); // Reply from SBB

        if (result1 == null)
            throw new TCKTestErrorException("Test timed out while waiting for response from test resource adaptor");
        // If SBB reply is missing it's a test failure, not an error.
        if (result2 == null)
            throw new TCKTestFailureException(ASSERTION_ID,
                    "Test timed out waiting for response from test sbb (which would indicate onActivityEndEvent has been invoked)");

        if (result1 instanceof Exception)
            throw new TCKTestFailureException(ASSERTION_ID, "Unexpected exception thrown during test by resource adaptor:", (Exception) result1);
        if (!Boolean.TRUE.equals(result1))
            throw new TCKTestErrorException("Unexpected result received from resource adaptor test component: " + result1);

        if (!Boolean.TRUE.equals(result2))
            throw new TCKTestErrorException("Unexpected result received from sbb test component: " + result2);

        return TCKTestResult.passed();
    }
}
