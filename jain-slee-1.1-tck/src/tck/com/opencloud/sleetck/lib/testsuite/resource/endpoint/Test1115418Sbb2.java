/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.resource.endpoint;

import java.util.HashMap;

import javax.slee.ActivityContextInterface;

import com.opencloud.sleetck.lib.rautils.UOID;
import com.opencloud.sleetck.lib.testsuite.resource.BaseResourceSbb;
import com.opencloud.sleetck.lib.testsuite.resource.RAMethods;
import com.opencloud.sleetck.lib.testsuite.resource.SimpleEvent;

/**
 * This SBB should only ever receive events fired from SBB 1.
 */
public abstract class Test1115418Sbb2 extends BaseResourceSbb {

    // Event handler
    public void onSimpleEvent(SimpleEvent event, ActivityContextInterface aci) {
        tracer.info("(SBB2) onSimpleEvent() event handler called with event: " + event);
        int sequenceID = event.getSequenceID();

        HashMap results = new HashMap();

        if ("from SBB".equals(event.getPayload())) {
            tracer.info("Informing TCK of SBB 2 event processing.");
            results.put("result-sbb2", Boolean.TRUE);
        } else {
            // Ignore events not from the RA and the other SBB
            tracer.info("Ignoring event on SBB 2: " + event);
            detach(aci);
            return;
        }

        sendSbbMessage(sbbUID, sequenceID, RAMethods.startActivitySuspended, results);

        detach(aci);
    }

    private static final UOID sbbUID = UOID.createUOID();
}
