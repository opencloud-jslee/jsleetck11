/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.management.ProfileProvisioningMBean;

import java.util.Collection;

import javax.management.Attribute;
import javax.management.ObjectName;
import javax.slee.profile.ProfileSpecificationID;
import javax.slee.profile.query.Equals;
import javax.slee.profile.query.GreaterThan;
import javax.slee.profile.query.GreaterThanOrEquals;
import javax.slee.profile.query.LessThan;
import javax.slee.profile.query.LessThanOrEquals;
import javax.slee.profile.query.NotEquals;
import javax.slee.profile.query.QueryExpression;
import javax.slee.profile.query.RangeMatch;

import com.opencloud.sleetck.lib.AbstractSleeTCKTest;
import com.opencloud.sleetck.lib.TCKTestFailureException;
import com.opencloud.sleetck.lib.TCKTestResult;
import com.opencloud.sleetck.lib.infra.SleeTCKComponentConstants;
import com.opencloud.sleetck.lib.testutils.ProfileUtils;
import com.opencloud.sleetck.lib.testutils.jmx.ProfileMBeanProxy;
import com.opencloud.sleetck.lib.testutils.jmx.ProfileProvisioningMBeanProxy;

/*
 * The getProfilesByDynamicQuery method of the ProfileProvisioningMBean interface
 * A Collection of ProfileID objects that identify the Profiles in the Profile Table
 * that satisfy the search criteria are expected
 */

public class Test1114007Test extends AbstractSleeTCKTest {

    public static final String SERVICE_DU_PATH_PARAM= "serviceDUPath";

    public static final String SPEC_NAME = "Test1114007Profile";
    public static final String SPEC_VERSION = "1.1";
    public static final String PROFILE_TABLE_NAME = "Test1114007ProfileTable";
    public static final String QUERY_NAME = "compareStringParam";
    public static final String QUERY_PARAM = "Test1114007Query";
    public static final String PROFILE_NAME  = "Test1114007Profile_1";

    public static final QueryExpression equalsString = new Equals("stringValue", "42");
    public static final QueryExpression notEqualsString = new NotEquals("stringValue", "41");
    public static final QueryExpression greaterThanIntObj = new GreaterThan("intObjValue", new Integer(41));
    public static final QueryExpression greaterThanOrEqualsIntObj = new GreaterThanOrEquals("intObjValue", new Integer(42));
    public static final QueryExpression lessThanIntObj = new LessThan("intObjValue", new Integer(43));
    public static final QueryExpression lessThanOrEqualsIntObj = new LessThanOrEquals("intObjValue", new Integer(42));
    public static final QueryExpression rangeMatchString = new RangeMatch("stringValue", "23", "42");

    public static final int TEST_ID = 1114007;


    /*
     * 1114007 test the dynamic query behaviour of ProfileProvisioningMBean
     */

    public TCKTestResult run() throws Exception {

        try {
            //Create profile table
            ProfileSpecificationID specID = new ProfileSpecificationID(SPEC_NAME, SleeTCKComponentConstants.TCK_VENDOR, SPEC_VERSION);
            profileProvisioning.createProfileTable(specID, PROFILE_TABLE_NAME);
            getLog().fine("1114007: Added profile table "+PROFILE_TABLE_NAME);

            // Set the attribute value
            ObjectName profile = profileProvisioning.createProfile(PROFILE_TABLE_NAME, PROFILE_NAME);
            ProfileMBeanProxy proxy = utils().getMBeanProxyFactory().createProfileMBeanProxy(profile);
            utils().getMBeanFacade().setAttribute(profile, new Attribute("StringValue", "42"));
            utils().getMBeanFacade().setAttribute(profile, new Attribute("IntObjValue", new Integer(42)));
            getLog().fine("1114007: Set Attribute in profile table to: "+QUERY_PARAM);

            // Commit the profile,
            proxy.commitProfile();
            proxy.closeProfile();
        } catch (Exception e) {
            getLog().warning(e);
            return TCKTestResult.failed(1114007, "ProfileProvisioningMBean has error creating ProfileTables: " + e.getClass().toString());
        }


        try {
            // 1114019 Equals
            testQuery(equalsString, 1, 1114019);

            // 1114010 NotEquals
            testQuery(notEqualsString, 1, 1114010);

            // 1114013 GreaterThan
            testQuery(greaterThanIntObj, 1, 1114013);

            // 1114016 GreaterThanOrEquals
            testQuery(greaterThanOrEqualsIntObj, 1, 1114016);

            // 1114005 LessThan
            testQuery(lessThanIntObj, 1, 1114005);

            // 1114017 LessThanOrEquals
            testQuery(lessThanOrEqualsIntObj, 1, 1114017);

        } catch (Exception e) {
            getLog().warning(e);
            return TCKTestResult.error("ProfileProvisioningMBean did not perform getProfilesByStaticQuery");
        }

        logSuccessfulCheck(1114007);
        return TCKTestResult.passed();
    }



    public void setUp() throws Exception {

        setupService(SERVICE_DU_PATH_PARAM);

        getLog().fine("1114007: setUp: Create ProfileProvisioningProxy ");
        profileUtils = new ProfileUtils(utils());
        profileProvisioning = profileUtils.getProfileProvisioningProxy();
    }

    public void tearDown() throws Exception {

        try {
            profileUtils.removeProfileTable(PROFILE_TABLE_NAME);
        }
        catch (Exception e) {
            getLog().warning("Caught exception while trying to remove profile table:");
            getLog().warning(e);
        }

        super.tearDown();
    }

    private void logSuccessfulCheck(int assertionID) {
        utils().getLog().info("Check for assertion "+assertionID+" OK");
    }

    private void testQuery(QueryExpression query, int expReturn, int assertionID) throws TCKTestFailureException {
        Collection col;

        //run query
        try {
            col = profileProvisioning.getProfilesByDynamicQuery(PROFILE_TABLE_NAME, query);
            getLog().fine(assertionID + ": found "+col.size()+" records");
            //check returned profile
            if (col.size()!=expReturn)
                throw new TCKTestFailureException(assertionID, "The following query did not return the expected number of profiles: "+query+". Number of found profiles was "+col.size()+", expected "+expReturn);
        } catch (Exception e) {
            throw new TCKTestFailureException(assertionID, "Exception occurred when executing query "+query, e);
        }
        logSuccessfulCheck(assertionID);
    }


    private ProfileUtils profileUtils;
    private ProfileProvisioningMBeanProxy profileProvisioning;
}
