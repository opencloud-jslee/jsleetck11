/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.sbb.abstractclass;

import com.opencloud.sleetck.lib.sbbutils.TCKSbbUtils;
import com.opencloud.sleetck.lib.resource.events.TCKResourceEventX;

import javax.slee.*;

/**
 * Test sbbExceptionThrown() method from spec s6.11.3
 */
public abstract class SbbExceptionThrownNonEventSbb extends SendResultsSbb {
    private static final String EXCEPTION_MSG = "Exception Test Message for SbbExceptionThrownSbbNonEvent SBB";

    public void sbbPostCreate() {
        throw new SLEEException(EXCEPTION_MSG, new Exception(EXCEPTION_MSG));
    }

    public void onTCKResourceEventX1(TCKResourceEventX event, ActivityContextInterface aci) {}

    public void sbbExceptionThrown(Exception exception, Object event, ActivityContextInterface aci) {
        try {
            // check parameters for exception thrown from non-event method
            if (event != null) {
                setResultFailed(463, "sbbExceptionThrown should have a null event parameter for exceptions thrown from non-event methods");
                return;
            }
            if (aci != null) {
                setResultFailed(463, "sbbExceptionThrown should have a null ActivityContextInterface parameter for exceptions thrown from non-event methods");
                return;
            }

            setResultPassed("sbbExceptionThrown for non-event methods passed");
        }
        catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }
    }
}

