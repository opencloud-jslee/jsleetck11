/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.transactions;

import java.util.HashMap;

import javax.slee.ActivityContextInterface;

import com.opencloud.sleetck.lib.resource.events.TCKResourceEventX;
import com.opencloud.sleetck.lib.sbbutils.BaseTCKSbb;
import com.opencloud.sleetck.lib.sbbutils.TCKSbbUtils;

public abstract class Test848Sbb extends BaseTCKSbb {

    // Initial event
    public void onTCKResourceEventX1(TCKResourceEventX ev, ActivityContextInterface aci) {
        try {
            setTransaction(TCKSbbUtils.getResourceAdaptorInterface().getTransactionIDAccess().getCurrentTransactionID());

        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
            return;
        }

        checkTransaction();
    }

    public void checkTransaction() {

        try {
            HashMap map = new HashMap();

            Object currentTransaction = TCKSbbUtils.getResourceAdaptorInterface().getTransactionIDAccess().getCurrentTransactionID();
            if (currentTransaction.equals(getTransaction())) {
                map.put("Result", new Boolean(true));
                map.put("Message", "Ok");
            } else {
                map.put("Result", new Boolean(false));
                map.put("Message", "Invoked transactional method was not invoked in the same transaction as the top-level method called by the SLEE.");
            }
            TCKSbbUtils.getResourceInterface().sendSbbMessage(map);
        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }
    }

    public abstract void setTransaction(Object txnID);
    public abstract Object getTransaction();
}
