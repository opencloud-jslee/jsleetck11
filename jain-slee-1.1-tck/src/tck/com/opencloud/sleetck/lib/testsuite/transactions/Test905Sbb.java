/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.transactions;

import com.opencloud.sleetck.lib.TCKTestErrorException;
import com.opencloud.sleetck.lib.resource.events.TCKResourceEventX;
import com.opencloud.sleetck.lib.sbbutils.BaseTCKSbb;
import com.opencloud.sleetck.lib.sbbutils.TCKSbbUtils;

import javax.slee.ActivityContextInterface;
import javax.slee.Address;
import javax.slee.ChildRelation;
import javax.slee.facilities.Level;
import java.util.HashMap;

/**
 * In initial event create child SBB, attach it to the ACI, and tell it about its
 * parent's SbbLocalObject.  Then fire two events, the first of which the child
 * receives, the second of which the parent receives.
 *
 * In the child's event handler for the event the child calls a method on the
 * parent's SBB local object to set a CMP field, sends a message to the TCK containing
 * the transaction ID, and then throwns RuntimeException.
 *
 * The sbbExceptionThrown() method returns a value to the TCK.
 *
 * The sbbRolledBack() method returns a value to the TCK.
 *
 * In the parent's event handler for the second event the parent verifies that its
 * CMP field is back to zero value (i.e. the transaction rolled back) and sends
 * a message to the TCK with pass/fail on that test.  The parent then sends a message
 * to the TCK asking it to compute the overall test result.
 */

public abstract class Test905Sbb extends BaseTCKSbb {

    public void sbbRolledBack(javax.slee.RolledBackContext context) {
        try {
            TCKSbbUtils.createTrace(getSbbID(), Level.FINE, "In parent's sbbRolledBack method.", null);
        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }
    }

    public void sbbExceptionThrown(Exception exception, Object event, ActivityContextInterface aci) {
        try {
            TCKSbbUtils.createTrace(getSbbID(), Level.FINE, "In parent's sbbExceptionThrown method.", null);
        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }
    }

    // Initial event
    public void onTCKResourceEventX1(TCKResourceEventX ev, ActivityContextInterface aci) {

        Test905ChildSbbLocalObject child = null;
        try {
            // Create the child, attach to the ACI, then set its parent's local object.
            child = (Test905ChildSbbLocalObject) getChildRelation().create();
            aci.attach(child);
            child.setParentSbbLocalObject((Test905SbbLocalObject) getSbbContext().getSbbLocalObject());
            setChildSbbLocalObject(child);
        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
            return;
        }

        // Fire two events - the first will be processed by the child and the second by this SBB.
        fireTest905Event(new Test905Event(), aci, null);
        fireTest905SecondEvent(new Test905SecondEvent(), aci, null);
    }

    public void onTest905SecondEvent(Test905SecondEvent event, ActivityContextInterface aci) {

        HashMap map = new HashMap();

        // Verify if the rollback occurred.
        map.put("Type", "RolledBack");
        switch (getRealInteger()) {
        case 0:
            map.put("Data", new Boolean(true));
            break;
        case 42:
            map.put("Data", new Boolean(false));
            break;
        default:
            TCKSbbUtils.handleException(new TCKTestErrorException("CMP field in Parent SBB had unexpected value."));
            return;
        }

        try {
            TCKSbbUtils.getResourceInterface().sendSbbMessage(map);
        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }

        // And finally, the "give me the results" message.
        map = new HashMap();
        map.put("Type", "Result");
        try {
            TCKSbbUtils.getResourceInterface().sendSbbMessage(map);
        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }
    }

    public abstract void fireTest905SecondEvent(Test905SecondEvent event, ActivityContextInterface aci, Address address);
    public abstract void fireTest905Event(Test905Event event, ActivityContextInterface aci, Address address);

    public abstract ChildRelation getChildRelation();

    public void setInteger(int foo) {
        setRealInteger(foo);
    }

    public abstract void setRealInteger(int foo);
    public abstract int getRealInteger();

    public abstract void setChildSbbLocalObject(Test905ChildSbbLocalObject sbbLocalObject);
    public abstract Test905ChildSbbLocalObject getChildSbbLocalObject();

}
