/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.profiles.querystatic;

import java.rmi.RemoteException;
import java.util.Collection;
import java.util.HashMap;

import javax.management.Attribute;
import javax.management.ObjectName;
import javax.slee.Address;
import javax.slee.AddressPlan;
import javax.slee.profile.ProfileSpecificationID;

import com.opencloud.sleetck.lib.AbstractSleeTCKTest;
import com.opencloud.sleetck.lib.OperationTimedOutException;
import com.opencloud.sleetck.lib.TCKTestErrorException;
import com.opencloud.sleetck.lib.TCKTestFailureException;
import com.opencloud.sleetck.lib.TCKTestResult;
import com.opencloud.sleetck.lib.infra.SleeTCKComponentConstants;
import com.opencloud.sleetck.lib.resource.TCKActivityID;
import com.opencloud.sleetck.lib.resource.TCKSbbMessage;
import com.opencloud.sleetck.lib.resource.events.TCKResourceEventX;
import com.opencloud.sleetck.lib.resource.testapi.TCKResourceTestInterface;
import com.opencloud.sleetck.lib.sbbutils.events2.SbbBaseMessageConstants;
import com.opencloud.sleetck.lib.testutils.ProfileUtils;
import com.opencloud.sleetck.lib.testutils.QueuingResourceListener;
import com.opencloud.sleetck.lib.testutils.jmx.ProfileMBeanProxy;
import com.opencloud.sleetck.lib.testutils.jmx.ProfileProvisioningMBeanProxy;


public class Test1110415Test extends AbstractSleeTCKTest {

    private static final String SERVICE_DU_PATH_PARAM= "serviceDUPath";
    private static final String EVENT_DU_PATH_PARAM= "eventDUPath";

    private static final String SPEC_NAME = "Test1110415Profile";
    private static final String SPEC_VERSION = "1.0";

    private void setupProfile(String profileName,
            String stringValue, int intValue, char charValue,
            boolean boolValue, double doubleValue, byte byteValue, short shortValue,
            long longValue, float floatValue, Integer intObjValue, Character charObjValue,
            Boolean boolObjValue, Double doubleObjValue, Byte byteObjValue, Short shortObjValue,
            Long longObjValue, Float floatObjValue, Address address) throws Exception {

        ObjectName profile = profileProvisioning.createProfile(Test1110415Sbb.PROFILE_TABLE_NAME, profileName);
        ProfileMBeanProxy profileProxy = utils().getMBeanProxyFactory().createProfileMBeanProxy(profile);
        getLog().fine("Added profile "+profileName);

        //singular values:
        utils().getMBeanFacade().setAttribute(profile, new Attribute("StringValue", stringValue));
        utils().getMBeanFacade().setAttribute(profile, new Attribute("IntValue", new Integer(intValue)));
        utils().getMBeanFacade().setAttribute(profile, new Attribute("CharValue", new Character(charValue)));
        utils().getMBeanFacade().setAttribute(profile, new Attribute("BoolValue", new Boolean(boolValue)));
        utils().getMBeanFacade().setAttribute(profile, new Attribute("DoubleValue", new Double(doubleValue)));
        utils().getMBeanFacade().setAttribute(profile, new Attribute("ByteValue", new Byte(byteValue)));
        utils().getMBeanFacade().setAttribute(profile, new Attribute("ShortValue", new Short(shortValue)));
        utils().getMBeanFacade().setAttribute(profile, new Attribute("LongValue", new Long(longValue)));
        utils().getMBeanFacade().setAttribute(profile, new Attribute("FloatValue", new Float(floatValue)));
        utils().getMBeanFacade().setAttribute(profile, new Attribute("IntObjValue", intObjValue));
        utils().getMBeanFacade().setAttribute(profile, new Attribute("CharObjValue", charObjValue));
        utils().getMBeanFacade().setAttribute(profile, new Attribute("BoolObjValue", boolObjValue));
        utils().getMBeanFacade().setAttribute(profile, new Attribute("DoubleObjValue", doubleObjValue));
        utils().getMBeanFacade().setAttribute(profile, new Attribute("ByteObjValue", byteObjValue));
        utils().getMBeanFacade().setAttribute(profile, new Attribute("ShortObjValue", shortObjValue));
        utils().getMBeanFacade().setAttribute(profile, new Attribute("LongObjValue", longObjValue));
        utils().getMBeanFacade().setAttribute(profile, new Attribute("FloatObjValue", floatObjValue));
        utils().getMBeanFacade().setAttribute(profile, new Attribute("Address", address));

        getLog().fine("Set CMP field values for profile "+profileName);

        profileProxy.commitProfile();
        profileProxy.closeProfile();
    }

    /**
     * This test trys to run some static queries that cover a vast range of theoretically possible
     * combinations and variations.
     */
    public TCKTestResult run() throws Exception {

        //Create profile tables
        ProfileSpecificationID specID = new ProfileSpecificationID(SPEC_NAME, SleeTCKComponentConstants.TCK_VENDOR, SPEC_VERSION);
        profileProvisioning.createProfileTable(specID, Test1110415Sbb.PROFILE_TABLE_NAME);
        getLog().fine("Added profile table "+Test1110415Sbb.PROFILE_TABLE_NAME);


        //create profiles
        setupProfile(Test1110415Sbb.PROFILE_NAME0_1,
                "42", 42, 'X', true, (double)42, (byte)42, (short)42, (long)42, (float)42,
                new Integer(42), new Character('X'), new Boolean(true), new Double(42), new Byte("42"), new Short("42"), new Long(42), new Float(42), new Address(AddressPlan.SLEE_PROFILE, "XY/X"));
        setupProfile(Test1110415Sbb.PROFILE_NAME0_2,
                "42", 42, 'X', true, (double)42, (byte)42, (short)42, (long)42, (float)42,
                new Integer(42), new Character('X'), new Boolean(true), new Double(42), new Byte("42"), new Short("42"), new Long(42), new Float(42), new Address(AddressPlan.SLEE_PROFILE, "XY/X"));



        setupProfile(Test1110415Sbb.PROFILE_NAME1_1,
                "41", 41, 'Y', true, (double)41, (byte)41, (short)41, (long)41, (float)41,
                new Integer(41), new Character('Y'), new Boolean(true), new Double(41), new Byte("41"), new Short("41"), new Long(41), new Float(41), new Address(AddressPlan.SLEE_PROFILE, "XY/Y"));
        setupProfile(Test1110415Sbb.PROFILE_NAME1_2,
                "41", 41, 'Y', true, (double)41, (byte)41, (short)41, (long)41, (float)41,
                new Integer(41), new Character('Y'), new Boolean(true), new Double(41), new Byte("41"), new Short("41"), new Long(41), new Float(41), new Address(AddressPlan.SLEE_PROFILE, "XY/Y"));



        setupProfile(Test1110415Sbb.PROFILE_NAME2_1,
                "23", 23, 'Z', true, (double)23, (byte)23, (short)23, (long)23, (float)23,
                new Integer(23), new Character('Z'), new Boolean(true), new Double(23), new Byte("23"), new Short("23"), new Long(23), new Float(23), new Address(AddressPlan.SLEE_PROFILE, "XY/Z"));
        setupProfile(Test1110415Sbb.PROFILE_NAME2_2,
                "23", 23, 'Z', true, (double)23, (byte)23, (short)23, (long)23, (float)23,
                new Integer(23), new Character('Z'), new Boolean(true), new Double(23), new Byte("23"), new Short("23"), new Long(23), new Float(23), new Address(AddressPlan.SLEE_PROFILE, "XY/Z"));



        setupProfile(Test1110415Sbb.PROFILE_NAME3,
                "99", 99, 'A', false, (double)99, (byte)99, (short)99, (long)99, (float)99,
                new Integer(99), new Character('A'), new Boolean(false), new Double(99), new Byte("99"), new Short("99"), new Long(99), new Float(99), new Address(AddressPlan.SLEE_PROFILE, "XY/A"));


        getLog().fine("Start firing static queries to the SLEE via ProfileProvisioningMBean...");
        do_RUN_QUERIES();
        getLog().fine("Completed queries via ProfileProvisioningMBean successfully.");


        getLog().fine("Start sending events to SLEE to get Sbb to run the static queries...");
        sendResourceEvent(Test1110415Sbb.RUN_QUERIES);
        getLog().fine("Completed test sequence successfully.");


        return TCKTestResult.passed();
    }

    private void do_RUN_QUERIES() throws TCKTestFailureException, Exception {

        Collection col;

        //1110419: query options element is accepted if it's there
        //1110420: read-only may be set to 'False'
        //1110424: as a max-match of 1 is specified only 1 profile (instead of 2) should be returned
        col = profileProvisioning.getProfilesByStaticQuery(Test1110415Sbb.PROFILE_TABLE_NAME, "allParamTypesMatches1NotReadOnly", new Object[]{"42",
                new Character('X'), new Integer(42), new Boolean(true), new Double(42), new Byte(42+""), new Short(42+""), new Long(42), new Float(42.0),
                new Character('X'), new Integer(42), new Boolean(true), new Double(42), new Byte(42+""), new Short(42+""), new Long(42), new Float(42.0)});
        if (col.size()==1)
            getLog().fine("Maximum no. of profiles returned by query was reduced as expected.");
        else
            throw new TCKTestFailureException(1110424, "Maximum no. of profiles returned by the query should have been reduced to 1 but were "+col.size());


        //1110420: read-only may be set to 'True'
        //1110424: as no max-match is specified all matches should be returned
        col = profileProvisioning.getProfilesByStaticQuery(Test1110415Sbb.PROFILE_TABLE_NAME, "hasDescriptionNoMatchMaxIsReadOnly", new Object[]{});
        if (col.size()==2)
            getLog().fine("Expected no. of profiles returned by query 'hasDescriptionNoMatchMaxIsReadOnly'.");
        else
            throw new TCKTestFailureException(1110424, "No. of matching profiles in query 'hasDescriptionNoMatchMaxIsReadOnly' was "+col.size()+" but should have been 2");



        //1110422: read-only can be omitted
        col = profileProvisioning.getProfilesByStaticQuery(Test1110415Sbb.PROFILE_TABLE_NAME, "defaultReadOnlyNoMaxMatches", new Object[]{});
        if (col.size()==2)
            getLog().fine("Expected no. of profiles returned by query 'defaultReadOnlyNoMaxMatches'.");
        else
            throw new TCKTestFailureException(1110422, "No. of matching profiles in query 'defaultReadOnlyNoMaxMatches' was "+col.size()+" but should have been 2");



        //1110419: query-options element can be omitted
        //1110418: description element is optional
        col = profileProvisioning.getProfilesByStaticQuery(Test1110415Sbb.PROFILE_TABLE_NAME, "noDescriptionNoOptions", new Object[]{});
        if (col.size()==2)
            getLog().fine("Expected no. of profiles returned by query 'noDescriptionNoOptions'.");
        else
            throw new TCKTestFailureException(1110418, "No. of matching profiles in query 'noDescriptionNoOptions' was "+col.size()+" but should have been 2");



        //1110433: op is one of the binary operators, thus can be "equals", "not-equals", "less-than
        //"less-than-or-equals", "greater-than", "greater-than-or-equals"
        col = profileProvisioning.getProfilesByStaticQuery(Test1110415Sbb.PROFILE_TABLE_NAME, "compareIntEquals", new Object[]{});
        if (col.size()==2)
            getLog().fine("Expected no. of profiles returned by query 'compareIntEquals'.");
        else
            throw new TCKTestFailureException(1110433, "No. of matching profiles in query 'compareIntEquals' was "+col.size()+" but should have been 2");


        col = profileProvisioning.getProfilesByStaticQuery(Test1110415Sbb.PROFILE_TABLE_NAME, "compareIntNotEquals", new Object[]{});
        if (col.size()==5)
            getLog().fine("Expected no. of profiles returned by query 'compareIntNotEquals'.");
        else
            throw new TCKTestFailureException(1110433, "No. of matching profiles in query 'compareIntNotEquals' was "+col.size()+" but should have been 5");


        col = profileProvisioning.getProfilesByStaticQuery(Test1110415Sbb.PROFILE_TABLE_NAME, "compareIntLessThan", new Object[]{});
        if (col.size()==4)
            getLog().fine("Expected no. of profiles returned by query 'compareIntLessThan'.");
        else
            throw new TCKTestFailureException(1110433, "No. of matching profiles in query 'compareIntLessThan' was "+col.size()+" but should have been 4");


        col = profileProvisioning.getProfilesByStaticQuery(Test1110415Sbb.PROFILE_TABLE_NAME, "compareIntLessThanOrEquals", new Object[]{});
        if (col.size()==6)
            getLog().fine("Expected no. of profiles returned by query 'compareIntLessThanOrEquals'.");
        else
            throw new TCKTestFailureException(1110433, "No. of matching profiles in query 'compareIntLessThanOrEquals' was "+col.size()+" but should have been 6");


        col = profileProvisioning.getProfilesByStaticQuery(Test1110415Sbb.PROFILE_TABLE_NAME, "compareIntGreaterThan", new Object[]{});
        if (col.size()==1)
            getLog().fine("Expected no. of profiles returned by query 'compareIntGreaterThan'.");
        else
            throw new TCKTestFailureException(1110433, "No. of matching profiles in query 'compareIntGreaterThan' was "+col.size()+" but should have been 1");


        col = profileProvisioning.getProfilesByStaticQuery(Test1110415Sbb.PROFILE_TABLE_NAME, "compareIntGreaterThanOrEquals", new Object[]{});
        if (col.size()==3)
            getLog().fine("Expected no. of profiles returned by query 'compareIntGreaterThanOrEquals'.");
        else
            throw new TCKTestFailureException(1110433, "No. of matching profiles in query 'compareIntGreaterThanOrEquals' was "+col.size()+" but should have been 3");




        //1110437: Either "value" or "parameter" element can be used in the "compare" element
        col = profileProvisioning.getProfilesByStaticQuery(Test1110415Sbb.PROFILE_TABLE_NAME, "compareStringValue", new Object[]{});
        if (col.size()==2)
            getLog().fine("Expected no. of profiles returned by query 'compareStringValue'.");
        else
            throw new TCKTestFailureException(1110437, "No. of matching profiles in query 'compareStringValue' was "+col.size()+" but should have been 2");


        col = profileProvisioning.getProfilesByStaticQuery(Test1110415Sbb.PROFILE_TABLE_NAME, "compareStringParam", new Object[]{"42"});
        if (col.size()==2)
            getLog().fine("Expected no. of profiles returned by query 'compareStringParam'.");
        else
            throw new TCKTestFailureException(1110437, "No. of matching profiles in query 'compareStringParam' was "+col.size()+" but should have been 2");



        //1110432: "compare" can be used for attributes of type String and primitive types or their wrappers
        col = profileProvisioning.getProfilesByStaticQuery(Test1110415Sbb.PROFILE_TABLE_NAME, "compareIntObj", new Object[]{});
        if (col.size()==2)
            getLog().fine("Expected no. of profiles returned by query 'compareIntObj'.");
        else
            throw new TCKTestFailureException(1110432, "No. of matching profiles in query 'compareIntObj' was "+col.size()+" but should have been 2");


        col = profileProvisioning.getProfilesByStaticQuery(Test1110415Sbb.PROFILE_TABLE_NAME, "compareCharObj", new Object[]{});
        if (col.size()==2)
            getLog().fine("Expected no. of profiles returned by query 'compareCharObj'.");
        else
            throw new TCKTestFailureException(1110432, "No. of matching profiles in query 'compareCharObj' was "+col.size()+" but should have been 2");


        col = profileProvisioning.getProfilesByStaticQuery(Test1110415Sbb.PROFILE_TABLE_NAME, "compareBoolObj", new Object[]{});
        if (col.size()==6)
            getLog().fine("Expected no. of profiles returned by query 'compareBoolObj'.");
        else
            throw new TCKTestFailureException(1110432, "No. of matching profiles in query 'compareBoolObj' was "+col.size()+" but should have been 6");


        col = profileProvisioning.getProfilesByStaticQuery(Test1110415Sbb.PROFILE_TABLE_NAME, "compareDoubleObj", new Object[]{});
        if (col.size()==2)
            getLog().fine("Expected no. of profiles returned by query 'compareDoubleObj'.");
        else
            throw new TCKTestFailureException(1110432, "No. of matching profiles in query 'compareDoubleObj' was "+col.size()+" but should have been 2");


        col = profileProvisioning.getProfilesByStaticQuery(Test1110415Sbb.PROFILE_TABLE_NAME, "compareByteObj", new Object[]{});
        if (col.size()==2)
            getLog().fine("Expected no. of profiles returned by query 'compareByteObj'.");
        else
            throw new TCKTestFailureException(1110432, "No. of matching profiles in query 'compareByteObj' was "+col.size()+" but should have been 2");


        col = profileProvisioning.getProfilesByStaticQuery(Test1110415Sbb.PROFILE_TABLE_NAME, "compareShortObj", new Object[]{});
        if (col.size()==2)
            getLog().fine("Expected no. of profiles returned by query 'compareShortObj'.");
        else
            throw new TCKTestFailureException(1110432, "No. of matching profiles in query 'compareShortObj' was "+col.size()+" but should have been 2");


        col = profileProvisioning.getProfilesByStaticQuery(Test1110415Sbb.PROFILE_TABLE_NAME, "compareLongObj", new Object[]{});
        if (col.size()==2)
            getLog().fine("Expected no. of profiles returned by query 'compareLongObj'.");
        else
            throw new TCKTestFailureException(1110432, "No. of matching profiles in query 'compareLongObj' was "+col.size()+" but should have been 2");


        col = profileProvisioning.getProfilesByStaticQuery(Test1110415Sbb.PROFILE_TABLE_NAME, "compareFloatObj", new Object[]{});
        if (col.size()==2)
            getLog().fine("Expected no. of profiles returned by query 'compareFloatObj'.");
        else
            throw new TCKTestFailureException(1110432, "No. of matching profiles in query 'compareFloatObj' was "+col.size()+" but should have been 2");



        col = profileProvisioning.getProfilesByStaticQuery(Test1110415Sbb.PROFILE_TABLE_NAME, "compareInt", new Object[]{});
        if (col.size()==2)
            getLog().fine("Expected no. of profiles returned by query 'compareInt'.");
        else
            throw new TCKTestFailureException(1110432, "No. of matching profiles in query 'compareInt' was "+col.size()+" but should have been 2");


        col = profileProvisioning.getProfilesByStaticQuery(Test1110415Sbb.PROFILE_TABLE_NAME, "compareChar", new Object[]{});
        if (col.size()==2)
            getLog().fine("Expected no. of profiles returned by query 'compareChar'.");
        else
            throw new TCKTestFailureException(1110432, "No. of matching profiles in query 'compareChar' was "+col.size()+" but should have been 2");


        col = profileProvisioning.getProfilesByStaticQuery(Test1110415Sbb.PROFILE_TABLE_NAME, "compareBool", new Object[]{});
        if (col.size()==6)
            getLog().fine("Expected no. of profiles returned by query 'compareBool'.");
        else
            throw new TCKTestFailureException(1110432, "No. of matching profiles in query 'compareBool' was "+col.size()+" but should have been 6");


        col = profileProvisioning.getProfilesByStaticQuery(Test1110415Sbb.PROFILE_TABLE_NAME, "compareDouble", new Object[]{});
        if (col.size()==2)
            getLog().fine("Expected no. of profiles returned by query 'compareDouble'.");
        else
            throw new TCKTestFailureException(1110432, "No. of matching profiles in query 'compareDouble' was "+col.size()+" but should have been 2");


        col = profileProvisioning.getProfilesByStaticQuery(Test1110415Sbb.PROFILE_TABLE_NAME, "compareByte", new Object[]{});
        if (col.size()==2)
            getLog().fine("Expected no. of profiles returned by query 'compareByte'.");
        else
            throw new TCKTestFailureException(1110432, "No. of matching profiles in query 'compareByte' was "+col.size()+" but should have been 2");


        col = profileProvisioning.getProfilesByStaticQuery(Test1110415Sbb.PROFILE_TABLE_NAME, "compareShort", new Object[]{});
        if (col.size()==2)
            getLog().fine("Expected no. of profiles returned by query 'compareShort'.");
        else
            throw new TCKTestFailureException(1110432, "No. of matching profiles in query 'compareShort' was "+col.size()+" but should have been 2");


        col = profileProvisioning.getProfilesByStaticQuery(Test1110415Sbb.PROFILE_TABLE_NAME, "compareLong", new Object[]{});
        if (col.size()==2)
            getLog().fine("Expected no. of profiles returned by query 'compareLong'.");
        else
            throw new TCKTestFailureException(1110432, "No. of matching profiles in query 'compareLong' was "+col.size()+" but should have been 2");


        col = profileProvisioning.getProfilesByStaticQuery(Test1110415Sbb.PROFILE_TABLE_NAME, "compareFloat", new Object[]{});
        if (col.size()==2)
            getLog().fine("Expected no. of profiles returned by query 'compareFloat'.");
        else
            throw new TCKTestFailureException(1110432, "No. of matching profiles in query 'compareFloat' was "+col.size()+" but should have been 2");


        col=profileProvisioning.getProfilesByStaticQuery(Test1110415Sbb.PROFILE_TABLE_NAME, "compareAddress", new Object[]{new Address(AddressPlan.SLEE_PROFILE, "XY/X")});
        if (col.size()==2)
            getLog().fine("Expected no. of profiles returned by query 'queryCompareAddress'.");
        else
            throw new TCKTestFailureException(1110432, "No. of matching profiles in query 'queryCompareAddress' was "+col.size()+" but should have been 2");



        //1110443: Either "value" or "parameter" can be used for "longest-prefix-match" element
        col = profileProvisioning.getProfilesByStaticQuery(Test1110415Sbb.PROFILE_TABLE_NAME, "longestPrefixMatchValue", new Object[]{});
        if (col.size()==2)
            getLog().fine("Expected no. of profiles returned by query 'longestPrefixMatchValue'.");
        else
            throw new TCKTestFailureException(1110443, "No. of matching profiles in query 'longestPrefixMatchValue' was "+col.size()+" but should have been 2");


        col = profileProvisioning.getProfilesByStaticQuery(Test1110415Sbb.PROFILE_TABLE_NAME, "longestPrefixMatchParam", new Object[]{"42"});
        if (col.size()==2)
            getLog().fine("Expected no. of profiles returned by query 'longestPrefixMatchParam'.");
        else
            throw new TCKTestFailureException(1110443, "No. of matching profiles in query 'longestPrefixMatchParam' was "+col.size()+" but should have been 2");




        //1110449: Either "value" or "parameter" can be used for "has-prefix" element
        col = profileProvisioning.getProfilesByStaticQuery(Test1110415Sbb.PROFILE_TABLE_NAME, "hasPrefixValue", new Object[]{});
        if (col.size()==2)
            getLog().fine("Expected no. of profiles returned by query 'hasPrefixValue'.");
        else
            throw new TCKTestFailureException(1110449, "No. of matching profiles in query 'hasPrefixValue' was "+col.size()+" but should have been 2");


        col = profileProvisioning.getProfilesByStaticQuery(Test1110415Sbb.PROFILE_TABLE_NAME, "hasPrefixParam", new Object[]{"42"});
        if (col.size()==2)
            getLog().fine("Expected no. of profiles returned by query 'hasPrefixParam'.");
        else
            throw new TCKTestFailureException(1110449, "No. of matching profiles in query 'hasPrefixParam' was "+col.size()+" but should have been 2");





        //1110457: Either "from-value" or "from-parameter" and "to-value" or "to-parameter" can be used in "range-match"
        col = profileProvisioning.getProfilesByStaticQuery(Test1110415Sbb.PROFILE_TABLE_NAME, "rangeMatchStringValue", new Object[]{});
        if (col.size()==6)
            getLog().fine("Expected no. of profiles returned by query 'rangeMatchStringValue'.");
        else
            throw new TCKTestFailureException(1110457, "No. of matching profiles in query 'rangeMatchStringValue' was "+col.size()+" but should have been 6");


        col = profileProvisioning.getProfilesByStaticQuery(Test1110415Sbb.PROFILE_TABLE_NAME, "rangeMatchStringParam", new Object[]{"23", "42"});
        if (col.size()==6)
            getLog().fine("Expected no. of profiles returned by query 'rangeMatchStringParam'.");
        else
            throw new TCKTestFailureException(1110457, "No. of matching profiles in query 'rangeMatchStringParam' was "+col.size()+" but should have been 6");


        col = profileProvisioning.getProfilesByStaticQuery(Test1110415Sbb.PROFILE_TABLE_NAME, "rangeMatchStringParamValueMix", new Object[]{"23"});
        if (col.size()==6)
            getLog().fine("Expected no. of profiles returned by query 'rangeMatchStringParamValueMix'.");
        else
            throw new TCKTestFailureException(1110457, "No. of matching profiles in query 'rangeMatchStringParamValueMix' was "+col.size()+" but should have been 6");



        //1110451: profile attributes for range match can be of type String or primitive types or their Wrapper classes
        col = profileProvisioning.getProfilesByStaticQuery(Test1110415Sbb.PROFILE_TABLE_NAME, "rangeMatchIntObj", new Object[]{});
        if (col.size()==6)
            getLog().fine("Expected no. of profiles returned by query 'rangeMatchIntObj'.");
        else
            throw new TCKTestFailureException(1110451, "No. of matching profiles in query 'rangeMatchIntObj' was "+col.size()+" but should have been 6");


        col = profileProvisioning.getProfilesByStaticQuery(Test1110415Sbb.PROFILE_TABLE_NAME, "rangeMatchCharObj", new Object[]{});
        if (col.size()==6)
            getLog().fine("Expected no. of profiles returned by query 'rangeMatchCharObj'.");
        else
            throw new TCKTestFailureException(1110451, "No. of matching profiles in query 'rangeMatchCharObj' was "+col.size()+" but should have been 6");


        col = profileProvisioning.getProfilesByStaticQuery(Test1110415Sbb.PROFILE_TABLE_NAME, "rangeMatchBoolObj", new Object[]{});
        if (col.size()==7)
            getLog().fine("Expected no. of profiles returned by query 'rangeMatchBoolObj'.");
        else
            throw new TCKTestFailureException(1110451, "No. of matching profiles in query 'rangeMatchBoolObj' was "+col.size()+" but should have been 7");


        col = profileProvisioning.getProfilesByStaticQuery(Test1110415Sbb.PROFILE_TABLE_NAME, "rangeMatchDoubleObj", new Object[]{});
        if (col.size()==6)
            getLog().fine("Expected no. of profiles returned by query 'rangeMatchDoubleObj'.");
        else
            throw new TCKTestFailureException(1110451, "No. of matching profiles in query 'rangeMatchDoubleObj' was "+col.size()+" but should have been 6");


        col = profileProvisioning.getProfilesByStaticQuery(Test1110415Sbb.PROFILE_TABLE_NAME, "rangeMatchByteObj", new Object[]{});
        if (col.size()==6)
            getLog().fine("Expected no. of profiles returned by query 'rangeMatchByteObj'.");
        else
            throw new TCKTestFailureException(1110451, "No. of matching profiles in query 'rangeMatchByteObj' was "+col.size()+" but should have been 6");


        col = profileProvisioning.getProfilesByStaticQuery(Test1110415Sbb.PROFILE_TABLE_NAME, "rangeMatchShortObj", new Object[]{});
        if (col.size()==6)
            getLog().fine("Expected no. of profiles returned by query 'rangeMatchShortObj'.");
        else
            throw new TCKTestFailureException(1110451, "No. of matching profiles in query 'rangeMatchShortObj' was "+col.size()+" but should have been 6");


        col = profileProvisioning.getProfilesByStaticQuery(Test1110415Sbb.PROFILE_TABLE_NAME, "rangeMatchLongObj", new Object[]{});
        if (col.size()==6)
            getLog().fine("Expected no. of profiles returned by query 'rangeMatchLongObj'.");
        else
            throw new TCKTestFailureException(1110451, "No. of matching profiles in query 'rangeMatchLongObj' was "+col.size()+" but should have been 6");


        col = profileProvisioning.getProfilesByStaticQuery(Test1110415Sbb.PROFILE_TABLE_NAME, "rangeMatchFloatObj", new Object[]{});
        if (col.size()==6)
            getLog().fine("Expected no. of profiles returned by query 'rangeMatchFloatObj'.");
        else
            throw new TCKTestFailureException(1110451, "No. of matching profiles in query 'rangeMatchFloatObj' was "+col.size()+" but should have been 6");



        col = profileProvisioning.getProfilesByStaticQuery(Test1110415Sbb.PROFILE_TABLE_NAME, "rangeMatchInt", new Object[]{});
        if (col.size()==6)
            getLog().fine("Expected no. of profiles returned by query 'rangeMatchInt'.");
        else
            throw new TCKTestFailureException(1110451, "No. of matching profiles in query 'rangeMatchInt' was "+col.size()+" but should have been 6");


        col = profileProvisioning.getProfilesByStaticQuery(Test1110415Sbb.PROFILE_TABLE_NAME, "rangeMatchChar", new Object[]{});
        if (col.size()==6)
            getLog().fine("Expected no. of profiles returned by query 'rangeMatchChar'.");
        else
            throw new TCKTestFailureException(1110451, "No. of matching profiles in query 'rangeMatchChar' was "+col.size()+" but should have been 6");


        col = profileProvisioning.getProfilesByStaticQuery(Test1110415Sbb.PROFILE_TABLE_NAME, "rangeMatchBool", new Object[]{});
        if (col.size()==7)
            getLog().fine("Expected no. of profiles returned by query 'rangeMatchBool'.");
        else
            throw new TCKTestFailureException(1110451, "No. of matching profiles in query 'rangeMatchBool' was "+col.size()+" but should have been 7");


        col = profileProvisioning.getProfilesByStaticQuery(Test1110415Sbb.PROFILE_TABLE_NAME, "rangeMatchDouble", new Object[]{});
        if (col.size()==6)
            getLog().fine("Expected no. of profiles returned by query 'rangeMatchDouble'.");
        else
            throw new TCKTestFailureException(1110451, "No. of matching profiles in query 'rangeMatchDouble' was "+col.size()+" but should have been 6");


        col = profileProvisioning.getProfilesByStaticQuery(Test1110415Sbb.PROFILE_TABLE_NAME, "rangeMatchByte", new Object[]{});
        if (col.size()==6)
            getLog().fine("Expected no. of profiles returned by query 'rangeMatchByte'.");
        else
            throw new TCKTestFailureException(1110451, "No. of matching profiles in query 'rangeMatchByte' was "+col.size()+" but should have been 6");


        col = profileProvisioning.getProfilesByStaticQuery(Test1110415Sbb.PROFILE_TABLE_NAME, "rangeMatchShort", new Object[]{});
        if (col.size()==6)
            getLog().fine("Expected no. of profiles returned by query 'rangeMatchShort'.");
        else
            throw new TCKTestFailureException(1110451, "No. of matching profiles in query 'rangeMatchShort' was "+col.size()+" but should have been 6");


        col = profileProvisioning.getProfilesByStaticQuery(Test1110415Sbb.PROFILE_TABLE_NAME, "rangeMatchLong", new Object[]{});
        if (col.size()==6)
            getLog().fine("Expected no. of profiles returned by query 'rangeMatchLong'.");
        else
            throw new TCKTestFailureException(1110451, "No. of matching profiles in query 'rangeMatchLong' was "+col.size()+" but should have been 6");


        col = profileProvisioning.getProfilesByStaticQuery(Test1110415Sbb.PROFILE_TABLE_NAME, "rangeMatchFloat", new Object[]{});
        if (col.size()==6)
            getLog().fine("Expected no. of profiles returned by query 'rangeMatchFloat'.");
        else
            throw new TCKTestFailureException(1110451, "No. of matching profiles in query 'rangeMatchFloat' was "+col.size()+" but should have been 6");




        //1110459: and element working with >=2 subelements
        col = profileProvisioning.getProfilesByStaticQuery(Test1110415Sbb.PROFILE_TABLE_NAME, "and2Compares", new Object[]{});
        if (col.size()==2)
            getLog().fine("Expected no. of profiles returned by query 'and2Compares'.");
        else
            throw new TCKTestFailureException(1110459, "No. of matching profiles in query 'and2Compares' was "+col.size()+" but should have been 2");


        col = profileProvisioning.getProfilesByStaticQuery(Test1110415Sbb.PROFILE_TABLE_NAME, "andMoreElements", new Object[]{});
        if (col.size()==0)
            getLog().fine("Expected no. of profiles returned by query 'andMoreElements'.");
        else
            throw new TCKTestFailureException(1110459, "No. of matching profiles in query 'andMoreElements' was "+col.size()+" but should have been 0");




        //1110461: or element working with >=2 subelements
        col = profileProvisioning.getProfilesByStaticQuery(Test1110415Sbb.PROFILE_TABLE_NAME, "or2Compares", new Object[]{});
        if (col.size()==2)
            getLog().fine("Expected no. of profiles returned by query 'or2Compares'.");
        else
            throw new TCKTestFailureException(1110461, "No. of matching profiles in query 'or2Compares' was "+col.size()+" but should have been 2");


        col = profileProvisioning.getProfilesByStaticQuery(Test1110415Sbb.PROFILE_TABLE_NAME, "orMoreElements", new Object[]{});
        if (col.size()==7)
            getLog().fine("Expected no. of profiles returned by query 'orMoreElements'.");
        else
            throw new TCKTestFailureException(1110461, "No. of matching profiles in query 'orMoreElements' was "+col.size()+" but should have been 7");




        //1110463: not element with 1 subelement
        col = profileProvisioning.getProfilesByStaticQuery(Test1110415Sbb.PROFILE_TABLE_NAME, "not1Compare", new Object[]{});
        if (col.size()==5)
            getLog().fine("Expected no. of profiles returned by query 'not1Compare'.");
        else
            throw new TCKTestFailureException(1110463, "No. of matching profiles in query 'not1Compare' was "+col.size()+" but should have been 5");

    }


    public void sendResourceEvent(int operationID) throws TCKTestErrorException, RemoteException, TCKTestFailureException {
      // send an initial event to the test
      TCKResourceTestInterface resource = utils().getResourceInterface();
      TCKActivityID activityID = resource.createActivity(getClass().getName());
      Object message = new Integer(operationID);
      resource.fireEvent(TCKResourceEventX.X1, message, activityID, null);

      try {
          TCKSbbMessage reply = resourceListener.nextMessage();
          HashMap map = (HashMap) reply.getMessage();
          int type = ((Integer)map.get("Type")).intValue();
          String msg = (String) map.get("Message");
          int id = ((Integer)map.get("ID")).intValue();
          boolean result = ((Boolean)map.get("Result")).booleanValue();

          switch (type) {
          case SbbBaseMessageConstants.TYPE_SET_RESULT:
              if (result)
                  getLog().fine(id+": "+msg);
              else {
                  getLog().fine("FAILURE: "+msg);
                  throw new TCKTestFailureException(id, msg);
              }
              break;
          }
      } catch (OperationTimedOutException ex) {
          throw new TCKTestErrorException("Timed out waiting for processing of initial resource event.", ex);
      }
    }

    public void setUp() throws Exception {

        setupService(EVENT_DU_PATH_PARAM);

        setupService(SERVICE_DU_PATH_PARAM);

        profileUtils = new ProfileUtils(utils());
        profileProvisioning = profileUtils.getProfileProvisioningProxy();

        resourceListener = new QueuingResourceListener(utils()) {
            public Object onSbbCall(Object argument) throws Exception {
                HashMap map = (HashMap) argument;
                int type = ((Integer)map.get(SbbBaseMessageConstants.TYPE)).intValue();
                switch (type) {
                case SbbBaseMessageConstants.TYPE_LOG_MSG:
                    getLog().fine((String)map.get(SbbBaseMessageConstants.MSG));
                }
                return null;
            }
        };
        setResourceListener(resourceListener);
    }

    public void tearDown() throws Exception {

        try {
            profileUtils.removeProfileTable(Test1110415Sbb.PROFILE_TABLE_NAME);
        }
        catch (Exception e) {
            getLog().warning("Caught exception while trying to remove profile table:");
            getLog().warning(e);
        }

        super.tearDown();
    }

    private ProfileUtils profileUtils;
    private ProfileProvisioningMBeanProxy profileProvisioning;
    private QueuingResourceListener resourceListener;
}
