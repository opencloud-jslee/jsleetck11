/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.sbb.abstractclass;

import com.opencloud.sleetck.lib.resource.events.TCKResourceEventX;
import com.opencloud.sleetck.lib.sbbutils.BaseTCKSbb;
import com.opencloud.sleetck.lib.sbbutils.TCKSbbUtils;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.slee.ActivityContextInterface;
import javax.slee.Address;
import javax.slee.ChildRelation;

public abstract class Test1987Sbb extends BaseTCKSbb {

    // Initial event
    public void onTCKResourceEventX1(TCKResourceEventX ev, ActivityContextInterface aci) {
        try {
            aci.attach(getChildRelation().create());
            fireTest1987Event(new Test1987Event(), aci, null);
            fireTest1987SecondEvent(new Test1987SecondEvent(), aci, null);
        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }
    }

    public void onTest1987SecondEvent(Test1987SecondEvent event, ActivityContextInterface aci) {

        boolean passed = false;
        Context context = null;

        try {
            context = (Context) new InitialContext().lookup("java:comp/env");
        } catch (NamingException e) {
            TCKSbbUtils.handleException(e);
            return;
        }

        try {
            context.lookup("slee/resources/tck/resource-alt");
        } catch (NamingException e) {
            passed = true;
        }

        if (passed == true) {
            Test1987SecondSbbLocalObject sbbLocalObject = (Test1987SecondSbbLocalObject) getChildRelation().iterator().next();
            passed = sbbLocalObject.getResult();
        }

        try {
            TCKSbbUtils.getResourceInterface().sendSbbMessage(new Boolean(passed));
        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }

    }

    public abstract void fireTest1987Event(Test1987Event event, ActivityContextInterface aci, Address address);
    public abstract void fireTest1987SecondEvent(Test1987SecondEvent event, ActivityContextInterface aci, Address address);

    public abstract ChildRelation getChildRelation();

}
