/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.events.eventcontext;

import java.util.HashMap;

import javax.slee.ActivityContextInterface;
import javax.slee.Address;
import javax.slee.EventContext;
import javax.slee.ServiceID;
import javax.slee.facilities.Tracer;

import com.opencloud.sleetck.lib.resource.events.TCKResourceEventX;
import com.opencloud.sleetck.lib.sbbutils.BaseTCKSbb;
import com.opencloud.sleetck.lib.sbbutils.TCKSbbUtils;

/**
 * AssertionID(1108015): Test This getEvent method returns the event 
 * object of the event that this Event Context is associated with. 
 * 
 * AssertionID(1108175): Test This getActivityContextInterface method 
 * returns the Activity Context Interface for the Activity which the 
 * event was fired on. The returned Activity Context is the generic 
 * Activity Context Interface.
 * 
 * AssertionID(1108176): Test This getAddress method returns the address 
 * which was provided when the event was fired.
 * 
 * AssertionID(1108178): Test This getService method returns the component 
 * identifier of the Service provided when the event was fired.
 * 
 * AssertionID(1108028): Test The java.lang.IllegalArgumentException is 
 * thrown if the timeout argument is zero or a negative value.
 * 
 */
public abstract class Test1108015Sbb extends BaseTCKSbb {

    public void onTCKResourceEventX1(TCKResourceEventX event, ActivityContextInterface aci, EventContext context) {
        try {
            tracer = getSbbContext().getTracer("com.test");
            tracer.info("Received event TCKResourceEventX");
            setTest1108015Event(new Test1108015Event());

            setActivityContextInterface(context.getActivityContextInterface());

            fireTest1108015Event(getTest1108015Event(), getActivityContextInterface(), ADDRESS_1, SERVICEID_1);

        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }
    }

    public void onTest1108015Event(Test1108015Event event, ActivityContextInterface aci, EventContext context) {
        try {
            tracer = getSbbContext().getTracer("com.test");
            tracer.info("Received event Test1108015Event");

            Test1108015Event sentEvent = (Test1108015Event) context.getEvent();
            if (!getTest1108015Event().equals(sentEvent)) {
                sendResultToTCK("Test1108015Test", false, "SBB:onTest1108015Event-ERROR: This EventContext.getEvent() method didn't return the event"
                        + "object of the event that this Event Context is associated with.", 1108015);
                return;
            }

            ActivityContextInterface sentActivityContextInterface = context.getActivityContextInterface();
            if (!getActivityContextInterface().equals(sentActivityContextInterface)) {
                sendResultToTCK("Test1108015Test", false, "SBB:onTest1108015Event-ERROR: This EventContext.getActivityContextInterface() method didn't "
                        + "return  the Activity Context Interface for the Activity which the event was fired on", 1108175);
                return;

            }

            Address sentAddress = context.getAddress();
            if (!ADDRESS_1.equals(sentAddress)) {
                sendResultToTCK("Test1108015Test", false, "SBB:onTest1108015Event-ERROR: This EventContext.getAddress() method didn't return the address"
                        + "which was provided when the event was fired", 1108176);
                return;
            }

            ServiceID sentServiceID = context.getService();
            if (!SERVICEID_1.equals(sentServiceID)) {
                sendResultToTCK("Test1108015Test", false, "SBB:onTest1108015Event-ERROR: This EventContext.getService() method didn't not match the "
                        + "ServiceID of the Service provided when the event was fired", 1108178);
                return;
            }

            boolean passed = false;
            try {
                context.suspendDelivery(0);
            } catch (java.lang.IllegalArgumentException e) {
                tracer.info("got expected IllegalArgumentException when the timeout argument is zero");
                passed = true;
            }

            if (!passed) {
                try {
                    if (context.isSuspended())
                        context.resumeDelivery();
                } catch (Exception e) {
                    tracer.severe("Failed to resume the suspended event delivery.");
                }
                
                sendResultToTCK("Test1108015Test", false, "SBB:onTest1108015Event-ERROR: EventContext.suspendDelivery(0) "
                        + "should have thrown java.lang.IllegalArgumentException.", 1108028);
                return;
            }

            passed = false;
            int negativeValue = -1;
            try {
                context.suspendDelivery(negativeValue);
            } catch (java.lang.IllegalArgumentException e) {
                tracer.info("got expected IllegalArgumentException when the timeout argument is negative value");
                passed = true;
            }

            if (!passed) {
                try {
                    if (context.isSuspended())
                        context.resumeDelivery();
                } catch (Exception e) {
                    tracer.severe("Failed to resume the suspended event delivery.");
                }
                
                sendResultToTCK("Test1108015Test", false, "SBB:onTest1108015Event-ERROR: EventContext.suspendDelivery(negativeValue) "
                        + "should have thrown java.lang.IllegalArgumentException.", 1108028);
                return;
            }

            sendResultToTCK("Test1108015Test", true, "The EventContext interface tests passed!", 1108015);

        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }

    }

    public abstract void fireTest1108015Event(Test1108015Event event, ActivityContextInterface aci, Address address,
            ServiceID serviceID);

    private void sendResultToTCK(String testName, boolean result, String message, int failedAssertionID) throws Exception {
        HashMap sbbData = new HashMap();
        sbbData.put("testname", testName);
        sbbData.put("result", result ? "pass" : "fail");
        sbbData.put("message", message);
        sbbData.put("id", new Integer(failedAssertionID));
        TCKSbbUtils.getResourceInterface().sendSbbMessage(sbbData);
    }

    //cmp fields
    public abstract void setEventContext(EventContext eventContext);
    public abstract EventContext getEventContext();
    
    public abstract void setActivityContextInterface(ActivityContextInterface activityContextInterface);
    public abstract ActivityContextInterface getActivityContextInterface();
    
    public abstract void setTest1108015Event(Test1108015Event test1108015Event);
    public abstract Test1108015Event getTest1108015Event();
    
    private Tracer tracer;

    public static final Address ADDRESS_1 = new Address(javax.slee.AddressPlan.IP, "1.0.0.1");
    private static final ServiceID SERVICEID_1 = new ServiceID("Test1108015Service", "jain.slee.tck", "1.1");
}
