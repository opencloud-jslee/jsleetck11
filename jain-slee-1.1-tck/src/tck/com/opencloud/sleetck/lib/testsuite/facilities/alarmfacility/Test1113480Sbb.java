/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.facilities.alarmfacility;

import java.util.HashMap;

import javax.naming.InitialContext;
import javax.slee.ActivityContextInterface;
import javax.slee.facilities.AlarmFacility;
import javax.slee.facilities.AlarmLevel;
import javax.slee.facilities.Tracer;

import com.opencloud.sleetck.lib.resource.events.TCKResourceEventX;
import com.opencloud.sleetck.lib.sbbutils.BaseTCKSbb;
import com.opencloud.sleetck.lib.sbbutils.TCKSbbUtils;

/*  
 *  AssertionID(1113480): Test The alarmType argument. This 
 *  argument specifies the type of alarm being generated. 
 *  It will be included in the AlarmNotification object emitted 
 *  by the AlarmMBean object for the alarm notification.
 *    
 *  AssertionID(1113482): Test the instanceID argument. This 
 *  argument specifies the particular instance of the alarm 
 *  type that is occuring.
 *  
 *  AssertionID(1113483): Test The alarmlevel argument. This 
 *  argument takes an AlarmLevel object that specifies the 
 *  alarm level of the alarm notification.
 *  
 *  AssertionID(1113485): Test The message argument. This 
 *  argument specifies the message that will be placed into 
 *  the message attribute of the AlarmNotification object 
 *  emitted by the AlarmMBean object for the alarm notification 
 *  if the alarm is raised as a result of this method.
 *  
 */

public abstract class Test1113480Sbb extends BaseTCKSbb {

    private static final String JNDI_ALARMFACILITY_NAME = AlarmFacility.JNDI_NAME;

    public static final String ALARM_MESSAGE = "Test1113480AlarmMessage";

    public static final String ALARM_INSTANCEID = "Test1113480AlarmInstanceID";

    public static final String ALARM_TYPE = "javax.slee.management.Alarm";

    public static final AlarmLevel ALARM_LEVEL_MAJOR = AlarmLevel.MAJOR;

    // Initial event
    public void onTCKResourceEventX1(TCKResourceEventX event, ActivityContextInterface aci) {

        try {
            sbbTracer = getSbbContext().getTracer("com.test");
            sbbTracer.info("Received " + event + " message", null);
            AlarmFacility facility = (AlarmFacility) new InitialContext().lookup(JNDI_ALARMFACILITY_NAME);
            String alarmID = facility.raiseAlarm(ALARM_TYPE, ALARM_INSTANCEID, ALARM_LEVEL_MAJOR, ALARM_MESSAGE);
            if (alarmID == null)
                sendResultToTCK("Test1113480Test", false, 1113480, "An alarm failed to raise!");

        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }
    }

    private void sendResultToTCK(String testName, boolean result, int failedAssertionID,  String message) throws Exception {
        HashMap sbbData = new HashMap();
        sbbData.put("testname", testName);
        sbbData.put("result", result ? "pass" : "fail");
        sbbData.put("message", message);
        sbbData.put("id", new Integer(failedAssertionID));
        TCKSbbUtils.getResourceInterface().sendSbbMessage(sbbData);
    }

    private Tracer sbbTracer;
}
