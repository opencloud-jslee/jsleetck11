/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.profiles.profilespec;

import javax.slee.CreateException;
import javax.slee.profile.Profile;
import javax.slee.profile.ProfileContext;
import javax.slee.profile.ProfileVerificationException;
//Test1110046ProfileCMP

public abstract class Test1110046_1Profile implements Profile, ProfileSpecTestsProfileManagement  {
    public void setProfileContext(ProfileContext context) {
        this.context = context;
    }

    public void unsetProfileContext() {
    }

    public void profileInitialize() {
    }

    public void profilePostCreate() throws CreateException {
    }

    public void profileActivate() {
    }

    public void profilePassivate() {
    }

    public void profileLoad() {
    }

    public void profileStore() {
    }

    public void profileRemove() {
    }

    public void profileVerify() throws ProfileVerificationException {
    }

    public void manage() {

    }

    private ProfileContext context;

}
