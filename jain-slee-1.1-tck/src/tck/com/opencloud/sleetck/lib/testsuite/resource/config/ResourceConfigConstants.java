/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.resource.config;

public final class ResourceConfigConstants {
    /**
     * Property key used by both the Test and the Resource Adaptor.
     */
    public static String INVALID_PROPERTY_KEY = "TCK: I am an invalid property, please throw an exception";

}
