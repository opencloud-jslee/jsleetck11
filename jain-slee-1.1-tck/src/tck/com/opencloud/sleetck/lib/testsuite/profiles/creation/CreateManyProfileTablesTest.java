/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.profiles.creation;

import javax.slee.profile.ProfileSpecificationID;
import javax.slee.profile.ProfileAlreadyExistsException;
import javax.management.ObjectName;
import com.opencloud.sleetck.lib.AbstractSleeTCKTest;
import com.opencloud.sleetck.lib.DescriptionKeys;
import com.opencloud.sleetck.lib.TCKTestResult;
import com.opencloud.sleetck.lib.testsuite.profiles.ProfileTestConstants;
import com.opencloud.sleetck.lib.testutils.ProfileUtils;
import com.opencloud.sleetck.lib.testutils.ComponentIDLookup;
import com.opencloud.sleetck.lib.testutils.jmx.ProfileProvisioningMBeanProxy;
import com.opencloud.sleetck.lib.infra.SleeTCKComponentConstants;
import com.opencloud.sleetck.lib.AbstractSleeTCKTest;
import java.util.Vector;
import java.util.Iterator;

/**
 * Tests assertion 912, that many profile tables can exist for a single profile specification.
 */
public class CreateManyProfileTablesTest extends AbstractSleeTCKTest {

    private static final String PROFILE_TABLE_NAME_A = "tck.CreateManyProfileTablesTest.tableA";
    private static final String PROFILE_TABLE_NAME_B = "tck.CreateManyProfileTablesTest.tableB";
    private static final String PROFILE_TABLE_NAME_C = "tck.CreateManyProfileTablesTest.tableC";

    public TCKTestResult run() throws Exception {
        ProfileProvisioningMBeanProxy profileProvisioning = profileUtils.getProfileProvisioningProxy();
        ProfileSpecificationID profileSpecID = new ComponentIDLookup(utils()).lookupProfileSpecificationID(
            ProfileTestConstants.SIMPLE_PROFILE_SPEC_NAME,SleeTCKComponentConstants.TCK_VENDOR,"1.0");
        String[] tableNames = {PROFILE_TABLE_NAME_A,PROFILE_TABLE_NAME_B,PROFILE_TABLE_NAME_C};
        tablesCreated = new Vector();
        for (int i = 0; i < tableNames.length; i++) {
            getLog().info("Creating profile table: "+tableNames[i]);
            profileProvisioning.createProfileTable(profileSpecID,tableNames[i]);
            tablesCreated.addElement(tableNames[i]);
        }

        // check that all the profile tables still exist
        java.util.Collection profileTables = profileProvisioning.getProfileTables();
        for (int i = 0; i < tableNames.length; i++) {
            if (!profileTables.contains(tableNames[i])) {
                return TCKTestResult.failed(912,"Profile table "+tableNames[i]+
                    " did not remain in the SLEE after adding a total of "+tableNames.length+" profile tables");
            }
        }
        return TCKTestResult.passed();
    }

    public void setUp() throws Exception {
        String duPath = utils().getTestParams().getProperty(ProfileTestConstants.PROFILE_SPEC_DU_PATH_PARAM);
        getLog().fine("Installing profile specification");
        utils().install(duPath);
        profileUtils = new ProfileUtils(utils());
    }

    public void tearDown() throws Exception {
        if(profileUtils != null && tablesCreated != null && !tablesCreated.isEmpty()) {
            getLog().fine("Removing profile tables");
            Iterator tablesCreatedIter = tablesCreated.iterator();
            while(tablesCreatedIter.hasNext()) {
                String tableName = (String)tablesCreatedIter.next();
                try {
                    profileUtils.removeProfileTable(tableName);
                } catch (Exception ex) {
                    getLog().warning("Caught Exception while trying to remove profile table "+tableName+":");
                    getLog().warning(ex);
                }
            }
        }
        super.tearDown();
    }

    private ProfileUtils profileUtils;
    private Vector tablesCreated;

}
