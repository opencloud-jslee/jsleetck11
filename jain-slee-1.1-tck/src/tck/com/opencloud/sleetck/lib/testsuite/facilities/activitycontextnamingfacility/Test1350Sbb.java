/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.facilities.activitycontextnamingfacility;

import com.opencloud.sleetck.lib.TCKTestErrorException;
import com.opencloud.sleetck.lib.resource.events.TCKResourceEventX;
import com.opencloud.sleetck.lib.sbbutils.BaseTCKSbb;
import com.opencloud.sleetck.lib.sbbutils.TCKSbbUtils;

import javax.slee.ActivityContextInterface;
import javax.slee.facilities.ActivityContextNamingFacility;
import javax.slee.facilities.Level;
import javax.slee.nullactivity.NullActivity;
import javax.slee.nullactivity.NullActivityContextInterfaceFactory;
import javax.slee.nullactivity.NullActivityFactory;

/**
 *
 */
public abstract class Test1350Sbb extends BaseTCKSbb {
    private static final String ACTIVITY_CONTEXT_NAME = "Test1350ActivityContext";

    public void onTCKResourceEventX1(TCKResourceEventX event, ActivityContextInterface aci) {
        try {
            createTrace("onTCKResourceEventX1 - event received");
            NullActivityFactory nullActivityFactory = (NullActivityFactory)
                    TCKSbbUtils.getSbbEnvironment().lookup("slee/nullactivity/factory");
            NullActivity activity = nullActivityFactory.createNullActivity();
            NullActivityContextInterfaceFactory nullACIFactory = (NullActivityContextInterfaceFactory)
                    TCKSbbUtils.getSbbEnvironment().lookup("slee/nullactivity/activitycontextinterfacefactory");
            ActivityContextInterface nullACI = nullACIFactory.getActivityContextInterface(activity);

            ActivityContextNamingFacility acNaming = (ActivityContextNamingFacility)
                    TCKSbbUtils.getSbbEnvironment().lookup("slee/facilities/activitycontextnaming");

            createTrace("onTCKResourceEventX1 - binding activity context");
            acNaming.bind(nullACI, ACTIVITY_CONTEXT_NAME);
        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }
    }

    public void onTCKResourceEventX2(TCKResourceEventX event, ActivityContextInterface aci) {
        try {
            createTrace("onTCKResourceEventX2 - event received");

            ActivityContextNamingFacility acNaming = (ActivityContextNamingFacility)
                    TCKSbbUtils.getSbbEnvironment().lookup("slee/facilities/activitycontextnaming");

            ActivityContextInterface tempaci = acNaming.lookup(ACTIVITY_CONTEXT_NAME);
            TCKSbbUtils.getResourceInterface().sendSbbMessage(Boolean.valueOf(null != tempaci));

            if (tempaci == null)
                return;

            boolean isEnding = tempaci.isEnding();

            createTrace("onTCKResourceEventX2 - unbinding activity context");
            acNaming.unbind(ACTIVITY_CONTEXT_NAME);

            createTrace("onTCKResourceEventX2 - ending activity");
            NullActivity activity = (NullActivity)tempaci.getActivity();
            activity.endActivity();

            TCKSbbUtils.getResourceInterface().sendSbbMessage(Boolean.valueOf(isEnding));

        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }
    }

    private void createTrace(String message) {
        try {
            TCKSbbUtils.createTrace(getSbbID(), Level.INFO, message, null);
        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }
    }
}
