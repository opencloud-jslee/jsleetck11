/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.management.TraceMBean;

import javax.management.Notification;
import javax.management.NotificationListener;
import javax.management.ObjectName;
import javax.slee.ComponentID;
import javax.slee.SbbID;
import javax.slee.ServiceID;
import javax.slee.facilities.TraceLevel;
import javax.slee.management.DeployableUnitDescriptor;
import javax.slee.management.DeployableUnitID;
import javax.slee.management.NotificationSource;
import javax.slee.management.SbbNotification;
import javax.slee.management.TraceNotification;
import javax.slee.management.UnrecognizedNotificationSourceException;

import com.opencloud.sleetck.lib.AbstractSleeTCKTest;
import com.opencloud.sleetck.lib.SleeTCKTestUtils;
import com.opencloud.sleetck.lib.TCKTestErrorException;
import com.opencloud.sleetck.lib.resource.TCKActivityID;
import com.opencloud.sleetck.lib.resource.events.TCKResourceEventX;
import com.opencloud.sleetck.lib.resource.testapi.TCKResourceListener;
import com.opencloud.sleetck.lib.resource.testapi.TCKResourceTestInterface;
import com.opencloud.sleetck.lib.testutils.FutureResult;
import com.opencloud.sleetck.lib.testutils.jmx.DeploymentMBeanProxy;
import com.opencloud.sleetck.lib.testutils.jmx.SleeManagementMBeanProxy;
import com.opencloud.sleetck.lib.testutils.jmx.TraceMBeanProxy;
import com.opencloud.sleetck.lib.testutils.jmx.impl.TraceMBeanProxyImpl;

/*
 * TraceMBean methods: Create a Tracer and check the TraceMBean operates on it,
 * and check the constructor for Trace Notification
 *
 */

public class Test1114191Test extends  AbstractSleeTCKTest {

    private static final String SERVICE_DU_PATH_PARAM = "serviceDUPath";
    private static final int TEST_ID = 1114191;

    /**
     * Perform the actual test.
     */

    public void run(FutureResult result) throws Exception {

        this.result = result;
        expectedTraceNotifications = 1;

        if(sbbID == null) throw new TCKTestErrorException("sbbID not found for "+SERVICE_DU_PATH_PARAM);
        if(serviceID == null) throw new TCKTestErrorException("serviceID not found for "+SERVICE_DU_PATH_PARAM);

        traceMBeanProxy = utils().getMBeanProxyFactory().createTraceMBeanProxy(utils().getSleeManagementMBeanProxy().getTraceMBean());
        sbbNotification = new SbbNotification(serviceID, sbbID);

        String activityName = "Test1114191Test";
        TCKResourceTestInterface resource = utils().getResourceInterface();
        TCKActivityID activityID = resource.createActivity(activityName);
        utils().getLog().info("Firing event: " + testName );

        // kickoff the test
        resource.fireEvent(TCKResourceEventX.X1, testName, activityID, null);

        synchronized (this) {
            wait(utils().getTestTimeout());
        }

        if ((passed) && (expectedTraceNotifications == receivedTraceNotifications)) {
            result.setPassed();
        } else {
            if (!result.isSet())
                result.setFailed(1114191, "Expected number of trace messages not received, traces were not delivered by " +
                    "the TraceFacility (expected " + expectedTraceNotifications + ", received " + receivedTraceNotifications + ")");
        }

    }

    private boolean doNotficationSourcesCheck(NotificationSource sbbNotification) {
        boolean passed = true;

        //1114194
        try {
            utils().getLog().fine("Starting a test to set the level of the tracer.");
            traceMBeanProxy.setTraceLevel(sbbNotification, "com.foo", TraceLevel.FINE);
            if (traceMBeanProxy.getTraceLevel(sbbNotification, "com.foo") == TraceLevel.FINE) {
                logSuccessfulCheck(1114194);
            } else {
                result.setFailed(1114194, "setTraceLevel did not set Tracer level");
                passed = false;
            }
        }
        catch (Exception e) {
            utils().getLog().warning(e);
            result.setFailed(1114194, "Exception occured while setting Tracer level.");
            return false;
        }

        //1114802
        try {
            utils().getLog().fine("Starting to test an invalid tracer name.");
            traceMBeanProxy.setTraceLevel(sbbNotification, "com..foo", TraceLevel.FINE);
            result.setFailed(1114802, "Invalid Tracer name failed to throw InvalidArgumentException");
            passed = false;
        }
        catch (javax.slee.InvalidArgumentException iae) {
            logSuccessfulCheck(1114802);
        }
        catch (Exception e) {
            utils().getLog().warning(e);
            result.setFailed(1114802, "Tracer name components must be at least " +
                    "one character in length, that means that “com..foo” is not a legal name " +
                    "because the middle name component of this name is zero-length.");
            return false;
        }

        // 1114190
        ServiceID badServiceID = new ServiceID("bad", "jain.slee.tck", "1.0");
        SbbID badSbbID = new SbbID("bad", "jain.slee.tck", "1.0");
        SbbNotification badSbbNotification = new SbbNotification(badServiceID, badSbbID);
        try {
            utils().getLog().fine("Starting to test an invalid SBB Nofification.");
            traceMBeanProxy.setTraceLevel(badSbbNotification, "com.foo", TraceLevel.FINE);
            result.setFailed(1114190, "setTraceLevel(badSbbNotification) failed to throw UnrecognizedNotificationSourceException");
            passed = false;
        } catch (UnrecognizedNotificationSourceException e) {
            logSuccessfulCheck(1114190);
        } catch (Exception e) {
            utils().getLog().warning(e);
            result.setFailed(1114190, "setTraceLevel(badSbbNotification) failed");
            return false;
        }

        //1114413
        try {
            // In spec 1.1-pfd the default trace level of a Tracer with the root tracername is INFO,
            utils().getLog().fine("Starting to test the default trace level for all root tracers is TraceLevel.INFO.");
            if (traceMBeanProxy.getTraceLevel(sbbNotification, "") != TraceLevel.INFO) {
                result.setFailed(1114413, "The default trace level for all root tracers " +
                                "must set to TraceLevel.INFO, but it returned "+ traceMBeanProxy.getTraceLevel(sbbNotification, "").toString());
                 passed = false;
            } else {
                logSuccessfulCheck(1114413);
            }
        }
        catch (Exception e) {
            utils().getLog().warning(e);
            result.setFailed(1114413, "getTraceLevel failed");
            return false;
        }

        //1114196
        try {
            utils().getLog().fine("Starting to test the Administrator uses the TraceMBean to " +
                        "change the trace level for a tracer. ");
            traceMBeanProxy.setTraceLevel(sbbNotification, "com.foo", TraceLevel.FINE);
            if (traceMBeanProxy.getTraceLevel(sbbNotification, "com.foo") != TraceLevel.FINE) {
                result.setFailed(1114196, "The old TraceLevel for the tracername com.foo did not " +
                                "return TraceLevel.FINE.");
                 passed = false;
            } else {
                traceMBeanProxy.setTraceLevel(sbbNotification, "com.foo", TraceLevel.CONFIG);
                if (traceMBeanProxy.getTraceLevel(sbbNotification, "com.foo") != TraceLevel.CONFIG) {
                    result.setFailed(1114196, "The new TraceLevel for the tracername com.foo did not " +
                                "return TraceLevel.CONFIG.");
                    passed = false;
                } else {
                    logSuccessfulCheck(1114196);
                }
            }
        }
        catch (Exception e) {
            utils().getLog().warning(e);
            result.setFailed(1114196, "There is an error when the Administrator uses " +
                        "the TraceMBean to change the trace level for a tracer. ");
            return false;
        }

        //1114197 getTracersSet()
        try {
            utils().getLog().fine("Starting to test getTracersSet()");
            String[] tracersSet = null;
            tracersSet = traceMBeanProxy.getTracersSet(sbbNotification);
            if (tracersSet.length == 0) {
                result.setFailed(1114197, "getTracersSet() did not return any Tracers");
                 passed = false;
            } else {
                boolean found = false;
                for (int i=0; i < tracersSet.length; i++) {
                    utils().getLog().fine("Tracer set is " + tracersSet[i]);
                    if (tracersSet[i].equals("com.foo")) {
                        logSuccessfulCheck(1114197);
                        found = true;
                    }
                }
                if (!found) {
                    result.setFailed(1114197, "getTracersSet() did not return correct Tracers");
                    passed = false;
                }
            }
        }
        catch (Exception e) {
            utils().getLog().warning(e);
            result.setFailed(1114197, "getTracersSet failed");
            return false;
        }

        //1114191 getTracersUsed()
        try {
            utils().getLog().fine("Starting to test getTracersUsed()");
            String[] tracersUsed = null;
            tracersUsed = traceMBeanProxy.getTracersUsed(sbbNotification);
            if (tracersUsed.length == 0) {
                result.setFailed(1114191, "getTracersUsed() did not return any Tracers");
                 passed = false;
            } else {
                boolean found = false;
                for (int i=0; i < tracersUsed.length; i++) {
                    utils().getLog().fine("Tracer used is " + tracersUsed[i]);
                   if (tracersUsed[i].equals("com.foo")) {
                        logSuccessfulCheck(1114191);
                        found = true;
                   }
                }
                if (!found) {
                    result.setFailed(1114191, "getTracersUsed() did not return correct Tracers");
                    passed = false;
                }
            }
        }
        catch (Exception e) {
            utils().getLog().warning(e);
            result.setFailed(1114191, "getTracersUsed failed");
            return false;
        }

        //1114189 unsetTraceLevel()
        try {
            utils().getLog().fine("Starting to test unsetTracerLevel().");
            String[] noTracersSet = null;
            boolean found = false;
            traceMBeanProxy.unsetTraceLevel(sbbNotification, "com.foo");
            noTracersSet = traceMBeanProxy.getTracersSet(sbbNotification);
            utils().getLog().fine("getTracersSet returns length : " + noTracersSet.length);
            if (noTracersSet.length > 0) {
                for (int i=0; i<noTracersSet.length; i++){
                    if (noTracersSet[i].equals("com.foo"))
                        found = true;
                }
                if (found) {
                    utils().getLog().fine("ERROR: Still have Tracer set");
                    result.setFailed(1114189, "unsetTraceLevel() returned Tracers");
                    passed = false;
                } else {
                    logSuccessfulCheck(1114189);
                }
            } else {
                logSuccessfulCheck(1114189);
            }
        }
        catch (Exception e) {
            utils().getLog().warning(e);
            result.setFailed(1114189, "unsetTraceLevel failed");
            return false;
        }

        return passed;
    }

    public class TraceNotificationListenerImpl implements NotificationListener {
        public final void handleNotification(Notification notification, Object handback) {

        if (notification instanceof TraceNotification) {
            TraceNotification traceNotification = (TraceNotification) notification;

            utils().getLog().fine("Received Trace Notification from: " + traceNotification);

            if (traceNotification.getNotificationSource() instanceof SbbNotification) {
                if (traceNotification.getType().equals(SbbNotification.TRACE_NOTIFICATION_TYPE)) {
                    NotificationSource notificationSource =  traceNotification.getNotificationSource();
                    passed = doNotficationSourcesCheck(notificationSource);
                    receivedTraceNotifications++;
                }
            }
        }
        }
    }


    /**
     * Do all the pre-run configuration of the test.
     */

    public void setUp() throws Exception {

        utils().getLog().fine("Installing and activating service");

        // Install the Deployable Units
        String duPath = utils().getTestParams().getProperty(SERVICE_DU_PATH_PARAM);
        duID = utils().install(duPath);

        // Activate the DU
        utils().activateServices(duID, true);

        // Setup TraceNotificationListener
        SleeManagementMBeanProxy proxy = utils().getSleeManagementMBeanProxy();
        traceMBeanName = proxy.getTraceMBean();
        listener = new TraceNotificationListenerImpl();

        tracembean = new TraceMBeanProxyImpl(traceMBeanName, utils().getMBeanFacade());
        tracembean.addNotificationListener(listener, null, null);


        DeploymentMBeanProxy duProxy = utils().getDeploymentMBeanProxy();
        DeployableUnitDescriptor duDesc = duProxy.getDescriptor(duID);
        ComponentID components[] = duDesc.getComponents();
        // Get the SbbID from the components.
        for (int i = 0; i < components.length; i++) {
            if (components[i] instanceof ServiceID) {
                utils().getLog().fine("Setting serviceID value.");
                serviceID = (ServiceID) components[i];
                continue;
            }

            if (components[i] instanceof SbbID) {
                utils().getLog().fine("Setting sbbID value.");
                sbbID = (SbbID) components[i];
                continue;
            }
        }

    }

    /**
     * Clean up after the test.
     */

    public void tearDown() throws Exception {
        utils().getLog().fine("Disconnecting from resource");
        utils().getResourceInterface().clearActivities();
        utils().getLog().fine("Deactivating and uninstalling service");
        utils().deactivateAllServices();
        utils().uninstallAll();
    }

    private void logSuccessfulCheck(int assertionID) {
        utils().getLog().info("Check for assertion "+assertionID+" OK");
    }

    private TCKResourceListener resourceListener;
    private SleeTCKTestUtils utils;
    private FutureResult result;
    private DeployableUnitID duID;
    private SbbID sbbID;
    private ServiceID serviceID;
    private String testName = "Test1114191";
    private ObjectName traceMBeanName;
    private TraceMBeanProxy tracembean;
    private NotificationListener listener;
    private TraceMBeanProxy traceMBeanProxy;
    private SbbNotification sbbNotification;
    private int receivedTraceNotifications = 0;
    private int expectedTraceNotifications;
    private boolean passed = false;
}
