/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.events.eventcontext;

import javax.slee.ActivityContextInterface;
import javax.slee.InitialEventSelector;
import javax.slee.EventContext;
import javax.slee.facilities.Tracer;

import com.opencloud.sleetck.lib.sbbutils.BaseTCKSbb;
import com.opencloud.sleetck.lib.sbbutils.TCKSbbUtils;
import com.opencloud.sleetck.lib.resource.events.TCKResourceEventX;
import com.opencloud.sleetck.lib.resource.sbbapi.TCKActivity;
import com.opencloud.sleetck.lib.resource.TCKActivityID;

import java.util.HashMap;

/**
 * AssertionID(1108039): Test This resumeDelivery method resumes the delivery of
 * the event associated with this Event Context.
 */
public abstract class Test1108039Sbb2 extends BaseTCKSbb {
    public static final String TRACE_MESSAGE_TCKResourceEventX1 = "Test1108039Sbb2:I got TCKResourceEventX1 on ActivityA";

    public static final String TRACE_MESSAGE_TCKResourceEventX2A = "Test1108039Sbb2:I got TCKResourceEventX2 on ActivityA";

    public static final String TRACE_MESSAGE_TCKResourceEventX2B = "Test1108039Sbb2:I got TCKResourceEventX2 on ActivityB";

    public static final String TRACE_MESSAGE_TCKResourceEventX3 = "Test1108039Sbb2:I got TCKResourceEventX3 on ActivityA";

    public InitialEventSelector initialEventSelector(InitialEventSelector ies) {
        ies.setCustomName("test");
        return ies;
    }

    public void onTCKResourceEventX1(TCKResourceEventX event, ActivityContextInterface aci, EventContext context) {
        try {
            // send message to TCK: I got TCKResourceEventX1 on ActivityA
            tracer = getSbbContext().getTracer("com.test");
            tracer.info(TRACE_MESSAGE_TCKResourceEventX1);
        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }
    }

    public void onTCKResourceEventX2(TCKResourceEventX event, ActivityContextInterface aci, EventContext context) {
        try {
            TCKActivity activity = (TCKActivity) context.getActivityContextInterface().getActivity();
            TCKActivityID activityID = activity.getID();
            tracer = getSbbContext().getTracer("com.test");
            tracer.info("Sbb2:Entering onTCKResourceEventX2");
            if (activityID.getName().equals(Test1108039Test.activityNameB)) {
                // send message to TCK: I got TCKResourceEventX2 on ActivityB
                tracer.info(TRACE_MESSAGE_TCKResourceEventX2B);
                setPassedX2(true);
            } else if (activityID.getName().equals(Test1108039Test.activityNameA)) {
                // send message to TCK: I got TCKResourceEventX2 on ActivityA
                tracer.info(TRACE_MESSAGE_TCKResourceEventX2A);
                if (getPassedX2()) 
                    setPassedX1(true);
            }
            tracer.info("Sbb2:Exiting onTCKResourceEventX2");
            return;

        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }
    }

    public void onTCKResourceEventX3(TCKResourceEventX event, ActivityContextInterface aci, EventContext context) {
        try {
            tracer = getSbbContext().getTracer("com.test");
            tracer.info(TRACE_MESSAGE_TCKResourceEventX3);
            if (getPassedX1())
                sendResultToTCK("Test1108039Test", true, "Sbb2 received TCKResourceEventX3 on ActivityA, this means the resumeDelivery "
                        + "method resumed the delivery of the event associated with this Event Context.", 1108039);
            else
                sendResultToTCK("Test1108039Test", false, "SBB2:onTCKResourceEventX3-ERROR: Sbb2 did not receive TCKResourceEventX3 on ActivityA, this means the resumeDelivery "
                                + "method still does not resume the delivery of the event associated with this Event Context.", 1108039);
            return;
        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }
    }

    private void sendResultToTCK(String testName, boolean result, String message, int failedAssertionID) throws Exception {
        HashMap sbbData = new HashMap();
        sbbData.put("testname", testName);
        sbbData.put("result", result ? "pass" : "fail");
        sbbData.put("message", message);
        sbbData.put("id", new Integer(failedAssertionID));
        TCKSbbUtils.getResourceInterface().sendSbbMessage(sbbData);
    }

    public abstract void setPassedX1(boolean passed);
    public abstract boolean getPassedX1();
        
    public abstract void setPassedX2(boolean passed);
    public abstract boolean getPassedX2();

    private Tracer tracer = null;
}
