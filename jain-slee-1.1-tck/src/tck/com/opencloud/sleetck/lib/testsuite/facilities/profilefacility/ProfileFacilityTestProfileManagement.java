/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.facilities.profilefacility;

import javax.slee.profile.ProfileManagement;
import javax.slee.profile.ProfileVerificationException;

public abstract class ProfileFacilityTestProfileManagement implements ProfileFacilityTestProfileCMP, ProfileManagement {

    private static final int defaultAttr1 = 1;
    private static final int[] defaultAttr2 = { 2, 3, 4 };
    private static final String defaultAttr3 = "ProfileFacilityTest";

    public void profileInitialize() {
        setAttr1(defaultAttr1);
        setAttr2(defaultAttr2);
        setAttr3(defaultAttr3);
    }
    
    public void profileLoad() {}
    public void profileStore() {}
    public void profileVerify() throws ProfileVerificationException {}

}
