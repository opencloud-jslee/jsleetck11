/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.management.sleestate;

import com.opencloud.sleetck.lib.AbstractSleeTCKTest;
import com.opencloud.sleetck.lib.SleeTCKTestUtils;
import com.opencloud.sleetck.lib.SleeTCKTest;
import com.opencloud.sleetck.lib.TCKTestResult;
import com.opencloud.sleetck.lib.TCKTestFailureException;
import com.opencloud.sleetck.lib.TCKTestErrorException;
import com.opencloud.sleetck.lib.testutils.ExceptionsUtil;
import javax.slee.management.SleeState;
import javax.slee.InvalidStateException;

/**
 * Tests assertion 4034, that SleeManagementMBean.shutdown() throws an InvalidStateException
 * if invoked during the RUNNING state.
 */
public class ShutdownFailsFromRunning implements SleeTCKTest {

    public void init(SleeTCKTestUtils utils) { this.utils=utils; }
    public void setUp() {}
    public void tearDown() {}

    public TCKTestResult run() throws Exception {
        SleeState currentState = utils.getSleeManagementMBeanProxy().getState();
        if(!currentState.isRunning()) return TCKTestResult.error("Expected SLEE to be in RUNNING state prior to test run,"+
            " but current state is "+currentState);
        try {
            utils.getSleeManagementMBeanProxy().shutdown();
        } catch (InvalidStateException ise) {
            return TCKTestResult.passed();
        } catch (Exception e) {
            return TCKTestResult.failed(4034,"SleeManagementMBean.shutdown() did not throw an InvalidStateException; "+
                "it threw another Exception:"+ExceptionsUtil.formatThrowable(e));
        }
        return TCKTestResult.failed(4034,"SleeManagementMBean.shutdown() did not throw an InvalidStateException");
    }

    private SleeTCKTestUtils utils;

}