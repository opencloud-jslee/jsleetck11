/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.events.concurrency;

import com.opencloud.sleetck.lib.AbstractSleeTCKTest;
import com.opencloud.sleetck.lib.TCKTestErrorException;
import com.opencloud.sleetck.lib.TCKTestFailureException;
import com.opencloud.sleetck.lib.TCKTestResult;
import com.opencloud.sleetck.lib.resource.TCKActivityID;
import com.opencloud.sleetck.lib.resource.events.TCKResourceEventX;
import com.opencloud.sleetck.lib.resource.events.TCKResourceEventY;
import com.opencloud.sleetck.lib.resource.testapi.TCKResourceTestInterface;
import com.opencloud.sleetck.lib.testutils.BaseTCKResourceListener;
import com.opencloud.sleetck.lib.testutils.FutureResult;
import com.opencloud.util.Future;

import javax.slee.Address;
import javax.slee.AddressPlan;
import java.rmi.RemoteException;
import java.util.HashMap;
import java.util.Map;

/**
 * Tests assertion 770 - that the SLEE invokes sbb event handler methods and callbacks serially.
 * It tests this by blocking inside method invocations and ensuring that no other methods are called on
 * the same sbb object during the block.
 */
public class SbbObjectConcurrencyTest extends AbstractSleeTCKTest {

    // Test parameters

    private static final String WAIT_PERIOD_MS_PARAM = "waitPeriodMs";

    // SleeTCKTest methods

    public TCKTestResult run() throws Exception {
        TCKResourceTestInterface resource = utils().getResourceInterface();
        getLog().info("Creating activities A and B");
        TCKActivityID activityA = resource.createActivity("SbbObjectConcurrencyTest-ActvityA");
        activityB = resource.createActivity("SbbObjectConcurrencyTest-ActvityB");
        getLog().info("firing an X1 event on activity A");
        address = new Address(AddressPlan.IP,"1.0.0.1");
        resource.fireEvent(TCKResourceEventX.X1,null,activityA,address);
        long timeout = utils().getTestTimeout() + waitPeriodMs * 3;
        getLog().info("Total timeout is "+timeout+" ms");
        try {
            return result.waitForResult(timeout);
        } catch (Future.TimeoutException timeoutException) {
            return diagnoseTimeout(timeoutException);
        }
    }

    private TCKTestResult diagnoseTimeout(Future.TimeoutException timeoutException) {
        // try to make as specific a failure message as we can
        synchronized(stateLock) {
            String diagnosis = null;
            boolean isY1EventFired = y1EventObjectID == -1;
            boolean isY2EventFired = y2EventObjectID == -1;
            boolean isY3EventFired = y3EventObjectID == -1;
            if(isX1HandlerCalled && isSbbExceptionThrownCalled && isX1RolledBack) {
                if(!isY1HandlerCalled && !isY1RolledBack && !isY1ProcessingFinished) {
                    if(isY1EventFired) diagnosis = "Test error: test didn't appear to successfully fire Y1 event";
                    else diagnosis = "Didn't receive ACK from event handler or rollback or indication of event processing completion for Y1 event";
                }
                else if(!isY2HandlerCalled && !isY2RolledBack && !isY2ProcessingFinished) {
                    if(isY2EventFired) diagnosis = "Test error: test didn't appear to successfully fire Y2 event";
                    else diagnosis = "Didn't receive ACK from event handler or rollback or indication of event processing completion for Y2 event";
                }
                else if(!isY3HandlerCalled && !isY3RolledBack && !isY3ProcessingFinished) {
                    if(isY3EventFired) diagnosis = "Test error: test didn't appear to successfully fire Y3 event";
                    else diagnosis = "Didn't receive ACK from event handler or rollback or indication of event processing completion for Y3 event";
                }
            }
            else if(!isX1RolledBack) diagnosis = "sbbRolledBack() wasn't called for the X1 event";
            else if(!isSbbExceptionThrownCalled) diagnosis = "sbbExceptionThrown() wasn't called";
            else if(!isX1HandlerCalled) diagnosis = "X1 event handler wasn't called";
            String testState = "isX1HandlerCalled="+isX1HandlerCalled+",isSbbExceptionThrownCalled="+isSbbExceptionThrownCalled+
            ",isX1RolledBack="+isX1RolledBack+
            ",isY1EventFired="+isY1EventFired+",isY1HandlerCalled="+isY1HandlerCalled+",isY1ProcessingFinished="+isY1ProcessingFinished+
            ",isY2EventFired="+isY2EventFired+",isY2HandlerCalled="+isY2HandlerCalled+",isY2ProcessingFinished="+isY2ProcessingFinished+
            ",isY3EventFired="+isY3EventFired+",isY3HandlerCalled="+isY3HandlerCalled+",isY3ProcessingFinished="+isY3ProcessingFinished;
            return TCKTestResult.error("Timed out while waiting for event handler invocations and sbb callbacks. Diagnosis:"+
                diagnosis+". Test state:"+testState,timeoutException);
        }
    }

    public void setUp() throws Exception {
        waitPeriodMs = Integer.parseInt(utils().getTestParams().getProperty(WAIT_PERIOD_MS_PARAM));
        activeCalls = new HashMap();
        y1EventObjectID = y2EventObjectID = y3EventObjectID = 0L;
        isX1HandlerCalled = isSbbExceptionThrownCalled = isX1RolledBack = false;
        isY1HandlerCalled = isY2HandlerCalled = isY3HandlerCalled = false;
        isY1RolledBack = isY2RolledBack = isY3RolledBack = false;
        isY1ProcessingFinished = isY2ProcessingFinished = isY3ProcessingFinished = false;
        result = new FutureResult(getLog());
        setResourceListener(new ResourceListenerImpl());
        super.setUp();
    }

    public void tearDown() throws Exception {
        super.tearDown();
        activeCalls.clear();
        activityB = null;
    }

    // Private classes

    private class ResourceListenerImpl extends BaseTCKResourceListener {

        // TCKResourceListener methods

        /**
         * Called synchronously by the Sbb.
         * It expects an int array argument in the form {code,sbbObjectID}.
         * This method is responsible for:
         * a) servicing requests for sbb object IDs
         * b) blocking method invocations
         * c) checking that method invocations are not made in parallel for a given sbb object
         * d) detecting when all expected calls have been made
         */
        public Object onSbbCall(Object argument) throws Exception {
            try {
                int[] codeAndSbbOID = (int[])argument;
                Integer code = new Integer(codeAndSbbOID[0]);
                Integer sbbObjectID = new Integer(codeAndSbbOID[1]);
                getLog().info("Received call from sbb object "+sbbObjectID+". Code="+formatCallCode(code));
                if(code.intValue() == SbbObjectConcurrencyTestConstants.REQUEST_SBB_OID) return new Integer(allocateSbbOID());
                synchronized (stateLock) {
                    boolean shouldBlock = handleSbbInvocationACK(code.intValue());
                    Integer currentForSbbObject = (Integer)activeCalls.get(sbbObjectID);
                    if(currentForSbbObject != null) {
                        // we received a syncronous call from the sbb object during another method invocation
                        throw new TCKTestFailureException(770,"Serial method invocation was not maintained on a single sbb object."+
                            "sbbObjectID="+sbbObjectID+", active method type="+formatCallCode(currentForSbbObject)+". second method type="+formatCallCode(code));
                    }
                    if(shouldBlock) {
                        getLog().info("Registering the current method invocation for sbb object "+sbbObjectID);
                        activeCalls.put(sbbObjectID,code);
                        long now = System.currentTimeMillis();
                        long timeoutAt = now + waitPeriodMs;
                        getLog().info("Will wait for "+waitPeriodMs+" ms, or until a result is set");
                        while(now < timeoutAt && !result.isSet()) {
                            try {
                                stateLock.wait(timeoutAt - now);
                            } catch (InterruptedException ie) { /*no-op*/}
                            now = System.currentTimeMillis();
                        }
                        getLog().info("Timeout reached, or result set. Deregistering method invocation for sbb object "+sbbObjectID);
                        activeCalls.remove(sbbObjectID);
                    }
                    checkIsSatisfied();
                }
            } catch(Exception exception) {
                onException(exception);
            }
            return null; // a return value is only required for sbb oid requests
        }

        public void onEventProcessingSuccessful(long eventObjectID) throws RemoteException {
            onEventProcessingCompleted(eventObjectID,true,null,null);
        }

        public void onEventProcessingFailed(long eventObjectID, String message, Exception exception) {
            onEventProcessingCompleted(eventObjectID,false,message,exception);
        }

        public void onException(Exception exception) {
            if(exception instanceof TCKTestFailureException) result.setFailed((TCKTestFailureException)exception);
            else result.setError(exception);
            synchronized (stateLock) {
                stateLock.notifyAll();
            }
        }

        /**
         * Handles the given SBB invocation ACK, and returns whether the call should block
         */
        private boolean handleSbbInvocationACK(int callCode) throws Exception {
            boolean shouldBlock = false;
            switch (callCode) {
                case SbbObjectConcurrencyTestConstants.RECEIVED_X1:
                    isX1HandlerCalled = true;
                    getLog().info("Firing a Y1 event on activity B");
                    y1EventObjectID = utils().getResourceInterface().fireEvent(TCKResourceEventY.Y1,null,activityB,address);
                    shouldBlock = true;
                    break;
                case SbbObjectConcurrencyTestConstants.SBB_EXCEPTION_THROWN:
                    isSbbExceptionThrownCalled = true;
                    getLog().info("Firing a Y2 event on activity B");
                    y2EventObjectID = utils().getResourceInterface().fireEvent(TCKResourceEventY.Y2,null,activityB,address);
                    shouldBlock = true;
                    break;
                case SbbObjectConcurrencyTestConstants.SBB_ROLLED_BACK_X1:
                    isX1RolledBack = true;
                    getLog().info("Firing a Y3 event on activity B");
                    y3EventObjectID = utils().getResourceInterface().fireEvent(TCKResourceEventY.Y3,null,activityB,address);
                    shouldBlock = true;
                    break;
                case SbbObjectConcurrencyTestConstants.RECEIVED_Y1:
                    isY1HandlerCalled = true;
                    break;
                case SbbObjectConcurrencyTestConstants.RECEIVED_Y2:
                    isY2HandlerCalled = true;
                    break;
                case SbbObjectConcurrencyTestConstants.RECEIVED_Y3:
                    isY3HandlerCalled = true;
                    break;
                case SbbObjectConcurrencyTestConstants.SBB_ROLLED_BACK_Y1:
                    isY1RolledBack = true;
                    break;
                case SbbObjectConcurrencyTestConstants.SBB_ROLLED_BACK_Y2:
                    isY2RolledBack = true;
                    break;
                case SbbObjectConcurrencyTestConstants.SBB_ROLLED_BACK_Y3:
                    isY3RolledBack = true;
                    break;
                default: throw new IllegalArgumentException("Unrecognized code passed to onSbbCall(): "+callCode);
            }
            return shouldBlock;
        }

        /**
         * Called by onEventProcessingSuccessful and onEventProcessingFailed()
         * @param eventObjectID the eventObjectID passed to the calling method
         * @param wasSuccess true for onEventProcessingSuccessful, false for onEventProcessingFailed
         * @param message the message passed to onEventProcessingFailed, or null
         * @param exception the Exception passed to onEventProcessingFailed, or null
         */
        private void onEventProcessingCompleted(long eventObjectID, boolean wasSuccess, String message, Exception exception) {
            synchronized (stateLock) {
                boolean isY1Event = eventObjectID == y1EventObjectID;
                boolean isY2Event = eventObjectID == y2EventObjectID;
                boolean isY3Event = eventObjectID == y3EventObjectID;
                if(isY1Event || isY2Event || isY3Event) {
                    boolean isACKOrRollbackReceived =
                            isY1Event ? (isY1HandlerCalled || isY1RolledBack) :
                            isY2Event ? (isY2HandlerCalled || isY2RolledBack) :
                                        (isY3HandlerCalled || isY3RolledBack);
                    if(!isACKOrRollbackReceived) {
                        String eventName  = isY1Event ? "Y1" :
                                            isY2Event ? "Y2" : "Y3";
                        String methodName = wasSuccess ? "onEventProcessingSuccessful()" : "onEventProcessingFailed()";
                        getLog().warning("Received "+methodName+" call for a "+eventName+" event before an ACK was received " +
                                "from the "+eventName+" event handler, or from an sbbRolledBack() call for the event. "+
                                "This is allowed by the test, as the SLEE may choose to timeout the "+eventName+
                                " event while waiting for the blocking callback to return.");
                        if(!wasSuccess) {
                            getLog().warning("Failure message="+message+". Failure exception follows:");
                            getLog().warning(exception);
                        }
                    }
                    if(isY1Event) isY1ProcessingFinished = true;
                    else if(isY2Event) isY2ProcessingFinished = true;
                    else if(isY3Event) isY3ProcessingFinished = true;
                    checkIsSatisfied();
                } else if(wasSuccess) {
                    getLog().fine("Received onEventProcessingSuccessful() callback for X1 event");
                } else {
                    onException(new TCKTestErrorException("Unexpected call to onEventProcessingFailed() received "+
                        " for the X1 event. eventObjectID="+eventObjectID+". Failure message="+message,exception));
                }
            }
        }

        /**
         * Checks whether all the expected callbacks have been made.
         * If they have been, and there are no active calls, and no fail or error result has been set,
         * the result is set to a pass.
         */
        private void checkIsSatisfied() {
            synchronized (stateLock) {
                if(isX1HandlerCalled && isSbbExceptionThrownCalled && isX1RolledBack &&
                        (isY1HandlerCalled || isY1RolledBack || isY1ProcessingFinished) &&
                        (isY2HandlerCalled || isY2RolledBack || isY2ProcessingFinished) &&
                        (isY3HandlerCalled || isY3RolledBack || isY3ProcessingFinished) &&
                        activeCalls.isEmpty() && !result.isSet()) {
                    getLog().info("All expected callbacks received serially");
                    result.setPassed();
                    stateLock.notifyAll();
                }
            }
        }
    }

    // Private methods

    /**
     * Returns a unique sbb object ID
     */
    private synchronized int allocateSbbOID() {
        return nextSbbOID++;
    }

    private String formatCallCode(Integer callCode) {
        switch (callCode.intValue()) {
            case SbbObjectConcurrencyTestConstants.REQUEST_SBB_OID: return "REQUEST_SBB_OID";
            case SbbObjectConcurrencyTestConstants.RECEIVED_X1: return "RECEIVED_X1";
            case SbbObjectConcurrencyTestConstants.RECEIVED_Y1: return "RECEIVED_Y1";
            case SbbObjectConcurrencyTestConstants.RECEIVED_Y2: return "RECEIVED_Y2";
            case SbbObjectConcurrencyTestConstants.RECEIVED_Y3: return "RECEIVED_Y3";
            case SbbObjectConcurrencyTestConstants.SBB_ROLLED_BACK_X1: return "SBB_ROLLED_BACK_X1";
            case SbbObjectConcurrencyTestConstants.SBB_ROLLED_BACK_Y1: return "SBB_ROLLED_BACK_Y1";
            case SbbObjectConcurrencyTestConstants.SBB_ROLLED_BACK_Y2: return "SBB_ROLLED_BACK_Y2";
            case SbbObjectConcurrencyTestConstants.SBB_ROLLED_BACK_Y3: return "SBB_ROLLED_BACK_Y3";
            case SbbObjectConcurrencyTestConstants.SBB_EXCEPTION_THROWN: return "SBB_EXCEPTION_THROWN";
            default: return "(Invalid call code:"+callCode+")";
        }
    }

    // Private state

    private int waitPeriodMs; // the number of milliseconds to block during a method invocation
    private TCKActivityID activityB;
    private Address address;

    private Map activeCalls; // codes for activate method calls, indexed by sbb object ID
    private int nextSbbOID = 1;
    private FutureResult result;

    private Object stateLock = new Object();

    private volatile boolean isX1HandlerCalled;
    private volatile boolean isSbbExceptionThrownCalled;
    private volatile boolean isX1RolledBack;

    private long y1EventObjectID;
    private long y2EventObjectID;
    private long y3EventObjectID;

    private volatile boolean isY1HandlerCalled;
    private volatile boolean isY2HandlerCalled;
    private volatile boolean isY3HandlerCalled;

    private volatile boolean isY1RolledBack;
    private volatile boolean isY2RolledBack;
    private volatile boolean isY3RolledBack;

    private volatile boolean isY1ProcessingFinished;
    private volatile boolean isY2ProcessingFinished;
    private volatile boolean isY3ProcessingFinished;

}
