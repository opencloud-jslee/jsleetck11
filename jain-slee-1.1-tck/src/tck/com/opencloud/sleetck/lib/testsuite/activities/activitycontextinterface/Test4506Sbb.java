/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.activities.activitycontextinterface;

import com.opencloud.sleetck.lib.resource.events.TCKResourceEventX;
import com.opencloud.sleetck.lib.sbbutils.BaseTCKSbb;
import com.opencloud.sleetck.lib.sbbutils.TCKSbbUtils;
import com.opencloud.sleetck.lib.TCKTestFailureException;

import javax.slee.ActivityContextInterface;
import javax.slee.Address;
import javax.slee.facilities.Level;
import javax.slee.nullactivity.NullActivity;
import javax.slee.nullactivity.NullActivityContextInterfaceFactory;
import javax.slee.nullactivity.NullActivityFactory;

/**
 * An SBB to test assertsions 4506 and 4507.
 * Assertion 4506 tests that an event cannot be fired on an activity that is in ending state.
 * Assertion 4507 tests the ActivityContextInterface.isEnding() method.
 */
public abstract class Test4506Sbb extends BaseTCKSbb {
    /**
     * Initiates the test.
     * @param event
     * @param aci
     */
    public void onTCKResourceEventX1(TCKResourceEventX event, ActivityContextInterface aci) {
        try {
            TCKSbbUtils.createTrace(getSbbID(), Level.INFO, "X1 event handler", null);

            NullActivityFactory factory =
                    (NullActivityFactory) TCKSbbUtils.getSbbEnvironment().lookup("slee/nullactivity/factory");
            NullActivityContextInterfaceFactory factory2 = (NullActivityContextInterfaceFactory)
                    TCKSbbUtils.getSbbEnvironment().lookup("slee/nullactivity/activitycontextinterfacefactory");

            NullActivity nullactivity = factory.createNullActivity();
            ActivityContextInterface nullaci = factory2.getActivityContextInterface(nullactivity);

            TCKSbbUtils.createTrace(getSbbID(), Level.INFO, "Created and attached null activity", null);

            // Fire two events on the new NullActivity
            fireTest4506Event(new Test4506Event(1), nullaci, null);
            fireTest4506Event(new Test4506Event(2), nullaci, null);
        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }
    }

    /**
     * This event will be fired twice on a NULL activity.  The first invocation will call the NullActivity's endActivity()
     * method.  The second invocation will check the status of the ActivityContextInterface.isEnding() method and check
     * that no more events can be fired on the activity.
     * @param event
     * @param aci
     */
    public void onTest4506Event(Test4506Event event, ActivityContextInterface aci) {
        try {
            final int sequenceNumber = event.getSequenceNumber();
            TCKSbbUtils.createTrace(getSbbID(), Level.INFO, "Test4506Event event handler, seq=" + sequenceNumber, null);

            if (1 == sequenceNumber) {
                // first invocation, end the NullActivity
                if (true == aci.isEnding()) {
                    fail(4507, "ActivityContextInterface.isEnding should return false for an activity that is not ending");
                } else {
                    TCKSbbUtils.getResourceInterface().sendSbbMessage(new Integer(sequenceNumber));
                }
                TCKSbbUtils.createTrace(getSbbID(), Level.INFO, "Ending the null activity", null);
                NullActivity activity = (NullActivity) aci.getActivity();
                activity.endActivity();
            }

            else if (2 == sequenceNumber) {
                // second invocation, verify that the NullActivity is ending and that we cannot fire any more events
                // on it.
                TCKSbbUtils.createTrace(getSbbID(), Level.INFO, "Null activity ending state: " + aci.isEnding(), null);
                if (false == aci.isEnding()) {
                    fail(4507, "ActivityContextInterface.isEnding should return true (NullActivity.endActivity() " +
                               "has been invoked");
                }
                else {
                    try {
                        fireTest4506Event(new Test4506Event(3), aci, null);
                        fail(4506, "The SLEE allowed an event to be fired on an activity in the ending state");
                    } catch (Exception e) {
                        TCKSbbUtils.createTrace(getSbbID(), Level.INFO, "Got expected exception", e);
                        TCKSbbUtils.getResourceInterface().sendSbbMessage(new Integer(sequenceNumber));
                    }
                }
            }

            else if (3 == event.getSequenceNumber()) {
                // should never get here
                fail(4506, "An event was fired and delivered on an activity in the ending state");
            }

        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }
    }

    public abstract void fireTest4506Event(Test4506Event event, ActivityContextInterface aci,
                                           Address address);

    private void fail(int assertionId, String message) {
        TCKSbbUtils.handleException(new TCKTestFailureException(assertionId, message));
    }

}
