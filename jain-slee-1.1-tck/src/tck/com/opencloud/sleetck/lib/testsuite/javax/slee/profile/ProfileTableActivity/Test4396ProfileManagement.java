/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.javax.slee.profile.ProfileTableActivity;

import javax.slee.profile.ProfileManagement;
import javax.slee.profile.ProfileVerificationException;

public abstract class Test4396ProfileManagement implements Test4396ProfileCMP, ProfileManagement {

    public void profileInitialize() {
        setStringArray(new String[] { "Foo", "Bar", "Baz" });
        setIntArray(new int[] {1, 2, 4, 8});
    }

    public void profileLoad() {}
    public void profileStore() {}
    public void profileVerify() throws ProfileVerificationException {}

}
