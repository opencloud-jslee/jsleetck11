/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.javax.slee.management.TraceNotification;

import com.opencloud.sleetck.lib.SleeTCKTest;
import com.opencloud.sleetck.lib.SleeTCKTestUtils;
import com.opencloud.sleetck.lib.TCKTestResult;
import com.opencloud.sleetck.lib.resource.TCKActivityID;
import com.opencloud.sleetck.lib.resource.events.TCKResourceEventX;
import com.opencloud.sleetck.lib.resource.testapi.TCKResourceTestInterface;
import com.opencloud.sleetck.lib.testutils.FutureResult;
import com.opencloud.sleetck.lib.testutils.jmx.DeploymentMBeanProxy;
import com.opencloud.sleetck.lib.testutils.jmx.TraceMBeanProxy;

import javax.management.Notification;
import javax.slee.ComponentID;
import javax.slee.SbbID;
import javax.slee.management.DeployableUnitDescriptor;
import javax.slee.management.DeployableUnitID;
import javax.slee.management.TraceNotification;

public class Test4185Test implements SleeTCKTest, javax.management.NotificationListener {

    private static final String SERVICE_DU_PATH_PARAM = "DUPath";
    private static final int TEST_ID = 4185;

    public void init(SleeTCKTestUtils utils) {
    this.utils = utils;
    }

    /**
     * Perform the actual test.
     */

    public TCKTestResult run() throws Exception {
    result = new FutureResult(utils.getLog());

    TCKResourceTestInterface resource = utils.getResourceInterface();
    TCKActivityID activityID = resource.createActivity("Test4185InitialActivity");
    resource.fireEvent(TCKResourceEventX.X1, TCKResourceEventX.X1, activityID, null);

    return result.waitForResultOrFail(utils.getTestTimeout(), "Timeout waiting for test result.", TEST_ID);
    }

    /**
     * Do all the pre-run configuration of the test.
     */

    public void setUp() throws Exception {

    utils.getLog().fine("Installing and activating service");

    TraceMBeanProxy traceMBeanProxy = utils.getMBeanProxyFactory().createTraceMBeanProxy(utils.getSleeManagementMBeanProxy().getTraceMBean());
    traceMBeanProxy.addNotificationListener(this, null, null);

    // utils.getSleeManagementMBeanProxy().addNotificationListener(this, null, null);

    // Install the Deployable Units
    String duPath = utils.getTestParams().getProperty(SERVICE_DU_PATH_PARAM);
    duID = utils.install(duPath);

    // Activate the DU
    utils.activateServices(duID, true);

    // Retrieve the SbbID for later on.
        DeploymentMBeanProxy duProxy = utils.getDeploymentMBeanProxy();
        DeployableUnitDescriptor duDesc = duProxy.getDescriptor(duID);
        ComponentID components[] = duDesc.getComponents();
        // Get the SbbID from the components.
        for (int i = 0; i < components.length; i++) {
            if (components[i] instanceof SbbID) {
                utils.getLog().fine("Setting sbbID value.");
                sbbID = (SbbID) components[i];
                continue;
            }
        }
    }

    /**
     * Clean up after the test.
     */

    public void tearDown() throws Exception {
    utils.getLog().fine("Disconnecting from resource");
        utils.getResourceInterface().clearActivities();
        utils.getLog().fine("Deactivating and uninstalling service");
        utils.deactivateAllServices();
        utils.uninstallAll();
    }

    public synchronized void handleNotification(Notification notification, Object handback) {

    if (notification instanceof TraceNotification) {
        TraceNotification traceNotification = (TraceNotification) notification;

        if ((traceNotification.getType() != null) && (traceNotification.getType().equals("javax.slee.management.trace.raentity")))
            return;


        if (firstNotification == null) {
        firstNotification = traceNotification;
        return;
        }

        if (traceNotification.getCause() != null) {
        result.setFailed(4191, "TraceNotification.getCause() returned non-null for a notification with no cause.");
        return;
        }

        if (!traceNotification.getMessageType().equals("javax.slee.management.trace")) {
        result.setFailed(4185, "TraceNotification.getMessageType() returned incorrect message type.");
        return;
        }

        if (!traceNotification.getMessageSource().equals(sbbID)) {
        result.setFailed(4187, "TraceNotification.getMessageSource() returned incorrect message source.");
        return;
        }

        if (traceNotification.equals(firstNotification)) {
        result.setFailed(4193, "TraceNotification.equals(TraceNotification) returned true for non-matching TraceNotification objects.");
        return;
        }

        if (!traceNotification.equals(traceNotification)) {
        result.setFailed(4193, "TraceNotification.equals(TraceNotification) returned false for matching TraceNotification objects.");
        return;
        }

        try {
        traceNotification.hashCode();
        } catch (Exception e) {
        result.setFailed(4195, "TraceNotification.hashCode() threw an exception.");
        return;
        }

        try {
        traceNotification.toString();
        } catch (Exception e) {
        result.setFailed(4197, "TraceNotification.toString() threw an exception.");
        return;
        }

        if (traceNotification.toString() == null) {
        result.setFailed(4197, "TraceNotification.toString() returned null.");
        return;
        }


        result.setPassed();
        return;
    }

    return;

    }

    private SleeTCKTestUtils utils;
    private FutureResult result;
    private DeployableUnitID duID;
    private TraceNotification firstNotification;
    private SbbID sbbID;
}
