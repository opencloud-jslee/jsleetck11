/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.javax.slee.management.SleeManagementMBean;

import javax.management.ObjectName;
import javax.slee.InvalidArgumentException;
import javax.slee.SbbID;
import javax.slee.management.DeployableUnitID;
import javax.slee.management.ManagementException;
import javax.slee.management.UnrecognizedSubsystemException;

import com.opencloud.sleetck.lib.AbstractSleeTCKTest;
import com.opencloud.sleetck.lib.TCKTestResult;
import com.opencloud.sleetck.lib.testutils.jmx.DeploymentMBeanProxy;
import com.opencloud.sleetck.lib.testutils.jmx.SleeManagementMBeanProxy;

/*
 * The SleeManagementMBean interface defines the central management interface for the SLEE.
 * Use this MBean interface to access subsystem details. If this test looks trivial it is
 * because it is Javadoc testing and we are only interested in the interfaces.
 * Note: If the SLEE does not have a subsystem with Usage, there's nothing this test can do
 * about it
 */

public class Test1114500Test extends AbstractSleeTCKTest {

    private static final String SERVICE_DU_PATH_PARAM = "serviceDUPath";
    private static final int TEST_ID = 1114500;

    /**
     * Perform the actual test.
     */
    public TCKTestResult run() throws Exception {
        boolean subsystemWithUsageExists = false;
        boolean subsystemWithoutUsageExists = false;
        String usageSubsystem = null;
        String noUsageSubsystem = null;
        SleeManagementMBeanProxy sleeProxy;
        TCKTestResult testResult = null;

       //1114301
        try {
            sleeProxy = utils().getSleeManagementMBeanProxy();
            String strObjectName = utils().getSleeManagementMBeanName().toString();
            getLog().fine("1114301: SleeManagementMBean = "+strObjectName);
         }
        catch (Exception e) {
            getLog().warning(e);
            return TCKTestResult.error("Failed to create SleeManagementMBean");
        }

        try{

            try {
                 String[] installedSubsystems = sleeProxy.getSubsystems();
                 logSuccessfulCheck(1114504);

                 /* Check the subsystems in the SLEE and assign last one found for both Usage and no Usage
                  * Note: If the SLEE does not have a subsystem with Usage, there's nothing this test can do
                  * about it
                  * */
                 for (int i=0; i<installedSubsystems.length; i++) {
                     utils().getLog().fine("Installed Subsystem is: " + installedSubsystems[i]);
                     try{
                         boolean usage = sleeProxy.hasUsage(installedSubsystems[i]);
                         utils().getLog().fine("Installed Subsystem hasUsage = " +usage);
                         if (usage) {
                             usageSubsystem = installedSubsystems[i];  // Usage Subsystem
                             subsystemWithUsageExists = true;
                         } else {
                             noUsageSubsystem = installedSubsystems[i];    // No Usage Subsystem
                             subsystemWithoutUsageExists = true;
                         }
                     } catch (Exception e) {
                         return TCKTestResult.failed(1114507, "SleeManagementMBeanProxy.hasUsage() has thrown Exception: " + e.getClass().toString());
                     }
                 }

            } catch (ManagementException e) {
                 return TCKTestResult.failed(1114504, "The Installed Subsystems name(s) could not obtained due to a system-level failure");
            } catch (Exception e) {
                 return TCKTestResult.failed(1114504, "SleeManagementMBeanProxy.getSubsystems() has thrown Exception: " + e.getClass().toString());
            }

            // Test the Usage and No Usage cases by calling the test with each subsystem
            if (subsystemWithoutUsageExists) {
                utils().getLog().fine("Checking SleeManagementMBean for subsystem without Usage...");
                testResult = checkSleeSubsystems(sleeProxy, noUsageSubsystem, false);
            }

            if (subsystemWithUsageExists && (testResult == null || testResult == TCKTestResult.passed())) {
                utils().getLog().fine("Now checking SleeManagementMBean for subsystem with Usage...");
                testResult = checkSleeSubsystems(sleeProxy, usageSubsystem, true);
            }

        } finally {
            utils().getLog().fine("Test Complete");
        }

        return testResult;

    }

    public TCKTestResult checkSleeSubsystems(SleeManagementMBeanProxy sleeProxy, String installedSubsystem, boolean subsystemUsage) throws Exception {
        String usageParam = "NoUsageParams";
        String[] notrecognizedusageParams = null;
        String notrecognizedSubsystem = "notrecognizedSubsystem";
        String nullSubsystem = null;


        try {
            boolean usage = sleeProxy.hasUsage(installedSubsystem);
            if (usage == subsystemUsage)
                logSuccessfulCheck(1114507);
            else
                return TCKTestResult.failed(1114507, "SleeManagementMBeanProxy.hasUsage() has returned incorrect value: " +usage);
        } catch (UnrecognizedSubsystemException e) {
            return TCKTestResult.failed(1114507, "The Installed Subsystems name(s) was not recognised");
        } catch (ManagementException e) {
            return TCKTestResult.failed(1114507, "The Installed Subsystems name(s) could not obtained due to a system-level failure");
        } catch (Exception e) {
            return TCKTestResult.failed(1114507, "SleeManagementMBeanProxy.hasUsage() has thrown Exception: " + e.getClass().toString());
        }

        try {
            boolean usage = sleeProxy.hasUsage(notrecognizedSubsystem);
            return TCKTestResult.failed(1114508, "SleeManagementMBeanProxy.hasUsage() has not thrown Exception: UnrecognizedSubsystemException");
        } catch (UnrecognizedSubsystemException e) {
            logSuccessfulCheck(1114508);
        } catch (Exception e) {
            return TCKTestResult.failed(1114508, "SleeManagementMBeanProxy.hasUsage() has thrown Exception: " + e.getClass().toString());
        }

        try {
            String[] usageParams = sleeProxy.getUsageParameterSets(installedSubsystem);
            logSuccessfulCheck(1114511);
            usageParam = usageParams[0];
        } catch (NullPointerException e) {
            return TCKTestResult.failed(1114511, "The Installed Subsystems name(s) was null");
        } catch (UnrecognizedSubsystemException e) {
            return TCKTestResult.failed(1114511, "The Installed Subsystems name(s) was not recognised");
        } catch (InvalidArgumentException e) {
            // Can be that this subsystem has no Usage
            if (!subsystemUsage) {
                logSuccessfulCheck(1114511);
            } else {
                return TCKTestResult.failed(1114511, "The Installed Subsystems name(s) was not valid");
            }
        } catch (ManagementException e) {
            return TCKTestResult.failed(1114511, "The Installed Subsystems name(s) could not obtained due to a system-level failure");
        } catch (Exception e) {
            return TCKTestResult.failed(1114511, "SleeManagementMBeanProxy.getUsageParameterSets() has thrown Exception: " + e.getClass().toString());
        }

        try {
            String[] usageParams = sleeProxy.getUsageParameterSets(nullSubsystem);
            return TCKTestResult.failed(1114512, "SleeManagementMBeanProxy.getUsageParameterSets() has not thrown Exception NullPointerException");
        } catch (NullPointerException e) {
            logSuccessfulCheck(1114512);
        } catch (Exception e) {
            return TCKTestResult.failed(1114512, "SleeManagementMBeanProxy.getUsageParameterSets() has thrown Exception: " + e.getClass().toString());
        }

        try {
            String[] usageParams = sleeProxy.getUsageParameterSets(notrecognizedSubsystem);
            return TCKTestResult.failed(1114513, "SleeManagementMBeanProxy.getUsageParameterSets() has not thrown Exception UnrecognizedSubsystemException");
        } catch (UnrecognizedSubsystemException e) {
            logSuccessfulCheck(1114513);
        } catch (Exception e) {
            return TCKTestResult.failed(1114513, "SleeManagementMBeanProxy.getUsageParameterSets() has thrown Exception: " + e.getClass().toString());
        }

        if (!subsystemUsage) {
            try {
                String[] usageParams = sleeProxy.getUsageParameterSets(installedSubsystem);
                return TCKTestResult.failed(1114514, "SleeManagementMBeanProxy.getUsageParameterSets() has not thrown Exception UnsupportedOperationException");
            } catch (InvalidArgumentException e) {
                logSuccessfulCheck(1114514);
            } catch (Exception e) {
                return TCKTestResult.failed(1114514, "SleeManagementMBeanProxy.getUsageParameterSets() has thrown Exception: " + e.getClass().toString());
            }
        }

        try {
            ObjectName objectName = sleeProxy.getUsageMBean(installedSubsystem);
            logSuccessfulCheck(1114517);
        } catch (NullPointerException e) {
            return TCKTestResult.failed(1114517, "The Installed Subsystems name(s) was null");
        } catch (UnrecognizedSubsystemException e) {
            return TCKTestResult.failed(1114517, "The Installed Subsystems name(s) was not recognised");
        } catch (InvalidArgumentException e) {
            // Can be that this subsystem has no Usage
            if (!subsystemUsage) {
                logSuccessfulCheck(1114517);
            } else {
                return TCKTestResult.failed(1114517, "The Installed Subsystems name(s) was not valid");
            }
        } catch (ManagementException e) {
            return TCKTestResult.failed(1114517, "The Installed Subsystems name(s) could not obtained due to a system-level failure");
        } catch (Exception e) {
            return TCKTestResult.failed(1114517, "SleeManagementMBeanProxy.getSubsystems() has thrown Exception: " + e.getClass().toString());
        }

        try {
            ObjectName objectName = sleeProxy.getUsageMBean(nullSubsystem);
            return TCKTestResult.failed(1114518, "SleeManagementMBeanProxy.getUsageMBean() has not thrown Exception NullPointerException");
         } catch (NullPointerException e) {
            logSuccessfulCheck(1114518);
        } catch (Exception e) {
            return TCKTestResult.failed(1114518, "SleeManagementMBeanProxy.getUsageMBean() has thrown Exception: " + e.getClass().toString());
        }

        try {
            ObjectName objectName = sleeProxy.getUsageMBean(notrecognizedSubsystem);
            return TCKTestResult.failed(1114519, "SleeManagementMBeanProxy.getUsageMBean() has not thrown Exception: UnrecognizedSubsystemException");
        } catch (UnrecognizedSubsystemException e) {
            logSuccessfulCheck(1114519);
        } catch (Exception e) {
            return TCKTestResult.failed(1114519, "SleeManagementMBeanProxy.getUsageMBean() has thrown Exception: " + e.getClass().toString());
        }

        if (!subsystemUsage) {
            try {
                ObjectName objectName = sleeProxy.getUsageMBean(installedSubsystem);
                return TCKTestResult.failed(1114520, "SleeManagementMBeanProxy.getUsageMBean() has not thrown Exception: InvalidArgumentException");
            } catch (InvalidArgumentException e) {
                logSuccessfulCheck(1114520);
            } catch (Exception e) {
                return TCKTestResult.failed(1114520, "SleeManagementMBeanProxy.getUsageMBean() has thrown Exception: " + e.getClass().toString());
            }
        }

        usageParam = "firstCount";
        try {
            ObjectName objectName = sleeProxy.getUsageMBean(installedSubsystem, usageParam);
            logSuccessfulCheck(1114523);
        } catch (NullPointerException e) {
            return TCKTestResult.failed(1114523, "The Installed Subsystems name(s) was null");
        } catch (UnrecognizedSubsystemException e) {
            return TCKTestResult.failed(1114523, "The Installed Subsystems name(s) was not recognised");
        } catch (InvalidArgumentException e) {
            // Can be that this subsystem has no Usage
            if (!subsystemUsage) {
                logSuccessfulCheck(1114523);
            } else {
                return TCKTestResult.failed(1114523, "The Installed Subsystems name(s) was not valid");
            }
        } catch (ManagementException e) {
            return TCKTestResult.failed(1114523, "The Installed Subsystems name(s) could not obtained due to a system-level failure");
        } catch (Exception e) {
            return TCKTestResult.failed(1114523, "SleeManagementMBeanProxy.getSubsystems() has thrown Exception: " + e.getClass().toString());
        }

        try {
            ObjectName objectName = sleeProxy.getUsageMBean(nullSubsystem, usageParam);
            return TCKTestResult.failed(1114524, "SleeManagementMBeanProxy.getUsageMBean() has not thrown Exception: NullPointerException");
         } catch (NullPointerException e) {
            logSuccessfulCheck(1114524);
        } catch (Exception e) {
            return TCKTestResult.failed(1114524, "SleeManagementMBeanProxy.getUsageMBean() has thrown Exception: " + e.getClass().toString());
        }

        try {
            ObjectName objectName = sleeProxy.getUsageMBean(notrecognizedSubsystem, usageParam);
            return TCKTestResult.failed(1114525, "SleeManagementMBeanProxy.getUsageMBean() has not thrown Exception: UnrecognizedSubsystemException");
         } catch (UnrecognizedSubsystemException e) {
            logSuccessfulCheck(1114525);
        } catch (Exception e) {
            return TCKTestResult.failed(1114525, "SleeManagementMBeanProxy.getUsageMBean() has thrown Exception: " + e.getClass().toString());
        }

        try {
            notrecognizedusageParams = sleeProxy.getUsageParameterSets(installedSubsystem);
            notrecognizedusageParams[0] = "Test1114500Test";
            ObjectName objectName = sleeProxy.getUsageMBean(installedSubsystem, notrecognizedusageParams[0]);
            return TCKTestResult.failed(1114526, "SleeManagementMBeanProxy.getUsageMBean() has not thrown Exception: UnrecognizedSubsystemException");
         } catch (UnrecognizedSubsystemException e) {
            logSuccessfulCheck(1114526);
        } catch (InvalidArgumentException e) {
            // Can be that this subsystem has no Usage
            if (!subsystemUsage) {
                logSuccessfulCheck(1114526);
            } else {
                return TCKTestResult.failed(1114526, "SleeManagementMBeanProxy.getUsageMBean() has thrown Exception: " + e.getClass().toString());
            }
        } catch (Exception e) {
            return TCKTestResult.failed(1114526, "SleeManagementMBeanProxy.getUsageMBean() has thrown Exception: " + e.getClass().toString());
        }

        if (!subsystemUsage) {
            try {
                ObjectName objectName = sleeProxy.getUsageMBean(installedSubsystem, usageParam);
                return TCKTestResult.failed(1114527, "SleeManagementMBeanProxy.getUsageMBean() has not thrown Exception: InvalidArgumentException");
            } catch (InvalidArgumentException e) {
                logSuccessfulCheck(1114527);
            } catch (Exception e) {
                return TCKTestResult.failed(1114527, "SleeManagementMBeanProxy.getUsageMBean() has thrown Exception: " + e.getClass().toString());
            }
        }

        try {
            ObjectName objectName = sleeProxy.getUsageNotificationManagerMBean(installedSubsystem);
            logSuccessfulCheck(1114530);
        } catch (NullPointerException e) {
            return TCKTestResult.failed(1114530, "The Installed Subsystems name(s) was null");
        } catch (UnrecognizedSubsystemException e) {
            return TCKTestResult.failed(1114530, "The Installed Subsystems name(s) was not recognised");
        } catch (InvalidArgumentException e) {
            // Can be that this subsystem has no Usage
            if (!subsystemUsage) {
                logSuccessfulCheck(1114530);
            } else {
                return TCKTestResult.failed(1114530, "The Installed Subsystems name(s) was not valid");
            }
        } catch (ManagementException e) {
            return TCKTestResult.failed(1114530, "The Installed Subsystems name(s) could not obtained due to a system-level failure");
        } catch (Exception e) {
            return TCKTestResult.failed(1114530, "SleeManagementMBeanProxy.getUsageNotificationManagerMBean() has thrown Exception: " + e.getClass().toString());
        }

        try {
            ObjectName objectName = sleeProxy.getUsageNotificationManagerMBean(nullSubsystem);
            return TCKTestResult.failed(1114531, "SleeManagementMBeanProxy.getUsageNotificationManagerMBean() has not thrown Exception: NullPointerException");
        } catch (NullPointerException e) {
            logSuccessfulCheck(1114531);
        } catch (Exception e) {
            return TCKTestResult.failed(1114531, "SleeManagementMBeanProxy.getUsageNotificationManagerMBean() has thrown Exception: " + e.getClass().toString());
        }

        try {
            ObjectName objectName = sleeProxy.getUsageNotificationManagerMBean(notrecognizedSubsystem);
            return TCKTestResult.failed(1114532, "SleeManagementMBeanProxy.getUsageNotificationManagerMBean() has not thrown Exception: UnrecognizedSubsystemException");
        } catch (UnrecognizedSubsystemException e) {
            logSuccessfulCheck(1114532);
        } catch (Exception e) {
            return TCKTestResult.failed(1114532, "SleeManagementMBeanProxy.getUsageNotificationManagerMBean() has thrown Exception: " + e.getClass().toString());
        }

        if (!subsystemUsage) {
            try {
                ObjectName objectName = sleeProxy.getUsageNotificationManagerMBean(installedSubsystem);
                return TCKTestResult.failed(1114533, "SleeManagementMBeanProxy.getUsageNotificationManagerMBean() has not thrown Exception: InvalidArgumentException");
            } catch (InvalidArgumentException e) {
                logSuccessfulCheck(1114533);
            } catch (Exception e) {
                return TCKTestResult.failed(1114533, "SleeManagementMBeanProxy.getUsageNotificationManagerMBean() has thrown Exception: " + e.getClass().toString());
            }
        }


        return TCKTestResult.passed();

    }

    /**
     * Do all the pre-run configuration of the test.
     */
    public void setUp() throws Exception {
        // Install the Deployable Units
        try {
            String duPath = utils().getTestParams().getProperty(SERVICE_DU_PATH_PARAM);
            utils().getLog().fine("Installing " + duPath);
            duID = utils().install(duPath);

            // Activate the DU
            utils().activateServices(duID, true);

            DeploymentMBeanProxy duProxy = utils().getDeploymentMBeanProxy();
            SbbID[] sbbs = duProxy.getSbbs();
            utils().getLog().fine("Installed SBB: " + sbbs[0]);

         } catch (Exception e) {
            getLog().warning(e);
            getLog().warning("Failed to install deployable unit.");
        }
     }

    /**
     * Clean up after the test.
     */
    public void tearDown() throws Exception {
        utils().getLog().fine("Disconnecting from resource");
        utils().getResourceInterface().clearActivities();
        utils().getLog().fine("Deactivating and uninstalling service");
        utils().deactivateAllServices();
        utils().uninstallAll();
    }

    private void logSuccessfulCheck(int assertionID) {
        utils().getLog().info("Check for assertion "+assertionID+" OK");
    }

    private DeployableUnitID duID;

}
