/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.profiles.profilespec;

import com.opencloud.sleetck.lib.AbstractSleeTCKTest;
import com.opencloud.sleetck.lib.TCKTestErrorException;
import com.opencloud.sleetck.lib.TCKTestResult;


public class Test1110046Test extends AbstractSleeTCKTest {

    private static final String NO_CMP_DU_PATH_PARAM= "noCMPDUPath";
    private static final String NO_PROF_MNGMT_DU_PATH_PARAM= "noMngmtDUPath";
    private static final String NO_PROF_DU_PATH_PARAM= "noProfDUPath";
    private static final String ALL_OK_DU_PATH_PARAM= "allOKDUPath";
    private static final String ABSTRACT_MISS_DU_PATH_PARAM= "abstractMissDUPath";

    /**
     * This test trys to deploy some profile specs which are invalid due to various
     * problems with the Profile Abstract class's inheritance signature or because
     * a profile abstract class was required as there is a Mngmt interface but is missing.
     * The test fails if any of the invalid profile specs is deployed successfully into the SLEE.
     */
    public TCKTestResult run() throws Exception {

        //Profile abstract class does not implement the spec's Profile CMP interface
        getLog().fine("Deploying profile spec.");
        try {
            setupService(NO_CMP_DU_PATH_PARAM);
            return TCKTestResult.failed(1110046,"Deployment of Profile spec with Profile abstract class not implementing CMP interface should fail but was successful.");
        }
        catch (TCKTestErrorException e) {
            getLog().fine("Caught expected exception when trying to deploy profile spec. Exception: "+e.getMessage());
        }

        //Profile abstract class does not implement the spec's Profile Management interface
        getLog().fine("Deploying profile spec.");
        try {
            setupService(NO_PROF_MNGMT_DU_PATH_PARAM);
            return TCKTestResult.failed(1110046,"Deployment of Profile spec with Profile abstract class not implementing Management interface should fail but was successful.");
        }
        catch (TCKTestErrorException e) {
            getLog().fine("Caught expected exception when trying to deploy profile spec. Exception: "+e.getMessage());
        }

        //Profile abstract class does not implement the SLEE's Profile interface
        getLog().fine("Deploying profile spec.");
        try {
            setupService(NO_PROF_DU_PATH_PARAM);
            return TCKTestResult.failed(1110046,"Deployment of Profile spec with Profile abstract class not implementing SLEE Profile interface should fail but was successful.");
        }
        catch (TCKTestErrorException e) {
            getLog().fine("Caught expected exception when trying to deploy profile spec. Exception: "+e.getMessage());
        }

        //No Profile abstract class defined though a Profile Management interface is present
        getLog().fine("Deploying profile spec.");
        try {
            setupService(ABSTRACT_MISS_DU_PATH_PARAM);
            return TCKTestResult.failed(1110199,"Deployment of Profile spec with no Profile abstract class defined though Profile Management interface is there should fail but was successful.");
        }
        catch (TCKTestErrorException e) {
            getLog().fine("Caught expected exception when trying to deploy profile spec. Exception: "+e.getMessage());
        }

        //Make sure that the correct Profile abstract class deploys fine.
        getLog().fine("Deploying profile spec.");
        try {
            setupService(ALL_OK_DU_PATH_PARAM);
            getLog().fine("Correct Profile abstract class deploys fine.");
        }
        catch (TCKTestErrorException e) {
            return TCKTestResult.failed(1110046,"Deployment of valid profile spec failed: "+e);
        }

        return TCKTestResult.passed();
    }

    public void setUp() throws Exception {

    }

    public void tearDown() throws Exception {

        super.tearDown();
    }
}
