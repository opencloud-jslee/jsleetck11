/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.profiles.profilespec;

public class Test1110225_2Test extends Test1110225BaseTest {

    private static final String SPEC_NAME = "Test1110225_2Profile";

    public String getProfileSpecName() {
        return SPEC_NAME;
    }
}
