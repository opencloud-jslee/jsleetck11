/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.profiles.profilespec;

import javax.management.ObjectName;
import javax.slee.profile.ProfileSpecificationID;

import com.opencloud.logging.Logable;
import com.opencloud.sleetck.lib.AbstractSleeTCKTest;
import com.opencloud.sleetck.lib.TCKTestErrorException;
import com.opencloud.sleetck.lib.TCKTestFailureException;
import com.opencloud.sleetck.lib.TCKTestResult;
import com.opencloud.sleetck.lib.infra.SleeTCKComponentConstants;
import com.opencloud.sleetck.lib.resource.TCKActivityID;
import com.opencloud.sleetck.lib.resource.events.TCKResourceEventX;
import com.opencloud.sleetck.lib.resource.testapi.TCKResourceTestInterface;
import com.opencloud.sleetck.lib.sbbutils.events2.SbbMessageAdapter;
import com.opencloud.sleetck.lib.testutils.FutureResult;
import com.opencloud.sleetck.lib.testutils.ProfileUtils;
import com.opencloud.sleetck.lib.testutils.jmx.ProfileMBeanProxy;
import com.opencloud.sleetck.lib.testutils.jmx.ProfileProvisioningMBeanProxy;


public class Test1110051Test extends AbstractSleeTCKTest {

    private static final String NO_PROF_TAB_IMPL_DU_PATH_PARAM = "noProfTabImplDUPath";
    private static final String ALL_OK_DU_PATH_PARAM= "allOKDUPath";
    private static final String DEFAULT_PROF_TABLE= "defaultProfTableDUPath";

    private static final String SPEC_NAME = "Test1110051_3Profile";
    private static final String SPEC_VERSION = "1.0";

    private static final String SEND_RESULT_EVENT_DU_PATH_PARAM = "resultEventDUPath";

    private static final int TEST_ID = 1110051;


    /**
     * This test trys to deploy various profile specs that are invalid due to interfaces not being implemented
     * in the custom ProfileTable interface.
     * The test will fail if one of the invalid specs is deployed into the SLEE successfully.
     * Furthermore an initial event is sent to an Sbb which then ensures that a default ProfileTable class is implemented by the SLEE.
     */
    public TCKTestResult run() throws Exception {
        result = new FutureResult(utils().getLog());

        //ProfileTable interface does not extend SLEE ProfileTable interface
        getLog().fine("Deploying profile spec.");
        try {
            setupService(NO_PROF_TAB_IMPL_DU_PATH_PARAM);
            throw new TCKTestFailureException(1110051,"Deployment of Profile spec with ProfileTable interface not extending SLEE ProfileTable interface should fail but was successful.");
        }
        catch (TCKTestErrorException e) {
            getLog().fine("Caught expected exception when trying to deploy profile spec. Exception: "+e.getMessage());
        }

        //Make sure that the spec with the correct ProfileTable interface deploys fine.
        getLog().fine("Deploying profile spec.");
        try {
            setupService(ALL_OK_DU_PATH_PARAM);
            getLog().fine("Profile spec with correct ProfileTable interface deploys fine.");
        }
        catch (TCKTestErrorException e) {
            throw new TCKTestFailureException(1110051,"Deployment of valid profile spec failed.",e);
        }
        utils().uninstallAll();

        setupService(SEND_RESULT_EVENT_DU_PATH_PARAM);

        //Deploy the profile spec that uses the standard/default SLEE defined ProfileTable (i.e. no custom profile table element).
        getLog().fine("Deploying profile spec.");
        try {
            setupService(DEFAULT_PROF_TABLE);
            getLog().fine("Profile spec with SLEE's default ProfileTable implementation deploys fine.");
        }
        catch (TCKTestErrorException e) {
            throw new TCKTestFailureException(1110052,"Deployment of valid profile spec failed.",e);
        }

        //Create a profile table
        ProfileProvisioningMBeanProxy profileProvisioning = profileUtils.getProfileProvisioningProxy();
        ProfileSpecificationID specID = new ProfileSpecificationID(SPEC_NAME, SleeTCKComponentConstants.TCK_VENDOR, SPEC_VERSION);
        profileProvisioning.createProfileTable(specID, Test1110051Sbb.PROFILE_TABLE_NAME);
        getLog().fine("Added profile table "+Test1110051Sbb.PROFILE_TABLE_NAME);

        //add a profile to the table
        ObjectName profile = profileProvisioning.createProfile(Test1110051Sbb.PROFILE_TABLE_NAME, Test1110051Sbb.PROFILE_NAME);
        ProfileMBeanProxy profileProxy = utils().getMBeanProxyFactory().createProfileMBeanProxy(profile);
        getLog().fine("Created profile "+Test1110051Sbb.PROFILE_NAME+" for profile table "+Test1110051Sbb.PROFILE_TABLE_NAME);

        //commit and close the Profile
        profileProxy.commitProfile();
        profileProxy.closeProfile();
        getLog().fine("Commit and close profile "+Test1110051Sbb.PROFILE_NAME);

        // send an initial event to the test
        TCKResourceTestInterface resource = utils().getResourceInterface();
        TCKActivityID activityID = resource.createActivity(getClass().getName());
        Object message = new String("None");
        getLog().fine("About to fire TCKResourceEventX.X1 event to the Sbb");
        resource.fireEvent(TCKResourceEventX.X1, message, activityID, null);

        //wait for test to set result to passed or failed
        return result.waitForResultOrFail(utils().getTestTimeout(), "Timeout waiting for test result", TEST_ID);
    }

    public void setPassed() {
        result.setPassed();
    }

    public void setFailed(int id, String msg) {
        result.setFailed(id, msg);
    }

    public void setError(Exception e) {
        result.setError(e);
    }

    public void setUp() throws Exception {

        profileUtils = new ProfileUtils(utils());

        setResourceListener(new SbbMessageAdapter() {
            public Logable getLog() {
                return utils().getLog();
            }
            public void onSetPassed(int id, String msg) { setPassed(); }
            public void onSetFailed(int id, String msg) { setFailed(id, msg); }
            public void onSetException(Exception e) { setError(e); }
        });
    }

    public void tearDown() throws Exception {

        try {
            profileUtils.removeProfileTable(Test1110051Sbb.PROFILE_TABLE_NAME);
        }
        catch (Exception e) {
            getLog().warning("Caught exception while trying to remove profile table:");
            getLog().warning(e);
        }

        super.tearDown();
    }


    private ProfileUtils profileUtils;
    private FutureResult result;
}
