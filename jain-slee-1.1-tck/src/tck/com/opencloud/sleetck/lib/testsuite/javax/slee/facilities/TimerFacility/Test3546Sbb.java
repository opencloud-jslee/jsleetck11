/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.javax.slee.facilities.TimerFacility;

import com.opencloud.sleetck.lib.resource.events.TCKResourceEventX;
import com.opencloud.sleetck.lib.sbbutils.BaseTCKSbb;
import com.opencloud.sleetck.lib.sbbutils.TCKSbbUtils;

import javax.naming.InitialContext;
import javax.slee.ActivityContextInterface;
import javax.slee.facilities.*;
import java.util.HashMap;

public abstract class Test3546Sbb extends BaseTCKSbb {

    private static final String JNDI_TIMERFACILITY_NAME = "java:comp/env/slee/facilities/timer";

    private static final long TIMEOUT = 500; // half a second
    private static final long PERIOD = TIMEOUT * 3;

    public void sbbRolledBack(javax.slee.RolledBackContext context) {
    }

    // Initial event
    public void onTCKResourceEventX1(TCKResourceEventX ev, ActivityContextInterface aci) {

        long currentTime = System.currentTimeMillis();

        try {
            TimerOptions options = new TimerOptions();
            options.setTimeout(TIMEOUT);
            options.setPreserveMissed(TimerPreserveMissed.ALL);

            TimerFacility facility = (TimerFacility) new InitialContext().lookup(JNDI_TIMERFACILITY_NAME);

            // Create periodic timer with three repetitions.
            setTimerID(facility.setTimer(aci, null, currentTime, PERIOD, 3, options));

        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }

    }

    public void onTimerEvent(TimerEvent event, ActivityContextInterface aci) {

        try {
            HashMap map = new HashMap();

            // Increment the counter
            setCount(getCount() + 1);

            TCKSbbUtils.createTrace(getSbbID(), Level.FINE, "In timer event repetition " + getCount(), null);

            long scheduledTime = event.getScheduledTime();

            switch (getCount()) {
                case 1: // First time event seen.
                    setLastTime(scheduledTime);
                    return;

                case 2: // Second time event seen.

                    if (getLastTime() != scheduledTime - PERIOD) {
                        // Timer incorrectly scheduled
                        map.put("Result", new Boolean(false));
                        map.put("Message", "Repeat timer incorrectly scheduled.");
                        TCKSbbUtils.getResourceInterface().sendSbbMessage(map);
                        return;
                    }

                    setLastTime(scheduledTime);
                    return;

                case 3: // Last time event should be seen.

                    if (getLastTime() != scheduledTime - PERIOD) {
                        // Timer incorrectly scheduled
                        map.put("Result", new Boolean(false));
                        map.put("Message", "Repeat timer incorrectly scheduled.");
                        TCKSbbUtils.getResourceInterface().sendSbbMessage(map);
                        return;
                    }

                    if (event.getRemainingRepetitions() != 0) {
                        map.put("Result", new Boolean(false));
                        map.put("Message", "Repeating timer has remaining repetitions when it should have none.");
                        TCKSbbUtils.getResourceInterface().sendSbbMessage(map);
                        return;
                    }

                    map.put("Result", new Boolean(true));
                    map.put("Message", "Ok");
                    TCKSbbUtils.getResourceInterface().sendSbbMessage(map);
                    return;

                default: // Invalid count value;
                    map.put("Result", new Boolean(false));
                    map.put("Message", "EventSeen counter in unexpected state");
                    TCKSbbUtils.getResourceInterface().sendSbbMessage(map);
                    return;

            }

        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }
    }

    public abstract void setTimerID(TimerID timerID);
    public abstract TimerID getTimerID();

    public abstract void setCount(int count);
    public abstract int getCount();

    public abstract void setLastTime(long time);
    public abstract long getLastTime();

}

