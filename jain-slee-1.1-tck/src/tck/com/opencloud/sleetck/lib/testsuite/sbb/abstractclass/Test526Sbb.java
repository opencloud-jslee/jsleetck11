/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.sbb.abstractclass;

import com.opencloud.sleetck.lib.resource.events.TCKResourceEventX;
import com.opencloud.sleetck.lib.sbbutils.BaseTCKSbb;
import com.opencloud.sleetck.lib.sbbutils.TCKSbbUtils;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.slee.ActivityContextInterface;
import java.util.HashMap;

public abstract class Test526Sbb extends BaseTCKSbb {

    // Initial event
    public void onTCKResourceEventX1(TCKResourceEventX event, ActivityContextInterface aci) {
        HashMap map = new HashMap();
        try {
            Context context = (Context) new InitialContext().lookup("java:comp/env");
            StringBuffer errorBuf = new StringBuffer();

            lookupEntry(context,"slee/resources/tck/activitycontextinterfacefactory",errorBuf);
            lookupEntry(context,"slee/resources/tck/resource",errorBuf);
            lookupEntry(context,"slee/resources/tck",errorBuf);

            if(errorBuf.length() > 0) {
                map.put("Result", new Boolean(false));
                map.put("Message", errorBuf.toString());
            } else {
                map.put("Result", new Boolean(true));
                map.put("Message", "Ok");
            }
            TCKSbbUtils.getResourceInterface().sendSbbMessage(map);
        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }
    }


    private void lookupEntry(Context context, String location, StringBuffer errorBuf) {
        try {
            Object result = context.lookup(location);
            if(result == null) errorBuf.append("lookup attempt for environment entry at "+location+" returned a null value");
        } catch (javax.naming.NamingException e) {
            errorBuf.append("Attempt to lookup environment entry at "+location+" resulted in a NamingException:"+e);
        }
    }

}
