/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.javax.slee.facilities.TimerFacility;

import javax.slee.ActivityEndEvent;
import javax.slee.ActivityContextInterface;
import javax.slee.SbbContext;
import javax.slee.Address;
import javax.slee.SbbLocalObject;
import javax.slee.facilities.Level;
import javax.slee.ChildRelation;
import javax.slee.CreateException;

import com.opencloud.sleetck.lib.sbbutils.BaseTCKSbb;
import com.opencloud.sleetck.lib.sbbutils.TCKSbbUtils;
import com.opencloud.sleetck.lib.sbbutils.TCKSbbConstants;
import com.opencloud.sleetck.lib.resource.events.TCKResourceEventX;
import com.opencloud.sleetck.lib.resource.sbbapi.TCKResourceSbbInterface;

import java.util.HashMap;
import java.util.Iterator;

import javax.slee.facilities.ActivityContextNamingFacility;
import javax.slee.nullactivity.NullActivity;
import javax.slee.nullactivity.NullActivityFactory;
import javax.slee.nullactivity.NullActivityContextInterfaceFactory;

import javax.slee.facilities.*;
import javax.naming.*;

public abstract class Test3562Sbb extends BaseTCKSbb {

    private static final String JNDI_TIMERFACILITY_NAME = "java:comp/env/slee/facilities/timer";

    private static final long TIMEOUT = 1000; // 1 second

    public void sbbRolledBack(javax.slee.RolledBackContext context) {
    }

    // Initial event
    public void onTCKResourceEventX1(TCKResourceEventX ev, ActivityContextInterface aci) {

        try {
            HashMap map = new HashMap();

            boolean passed = false;
            TimerOptions options = new TimerOptions();
            options.setTimeout(TIMEOUT);

            TimerFacility facility = (TimerFacility) new InitialContext().lookup(JNDI_TIMERFACILITY_NAME);

            // Create infinitely repeating timer.
            setTimerID(facility.setTimer(aci, null, System.currentTimeMillis(), TIMEOUT + 1, 0, options));

        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }

    }

    public void onTCKResourceEventX2(TCKResourceEventX ev, ActivityContextInterface aci) {
        try {
            HashMap map = new HashMap();
            TimerFacility facility = (TimerFacility) new InitialContext().lookup(JNDI_TIMERFACILITY_NAME);
            facility.cancelTimer(getTimerID());

            try {
                facility.cancelTimer(getTimerID());
            } catch (Exception e) {
                map.put("Result", new Boolean(false));
                map.put("Message", "TimerFacility.cancelTimer() threw exception the second time it was called on the same timer.");
                map.put("ID", new Integer(3562));
                TCKSbbUtils.getResourceInterface().sendSbbMessage(map);
                return;
            }

            getSbbContext().setRollbackOnly();


            map.put("Type", "Ready");
            TCKSbbUtils.getResourceInterface().sendSbbMessage(map);
        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }
    }

    public void onTimerEvent(TimerEvent event, ActivityContextInterface aci) {

        HashMap map = new HashMap();
        map.put("Result", new Boolean(true));
        map.put("Message", "Ok");
        try {
            TCKSbbUtils.getResourceInterface().sendSbbMessage(map);
        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }
    }

    public abstract void setTimerID(TimerID timerID);
    public abstract TimerID getTimerID();

}

