/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.resource.endpoint;

import javax.slee.ActivityContextInterface;
import javax.slee.ActivityEndEvent;

import com.opencloud.sleetck.lib.testsuite.resource.BaseResourceSbb;

public abstract class Test1115276Sbb extends BaseResourceSbb {

    private static final int ASSERTION_ID = 1115276;

    public void onActivityEndEvent(ActivityEndEvent event, ActivityContextInterface aci) {
        tracer.info("onActivityEnd() event handler called with event: " + event);        
        try {
            getSbbInterface().executeTestLogic(new Integer(ASSERTION_ID));
        } catch (Exception e) {
            tracer.severe("An error occured invoking test logic on the test Resource Adaptor", e);
        }
    }

}
