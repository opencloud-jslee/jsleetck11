/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.resource.endpoint;

import java.util.HashMap;

import javax.slee.ActivityContextInterface;

import com.opencloud.sleetck.lib.rautils.UOID;
import com.opencloud.sleetck.lib.testsuite.resource.BaseResourceSbb;
import com.opencloud.sleetck.lib.testsuite.resource.RAMethods;
import com.opencloud.sleetck.lib.testsuite.resource.SimpleEvent;

public abstract class Test1115232Sbb2 extends BaseResourceSbb {

    // Event handler
    public void onSimpleEvent(SimpleEvent event, ActivityContextInterface aci) {
        tracer.info("onSimpleEvent() event handler called with event: " + event);
        try {
            int sequenceID = event.getSequenceID();
            HashMap results = new HashMap();
            tracer.info("Checking activity state (in subsequent transaction).");
            if (aci.isEnding()) {
                tracer.info("Activity Context was ending.");
                results.put("result-sbb2", Boolean.TRUE);
            } else {
                tracer.severe("Error: Activity Context was not in ending state after call to endActivityTrasacted() (in an earlier transaction)");
                results.put("result-sbb2", Boolean.FALSE);
            }
            sendSbbMessage(sbbUID, sequenceID, RAMethods.endActivityTransacted, results);
        } catch (Exception e) {
            tracer.severe("Exception caught while trying to execute test logic in RA", e);
        }
    }

    private static final UOID sbbUID = UOID.createUOID();    
}
