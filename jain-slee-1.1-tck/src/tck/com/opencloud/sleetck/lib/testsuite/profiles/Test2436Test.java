/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.profiles;

import com.opencloud.sleetck.lib.SleeTCKTest;
import com.opencloud.sleetck.lib.SleeTCKTestUtils;
import com.opencloud.sleetck.lib.TCKTestErrorException;
import com.opencloud.sleetck.lib.TCKTestResult;
import com.opencloud.sleetck.lib.resource.TCKActivityID;
import com.opencloud.sleetck.lib.resource.TCKSbbMessage;
import com.opencloud.sleetck.lib.resource.testapi.TCKResourceListener;
import com.opencloud.sleetck.lib.testutils.BaseTCKResourceListener;
import com.opencloud.sleetck.lib.testutils.FutureResult;
import com.opencloud.sleetck.lib.testutils.jmx.ProfileProvisioningMBeanProxy;

import javax.slee.management.DeployableUnitID;
import javax.slee.management.DeploymentException;
import java.rmi.RemoteException;
import java.util.HashMap;

public class Test2436Test implements SleeTCKTest {

    private static final String SERVICE_DU_PATH_PARAM = "DUPath";
    private static final int TEST_ID = 2436;

    public void init(SleeTCKTestUtils utils) {
        this.utils = utils;
    }

    /**
     * Perform the actual test.
     */

    public TCKTestResult run() throws Exception {

        // Install the Deployable Units
        String duPath = utils.getTestParams().getProperty(SERVICE_DU_PATH_PARAM);
        try {
            utils.install(duPath);
        } catch (TCKTestErrorException e) {
            if (e.getEnclosedException().getClass().equals(DeploymentException.class))
                return TCKTestResult.passed();
        }

        return TCKTestResult.failed(TEST_ID, "Was able to install a profile specification with a CMP with an invalid name.");
    }

    /**
     * Do all the pre-run configuration of the test.
     */

    public void setUp() throws Exception {

        utils.getLog().fine("Connecting to resource");
        resourceListener = new TCKResourceListenerImpl();
        utils.getResourceInterface().setResourceListener(resourceListener);

        utils.getLog().fine("Installing and activating service");
    }

    /**
     * Clean up after the test.
     */

    public void tearDown() throws Exception {

        utils.getLog().fine("Disconnecting from resource");
        utils.getResourceInterface().clearActivities();
        utils.getResourceInterface().removeResourceListener();
        utils.getLog().fine("Deactivating and uninstalling service");
        utils.deactivateAllServices();
        utils.uninstallAll();
    }

    private class TCKResourceListenerImpl extends BaseTCKResourceListener {
        public synchronized void onSbbMessage(TCKSbbMessage message, TCKActivityID activity) throws RemoteException {
            utils.getLog().info("Received message from SBB: " + message.getMessage());

            HashMap map = (HashMap) message.getMessage();

            String type = (String) map.get("Type");
            if (type != null) {
                try {
                    profileProxy.removeProfile("Test2436ProfileTable", "Test2436Profile");
                } catch (Exception e) {
                    onException(e);
                }

            } else {
                Boolean passed = (Boolean) map.get("Result");
                String msgString = (String) map.get("Message");
                Integer id = (Integer) map.get("ID");
                
                if (passed.booleanValue() == true)
                    result.setPassed();
                else
                    result.setFailed(id.intValue(), msgString);
            }
        }

        public void onException(Exception e) throws RemoteException {
            utils.getLog().warning("Received exception from SBB.");
            utils.getLog().warning(e);
        result.setError(e);
        }

    }

    private SleeTCKTestUtils utils;
    private TCKResourceListener resourceListener;
    private FutureResult result;
    private ProfileProvisioningMBeanProxy profileProxy;
}
