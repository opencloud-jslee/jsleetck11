/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.facilities.alarmfacility.alarmlevel;

import java.rmi.RemoteException;
import java.util.Map;

import javax.management.Notification;
import javax.management.NotificationListener;
import javax.slee.facilities.AlarmLevel;
import javax.slee.management.AlarmNotification;
import javax.slee.management.DeployableUnitID;

import com.opencloud.sleetck.lib.AbstractSleeTCKTest;
import com.opencloud.sleetck.lib.DescriptionKeys;
import com.opencloud.sleetck.lib.TCKTestFailureException;
import com.opencloud.sleetck.lib.resource.TCKActivityID;
import com.opencloud.sleetck.lib.resource.TCKSbbMessage;
import com.opencloud.sleetck.lib.resource.events.TCKResourceEventX;
import com.opencloud.sleetck.lib.resource.testapi.TCKResourceListener;
import com.opencloud.sleetck.lib.resource.testapi.TCKResourceTestInterface;
import com.opencloud.sleetck.lib.testutils.Assert;
import com.opencloud.sleetck.lib.testutils.BaseTCKResourceListener;
import com.opencloud.sleetck.lib.testutils.FutureResult;
import com.opencloud.sleetck.lib.testutils.jmx.AlarmMBeanProxy;

/*
 * AssertionID (1113526): Test when CLEAR is specified in an alarm filter 
 * (see Section 14.14.4), only alarm notifications that indicate an alarm 
 * has been cleared will be emitted.
 */
public class Test1113526Test extends AbstractSleeTCKTest {

    /**
     * Perform the actual test.
     */
    public void run(FutureResult result) throws Exception {
        this.result = result;
        if (getTestName().equals("CLEAR"))
            doTest1113526Test();
    }

    public void doTest1113526Test() throws Exception {

        String activityName = "Test1113526Test";
        TCKResourceTestInterface resource = utils().getResourceInterface();
        TCKActivityID activityID = resource.createActivity(activityName);
        getLog().info("Firing event: " + testName);
        expectedAlarmNotifications = 0;
        receivedAlarmNotifications = 0;

        // kickoff the test
        resource.fireEvent(TCKResourceEventX.X1, testName, activityID, null);

        synchronized (this) {
            wait(utils().getTestTimeout());
        }

        if (expectedAlarmNotifications == receivedAlarmNotifications)
            result.setPassed();
        else
            result.setFailed(1113526, "Expected number of alarm messages not received, alarms were not delivered by "
                    + "the AlarmFacility (expected " + expectedAlarmNotifications + ", received "
                    + receivedAlarmNotifications + ")");

    }

    /**
     * Do all the pre-run configuration of the test.
     */

    public void setUp() throws Exception {

        getLog().fine("Connecting to resource");
        getLog().fine("Installing and activating service");

        resourceListener = new TCKResourceListenerImpl();
        setResourceListener(resourceListener);

        // Install the Deployable Units
        String duPath = utils().getTestParams().getProperty(DescriptionKeys.SERVICE_DU_PATH_PARAM);
        duID = utils().install(duPath);

        // Activate the DU
        utils().activateServices(duID, true);
        alarmMBeanProxy = utils().getAlarmMBeanProxy();
    }

    /**
     * Clean up after the test.
     */

    public void tearDown() throws Exception {

        String[] listActiveAlarms = alarmMBeanProxy.getAlarms();
        if (alarmMBeanProxy.getAlarms().length > 0)
            for (int i = 0; i < listActiveAlarms.length; i++)
                alarmMBeanProxy.clearAlarm(listActiveAlarms[i]);

        if (null != alarmMBeanProxy)
            alarmMBeanProxy.removeNotificationListener(listener);
        // cleanup
        super.tearDown();
    }

    public String getTestName() {
        String testName = "testName";
        return utils().getTestParams().getProperty(testName);
    }

    private class TCKResourceListenerImpl extends BaseTCKResourceListener {
        // TCKResourceListener methods

        public synchronized void onSbbMessage(TCKSbbMessage message, TCKActivityID calledActivity)
                throws RemoteException {

            Map sbbData = (Map) message.getMessage();
            String sbbTestName = "Test1113526Test";
            String sbbTestResult = (String) sbbData.get("result");
            String sbbTestMessage = (String) sbbData.get("message");
            int assertionID = ((Integer) sbbData.get("id")).intValue();
            getLog().info(
                    "Received message from SBB: testname=" + sbbTestName + ", result=" + sbbTestResult + ", message="
                            + sbbTestMessage + ", id=" + assertionID);
            try {
                Assert.assertEquals(assertionID, "Test " + sbbTestName + " failed.", "pass", sbbTestResult);
                result.setPassed();
            } catch (TCKTestFailureException ex) {
                result.setFailed(assertionID, sbbTestMessage);
            }
        }

        public void onException(Exception exception) throws RemoteException {
            getLog().warning("Received Exception from SBB or resource:");
            getLog().warning(exception);
            result.setError(exception);
        }
    }

    public class AlarmNotificationListenerImpl implements NotificationListener {
        public final void handleNotification(Notification notification, Object handback) {

            /*
             * 
             * public static final int LEVEL_CLEAR = 0; 
             * public static final int LEVEL_CRITICAL = 1; 
             * public static final int LEVEL_MAJOR = 2;
             * public static final int LEVEL_WARNING = 3; 
             * public static final int LEVEL_INDETERMINATE = 4; 
             * public static final int LEVEL_MINOR = 5;
             * 
             */

            if (notification instanceof AlarmNotification) {
                AlarmNotification alarmNotification = (AlarmNotification) notification;

                if (alarmNotification.getMessage().equals(Test1113526Sbb.ALARM_MESSAGE_CRITICAL))
                    receivedAlarmNotifications = receivedAlarmNotifications + AlarmLevel.LEVEL_CRITICAL;
                else if (alarmNotification.getMessage().equals(Test1113526Sbb.ALARM_MESSAGE_MAJOR))
                    receivedAlarmNotifications = receivedAlarmNotifications + AlarmLevel.LEVEL_MAJOR;
                else if (alarmNotification.getMessage().equals(Test1113526Sbb.ALARM_MESSAGE_WARNING))
                    receivedAlarmNotifications = receivedAlarmNotifications + AlarmLevel.LEVEL_WARNING;
                else if (alarmNotification.getMessage().equals(Test1113526Sbb.ALARM_MESSAGE_INDETERMINATE))
                    receivedAlarmNotifications = receivedAlarmNotifications + AlarmLevel.LEVEL_INDETERMINATE;
                else if (alarmNotification.getMessage().equals(Test1113526Sbb.ALARM_MESSAGE_MINOR))
                    receivedAlarmNotifications = receivedAlarmNotifications + AlarmLevel.LEVEL_MINOR;

                return;
            }

            getLog().info("Notification received: " + notification);
            return;
        }
    }

    private TCKResourceListener resourceListener;

    private NotificationListener listener;

    private FutureResult result;

    private DeployableUnitID duID;

    private String testName = "Test1113526";

    private int receivedAlarmNotifications;

    private int expectedAlarmNotifications;

    private AlarmMBeanProxy alarmMBeanProxy;

}