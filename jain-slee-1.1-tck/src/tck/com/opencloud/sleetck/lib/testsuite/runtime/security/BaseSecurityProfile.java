/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.runtime.security;

import java.awt.AWTPermission;
import java.io.FilePermission;
import java.io.SerializablePermission;
import java.lang.reflect.ReflectPermission;
import java.net.NetPermission;
import java.security.AccessControlException;
import java.security.AccessController;
import java.security.AllPermission;
import java.security.Permission;
import java.security.SecurityPermission;
import java.util.PropertyPermission;

import javax.slee.CreateException;
import javax.slee.facilities.Tracer;
import javax.slee.profile.Profile;
import javax.slee.profile.ProfileContext;
import javax.slee.profile.ProfileVerificationException;

import com.opencloud.sleetck.lib.TCKTestErrorException;
import com.opencloud.sleetck.lib.TCKTestFailureException;

public abstract class BaseSecurityProfile implements Profile {
    public void setProfileContext(ProfileContext context) {
        trace = context.getTracer("runtime.security");
    }
    public void unsetProfileContext() { }
    public void profileInitialize() { }
    public void profilePostCreate() throws CreateException { }
    public void profileActivate() { }
    public void profilePassivate() { }
    public void profileLoad() { }
    public void profileStore() { }
    public void profileRemove() { }
    public void profileVerify() throws ProfileVerificationException { }

    /**
     * Checks that the SLEE will grant this profile the given permission
     */
    protected void checkGrant(Permission permission, int assertionID) throws TCKTestErrorException, TCKTestFailureException {
        try {
            AccessController.checkPermission(permission);
            trace.info("The SLEE granted this profile permission " + permission + " as expected");
        } catch (AccessControlException ace) {
            trace.warning("The SLEE denied this Profile a permission which it was expected to grant: ", ace);
            throw new TCKTestFailureException(assertionID, "The SLEE denied this profile a permission which it was expected to grant: ", ace);
        }
    }

    /**
     * Checks that the SLEE will deny this profile the given permission
     */
    protected void checkDeny(Permission permission, int assertionID) throws TCKTestErrorException, TCKTestFailureException {
        try {
            AccessController.checkPermission(permission);
            trace.warning("The SLEE granted this profile a permission which it was expected to deny: ");
            throw new TCKTestFailureException(assertionID, "The SLEE granted this profile a permission which it was expected to deny: ");
        } catch (AccessControlException ace) {
            trace.info("The SLEE denied this profile permission " + permission + " as expected");
        }
    }

    /**
     * Checks the various permissions, with target names and actions introduced in Java 1.3 or lower
     */
    protected void checkPermissions(int assertionID) throws TCKTestErrorException, TCKTestFailureException {

        /* From the SLEE 1.1 spec:
        Permission name                     SLEE policy
        --------------------                ------------------
        java.security.AllPermission         deny
        java.awt.AWTPermission              deny
        java.io.FilePermission              deny
        java.net.NetPermission              deny
        java.util.PropertyPermission        grant “read”, “*”
                                            deny all other
        java.lang.reflect.ReflectPermission deny
        java.lang.RuntimePermission         deny
        java.lang.SecurityPermission        deny
        java.io.SerializablePermission      deny
        java.net.SocketPermission           deny
        */

        checkDeny(new AllPermission(),assertionID);

        checkDeny(new AWTPermission("*"),assertionID);
        checkDeny(new AWTPermission("accessClipboard"),assertionID);
        checkDeny(new AWTPermission("accessEventQueue"),assertionID);
        checkDeny(new AWTPermission("listenToAllAWTEvents"),assertionID);
        checkDeny(new AWTPermission("showWindowWithoutWarningBanner"),assertionID);
        checkDeny(new AWTPermission("readDisplayPixels"),assertionID);
        checkDeny(new AWTPermission("createRobot"),assertionID);

        checkDeny(new FilePermission("*","read"),assertionID);
        checkDeny(new FilePermission("*","write"),assertionID);
        checkDeny(new FilePermission("*","execute"),assertionID);
        checkDeny(new FilePermission("*","delete"),assertionID);

        checkDeny(new NetPermission("setDefaultAuthenticator"),assertionID);
        checkDeny(new NetPermission("requestPasswordAuthentication"),assertionID);
        checkDeny(new NetPermission("specifyStreamHandler"),assertionID);

        checkGrant(new PropertyPermission("*","read"),assertionID);
        checkDeny(new PropertyPermission("*","write"),assertionID);

        checkDeny(new ReflectPermission("suppressAccessChecks"),assertionID);

        // queuePrintJob no longer allowed
        checkDeny(new RuntimePermission("queuePrintJob"), assertionID);
        checkDeny(new RuntimePermission("createClassLoader"),assertionID);
        checkDeny(new RuntimePermission("getClassLoader"),assertionID);
        checkDeny(new RuntimePermission("setContextClassLoader"),assertionID);
        checkDeny(new RuntimePermission("setSecurityManager"),assertionID);
        checkDeny(new RuntimePermission("createSecurityManager"),assertionID);
        checkDeny(new RuntimePermission("exitVM"),assertionID);
        checkDeny(new RuntimePermission("shutdownHooks"),assertionID);
        checkDeny(new RuntimePermission("setFactory"),assertionID);        // the following two checks have been removed, as they would prohibit a typical
        // ejb implementation of SLEE from working
        //checkDeny(new SocketPermission("*", "connect"), 1142);
        //checkDeny(new SocketPermission("*", "resolve"), 1142);

        checkDeny(new RuntimePermission("setIO"),assertionID);
        checkDeny(new RuntimePermission("modifyThread"),assertionID);
        checkDeny(new RuntimePermission("stopThread"),assertionID);
        checkDeny(new RuntimePermission("modifyThreadGroup"),assertionID);
        checkDeny(new RuntimePermission("getProtectionDomain"),assertionID);
        checkDeny(new RuntimePermission("readFileDescriptor"),assertionID);
        checkDeny(new RuntimePermission("writeFileDescriptor"),assertionID);
        checkDeny(new RuntimePermission("loadLibrary.*"),assertionID);
        checkDeny(new RuntimePermission("accessClassInPackage.*"),assertionID);
        checkDeny(new RuntimePermission("defineClassInPackage.*"),assertionID);
        checkDeny(new RuntimePermission("accessDeclaredMembers"),assertionID);
        checkDeny(new RuntimePermission("*"),assertionID);

        checkDeny(new SecurityPermission("*"),assertionID);
        checkDeny(new SecurityPermission("createAccessControlContext"),assertionID);
        checkDeny(new SecurityPermission("getDomainCombiner"),assertionID);
        checkDeny(new SecurityPermission("getPolicy"),assertionID);
        checkDeny(new SecurityPermission("setPolicy"),assertionID);
        checkDeny(new SecurityPermission("getProperty.*"),assertionID);
        checkDeny(new SecurityPermission("setProperty.*"),assertionID);
        checkDeny(new SecurityPermission("insertProvider.*"),assertionID);
        checkDeny(new SecurityPermission("removeProvider.*"),assertionID);
        checkDeny(new SecurityPermission("setSystemScope"),assertionID);
        checkDeny(new SecurityPermission("setIdentityPublicKey"),assertionID);
        checkDeny(new SecurityPermission("setIdentityInfo"),assertionID);
        checkDeny(new SecurityPermission("addIdentityCertificate"),assertionID);
        checkDeny(new SecurityPermission("removeIdentityCertificate"),assertionID);
        checkDeny(new SecurityPermission("printIdentity"),assertionID);
        checkDeny(new SecurityPermission("clearProviderProperties.*"),assertionID);
        checkDeny(new SecurityPermission("putProviderProperty.*"),assertionID);
        checkDeny(new SecurityPermission("removeProviderProperty.*"),assertionID);
        checkDeny(new SecurityPermission("getSignerPrivateKey"),assertionID);
        checkDeny(new SecurityPermission("setSignerKeyPair"),assertionID);

        checkDeny(new SerializablePermission("enableSubclassImplementation"),assertionID);
        checkDeny(new SerializablePermission("enableSubstitution"),assertionID);

        // the following two checks have been removed, as they would prohibit a typical
        // ejb implementation of SLEE from working
        //checkDeny(new SocketPermission("*", "connect"), 1142);
        //checkDeny(new SocketPermission("*", "resolve"), 1142);

        // Test to make sure this test fails well:
        //checkGrant(new SocketPermission("*","listen"),1142);
    }

    protected Tracer trace;
}
