/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.sbb.abstractclass;

import com.opencloud.sleetck.lib.sbbutils.TCKSbbUtils;
import com.opencloud.sleetck.lib.resource.events.TCKResourceEventX;

import javax.slee.*;
import javax.slee.facilities.*;

import javax.naming.*;

/**
 * Test valid types of environment entries
 */
public abstract class EnvEntryTypesSbb extends SendResultsSbb {
    public void onTCKResourceEventX1(TCKResourceEventX event, ActivityContextInterface aci) {
        try {
            if (checkEnvEntry("string", String.class) &&
                checkEnvEntry("character", Character.class) &&
                checkEnvEntry("integer", Integer.class) &&
                checkEnvEntry("boolean", Boolean.class) &&
                checkEnvEntry("double", Double.class) &&
                checkEnvEntry("byte", Byte.class) &&
                checkEnvEntry("short", Short.class) &&
                checkEnvEntry("long", Long.class) &&
                checkEnvEntry("float", Float.class)
               ) {
                setResultPassed("environment entry types test passed");
            }
        }
        catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }
    }

    private boolean checkEnvEntry(String name, Class type) throws Exception {
        try {
            Context context = (Context)new InitialContext().lookup("java:comp/env");

            Object o = context.lookup(name);
            if (!o.getClass().equals(type)) {
                setResultFailed(497, "environment entry '" + name + "' expected to be of type " + type.getName() + ", but actual type was '" + o.getClass().getName());
                return false;
            }
        }
        catch (NameNotFoundException nnfe) {
            setResultFailed(497, "environment entry '" + name + "' not found in jndi environment: " + nnfe);
            return false;
        }

        return true;
    }
}

