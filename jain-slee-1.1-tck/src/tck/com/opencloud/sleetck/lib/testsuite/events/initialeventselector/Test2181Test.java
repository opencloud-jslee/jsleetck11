/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.events.initialeventselector;

import javax.slee.management.DeploymentException;
import com.opencloud.sleetck.lib.TCKTestErrorException;

import com.opencloud.sleetck.lib.AbstractSleeTCKTest;
import com.opencloud.sleetck.lib.TCKTestResult;
import com.opencloud.sleetck.lib.OperationTimedOutException;
import com.opencloud.sleetck.lib.testutils.QueuingResourceListener;
import com.opencloud.sleetck.lib.resource.TCKActivityID;
import com.opencloud.sleetck.lib.resource.events.TCKResourceEventX;

/**
 * Test assertion 2181: that the SLEE only invokes initial event selector methods on Sbb objects in the pooled state
 */
public class Test2181Test extends AbstractSleeTCKTest {
    private static final String SERVICE_DU_PATH_PARAM = "serviceDUPath";
    private static final String SERVICE_DU_PATH_PARAM_2 = "serviceDUPath2";
    private static final int TEST_ID = 2181;

    public TCKTestResult run() throws Exception {

        // Install the Deployable Units

        boolean passed = false;

        String duPath = utils().getTestParams().getProperty(SERVICE_DU_PATH_PARAM);
        try {
            utils().install(duPath);
        } catch (TCKTestErrorException e) {
            if (e.getEnclosedException().getClass().equals(DeploymentException.class))
                passed = true;
        }
        
        if (!passed)
            return TCKTestResult.failed(TEST_ID, "The SLEE allowed the installation of an SBB containing an initial-event-selector method whose name began with 'sbb'");

        passed = false;
        duPath = utils().getTestParams().getProperty(SERVICE_DU_PATH_PARAM_2);
        try {
            utils().install(duPath);
        } catch (TCKTestErrorException e) {
            if (e.getEnclosedException().getClass().equals(DeploymentException.class))
                passed = true;
        }

        if (!passed)
            return TCKTestResult.failed(TEST_ID, "The SLEE allowed the installation of an SBB containing an initial-event-selector method whose name began with 'ejb'");

        return TCKTestResult.passed();
    }

    public void setUp() throws Exception {
    }

    public void tearDown() throws Exception {
        utils().uninstallAll();
    }

}
