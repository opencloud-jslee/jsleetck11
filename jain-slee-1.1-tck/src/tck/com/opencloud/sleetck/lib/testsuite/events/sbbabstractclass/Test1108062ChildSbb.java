/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.events.sbbabstractclass;

import java.util.HashMap;

import javax.naming.InitialContext;
import javax.slee.ActivityContextInterface;
import javax.slee.Address;
import javax.slee.ServiceID;
import javax.slee.facilities.ActivityContextNamingFacility;
import javax.slee.facilities.Tracer;

import com.opencloud.sleetck.lib.sbbutils.BaseTCKSbb;
import com.opencloud.sleetck.lib.sbbutils.TCKSbbUtils;

/**
 * AssertionID(1108062): Test The SBB object must be in the Ready state, 
 * i.e. it must have an assigned SBB entity, when it invokes this method. 
 * Otherwise, this method throws a java.lang.IllegalStateException.
 */
public abstract class Test1108062ChildSbb extends BaseTCKSbb {

    public void sbbCreate() throws javax.slee.CreateException {
        try {
            tracer = getSbbContext().getTracer("com.test");
            ActivityContextNamingFacility acNaming = (ActivityContextNamingFacility) new InitialContext()
                    .lookup(ActivityContextNamingFacility.JNDI_NAME);
            ActivityContextInterface aci = acNaming.lookup("name");
            Test1108062Event testEvent = new Test1108062Event();

            ServiceID serviceID = getSbbContext().getService();
            tracer.info("The particular target ServiceID is: " + serviceID);

            boolean passed = false;
            try {
                fireTest1108062Event(testEvent, aci, null, serviceID);
            } catch (IllegalStateException ise) {
                tracer.info("got expected IllegalStateException when the SBB object is not in the Ready state.", null);
                passed = true;
            }

            if (!passed) {
                sendResultToTCK(1108062, "fireCustomEventName() didnot throw IllegalStateException.", "Test1108062Test", false);
            } else
                sendResultToTCK(1108062, "fireCustomEventName() did throw IllegalStateException.", "Test1108062Test", true);
            return;
        } catch (Exception e) {
            throw new RuntimeException(e.getMessage());
        }

    }

    public abstract void fireTest1108062Event(Test1108062Event event, ActivityContextInterface aci, Address address,
            ServiceID serviceID);

    private void sendResultToTCK(int failedAssertionID, String message, String testName, boolean result) throws Exception {
        HashMap sbbData = new HashMap();
        sbbData.put("testname", testName);
        sbbData.put("result", result ? "pass" : "fail");
        sbbData.put("message", message);
        sbbData.put("id", new Integer(failedAssertionID));
        TCKSbbUtils.getResourceInterface().sendSbbMessage(sbbData);
    }

    private Tracer tracer;
}
