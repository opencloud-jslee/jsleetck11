/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.activities.activitycontext;

import javax.slee.management.DeployableUnitID;

import com.opencloud.sleetck.lib.*;
import com.opencloud.sleetck.lib.testutils.*;
import com.opencloud.sleetck.lib.resource.TCKSbbMessage;
import com.opencloud.sleetck.lib.resource.testapi.TCKResourceListener;
import com.opencloud.sleetck.lib.resource.TCKActivityID;
import com.opencloud.sleetck.lib.resource.testapi.TCKResourceTestInterface;
import com.opencloud.sleetck.lib.resource.events.TCKResourceEventX;
import com.opencloud.sleetck.lib.resource.events.TCKResourceEventY;

import com.opencloud.logging.Logable;

import java.rmi.RemoteException;
import java.util.HashMap;

/**
 * Send event X1 to the Parent SBB.  This causes the parent to create a child SBB and two activities,
 * to which the child is attached.
 *
 * Send event X1 to the Parent SBB.  This causes the parent to terminate the child SBB.
 *
 * Send event Y1 on an Activity that the child was attached to.  The child should have an event handler
 * for this which will cause a method on the parent to be called that sets a variable.
 *
 * Send event Y2 to the Parent SBB.  The Parent SBB then checks the variable to make sure the child
 * hasn't set it to some non-default value.
 *
 * Test succeeds if value of variable does not change and no other errors occur.
 * Test fails if variable value changes or another error occurs.
 */

public class AutoDetachAllTest implements SleeTCKTest {

    private static final String SERVICE_DU_PATH_PARAM = "serviceDUPath";
    private static final int TEST_ID = 540;

    public void init(SleeTCKTestUtils utils) {
        this.utils = utils;
    }

    /**
     * Perform the actual test.
     */

    public TCKTestResult run() throws Exception {
        result = new FutureResult(utils.getLog());

        // Create an Activity object.
        resource = utils.getResourceInterface();
        activityName = "AutoDetachAllTest";
        activityID = resource.createActivity(activityName);

        // Fire TCKResourceEventX.X1 on this activity.
        utils.getLog().fine("Firing TCKResourceEventX.X1 on activity " + activityName);
        resource.fireEvent(TCKResourceEventX.X1, TCKResourceEventX.X1, activityID, null);

        // Fire TCKResourceEventX.X2 on this activity.
        utils.getLog().fine("Firing TCKResourceEventX.X2 on activity " + activityName);
        resource.fireEvent(TCKResourceEventX.X2, TCKResourceEventX.X2, activityID, null);
        
        // Now wait for the response from the SBB.
        return result.waitForResultOrFail(utils.getTestTimeout(), "No response from the SBB", 3018);
    }

    /**
     * Do all the pre-run configuration of the test.
     */

    public void setUp() throws Exception {
        utils.getLog().fine("Connecting to resource");
        resourceListener = new TCKResourceListenerImpl();
        utils.getResourceInterface().setResourceListener(resourceListener);
        utils.getLog().fine("Installing and activating service");

        // Install the Deployable Unit.
        String duPath = utils.getTestParams().getProperty(SERVICE_DU_PATH_PARAM);
        DeployableUnitID duID = utils.install(duPath);
        utils.activateServices(duID, true); // Activate the service
    }

    /**
     * Clean up after the test.
     */

    public void tearDown() throws Exception {
        utils.getLog().fine("Disconnecting from resource");
        utils.getResourceInterface().clearActivities();
        utils.getResourceInterface().removeResourceListener();
        utils.getLog().fine("Deactivating and uninstalling service");
        utils.deactivateAllServices();
        utils.uninstallAll();
    }

    private class TCKResourceListenerImpl extends BaseTCKResourceListener {
        public synchronized void onSbbMessage(TCKSbbMessage message, TCKActivityID calledActivity) throws RemoteException {
            utils.getLog().info("Received message from SBB");

            HashMap map = (HashMap) message.getMessage();

            Boolean passed = (Boolean) map.get("Result");
            String msgString = (String) map.get("Message");

            if (passed.booleanValue() == true)
                result.setPassed();
            else
                result.setFailed(TEST_ID, msgString);
        }

        public void onException(Exception e) throws RemoteException {
            utils.getLog().warning("Received exception from SBB");
            utils.getLog().warning(e);
            result.setError(e);
        }
    }

    private SleeTCKTestUtils utils;
    private TCKResourceListener resourceListener;
    private FutureResult result;
    private TCKActivityID activityID;
    private TCKResourceTestInterface resource;
    private String activityName;
}
