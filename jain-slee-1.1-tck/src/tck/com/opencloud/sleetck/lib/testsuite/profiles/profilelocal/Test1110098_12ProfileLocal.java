/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.profiles.profilelocal;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.rmi.RemoteException;
import java.sql.SQLException;

import javax.slee.profile.ProfileLocalObject;

/**
 * Business method defines throws clause (== java.rmi.RemoteException)
 */
public interface Test1110098_12ProfileLocal extends ProfileLocalObject {
    public void business(String exToThrow) throws RemoteException;
}
