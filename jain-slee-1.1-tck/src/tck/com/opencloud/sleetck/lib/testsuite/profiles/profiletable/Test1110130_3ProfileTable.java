/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.profiles.profiletable;

import java.util.Collection;
import javax.slee.profile.ProfileTable;
/**
 *
 * According to the SLEE1.1 spec the type and order of parameters in this interface
 * and the deployment descriptor must match, but the parameters' names can differ.
 */
public interface Test1110130_3ProfileTable extends ProfileTable {
    public Collection queryIsAnswerToLife();
    public Collection queryEqualsValue(String value);
    public Collection queryNotEqualsValue(String value);
    public Collection queryNotEqualsValues(String valueSomeName, int intValueSomeName);
}
