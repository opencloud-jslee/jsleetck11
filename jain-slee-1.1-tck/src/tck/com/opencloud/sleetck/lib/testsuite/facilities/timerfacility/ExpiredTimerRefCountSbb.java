/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.facilities.timerfacility;

import com.opencloud.sleetck.lib.resource.events.TCKResourceEventX;
import com.opencloud.sleetck.lib.sbbutils.BaseTCKSbb;
import com.opencloud.sleetck.lib.sbbutils.TCKSbbUtils;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.slee.ActivityContextInterface;
import javax.slee.ActivityEndEvent;
import javax.slee.InitialEventSelector;
import javax.slee.SbbContext;
import javax.slee.facilities.*;
import javax.slee.nullactivity.NullActivity;
import javax.slee.nullactivity.NullActivityContextInterfaceFactory;
import javax.slee.nullactivity.NullActivityFactory;
import java.util.HashMap;

/**
 * Test that an expired timer (where all its timer events have fired)
 * releases its references to the aci.
 */
public abstract class ExpiredTimerRefCountSbb extends BaseTCKSbb {

    public void setSbbContext(SbbContext context) {
        super.setSbbContext(context);
        try {
            Context myEnv = (Context) new InitialContext().lookup("java:comp/env");

            nullActivityFactory = (NullActivityFactory) myEnv.lookup("slee/nullactivity/factory");
            nullACIFactory = (NullActivityContextInterfaceFactory) myEnv.lookup("slee/nullactivity/activitycontextinterfacefactory");
            timerFacility = (TimerFacility) myEnv.lookup("slee/facilities/timer");
        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }
    }

    // test starts here...
    public void onTCKResourceEventX1(TCKResourceEventX event, ActivityContextInterface aci) {
        try {
            TCKSbbUtils.createTrace(getSbbID(),Level.INFO,"Received start message",null);
            setTestName((String)event.getMessage());

            // Create a null aci
            NullActivity nobody = nullActivityFactory.createNullActivity();
            ActivityContextInterface nobodyACI = nullACIFactory.getActivityContextInterface(nobody);

            TimerOptions preserveAll = new TimerOptions();
            preserveAll.setPreserveMissed(TimerPreserveMissed.ALL);

            // set a single shot timer - this assumes setTimer actually works.
            // use starttime of zero, will fire right away
            timerFacility.setTimer(nobodyACI, null, 0, preserveAll);

            // The timerfacility now has a reference to the null aci (assumed).
            // After the timer event fires, the timer facility should release its
            // reference (assertion 1186) and we should see the activity end event
            // for the null aci.

        } catch(Exception e) {
            TCKSbbUtils.handleException(e);
        }
    }

    public void onTimerEvent(TimerEvent event, ActivityContextInterface aci) {
        try {
            TCKSbbUtils.createTrace(getSbbID(),Level.INFO,"Received timer event",null);

            // detach from aci so there are no references to it.
            aci.detach(getSbbContext().getSbbLocalObject());

            // now wait for end event...

        } catch(Exception e) {
            TCKSbbUtils.handleException(e);
        }
    }

    public void onActivityEndEvent(ActivityEndEvent event, ActivityContextInterface aci) {
        try {
            if (aci.getActivity() instanceof NullActivity) {

                // This is the end event for our null activity. It should have ended implicitly
                // when its ref count dropped to zero after the timer fired above.
                TCKSbbUtils.createTrace(getSbbID(),Level.INFO,"Received NullActivity end event",null);

                // PASSED - we received the activity end event first
                setResultPassed("aci ref count decremented correctly when timer expired");

                // Note no failed case - implicitly fails if this end event is never received and the
                // test times out.

                // we are done
                sendResultToTCK();
            }

        } catch(Exception e) {
            TCKSbbUtils.handleException(e);
        }
    }

    // return a static string so that this SBB entity gets selected every time
    public InitialEventSelector getStaticConvergenceName(InitialEventSelector ies) {
        ies.setCustomName("foobar");
        return ies;
    }

    public abstract void setTestName(String testName);
    public abstract String getTestName();

    private void sendResultToTCK() throws Exception {
        HashMap sbbData = new HashMap();
        sbbData.put("testname", getTestName());
        sbbData.put("result", result?"pass":"fail");
        sbbData.put("message", message);
        sbbData.put("id", new Integer(failedAssertionID));
        TCKSbbUtils.getResourceInterface().sendSbbMessage(sbbData);
    }

    private void setResultPassed(String msg) throws Exception {
        result = true;
        message = "Success: " + msg;
        TCKSbbUtils.createTrace(getSbbID(), Level.INFO, message, null);
    }

    private NullActivityFactory nullActivityFactory;
    private NullActivityContextInterfaceFactory nullACIFactory;
    private TimerFacility timerFacility;

    private boolean result = false;
    private String message;
    private int failedAssertionID = -1;

}
