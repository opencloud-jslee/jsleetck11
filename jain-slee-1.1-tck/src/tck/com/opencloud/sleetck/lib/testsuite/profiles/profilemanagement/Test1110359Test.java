/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.profiles.profilemanagement;

import com.opencloud.sleetck.lib.AbstractSleeTCKTest;
import com.opencloud.sleetck.lib.TCKTestResult;


public class Test1110359Test extends AbstractSleeTCKTest {

    private static final String SERVICE_DU_PATH= "DUPath";

    private static final String SPEC_NAME = "Test1110359Profile";
    private static final String SPEC_VERSION = "1.0";

    private static final int TEST_ID = 1110359;

    /**
     * Ensure that CMP attributes can't be non-serializable
     */
    public TCKTestResult run() throws Exception {

        try {
            setupService(SERVICE_DU_PATH);
            return TCKTestResult.failed(TEST_ID, "DU should not have been deployable as some of the CMP " +
                    "fields are of a non-serializable and non-primitive type.");
        }
        catch (Exception e) {
            getLog().fine("Caught exception as expected: ");
            getLog().fine(e);
            return TCKTestResult.passed();
        }
    }


    public void setUp() throws Exception {


    }

    public void tearDown() throws Exception {


        super.tearDown();
    }

}
