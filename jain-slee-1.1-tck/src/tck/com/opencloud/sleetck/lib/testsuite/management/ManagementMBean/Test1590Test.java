/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.management.ManagementMBean;

import com.opencloud.sleetck.lib.AbstractSleeTCKTest;
import com.opencloud.sleetck.lib.TCKTestResult;
import com.opencloud.sleetck.lib.testutils.jmx.DeploymentMBeanProxy;

import javax.slee.management.DeployableUnitID;

public class Test1590Test extends AbstractSleeTCKTest {

    private static final String SERVICE_DU_PATH_PARAM = "serviceDUPath";
    private static final int TEST_ID = 1590;

    /**
     * Perform the actual test.
     */
    public TCKTestResult run() throws Exception {
        DeploymentMBeanProxy duProxy = utils().getDeploymentMBeanProxy();
        // Attempt to uninstall the service.
        try {
            duProxy.uninstall(duID);
        } catch (javax.slee.InvalidStateException e) {
            return TCKTestResult.passed();
        }
        return TCKTestResult.failed(TEST_ID, "Was allowed to uninstall a Service that was in the ACTIVE state.");
    }

    /**
     * Do all the pre-run configuration of the test.
     */
    public void setUp() throws Exception {
        // Install the Deployable Unit.
        String duPath = utils().getTestParams().getProperty(SERVICE_DU_PATH_PARAM);
        duID = utils().install(duPath);
        utils().activateServices(duID, true); // Activate services.
    }

    private DeployableUnitID duID;
}
