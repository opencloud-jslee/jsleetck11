/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.profiles.profilecmp;

import com.opencloud.sleetck.lib.AbstractSleeTCKTest;
import com.opencloud.sleetck.lib.TCKTestFailureException;
import com.opencloud.sleetck.lib.TCKTestResult;

public class Test1110627Test extends AbstractSleeTCKTest {

    private static final String CMPFIELD_NO_UPPERCASE_DU_PATH_PARAM= "CMPFieldNoUpperCaseDUPath";
    private static final String REF_NO_LOWERCASE_DU_PATH_PARAM= "RefNoLowerCaseDUPath";
    private static final String ALL_LEGAL_TYPES_DU_PATH_PARAM= "AllLegalTypesDUPath";
    private static final String NON_SERIALIZABLE_DU_PATH_PARAM= "NonSerializableDUPath";

    /**
     * This test trys to deploy various profile specs that are invalid due to violations
     * on restrictions regarding the ProfileCMP interface or references to CMP fields
     * in the profile specification deployment descriptor.
     */
    public TCKTestResult run() throws Exception {

        getLog().fine("Deploying profile specs...");
        //Spec 1: 982: CMP field does not start with Uppercase letter after the initial 'get/set'
        try {
            setupService(CMPFIELD_NO_UPPERCASE_DU_PATH_PARAM);
            return TCKTestResult.failed(982,"Deployment of Profile spec with CMP fields not starting with uppercase letter after 'get'/'set' prefix " +
                    "worked fine but should have failed.");
        }
        catch (Exception e) {
            getLog().fine("Caught expected exception when trying to deploy profile spec. Exception: "+e.getMessage());
        }

        //Spec 2: 1110627: When a Profile CMP field is referenced in a
        //Profile Specification deployment descriptor the name of the
        //field must be a valid Java identifier and must begin with a
        //lowercase letter, as determined by java.lang.Character.isLowerCase.
        try {
            setupService(REF_NO_LOWERCASE_DU_PATH_PARAM);
            return TCKTestResult.failed(1110627,"Deployment of Profile spec with CMP field reference in DD not starting with lowercase letter " +
                    "worked fine but should have failed.");
        }
        catch (Exception e) {
            getLog().fine("Caught expected exception when trying to deploy profile spec. Exception: "+e.getMessage());
        }

        //Spec 3: 984: The Java types of the Profile CMP fields are restricted to Java primitive
        //types and Java serializable types, and arrays of such types.
        try {
            setupService(ALL_LEGAL_TYPES_DU_PATH_PARAM);
            getLog().fine("Deploying spec with CMP fields of primitive types, primtive type wrappers, custom serializable type and arrays of all these types worked fine.");
        }
        catch (Exception e) {
            return TCKTestResult.failed(new TCKTestFailureException(984,"Unexpected failure when deploying spec with CMP fields of primitive types, " +
                    "primtive type wrappers, custom serializable type and arrays of all these types.", e));
        }

        try {
            setupService(NON_SERIALIZABLE_DU_PATH_PARAM);
            return TCKTestResult.failed(984,"Deployment of Profile spec with CMP field of non-serializable type " +
                  "worked fine but should have failed.");
        }
        catch (Exception e) {
            getLog().fine("Caught expected exception when trying to deploy profile spec. Exception: "+e.getMessage());
        }

        return TCKTestResult.passed();
    }


    public void setUp() throws Exception {

    }

    public void tearDown() throws Exception {
        super.tearDown();
    }

}
