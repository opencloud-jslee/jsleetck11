/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.events.convergencename.addressprofile;

import com.opencloud.sleetck.lib.TCKTestResult;
import com.opencloud.sleetck.lib.resource.TCKActivityID;
import com.opencloud.sleetck.lib.resource.events.TCKResourceEventX;
import com.opencloud.sleetck.lib.resource.testapi.TCKResourceTestInterface;
import com.opencloud.sleetck.lib.testsuite.events.convergencename.AbstractConvergenceNameTest;
import com.opencloud.sleetck.lib.testsuite.events.convergencename.InitialEventSelectorParameters;
import com.opencloud.sleetck.lib.testutils.ProfileUtils;
import com.opencloud.sleetck.lib.testutils.jmx.ProfileProvisioningMBeanProxy;

import javax.slee.Address;
import javax.slee.AddressPlan;

/**
 * Checks that if the Address Profile identifier is selected for inclusion in the convergence name
 * and the default address is not null, but a matching Address Profile is not found,
 * no convergence name is created for the SBB for the event and the event is not an initial event for the SBB.
 */
public class NoAddressProfileConvergenceNameTest extends AbstractConvergenceNameTest {

    // -- Constants -- //

    private static final String ADDRESS_PROFILE_TABLE = "tck.AddressAndProfileTestProfile";

    // ADDRESS_1 goes in no address profile
    private static final Address ADDRESS_1 = new Address(AddressPlan.IP, "1.0.0.1");

    public TCKTestResult run() throws Exception {
        // Configure IES params for ActivityContext,Address,AddressProfile
        //  (only effective if the test description configures a service that utilises an IES method).
        InitialEventSelectorParameters iesParams = new InitialEventSelectorParameters(true, true, true, false, false,
                                                                                      null, false, false, false, null);
        TCKResourceTestInterface resource = utils().getResourceInterface();

        String eventType = TCKResourceEventX.X1;
        TCKActivityID activityID = resource.createActivity(ACTIVITY_ID_PREFIX + (currentActivityIDSuffix++));

        // Send an event on a non-null address that doesn't match any profile (no SBB should be created for the event)
        sendEventAndWait(eventType, "1", activityID, ADDRESS_1, null, iesParams);

        return TCKTestResult.passed();
    }

    public void setUp() throws Exception {
        // setup the address profile table before installing the service
        profileUtils = new ProfileUtils(utils());
        profileProvisioningProxy = profileUtils.getProfileProvisioningProxy();
        profileUtils.createStandardAddressProfileTable(ADDRESS_PROFILE_TABLE);
        super.setUp();
    }

    public void tearDown() throws Exception {
        super.tearDown();
        if (profileUtils != null) {
            try {
                profileProvisioningProxy.removeProfileTable(ADDRESS_PROFILE_TABLE);
            } catch (Exception e) {
                utils().getLog().warning(e);
            }
        }
    }

    private int currentActivityIDSuffix = 0;
    private ProfileProvisioningMBeanProxy profileProvisioningProxy;
    private ProfileUtils profileUtils;

}
