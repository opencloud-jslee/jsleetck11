/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.sbb.abstractclass;

import com.opencloud.sleetck.lib.resource.events.TCKResourceEventX;
import com.opencloud.sleetck.lib.sbbutils.BaseTCKSbb;
import com.opencloud.sleetck.lib.sbbutils.TCKSbbUtils;

import javax.slee.ActivityContextInterface;
import javax.slee.ChildRelation;
import javax.slee.SbbLocalObject;

public abstract class Test375Sbb extends BaseTCKSbb {

    // Initial event
    public void onTCKResourceEventX1(TCKResourceEventX ev, ActivityContextInterface aci) {

        try {
            ChildRelation childRelation = getChildRelation();
            SbbLocalObject childSbbLocalObject = childRelation.create();
            childSbbLocalObject.remove();
        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }

    }

    // ChildRelation method for the Child SBB.
    public abstract ChildRelation getChildRelation();

}
