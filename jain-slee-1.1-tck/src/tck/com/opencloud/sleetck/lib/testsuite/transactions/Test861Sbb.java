/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.transactions;

import javax.slee.ActivityEndEvent;
import javax.slee.ActivityContextInterface;
import javax.slee.SbbContext;
import javax.slee.Address;
import javax.slee.SbbLocalObject;
import javax.slee.facilities.Level;
import javax.slee.ChildRelation;
import javax.slee.CreateException;

import com.opencloud.sleetck.lib.TCKTestErrorException;
import com.opencloud.sleetck.lib.sbbutils.BaseTCKSbb;
import com.opencloud.sleetck.lib.sbbutils.TCKSbbUtils;
import com.opencloud.sleetck.lib.sbbutils.TCKSbbConstants;
import com.opencloud.sleetck.lib.resource.events.TCKResourceEventX;
import com.opencloud.sleetck.lib.resource.events.TCKResourceEventY;
import com.opencloud.sleetck.lib.resource.sbbapi.TCKResourceSbbInterface;

import java.util.HashMap;
import java.util.Iterator;

public abstract class Test861Sbb extends BaseTCKSbb {

    public void sbbRolledBack(javax.slee.RolledBackContext context) {
    }

    public void onTCKResourceEventX1(TCKResourceEventX ev, ActivityContextInterface aci) {

        try {
            asSbbActivityContextInterface(aci).setNumber(42);

            // Mark TXN for rollback.
            getSbbContext().setRollbackOnly();
        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }

    }

    public void onTCKResourceEventX2(TCKResourceEventX ev, ActivityContextInterface aci) {
        try {
            HashMap map = new HashMap();

            switch (asSbbActivityContextInterface(aci).getNumber()) {
            case 0:
                map.put("Result", new Boolean(true));
                map.put("Message", "Ok");
                break;

            case 42:
                map.put("Result", new Boolean(false));
                map.put("Message", "Transaction was marked for rollback, but ACI attribute maintained its set value.");
                break;
                
            default:
                map.put("Result", new Boolean(false));
                map.put("Message", "Unexpected value in ACI attribute.");
                break;
            }

            TCKSbbUtils.getResourceInterface().sendSbbMessage(map);
        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }
    }

    public abstract Test861SbbActivityContextInterface asSbbActivityContextInterface(ActivityContextInterface aci);

}
