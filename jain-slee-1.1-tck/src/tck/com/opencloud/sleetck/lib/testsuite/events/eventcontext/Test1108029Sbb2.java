/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.events.eventcontext;

import javax.slee.ActivityContextInterface;
import javax.slee.InitialEventSelector;
import javax.slee.EventContext;
import javax.slee.facilities.Tracer;

import com.opencloud.sleetck.lib.sbbutils.BaseTCKSbb;
import com.opencloud.sleetck.lib.sbbutils.TCKSbbUtils;
import com.opencloud.sleetck.lib.resource.events.TCKResourceEventX;

import java.util.HashMap;

/**
 * AssertionID(1108029): Test The javax.slee.IllegalStateException is thrown 
 * if the event delivery has already been suspended or suspendDelivery is 
 * invoked on an Event Context object that is not the Event Context object 
 * provided by the SLEE to the event handler method invoked by the SLEE.
 */
public abstract class Test1108029Sbb2 extends BaseTCKSbb {
    public static final String TRACE_MESSAGE_TCKResourceEventX1 = "Test1108029Sbb2:I got TCKResourceEventX1 on ActivityA";
    
    public static final String TRACE_MESSAGE_TCKResourceEventX2 = "Test1108029Sbb2:I got TCKResourceEventX2 on ActivityB";
    
    public static final String TRACE_MESSAGE_TCKResourceEventX3 = "Test1108029Sbb2:I got TCKResourceEventX3 on ActivityB";

    public InitialEventSelector initialEventSelector(InitialEventSelector ies) {
        ies.setCustomName("test");
        return ies;
    }

    public void onTCKResourceEventX1(TCKResourceEventX event, ActivityContextInterface aci, EventContext context) {
        try {
            //send message to TCK: I got TCKResourceEventX1 on ActivityA
            tracer = getSbbContext().getTracer("com.test");
            tracer.info(TRACE_MESSAGE_TCKResourceEventX1);

            if (getPassedX2() && getPassedX3()) {
                //Perhaps never reach here.
                sendResultToTCK("Test1108029Test", false, "SBB2:onTCKResourceEventX1-ERROR: While the second variant suspends delivery of the event for a "
                        + "specific timeout period in milliseconds, the event should not be "
                        + "delivered to other eligible SBBs.", 1108029);
                return;
            }

        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }
    }
    
    public void onTCKResourceEventX2(TCKResourceEventX event, ActivityContextInterface aci, EventContext context) {
        try {
            // send message to TCK: I got TCKResourceEventX2 on ActivityB
            tracer = getSbbContext().getTracer("com.test");
            tracer.info(TRACE_MESSAGE_TCKResourceEventX2);
            
            setPassedX3(true);
        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }
    }
    
    public void onTCKResourceEventX3(TCKResourceEventX event, ActivityContextInterface aci, EventContext context) {
        try {
            // send message to TCK: I got TCKResourceEventX2 on ActivityB
            tracer = getSbbContext().getTracer("com.test");
            tracer.info(TRACE_MESSAGE_TCKResourceEventX3);
            
            setPassedX3(true);
        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }
    }

    private void sendResultToTCK(String testName, boolean result, String message, int failedAssertionID) throws Exception {
        HashMap sbbData = new HashMap();
        sbbData.put("testname", testName);
        sbbData.put("result", result ? "pass" : "fail");
        sbbData.put("message", message);
        sbbData.put("id", new Integer(failedAssertionID));
        TCKSbbUtils.getResourceInterface().sendSbbMessage(sbbData);
    }

    private Tracer tracer = null;
    
    public abstract void setPassedX2(boolean passed);
    public abstract boolean getPassedX2();
    
    public abstract void setPassedX3(boolean passed);
    public abstract boolean getPassedX3();
}
