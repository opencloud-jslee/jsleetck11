/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.events.eventrouting;

import java.util.HashMap;

import javax.slee.ActivityContextInterface;
import javax.slee.Address;
import javax.slee.AddressPlan;
import javax.slee.EventContext;
import javax.slee.facilities.Tracer;

import com.opencloud.sleetck.lib.resource.events.TCKResourceEventX;
import com.opencloud.sleetck.lib.sbbutils.BaseTCKSbb;
import com.opencloud.sleetck.lib.sbbutils.TCKSbbUtils;

/**
 * AssertionID(1108081): Test If the selected address is not null and an Address Profile 
 * is found for this address, then the value of this variable is the name of the Address 
 * Profile.
 * 
 * AssertionID(1108082): Test If the selected address is not null and an Address Profile 
 * cannot be found for the address, then the SLEE does not construct a convergence name 
 * at all for the event and the event is not an initial event for the Service.
 *
 */
public abstract class Test1108081Sbb extends BaseTCKSbb {
    // Initial event method.
    public void onTCKResourceEventX1(TCKResourceEventX event, ActivityContextInterface aci, EventContext context) {
        try {
            tracer = getSbbContext().getTracer("com.test");
            Address address = context.getAddress();
            tracer.info("Sbb: Received a TCK event " + event + " on " + address);
            setTestName((String) event.getMessage());

            if (getTestName().equals("ADDRESSFOUND")) {
                if (!address.equals(ADDRESS_1) && !address.equals(ADDRESS_2)) {
                    sendResultToTCK(1108081, "The Address returned by EventContext didn't match the selected"
                            + "address passed to the event router from the test harness side.",
                            "Test1108081Test", false);
                    return;
                }
            }
            else if (getTestName().equals("ADDRESSNOTFOUND")) {}
            else if (getTestName().equals("NULLADDRESS")) {}
            else if (getTestName().equals("ADDRESSFOUNDANDNOTFOUND")) {}
            else {
                tracer.severe("Unexpected test name encountered during X1 event handler: " + getTestName());
                return;
            }
            
            sendResultToTCK(1108081, "Received a TCK event, sending it back to the TestHarness side.", 
                    "Test1108081Test", true);

        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }
    }

    private void sendResultToTCK(int failedAssertionID, String message, String testName, boolean result) throws Exception {
        HashMap sbbData = new HashMap();
        sbbData.put("testname", testName);
        sbbData.put("result", result ? "pass" : "fail");
        sbbData.put("message", message);
        sbbData.put("id", new Integer(failedAssertionID));
        TCKSbbUtils.getResourceInterface().sendSbbMessage(sbbData);
    }

    public abstract void setTestName(String testName);
    public abstract String getTestName();

    private Tracer tracer;

    public static final Address ADDRESS_1 = new Address(javax.slee.AddressPlan.IP, "1.0.0.1");

    public static final Address ADDRESS_2 = new Address(javax.slee.AddressPlan.IP, "1.0.0.2");
    
    public static final Address ADDRESS_3 = new Address(AddressPlan.IP, "101.210.30.3");

    public static final Address ADDRESS_4 = new Address(AddressPlan.IP, "101.210.30.4");
}
