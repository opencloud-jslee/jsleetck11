/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.facilities.timerfacility;

import com.opencloud.sleetck.lib.*;
import com.opencloud.sleetck.lib.infra.*;
import com.opencloud.sleetck.lib.testutils.jmx.MBeanFacade;
import com.opencloud.sleetck.lib.testutils.ExceptionsUtil;
import com.opencloud.sleetck.lib.testutils.BaseTCKResourceListener;
import com.opencloud.sleetck.lib.resource.TCKActivityID;
import com.opencloud.sleetck.lib.resource.testapi.TCKResourceTestInterface;
import com.opencloud.sleetck.lib.resource.testapi.TCKResourceListener;
import com.opencloud.sleetck.lib.resource.events.TCKResourceEventX;
import com.opencloud.sleetck.lib.resource.events.TCKResourceEventY;
import com.opencloud.sleetck.lib.testutils.*;
import com.opencloud.sleetck.lib.testutils.jmx.*;
import com.opencloud.sleetck.lib.resource.TCKSbbMessage;
import java.util.Map;
import java.util.Properties;
import java.rmi.RemoteException;
import javax.management.ObjectName;
import com.opencloud.logging.Logable;
import javax.slee.management.DeployableUnitID;
import javax.slee.profile.ProfileSpecificationID;

/**
 * Tests methods of the TimerFacility interface.<p>
 * Use the <code>testName</code> parameter in test description files to
 * test different TimerFacility features.<p>
 * Currently supported values for <code>testname</code> are:
 * <li><code>jndi</code> - tests that an SBB can find the Timer Facility
 * in its JNDI component environment.
 * <li><code>exception</code> - tests that correct exceptions are thrown when
 * TimerFacility methods called with improper arguments.
 */
public class TimerFacilityTest extends AbstractSleeTCKTest {

    private static final String PROFILE2_TABLE_NAME = "SetTimerAddressSbb2ProfileTable";
    private static final String PROFILE2_SPEC_NAME = "SetTimerAddressSbb2Profile";
    private static final String PROFILE2_NAME = "sbb2AddressProfile";
    private static final String PROFILE3_TABLE_NAME = "SetTimerAddressSbb3ProfileTable";
    private static final String PROFILE3_SPEC_NAME = "SetTimerAddressSbb3Profile";
    private static final String PROFILE3_NAME = "sbb3AddressProfile";

    public void run(FutureResult result) throws Exception {
        this.result = result;
        String activityName = "TimerFacilityTest";
        TCKResourceTestInterface resource = utils().getResourceInterface();
        TCKActivityID activityID = resource.createActivity(activityName);
        getLog().info("Firing event: " + testName );

        // kickoff the test
        resource.fireEvent(TCKResourceEventX.X1, testName, activityID, null);

        if ((testName.equals("cancelTimer")) || (testName.startsWith("TimerPreserveMissed"))) {
            // wait for a bit, then end the timer test.
            // a test timer should have been cancelled after 10s.
            Thread.sleep(10000);
            resource.fireEvent(TCKResourceEventY.Y1, testName, activityID, null);
        }

    }

    public void setUp() throws Exception {
        getLog().fine("Connecting to resource");
        resourceListener = new TCKResourceListenerImpl();
        setResourceListener(resourceListener);
        this.testName = utils().getTestParams().getProperty("testName");
        ProfileUtils profileUtils = new ProfileUtils(utils());

        if (testName.equals("setTimerRefCount1") || testName.equals("setTimerRefCount2")) {
            // deploy custom events
            String eventDUPath = utils().getTestParams().getProperty("eventDUPath");
            utils().install(eventDUPath);
        }
        else if (testName.equals("setTimerAddress-single") || testName.equals("setTimerAddress-periodic")) {
            // deploy additional services
            String service2DUPath = utils().getTestParams().getProperty("service2DUPath");
            DeployableUnitID du2ID = utils().install(service2DUPath);
            String service3DUPath = utils().getTestParams().getProperty("service3DUPath");
            DeployableUnitID du3ID = utils().install(service3DUPath);

            // create address profiles
            profileProvisioningMBean = profileUtils.getProfileProvisioningProxy();

            ProfileSpecificationID profileSpecID = new ComponentIDLookup(utils()).lookupProfileSpecificationID(PROFILE2_SPEC_NAME, SleeTCKComponentConstants.TCK_VENDOR,"1.0");
            getLog().info("creating profile table " + PROFILE2_TABLE_NAME);
            profileProvisioningMBean.createProfileTable(profileSpecID, PROFILE2_TABLE_NAME);
            getLog().info("creating profile " + PROFILE2_TABLE_NAME + "/" + PROFILE2_NAME);
            javax.management.ObjectName profile = profileProvisioningMBean.createProfile(PROFILE2_TABLE_NAME, PROFILE2_NAME);
        ProfileMBeanProxy profProxy = utils().getMBeanProxyFactory().createProfileMBeanProxy(profile);
        profProxy.commitProfile();


            profileSpecID = new ComponentIDLookup(utils()).lookupProfileSpecificationID(PROFILE3_SPEC_NAME, SleeTCKComponentConstants.TCK_VENDOR,"1.0");
            getLog().info("creating profile table " + PROFILE3_TABLE_NAME);
            profileProvisioningMBean.createProfileTable(profileSpecID, PROFILE3_TABLE_NAME);
            getLog().info("creating profile " + PROFILE3_TABLE_NAME + "/" + PROFILE3_NAME);
            profile = profileProvisioningMBean.createProfile(PROFILE3_TABLE_NAME, PROFILE3_NAME);
        profProxy = utils().getMBeanProxyFactory().createProfileMBeanProxy(profile);
        profProxy.commitProfile();


            // activate services
            utils().activateServices(du2ID, true);
            utils().activateServices(du3ID, true);
        }

        setupService(DescriptionKeys.SERVICE_DU_PATH_PARAM, true);

    }

    public void tearDown() throws Exception {

        try {
            // uninstall profiles & tables
            if (testName.equals("setTimerAddress-single") || testName.equals("setTimerAddress-periodic")) {
                if (profileProvisioningMBean != null) {
                    getLog().info("removing profile " + PROFILE2_TABLE_NAME + "/" + PROFILE2_NAME);
                    profileProvisioningMBean.removeProfile(PROFILE2_TABLE_NAME, PROFILE2_NAME);
                    getLog().info("removing profile table " + PROFILE2_TABLE_NAME);
                    profileProvisioningMBean.removeProfileTable(PROFILE2_TABLE_NAME);

                    getLog().info("removing profile " + PROFILE3_TABLE_NAME + "/" + PROFILE3_NAME);
                    profileProvisioningMBean.removeProfile(PROFILE3_TABLE_NAME, PROFILE3_NAME);
                    getLog().info("removing profile table " + PROFILE3_TABLE_NAME);
                    profileProvisioningMBean.removeProfileTable(PROFILE3_TABLE_NAME);
                }
            }
        } catch (Exception e) {
            getLog().error("Exception occurred while trying to remove profile tables.");
            getLog().error(e);
        }
        // cleanup
        super.tearDown();
    }


    // Private classes

    private class TCKResourceListenerImpl extends BaseTCKResourceListener {

        // TCKResourceListener methods

        public synchronized void onSbbMessage(TCKSbbMessage message, TCKActivityID calledActivity) throws RemoteException {
            Map sbbData = (Map)message.getMessage();
            String sbbTestName = (String)sbbData.get("testname");
            String sbbTestResult = (String)sbbData.get("result");
            String sbbTestMessage = (String)sbbData.get("message");
            int assertionID = ((Integer)sbbData.get("id")).intValue();
            getLog().info("Received message from SBB: testname="+ sbbTestName+", result="+sbbTestResult+", message="+sbbTestMessage+", id="+assertionID);
            try {
                if(sbbTestName.equals(testName)) {

                    Assert.assertEquals(assertionID,"Test " + testName + " failed.", "pass", sbbTestResult);
                    result.setPassed();

                } else result.setError("Invalid response sent by SBB: "+sbbTestName);
            } catch (TCKTestFailureException ex) {
                result.setFailed(ex);
            }
        }

        public void onException(Exception exception) throws RemoteException {
            getLog().warning("Received Exception from SBB or resource:");
            getLog().warning(exception);
            result.setError(exception);
        }

    }

    // Private state

    private TCKResourceListener resourceListener;
    private FutureResult result;
    private String testName;
    private ProfileProvisioningMBeanProxy profileProvisioningMBean;

}
