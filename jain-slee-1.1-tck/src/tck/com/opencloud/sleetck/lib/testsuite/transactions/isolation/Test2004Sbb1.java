/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.transactions.isolation;

import com.opencloud.sleetck.lib.resource.events.TCKResourceEventX;
import com.opencloud.sleetck.lib.resource.events.TCKResourceEventY;
import com.opencloud.sleetck.lib.sbbutils.TCKSbbUtils;
import com.opencloud.sleetck.lib.TCKTestErrorException;

import javax.naming.Context;
import javax.slee.ActivityContextInterface;
import javax.slee.facilities.ActivityContextNamingFacility;
import javax.slee.facilities.Level;
import javax.slee.nullactivity.NullActivity;
import javax.slee.nullactivity.NullActivityContextInterfaceFactory;
import javax.slee.nullactivity.NullActivityFactory;

public abstract class Test2004Sbb1 extends Test2004BaseSbb {

    /**
     * Creates a NullActivity and binds its activity context to a name (TCKResourceEventX1's event type name).
     */
    public void onTCKResourceEventX1(TCKResourceEventX event, ActivityContextInterface aci) {
        try {
            createTraceSafe(Level.INFO,"Test2004Sbb1.onTCKResourceEventX1(): Transaction ID: "+getTransactionId());
            createTraceSafe(Level.INFO,"Test2004Sbb1.onTCKResourceEventX1(): Creating null activity");

            // create a NullActivity
            Context env = TCKSbbUtils.getSbbEnvironment();
            NullActivityFactory naf = (NullActivityFactory) env.lookup("slee/nullactivity/factory");
            NullActivity activity = naf.createNullActivity();

            // gets its NullActivityContext
            NullActivityContextInterfaceFactory nif =
                    (NullActivityContextInterfaceFactory) env.lookup("slee/nullactivity/activitycontextinterfacefactory");
            ActivityContextInterface nullActivityACI = nif.getActivityContextInterface(activity);

            // bind the ActivityContextInterface at three locations
            ActivityContextNamingFacility acNaming = getActivityContextNamingFacility();
            acNaming.bind(nullActivityACI, INITIAL_ACI_NAME_1);
            acNaming.bind(nullActivityACI, INITIAL_ACI_NAME_2);
            acNaming.bind(nullActivityACI, INITIAL_ACI_NAME_3);

            // send an ACK to the test
            sendResponse(TCKResourceEventX.X1, null);
        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }
    }

    /**
     * Looks up the bindings at INITIAL_ACI_NAME_1 and INITIAL_ACI_NAME_2, then calls the test to block
     * (passing an array of two Boolean flags indicating whether the bindings were found),
     * then tries to lookup binding set by Y1 (in a concurrent transaction),
     * and INITIAL_ACI_NAME_2 (which the Y1 event handler should have tried to unbind),
     * then passes a response to the test.
     * The response is an array of type Object with two elements, one for each lookup attempt.
     * For each lookup attempt, the response is Boolean.TRUE (if the lookup returned a non-null ACI),
     * or the Exception caught while performing the lookup.
     */
    public void onTCKResourceEventX2(TCKResourceEventX event, ActivityContextInterface aci) {
        try {
            createTraceSafe(Level.INFO,"Test2004Sbb1.onTCKResourceEventX2(): Transaction ID: "+getTransactionId());

            // access the ACI at INITIAL_ACI_NAME_1 and INITIAL_ACI_NAME_2
            ActivityContextNamingFacility acNaming = getActivityContextNamingFacility();
            Boolean isAtBinding1 = Boolean.valueOf(acNaming.lookup(INITIAL_ACI_NAME_1) != null);
            Boolean isAtBinding2 = Boolean.valueOf(acNaming.lookup(INITIAL_ACI_NAME_2) != null);

            createTraceSafe(Level.INFO,"Test2004Sbb1.onTCKResourceEventX2(): calling test to block");
            sendResponse(TCKResourceEventX.X2, new Boolean[]{isAtBinding1,isAtBinding2});

            createTraceSafe(Level.INFO,"Test2004Sbb1.onTCKResourceEventX2(): finished blocking. looking up bindings...");

            String[] names = {TCKResourceEventY.Y1,INITIAL_ACI_NAME_2};
            Object[] responses = new Object[names.length];
            for (int i = 0; i < names.length; i++) {
                String name = names[i];
                try {
                    responses[i] = Boolean.valueOf(acNaming.lookup(names[i]) != null);
                    createTraceSafe(Level.INFO,"Test2004Sbb1.onTCKResourceEventX2(): was binding found at "+name+"?:"+responses[i]);
                } catch (Exception aciEx) {
                    responses[i] = aciEx;
                    createTraceSafe(Level.INFO,"Test2004Sbb1.onTCKResourceEventX2(): Exception received while trying to "+
                            "access ACI at "+names[i]);
                }
            }
            sendResponse(TCKResourceEventX.X2, responses);
        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }
    }

    /**
     * Looks up the bindings at INITIAL_ACI_NAME_1 and INITIAL_ACI_NAME_3, then
     * binds the NullActivity's ACI at TCKResourceEventX3's event type name, then
     * removes the binding at INITIAL_ACI_NAME_3, then calls the test to block,
     * passing an array of two Boolean flags indicating whether the
     * bindings at INITIAL_ACI_NAME_1 and INITIAL_ACI_NAME_3 were found.
     */
    public void onTCKResourceEventX3(TCKResourceEventX event, ActivityContextInterface aci) {
        try {
            createTraceSafe(Level.INFO,"Test2004Sbb1.onTCKResourceEventX3(): Transaction ID: "+getTransactionId());
            ActivityContextNamingFacility acNaming = getActivityContextNamingFacility();
            // lookup the bindings at INITIAL_ACI_NAME_1 and INITIAL_ACI_NAME_3
            createTraceSafe(Level.INFO,"Test2004Sbb1.onTCKResourceEventX3(): looking up expected ACI bindings");
            ActivityContextInterface nullActivityACI = acNaming.lookup(INITIAL_ACI_NAME_1);
            if(nullActivityACI == null) throw new TCKTestErrorException("ActivityContextInterface binding not found at "+
                    INITIAL_ACI_NAME_1+". This test SBB needs to access this binding to continue. Aborting the test...");
            Boolean isAtBinding1 = Boolean.TRUE;
            Boolean isAtBinding3 = Boolean.valueOf(acNaming.lookup(INITIAL_ACI_NAME_3) != null);

            String newLocation = TCKResourceEventX.X3;
            createTraceSafe(Level.INFO,"Test2004Sbb1.onTCKResourceEventX3(): binding value at: "+newLocation);
            acNaming.bind(nullActivityACI,newLocation);
            createTraceSafe(Level.INFO,"Test2004Sbb1.onTCKResourceEventX3(): removing binding at: "+INITIAL_ACI_NAME_3);
            acNaming.unbind(INITIAL_ACI_NAME_3);
            createTraceSafe(Level.INFO,"Test2004Sbb1.onTCKResourceEventX3(): calling test to block");
            sendResponse(TCKResourceEventX.X3, new Boolean[]{isAtBinding1,isAtBinding3});
            createTraceSafe(Level.INFO,"Test2004Sbb1.onTCKResourceEventX3(): finished blocking");
        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }
    }

    /**
     * Performs clean up: unbinds the ActivityContextInterfaces from their bindings
     */
    public void onTCKResourceEventY3(TCKResourceEventY event, ActivityContextInterface aci) {
        try {
            createTraceSafe(Level.INFO,"Test2004Sbb1.onTCKResourceEventY3(): Transaction ID: "+getTransactionId());
            createTraceSafe(Level.INFO,"Test2004Sbb1.onTCKResourceEventY3(): removing null activity bindings");
            String[] names = {INITIAL_ACI_NAME_1,INITIAL_ACI_NAME_2,INITIAL_ACI_NAME_3,
                              TCKResourceEventY.Y1,TCKResourceEventX.X3};
            ActivityContextNamingFacility acNaming = getActivityContextNamingFacility();
            boolean activityEnded = false;
            for (int i = 0; i < names.length; i++) {
                String name = names[i];
                ActivityContextInterface genericACI = null;
                try {
                    if((genericACI=acNaming.lookup(name)) != null) {
                        if (!activityEnded) {
                            NullActivity activity = (NullActivity)genericACI.getActivity();
                            activity.endActivity();
                            activityEnded = true;
                        }
                        acNaming.unbind(name);
                    }
                } catch (Exception e1) {
                    createTraceSafe(Level.WARNING,"Caught Exception while trying to remove binding in the ActivityContextNamingFacility:",e1);
                    TCKSbbUtils.handleException(e1);
                }
            }
        } catch (Exception e2) {
            TCKSbbUtils.handleException(e2);
            return;
        }
    }
}
