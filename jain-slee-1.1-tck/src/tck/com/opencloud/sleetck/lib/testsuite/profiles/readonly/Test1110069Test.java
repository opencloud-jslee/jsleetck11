/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.profiles.readonly;

import javax.slee.profile.ProfileSpecificationID;

import com.opencloud.logging.Logable;
import com.opencloud.sleetck.lib.AbstractSleeTCKTest;
import com.opencloud.sleetck.lib.TCKTestResult;
import com.opencloud.sleetck.lib.infra.SleeTCKComponentConstants;
import com.opencloud.sleetck.lib.resource.TCKActivityID;
import com.opencloud.sleetck.lib.resource.events.TCKResourceEventX;
import com.opencloud.sleetck.lib.resource.testapi.TCKResourceTestInterface;
import com.opencloud.sleetck.lib.sbbutils.events2.SbbMessageAdapter;
import com.opencloud.sleetck.lib.testutils.FutureResult;
import com.opencloud.sleetck.lib.testutils.ProfileUtils;
import com.opencloud.sleetck.lib.testutils.jmx.ProfileProvisioningMBeanProxy;


public class Test1110069Test extends AbstractSleeTCKTest {

    private static final String SERVICE_DU_PATH_PARAM= "serviceDUPath";
    private static final String RESULT_EVENT_DU_PATH_PARAM= "ResultEventDUPath";
    private static final String TCKSBB_EVENT_DU_PATH_PARAM= "TCKSbbEventDUPath";

    private static final String SPEC_NAME = "Test1110069Profile";
    private static final String SPEC_VERSION = "1.0";



    private static final int TEST_ID = 1110069;


    /**
     * This test checks whether profiles can be created, updated, removed by Sbb's for a profile spec
     * with 'profile-read-only' attribute set to 'false'.
     */
    public TCKTestResult run() throws Exception {
        result = new FutureResult(utils().getLog());

        //Create a profile table
        ProfileProvisioningMBeanProxy profileProvisioning = profileUtils.getProfileProvisioningProxy();
        ProfileSpecificationID specID = new ProfileSpecificationID(SPEC_NAME, SleeTCKComponentConstants.TCK_VENDOR, SPEC_VERSION);
        profileProvisioning.createProfileTable(specID, Test1110069Sbb.PROFILE_TABLE_NAME);
        getLog().fine("Added profile table "+Test1110069Sbb.PROFILE_TABLE_NAME);

        // send an initial event to the test
        TCKResourceTestInterface resource = utils().getResourceInterface();
        TCKActivityID activityID = resource.createActivity(getClass().getName());
        Object message = new String("Create");
        getLog().fine("About to fire TCKResourceEventX.X1 event to the Sbb");
        resource.fireEvent(TCKResourceEventX.X1, message, activityID, null);

        //wait for test to set result to passed or failed
        return result.waitForResultOrFail(utils().getTestTimeout(), "Timeout waiting for test result", TEST_ID);
    }

    public void setUp() throws Exception {

        setupService(RESULT_EVENT_DU_PATH_PARAM);

        setupService(TCKSBB_EVENT_DU_PATH_PARAM);

        setupService(SERVICE_DU_PATH_PARAM);

        profileUtils = new ProfileUtils(utils());
        setResourceListener(new SbbMessageAdapter() {
            public Logable getLog() { return utils().getLog(); }
            public void onSbbLogCall(String msg) { sbbLogCall(msg); }
            public void onSetPassed(int id, String msg) { setPassed(id, msg); }
            public void onSetFailed(int id, String msg) { setFailed(id, msg); }
            public void onSetException(Exception e) { setException(e); }
        });
    }

    public void tearDown() throws Exception {

        try {
            profileUtils.removeProfileTable(Test1110069Sbb.PROFILE_TABLE_NAME);
        }
        catch (Exception e) {
            getLog().warning("Caught exception while trying to remove profile table:");
            getLog().warning(e);
        }

        super.tearDown();
    }

    public void sbbLogCall(String msg) {
        getLog().fine("Sbb wants to log: "+msg);
    }

    public void setPassed(int id, String msg) {
        result.setPassed();
    }

    public void setFailed(int id, String msg) {
        result.setFailed(id, msg);
    }

    public void setException(Exception e) {
        result.setError(e);
    }


    private ProfileUtils profileUtils;
    private FutureResult result;
}
