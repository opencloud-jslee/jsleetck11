/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.events.sbbabstractclass;

import javax.naming.InitialContext;
import javax.slee.ActivityContextInterface;
import javax.slee.ChildRelation;
import javax.slee.facilities.ActivityContextNamingFacility;
import javax.slee.facilities.Tracer;
import javax.slee.EventContext;

import com.opencloud.sleetck.lib.resource.events.TCKResourceEventX;
import com.opencloud.sleetck.lib.sbbutils.BaseTCKSbb;
import com.opencloud.sleetck.lib.sbbutils.TCKSbbUtils;

/**
 * AssertionID(1108077): Test The SBB object of the SbbContext object must be in
 * the Ready state, i.e. it must have an assigned SBB entity, when it invokes
 * this method. Otherwise, this method throws a java.lang.IllegalStateException.
 */
public abstract class Test1108077Sbb extends BaseTCKSbb {
    // Initial event method.
    public void onTCKResourceEventX1(TCKResourceEventX ev, ActivityContextInterface aci, EventContext context) {

        try {
            tracer = getSbbContext().getTracer("com.test");
            tracer.info("Test1108077Sbb: Received a TCKResourceEventX");
            InitialContext ic = new InitialContext();
            ActivityContextNamingFacility acNaming = (ActivityContextNamingFacility) ic
                    .lookup(ActivityContextNamingFacility.JNDI_NAME);
            acNaming.bind(aci, "name");
            getChildRelation().create();
        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }
    }

    public abstract ChildRelation getChildRelation();

    private Tracer tracer;
}
