/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.resource.lifecycle;

import java.util.HashSet;

import javax.slee.resource.ConfigProperties;
import javax.slee.resource.ResourceAdaptorID;

import com.opencloud.sleetck.lib.AbstractSleeTCKTest;
import com.opencloud.sleetck.lib.TCKTestResult;
import com.opencloud.sleetck.lib.infra.SleeTCKComponentConstants;
import com.opencloud.sleetck.lib.rautils.MessageHandler;
import com.opencloud.sleetck.lib.rautils.RMIObjectChannel;
import com.opencloud.sleetck.lib.rautils.UOID;
import com.opencloud.sleetck.lib.testsuite.resource.RAMethods;
import com.opencloud.sleetck.lib.testsuite.resource.TCKMessage;
import com.opencloud.sleetck.lib.testutils.FutureResult;
import com.opencloud.sleetck.lib.testutils.jmx.ResourceManagementMBeanProxy;

/**
 * Test multiple resource adaptor entities can be instantiated from a single
 * Resource Adaptor.
 * <p>
 * Test assertions: 1115037
 */
public class Test1115037Test extends AbstractSleeTCKTest {

    private static final String RA_NAME = "TCK_Lifecycle_Test_RA";
    private static final String RA_ENTITY_NAME1 = RA_NAME + "_Entity1";
    private static final String RA_ENTITY_NAME2 = RA_NAME + "_Entity2";
    private static final String RA_ENTITY_NAME3 = RA_NAME + "_Entity3";
    private static final String RA_VENDOR = SleeTCKComponentConstants.TCK_VENDOR;
    private static final String RA_VERSION = "1.1";

    private static final String RESOURCE_DU_PATH_PARAM = "resourceDUPath";

    public TCKTestResult run() throws Exception {
        ResourceManagementMBeanProxy resourceMBean = utils().getResourceManagementMBeanProxy();

        FutureResult future = new FutureResult(getLog());

        RMIObjectChannel in = utils().getRMIObjectChannel();
        in.setMessageHandler(new CallbackListener(future, 3));

        // Create 3 RA Entities from the Lifecycle Resource Adaptor.
        ResourceAdaptorID raID = new ResourceAdaptorID(RA_NAME, RA_VENDOR, RA_VERSION);
        resourceMBean.createResourceAdaptorEntity(raID, RA_ENTITY_NAME1, new ConfigProperties());
        resourceMBean.createResourceAdaptorEntity(raID, RA_ENTITY_NAME2, new ConfigProperties());
        resourceMBean.createResourceAdaptorEntity(raID, RA_ENTITY_NAME3, new ConfigProperties());

        // The CallbackListener will check to see that we received 3 entity
        // creation callbacks from the lifecycle RA, and if we did will set the
        // future result.
        return future.waitForResultOrFail(utils().getTestTimeout(), "Test did not receive the expected number of Resource Adaptor Entity Creation callbacks",
                1115037);
    }

    public void tearDown() throws Exception {
        try {
            utils().removeRAEntities();
        } finally {
            super.tearDown();
        }
    }

    public void setUp() throws Exception {
        String duPath = utils().getTestParams().getProperty(RESOURCE_DU_PATH_PARAM);
        utils().install(duPath);
    }

    //
    // Private
    //

    private class CallbackListener implements MessageHandler {
        public CallbackListener(FutureResult result, int expectedCount) {
            this.result = result;
            this.expectedCount = expectedCount;
        }

        public boolean handleMessage(Object objectMessage) {
            if (objectMessage instanceof TCKMessage) {
                TCKMessage message = (TCKMessage) objectMessage;
                UOID uoid = message.getUID();
                int method = message.getMethod();
                if (method == RAMethods.raConfigure) {
                    synchronized (raEntities) {
                        raEntities.add(uoid);
                        if (raEntities.size() == expectedCount)
                            result.setPassed();
                        // TODO - May have to revisit this for the multiple-jvm
                        // case?
                    }
                } else if (method == RAMethods.raUnconfigure) {
                    synchronized (raEntities) {
                        raEntities.remove(uoid);
                    }
                }
                return true;
            }
            return false;
        }

        private FutureResult result;
        private int expectedCount;
        private HashSet raEntities = new HashSet();
    };
}
