/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.management.SleeManagementMBean;

import com.opencloud.sleetck.lib.resource.TCKSbbMessage;
import com.opencloud.sleetck.lib.testutils.*;
import com.opencloud.sleetck.lib.*;
import com.opencloud.sleetck.lib.resource.testapi.TCKResourceListener;
import com.opencloud.sleetck.lib.testsuite.management.sleestate.QueuingSleeStateListener;
import com.opencloud.sleetck.lib.resource.TCKActivityID;
import com.opencloud.sleetck.lib.resource.testapi.TCKResourceTestInterface;
import com.opencloud.sleetck.lib.resource.events.TCKResourceEventX;
import com.opencloud.sleetck.lib.testutils.jmx.SleeManagementMBeanProxy;
import javax.management.ObjectName;
import javax.slee.management.*;
import java.rmi.RemoteException;
import java.util.HashMap;

public class Test1880Test implements SleeTCKTest {
    private static final String SERVICE_DU_PATH_PARAM = "duPath";
    private static final int TEST_ID = 1880;

    public void init(SleeTCKTestUtils utils) { this.utils=utils; }

    public void setUp() throws Exception {
    management = utils.getSleeManagementMBeanProxy();
    stateListener = new QueuingSleeStateListener(utils);
    management.addNotificationListener(stateListener, null, null);

    resourceListener = new QueuingResourceListener(utils);
    utils.getResourceInterface().setResourceListener(resourceListener);


    // Install Service
    String duPath = utils.getTestParams().getProperty(SERVICE_DU_PATH_PARAM);
    utils.getLog().fine("Installing " + duPath);
    duID = utils.install(duPath);

    // Activate Service
    utils.activateServices(duID, true);

    }

    public void tearDown() throws Exception {

    // Restart SLEE for next test in case this option not selected
    management.start();
    while (!stateListener.nextNotification().getNewState().isRunning());

    try {
        utils.getResourceInterface().clearActivities();
    } catch (Exception e) {
        utils.getLog().error("Failed to clear activities");
    }
    utils.getResourceInterface().removeResourceListener();
    management.removeNotificationListener(stateListener);
    utils.deactivateAllServices();
    utils.uninstallAll();
    }

    public TCKTestResult run() throws Exception {

    // Try to stop the SLEE
    management.stop();
    while (!stateListener.nextNotification().getNewState().isStopped());

    // Fire an event.
        TCKResourceTestInterface resource = utils.getResourceInterface();
        TCKActivityID activityID = resource.createActivity("Test1880InitialActivity");

        utils.getLog().fine("Firing TCKResourceEventX1 on Test1880InitialActivity.");
    try {
        resource.fireEvent(TCKResourceEventX.X1, TCKResourceEventX.X1, activityID, null);
    } catch (Exception e) {
        return TCKTestResult.passed();
    }

    utils.getLog().fine("Waiting for message from SBB.");

    try {
        TCKSbbMessage message = resourceListener.nextMessage();

        utils.getLog().fine("Received message from SBB.");

        HashMap map = (HashMap) message.getMessage();
        Boolean passed = (Boolean) map.get("Result");
        if (passed.booleanValue() == true)
        return TCKTestResult.passed();

        utils.getLog().fine("Returning failed.");

        return TCKTestResult.failed(TEST_ID, (String) map.get("Message"));
    } catch (OperationTimedOutException e) {
        utils.getLog().fine("OperationTimedOutException.");
        return TCKTestResult.passed();
    }

    }


    private SleeTCKTestUtils utils;
    private SleeManagementMBeanProxy management;
    private QueuingSleeStateListener stateListener;
    private DeployableUnitID duID;
    private QueuingResourceListener resourceListener;

}
