/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.activities.activitycontext;

import javax.slee.ActivityEndEvent;
import javax.slee.ActivityContextInterface;
import javax.slee.SbbContext;
import javax.slee.Address;
import javax.slee.facilities.Level;

import com.opencloud.sleetck.lib.sbbutils.BaseTCKSbb;
import com.opencloud.sleetck.lib.sbbutils.TCKSbbUtils;
import com.opencloud.sleetck.lib.sbbutils.TCKSbbConstants;
import com.opencloud.sleetck.lib.resource.events.TCKResourceEventX;
import com.opencloud.sleetck.lib.resource.TCKActivityID;
import com.opencloud.sleetck.lib.resource.sbbapi.TCKActivity;
import com.opencloud.sleetck.lib.resource.sbbapi.TCKResourceSbbInterface;
import com.opencloud.sleetck.lib.resource.sbbapi.TCKActivityContextInterfaceFactory;

import java.util.HashMap;

public abstract class Test597Sbb extends BaseTCKSbb {

    public void sbbCreate() throws javax.slee.CreateException {
        try {
            HashMap map = new HashMap();
            try {
                getSbbContext().getActivities();
            } catch (IllegalStateException e) {
                map.put("Result", new Boolean(true));
                map.put("Message", "Ok");
                TCKSbbUtils.getResourceInterface().sendSbbMessage(map);
            }
            map.put("Result", new Boolean(false));
            map.put("Message", "Should have received IllegalStateException");
            TCKSbbUtils.getResourceInterface().sendSbbMessage(map);
        } catch (Exception f) {
            TCKSbbUtils.handleException(f);
        }
    }

    public void onTCKResourceEventX1(TCKResourceEventX event, ActivityContextInterface aci) {
    }

}
