/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.resource.config;

import java.util.HashMap;

import javax.slee.resource.ConfigProperties;
import javax.slee.resource.InvalidConfigurationException;

import com.opencloud.sleetck.lib.rautils.BaseTCKRA;
import com.opencloud.sleetck.lib.testsuite.resource.RAMethods;
import com.opencloud.sleetck.lib.testsuite.resource.TCKMessage;

public class ConfigTestResourceAdaptor extends BaseTCKRA {

    // Configuration related

    public void raConfigurationUpdate(ConfigProperties properties) {
        sendConfigMessage(RAMethods.raConfigurationUpdate, sanitizeConfigProperties(properties));
    }

    public void raConfigure(ConfigProperties properties) {
        sendConfigMessage(RAMethods.raConfigure, sanitizeConfigProperties(properties));
        configured = true;
    }

    public void raVerifyConfiguration(ConfigProperties properties) throws InvalidConfigurationException {
        sendConfigMessage(RAMethods.raVerifyConfiguration, sanitizeConfigProperties(properties));
        if (properties.getProperty(ResourceConfigConstants.INVALID_PROPERTY_KEY) != null)
            throw new InvalidConfigurationException("InvalidConfigurationException generated at the request of the TCK");
    }

    public void raUnconfigure() {
        configured = false;
    }

    //
    // Private
    //

    private void sendConfigMessage(int method, ConfigProperties properties) {
        HashMap results = new HashMap();
        results.put("properties", properties);
        results.put("configured", new Boolean(configured));
        sendMessage(new TCKMessage(getRAUID(), method, results));
    }

    private boolean configured = false;
}
