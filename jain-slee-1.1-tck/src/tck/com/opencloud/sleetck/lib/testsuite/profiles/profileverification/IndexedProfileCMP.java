/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.profiles.profileverification;

/**
 * Define a simple interface that we will provide a profile management abstract class for
 */
public interface IndexedProfileCMP {
    public String getValue();
    public void setValue(String value);
    public String[] getTable1();
    public void setTable1(String[] addresses);
    public String[] getTable2();
    public void setTable2(String[] addresses);
}
