/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.transactions;

import java.rmi.RemoteException;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Iterator;

import com.opencloud.sleetck.lib.AbstractSleeTCKTest;
import com.opencloud.sleetck.lib.TCKTestFailureException;
import com.opencloud.sleetck.lib.TCKTestResult;
import com.opencloud.sleetck.lib.profileutils.BaseMessageAdapter;
import com.opencloud.sleetck.lib.profileutils.BaseMessageConstants;
import com.opencloud.sleetck.lib.rautils.RMIObjectChannel;
import com.opencloud.sleetck.lib.resource.TCKActivityID;
import com.opencloud.sleetck.lib.resource.events.TCKResourceEventX;
import com.opencloud.sleetck.lib.resource.testapi.TCKResourceTestInterface;
import com.opencloud.sleetck.lib.testutils.FutureResult;

public class Test1109011Test extends AbstractSleeTCKTest {

    private static final String SERVICE_DU_PATH_PARAM = "ServiceDUPath";

    /**
     * Perform the actual test.
     */
    public TCKTestResult run() throws Exception {

        TCKResourceTestInterface resource = utils().getResourceInterface();
        TCKActivityID activityID = resource.createActivity("Test1109011InitialActivity");

        //fire two initial event into SLEE to instantiate two root Sbbs
        //initial-event-select variable is set to 'Event'
        getLog().fine("Create two root SBBs by firing initial events into SLEE.");
        futureResult = new FutureResult(getLog());
        resource.fireEvent(TCKResourceEventX.X1, "23", activityID, null);
        futureResult.waitForResult(utils().getTestTimeout());

        futureResult = new FutureResult(getLog());
        resource.fireEvent(TCKResourceEventX.X1, "42", activityID, null);
        futureResult.waitForResult(utils().getTestTimeout());


        //fire event to the two root Sbbs and wait
        getLog().fine("Fire event into SLEE to start tests. Should be received by both Sbbs.");
        resource.fireEvent(TCKResourceEventX.X2, TCKResourceEventX.X2, activityID, null);

        Thread.sleep(utils().getTestTimeout());
        //if a message callback was called by the Sbb during the waiting time that resulted
        //in a test failure (or success) being set, terminate the test
        if (exResult!=null)
            return exResult;

        //1109011: check that for each sbb that had an event handler called, there was also an invokation of sbbRolledBack()
        getLog().fine("Number of sbb callbacks: "+register.size());
        Iterator iter = register.values().iterator();
        while (iter.hasNext()) {
            Boolean rolledBack  = (Boolean)iter.next();
            if (rolledBack.booleanValue() == false) {
                return TCKTestResult.failed(1109011, "Though the TCKResourceEventX.X2 event handler TXNs " +
                        "are marked for rollback by the sbb code, the sbbRolledBack() " +
                        "method was not called for at least one of the Sbbs.");
            }
        }

        //check that sbbRolledBack() was actually rolled back, too
        getLog().fine("Fire event into SLEE to make Sbb's send back their stringValue CMP fields.");
        futureResult = new FutureResult(getLog());
        resource.fireEvent(TCKResourceEventX.X3, TCKResourceEventX.X3, activityID, null);
        futureResult.waitForResult(utils().getTestTimeout());

        return TCKTestResult.passed();
    }

    private void onCMPValue(HashMap map) {
        String value = (String)map.get("stringValue");

        if (value!=null)
            setIfEmpty(TCKTestResult.failed(1109011,"Changes made in sbbRolledBack() were not undone though sbbRolledBack's TXN was marked for rollback."));
        else {
            getLog().fine("Changes made in sbbRolledBack() were undone as sbbRolledBack's TXN was marked for rollback itself.");
            futureResult.setPassed();
        }
    }

    private void onFromRollback(HashMap map) {
        String sbbID = (String)map.get("SbbID");
        Boolean status = (Boolean)register.get(sbbID);

        //Sbb entity must be one of those that were previously used to handle the events
        if (status == null) {
            setIfEmpty(TCKTestResult.failed(1109011, "Received rollback message for Sbb entity that did not previously send an ACK for receiving an initial event."));
            return;
        }

        //Failure if there has already been a rollback message for this Sbb entity
        //1109013
        if (status.booleanValue()) {
            setIfEmpty(TCKTestResult.failed(1109013, "Received more than one rollback message for an Sbb entity."));
            return;
        }
        register.put(sbbID, new Boolean(true));
    }

    private void onEventHandler(HashMap map) {
        String sbbID = (String)map.get("SbbID");
        Boolean status = (Boolean)register.get(sbbID);

        //if the event handler method is called more than once for the same Sbb entity sth is seriously wrong with the msg delivery
        if (status != null) {
            setIfEmpty(TCKTestResult.failed(1109011, "Event handler msg was received twice for the same Sbb entity."));
            return;
        }
        //register sbb entity, waiting for a msg that sbbRolledBack has been called for this entity
        register.put(sbbID, new Boolean(false));
    }

    public void setPassed(int assertionID, String msg) {
        getLog().fine(assertionID+": "+msg);
        if (futureResult!=null && !futureResult.isSet())
            futureResult.setPassed();
    }

    public void setFailed(int assertionID, String msg, Exception e) {
        if (e==null) {
            getLog().fine("FAILURE: "+assertionID+":"+msg);
            if (futureResult!=null && !futureResult.isSet())
                futureResult.setFailed(assertionID, msg);

            //in case the currently active futureResult has already been set to passed() before the failure
            //comes in, we have to provide a mechanism to still detect this failure
            setIfEmpty(TCKTestResult.failed(assertionID, msg));
        }
        else {
            getLog().fine("FAILURE: "+assertionID+":"+msg);
            getLog().fine(e);
            if (futureResult!=null && !futureResult.isSet())
                futureResult.setFailed(new TCKTestFailureException(assertionID, msg, e));

            //in case the currently active futureResult has already been set to passed() before the failure
            //comes in, we have to provide a mechanism to still detect this failure
            setIfEmpty(TCKTestResult.failed(new TCKTestFailureException(assertionID, msg, e)));
        }
    }

    public void setError(String msg, Exception e) {
        if (e==null) {
            getLog().warning(msg);
            if (futureResult!=null && !futureResult.isSet())
                futureResult.setError(msg);

            //in case the currently active futureResult has already been set to passed() before the error
            //comes in, we have to provide a mechanism to still detect this error
            setIfEmpty(TCKTestResult.error(msg));
        }
        else {
            getLog().warning(msg);
            getLog().warning(e);
            if (futureResult!=null && !futureResult.isSet())
                futureResult.setError(msg, e);

            //in case the currently active futureResult has already been set to passed() before the error
            //comes in, we have to provide a mechanism to still detect this failure
            setIfEmpty(TCKTestResult.error(msg, e));
        }
    }

    /**
     * Do all the pre-run configuration of the test.
     */
    public void setUp() throws Exception {

        // Install the Deployable Units
        setupService(SERVICE_DU_PATH_PARAM);

        in = utils().getRMIObjectChannel();
        in.setMessageHandler(new BaseMessageAdapter(getLog()) {
            public void onSetPassed(int assertionID, String msg) { setPassed(assertionID, msg); }
            public void onSetFailed(int assertionID, String msg, Exception e) { setFailed(assertionID, msg, e); }
            public void onLogCall(String msg) { getLog().fine(msg); }
            public void onSetError(String msg, Exception e) { setError(msg, e); }

            public boolean handleMessage(Object message) throws RemoteException {
                //if msg type is not already recognized and handled by BaseMessageAdapter...
                if (!super.handleMessage(message)) {
                    HashMap map = (HashMap)message;
                    int type = ((Integer)map.get(BaseMessageConstants.TYPE)).intValue();
                    switch (type) {
                    case Test1109011Sbb.TYPE_EVENTHANDLER:
                        onEventHandler((HashMap)message);
                        return true;
                    case Test1109011Sbb.TYPE_ROLLBACK:
                        onFromRollback((HashMap)message);
                        return true;
                    case Test1109011Sbb.TYPE_CMP_VALUE:
                        onCMPValue((HashMap)message);
                        return true;
                    }
                    getLog().warning("Received message with unrecognized 'Type' field: "+type);
                    return false;
                }
                return true;
            }
        });

        //Initialize the Sbb entity register table
        register = new Hashtable();
    }

    private void setIfEmpty(TCKTestResult exResult) {
        if (this.exResult==null)
            this.exResult = exResult;
    }

    /**
     * Clean up after the test.
     */
    public void tearDown() throws Exception {
        in.clearQueue();

        super.tearDown();
    }

    private Hashtable register;
    private RMIObjectChannel in;

    private FutureResult futureResult;
    private TCKTestResult exResult;


}
