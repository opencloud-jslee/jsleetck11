/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.profiles.profileabstractclass;

import javax.slee.profile.ProfileLocalObject;

/**
 * Used for the test case in which a ProfileLocal interface is present, has
 * business methods (i.e. it contains not only CMP get/set accessors) and no Profile abstract
 * class is defined. The spec created for this should NOT deploy!
 */
public interface Test1110093_1ProfileLocal extends ProfileLocalObject {
    public void localManage();
    public String getValue();
}
