/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.management.ServiceUsageMBean;


import javax.management.ObjectName;
import javax.slee.ComponentID;
import javax.slee.InvalidArgumentException;
import javax.slee.SbbID;
import javax.slee.ServiceID;
import javax.slee.UnrecognizedSbbException;
import javax.slee.management.DeployableUnitDescriptor;
import javax.slee.management.DeployableUnitID;

import com.opencloud.sleetck.lib.AbstractSleeTCKTest;
import com.opencloud.sleetck.lib.DescriptionKeys;
import com.opencloud.sleetck.lib.TCKTestResult;
import com.opencloud.sleetck.lib.testutils.jmx.DeploymentMBeanProxy;
import com.opencloud.sleetck.lib.testutils.jmx.MBeanProxyFactory;
import com.opencloud.sleetck.lib.testutils.jmx.ServiceUsageMBeanProxy;


/**
 * Checks that a ServiceUsageMBean returned by getSbbUsageNotificationManager()
 * extends the ServiceUsageMBean interface, and that it represents
 * the correct service.
 */
public class Test1114343Test extends AbstractSleeTCKTest {

    public static final String SERVICE_NAME = "serviceDUPath";
    public static final int TEST_ID = 1114343;

    public TCKTestResult run() throws Exception {

        try {
            // Assertion 1114343 - Call getSbbUsageNotificationManager
            utils().getLog().fine("Starting test....");
            ObjectName usageMBeanName = serviceUsageMBeanProxy.getSbbUsageMBean(sbbID);
            utils().getLog().fine("UsageMBean set up: " +usageMBeanName.toString());

            try {
                javax.management.ObjectName usageObjectName=serviceUsageMBeanProxy.getSbbUsageNotificationManagerMBean(sbbID);
                String usageObjectNameStr = usageObjectName.toString();
                utils().getLog().fine("getSbbUsageNotificationManagerMBean name = : " +usageObjectNameStr);
                if (usageObjectNameStr.equals("javax.slee.usage:type=UsageNotificationManager," +
                                              "notificationSource=javax.slee.management.usage.sbb," +
                                              "serviceName=\"Test1114343Service\",serviceVendor=\"jain.slee.tck\",serviceVersion=\"1.1\"," +
                                              "sbbName=\"GenericUsageSbb\",sbbVendor=\"jain.slee.tck\",sbbVersion=\"1.1\""))
                    logSuccessfulCheck(1114343);
                else
                    return TCKTestResult.failed(1114343, "ServiceUsageMBean.getSbbUsageNotificationManager(SbbID) has not returned correct ObjectName");
            } catch (NullPointerException e) {
                return TCKTestResult.failed(1114343, "ServiceUsageMBean.getSbbUsageNotificationManager(SbbID) didn't contain SbbID");
            } catch (UnrecognizedSbbException e) {
                return TCKTestResult.failed(1114343, "ServiceUsageMBean.getSbbUsageNotificationManager(SbbID) didn't recognise SbbID");
            } catch (InvalidArgumentException e) {
                return TCKTestResult.failed(1114343, "ServiceUsageMBean.getSbbUsageNotificationManager(SbbID)) didn't have valid SbbID");
            } catch (Exception e) {
                utils().getLog().fine(e.getMessage());
                return TCKTestResult.failed(1114343, "ServiceUsageMBean.getSbbUsageNotificationManager(SbbID) has thrown Exception: " + e.getClass().toString());
            }

            try {
                sbbID = null;
                javax.management.ObjectName usageObjectName=serviceUsageMBeanProxy.getSbbUsageNotificationManagerMBean(sbbID);
                return TCKTestResult.failed(1114547, "ServiceUsageMBean.getSbbUsageNotificationManager(SbbID) has not thrown NullPointerException");
            } catch (NullPointerException e) {
                logSuccessfulCheck(1114547);
            } catch (Exception e) {
                utils().getLog().fine(e.getMessage());
                return TCKTestResult.failed(1114547, "ServiceUsageMBean.getSbbUsageNotificationManager(SbbID) has thrown Exception: " + e.getClass().toString());
            }

            try {
                SbbID dummySbbID = new SbbID("DummyUsageSbb","jain.slee.tck","1.0");
                javax.management.ObjectName usageObjectName=serviceUsageMBeanProxy.getSbbUsageNotificationManagerMBean(dummySbbID);
                return TCKTestResult.failed(1114548, "ServiceUsageMBean.getSbbUsageNotificationManagerMBean(SbbID) has not thrown UnrecognizedSbbException");
            } catch (UnrecognizedSbbException e) {
                logSuccessfulCheck(1114548);
            } catch (Exception e) {
                utils().getLog().fine(e.getMessage());
                return TCKTestResult.failed(1114548, "ServiceUsageMBean.getSbbUsageNotificationManagerMBean(SbbID) has thrown Exception: " + e.getClass().toString());
            }

        } finally {
            utils().getLog().fine("Deactivating and uninstalling service");
        }

        return TCKTestResult.passed();

    }

    public void setUp() throws Exception {
        ObjectName objName;

        // Install the Deployable Units
        String duPath = utils().getTestParams().getProperty(DescriptionKeys.SERVICE_DU_PATH_PARAM);
        duID = utils().install(duPath);

        // Activate the DU
        utils().activateServices(duID, true);

        DeploymentMBeanProxy duProxy = utils().getDeploymentMBeanProxy();
        DeployableUnitDescriptor duDesc = duProxy.getDescriptor(duID);
        ComponentID components[] = duDesc.getComponents();
        // Get the SbbID from the components.
        for (int i = 0; i < components.length; i++) {
            if (components[i] instanceof ServiceID) {
                getLog().fine("Setting serviceID value.");
                serviceID = (ServiceID) components[i];
                continue;
            }

            if (components[i] instanceof SbbID) {
                getLog().fine("Setting sbbID value.");
                sbbID = (SbbID) components[i];
                continue;
            }
        }
        utils().getLog().fine("sbbID = " +sbbID.toString());

        proxyFactory = utils().getMBeanProxyFactory();

        try {
            objName = utils().getServiceManagementMBeanProxy().getServiceUsageMBean(serviceID);
            serviceUsageMBeanProxy = proxyFactory.createServiceUsageMBeanProxy(objName);

            if (serviceUsageMBeanProxy == null) {
                utils().getLog().warning("serviceUsageMBeanProxy is null");
            } else {
                utils().getLog().fine("serviceUsageMBeanProxy = " +serviceUsageMBeanProxy.toString());
            }
        } catch(Exception e) {
            getLog().warning(e);
            utils().getLog().fine("serviceUsageMBeanProxy not set up: = "+serviceUsageMBeanProxy.toString());
        }

    }

    public void tearDown() throws Exception {
        utils().getLog().fine("Disconnecting from resource");
        utils().getResourceInterface().clearActivities();
        utils().getLog().fine("Deactivating and uninstalling service");
        utils().deactivateAllServices();
        utils().uninstallAll();
    }

    private void logSuccessfulCheck(int assertionID) {
        utils().getLog().info("Check for assertion "+assertionID+" OK");
    }


    private ServiceUsageMBeanProxy serviceUsageMBeanProxy;
    private MBeanProxyFactory proxyFactory;
    private DeployableUnitID duID;
    private SbbID sbbID;
    private ServiceID serviceID;
}
