/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.management.ProfileProvisioningMBean;

import com.opencloud.sleetck.lib.*;
import com.opencloud.sleetck.lib.infra.SleeTCKComponentConstants;
import com.opencloud.sleetck.lib.resource.testapi.TCKResourceListener;
import com.opencloud.sleetck.lib.testutils.Assert;
import com.opencloud.sleetck.lib.testutils.BaseTCKResourceListener;
import com.opencloud.sleetck.lib.testutils.ComponentIDLookup;
import com.opencloud.sleetck.lib.testutils.ProfileUtils;
import com.opencloud.sleetck.lib.testutils.jmx.ProfileMBeanProxy;
import com.opencloud.sleetck.lib.testutils.jmx.ProfileProvisioningMBeanProxy;
import com.opencloud.sleetck.lib.testutils.jmx.impl.EmptyArrays;

import javax.management.*;
import javax.slee.management.ManagementException;
import javax.slee.profile.ProfileSpecificationID;
import java.rmi.RemoteException;

/**
 * Tests assertion 1083, that where a SLEE MBean interface defines a get and/or set accessor
 * method that follows the design pattern for a managed attribute,
 * the SLEE MBean class must ensure that the target of the accessor is accessible
 * and/or updateable via both the invoke and getAttribute/setAttribute methods of the MBean Server.
 */
public class Test1083Test implements SleeTCKTest {

    private static final String PROFILE_DU_PATH_PARAM = "profileDUPath";
    private static final int TEST_ID = 1083;
    private static final String ATTRIBUTE_NAME = "AttributeA";
    private static final String PROFILE_SPEC_NAME = "Test1083ProfileCMP";

    public void init(SleeTCKTestUtils utils) {
        this.utils = utils;
    }

    /**
     * Perform the actual test.
     */

    public TCKTestResult run() throws Exception {

        ProfileProvisioningMBeanProxy profileProvisioningProxy = profileUtils.getProfileProvisioningProxy();

        ProfileSpecificationID profileSpecID = new ComponentIDLookup(utils).lookupProfileSpecificationID(
            PROFILE_SPEC_NAME,SleeTCKComponentConstants.TCK_VENDOR,"1.0");

        if(profileSpecID == null) throw new TCKTestErrorException("No ProfileSpecification found.");

        ObjectName profileObj = null;
        // Create the profile table.
        try {
            profileProvisioningProxy.createProfileTable(profileSpecID, "Test1083ProfileTable");
            profileObj = profileProvisioningProxy.createProfile("Test1083ProfileTable", "Test1083Profile");
        } catch (Exception e) {
            return TCKTestResult.error("Failed to create profile table or profile.");
        }

        String newValue = "ChangedValue1";

        ProfileMBeanProxy profileProxy = utils.getMBeanProxyFactory().createProfileMBeanProxy(profileObj);
        try {
            profileProxy.editProfile();

            utils.getLog().info("setting attribute using MBeanServer.setAttribute() to value "+newValue);
            setViaSetAttribute(profileObj,newValue);

            utils.getLog().info("getting attribute using MBeanServer.getAttribute()");
            String currentValue = getViaGetAttribute(profileObj);
            utils.getLog().info("MBeanServer.getAttribute() return value="+currentValue);

            Assert.assertEquals(1083,"Value returned by MBeanServer.getAttribute() was not the same as the value "+
                "set using MBeanServer.setAttribute()",newValue,currentValue);

            newValue = "ChangedValue2";

            utils.getLog().info("setting attribute using MBeanServer.invoke(...,\"setAttributeA\",...) to value "+newValue);
            setViaInvoke(profileObj,newValue);

            utils.getLog().info("getting attribute using MBeanServer.invoke(...,\"getAttributeA\",...)");
            currentValue = getViaInvoke(profileObj);
            utils.getLog().info("MBeanServer.invoke(...,\"getAttributeA\",...) return value="+currentValue);

            Assert.assertEquals(1083,"Value returned by MBeanServer.invoke(...,\"getAttributeA\",...) was not the "+
                "same as the value set using MMBeanServer.invoke(...,\"getAttributeA\",...)",newValue,currentValue);

        } finally {
            if (profileProxy.isProfileWriteable())
                profileProxy.commitProfile();
            profileProxy.closeProfile();
        }

        return TCKTestResult.passed();

    }

    /**
     * Do all the pre-run configuration of the test.
     */
    public void setUp() throws Exception {

        utils.getLog().fine("Connecting to resource");
        resourceListener = new TCKResourceListenerImpl();
        utils.getResourceInterface().setResourceListener(resourceListener);

        // Install the Deployable Units
        utils.getLog().fine("Installing the profile spec");
        String duPath = utils.getTestParams().getProperty(PROFILE_DU_PATH_PARAM);
        utils.install(duPath);
        profileUtils = new ProfileUtils(utils);

    }

    /**
     * Clean up after the test.
     */

    public void tearDown() throws Exception {

        if (profileUtils != null) {
            ProfileProvisioningMBeanProxy profileProvisioningProxy = profileUtils.getProfileProvisioningProxy();

            try {
                profileProvisioningProxy.removeProfile("Test1083ProfileTable", "Test1083Profile");
            } catch (Exception e) {
                utils.getLog().warning(e);
            }

            try {
                profileUtils.removeProfileTable("Test1083ProfileTable");
            } catch (Exception e) {
                utils.getLog().warning(e);
            }
        }

        utils.getLog().fine("Disconnecting from resource");
        utils.getResourceInterface().clearActivities();
        utils.getResourceInterface().removeResourceListener();
        utils.getLog().fine("Uninstalling the profile spec");
        utils.uninstallAll();
    }

    /**
     * Sets the attribute using MBeanServer.setAttribute()
     */
    private void setViaSetAttribute(ObjectName objName, String value)
        throws ManagementException, TCKTestErrorException, TCKTestFailureException {
       try {
            utils.getMBeanFacade().setAttribute(objName, new Attribute(ATTRIBUTE_NAME, value));
       } catch(InstanceNotFoundException ie) {
           throw new TCKTestErrorException("Caught Exception while calling MBeanServer.getAttribute()",ie);
       } catch(ReflectionException re) {
           throw new TCKTestFailureException(TEST_ID,"Caught ReflectionException while calling MBeanServer.setAttribute(...,\"AttributeA\",...)",re);
       } catch(AttributeNotFoundException e) {
           throw new TCKTestFailureException(TEST_ID,"Caught AttributeNotFoundException while calling MBeanServer.setAttribute(...,\"AttributeA\",...)", e);
       } catch(InvalidAttributeValueException e) {
           throw new TCKTestFailureException(TEST_ID,"Caught InvalidAttributeValueException while calling MBeanServer.setAttribute(...,\"AttributeA\",...)", e);
       } catch(MBeanException e) {
           Exception enclosed = e.getTargetException();
           if(enclosed instanceof ManagementException) throw (ManagementException)enclosed;
           if(enclosed instanceof RuntimeException) throw (RuntimeException)enclosed;
           throw new TCKTestErrorException("Caught unexpected exception",enclosed);
       }
    }

    /**
     * Gets the attribute using MBeanServer.getAttribute()
     */
    private String getViaGetAttribute(ObjectName objName)
        throws ManagementException, TCKTestErrorException, TCKTestFailureException {
       try {
           return (String)utils.getMBeanFacade().getAttribute(objName, ATTRIBUTE_NAME);
       } catch(InstanceNotFoundException ie) {
           throw new TCKTestErrorException("Caught Exception while calling MBeanServer.getAttribute()",ie);
       } catch(ReflectionException re) {
           throw new TCKTestFailureException(TEST_ID,"Caught ReflectionException while calling MBeanServer.getAttribute(\"AttributeA\")",re);
       } catch(AttributeNotFoundException e) {
           throw new TCKTestFailureException(TEST_ID,"Caught AttributeNotFoundException while calling MBeanServer.getAttribute(\"AttributeA\")", e);
       } catch(MBeanException e) {
           Exception enclosed = e.getTargetException();
           if(enclosed instanceof ManagementException) throw (ManagementException)enclosed;
           if(enclosed instanceof RuntimeException) throw (RuntimeException)enclosed;
           throw new TCKTestErrorException("Caught unexpected exception",enclosed);
       }
    }

    /**
     * Sets the attribute using MBeanServer.invoke(...,"setAttributeA",...)
     */
    private void setViaInvoke(ObjectName objName, String value) throws TCKTestErrorException, TCKTestFailureException {
      try {
         utils.getMBeanFacade().invoke(objName,"setAttributeA",new Object[]{value},new String[]{"java.lang.String"});
      } catch(InstanceNotFoundException ie) {
         throw new TCKTestErrorException("Caught Exception while calling MBeanServer.invoke(...,\"setAttributeA\",...)",ie);
      } catch(ReflectionException re) {
         throw new TCKTestFailureException(TEST_ID,"Caught ReflectionException while calling MBeanServer.invoke(...,\"setAttributeA\",...)",re);
      } catch(MBeanException e) {
         Exception enclosed = e.getTargetException();
         if(enclosed instanceof RuntimeException) throw (RuntimeException)enclosed;
         throw new TCKTestErrorException("Caught unexpected exception",enclosed);
      }
    }

    /**
     * Gets the attribute using MBeanServer.invoke(...,"getAttributeA",...)
     */
    private String getViaInvoke(ObjectName objName) throws TCKTestErrorException, TCKTestFailureException {
      try {
         return (String)utils.getMBeanFacade().invoke(objName,"getAttributeA",
            EmptyArrays.EMPTY_OBJECT_ARRAY,EmptyArrays.EMPTY_STRING_ARRAY);
      } catch(InstanceNotFoundException ie) {
         throw new TCKTestErrorException("Caught Exception while calling MBeanServer.invoke(...,\"getAttributeA\",...)",ie);
      } catch(ReflectionException re) {
         throw new TCKTestFailureException(TEST_ID,"Caught ReflectionException while calling MBeanServer.invoke(...,\"getAttributeA\",...)",re);
      } catch(MBeanException e) {
         Exception enclosed = e.getTargetException();
         if(enclosed instanceof RuntimeException) throw (RuntimeException)enclosed;
         throw new TCKTestErrorException("Caught unexpected exception",enclosed);
      }
    }

    private class TCKResourceListenerImpl extends BaseTCKResourceListener {
        public void onException(Exception e) throws RemoteException {
            utils.getLog().warning("Received exception from the TCK resource");
            utils.getLog().warning(e);
        }
    }

    private SleeTCKTestUtils utils;
    private TCKResourceListener resourceListener;
    private ProfileUtils profileUtils;

}
