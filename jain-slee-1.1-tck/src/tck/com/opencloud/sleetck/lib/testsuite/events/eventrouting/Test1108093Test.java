/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.events.eventrouting;

import javax.slee.Address;
import javax.slee.AddressPlan;

import com.opencloud.sleetck.lib.TCKTestResult;
import com.opencloud.sleetck.lib.resource.TCKActivityID;
import com.opencloud.sleetck.lib.resource.events.TCKResourceEventX;
import com.opencloud.sleetck.lib.resource.testapi.TCKResourceTestInterface;
import com.opencloud.sleetck.lib.testsuite.events.convergencename.AbstractConvergenceNameTest;
import com.opencloud.sleetck.lib.testsuite.events.convergencename.InitialEventSelectorParameters;

/**
 * AssertionID(1108093): Test If the initialEvent attribute is set to false,
 * then the event is not an initial event for this Service and no further
 * convergence name and initial event processing is performed for this Service,
 * i.e. the SLEE will not create any new root SBB entity for this Service to
 * process this event.
 * 
 */
public class Test1108093Test extends AbstractConvergenceNameTest {

    public TCKTestResult run() throws Exception {
        TCKResourceTestInterface resource = utils().getResourceInterface();

        TCKActivityID activityID = resource.createActivity("ModifyIsInitialEventTest-Activity1");
        Address address = new Address(AddressPlan.IP, "1.0.0.1");

        setMainAssertionID(1108093);
        sendEventAndWait(TCKResourceEventX.X1,"1",activityID,address,
                null,// null indicates that no SBB should be created for the event
                createIESParams(false));

        return TCKTestResult.passed();
    }

    private InitialEventSelectorParameters createIESParams(boolean isInitialEventFlag) {
        return new InitialEventSelectorParameters(false, false, false, true, false, null, true, isInitialEventFlag,
                false, null);
    }
}