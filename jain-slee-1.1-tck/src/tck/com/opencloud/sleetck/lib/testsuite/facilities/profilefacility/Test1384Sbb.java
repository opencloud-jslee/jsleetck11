/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.facilities.profilefacility;

import com.opencloud.sleetck.lib.resource.events.TCKResourceEventX;
import com.opencloud.sleetck.lib.sbbutils.BaseTCKSbb;
import com.opencloud.sleetck.lib.sbbutils.TCKSbbUtils;

import javax.naming.InitialContext;
import javax.slee.ActivityContextInterface;
import javax.slee.profile.*;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;

public abstract class Test1384Sbb extends BaseTCKSbb {

    private static final String JNDI_PROFILEFACILITY_NAME = "java:comp/env/slee/facilities/profile";
    public static final String PROFILE_TABLE_NAME = "Test1384ProfileTable";
    public static final String PROFILE_NAME = "Test1384Profile";

    // Initial event
    public void onTCKResourceEventX1(TCKResourceEventX ev, ActivityContextInterface aci) {

        try {
            HashMap map = new HashMap();

            ProfileFacility facility = (ProfileFacility) new InitialContext().lookup(JNDI_PROFILEFACILITY_NAME);

            // 1384

            try {
                facility.getProfilesByIndexedAttribute(PROFILE_TABLE_NAME, "stringVal", new String("attrValue"));
            } catch (AttributeTypeMismatchException e) {
                map.put("Result", new Boolean(false));
                map.put("Message", "attributeValue (String type)  argument was the correct type, yet exception was thrown getting matching profiles.");
                map.put("ID", new Integer(1384));
                TCKSbbUtils.getResourceInterface().sendSbbMessage(map);
                return;
            }

            boolean passed = false;
            try {
                facility.getProfilesByIndexedAttribute(PROFILE_TABLE_NAME, "stringVal", new Integer(1));
            } catch (AttributeTypeMismatchException e) {
                passed = true;
            } catch (Exception e) {
                TCKSbbUtils.handleException(e);
                return;
            }

            if (!passed) {
                map.put("Result", new Boolean(false));
                map.put("Message", "Incorrect object type given in attributeValue argument, but AttributeTypeMismatchException was not thrown.");
                map.put("ID", new Integer(1384));
                TCKSbbUtils.getResourceInterface().sendSbbMessage(map);
                return;
            }

            // 1385 - Must use wrapper type
            try {
                facility.getProfilesByIndexedAttribute(PROFILE_TABLE_NAME, "intVal", new Integer(1));
            } catch (AttributeTypeMismatchException e) {
                map.put("Result", new Boolean(false));
                map.put("Message", "attributeValue (Integer type) argument was the correct type, yet exception was thrown getting matching profiles.");
                map.put("ID", new Integer(1385));
                TCKSbbUtils.getResourceInterface().sendSbbMessage(map);
                return;
            }

            passed = false;
            try {
                facility.getProfilesByIndexedAttribute(PROFILE_TABLE_NAME, "intVal", new Boolean(false));
            } catch (AttributeTypeMismatchException e) {
                passed = true;
            } catch (Exception e) {
                TCKSbbUtils.handleException(e);
                return;
            }

            if (!passed) {
                map.put("Result", new Boolean(false));
                map.put("Message", "Incorrect object type given in attributeValue argument, but AttributeTypeMismatchException was not thrown.");
                map.put("ID", new Integer(1385));
                TCKSbbUtils.getResourceInterface().sendSbbMessage(map);
                return;
            }

            // 1387
            try {
                facility.getProfilesByIndexedAttribute(PROFILE_TABLE_NAME, "stringArray", new String("bar"));
            } catch (AttributeTypeMismatchException e) {
                map.put("Result", new Boolean(false));
                map.put("Message", "attributeValue (String [] type) argument was the correct type, yet exception was thrown getting matching profiles.");
                map.put("ID", new Integer(1387));
                TCKSbbUtils.getResourceInterface().sendSbbMessage(map);
                return;
            }
            passed = false;
            try {
                facility.getProfilesByIndexedAttribute(PROFILE_TABLE_NAME, "stringArray", new Boolean(false));

            } catch (AttributeTypeMismatchException e) {
                passed = true;
            } catch (Exception e) {
                TCKSbbUtils.handleException(e);
                return;
            }

            if (!passed) {
                map.put("Result", new Boolean(false));
                map.put("Message", "Incorrect object type given in attributeValue argument, but AttributeTypeMismatchException was not thrown.");
                map.put("ID", new Integer(1387));
                TCKSbbUtils.getResourceInterface().sendSbbMessage(map);
                return;
            }

            // 1388
            try {
                facility.getProfilesByIndexedAttribute(PROFILE_TABLE_NAME, "intArray", new Integer(2));
            } catch (AttributeTypeMismatchException e) {
                map.put("Result", new Boolean(false));
                map.put("Message", "attributeValue (Integer [] type) argument was the correct type, yet exception was thrown getting matching profiles.");
                map.put("ID", new Integer(1388));
                TCKSbbUtils.getResourceInterface().sendSbbMessage(map);
                return;
            }

            passed = false;
            try {
                facility.getProfilesByIndexedAttribute(PROFILE_TABLE_NAME, "intArray", new Boolean(false));
            } catch (AttributeTypeMismatchException e) {
                passed = true;
            } catch (Exception e) {
                TCKSbbUtils.handleException(e);
                return;
            }

            if (!passed) {
                map.put("Result", new Boolean(false));
                map.put("Message", "Incorrect object type given in attributeValue argument, but AttributeTypeMismatchException was not thrown.");
                map.put("ID", new Integer(1388));
                TCKSbbUtils.getResourceInterface().sendSbbMessage(map);
                return;
            }


            Collection collection = facility.getProfilesByIndexedAttribute(PROFILE_TABLE_NAME, "intArray", new Integer(1));
            Iterator iter = collection.iterator();
            passed = false;
            iter.next();
            try {
                iter.remove();
            } catch (UnsupportedOperationException e) {
                passed = true;
            }

            if (!passed) {
                map.put("Result", new Boolean(false));
                map.put("Message", "Collection.iterator() returned by ProfileFacility.getProfilesByIndexedAttribute() supported the remove() operation.");
                map.put("ID", new Integer(2092));
                TCKSbbUtils.getResourceInterface().sendSbbMessage(map);
                return;
            }

            passed = false;
            try {
                collection.add(Boolean.TRUE);
            } catch (UnsupportedOperationException e) {
                passed = true;
            }

            if (!passed) {
                map.put("Result", Boolean.FALSE);
                map.put("Message", "Collection returned by ProfileFacility.getProfilesByIndexedAttribute() supported the add(Object) operation - it should be immutable");
                map.put("ID", new Integer(2092));
                TCKSbbUtils.getResourceInterface().sendSbbMessage(map);
                return;
            }

            iter = facility.getProfilesByIndexedAttribute(PROFILE_TABLE_NAME, "intArray", new Integer(1)).iterator();

            // Make sure some profiles were returned.
            if (!iter.hasNext()) {
                map.put("Result", new Boolean(false));
                map.put("Message", "No profiles returned from ProfileFacility.getProfilesByIndexedAttribute(intArray)) - cannot continue with test.");
                map.put("ID", new Integer(4294));
                TCKSbbUtils.getResourceInterface().sendSbbMessage(map);
                return;
            }

            while (iter.hasNext()) {
                Object item = iter.next();
                if (! (item instanceof ProfileID)) {
                    map.put("Result", new Boolean(false));
                    map.put("Message", "ProfileFacility.getProfilesByIndexedAttribute returned an Iterator iterating over non-ProfileID objects.");
                    map.put("ID", new Integer(4294));
                    TCKSbbUtils.getResourceInterface().sendSbbMessage(map);
                    return;
                }

                // 2093 - no default profile included.
                ProfileID profileID = (ProfileID) item;
                if (!profileID.getProfileName().equals(PROFILE_NAME)) {
                    map.put("Result", new Boolean(false));
                    map.put("Message", "ProfileFacility.getProfilesByIndexedAttribute included the default profile.");
                    map.put("ID", new Integer(2093));
                    TCKSbbUtils.getResourceInterface().sendSbbMessage(map);
                    return;
                }
            }

            map.put("Result", new Boolean(true));
            map.put("Message", "Ok");
            TCKSbbUtils.getResourceInterface().sendSbbMessage(map);

        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }

    }

    public abstract Test1384ProfileCMP getProfileCMP(ProfileID profileID) throws UnrecognizedProfileTableNameException, UnrecognizedProfileNameException;

}
