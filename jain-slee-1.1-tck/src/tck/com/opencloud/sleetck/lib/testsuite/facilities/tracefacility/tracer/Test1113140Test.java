/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.facilities.tracefacility.tracer;

import javax.management.Notification;
import javax.management.NotificationListener;
import javax.slee.management.DeployableUnitID;
import javax.slee.management.SbbNotification;
import javax.slee.management.TraceNotification;

import com.opencloud.sleetck.lib.AbstractSleeTCKTest;
import com.opencloud.sleetck.lib.DescriptionKeys;
import com.opencloud.sleetck.lib.resource.TCKActivityID;
import com.opencloud.sleetck.lib.resource.events.TCKResourceEventX;
import com.opencloud.sleetck.lib.resource.testapi.TCKResourceTestInterface;
import com.opencloud.sleetck.lib.testutils.FutureResult;
import com.opencloud.sleetck.lib.testutils.jmx.TraceMBeanProxy;

/*
 * AssertionID (1113140): Test the message argument. This argument specifies 
 * the message that will be placed into the message attribute of the 
 * TraceNotification object emitted by the TraceMBean object for the trace 
 * message.
 */
public class Test1113140Test extends AbstractSleeTCKTest {

    /**
     * Perform the actual test.
     */
    public void run(FutureResult result) throws Exception {
        this.result = result;
        String activityName = "Test1113140Test";
        TCKResourceTestInterface resource = utils().getResourceInterface();
        TCKActivityID activityID = resource.createActivity(activityName);
        getLog().info("Firing event: " + testName);

        // kickoff the test
        resource.fireEvent(TCKResourceEventX.X1, testName, activityID, null);

    }

    /**
     * Do all the pre-run configuration of the test.
     */

    public void setUp() throws Exception {

        getLog().fine("Connecting to resource");
        getLog().fine("Installing and activating service");

        listener = new TraceNotificationListenerImpl();
        TraceMBeanProxy traceMBeanProxy = utils().getMBeanProxyFactory().createTraceMBeanProxy(
                utils().getSleeManagementMBeanProxy().getTraceMBean());
        traceMBeanProxy.addNotificationListener(listener, null, null);

        // utils().getSleeManagementMBeanProxy().addNotificationListener(this,
        // null, null);

        // Install the Deployable Units
        String duPath = utils().getTestParams().getProperty(DescriptionKeys.SERVICE_DU_PATH_PARAM);
        duID = utils().install(duPath);

        // Activate the DU
        utils().activateServices(duID, true);

    }

    /**
     * Clean up after the test.
     */

    public void tearDown() throws Exception {
        // cleanup
        super.tearDown();
    }

    public class TraceNotificationListenerImpl implements NotificationListener {
        public final void handleNotification(Notification notification, Object handback) {

            if (notification instanceof TraceNotification) {
                TraceNotification traceNotification = (TraceNotification) notification;

                if (traceNotification.getType().equals(SbbNotification.TRACE_NOTIFICATION_TYPE)) {
                    if (traceNotification.getMessage().equals(Test1113140Sbb.TRACE_MESSAGE)) {
                        receivedMessages++;
                        if (receivedMessages == 2)
                            result.setPassed();
                    }
                    else
                        result.setFailed(1113140, "Message in TraceNotification was not that set in Tracer.trace(): "
                            + traceNotification.getMessage() + ":" + Test1113140Sbb.TRACE_MESSAGE);
                return;
                }
            }

            return;
        }
    }

    private NotificationListener listener;

    private FutureResult result;

    private DeployableUnitID duID;

    private String testName = "Test1113140";

    private int assertionID;
    
    private int receivedMessages = 0;

}