/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.usage.common;

import com.opencloud.sleetck.lib.testutils.jmx.SbbUsageMBeanProxy;
import com.opencloud.sleetck.lib.TCKTestErrorException;

import javax.slee.management.ManagementException;
import javax.slee.usage.SampleStatistics;

/**
 * Extends SbbUsageMBeanProxy to provide generic, parameterised accessor methods
 * to the usage parameters.
 */
public interface UniversalUsageMBeanProxy extends SbbUsageMBeanProxy {

    /**
     * Invokes the getter method for the given counter-type usage parameter.
     * @param parameterName The usage parameter name. E.g. if 'foo', the proxy will invoke 'getFoo()'
     * @return the current value for the given parameter
     */
    public long getCounterParameter(String parameterName, boolean reset) throws ManagementException, TCKTestErrorException;

    /**
     * Invokes the getter method for the given sample-type usage parameter.
     * @param parameterName The usage parameter name. E.g. if 'foo', the proxy will invoke 'getFoo()'
     * @return the current value for the given parameter
     */
    public SampleStatistics getSampleParameter(String parameterName, boolean reset) throws ManagementException, TCKTestErrorException;

}
