/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.facilities.timerfacility.address;

public interface SetTimerAddressSbb1ActivityContextInterface extends javax.slee.ActivityContextInterface {

    // received2 - aliased
    public boolean getReceived2();
    public void setReceived2(boolean received);

    // received3 - aliased
    public boolean getReceived3();
    public void setReceived3(boolean received);

}
