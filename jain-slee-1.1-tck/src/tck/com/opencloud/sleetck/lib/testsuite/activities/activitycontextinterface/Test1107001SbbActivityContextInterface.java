/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.activities.activitycontextinterface;

import javax.slee.ActivityContextInterface;

public interface Test1107001SbbActivityContextInterface extends ActivityContextInterface {

    // Activity Context attributes
    public void setAValue(int value);
    public int getAValue();

    public void setBValue(int value);
    public int getBValue();

    public void setCValue(int value);
    public int getCValue();

}
