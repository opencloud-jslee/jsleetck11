/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.profiles.readonly;

import java.util.HashMap;

import javax.naming.Context;
import javax.slee.ActivityContextInterface;
import javax.slee.RolledBackContext;
import javax.slee.TransactionRolledbackLocalException;
import javax.slee.profile.ProfileFacility;
import javax.slee.profile.ProfileID;
import javax.slee.profile.ProfileTable;
import javax.slee.profile.ReadOnlyProfileException;
import javax.slee.profile.UnrecognizedProfileNameException;
import javax.slee.profile.UnrecognizedProfileTableNameException;

import com.opencloud.sleetck.lib.resource.events.TCKResourceEventX;
import com.opencloud.sleetck.lib.sbbutils.BaseTCKSbb;
import com.opencloud.sleetck.lib.sbbutils.TCKSbbUtils;
import com.opencloud.sleetck.lib.sbbutils.events2.SbbBaseMessageComposer;

public abstract class ReadOnlyIsTrueTestsSbb extends BaseTCKSbb {

    public static final String PROFILE_TABLE_NAME = "ReadOnlyIsTrueTestsSbbProfileTable";
    public static final String PROFILE_NAME = "ReadOnlyIsTrueTestsSbbProfile";
    public static final String PROFILE_NAME2 = "ReadOnlyIsTrueTestsSbbProfile2";

    public static final int CMP_SET = 0;
    public static final int PROF_LOCAL_SET = 1;
    public static final int PROF_LOCAL_INDIR_SET = 2;
    public static final int PROF_TAB_CREATE = 3;
    public static final int PROF_TAB_REMOVE = 4;
    public static final int PROF_LOCAL_REMOVE = 5;


    private void sendLogMsgCall (String msg) {
        HashMap map = SbbBaseMessageComposer.getLogMsg(msg);
        try {
            TCKSbbUtils.getResourceInterface().callTest(map);
        }
        catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }
    }

    private void sendResult(boolean result, int id, String msg) {
        HashMap map = SbbBaseMessageComposer.getSetResultMsg(result, id, msg);
        try {
            TCKSbbUtils.getResourceInterface().sendSbbMessage(map);

        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }
    }

    public void onTCKResourceEventX1(TCKResourceEventX event, ActivityContextInterface aci) {
        try {

            int operationID = ((Integer)event.getMessage()).intValue();
            Context env = TCKSbbUtils.getSbbEnvironment();
            ProfileFacility profileFacility = (ProfileFacility)env.lookup("slee/facilities/profile");
            ProfileTable profileTable = profileFacility.getProfileTable(PROFILE_TABLE_NAME);
            ReadOnlyTestsProfileLocal profileLocal = null;

            switch (operationID) {
            case CMP_SET:
                //1110071: Profile may not be modified via set on CMP interface
                //1110625: ReadOnlyProfileException is translated into an UnsupportedOperationException
                ReadOnlyTestsProfileCMP profileCMP = getProfileCMP(new ProfileID(PROFILE_TABLE_NAME, PROFILE_NAME));
                try {
                    profileCMP.setValue("newValue");
                    sendResult(false, 1110071, "Set accessor method on ProfileCMP interface was successful but should have " +
                            "failed as profile spec is marked as read-only.");
                }
                catch ( UnsupportedOperationException e) {
                    sendResult(true, 1110625, "Caught expected exception when setting new profile value via ProfileCMP interface: "+e.getClass().getName());
                }
                catch (Exception e) {
                    sendResult(false, 1110625, "Wrong type of exception received: "+e.getClass().getName()+
                            " Expected: java.lang.UnsupportedOperationException");
                }

                break;
            case PROF_LOCAL_SET:
                //1110071: Profile may not be modified via set on Local interface
                profileLocal = (ReadOnlyTestsProfileLocal) profileTable.find(PROFILE_NAME);
                try {
                    profileLocal.setValue("newValue");
                    sendResult(false, 1110071,"Set accessor method on ProfileLocal interface was successful but should have " +
                            "failed as profile spec is marked as read-only.");
                }
                catch ( TransactionRolledbackLocalException e) {
                    sendLogMsgCall("Caught expected exception when setting new profile value via ProfileLocal interface: "+e.getClass().getName());
                }
                catch (Exception e) {
                    sendResult(false, 1110071,"Wrong type of exception received: "+e.getClass().getName()+
                            " Expected: javax.slee.TransactionRolledbackLocalException");
                }

                //1110072: In the case of a method on the Profile Local interface being invoked,
                //the ReadOnlyProfileException thrown from the CMP setter method in the Profile object
                //causes the current transaction to be marked for rollback if the
                //ReadOnlyProfileException propagates unhandled out of the Profile object
                if (!getSbbContext().getRollbackOnly()) {
                    sendResult(false, 1110072, "Current TXN context should have been marked for rollback " +
                            "as the raised ReadOnlyProfileException propagated unhandled out of the Profile object.");
                } else {
                    sendResult(true, 1110071, "PROF_LOCAL_SET completed as expected.");
                }

                break;
            case PROF_LOCAL_INDIR_SET:
                //1110071: Profile may not be modified via a call to a Local interface business method whose implementation calls a CMP set accessor
                profileLocal = (ReadOnlyTestsProfileLocal) profileTable.find(PROFILE_NAME);
                try {
                    profileLocal.localSetValue("newValue");
                    sendResult(false, 1110071,"Invoking business method on ProfileLocal interface that indirectly calls CMP set accessor" +
                            " was successful but should have failed as profile spec is marked as read-only.");
                }
                catch ( TransactionRolledbackLocalException e) {
                    sendResult(true, 1110071, "Caught expected exception when setting new profile value indirectly via business method on ProfileLocal interface: "+e.getClass().getName());
                }
                catch (Exception e) {
                    sendResult(false, 1110071, "Wrong type of exception received: "+e.getClass().getName()+
                            " Expected: javax.slee.TransactionRolledbackLocalException");
                }
                break;
            case PROF_TAB_CREATE:
                //1110073: Sbb may not create a profile via ProfileTable interface
                try {
                    profileTable.create(PROFILE_NAME2);
                    sendResult(false, 1110073, "Creating profile " +PROFILE_NAME2 +" via ProfileTable interface "+
                            " was successful but should have failed as profile spec is marked as read-only.");
                }
                catch ( ReadOnlyProfileException e) {
                    sendResult(true, 1110073, "Caught expected exception when creating profile via ProfileTable interface: "+e.getClass().getName());
                }
                catch (Exception e) {
                    sendResult(false, 1110073, "Wrong type of exception received: "+e.getClass().getName()+" Expected: javax.slee.profile.ReadOnlyProfileException");
                }
                break;
            case PROF_TAB_REMOVE:
                //1110073: Sbb may not remove a profile via ProfileTable interface
                try {
                    profileTable.remove(PROFILE_NAME);
                    sendResult(false, 1110073, "Removing profile " +PROFILE_NAME +" via ProfileTable interface "+
                            " was successful but should have failed as profile spec is marked as read-only.");
                }
                catch ( ReadOnlyProfileException e) {
                    sendResult(true, 1110073, "Caught expected exception when removing profile via ProfileTable interface: "+e.getClass().getName());
                }
                catch (Exception e) {
                    sendResult(false, 1110073, "Wrong type of exception received: "+e.getClass().getName()+" Expected: javax.slee.profile.ReadOnlyProfileException");
                }
                break;
            case PROF_LOCAL_REMOVE:
                //1110074: Sbb may not remove a profile via ProfileLocal interface
                try {
                    profileLocal = (ReadOnlyTestsProfileLocal) profileTable.find(PROFILE_NAME);
                    profileLocal.remove();
                    sendResult(false, 1110074, "Removing profile " +PROFILE_NAME +" via ProfileLocal interface "+
                            " was successful but should have failed as profile spec is marked as read-only.");
                }
                catch ( TransactionRolledbackLocalException e) {
                    sendResult(true, 1110074, "Caught expected exception when removing profile via ProfileLocal interface: "+e.getClass().getName());
                }
                catch (Exception e) {
                    sendResult(false, 1110074, "Wrong type of exception received: "+e.getClass().getName()+" Expected: javax.slee.TransactionRolledbackLocalException");
                }
                break;
            }

        } catch(Exception e) {
            TCKSbbUtils.handleException(e);
        }
    }

    public void sbbRolledBack(RolledBackContext context) {
        //ignore rollback caused by ReadOnlyProfileExceptions from tests perspective
    }

    public abstract ReadOnlyTestsProfileCMP getProfileCMP(ProfileID profileID) throws UnrecognizedProfileTableNameException, UnrecognizedProfileNameException;

}
