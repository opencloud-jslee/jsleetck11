/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.facilities.alarmfacility;

import javax.management.Notification;
import javax.management.NotificationListener;
import javax.slee.facilities.AlarmLevel;
import javax.slee.management.AlarmNotification;
import javax.slee.management.DeployableUnitID;
import javax.slee.management.SbbNotification;

import com.opencloud.sleetck.lib.AbstractSleeTCKTest;
import com.opencloud.sleetck.lib.DescriptionKeys;
import com.opencloud.sleetck.lib.resource.TCKActivityID;
import com.opencloud.sleetck.lib.resource.events.TCKResourceEventX;
import com.opencloud.sleetck.lib.resource.testapi.TCKResourceTestInterface;
import com.opencloud.sleetck.lib.testutils.FutureResult;
import com.opencloud.sleetck.lib.testutils.jmx.AlarmMBeanProxy;

/*  
 *  AssertionID(1113507): Test If one or more alarms are cleared 
 *  by this method, the SLEE’s AlarmMBean object emits an alarm 
 *  notification for each cleared alarm with the alarm level set 
 *  to AlarmLevel.CLEAR.
 *    
 *  AssertionID(1113509): Test The alarmType argument. This argument 
 *  specifies the alarm type of the alarms to be cleared.
 *  
 *  AssertionID(1113510): Test The clearAlarms(String) method throws 
 *  a java.lang.NullPointerException if the alarmID argument is null.
 *  
 */

public class Test1113507Test extends AbstractSleeTCKTest {

    /**
     * Perform the actual test.
     */
    public void run(FutureResult result) throws Exception {
        this.result = result;

        expectedAlarmNotifications = 15;
        receivedAlarmNotifications = 0;

        String activityName = "Test1113507Test";
        TCKResourceTestInterface resource = utils().getResourceInterface();
        TCKActivityID activityID = resource.createActivity(activityName);
        getLog().info("Firing event: " + TCKResourceEventX.X1);

        // kickoff the test
        resource.fireEvent(TCKResourceEventX.X1, testName, activityID, null);

        synchronized (this) {
            wait(utils().getTestTimeout());
        }

        if (expectedAlarmNotifications == receivedAlarmNotifications)
            result.setPassed();
        else
            result.setFailed(1113507, "Expected number of alarm messages not received, alarms were not delivered by "
                    + "the AlarmFacility (expected " + expectedAlarmNotifications + ", received "
                    + receivedAlarmNotifications + ")");

    }

    /**
     * Do all the pre-run configuration of the test.
     */

    public void setUp() throws Exception {

        getLog().fine("Connecting to resource");
        getLog().fine("Installing and activating service");

        // Install the Deployable Units
        String duPath = utils().getTestParams().getProperty(DescriptionKeys.SERVICE_DU_PATH_PARAM);
        duID = utils().install(duPath);

        // Activate the DU
        utils().activateServices(duID, true);

        alarmMBeanProxy = utils().getAlarmMBeanProxy();
        listener = new AlarmNotificationListenerImpl();
        alarmMBeanProxy.addNotificationListener(listener, null, null);
    }

    /**
     * Clean up after the test.
     */

    public void tearDown() throws Exception {

        String[] listActiveAlarms = alarmMBeanProxy.getAlarms();
        if (alarmMBeanProxy.getAlarms().length > 0)
            for (int i = 0; i < listActiveAlarms.length; i++)
                alarmMBeanProxy.clearAlarm(listActiveAlarms[i]);

        if (null != alarmMBeanProxy)
            alarmMBeanProxy.removeNotificationListener(listener);
        // cleanup
        super.tearDown();
    }

    public class AlarmNotificationListenerImpl implements NotificationListener {
        public final void handleNotification(Notification notification, Object handback) {

            if (notification instanceof AlarmNotification) {
                AlarmNotification alarmNotification = (AlarmNotification) notification;

                getLog().info("Received " + alarmNotification);
                // 1113509
                if (alarmNotification.getAlarmLevel().equals(AlarmLevel.CLEAR)) {
                    String clearedAlarmType = alarmNotification.getAlarmType();
                    SbbNotification sbbNotification = (SbbNotification) alarmNotification.getNotificationSource();
                    try {
                        if (alarmMBeanProxy.clearAlarms(sbbNotification, clearedAlarmType) >= 1) {
                            result.setFailed(1113509, "Some active alarms in the SLEE were cleared, but"
                                    + "there should not be any active alarms in the SLEE.");
                            return;
                        }
                    } catch (Exception e) {
                        getLog().warning("Received Exception from SBB or resource:");
                        getLog().warning(e);
                        result.setError(e);
                    }
                }

                // 1113507
                if (alarmNotification.getAlarmLevel().equals(AlarmLevel.CLEAR)) {
                    if (alarmNotification.getMessage().equals(Test1113507Sbb.ALARM_MESSAGE_CRITICAL))
                        receivedAlarmNotifications = receivedAlarmNotifications + AlarmLevel.LEVEL_CRITICAL;
                    else if (alarmNotification.getMessage().equals(Test1113507Sbb.ALARM_MESSAGE_MAJOR))
                        receivedAlarmNotifications = receivedAlarmNotifications + AlarmLevel.LEVEL_MAJOR;
                    else if (alarmNotification.getMessage().equals(Test1113507Sbb.ALARM_MESSAGE_WARNING))
                        receivedAlarmNotifications = receivedAlarmNotifications + AlarmLevel.LEVEL_WARNING;
                    else if (alarmNotification.getMessage().equals(Test1113507Sbb.ALARM_MESSAGE_INDETERMINATE))
                        receivedAlarmNotifications = receivedAlarmNotifications + AlarmLevel.LEVEL_INDETERMINATE;
                    else if (alarmNotification.getMessage().equals(Test1113507Sbb.ALARM_MESSAGE_MINOR))
                        receivedAlarmNotifications = receivedAlarmNotifications + AlarmLevel.LEVEL_MINOR;
                }
            }

            getLog().info("Notification received: " + notification);
            return;
        }
    }

    private NotificationListener listener;

    private DeployableUnitID duID;

    private AlarmMBeanProxy alarmMBeanProxy;

    private FutureResult result;

    private int receivedAlarmNotifications;

    private int expectedAlarmNotifications;
    
    private String testName = "Test1113507";
}
