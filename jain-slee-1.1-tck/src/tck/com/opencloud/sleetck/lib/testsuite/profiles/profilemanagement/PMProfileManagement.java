/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.profiles.profilemanagement;

import javax.slee.profile.ProfileID;

/**
 * Define a profile management interface for the PM profile
 */
public interface PMProfileManagement {
    // Management interface methods
    public void testExceptionWrapper() throws Exception;
    public void testMarkDirty();
    public boolean testIsProfileValid(ProfileID profileID);

    public void setValueIndirectly(String value);

    // CMP Methods
    public void setValue(String value);
    public String getValue();
}
