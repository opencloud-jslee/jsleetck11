/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.sbblocal;

import javax.slee.ActivityEndEvent;
import javax.slee.ActivityContextInterface;
import javax.slee.SbbContext;
import javax.slee.Address;
import javax.slee.SbbLocalObject;
import javax.slee.facilities.Level;
import javax.slee.ChildRelation;

import javax.slee.nullactivity.NullActivityFactory;
import javax.slee.nullactivity.NullActivity;
import javax.slee.nullactivity.NullActivityContextInterfaceFactory;
import javax.slee.facilities.ActivityContextNamingFacility;

import com.opencloud.sleetck.lib.sbbutils.BaseTCKSbb;
import com.opencloud.sleetck.lib.sbbutils.TCKSbbUtils;
import com.opencloud.sleetck.lib.sbbutils.TCKSbbConstants;
import com.opencloud.sleetck.lib.resource.events.TCKResourceEventX;
import com.opencloud.sleetck.lib.resource.TCKActivityID;
import com.opencloud.sleetck.lib.resource.sbbapi.TCKActivity;
import com.opencloud.sleetck.lib.resource.sbbapi.TCKResourceSbbInterface;
import com.opencloud.sleetck.lib.resource.sbbapi.TCKActivityContextInterfaceFactory;

import java.util.HashMap;
import java.util.Iterator;


/**
 * It removes the SBB entity from the ChildRelation object that the SBB
 *  entity belongs to.
 */

public abstract class Test296Sbb extends BaseTCKSbb {

    public void onTCKResourceEventX1(TCKResourceEventX event, ActivityContextInterface aci) {
        try {

            // HashMap for the test result.
            HashMap map = new HashMap();

            // Get the ChildRelation object - we could just call getChild(). each time too.
            ChildRelation childRelation = getChild();

            // Create the child.
            SbbLocalObject child = childRelation.create();


            for (int i = -128; i <= 127; i++) {
                try {

                    child.setSbbPriority((byte) i);

                    if (child.getSbbPriority() < -128 || child.getSbbPriority() > 127) {
                        map.put("Result", new Boolean(false));
                        map.put("Message", "The value returned by SbbLocalObject.getSbbPriority() was outside the valid range.");
                        TCKSbbUtils.getResourceInterface().sendSbbMessage(map);
                        return;
                    }
                } catch (Exception e) {
                    map.put("Result", new Boolean(false));
                    map.put("Message", "A valid value was given to setSbbPriority(), yet an exception was thrown.");
                    TCKSbbUtils.getResourceInterface().sendSbbMessage(map);
                    return;
                }
            }

            map.put("Result", new Boolean(true));
            map.put("Message", "Ok");
            TCKSbbUtils.getResourceInterface().sendSbbMessage(map);
            return;

        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }
    }

    public abstract ChildRelation getChild();

}
