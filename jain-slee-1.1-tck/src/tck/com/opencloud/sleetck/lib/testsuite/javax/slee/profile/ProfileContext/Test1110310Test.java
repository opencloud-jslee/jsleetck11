/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.javax.slee.profile.ProfileContext;

import javax.management.Notification;
import javax.management.NotificationListener;
import javax.management.ObjectName;
import javax.slee.management.ProfileTableNotification;
import javax.slee.management.TraceNotification;
import javax.slee.profile.ProfileSpecificationID;

import com.opencloud.logging.StdErrLog;
import com.opencloud.sleetck.lib.AbstractSleeTCKTest;
import com.opencloud.sleetck.lib.TCKTestFailureException;
import com.opencloud.sleetck.lib.TCKTestResult;
import com.opencloud.sleetck.lib.infra.SleeTCKComponentConstants;
import com.opencloud.sleetck.lib.profileutils.BaseMessageAdapter;
import com.opencloud.sleetck.lib.profileutils.BaseMessageSender;
import com.opencloud.sleetck.lib.rautils.RMIObjectChannel;
import com.opencloud.sleetck.lib.rautils.TCKRAUtils;
import com.opencloud.sleetck.lib.testutils.FutureAssertionsResult;
import com.opencloud.sleetck.lib.testutils.ProfileUtils;
import com.opencloud.sleetck.lib.testutils.jmx.ProfileMBeanProxy;
import com.opencloud.sleetck.lib.testutils.jmx.ProfileProvisioningMBeanProxy;
import com.opencloud.sleetck.lib.testutils.jmx.SleeManagementMBeanProxy;
import com.opencloud.sleetck.lib.testutils.jmx.TraceMBeanProxy;
import com.opencloud.sleetck.lib.testutils.jmx.impl.TraceMBeanProxyImpl;


public class Test1110310Test extends AbstractSleeTCKTest {

    private static final String DU_PATH_PARAM= "DUPath";

    private static final String SPEC_NAME = "Test1110310Profile";
    private static final String SPEC_VERSION = "1.0";

    private static final int TEST_ID = 1110310;

    /**
     * This test checks various assertions for the javax.slee.profile.ProfileContext
     * class as outlined in the JAIN SLEE 1.1 API JavaDoc
     */
    public TCKTestResult run() throws Exception {

        TCKTestResult res;
        exResult = null;

        futureResult = new FutureAssertionsResult(new int[]{1110310, 1110311, 1110315, 1110319, 1110324, 1110327, 1110328, 1110334}, getLog());

        //create a profile table
        ProfileSpecificationID specID = new ProfileSpecificationID(SPEC_NAME, SleeTCKComponentConstants.TCK_VENDOR, SPEC_VERSION);
        profileProvisioning.createProfileTable(specID, Test1110310Profile.PROFILE_TABLE_NAME);
        getLog().fine("Added profile table "+Test1110310Profile.PROFILE_TABLE_NAME+" based on profile spec "+SPEC_NAME);

        //check messages and wait till 'passed' messages for all the assertions specified above have
        //come in
        res = futureResult.waitForResult(utils().getTestTimeout());
        if (!res.isPassed())
            return res;


        //create a profile
        ObjectName profile = profileProvisioning.createProfile(Test1110310Profile.PROFILE_TABLE_NAME, Test1110310Profile.PROFILE_NAME);
        ProfileMBeanProxy profileProxy = utils().getMBeanProxyFactory().createProfileMBeanProxy(profile);
        getLog().fine("Created profile "+Test1110310Profile.PROFILE_NAME+" for profile table "+Test1110310Profile.PROFILE_TABLE_NAME);
        profileProxy.commitProfile();
        profileProxy.closeProfile();


        //call management method on PROFILE_NAME, this should trigger the Tracer notifications
        //wait till the test has come back with success msgs for the expected assertionID's, or time out
        getLog().fine("Call management method on profile, this should also trigger the Tracer tests.");
        futureResult = new FutureAssertionsResult(new int[]{1110339, 1110340}, getLog());
        profile = profileProvisioning.getProfile(Test1110310Profile.PROFILE_TABLE_NAME, Test1110310Profile.PROFILE_NAME);
        utils().getMBeanFacade().invoke(profile, "manage", new Object[0], new String[0]);
        res = futureResult.waitForResult(utils().getTestTimeout());
        if (!res.isPassed())
            return res;


        //call special management method on Default Profile
        getLog().fine("Call management method on default profile.");
        futureResult = new FutureAssertionsResult(new int[]{1110756}, getLog());
        ObjectName defaultProfile = profileProvisioning.getDefaultProfile(Test1110310Profile.PROFILE_TABLE_NAME);
        utils().getMBeanFacade().invoke(defaultProfile, "invokeOnDefaultProfile", new Object[0], new String[0]);
        res = futureResult.waitForResult(utils().getTestTimeout());
        if (!res.isPassed())
            return res;

        //remove PROFILE_NAME
        getLog().fine("Remove profile "+Test1110310Profile.PROFILE_NAME);
        profileProvisioning.removeProfile(Test1110310Profile.PROFILE_TABLE_NAME, Test1110310Profile.PROFILE_NAME);


        //give the SLEE 'some time' to complete the removal and detect errors during
        //the profileRemove etc. callback methods. In case the SLEE implementation
        //is single-threaded this call would be more or less meaningless
        Thread.sleep(2000);

        if (exResult!=null)
            return exResult;

        return TCKTestResult.passed();
    }

    public void setPassed(int assertionID, String msg) {
        getLog().fine(assertionID+": "+msg);
        if (futureResult!=null && !futureResult.isSet())
            futureResult.setPassed(assertionID);
    }

    public void setFailed(int assertionID, String msg, Exception e) {
        if (e==null) {
            getLog().fine("FAILURE: "+assertionID+":"+msg);
            if (futureResult!=null && !futureResult.isSet())
                futureResult.setFailed(assertionID, msg);

            //in case the currently active futureResult has already been set to passed() before the failure
            //comes in, we have to provide a mechanism to still detect this failure
            setIfEmpty(TCKTestResult.failed(assertionID, msg));
        }
        else {
            getLog().fine("FAILURE: "+assertionID+":"+msg);
            getLog().fine(e);
            if (futureResult!=null && !futureResult.isSet())
                futureResult.setFailed(new TCKTestFailureException(assertionID, msg, e));

            //in case the currently active futureResult has already been set to passed() before the failure
            //comes in, we have to provide a mechanism to still detect this failure
            setIfEmpty(TCKTestResult.failed(new TCKTestFailureException(assertionID, msg, e)));
        }
    }

    public void setError(String msg, Exception e) {
        if (e==null) {
            getLog().warning(msg);
            if (futureResult!=null && !futureResult.isSet())
                futureResult.setError(msg);

            //in case the currently active futureResult has already been set to passed() before the error
            //comes in, we have to provide a mechanism to still detect this error
            setIfEmpty(TCKTestResult.error(msg));
        }
        else {
            getLog().warning(msg);
            getLog().warning(e);
            if (futureResult!=null && !futureResult.isSet())
                futureResult.setError(msg, e);

            //in case the currently active futureResult has already been set to passed() before the error
            //comes in, we have to provide a mechanism to still detect this failure
            setIfEmpty(TCKTestResult.error(msg, e));
        }
    }


    public void setUp() throws Exception {

        setupService(DU_PATH_PARAM);

        in = utils().getRMIObjectChannel();
        in.setMessageHandler(new BaseMessageAdapter(getLog()) {
            public void onSetPassed(int assertionID, String msg) { setPassed(assertionID, msg); }
            public void onSetFailed(int assertionID, String msg, Exception e) { setFailed(assertionID, msg, e); }
            public void onLogCall(String msg) { getLog().fine(msg); }
            public void onSetError(String msg, Exception e) { setError(msg, e); }

        });

        profileUtils = new ProfileUtils(utils());
        profileProvisioning = profileUtils.getProfileProvisioningProxy();

        // Setup TraceNotificationListener
        SleeManagementMBeanProxy sleeManagementProxy = utils().getSleeManagementMBeanProxy();
        ObjectName traceMBeanName = sleeManagementProxy.getTraceMBean();
        notificationListener = new TraceNotificationListenerImpl();

        tracembeanProxy = new TraceMBeanProxyImpl(traceMBeanName, utils().getMBeanFacade());
        tracembeanProxy.addNotificationListener(notificationListener, null, null);

    }

    public void tearDown() throws Exception {

        in.clearQueue();

        if (null != tracembeanProxy)
            tracembeanProxy.removeNotificationListener(notificationListener);

        try {
            profileUtils.removeProfileTable(Test1110310Profile.PROFILE_TABLE_NAME);
        }
        catch (Exception e) {
            getLog().warning("Caught exception while trying to remove profile table:");
            getLog().warning(e);
        }

        super.tearDown();
    }

    public class TraceNotificationListenerImpl implements NotificationListener {
        public final void handleNotification(Notification notification, Object handback) {


            if (notification instanceof TraceNotification) {
                TraceNotification traceNotification = (TraceNotification) notification;

                if (notification.getMessage().equals(Test1110310Profile.TRACE_SUCCESS) ) {
                    //1110339: The notification source used by the tracer is a
                    //javax.slee.management.ProfileTableNotification that contains
                    //the name of the Profile Table the Profile Context is associated
                    //with.
                    RMIObjectChannel out;
                    BaseMessageSender msgSender;
                    try {
                        out = TCKRAUtils.lookupRMIObjectChannel();
                        msgSender = new BaseMessageSender(out, new StdErrLog());
                    }
                    catch (Exception e) {
                        getLog().warning("An error occured creating an RMIObjectChannel: ");
                        getLog().warning(e);
                        setIfEmpty(TCKTestResult.error("An error occured creating an RMIObjectChannel: ", e));
                        return;
                    }

                    if (traceNotification.getNotificationSource() instanceof ProfileTableNotification)
                        msgSender.sendSuccess(1110339, "Notification source is " +
                                "of type javax.slee.management.ProfileTableNotification as expected");
                    else
                        msgSender.sendFailure(1110339, "Notification source is " +
                                "of wrong type: "+ traceNotification.getNotificationSource().getClass().getName() +
                                " Expected javax.slee.management.ProfileTableNotification");

                    //1110340: Trace notifications generated by a tracer obtained
                    //using this method are of the type
                    //javax.slee.management.ProfileTableNotification.TRACE_NOTIFICATION_TYPE.
                    if (traceNotification.getType().equals(ProfileTableNotification.TRACE_NOTIFICATION_TYPE))
                        msgSender.sendSuccess(1110340, "Notification type is " +
                                "ProfileTableNotification.TRACE_NOTIFICATION_TYPE as expected");
                    else
                        msgSender.sendSuccess(1110340, "Notification type is wrong " +
                                traceNotification.getType()+" Expected ProfileTableNotification.TRACE_NOTIFICATION_TYPE");
                }

                return;
            }

            return;
        }
    }

    private void setIfEmpty(TCKTestResult exResult) {
        if (this.exResult==null)
            this.exResult = exResult;
    }

    private ProfileUtils profileUtils;
    private ProfileProvisioningMBeanProxy profileProvisioning;
    private NotificationListener notificationListener;
    private TraceMBeanProxy tracembeanProxy;

    private RMIObjectChannel in;
    private TCKTestResult exResult;
    private FutureAssertionsResult futureResult;

}
