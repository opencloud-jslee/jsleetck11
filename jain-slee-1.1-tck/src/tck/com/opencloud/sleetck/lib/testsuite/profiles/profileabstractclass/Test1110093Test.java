/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.profiles.profileabstractclass;

import javax.slee.CreateException;
import javax.slee.profile.Profile;
import javax.slee.profile.ProfileContext;
import javax.slee.profile.ProfileVerificationException;

import com.opencloud.sleetck.lib.AbstractSleeTCKTest;
import com.opencloud.sleetck.lib.DescriptionKeys;
import com.opencloud.sleetck.lib.TCKTestErrorException;
import com.opencloud.sleetck.lib.TCKTestFailureException;
import com.opencloud.sleetck.lib.TCKTestResult;

/**
 * non-public profile abstract class
 */
abstract class Test1110093_3Profile implements ProfileAbstractClassTestsProfileCMP, Profile  {
    public void setProfileContext(ProfileContext context) { }
    public void unsetProfileContext() {}
    public void profileInitialize() {}
    public void profilePostCreate() throws CreateException {}
    public void profileActivate() {}
    public void profilePassivate() {}
    public void profileLoad() {}
    public void profileStore() {}
    public void profileRemove() {}
    public void profileVerify() throws ProfileVerificationException {}
}

public class Test1110093Test extends AbstractSleeTCKTest {

    private static final String WITH_BUSINESS_DU_PATH_PARAM= "withBusinessDUPath";
    private static final String NO_BUSINESS_DU_PATH_PARAM= "noBusinessDUPath";
    private static final String NON_PUBLIC_DU_PATH_PARAM= "nonPublicDUPath";
    private static final String NON_ABSTRACT_DU_PATH_PARAM= "nonAbstractDUPath";
    private static final String NON_PUBLIC_CONSTRUCTOR_DU_PATH_PARAM= "nonPublicConstructDUPath";
    private static final String NO_NOARGS_DU_PATH_PARAM= "noNoArgsDUPath";
    private static final String CONSTRUCTOR_THROWS_DU_PATH_PARAM= "constructThrowsDUPath";
    private static final String IMPLEMENT_ACCESSORS_DU_PATH_PARAM= "implAccessorsDUPath";
    private static final String SIGNATURE_MATCH_DU_PATH_PARAM= "signatureMatchDUPath";
    private static final String NO_SIGNATURE_MATCH_DU_PATH_PARAM= "noSignatureMatchDUPath";
    private static final String NON_PUBLIC_METHOD_DU_PATH_PARAM= "nonPublicMethodDUPath";
    private static final String STATIC_BUSINESS_DU_PATH_PARAM= "staticDUPath";
    private static final String ABSTRACT_BUSINESS_DU_PATH_PARAM= "abstrDUPath";
    private static final String FINAL_BUSINESS_DU_PATH_PARAM= "finalDUPath";
    private static final String ABSTRACT_MANAGE_DU_PATH_PARAM= "mngAbstrDUPath";
    private static final String FINAL_MANAGE_DU_PATH_PARAM= "mngFinalDUPath";
    private static final String NO_USAGE_DU_PATH_PARAM= "noUsageMethodDUPath";
    private static final String USAGE_1_DU_PATH_PARAM= "usageMethod1DUPath";
    private static final String USAGE_2_DU_PATH_PARAM= "usageMethod2DUPath";
    private static final String USAGE_BOTH_DU_PATH_PARAM= "usageMethodBothDUPath";
    private static final String VALID_SUPER_DU_PATH_PARAM= "validSuperDUPath";
    private static final String INVALID_SUPER_DU_PATH_PARAM= "invalidSuperDUPath";

    /**
     * This test trys to deploy profile specs that define a ProfileLocal interface but
     * no Profile abstract class. In case the ProfileLocal interface contains business methods
     * the deployment should fail, in case there are only CMP accessor methods present
     * in the ProfileLocal interface the deployment should succeed.
     * Furthermore other invalid profile specs that violate various rules for
     * Profile abstract classes are deployed and should fail.
     */
    public TCKTestResult run() throws Exception {

        //Spec 1: Profile abstract class not defined, business methods present
        getLog().fine("Deploying profile spec.");
        try {
            setupService(WITH_BUSINESS_DU_PATH_PARAM);
            throw new TCKTestFailureException(1110093,"Deployment of Profile spec with Profile abstract class " +
                    "not defined though ProfileLocal interface is present and contains business methods was successful but should have failed.");
        }
        catch (TCKTestErrorException e) {
            getLog().fine("Caught expected exception when trying to deploy profile spec. Exception: "+e.getMessage());
        }

        //Spec 2: Profile abstract class not defined, no business methods present
        getLog().fine("Deploying profile spec.");
        try {
            setupService(NO_BUSINESS_DU_PATH_PARAM);
            getLog().fine("Deployment of profile spec successful.");
        }
        catch (TCKTestErrorException e) {
            throw new TCKTestFailureException(1110093,"Deployment of Profile spec with Profile abstract class " +
            "not defined and ProfileLocal interface is present but does not contain business methods failed but should have been successful.");
        }

        //Spec 3: Profile abstract class not public
        getLog().fine("Deploying profile spec.");
        try {
            setupService(NON_PUBLIC_DU_PATH_PARAM);
            throw new TCKTestFailureException(1110211,"Deployment of Profile spec with Profile abstract class " +
                "not public succeeded but should have failed.");
        }
        catch (TCKTestErrorException e) {
            getLog().fine("Caught expected exception when trying to deploy profile spec. Exception: "+e.getMessage());
        }

        //Spec 4: Profile abstract class not abstract
        getLog().fine("Deploying profile spec.");
        try {
            setupService(NON_ABSTRACT_DU_PATH_PARAM);
            throw new TCKTestFailureException(1110211,"Deployment of Profile spec with Profile abstract class " +
                "not abstract succeeded but should have failed.");
        }
        catch (TCKTestErrorException e) {
            getLog().fine("Caught expected exception when trying to deploy profile spec. Exception: "+e.getMessage());
        }

        //Spec 5: No-args constructor is not public
        getLog().fine("Deploying profile spec.");
        try {
            setupService(NON_PUBLIC_CONSTRUCTOR_DU_PATH_PARAM);
            throw new TCKTestFailureException(1110212,"Deployment of Profile spec with no public no-args constructor present in Profile abstract class " +
                "succeeded but should have failed.");
        }
        catch (TCKTestErrorException e) {
            getLog().fine("Caught expected exception when trying to deploy profile spec. Exception: "+e.getMessage());
        }

        //Spec 6: no no-args constructor
        getLog().fine("Deploying profile spec.");
        try {
            setupService(NO_NOARGS_DU_PATH_PARAM);
            throw new TCKTestFailureException(1110212,"Deployment of Profile spec with no no-args constructor present in Profile abstract class " +
                "succeeded but should have failed.");
        }
        catch (TCKTestErrorException e) {
            getLog().fine("Caught expected exception when trying to deploy profile spec. Exception: "+e.getMessage());
        }

        //Spec 7: no-args constructor has a throws clause
        getLog().fine("Deploying profile spec.");
        try {
            setupService(CONSTRUCTOR_THROWS_DU_PATH_PARAM);
            throw new TCKTestFailureException(1110212,"Deployment of Profile spec with no-args constructor declaring a throws-clause " +
                "succeeded but should have failed.");
        }
        catch (TCKTestErrorException e) {
            getLog().fine("Caught expected exception when trying to deploy profile spec. Exception: "+e.getMessage());
        }

        //Spec 8: Profile abstract class implements the CMP accessors
        getLog().fine("Deploying profile spec.");
        try {
            setupService(IMPLEMENT_ACCESSORS_DU_PATH_PARAM);
            throw new TCKTestFailureException(1110214,"Deployment of Profile spec with Profile abstract class implementing the CMP accessor methods " +
                "succeeded but should have failed.");
        }
        catch (TCKTestErrorException e) {
            getLog().fine("Caught expected exception when trying to deploy profile spec. Exception: "+e.getMessage());
        }

        //Spec 9: Multiple business methods present, Profile abstract class defines implementations with matching+non-matching signatures
        //though return types are different
        getLog().fine("Deploying profile spec.");
        try {
            setupService(SIGNATURE_MATCH_DU_PATH_PARAM);
            getLog().fine("Deployment of profile spec successful.");
        }
        catch (TCKTestErrorException e) {
            throw new TCKTestFailureException(1110217,"Deployment of Profile spec with Profile abstract class " +
            "implementing business methods with correct signature failed but should have been successful.");
        }

        //Spec 10: Multiple business methods present, Profile abstract class does not define implementations with matching signatures
        //only non-matching signatures(arguments are different, name matches)
        getLog().fine("Deploying profile spec.");
        try {
            setupService(NO_SIGNATURE_MATCH_DU_PATH_PARAM);
            throw new TCKTestFailureException(1110217,"Deployment of Profile spec with Profile abstract class not implementing business methods with matching signatures " +
                "succeeded but should have failed.");
        }
        catch (TCKTestErrorException e) {
            getLog().fine("Caught expected exception when trying to deploy profile spec. Exception: "+e.getMessage());
        }

        //Spec 11: Business method implementations are not public
        getLog().fine("Deploying profile spec.");
        try {
            setupService(NON_PUBLIC_METHOD_DU_PATH_PARAM);
            throw new TCKTestFailureException(1110218,"Deployment of Profile spec with business method implementations not being public " +
                "succeeded but should have failed.");
        }
        catch (TCKTestErrorException e) {
            getLog().fine("Caught expected exception when trying to deploy profile spec. Exception: "+e.getMessage());
        }

        //Spec 12: Business method implementations are static
        getLog().fine("Deploying profile spec.");
        try {
            setupService(STATIC_BUSINESS_DU_PATH_PARAM);
            throw new TCKTestFailureException(1110218,"Deployment of Profile spec with business method implementations being static " +
                "succeeded but should have failed.");
        }
        catch (TCKTestErrorException e) {
            getLog().fine("Caught expected exception when trying to deploy profile spec. Exception: "+e.getMessage());
        }

        //Spec 13: Business method implementations are abstract
        getLog().fine("Deploying profile spec.");
        try {
            setupService(ABSTRACT_BUSINESS_DU_PATH_PARAM);
            throw new TCKTestFailureException(1110218,"Deployment of Profile spec with business method implementations being abstract " +
                "succeeded but should have failed.");
        }
        catch (TCKTestErrorException e) {
            getLog().fine("Caught expected exception when trying to deploy profile spec. Exception: "+e.getMessage());
        }

        //Spec 14: Business method implementations are final
        getLog().fine("Deploying profile spec.");
        try {
            setupService(FINAL_BUSINESS_DU_PATH_PARAM);
            throw new TCKTestFailureException(1110218,"Deployment of Profile spec with business method implementations being final " +
                "succeeded but should have failed.");
        }
        catch (TCKTestErrorException e) {
            getLog().fine("Caught expected exception when trying to deploy profile spec. Exception: "+e.getMessage());
        }

        //Spec 15: Management method implementations are abstract
        getLog().fine("Deploying profile spec.");
        try {
            setupService(ABSTRACT_MANAGE_DU_PATH_PARAM);
            throw new TCKTestFailureException(1110222,"Deployment of Profile spec with management method implementations being abstract " +
                "succeeded but should have failed.");
        }
        catch (TCKTestErrorException e) {
            getLog().fine("Caught expected exception when trying to deploy profile spec. Exception: "+e.getMessage());
        }

        //Spec 16: Management method implementations are final
        getLog().fine("Deploying profile spec.");
        try {
            setupService(FINAL_MANAGE_DU_PATH_PARAM);
            throw new TCKTestFailureException(1110222,"Deployment of Profile spec with management method implementations being final " +
                "succeeded but should have failed.");
        }
        catch (TCKTestErrorException e) {
            getLog().fine("Caught expected exception when trying to deploy profile spec. Exception: "+e.getMessage());
        }

        //Spec 17: GetUsageParameterSet methods are both missing
        getLog().fine("Deploying profile spec.");
        try {
            setupService(NO_USAGE_DU_PATH_PARAM);
            throw new TCKTestFailureException(1110223,"Deployment of Profile spec with missing getUsageParameterSet methods in Profile abstract class " +
                "succeeded but should have failed.");
        }
        catch (TCKTestErrorException e) {
            getLog().fine("Caught expected exception when trying to deploy profile spec. Exception: "+e.getMessage());
        }

        //Spec 18: GetDefaultUsageParameterSet defined
        getLog().fine("Deploying profile spec.");
        try {
            setupService(USAGE_1_DU_PATH_PARAM);
            getLog().fine("Deployment of profile spec successful.");
        }
        catch (TCKTestErrorException e) {
            throw new TCKTestFailureException(1110223,"Deployment of Profile spec with Profile abstract class " +
            "defining GetDefaultUsageParameterSet() failed but should have been successful.");
        }

        //Spec 19: GetUsageParameterSet(String name) defined
        getLog().fine("Deploying profile spec.");
        try {
            setupService(USAGE_2_DU_PATH_PARAM);
            getLog().fine("Deployment of profile spec successful.");
        }
        catch (TCKTestErrorException e) {
            throw new TCKTestFailureException(1110223,"Deployment of Profile spec with Profile abstract class " +
            "defining GetUsageParameterSet(String name) failed but should have been successful.");
        }

        //Spec 20: Both methods defined in profile abstract class
        getLog().fine("Deploying profile spec.");
        try {
            setupService(USAGE_BOTH_DU_PATH_PARAM);
            getLog().fine("Deployment of profile spec successful.");
        }
        catch (TCKTestErrorException e) {
            throw new TCKTestFailureException(1110223,"Deployment of Profile spec with Profile abstract class " +
            "defining both getUsageParameter methods failed but should have been successful.");
        }

        //Spec 21: Profile abstract class has super class adhering to all rules
        getLog().fine("Deploying profile spec.");
        try {
            setupService(VALID_SUPER_DU_PATH_PARAM);
            getLog().fine("Deployment of profile spec successful.");
        }
        catch (TCKTestErrorException e) {
            throw new TCKTestFailureException(1110224,"Deployment of Profile spec with Profile abstract class " +
            "subclassed from a class that adheres to all rules failed but should have been successful.");
        }

        //Spec 22: Super class breaks rule (not defining all business methods)
        getLog().fine("Deploying profile spec.");
        try {
            setupService(INVALID_SUPER_DU_PATH_PARAM);
            throw new TCKTestFailureException(1110224,"Deployment of Profile spec with super class not adhering to rules for Profile abstract classes " +
                "succeeded but should have failed.");
        }
        catch (TCKTestErrorException e) {
            getLog().fine("Caught expected exception when trying to deploy profile spec. Exception: "+e.getMessage());
        }

        return TCKTestResult.passed();
    }

    public void setUp() throws Exception {
    }
}
