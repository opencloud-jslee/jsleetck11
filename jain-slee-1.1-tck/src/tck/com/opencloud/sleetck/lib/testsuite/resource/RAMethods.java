/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.resource;

/**
 * Method IDs shared by the various RA tests.
 */
public class RAMethods {

    // Base Resource Adapator methods
    public final static int activityEnded = 1;
    public final static int activityUnreferenced = 2;
    public final static int administrativeRemove = 3;
    public final static int raConfigurationUpdate = 4;
    public final static int raConfigure = 5;
    public final static int raUnconfigure = 6;
    public final static int raActive = 7;
    public final static int raInactive = 8;
    public final static int raStopping = 9;
    public final static int raVerifyConfiguration = 10;
    public final static int eventProcessingFailed = 11;
    public final static int eventProcessingSuccessful = 12;
    public final static int eventUnreferenced = 13;
    public final static int getActivity = 14;
    public final static int getActivityHandle = 15;
    public final static int getMarshaler = 16;
    public final static int getResourceAdaptorInterface = 17;
    public final static int queryLiveness = 18;
    public final static int serviceActive = 19;
    public final static int serviceInactive = 20;
    public final static int serviceStopping = 21;
    public final static int setResourceAdaptorContext = 22;
    public final static int unsetResourceAdaptorContext = 23;
    public final static int constructor = 24;
    public final static int finalizer = 25;

    // ResourceAdaptorContext
    public final static int getAlarmFacility = 26;
    public final static int getEventLookupFacility = 28;
    public final static int getDefaultUsageParameterSet = 29;
    public final static int getUsageParameterSet = 30;
    public final static int getProfileTable = 31;
    public final static int getInvokingService = 32;
    public final static int getSleeEndpoint = 33;
    public final static int getSleeTransactionManager = 34;
    public final static int getEntityName = 35;
    public final static int getTimer = 36;
    public final static int getTracer = 37;
    public final static int getResourceAdaptor = 38;
    public final static int getResourceAdaptorTypes = 39;

    // SleeEndpoint

    public final static int endActivity = 40;
    public final static int endActivityTransacted = 41;
    public final static int enrollInTransaction = 42;
    public final static int fireEvent = 43;
    public final static int fireEventWithFlags = 44;
    public final static int fireEventTransacted = 45;
    public final static int fireEventTransactedWithFlags = 46;
    public final static int startActivity = 47;
    public final static int startActivityWithFlags = 48;
    public final static int startActivityTransacted = 49;
    public final static int startActivityTransactedWithFlags = 50;
    public final static int startActivitySuspended = 51;
    public final static int suspendActivity = 52;

    /**
     * Returns the text description of the specified methodID.
     */
    public static String getMethodName(int methodID) {
        switch (methodID) {
        // ResourceAdaptor
        case activityEnded:
            return "activityEnded";
        case activityUnreferenced:
            return "activityUnreferenced";
        case administrativeRemove:
            return "administrativeRemove";
        case raConfigurationUpdate:
            return "raConfigurationUpdate";
        case raConfigure:
            return "raConfigure";
        case raUnconfigure:
            return "raUnconfigure";
        case raActive:
            return "raActive";
        case raInactive:
            return "raInactive";
        case raStopping:
            return "raStopping";
        case raVerifyConfiguration:
            return "raVerifyConfiguration";
        case eventProcessingFailed:
            return "eventProcessingFailed";
        case eventProcessingSuccessful:
            return "eventProcessingSuccessful";
        case eventUnreferenced:
            return "eventUnreferenced";
        case getActivity:
            return "getActivity";
        case getActivityHandle:
            return "getActivityHandle";
        case getMarshaler:
            return "getMarshaler";
        case getResourceAdaptorInterface:
            return "getResourceAdaptorInterface";
        case queryLiveness:
            return "queryLiveness";
        case serviceActive:
            return "serviceActive";
        case serviceInactive:
            return "serviceInactive";
        case serviceStopping:
            return "serviceStopping";
        case setResourceAdaptorContext:
            return "setResourceAdaptorContext";
        case unsetResourceAdaptorContext:
            return "unsetResourceAdaptorContext";
        case constructor:
            return "constructor";
        case finalizer:
            return "finalizer";

            // ResourceAdaptorContext
        case getAlarmFacility:
            return "getAlarmFacility";
        case getEventLookupFacility:
            return "getEventLookupFacility";
        case getDefaultUsageParameterSet:
            return "getDefaultUsageParameterSet";
        case getUsageParameterSet:
            return "getUsageParameterSet";
        case getProfileTable:
            return "getProfileTable";
        case getInvokingService:
            return "getInvokingService";
        case getSleeEndpoint:
            return "getSleeEndpoint";
        case getSleeTransactionManager:
            return "getSleeTransactionManager";
        case getEntityName:
            return "getEntityName";
        case getTimer:
            return "getTimer";
        case getTracer:
            return "getTracer";
        case getResourceAdaptor:
            return "getResourceAdaptor";
        case getResourceAdaptorTypes:
            return "getResourceAdaptorTypes";

            // SleeEndpoint
        case endActivity:
            return "endActivity";
        case endActivityTransacted:
            return "endActivityTransacted";
        case enrollInTransaction:
            return "enrollInTransaction";
        case fireEvent:
            return "fireEvent";
        case fireEventWithFlags:
            return "fireEvent<flags variant>";
        case fireEventTransacted:
            return "fireEventTransacted";
        case fireEventTransactedWithFlags:
            return "fireEventTransacted<flags variant>";
        case startActivity:
            return "startActivity";
        case startActivityWithFlags:
            return "startActivity<flags variant>";
        case startActivityTransacted:
            return "startActivityTransacted";
        case startActivityTransactedWithFlags:
            return "startActivityTransacted<flags variant>";
        case startActivitySuspended:
            return "startActivitySuspended";
        case suspendActivity:
            return "suspendActivity";

        default:
            return "unknown method (" + methodID + ")";
        }
    }
}
