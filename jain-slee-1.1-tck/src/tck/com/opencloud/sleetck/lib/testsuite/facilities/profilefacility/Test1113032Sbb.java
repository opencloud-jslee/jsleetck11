/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.facilities.profilefacility;

import java.util.HashMap;

import javax.naming.InitialContext;
import javax.slee.ActivityContextInterface;
import javax.slee.facilities.Tracer;
import javax.slee.profile.ProfileFacility;
import javax.slee.profile.ProfileID;
import javax.slee.profile.ProfileTable;
import javax.slee.profile.UnrecognizedProfileNameException;
import javax.slee.profile.UnrecognizedProfileTableNameException;

import com.opencloud.sleetck.lib.resource.events.TCKResourceEventX;
import com.opencloud.sleetck.lib.sbbutils.BaseTCKSbb;
import com.opencloud.sleetck.lib.sbbutils.TCKSbbUtils;

/*
 * AssertionID(1113032): Test The getProfileTable method returns the ProfileTable 
 * object of the Profile Table specified by the profileTableName argument.
 * AssertionID(1113033): Test the returned object may be cast to the profile table 
 * interface defined by the Profile Specification the Profile Table was created from 
 * using the standard Java typecast mechanism.
 * AssertionID(1113036): Test If the profileTableName argument does not identify 
 * a Profile Table recognized by the SLEE, the method throws a 
 * javax.slee.profile.UnrecognizedProfileTableNameException.
 * AssertionID(1113037): Test If the profileTableName argument is null, the method 
 * throws a java.lang.NullPointerException.
 */
public abstract class Test1113032Sbb extends BaseTCKSbb {

    private static final String JNDI_PROFILEFACILITY_NAME = ProfileFacility.JNDI_NAME;

    public static final String PROFILE_TABLE_NAME = "Test1113032ProfileTable";

    public static final String PROFILE_NAME = "Test1113032Profile";

    // Initial event
    public void onTCKResourceEventX1(TCKResourceEventX event, ActivityContextInterface aci) {

        try {
            tracer = getSbbContext().getTracer("com.test");
            tracer.info("Received event TCKResourceEventX");

            doTest1113032Test();
        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }
    }

    private void sendResultToTCK(String testName, boolean result, int failedAssertionID,  String message) throws Exception {
        HashMap sbbData = new HashMap();
        sbbData.put("testname", testName);
        sbbData.put("result", result ? "pass" : "fail");
        sbbData.put("message", message);
        sbbData.put("id", new Integer(failedAssertionID));
        TCKSbbUtils.getResourceInterface().sendSbbMessage(sbbData);
    }


    private void doTest1113032Test() throws Exception {
        ProfileFacility facility = getProfileFacility();
        ProfileTable profileTable = null;

        // 1113032
        try {
            
            profileTable = facility.getProfileTable(PROFILE_TABLE_NAME);
            if (profileTable == null) {
                sendResultToTCK("Test1113032Test", false, 1113032, "Invalid profiletable returned from ProfileFacility.getProfileTable()");
                return;
            }
            tracer.info("got expected ProfileTable", null);
        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }

        // 1113033
        try {
            Test1113032ProfileTable testProfileTable = (Test1113032ProfileTable) facility
                    .getProfileTable(PROFILE_TABLE_NAME);
            tracer.info("got expected returned object which was casted to the profile table interface " +
                        "dined by the Profile Specification", null);
        } catch (javax.slee.profile.UnrecognizedProfileTableNameException uptne) {
            tracer.info("got unexpected UnrecognizedProfileTableNameException", null);
            TCKSbbUtils.handleException(uptne);
        } catch (ClassCastException cce) {
            tracer.info("profile table " + PROFILE_TABLE_NAME + " is not of expected profile specification: " + cce, null);
            sendResultToTCK("Test1113032Test", false, 1113033, "profile table " + PROFILE_TABLE_NAME
                    + " is not of expected profile specification: " + cce);
            return;
        }

        // 1113036
        boolean passedA = false;
        try {
            profileTable = facility.getProfileTable("foo");
        } catch (javax.slee.profile.UnrecognizedProfileTableNameException e) {
            tracer.info("got expected UnrecognizedProfileTableNameException", null);
            passedA = true;
        }

        if (!passedA) {
            sendResultToTCK("Test1113032Test", false, 1113036,
                    "ProfileFacility.getProfileTable(InvalidProfileTableName) should have thrown UnrecognizedProfileTableNameException.");
            return;
        }

        // 1113037
        boolean passedB = false;
        try {
            profileTable = facility.getProfileTable(null);
        } catch (java.lang.NullPointerException e) {
            tracer.info("got expected NullPointerException", null);
            passedB = true;
        }

        if (!passedB) {
            sendResultToTCK("Test1113032Test", false, 1113037,
                    "ProfileFacility.getProfileTable(null) should have thrown java.lang.NullPointerException.");
            return;
        }

        sendResultToTCK("Test1113032Test", true, 1113032,"ProfileFacility.getProfileTable tests passed");
    }

    private ProfileFacility getProfileFacility() throws Exception {
        tracer = getSbbContext().getTracer("com.test");
        ProfileFacility facility = null;
        try {
            facility = (ProfileFacility) new InitialContext().lookup(JNDI_PROFILEFACILITY_NAME);
        } catch (Exception e) {
            tracer.warning("got unexpected Exception: " + e, null);
        }
        return facility;
    }

    public abstract Test1113032ProfileCMP getProfileCMP(ProfileID profileID)
            throws UnrecognizedProfileTableNameException, UnrecognizedProfileNameException;

    private Tracer tracer;
}
