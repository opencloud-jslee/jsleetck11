/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.profiles.noevents;

import javax.slee.ActivityContextInterface;
import javax.slee.profile.ProfileAddedEvent;
import javax.slee.profile.ProfileRemovedEvent;
import javax.slee.profile.ProfileUpdatedEvent;

import com.opencloud.sleetck.lib.sbbutils.BaseTCKSbb;
import com.opencloud.sleetck.lib.sbbutils.TCKSbbUtils;

/**
 * The Sbb responds to any Profile event with an ACK msg in form of the full class identifier of the event
 */
public abstract class Test1110041Sbb extends BaseTCKSbb {

    // Event handler methods
    public void onProfileAddedEvent(ProfileAddedEvent event, ActivityContextInterface aci) {
        try {
            getSbbContext().getTracer("Test1110041Sbb").info("Received ProfileAddedEvent event:"+event);

            // send an ACK to the test
            TCKSbbUtils.getResourceInterface().sendSbbMessage(ProfileAddedEvent.class.getName());

        } catch(Exception e) {
            TCKSbbUtils.handleException(e);
        }
    }

    public void onProfileRemovedEvent(ProfileRemovedEvent event, ActivityContextInterface aci) {
        try {
            getSbbContext().getTracer("Test1110041Sbb").info("Received ProfileRemovedEvent event:"+event);

            // send an ACK to the test
            TCKSbbUtils.getResourceInterface().sendSbbMessage(ProfileRemovedEvent.class.getName());

        } catch(Exception e) {
            TCKSbbUtils.handleException(e);
        }
    }

    public void onProfileUpdatedEvent(ProfileUpdatedEvent event, ActivityContextInterface aci) {
        try {
            getSbbContext().getTracer("Test1110041Sbb").info("Received ProfileUpdatedEvent event:"+event);

            TCKSbbUtils.getResourceInterface().sendSbbMessage(ProfileUpdatedEvent.class.getName());

        } catch(Exception e) {
            TCKSbbUtils.handleException(e);
        }
    }
}
