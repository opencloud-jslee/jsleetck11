/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.profiles.profileadded;

import java.util.HashMap;

import javax.slee.ActivityContextInterface;
import javax.slee.Address;
import javax.slee.CreateException;
import javax.slee.profile.ProfileLocalObject;
import javax.slee.profile.ProfileUpdatedEvent;

import com.opencloud.sleetck.lib.sbbutils.BaseTCKSbb;
import com.opencloud.sleetck.lib.sbbutils.TCKSbbUtils;
import com.opencloud.sleetck.lib.sbbutils.events.TCKSbbEvent;
import com.opencloud.sleetck.lib.sbbutils.events.TCKSbbEventImpl;
import com.opencloud.sleetck.lib.sbbutils.events2.SbbBaseMessageComposer;
import com.opencloud.sleetck.lib.sbbutils.events2.SbbBaseMessageConstants;
import com.opencloud.sleetck.lib.sbbutils.events2.SendResultEvent;

public abstract class Test1110024Sbb extends BaseTCKSbb {

    public static final int TYPE_UPDATE_PROFILE = 1024;

    private void sendLogMsgCall (String msg) {
        HashMap map = SbbBaseMessageComposer.getLogMsg(msg);
        try {
            TCKSbbUtils.getResourceInterface().callTest(map);
        }
        catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }
    }

    private Object sendUpdateRequestCall (int newValue, int id) throws Exception {
        HashMap map = SbbBaseMessageComposer.getCustomMessage(TYPE_UPDATE_PROFILE,
                new String[]{"NewValue", SbbBaseMessageConstants.ID},
                new Object[]{new Integer(newValue), new Integer(id)});
        return TCKSbbUtils.getResourceInterface().callTest(map);
    }

    private void sendResult(boolean result, int id, String msg, ActivityContextInterface aci) {
        HashMap map = SbbBaseMessageComposer.getSetResultMsg(result, id, msg);
        setResult(map);
        fireSendResultEvent(new SendResultEvent(), aci, null);
    }

    public void onProfileUpdatedEvent(ProfileUpdatedEvent event, ActivityContextInterface aci) {

        try {
            // make sure that ProfileUpdatedEvent is only processed once
            if (getStopProcessUpdateEvent()) {
                sendLogMsgCall("ProfileUpdatedEvent has already been received once before. Discarding this event otherwise we would run into an endless loop.");
                return;
            }

            setStopProcessUpdateEvent(true);
            sendLogMsgCall("Received ProfileUpdatedEvent");

            ProfileEventsTestsProfileLocal beforeUpdateProfile;
            ProfileEventsTestsProfileLocal afterUpdateProfile;

            //1110024, 1110025: test getBeforeUpdateProfileLocal and java typecast;
            try {
                beforeUpdateProfile = (ProfileEventsTestsProfileLocal) event.getBeforeUpdateProfileLocal();
            } catch (ClassCastException e) {
                sendResult(false, 1110025, "The Profile Local interface returned by ProfileUpdatedEvent.getBeforeUpdateProfileLocal() was not of type ProfileEventsTestsProfileLocal", aci);
                return;
            }
            //1110024, 1110025: test getAfterUpdateProfileLocal and java typecast;
            try {
                afterUpdateProfile = (ProfileEventsTestsProfileLocal) event.getAfterUpdateProfileLocal();
            } catch (ClassCastException e) {
                sendResult(false, 1110025, "The Profile Local interface returned by ProfileUpdatedEvent.getAfterUpdateProfileLocal() was not of type ProfileEventsTestsProfileLocal",aci);
                return;
            }
            sendLogMsgCall("Performed method calls and class casts.");

            //1110024: access the contents of the profile before the update
            if (beforeUpdateProfile.getValue() != 0) {
                sendResult(false, 1110024,"The 'value' field in the ProfileEventsTestsProfileLocal object representing the values prior to the update was not correct." ,aci);
                return;
            }
            //1110024: access the contents of the profile after the update
            if (afterUpdateProfile.getValue() != 42) {
                sendResult(false, 1110024,"The 'value' field in the ProfileEventsTestsProfileLocal object representing the values after the update was not correct." , aci);
                return;
            }
            sendLogMsgCall("Checked expected values for profile before and after update.");

            //send message to test to change profile value
            //block as otherwise transaction in which onProfileUpdatedEvent was called commits
            sendLogMsgCall("Request tck test to update profile field value to 2048");
            sendUpdateRequestCall(2048, 1110026);

            //1110026: check that value is still 0 and 42 as ProfileLocalObject is a snapshot of the time when profile was updated the first time
            if ((beforeUpdateProfile.getValue() != 0) || (afterUpdateProfile.getValue() != 42)) {
                sendResult(false,1110026,"The 'value' field in the ProfileEventsTestsProfileLocal objects should represent a snapshot of the time when the profile was modified the first time and not be subject to changes done to the profile since then.",aci);
                return;
            }
            sendLogMsgCall("Check that values have not been affected by latest profile update.");

            //store the obtained ProfileLocalObjects in CMP fields
            //1110640: An SBB component may retrieve a Profile local object from a SBB CMP field.
            try {
                setBeforeUpdateProfileLocalObject(beforeUpdateProfile);
                setAfterUpdateProfileLocalObject(afterUpdateProfile);
            } catch (Exception e) {
                sendResult(false, 1110640,"Attempt to store a ProfileLocal object in an SBB CMP field failed.", aci );
                return;
            }

            //get ProfileLocalObject from CMP field
            //1110640: An SBB component may retrieve a Profile local object from a SBB CMP field.
            try {
                ProfileLocalObject profileLocal1 = getBeforeUpdateProfileLocalObject();
                ProfileLocalObject profileLocal2 = getAfterUpdateProfileLocalObject();
                if (!beforeUpdateProfile.isIdentical(profileLocal1) || !afterUpdateProfile.isIdentical(profileLocal2)) {
                    sendResult(false, 1110640,"Attempt to retrieve a ProfileLocal object from an SBB CMP field failed. " +
                            "Retrieved and original ProfileLocalObject are not identical (as determined by ProfileObject.isIdentical()).", aci );
                    return;
                }
            } catch (Exception e) {
                sendResult(false, 1110640,"Attempt to retrieve a ProfileLocal object from an SBB CMP field failed with exception: "+e, aci );
                return;
            }

            //sbb fires event to itself thus allowing the current transaction to complete
            fireTCKSbbEvent(new TCKSbbEventImpl(null),aci,null);

        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }
    }

    public void onTCKSbbEvent(TCKSbbEvent event, ActivityContextInterface aci) {
        sendResult(true, 1110024, "Test completed successfully", aci);
    }

    public void onSendResultEvent(SendResultEvent event, ActivityContextInterface aci) {

        try {
            TCKSbbUtils.getResourceInterface().sendSbbMessage(getResult());

        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }
    }

    public void sbbPostCreate() throws CreateException  {
        setStopProcessUpdateEvent(false);
    }

    public abstract void fireSendResultEvent(SendResultEvent event, ActivityContextInterface aci, Address address);
    public abstract void fireTCKSbbEvent(TCKSbbEvent event, ActivityContextInterface aci, Address address);

    public abstract void setResult(HashMap map);
    public abstract HashMap getResult();

    public abstract void setStopProcessUpdateEvent(boolean value);
    public abstract boolean getStopProcessUpdateEvent();

    public abstract void setBeforeUpdateProfileLocalObject(ProfileLocalObject value);
    public abstract ProfileLocalObject getBeforeUpdateProfileLocalObject();

    public abstract void setAfterUpdateProfileLocalObject(ProfileLocalObject value);
    public abstract ProfileLocalObject getAfterUpdateProfileLocalObject();
}
