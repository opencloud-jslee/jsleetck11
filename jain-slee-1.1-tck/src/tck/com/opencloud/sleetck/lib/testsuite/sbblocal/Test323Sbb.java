/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.sbblocal;

import javax.slee.ActivityEndEvent;
import javax.slee.ActivityContextInterface;
import javax.slee.SbbContext;
import javax.slee.Address;
import javax.slee.SbbLocalObject;
import javax.slee.facilities.Level;
import javax.slee.ChildRelation;

import javax.slee.nullactivity.NullActivityFactory;
import javax.slee.nullactivity.NullActivity;
import javax.slee.nullactivity.NullActivityContextInterfaceFactory;
import javax.slee.facilities.ActivityContextNamingFacility;

import com.opencloud.sleetck.lib.sbbutils.BaseTCKSbb;
import com.opencloud.sleetck.lib.sbbutils.TCKSbbUtils;
import com.opencloud.sleetck.lib.sbbutils.TCKSbbConstants;
import com.opencloud.sleetck.lib.resource.events.TCKResourceEventX;
import com.opencloud.sleetck.lib.resource.TCKActivityID;
import com.opencloud.sleetck.lib.resource.sbbapi.TCKActivity;
import com.opencloud.sleetck.lib.resource.sbbapi.TCKResourceSbbInterface;
import com.opencloud.sleetck.lib.resource.sbbapi.TCKActivityContextInterfaceFactory;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;


/**
 * Child not attached callbacks are not invoked on the SBB
 * entities in the sub-tree when the sub-tree is removed.
 */

public abstract class Test323Sbb extends BaseTCKSbb {

    public void sbbPostCreate() throws javax.slee.CreateException {
        try {

            NullActivityFactory factory = (NullActivityFactory) TCKSbbUtils.getSbbEnvironment().lookup("slee/nullactivity/factory");
            NullActivity nullActivity = factory.createNullActivity();

            NullActivityContextInterfaceFactory aciFactory = (NullActivityContextInterfaceFactory) TCKSbbUtils.getSbbEnvironment().lookup("slee/nullactivity/activitycontextinterfacefactory");
            ActivityContextInterface nullACI = aciFactory.getActivityContextInterface(nullActivity);


            // Register this nullACI with the naming facitily.
            ActivityContextNamingFacility namingFacility = (ActivityContextNamingFacility) TCKSbbUtils.getSbbEnvironment().lookup("slee/facilities/activitycontextnaming");
            namingFacility.bind(nullACI, "Test323NullActivity");

            SbbLocalObject child = getChild().create();
            nullACI.attach(child);
            setChildSbbLocalObject(child);

            // Attach this SBB to the NullActivity.
            nullACI.attach(getSbbContext().getSbbLocalObject());

        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }
    }

    public void onTCKResourceEventX1(TCKResourceEventX event, ActivityContextInterface aci) {
        try {

            HashMap map = new HashMap();

            // Retrieve the stored child reference.
            SbbLocalObject child = getChildSbbLocalObject();
            // Remove the child, the reference now becomes invalid.
            child.remove();

            try { // Any method on the SbbLocalObject should now throw exception
                child.remove();
            } catch (javax.slee.TransactionRolledbackLocalException e) {
                // And mark TXN for rollback.
                if (getSbbContext().getRollbackOnly()) {
                    map.put("Result", new Boolean(true));
                    map.put("Message", "Ok");
                } else {
                    map.put("Result", new Boolean(false));
                    map.put("Message", "Transaction was not marked for rollback.");
                }
            } catch (Exception e) {
                map.put("Result", new Boolean(false));
                map.put("Message", "Accessing an invalid SbbLocalObject should have resulted in javax.slee.TransactionRolledbackLocalException - it resulted in " + e.toString());
            }

            // Send the test result.
            TCKSbbUtils.getResourceInterface().sendSbbMessage(map);
        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }
    }

    public void sbbRolledBack(javax.slee.RolledBackContext context) {
        Object event = context.getEvent();
        if (event instanceof TCKResourceEventX && ((TCKResourceEventX)event).getEventTypeName().equals(TCKResourceEventX.X1)) {
            // ignore expected rollback
            return;
        }
        super.sbbRolledBack(context);
    }

    public void onTCKResourceEventX2(TCKResourceEventX event, ActivityContextInterface aci) {
        try {
            // Lookup and end the NullActivity.
            ActivityContextNamingFacility namingFacility = (ActivityContextNamingFacility) TCKSbbUtils.getSbbEnvironment().lookup("slee/facilities/activitycontextnaming");
            ActivityContextInterface nullACI = namingFacility.lookup("Test323NullActivity");
            NullActivity nullActivity = (NullActivity)nullACI.getActivity();

            // End the NullActivity.
            nullActivity.endActivity();

            // Remove its binding from the ACNF.
            namingFacility.unbind("Test323NullActivity");

        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }
    }

    public abstract ChildRelation getChild();

    public abstract void setChildSbbLocalObject(SbbLocalObject child);
    public abstract SbbLocalObject getChildSbbLocalObject();

}
