/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.resource.endpoint;

import com.opencloud.sleetck.lib.TCKTestErrorException;
import com.opencloud.sleetck.lib.TCKTestFailureException;
import com.opencloud.sleetck.lib.TCKTestResult;
import com.opencloud.sleetck.lib.testsuite.resource.BaseResourceTest;
import com.opencloud.sleetck.lib.testsuite.resource.RAMethods;

/**
 * Test to ensure that startActivityTransacted() is throwing
 * ActivityAlreadyExists exception correctly.
 * <p>
 * Test assertion: 1115265
 */
public class Test1115265Test extends BaseResourceTest {

    private static final int ASSERTION_ID = 1115265;

    public TCKTestResult run() throws Exception {
        int sequenceID = nextMessageID();
        MultiResponseListener listener = new MultiResponseListener(sequenceID);

        listener.addExpectedResult("result1a");
        listener.addExpectedResult("result1b");

        listener.addExpectedResult("result2a");
        listener.addExpectedResult("result2b");

        listener.addExpectedResult("result3");
        listener.addExpectedResult("result3a");
        listener.addExpectedResult("result3b");

        sendMessage(RAMethods.startActivityTransacted, new Integer(ASSERTION_ID), listener, sequenceID);

        Object result1a = listener.getResult("result1a");
        if (result1a == null)
            throw new TCKTestErrorException("Test timed out while waiting for response from test component");
        checkResult(result1a, ASSERTION_ID,
                "startActivityTransacted(handle) failed to throw a ActivityAlreadyExistsException when activity was previously started non-transactionally");
        Object result1b = listener.getResult("result1b");
        if (result1b == null)
            throw new TCKTestErrorException("Test timed out while waiting for response from test component");
        checkResult(result1b, ASSERTION_ID,
                "startActivityTransacted(handle, flags) failed to throw a ActivityAlreadyExistsException when activity was previously started non-transactionally");

        Object result2a = listener.getResult("result2a");
        if (result2a == null)
            throw new TCKTestErrorException("Test timed out while waiting for response from test component");
        checkResult(
                result2a,
                ASSERTION_ID,
                "startActivityTransacted(handle, flags) failed to throw a ActivityAlreadyExistsException when activity was previously started transactionally in the same transaction");
        Object result2b = listener.getResult("result2b");
        if (result2b == null)
            throw new TCKTestErrorException("Test timed out while waiting for response from test component");

        checkResult(
                result2b,
                ASSERTION_ID,
                "startActivityTransacted(handle, flags) failed to throw a ActivityAlreadyExistsException when activity was previously started transactionally in the same transaction");

        Object result3 = listener.getResult("result3");
        if (result3 == null)
            throw new TCKTestErrorException("Test timed out while waiting for response from test component");

        // Only check to see that we received something back from the test
        // components.
        checkResult(result3, ASSERTION_ID);

        // From SBB
        Object result3asbb = listener.getResult("result3a");
        Object result3bsbb = listener.getResult("result3b");

        /*
         * We should not see results from both SBBs (only one of the event
         * firing transactions should have committed).
         */
        getLog().info("Checking that both transactions did not commit successfully");
        if (Boolean.TRUE.equals(result3asbb) && Boolean.TRUE.equals(result3bsbb)) {
            throw new TCKTestFailureException(
                    ASSERTION_ID,
                    "Concurrent calls to startActivityTransacted() on the same activity handle still resulted in both transactions firing transacted events, when only one was expected to succeed");
        }

        return TCKTestResult.passed();
    }
}
