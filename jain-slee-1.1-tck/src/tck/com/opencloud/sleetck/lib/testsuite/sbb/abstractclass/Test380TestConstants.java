/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.sbb.abstractclass;

public class Test380TestConstants {

    public static final String RECEIVED_EVENT            = "Received event";
    public static final String SBB_STORE_CALLED_WITH_TXN = "sbbStore() called with a txn";
    public static final String SBB_STORE_CALLED_NO_TXN   = "sbbStore() called with no txn";

}