/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.resource.endpoint;

import java.rmi.RemoteException;
import java.util.HashMap;

import com.opencloud.sleetck.lib.TCKTestErrorException;
import com.opencloud.sleetck.lib.TCKTestFailureException;
import com.opencloud.sleetck.lib.TCKTestResult;
import com.opencloud.sleetck.lib.rautils.MessageHandler;
import com.opencloud.sleetck.lib.testsuite.resource.BaseResourceTest;
import com.opencloud.sleetck.lib.testsuite.resource.RAMethods;
import com.opencloud.sleetck.lib.testsuite.resource.TCKMessage;
import com.opencloud.util.Future;
import com.opencloud.util.Future.TimeoutException;

/**
 * Test to ensure the SLEE is delivering events in order on the same activity w/
 * fireEvent().
 * <p>
 * Test assertion: 1115296
 */
public class Test1115296Test extends BaseResourceTest {

    private final static int ASSERTION_ID = 1115296;

    public TCKTestResult run() throws Exception {
        int sequenceID = nextMessageID();

        ResponseListener listener = new ResponseListener(sequenceID);

        sendMessage(RAMethods.fireEvent, new Integer(ASSERTION_ID), listener, sequenceID);

        Object raResult = listener.getRAResult();
        Object eventsResult = listener.getEventsResult();

        if (raResult == null)
            throw new TCKTestErrorException("Test timed out while waiting for response from test resource adaptor component");
        if (eventsResult == null)
            throw new TCKTestErrorException("Test timed out while waiting for event notifications from sbb");
        if (raResult instanceof Exception)
            throw new TCKTestFailureException(ASSERTION_ID, "An exception was thrown by test resource adaptor while firing an event on a new activity",
                    (Exception) raResult);
        if (!Boolean.TRUE.equals(raResult))
            throw new TCKTestErrorException("Unexpected result received from test resource adaptor component: " + raResult);

        if (eventsResult instanceof Exception)
            throw (Exception) eventsResult;
        if (!Boolean.TRUE.equals(eventsResult))
            throw new TCKTestErrorException("Unexpected result received from event processing logic: " + eventsResult);

        return TCKTestResult.passed();
    }

    private class ResponseListener implements MessageHandler {

        public ResponseListener(int expectedResponse) {
            this.expectedResponse = expectedResponse;
        }

        public boolean handleMessage(Object obj) throws RemoteException {
            getLog().info("Received message from test component: " + obj);

            if (!(obj instanceof TCKMessage)) {
                getLog().error("Unhandled message type: " + obj);
                return false;
            }
            TCKMessage message = (TCKMessage) obj;

            if (message.getSequenceID() != expectedResponse)
                return true;

            HashMap results = (HashMap) message.getArgument();
            Object raResult = results.get("result1");
            Object eventResult = results.get("result2");

            if (raResult != null && !raFuture.isSet())
                raFuture.setValue(raResult);
            else if (eventResult != null && !eventsFuture.isSet()) {
                int nextEventID = currentEventCount + 1;
                int eventCount = ((Integer) eventResult).intValue();

                getLog().info("Received notification for event '" + eventCount + "', next event expected is '" + nextEventID + "'.");

                if (eventCount < nextEventID) {
                    getLog().info("Ignoring duplicate or previous event notification for event '" + eventCount + "'.");
                } else if (eventCount > nextEventID) {
                    eventsFuture.setValue(new TCKTestFailureException(ASSERTION_ID, "Missing or skipped event detected while processing test results"));
                } else if (eventCount == nextEventID) {
                    currentEventCount = eventCount;
                    if (currentEventCount == 42)
                        eventsFuture.setValue(Boolean.TRUE);
                }

                return true;
            }

            return true;
        }

        public Object getEventsResult() {
            Object result;
            try {
                result = eventsFuture.getValue(utils().getTestTimeout());
            } catch (TimeoutException e) {
                return null;
            }
            return result;
        }

        public Object getRAResult() {
            Object result;
            try {
                result = raFuture.getValue(utils().getTestTimeout());
            } catch (TimeoutException e) {
                return null;
            }
            return result;
        }

        private Future eventsFuture = new Future();
        private Future raFuture = new Future();

        private int currentEventCount = 0;

        private int expectedResponse = -1;

    }
}
