/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.activities.nullactivity;

import com.opencloud.sleetck.lib.resource.events.TCKResourceEventX;
import com.opencloud.sleetck.lib.sbbutils.BaseTCKSbb;
import com.opencloud.sleetck.lib.sbbutils.TCKSbbUtils;

import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.slee.ActivityContextInterface;
import javax.slee.ActivityEndEvent;
import javax.slee.facilities.Level;
import javax.slee.nullactivity.NullActivityFactory;
import javax.slee.nullactivity.NullActivity;

public abstract class Test4464Sbb extends BaseTCKSbb {
    /**
     * Creates a NullActivity then calls setRolbackOnly().
     * @param event
     * @param aci
     */
    public void onTCKResourceEventX1(TCKResourceEventX event, ActivityContextInterface aci) {
        try {
            createTrace("X1: enter");
            NullActivityFactory factory = (NullActivityFactory)
                    new InitialContext().lookup("java:comp/env/slee/nullactivity/factory");
            factory.createNullActivity();
            getSbbContext().setRollbackOnly();
            createTrace("X1: exit");
        } catch (NamingException e) {
            TCKSbbUtils.handleException(e);
        }
    }

    /**
     * Checks an ActivityEndEvent to see if it is for a NullActivity.  If an ActivityEndEvent is received for a
     *  NullActivity then callback to the test.
     * @param event
     * @param aci
     */
    public void onActivityEndEvent(ActivityEndEvent event, ActivityContextInterface aci) {
        createTrace("onActivityEndEvent: enter");
        createTrace("activity: " + aci.getActivity().getClass().
                                   toString());
        if (aci.getActivity() instanceof NullActivity) {
            createTrace("Received ActivityEndEvent for a NullActivity");
            handleEvent("onActivityEndEvent");
        }
        createTrace("onActivityEndEvent: exit");
    }

    public void sbbRolledBack(javax.slee.RolledBackContext context) {
        // no-op: disable throwing an exception when the Sbb is rolled back
    }

    // Private methods
    private void handleEvent(String handlerName) {
        try {
            createTrace("Sending response: " + handlerName);
            TCKSbbUtils.getResourceInterface().callTest(handlerName);
        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }
    }

    protected void createTrace(String trace) {
        try {
            TCKSbbUtils.createTrace(getSbbID(), Level.INFO, transactionId() + ": " + trace,  null);
        } catch (Exception e) {}
    }

    private String transactionId() {
        try {
            return TCKSbbUtils.getResourceAdaptorInterface().getTransactionIDAccess().getCurrentTransactionID().toString();
        } catch (Exception e) {
            return null;
        }
    }
}
