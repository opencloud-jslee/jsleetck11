/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.profiles.profiletxcontext;

import com.opencloud.sleetck.lib.AbstractSleeTCKTest;
import com.opencloud.sleetck.lib.TCKTestResult;
import com.opencloud.sleetck.lib.TCKTestFailureException;
import com.opencloud.sleetck.lib.infra.SleeTCKComponentConstants;
import com.opencloud.sleetck.lib.testsuite.profiles.ProfileTestConstants;
import com.opencloud.sleetck.lib.testutils.Assert;
import com.opencloud.sleetck.lib.testutils.ComponentIDLookup;
import com.opencloud.sleetck.lib.testutils.ProfileUtils;
import com.opencloud.sleetck.lib.testutils.jmx.ProfileProvisioningMBeanProxy;

import javax.management.ObjectName;
import javax.slee.profile.ProfileSpecificationID;
import javax.slee.profile.ProfileVerificationException;
import java.util.Iterator;
import java.util.Vector;

/**
 * Run tests on the transaction contexts used for profileLoad(), profileStore() callbacks,
 * management methods and CMP field accessors, and checks that profileStore()
 * is called after profileLoad()
 */
    public class ProfileTXContextTest extends AbstractSleeTCKTest {
    private static final String TABLE_NAME = "tck.CheckLProfileTXTest.table";
    private static final String PROFILE_SPEC_NAME = "CheckProfileTXProfile";
    private static final String INITIAL_VALUE = CheckProfileTXProfileManagement.INITIAL_VALUE;
    private static final String CHANGED_VALUE = CheckProfileTXProfileManagement.CHANGED_VALUE;

    private static final int PROFILE_STORE_AFTER_PROFILE_LOAD_ASSERTION = 1940;
    private static final int PROFILE_METHODS_SHARE_TXN_CONTEXT_ASSERTION = 1941;

    public TCKTestResult run() throws Exception {
        // create the profile table
        ProfileProvisioningMBeanProxy profileProvisioning = profileUtils.getProfileProvisioningProxy();
        ProfileSpecificationID profileSpecID = new ComponentIDLookup(utils()).lookupProfileSpecificationID(
                PROFILE_SPEC_NAME, SleeTCKComponentConstants.TCK_VENDOR, "1.0");
        getLog().info("Creating profile table: " + TABLE_NAME);
        profileProvisioning.createProfileTable(profileSpecID, TABLE_NAME);
        isTableCreated = true;

        // check that the default profile has been initialized
        ObjectName profileJmxName = profileProvisioning.getDefaultProfile(TABLE_NAME);

        // Create profile: profileInititialize will set all CMP values to INITIAL_VALUE
        CheckProfileTXProfileProxy proxy = getProfileProxy(profileJmxName);

        // Check the initial values of the profile
        String[] initialValues = {proxy.getValue2(),proxy.getValue3(),proxy.getValue2InManagementMethod(),
                proxy.getValue1InProfileStore(),proxy.getValue2InProfileStore(),proxy.getValue3InProfileStore()};
        for (int i = 0; i < initialValues.length; i++) {
            if(!INITIAL_VALUE.equals(initialValues[i])) {
                return TCKTestResult.error("One or more CMP fields were not set to the expected value "+
                        INITIAL_VALUE+" at the start of the test.");
            }
        }

        // Begin a transaction
        // The SLEE will call profileLoad which sets the instance variable value 1 to CHANGED_VALUE
        getLog().fine("Starting to edit to profile");
        proxy.editProfile();

        // Set value 2 directory via the CMP setter
        getLog().fine("Setting value 2 via a CMP setter");
        proxy.setValue2(CHANGED_VALUE);

        // Set value 3 via a profile management method
        getLog().fine("Setting value 3 via a management method");
        proxy.setValue3ViaManagementMethod(CHANGED_VALUE);

        // Verify that the change made to value 2 was visible to the management method
        Assert.assertEquals(PROFILE_METHODS_SHARE_TXN_CONTEXT_ASSERTION,
                "The change made to value 2 was not visible to management method setValue3ViaManagementMethod()",
                CHANGED_VALUE, proxy.getValue2InManagementMethod());

        // Verify that changes made to values 2 and 3 are visible
        if(!CHANGED_VALUE.equals(proxy.getValue2())) return TCKTestResult.error("A change made to value 2 was not visible to the client");
        if(!CHANGED_VALUE.equals(proxy.getValue3())) return TCKTestResult.error("A change made to value 3 was not visible to the client");

        // Set the profile to invalid (we want profileCommit to call profileStore but not to commit the profile)
        getLog().fine("Marking the profile as invalid, then will try to commit the profile, to cause profileStore() to be called");
        proxy.setValid(false);

        // Call profileCommit
        // profileCommit will invoke profileStore, which will copy value to value2, giving value2 == VALUE_Y
        try {
            proxy.commitProfile();
            return TCKTestResult.error("Attempt to commit an invalid profile did not throw the expected ProfileVerificationException");
        } catch (ProfileVerificationException e) {
            getLog().finest("OK - Received expected ProfileVerificationException when trying to commit an invalid profile");
        }

        // The transaction is still alive -- so the changes made in profileStore should still be visible.
        // Check that profileStore() could see changes to transactional state

        getLog().fine("Checking the values seen in profileStore()");
        checkFieldInProfileStore("Value2", proxy.getValue2InProfileStore());
        checkFieldInProfileStore("Value3", proxy.getValue3InProfileStore());

        // We now know that profileStore() was called, and it's changes kept --
        // check that it was called on the same profile management object on which profileLoad() was called
        Assert.assertEquals(PROFILE_STORE_AFTER_PROFILE_LOAD_ASSERTION,
                "profileStore() was called, and it's changes were kept, but changes to instance state " +
                        "made in profileLoad() were not visible to profileStore(), indicating that either " +
                        "profileLoad() was not called, or that profileLoad() and profileStore() were " +
                        "called on different objects.", CHANGED_VALUE, proxy.getValue1InProfileStore());

        // Now rollback the transaction.
        getLog().fine("Restoring the profile");
        proxy.restoreProfile();

        // If all CMP fields revert to their initial state we have proven that all the above
        // updates were part of the same transaction.

        checkFieldRolledBack("Value2", proxy.getValue2());
        checkFieldRolledBack("Value3", proxy.getValue3());
        checkFieldRolledBack("Value2InManagementMethod", proxy.getValue2InManagementMethod());
        checkFieldRolledBack("Value1InProfileStore", proxy.getValue1InProfileStore());
        checkFieldRolledBack("Value2InProfileStore", proxy.getValue2InProfileStore());
        checkFieldRolledBack("Value3InProfileStore", proxy.getValue3InProfileStore());

        getLog().info("All checks passed");
        return TCKTestResult.passed();
    }

    private void checkFieldInProfileStore(String cmpFieldName, String cmpFieldValue) throws TCKTestFailureException {
        Assert.assertEquals(PROFILE_METHODS_SHARE_TXN_CONTEXT_ASSERTION, "A change made to CMP field "+cmpFieldName+
                " was not visible in profileStore(). (Or possibly the changes made in profileStore() were not kept" +
                " after the verification failure -- this would also be invalid as the profile has not been restored)",
                            CHANGED_VALUE, cmpFieldValue);
    }

    private void checkFieldRolledBack(String cmpFieldName, String cmpFieldValue) throws TCKTestFailureException {
        Assert.assertEquals(PROFILE_METHODS_SHARE_TXN_CONTEXT_ASSERTION, "Some changes made to the profile " +
                "methods were still visible after the transaction had been rolled back by restoring the profile. " +
                "This indicates that the changes were not made as within the same transaction context, " +
                "or were not transactional at all. CMP field expected to be rolled back: "+
                    cmpFieldName, INITIAL_VALUE, cmpFieldValue);
    }

    public void setUp() throws Exception {
        String duPath = utils().getTestParams().getProperty(ProfileTestConstants.PROFILE_SPEC_DU_PATH_PARAM);
        getLog().fine("Installing profile specification");
        utils().install(duPath);
        profileUtils = new ProfileUtils(utils());
        activeProxies = new Vector();
    }

    public void tearDown() throws Exception {
        // close profiles
        if (activeProxies != null) {
            getLog().fine("Closing profiles");
            Iterator activeProxiesIter = activeProxies.iterator();
            while (activeProxiesIter.hasNext()) {
                CheckProfileTXProfileProxy aProxy = (CheckProfileTXProfileProxy) activeProxiesIter.next();
                try {
                    if (aProxy != null) {
                        if (aProxy.isProfileWriteable()) aProxy.restoreProfile();
                        aProxy.closeProfile();
                    }
                } catch (Exception ex) {
                    getLog().warning("Exception caught while trying to close profiles:");
                    getLog().warning(ex);
                }
            }
        }
        // delete profile table
        try {
            if (profileUtils != null && isTableCreated) {
                getLog().fine("Removing profile table");
                profileUtils.removeProfileTable(TABLE_NAME);
            }
        } catch (Exception e) {
            getLog().warning("Caught exception while trying to remove profile table:");
            getLog().warning(e);
        }
        profileUtils = null;
        isTableCreated = false;
        activeProxies = null;
        super.tearDown();
    }

    private CheckProfileTXProfileProxy getProfileProxy(ObjectName mbeanName) {
        CheckProfileTXProfileProxy rProxy = new CheckProfileTXProfileProxy(mbeanName, utils().getMBeanFacade());
        activeProxies.addElement(rProxy);
        return rProxy;
    }

    private ProfileUtils profileUtils;
    private Vector activeProxies;
    private boolean isTableCreated;
}























