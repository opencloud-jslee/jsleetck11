/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.facilities.alarmfacility;

import javax.naming.InitialContext;
import javax.slee.ActivityContextInterface;
import javax.slee.facilities.AlarmFacility;
import javax.slee.facilities.AlarmLevel;

import com.opencloud.sleetck.lib.resource.events.TCKResourceEventX;
import com.opencloud.sleetck.lib.sbbutils.BaseTCKSbb;
import com.opencloud.sleetck.lib.sbbutils.TCKSbbUtils;

/*  
 *  AssertionID(1113484): Test All the AlarmLevel objects defined 
 *  in Section 13.2.3 can be specified except the Alarm- Level.CLEAR 
 *  object.
 *  
 */
public abstract class Test1113484Sbb extends BaseTCKSbb {

    private static final String JNDI_ALARMFACILITY_NAME = AlarmFacility.JNDI_NAME;

    public static final String ALARM_TYPE = "javax.slee.management.Alarm";

    public static final String ALARM_INSTANCEID_CRITICAL = "CRITICAL:Test1113484AlarmInstanceID";

    public static final String ALARM_INSTANCEID_MAJOR = "MAJOR:Test1113484AlarmInstanceID";

    public static final String ALARM_INSTANCEID_WARNING = "WARNING:Test1113484AlarmInstanceID";

    public static final String ALARM_INSTANCEID_INDETERMINATE = "INDETERMINATE:Test1113484AlarmInstanceID";

    public static final String ALARM_INSTANCEID_MINOR = "MINOR:Test1113484AlarmInstanceID";

    public static final String ALARM_MESSAGE_CRITICAL = "CRITICAL:Test1113484AlarmMessage";

    public static final String ALARM_MESSAGE_MAJOR = "MAJOR:Test1113484AlarmMessage";

    public static final String ALARM_MESSAGE_WARNING = "WARNING:Test1113484AlarmMessage";

    public static final String ALARM_MESSAGE_INDETERMINATE = "INDETERMINATE:Test1113484AlarmMessage";

    public static final String ALARM_MESSAGE_MINOR = "MINOR:Test1113484AlarmMessage";

    // Initial event
    public void onTCKResourceEventX1(TCKResourceEventX ev, ActivityContextInterface aci) {

        try {
            AlarmFacility facility = (AlarmFacility) new InitialContext().lookup(JNDI_ALARMFACILITY_NAME);
            facility.raiseAlarm(ALARM_TYPE, ALARM_INSTANCEID_CRITICAL, AlarmLevel.CRITICAL, ALARM_MESSAGE_CRITICAL);
            facility.raiseAlarm(ALARM_TYPE, ALARM_INSTANCEID_MAJOR, AlarmLevel.MAJOR, ALARM_MESSAGE_MAJOR);
            facility.raiseAlarm(ALARM_TYPE, ALARM_INSTANCEID_WARNING, AlarmLevel.WARNING, ALARM_MESSAGE_WARNING);
            facility.raiseAlarm(ALARM_TYPE, ALARM_INSTANCEID_INDETERMINATE, AlarmLevel.INDETERMINATE,
                    ALARM_MESSAGE_INDETERMINATE);
            facility.raiseAlarm(ALARM_TYPE, ALARM_INSTANCEID_MINOR, AlarmLevel.MINOR, ALARM_MESSAGE_MINOR);
        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }
    }

}
