/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.resource.endpoint;

import com.opencloud.sleetck.lib.TCKTestErrorException;
import com.opencloud.sleetck.lib.TCKTestFailureException;
import com.opencloud.sleetck.lib.TCKTestResult;
import com.opencloud.sleetck.lib.testsuite.resource.BaseResourceTest;
import com.opencloud.sleetck.lib.testsuite.resource.RAMethods;

/**
 * Test to ensure that fireEvent() is throwing ActivityIsEndingException
 * correctly.
 * <p>
 * Test assertion: 1115303
 */
public class Test1115303Test extends BaseResourceTest {

    private final static int ASSERTION_ID = 1115303;

    public TCKTestResult run() throws Exception {
        int sequenceID = nextMessageID();

        MultiResponseListener listener = new MultiResponseListener(sequenceID);
        listener.addExpectedResult("result1");
        listener.addExpectedResult("result2");
        listener.addExpectedResult("result3");

        sendMessage(RAMethods.fireEvent, new Integer(ASSERTION_ID), listener, sequenceID);

        Object result1 = listener.getResult("result1");
        Object result2 = listener.getResult("result2");
        Object result3 = listener.getResult("result3");

        if (result1 == null)
            throw new TCKTestErrorException("Test timed out while waiting for initial response from test resource adaptor component");
        if (result2 == null)
            throw new TCKTestErrorException("Test timed out while waiting for response from test sbb component");
        if (result3 == null)
            throw new TCKTestErrorException("Test timed out while waiting for final response from test resource adaptor component");

        if (result1 instanceof Exception)
            throw new TCKTestFailureException(ASSERTION_ID, "Unexpected exception thrown during test by resource adaptor:", (Exception) result1);
        if (!Boolean.TRUE.equals(result1))
            throw new TCKTestErrorException("Unexpected result received from resource adaptor test component: " + result1);

        if (result2 instanceof Exception)
            throw new TCKTestFailureException(ASSERTION_ID, "Unexpected exception thrown during test by sbb:", (Exception) result2);
        if (!Boolean.TRUE.equals(result2))
            throw new TCKTestErrorException("Unexpected result received from resource adaptor test component: " + result2);

        if (result3 instanceof Exception)
            throw new TCKTestFailureException(ASSERTION_ID, "Unexpected exception thrown during test by resource adaptor:", (Exception) result3);
        if (Boolean.FALSE.equals(result3))
            throw new TCKTestFailureException(ASSERTION_ID, "fireEvent() failed to throw a ActivityIsEndingException when required");
        if (!Boolean.TRUE.equals(result3))
            throw new TCKTestErrorException("Unexpected result received from test component: " + result3);

        return TCKTestResult.passed();
    }
}
