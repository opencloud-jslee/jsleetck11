/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.usage.misc;

import com.opencloud.sleetck.lib.AbstractSleeTCKTest;
import com.opencloud.sleetck.lib.TCKTestResult;
import com.opencloud.sleetck.lib.testsuite.usage.common.GenericUsageMBeanLookup;
import com.opencloud.sleetck.lib.testutils.SleeStarter;

/**
 * Creates a named usage parameter set, restarts the SLEE,
 * then checks that the parameter set still exists.
 */
public class ParameterSetPersistenceTest extends AbstractSleeTCKTest {

    private static final int TEST_ID = 2288;

    public TCKTestResult run() throws Exception {
        String PARAMETER_SET_NAME = "PersistentParameterSet";

        getLog().info("Creating a named usage parameter set");
        GenericUsageMBeanLookup mBeanLookup = new GenericUsageMBeanLookup(utils());
        try {
            mBeanLookup.getServiceUsageMBeanProxy().createUsageParameterSet(mBeanLookup.getSbbID(), PARAMETER_SET_NAME);
            // check that the parameter set exists now, so that its absence after a restart
            // indicates lack of persistence across restarts rather than some other problem
            String[] parameterSets = mBeanLookup.getServiceUsageMBeanProxy().getUsageParameterSets(mBeanLookup.getSbbID());
            if(parameterSets == null || parameterSets.length == 0 || !parameterSets[0].equals(PARAMETER_SET_NAME))
                return TCKTestResult.error("Named usage parameter set \""+PARAMETER_SET_NAME+"\" not visible after creation");
        } finally {
            mBeanLookup.closeAllMBeans();
        }

        getLog().info("Restarting the SLEE");
        SleeStarter.restartSlee(utils().getSleeManagementMBeanProxy(),utils().getDefaultTimeout());

        getLog().info("Checking that the parameter set still exists");
        // create a new MBean lookup object - the old MBean names may no longer be valid
        mBeanLookup = new GenericUsageMBeanLookup(utils());
        try {
            String[] parameterSets = mBeanLookup.getServiceUsageMBeanProxy().getUsageParameterSets(mBeanLookup.getSbbID());
            if(parameterSets == null || parameterSets.length == 0 || !parameterSets[0].equals(PARAMETER_SET_NAME))
                return TCKTestResult.failed(TEST_ID,
                    "Named usage parameter set \""+PARAMETER_SET_NAME+"\" did not survive a SLEE restart");
        } finally {
            mBeanLookup.closeAllMBeans();
        }

        return TCKTestResult.passed();
    }

}
