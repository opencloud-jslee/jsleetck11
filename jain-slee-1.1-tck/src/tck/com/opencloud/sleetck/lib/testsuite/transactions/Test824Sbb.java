/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.transactions;

import javax.slee.ActivityEndEvent;
import javax.slee.ActivityContextInterface;
import javax.slee.SbbContext;
import javax.slee.Address;
import javax.slee.SbbLocalObject;
import javax.slee.facilities.Level;
import javax.slee.ChildRelation;
import javax.slee.CreateException;

import com.opencloud.sleetck.lib.TCKTestErrorException;
import com.opencloud.sleetck.lib.sbbutils.BaseTCKSbb;
import com.opencloud.sleetck.lib.sbbutils.TCKSbbUtils;
import com.opencloud.sleetck.lib.sbbutils.TCKSbbConstants;
import com.opencloud.sleetck.lib.resource.events.TCKResourceEventX;
import com.opencloud.sleetck.lib.resource.sbbapi.TCKResourceSbbInterface;

import java.util.HashMap;
import java.util.Iterator;

public abstract class Test824Sbb extends BaseTCKSbb {

    public void sbbRolledBack(javax.slee.RolledBackContext context) {
    }
    
    // Initial event
    public void onTCKResourceEventX1(TCKResourceEventX ev, ActivityContextInterface aci) {

        try {

            HashMap map = new HashMap();
            if (getSbbContext().getRollbackOnly()) { // Eeek!
                map.put("Result", new Boolean(true));
                map.put("Message", "Transaction was initially marked for rollback.");
                TCKSbbUtils.getResourceInterface().sendSbbMessage(map);
                return;
            }

            // Mark for rollback.
            getSbbContext().setRollbackOnly();

            if (!getSbbContext().getRollbackOnly()) { // Eeek!
                map.put("Result", new Boolean(true));
                map.put("Message", "Transaction not marked for rollback after setRollbackOnly() called.");
                TCKSbbUtils.getResourceInterface().sendSbbMessage(map);
                return;
            }
        
            map.put("Result", new Boolean(true));
            map.put("Message", "Ok");
            TCKSbbUtils.getResourceInterface().sendSbbMessage(map);
        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }

    }

}
