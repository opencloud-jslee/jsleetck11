/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.events.sbbabstractclass;

import javax.slee.management.DeployableUnitID;

import com.opencloud.sleetck.lib.AbstractSleeTCKTest;
import com.opencloud.sleetck.lib.testutils.FutureResult;

/**
 * AssertionID(1108066): Test An SBB can only have one event handler method for
 * each event type.
 */
public class Test1108066Test extends AbstractSleeTCKTest {
    private static final String SERVICE1_DU_PATH_PARAM = "service1DUPath";

    private static final String SERVICE2_DU_PATH_PARAM = "service2DUPath";

    /**
     * Perform the actual test.
     */

    public void run(FutureResult result) throws Exception {

        // Install the Deployable Unit.
        String duPath = null;
        if (getTestName().equals("SAMEEVENTNAME"))
            duPath = utils().getTestParams().getProperty(SERVICE1_DU_PATH_PARAM);
        else if (getTestName().equals("DIFFEVENTNAME"))
            duPath = utils().getTestParams().getProperty(SERVICE2_DU_PATH_PARAM);
        else {
            result.setError("Unexpected test name encountered during test run: " + getTestName());
            return;
        }

        duPath = utils().getDeploymentUnitURL(duPath);

        try {
            duID = utils().getDeploymentMBeanProxy().install(duPath);
            result.setFailed(1108066,
                    "Expected javax.slee.management.DeploymentException not thrown when installing a "
                            + "deployable unit that contains invalid components");
        } catch (javax.slee.management.DeploymentException de) {
            getLog().info("Got expected javax.slee.management.DeploymentException" + de);
        } catch (Exception e) {
            getLog().warning("Got unexpected exception");
        }

        result.setPassed();
    }

    /**
     * Do all the pre-run configuration of the test.
     */
    public void setUp() throws Exception {
    }

    /**
     * Clean up after the test.
     */

    public void tearDown() throws Exception {
        // uninstall the DU if for some reason it did get installed
        if (null != duID) {
            utils().getDeploymentMBeanProxy().uninstall(duID);
        }
    }

    public String getTestName() {
        String testName = "testName";
        return utils().getTestParams().getProperty(testName);
    }

    private DeployableUnitID duID = null;
}