/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.management.DeploymentMBean;

import com.opencloud.sleetck.lib.AbstractSleeTCKTest;
import com.opencloud.sleetck.lib.TCKTestResult;
import com.opencloud.sleetck.lib.testutils.jmx.DeploymentMBeanProxy;

import javax.slee.ComponentID;
import javax.slee.EventTypeID;
import javax.slee.management.DeployableUnitDescriptor;
import javax.slee.management.DeployableUnitID;

public class Test1539Test extends AbstractSleeTCKTest {

    private static final String EVENTS_DU_PATH_PARAM = "DUPath";
    private static final int TEST_ID = 1539;

    /**
     * Perform the actual test.
     */
    public TCKTestResult run() throws Exception {

        DeployableUnitID duID;
        
        // Install the Deployable Units
        String duPath = utils().getTestParams().getProperty(EVENTS_DU_PATH_PARAM);
        try {
            duID = utils().install(duPath);
        } catch (Exception e) {
            return TCKTestResult.failed(TEST_ID, "Failed to install deployable unit containing two different Events.");
        }

        DeploymentMBeanProxy duProxy = utils().getDeploymentMBeanProxy();
        DeployableUnitDescriptor duDesc = duProxy.getDescriptor(duID);
        ComponentID components[] = duDesc.getComponents();

        EventTypeID events[] = new EventTypeID[2];

        for (int i = 0; i < components.length; i++) {
            if (components[i] instanceof EventTypeID) {
                if (events[0] == null) {
                    events[0] = (EventTypeID) components[i];
                    continue;
                }

                if (events[1] == null) {
                    events[1] = (EventTypeID) components[i];
                    break;
                }
            }
        }

        if (events[1] == null) {
            return TCKTestResult.failed(TEST_ID, "Failed to load both Events.");
        }

        return TCKTestResult.passed();
    }

    // Empty implementation
    public void setUp() throws Exception {}

}
