/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.resource.endpoint;

import java.util.HashMap;

import com.opencloud.sleetck.lib.TCKTestErrorException;
import com.opencloud.sleetck.lib.TCKTestFailureException;
import com.opencloud.sleetck.lib.TCKTestResult;
import com.opencloud.sleetck.lib.testsuite.resource.BaseResourceTest;
import com.opencloud.sleetck.lib.testsuite.resource.RAMethods;

/**
 * Test to ensure that visibility of activities started with
 * startActivityTransacted() is transactionally correct.
 * <p>
 * Test assertion: 1115217
 */
public class Test1115217Test extends BaseResourceTest {

    private static final int ASSERTION_ID = 1115217;

    public TCKTestResult run() throws Exception {
        HashMap results = sendMessage(RAMethods.startActivityTransacted, new Integer(1115217));
        if (results == null)
            throw new TCKTestFailureException(ASSERTION_ID, "Test timed out while waiting for response from test component");

        Object result1 = results.get("result");

        if (result1 instanceof Exception)
            throw new TCKTestFailureException(ASSERTION_ID, "Unexpected exception thrown during test:", (Exception) result1);
        if (Boolean.FALSE.equals(result1))
            throw new TCKTestFailureException(ASSERTION_ID, "Rollback of transaction containing startActivityTransacted() call still created activity");
        if (!Boolean.TRUE.equals(result1))
            throw new TCKTestErrorException("Unexpected result received from test component: " + result1);

        return TCKTestResult.passed();
    }
}
