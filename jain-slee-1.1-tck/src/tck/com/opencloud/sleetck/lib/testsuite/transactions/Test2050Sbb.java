/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.transactions;

import com.opencloud.sleetck.lib.resource.events.TCKResourceEventX;
import com.opencloud.sleetck.lib.sbbutils.BaseTCKSbb;
import com.opencloud.sleetck.lib.sbbutils.TCKSbbUtils;

import javax.slee.ActivityContextInterface;
import java.util.HashMap;

public abstract class Test2050Sbb extends BaseTCKSbb {

    public void sbbRolledBack(javax.slee.RolledBackContext context) {
        try {

            HashMap map = new HashMap();

            try {
                getSbbContext().getActivities();
            } catch (IllegalStateException e) {
                map.put("Result", new Boolean(false));
                map.put("Message", "SBB was not in the Ready state during sbbRolledBack.");
                TCKSbbUtils.getResourceInterface().sendSbbMessage(map);
                return;
            }

            map.put("Result", new Boolean(true));
            map.put("Message", "Ok");
            TCKSbbUtils.getResourceInterface().sendSbbMessage(map);

        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }
    }

    // Initial event
    public void onTCKResourceEventX1(TCKResourceEventX ev, ActivityContextInterface aci) {

        try {

            try {
                getSbbContext().getActivities();
            } catch (IllegalStateException e) {
                HashMap map = new HashMap();
                map.put("Result", new Boolean(false));
                map.put("Message", "SBB was not in the Ready state in the event handler.");
                TCKSbbUtils.getResourceInterface().sendSbbMessage(map);
                return;
            }

            getSbbContext().setRollbackOnly();
        
        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }
    }

}
