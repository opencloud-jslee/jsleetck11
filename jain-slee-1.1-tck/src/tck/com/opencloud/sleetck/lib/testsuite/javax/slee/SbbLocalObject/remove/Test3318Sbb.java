/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.javax.slee.SbbLocalObject.remove;

import javax.slee.ActivityEndEvent;
import javax.slee.ActivityContextInterface;
import javax.slee.Address;
import javax.slee.SbbLocalObject;
import javax.slee.facilities.Level;
import javax.slee.ChildRelation;

import com.opencloud.sleetck.lib.sbbutils.BaseTCKSbb;
import com.opencloud.sleetck.lib.sbbutils.TCKSbbUtils;
import com.opencloud.sleetck.lib.sbbutils.TCKSbbConstants;
import com.opencloud.sleetck.lib.resource.events.TCKResourceEventX;
import com.opencloud.sleetck.lib.resource.TCKActivityID;
import com.opencloud.sleetck.lib.resource.sbbapi.TCKActivity;
import com.opencloud.sleetck.lib.resource.sbbapi.TCKResourceSbbInterface;
import com.opencloud.sleetck.lib.resource.sbbapi.TCKActivityContextInterfaceFactory;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

/**
 * Test 3318: SbbLocalObject.remove(SbbLocalObject).
 *
 * Any children of the removed SBB entity will also be removed.
 */

public abstract class Test3318Sbb extends BaseTCKSbb {

    public void onTCKResourceEventX1(TCKResourceEventX event, ActivityContextInterface aci) {

        try {

            // Create the Child SBB and attach it to the ACI.
            SbbLocalObject childLocalObject = getChildRelation().create();
            aci.attach(childLocalObject);

            // Create the second Child SBB, which will record test success.
            aci.attach(getSecondChildRelation().create());

            // Fire an event, to allow the transaction to commit.
            // This is not strictly necessary, but it adds variety
            // into the test cases.
            fireTest3318Event(new Test3318Event(), aci, null);
        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }
    }

    public void onTest3318Event(Test3318Event event, ActivityContextInterface aci) {
        try {
            ArrayList copy = new ArrayList();
            // copy list of children to new collection so we don't concurrently modify the iterator
            for (Iterator iter = getChildRelation().iterator(); iter.hasNext(); ) copy.add(iter.next());

            Iterator iter = copy.iterator();
            while (iter.hasNext()) {
                SbbLocalObject childLocalObject = (SbbLocalObject) iter.next();
                childLocalObject.remove();
            }
            fireTest3318EventTwo(new Test3318EventTwo(), aci, null);
        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }
    }

    // Event fire method.
    public abstract void fireTest3318Event(Test3318Event event, ActivityContextInterface aci, Address address);
    public abstract void fireTest3318EventTwo(Test3318EventTwo event, ActivityContextInterface aci, Address address);

    // Get child relation method.
    public abstract ChildRelation getChildRelation();
    public abstract ChildRelation getSecondChildRelation();

}
