/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.events.eventcontext;

import java.util.HashMap;

import javax.slee.ActivityContextInterface;
import javax.slee.EventContext;
import javax.slee.InitialEventSelector;
import javax.slee.facilities.Tracer;

import com.opencloud.sleetck.lib.resource.TCKActivityID;
import com.opencloud.sleetck.lib.resource.events.TCKResourceEventX;
import com.opencloud.sleetck.lib.resource.sbbapi.TCKActivity;
import com.opencloud.sleetck.lib.sbbutils.BaseTCKSbb;
import com.opencloud.sleetck.lib.sbbutils.TCKSbbUtils;

/**
 * AssertionID(1108023): Test Event delivery can also be manually resumed 
 * by an SBB before the timeout period expires using the Event Context’s 
 * resumeDelivery method.
 * 
 */
public abstract class Test1108023Sbb extends BaseTCKSbb {
    public static final String TRACE_MESSAGE_TCKResourceEventX1 = "Test1108023Sbb:I got TCKResourceEventX1 on ActivityA";

    public static final String TRACE_MESSAGE_TCKResourceEventX2A = "Test1108023Sbb:I got TCKResourceEventX2 on ActivityA";

    public static final String TRACE_MESSAGE_TCKResourceEventX2B = "Test1108023Sbb:I got TCKResourceEventX2 on ActivityB";

    public static final String TRACE_MESSAGE_TCKResourceEventX3 = "Test1108023Sbb:I got TCKResourceEventX3 on ActivityA";

    public static int waitPeriodMs = 15000;

    public InitialEventSelector initialEventSelector(InitialEventSelector ies) {
        ies.setCustomName("test");
        return ies;
    }

    public void onTCKResourceEventX1(TCKResourceEventX event, ActivityContextInterface aci, EventContext context) {
        try {
            //send message to TCK: I got TCKResourceEventX1 on ActivityA
            tracer = getSbbContext().getTracer("com.test");
            tracer.info(TRACE_MESSAGE_TCKResourceEventX1);

            setTestName((String) event.getMessage());

            //store context in CMP field
            setEventContext(context);

            if (getTestName().equals("TIMEOUT")) {
                //call suspendDelivery(15000ms) method now 
                context.suspendDelivery(waitPeriodMs);
            }
            else if (getTestName().equals("NOTIMEOUT")) {
                //call suspendDelivery() method now, we assumed the default timeout 
                //on the SLEE will be more than 10000ms to complete this test.
                context.suspendDelivery();
            }
            else {
                tracer.severe("Unexpected test name encountered during X1 event handler: " + getTestName());
                return;
            }

        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }
    }

    public void onTCKResourceEventX2(TCKResourceEventX event, ActivityContextInterface aci, EventContext context) {
        try {
            //send message to TCK: I got TCKResourceEventX2 on ActivityB
            tracer = getSbbContext().getTracer("com.test");

            TCKActivity activity = (TCKActivity) context.getActivityContextInterface().getActivity();
            TCKActivityID activityID = activity.getID();
            tracer.info("Received " + activityID.getName());

            if (activityID.getName().equals(Test1108023Test.activityNameB)) {
                tracer.info("Received " + TRACE_MESSAGE_TCKResourceEventX2B);
                //get EventContext ec from CMP field
                EventContext ec = getEventContext();

                //assert ec.isSuspended(), check ec has been suspended or not 
                if (!ec.isSuspended()) {
                    sendResultToTCK("Test1108023Test", false, "SBB onTCKResourceEventX2-ERROR: The event delivery hasn't been suspended, the "
                            + "EventContext.isSuspended() returned false!", 1108023);
                    return;
                }

                //call resumeDelivery method now
                ec.resumeDelivery();
                setHaveResumedX1(true);
            } else if (activityID.getName().equals(Test1108023Test.activityNameA)) {
                tracer.info("Received " + TRACE_MESSAGE_TCKResourceEventX2A);
                if (!getHaveResumedX1()) {
                    sendResultToTCK("Test1108023Test", false, "SBB onTCKResourceEventX2-ERROR: The event delivery on ActivityA did not suspend, the "
                        + "event was still delivered from TCKResourceEventX2 on ActivityA", 1108023);
                    return;
                }
            }

        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }
    }

    public void onTCKResourceEventX3(TCKResourceEventX event, ActivityContextInterface aci, EventContext context) {
        try {
            //send message to TCK: I got TCKResourceEventX3 on ActivityB
            tracer = getSbbContext().getTracer("com.test");
            tracer.info(TRACE_MESSAGE_TCKResourceEventX3);
            //get EventContext ec from CMP field
            EventContext ec = getEventContext();

            //assert ec.isSuspended(), check ec has been suspended or not 
            if (ec != null && ec.isSuspended()) {
                sendResultToTCK("Test1108023Test", false, "SBB:onTCKResourceEventX3-ERROR: The event delivery has still been suspended, the "
                        + "EventContext.isSuspended() returned true!", 1108023);
                return;
            }

            sendResultToTCK("Test1108023Test", true, "The test of Event delivery can also be manually resumed by an SBB before "
                    + "the timeout period expires using the Event Context’s resumeDelivery method passed.", 1108023);

            return;

        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }
    }

    private void sendResultToTCK(String testName, boolean result, String message, int failedAssertionID) throws Exception {
        HashMap sbbData = new HashMap();
        sbbData.put("testname", testName);
        sbbData.put("result", result ? "pass" : "fail");
        sbbData.put("message", message);
        sbbData.put("id", new Integer(failedAssertionID));
        TCKSbbUtils.getResourceInterface().sendSbbMessage(sbbData);
    }
    
    public abstract void setTestName(String testName);
    public abstract String getTestName();

    public abstract void setEventContext(EventContext eventContext);
    public abstract EventContext getEventContext();
    
    public abstract void setHaveResumedX1(boolean haveResumedX1);
    public abstract boolean getHaveResumedX1();

    private Tracer tracer = null;
}
