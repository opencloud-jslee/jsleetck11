/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.events.eventcontext;

import java.util.HashMap;

import javax.slee.ActivityContextInterface;
import javax.slee.EventContext;
import javax.slee.InitialEventSelector;
import javax.slee.facilities.Tracer;

import com.opencloud.sleetck.lib.resource.events.TCKResourceEventX;
import com.opencloud.sleetck.lib.sbbutils.BaseTCKSbb;
import com.opencloud.sleetck.lib.sbbutils.TCKSbbUtils;

/**
 * AssertionID(1108021): Test The timeout period is measured from 
 * the time the suspendDelivery method is invoked.
 */
public abstract class Test1108021Sbb extends BaseTCKSbb {
    public static final String TRACE_MESSAGE_TCKResourceEventX1 = "Test1108021Sbb:I got TCKResourceEventX1 on ActivityA";

    public static final String TRACE_MESSAGE_TCKResourceEventX2 = "Test1108021Sbb:I got TCKResourceEventX2 on ActivityB";

    public static final String TRACE_MESSAGE_TCKResourceEventX3 = "Test1108021Sbb:I got TCKResourceEventX3 on ActivityA";

    public static int waitPeriodMs = 10000;

    public InitialEventSelector initialEventSelector(InitialEventSelector ies) {
        ies.setCustomName("test");
        return ies;
    }

    public void onTCKResourceEventX1(TCKResourceEventX event, ActivityContextInterface aci, EventContext context) {
        try {
            //send message to TCK: I got TCKResourceEventX1 on ActivityA
            tracer = getSbbContext().getTracer("com.test");
            tracer.info(TRACE_MESSAGE_TCKResourceEventX1);

            //store context in CMP field
            setEventContext(context);

            //call suspendDelivery(10000ms) method now 
            context.suspendDelivery(waitPeriodMs);
            setStartTime(System.currentTimeMillis());
            tracer.info("The starttime is" + String.valueOf(getStartTime()));

        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }
    }

    public void onTCKResourceEventX2(TCKResourceEventX event, ActivityContextInterface aci, EventContext context) {
        try {
            //send message to TCK: I got TCKResourceEventX2 on ActivityB
            tracer = getSbbContext().getTracer("com.test");
            tracer.info(TRACE_MESSAGE_TCKResourceEventX2);
            //get EventContext ec from CMP field
            EventContext ec = getEventContext();

            //assert ec.isSuspended(), check ec has been suspended or not 
            if (!ec.isSuspended()) {
                sendResultToTCK("Test1108021Test", false, "SBB:onTCKResourceEventX2-ERROR: The event delivery hasn't been suspended, the "
                        + "EventContext.isSuspended() returned false!" ,1108021);
                return;
            }

        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }
    }

    public void onTCKResourceEventX3(TCKResourceEventX event, ActivityContextInterface aci, EventContext context) {
        try {
            //send message to TCK: I got TCKResourceEventX3 on ActivityA
            tracer = getSbbContext().getTracer("com.test");
            tracer.info(TRACE_MESSAGE_TCKResourceEventX3);
            //get EventContext ec from CMP field
            EventContext ec = getEventContext();
            //assert ec.isSuspended(), check ec has been suspended or not 
            if (ec != null && ec.isSuspended()) {
                sendResultToTCK("Test1108021Test", false, "SBB:onTCKResourceEventX3-ERROR: The event delivery has still been suspended, the "
                        + "EventContext.isSuspended() returned true!" ,1108021);
                return;
            }

            long endTime = System.currentTimeMillis();
            tracer.info("The endtime is" + String.valueOf(endTime));

            if (endTime - getStartTime() < waitPeriodMs - 1000 || endTime - getStartTime() > waitPeriodMs + 1000) {
                sendResultToTCK("Test1108021Test", false, "SBB:onTCKResourceEventX3-ERROR: The timeout period is measured "
                        + "which did not equal to the substracting the time of the "
                        + "suspendDelivery method from the time of the resumeDelivery!" ,1108021);
                return;
            }

            sendResultToTCK("Test1108021Test", true, "The timeout period which is measured is equal to the substracting "
                    + "the time of the suspendDelivery method from the time of the resumeDelivery.!" ,1108021);
            return;

        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }
    }

    private void sendResultToTCK(String testName, boolean result, String message, int failedAssertionID) throws Exception {
        HashMap sbbData = new HashMap();
        sbbData.put("testname", testName);
        sbbData.put("result", result ? "pass" : "fail");
        sbbData.put("message", message);
        sbbData.put("id", new Integer(failedAssertionID));
        TCKSbbUtils.getResourceInterface().sendSbbMessage(sbbData);
    }

    public abstract void setEventContext(EventContext eventContext);
    public abstract EventContext getEventContext();
    
    public abstract void setStartTime(long startTime);
    public abstract long getStartTime();

    private Tracer tracer = null;
    
}
