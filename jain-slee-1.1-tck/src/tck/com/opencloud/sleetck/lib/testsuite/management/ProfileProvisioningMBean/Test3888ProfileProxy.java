/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.management.ProfileProvisioningMBean;

import com.opencloud.sleetck.lib.TCKTestErrorException;
import com.opencloud.sleetck.lib.testutils.jmx.MBeanFacade;
import com.opencloud.sleetck.lib.testutils.jmx.impl.EmptyArrays;
import javax.management.InstanceNotFoundException;
import javax.management.MBeanException;
import javax.management.ObjectName;
import javax.management.ReflectionException;
import javax.management.RuntimeMBeanException;
import javax.slee.InvalidStateException;
import javax.slee.management.ManagementException;
import javax.slee.profile.ProfileVerificationException;

/**
 * Defines a proxy implementation for the following interfaces:
 * javax.slee.profile.ProfileMBean
 * com.opencloud.sleetck.lib.testsuite.management.ProfileProvisioningMBean.Test3888ProfileCMP
 *
 * In addition to the Exceptions declared by each proxied method, 
 * each proxy method can throw a TCKTestErrorException - for commication failures, 
 * and to wrap InstanceNotFoundExceptions and ReflectionExceptions generated by the 
 * MBeanServer.invoke() method, and unchecked Exceptions other than RuntimeExceptions. 
 * RuntimeExceptions generated by the MBean are rethrown by the proxy.
 * 
 * This class was generated by the ProxyGenerator tool - Tue May 03 17:28:07 CEST 2005
 * Command line args: -c com.opencloud.sleetck.lib.testsuite.management.ProfileProvisioningMBean.Test3888ProfileCMP com.opencloud.sleetck.lib.testsuite.management.ProfileProvisioningMBean.Test3888ProfileProxy 
 */
public class Test3888ProfileProxy {

    public Test3888ProfileProxy(ObjectName objName, MBeanFacade facade) {
        this.objName = objName;
        this.facade = facade;
    }

    public void restoreProfile() throws InvalidStateException, ManagementException, TCKTestErrorException { 
        try {
            facade.invoke(objName,"restoreProfile",EmptyArrays.EMPTY_OBJECT_ARRAY,EmptyArrays.EMPTY_STRING_ARRAY);
        } catch(InstanceNotFoundException ie) {
            throw new TCKTestErrorException("Caught Exception while calling MBeanServer.invoke()",ie);
        } catch(ReflectionException re) {
            throw new TCKTestErrorException("Caught Exception while calling MBeanServer.invoke()",re);
        } catch(RuntimeMBeanException rmbe) {
            throw rmbe.getTargetException();
        } catch(MBeanException e) {
            Exception enclosed = e.getTargetException();
            if(enclosed instanceof InvalidStateException) throw (InvalidStateException)enclosed;
            if(enclosed instanceof ManagementException) throw (ManagementException)enclosed;
            if(enclosed instanceof RuntimeException) throw (RuntimeException)enclosed;
            throw new TCKTestErrorException("Caught undeclared exception",enclosed);
        }
    }

    public String getAttributeB() throws ManagementException, TCKTestErrorException { 
        try {
            return (String)facade.invoke(objName,"getAttributeB",EmptyArrays.EMPTY_OBJECT_ARRAY,EmptyArrays.EMPTY_STRING_ARRAY);
        } catch(InstanceNotFoundException ie) {
            throw new TCKTestErrorException("Caught Exception while calling MBeanServer.invoke()",ie);
        } catch(ReflectionException re) {
            throw new TCKTestErrorException("Caught Exception while calling MBeanServer.invoke()",re);
        } catch(RuntimeMBeanException rmbe) {
            throw rmbe.getTargetException();
        } catch(MBeanException e) {
            Exception enclosed = e.getTargetException();
            if(enclosed instanceof ManagementException) throw (ManagementException)enclosed;
            if(enclosed instanceof RuntimeException) throw (RuntimeException)enclosed;
            throw new TCKTestErrorException("Caught undeclared exception",enclosed);
        }
    }

    public void setAttributeB(String p0) throws ManagementException, InvalidStateException, TCKTestErrorException { 
        try {
            facade.invoke(objName,"setAttributeB",new Object[]{p0},new String[]{"java.lang.String"});
        } catch(InstanceNotFoundException ie) {
            throw new TCKTestErrorException("Caught Exception while calling MBeanServer.invoke()",ie);
        } catch(ReflectionException re) {
            throw new TCKTestErrorException("Caught Exception while calling MBeanServer.invoke()",re);
        } catch(RuntimeMBeanException rmbe) {
            throw rmbe.getTargetException();
        } catch(MBeanException e) {
            Exception enclosed = e.getTargetException();
            if(enclosed instanceof ManagementException) throw (ManagementException)enclosed;
            if(enclosed instanceof InvalidStateException) throw (InvalidStateException)enclosed;
            if(enclosed instanceof RuntimeException) throw (RuntimeException)enclosed;
            throw new TCKTestErrorException("Caught undeclared exception",enclosed);
        }
    }

    public boolean isProfileWriteable() throws ManagementException, TCKTestErrorException { 
        try {
            Boolean rValue = (Boolean)facade.invoke(objName,"isProfileWriteable",EmptyArrays.EMPTY_OBJECT_ARRAY,EmptyArrays.EMPTY_STRING_ARRAY);
            return rValue.booleanValue();
        } catch(InstanceNotFoundException ie) {
            throw new TCKTestErrorException("Caught Exception while calling MBeanServer.invoke()",ie);
        } catch(ReflectionException re) {
            throw new TCKTestErrorException("Caught Exception while calling MBeanServer.invoke()",re);
        } catch(RuntimeMBeanException rmbe) {
            throw rmbe.getTargetException();
        } catch(MBeanException e) {
            Exception enclosed = e.getTargetException();
            if(enclosed instanceof ManagementException) throw (ManagementException)enclosed;
            if(enclosed instanceof RuntimeException) throw (RuntimeException)enclosed;
            throw new TCKTestErrorException("Caught undeclared exception",enclosed);
        }
    }

    public void editProfile() throws ManagementException, TCKTestErrorException { 
        try {
            facade.invoke(objName,"editProfile",EmptyArrays.EMPTY_OBJECT_ARRAY,EmptyArrays.EMPTY_STRING_ARRAY);
        } catch(InstanceNotFoundException ie) {
            throw new TCKTestErrorException("Caught Exception while calling MBeanServer.invoke()",ie);
        } catch(ReflectionException re) {
            throw new TCKTestErrorException("Caught Exception while calling MBeanServer.invoke()",re);
        } catch(RuntimeMBeanException rmbe) {
            throw rmbe.getTargetException();
        } catch(MBeanException e) {
            Exception enclosed = e.getTargetException();
            if(enclosed instanceof ManagementException) throw (ManagementException)enclosed;
            if(enclosed instanceof RuntimeException) throw (RuntimeException)enclosed;
            throw new TCKTestErrorException("Caught undeclared exception",enclosed);
        }
    }

    public void setAttributeA(boolean p0) throws ManagementException, InvalidStateException, TCKTestErrorException { 
        try {
            facade.invoke(objName,"setAttributeA",new Object[]{new Boolean(p0)},new String[]{"boolean"});
        } catch(InstanceNotFoundException ie) {
            throw new TCKTestErrorException("Caught Exception while calling MBeanServer.invoke()",ie);
        } catch(ReflectionException re) {
            throw new TCKTestErrorException("Caught Exception while calling MBeanServer.invoke()",re);
        } catch(RuntimeMBeanException rmbe) {
            throw rmbe.getTargetException();
        } catch(MBeanException e) {
            Exception enclosed = e.getTargetException();
            if(enclosed instanceof ManagementException) throw (ManagementException)enclosed;
            if(enclosed instanceof InvalidStateException) throw (InvalidStateException)enclosed;
            if(enclosed instanceof RuntimeException) throw (RuntimeException)enclosed;
            throw new TCKTestErrorException("Caught undeclared exception",enclosed);
        }
    }

    public void closeProfile() throws InvalidStateException, ManagementException, TCKTestErrorException { 
        try {
            facade.invoke(objName,"closeProfile",EmptyArrays.EMPTY_OBJECT_ARRAY,EmptyArrays.EMPTY_STRING_ARRAY);
        } catch(InstanceNotFoundException ie) {
            throw new TCKTestErrorException("Caught Exception while calling MBeanServer.invoke()",ie);
        } catch(ReflectionException re) {
            throw new TCKTestErrorException("Caught Exception while calling MBeanServer.invoke()",re);
        } catch(RuntimeMBeanException rmbe) {
            throw rmbe.getTargetException();
        } catch(MBeanException e) {
            Exception enclosed = e.getTargetException();
            if(enclosed instanceof InvalidStateException) throw (InvalidStateException)enclosed;
            if(enclosed instanceof ManagementException) throw (ManagementException)enclosed;
            if(enclosed instanceof RuntimeException) throw (RuntimeException)enclosed;
            throw new TCKTestErrorException("Caught undeclared exception",enclosed);
        }
    }

    public boolean isProfileDirty() throws ManagementException, TCKTestErrorException { 
        try {
            Boolean rValue = (Boolean)facade.invoke(objName,"isProfileDirty",EmptyArrays.EMPTY_OBJECT_ARRAY,EmptyArrays.EMPTY_STRING_ARRAY);
            return rValue.booleanValue();
        } catch(InstanceNotFoundException ie) {
            throw new TCKTestErrorException("Caught Exception while calling MBeanServer.invoke()",ie);
        } catch(ReflectionException re) {
            throw new TCKTestErrorException("Caught Exception while calling MBeanServer.invoke()",re);
        } catch(RuntimeMBeanException rmbe) {
            throw rmbe.getTargetException();
        } catch(MBeanException e) {
            Exception enclosed = e.getTargetException();
            if(enclosed instanceof ManagementException) throw (ManagementException)enclosed;
            if(enclosed instanceof RuntimeException) throw (RuntimeException)enclosed;
            throw new TCKTestErrorException("Caught undeclared exception",enclosed);
        }
    }

    public void commitProfile() throws InvalidStateException, ProfileVerificationException, ManagementException, TCKTestErrorException { 
        try {
            facade.invoke(objName,"commitProfile",EmptyArrays.EMPTY_OBJECT_ARRAY,EmptyArrays.EMPTY_STRING_ARRAY);
        } catch(InstanceNotFoundException ie) {
            throw new TCKTestErrorException("Caught Exception while calling MBeanServer.invoke()",ie);
        } catch(ReflectionException re) {
            throw new TCKTestErrorException("Caught Exception while calling MBeanServer.invoke()",re);
        } catch(RuntimeMBeanException rmbe) {
            throw rmbe.getTargetException();
        } catch(MBeanException e) {
            Exception enclosed = e.getTargetException();
            if(enclosed instanceof InvalidStateException) throw (InvalidStateException)enclosed;
            if(enclosed instanceof ProfileVerificationException) throw (ProfileVerificationException)enclosed;
            if(enclosed instanceof ManagementException) throw (ManagementException)enclosed;
            if(enclosed instanceof RuntimeException) throw (RuntimeException)enclosed;
            throw new TCKTestErrorException("Caught undeclared exception",enclosed);
        }
    }

    public boolean getAttributeA() throws ManagementException, TCKTestErrorException { 
        try {
            Boolean rValue = (Boolean)facade.invoke(objName,"getAttributeA",EmptyArrays.EMPTY_OBJECT_ARRAY,EmptyArrays.EMPTY_STRING_ARRAY);
            return rValue.booleanValue();
        } catch(InstanceNotFoundException ie) {
            throw new TCKTestErrorException("Caught Exception while calling MBeanServer.invoke()",ie);
        } catch(ReflectionException re) {
            throw new TCKTestErrorException("Caught Exception while calling MBeanServer.invoke()",re);
        } catch(RuntimeMBeanException rmbe) {
            throw rmbe.getTargetException();
        } catch(MBeanException e) {
            Exception enclosed = e.getTargetException();
            if(enclosed instanceof ManagementException) throw (ManagementException)enclosed;
            if(enclosed instanceof RuntimeException) throw (RuntimeException)enclosed;
            throw new TCKTestErrorException("Caught undeclared exception",enclosed);
        }
    }

    private ObjectName objName;
    private MBeanFacade facade;

}
