/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.events.eventcontext;

import java.util.HashMap;

import javax.slee.ActivityContextInterface;
import javax.slee.Address;
import javax.slee.EventContext;
import javax.slee.InitialEventSelector;
import javax.slee.ServiceID;
import javax.slee.facilities.Tracer;

import com.opencloud.sleetck.lib.resource.TCKActivityID;
import com.opencloud.sleetck.lib.resource.events.TCKResourceEventX;
import com.opencloud.sleetck.lib.resource.sbbapi.TCKActivity;
import com.opencloud.sleetck.lib.sbbutils.BaseTCKSbb;
import com.opencloud.sleetck.lib.sbbutils.TCKSbbUtils;

/**
 * AssertionID(1108195): Test whilst the delivery of an event is suspended, 
 * other events fired on the Activity Context are not processed. This is to 
 * preserve the FIFO ordering of event processing, see 9.10.
 * 
 * AssertionID(1108191): Test This API allows an SBB to suspend event delivery,
 * indicating that the SBB has not finished its event handling work.
 * 
 * AssertionID(1108192): Test The SBB is then expected to use the API to resume 
 * event delivery at a later point, typically in another event handler method. 
 * Once delivery is resumed the SLEE continues to deliver the original event. 
 */

public abstract class Test1108195Sbb1 extends BaseTCKSbb {
    public static final String TRACE_MESSAGE_TCKResourceEventX1 = "Test1108195Sbb1:I got TCKResourceEventX1 on ActivityA";
    
    public static final String TRACE_MESSAGE_Test1108195Event = "Test1108195Sbb1:I got Test1108195Event";

    public static final String TRACE_MESSAGE_TCKResourceEventX2 = "Test1108195Sbb1:I got TCKResourceEventX2 on ActivityA";

    public static final String TRACE_MESSAGE_TCKResourceEventX3A = "Test1108195Sbb1:I got TCKResourceEventX3 on ActivityA";

    public static final String TRACE_MESSAGE_TCKResourceEventX3B = "Test1108195Sbb1:I got TCKResourceEventX3 on ActivityB";

    public InitialEventSelector initialEventSelector(InitialEventSelector ies) {
        ies.setCustomName("test");
        return ies;
    }

    public void onTCKResourceEventX1(TCKResourceEventX event, ActivityContextInterface aci, EventContext context) {
        try {
            //send message to TCK: I got TCKResourceEventX1 on ActivityA
            tracer = getSbbContext().getTracer("com.test");
            tracer.info(TRACE_MESSAGE_TCKResourceEventX1);
            //store context in CMP field
            setEventContext(context);
            //call suspendDelivery() method now, we assumed the default timeout 
            //on the SLEE will be more than 10000ms to complete this test.
            context.suspendDelivery();
            sendResultToTCK("Test1108195Test", true, "Sbb1 received TCKResourceEventX1 on ActivityA", 1108195);
            
            setTest1108195Event(new Test1108195Event());

            setActivityContextInterface(context.getActivityContextInterface());
            fireTest1108195Event(getTest1108195Event(), getActivityContextInterface(), ADDRESS_1, SERVICEID_1);
        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }
    }
    
    public void onTest1108195Event(Test1108195Event event, ActivityContextInterface aci, EventContext context) {
        try {
            tracer = getSbbContext().getTracer("com.test");
            tracer.info(TRACE_MESSAGE_Test1108195Event);

            sendResultToTCK("Test1108195Test", true, "Sbb1 received Test1108195Event", 1108195);

        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }

    }

    public void onTCKResourceEventX2(TCKResourceEventX event, ActivityContextInterface aci, EventContext context) {
        try {
            //send message to TCK: I got TCKResourceEventX2 on ActivityA
            tracer = getSbbContext().getTracer("com.test");
            tracer.info(TRACE_MESSAGE_TCKResourceEventX2);
            sendResultToTCK("Test1108195Test", true, "Sbb1 received TCKResourceEventX2 on ActivityA", 1108195);

        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }
    }

    public void onTCKResourceEventX3(TCKResourceEventX event, ActivityContextInterface aci, EventContext context) {
        try {
            TCKActivity activity = (TCKActivity) context.getActivityContextInterface().getActivity();
            TCKActivityID activityID = activity.getID();

            if (activityID.getName().equals(Test1108195Test.activityNameB)) {
                //send message to TCK: I got TCKResourceEventX3 on ActivityB
                tracer = getSbbContext().getTracer("com.test");
                tracer.info(TRACE_MESSAGE_TCKResourceEventX3B);
                //get EventContext ec from CMP field
                EventContext ec = getEventContext();
                //assert ec.isSuspended(), check ec has been suspended or not 
                if (!ec.isSuspended()) {
                    sendResultToTCK("Test1108195Test", false, "SBB1:onTCKResourceEventX3-ERROR: The event delivery hasn't been " +
                            "suspended, the EventContext.isSuspended() returned false!", 1108195);
                    return;
                }
                //call resumeDelivery method now
                ec.resumeDelivery();
                sendResultToTCK("Test1108195Test", true, "Sbb1 received TCKResourceEventX3 on ActivityB", 1108195);
            } else if (activityID.getName().equals(Test1108195Test.activityNameA)) {
                //send message to TCK: I got TCKResourceEventX3 on ActivityA
                tracer = getSbbContext().getTracer("com.test");
                tracer.info(TRACE_MESSAGE_TCKResourceEventX3A);
                sendResultToTCK("Test1108195Test", true, "Sbb1 received TCKResourceEventX3 on ActivityA", 1108195);
            }

        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }
    }
    
    public abstract void fireTest1108195Event(Test1108195Event event, ActivityContextInterface aci, Address address,
            ServiceID serviceID);

    private void sendResultToTCK(String testName, boolean result, String message, int failedAssertionID) throws Exception {
        HashMap sbbData = new HashMap();
        sbbData.put("testname", testName);
        sbbData.put("result", result ? "pass" : "fail");
        sbbData.put("message", message);
        sbbData.put("id", new Integer(failedAssertionID));
        TCKSbbUtils.getResourceInterface().sendSbbMessage(sbbData);
    }

    //cmp fields
    public abstract void setEventContext(EventContext eventContext);
    public abstract EventContext getEventContext();
    
    public abstract void setActivityContextInterface(ActivityContextInterface activityContextInterface);
    public abstract ActivityContextInterface getActivityContextInterface();
    
    public abstract void setTest1108195Event(Test1108195Event test1108195Event);
    public abstract Test1108195Event getTest1108195Event();
    
    private Tracer tracer = null;
    public static final Address ADDRESS_1 = new Address(javax.slee.AddressPlan.IP, "1.0.0.1");
    private static final ServiceID SERVICEID_1 = new ServiceID("Test1108195Service1", "jain.slee.tck", "1.1");

}
