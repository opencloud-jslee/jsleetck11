/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.javax.slee.facilities.TimerOptions;

import com.opencloud.sleetck.lib.resource.events.TCKResourceEventX;
import com.opencloud.sleetck.lib.sbbutils.BaseTCKSbb;
import com.opencloud.sleetck.lib.sbbutils.TCKSbbUtils;

import javax.slee.ActivityContextInterface;
import javax.slee.facilities.TimerOptions;
import java.util.HashMap;

public abstract class Test3671Sbb extends BaseTCKSbb {

    // Initial event
    public void onTCKResourceEventX1(TCKResourceEventX ev, ActivityContextInterface aci) {

        try {
            HashMap map = new HashMap();
            boolean passed = false;
            Exception unexpectedException = null;
            try {
                new TimerOptions(false, 1000, null);
            } catch (NullPointerException e) {
                passed = true;
            } catch (Exception e) {
                unexpectedException = e;
            }
            if (passed == true)
                map.put("Result", new Boolean(true));
            else {
                map.put("Result", new Boolean(false));
                map.put("ID", new Integer(3671));
                String messageSuffix = unexpectedException == null ? "The constructor threw no Exception" :
                                        "The constructor threw the following Exception: "+unexpectedException;
                map.put("Message", "new TimerOptions(persistent, timeout, preserveMissed) with null preserveMissed " +
                        "argument didn't throw the expected NullPointerException. "+messageSuffix);
            }

            TCKSbbUtils.getResourceInterface().sendSbbMessage(map);
        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }

    }

}

