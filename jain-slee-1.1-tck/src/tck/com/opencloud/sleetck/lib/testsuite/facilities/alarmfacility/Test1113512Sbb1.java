/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.facilities.alarmfacility;

import java.util.HashMap;

import javax.naming.InitialContext;
import javax.slee.ActivityContextInterface;
import javax.slee.facilities.AlarmFacility;
import javax.slee.facilities.AlarmLevel;
import javax.slee.facilities.Tracer;

import com.opencloud.sleetck.lib.resource.events.TCKResourceEventX;
import com.opencloud.sleetck.lib.sbbutils.BaseTCKSbb;
import com.opencloud.sleetck.lib.sbbutils.TCKSbbUtils;

/*  
 *  AssertionID(1113512): Test If one or more alarms are cleared by 
 *  this method, the SLEE’s AlarmMBean object emits an alarm notification 
 *  for each cleared alarm with the alarm level set to AlarmLevel.CLEAR.
 *  
 *  AssertionID(1113588): Test This method only affects alarms belonging 
 *  to the notification source associated with the AlarmFacility object instance.
 *
 */

public abstract class Test1113512Sbb1 extends BaseTCKSbb {

    public static final String ALARM_MESSAGE = "Sbb1:Test1113512AlarmMessage";

    // Initial event
    public void onTCKResourceEventX1(TCKResourceEventX event, ActivityContextInterface aci) {

        try {
            tracer = this.getSbbContext().getTracer("com.test");
            tracer.info("Received " + event + " message", null);

            doTest1113512Test();
            

        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }
    }

    private void doTest1113512Test() throws Exception {

        AlarmFacility facility = getAlarmFacility();

        // 1113512
        try {
            // raise an alarm
            String[] alarmIDs = {
                    facility.raiseAlarm("javax.slee.management.Alarm1", "Test1113512AlarmInstanceID1",
                            AlarmLevel.CRITICAL, ALARM_MESSAGE),
                    facility.raiseAlarm("javax.slee.management.Alarm2", "Test1113512AlarmInstanceID2",
                            AlarmLevel.MAJOR, ALARM_MESSAGE),
                    facility.raiseAlarm("javax.slee.management.Alarm3", "Test1113512AlarmInstanceID3",
                            AlarmLevel.WARNING, ALARM_MESSAGE),
                    facility.raiseAlarm("javax.slee.management.Alarm4", "Test1113512AlarmInstanceID4",
                            AlarmLevel.INDETERMINATE, ALARM_MESSAGE),
                    facility.raiseAlarm("javax.slee.management.Alarm5", "Test1113512AlarmInstanceID5",
                            AlarmLevel.MINOR, ALARM_MESSAGE) };

            // clear the alarm has been raised
            if (facility.clearAlarms() != alarmIDs.length) {
                sendResultToTCK("Test1113512Test", false, 1113512, "Sbb1: The return value of this facility.clearAlarms() method "
                        + "didn't match the number of alarms that supposed to be cleared!"
                        + "AlarmFacility.clearAlarms()= " + facility.clearAlarms() + "vs Total alarms= "
                        + alarmIDs.length);
                return;
            }

        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }

    }

    private void sendResultToTCK(String testName, boolean result, int failedAssertionID,  String message) throws Exception {
        HashMap sbbData = new HashMap();
        sbbData.put("testname", testName);
        sbbData.put("result", result ? "pass" : "fail");
        sbbData.put("message", message);
        sbbData.put("id", new Integer(failedAssertionID));
        TCKSbbUtils.getResourceInterface().sendSbbMessage(sbbData);
    }

    private AlarmFacility getAlarmFacility() throws Exception {
        AlarmFacility facility = null;
        String JNDI_ALARMFACILITY_NAME = AlarmFacility.JNDI_NAME;
        try {
            facility = (AlarmFacility) new InitialContext().lookup(JNDI_ALARMFACILITY_NAME);
        } catch (Exception e) {
            tracer.warning("got unexpected Exception: " + e, null);
        }
        return facility;
    }

    private Tracer tracer;

}
