/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.usage.sets;

import com.opencloud.sleetck.lib.AbstractSleeTCKTest;
import com.opencloud.sleetck.lib.TCKTestResult;
import com.opencloud.sleetck.lib.resource.events.TCKResourceEventX;
import com.opencloud.sleetck.lib.resource.TCKActivityID;
import com.opencloud.sleetck.lib.resource.TCKSbbMessage;
import com.opencloud.sleetck.lib.testutils.QueuingResourceListener;

/**
 * Asks the SBB to lookup an invalid parameter set, then waits for a test result.
 */
public class Test2258Test extends AbstractSleeTCKTest {

    public TCKTestResult run() throws Exception {
        getLog().fine("Firing an event to the SBB");
        TCKActivityID activityID = utils().getResourceInterface().createActivity("Test2258Test-Activity");
        utils().getResourceInterface().fireEvent(TCKResourceEventX.X1,"UncreatedParameterSetFoo",activityID,null);

        getLog().fine("Waiting for test result from the SBB");
        TCKSbbMessage sbbMessage = resourceListener.nextMessage();
        return TCKTestResult.fromExported(sbbMessage.getMessage());
    }

    public void setUp() throws Exception {
        super.setUp();
        setResourceListener(resourceListener = new QueuingResourceListener(utils()));
    }

    private QueuingResourceListener resourceListener;

}
