/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.facilities.tracefacility.tracelevel;

import javax.slee.facilities.TraceLevel;

import com.opencloud.sleetck.lib.AbstractSleeTCKTest;
import com.opencloud.sleetck.lib.TCKTestResult;

/*
 * AssertionID (1113146): Test A singleton instance of each enumerated value is guaranteed 
 * (via an implementation of the readResolve method, see java.io.Serializable for more 
 * details), so that equality tests using == are always evaluated correctly.
 *
 * AssertionID (1113416): Test It returns true if obj is an instance of this class 
 * representing the same level as this, false otherwise.
 *
 * AssertionID (1113419): Test It returns a hash code value.  
 *
 * AssertionID (1113427): Test It returns the textual representation of the TraceLevel object.  
 *
 */

public class Test1113146Test extends AbstractSleeTCKTest {

    /**
     * Perform the actual test.
     */

    public TCKTestResult run() throws Exception {
        // 1113146:OFF
        try {
            if (TraceLevel.OFF != TraceLevel.fromInt(TraceLevel.LEVEL_OFF)) {
                return TCKTestResult.failed(1113146,
                        "This TraceLevel.OFF didn't equivalent to TraceLevel.fromInt(TraceLevel.LEVEL_OFF)");
            }

            if (TraceLevel.OFF.toInt() != TraceLevel.LEVEL_OFF) {
                return TCKTestResult.failed(1113146,
                        "This TraceLevel.OFF.toInt() didn't equivalent to TraceLevel.LEVEL_OFF");
            }
            getLog().info("The equality test for TraceLevel.OFF");
        } catch (Exception e) {
            return TCKTestResult.failed(1113146, "The equality test of TraceLevel.OFF threw an exception.");
        }

        // 1113146:SEVERE
        try {
            if (TraceLevel.SEVERE != TraceLevel.fromInt(TraceLevel.LEVEL_SEVERE)) {
                return TCKTestResult.failed(1113146,
                        "This TraceLevel.SEVERE didn't equivalent to TraceLevel.fromInt(TraceLevel.LEVEL_SEVERE)");
            }
            if (TraceLevel.SEVERE.toInt() != TraceLevel.LEVEL_SEVERE) {
                return TCKTestResult.failed(1113146,
                        "This TraceLevel.SEVERE.toInt() didn't equivalent to TraceLevel.LEVEL_SEVERE");
            }
            getLog().info("The equality test for TraceLevel.SEVERE");
        } catch (Exception e) {
            return TCKTestResult.failed(1113146, "The equality test of TraceLevel.SEVERE threw an exception.");
        }

        // 1113146:WARNING
        try {
            if (TraceLevel.WARNING != TraceLevel.fromInt(TraceLevel.LEVEL_WARNING)) {
                return TCKTestResult.failed(1113146,
                        "This TraceLevel.WARNING didn't equivalent to TraceLevel.fromInt(TraceLevel.LEVEL_WARNING)");
            }
            if (TraceLevel.WARNING.toInt() != TraceLevel.LEVEL_WARNING) {
                return TCKTestResult.failed(1113146,
                        "This TraceLevel.WARNING.toInt() didn't equivalent to TraceLevel.LEVEL_WARNING");
            }
            getLog().info("The equality test for TraceLevel.WARNING");
        } catch (Exception e) {
            return TCKTestResult.failed(1113146, "The equality test of TraceLevel.WARNING threw an exception.");
        }

        // 1113146:INFO
        try {
            if (TraceLevel.INFO != TraceLevel.fromInt(TraceLevel.LEVEL_INFO)) {
                return TCKTestResult.failed(1113146,
                        "This TraceLevel.INFO didn't equivalent to TraceLevel.fromInt(TraceLevel.LEVEL_INFO)");
            }
            if (TraceLevel.INFO.toInt() != TraceLevel.LEVEL_INFO) {
                return TCKTestResult.failed(1113146,
                        "This TraceLevel.INFO.toInt() didn't equivalent to TraceLevel.LEVEL_INFO");
            }
            getLog().info("The equality test for TraceLevel.INFO");
        } catch (Exception e) {
            return TCKTestResult.failed(1113146, "The equality test of TraceLevel.INFO threw an exception.");
        }

        // 1113146:CONFIG
        try {
            if (TraceLevel.CONFIG != TraceLevel.fromInt(TraceLevel.LEVEL_CONFIG)) {
                return TCKTestResult.failed(1113146,
                        "This TraceLevel.CONFIG didn't equivalent to TraceLevel.fromInt(TraceLevel.LEVEL_CONFIG)");
            }
            if (TraceLevel.CONFIG.toInt() != TraceLevel.LEVEL_CONFIG) {
                return TCKTestResult.failed(1113146,
                        "This TraceLevel.CONFIG.toInt() didn't equivalent to TraceLevel.LEVEL_CONFIG");
            }
            getLog().info("The equality test for TraceLevel.CONFIG");
        } catch (Exception e) {
            return TCKTestResult.failed(1113146, "The equality test of TraceLevel.CONFIG threw an exception.");
        }

        // 1113146:FINE
        try {
            if (TraceLevel.FINE != TraceLevel.fromInt(TraceLevel.LEVEL_FINE)) {
                return TCKTestResult.failed(1113146,
                        "This TraceLevel.FINE didn't equivalent to TraceLevel.fromInt(TraceLevel.LEVEL_FINE)");
            }
            if (TraceLevel.FINE.toInt() != TraceLevel.LEVEL_FINE) {
                return TCKTestResult.failed(1113146,
                        "This TraceLevel.FINE.toInt() didn't equivalent to TraceLevel.LEVEL_FINE");
            }
            getLog().info("The equality test for TraceLevel.FINE");
        } catch (Exception e) {
            return TCKTestResult.failed(1113146, "The equality test of TraceLevel.FINE threw an exception.");
        }

        // 1113146:FINER
        try {
            if (TraceLevel.FINER != TraceLevel.fromInt(TraceLevel.LEVEL_FINER)) {
                return TCKTestResult.failed(1113146,
                        "This TraceLevel.FINER didn't equivalent to TraceLevel.fromInt(TraceLevel.LEVEL_FINER)");
            }
            if (TraceLevel.FINER.toInt() != TraceLevel.LEVEL_FINER) {
                return TCKTestResult.failed(1113146,
                        "This TraceLevel.FINER.toInt() didn't equivalent to TraceLevel.LEVEL_FINER");
            }
            getLog().info("The equality test for TraceLevel.FINER");
        } catch (Exception e) {
            return TCKTestResult.failed(1113146, "The equality test of TraceLevel.FINER threw an exception.");
        }

        // 1113146:FINEST
        try {
            if (TraceLevel.FINEST != TraceLevel.fromInt(TraceLevel.LEVEL_FINEST)) {
                return TCKTestResult.failed(1113146,
                        "This TraceLevel.FINEST didn't equivalent to TraceLevel.fromInt(TraceLevel.LEVEL_FINEST)");
            }
            if (TraceLevel.FINEST.toInt() != TraceLevel.LEVEL_FINEST) {
                return TCKTestResult.failed(1113146,
                        "This TraceLevel.FINEST.toInt() didn't equivalent to TraceLevel.LEVEL_FINEST");
            }
            getLog().info("The equality test for TraceLevel.FINEST");
        } catch (Exception e) {
            return TCKTestResult.failed(1113146, "The equality test of TraceLevel.FINEST threw an exception.");
        }

        // 1113416
        TraceLevel firstTraceLevel = TraceLevel.FINE;
        TraceLevel secondTraceLevel = TraceLevel.INFO;
        if (firstTraceLevel.equals(secondTraceLevel))
            return TCKTestResult.failed(1113416, "TraceLevel.equals(different TraceLevel) returned true.");

        if (!firstTraceLevel.equals(firstTraceLevel))
            return TCKTestResult.failed(1113416, "TraceLevel.equals(self) returned false.");

        // 1113419
        try {
            firstTraceLevel.hashCode();
        } catch (Exception e) {
            return TCKTestResult.failed(1113419, "firstTraceLevel.hashCode() threw an exception.");
        }

        // 1113427
        try {
            firstTraceLevel.toString();
        } catch (Exception e) {
            return TCKTestResult.failed(1113427, "firstTraceLevel.toString() threw an exception.");
        }

        if (firstTraceLevel.toString() == null)
            return TCKTestResult.failed(1113427, "firstTraceLevel.toString() returned null.");

        return TCKTestResult.passed();
    }

    /**
     * Do all the pre-run configuration of the test.
     */

    public void setUp() throws Exception {

    }

    /**
     * Clean up after the test.
     */

    public void tearDown() throws Exception {
    }
}
