/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.profiles.profileenv;

import java.io.Serializable;

public class MyInteger implements Serializable {
    private int myInteger;

    public MyInteger(int value) {
        this.myInteger = value;
    }

    public boolean equals(Object o) {
        return (o instanceof MyInteger && ((MyInteger)o).myInteger == this.myInteger);
    }

    public int hashCode() {
        return (new Integer(myInteger)).hashCode();
    }
}
