/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.resource.context;

import java.util.HashMap;

import com.opencloud.sleetck.lib.TCKTestResult;
import com.opencloud.sleetck.lib.testsuite.resource.BaseResourceTest;
import com.opencloud.sleetck.lib.testsuite.resource.RAMethods;

/**
 * Test to ensure that getResourceAdaptor() is returning a correct value.
 * <p>
 * Test assertion ID: 1115406
 */
public class Test1115406Test extends BaseResourceTest {

    public TCKTestResult run() throws Exception {
        HashMap results = sendMessage(RAMethods.getResourceAdaptor, new Integer(1115406));
        
        checkResult(results, "result1", 1115406, "getResourceadaptor() returned incorrect ID");

        return TCKTestResult.passed();
    }
}
