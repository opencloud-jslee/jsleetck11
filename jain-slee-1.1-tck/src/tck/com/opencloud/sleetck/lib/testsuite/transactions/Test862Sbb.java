/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.transactions;

import java.util.HashMap;

import javax.slee.ActivityContextInterface;
import javax.slee.Address;
import javax.slee.facilities.ActivityContextNamingFacility;
import javax.slee.nullactivity.NullActivity;
import javax.slee.nullactivity.NullActivityContextInterfaceFactory;
import javax.slee.nullactivity.NullActivityFactory;

import com.opencloud.sleetck.lib.resource.events.TCKResourceEventX;
import com.opencloud.sleetck.lib.resource.events.TCKResourceEventY;
import com.opencloud.sleetck.lib.sbbutils.BaseTCKSbb;
import com.opencloud.sleetck.lib.sbbutils.TCKSbbUtils;

public abstract class Test862Sbb extends BaseTCKSbb {

    public static final String ACTIVITY_NAME = "Test862NullActivity";

    public void sbbRolledBack(javax.slee.RolledBackContext context) {
    }

    public void sbbRemove() {
        try {
            ActivityContextNamingFacility facility = (ActivityContextNamingFacility) TCKSbbUtils.getSbbEnvironment().lookup("slee/facilities/activitycontextnaming");
            ActivityContextInterface nullACI = facility.lookup(ACTIVITY_NAME);
            NullActivity nullActivity = (NullActivity) nullACI.getActivity();
            nullActivity.endActivity();
            facility.unbind(ACTIVITY_NAME);
        } catch (Exception e) {
        }
    }

    // Initial event
    public void onTCKResourceEventX1(TCKResourceEventX ev, ActivityContextInterface aci) {
        try {
        // Create a NullActivity, store in the ACNF, attach to it.
            NullActivityFactory factory = (NullActivityFactory) TCKSbbUtils.getSbbEnvironment().lookup("slee/nullactivity/factory");
            NullActivity nullActivity = factory.createNullActivity();
            NullActivityContextInterfaceFactory aciFactory = (NullActivityContextInterfaceFactory) TCKSbbUtils.getSbbEnvironment().lookup("slee/nullactivity/activitycontextinterfacefactory");
            ActivityContextInterface nullACI = aciFactory.getActivityContextInterface(nullActivity);
            nullACI.attach(getSbbContext().getSbbLocalObject());

            ActivityContextNamingFacility facility = (ActivityContextNamingFacility) TCKSbbUtils.getSbbEnvironment().lookup("slee/facilities/activitycontextnaming");
            facility.bind(nullACI, ACTIVITY_NAME);
        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }
    }

    public void onTCKResourceEventX2(TCKResourceEventX ev, ActivityContextInterface aci) {

        try {
            // Retrieve the NullActivity from the ACNF, end it.
            ActivityContextNamingFacility facility = (ActivityContextNamingFacility) TCKSbbUtils.getSbbEnvironment().lookup("slee/facilities/activitycontextnaming");
            ActivityContextInterface nullACI = facility.lookup(ACTIVITY_NAME);
            NullActivity nullActivity = (NullActivity) nullACI.getActivity();
            nullActivity.endActivity();

            // Rollback the TXN.
            getSbbContext().setRollbackOnly();
        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }
    }

    public void onTCKResourceEventY1(TCKResourceEventY ev, ActivityContextInterface aci) {
        try {
            // Retrieve the NullActivity from the ACNF, fire Test862Event and Test862SecondEvent on it.
            ActivityContextNamingFacility facility = (ActivityContextNamingFacility) TCKSbbUtils.getSbbEnvironment().lookup("slee/facilities/activitycontextnaming");
            ActivityContextInterface nullACI = facility.lookup(ACTIVITY_NAME);

            try {
                fireTest862Event(new Test862Event(), nullACI, null);
                fireTest862SecondEvent(new Test862SecondEvent(), nullACI, null);
            } catch (java.lang.IllegalStateException e) {
                HashMap map = new HashMap();
                map.put("Result", new Boolean(false));
                map.put("Message", "Received IllegalStateException firing event on NullActivity ended in a transaction that rolledback.");
                TCKSbbUtils.getResourceInterface().sendSbbMessage(map);
                return;
            }
        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }
    }

    public void onTest862Event(Test862Event event, ActivityContextInterface aci) {
        try {
            // Retrieve the NullActivity from the ACNF, end it.  Do NOT rollback TXN.
            ActivityContextNamingFacility facility = (ActivityContextNamingFacility) TCKSbbUtils.getSbbEnvironment().lookup("slee/facilities/activitycontextnaming");
            ActivityContextInterface nullACI = facility.lookup(ACTIVITY_NAME);
            NullActivity nullActivity = (NullActivity) nullACI.getActivity();
            nullActivity.endActivity();
        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }
    }


    public void onTest862SecondEvent(Test862SecondEvent event, ActivityContextInterface aci) {
        try {
            // Retreive the NullActivity from the ACNF.  Fire Test862ThirdEvent on it, expect IllegalStateException
            ActivityContextNamingFacility facility = (ActivityContextNamingFacility) TCKSbbUtils.getSbbEnvironment().lookup("slee/facilities/activitycontextnaming");
            ActivityContextInterface nullACI = facility.lookup(ACTIVITY_NAME);

            try {
                fireTest862ThirdEvent(new Test862ThirdEvent(), nullACI, null);
            } catch (java.lang.IllegalStateException e) {
                HashMap map = new HashMap();
                map.put("Result", new Boolean(true));
                map.put("Message", "Ok");
                TCKSbbUtils.getResourceInterface().sendSbbMessage(map);
                return;
            }

            // Shouldn't have fired the event successfully - the event handler will return failure.

        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }
    }

    public void onTest862ThirdEvent(Test862ThirdEvent event, ActivityContextInterface aci) {
        try {
            // Send test failure - shouldn't receive an event fired on a dead activity.
            HashMap map = new HashMap();
            map.put("Result", new Boolean(false));
            map.put("Message", "Was able to fire an event on a NullActivity that should have ended when its transaction was committed.");
            TCKSbbUtils.getResourceInterface().sendSbbMessage(map);
        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }
    }

    public abstract void fireTest862Event(Test862Event event, ActivityContextInterface aci, Address address);
    public abstract void fireTest862SecondEvent(Test862SecondEvent event, ActivityContextInterface aci, Address address);
    public abstract void fireTest862ThirdEvent(Test862ThirdEvent event, ActivityContextInterface aci, Address address);
}
