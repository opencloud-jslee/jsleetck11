/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.management.ManagementMBean;

import com.opencloud.sleetck.lib.AbstractSleeTCKTest;
import com.opencloud.sleetck.lib.TCKTestErrorException;
import com.opencloud.sleetck.lib.TCKTestResult;

import javax.slee.management.DeployableUnitID;

public class Test1579Test extends AbstractSleeTCKTest {

    private static final String SERVICE_DU_PATH_PARAM = "serviceDUPath";
    private static final String INVALID_SERVICE_DU_PATH_PARAM = "serviceInvalidDUPath";
    private static final int TEST_ID = 1579;

    /**
     * Perform the actual test.
     */
    public TCKTestResult run() throws Exception {

        DeployableUnitID duID = null;
        // Install the Deployable Unit.
        String duPath = utils().getTestParams().getProperty(SERVICE_DU_PATH_PARAM);
        boolean passed = false;

        try {
            duID = utils().install(duPath);
        } catch (TCKTestErrorException e) {
            if (e.getEnclosedException().getClass().equals(javax.slee.management.AlreadyDeployedException.class)) {
                passed = true;
            } else {
                throw (e);
            }
        }
        if (!passed) return TCKTestResult.failed(TEST_ID, "Succeeded in installing two components of the same type " +
                "with the same identity simultaneously. DeployableUnitID:"+duID);

        passed = false;
        try {
            duID = utils().install(utils().getTestParams().getProperty(INVALID_SERVICE_DU_PATH_PARAM));
        } catch (TCKTestErrorException e) {
            if (e.getEnclosedException().getClass().equals(javax.slee.management.DeploymentException.class)) {
                passed = true;
            } else {
                throw (e);
            }
        }

        if (!passed)return TCKTestResult.failed(1525, "Succeeded in installing two components of the same type with " +
                "the same identity simultaneously. DeployableUnitID:"+duID);

        return TCKTestResult.passed();
    }

    // Empty implementation
    public void setUp() throws Exception {}

}
