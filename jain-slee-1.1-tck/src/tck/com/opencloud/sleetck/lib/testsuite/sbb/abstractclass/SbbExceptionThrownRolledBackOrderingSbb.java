/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.sbb.abstractclass;

import com.opencloud.sleetck.lib.sbbutils.BaseTCKSbb;
import com.opencloud.sleetck.lib.sbbutils.TCKSbbUtils;
import com.opencloud.sleetck.lib.resource.events.TCKResourceEventX;
import com.opencloud.sleetck.lib.resource.sbbapi.TCKActivity;

import java.util.HashMap;

import javax.slee.*;
import javax.slee.facilities.*;

/**
 * Test ordering of sbbExceptionThrown and sbbRolledBack methods from spec s6.12.1
 */
public abstract class SbbExceptionThrownRolledBackOrderingSbb extends BaseTCKSbb {
    public void onTCKResourceEventX1(TCKResourceEventX event, ActivityContextInterface aci) {
        throw new SLEEException("");
    }

    public void sbbExceptionThrown(Exception exception, Object event, ActivityContextInterface aci) {
        try {
            HashMap sbbData = new HashMap();
            sbbData.put("sbbExceptionThrown", Boolean.TRUE);
            TCKSbbUtils.getResourceInterface().sendSbbMessage(sbbData);
        }
        catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }
    }

    public void sbbRolledBack(javax.slee.RolledBackContext context) {
        try {
            HashMap sbbData = new HashMap();
            sbbData.put("sbbRolledBack", Boolean.TRUE);
            TCKSbbUtils.getResourceInterface().sendSbbMessage(sbbData);
        }
        catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }
    }
}

