/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.events.convergencename.modifyIsInitial;

import com.opencloud.sleetck.lib.TCKTestResult;
import com.opencloud.sleetck.lib.resource.TCKActivityID;
import com.opencloud.sleetck.lib.resource.events.TCKResourceEventX;
import com.opencloud.sleetck.lib.resource.testapi.TCKResourceTestInterface;
import com.opencloud.sleetck.lib.testsuite.events.convergencename.AbstractConvergenceNameTest;
import com.opencloud.sleetck.lib.testsuite.events.convergencename.InitialEventSelectorParameters;

import javax.slee.Address;
import javax.slee.AddressPlan;

/**
 * Tests the SLEE's handling of the isInitialEvent variable in the InitialEventSelector
 */
public class ModifyIsInitialEventTest extends AbstractConvergenceNameTest {

    public TCKTestResult run() throws Exception {
        TCKResourceTestInterface resource = utils().getResourceInterface();
        // we fire both events on this one activity, so that the event order will be maintained
        TCKActivityID activityID = resource.createActivity("ModifyIsInitialEventTest-Activity1");
        Address address = new Address(AddressPlan.IP,"1.0.0.1");

        // fire event 1, with a message indicating that isInitialEvent be set to false
        // (setting it to false should be prevent the Sbb creation, therefore the event
        //  should not be received)
        setMainAssertionID(764);
        sendEventAndWait(TCKResourceEventX.X1,"1",activityID,address,
            null,// null indicates that no SBB should be created for the event
            createIESParams(false));

        // fire event 2, with a message indicating that isInitialEvent be set to true
        // (setting it to true should allow the Sbb creation, therefore the event should be received)
        setMainAssertionID(1923);
        sendEventAndWait(TCKResourceEventX.X1,"2",activityID,address,"2",createIESParams(true));

        return TCKTestResult.passed();
    }

    /**
     * Returns an InitialEventSelectorParameters object with all values set
     * to null/false exception for
     * (1) the selectEventFlag (set to true)
     * (2) the changePossibleInIntialEventFlag (set to true)
     * (3) the possibleInitialEventFlag (set to the given value)
     */
    private InitialEventSelectorParameters createIESParams(boolean isInitialEventFlag) {
        return new InitialEventSelectorParameters(
            false,false,false,true,false,null,true,isInitialEventFlag,false,null);
    }

}