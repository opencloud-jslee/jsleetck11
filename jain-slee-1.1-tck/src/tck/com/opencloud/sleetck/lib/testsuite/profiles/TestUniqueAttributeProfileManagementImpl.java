/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.profiles;

import javax.slee.profile.ProfileManagement;
import javax.slee.profile.ProfileVerificationException;

/**
 * This profile management class will setup value in profileInitialize then copy value in profileStore to value2.
 * profileVerify will check that both values correspond to the valid value.
 */
public abstract class TestUniqueAttributeProfileManagementImpl implements ProfileManagement, TestUniqueAttributeProfileCMP {

    public void profileInitialize() {
        setUniqueAttribute(42);
    }

    public void profileLoad() {
    }

    public void profileStore() {
    }

    public void profileVerify() throws ProfileVerificationException {
    }
}
