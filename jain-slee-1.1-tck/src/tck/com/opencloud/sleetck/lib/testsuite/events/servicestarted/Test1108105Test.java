/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.events.servicestarted;

import java.rmi.RemoteException;
import java.util.Arrays;
import java.util.Map;

import javax.slee.ComponentID;
import javax.slee.ServiceID;
import javax.slee.management.DeployableUnitDescriptor;
import javax.slee.management.DeployableUnitID;
import javax.slee.management.ServiceState;

import com.opencloud.sleetck.lib.AbstractSleeTCKTest;
import com.opencloud.sleetck.lib.TCKTestFailureException;
import com.opencloud.sleetck.lib.resource.TCKActivityID;
import com.opencloud.sleetck.lib.resource.TCKSbbMessage;
import com.opencloud.sleetck.lib.resource.testapi.TCKResourceListener;
import com.opencloud.sleetck.lib.testutils.Assert;
import com.opencloud.sleetck.lib.testutils.BaseTCKResourceListener;
import com.opencloud.sleetck.lib.testutils.FutureResult;
import com.opencloud.sleetck.lib.testutils.jmx.DeploymentMBeanProxy;
import com.opencloud.sleetck.lib.testutils.jmx.ServiceManagementMBeanProxy;

/**
 * AssertionID(1108105): Test In the SLEE 1.0 specification, a Service Started
 * Event could be delivered to the root SBB of any Service, not just of the
 * Service that is starting.
 *
 */
public class Test1108105Test extends AbstractSleeTCKTest {

    public static final String SERVICE1_DU_PATH_PARAM = "service1DUPath";

    public static final String SERVICE2_DU_PATH_PARAM = "service2DUPath";

    private static final int defaultTimeout = 25000;

    /**
     * Perform the actual test.
     */

    public void run(FutureResult result) throws Exception {
        this.result = result;

        getLog().fine("Installing and activating service1");

        setupService(SERVICE1_DU_PATH_PARAM, true);

        getLog().fine("Installing service2");
        String duPath = utils().getTestParams().getProperty(SERVICE2_DU_PATH_PARAM);
        DeployableUnitID duID = utils().install(duPath);

        DeploymentMBeanProxy duProxy = utils().getDeploymentMBeanProxy();
        ServiceManagementMBeanProxy serviceProxy = utils().getServiceManagementMBeanProxy();
        DeployableUnitDescriptor duDesc = duProxy.getDescriptor(duID);
        ComponentID components[] = duDesc.getComponents();

        ServiceID secondService = null;

        for (int i = 0; i < components.length; i++) {
            if (components[i] instanceof ServiceID) {
                ServiceID service = (ServiceID) components[i];

                if (secondService == null) {
                    secondService = service;
                    continue;
                }
            }
        }

        if (secondService == null) {
            result.setError("Failed to find the second test services.");
            return;
        }

        getLog().fine("Activating service2");
        serviceProxy.activate(secondService);

        ServiceID services[] = serviceProxy.getServices(ServiceState.ACTIVE);

        if (services.length != 2 || !Arrays.asList(services).contains(secondService)) {
            result.setError("Failed to find two active service: found" + services.length + " instead.");
            return;
        }

        getLog().fine("The service " + secondService + " is in Active state");

        synchronized (this) {
            wait(defaultTimeout);
        }

        if (receivedCount == expectedCount) {
            getLog().info("Test should be passed here!");
            result.setPassed();
        }
        else
            result.setFailed(1108105, "Expected number of successful messages not received, messages were"
                    + "not delivered by the SBBs (expected " + expectedCount + ", received " + receivedCount + ")");

        for (int i = 0; i < services.length; i++)
            serviceProxy.deactivate(services[i]);

    }

    /**
     * Do all the pre-run configuration of the test.
     */

    public void setUp() throws Exception {
        getLog().fine("Connecting to resource");
        resourceListener = new TCKResourceListenerImpl();
        setResourceListener(resourceListener);
    }

    /**
     * Clean up after the test.
     */

    public void tearDown() throws Exception {
        // cleanup
        super.tearDown();
    }

    private class TCKResourceListenerImpl extends BaseTCKResourceListener {
        // TCKResourceListener methods

        public synchronized void onSbbMessage(TCKSbbMessage message, TCKActivityID calledActivity)
                throws RemoteException {

            Map sbbData = (Map) message.getMessage();
            String sbbTestName = "Test1108105Test";
            String sbbTestResult = (String) sbbData.get("result");
            String sbbTestMessage = (String) sbbData.get("message");
            int assertionID = ((Integer) sbbData.get("id")).intValue();
            getLog().info(
                    "Received message from SBB: testname=" + sbbTestName + ", result=" + sbbTestResult + ", message="
                            + sbbTestMessage + ", id=" + assertionID);
            try {
                Assert.assertEquals(assertionID, "Test " + sbbTestName + " failed.", "pass", sbbTestResult);
                receivedCount++;
            } catch (TCKTestFailureException ex) {
                result.setFailed(assertionID, sbbTestMessage);
            }
        }

        public void onException(Exception exception) throws RemoteException {
            getLog().warning("Received Exception from SBB or resource:");
            getLog().warning(exception);
            result.setError(exception);
        }
    }

    private TCKResourceListener resourceListener;

    private FutureResult result;

    private static final int expectedCount = 3;

    private int receivedCount = 0;
}
