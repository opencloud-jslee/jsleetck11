/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.sbb.abstractclass;

import com.opencloud.sleetck.lib.sbbutils.TCKSbbUtils;
import com.opencloud.sleetck.lib.resource.events.TCKResourceEventX;

import java.util.HashMap;

import javax.slee.*;
import javax.slee.facilities.*;

/**
 * Test legal reentrancy from s6.13
 */
public abstract class ReentrantSbb extends SendResultsSbb {
    public void onTCKResourceEventX1(TCKResourceEventX event, ActivityContextInterface aci) {
        try {
            ReentrantSbbLocal me = (ReentrantSbbLocal)getSbbContext().getSbbLocalObject();

            setValue(7);
            me.doOneThing();

            setValue(16);
            me.doAnother();

            TCKSbbUtils.createTrace(getSbbID(), Level.INFO, "reentrant methods invoked okay", null);
            setResultPassed("reentrancy test passed");
        }
        catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }
    }

    public void doOneThing() {
        try {
            TCKSbbUtils.createTrace(getSbbID(), Level.INFO, "in doOneThing() method", null);

            if (getValue() != 7) {
                setResultFailed(480, "reentrant method invoked on object with different transactional state");
            }

        }
        catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }
    }

    public void doAnother() {
        try {
            TCKSbbUtils.createTrace(getSbbID(), Level.INFO, "in doAnother() method", null);

            if (getValue() != 16) {
                setResultFailed(480, "reentrant method invoked on object with different transactional state");
            }
        }
        catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }
    }

    public abstract void setValue(int i);
    public abstract int getValue();
}

