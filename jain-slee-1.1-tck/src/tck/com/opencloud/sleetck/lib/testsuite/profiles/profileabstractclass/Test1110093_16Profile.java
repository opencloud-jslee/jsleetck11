/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.profiles.profileabstractclass;

import javax.slee.CreateException;
import javax.slee.profile.Profile;
import javax.slee.profile.ProfileContext;
import javax.slee.profile.ProfileVerificationException;

/**
 * Management methods implementations are final
 */
public abstract class Test1110093_16Profile implements ProfileAbstractClassTestsProfileCMP, Profile, Test1110093ProfileManagement  {

    public final void manage1() {}

    public final int manage2(int dummy) {
        return -1;
    }

    public final String manage3(int dummy, String dummy2) {
        return "";
    }

    public void setProfileContext(ProfileContext context) {
    }

    public void unsetProfileContext() {
    }

    public void profileInitialize() {
    }

    public void profilePostCreate() throws CreateException {
    }

    public void profileActivate() {
    }

    public void profilePassivate() {
    }

    public void profileLoad() {
    }

    public void profileStore() {
    }

    public void profileRemove() {
    }

    public void profileVerify() throws ProfileVerificationException {
    }
}
