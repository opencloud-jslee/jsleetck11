/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.events.sbbabstractclass;

import java.util.HashMap;

import javax.slee.ActivityContextInterface;
import javax.slee.EventContext;
import javax.slee.facilities.Tracer;

import com.opencloud.sleetck.lib.resource.events.TCKResourceEventX;
import com.opencloud.sleetck.lib.sbbutils.BaseTCKSbb;
import com.opencloud.sleetck.lib.sbbutils.TCKSbbUtils;

/**
 * AssertionID(1108054): Test An additional fire event method has been 
 * added. This method allows the SBB to fire an event to a particular 
 * target Service on an Activity Context.
 */
public abstract class Test1108054Sbb2 extends BaseTCKSbb {

    // Initial event method.
    public void onTCKResourceEventX1(TCKResourceEventX event, ActivityContextInterface aci, EventContext context) {
        try {
            tracer = getSbbContext().getTracer("com.test");
            tracer.info("Sbb2: Received a TCK event " + event);
            setTestName((String) event.getMessage());
        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }
    }

    public void onTest1108054Event(Test1108054Event event, ActivityContextInterface aci, EventContext context) {
        try {
            tracer = getSbbContext().getTracer("com.test");
            tracer.info("Sbb2: Received a custom event " + event);
            if (getTestName().equals("SERVICEID"))
                sendResultToTCK(1108054, "Sbb1 fired an event to a particular target Service1 only, "
                        + "hence there should be no event receives in this Service2. But Sbb2 received the "
                        + "custom event fired by Sbb1!", "Test1108054Test", false);
            else if (getTestName().equals("NULLSERVICEID"))
                sendResultToTCK(1108054, "Sbb2: SBB entities belonging to all Services are eligible to receive the event."
                        + "Sbb2 received the custom event " + event, "Test1108054Test", true);
            else {
                tracer.severe("Unexpected test name encountered during X1 event handler: " + getTestName());
            }
            return;
        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }
    }

    private void sendResultToTCK(int failedAssertionID, String message, String testName, boolean result) throws Exception {
        HashMap sbbData = new HashMap();
        sbbData.put("testname", testName);
        sbbData.put("result", result ? "pass" : "fail");
        sbbData.put("message", message);
        sbbData.put("id", new Integer(failedAssertionID));
        TCKSbbUtils.getResourceInterface().sendSbbMessage(sbbData);
    }

    public abstract void setTestName(String testName);

    public abstract String getTestName();

    private Tracer tracer;
}
