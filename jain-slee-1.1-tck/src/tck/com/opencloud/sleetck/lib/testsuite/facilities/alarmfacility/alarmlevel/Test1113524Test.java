/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.facilities.alarmfacility.alarmlevel;

import java.rmi.RemoteException;
import java.util.Map;

import javax.slee.management.DeployableUnitID;

import com.opencloud.sleetck.lib.AbstractSleeTCKTest;
import com.opencloud.sleetck.lib.DescriptionKeys;
import com.opencloud.sleetck.lib.TCKTestFailureException;
import com.opencloud.sleetck.lib.resource.TCKActivityID;
import com.opencloud.sleetck.lib.resource.TCKSbbMessage;
import com.opencloud.sleetck.lib.resource.events.TCKResourceEventX;
import com.opencloud.sleetck.lib.resource.testapi.TCKResourceListener;
import com.opencloud.sleetck.lib.resource.testapi.TCKResourceTestInterface;
import com.opencloud.sleetck.lib.testutils.Assert;
import com.opencloud.sleetck.lib.testutils.BaseTCKResourceListener;
import com.opencloud.sleetck.lib.testutils.FutureResult;

/*
 * AssertionID (1113524): Test The isHigherLevel method returns true if the 
 * level represented by this AlarmLevel object is higher or more severe than 
 * the level specified by AlarmLevel object passed as the other argument.
 * 
 * AssertionID (1113525): Test CLEAR has a higher level than CRITICAL,

 * AssertionID (1113527): Test it throws a java.lang.NullPointerException 
 * if the other argument is null.
 * 
 * AssertionID (1113530): Test the fromInt and toInt methods allow conversion 
 * between the AlarmLevel object form and numeric form.
 * 
 * AssertionID (1113531): Test The fromInt method throws a java.lang.IllegalArgumentException 
 * if the level argument is not one of the six integer representations.
 * 
 * AssertionID (1113532): Test The fromString method allows conversion between String and AlarmLevel 
 * forms. The conversion is case insensitive.
 * 
 * AssertionID (1113533): Test The fromString method throws java.lang.NullPointerException 
 * - if the argument is null.
 * 
 * AssertionID (1113534): Test The fromString method throws a java.lang.IllegalArgumentException
 * if the argument is not an alarm level known to this class.
 * 
 */
public class Test1113524Test extends AbstractSleeTCKTest {

    /**
     * Perform the actual test.
     */
    public void run(FutureResult result) throws Exception {
        this.result = result;

        String activityName = "Test1113524Test";
        TCKResourceTestInterface resource = utils().getResourceInterface();
        TCKActivityID activityID = resource.createActivity(activityName);
        getLog().info("Firing event: " + testName);

        // kickoff the test
        resource.fireEvent(TCKResourceEventX.X1, testName, activityID, null);
    }

    /**
     * Do all the pre-run configuration of the test.
     */

    public void setUp() throws Exception {

        getLog().fine("Connecting to resource");
        getLog().fine("Installing and activating service");

        resourceListener = new TCKResourceListenerImpl();
        setResourceListener(resourceListener);

        // Install the Deployable Units
        String duPath = utils().getTestParams().getProperty(DescriptionKeys.SERVICE_DU_PATH_PARAM);
        duID = utils().install(duPath);

        // Activate the DU
        utils().activateServices(duID, true);

    }

    /**
     * Clean up after the test.
     */

    public void tearDown() throws Exception {
        // cleanup
        super.tearDown();
    }

    private class TCKResourceListenerImpl extends BaseTCKResourceListener {
        // TCKResourceListener methods

        public synchronized void onSbbMessage(TCKSbbMessage message, TCKActivityID calledActivity)
                throws RemoteException {

            Map sbbData = (Map) message.getMessage();
            String sbbTestName = "Test1113524Test";
            String sbbTestResult = (String) sbbData.get("result");
            String sbbTestMessage = (String) sbbData.get("message");
            int assertionID = ((Integer) sbbData.get("id")).intValue();
            getLog().info(
                    "Received message from SBB: testname=" + sbbTestName + ", result=" + sbbTestResult + ", message="
                            + sbbTestMessage + ", id=" + assertionID);
            try {
                Assert.assertEquals(assertionID, "Test " + sbbTestName + " failed.", "pass", sbbTestResult);
                result.setPassed();
            } catch (TCKTestFailureException ex) {
                result.setFailed(assertionID, sbbTestMessage);
            }
        }

        public void onException(Exception exception) throws RemoteException {
            getLog().warning("Received Exception from SBB or resource:");
            getLog().warning(exception);
            result.setError(exception);
        }
    }

    // private SleeTCKTestUtils utils;
    private TCKResourceListener resourceListener;

    private FutureResult result;

    private DeployableUnitID duID;

    private String testName = "Test1113524";

}