/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.usage.sets;

import com.opencloud.sleetck.lib.TCKTestResult;
import com.opencloud.sleetck.lib.resource.TCKActivityID;
import com.opencloud.sleetck.lib.resource.events.TCKResourceEventX;
import com.opencloud.sleetck.lib.testsuite.usage.common.GenericUsageMBeanLookup;
import com.opencloud.sleetck.lib.testsuite.usage.common.GenericUsageSbbInstructions;
import com.opencloud.sleetck.lib.testsuite.usage.common.GenericUsageTest;
import com.opencloud.sleetck.lib.testutils.QueuingNotificationListener;
import com.opencloud.sleetck.lib.testutils.QueuingResourceListener;

/**
 * Makes updates to parameters of the same name in different parameter sets --
 * the unnamed set and a named set -- then checks that the updates were independent.
 */
public class Test2253and2254Test extends GenericUsageTest {

    private static final int TEST_ID_FOR_UNNAMED = 2253;
    private static final int TEST_ID_FOR_NAMED   = 2254;
    private static final String NAMED_SET_NAME = "NamedParameterSet";

    public TCKTestResult run() throws Exception {

        TCKActivityID activityID = utils().getResourceInterface().createActivity("token activity");

        GenericUsageSbbInstructions instructionsForUnnamed = new GenericUsageSbbInstructions(null);
        instructionsForUnnamed.addFirstCountIncrement(1);
        instructionsForUnnamed.addTimeBetweenNewConnectionsSamples(2);
        getLog().info("firing event to Sbb");
        utils().getResourceInterface().fireEvent(TCKResourceEventX.X1,instructionsForUnnamed.toExported(),activityID,null);

        GenericUsageSbbInstructions instructionsForNamed = new GenericUsageSbbInstructions(NAMED_SET_NAME);
        instructionsForNamed.addFirstCountIncrement(4);
        instructionsForNamed.addTimeBetweenNewConnectionsSamples(5);
        getLog().info("firing event to Sbb");
        utils().getResourceInterface().fireEvent(TCKResourceEventX.X1,instructionsForNamed.toExported(),activityID,null);

        getLog().info("waiting for replies");
        resourceListener.nextMessage();
        resourceListener.nextMessage();
        getLog().info("received replies");

        getLog().info("waiting for usage notifications");
        for (int i = 0; i < instructionsForUnnamed.getTotalUpdates(); i++) {
              notificationListenerUnnamed.nextNotification();
        }
        for (int i = 0; i < instructionsForNamed.getTotalUpdates(); i++) {
              notificationListenerNamed.nextNotification();
        }
        getLog().info("received all "+(instructionsForUnnamed.getTotalUpdates()+instructionsForNamed.getTotalUpdates())+" usage notifications");

        getLog().info("checking that parameter updates to both sets were independent");

        long firstCountForUnnamed = mBeanLookup.getUnnamedGenericUsageMBeanProxy().getFirstCount(false);
        long firstCountForNamed = mBeanLookup.getNamedGenericSbbUsageMBeanProxy(NAMED_SET_NAME).getFirstCount(false);
        double timeBetweenNewConnectionsCountForUnnamed =
            mBeanLookup.getUnnamedGenericUsageMBeanProxy().getTimeBetweenNewConnections(false).getSampleCount();
        double timeBetweenNewConnectionsCountForNamed =
            mBeanLookup.getNamedGenericSbbUsageMBeanProxy(NAMED_SET_NAME).getTimeBetweenNewConnections(false).getSampleCount();

        String expectedValues = "firstCountForUnnamed="+1+",firstCountForNamed="+4+
            "timeBetweenNewConnectionsCountForUnnamed="+1+"timeBetweenNewConnectionsCountForNamed="+1;
        String actualValues = "firstCountForUnnamed="+firstCountForUnnamed+",firstCountForNamed="+firstCountForNamed+
            "timeBetweenNewConnectionsCountForUnnamed="+timeBetweenNewConnectionsCountForUnnamed+
            "timeBetweenNewConnectionsCountForNamed="+timeBetweenNewConnectionsCountForNamed;

        if(firstCountForNamed == 0) return TCKTestResult.failed(TEST_ID_FOR_NAMED,"Updates to the firstCount parameter "+
                "in a named parameter set were not applied.");
        if(firstCountForNamed == 5) return TCKTestResult.failed(TEST_ID_FOR_UNNAMED,"Updates to firstCount usage parameter in different "+
                "parameter sets were combined -- the parameter sets should be independent.");
        if(firstCountForUnnamed == 0) return TCKTestResult.failed(TEST_ID_FOR_UNNAMED,"Updates to the firstCount parameter "+
                "in the unnamed parameter set were not applied.");
        if(firstCountForUnnamed == 5) return TCKTestResult.failed(TEST_ID_FOR_NAMED,"Updates to firstCount usage parameter in different "+
                "parameter sets were combined -- the parameter sets should be independent.");

        if(timeBetweenNewConnectionsCountForNamed == 0) return TCKTestResult.failed(TEST_ID_FOR_NAMED,"Updates to the timeBetweenNewConnections parameter "+
                "in a named parameter set were not applied.");
        if(timeBetweenNewConnectionsCountForNamed == 2) return TCKTestResult.failed(TEST_ID_FOR_UNNAMED,"Updates to timeBetweenNewConnections usage parameter in different "+
                "parameter sets were combined -- the parameter sets should be independent.");
        if(timeBetweenNewConnectionsCountForUnnamed == 0) return TCKTestResult.failed(TEST_ID_FOR_UNNAMED,"Updates to the timeBetweenNewConnections parameter "+
                "in the unnamed parameter set were not applied.");
        if(timeBetweenNewConnectionsCountForUnnamed == 2) return TCKTestResult.failed(TEST_ID_FOR_NAMED,"Updates to timeBetweenNewConnections usage parameter in different "+
                "parameter sets were combined -- the parameter sets should be independent.");

        if(firstCountForUnnamed != 1 || timeBetweenNewConnectionsCountForUnnamed != 1)
            return TCKTestResult.failed(TEST_ID_FOR_UNNAMED,"Usage parameter values were not set as expected. Expected values: "+
                    expectedValues+", actual values: "+actualValues);
        if(firstCountForNamed != 4 || timeBetweenNewConnectionsCountForNamed != 1)
            return TCKTestResult.failed(TEST_ID_FOR_NAMED,"Usage parameter values were not set as expected. Expected values: "+
                    expectedValues+", actual values: "+actualValues);
        getLog().info("parameter checks ok");
        return TCKTestResult.passed();
    }

    public void setUp() throws Exception {
        super.setUp();
        setResourceListener(resourceListener = new QueuingResourceListener(utils()));
        mBeanLookup = getGenericUsageMBeanLookup();
        mBeanLookup.getServiceUsageMBeanProxy().createUsageParameterSet(mBeanLookup.getSbbID(),NAMED_SET_NAME);
        notificationListenerUnnamed = new QueuingNotificationListener(utils());
        notificationListenerNamed = new QueuingNotificationListener(utils());
        mBeanLookup.getUnnamedGenericUsageMBeanProxy().addNotificationListener(notificationListenerUnnamed,null,null);
        mBeanLookup.getNamedGenericSbbUsageMBeanProxy(NAMED_SET_NAME).addNotificationListener(notificationListenerNamed,null,null);
    }

    public void tearDown() throws Exception {
        if(mBeanLookup != null) {
            if(notificationListenerUnnamed != null)mBeanLookup.getUnnamedGenericUsageMBeanProxy().removeNotificationListener(notificationListenerUnnamed);
            if(notificationListenerNamed != null)mBeanLookup.getNamedGenericSbbUsageMBeanProxy(NAMED_SET_NAME).removeNotificationListener(notificationListenerNamed);
        }
        super.tearDown();
    }

    private GenericUsageMBeanLookup mBeanLookup;
    private QueuingResourceListener resourceListener;
    private QueuingNotificationListener notificationListenerUnnamed;
    private QueuingNotificationListener notificationListenerNamed;

}
