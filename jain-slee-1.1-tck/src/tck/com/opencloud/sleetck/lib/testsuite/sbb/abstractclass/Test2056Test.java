/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.sbb.abstractclass;

import com.opencloud.sleetck.lib.SleeTCKTest;
import com.opencloud.sleetck.lib.SleeTCKTestUtils;
import com.opencloud.sleetck.lib.TCKTestResult;
import com.opencloud.sleetck.lib.resource.TCKActivityID;
import com.opencloud.sleetck.lib.resource.TCKSbbMessage;
import com.opencloud.sleetck.lib.resource.events.TCKResourceEventX;
import com.opencloud.sleetck.lib.resource.testapi.TCKResourceTestInterface;
import com.opencloud.sleetck.lib.testutils.BaseTCKResourceListener;
import com.opencloud.sleetck.lib.testutils.FutureResult;
import com.opencloud.util.Future;

import javax.slee.management.DeployableUnitID;
import java.rmi.RemoteException;

public class Test2056Test implements SleeTCKTest {

    private static final String SERVICE_DU_PATH_PARAM = "DUPath";
    private static final int TEST_ID = 2056;

    public void init(SleeTCKTestUtils utils) {
        this.utils = utils;
    }

    /**
     * Perform the actual test.
     */

    public TCKTestResult run() throws Exception {
        futureResult = new FutureResult(utils.getLog());

        TCKResourceTestInterface resource = utils.getResourceInterface();
        TCKActivityID activityID = resource.createActivity("Test2056InitialActivity");
        resource.fireEvent(TCKResourceEventX.X1, TCKResourceEventX.X1, activityID, null);

        try {
            return futureResult.waitForResult(utils.getTestTimeout());
        } catch (Future.TimeoutException e) {
            synchronized(futureResult) {
                if(hasReceivedFirstMessage) {
                    utils.getLog().info("Timed out as expected waiting for notification of a second invocation of "+
                            "sbbExceptionThrown()");
                    return TCKTestResult.passed();
                } else {
                    return TCKTestResult.error("Timed out waiting for a notification that the SBB received an "+
                            "sbbExceptionThrown() callback after throwing a RuntimeException from the sbbCreate() method",e);
                }
            }
        }
    }

    /**
     * Do all the pre-run configuration of the test.
     */

    public void setUp() throws Exception {

        utils.getLog().fine("Connecting to resource");
        resourceListener = new TCKResourceListenerImpl();
        utils.getResourceInterface().setResourceListener(resourceListener);
        utils.getLog().fine("Installing and activating service");

        // Install the Deployable Units
        String duPath = utils.getTestParams().getProperty(SERVICE_DU_PATH_PARAM);
        duID = utils.install(duPath);

        // Activate the DU
        utils.activateServices(duID, true);

    }

    /**
     * Clean up after the test.
     */

    public void tearDown() throws Exception {
        utils.getLog().fine("Disconnecting from resource");
        utils.getResourceInterface().clearActivities();
        utils.getResourceInterface().removeResourceListener();
        utils.getLog().fine("Deactivating and uninstalling service");
        utils.deactivateAllServices();
        utils.uninstallAll();
    }

    private class TCKResourceListenerImpl extends BaseTCKResourceListener {
        public void onSbbMessage(TCKSbbMessage sbbMessage, TCKActivityID calledActivity) throws RemoteException {
            String exceptionMessage = (String)sbbMessage.getMessage();
            if(Test2056Sbb.EXCEPTION_FROM_SBB_CREATE.equals(exceptionMessage)) {
                if(!hasReceivedFirstMessage) {
                    utils.getLog().info("The SBB received the expected sbbExceptionThrown() callback for the "+
                            "exception thrown in sbbCreate()");
                    synchronized(futureResult) {
                        hasReceivedFirstMessage = true;
                    }
                } else {
                    utils.getLog().info("The SBB received another sbbExceptionThrown() callback for an "+
                            "exception thrown in sbbCreate()");
                }
            } else if(Test2056Sbb.EXCEPTION_FROM_SBB_POST_CREATE.equals(exceptionMessage)) {
                String failureMessage = "The SBB received an sbbExceptionThrown() callback for the "+
                        "exception thrown in sbbPostCreate(). sbbPostCreate() should never have even been called, "+
                        "as the SBB threw a RuntimeException in sbbCreate(), which should have caused a call to "+
                        "sbbExceptionThrown(), then the SBB should have been put in the Does Not Exist state.";
                synchronized(futureResult) {
                    if(!hasReceivedFirstMessage) failureMessage += "Note: the call to sbbExceptionThrown() following sbbCreate() was not confirmed";
                }
                futureResult.setFailed(TEST_ID,failureMessage);
            }
        }

        public void onException(Exception exception) throws RemoteException {
            futureResult.setError(exception);
        }
    }

    // set tp true when the test receives notification of the first call to sbbExceptionThrown() for the Exception thrown in sbbCreate()
    private boolean hasReceivedFirstMessage;

    private SleeTCKTestUtils utils;
    private TCKResourceListenerImpl resourceListener;
    private DeployableUnitID duID;
    private FutureResult futureResult;

}
