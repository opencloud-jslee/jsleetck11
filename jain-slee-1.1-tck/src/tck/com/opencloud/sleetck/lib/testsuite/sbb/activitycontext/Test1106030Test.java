/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.sbb.activitycontext;

import java.rmi.RemoteException;
import java.util.HashMap;

import javax.slee.management.DeployableUnitID;

import com.opencloud.sleetck.lib.SleeTCKTest;
import com.opencloud.sleetck.lib.SleeTCKTestUtils;
import com.opencloud.sleetck.lib.TCKTestResult;
import com.opencloud.sleetck.lib.resource.TCKActivityID;
import com.opencloud.sleetck.lib.resource.TCKSbbMessage;
import com.opencloud.sleetck.lib.resource.events.TCKResourceEventX;
import com.opencloud.sleetck.lib.resource.testapi.TCKResourceTestInterface;
import com.opencloud.sleetck.lib.testutils.BaseTCKResourceListener;
import com.opencloud.sleetck.lib.testutils.QueuingResourceListener;

public class Test1106030Test implements SleeTCKTest {

    private static final String SERVICE_DU_PATH_PARAM = "DUPath";
    private static final int TEST_ID = 1106030;

    public void init(SleeTCKTestUtils utils) {
        this.utils = utils;
    }

    /**
     * Perform the actual test.
     */

    public TCKTestResult run() throws Exception {

        TCKSbbMessage message;
        HashMap messageContents;
        Boolean passed;
        String msgString;

        TCKActivityID activityID = resource.createActivity("Test1106030InitialActivity");

        // Fire event X1
        resource.fireEvent(TCKResourceEventX.X1, TCKResourceEventX.X1, activityID, null);
        message = queue.nextMessage();
        messageContents = (HashMap) message.getMessage();
        passed = (Boolean) messageContents.get("Result");
        msgString = (String) messageContents.get("Message");
        utils.getLog().info("Received message from SBB: "+msgString);
        if (!passed.booleanValue()) 
            return TCKTestResult.failed(TEST_ID, msgString);

        // Fire event X2
        resource.fireEvent(TCKResourceEventX.X2, TCKResourceEventX.X2, activityID, null);
        message = queue.nextMessage();
        messageContents = (HashMap) message.getMessage();
        passed = (Boolean) messageContents.get("Result");
        msgString = (String) messageContents.get("Message");
        utils.getLog().info("Received message from SBB: "+msgString);
        if (!passed.booleanValue()) 
            return TCKTestResult.failed(TEST_ID, msgString);

        return TCKTestResult.passed();

    }

    /**
     * Do all the pre-run configuration of the test.
     */

    public void setUp() throws Exception {
        String duPath = utils.getTestParams().getProperty(SERVICE_DU_PATH_PARAM);
        duID = utils.install(duPath);
        utils.activateServices(duID, true);

        resource = utils.getResourceInterface();
        queue = new QueuingResourceListener(utils);
        resource.setResourceListener(queue);
    }


    /**
     * Clean up after the test.
     */

    public void tearDown() throws Exception {
        utils.getLog().fine("Disconnecting from resource");
        utils.getResourceInterface().clearActivities();
        utils.getResourceInterface().removeResourceListener();
        utils.getLog().fine("Deactivating and uninstalling service");
        utils.deactivateAllServices();
        utils.uninstallAll();
    }

    private SleeTCKTestUtils utils;
    private DeployableUnitID duID;
    private TCKResourceTestInterface resource; 
    private QueuingResourceListener queue;
}
