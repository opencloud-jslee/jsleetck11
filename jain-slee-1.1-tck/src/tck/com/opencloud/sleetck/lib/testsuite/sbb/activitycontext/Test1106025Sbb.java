/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.sbb.activitycontext;

import java.util.HashMap;

import javax.naming.InitialContext;
import javax.slee.ActivityContextInterface;
import javax.slee.nullactivity.NullActivity;
import javax.slee.nullactivity.NullActivityContextInterfaceFactory;
import javax.slee.nullactivity.NullActivityFactory;

import com.opencloud.sleetck.lib.resource.TCKActivityID;
import com.opencloud.sleetck.lib.resource.events.TCKResourceEventX;
import com.opencloud.sleetck.lib.resource.sbbapi.TCKActivity;
import com.opencloud.sleetck.lib.resource.sbbapi.TCKActivityContextInterfaceFactory;
import com.opencloud.sleetck.lib.sbbutils.BaseTCKSbb;
import com.opencloud.sleetck.lib.sbbutils.TCKSbbConstants;
import com.opencloud.sleetck.lib.sbbutils.TCKSbbUtils;

public abstract class Test1106025Sbb extends BaseTCKSbb {
    private final static String nullACIName = "java:comp/env/slee/nullactivity/factory";
    private final static String nullACIFactoryName = "java:comp/env/slee/nullactivity/activitycontextinterfacefactory";

    /*
     * Test whether an activity context stored in a CMP field is null
     * after the activity has ended.
     */

    public void onTCKResourceEventX1(TCKResourceEventX ev, ActivityContextInterface aci) {
        HashMap map = new HashMap();

        try {
            TCKActivityID activityID = TCKSbbUtils.getResourceAdaptorInterface().getResource().createActivity("Test1106025SbbActivity");

            // Get the ACI and store in CMP field
            TCKActivity tckActivity = TCKSbbUtils.getResourceInterface().getActivity(activityID);
            TCKActivityContextInterfaceFactory aciFactory = (TCKActivityContextInterfaceFactory) TCKSbbUtils.getSbbEnvironment().lookup(TCKSbbConstants.TCK_ACI_FACTORY_LOCATION);
            ActivityContextInterface sbbActivityACI = aciFactory.getActivityContextInterface(tckActivity);
            setMyActivityContext(sbbActivityACI);

            // check that the ACI is there
            if (null == getMyActivityContext()) {
                map.put("Result", new Boolean(false));
                map.put("ID", new Integer(1106025));
                map.put("Message", "ERROR: ActivityContext stored in CMP field was null before the activity had ended.");
                TCKSbbUtils.getResourceInterface().sendSbbMessage(map);
            }

            // End Activity to remove ACI
            TCKSbbUtils.getResourceAdaptorInterface().getResource().endActivity(activityID);
            return;
        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }

    }

    public void onTCKResourceEventX2(TCKResourceEventX ev, ActivityContextInterface aci) {
        HashMap map = new HashMap();

        try {
            // Check to see if ACI is no longer present
            if (null == getMyActivityContext()) {
                // test passed
                map.put("Result", new Boolean(true));
                map.put("ID", new Integer(1106025));
                map.put("Message", "Test passed.");
            } else {
                // test failed
                map.put("Result", new Boolean(false));
                map.put("ID", new Integer(1106025));
                map.put("Message", "ERROR: ActivityContext stored in CMP field was not null after the activity had ended.");
            }

            TCKSbbUtils.getResourceInterface().sendSbbMessage(map);
        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }
    }


    public abstract void setMyActivityContext(ActivityContextInterface object);
    public abstract ActivityContextInterface getMyActivityContext();
}
