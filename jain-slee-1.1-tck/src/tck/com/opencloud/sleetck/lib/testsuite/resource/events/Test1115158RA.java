/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.resource.events;

import java.util.HashMap;

import javax.slee.Address;
import javax.slee.EventTypeID;
import javax.slee.ServiceID;
import javax.slee.resource.ActivityHandle;
import javax.slee.resource.ConfigProperties;
import javax.slee.resource.EventFlags;
import javax.slee.resource.FailureReason;
import javax.slee.resource.FireableEventType;
import javax.slee.resource.InvalidConfigurationException;
import javax.slee.resource.Marshaler;
import javax.slee.resource.ReceivableService;
import javax.slee.resource.ResourceAdaptorContext;
import javax.slee.resource.SleeEndpoint;

import com.opencloud.sleetck.lib.infra.SleeTCKComponentConstants;
import com.opencloud.sleetck.lib.rautils.BaseTCKRA;
import com.opencloud.sleetck.lib.resource.adaptor11.TCKActivityHandleImpl;
import com.opencloud.sleetck.lib.testsuite.resource.RAMethods;
import com.opencloud.sleetck.lib.testsuite.resource.TCKMessage;
import com.opencloud.sleetck.lib.testsuite.resource.SimpleEvent;

public class Test1115158RA extends BaseTCKRA {

    // Configuration related

    public void raConfigurationUpdate(ConfigProperties properties) {
    }

    public void raConfigure(ConfigProperties properties) {
    }

    public void raVerifyConfiguration(ConfigProperties properties) throws InvalidConfigurationException {
    }

    public void raUnconfigure() {
    }

    //
    // Ignored
    //

    public void eventProcessingFailed(ActivityHandle handle, FireableEventType eventType, Object event, Address address, ReceivableService serviceInfo, int flags, FailureReason reason) {
        // Impossible to test
    }

    public void eventProcessingSuccessful(ActivityHandle handle, FireableEventType eventType, Object event, Address address, ReceivableService serviceInfo, int flags) {
        getLog().info("Event processing successful: " + event);
        sendEventMessage(RAMethods.eventProcessingSuccessful, (SimpleEvent) event);
    }

    public void eventUnreferenced(ActivityHandle handle, FireableEventType eventType, Object event, Address address, ReceivableService serviceInfo, int flags) {
        getLog().info("Event unreferenced: " + event);
        sendEventMessage(RAMethods.eventUnreferenced, (SimpleEvent) event);
    }

    public Marshaler getMarshaler() {
        return null;
    }

    public void queryLiveness(ActivityHandle handle) {
    }

    public void serviceActive(ReceivableService serviceInfo) {
    }

    public void serviceInactive(ReceivableService serviceInfo) {
    }

    public void serviceStopping(ReceivableService serviceInfo) {
    }

    public void setResourceAdaptorContext(ResourceAdaptorContext context) {
        this.context = context;
        setTracer(context.getTracer("Test1115158RA"));
    }

    public void unsetResourceAdaptorContext() {
        setTracer(null);
        context = null;
    }

    public void activityEnded(ActivityHandle handle) {
    }

    public void activityUnreferenced(ActivityHandle handle) {
    }

    public void administrativeRemove(ActivityHandle handle) {
    }

    public void raActive() {
        try {
            EventTypeID eventTypeID = new EventTypeID(SimpleEvent.SIMPLE_EVENT_NAME, SleeTCKComponentConstants.TCK_VENDOR, "1.1");
            FireableEventType fireableEventType = context.getEventLookupFacility().getFireableEventType(eventTypeID);
            SleeEndpoint endpoint = context.getSleeEndpoint();
            ActivityHandle handle = new TCKActivityHandleImpl(System.currentTimeMillis(), 0);
            getLog().info("Starting activity: " + handle);
            endpoint.startActivity(handle, handle);
            getLog().info("Firing event 1");
            endpoint.fireEvent(handle, fireableEventType, new SimpleEvent(1), null, null);
            getLog().info("Firing event 2 (no flags)");
            endpoint.fireEvent(handle, fireableEventType, new SimpleEvent(2), null, null, EventFlags.NO_FLAGS);
            getLog().info("Firing event 3 (unreferenced callback)");
            endpoint.fireEvent(handle, fireableEventType, new SimpleEvent(3), null, null, EventFlags.REQUEST_EVENT_UNREFERENCED_CALLBACK);
            getLog().info("Firing event 4 (processing successful callback)");
            endpoint.fireEvent(handle, fireableEventType, new SimpleEvent(4), null, null, EventFlags.REQUEST_PROCESSING_SUCCESSFUL_CALLBACK);
            getLog().info("Firing event 5 (both callbacks)");
            endpoint.fireEvent(handle, fireableEventType, new SimpleEvent(5), null, null, EventFlags.REQUEST_PROCESSING_SUCCESSFUL_CALLBACK
                    | EventFlags.REQUEST_EVENT_UNREFERENCED_CALLBACK);
            getLog().info("Ending activity");
            endpoint.endActivity(handle);
        } catch (Exception e) {
            context.getTracer("Test1115158RA").severe("An exception occured while firing events", e);
        }
    }

    public void raInactive() {
    }

    public void raStopping() {
    }

    //
    // Private
    //

    private void sendEventMessage(int methodID, SimpleEvent event) {
        HashMap results = new HashMap();
        results.put("event", event);
        results.put("fromsbb", Boolean.FALSE);
        sendMessage(new TCKMessage(getRAUID(), methodID, results));
    }

    private ResourceAdaptorContext context;
}
