/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.facilities.timerfacility;

import com.opencloud.sleetck.lib.sbbutils.BaseTCKSbb;
import com.opencloud.sleetck.lib.sbbutils.TCKSbbUtils;
import com.opencloud.sleetck.lib.resource.events.TCKResourceEventX;
import com.opencloud.sleetck.lib.resource.sbbapi.TCKActivity;

import javax.naming.Context;
import javax.naming.InitialContext;

import java.util.HashMap;

import javax.slee.*;
import javax.slee.facilities.*;
import javax.slee.nullactivity.*;

/**
 * Test that TimerEvents generated by the TimerFacility have the correct values
 */
public abstract class TimerEventSbb extends BaseTCKSbb {

    public void setSbbContext(SbbContext context) {
        super.setSbbContext(context);
        try {
            Context myEnv = (Context) new InitialContext().lookup("java:comp/env");
            timerFacility = (TimerFacility) myEnv.lookup("slee/facilities/timer");
        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }
    }

    // test starts here...
    public void onTCKResourceEventX1(TCKResourceEventX event, ActivityContextInterface aci) {
        try {
            TCKSbbUtils.createTrace(getSbbID(),Level.INFO,"Received start message",null);
            setTestName((String)event.getMessage());

            TimerID id = null;

            // fire in 5s
            long startTime = System.currentTimeMillis() + 5000;
            setStartTime(startTime);
            long period = 2000;
            int numRepetitions = 2;

            TimerOptions options = new TimerOptions();
            options.setPreserveMissed(TimerPreserveMissed.ALL);

            if (getTestName().equals("timerEvent-single")) {
                // set a single shot timer - this assumes setTimer actually works.
                id = timerFacility.setTimer(aci, null, startTime, options);
            }
            else if (getTestName().equals("timerEvent-periodic-finite")) {
                // set a finite periodic timer
                id = timerFacility.setTimer(aci, null, startTime, period, numRepetitions, options);
                setPeriod(period);
                setNumRepetitions(numRepetitions);
            }
            else if (getTestName().equals("timerEvent-periodic-infinite")) {
                // set an infinite periodic timer
                id = timerFacility.setTimer(aci, null, startTime, period, 0, options);
                setPeriod(period);
                setNumRepetitions(0);
            }
            setTimerID(id);

        } catch(Exception e) {
            TCKSbbUtils.handleException(e);
        }
    }

    public void onTimerEvent(TimerEvent event, ActivityContextInterface aci) {
        try {
            TCKSbbUtils.createTrace(getSbbID(),Level.INFO,"Received timer event",null);

            // increment count of timer events
            setNumTimerEvents(getNumTimerEvents() + 1);

            if (getTestName().equals("timerEvent-single")) {
                // test event attributes
                if (getStartTime() != event.getScheduledTime()) {
                    setResultFailed(1240, "non-periodic timer: timerEvent.getScheduledTime() != setTimer() startTime");
                }
                else if (event.getPeriod() != Long.MAX_VALUE) {
                    setResultFailed(1243, "non-periodic timer: timerEvent.getPeriod() != Long.MAX_VALUE");
                }
                else if (event.getNumRepetitions() != 1) {
                    setResultFailed(1244, "non-periodic timer: timerEvent.getNumRepetitions() != 1");
                }
                else if (event.getRemainingRepetitions() != 0) {
                    setResultFailed(1245, "non-periodic timer: timerEvent.getRemainingRepetitions() != 0");
                }
                else {
                    setResultPassed("non-periodic TimerEvent tests passed");
                }
                sendResultToTCK();
            }
            else if (getTestName().equals("timerEvent-periodic-finite")) {

                boolean failed = false;

                // if this is the first event, startTime should equal event.getScheduledTime()
                if (getNumTimerEvents() == 1) {
                    if (getStartTime() != event.getScheduledTime()) {
                        setResultFailed(1240, "periodic timer: timerEvent.getScheduledTime() did not match setTimer() startTime");
                        failed = true;
                    }
                } // subsequent events should have event.getScheduledTime() == startTime + period
                else if (getNumTimerEvents() == 2) {
                    if (getStartTime()+getPeriod() != event.getScheduledTime()) {
                        setResultFailed(1241, "periodic timer: timerEvent.getScheduledTime() did not match setTimer() startTime + period");
                        failed = true;
                    }
                }
                else if (event.getPeriod() != getPeriod()) {
                    setResultFailed(1243, "periodic timer: timerEvent.getPeriod() != setTimer() period");
                    failed = true;
                }
                else if (event.getNumRepetitions() != getNumRepetitions()) {
                    setResultFailed(1244, "periodic timer: timerEvent.getNumRepetitions() != setTimer() numRepetitions");
                    failed = true;
                }
                else if (event.getRemainingRepetitions() != (getNumRepetitions() - getNumTimerEvents())) {
                    setResultFailed(1245, "periodic timer: timerEvent.getRemainingRepetitions() incorrect");
                    failed = true;
                }

                if (failed) {
                    timerFacility.cancelTimer(getTimerID());
                    sendResultToTCK();
                }
                else {
                    if (getNumTimerEvents() == 2) {
                        setResultPassed("finite non-periodic timer TimerEvent tests passed");
                        sendResultToTCK();
                    }
                }
            }
            else if (getTestName().equals("timerEvent-periodic-infinite")) {

                boolean failed = false;

                // if this is the first event, startTime should equal event.getScheduledTime()
                if (getNumTimerEvents() == 1) {
                    if (getStartTime() != event.getScheduledTime()) {
                        setResultFailed(1240, "infinite periodic timer: timerEvent.getScheduledTime() did not match setTimer() startTime");
                        failed = true;
                    }
                } // subsequent events should have event.getScheduledTime() == startTime + period
                else if (getNumTimerEvents() == 2) {
                    if (getStartTime()+getPeriod() != event.getScheduledTime()) {
                        setResultFailed(1241, "infinite periodic timer: timerEvent.getScheduledTime() did not match setTimer() startTime + period");
                        failed = true;
                    }
                }
                else if (event.getPeriod() != getPeriod()) {
                    setResultFailed(1243, "infinite periodic timer: timerEvent.getPeriod() != setTimer() period");
                    failed = true;
                }
                else if (event.getNumRepetitions() != 0) {
                    setResultFailed(1244, "infinite periodic timer: timerEvent.getNumRepetitions() != 0");
                    failed = true;
                }
                else if (event.getRemainingRepetitions() != Integer.MAX_VALUE) {
                    setResultFailed(1245, "infinite periodic timer: timerEvent.getRemainingRepetitions() != Integer.MAX_VALUE");
                    failed = true;
                }

                if (failed) {
                    timerFacility.cancelTimer(getTimerID());
                    sendResultToTCK();
                }
                else {
                    if (getNumTimerEvents() == 2) {
                        timerFacility.cancelTimer(getTimerID());
                        setResultPassed("infinite non-periodic timer TimerEvent tests passed");
                        sendResultToTCK();
                    }
                }
            }

        } catch(Exception e) {
            TCKSbbUtils.handleException(e);
        }
    }

    // return a static string so that this SBB entity gets selected every time
    public InitialEventSelector getStaticConvergenceName(InitialEventSelector ies) {
        ies.setCustomName("foobar");
        return ies;
    }

    public abstract void setTestName(String testName);
    public abstract String getTestName();

    public abstract void setStartTime(long startTime);
    public abstract long getStartTime();

    public abstract void setPeriod(long period);
    public abstract long getPeriod();

    public abstract void setNumRepetitions(int numRepetitions);
    public abstract int getNumRepetitions();

    public abstract void setTimerID(TimerID timerID);
    public abstract TimerID getTimerID();

    public abstract void setNumTimerEvents(int numTimerEvents);
    public abstract int getNumTimerEvents();

    private void sendResultToTCK() throws Exception {
        HashMap sbbData = new HashMap();
        sbbData.put("testname", getTestName());
        sbbData.put("result", result?"pass":"fail");
        sbbData.put("message", message);
        sbbData.put("id", new Integer(failedAssertionID));
        TCKSbbUtils.getResourceInterface().sendSbbMessage(sbbData);
    }

    private void setResultFailed(int assertionID, String msg) throws Exception {
        result = false;
        failedAssertionID = assertionID;
        message =  "Assertion " + assertionID + " failed: " + msg;
        TCKSbbUtils.createTrace(getSbbID(), Level.WARNING, message, null);
    }

    private void setResultPassed(String msg) throws Exception {
        result = true;
        message = "Success: " + msg;
        TCKSbbUtils.createTrace(getSbbID(), Level.INFO, message, null);
    }

    private TimerFacility timerFacility;

    private boolean result = false;
    private String message;
    private int failedAssertionID = -1;

}
