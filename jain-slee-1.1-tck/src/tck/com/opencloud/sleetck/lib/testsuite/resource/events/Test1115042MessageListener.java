/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.resource.events;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.HashMap;

import javax.slee.EventTypeID;
import javax.slee.resource.ActivityHandle;
import javax.slee.resource.FireableEventType;
import javax.slee.resource.SleeEndpoint;

import com.opencloud.logging.StdErrLog;
import com.opencloud.sleetck.lib.infra.SleeTCKComponentConstants;
import com.opencloud.sleetck.lib.profileutils.BaseMessageSender;
import com.opencloud.sleetck.lib.rautils.MessageHandler;
import com.opencloud.sleetck.lib.rautils.RMIObjectChannel;
import com.opencloud.sleetck.lib.rautils.TCKRAUtils;
import com.opencloud.sleetck.lib.resource.adaptor11.TCKActivityHandleImpl;
import com.opencloud.sleetck.lib.testsuite.resource.SimpleEvent;

public class Test1115042MessageListener extends UnicastRemoteObject implements MessageHandler {

    private static Test1115042MessageListener instance;
    public static final int CREATE_VALID_ACTIVITY = 1;
    public static final int CHECK_ASSERTION = 2;

    public static Test1115042MessageListener getInstance(Test1115042ResourceAdaptor ra) throws RemoteException {
        if (instance==null || instance.ra != ra)
            instance = new Test1115042MessageListener(ra);
        return instance;
    }

    private Test1115042MessageListener(Test1115042ResourceAdaptor ra) throws RemoteException {
        super();

        try {
            out = TCKRAUtils.lookupRMIObjectChannel();
            msgSender = new BaseMessageSender(out, new StdErrLog());
        }
        catch (Exception e) {
            ra.getLog().warning("Exception occurred when trying to acquire RMIObjectChannel: ",e);
        }

        this.ra = ra;
    }

    public boolean handleMessage(Object message) throws RemoteException {
        SleeEndpoint endpoint=null;


        int type = ((Integer)((HashMap)message).get("Type")).intValue();
        endpoint = ra.getResourceAdaptorContext().getSleeEndpoint();

        switch (type) {
        case CREATE_VALID_ACTIVITY:
            try {
                // start an activity while the SLEE is still in the RUNNING state and store a handle
                //for later use
                handle1 = new TCKActivityHandleImpl(System.currentTimeMillis(), 0);
                try {
                    endpoint.startActivity(handle1, handle1);
                    msgSender.sendSuccess(1115042, "Created activity.");
                } catch (Exception e) {
                    msgSender.sendError("Could not create activity.");
                    return true;
                }

                break;
            } catch (Exception e) {
                msgSender.sendError("Unexpected Exception occurred: "+e);
                return true;
            }

        case CHECK_ASSERTION:
            try {
                EventTypeID eventTypeID = new EventTypeID(SimpleEvent.SIMPLE_EVENT_NAME, SleeTCKComponentConstants.TCK_VENDOR, "1.1");
                FireableEventType eventType = ra.getResourceAdaptorContext().getEventLookupFacility().getFireableEventType(eventTypeID);
                ActivityHandle handle2 = new TCKActivityHandleImpl(System.currentTimeMillis(), 0);
                //1115042: If the SLEE is in the Stopping state, resource adaptor objects associated with
                //the resource adaptor entity cannot start new activities, ...
                try {
                    ra.getLog().info("Starting activity: " + handle2);
                    endpoint.startActivity(handle2, handle2);
                    msgSender.sendFailure(1115042, "Starting new activity succeeded though SLEE is in the STOPPING state.");
                    return true;
                } catch (Exception e) {
                    msgSender.sendLogMsg("Starting new activity caused exception as expected: "+e);
                }

                //1115042: ...but may submit events on existing activities
                try{
                    endpoint.fireEvent(handle1, eventType, new SimpleEvent(1), null, null);
                    msgSender.sendLogMsg("RA was allowed to submit events on an existing activity.");
                } catch (Exception e) {
                    msgSender.sendFailure(1115042, "RA object should have been allowed to submit an event on an existing activity though SLEE" +
                            " is in the STOPPING state.");
                    return true;
                }

                //1115042: ...and end activities.
                try {
                    endpoint.endActivity(handle1);
                    handle1=null;
                    msgSender.sendLogMsg("RA was allowed to end existing activity.");
                } catch (Exception e) {
                    msgSender.sendFailure(1115042, "RA object should have been allowed to end an" +
                            " existing activity though SLEE is in the STOPPING state.");
                    return true;
                }

                msgSender.sendSuccess(1115042, "Test passed successfully.");

                break;
            } catch (Exception e) {
                msgSender.sendError("Unexpected Exception occurred: "+e);
            } finally {
                if (handle1!=null && endpoint!=null) {
                    try {
                        endpoint.endActivity(handle1);
                    } catch (Exception e) {
                        ra.getLog().info("Exception occurred when trying to cleanup RA activity.");
                    }
                }
            }
        }



        return true;
    }

    private Test1115042ResourceAdaptor ra;
    private BaseMessageSender msgSender;
    private RMIObjectChannel out;
    private ActivityHandle handle1;

}

