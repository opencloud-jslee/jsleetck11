/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.management.NotificationSource;

import java.rmi.RemoteException;
import java.util.HashMap;

import javax.slee.ActivityContextInterface;
import javax.slee.CreateException;
import javax.slee.facilities.TraceLevel;
import javax.slee.facilities.Tracer;
import javax.slee.profile.Profile;
import javax.slee.profile.ProfileContext;
import javax.slee.profile.ProfileTable;
import javax.slee.profile.ProfileVerificationException;
import javax.slee.profile.UnrecognizedProfileTableNameException;
//import com.opencloud.logging.Logable;
import com.opencloud.logging.StdErrLog;
import com.opencloud.sleetck.lib.TCKTestResult;
import com.opencloud.sleetck.lib.rautils.RMIObjectChannel;
import com.opencloud.sleetck.lib.rautils.TCKRAUtils;
import com.opencloud.sleetck.lib.resource.events.TCKResourceEventX;
import com.opencloud.sleetck.lib.sbbutils.TCKSbbUtils;

/**
 * The profile spec's Profile abstract class.
 * The ProfileTableNotification class identifies a profile table as the source of a notification.
 */
public abstract class Test1114235Profile implements Test1114235CMP, Profile, Test1114235ProfileInterface  {

    public Test1114235Profile() {
        Tracer tracer = null;
        try {
            javax.slee.profile.ProfileContext profileContext = this.getProfileContext();
            tracer = profileContext.getTracer("Test1114235ProfileTracer");
            tracer.trace(TraceLevel.INFO, "INFO:Test1114235TraceMessage");
         }
        catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void registerResult(boolean result, int assertionID, String msg) {

    }

    public void setProfileContext(ProfileContext context) {
        context.getTracer("setProfileContext").info("setProfileContext called");

        //Sort of self-evident but note it down here formally however
        //1114235: The ProfileContext object implements the javax.slee.profile.ProfileContext interface.
        try {
            javax.slee.profile.ProfileContext testcontext = (javax.slee.profile.ProfileContext)context;
            registerResult(true, 1114235, "ProfileContext object implements javax.slee.profile.ProfileContext interface.");
        }
        catch (ClassCastException e) {
            registerResult(false, 1114235, "ProfileContext object does NOT implement javax.slee.profile.ProfileContext interface: "+e);
        }

        this.context = context;
    }

    public javax.slee.profile.ProfileContext getProfileContext() {
        context.getTracer("getProfileContext").info("setProfileContext called");

        return this.context;
    }




    public void unsetProfileContext() {
        context.getTracer("unsetProfileContext").info("unsetProfileContext called");

        try {
            context.getProfileName();
            registerResult(false, 1110315, "Calling getProfileName in unsetProfileContext should have resulted in an exception being thrown.");
        }
        catch (IllegalStateException e) {
            registerResult(true, 1110315, "Calling getProfileName in unsetProfileContext threw exception as expected.");
        }
        catch (Exception e) {
            registerResult(false, 1110315, "Wrong type of exception: "+e.getClass().getName()+" Expected: java.lang.IllegalStateException");
        }


        context = null;

    }

    public void profileInitialize() {
        context.getTracer("profileInitialize").info("profileInitialize called");

        try {
            context.getProfileName();
            registerResult(false, 1110315, "Calling getProfileName in profileInitialize should have resulted in an exception being thrown.");
        }
        catch (IllegalStateException e) {
            registerResult(true, 1110315, "Calling getProfileName in profileInitialize threw exception as expected.");
        }
        catch (Exception e) {
            registerResult(false, 1110315, "Wrong type of exception: "+e.getClass().getName()+" Expected: java.lang.IllegalStateException");
        }

    }

    public void profilePostCreate() throws CreateException {
        context.getTracer("profilePostCreate").info("profilePostCreate called");

        try {
            context.getProfileName();
            registerResult(true, 1110315, "Calling getProfileName in profilePostCreate succeeded as expected.");
        }
        catch (Exception e) {
            registerResult(false, 1110315, "Calling getProfileName in profilePostCreate should have succeeded but threw exception: "+e);
        }

    }

    public void profileActivate() {
        context.getTracer("profileActivate").info("profileActivate called");

        try {
            context.getProfileName();
            registerResult(false, 1110315, "Calling getProfileName in profileActivate should have resulted in an exception being thrown.");
        }
        catch (IllegalStateException e) {
            registerResult(true, 1110315, "Calling getProfileName in profileActivate threw exception as expected.");
        }
        catch (Exception e) {
            registerResult(false, 1110315, "Wrong type of exception: "+e.getClass().getName()+" Expected: java.lang.IllegalStateException");
        }

    }

    public void profilePassivate() {
        context.getTracer("profilePassivate").info("profilePassivate called");

        try {
            context.getProfileName();
            registerResult(true, 1110315, "Calling getProfileName in profilePassivate succeeded as expected.");
        }
        catch (Exception e) {
            registerResult(false, 1110315, "Calling getProfileName in profilePassivate should have succeeded but threw exception: "+e);
        }

    }

    public void profileLoad() {
        context.getTracer("profileLoad").info("profileLoad called");

        try {
            context.getProfileName();
            registerResult(true, 1110315, "Calling getProfileName in profileLoad succeeded as expected.");
        }
        catch (Exception e) {
            registerResult(false, 1110315, "Calling getProfileName in profileLoad should have succeeded but threw exception: "+e);
        }

    }

    public void profileStore() {
        context.getTracer("profileStore").info("profileStore called");

        try {
            context.getProfileName();
            registerResult(true, 1110315, "Calling getProfileName in profileStore succeeded as expected.");
        }
        catch (Exception e) {
            registerResult(false, 1110315, "Calling getProfileName in profileStore should have succeeded but threw exception: "+e);
        }

    }

    public void profileRemove() {
        context.getTracer("profileRemove").info("profileRemove called");

        try {
            context.getProfileName();
            registerResult(true, 1110315, "Calling getProfileName in profileRemove succeeded as expected.");
        }
        catch (Exception e) {
            registerResult(false, 1110315, "Calling getProfileName in profileRemove should have succeeded but threw exception: "+e);
        }

    }

    public void profileVerify() throws ProfileVerificationException {
        context.getTracer("profileVerify").info("profileVerify called");

        try {
            context.getProfileName();
            registerResult(true, 1110315, "Calling getProfileName in profileVerify succeeded as expected.");
        }
        catch (Exception e) {
            registerResult(false, 1110315, "Calling getProfileName in profileVerify should have succeeded but threw exception: "+e);
        }

    }

    public void business() {
        context.getTracer("business").fine("Business method called.");

        try {
            context.getProfileName();
            registerResult(true, 1110315, "Calling getProfileName in business method succeeded as expected.");
        }
        catch (Exception e) {
            registerResult(false, 1110315, "Calling getProfileName in business method should have succeeded but threw exception: "+e);
        }

    }

    public void manage() {
        context.getTracer("manage").fine("Management method called.");

        try {
            context.getProfileName();
            registerResult(true, 1110315, "Calling getProfileName in management method succeeded as expected.");
        }
        catch (Exception e) {
            registerResult(false, 1110315, "Calling getProfileName in management method should have succeeded but threw exception: "+e);
        }

    }

    public void invokeOnDefaultProfile() {
        context.getTracer("invokeOnDefaultProfile").fine("Management method invoked on Default Profile.");

        if (context.getProfileName()!=null)
            registerResult(false, 1110756, "getProfileName should return 'null' when called for the Default Profile.");
        else
            registerResult(true, 1110756, "getProfileName returned 'null' when called for the Default Profile as expected.");

    }


    private ProfileContext context;

    private RMIObjectChannel out;

}
