/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.sbb.abstractclass;

import com.opencloud.sleetck.lib.sbbutils.BaseTCKSbb;
import com.opencloud.sleetck.lib.sbbutils.TCKSbbUtils;
import com.opencloud.sleetck.lib.resource.events.TCKResourceEventX;
import com.opencloud.sleetck.lib.resource.sbbapi.TCKActivity;

import java.util.HashMap;

import javax.slee.*;
import javax.slee.facilities.*;

/**
 * Test sbbExceptionThrown() method from spec s6.11.1
 */
public abstract class SbbExceptionThrownRethrowSbb extends BaseTCKSbb {
    public void onTCKResourceEventX1(TCKResourceEventX event, ActivityContextInterface aci) {
        // throw runtime exception to cause sbbExceptionThrown to be invoked
        throw new SLEEException("");
    }

    public void onTCKResourceEventX2(TCKResourceEventX event, ActivityContextInterface aci) {
        try {
            // notify receipt of second event
            HashMap sbbData = new HashMap();
            sbbData.put("x2", Boolean.TRUE);
            TCKSbbUtils.getResourceInterface().sendSbbMessage(sbbData);
        }
        catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }
    }

    public void sbbExceptionThrown(Exception exception, Object event, ActivityContextInterface aci) {
        if (invoked) {
            try {
                // already been here!
                TCKSbbUtils.createTrace(getSbbID(), Level.WARNING, "Assertion 459 failed: sbbExceptionThrown was reinvoked", null);

                HashMap sbbData = new HashMap();
                sbbData.put("reinvoked", Boolean.TRUE);
                TCKSbbUtils.getResourceInterface().sendSbbMessage(sbbData);
            }
            catch (Exception e) {
                TCKSbbUtils.handleException(e);
            }
        }
        else {
            // set flag and rethrow exception
            invoked = true;
            throw new SLEEException("");
        }
    }

    public void sbbRolledBack(RolledBackContext context) {
        // do nothing, we expect this to happen
    }

    private boolean invoked = false;
}

