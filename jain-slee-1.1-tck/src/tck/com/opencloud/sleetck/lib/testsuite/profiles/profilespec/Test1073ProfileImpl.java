/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.profiles.profilespec;

import javax.slee.profile.ProfileManagement;

/**
 * Define a profile that the SLEE cannot deploy due to a method naming conflict
 */
public abstract class Test1073ProfileImpl implements Test1073ProfileCMP, ProfileManagement {
    public void editProfile() {
        // duplicate method -- the SLEE should not deploy this profile

    }
}
