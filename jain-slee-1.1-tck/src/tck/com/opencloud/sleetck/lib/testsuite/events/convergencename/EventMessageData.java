/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.events.convergencename;

/**
 * Encapsulates the data sent via the event message to the event handler
 * methods of the ConvergenceNameTestSbb.
 */
public class EventMessageData {

    public EventMessageData(String eventID, InitialEventSelectorParameters initialEventSelectorData) {
        this.eventID=eventID;
        this.initialEventSelectorData=initialEventSelectorData;
    }

    /**
     * Exports the Object into an Object or array of primitive types,
     * J2SE types or SLEE types.
     */
    public Object toExported() {
        Object exportedIESData = initialEventSelectorData == null ? null :
                                initialEventSelectorData.toExported();
        return new Object[] {eventID,exportedIESData};
    }

    /**
     * Returns an EventMessageData Object from the
     * given object in exported form
     */
    public static EventMessageData fromExported(Object exported) {
        Object[] objArray = (Object[])exported;
        String eventID = (String)objArray[0];
        Object exportdIESData = objArray[1];
        InitialEventSelectorParameters iesData = exportdIESData == null ? null :
            InitialEventSelectorParameters.fromExported(exportdIESData);
        return new EventMessageData(eventID,iesData);
    }

    // Accessor methods

    public String getEventID() {
        return eventID;
    }

    public InitialEventSelectorParameters getInitialEventSelectorData() {
        return initialEventSelectorData;
    }

    // Private state

    private String eventID;
    private InitialEventSelectorParameters initialEventSelectorData;

}