/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.transactions;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.HashMap;

import javax.slee.profile.ProfileTable;
import javax.slee.transaction.SleeTransaction;
import javax.slee.transaction.SleeTransactionManager;
import javax.transaction.RollbackException;

import com.opencloud.logging.StdErrLog;
import com.opencloud.sleetck.lib.profileutils.BaseMessageSender;
import com.opencloud.sleetck.lib.rautils.MessageHandler;
import com.opencloud.sleetck.lib.rautils.RMIObjectChannel;
import com.opencloud.sleetck.lib.rautils.TCKRAUtils;
import com.opencloud.util.Future;
import com.opencloud.util.Future.TimeoutException;

/**
 * Provides callback method for handling messages sent from the TCK test to the RA.
 * The actual test code is located in the 'handleMessage()' method.
 */
public class Test1109314MessageListener extends UnicastRemoteObject implements MessageHandler {

    public static final String PROFILE_TABLE_NAME= "Test1109314ProfileTable";
    public static final String PROFILE_NAME= "Test1109314Table";

    /**
     *
     */
    private class MyThread extends Thread {
        private SleeTransactionManager txnManager;
        private ProfileTable profileTable;

        public MyThread(SleeTransactionManager txnManager, ProfileTable profileTable) {
            this.txnManager = txnManager;
            this.profileTable = profileTable;
        }

        public void run() {
            SleeTransaction txn=null;

            //there should be no exceptions when acquiring a new TXN for the thread even
            //if the SLEE implementation is single-threaded as this
            //has to be possible in regards of a correct JTA implementation
            //if it locks a timeout in the calling thread will occur which will be handled as test success
            try {
                txn = txnManager.beginSleeTransaction();
            } catch (Exception e) {
                msgSender.sendException(e);
            }

            try {
                //a SLEE implementation with pessimistic locking could lock up or error:
                //here...
                Test1109314ProfileCMP profile = (Test1109314ProfileCMP)profileTable.find(PROFILE_NAME);

                //...or possibly here
                String value = profile.getStringValue();

                future.setValue(value);

                txn.commit();

            } catch (Exception e) {
                try {
                    if (txn!=null)
                        txn.rollback();
                } catch (Exception ex) {
                    ra.getLog().warning("Error occured when trying to rollback RA started TXN.", ex);
                    msgSender.sendLogMsg("Exception occurred when trying to safely rollback RA started TXN: "+ex.getMessage());
                }
                //don't report this exception as error to the TCK test as it might be
                //due to a SLEE implementation using pessimistic locking.
            }
        }

    }

    public Test1109314MessageListener(Test1109314ResourceAdaptor ra) throws RemoteException {
        super();
        try {
            out = TCKRAUtils.lookupRMIObjectChannel();
            msgSender = new BaseMessageSender(out, new StdErrLog());
        }
        catch (Exception e) {
            ra.getLog().warning("Exception occurred when trying to acquire RMIObjectChannel: ",e);
        }
        this.ra = ra;
    }

    public boolean handleMessage(Object message) throws RemoteException {

        HashMap map = (HashMap)message;
        blockTimeout = 2000;
        if (map.get("Timeout") != null)
            blockTimeout = ((Long)map.get("Timeout")).longValue();

        //run the tests, if both run through successfully and completely (=both return 'true')
        //send success msg back to TCK test
        if (testCommit() && testStatus() && testRollback())
            msgSender.sendSuccess(1109314, "Test completed successfully.");

        return true;
    }

    private boolean testCommit() {
        SleeTransactionManager txnManager = ra.getResourceAdaptorContext().getSleeTransactionManager();
        SleeTransaction txn=null;
        String value;

        try {
            ProfileTable profileTable = ra.getResourceAdaptorContext().getProfileTable(PROFILE_TABLE_NAME);
            MyThread thread;

            //start a new TXN as profileTable.find is mandatory transactional
            txn = txnManager.beginSleeTransaction();
            Test1109314ProfileCMP profile = (Test1109314ProfileCMP)profileTable.find(PROFILE_NAME);

            profile.setStringValue("newValue");
            future = new Future();
            thread = new MyThread(txnManager, profileTable);
            thread.start();

            //if the concurrent CMP get access attempt times out this is
            //probably due to a SLEE implementation which uses pessimistic locking
            //as this is OK, a timout has to be treated as 'Success'
            try {
                value = (String)future.getValue(blockTimeout);
            } catch (TimeoutException e) {
                msgSender.sendLogMsg("Request to concurrently access profile CMP field timed out, possibly due to a SLEE implementation with pessimistic locking? "+e);
                txn.rollback();
                return true;
            }

            //1109134: This also means changes to the transactional execution state of one transaction are not visible to other concurrently executing transactions.
            if ("newValue".equals(value)) {
                msgSender.sendFailure(1109134, "Change to profile CMP field should not have been visible from separate TXN");
                return false;
            }
            else {
                msgSender.sendLogMsg("Change to profile CMP field was not visible from separate TXN as expected.");
            }

            value = profile.getStringValue();
            if (!"newValue".equals(value)) {
                msgSender.sendFailure(1109134, "Change to profile CMP field should have been visible from current TXN");
                return false;
            }
            else {
                msgSender.sendLogMsg("Change to profile CMP field was visible from current TXN as expected.");
            }

            //Now commit the TXN
            txn.commit();

            //start new TXN as CMP accessors have to be run in a TXN context
            txn = txnManager.beginSleeTransaction();
            profile = (Test1109314ProfileCMP)profileTable.find(PROFILE_NAME);

            //1109314: Complete the transaction represented by this Transaction object
            value = profile.getStringValue();
            if (!"newValue".equals(value)) {
                msgSender.sendFailure(1109314, "Change to profile CMP field should have been visible after the TXN committed.");
                return false;
            }
            else {
                msgSender.sendLogMsg("Change to profile CMP field was visible after TXN commit as expected.");
            }

            //start the thread again and get the CMP value after the commit
            future = new Future();
            thread = new MyThread(txnManager, profileTable);
            thread.start();

            try {
                value = (String)future.getValue(blockTimeout);
            } catch (TimeoutException e) {
                msgSender.sendLogMsg("Request to concurrently access profile CMP field timed out, possibly due to a SLEE implementation with pessimistic locking? "+e);
                return true;
            }

            //1109132: Atomicity guarantees that when a transaction completes, all changes to the transactional execution state are applied (committed)...
            //Check that changes are visible from both the current and the separate thread as changes have now been committed
            if (!"newValue".equals(value)) {
                msgSender.sendFailure(1109132, "Change to profile CMP field should have been visible from separate TXN after the original TXN committed.");
                return false;
            }
            else {
                msgSender.sendLogMsg("Change to profile CMP field was visible from separate TXN after TXN commit as expected.");
            }

            //1109315: RollbackException is thrown to indicate that the transaction has been rolled back rather than committed.
            //1109336: Modify the transaction associated with the current thread such that the only possible outcome of the transaction is to roll back the transaction.
            txn.setRollbackOnly();
            try {
                txn.commit();
                msgSender.sendFailure(1109315, "commit() should have thrown a RollbackException as TXN was previously marked for rollback.");
                return false;
            } catch (RollbackException e) {
                msgSender.sendLogMsg("Exception was thrown as expected: "+e);
            } catch (Exception e) {
                msgSender.sendFailure(1109315,"Wrong type of exception thrown, expected javax.transaction.RollbackException.", e);
                return false;
            }
            txn = txnManager.beginSleeTransaction();
            txn.setRollbackOnly();
            try {
                txn.rollback();
                msgSender.sendLogMsg("Though marked via 'setRollbackOnly()' TXN still successfully rolled back as expected.");
            } catch (Exception e) {
                msgSender.sendFailure(1109336,"Though previously marked via 'setRollbackOnly()' when calling rollback() on the TXN this still should have succeeded. Instead an " +
                        "exception occured: ", e);
                return false;
            }


            //start fresh TXN and commit it with asyncCommit as the SLEE spec ensures
            //that when this method returns the association with the current thread must have been cleared
            //thus the probably faulty JTA javadoc as well as its probably intended meaning are fulfilled
            txn = txnManager.beginSleeTransaction();
            txn.asyncCommit(null);
            //1109319: java.lang.IllegalStateException is thrown if the current thread is not associated with a transaction.
            try {
                txn.commit();
                msgSender.sendFailure(1109319, "commit() should throw an IllegalStateException if current thread is not associated with a TXN.");
                return false;
            } catch (IllegalStateException e) {
                msgSender.sendLogMsg("As expected, commit() threw an IllegalStateException as current thread is not associated with a TXN."+e);
            } catch (Exception e) {
                msgSender.sendFailure(1109319, "Wrong type of Exception thrown, expected java.lang.IllegalStateException.", e);
                return false;
            }

        } catch (Exception e) {
            try {
                if (txn!=null)
                    txn.rollback();
            } catch (Exception ex) {
                ra.getLog().warning("Error occured when trying to rollback RA started TXN.", ex);
                msgSender.sendLogMsg("Exception occurred when trying to safely rollback RA started TXN: "+ex.getMessage());
            }
            msgSender.sendException(e);
            return false;
        }

        return true;
    }


    private boolean testStatus() {
        SleeTransactionManager txnManager = ra.getResourceAdaptorContext().getSleeTransactionManager();
        SleeTransaction txn=null;

        try {
            txn = txnManager.beginSleeTransaction();
            //1109326: Obtain the status of the transaction associated with the current thread.
            //This is probably another JTA JavaDoc bug. It should rather be: Return the current status of the TXN
            try {
                String status = StatusUtil.statusToString(txn.getStatus());
                if (StatusUtil.INVALID.equals(status)) {
                    msgSender.sendFailure(1109326, "Transaction.getStatus() returned a value different to the constants defined in the javax.transaction.Status interface.");
                    return false;
                }
                else {
                    msgSender.sendLogMsg("Transaction.getStatus() returned current status of TXN: "+status);
                }
            } catch (Exception e) {
                msgSender.sendFailure(1109326, "Exception thrown when trying to query current TXN status.", e);
                return false;
            }

            txn.commit();

        } catch (Exception e) {
            try {
                if (txn!=null)
                    txn.rollback();
            } catch (Exception ex) {
                ra.getLog().warning("Error occured when trying to rollback RA started TXN.", ex);
                msgSender.sendLogMsg("Exception occurred when trying to safely rollback RA started TXN: "+ex.getMessage());
            }

            msgSender.sendException(e);
            return false;
        }

        return true;
    }

    private boolean testRollback() {
        SleeTransactionManager txnManager = ra.getResourceAdaptorContext().getSleeTransactionManager();
        SleeTransaction txn=null;
        String value;

        try {
            ProfileTable profileTable = ra.getResourceAdaptorContext().getProfileTable(PROFILE_TABLE_NAME);
            MyThread thread;

            //start a new TXN as profileTable.find is mandatory transactional
            txn = txnManager.beginSleeTransaction();
            Test1109314ProfileCMP profile = (Test1109314ProfileCMP)profileTable.find(PROFILE_NAME);

            profile.setStringValue("newValue2");
            future = new Future();
            thread = new MyThread(txnManager, profileTable);
            thread.start();

            //if the concurrent CMP get access attempt times out this is
            //probably due to a SLEE implementation which uses pessimistic locking
            //as this is OK, a timout has to be treated as 'Success'
            try {
                value = (String)future.getValue(blockTimeout);
            } catch (TimeoutException e) {
                msgSender.sendLogMsg("Request to concurrently access profile CMP field timed out, possibly due to a SLEE implementation with pessimistic locking? "+e);
                return true;
            }

            //1109134: This also means changes to the transactional execution state of one transaction are not visible to other concurrently executing transactions.
            if ("newValue2".equals(value)) {
                msgSender.sendFailure(1109134, "Change to profile CMP field should not have been visible from separate TXN");
                return false;
            }
            else {
                msgSender.sendLogMsg("Change to profile CMP field was not visible from separate TXN as expected.");
            }

            value = profile.getStringValue();
            if (!"newValue2".equals(value)) {
                msgSender.sendFailure(1109134, "Change to profile CMP field should have been visible from current TXN");
                return false;
            }
            else {
                msgSender.sendLogMsg("Change to profile CMP field was visible from current TXN as expected.");
            }

            //Now rollback the TXN
            txn.rollback();

            //start new TXN as CMP accessors have to be run in a TXN context
            txn = txnManager.beginSleeTransaction();
            profile = (Test1109314ProfileCMP)profileTable.find(PROFILE_NAME);

            //1109332: Rollback the transaction represented by this Transaction object.
            //Check that changes to CMP field have been undone.
            value = profile.getStringValue();
            if ("newValue2".equals(value)) {
                msgSender.sendFailure(1109332, "Changes should be rolled back and thus not visible from original Thread.");
                return false;
            }
            else {
                msgSender.sendLogMsg("Changes were rolled back and are thus not visible to original Thread.");
            }

            //start the thread again and get the CMP value after the commit
            future = new Future();
            thread = new MyThread(txnManager, profileTable);
            thread.start();

            try {
                value = (String)future.getValue(blockTimeout);
            } catch (TimeoutException e) {
                msgSender.sendLogMsg("Request to concurrently access profile CMP field, timed out, probably due to a SLEE implementation with pessimistic locking!? "+e);
                return true;
            }

            //1109132: Atomicity guarantees that when a transaction completes, ... or the transactional execution state
            //is restored to its previous value (the transaction is rolled back)
            //Check that changes are not visible from both the current and the separate thread as changes have been rolled back
            if ("newValue2".equals(value)) {
                msgSender.sendFailure(1109132, "Changes should be rolled back and thus not visible from separate Thread.");
                return false;
            }
            else {
                msgSender.sendLogMsg("Changes were rolled back and are thus not visible to separate Thread.");
            }

            txn.commit();

        } catch (Exception e) {
            try {
                if (txn!=null)
                    txn.rollback();
            } catch (Exception ex) {
                ra.getLog().warning("Error occured when trying to rollback RA started TXN.", ex);
                msgSender.sendLogMsg("Exception occurred when trying to safely rollback RA started TXN: "+ex.getMessage());
            }
            msgSender.sendException(e);
            return false;
        }

        return true;
    }

    private BaseMessageSender msgSender;
    private RMIObjectChannel out;
    private Future future;
    private long blockTimeout;
    private Test1109314ResourceAdaptor ra;
}

