/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.profiles.profileinitialstate;

import com.opencloud.sleetck.lib.AbstractSleeTCKTest;
import com.opencloud.sleetck.lib.TCKTestResult;
import com.opencloud.sleetck.lib.infra.SleeTCKComponentConstants;
import com.opencloud.sleetck.lib.testsuite.profiles.ProfileTestConstants;
import com.opencloud.sleetck.lib.testutils.Assert;
import com.opencloud.sleetck.lib.testutils.ComponentIDLookup;
import com.opencloud.sleetck.lib.testutils.ProfileUtils;
import com.opencloud.sleetck.lib.testutils.jmx.ProfileProvisioningMBeanProxy;

import javax.slee.management.ManagementException;
import javax.slee.profile.ProfileSpecificationID;
import javax.slee.profile.UnrecognizedProfileTableNameException;

/**
 * Test assertion 1020; that a profile table cannot be created if it's initial state does not pass the
 * checks in profileVerify().
 */
public class CheckProfileInitialStateTest extends AbstractSleeTCKTest {
    private static final String TABLE_NAME = "tck.CheckProfileInitialStateTest.table";
    private static final String PROFILE_SPEC_NAME = "CheckProfileInitialState";

    public TCKTestResult run() throws Exception {
        // create the profile table
        ProfileProvisioningMBeanProxy profileProvisioning = profileUtils.getProfileProvisioningProxy();
        ProfileSpecificationID profileSpecID = new ComponentIDLookup(utils()).lookupProfileSpecificationID(
            PROFILE_SPEC_NAME,SleeTCKComponentConstants.TCK_VENDOR,"1.0");
        getLog().info("Creating profile table: "+TABLE_NAME + " spec=" + profileSpecID.toString());
        try {
            profileProvisioning.createProfileTable(profileSpecID, TABLE_NAME);
            isTableCreated = true;
            Assert.fail(1020, "It should not be possible to create a profile table when the profile's initial state " +
                              "does not pass the checks in profileVerify");
        } catch(ManagementException e) {
            getLog().info("Expected exception received when trying to create a profile with initial values that do not " +
                          "pass the checks in profileVerify()");
        }

        try {
            profileUtils.getProfileProvisioningProxy().getDefaultProfile(TABLE_NAME);
            Assert.fail(1020, "It should not be possible to create a profile table when the profile's initial state " +
                              "does not pass the checks in profileVerify");
        } catch (UnrecognizedProfileTableNameException e) {}

        return TCKTestResult.passed();
    }

    public void setUp() throws Exception {
        String duPath = utils().getTestParams().getProperty(ProfileTestConstants.PROFILE_SPEC_DU_PATH_PARAM);
        getLog().fine("Installing profile specification");
        utils().install(duPath);
        profileUtils = new ProfileUtils(utils());
    }

    public void tearDown() throws Exception {
        // delete profile table
        try {
            if(profileUtils != null && isTableCreated) {
                getLog().fine("Removing profile table");
                profileUtils.removeProfileTable(TABLE_NAME);
            }
        } catch(Exception e) {
            getLog().warning("Caught exception while trying to remove profile table:");
            getLog().warning(e);
        }
        profileUtils = null;
        isTableCreated = false;
        super.tearDown();
    }

    private ProfileUtils profileUtils;
    private boolean isTableCreated;
}























