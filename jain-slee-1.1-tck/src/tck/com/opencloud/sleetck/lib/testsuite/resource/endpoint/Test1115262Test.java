/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.resource.endpoint;

import java.util.HashMap;

import com.opencloud.sleetck.lib.TCKTestErrorException;
import com.opencloud.sleetck.lib.TCKTestResult;
import com.opencloud.sleetck.lib.testsuite.resource.BaseResourceTest;
import com.opencloud.sleetck.lib.testsuite.resource.RAMethods;

/**
 * Test to ensure that startActivityTransacted() is throwing
 * NullPointerException correctly.
 * <p>
 * Test assertion: 1115262
 */
public class Test1115262Test extends BaseResourceTest {

    public TCKTestResult run() throws Exception {
        HashMap results = sendMessage(RAMethods.startActivityTransacted, new Integer(1115262));
        if (results == null)
            throw new TCKTestErrorException("Test timed out while waiting for response from test component");

        // NullPointerException
        checkResult(results, "result1", 1115262, "startActivityTransacted(null, null) failed to throw a NullPointerException");
        checkResult(results, "result2", 1115262, "startActivityTransacted(handle, null) failed to throw a NullPointerException");
        checkResult(results, "result3", 1115262, "startActivityTransacted(null, object) failed to throw a NullPointerException");

        checkResult(results, "result4", 1115262, "startActivityTransacted(null, null, flags) failed to throw a NullPointerException");
        checkResult(results, "result5", 1115262, "startActivityTransacted(handle, null, flags) failed to throw a NullPointerException");
        checkResult(results, "result6", 1115262, "startActivityTransacted(null, object, flags) failed to throw a NullPointerException");

        return TCKTestResult.passed();
    }

}
