/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.facilities.alarmfacility;

import java.util.HashMap;

import javax.naming.InitialContext;
import javax.slee.ActivityContextInterface;
import javax.slee.facilities.AlarmFacility;
import javax.slee.facilities.AlarmLevel;
import javax.slee.facilities.Tracer;

import com.opencloud.sleetck.lib.resource.events.TCKResourceEventX;
import com.opencloud.sleetck.lib.sbbutils.BaseTCKSbb;
import com.opencloud.sleetck.lib.sbbutils.TCKSbbUtils;

/*  
 *  AssertionID(1113498): Test If the alarm is cleared, the 
 *  SLEE’s AlarmMBean object emits an alarm notification for 
 *  the alarm with the alarm level set to AlarmLevel.CLEAR.
 *    
 *  AssertionID(1113500): Test The alarmID argument. This 
 *  argument specifies the alarm identifier of the alarm being 
 *  cleared.
 *  
 *  AssertionID(1113501): Test The clearAlarm method throws 
 *  a java.lang.NullPointerException if the alarmID argument is 
 *  null.
 *  
 */

public abstract class Test1113498Sbb extends BaseTCKSbb {

    public static final String ALARM_MESSAGE = "Test1113498AlarmMessage";

    public static final String ALARM_INSTANCEID = "Test1113498AlarmInstanceID";

    public static final String ALARM_TYPE = "javax.slee.management.Alarm";

    public static final AlarmLevel ALARM_LEVEL = AlarmLevel.MAJOR;

    // Initial event
    public void onTCKResourceEventX1(TCKResourceEventX event, ActivityContextInterface aci) {

        try {
            tracer = this.getSbbContext().getTracer("com.test");
            tracer.info("Received " + event + " message", null);

            doTest1113501Test();

        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }
    }

    private void doTest1113501Test() throws Exception {
        tracer.info("Testing Assertion Number 1113501...");

        AlarmFacility facility = getAlarmFacility();

        boolean passed = false;
        // 1113501
        try {
            facility.clearAlarm(null);
        } catch (java.lang.NullPointerException e) {
            tracer.info("got expected NullPointerException when ALARM_ID is null", null);
            passed = true;
        }

        if (!passed) {
            sendResultToTCK("Test1113501Test", false, 1113501, "AlarmFacility.clearAlarm(null) "
                    + "should have thrown java.lang.NullPointerException.");
            return;
        }

        tracer.info("Testing Assertion Number 1113498...");

        // 1113498
        try {
            // raise an alarm
            setFirstAlarm(facility.raiseAlarm(ALARM_TYPE, ALARM_INSTANCEID, ALARM_LEVEL, ALARM_MESSAGE));

            // clear the alarm has been raised
            if (!facility.clearAlarm(getFirstAlarm())) {
                sendResultToTCK("Test1113501Test", false, 1113498, "The alarm which has been raised cannot be cleared!");
                return;
            }

        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }

    }

    private void sendResultToTCK(String testName, boolean result, int failedAssertionID, String message)
            throws Exception {
        HashMap sbbData = new HashMap();
        sbbData.put("testname", testName);
        sbbData.put("result", result ? "pass" : "fail");
        sbbData.put("message", message);
        sbbData.put("id", new Integer(failedAssertionID));
        TCKSbbUtils.getResourceInterface().sendSbbMessage(sbbData);
    }

    private AlarmFacility getAlarmFacility() throws Exception {
        AlarmFacility facility = null;
        String JNDI_ALARMFACILITY_NAME = AlarmFacility.JNDI_NAME;
        try {
            facility = (AlarmFacility) new InitialContext().lookup(JNDI_ALARMFACILITY_NAME);
        } catch (Exception e) {
            tracer.warning("got unexpected Exception: " + e, null);
        }
        return facility;
    }

    public abstract void setFirstAlarm(String alarmID);

    public abstract String getFirstAlarm();

    private Tracer tracer;
}
