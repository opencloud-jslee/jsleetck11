/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.events.concurrency;

import com.opencloud.sleetck.lib.resource.events.TCKResourceEventY;
import com.opencloud.sleetck.lib.resource.events.TCKResourceEvent;
import com.opencloud.sleetck.lib.resource.events.TCKResourceEventX;

import javax.slee.ActivityContextInterface;
import javax.slee.RolledBackContext;

public abstract class SbbActivityConcurrencyTestSbb2 extends BaseSbbActivityConcurrencyTestSbb {

    public void onTCKResourceEventY2(TCKResourceEventY event, ActivityContextInterface aci) {
        callTest(SbbActivityConcurrencyTestConstants.SBB2_RECEIVED_Y2,"TCKResourceEventY2");
    }

    public void sbbRolledBack(RolledBackContext context) {
        Object event = context.getEvent();
        if(event instanceof TCKResourceEvent && TCKResourceEventY.Y2.equals(((TCKResourceEvent)event).getEventTypeName())) {
            callTest(SbbActivityConcurrencyTestConstants.SBB2_RECEIVED_Y2_ROLL_BACK,"sbbRolledBack() for TCKResourceEventY2");
        } else super.sbbRolledBack(context);
    }

}