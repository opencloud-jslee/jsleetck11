/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.profiles.profileinitialstate;

import javax.slee.profile.ProfileManagement;
import javax.slee.profile.ProfileVerificationException;

/**
 * This profile management class will initialize the profile with a value that fails the check in profileVerify
 */
public abstract class CheckProfileInitialStateImpl implements ProfileManagement, CheckProfileInitialStateCMP {

    private static final String INVALID_VALUE = "Test1020Invalid";

    public void profileInitialize() {
        setValue(INVALID_VALUE);
    }

    public void profileLoad() {
    }

    public void profileStore() {
    }

    public void profileVerify() throws ProfileVerificationException {
        if( getValue().equals(INVALID_VALUE) ) {
            throw new ProfileVerificationException("Field value is invalid: " + getValue());
        }
    }

}
