/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.facilities.activitycontextnamingfacility;

import com.opencloud.sleetck.lib.AbstractSleeTCKTest;
import com.opencloud.sleetck.lib.TCKTestResult;
import com.opencloud.sleetck.lib.testutils.QueuingResourceListener;
import com.opencloud.sleetck.lib.testutils.Assert;
import com.opencloud.sleetck.lib.resource.TCKActivityID;
import com.opencloud.sleetck.lib.resource.events.TCKResourceEventX;

/**
 * Test assertion 1350 : that an activity will not be reclaimed if a reference to it is held by the activity context
 * naming facility.  The test sends two messages to an SBB with a delay in between.  The first event handler creates
 * a null activity (doesn't attach to it) and binds it in the activity context naming facility.  The second event
 * handler verifies that the activity is still bound in the naming facility and that it is not ending.
 */
public class Test1350Test extends AbstractSleeTCKTest {
    public TCKTestResult run() throws Exception {
        setResourceListener(listener);
        TCKActivityID activity = utils().getResourceInterface().createActivity("Test1187Activity");

        utils().getResourceInterface().fireEvent(TCKResourceEventX.X1, null, activity, null);
        synchronized (this) {
            this.wait(testDelay);
        }

        utils().getResourceInterface().fireEvent(TCKResourceEventX.X2, null, activity, null);

        Boolean result1, result2;

        // First response = true if the activity context could be looked up or false if the naming facility returned null
        // (should be TRUE)
        result1 = (Boolean) listener.nextMessage().getMessage();
        Assert.assertTrue(1350, "An activity context bound in the ActivityContextNaming facility could not be retrieved",
                          result1.booleanValue());

        // Second response = return value of ActivityContextInterface.isEnding() (should be FALSE)
        result2 = (Boolean) listener.nextMessage().getMessage();
        Assert.assertTrue(1350, "An activity context bound in the ActivityContextNaming facility was found to be in the " +
                                "ending state (isEnding() has not been invoked)",
                          !result2.booleanValue());

        return TCKTestResult.passed();
    }

    public void setUp() throws Exception {
        testDelay = Integer.parseInt(utils().getTestParams().getProperty("waitPeriodMS"));
        listener = new QueuingResourceListener(utils());
        super.setUp();
    }

    private long testDelay;
    private QueuingResourceListener listener;
}
