/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.events.convergencename.iesSetsAddress;

import com.opencloud.sleetck.lib.testsuite.events.convergencename.AbstractConvergenceNameTest;
import com.opencloud.sleetck.lib.testsuite.events.convergencename.InitialEventSelectorParameters;
import com.opencloud.sleetck.lib.TCKTestResult;
import com.opencloud.sleetck.lib.resource.events.TCKResourceEventX;
import com.opencloud.sleetck.lib.resource.TCKActivityID;

import javax.slee.Address;
import javax.slee.AddressPlan;

/**
 * Test assertion 1924: That the SBB's initial event selector method may set the default address to null
 */
public class IesSetsNullAddressTest extends AbstractConvergenceNameTest {
    private static final Address ADDRESS_1 = new Address(AddressPlan.IP, "1.0.0.1");

    public TCKTestResult run() throws Exception {
        TCKActivityID activityID = utils().getResourceInterface().createActivity("Test1924Activity");
        InitialEventSelectorParameters iesParams;

        // Test preparation: create an SBB ("1") with convergence variable activitycontext:ADDRESS_1
        iesParams = new InitialEventSelectorParameters(true, true, false, false, false, null, false, false, false, null);
        sendEventAndWait(TCKResourceEventX.X1, "1", activityID, ADDRESS_1, "1", iesParams);

        // Assertion 1924: the IES method sets the address to NULL.  this should produce a different convergence variable
        // causing a new SBB ("2") to be created
        iesParams = new InitialEventSelectorParameters(true, true, false, false, false, null, false, false, true, null);
        sendEventAndWait(TCKResourceEventX.X1, "2", activityID, ADDRESS_1, "2", iesParams);

        return TCKTestResult.passed();
    }
}
