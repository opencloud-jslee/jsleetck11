/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.facilities.tracefacility.tracer;

import java.util.HashMap;

import javax.slee.ActivityContextInterface;
import javax.slee.facilities.TraceLevel;
import javax.slee.facilities.Tracer;

import com.opencloud.sleetck.lib.resource.events.TCKResourceEventX;
import com.opencloud.sleetck.lib.sbbutils.BaseTCKSbb;
import com.opencloud.sleetck.lib.sbbutils.TCKSbbUtils;

/*
 * AssertionID (1113123): Test this getTracerName() method returns the 
 * tracer name of the tracer represented by this Tracer object.
 * 
 * AssertionID (1113124): Test this method returns the name of the 
 * immediate parent tracer of the tracer represented by this Tracer object.
 * 
 * AssertionID (1113125): Test If this Tracer object represents the root 
 * tracer, this method returns null.
 * 
 * AssertionID (1113126): Test The getTraceLevel method returns the current 
 * trace level of the tracer and notification source represented by this Tracer.
 */
public abstract class Test1113123Sbb extends BaseTCKSbb {

    // Initial event
    public void onTCKResourceEventX1(TCKResourceEventX event, ActivityContextInterface aci) {

        try {
            sbbTracer = getSbbContext().getTracer("com.test");
            sbbTracer.info("Received " + event + " message", null);

            doTest1113123Test();
        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }
    }

    private void sendResultToTCK(String testName, boolean result, int failedAssertionID,  String message) throws Exception {
        HashMap sbbData = new HashMap();
        sbbData.put("testname", testName);
        sbbData.put("result", result ? "pass" : "fail");
        sbbData.put("message", message);
        sbbData.put("id", new Integer(failedAssertionID));
        TCKSbbUtils.getResourceInterface().sendSbbMessage(sbbData);
    }

    private void doTest1113123Test() throws Exception {
        sbbTracer.info("Testing Assertion Number 1113123...");
        String tracerName = "com.test";
        Tracer tracer = null;

        //1113123
        try {
            tracer = getSbbContext().getTracer("");
            tracerName = tracer.getTracerName();
            if (tracerName != "") {
                sendResultToTCK("Test1113123Test", false, 1113123, "This getTracerName method didn't return the tracer name of the tracer"
                        + "represented by this Tracer object.");
                return;
            }
            sbbTracer.info("got expected TracerName: " + tracerName, null);
        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }

        sbbTracer.info("Testing Assertion Number 1113124...");
        //1113124
        String newTracerName = "com.test";
        try {
            tracer = getSbbContext().getTracer(newTracerName);
            tracerName = tracer.getParentTracerName();
            if (tracerName == null || !tracerName.equals("com")) {
                sendResultToTCK("Test1113123Test", false, 1113124, "This method didn't return the name of the immediate parent "
                        + "tracer of the tracer represented by this Tracer object." + tracerName);
                return;
            }
            sbbTracer.info("got expected TracerName: " + tracerName, null);
        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }

        sbbTracer.info("Testing Assertion Number 1113125...");

        //1113125
        Tracer rootTracer = null;
        try {
            //the root tracer
            rootTracer = getSbbContext().getTracer("");
            tracerName = rootTracer.getParentTracerName();
            if (tracerName != null) {
                sendResultToTCK("Test1113123Test", false, 1113125, "This getParentTracerName method didn't return null when this Tracer "
                        + "object represented the root tracer.");
                return;
            }
            sbbTracer.info("got expected null TracerName: " + tracerName, null);
        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }

        sbbTracer.info("Testing Assertion Number 1113126...");
        //1113126
        TraceLevel traceLevel = TraceLevel.FINEST;
        try {
            //The default tracelevel for the root tracer is INFO
            tracer = getSbbContext().getTracer("");
            traceLevel = tracer.getTraceLevel();
            if (traceLevel != TraceLevel.INFO) {
                sendResultToTCK("Test1113123Test", false, 1113126, "This getTraceLevel method didn't return the current trace level (INFO)of "
                        + "the tracer, but it returned " + traceLevel.toString());
                return;
            }
            sbbTracer.info("got expected trace level of the tracer: " + traceLevel, null);
        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }

        sendResultToTCK("Test1113123Test", true, 1113123, "This Tracer interface tests passed!");
    }


    private Tracer sbbTracer;

}
