/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.javax.slee.management.ProfileProvisioningMBean;

import javax.slee.ActivityEndEvent;
import javax.slee.ActivityContextInterface;
import javax.slee.SbbContext;
import javax.slee.Address;
import javax.slee.SbbLocalObject;
import javax.slee.facilities.Level;
import javax.slee.ChildRelation;
import javax.slee.profile.ProfileFacility;
import javax.slee.profile.ProfileID;

import javax.slee.nullactivity.NullActivityFactory;
import javax.slee.nullactivity.NullActivity;
import javax.slee.nullactivity.NullActivityContextInterfaceFactory;
import javax.slee.facilities.ActivityContextNamingFacility;

import javax.naming.InitialContext;

import com.opencloud.sleetck.lib.sbbutils.BaseTCKSbb;
import com.opencloud.sleetck.lib.sbbutils.TCKSbbUtils;
import com.opencloud.sleetck.lib.sbbutils.TCKSbbConstants;
import com.opencloud.sleetck.lib.resource.events.TCKResourceEventX;
import com.opencloud.sleetck.lib.resource.TCKActivityID;
import com.opencloud.sleetck.lib.resource.sbbapi.TCKActivity;
import com.opencloud.sleetck.lib.resource.sbbapi.TCKResourceSbbInterface;
import com.opencloud.sleetck.lib.resource.sbbapi.TCKActivityContextInterfaceFactory;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Collection;

public abstract class Test4551Sbb extends BaseTCKSbb {

    private static final String JNDI_PROFILEFACILITY_NAME = "java:comp/env/slee/facilities/profile";
    public static final String PROFILE_TABLE_NAME = "Test4551ProfileTable";
    public static final String PROFILE_NAME = "Test4551Profile";

    public void onTCKResourceEventX1(TCKResourceEventX ev, ActivityContextInterface aci) {
        try {
            HashMap map = new HashMap();
            map.put("Stage", new Integer(1));

            ProfileFacility facility = (ProfileFacility) new InitialContext().lookup(JNDI_PROFILEFACILITY_NAME);
            Collection profiles = facility.getProfiles(PROFILE_TABLE_NAME);
            Iterator iter = profiles.iterator();
            while (iter.hasNext()) {
                ProfileID profileID = (ProfileID) iter.next();
                if (profileID.getProfileName().equals(PROFILE_NAME)) {
                    map.put("Result", new Boolean(false));
                    map.put("Message", "Newly created-via-mgmt-iface profile was accessible to an SBB before it had been committed.");
                    setResult(map);
                    fireSendResultEvent(new SendResultEvent(), aci, null);
                    return;
                }
            }

            map.put("Result", new Boolean(true));
            setResult(map);
            fireSendResultEvent(new SendResultEvent(), aci, null);
        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }
    }

    public void onTCKResourceEventX2(TCKResourceEventX ev, ActivityContextInterface aci) {
        try {
            HashMap map = new HashMap();
            map.put("Stage", new Integer(2));

            ProfileFacility facility = (ProfileFacility) new InitialContext().lookup(JNDI_PROFILEFACILITY_NAME);
            Collection profiles = facility.getProfiles(PROFILE_TABLE_NAME);
            Iterator iter = profiles.iterator();
            while (iter.hasNext()) {
                ProfileID profileID = (ProfileID) iter.next();
                if (profileID.getProfileName().equals(PROFILE_NAME)) {
                    map.put("Result", new Boolean(true));
                    setResult(map);
                    fireSendResultEvent(new SendResultEvent(), aci, null);
                    return;
                }
            }

            map.put("Result", new Boolean(false));
            map.put("Message", "Newly created-via-mgmt-iface profile was inaccessible to an SBB after it had been committed.");
            setResult(map);
            fireSendResultEvent(new SendResultEvent(), aci, null);

        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }
    }


    public void onSendResultEvent(SendResultEvent event, ActivityContextInterface aci) {
        try {
            TCKSbbUtils.getResourceInterface().sendSbbMessage(getResult());
        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }
    }

    public abstract void setResult(HashMap map);
    public abstract HashMap getResult();

    public abstract void fireSendResultEvent(SendResultEvent event, ActivityContextInterface aci, Address address);

    public abstract Test4551ProfileCMP getProfileCMP(javax.slee.profile.ProfileID id) throws javax.slee.profile.UnrecognizedProfileTableNameException, javax.slee.profile.UnrecognizedProfileNameException;  

}
