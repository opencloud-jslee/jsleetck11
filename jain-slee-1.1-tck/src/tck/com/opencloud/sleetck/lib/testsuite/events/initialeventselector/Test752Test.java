/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.events.initialeventselector;

import com.opencloud.sleetck.lib.AbstractSleeTCKTest;
import com.opencloud.sleetck.lib.TCKTestResult;
import com.opencloud.sleetck.lib.OperationTimedOutException;
import com.opencloud.sleetck.lib.testutils.QueuingResourceListener;
import com.opencloud.sleetck.lib.resource.TCKActivityID;
import com.opencloud.sleetck.lib.resource.events.TCKResourceEventX;

/**
 * Test assertion 752: that the SLEE only invokes initial event selector methods on Sbb objects in the pooled state
 */
public class Test752Test extends AbstractSleeTCKTest {
    public TCKTestResult run() throws Exception {
        activityA = utils().getResourceInterface().createActivity("Test752_ActivityA");
        activityB = utils().getResourceInterface().createActivity("Test752_ActivityB");

        utils().getResourceInterface().fireEvent(TCKResourceEventX.X1, null, activityA, null);
        utils().getResourceInterface().fireEvent(TCKResourceEventX.X1, null, activityA, null);
        utils().getResourceInterface().fireEvent(TCKResourceEventX.X1, null, activityA, null);

        try {
            Object o = listener.nextMessage().getMessage();
            throw (Exception) o;
        } catch (OperationTimedOutException e) {
            // timeout is ok - means there wasn't any exception in the Q
        }

        return TCKTestResult.passed();
    }

    public void setUp() throws Exception {
        super.setUp();
        listener = new ResourceListener();
        setResourceListener(listener);
    }

    public class ResourceListener extends QueuingResourceListener {
        public ResourceListener() {
            super(utils());
        }
        public Object onSbbCall(Object args) throws Exception {
            utils().getResourceInterface().fireEvent(TCKResourceEventX.X2, null, activityB, null);
            return null;
        }
    }

    private ResourceListener listener;
    private TCKActivityID activityA;
    private TCKActivityID activityB;
}
