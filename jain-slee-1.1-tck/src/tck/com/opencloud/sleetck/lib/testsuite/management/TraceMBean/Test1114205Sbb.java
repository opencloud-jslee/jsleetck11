/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.management.TraceMBean;

import javax.slee.ActivityContextInterface;
import javax.slee.facilities.TraceLevel;
import javax.slee.facilities.Tracer;

import com.opencloud.sleetck.lib.resource.events.TCKResourceEventX;
import com.opencloud.sleetck.lib.sbbutils.BaseTCKSbb;
import com.opencloud.sleetck.lib.sbbutils.TCKSbbUtils;

/*
 * AssertionID (1114205): Test if the specified trace level is equal
 * or above the trace filter level set, the Trace Facility will accept
 * the trace message and propagate the trace message by emitting a
 * TraceNotification object from a TraceMBean object.
 *
 */
public abstract class Test1114205Sbb extends BaseTCKSbb {
    public static final String TRACE_MESSAGE_SEVERE = "SEVERE:Test1114205TraceMessage";
    public static final String TRACE_MESSAGE_WARNING = "WARNING:Test1114205TraceMessage";
    public static final String TRACE_MESSAGE_INFO = "INFO:Test1114205TraceMessage";
    public static final String TRACE_MESSAGE_CONFIG = "CONFIG:Test1114205TraceMessage";
    public static final String TRACE_MESSAGE_FINE = "FINE:Test1114205TraceMessage";
    public static final String TRACE_MESSAGE_FINER = "FINER:Test1114205TraceMessage";
    public static final String TRACE_MESSAGE_FINEST = "FINEST:Test1114205TraceMessage";

    // Initial event
    public void onTCKResourceEventX1(TCKResourceEventX event, ActivityContextInterface aci) {

        try {

            doTest1114205Test();

            } catch(Exception e) {
            TCKSbbUtils.handleException(e);
        }
    }

    private void doTest1114205Test() throws Exception {
        Tracer tracer = null;
        //1114205
        try {
            tracer = getSbbContext().getTracer("com.foo");
            if (tracer.isTraceable(TraceLevel.SEVERE))
                tracer.trace(TraceLevel.SEVERE, TRACE_MESSAGE_SEVERE);
            if (tracer.isTraceable(TraceLevel.WARNING))
                tracer.trace(TraceLevel.WARNING, TRACE_MESSAGE_WARNING);
            if (tracer.isTraceable(TraceLevel.INFO))
                tracer.trace(TraceLevel.INFO, TRACE_MESSAGE_INFO);
            if (tracer.isTraceable(TraceLevel.CONFIG))
                tracer.trace(TraceLevel.CONFIG, TRACE_MESSAGE_CONFIG);
            if (tracer.isTraceable(TraceLevel.FINE))
                tracer.trace(TraceLevel.FINE, TRACE_MESSAGE_FINE);
            if (tracer.isTraceable(TraceLevel.FINER))
                tracer.trace(TraceLevel.FINER, TRACE_MESSAGE_FINER);
            if (tracer.isTraceable(TraceLevel.FINEST))
                tracer.trace(TraceLevel.FINEST, TRACE_MESSAGE_FINEST);
        }catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }
    }

}

