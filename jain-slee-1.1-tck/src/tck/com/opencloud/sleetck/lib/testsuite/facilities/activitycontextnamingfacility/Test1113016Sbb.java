/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.facilities.activitycontextnamingfacility;

import java.util.HashMap;

import javax.naming.InitialContext;
import javax.slee.ActivityContextInterface;
import javax.slee.facilities.ActivityContextNamingFacility;
import javax.slee.facilities.Tracer;

import com.opencloud.sleetck.lib.resource.events.TCKResourceEventX;
import com.opencloud.sleetck.lib.sbbutils.BaseTCKSbb;
import com.opencloud.sleetck.lib.sbbutils.TCKSbbUtils;

/**
 * AssertionID (1113023): Test The JNDI_NAME constant. This constant specifies
 * the JNDI location where an ActivityContextNamingFacility object may be
 * located by an SBB component in its component environment.
 * 
 * AssertionID (1113062): Test The value of this constant is 
 * "java:comp/env/slee/facilities/activitycontextnaming".
 */
public abstract class Test1113016Sbb extends BaseTCKSbb {

    public void onTCKResourceEventX1(TCKResourceEventX event, ActivityContextInterface aci) {

        try {
            tracer = getSbbContext().getTracer("com.test");
            tracer.info("Sbb: Received a TCK event " + event);

            doJNDITest();
        } catch (Exception e) {
            TCKSbbUtils.handleException(e);
        }
    }

    private void sendResultToTCK(String testName, boolean result, int failedAssertionID,  String message) throws Exception {
        HashMap sbbData = new HashMap();
        sbbData.put("testname", testName);
        sbbData.put("result", result ? "pass" : "fail");
        sbbData.put("message", message);
        sbbData.put("id", new Integer(failedAssertionID));
        TCKSbbUtils.getResourceInterface().sendSbbMessage(sbbData);
    }

    private void doJNDITest() throws Exception {
        ActivityContextNamingFacility acnf = getACNF();
        // 1113016
        if (acnf == null) {
            sendResultToTCK("Test1113016Test", false, 1113016, "Could not find ActivityContextNamingFacility object in JNDI at "
                    + ActivityContextNamingFacility.JNDI_NAME);
            return;
        }
        tracer.info("got expected ActivityContextNamingFacility object in JNDI at "
                + ActivityContextNamingFacility.JNDI_NAME, null);

        // /1113062
        String JNDI_ACNF_NAME = ActivityContextNamingFacility.JNDI_NAME;
        if (JNDI_ACNF_NAME != "java:comp/env/slee/facilities/activitycontextnaming") {
            sendResultToTCK("Test1113016Test", false, 1113062,
                    "The value of this JNDI_NAME is not equal to java:comp/env/slee/facilities/activitycontextnaming");
            return;
        }
        tracer.info("got expected value of this ActivityContextNamingFacility.JNDI_NAME", null);

        sendResultToTCK("Test1113016Test", true, 1113016, "ActivityContextNamingFacility.JNDI_NAME tests passed");
    }

    private ActivityContextNamingFacility getACNF() throws Exception {
        ActivityContextNamingFacility acnf = null;
        String JNDI_ACNF_NAME = ActivityContextNamingFacility.JNDI_NAME;
        try {
            acnf = (ActivityContextNamingFacility) new InitialContext().lookup(JNDI_ACNF_NAME);
        } catch (Exception e) {
            tracer.warning("got unexpected Exception: " + e, null);
        }
        return acnf;
    }

    public abstract ActivityContextNamingFacilityTestACI asSbbActivityContextInterface(ActivityContextInterface aci);

    private Tracer tracer;
}