/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.facilities.alarmfacility;

import java.rmi.RemoteException;
import java.util.Map;

import com.opencloud.sleetck.lib.AbstractSleeTCKTest;
import com.opencloud.sleetck.lib.TCKTestFailureException;
import com.opencloud.sleetck.lib.resource.TCKActivityID;
import com.opencloud.sleetck.lib.resource.TCKSbbMessage;
import com.opencloud.sleetck.lib.resource.events.TCKResourceEventX;
import com.opencloud.sleetck.lib.resource.events.TCKResourceEventY;
import com.opencloud.sleetck.lib.resource.testapi.TCKResourceListener;
import com.opencloud.sleetck.lib.resource.testapi.TCKResourceTestInterface;
import com.opencloud.sleetck.lib.testutils.Assert;
import com.opencloud.sleetck.lib.testutils.BaseTCKResourceListener;
import com.opencloud.sleetck.lib.testutils.FutureResult;
import com.opencloud.sleetck.lib.testutils.jmx.AlarmMBeanProxy;

/*  
 * AssertionID(1113513): Test The return value of this clearAlarms() 
 * method is the number of alarms that were cleared.
 * 
 */
public class Test1113513Test extends AbstractSleeTCKTest {
    public static final String SERVICE1_DU_PATH_PARAM = "service1DUPath";

    public static final String SERVICE2_DU_PATH_PARAM = "service2DUPath";

    /**
     * Perform the actual test.
     */
    public void run(FutureResult result) throws Exception {
        this.result = result;

        String activityName = "Test1113513Test";
        TCKResourceTestInterface resource = utils().getResourceInterface();
        TCKActivityID activityID = resource.createActivity(activityName);

        // Fire the first event towards Sbb2 to raise an alarm in Sbb2
        getLog().info("Firing event: " + TCKResourceEventX.X1);
        resource.fireEvent(TCKResourceEventX.X1, testName, activityID, null);

        // Fire the second event towards Sbb1 to raise an alarm in Sbb1
        getLog().info("Firing event: " + TCKResourceEventY.Y1);
        resource.fireEvent(TCKResourceEventY.Y1, testName, activityID, null);

        // Make sure the above two events have been raised
        synchronized (this) {
            wait(5000);
        }

        // Fire the last event towards Sbb1 to check the alarm in Sbb1 had been
        // cleared,
        // and the alarm in Sbb2 still alive.
        getLog().info("Firing event: " + TCKResourceEventY.Y2);
        resource.fireEvent(TCKResourceEventY.Y2, testName, activityID, null);
    }

    /**
     * Do all the pre-run configuration of the test.
     */

    public void setUp() throws Exception {

        getLog().fine("Connecting to resource");
        resourceListener = new TCKResourceListenerImpl();
        setResourceListener(resourceListener);
        getLog().fine("Installing and activating service");

        setupService(SERVICE1_DU_PATH_PARAM, true);
        setupService(SERVICE2_DU_PATH_PARAM, true);

        alarmMBeanProxy = utils().getAlarmMBeanProxy();

    }

    /**
     * Clean up after the test.
     */

    public void tearDown() throws Exception {

        String[] listActiveAlarms = alarmMBeanProxy.getAlarms();
        if (alarmMBeanProxy.getAlarms().length > 0)
            for (int i = 0; i < listActiveAlarms.length; i++)
                alarmMBeanProxy.clearAlarm(listActiveAlarms[i]);

        // cleanup
        super.tearDown();
    }

    private class TCKResourceListenerImpl extends BaseTCKResourceListener {
        // TCKResourceListener methods

        public synchronized void onSbbMessage(TCKSbbMessage message, TCKActivityID calledActivity)
                throws RemoteException {

            Map sbbData = (Map) message.getMessage();
            String sbbTestName = "Test1113513Test";
            String sbbTestResult = (String) sbbData.get("result");
            String sbbTestMessage = (String) sbbData.get("message");
            int assertionID = ((Integer) sbbData.get("id")).intValue();
            getLog().info(
                    "Received message from SBB: testname=" + sbbTestName + ", result=" + sbbTestResult + ", message="
                            + sbbTestMessage + ", id=" + assertionID);
            try {
                Assert.assertEquals(assertionID, "Test " + sbbTestName + " failed.", "pass", sbbTestResult);
                result.setPassed();
            } catch (TCKTestFailureException ex) {
                result.setFailed(assertionID, sbbTestMessage);
            }
        }

        public void onException(Exception exception) throws RemoteException {
            getLog().warning("Received Exception from SBB or resource:");
            getLog().warning(exception);
            result.setError(exception);
        }
    }

    private TCKResourceListener resourceListener;

    private AlarmMBeanProxy alarmMBeanProxy;

    private FutureResult result;
    
    private String testName = "Test1113513";
}
