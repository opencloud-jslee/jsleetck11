/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.runtime.security;

import java.security.AccessController;
import java.security.PrivilegedActionException;
import java.security.PrivilegedExceptionAction;
import java.util.PropertyPermission;

import javax.slee.CreateException;
import javax.slee.profile.ProfileContext;
import javax.slee.profile.ProfileVerificationException;

import com.opencloud.logging.Logable;
import com.opencloud.logging.StdErrLog;
import com.opencloud.sleetck.lib.TCKTestErrorException;
import com.opencloud.sleetck.lib.TCKTestFailureException;
import com.opencloud.sleetck.lib.profileutils.BaseMessageSender;
import com.opencloud.sleetck.lib.rautils.RMIObjectChannel;
import com.opencloud.sleetck.lib.rautils.TCKRAUtils;


public abstract class Test1112019ProfileAbstract extends BaseSecurityProfile implements Test1112019ProfileCMP, Test1112019ProfileManagement {
    public final static int TEST_ID = 1112019;

    public Test1112019ProfileAbstract() {
        Logable log = new StdErrLog();
        try {
            out = TCKRAUtils.lookupRMIObjectChannel();
            msgSender = new BaseMessageSender(out, log);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public void manage() {
        try {
            trace.info("Running profile.manage()");
            checkPermissions(TEST_ID);
        } catch (TCKTestFailureException e) {
            msgSender.sendFailure(e.getAssertionID(), e.getMessage());
        } catch (Exception e) {
            trace.warning("Got exception:", e);
            msgSender.sendError("General exception occured when checking permissions: "+ e);
        }
    }

    private void checkAllPermissions() {
        try {
            checkPermissions(TEST_ID);
        } catch (TCKTestFailureException e) {
            msgSender.sendFailure(e.getAssertionID(), e.getMessage());
            return;
        } catch (Exception e) {
            trace.warning("Got exception:", e);
            msgSender.sendError(e.getMessage());
            return;
        }

        try {
            AccessController.doPrivileged(new PrivilegedExceptionAction() {
                public Object run() throws TCKTestFailureException, TCKTestErrorException {
                    checkGrant(new PropertyPermission("tcktest.profile", "write"), TEST_ID);
                    return null;
                }
            } );
        } catch (PrivilegedActionException e) {
            Exception unknownException = e.getException();
            if (unknownException instanceof TCKTestFailureException)
                msgSender.sendFailure(((TCKTestFailureException)unknownException).getAssertionID(), unknownException.getMessage());
            else
                msgSender.sendError(unknownException.getMessage());
        }
    }

    public void setProfileContext(ProfileContext context) {
        super.setProfileContext(context);
        msgSender.sendLogMsg("Checking setProfileContext().");
        checkAllPermissions();
        msgSender.sendLogMsg("setProfileContext() finished.");
    }

    public void unsetProfileContext() {
        msgSender.sendLogMsg("Checking unsetProfileContext()...");
        checkAllPermissions();
        msgSender.sendLogMsg("unsetProfileContext() finished.");
    }

    public void profileInitialize() {
        msgSender.sendLogMsg("Checking profileInitialize()...");
        // Test 1112021 - runs with default SLEE permissions plus DU permissions
        try {
            checkPermissions(1112021);
            checkGrant(new PropertyPermission("tcktest.profile", "write"), TEST_ID);
        } catch (TCKTestFailureException e) {
            msgSender.sendFailure(e.getAssertionID(), e.getMessage());
        } catch (Exception e) {
            trace.warning("Got exception:", e);
            msgSender.sendError(e.getMessage());
        }
        msgSender.sendLogMsg("profileInitialize() finished.");
    }

    public void profilePostCreate() throws CreateException {
        msgSender.sendLogMsg("Checking profilePostCreate()...");
        checkAllPermissions();
        msgSender.sendLogMsg("profilePostCreate() finished.");
    }

    public void profileActivate() {
        msgSender.sendLogMsg("Checking profileActivate()...");
        checkAllPermissions();
        msgSender.sendLogMsg("profileActivate() finished.");
    }

    public void profilePassivate() {
        msgSender.sendLogMsg("Checking profilePassivate()...");
        checkAllPermissions();
        msgSender.sendLogMsg("profilePassivate() finished.");
    }

    public void profileLoad() {
        msgSender.sendLogMsg("Checking profileLoad()...");
        checkAllPermissions();
        msgSender.sendLogMsg("profileLoad() finished.");
    }

    public void profileStore() {
        msgSender.sendLogMsg("Checking profileStore()...");
        checkAllPermissions();
        msgSender.sendLogMsg("profileStore() finished.");
    }

    public void profileRemove() {
        msgSender.sendLogMsg("Checking profileRemove()...");
        checkAllPermissions();
        msgSender.sendLogMsg("profileRemove() finished.");
    }

    public void profileVerify() throws ProfileVerificationException {
        msgSender.sendLogMsg("Checking profileVerify()...");
        // Test 1112021 - runs with default SLEE permissions plus DU permissions
        try {
            checkPermissions(1112021);
            checkGrant(new PropertyPermission("tcktest.profile", "write"), TEST_ID);
        } catch (TCKTestFailureException e) {
            msgSender.sendFailure(e.getAssertionID(), e.getMessage());
        } catch (Exception e) {
            trace.warning("Got exception:", e);
            msgSender.sendError(e.getMessage());
        }
        msgSender.sendLogMsg("profileVerify() finished.");
    }

    private RMIObjectChannel out;
    private BaseMessageSender msgSender;
}
