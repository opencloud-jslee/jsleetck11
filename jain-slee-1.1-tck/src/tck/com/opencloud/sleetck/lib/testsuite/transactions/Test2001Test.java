/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.testsuite.transactions;

import com.opencloud.sleetck.lib.*;
import com.opencloud.sleetck.lib.testutils.jmx.MBeanFacade;
import com.opencloud.sleetck.lib.testutils.ExceptionsUtil;
import com.opencloud.sleetck.lib.testutils.BaseTCKResourceListener;
import com.opencloud.sleetck.lib.testutils.QueuingResourceListener;
import com.opencloud.sleetck.lib.resource.TCKActivityID;
import com.opencloud.sleetck.lib.resource.testapi.TCKResourceTestInterface;
import com.opencloud.sleetck.lib.resource.testapi.TCKResourceListener;
import com.opencloud.sleetck.lib.resource.events.TCKResourceEventX;
import com.opencloud.sleetck.lib.testutils.*;
import com.opencloud.sleetck.lib.testutils.jmx.*;
import com.opencloud.sleetck.lib.resource.TCKSbbMessage;
import java.util.Properties;
import java.rmi.RemoteException;
import javax.management.ObjectName;
import com.opencloud.logging.Logable;
import javax.slee.management.DeployableUnitID;

/**
 * Tests the successful commit case, by testing that changes to transactional
 * state are persisted after a transaction.
 */
public class Test2001Test extends AbstractSleeTCKTest {

    public TCKTestResult run() throws Exception {

        QueuingResourceListener listener = new QueuingResourceListener(utils());
        setResourceListener(listener);
        TCKResourceTestInterface resource = utils().getResourceInterface();

        getLog().fine("Creaing activity");
        TCKActivityID activityID = resource.createActivity(getClass().getName());
        getLog().info("Firing events");
        resource.fireEvent(TCKResourceEventX.X1,null,activityID,null);
        resource.fireEvent(TCKResourceEventX.X2,null,activityID,null);

        // Confirm that the X1 event was delivered
        Object message = listener.nextMessage().getMessage();
        if(!TCKResourceEventX.X1.equals(message)) throw new TCKTestErrorException(
            "Unexpected response from Sbb while waiting for event 1 ACK: "+message);

        // We receive an ACK if the CMP field change made during the X1 event handler
        // is visible in the X2 event handler.
        // If the change is not visible, a TCKTestFailureException will be thrown
        message = listener.nextMessage().getMessage();
        if(!TCKResourceEventX.X2.equals(message)) throw new TCKTestErrorException(
            "Unexpected response from Sbb while waiting for event 2 ACK: "+message);

        // Making it here indicates a test pass
        return TCKTestResult.passed();

    }

}
