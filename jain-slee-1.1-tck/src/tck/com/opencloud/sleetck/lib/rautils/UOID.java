/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.rautils;

import java.io.Serializable;
import java.rmi.server.UID;

/**
 * Unique Object ID
 * <p>
 * This class is used to uniquely identify a Java object which exists in a SLEE
 * JVM. It is required for robustly testing Resource Adaptor lifecycle callbacks
 * from the SLEE, and may eventually be used for other purposes.
 */
public class UOID implements Serializable {

    /**
     * Create and return a new OUID() object, specific to this host and JVM.
     */
    public static UOID createUOID() {
        UID uid = new UID();
        return new UOID(uid);
    }

    //
    // Public
    //

    /**
     * Returns the JVMUID associated with this UOID.
     */
    public JVMUID getJVMUID() {
        return jvmuid;
    }

    //
    // Object
    //

    public boolean equals(Object obj) {
        if (obj == null)
            return false;
        if (!(obj instanceof UOID))
            return false;

        UOID rauid = (UOID) obj;
        if (!uid.equals(rauid.uid))
            return false;
        return jvmuid.equals(rauid.jvmuid);
    }

    public int hashCode() {
        return jvmuid.hashCode() ^ uid.hashCode();
    }
    
    public String toString() {
        return uid + ":" + jvmuid;
    }

    //
    // Private
    //

    private UOID(UID uid) {
        this.jvmuid = JVMUID.getJVMUID();
        this.uid = uid;
    }

    private final JVMUID jvmuid;
    private final UID uid;
}
