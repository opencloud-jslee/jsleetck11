/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.rautils;

/**
 * Thrown by TCKRAUtils during RMIObjectChannel lookup.
 */
public class RMIObjectChannelException extends Exception {

    public RMIObjectChannelException() {
        super();
    }

    public RMIObjectChannelException(String message, Throwable cause) {
        super(message, cause);
    }

    public RMIObjectChannelException(String message) {
        super(message);
    }

    public RMIObjectChannelException(Throwable cause) {
        super(cause);
    }

}
