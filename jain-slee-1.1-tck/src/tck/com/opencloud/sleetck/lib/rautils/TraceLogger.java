/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.rautils;

import javax.slee.facilities.FacilityException;
import javax.slee.facilities.TraceLevel;
import javax.slee.facilities.Tracer;

/**
 * A logger class which will log to the trace facility, or to STDERR if no trace
 * facility has been set.
 */
public class TraceLogger {

    //
    // Interface based on the Tracer object
    //

    public void config(String message) throws NullPointerException, FacilityException {
        trace(TraceLevel.CONFIG, message);
    }

    public void config(String message, Throwable cause) throws NullPointerException, FacilityException {
        trace(TraceLevel.CONFIG, message, cause);
    }

    public void fine(String message) throws NullPointerException, FacilityException {
        trace(TraceLevel.FINE, message);
    }

    public void fine(String message, Throwable cause) throws NullPointerException, FacilityException {
        trace(TraceLevel.FINE, message, cause);
    }

    public void finer(String message) throws NullPointerException, FacilityException {
        trace(TraceLevel.FINER, message);
    }

    public void finer(String message, Throwable cause) throws NullPointerException, FacilityException {
        trace(TraceLevel.FINER, message, cause);
    }

    public void finest(String message) throws NullPointerException, FacilityException {
        trace(TraceLevel.FINEST, message);
    }

    public void finest(String message, Throwable cause) throws NullPointerException, FacilityException {
        trace(TraceLevel.FINEST, message, cause);
    }

    public void info(String message) throws NullPointerException, FacilityException {
        trace(TraceLevel.INFO, message);
    }

    public void info(String message, Throwable cause) throws NullPointerException, FacilityException {
        trace(TraceLevel.INFO, message, cause);
    }

    public void severe(String message) throws NullPointerException, FacilityException {
        trace(TraceLevel.SEVERE, message);
    }

    public void severe(String message, Throwable cause) throws NullPointerException, FacilityException {
        trace(TraceLevel.SEVERE, message, cause);
    }

    public void warning(String message) throws NullPointerException, FacilityException {
        trace(TraceLevel.WARNING, message);
    }

    public void warning(String message, Throwable cause) throws NullPointerException, FacilityException {
        trace(TraceLevel.WARNING, message, cause);
    }

    public boolean isFineEnabled() throws FacilityException {
        if (tracer != null)
            return tracer.isFineEnabled();
        return true;
    }

    public boolean isFinerEnabled() throws FacilityException {
        if (tracer != null)
            return tracer.isFinerEnabled();
        return true;
    }

    public boolean isFinestEnabled() throws FacilityException {
        if (tracer != null)
            return tracer.isFinestEnabled();
        return true;
    }

    public boolean isInfoEnabled() throws FacilityException {
        if (tracer != null)
            return tracer.isInfoEnabled();
        return true;
    }

    public boolean isSevereEnabled() throws FacilityException {
        if (tracer != null)
            return tracer.isSevereEnabled();
        return true;
    }

    public boolean isWarningEnabled() throws FacilityException {
        if (tracer != null)
            return tracer.isWarningEnabled();
        return true;

    }

    public void trace(TraceLevel level, String message) throws NullPointerException, IllegalArgumentException, FacilityException {
        if (tracer == null)
            System.err.println("TCKTraceLogger [" + level.toString() + "] " + message);
        else
            tracer.trace(level, message);
    }

    public void trace(TraceLevel level, String message, Throwable cause) throws NullPointerException, IllegalArgumentException, FacilityException {
        if (tracer == null) {
            System.err.println("TCKTraceLogger [" + level.toString() + "] " + message);
            cause.printStackTrace(System.err);
        } else
            tracer.trace(level, message, cause);

    }

    //
    // Setters
    //

    public void setTracer(Tracer tracer) {
        this.tracer = tracer;
    }

    private Tracer tracer;
}
