/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.rautils;

import java.rmi.Remote;
import java.rmi.RemoteException;

/**
 * The RMIObjectChannel is a mechanism for returning messages from multiple
 * object instances from inside a JVM (or multiple JVMs) to a test for
 * processing.
 */
public interface RMIObjectChannel extends Remote {

    public static String RMI_NAME = "RMIObjectChannel";

    /**
     * Returns the next Object written to this RMIObjectChannel. If timeout is
     * exceeded with no object being written to the channel, null will be
     * returned.
     */
    public Object readObject(long timeout) throws RemoteException;

    /**
     * Returns all objects which have been written the the RMIObjectChannel
     * message queue. The timeout indicates how long to wait for a new message
     * before considering the message queue 'stable'. The timeout value should
     * in general be low (a few seconds at most).
     */
    public Object[] readStableQueue(long timeout) throws RemoteException;

    /**
     * Sleep for timeout (in milliseconds), then return all currently pending
     * messages from the message queue (clearing the queue in the process).
     */
    public Object[] readQueue(long timeout) throws RemoteException;

    /**
     * Clears the current message queue.
     */
    public void clearQueue() throws RemoteException;

    /**
     * Returns true if the message queue is currently empty.
     */
    public boolean isEmpty() throws RemoteException;

    /**
     * Registers a message handler for objects written to the RMIObjectChannel.
     * If no message handler is set (or is set to null), objects will be added
     * to the message queue when writeObject() is called. If a message handler
     * is set, then objects will be passed to the MessageHandler.handleMessage()
     * method. If the handleMessage() method returns true, then the object will
     * not be written to the object queue. If handleMessage() returns false,
     * then objects will continue to be written to the Object queue.
     * <p>
     * This method is intended for tests which want to immediately know when a
     * message is received.
     */
    public void setMessageHandler(MessageHandler handler) throws RemoteException;

    /**
     * Writes an Object to this RMIObjectChannel message queue.
     */
    public void writeObject(Object obj) throws RemoteException;
}
