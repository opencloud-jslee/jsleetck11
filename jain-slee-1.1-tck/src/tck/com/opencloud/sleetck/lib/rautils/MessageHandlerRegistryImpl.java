/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.rautils;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.ArrayList;
import java.util.Iterator;

/**
 * This class is used by test components to remotely register interest in
 * receiving messages.
 */
public class MessageHandlerRegistryImpl extends UnicastRemoteObject implements MessageHandlerRegistry {

    public MessageHandlerRegistryImpl() throws RemoteException {
        super();
    }

    public void registerMessageHandler(MessageHandler handler) throws RemoteException {
        synchronized (messageHandlers) {
            messageHandlers.add(handler);
        }
    }

    public void unregisterMessageHandler(MessageHandler handler) throws RemoteException {
        synchronized (messageHandlers) {
            messageHandlers.remove(handler);
        }
    }

    public void sendMessage(Object message) throws RemoteException {
        synchronized (messageHandlers) {
            Iterator iter = messageHandlers.iterator();
            while (iter.hasNext()) {
                MessageHandler handler = (MessageHandler) iter.next();
                handler.handleMessage(message);
            }
        }
    }

    private final ArrayList messageHandlers = new ArrayList();
}
