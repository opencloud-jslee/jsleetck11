/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.rautils;

import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.security.AccessController;
import java.security.PrivilegedAction;

public class TCKRAUtils {
    /**
     * Lookup the RMIObjectChannel from the RMI Registry.
     * 
     * @throws RMIObjectChannelException
     */
    public static RMIObjectChannel lookupRMIObjectChannel() throws RMIObjectChannelException {
        Object result = AccessController.doPrivileged(new PrivilegedAction() {
            public Object run() {
                Registry registry;
                try {
                    registry = LocateRegistry.getRegistry(getRMIRegistryPort());
                    return (RMIObjectChannel) registry.lookup(RMIObjectChannel.RMI_NAME);
                } catch (Exception e) {
                    return e;
                }
            }
        });

        if (result instanceof RMIObjectChannel)
            return (RMIObjectChannel) result;
        else if (result instanceof Exception)
            throw new RMIObjectChannelException((Exception) result);
        else
            throw new RMIObjectChannelException("Unexpected result returned from RMIObjectChannel lookup: " + result);
    }

    /**
     * Lookup the MessageHandlerRegistry from the RMI Registry.
     * 
     * @throws RMIObjectChannelException
     */
    public static MessageHandlerRegistry lookupMessageHandlerRegistry() throws RMIObjectChannelException {
        Object result = AccessController.doPrivileged(new PrivilegedAction() {
            public Object run() {
                Registry registry;
                try {
                    registry = LocateRegistry.getRegistry(getRMIRegistryPort());
                    return (MessageHandlerRegistry) registry.lookup(MessageHandlerRegistry.RMI_NAME);
                } catch (Exception e) {
                    return e;
                }
            }
        });

        if (result instanceof MessageHandlerRegistry)
            return (MessageHandlerRegistry) result;
        else if (result instanceof Exception)
            throw new RMIObjectChannelException((Exception) result);
        else
            throw new RMIObjectChannelException("Unexpected result returned from RMIObjectChannel lookup: " + result);
    }

    public static int getRMIRegistryPort() {
        // TODO - Make this more sensible.
        return 4099;
    }
}
