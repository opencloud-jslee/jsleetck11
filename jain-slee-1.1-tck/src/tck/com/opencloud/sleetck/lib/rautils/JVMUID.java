/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.rautils;

import java.io.Serializable;
import java.net.InetAddress;
import java.net.UnknownHostException;

/**
 * Unique JVM identifier.
 */
public class JVMUID implements Serializable {

    /**
     * Returns a unique identifier for this JVM.
     */
    public static JVMUID getJVMUID() {
        return jvmuid;
    }

    //
    // Object
    //

    public boolean equals(Object obj) {
        if (obj == null)
            return false;
        if (!(obj instanceof JVMUID))
            return false;

        JVMUID jvmuid = (JVMUID) obj;

        if (unique != jvmuid.unique)
            return false;
        if (startTime != jvmuid.startTime)
            return false;
        if (hostAddress != null)
            return hostAddress.equals(jvmuid.hostAddress);
        return hostAddress == jvmuid.hostAddress;
    }

    public int hashCode() {
        return (int) startTime;
    }
    
    public String toString() {
        return unique + ":" + startTime + ":" + hostAddress;
    }
    
    //
    // Private
    //

    private JVMUID() {
        unique = (new Object()).hashCode();
        hostAddress = getHostAddress();
        startTime = System.currentTimeMillis();
    }

    private InetAddress getHostAddress() {
        InetAddress address;
        try {
            address = InetAddress.getLocalHost();
        } catch (UnknownHostException e) {
            address = null;
        }
        return address;
    }

    private final static JVMUID jvmuid = new JVMUID();

    private final int unique;
    private final InetAddress hostAddress;
    private final long startTime;
}
