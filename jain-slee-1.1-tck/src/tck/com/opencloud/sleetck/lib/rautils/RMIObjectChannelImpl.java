/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.rautils;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.ArrayList;

/**
 * Basic RMI based object communication channel.
 */
public class RMIObjectChannelImpl extends UnicastRemoteObject implements RMIObjectChannel {

    public RMIObjectChannelImpl() throws RemoteException {
        super();
    }

    //
    // RMIObjectChannel
    //

    public Object readObject(long timeout) throws RemoteException {
        long startTime = System.currentTimeMillis();
        synchronized (objectQueue) {
            while (objectQueue.isEmpty()) {
                long remaining = timeout - (System.currentTimeMillis() - startTime);
                if (remaining <= 0)
                    break;
                try {
                    objectQueue.wait(remaining);
                } catch (InterruptedException e) {
                }
            }

            if (objectQueue.isEmpty())
                return null;
            return objectQueue.remove(0);
        }
    }

    public Object[] readStableQueue(long timeout) throws RemoteException {
        ArrayList returnList = new ArrayList();
        // Uses readObject() to do queue reads so that the semantics of the
        // timeout are correct. This will return the queue once no message
        // has been written within a period of 'timeout'. This may however mean
        // the this method waits for longer than 'timeout'.
        synchronized (objectQueue) {
            while (true) {
                Object obj = readObject(timeout);
                if (obj == null)
                    return returnList.toArray(new Object[returnList.size()]);
                returnList.add(obj);
            }
        }
    }

    public Object[] readQueue(long timeout) throws RemoteException {
        try {
            Thread.sleep(timeout);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        synchronized (objectQueue) {
            Object[] returnList = objectQueue.toArray(new Object[objectQueue.size()]);
            objectQueue.clear();
            return returnList;
        }
    }

    public void clearQueue() throws RemoteException {
        synchronized (objectQueue) {
            objectQueue.clear();
        }
    }

    public void writeObject(Object obj) throws RemoteException {
        synchronized (objectQueue) {
            if (messageHandler != null) {
                // If we have a message handler set, pass the message to it.
                // Return if the message handler returned 'true'.
                boolean handled = messageHandler.handleMessage(obj);
                if (handled)
                    return;
            }
            objectQueue.add(obj);
            objectQueue.notify();
        }
    }

    public boolean isEmpty() throws RemoteException {
        return objectQueue.isEmpty();
    }

    public void setMessageHandler(MessageHandler handler) throws RemoteException {
        synchronized (objectQueue) {
            this.messageHandler = handler;
        }
    }

    //
    // Private
    //

    private ArrayList objectQueue = new ArrayList();
    private MessageHandler messageHandler;
}
