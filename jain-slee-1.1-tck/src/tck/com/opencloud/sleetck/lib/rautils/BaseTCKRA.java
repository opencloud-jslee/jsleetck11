/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.rautils;

import java.rmi.RemoteException;
import java.security.AccessController;
import java.security.PrivilegedAction;

import javax.slee.Address;
import javax.slee.facilities.Tracer;
import javax.slee.resource.ActivityHandle;
import javax.slee.resource.ConfigProperties;
import javax.slee.resource.FailureReason;
import javax.slee.resource.FireableEventType;
import javax.slee.resource.InvalidConfigurationException;
import javax.slee.resource.Marshaler;
import javax.slee.resource.ReceivableService;
import javax.slee.resource.ResourceAdaptor;
import javax.slee.resource.ResourceAdaptorContext;
import javax.slee.resource.ConfigProperties.Property;

import com.opencloud.sleetck.lib.resource.adaptor11.TCKActivityHandleImpl;
import com.opencloud.sleetck.lib.testsuite.resource.SimpleSbbInterface;

public abstract class BaseTCKRA implements ResourceAdaptor, SimpleSbbInterface {

    protected BaseTCKRA() {
        // Logging
        traceLogger = new TraceLogger();
        rauid = UOID.createUOID();

        // Lookup the RMIObjectChannel from the RMI Registry.
        try {
            out = TCKRAUtils.lookupRMIObjectChannel();
        } catch (Exception e) {
            out = null;
            getLog().severe("An error occured creating the RMIObjectChannel for RA Object: " + rauid, e);
        }
    }

    /**
     * Write an Object to the output stream. All messages are passed as objects.
     */
    protected void sendMessage(final Object obj) {
        AccessController.doPrivileged(new PrivilegedAction() {
            public Object run() {
                if (out == null) {
                    getLog().severe("Attempted to write to the RMIObjectChannel before it was initialized: rauid=" + rauid + ", message=" + obj);
                    return null;
                }
                try {
                    out.writeObject(obj);
                } catch (RemoteException e) {
                    getLog().severe("An error occured writing to the RMIObjectChannel for RA Object: " + rauid, e);
                }
                return null;
            }
        });
    }

    public Object getResourceAdaptorInterface(String className) {
        if (className.equals("com.opencloud.sleetck.lib.testsuite.resource.SimpleSbbInterface"))
            return this;
        return null;
    }

    public void executeTestLogic(Object arguments) throws Exception {
        // Noop
    }

    public Object getActivity(ActivityHandle handle) {
        if (handle instanceof TCKActivityHandleImpl)
            return (TCKActivityHandleImpl) handle;
        return null;
    }

    public ActivityHandle getActivityHandle(Object activity) {
        if (activity instanceof TCKActivityHandleImpl)
            return (ActivityHandle) activity;
        return null;
    }

    //
    // ResourceAdaptor
    //

    public void setResourceAdaptorContext(ResourceAdaptorContext context) {
        this.resourceAdaptorContext = context;
    }

    public void unsetResourceAdaptorContext() {
        this.resourceAdaptorContext = null;
    }

    public void activityEnded(ActivityHandle handle) {
    }

    public void activityUnreferenced(ActivityHandle handle) {
    }

    public void administrativeRemove(ActivityHandle handle) {
    }

    public void raConfigurationUpdate(ConfigProperties properties) {
    }

    public void raConfigure(ConfigProperties properties) {
    }

    public void raUnconfigure() {
    }

    public void raActive() {
    }

    public void raInactive() {
    }

    public void raStopping() {
    }

    public void raVerifyConfiguration(ConfigProperties properties) throws InvalidConfigurationException {
    }

    public void eventProcessingFailed(ActivityHandle handle, FireableEventType eventType, Object event, Address address, ReceivableService serviceInfo, int flags,
            FailureReason reason) {
    }

    public void eventProcessingSuccessful(ActivityHandle handle, FireableEventType eventType, Object event, Address address, ReceivableService serviceInfo, int flags) {
    }

    public void eventUnreferenced(ActivityHandle handle, FireableEventType eventType, Object event, Address address, ReceivableService serviceInfo, int flags) {
    }

    public Marshaler getMarshaler() {
        return null;
    }

    public void queryLiveness(ActivityHandle handle) {
    }

    public void serviceActive(ReceivableService serviceInfo) {
    }

    public void serviceInactive(ReceivableService serviceInfo) {
    }

    public void serviceStopping(ReceivableService serviceInfo) {
    }

    //
    // SBB Interface
    //

    public void sendSbbMessage(Object obj) {
        sendMessage(obj);
    }

    public void logTestString(String string) {
        getLog().info("Received log message from SBB: " + string);
    }

    public ResourceAdaptorContext getResourceAdaptorContext() {
        return resourceAdaptorContext;
    }

    // Utility

    /**
     * Require to remove potentially non-serializable vendor specific extensions from configuration properties.
     * Necessary if serializing the object from the SLEE -> TCK.
     */
    public ConfigProperties sanitizeConfigProperties(ConfigProperties configProperties) {
        ConfigProperties newConfigProperties = new ConfigProperties();
        Property[] properties = configProperties.getProperties();
        for (int i = 0; i < properties.length; i++) {
            if (properties[i].getName() != null && properties[i].getName().startsWith("slee-vendor:"))
                continue;
            newConfigProperties.addProperty(properties[i]);
        }
        return newConfigProperties;
    }

    //
    // Setters
    //

    protected void setTracer(Tracer tracer) {
        this.traceLogger.setTracer(tracer);
    }

    //
    // Getters
    //

    protected TraceLogger getLog() {
        return traceLogger;
    }

    protected UOID getRAUID() {
        return rauid;
    }

    //
    // Private
    //

    private ResourceAdaptorContext resourceAdaptorContext;
    private RMIObjectChannel out;
    private UOID rauid;
    private TraceLogger traceLogger;
}
