/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.rautils;

public class TCKRAConstants {
    public static int OBJECT_STREAM_PORT = 5055;
    
    public static String SIMPLE_RESOURCE_ADAPTOR_LOCATION = "slee/resources/tck/resource/simple";
    public static String SIMPLE_ACI_FACTORY_LOCATION = "slee/resources/tck/resource/simplefactory";    
}
