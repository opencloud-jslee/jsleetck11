/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.sbbutils.events2;

import java.rmi.RemoteException;
import java.util.HashMap;
import java.util.Iterator;

import com.opencloud.logging.Logable;
import com.opencloud.sleetck.lib.resource.TCKActivityID;
import com.opencloud.sleetck.lib.resource.TCKSbbMessage;
import com.opencloud.sleetck.lib.testutils.BaseTCKResourceListener;

public abstract class SbbMessageAdapter extends BaseTCKResourceListener {

    /**
     * Empty default implementation.
     * @param msg The message to log.
     */
    public void onSbbLogCall(String msg) { getLog().fine("Sbb wants to log: "+msg); }

    /**
     * Has to be implemented when subclassing a concrete implementation
     */
    public abstract Logable getLog();

    /**
     * Handler method for synchronous calls from Sbbs.
     * Forwards log requests to onSbbLogCall() all others to onHandleRequest()
     * @see com.opencloud.sleetck.lib.testutils.BaseTCKResourceListener#onSbbCall(java.lang.Object)
     * @param argument    The message load carried by the SendResultEvent, has to be a java.util.HashMap or subclass
     * @return Boolean(true) object if message was handled without unhandled exceptions.
     * @throws Exception if message load format is wrong or an exception occurs in one of the handler methods.
     */
    public Object onSbbCall(Object argument) throws Exception {
        getLog().info("Received synchronous call from Sbb");

        HashMap map = (HashMap) argument;
        Integer type = (Integer) map.get(SbbBaseMessageConstants.TYPE);
        switch (type.intValue()) {
        case SbbBaseMessageConstants.TYPE_LOG_MSG:
            String msg = (String) map.get(SbbBaseMessageConstants.MSG);
            onSbbLogCall(msg);
            break;
        default:
            onHandleRequest(type.intValue(), map);
        }

        return new Boolean(true);
    }

    /**
     * Empty default implementation.
     * @param id The tests assertion id.
     * @param msg A message string.
     */
    public void onSetPassed(int id, String msg) {}

    /**
     * Empty default implementation.
     * @param id The tests assertion id.
     * @param msg A message string.
     */
    public void onSetFailed(int id, String msg) {}

    /**
     * Empty default implementation.
     * @param type The SendResultEvent message type as extracted from the message load HashMap
     * @param msgLoad The event's message load.
     */
    public void onHandleRequest(int type, HashMap msgLoad) throws Exception {}

    /**
     * Handler method for asynchronous messages from Sbbs.
     * Forwards log requests to onSbbLogCall() all others to onHandleRequest()
     * @see com.opencloud.sleetck.lib.testutils.BaseTCKResourceListener#onSbbCall(java.lang.Object)
     * @param argument    The message load carried by the SendResultEvent, has to be a java.util.HashMap or subclass
     * @throws Exception if message load format is wrong or an exception occurs in one of the handler methods.
     */
    public synchronized void onSbbMessage(TCKSbbMessage message, TCKActivityID activity) throws RemoteException {
        getLog().info("Received message from SBB: " + message.getMessage());

        if (message.getMessage() instanceof HashMap) {
            //unpack the transferred message
            HashMap map = (HashMap) message.getMessage();
            if (!checkMsgClass(map)) {
                getLog().warning("Received message load was of type java.util.HashMap but did not carry the expected MsgClass field.");
                return;
            }

            try {
                Integer type = (Integer) map.get(SbbBaseMessageConstants.TYPE);
                switch (type.intValue()) {
                case SbbBaseMessageConstants.TYPE_SET_RESULT:
                    Integer id = (Integer) map.get(SbbBaseMessageConstants.ID);
                    Boolean passed = (Boolean) map.get(SbbBaseMessageConstants.RESULT);
                    String msgString = (String) map.get(SbbBaseMessageConstants.MSG);

                    //pass or fail the test
                    if (passed.booleanValue() == true)
                        onSetPassed(id.intValue(), msgString);
                    else
                        onSetFailed(id.intValue(), msgString);
                    break;
                default:
                    try {
                        onHandleRequest(type.intValue(), map);
                    } catch (Exception e) {
                        getLog().fine("Exception occured while trying handling SbbMessage: "
                                        + e.getMessage());
                    }
                }

            } catch (NullPointerException e) {
                getLog().warning("NullPointerException occured while trying to read fields of Sbb message. The msg contents were:");
                Iterator iter = map.keySet().iterator();
                while (iter.hasNext()) {
                    Object key = iter.next();
                    Object value = map.get(key);
                    getLog().warning("Key: "+key+" Value:"+value);
                }
                getLog().warning("Thrown exception: ");
                getLog().warning(e);
                return;
            }
        }
        else {
            getLog().warning("Received message load was not a java.util.HashMap as expected, found "+message.getMessage().getClass().getName()+" instead.");
        }
    }

    public void onSetException(Exception e) {

    }

    public void onException(Exception e) throws RemoteException {
        getLog().warning("Received exception from SBB.");
        getLog().warning(e);
        onSetException(e);
    }

    private boolean checkMsgClass(HashMap map) {
        Integer msgClass = (Integer)map.get(SbbBaseMessageConstants.MESSAGE_CLASS_KEY);
        if (msgClass==null || msgClass.intValue()!=SbbBaseMessageConstants.MESSAGE_CLASS)
            return false;

        return true;
    }

}
