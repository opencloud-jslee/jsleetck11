/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.sbbutils;

/**
 * Defines contants used by the TCK SBBs.
 */
public interface TCKSbbConstants {

    public static final String TCK_RESOURCE_ADAPTOR_LOCATION = "slee/resources/tck/resource";
    public static final String TCK_ACI_FACTORY_LOCATION = "slee/resources/tck/activitycontextinterfacefactory";

    public static final String TCK_MESSAGE_TYPE         = "sbb.tck.message";
    public static final String TCK_ALARM_TYPE           = "sbb.tck.message";

}