/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.sbbutils.events2;

import java.util.HashMap;

public class SbbBaseMessageComposer {

    private static HashMap getBaseConfig() {
        HashMap map = new HashMap();
        map.put(SbbBaseMessageConstants.MESSAGE_CLASS_KEY, new Integer(SbbBaseMessageConstants.MESSAGE_CLASS));
        return map;
    }


    public static HashMap getSetResultMsg(boolean result, int assertionID, String msg) {
        HashMap map = getBaseConfig();
        map.put(SbbBaseMessageConstants.TYPE, new Integer(SbbBaseMessageConstants.TYPE_SET_RESULT));
        map.put(SbbBaseMessageConstants.RESULT, new Boolean(result));
        map.put(SbbBaseMessageConstants.ID, new Integer(assertionID));
        if (msg!=null)
            map.put(SbbBaseMessageConstants.MSG, msg);

        return map;
    }

    public static HashMap getLogMsg(String msg) {
        HashMap map = getBaseConfig();
        map.put(SbbBaseMessageConstants.TYPE, new Integer(SbbBaseMessageConstants.TYPE_LOG_MSG));
        if (msg!=null)
            map.put(SbbBaseMessageConstants.MSG, msg);

        return map;
    }

    public static HashMap getCustomMessage(int type, String[] keys, Object[] values) {
        HashMap map = getBaseConfig();
        map.put(SbbBaseMessageConstants.TYPE, new Integer(type));

        if (keys==null || values==null || keys.length!=values.length)
            return map;

        for (int i=0; i<keys.length; i++)
            map.put(keys[i], values[i]);

        return map;
    }

}
