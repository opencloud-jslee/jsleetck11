/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.sbbutils.events2;

public interface SbbBaseMessageConstants {
    public static final int TYPE_SET_RESULT = 0;
    public static final int TYPE_LOG_MSG = 1;

    public static final String TYPE = "Type";
    public static final String RESULT = "Result";
    public static final String MSG = "Msg";
    public static final String ID = "ID";
    public static final String CAUSE = "Cause";

    public static final int MESSAGE_CLASS = 2;
    public static final String MESSAGE_CLASS_KEY = "MsgClass";
}
