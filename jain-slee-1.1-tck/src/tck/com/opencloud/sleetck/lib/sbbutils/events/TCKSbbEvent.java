/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.sbbutils.events;

/**
 * A generic event type for firing between TCK test SBBs.
 *
 * Test SBBs may fire TCKSbbEvents between themselves for communication,
 * or a single SBB instance may fire a TCKSbbEvent to itself as a means
 * to wait for a transaction to commit (receipt of an SBB fired event
 * indicates that the transaction in which it was fired completed successfully).
 */
public interface TCKSbbEvent {

    // Event type name
    public static final String EVENT_TYPE_NAME = "com.opencloud.sleetck.lib.sbbutils.events.TCKSbbEvent";

    /**
     * Returns the message passed via this event, which may be null.
     */
    public Object getMessage();

}
