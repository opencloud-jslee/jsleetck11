/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.sbbutils.events;

import java.io.Serializable;

/**
 * Implementation of TCKSbbEvent
 */
public class TCKSbbEventImpl implements TCKSbbEvent, Serializable {

    public TCKSbbEventImpl(Object message) {
        this.message = message;
        eventID = nextEventID();
        classLoadTime_Instance = classLoadTime_Static;
    }

    // -- Implementation of TCKSbbEvent -- //

    /**
     * Returns the message passed via this event, which may be null.
     */
    public Object getMessage() {
        return message;
    }

    // -- Implementation of java.lang.Object methods -- //

    public boolean equals(Object object) {
        if(object != null && object instanceof TCKSbbEventImpl) {
            TCKSbbEventImpl otherEvent = (TCKSbbEventImpl)object;
            return otherEvent.eventID == eventID &&
                   otherEvent.classLoadTime_Instance == classLoadTime_Instance &&
                   message == null ? otherEvent.message == null : message.equals(otherEvent);
        } else return false;
    }

    public int hashCode() {
        return (int)eventID;
    }

    public String toString() {
        return "TCKResourceEvent: message="+message+",eventID="+eventID+
                ",classLoadTime="+classLoadTime_Instance;
    }

    // -- Private state -- //

    private Object message;
    private long eventID;
    private long classLoadTime_Instance;

    // -- Static state and methods -- //

    private static long nextEventID = 1;
    private static long classLoadTime_Static = System.currentTimeMillis();

    private synchronized static long nextEventID() {
        return nextEventID++;
    }

}
