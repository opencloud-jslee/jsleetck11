/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.interview;

import java.util.Map;
import java.net.URL;
import java.net.MalformedURLException;
import java.io.File;

import com.sun.interview.Interview;
import com.sun.interview.Question;
import com.sun.interview.InetAddressQuestion;
import com.sun.interview.IntQuestion;
import com.sun.interview.StringQuestion;
import com.sun.interview.FinalQuestion;
import com.opencloud.sleetck.lib.EnvironmentKeys;

public class ConnectionsInterview extends Interview {

    public ConnectionsInterview(EnvironmentInterview parent) {
        super(parent,"connections");
        this.parent = parent;
        setFirstQuestion(qJmxAgentHostIP);
    }

    private String getLocalHost() throws java.net.UnknownHostException {
        return java.net.InetAddress.getLocalHost().getHostAddress();
    }

    // -- Questions -- //

    //-------------------------------------------------
    // What is the IP address of the JMX Agent?
    //
    // Suggested value: IP address of localhost
    private InetAddressQuestion qJmxAgentHostIP = new InetAddressQuestion(this,"jmxAgentHostIP") {

        {
        try {setValue(getLocalHost());} catch(Exception e) {e.printStackTrace();}}

        protected Question getNext() {
            return qJmxAgentPort;
        }
        protected void export(Map data) {
            data.put(parent.envEntry(EnvironmentKeys.JMX_AGENT_HOST_IP),value.getHostAddress());
        }
    };

    //-------------------------------------------------
    // Which RMI port is the JMX agent bound to?
    //
    // Suggested value: 4099
    private IntQuestion qJmxAgentPort = new IntQuestion(this,"jmxAgentPort") {

        {
        try {setValue(4099);} catch(Exception e) {e.printStackTrace();}}

        protected Question getNext() {
            return qJmxAgentDomain;
        }
        protected void export(Map data) {
            data.put(parent.envEntry(EnvironmentKeys.JMX_AGENT_PORT),Integer.toString(value));
        }
    };

    //-------------------------------------------------
    // Which JMX domain is the JAIN SLEE TCK plug-in MBean bound in? (Leave blank for default domain)
    //
    private StringQuestion qJmxAgentDomain = new StringQuestion(this,"jmxAgentDomain") {
        protected Question getNext() {
            return qTckResourceHostIP;
        }
        protected void export(Map data) {
            data.put(parent.envEntry(EnvironmentKeys.JMX_AGENT_DOMAIN),value);
        }
    };

    //-------------------------------------------------
    // What is the IP address of the JAIN SLEE TCK Resource?
    //
    // Suggested value: IP address of localhost
    private InetAddressQuestion qTckResourceHostIP = new InetAddressQuestion(this,"tckResourceHostIP") {

        {
        try {setValue(getLocalHost());} catch(Exception e) {e.printStackTrace();}}

        protected Question getNext() {
            return qTckResourcePort;
        }
        protected void export(Map data) {
            data.put(parent.envEntry(EnvironmentKeys.TCK_RESOURCE_HOST_IP),value.getHostAddress());
        }
    };

    //-------------------------------------------------
    // Which RMI port is the JAIN SLEE TCK Resource bound to?
    //
    // Suggested value: 4099
    private IntQuestion qTckResourcePort = new IntQuestion(this,"tckResourcePort") {

        {
        try {setValue(4099);} catch(Exception e) {e.printStackTrace();}}

        protected Question getNext() {
            return qComponentUrlPrefix;
        }
        protected void export(Map data) {
            data.put(parent.envEntry(EnvironmentKeys.TCK_RESOURCE_PORT),Integer.toString(value));
        }
    };

    //-------------------------------------------------
    // What URL prefix should the the container use to reach the slee-deployable components tree?
    //
    // Suggested value: the jars/deployable-units directory below the user's current working directory
    private StringQuestion qComponentUrlPrefix = new StringQuestion(this,"componentUrlPrefix") {

        {
            // guess the componentUrlPrefix based on the user's current directory
            String currentDir = new java.io.File(System.getProperty("user.dir")).getAbsolutePath();
            currentDir = currentDir.replace(File.separatorChar,'/');
            String expectedURL = "file://"+currentDir+"/jars/deployable-units";
            try {setValue(expectedURL);} catch(Exception e) {e.printStackTrace();}
        }

        protected Question getNext() {
            return isValid ? qEnd : null;
        }
        protected void export(Map data) {
            data.put(parent.envEntry(EnvironmentKeys.COMPONENT_URL_PREFIX),value);
        }
        public boolean isValidURL(String urlString) {
            try {
                new URL(urlString);
                return true;
            } catch(MalformedURLException mue) {
                return false;
            }
        }
        public void setValue(String newValue) {
            isValid = isValidURL(newValue);
            super.setValue(newValue);
        }
        private boolean isValid = false;
    };

    private FinalQuestion qEnd = new FinalQuestion(this);
    private EnvironmentInterview parent;

}
