/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.interview;

import java.util.Map;
import com.sun.interview.Interview;
import com.sun.interview.Question;
import com.sun.interview.FinalQuestion;
import com.sun.interview.ChoiceQuestion;
import com.sun.interview.IntQuestion;
import com.opencloud.sleetck.lib.EnvironmentKeys;
import com.opencloud.logging.LogLevel;
import com.opencloud.logging.LogLevelUtil;
import javax.slee.facilities.Level;

public class UserOptionsInterview extends Interview {

    public UserOptionsInterview(EnvironmentInterview parent) {
        super(parent,"user-options");
        this.parent = parent;
        setFirstQuestion(qDebugLevel);
    }

    // -- Questions -- //

    //-------------------------------------------------
    // What debug level should the tests be run at?
    //
    private ChoiceQuestion qDebugLevel = new ChoiceQuestion(this,"debugLevel") {
        {
            int[] validLevels = new int[] {LogLevel.SEVERE,LogLevel.WARNING,LogLevel.INFO,
                LogLevel.CONFIG,LogLevel.FINE,LogLevel.FINER,LogLevel.FINEST};
            String[] choices = new String[validLevels.length];
            for (int i = 0; i < validLevels.length; i++) choices[i] = LogLevelUtil.toString(validLevels[i]);
            setChoices(choices);
            setValue(LogLevelUtil.toString(LogLevel.INFO));
        }
        protected Question getNext() {
            return qSbbTraceLevel;
        }
        public void setValue(String s) {
            // convert the argument to one of our existing choices, and validate
            int logLevel = LogLevelUtil.toLogLevel(s);
            if(logLevel == LogLevel.UNKNOWN_LOG_LEVEL) throw new IllegalArgumentException("Invalid debug level: "+s);
            super.setValue(LogLevelUtil.toString(logLevel));
        }
        protected void export(Map data) {
            int level = LogLevelUtil.toLogLevel(value);
            data.put(parent.envEntry(EnvironmentKeys.DEBUG_LEVEL),Integer.toString(level));
        }
    };

    //-------------------------------------------------
    // What trace level should the TCK set for Sbbs activated via the SleeTCKTestUtils interface?
    //
    private ChoiceQuestion qSbbTraceLevel = new ChoiceQuestion(this,"sbbTraceLevel") {
        private Level[] validLevels = {Level.OFF,Level.SEVERE,Level.WARNING,
            Level.INFO,Level.CONFIG,Level.FINE,Level.FINER,Level.FINEST};

        // instance initializer
        {
            String[] choices = new String[validLevels.length];
            for (int i = 0; i < validLevels.length; i++) choices[i] = validLevels[i].toString();
            setChoices(choices);
            setValue(Level.INFO.toString());
        }
        protected Question getNext() {
            return qDefaultTimeout;
        }
        public void setValue(String s) {
            Level level = getLevel(s);
            if(level == null) throw new IllegalArgumentException("Invalid Sbb trace level: "+s);
            super.setValue(level.toString());
        }
        protected void export(Map data) {
            // lookup the Level from the array to find the int value
            String levelAsIntString = null;
            if(value != null) {
                Level level = getLevel(value);
                levelAsIntString = Integer.toString(level.toInt());
            }
            data.put(parent.envEntry(EnvironmentKeys.SBB_TRACE_LEVEL),levelAsIntString);
        }
        private Level getLevel(String levelString) {
            Level rLevel = null;
            if(levelString != null) {
                for (int i = 0; i < validLevels.length; i++) {
                    // note: this assumes that the return values of Level.toString() are unique and fixed for each Level
                    if(validLevels[i].toString().equalsIgnoreCase(levelString)) rLevel = validLevels[i];
                }
            }
            return rLevel;
        }
    };

    //-------------------------------------------------
    // What default time out (in milliseconds) should the tests use?
    //
    // Suggested value: 25000
    private IntQuestion qDefaultTimeout = new IntQuestion(this,"defaultTimeout") {

        {
        try {setValue(25000);} catch(Exception e) {e.printStackTrace();}}
        protected Question getNext() {
            return qRestartBetweenTests;
        }
        protected void export(Map data) {
            data.put(parent.envEntry(EnvironmentKeys.DEFAULT_TIMEOUT),Integer.toString(value));
        }

    };

    //-------------------------------------------------
    // Should the TCK restart the SLEE between tests?
    //
    private ChoiceQuestion qRestartBetweenTests = new ChoiceQuestion(this,"restartBetweenTests") {
        {
            String[] choices = {Boolean.TRUE.toString(),Boolean.FALSE.toString()};
            setChoices(choices);
            setValue(Boolean.TRUE.toString());
        }
        protected Question getNext() {
            return qEnd;
        }
        protected void export(Map data) {
            data.put(parent.envEntry(EnvironmentKeys.RESTART_BETWEEEN_TESTS),value);
        }
    };

    private FinalQuestion qEnd = new FinalQuestion(this);
    private EnvironmentInterview parent;

}
