/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.interview;

import java.util.Map;

import com.sun.interview.Interview;
import com.sun.interview.Question;
import com.sun.interview.FinalQuestion;
import com.sun.interview.StringQuestion;

import com.opencloud.sleetck.lib.EnvironmentKeys;

public class EnvironmentInterview extends Interview {

    public EnvironmentInterview(SleeTCKInterview parent) {
        super(parent, "env");
        this.parent = parent;
        setFirstQuestion(qEnvName);
    }

    String envEntry(String name) {
            return "env." + qEnvName.getValue() + "." + name;
    }

    String getName() {
            return qEnvName.getValue();
    }

    void setName(String name) {
            qEnvName.setValue(name);
    }

    //----------------------------------------------------------------------------
    //
    // Please provide a short identifier which will be used to name the
    // configuration you are creating here.
    //
    // Suggested value: Untitled
    private StringQuestion qEnvName = new StringQuestion(this, "envName") {

        {
         setValue("Untitled");}

        private boolean isValidIdentifier(String s) {
            if (s == null || s.equals(""))
            return false;

            if (!Character.isUnicodeIdentifierStart(s.charAt(0)))
            return false;

            for (int i = 1; i < s.length(); i++) {
            if (!Character.isUnicodeIdentifierPart(s.charAt(i)))
                return false;
            }

            return true;
        }

        public void setValue(String value) {
            valid = isValidIdentifier(value);
            super.setValue(value);
        }

        protected Question getNext() {
            if (valid)
            return qDescription;
            else
            return null;
        }

        private boolean valid;
    };

    //----------------------------------------------------------------------------
    //
    // Please provide a short description which can be used to identify
    // the configuration you are creating here.

    private Question qDescription = new StringQuestion(this, "description") {

        {
            setValue("JAIN SLEE TCK configuration");
        }
        
        protected void export(Map data) {
            data.put(envEntry(EnvironmentKeys.ENV_DESCRIPTION), value);
        }

        protected Question getNext() {
            if (value == null || value.trim().equals(""))
                return null;
            else
                return callInterview(iConnections,callInterview(iUserOptions,qEnd));
        }
    };


    //----------------------------------------------------------------------------
    private final SleeTCKInterview parent;
    private final Interview iConnections = new ConnectionsInterview(this);
    private final Interview iUserOptions = new UserOptionsInterview(this);
    private final Question qEnd = new FinalQuestion(this);

}

