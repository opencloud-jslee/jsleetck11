/*
* "Downloading, compilation and use of this TCK
* is governed by the terms of the JAIN SLEE (JSLEE) V1.1 TECHNOLOGY
* COMPATIBILITY KIT LICENCE AGREEMENT, the text of which is included in
* the download package as LICENSE and can be viewed at
* https://jsleetck11.dev.java.net/jslee-1.1-tck-license.txt"
*/

package com.opencloud.sleetck.lib.interview;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

import com.sun.interview.Interview;
import com.sun.javatest.interview.BasicInterviewParameters;
import com.sun.javatest.TestEnvironment;
import com.sun.interview.Question;
import com.sun.javatest.Parameters;
import com.sun.javatest.JavaTestError;
import com.sun.javatest.TestSuite;

/*

 Maintenance note:

 Each interview question is supported by:
  1) A key contant in EnvironmentKeys.java
  2) Two entries in the sleetck.properties file (smry and text)
  3) A 'more info' html help file
  4) An entry in the sleetckhelp.jhm file which references the more info file

 */
public class SleeTCKInterview extends BasicInterviewParameters implements Parameters.EnvParameters {

    public SleeTCKInterview() throws Interview.Fault {
        super("sleetck");
        // reference the properties file containing question keys and summaries
        setResourceBundle("sleetck");
        // reference the help set file containing 'more info' data
        setHelpSet("help/sleetck");
    }

    public SleeTCKInterview(TestSuite ts) throws Interview.Fault {
        this();
        setTestSuite(ts);
    }

    // -- Implementation of abstract methods -- //

    public TestEnvironment getEnv() {

        String envName = iEnvironment.getName();
        if (envName == null) return null;
        Map envMap = new HashMap();
        export(envMap);
        try {
            return new TestEnvironment(envName,envMap,"JAIN SLEE TCK Configuration Interview");
        }
        catch (TestEnvironment.Fault e) {
            throw new RuntimeException("TestEnvironment.Fault caught while creating the TestEnvironment: "+e);
        }
    }

    public Question getEnvFirstQuestion() {
        return callInterview(iEnvironment,getEnvSuccessorQuestion());
    }

    public Parameters.EnvParameters getEnvParameters() {
        return this;
    }

    // -- Private state -- //

    private final EnvironmentInterview iEnvironment = new EnvironmentInterview(this);

}
