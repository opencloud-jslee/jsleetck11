/*
 * @(#)BasedClassDescription.java	1.7 03/03/26
 *
 * Copyright (c) 2003 Sun Microsystems, Inc.  All rights reserved. 
 * SUN PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *
 * TDK Signature Test Tool. 
 *
 * @author Maxim Sokolnikov
 */

package com.sun.tdk.signaturetest;

import java.util.Enumeration;

/**
 * <b>BasedClassDescription</b> is used by <b>SignatureClassLoader</b> to load
 * some class description from signature file. Given some class description, all 
 * members declared by that class are listed here, as far as all members inherited 
 * by that class.
 *
 * @see SignatureClassLoader
 *
 * @version 03/03/26
 */
public class BasedClassDescription extends ClassDescription {

    /**
     * Parse <code>classDefinition</code> and <code>members</code> descriptions
     * (which are also strings). Use <b>FullItemDescription</b>.<code>setData()</code>
     * to parse <code>classDefinition</code>, and use <b>FullItemDescription</b>'s
     * constructor to parse descriptions in <code>members</code> enumeration.
     *
     * @see FullItemDescription#setData(String)
     */
    public BasedClassDescription(String classDefinition, Enumeration members) {
        setData(classDefinition);
        this.members = new ClassCollection();
        for (;members.hasMoreElements();) {
            FullItemDescription member;
            member = new FullItemDescription((String)members.nextElement());
            this.members.addElement(member.getItem(), member);
        }
    }

    /**
     * Return empty array.
     */    
    public FullItemDescription[] getDeclaredConstructors() {
        return new FullItemDescription[0];
    }
    
    /**
     * Return empty array.
     */    
    public FullItemDescription[] getDeclaredFields() {
        return new FullItemDescription[0];
    }
    
    /**
     * Return empty array.
     */    
    public FullItemDescription[] getDeclaredMethods() {
        return new FullItemDescription[0];
    }
    
    /**
     * Return empty array.
     */    
    public FullItemDescription[] getDeclaredClasses() {
        return new FullItemDescription[0];
    }
    
    /**
     * Return empty array.
     */    
    public FullItemDescription[] getInterfaces() {
        return new FullItemDescription[0];
    }
    
    /**
     * Return <code>null</code>.
     */    
    public FullItemDescription getSuperclass() {
        return null;
    }
}
    
