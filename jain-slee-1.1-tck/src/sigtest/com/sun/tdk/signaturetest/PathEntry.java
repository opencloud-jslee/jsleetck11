/*
 * @(#)PathEntry.java	1.6 03/06/09
 *
 * Copyright (c) 2003 Sun Microsystems, Inc.  All rights reserved. 
 * SUN PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 * 
 * TDK Signature Test Tool. 
 *
 * @author Maxim Sokolnikov
 * @author Serguei Ivashin
 */

package com.sun.tdk.signaturetest;

import java.io.IOException;
import java.io.InputStream;
import java.util.Vector;


public interface PathEntry {

    /** 
     * Initialize the module with given parameter: <code>path</code>. 
     * All errors encountered by the module should be placed into array 
     * <code>errors</code> 
     * 
     * @param path parameter provided to initialize module (usually a directory 
     *        or file path). 
     * @param errors vector of strings to place all generated error messages. 
     */      
    public void init(String path, Vector errors) throws IOException;
    
    
    /**
     * Free resources used (if any).
     */
    public void close();
    

    /** 
     * Search next class in the enumeration of classes found by this module.
     * After some class name is returned by <code>nextClassName()</code>
     * that class becomes <i>current</i> in classes enumeration and 
     * its bytecode can be accessed  with <code>getCurrentClass()</code> method. 
     *
     * @return Class qualified name or <code>null</code> if there is no 
     *         next class found.
     */
    public String nextClassName();


    /** 
     * Open an <b>InputStream</b> instance providing bytecode for the current class.
     * Note, that there is no current class defined until you call 
     * <code>nextClassName()</code>.
     *
     * @return <b>InputStream</b> providing bytecode for the current class.
     */
    public InputStream getCurrentClass() throws IOException;


    /** 
     * Returns <b>FileInputStream</b> providing bytecode for the required class
     * if the class could be found by the given qualified name. 
     *
     * @param name Qualified name of the class requested.
     */
    public InputStream findClass(String name)
        throws IOException, ClassNotFoundException;


    /**
     * Reset enumeration of classes which are found by this module.
     */
    public void setListToBegin();
}
