/*
 *  @(#)NestedClassTable.java	1.7 03/03/26 
 *
 * Copyright (c) 2003 Sun Microsystems, Inc.  All rights reserved. 
 * SUN PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *
 * TDK Signature Test Tool. 
 *
 * @author Maxim Sokolnikov
 */

package com.sun.tdk.signaturetest;

import java.util.Vector;

/**
 * <b>NestedClassTable</b> maintains a collection of names of some classes along with
 * lists of their nested classes. <b>ClassCollection</b> hashtable is used to implement 
 * such collection of nested class names. Given some enclosing class, its name is used 
 * as hashtable key, and names of its nested classes form <b>Vector</b> assigned to that 
 * key. Collection can maintain lists of nested classes for a number of enclosing classes.
 *
 * @see ClassCollection
 *
 * @version 03/03/26
 */
public class NestedClassTable {
    /**
     * Table of nested classes names.
     */
    private static ClassCollection nestedClasses = new ClassCollection();

    /**
     * Given some class <code>name</code> containing <code>'$'</code> character,
     * append it to <code>this</code> hastable using the enclosing class name
     * as hastable key.
     */
    public static void addNestedClass(String name) {
	if (name.indexOf('$') > 0) {
            String enc = name.substring(0, name.lastIndexOf('$'));
            nestedClasses.addUniqueElement(enc, name);
        }
    }

    /**
     * Return list of names of classes nested into the given enclosing class.
     *
     * @param name Name of the enclosing class.
     */
    public static Vector getNestedClasses(String name) {
        return nestedClasses.get(name);
    }
}
