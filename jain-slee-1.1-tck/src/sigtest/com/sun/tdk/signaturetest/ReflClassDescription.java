/*
 * @(#)ReflClassDescription.java	1.15 03/08/21
 *
 * Copyright (c) 2003 Sun Microsystems, Inc.  All rights reserved. 
 * SUN PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *
 * TDK Signature Test Tool. 
 *
 * @author Maxim Sokolnikov
 */

package com.sun.tdk.signaturetest;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.Vector;

/**
 * <b>ReflClassDescription</b> is intended to find public or protected members of
 * a class using the reflections package.
 *
 * @see ArchiveFinder
 *
 * @version 03/08/21
 */
public class ReflClassDescription extends ClassDescription {
    /** class object which is required in the declared members founding. **/
    Class classObject;

    /**
     * Describe the given <code>classObject</code>
     */
    ReflClassDescription(Class classObject) {
        this.classObject = classObject;
        memberType = CLASS;
	modifiers = (classObject == null) ? new Vector() :
                    printClassModifiers(classObject.getModifiers());
        parseQualifiedName(getName());
    }

    /**
     * Incarnate <b>ItemDescription</b> referring to the given <code>classObject</code>.
     * Such reference is intended to model relationship of being superclass or superinterface
     * (i.e.: relationship of being a class extended by some other class, or being an interface 
     * implemented by some class). The <code>classObject</code> is the class being extended,
     * or the interface being implemented. That <b>ClassDescription</b> instance containing
     * <code>this</code> reference in its <code>members</code> list just describes the class 
     * extending or implementing the given <code>classObject</code>.
     *
     * @param isClass Indicates that the <code>classObject</code> refers to class being 
     *     extended; otherwise, it refers to interface being implemented.
     *
     * @see ItemDescription
     * @see ClassDescription#members
     */
    ReflClassDescription(Class classObject, boolean isClass) {
        this.classObject = classObject;
        if (isClass)
            memberType = SUPER;
        else
            memberType = INTERFACE;
        parseQualifiedName(getName());
    }

    /**
     * Qualified name of that class described by <code>this</code> instance.
     */
    public String getName() {
        return classObject.getName();
    }
    
    /**
     * Generate new <b>FullItemDescription</b> for each of all methods declared by 
     * that class referred by <code>this</code> instance.
     */
    public FullItemDescription[] getDeclaredMethods() {
        Method[] methods = classObject.getDeclaredMethods();
        FullItemDescription[] retVal = new FullItemDescription[methods.length];
        for (int i = 0; i < retVal.length; i++)
            retVal[i] = new FullItemDescription(methods[i]);
        return retVal;
    }

    /**
     * Generate new <b>FullItemDescription</b> item for each of fields declared by that 
     * class referred by <code>this</code> instance.
     */
    public FullItemDescription[] getDeclaredFields() {
        Field[] fields = classObject.getDeclaredFields();
        FullItemDescription[] retVal = new FullItemDescription[fields.length];
        for (int i = 0; i < retVal.length; i++)
            retVal[i] = new FullItemDescription(fields[i]);
        return retVal;
    }
    
    /**
     * Return array of <b>ReflClassDescription</b> items to describe all nested classes 
     * declared by that class, which <code>this</code> instance refers to.
     */
    public FullItemDescription[] getDeclaredClasses() {
        Class nested[] = classObject.getDeclaredClasses();
        ReflClassDescription[] retVal = new ReflClassDescription[nested.length];
        for (int i = 0; i < retVal.length; i++)
            retVal[i] = new ReflClassDescription(nested[i]);
        return retVal;
    }

    /**
     * Generate new <b>FullItemDescription</b> item for each of constructors declared 
     * by that class, which <code>this</code> instance refers to.
     */
    public FullItemDescription[] getDeclaredConstructors() {
        Constructor[] ctors = classObject.getDeclaredConstructors();
        FullItemDescription[] retVal = new FullItemDescription[ctors.length];
        for (int i = 0; i < retVal.length; i++)
            retVal[i] = new FullItemDescription(ctors[i]);
        return retVal;
    }

    /**
     * Generate <b>ReflClassDescription</b> items for all interfaces implemented 
     * by that class referred by <code>this</code> instance.
     */
    public FullItemDescription[] getInterfaces() {
        Class interfaces[] = classObject.getInterfaces();
        ReflClassDescription[] retVal = new ReflClassDescription[interfaces.length];
        for (int i = 0; i < retVal.length; i++)
            retVal[i] = new ReflClassDescription(interfaces[i], false);
        return retVal;
    }

    /**
     * Generate new <b>ReflClassDescription</b> for the class extended by that
     * class, which <code>this</code> instance refers to.
     */
    public FullItemDescription getSuperclass() {
        Class spr = classObject.getSuperclass();
        if (spr == null)
            return null;
        else
            return new ReflClassDescription(spr, true);
    }

    /**
     * Return description of the class declaring that class described by 
     * <code>this</code> <b>ReflClassDescription</b> instance. 
     * Return <code>null</code>, if <code>this</code> item does not describe
     * nested class.
     *
     * @see ClassDescription#getEnclosingClass()
     * @see FullItemDescription#getDeclaringClass()
     */
    public FullItemDescription getEnclosingClass() {
        Class declClass = classObject.getDeclaringClass();
        return ((declClass == null) ? null :
                new ReflClassDescription(classObject.getDeclaringClass()));
    }
    
    /**
     * Compare classes described by <code>this</code> and by the given object <code>o</code>,
     * if <code>o</code> is an instance of <b>ReflClassDescription</b>. Compare only 
     * <code>classObject</code> fields. (The <code>classObject</code> field are described 
     * in comments to <b>ReflClassDescription</b> constructors.)
     * If <code>o</code> is an instance of <b>FullItemDescription</b>, then descriptions 
     * for the two classes are compared instead of the classes themselves.
     */
    public boolean equals(Object o) {
        if (o instanceof ReflClassDescription) 
            return this.classObject.equals(((ReflClassDescription)o).classObject);
        else
            return super.equals(o);
    }

    /**
     * Initiate the <code>members</code> field. 
     * (But do not list the <code>&lt;clinit&gt;</code> method.)
     *
     * @see ClassDescription#members
     */
    public void createPublicMembers() {
        Method[] methods = classObject.getMethods();
        for (int i = 0; i < methods.length; i++) {
            FullItemDescription meth = new FullItemDescription(methods[i]);
            if (!meth.getName().equals("<clinit>"))
                members.addUniqueElement(meth.getItem(), meth);
        }
        Field[] fields = classObject.getFields();
        for (int i = 0; i < fields.length; i++) {
            FullItemDescription field = new FullItemDescription(fields[i]);
            members.addUniqueElement(field.getItem(), field);
        }
        Constructor[] ctors = classObject.getConstructors();
        for (int i = 0; i < ctors.length; i++) {
            FullItemDescription ctor = new FullItemDescription(ctors[i]);
            members.addUniqueElement(ctor.getItem(), ctor);
        }
    }
        
}

    

    
    
