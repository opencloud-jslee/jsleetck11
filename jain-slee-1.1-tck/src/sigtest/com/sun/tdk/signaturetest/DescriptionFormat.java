/*
 * @(#)DescriptionFormat.java	1.17 03/08/01
 *
 * Copyright (c) 2003 Sun Microsystems, Inc.  All rights reserved. 
 * SUN PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *
 * TDK Signature Test Tool. 
 *
 * @author Maxim Sokolnikov
 */

package com.sun.tdk.signaturetest;

import java.util.Enumeration;
import java.util.Properties;
import java.util.Vector;

/**
 * <b>DescriptionFormat</b> describes several options for <b>SignatureTest</b>.
 * Namely, it determines if <b>SignatureTest</b> have to: <ul>
 * <li> use qualified names for classes (<code>"QualifiedName"</code> option),
 * <li> ignore <code>static</code> modifier for nested classes 
 *      (<code>"NestedStatic"</code> option),
 * <li> ignore <code>protected</code> modifier for nested classes 
 *      (<code>"NestedProtected"</code> option).
 * </ul>
 *
 * @see SignatureTest
 */
public class DescriptionFormat {
    
    /** 
     * Specify if <code>this</code> <b>DescriptionFormat</b> instance
     * demands qualified classes names.
     */
    protected boolean isQualifiedName;
    
    /** 
     * Specify if <code>this</code> <b>DescriptionFormat</b> instance
     * prescribes to ignore <code>static</code> modifier for nested classes.
     */
    private boolean isNestedStatic = true;

    /** 
     * Specify if <code>this</code> <b>DescriptionFormat</b> instance
     * prescribes to ignore <code>protected</code> modifier for nested 
     * classes.
     */
    private boolean isNestedProtected = true;

    /** 
     * Create new bunch of options for <b>SignatureTest</b>.
     * Namely, it determines if <b>SignatureTest</b> have to: <ul>
     * <li> use qualified names for classes (<code>"QualifiedName"</code> option),
     * <li> ignore <code>static</code> modifier for nested classes 
     *      (<code>"NestedStatic"</code> option),
     * <li> ignore <code>protected</code> modifier for nested classes 
     *      (<code>"NestedProtected"</code> option).
     * </ul>
     *
     * @param isQualifiedName If qualified names should be used.
     * @param details Get the properties <code>"NestedStatic"</code> and 
     *     <code>"NestedProtected"</code> from <code>details</code>.
     *
     * @see SignatureTest
     *
     * @see java.util.Properties
     */
    public DescriptionFormat(boolean isQualifiedName, Properties details) {
        this.isQualifiedName = isQualifiedName;
        
        // creates default DefinitionFormat for SignatureTest
        isNestedStatic = (details.getProperty("NestedStatic") == null);
        isNestedProtected = (details.getProperty("NestedProtected") == null);
    }

    /**
     * Default <b>DescriptionFormat</b>: assign all the options 
     * <code>"QualifiedName"</code>, <code>"NestedStatic"</code>, and <code>"NestedProtected"</code> 
     * to be <code>true</code>.
     */
    public DescriptionFormat() {
        this.isQualifiedName = true;
    }        

    /**
     * Return <code>item</code>'s signature along with its <code>memberType</code>, 
     * and modifiers, and returned value's type, so that rather short 
     * <code>item</code>'s name be displayed. However, qualified name should be 
     * displayed, if <code>this</code> <b>DescriptionFormat</b> instance includes 
     * the <code>"QualifiedName"</code> option. Also, modifiers <code>static</code> 
     * and/or <code>protected</code>, and <code>throws</code> clause should be omitted, 
     * if <code>this</code> instance demands so. (Note, that <code>memberType</code> 
     * for inner class is <code>INNER</code> instead of <code>CLASS</code>.)
     *
     * @see #convertShortClassDescription(FullItemDescription)
     * @see #convertClassDescription(FullItemDescription)
     * @see #convertDescription(FullItemDescription)
     *
     * @see ItemDescription#INNER
     * @see ItemDescription#CLASS
     *
     * @see FullItemDescription#memberType
     * @see FullItemDescription#getModifiers()
     * @see FullItemDescription#getSignature()
     */
    public String convertShortDescription(FullItemDescription item) {
        String retVal = convertDescription(item, true);
        if (retVal.startsWith(ItemDescription.CLASS))
            return (ItemDescription.INNER +
                    retVal.substring(ItemDescription.CLASS.length()));
        else
            return retVal;
    }

    /**
     * Return <code>item</code>'s signature along with its <code>memberType</code>, 
     * and modifiers, and returned value's type, so that qualified <code>item</code>'s
     * name is displayed. Modifiers <code>static</code> and/or <code>protected</code>, 
     * and <code>throws</code> clause should be omitted, if <code>this</code> 
     * <b>DescriptionFormat</b> instance demands so. (Note, that <code>memberType</code> 
     * for inner class is <code>INNER</code> instead of <code>CLASS</code>.)
     *
     * @see #convertShortClassDescription(FullItemDescription)
     * @see #convertClassDescription(FullItemDescription)
     * @see #convertShortDescription(FullItemDescription)
     *
     * @see ItemDescription#INNER
     * @see ItemDescription#CLASS
     *
     * @see FullItemDescription#memberType
     * @see FullItemDescription#getModifiers()
     * @see FullItemDescription#getSignature()
     */
    public String convertDescription(FullItemDescription item) {
        String retVal = convertDescription(item, false);
        if (retVal.startsWith(ItemDescription.CLASS))
            return (ItemDescription.INNER +
                    retVal.substring(ItemDescription.CLASS.length()));
        else
            return retVal;
    }

    /**
     * Return qualified name for the given class <code>item</code> along with its 
     * modifiers. The <code>item.memberType</code> is displayed as <code>CLASS</code> 
     * instead of <code>INNER</code>, if the given <code>item</code> is inner class. 
     * Modifiers <code>static</code> and/or <code>protected</code> are omitted, 
     * if <code>this</code> <b>DescriptionFormat</b> instance prescribes so.
     *
     * @exception IllegalArgumentException If the given <code>item</code> is not 
     *     a class item.
     *
     * @see #convertShortClassDescription(FullItemDescription)
     * @see #convertShortDescription(FullItemDescription)
     * @see #convertDescription(FullItemDescription)
     *
     * @see ItemDescription#INNER
     * @see ItemDescription#CLASS
     *
     * @see FullItemDescription#memberType
     * @see FullItemDescription#getModifiers()
     * @see FullItemDescription#getSignature()
     */
    public String convertClassDescription(FullItemDescription item) {
        String retVal = convertDescription(item, false);
        if (item.isClass())
	    return retVal;
	throw new IllegalArgumentException("It's not class");
    }

    /**
     * Return rather short name for the given class <code>item</code> 
     * along with its modifiers. The <code>item.memberType</code> is displayed 
     * as <code>CLASS</code> instead of <code>INNER</code>, if the given 
     * <code>item</code> is inner class item. Modifiers <code>static</code> 
     * and/or <code>protected</code> are omitted, if <code>this</code> 
     * <b>DescriptionFormat</b> instance prescribes so. Also, <code>this</code> 
     * instance may prescribe that qualified <code>item</code>'s name be 
     * displayed instead of short name.
     *
     * @exception IllegalArgumentException If the given <code>item</code> 
     *     is not a class item.
     *
     * @see #convertClassDescription(FullItemDescription)
     * @see #convertShortDescription(FullItemDescription)
     * @see #convertDescription(FullItemDescription)
     *
     * @see ItemDescription#INNER
     * @see ItemDescription#CLASS
     *
     * @see FullItemDescription#memberType
     * @see FullItemDescription#getModifiers()
     * @see FullItemDescription#getSignature()
     */
    public String convertShortClassDescription(FullItemDescription item) {
        String retVal = convertDescription(item, true);
        if (item.isClass())
	    return retVal;
	throw new IllegalArgumentException("It's not class");
    }
    
    /**
     * Return <code>item</code>'s signature along with its <code>memberType</code>, 
     * and modifiers, and returned value's type. Modifiers <code>static</code> and/or 
     * <code>protected</code>, and <code>throws</code> clause should be omitted, if 
     * <code>this</code> <b>DescriptionFormat</b> instance demands so.
     * (Note, that <code>memberType</code> for inner class is <code>INNER</code>
     * instead of <code>CLASS</code>.)
     *
     * @param isShort When <code>false</code>, it forces to display qualified name 
     *     for the given <code>item</code>, if that <code>item</code> is class item, 
     *     and <code>this.isQualifiedName</code> is <code>false</code>.
     *
     * @see #isQualifiedName
     *
     * @see ItemDescription#INNER
     * @see ItemDescription#CLASS
     *
     * @see FullItemDescription#memberType
     * @see FullItemDescription#getModifiers()
     * @see FullItemDescription#getSignature()
     */
    private String convertDescription(FullItemDescription item, boolean isShort) {
        String retVal = item.memberType;
        Vector modifiers = item.getModifiers();
        for (Enumeration e = modifiers.elements(); e.hasMoreElements();) {
            String modifier = (String)e.nextElement();
            if (isModifierTracked(modifier, item))
                retVal = retVal + modifier + " ";
        }

        String type = item.getType();
        retVal = retVal + (((type == null) || type.equals("")) ? "" : type + " ") + "";
        if (item.isClass() && !isShort || isQualifiedName) {
            String encl = item.getDeclaringClass();
            if ((encl != null) && !encl.equals(""))
                retVal = retVal + encl + (item.isClass() ? "$" : ".");
        }
        retVal = retVal + item.getSignature();
        String throwables = item.getThrowables();
        if (throwables != null && !throwables.equals(""))
            retVal = retVal + " " + throwables;
        return retVal;
    }

    /**
     * Concatenate those modifiers assigned to the given <code>item</code>,
     * which need to be verified according to demands of <code>this</code> 
     * <b>DescriptionFormat</b> instance. (Modifiers <code>static</code> and/or
     * <code>protected</code> should be omitted, if <code>this</code> instance
     * demands so.)
     *
     * @see #isModifierTracked(String,FullItemDescription)
     */
    private String convertModifiers(FullItemDescription item) {
        String retVal = "";
        Vector modifiers = item.getModifiers();
        for (Enumeration e = modifiers.elements(); e.hasMoreElements();) {
            String modifier = (String)e.nextElement();
            if (isModifierTracked(modifier, item))
                retVal = retVal + " " + modifier;
        }
        return retVal;
    }

    /**
     * Check if <code>this</code> <b>DescriptionFormat</b> instance demands to 
     * verify the given <code>modifier</code> for the given <code>item</code>.
     * Special cases are if the <code>item</code> is a class, and 
     * <code>"NestedStatic"</code> and/or <code>"NestedProtected"</code> property
     * was qualified for <code>this</code> <b>DescriptionFormat</b>.
     */
    protected boolean isModifierTracked(String modifier,
                                        FullItemDescription item) {
        if (modifier.equals("public") ||
            modifier.equals("final") ||
            modifier.equals("protected") &&
            (isNestedProtected || !item.isClass()) ||
            modifier.equals("static") &&
            (isNestedStatic || !item.isClass()) ||
            modifier.equals("abstract") ||
            modifier.equals("interface") || modifier.equals("volatile"))
            return true;
        else
            return false;
    }
                
}
