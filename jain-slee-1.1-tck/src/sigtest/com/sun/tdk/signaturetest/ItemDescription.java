/*
 * @(#)ItemDescription.java	1.13 03/03/26
 *
 * Copyright (c) 2003 Sun Microsystems, Inc.  All rights reserved. 
 * SUN PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *
 * TDK Signature Test Tool. 
 *
 * @author Maxim Sokolnikov
 */

package com.sun.tdk.signaturetest;

/**
 * This class maintains information about <code>memberType</code> and 
 * <code>name</code> of some class or class member described by some 
 * <b>ClassDescription</b>. Moreover, superclass and/or interfaces name(s) 
 * specified by ``<i><b>extends</b> SuperClass</i>'' and/or 
 * ``<i><b>implements</b> Interface_1, ..., Interface_N</i>''
 * suffixes are supported with special <code>memberType</code> codes.
 *
 * @see FullItemDescription
 * @see ClassDescription
 * 
 * @version 03/03/26
 */
public class ItemDescription {
    public static final String 
        CLASS        = "CLSS ",
        SUPER        = "supr ",
        INTERFACE    = "intf ",
        CONSTRUCTOR  = "cons ",
        METHOD       = "meth ",
        FIELD        = "fld  ",
        // If the field is a primitive constants, then this modifier is added
        // to the field description.
        PRIMITIVE_CONSTANT        = "<constant>",
	INNER        = "innr ",
	NATIVE       = "native",
	SYNCHRONIZED = "synchronized",
        TRANSIENT    = "transient",
        FLAG_SUPER   = "<flag_super>",
        // if the member is synthetic, then this modifier is added
        // to the field description.
        SYNTHETIC    = "<synthetic>",
        DEPRECATED   = "<deprecated>";
    

    static final String[][] prefixes = {
	{CLASS, "class "},
	{SUPER, "superclass "},
	{INTERFACE, "interface "},
	{CONSTRUCTOR, "constructor "},
	{METHOD, "method "},
	{FIELD, "field "},
	{PRIMITIVE_CONSTANT, "field "},
	{INNER, "innerclass "},
    };
    /* constants for class file parsing */
    
    /* Class File Constants */
    public static final int JAVA_MAGIC                   = 0xcafebabe;
    public static final int JAVA_VERSION                 = 45;
    public static final int JAVA_MINOR_VERSION           = 3;

    /* Constant table */
    public static final int CONSTANT_UTF8                = 1;
    public static final int CONSTANT_UNICODE             = 2;
    public static final int CONSTANT_INTEGER             = 3;
    public static final int CONSTANT_FLOAT               = 4;
    public static final int CONSTANT_LONG                = 5;
    public static final int CONSTANT_DOUBLE              = 6;
    public static final int CONSTANT_CLASS               = 7;
    public static final int CONSTANT_STRING              = 8;
    public static final int CONSTANT_FIELD               = 9;
    public static final int CONSTANT_METHOD              = 10;
    public static final int CONSTANT_INTERFACEMETHOD     = 11;
    public static final int CONSTANT_NAMEANDTYPE         = 12;

    /* Access and modifier flags */
    public static final int ACC_PUBLIC                   = 0x00000001;
    public static final int ACC_PRIVATE                  = 0x00000002;
    public static final int ACC_PROTECTED                = 0x00000004;
    public static final int ACC_STATIC                   = 0x00000008;
    public static final int ACC_FINAL                    = 0x00000010;
    public static final int ACC_SYNCHRONIZED             = 0x00000020;
    public static final int ACC_VOLATILE                 = 0x00000040;
    public static final int ACC_TRANSIENT                = 0x00000080;
    public static final int ACC_NATIVE                   = 0x00000100;
    public static final int ACC_INTERFACE                = 0x00000200;
    public static final int ACC_ABSTRACT                 = 0x00000400;
    public static final int ACC_SUPER                    = 0x00000020;
    public static final int ACC_STRICT		         = 0x00000800;
    public static final int ACC_EXPLICIT		 = 0x00001000;
    

    /**
     * Sort of entity referred by <code>this</code> item. It should be either field, 
     * or method, or constructor, or class or inner class, or interface being 
     * implemented by some class, or <i>superclass</i> being extended by some class.
     *
     * @see #isField()
     * @see #isMethod()
     * @see #isConstructor()
     * @see #isClass()
     * @see #isSuperClass()
     * @see #isSuperInterface()
     */
    public String memberType;
    
    /**
     * Short name of <code>this</code> item.
     */
    public String name;

    /** Dummy constructor: it does nothing. */
    protected ItemDescription() {
    }
    
    /**
     * Split the given <code>definition</code> into <code>memberType</code> 
     * and <code>name</code> fields; and substitute <i>class</i> 
     * <code>memberType</code> for <i>inner class</i>.
     * The <code>definition</code> may imply empty <code>name</code>, or it may 
     * be <code>null</code> to indicate that both <code>memberType</code> and 
     * <code>name</code> should incarnate with empty strings.
     *
     * @see #memberType
     * @see #name
     *
     * @see #CLASS
     * @see #INNER
     */
    public ItemDescription(String description) {
        if (description == null) {
            memberType = "";
            name = "";
        } else if (description.length() > CLASS.length()) {
            memberType = description.substring(0, CLASS.length());
            name = description.substring(CLASS.length());
        } else {
            memberType = description;
            name = "";
        }
        memberType = memberType.equals(INNER) ? CLASS : memberType;
    }

    /**
     * Check if both <code>name</code> and <code>memberType</code> for
     * <code>this</code> <b>ItemDescription</b> instance are the same as 
     * for the given <b>ItemDescription</b> <code>o</code>. 
     * In particular, this method returns <code>false</code>, 
     * if <b>Class</b> of the given <b>Object</b> <code>o</code> 
     * is not <b>ItemDescription</b>.
     *
     * @see #name
     * @see #memberType
     */
    public boolean equals(Object o) {
        if (o instanceof ItemDescription) 
            return (memberType.equals(((ItemDescription)o).memberType) &&
                    name.equals(((ItemDescription)o).name));
        else
            return false;
    }

    /** 
     * Check if <code>this</code> is either class or inner class item.
     *
     * @see #CLASS
     * @see #INNER
     */
    public boolean isClass() {
        return (this.memberType.equals(CLASS) ||
                this.memberType.equals(INNER));
    }

    /**
     * Check if <code>this</code> item describes superclass for some class.
     * (I.e., check if <code>this</code> describes ``<i><b>extends</b></i> ...'' 
     * suffix for some <b>ClassDescription</b>.)
     *
     * @see #SUPER
     *
     * @see ClassDescription
     */
    public boolean isSuperClass() {
        return this.memberType.equals(SUPER);
    }
    
    /**
     * Check if <code>this</code> item describes interface class for some class.
     * (I.e., check if <code>this</code> describes some of interface name(s) in 
     * ``<i><b>implements</b></i> ...'' suffix for some <b>ClassDescription</b>.)
     *
     * @see #INTERFACE
     *
     * @see ClassDescription
     */
    public boolean isSuperInterface() {
        return this.memberType.equals(INTERFACE);
    }

    /**
     * Check if <code>this</code> item describes some field.
     *
     * @see #FIELD
     */
    public boolean isField() {
        return this.memberType.equals(FIELD);
    }

    /**
     * Check if <code>this</code> item describes some method.
     *
     * @see #METHOD
     */
    public boolean isMethod() {
        return this.memberType.equals(METHOD);
    }

    /**
     * Check if <code>this</code> item describes some constructor.
     *
     * @see #CONSTRUCTOR
     *
     * @see ClassDescription
     */
    public boolean isConstructor() {
        return this.memberType.equals(CONSTRUCTOR);
    }

    /**
     * Hashcode for <code>this</code> item.
     *
     * @see #memberType
     * @see #name
     *
     * @see String#hashCode()
     */
    public int hashCode() {
        return (memberType + name).hashCode();
    }
}
