/*
 * @(#)SigTest.java	1.29 03/08/22
 *
 * Copyright (c) 2003 Sun Microsystems, Inc.  All rights reserved. 
 * SUN PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *
 * TDK Signature Test Tool. 
 *
 * This is the base class for Setup, SignatureTest and Merge.
 *
 * @author Maxim Sokolnikov
 * @author Serguei Ivashin
 */

package com.sun.tdk.signaturetest;

import java.lang.reflect.Method;
import java.util.Enumeration;
import java.util.Properties;
import java.util.Vector;


/**
 * This class represents common part of the signature tests.
 * It provides tools for parsing common parameters and defining common
 * attributes of the classes such as accessibility and appurtenance to
 * the required packages.
 *
 * <p>This class parses the following options common for signature tests:
 * <dl>
 *       <dt><code>-Package</code> &lt;package&gt;
 *       <dt><code>-PackageWithoutSubpackages</code> &lt;package&gt;
 *       <dt><code>-Exclude</code> &lt;package_or_class_name&gt;
 *       <dt><code>-Classpath</code> &lt;path&gt;
 *       <dt><code>-Version</code> &lt;version&gt;
 *       <dt><code>-static</code>
 *       <dt><code>-ClassCacheSize</code> &lt;number&gt;
 *       <dt><code>-AllPublic</code>
 *       <dt><code>-Detail</code> &lt;detail&gt; <i>This is obsoleted option.</i>
 *       <dt><code>-UseReflect</code> <i>This is obsoleted option.</i>
 * </dl>
 *
 * @version 03/08/22
 */
class SigTest extends Result {
    /**
     * If the <code>trackMode</code> field equals to <code>ALL_PUBLIC</code>,
     * every <code>public</code> or <code>protected</code> class is considered
     * to be accessible. Otherwise, ordinal accessibility rules are applied.
     * These rules imply, that <code>public</code> or <code>protected</code> nested
     * class may become inaccessible because of stronger accessibility limitations
     * assigned to its declaring class, or to class declaring its declaring class,
     * and so on.
     *
     * @see #trackMode
     * @see #isAccessible(ClassDescription)
     */
    protected static final int ALL_PUBLIC = 2;

    /**
     * Either equals to <code>ALL_PUBLIC</code>, or not.
     *
     * @see #ALL_PUBLIC
     * @see #isAccessible(ClassDescription)
     */
    protected int trackMode;

    /**
     * List of names of packages to be checked along with subpackages.
     */
    protected PackageGroup packages = new PackageGroup(true);

    /**
     * List of names of packages to be checked excluding subpackages.
     */
    protected PackageGroup purePackages = new PackageGroup(false);

    /**
     * List of names of packages to be ignored along with subpackages.
     */
    protected PackageGroup excludedPackages = new PackageGroup(true);

    /**
     * List of directories and/or zip-files containing the packages to be checked.
     *
     * @see java.io.File#pathSeparator
     */
    protected String classpath = null;

    /**
     * Contains properties specified by <code>-Detail</code> option.
     */
    protected Properties details = new Properties();

    /**
     * Collector for error messages, or <code>null</code> if log is not required.
     */
    protected  ErrorFormatter errorManager;

    /**
     * Either static or reflections-based class descriptions finder.
     *
     * @see #isStatic
     */
    protected ClassDescrLoader loader;

    /**
     * Options for descriptions of classes and/or class members.
     *
     * @see FullItemDescription
     */
    protected DescriptionFormat converter;

    /**
     * Version of the product being tested.
     */
    protected String apiVersion;

    /**
     * Use static <b>ClassDescrLoader</b> to load class descriptions;
     * otherwise, use reflections to obtain information about classes.
     *
     * @see ReflClassDescrLoader
     */
    protected boolean isStatic = false;

    /**
     * <b>ArchiveClassDescrLoader</b> may cache up to <code>cacheSize</code>
     * classes being loaded.
     */
    protected int cacheSize = 100;
    
    static final String sep = System.getProperty("path.separator");
    
    //  Debug mode (printing stack trace)
    static boolean debug = false;

    /**
     * Parse symbolic parameters passed to signature test. Vector <code>args</code>
     * contains some components of the array <code>args[]</code> passed to the
     * method <code>main</code>(<b>String</b>[]) of <b>SignatureTest</b> or some
     * other subclass of <b>SigTest</b>. Vector <code>args</code> should contains
     * only those symbolic parameters, which are not recognized by that subclass.
     *
     * @see SignatureTest
     * @see SignatureTest#parseParameters(Vector)
     */
    protected boolean parseParameters(Vector args) {
    
        boolean isPackageDefined = false;

        for (Enumeration e = args.elements(); e.hasMoreElements();) {
            String argument = (String)e.nextElement();
            
            // specification of the tested classes
            if (argument.equalsIgnoreCase("-Package")) {
                if (!e.hasMoreElements())
                    return failed("Missing value for option -Package");
                packages.addPackageName((String)e.nextElement());
                isPackageDefined = true;
                
            } else if (argument.equalsIgnoreCase("-PackageWithoutSubpackages")) {
                if (!e.hasMoreElements())
                    return failed("Missing value for option -PackageWithoutSubpackages");
                purePackages.addPackageName((String)e.nextElement());
                isPackageDefined = true;
                
            } else if (argument.equalsIgnoreCase("-Exclude")) {
                if (!e.hasMoreElements())
                    return failed("Missing value for option -Exclude");
                excludedPackages.addPackageName((String)e.nextElement());
                
            //specification of the tested class path
            } else if (argument.equalsIgnoreCase("-Classpath")) {
                if (!e.hasMoreElements())
                    return failed("Missing value for option -Classpath");
                if (classpath != null)
                    return failed("The -Classpath option is specified twice.");
                classpath = (String)e.nextElement();
                    
            } else if (argument.equalsIgnoreCase("-Detail")) {
                if (!e.hasMoreElements())
                    return failed("Missing value for option -Detail");
                details.put((String)e.nextElement(), "true");
                
            } else if (argument.equalsIgnoreCase("-Version") ||
                       argument.equalsIgnoreCase("-apiVersion")) {
                if (!e.hasMoreElements())
                    return failed("Missing value for option -apiVersion");
                apiVersion = (String)e.nextElement();
                
            } else if (argument.equalsIgnoreCase("-static")) {
                isStatic = true;
                
            } else if (argument.equalsIgnoreCase("-ClassCacheSize")) {
                cacheSize = 0;
                try {
                    if (e.hasMoreElements())
                        cacheSize = Integer.parseInt((String)e.nextElement());
                } catch (NumberFormatException ex) {
                    if (debug)
                        ex.printStackTrace();
                    cacheSize = 0;
                }
                if (cacheSize <= 0)
                    return failed("Invalid value for option: " + argument);
                    
            } else if (argument.equalsIgnoreCase("-AllPublic")) {
                trackMode = ALL_PUBLIC;
                
            } else if (argument.equalsIgnoreCase("-debug")) {
                debug = true;
                
            } else {
                return failed("Incorrect option: " + argument);
            }
        }
        
        if (apiVersion == null) 
            apiVersion = "";
        
        if (!isPackageDefined)
            packages.addPackageName("");
            
        return passed();
    }
    
    
    /**
     * Check if the given class <code>name</code> belongs to some of
     * the packages marked to be tested.
     *
     * @see #packages
     * @see #purePackages
     * @see #excludedPackages
     */
    protected boolean isPackageMember(String name) {
        return !excludedPackages.checkName(name) &&
                (packages.checkName(name) || purePackages.checkName(name));
    }

    /**
     * Check if the class described by <code>c</code> is to be traced accordingly
     * to <code>trackMode</code> set for <code>this</code> instance.
     * Every <code>public</code> or <code>protected</code> class is accessible,
     * if it is not nested to another class having stronger accessibility limitations.
     * However, if <code>trackMode</code> is set to <code>ALL_PUBLIC</code> for
     * <code>this</code> instance, every <code>public</code> or
     * <code>protected</code> class is considered to be accessible despite of
     * its accessibility limitations possibly inherited.
     *
     * @see #trackMode
     * @see #ALL_PUBLIC
     */
    boolean isAccessible(ClassDescription c) {
        if (trackMode == ALL_PUBLIC)
            return (c.isPublic() || c.isProtected());

        for (ClassDescription cl = c; cl != null;) {
            if (!cl.isPublic() && !cl.isProtected())
                return false;
            FullItemDescription spr = cl.getEnclosingClass();
            if (spr == null)
                return true;
            if (spr instanceof ClassDescription)
                cl = (ClassDescription)spr;
            else
                try {
                    cl = loader.loadClass(spr.getName());
                } catch (ClassNotFoundException e) {
                    return true;
                } catch (LinkageError er) {
                    if (debug)
                        er.printStackTrace();
                    if (errorManager != null)
                        errorManager.addError("LinkageError", c.getName(),
                                              "can't track accessibility : " +
                                              er + "thrown", null);
                    return false;
                }
        }
        return true;
    }

    /*----    Nested classes   ----*/

    /**
     * <b>PackageGroup</b> is intended to maintain a list of packages
     * names. It provides a tool to check if given class belongs to
     * some of the packages listed by <b>PackageGroup</b>, or if that
     * class belongs to some subpackage of some of the listed packages.
     */
    public static class PackageGroup {
	/**
	 * For every package listed in the <code>group</code> filed,
	 * indicate if its subpackages are implicitly implied.
	 *
	 * @see #group
	 */
        boolean isSubpackagesUses = true;

	/**
	 * Vector of strings intended to contain names of packages.
	 * If the field <code>isSubpackagesUses</code> is <code>true</code>,
	 * all subpackages names are implicitly implied for each package
	 * name listed here.
	 *
	 * @see #isSubpackagesUses
	 */
        Vector group;

	/**
	 * Create empty list of packages, and decide if subpackages
	 * should be implied.
	 */
        public PackageGroup(boolean isSubpackagesUses) {
            this.isSubpackagesUses = isSubpackagesUses;
            group = new Vector();
        }
        
        
        public boolean isEmpty () {
            return group.isEmpty();
        }
        
        
        public String toString () {
            return group.toString();
        }

        /**
	 * Add some package <code>name</code> to <code>this</code> group.
	 */
        public void addPackageName(String name) {
            group.addElement(name);
        }

        /**
	 * Check if the given class <code>name</code> belongs to some of
	 * the packages listed by <code>this</code> <b>PackageGroup</b>.
	 * If <code>isSubpackagesUsed</code> policy is set, also check if
	 * that class belongs to some subpackage of some of the packages
	 * listed here.
	 */
        public boolean checkName(String name) {
            for (Enumeration e = group.elements(); e.hasMoreElements();) {
                String pack = (String)e.nextElement();
                if ((name.startsWith(getPackageName(pack)) &&
                     ((name.lastIndexOf('.') <= pack.length()) ||
                      isSubpackagesUses)) || name.equals(pack))
                    return true;
            }
            return false;
        }

        /**
	 * Terminate the given package <code>name</code> with the dot symbol,
	 * if the <code>name</code> is nonempty.
	 */
        private static String getPackageName(String name) {
            return name + ((name.endsWith(".") || name.equals("")) ? "" : ".");
        }
    }
}
