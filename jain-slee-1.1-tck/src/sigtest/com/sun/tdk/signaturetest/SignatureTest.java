/*
 * @(#)SignatureTest.java	1.57 03/11/05
 *
 * Copyright (c) 2003 Sun Microsystems, Inc.  All rights reserved. 
 * SUN PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 * 
 * TDK Signature Test Tool. 
 *
 * This is the signature test.
 *
 * @author Jonathan Gibbons
 * @author Maxim Sokolnikov
 * @author Serguei Ivashin
 */

package com.sun.tdk.signaturetest;

import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.lang.reflect.Constructor;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Vector;


/**
 * <b>SignatureTest</b> is the main class of signature test. <p>
 *
 *  The main purpose of signature test is to ensure that programs written in Java
 *  using the class libraries of one Java implementation of a specific API version
 *  (ie. 1.1, 1.2, etc.) can rely on having the same and only the same Java APIs
 *  available in other implementations of the same API version.  This is a more
 *  stringent requirement than simple binary compatibility as defined in the Java
 *  Language Specification, chapter 13.  It is in essence a two-way binary
 *  compatibility requirement.  Therefore, third party implementations of the Java
 *  API library must retain binary compatibility with the JavaSoft API. Also, the
 *  JavaSoft API implementation must retain binary compatibility with the Java API
 *  library under test.<p>
 *
 *  <b>SignatureTest</b> implements the standard JavaTest 3.0
 *  <code>com.sun.javatest.Test</code> interface and uses the standard
 *  <code>main()</code> method implementation. <b>SignatureTest</b> allows
 *  to check only specified by command line the package or packages. <p>
 *
 *  SignatureTest tracks the following aspects of binary compatibility:
 *  <ul>
 *      <li>Fully qualified name of class or interface
 *      <li>Class modifiers abstract and final
 *      <li>Superclasses and superinterfaces
 *      <li>Public and protected class members
 *  </ul>
 *
 * <b>SignatureTest</b> tracks all of the super classes and all of the super
 * interfaces of each public class and public interface within required
 * packages. <p>
 *
 * <b>SignatureTest</b> tracks all of the public and protected class members
 * for each public class and interface.<p>
 *
 * For each constructor or method tracked, <b>SignatureTest</b> tracks all
 * modifiers except native and synchronized.  It also tracks other
 * attributes for constructors and methods: method name, argument types
 * and order, return type, and the declared throwables.<p>
 *
 * For each field tracked, <b>SignatureTest</b> tracks all modifiers except
 * transient.  It also tracks these other attributes for fields: data
 * type, and field name. <p>
 *
 * Usage: <code>java com.sun.tdk.signaturetest.SignatureTest</code> &lt;options&gt;<p>
 *
 * where &lt;options&gt; includes:
 *
 * <dl>
 *    <dt><code><b>-TestURL</b></code> &lt;URL&gt;
 *         <dd> URL of signature file.
 *
 *    <dt><code><b>-FileName</b></code> &lt;n&gt;
 *         <dd> Path name of signature file name.
 *
 *    <dt><code><b>-Package</b></code> &lt;package&gt;
 *         <dd> Name of the package to be tested.
 *              It is implied, that all subpackages the specified package should also be tested.
 *              Such option should be included for each package (but subpackages), which is
 *              required to be tested.
 *
 *    <dt><code><b>-PackageWithoutSubpackages</b></code> &lt;package&gt;
 *         <dd> Name of the package, which is to be traced itself excluding its subpackages.
 *              Such option should be included for each package required to be traced
 *              excluding subpackages.
 *
 *    <dt><code><b>-Exclude</b></code> &lt;package_or_class_name&gt;
 *         <dd> Name of the package or class, which is not required to be traced,
 *              despite of it is implied by <code>-Package</code> or by
 *              <code>-PackageWithoutSubpackages</code> options.
 *              If the specified parameter names a package, all its subpackages are implied
 *              to be also excluded.
 *              Such option should be included for each package (but subpackages) or class,
 *              which is not required to be traced.
 *
 *    <dt><code><b>-FormatPlain</b></code>
 *         <dd> Do not reorder errors report.
 *
 *    <dt><code><b>-AllPublic</b></code>
 *         <dd> Trace public nested classes, which are member of classes having default scope.
 *
 *    <dt><code><b>-Classpath</b></code> &lt;path&gt;
 *         <dd> Path to packages being tested. If there are several directories and/or zip-files
 *              containing the required packages, all of them should be specified here.
 *              Use <code>java.io.File.pathSeparator</code> to separate directory and/or
 *              zip-file names in the specified path.
 *              Only classes from &lt;path&gt; will be used for tracking adding classes.
 *
 *    <dt><code><b>-static</b></code>
 *         <dd> Run signature test in static mode.
 *
 *    <dt><code><b>-Version</b></code> &lt;version&gt;
 *         <dd> Specify API version. If this parameter is not specified, API version is assumed to
 *              be that reported by <code>getProperty("java.version")</code>.
 *
 *    <dt><code><b>-CheckValue</b><code>
 *         <dd> Check values of primitive constant. This option can be used in static mode only.
 *
 *    <dt><code><b>-Verbose</b><code>
 *         <dd> Enable error diagnostic for inherited class members.
 *
 *    <dt><code><b>-UseReflect</b></code>
 *         <dd> Use <code>java.lang.Class.getDeclaredClasses()</code> method to find nested classes.
 *              <br><i>This is obsoleted option.</i>
 *
 *    <dt><code><b>-Detail</b></code> &lt;detail&gt;
 *         <dd> Specify some detailed option, such as:
 *              <br>&nbsp; <code>NestedStatic</code> - do not check <code>static</code> modifiers
 *              of nested classes,
 *              <br>&nbsp; <code>NestedProtected</code> - do not check <code>protected</code>
 *              modifiers of nested classes.
 *              <br><i>This is obsoleted option.</i>
 * </dl>
 *
 * @see DescriptionFormat
 *
 * @version 03/11/05
 */
public class SignatureTest extends SigTest {

    /**
     * Log-file for error reporting, or <code>null</code> if log is not required.
     */
    protected PrintWriter log;

    /** Log-file is not the System.err */
    protected boolean logFile = false;

    /**
     * When signature test checks API, this table collects names of that
     * classes present in both signature file and in the API being tested.
     */
    protected Hashtable trackedClassNames = new Hashtable();

    /**
     * Specifies details: how to check class members.
     */
    protected DescriptionFormat converter;

    /**
     * Descriptions for all classes found at the specified classpath.
     */
    protected ArchiveFinder archive;
    
    protected ClassEnumerator classIterator;

    protected String pluginClass = null,
                     pluginArg   = null;
    
    /** URL pointing to signature file. */
    protected URL signatureFile;

    /** Enable diagnostics for inherited class members. */
    protected boolean isVerbose = false;

    protected boolean isValueTracked = false;
    
    /** Check mode selected. */
    protected String mode = null;
   
    protected boolean isSupersettingEnabled = false;
    protected boolean isThrowsNormalized = false;
    protected boolean isThrowsRemoved = false;


    /**
     * Run the test using command-line; return status via numeric exit code.
     *
     * @see #run(String[],PrintWriter,PrintWriter)
     * @see #parseParameters(Vector)
     */
    public static void main(String[] args) {
	SignatureTest t = new SignatureTest();
	t.run(args, null, null);
	t.exit();
    }

    /**
     * This is the gate to run the test with the JavaTest application.
     *
     * @param log This log-file is used for error messages.
     * @param ref This reference-file is ignored here.
     *
     * @see #main(String[])
     * @see #parseParameters(Vector)
     */
    public void run(String[] args, PrintWriter pw, PrintWriter ref) {
        log = pw;
        //ref ignored

        // Print help text inly and exit.
        if (args == null) {
            usage();
            notrun(null);
            return;
        }
        if (args.length == 1 && (args[0].equalsIgnoreCase("-help") || args[0].equals("-?"))) {
            usage();
            notrun(null);
            return;
        }

        Vector param = new Vector();
        for (int i = 0; i < args.length; param.addElement(args[i++]));
        
        if (parseParameters(param)) {
            check();
            if (logFile) 
                log.println(toString());
        }

        if (archive != null)
            archive.close();
        
        if (classIterator != null)
            classIterator.close();

        if (logFile) // don't close logfile if it was open in test harness
            log.close();
    }

    /**
     * Parse options specific for <b>SignatureTest</b>, and pass other
     * options to <b>SigTest</b> parameters parser.
     *
     * @param args Same as <code>args[]</code> passes to <code>main()</code>.
     *
     * @see SigTest#parseParameters(Vector)
     */
    protected boolean parseParameters(Vector args) {
        String testURL  = null;
        String fileName = null;
        String logName  = null;
        Vector commonParameters = new Vector();
        String format = null;
        
        // parse parameters
        for (Enumeration e = args.elements(); e.hasMoreElements();) {
            String parameter = (String)e.nextElement();
            
            if (parameter.equalsIgnoreCase("-TestURL")) {
                if (!e.hasMoreElements())
                    return error("Missing value for option -TestURL");
                if (testURL != null)
                    return error("The -TestURL option is specified twice.");
                testURL = (String)e.nextElement();
                
            } else if (parameter.equalsIgnoreCase("-FileName")) {
                if (!e.hasMoreElements())
                    return error("Missing value for option -FileName");
                if (fileName != null)
                    return error("The -FileName option is specified twice.");
                fileName = (String)e.nextElement();
                
            } else if (parameter.equalsIgnoreCase("-plugin")) {
                if (!e.hasMoreElements())
                    return error("Missing value for option -plugin");
                if (pluginClass != null)
                    return error("The -plugin option is specified twice.");
                pluginClass = (String)e.nextElement();
                
            } else if (parameter.equalsIgnoreCase("-PluginArg")) {
                if (!e.hasMoreElements())
                    return error("Missing value for option -PluginArg");
                if (pluginArg != null)
                    return error("The -PluginArg option is specified twice.");
                pluginArg = (String)e.nextElement();
                
            } else if (parameter.equalsIgnoreCase("-FormatPlain")) {
                format = "plain";
                
            } else if (parameter.equalsIgnoreCase("-verbose")) {
                isVerbose = true;
                
            } else if (parameter.equalsIgnoreCase("-CheckValue")) {
                isValueTracked = true;
                
            } else if (parameter.equalsIgnoreCase("-mode")) {
                if (!e.hasMoreElements())
                    return failed("Missing value for option -mode");
                String s = (String)e.nextElement();
                if (!s.equalsIgnoreCase("src") &&
                    !s.equalsIgnoreCase("bin"))
                    return failed("Invalid value for option -mode"); 
                if (mode != null)
                    return failed("The -mode option is specified twice");
                mode = s;
                    
            } else if (parameter.equalsIgnoreCase("-out")) {
                if (!e.hasMoreElements())
                    return error("Missing value for option -out");
                if (logName != null)
                    return error("The -out option is specified twice.");
                logName = (String)e.nextElement();
                
            } else if (parameter.equalsIgnoreCase("-help") || parameter.equals("-?")) {
                usage();
                
            } else if (parameter.equalsIgnoreCase("-EnableSuperSet")) {
                isSupersettingEnabled = true;
                
            } else {
                commonParameters.addElement(parameter);
            }
        }
        
        // parse common parameters
        if (!super.parseParameters(commonParameters)) {
            usage();
            return false;
        }

        if (isValueTracked && !isStatic)
            return error("The values could be checked in the static mode only.");

        if (pluginClass == null && pluginArg != null)
            return error("-PluginArg specified without -plugin");

        if (log == null) {
            if (logName == null)
                log = new PrintWriter(new OutputStreamWriter(System.err), true);
            else {
                try {
                    log = new PrintWriter(new BufferedWriter(new FileWriter(logName)));
                    logFile = true;
                } catch (IOException x) {
                    if (SigTest.debug)
                        x.printStackTrace();
                    return error("Invalid -out file name");
                }
            }
        }
        else {
            if (logName != null)
                System.err.println("-out option ignored");
        }

        // creates ErrorFormatter.
        if ((format != null) && format.equals("plain"))
            errorManager =  new ErrorFormatter(log);
        else
            errorManager =  new SortedErrorFormatter(log, isVerbose);
        
        //construct URL and ArchiveFinder
        if (testURL == null)
            testURL = "file:";
        else {
            int i1 = testURL.indexOf(':'),
                i2 = testURL.indexOf('/');
            if (i1 == -1 || i2 == -1 || i1 > i2)
                testURL = "file:" + testURL;
        }

        return constructFields(testURL, fileName);
    }


    /**
     *  Prints help text.
     */
    protected void usage () {
        System.err.println(
            "Available options are:"
         +  "\n-TestURL  <url>   Specify URL of signature file"
         +  "\n-FileName <file>  Specify signature file name"
         +  "\n-package <name>   Specify package to be tested along with subpackages"
         +  "\n-PackageWithoutSubpackages <name> Specify package to be tested excluding subpackages"
         +  "\n-exclude <name>   Specify package or class, which is not required to be tested"
         +  "\n-classpath <path> Specify search path for tested classes"
         +  "\n-out <file>       Specify report file name"
         +  "\n-static           Run SignatureTest in static mode"
	 +  "\n-apiVersion <str> Set API version for report"
	 +  "\n-plugin <class>   Load plugin (specified by its full class name)"
	 +  "\n-PluginArg <str>  Specify parameter for plugin"
	 +  "\n-AllPublic        Test public/protected nested classes, which are members of default scope classes"
	 +  "\n-CheckValue       Check values of primitive constants (static mode only)"
	 +  "\n-mode [src|bin]   Select check mode" 
	 +  "\n-ClassCacheSize <numb>  Specify size of class cache (default value is 100)"
	 +  "\n-FormatPlain      Do not sort error messages"
	 +  "\n-verbose          Enable error diagnostics for inherited class members"   
	 +  "\n-debug            Enable debug mode (prints stack trace)"
	 +  "\n-help             Print this text"
	 +  "\nAll options are case-insensitive." 
        );
    }


    /**
     *  creates signatureFile and classIterator
     *  @param testURL test URL specified by command line.
     *  @param fileName signature file name specified by  command line.
     */
    protected boolean constructFields(String testURL, String fileName) {
        // check arguments
        if (testURL == null)
            return error("URL not specified");
        if (fileName == null)
            return error("Signature file name not specified");

        // Construct file name
        try {
            signatureFile = new URL(new URL(testURL), fileName);
        } catch (MalformedURLException e) {
            if (SigTest.debug)
                e.printStackTrace();
            log.println(testURL + "  throw " + e);
            return error("Invalid URL");
        } catch (IOException er) {
            if (SigTest.debug)
                er.printStackTrace();
            log.println(er);
            return error("Can't read specification file.");
        }
        
        // create ArchiveFinder for founding of the added classes
        try {
            archive = new ArchiveFinder(classpath);
        } catch (SecurityException e) {
            if (SigTest.debug)
                e.printStackTrace();
            log.println("The security constraint does not allow tracking " +
                        "class path for tracking of the added new classes.");
        }
        
        if (pluginClass == null ) {
            classIterator = archive;
        }
        else {
            Class c;
            try {
                c = Class.forName(pluginClass);
            }
            catch (ClassNotFoundException e) {
                if (SigTest.debug)
                    e.printStackTrace();
                log.println(e);
                return error("Plugin not found: " + pluginClass);
            }
            catch (Throwable e) {
                if (SigTest.debug)
                    e.printStackTrace();
                log.println(e);
                return error("Failed to load plugin " + pluginClass);
            }

            Object o;            
            try {
                o = c.newInstance();
            } 
            catch (Throwable e) {
                if (SigTest.debug)
                    e.printStackTrace();
                log.println(e);
                return error("Failed to instantiate plugin " + pluginClass);
            }
            
            if (o instanceof ClassEnumerator) {
                classIterator = (ClassEnumerator)o;
                try {
                    classIterator.init(pluginArg);
                }
                catch (IOException e) {
                    if (SigTest.debug)
                        e.printStackTrace();
                    classIterator = null;
                    log.println(e);
                    return error("Failed to initialize plugin " + pluginClass);
                }
                catch (Error e) {
                    if (SigTest.debug)
                        e.printStackTrace();
                    classIterator = null;
                    log.println(e);
                    return error("Failed to initialize plugin " + pluginClass);
                }
            }
            
            else if (o instanceof PathEntry) {
                if (!isStatic)
                    return error("PathEntry plugin works in static mode only");
            
                PathEntry pe = (PathEntry)o;
                try {
                    pe.init(pluginArg,null);
                }
                catch (IOException e) {
                    if (SigTest.debug)
                        e.printStackTrace();
                    log.println(e);
                    return error("Failed to initialize plugin " + pluginClass);
                }
                catch (Error e) {
                    if (SigTest.debug)
                        e.printStackTrace();
                    log.println(e);
                    return error("Failed to initialize plugin " + pluginClass);
                }
                archive.insert(pe);
            }
            
            else {
                return error("Plugin of unknown type");
            }
        }
        
        if (isStatic && archive.isEmpty())
            return error("Class path not specified or incorrect in the static mode.");
        
        return passed();
    }

    /**
     * Do run signature test provided its arguments are successfully parsed.
     *
     * @see #parseParameters(Vector)
     */
    protected boolean check() {
    
        String msg = null;
    
    //  Open the specified sigfile and read standard headers.
    
        SignatureClassLoader in = new SignatureClassLoader();
	try {
	    if (!in.open(signatureFile))
                msg = "Invalid signature file: " + signatureFile;
        }
        catch (IOException e) {
            if (SigTest.debug)
                e.printStackTrace();
            msg = "Problem with signature file\n" + e;
        }
        catch (SecurityException e) {
            if (SigTest.debug)
                e.printStackTrace();
	    msg = "Security constraints does not allow to read from signature file\n" + e;
	} 
        
        if (msg != null) {
            in.close();
            log.println(msg);
            return error(msg);
        }
        
    //  Check the -mode option and sigfile tags.
    //
        msg = "";
    
        if (mode == null)
            mode = "src";
            
        if (in.isThrowsNormalized && in.isThrowsRemoved)
            msg += "\nCorrupted sigfile";
            
        if (mode.equals("src")) {
            isThrowsNormalized = in.isThrowsNormalized;
            isThrowsRemoved    = false;
            if (in.isThrowsRemoved)
                msg += "\nBinary sigfile specified in src mode";
        }
    
        if (mode.equals("bin")) {
            isThrowsNormalized = false;
            isThrowsRemoved    = true;
        }
        
        if (msg.length() != 0) {
            in.close();
            log.println(msg);
            return error(msg);
        }
        
        log.println("SignatureTest report");
        log.println("Tested version: " + apiVersion);
        log.println("Check mode: " + mode + (isThrowsNormalized ? " [throws normalized]" : ""));
        
        log.println();
        
	trackedClassNames = new Hashtable();
        converter = createDescriptionFormat();
        loader = createClassDescrLoader();
        
    //  Reading the sigfile: main loop.

        msg = null;
        
	try {
	    ClassDescription currentClass;
	    while ((currentClass = in.nextClass()) != null) {
                if (currentClass.isPublic() || currentClass.isProtected()) {
                    verifyClass(currentClass);
                }
	    }
	} 
        catch (IOException e) {
            if (SigTest.debug)
                e.printStackTrace();
            msg = "Problem with signature file\n" + e;
        }
        catch (SecurityException e) {
            if (SigTest.debug)
                e.printStackTrace();
	    msg = "Security constraints does not allow to read from signature file\n" + e;
	} 
        catch (Error e) {
            if (SigTest.debug)
                e.printStackTrace();
            msg = "Error in signature file: " + signatureFile;
        }
        
        if (msg != null) {
            in.close();
            log.println(msg);
            return error(msg);
        }
        
        in.close();
        
    //  Finished - the sigfile closed.

        if (!isSupersettingEnabled)
            checkAddedClasses();
        
	errorManager.printErrors();
        log.println("");
        
        int errors = errorManager.getNumErrors();
	if (errors == 0)
	    return passed();
	else
	    return failed(errors + " errors");
    }

    /**
     * Check if packages being tested do not contain any extra class,
     * which is not described in the <code>signatureFile</code>.
     * For each extra class detected, error message is appended to
     * the <code>log</code>.
     *
     * @see #signatureFile
     * @see #log
     */
    protected void checkAddedClasses() {
	//check that new classes are not added to the tracked packages.
	try {
	    String name;
            if (classIterator == null)
                return;
	    while ((name = classIterator.nextClassName()) != null) {
		// Check that class isn't tracked and this class is
                // accessible in the current tested mode
		if ((trackedClassNames.get(name) == null) &&
                    isPackageMember(name)) {
                    ClassDescription c = null;
		    try {
                        c = loader.loadClass(name);
                        if (isAccessible(c))
                            errorManager.addError("Added", c.getName(), null, "");
		    } catch (ClassNotFoundException ex) {
                        if (SigTest.debug)
                            ex.printStackTrace();
		    } catch (LinkageError ex1) {
                        if (SigTest.debug)
                            ex1.printStackTrace();
		    }
                }
            }
	} catch (SecurityException ex) {
            if (SigTest.debug)
                ex.printStackTrace();
	    log.println("The security constraints does not allow to inspect" +
			" class path.");
	    log.println(ex);
        }
    }

    /**
     * Check if the <code>required</code> class described in signature file
     * also presents (and is public or protected) in the API being tested.
     * If this method fails to find that class in the API being tested,
     * it appends corresponding message to the errors <code>log</code>.
     *
     * @return <code>Status.failed("...")</code> if security exception
     *     occurred; or <code>Status.passed("")</code> otherwise.
     *
     * @see #log
     */
    protected boolean verifyClass(ClassDescription required) {
        // checks that package from tested API
        if (!isPackageMember(required.getName()))
            return passed();
        String name = required.getName();
        try {
            ClassDescription found = loader.loadClass(name);
            if (found.isPublic() || found.isProtected()) {
                loader.createMembers(found);
                
                if (isThrowsNormalized) {
                    loader.normThrows(found);
                }
                
                if (isThrowsRemoved) {
                    ClassDescrLoader.removeThrows(required);
                    ClassDescrLoader.removeThrows(found);
                }
                
                verifyClass(required, found);
            } else
                errorManager.addError("Missing", name , null, "");
        } catch (ClassNotFoundException ex) {
            if (SigTest.debug)
                ex.printStackTrace();
            errorManager.addError("Missing", name , null, "");
        } catch (LinkageError er) {
            if (SigTest.debug)
                er.printStackTrace();
            errorManager.addError("LinkageError", name ,
                                  " thrown " + er, " " + name +
                                  " not linked");
            trackedClassNames.put(name, "non-linked");
        }
        return passed();
    }

    /**
     * Compare descriptions of the <code>required</code> and <code>found</code> classes.
     * It is assumed, that description for the <code>required</code> class is read
     * from signature file, and the <code>found</code> description belongs to that
     * API being tested. If the descriptions compared are not equal to each other
     * (class names differ, or there are different sets of public members in them),
     * this method appends corresponding error messages to the <code>log</code>-file.
     * Note, that equality of class or member names may do not imply that they have
     * the same <code>static</code> and <code>protected</code> attributes, and
     * <code>throws</code> clause, if the chosen <code>converter</code> enables
     * weaker equivalence.
     *
     * @see #converter
     * @see #log
     */
    protected void verifyClass(ClassDescription required,
                               ClassDescription found) {

	// adds class name to the table of the tracked classes.
        trackedClassNames.put(found.getName(), "tracked");
        
        if (errorManager instanceof SortedErrorFormatter)
            ((SortedErrorFormatter)errorManager).Tested(found);

        // track class modifiers
        checkClassDescription(required, found);

        // track members declared in the signature file.
	for (Enumeration e = required.items(); e.hasMoreElements();) {
	    ItemDescription key = (ItemDescription)e.nextElement();
            if (key.isClass())
                trackNestedClass(found, key, required.get(key));
            else
                trackMember(required.getName(), required.get(key),
                            found.get(key));
	}

        // track members which are added in the current implementation.
	for (Enumeration e = found.items(); e.hasMoreElements();) {
	    ItemDescription key = (ItemDescription)e.nextElement();
	    if (required.get(key) == null)
		trackMember(required.getName(), null, found.get(key));
	}
    }

    /**
     * Compare names of the <code>required</code> and <code>found</code> classes.
     * It is assumed, that description for the <code>required</code> class is read
     * from signature file, and the <code>found</code> description belongs to that
     * API being tested. If the descriptions compared are not equal to each other,
     * this method appends corresponding error messages to the <code>log</code>-file.
     * Note, that equality of descriptions may do not imply that they have the same
     * <code>static</code> and <code>protected</code> attributes, if the chosen
     * <code>converter</code> enables weaker equivalence.
     *
     * @see #converter
     * @see #log
     */
    protected void checkClassDescription(FullItemDescription required,
                                         FullItemDescription found) {

        String requiredDef = converter.convertShortClassDescription(required);
        String foundDef = converter.convertShortClassDescription(found);
        if (!requiredDef.equals(foundDef)) {
            errorManager.addError("Missing", required.getName(), requiredDef, "");
            errorManager.addError("Added", found.getName(), foundDef, "");
        }
    }

    /**
     * Compare the <code>required</code> and <code>found</code> sets of class members
     * having the same signature <code>name</code>. It is assumed, that vector of the
     * <code>required</code> descriptions is read from signature file, and vector of
     * the <code>found</code> descriptions belongs to that API being tested. If these
     * two sets of member descriptions are not equal to each other, this method appends
     * corresponding error messages to the <code>log</code>-file. Note, that equality
     * of member descriptions may do not imply that that descriptions have the same
     * <code>static</code> and <code>protected</code> attributes, and <code>throws</code>
     * clause, if the chosen <code>converter</code> enables weaker equivalence.
     *
     * @see #converter
     * @see #log
     */
    protected void trackMember(String name, Vector required, Vector found) {
        required = (required == null) ? new Vector() : required;
        found = (found == null) ? new Vector() : found;
	Vector req = convertDescription(required);
	Vector fou = convertDescription(found);

        if (mode.equals("bin")) {
       	    for (int i = 0; i < req.size()-1; i++) 
        	for (int pos; (pos = req.indexOf(req.elementAt(i), i+1)) > i;) 
	            req.removeElementAt(pos);
                
            for (int i = 0; i < fou.size()-1; i++) 
	        for (int pos; (pos = fou.indexOf(fou.elementAt(i), i+1)) > i;) 
		    fou.removeElementAt(pos);
        }
            
	for (int i = 0; i < req.size();) {
	    int pos = fou.indexOf(req.elementAt(i));
	    if (pos >= 0) {
		req.removeElementAt(i);
		fou.removeElementAt(pos);
	    } else
                i++;
	}

	for (int i = 0; i < req.size(); i++) {
            SigEntry def = (SigEntry)req.elementAt(i);
            errorManager.addError("Missing", name, def.value, "");
        }

        if (!isSupersettingEnabled)
	    for (int i = 0; i < fou.size(); i++) {
                SigEntry def = (SigEntry)fou.elementAt(i);
                errorManager.addError("Added", name, def.value, "");
            }

    }

    /**
     * Given the vector <code>h</code> of <b>FullItemDescription</b>s,
     * return vector of corresponding <b>SigEntry</b> instances.
     *
     * @see FullItemDescription
     * @see SigEntry
     */
    protected Vector convertDescription(Vector h) {
        Vector retVal = new Vector();
        if (h == null)
            return retVal;
        for (Enumeration e = h.elements(); e.hasMoreElements();
             retVal.addElement(new SigEntry((FullItemDescription)e.nextElement())));
        return retVal;
    }

    /**
     * track Vector of the nested classes with the same local name in the maintenance
     * mode.
     * @param enclousClass enclosing class in the tested implementation.
     * @param key local name of the checked nested classes
     * @param required definition of the based implementation.
     */
    protected void trackNestedClass(ClassDescription enclousClass,
                                    ItemDescription key, Vector required) {
        trackMember(enclousClass.getName(), required,
                    enclousClass.get(key));
    }

    /**
     * Return new <b>DescriptionFormat</b> taking into account <code>this</code>
     * instance <code>details</code> field and the parameter
     * <code>isQualifiedName</code> assigned to <code>false</code>.
     *
     * @see #details
     * @see DescriptionFormat#isQualifiedName
     */
    protected DescriptionFormat createDescriptionFormat() {
        return new DescriptionFormat(false, details){
            protected boolean isModifierTracked(String modifier, FullItemDescription item) {
                if (modifier.startsWith("<value"))
                    return isValueTracked;
                else
                    return super.isModifierTracked(modifier, item);
            }
        };
    }

    /**
     * Load either static (<b>ArchiveClassDescrLoader</b>) or reflection-based
     * (<b>ReflClassDescrLoader</b>) class descriptions loader.
     *
     * @see SigTest#isStatic
     */
    protected ClassDescrLoader createClassDescrLoader() {
        if (!isStatic)
            return new ReflClassDescrLoader();
        try {
            Class c = Class.forName("com.sun.tdk.signaturetest.ArchiveClassDescrLoader");
            Constructor ctor = c.getConstructor(new Class[] {
                archive.getClass(), Integer.TYPE });
            return (ClassDescrLoader)ctor.newInstance(new Object[] {
                archive, new Integer(cacheSize) });
        } catch (Throwable t) {
            if (SigTest.debug)
                t.printStackTrace();
            throw new LinkageError("Can't create static ClassDescrLoader");
        }
    }

    /**
     * This class represents signature entry.
     */
    class SigEntry {
        FullItemDescription entry;
        String value;
        //int status;

	/**
	 * Initiate <code>entry</code> and <code>value</code> fields.
	 */
        SigEntry(FullItemDescription entry) {
            this.entry = entry;
            value = converter.convertShortDescription(entry);
        }

	/**
	 * Compare <code>value</code> fields, if <code>o</code> is an
	 * instance of <b>SigEntry</b>.
	 */
        public boolean equals(Object o) {
            if (o instanceof SigEntry)
                return value.equals(((SigEntry)o).value);
            else
                return false;
        }
    }
}

