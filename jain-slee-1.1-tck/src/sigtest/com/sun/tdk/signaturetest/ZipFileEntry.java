/*
 * @(#)ZipFileEntry.java	1.6 03/08/22
 *
 * Copyright (c) 2003 Sun Microsystems, Inc.  All rights reserved. 
 * SUN PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *
 * TDK Signature Test Tool. 
 *
 * @author Maxim Sokolnikov
 */

package com.sun.tdk.signaturetest;

import java.io.IOException;
import java.io.InputStream;
import java.util.Enumeration;
import java.util.Vector;
import java.util.zip.ZipFile;
import java.util.zip.ZipEntry;


class ZipFileEntry implements PathEntry {
    /** Specified zip file. **/
    ZipFile zipfile;
    /** Enumeration of the classes placed in this zip file. */
    Enumeration entries;
    /** Entry representing last founded class. */
    ZipEntry currentEntry;

    /** 
     * Creates new <b>ZipEntry</b> for the given zip or jar file.
     *
     * @param zipfile Qualified file name.
     *
     * @see ZipFile
     **/
    public ZipFileEntry(String zipfile) throws IOException {
        init(zipfile, null);
    }


    public void init(String zipfile, Vector errs) throws IOException {
        this.zipfile = new ZipFile(zipfile);
        entries = this.zipfile.entries();
        currentEntry = null;
    }
    

    /**
     * Closes zip/jar file.
     * 
     */    
    public void close () {
        if (zipfile != null) {
            try {
                zipfile.close();
            } 
            catch (IOException e) {
                if (SigTest.debug)
                    e.printStackTrace();
            }
            zipfile = null;
            entries = null;
            currentEntry = null;
        }
    }
    
    
    /** 
     * Search next class in the enumeration of classes found inside 
     * <code>this</code> zip-file. After some class name is found by 
     * <code>nextClassName()</code>, that class becomes <i>current</i> in 
     * classes enumeration, and you may access to bytecode for that class 
     * with <code>getCurrentClass()</code> method. You may also invoke 
     * <code>setListToBegin()</code> to restore classes enumeration.
     *
     * @return Class qualified name, or <code>null</code> if there is no 
     *     next class found.
     *
     * @see #setListToBegin()
     * @see #getCurrentClass()
     * @see #findClass(String)
     *
     * @see PathEntry
     * @see DirectoryEntry
     */
    public String nextClassName() {
        String name = "";
        while (!name.endsWith(".class") && entries.hasMoreElements()) {
            currentEntry = (ZipEntry)entries.nextElement();
            name = currentEntry.getName();
        }
        if (!name.endsWith(".class")) 
            return null;
        else
            return name.substring(0, name.length() - 6).replace('/', '.');
    }

    /** 
     * Open an <b>InputStream</b> for a class, which became <i>current</i> 
     * after <code>nextClassName()</code> method invocation have found its 
     * name. Note, that there is no current class defined until you call 
     * <code>nextClassName()</code>.
     *
     * However, invocation of <code>findClass(name)</code> directly provides
     * bytecode for the named class, if it could be found in <code>this</code> 
     * <b>ZipFileEntry</b>.
     *
     * @return <b>InputStream</b> instance providing bytecode for the current 
     *     class.
     *
     * @see #nextClassName()
     * @see #setListToBegin()
     * @see #findClass(String)
     *
     * @see java.io.InputStream
     */
    public InputStream getCurrentClass() throws IOException {
        if ((currentEntry == null) || (currentEntry.getName() == null) ||
            !currentEntry.getName().endsWith(".class"))
            throw new IOException("The current class not exist.");
        return zipfile.getInputStream(currentEntry);
    }

    /**
     * Reset enumeration of classes found in <code>this</code> 
     * <b>ZipFileEntry</b>. Bytecode for those classes is available with 
     * the methods <code>nextClassName()</code> and <code>getCurrentClass()</code>, 
     * or <code>findClass(name)</code>.
     *
     * @see #nextClassName()
     * @see #getCurrentClass()
     * @see #findClass(String)
     */
    public void setListToBegin() {
        entries = this.zipfile.entries();
        currentEntry = null;
    }

    /** 
     * Returns <b>FileInputStream</b> providing bytecode for the required class,
     * if that class could be found by the given qualified name in <code>this</code>
     * <b>ZipFileEntry</b>.
     *
     * @param name Qualified name of the class requested.
     *
     * @exception ClassNotFoundException File "[package]/[name].<code>class</code>" 
     *     not found inside <code>this</code> <b>ZipFileEntry</b>.
     *
     * @see java.io.FileInputStream
     */
    public InputStream findClass(String name)
        throws IOException, ClassNotFoundException {
        ZipEntry zipEntry = zipfile.getEntry(name.replace('.', '/') + ".class");
        if (zipEntry == null)
            throw new ClassNotFoundException();
        else
            return zipfile.getInputStream(zipEntry);
    }
}
