/*
 * @(#)ArchiveFinder.java	1.25 03/08/22
 *
 * Copyright (c) 2003 Sun Microsystems, Inc.  All rights reserved. 
 * SUN PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *
 * TDK Signature Test Tool. 
 *
 * @author Maxim Sokolnikov
 */

package com.sun.tdk.signaturetest;

import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.System;
import java.lang.String;
import java.util.Enumeration;
import java.util.Vector;


/** 
 * <b>ArchiveFinder</b> provides access to all classes placed inside 
 * directories and/or zip-files listed in the classpath, which is given 
 * to the constructor for new <b>ArchiveFinder</b> instance.
 * 
 * <p>The constructor searches every directory or zip-file listed and keeps 
 * corresponding <b>PathEntry</b> element, which can provide access to a bytecode 
 * for each class found inside the directory or zip-file. All classes found inside
 * the listed directories and zip-files are virtually enumerated in the same 
 * order as they are found. The methods <code>getCurrentClass()</code>, 
 * <code>nextClassName()</code>, and <code>setListToBegin()</code> provide 
 * access to this classes enumeration. 
 *
 * <p>Also, the method <code>findClass(name)</code> provides access to class 
 * directly by its qualified name. Note however, that the names class must belong 
 * to some directory or zip-file pointed to the <b>ArchiveFinder</b> instance.
 *
 * @see PathEntry
 *
 * @version 03/08/22
 */
public class ArchiveFinder implements ClassEnumerator {

    /** 
     * Reference to the class implementing <b>DirectoryEntry</b>.
     *
     * @see DirectoryEntry
     * @see PathEntry
     */
    private static final String DIRECTORY_NAME =
        "com.sun.tdk.signaturetest.DirectoryEntry";
    /** 
     * Reference to the class implementing <b>ZipFileEntry</b>.
     *
     * @see ZipFileEntry
     * @see PathEntry
     */
    private static final String ZIP_NAME =
        "com.sun.tdk.signaturetest.ZipFileEntry";
	
    /** 
     * Collector for errors and warnings occurring while <b>ArchiveFinder</b> 
     * constructor searches archives of classes.
     */
    private Vector errors;

    /** 
     * Number of ignorable entries found in the path given to <b>ArchiveFinder</b>
     * constructor.
     */
    private int sizeIgnorables;
    
    /** 
     * List of <b>PathEntry</b> instances referring to directories and zip-files
     * found by <b>ArchiveFinder</b> constructor.
     *
     * @see PathEntry
     * @see DirectoryEntry
     * @see ZipFileEntry
     */
    private Vector entries;

    /** 
     * Enumeration of entries used to organize enumeration of classes
     * found by this <b>ArchiveFinder</b> instance.
     *
     * @see #getCurrentClass()
     * @see #nextClassName()
     * @see #setListToBegin()
     */
    private Enumeration elements;

    /**
     * <i>Current</i> directory or zip-file entry, containing <i>current</i> 
     * class. This field is used to organize transparent enumeration of all
     * classes found by this <b>ArchiveFinder</b> instance.
     *
     * @see #getCurrentClass()
     * @see #nextClassName()
     * @see #setListToBegin()
     */
    private PathEntry currentEntry;

    /** 
     * Path separator used by operating system.
     * Note, that <code>pathSeparator</code> is uniquely determined 
     * when JVM starts.
     */
    private static String pathSeparator;
    
    /**
     * Try to determine path separator used by operating system.
     * Path separator is found in <code>java.io.File</code>, or by
     * <code>System.getProperty()</code> invocation.
     *
     * @see #pathSeparator
     *
     * @see java.io.File#pathSeparator
     * @see System#getProperty(String)
     */
    static {
        try {
            // java.io.File is optional class and could be not implemented.
            Class c = Class.forName("java.io.File");
            Field f = c.getField("pathSeparator");
            pathSeparator = (String)f.get(null);
        } catch (Throwable t) {
            try {
                pathSeparator = System.getProperty("path.separator");
            } catch (SecurityException e) {
                if (SigTest.debug)
                    e.printStackTrace();
            } 
        }
    }

    /**
     * This constructor finds all classes within the given classpath, 
     * and creates a list of <b>PathEntry</b> elements - one element per 
     * each directory or zip-file found. Classes found inside the listed 
     * directories and zip files become available through the created 
     * <b>ArchiveFinder</b> instance.
     *
     * @param classPath Path string listing directories and/or zip files.
     * @exception SecurityException The <code>classPath</code> string has 
     *     invalid format.
     *
     * @see #findClass(String)
     * @see #getCurrentClass()
     * @see #nextClassName()
     * @see #setListToBegin()
     *
     * @see #createPathEntry(String)
     * @see PathEntry
     */
    public ArchiveFinder(String cp) {
        init(cp);
    }
    
    
    public void init(String classPath) {
        entries = new Vector();
	errors = new Vector();
        String path = (classPath == null) ? "" : classPath;
        if ((path != null) && !path.equals("") && (pathSeparator == null))
            throw new SecurityException("Can't define path separator");

        Vector pathEntries = new Vector();
	//creates Hashtable with ZipFiles and directories from path.
	while (path != null && path.length() > 0) {
	    String s;
	    int index = path.indexOf(pathSeparator);
	    if (index < 0) {
		s = path;
		path = null;
	    } else {
		s = path.substring(0, index);
		path = path.substring(index + pathSeparator.length());
	    }

            PathEntry entry = createPathEntry(s);
            if (entry != null)
                entries.addElement(entry);
	}
        elements = entries.elements();
    }
    
    
    
    public void close() {
        if (entries != null) {
            for (Enumeration e = entries.elements(); e.hasMoreElements();) 
                ((PathEntry)e.nextElement()).close();
            
            entries = null;
            elements = null;
        }
    }
    
    
    void insert(PathEntry pe) {
        entries.insertElementAt(pe, 0);
        elements = entries.elements();
    }
    
    
    boolean isEmpty () {
        return entries.isEmpty();
    }


    /**
     * Report about all errors occurred while construction of ArchiveFinder.
     *
     * @param PrintWriter Where to println error messages.
     */
    public void printErrors(PrintWriter out) {
        if (out != null)
            for (int i = 0; i < errors.size(); i++)
                out.println((String)errors.elementAt(i));
    }

    /**
     * Return number of significand errors occurred when <b>ArchiveFinder</b>
     * constructor was being working. Ignorable path entries are not 
     * taken into account here.
     *
     * @see #createPathEntry(String)
     */
    public int getNumErrors() {
        return errors.size() - sizeIgnorables;
    }

    /**
     * Reset list of directories and/or zip-files found by <b>ArchiveFinder</b>.
     * This also resets transparent enumeration of classes found inside those 
     * directories and zip-files, which are available with the methods
     * <code>nextClassName()</code>, <code>getCurrentClass()</code>, or 
     * <code>findClass(name)</code>.
     *
     * @see #nextClassName()
     * @see #getCurrentClass()
     * @see #findClass(String)
     */
    public void setListToBegin() {
        elements = entries.elements();
        currentEntry = null;
    }

    /** 
     * Search next class in the enumeration of classes found inside 
     * directories and zip-files pointed to <code>this</code> 
     * <b>ArchiveFinder</b> instance. After some class name is returned 
     * by <code>nextClassName()</code>, that class becomes 'current' in 
     * classes enumeration, and you may access to bytecode for that class 
     * with <code>getCurrentClass()</code> method. You may invoke 
     * <code>setListToBegin()</code> method to restore classes
     * enumeration to itsstarting point.
     *
     * @return Class qualified name, or null if there is no next 
     *     class found.
     *
     * @see #setListToBegin()
     * @see #getCurrentClass()
     * @see #findClass(String)
     */
    public String nextClassName() {
        String name = null;
        for (;((currentEntry == null) ||
               ((name = currentEntry.nextClassName()) == null)) &&
                 elements.hasMoreElements();) {
            currentEntry = (PathEntry)elements.nextElement();
            currentEntry.setListToBegin();
        }
        return name;
    }

    /** 
     * Open an <b>InputStream</b> instance for the class, which became 
     * <i>current</i> after the <code>nextClassName()</code> method have 
     * found its name. Note, that there is no current class defined until 
     * you call <code>nextClassName()</code>.
     *
     * <p>However, <code>findClass(name)</code> method provides direct way 
     * to access to bytecode for the named class, if that class is disposed
     * inside some directory or zip file found by <code>this</code> 
     * <b>ArchiveFinder</b> instance.
     *
     * @return <b>InputStream</b> instance providing bytecode for the current 
     *     class.
     *
     * @see #nextClassName()
     * @see #setListToBegin()
     * @see #findClass(String)
     *
     * @see java.io.InputStream
     */
    public InputStream getCurrentClass() throws IOException {
        if (currentEntry == null)
            return null;
        else
            return currentEntry.getCurrentClass();
    }

    /** 
     * Returns <b>FileInputStream</b> instance providing bytecode for the 
     * required class. The class must be found by the given qualified name 
     * inside some of <b>PathEntry</b> elements listed by <code>this</code> 
     * <b>ArchiveFinder</b> instance.
     *
     * @param name Qualified name of the class requested.
     * @exception ClassNotFoundException Not found in any <b>PathEntry</b>
     *     in <code>this</code> <b>ArchiveFinder</b> instance.
     *
     * @see PathEntry
     * @see PathEntry#findClass(String)
     *
     * @see java.io.FileInputStream
     */
    public InputStream findClass(String name)
        throws IOException, ClassNotFoundException {
        for (Enumeration e = entries.elements(); e.hasMoreElements();) {
            try {
                return ((PathEntry)e.nextElement()).findClass(name);
            } catch (ClassNotFoundException exc) {
            }
        }
        throw new ClassNotFoundException(name);
    }

    /**
     * Check if the given name is directory or zip-file name,
     * and create either new <b>DirectoryEntry</b> or new 
     * <b>ZipFileEntry</b> instance correspondingly.
     *
     * @param name Qualified name of some directory or zip file.
     * @return New <b>PathEntry</b> instance corresponding to 
     *     the given <code>name</code>.
     *
     * @see PathEntry
     */
    protected PathEntry createPathEntry(String name) {
        // try to create directory
        Throwable t;
        try {
            Class c = Class.forName(DIRECTORY_NAME);
            Constructor ctor = c.getConstructor(new Class[] { String.class,
                                                              Vector.class});
            return (PathEntry)ctor.newInstance(new Object[] { name, errors });
        } catch (InvocationTargetException e) {
            t = e.getTargetException();
        } catch (Throwable th) {
            t = th;
        }
        // try to create ZipFile entry
        try {
            Class c = Class.forName(ZIP_NAME);
            Constructor ctor = c.getConstructor(new Class[] { String.class });
            return (PathEntry)ctor.newInstance(new Object[] { name });
        } catch (InvocationTargetException e) {
            errors.addElement("Ignoring " + name + ": " + t + " : " +
                              e.getTargetException());
            sizeIgnorables++;
        } catch (Throwable th) {
            errors.addElement("Ignoring " + name + ": " + t + " : " + th);
            sizeIgnorables++;
        }
        return null;
    }
}

