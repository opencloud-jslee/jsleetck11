/*
 * @(#)FullItemDescription.java	1.21 03/10/29
 *
 * Copyright (c) 2003 Sun Microsystems, Inc.  All rights reserved. 
 * SUN PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *
 * TDK Signature Test Tool. 
 *
 * @author Maxim Sokolnikov
 */

package com.sun.tdk.signaturetest;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.Enumeration;
import java.util.StringTokenizer;
import java.util.Vector;

/**
 * <b>FullItemDescription</b> describes signature of class member, such as
 * field, or method, or constructor, or nested class or interface. It contains 
 * all modifiers, type for field or returned type for method, name, types of
 * method's or constructor's arguments, and declared exceptions. It does not keep
 * any ``<i><b>extends</b></i> ...'' nor ``<i><b>implements</b></i> ...''
 * information for nested class. However, <b>FullItemDescription</b> instance 
 * itself may be marked with <code>SUPER</code> or <code>INTERFACE</code> label
 * to describe name of superclass or interface. Information provided by 
 * <b>FullItemDescription</code> is used by <b>SignatureTest</b> and 
 * <b>APIChangesTest</b>. 
 *
 * @see DescriptionFormat
 *
 * @version 03/10/29
 */
public class FullItemDescription extends ItemDescription {
    
    /** Code for private access modifier. */
    public static final int PRIVATE   = 0;
    
    /** Code for default access modifier. */
    public static final int DEFAULT   = 1;
    
    /** Code for protected access modifier. */
    public static final int PROTECTED = 2;
    
    /** Code for public access modifier. */
    public static final int PUBLIC    = 3;

    /** True, if <code>this</code> description is Synthetic. */
    public boolean isSynthetic = false;
    
    /**
     * <b>String</b> names of all modifiers assigned to <code>this</code> item.
     *
     * @see #printModifiers(int)
     */
    public Vector modifiers = new Vector();

    /**
     * Field type, if <code>this</code> item describes some field, or
     * return type, if <code>this</code> item describes some methods.
     */
    public String type;

    /**
     * Qualified name of the class or interface, where 
     * <code>this</code> item is declared.
     */
    public String declaringClass;

    /**
     * If <code>this</code> item describes some method or constructor, 
     * <code>args</code> lists types of its arguments. Type names are 
     * separated by commas, and the whole <code>args</code> list is
     * embraced inside matching parentheses.
     */
    public String args;

    /**
     * Contains <i><b>throws</b></i> clause, if <code>this</code> item
     * describes some method or constructor.
     */
    public String throwables = "";

    /** the predecessor of the current analyzed tokens **/
    private String last;

    private boolean isThrowables;

    /**
     * Create description for the given <code>field</code>.
     *
     * @see ItemDescription#memberType
     * @see ItemDescription#name
     * @see #modifiers
     * @see #type
     * @see #declaringClass
     */
    public FullItemDescription(Field field) {
        memberType = FIELD;
        modifiers = printModifiers(field.getModifiers());
        type = field.getType().getName();
        declaringClass = field.getDeclaringClass().getName();
        name = field.getName();
    }
	
    /**
     * Create description for the given method.
     *
     * @see ItemDescription#memberType
     * @see ItemDescription#name
     * @see #modifiers
     * @see #type
     * @see #args
     * @see #throwables
     * @see #declaringClass
     */
    public FullItemDescription(Method meth) {
        memberType = METHOD;
	modifiers = printModifiers(meth.getModifiers());
	type = meth.getReturnType().getName();
        declaringClass = meth.getDeclaringClass().getName();
	name = meth.getName();
        // create args
        args = "(";
	Class argsTypes[] = meth.getParameterTypes();
	if (argsTypes.length > 0) {
	    args = args + argsTypes[0].getName();
	}
	for (int j = 1; j < argsTypes.length; j++) {
	    args = args + "," + argsTypes[j].getName();
	}
	args = args + ")";
        
        // create throws clause
	Class[] exceptions = meth.getExceptionTypes();
        String ExceptionName[] = new String[exceptions.length];
        for (int j = 0; j < exceptions.length; j++)
            ExceptionName[j] = exceptions[j].getName();
        sort(ExceptionName);
        if (ExceptionName.length > 0) {
            throwables = "throws " + ExceptionName[0];
        }
        for (int i = 1; i < ExceptionName.length; i++) {
            throwables = throwables + "," + ExceptionName[i];
        }
    }
    
    /**
     * Create description for the given constructor.
     *
     * @see ItemDescription#memberType
     * @see ItemDescription#name
     * @see #modifiers
     * @see #type
     * @see #args
     * @see #throwables
     * @see #declaringClass
     */
    public FullItemDescription(Constructor ctor) {
        memberType = CONSTRUCTOR;
	modifiers = printModifiers(ctor.getModifiers());
	parseQualifiedName(ctor.getName());
        declaringClass = ctor.getDeclaringClass().getName();
        // create args
        args = "(";
	Class argsTypes[] = ctor.getParameterTypes();
	if (argsTypes.length > 0) {
	    args = args + argsTypes[0].getName();
	}
	for (int j = 1; j < argsTypes.length; j++) {
	    args = args + "," + argsTypes[j].getName();
	}
	args = args + ")";
        
        // create throws clause
	Class[] exceptions = ctor.getExceptionTypes();
        String ExceptionName[] = new String[exceptions.length];
        for (int j = 0; j < exceptions.length; j++)
            ExceptionName[j] = exceptions[j].getName();
        sort(ExceptionName);
        if (ExceptionName.length > 0) {
            throwables = "throws " + ExceptionName[0];
        }
        for (int i = 1; i < ExceptionName.length; i++) {
            throwables = throwables + "," + ExceptionName[i];
        }
    }

    /** creates definition for given name of the member.
     *  @param ctor given name of the member.
     **/
    public FullItemDescription(String definition) {
        this.setData(definition);
    }

    /**
     * Incarnate <code>this</code> fields with default values.
     */
    public FullItemDescription() {
    }

    /**
     * Sort the given array <code>arr</code> lexicographically,
     * and return the reference to itself.
     */
    protected static String[] sort(String[] arr) {
	String temp;
	int pos;
	for (int j = 0; j < arr.length; j++) {
	    pos = j;
	    for (int i = j + 1; i < arr.length; i++)
		if (arr[i].compareTo(arr[pos]) < 0)
		    pos = i;
	    temp = arr[j];
	    arr[j] = arr[pos];
	    arr[pos] = temp;
	}
	return arr;
    }

    /**
     * Given the <code>access</code> flags, return
     * <b>Vector</b> of <b>String</b> names for those flags.
     *
     * @see ItemDescription#ACC_PUBLIC
     * @see ItemDescription#ACC_PRIVATE
     * @see ItemDescription#ACC_PROTECTED
     * @see ItemDescription#ACC_STATIC
     * @see ItemDescription#ACC_FINAL
     * @see ItemDescription#ACC_SYNCHRONIZED
     * @see ItemDescription#ACC_VOLATILE
     * @see ItemDescription#ACC_TRANSIENT
     * @see ItemDescription#ACC_NATIVE
     * @see ItemDescription#ACC_INTERFACE
     * @see ItemDescription#ACC_ABSTRACT
     * @see ItemDescription#ACC_SUPER
     * @see ItemDescription#ACC_STRICT
     * @see ItemDescription#ACC_EXPLICIT
     */
    public static Vector printModifiers(int access) {
        Vector retVal = new Vector();
        if ((access & ACC_PUBLIC) != 0)
            retVal.addElement("public");
        if ((access & ACC_PRIVATE) != 0)
            retVal.addElement("private");
        if ((access & ACC_PROTECTED) != 0)
            retVal.addElement("protected");
        if ((access & ACC_STATIC) != 0)
            retVal.addElement("static");
        if ((access & ACC_TRANSIENT) != 0)
            retVal.addElement("transient");
        if ((access & ACC_SYNCHRONIZED) != 0)
            retVal.addElement("synchronized");
        if (((access & ACC_ABSTRACT) != 0) ||
                 ((access & ACC_INTERFACE) != 0))
            retVal.addElement("abstract");
        if ((access & ACC_NATIVE) != 0)
            retVal.addElement("native");
        if ((access & ACC_FINAL) != 0)
            retVal.addElement("final");
        if ((access & ACC_INTERFACE) != 0)
            retVal.addElement("interface");
        if ((access & ACC_VOLATILE) != 0)
            retVal.addElement("volatile");
        return retVal;
    }

    /**
     * Return <code>memberType</code> and <code>name</code> of <code>this</code>
     * instance.
     *
     * @see ItemDescription#memberType
     * @see ItemDescription#name
     */
    public ItemDescription getItem() {
        if (memberType.equals(INTERFACE) || memberType.equals(SUPER))
            return new ItemDescription(memberType);
        else
            return new ItemDescription(memberType + name +
                                       ((args != null) ? args : ""));
    }

    /**
     * Display all attributes of <code>this</code> item.
     * Note, that there is no ``<i><b>extends</b></i> ...'' and 
     * no ``<i><b>implements</b></i> ...'' information for class
     * items in <b>FullItemDescription</b>.
     *
     * @see #setData(String)
     *
     * @see ClassDescription
     */
    public String toString() {
        String retVal = memberType;
        for (Enumeration e = modifiers.elements(); e.hasMoreElements();
             retVal = retVal + e.nextElement() + " ");
        retVal = retVal + ((type != null) ? type + " " : "");
        if ((declaringClass != null) && !declaringClass.equals(""))
            return (retVal + declaringClass + (isClass() ? "$" : ".") +
                    name + ((args != null) ? args : "") +
                    ((throwables != null) ? " " + throwables : "")).trim();
        else
            return (retVal + name + ((args != null) ? args : "") +
                    ((throwables != null) ? " " + throwables : "")).trim();
    }
    
    /**
     * Return access code for <code>this</code> item, which could be either
     * <code>PRIVATE</code>, or <code>PROTECTED</code>, or <code>PUBLIC</code>,
     * or <code>DEFAULT</code>.
     *
     * @see #PRIVATE
     * @see #PROTECTED
     * @see #PUBLIC
     * @see #DEFAULT
     */
    public int getAcces() {
        if (modifiers.contains("private"))
            return PRIVATE;
        if (modifiers.contains("protected"))
            return PROTECTED;
        if (modifiers.contains("public"))
            return PUBLIC;
        return DEFAULT;
    }

    /**
     * Return <b>Vector</b> of symbolic names of Java language modifiers 
     * assigned to <code>this</code> item.
     *
     * @see java.lang.Class#getModifiers()
     * @see java.lang.reflect.Field#getModifiers()
     * @see java.lang.reflect.Method#getModifiers()
     * @see java.lang.reflect.Constructor#getModifiers()
     */
    public Vector getModifiers() {
        return modifiers;
    }
    
    /**
     * Display return-type if <code>this</code> describes some method,
     * or type of the field if <code>this</code> describes some field.
     *
     * @see #type
     */
    public String getType() {
        return type;
    }
    
    /**
     * Display qualified name of the class or interface declaring 
     * <code>this</code> item. Empty string is returned if <code>this</code>
     * item describes top-level class or interface, which is not inner
     * class or interface.
     */
    public String getDeclaringClass() {
        return (declaringClass == null) ? "" : declaringClass;
    }

    /**
     * Return short <code>name</code> followed by <code>args</code> type codes.
     * (Note, that method's signature does not contains the <code>type</code> 
     *  for returned value.)
     *
     * @see ItemDescription#name
     * @see #args
     * @see #type
     */
    public String getSignature() {
        return ((name == null) ? "" : name) + ((args == null) ? "" : args);
    }

    /**
     * Either short name of <code>this</code> field, or method, or constructor,
     * or top-level class or interface; or qualified name, if <code>this</code>
     * item describes inner class or interface.
     */
    public String getName() {
        if (isClass() &&
            (!getDeclaringClass().equals("")))
            return declaringClass + "$" + name;
        else
            return ((name == null) ? "" : name);
    }

    /**
     * Return list of exception names declared in the <i><b>throws</b></i> clause
     * for that method or constructor described by <code>this</code> item.
     * If not empty, returned string contains the word <code>"throws"</code>
     * followed by space character, and then listed names of exceptions separated 
     * by commas.
     *
     * @see #throwables
     */
    public String getThrowables() {
        return (throwables == null) ? "" : throwables;
    }
    
    /**
     * Check if <code>this</code> item equals to the given 
     * <b>FullItemDescription</b> <code>o</code>.
     */
    public boolean equals(Object o) {
        if (o instanceof FullItemDescription) {
            String thisValue = this.toString();
            String otherValue = ((FullItemDescription)o).toString();
            return thisValue.equals(otherValue);
        } else
            return false;
    }

    /**
     * Check if modifiers list for <code>this</code> item contains
     * the <code>"protected"</code> string.
     *
     * @see #modifiers
     */
    public boolean isProtected() {
        return modifiers.contains("protected");
    }

    /**
     * Check if modifiers list for <code>this</code> item contains
     * the <code>"public"</code> string.
     *
     * @see #modifiers
     */
    public boolean isPublic() {
        return modifiers.contains("public");
    }
    
    /**
     * Check if modifiers list for <code>this</code> item contains
     * the <code>"private"</code> string.
     *
     * @see #modifiers
     */
    public boolean isPrivate() {
        return modifiers.contains("private");
    }

    /**
     * Check if modifiers list for <code>this</code> item contains
     * the <code>"abstract"</code> string.
     *
     * @see #modifiers
     */
    public boolean isAbstract() {
        return modifiers.contains("abstract");
    }

    /**
     * Check if modifiers list for <code>this</code> item contains
     * the <code>"static"</code> string.
     *
     * @see #modifiers
     */
    public boolean isStatic() {
        return modifiers.contains("static");
    }

    
    /**
     * Load <code>this</code> instance by parsing item's symbolic description.
     * Lexemes are well defined with space characters, or with parentheses 
     * <code>'('</code> and <code>')'</code>, or with brackets <code>'&lt;'</code> 
     * and <code>'&gt;'</code>. Brackets are used to keep some additional information
     * about <code>this</code> item, such as <code>&lt;synthetic&gt;</code> label
     * for some synthetic items, or <code>&lt;value="..."&gt;</code> for final 
     * constants. 
     *
     * <p>This method is inverse to <code>this.toString()</code>.
     *
     * @param definition Item's description to parse.
     *
     * @see #toString()
     */
    protected void setData(String defintion) {
        memberType = defintion.substring(0, CLASS.length());
        memberType = memberType.equals(INNER) ? CLASS : memberType;
            
        String entry = defintion.substring(CLASS.length());
        
        boolean isRead = false;
        for (StringTokenizer e = new StringTokenizer(entry, " <>()", true);
             e.hasMoreElements();) {
            String current = (String)e.nextElement();
            if (current.equals("<")) {
                addToken("<" + getElement(e, ">") + ">");
            } else if (current.equals("(")) {
                addToken( "(" + getElement(e, ")") + ")");
            } else if (!current.equals(" ")) {
                addToken(current);
            }
        }
        addToken(null);
        if (!modifiers.isEmpty() && (isMethod() || isField())) {
            type = (String)modifiers.lastElement();
            modifiers.setSize(modifiers.size() - 1);
        } 
    }

    /**
     * Given <code>access</code> flags, return vector of their <b>String</b> names.
     * This method replaces <code>SYNCHRONIZED</code> flag by <code>FLAG_SUPER</code>.
     *
     * @see #printModifiers(int)
     * @see ItemDescription#SYNCHRONIZED
     * @see ItemDescription#FLAG_SUPER
     */
    public static Vector printClassModifiers(int access) {
        Vector retVal = printModifiers(access);
        for (int i = 0; i < retVal.size(); i++)
            if (retVal.elementAt(i).equals(SYNCHRONIZED)) {
                retVal.setElementAt(FLAG_SUPER, i);
                break;
            }
        return retVal;
    }

    private String getElement(Enumeration e, String end) {
        String retVal = "";
        String current;
        while (e.hasMoreElements() && 
               (!(current = (String)e.nextElement()).equals(end)))
            retVal = retVal + current;
        return  retVal;
    }

    private void addToken(String lexem) {
        if (lexem == null) {
            if (args == null)
                parseQualifiedName(last);
        } else if (isThrowables) {
            throwables = throwables + " " + lexem;
        } else if (lexem.startsWith("(")) {
            parseQualifiedName(last);
            args = lexem;
            last = null;
        } else if (lexem.equals("throws")) {
            throwables = lexem;
            isThrowables = true;
        } else {
            if (last != null)
                modifiers.addElement(last);
            last = lexem;
        }
    }

    /**
     * Split <code>this</code> item's <code>qualifiedName</code> into
     * <code>declaringClass</code> qualified name and <code>this</code>
     * item's short <code>name</code>.
     *
     * @see #declaringClass
     * @see ItemDescription#name
     */
    protected void parseQualifiedName(String qualifiedName) {
        if (qualifiedName != null) {
            int pos = qualifiedName.lastIndexOf('.');
            int lastPos = qualifiedName.lastIndexOf('$');
            if (isConstructor() || isClass() && (lastPos > pos)) {
                if (lastPos > pos)
                    pos = lastPos;
            } else if (!(isMethod() || isField())) {
                pos = -1;
            }
            if (pos > 0)
                declaringClass = qualifiedName.substring(0, pos);
            else
                declaringClass = "";
            name = qualifiedName.substring(++pos);
        }
    }
}
