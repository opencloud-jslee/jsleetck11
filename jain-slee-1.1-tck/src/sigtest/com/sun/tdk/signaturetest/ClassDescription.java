/*
 * @(#)ClassDescription.java	1.14 03/08/21
 *
 * Copyright (c) 2003 Sun Microsystems, Inc.  All rights reserved. 
 * SUN PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *
 * TDK Signature Test Tool. 
 *
 * @author Maxim Sokolnikov
 */

package com.sun.tdk.signaturetest;

import java.util.Enumeration;
import java.util.Vector;

/**
 * <b>ClassDescription</b> lists all public or protected members of some class
 * or interface. Given some <b>ItemDescription</b> instance, it is possible to 
 * get a <b>Vector</b> of <b>FullItemDescription</b> references for all members
 * characterized with that shorter <b>ItemDescription</b>.
 *
 * <p>Several abstract methods for finding nested classes are declared in 
 * <b>ClassDescription</b>, which are implemented in the following two classes: <ul>
 * <li> <b>ReflClassDescription</b> using reflection, and
 * <li> <b>BasedClassDescription</b> using previously created table of nested classes.
 * </ul>
 *
 * @see BasedClassDescription
 * @see ReflClassDescription
 *
 * @version 03/08/21
 */
abstract public class ClassDescription extends FullItemDescription {
    
    /**
     * List of members of that class described by <code>this</code> instance.
     * This is a hashtable indexed by <b>ItemDescription</b> instances, and 
     * containing a <b>Vector</b> of <b>FullItemDescription</b> instances
     * describing all members characterized with the same <b>ItemDescription</b>.
     *
     * @see ItemDescription
     * @see FullItemDescription
     */
    protected ClassCollection members;

    /**
     * Description for empty class containing no members.
     *
     * @see #members
     */
    protected ClassDescription() {
        members = new ClassCollection();
    }
    
    
    /** 
     * Adds new class member.
     */
    public void add (FullItemDescription x) {
        members.addElement(x.getItem(), x);
    }

    /**
     * Return the <b>Vector</b> of <b>ItemDescription</b> instances corresponding
     * to those class members having the same <b>ItemDescription</b> as the given
     * <code>key</code>.
     *
     * @param key <b>ItemDescription</b> instance.
     *
     * @see #members
     * @see ItemDescription
     */
    public Vector get(ItemDescription key) {
	return members.get(key);
    }

    /**
     * Return <b>Enumeration</b> of <b>ItemDescription</b> items for all 
     * members of that class described by <code>this</code> instance.
     *
     * @see #members
     * @see ItemDescription
     */
    public Enumeration items() {
	return members.keys();
    }


    /**
     * Return description for that class declaring <code>this</code> item,
     * if the declaring class is not anonymous; otherwise, return <code>null</code>.
     *
     * @see FullItemDescription#getDeclaringClass()
     */
    public FullItemDescription getEnclosingClass() {
        String name = getDeclaringClass();
        if ((name == null) || name.equals(""))
            return null;
        else
            return new FullItemDescription(CLASS + name);
    }

    public abstract FullItemDescription[] getDeclaredConstructors();
    public abstract FullItemDescription[] getDeclaredFields();
    public abstract FullItemDescription[] getDeclaredMethods();
    public abstract FullItemDescription[] getDeclaredClasses();
    public abstract FullItemDescription[] getInterfaces();
    public abstract FullItemDescription   getSuperclass();
}
