/*
 * @(#)DirectoryEntry.java	1.8 03/08/22
 *
 * Copyright (c) 2003 Sun Microsystems, Inc.  All rights reserved. 
 * SUN PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *
 * TDK Signature Test Tool. 
 *
 * @author Maxim Sokolnikov
 */

package com.sun.tdk.signaturetest;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Enumeration;
import java.util.StringTokenizer;
import java.util.Vector;


class DirectoryEntry implements PathEntry {

    /**
     * Qualified names for all those classes found in <code>this</code> directory.
     */
    private Vector classes;

    /** 
     * The name of that class last found in <code>elements</code> enumeration.
     *
     * @see #elements
     */
    private String current;

    /**
     * List of errors occurred while <code>this</code> directory was scanned.
     *
     * @see #scanDirectory(File,String)
     */
    private Vector errors;

    /** 
     * Enumeration of the <code>classes</code> found in <code>this</code> directory.
     * These <code>elements</code> are available with the methods 
     * <code>nextClassName()</code> and <code>getCurrentClass()</code>, or
     * <code>findClass(name)</code>.
     *
     * @see #classes
     * @see #nextClassName()
     * @see #getCurrentClass()
     * @see #findClass(String)
     */
    private Enumeration elements;

    /**
     * The qualified name of <code>this</code> directory.
     */
    private String path = "";

    /**
     * Check if the given <code>name</code> is a directory name, and
     * create new <b>DirectoryEntry</b> for the given directory.
     *
     * @param name Directory name.
     * @param errors Address, where error messages should be reported to.
     * 
     * @see #scanDirectory(File,String)
     */
    public DirectoryEntry(String name, Vector errors) throws IOException {
        init(name, errors);
    }
     
     
    public void init(String name, Vector errors) throws IOException {
        File directory = new File(name);
        if (directory.isDirectory()) {
            path = name;
            this.errors = errors;
            classes = new Vector();
            scanDirectory(directory, "");
            elements = classes.elements();
        } else {
            throw new IOException(name + " isn't directory.");
        }
    }
    
    
    public void close () {
    }


    /** 
     * Find all classes placed in the given <code>directory</code> including
     * those placed in subdirectories. (Recursively walk subdirectories tree,
     * if needed.) Qualified names for all class files found inside the given 
     * <code>directory</code> are collected in <code>this</code> 
     * <b>DirectoryEntry</b> instance.
     *
     * @param directory Directory to scan.
     * @param packag Package name for classes inside the given 
     *     <code>directory</code>.
     */
    private void scanDirectory(File directory, String packag) {
        // check for infinite loop which could occurs in the case of
        // cyclic symbolic link.
        int depth = 0;
        for (StringTokenizer e = new StringTokenizer(packag, ".");
             e.hasMoreTokens(); e.nextElement())
            depth++;
        if (depth > 512)
            return;
        try {
            String[] files = directory.list();
            if (files == null)
                return;
            for (int i = 0; i < files.length; i++) {
                File current = new File(directory, files[i]);
                String namePrefix = packag.equals("") ? "" : (packag + ".");
                if (current.isDirectory()) { 
                    scanDirectory(current, namePrefix + files[i]);
                } else if (files[i].endsWith(".class")) {
                    String name = namePrefix +
                                  files[i].substring(0, files[i].length() - 6);
                    classes.addElement(name);
                }
            }
        } catch (SecurityException e) {
            if (SigTest.debug)
                e.printStackTrace();
            String error = "The Security constraints does not allow" +
                           " to track " + packag;
            if (!errors.contains(error))
                errors.addElement(error);
        }
    }

    
    /** 
     * Search next class in the enumeration of classes found inside this 
     * directory including those found by subdirectories recursive search.
     * After some class name is returned by <code>nextClassName()</code>, 
     * that class becomes <i>current</i> in classes enumeration, and you may 
     * access to its bytecode with <code>getCurrentClass()</code> method. 
     * Invoke <code>setListToBegin()</code> method to reset classes enumeration.
     *
     * @return Class qualified name, or <code>null</code> if there is no next 
     *     class found.
     *
     * @see #setListToBegin()
     * @see #getCurrentClass()
     * @see #findClass(String)
     *
     * @see PathEntry
     * @see ZipFileEntry
     */
    public String nextClassName() {
        if (elements.hasMoreElements())
            return (current = (String)elements.nextElement());
        else
            return (current = null);
    }

    /** 
     * Open an <b>InputStream</b> for the class, which became <i>current</i> 
     * after <code>nextClassName()</code> method have found its name. Note, 
     * that there is no current class defined until you call 
     * <code>nextClassName()</code>.
     *
     * However, <code>findClass(name)</code> provides direct way to obtain 
     * bytecode for the named class, if it could be found in <code>this</code> 
     * <b>DirectoryEntry</b> instance.
     *
     * @return <b>InputStream</b> instance providing bytecode for the current 
     *     class.
     *
     * @see #nextClassName()
     * @see #setListToBegin()
     * @see #findClass(String)
     *
     * @see java.io.InputStream
     */
    public InputStream getCurrentClass() throws IOException {
        if (current == null)
            throw new IOException("The current class not exist.");
        return new FileInputStream(new File(constructFileName(current)));
    }

    /**
     * Reset enumeration of classes found in <code>this</code> 
     * <b>DirectoryEntry</b>. Bytecode for those classes is available with 
     * the methods <code>nextClassName()</code> and <code>getCurrentClass()</code>, 
     * or <code>findClass(name)</code>.
     *
     * @see #nextClassName()
     * @see #getCurrentClass()
     * @see #findClass(String)
     */
    public void setListToBegin() {
        elements = classes.elements();
        current = null;
    }

    /** 
     * Returns <b>FileInputStream</b> instance providing bytecode for the 
     * required class, if the class could be found by the given qualified 
     * name in <code>this</code> <b>DirectoryEntry</b> instance.
     *
     * @param name Qualified name of the class required.
     *
     * @exception ClassNotFoundException The named class is not found in 
     *     <code>this</code> <b>DirectoryEntry</b>.
     *
     * @see java.io.FileInputStream
     */
    public InputStream findClass(String name)
        throws IOException, ClassNotFoundException {
        if (classes.contains(name)) {
            return new FileInputStream(new File(constructFileName(name)));
        } else {
            throw new ClassNotFoundException(name);
        }
    }

    /**
     * Replace dots in the given qualified class <code>name</code> with
     * appropriate files separator symbol.
     *
     * @see java.io.File#separator
     */
    private String constructFileName(String name) {
        String retVal = "";
        // construct name of the class file
        for (StringTokenizer e = new StringTokenizer(name, ".");
             e.hasMoreTokens(); ) {
            retVal = retVal + e.nextToken();
            retVal = retVal + (e.hasMoreTokens() ? File.separator : "");
        }
        return path + File.separator + retVal + ".class";
    }
}
