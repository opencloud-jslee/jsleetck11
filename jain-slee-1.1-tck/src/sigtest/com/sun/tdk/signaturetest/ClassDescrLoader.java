/*
 * @(#)ClassDescrLoader.java	1.13 03/11/05
 *
 * Copyright (c) 2003 Sun Microsystems, Inc.  All rights reserved. 
 * SUN PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *
 * TDK Signature Test Tool. 
 *
 * @author Maxim Sokolnikov
 */

package com.sun.tdk.signaturetest;

import java.util.Enumeration;
import java.util.Vector;
import java.util.StringTokenizer;

/**
 * This class provides methods to find an load a class and to compile 
 * a <b>ClassDescription</b> for it. The method <b>Class</b>.<code>forName()</code>
 * is used to find a <code>Class</code> object. If the advanced method
 * <code>forName</code>(<b>String</b>,<code>boolean</code>,<b>ClassLoader</b>)
 * is unavailable, the rougher method <code>forName</code>(<b>String</b>) is used.
 *
 * @version 03/11/05
 */
abstract public class ClassDescrLoader {
    /**
     * Load class description for the given class <code>name</code>.
     *
     * @see ReflClassDescrLoader#loadClass(String)
     */
    abstract public ClassDescription loadClass(String name)
        throws ClassNotFoundException;

    /**
     * List <b>ClassDescription</b> instances describing all nested classes of 
     * the given class, which have the given short name. Inherited nested classes 
     * are listed, as far as class declared by the given class (if exists).
     *
     * @param encl Description of the enclosing class to be investigated.
     * @param localName Short name of nested classes to be described.
     */
    public Vector getNestedClass(ClassDescription encl, String localName) {
	if (encl == null)
	    return new Vector();
        
	try {
	    ClassDescription c = loadClass(encl.getName() + "$" + localName);
	    //The declared nested class hides all inherited nested classes.
	    Vector h = new Vector();
            if (c.isPublic() || c.isProtected())
                h.addElement(c);
	    return h;
	} catch (ClassNotFoundException e) {
	} catch (LinkageError er) {
        }
        
	//found all inherited nested classes with the same name.
	Vector classes = getNestedClass(load(encl.getSuperclass()), localName);
	FullItemDescription[] interfaces = encl.getInterfaces();
	for (int i = 0; i < interfaces.length; i++) {
	    Vector h = getNestedClass(load(interfaces[i]), localName);
	    for (int j = 0; j < h.size(); j++) {
		Object ob = h.elementAt(j);
		if (classes.indexOf(ob) < 0)
		    classes.addElement(h.elementAt(j));
	    }
	}
        return classes;
    }

    
    /**
     * Generate <code>members</code> field for the given <b>ClassDescription</b>
     * <code>cl</code>. Recursively find all inherited fields, methods, nested
     * classes, and interfaces for the class having the name prescribed by 
     * <code>cl</code>.
     *
     * @see FullItemDescription
     */
    public void createMembers(ClassDescription cl) throws ClassNotFoundException {
	ClassCollection members = new ClassCollection();
        
	FullItemDescription spr = cl.getSuperclass();
        if (spr == null)
            spr = new FullItemDescription(ItemDescription.SUPER + "null");
	members.addElement(spr.getItem(), spr);

        Vector interfaces = new Vector();
        getInterfaces(cl, interfaces);
	for (Enumeration e = interfaces.elements(); e.hasMoreElements();) {
            FullItemDescription intf = (FullItemDescription)e.nextElement();
            members.addElement(intf.getItem(), intf);
        }

        //includes public and protected constructors
	FullItemDescription[] constr = cl.getDeclaredConstructors();
	for (int i = 0; i < constr.length; i++) {
	    if (constr[i].isPublic() || constr[i].isProtected())
		members.addElement(constr[i].getItem(), constr[i]);
	}

        //includes public and protected members, classes, fields
        ClassCollection inherited = getMembers(cl);
        for (Enumeration e = inherited.keys(); e.hasMoreElements();) {
            Vector h = (Vector)inherited.get(e.nextElement());
            for (Enumeration el = h.elements(); el.hasMoreElements();) {
                FullItemDescription v = (FullItemDescription)el.nextElement();
                if (v.isPublic() || v.isProtected())
                    members.addUniqueElement(v.getItem(), v);
            }
        }
        cl.members = members;
    }
    
    
    static void removeThrows (ClassDescription c) {
    //  For every methods and constructors ...    
        for (Enumeration e = c.items(); e.hasMoreElements();) {
            Vector h = c.get((ItemDescription)e.nextElement());
            for (Enumeration el = h.elements(); el.hasMoreElements();) {
               FullItemDescription mr = (FullItemDescription)el.nextElement();
               if (mr.isMethod() || mr.isConstructor()) 
                    mr.throwables = "";
            }
        }
    }

                
    void normThrows (ClassDescription c) {
    //  For every methods and constructors ...    
        for (Enumeration e = c.items(); e.hasMoreElements();) {
            Vector h = c.get((ItemDescription)e.nextElement());
            for (Enumeration el = h.elements(); el.hasMoreElements();) {
               FullItemDescription mr = (FullItemDescription)el.nextElement();
               if (mr.isMethod() || mr.isConstructor()) 
                if (mr.throwables != null && mr.throwables.startsWith("throws ")) {
                //  Found method or constructor with non-empty throws list.
                //  Parse the list into array of string (class names).
                    Vector/*String*/ xthrows = new Vector();
                    
                    StringTokenizer st = new StringTokenizer(mr.throwables.substring(7), ", ");
                    while (st.hasMoreTokens())
                        xthrows.add(st.nextToken());
                    st = null;
                    
                    int thrown = 0;
                    Vector supers = new Vector();
                    
                //  Scan over all throws ...
                    
                    for (int i = 0; i < xthrows.size(); i++) {
                        String s = (String)xthrows.get(i);
                        if (s != null) {
                            findSuperclasses(s, supers); // find all superclasses for the throw
                            supers.add(s);
                            
                            boolean norm = false;        // should it be nomalized (thrown away) ?
                            
                            if (supers.contains("java.lang.RuntimeException")) 
                                norm = true;
                            else
                                for (int k = 0; k < xthrows.size(); k++) 
                                    if (k != i) {
                                        String r = (String)xthrows.get(k);
                                        if (r != null && supers.contains(r)) {
                                            norm = true;
                                            break;
                                        }
                                    }
                                    
                            supers.clear();
                            
                            if (norm) {
                            //  This throw should be thrown away
                                xthrows.set(i, null);
                                thrown++;
                                //System.out.println("- " + s); 
                            }
                        }
                    }
                    supers = null;
                    
                //  Should the throws list be updated ?
                    
                    if (thrown != 0) { 
                        int count = 0;
                        StringBuffer sb = new StringBuffer();
                        
                        sb.append("throws ");
                        for (int i = 0; i < xthrows.size(); i++) {
                            String s = (String)xthrows.get(i);
                            if (s != null) {
                                if (count++ != 0)
                                    sb.append(',');
                                sb.append(s);
                            }
                        }
                        
                        if (count == 0)
                            mr.throwables = "";
                        else
                            mr.throwables = sb.toString();
                    }
                }
            }
        }

    }

    
    private void findSuperclasses (String fqn, Vector supers) {
        try {
            ClassDescription c = loadClass(fqn);
            FullItemDescription supr = c.getSuperclass();
            if (supr != null) {
                String s = supr.getName();
                supers.add(s);
                findSuperclasses(s, supers);
            }
        }
        catch (ClassNotFoundException e) {
        }
    }
    
    
    /**
     * Collect <b>FullItemDescription</b>s for all fields, methods, and nested 
     * classes of the given class described by <code>cl</code>. Recursively find 
     * all inherited members, as far as members declared by the class 
     * <code>cl</code>.
     *
     * @see FullItemDescription
     */
    /*private*/ ClassCollection getMembers(ClassDescription cl) {
        if (cl == null)
            return new ClassCollection();

        // creates members inherited from superclass
        ClassDescription superClass = load(cl.getSuperclass());
        ClassCollection retVal = getMembers(superClass);
        //exclude non-accessible members
        if (superClass != null)
            retVal = getAccessibleMembers(retVal, cl, superClass);

        // creates members inherited from superinterfaces
        ClassCollection interfaceMembers = new ClassCollection();
        FullItemDescription interfaces[] = cl.getInterfaces();
        
        for (int i = 0; i < interfaces.length; i++) {
            ClassCollection h = getMembers(load(interfaces[i]));
            interfaceMembers.addUniqueCollection(h);
        }
        
        // creates inherited members
        for (Enumeration e = interfaceMembers.keys(); e.hasMoreElements(); ) {
            ItemDescription member = (ItemDescription)e.nextElement();
            Vector h = retVal.get(member);
            if (!member.isMethod() ||
                (h == null) ||
                !((h.size() == 1) &&
                  !((FullItemDescription)h.firstElement()).isAbstract())) {
                // The inherited non-abstract method implements all inherited
                // abstract methods
                Vector tmp =  interfaceMembers.get(member);
                for (Enumeration el = tmp.elements(); el.hasMoreElements();
                     retVal.addUniqueElement(member, el.nextElement()));
                
            }
        }
        // creates declared members
        FullItemDescription[] methods = cl.getDeclaredMethods();
        for (int i = 0; i < methods.length; i++)
            retVal.put(methods[i].getItem(), methods[i]);
        FullItemDescription[] fields = cl.getDeclaredFields();
        for (int i = 0; i < fields.length; i++)
            retVal.put(fields[i].getItem(), fields[i]);
        FullItemDescription[] classes = cl.getDeclaredClasses();
        for (int i = 0; i < classes.length; i++)
            retVal.put(classes[i].getItem(), load(classes[i]));
        return retVal;
    }

    /**
     * Filter those <b>FullItemDescription</b> instances found inside the 
     * given <code>members</code> collection available for use by the given 
     * <code>subclass</code>, provided they are members of the given 
     * <code>superClass</code>.
     *
     * @see FullItemDescription
     */
    private ClassCollection getAccessibleMembers(ClassCollection members,
                                                 ClassDescription subclass,
                                                 ClassDescription superClass) {
        String pkg = getPackage(subclass.getName());
        boolean isSamePackage = pkg.equals(getPackage(superClass.getName()));
        ClassCollection retVal = new ClassCollection();
        
        for (Enumeration el = members.keys(); el.hasMoreElements();) {
            Object key = el.nextElement();
            Vector h = members.get(key);
            for (Enumeration e = h.elements(); e.hasMoreElements();) {
                FullItemDescription mbr = (FullItemDescription)e.nextElement();
                if (mbr.isPublic() || mbr.isProtected() || isSamePackage &&
                    !mbr.isPrivate())
                    retVal.addElement(key, mbr);
            }
        }
        return retVal;
    }

    /**
     * Given the (qualified) class <code>name</code>, extract its package name.
     */
    private String getPackage(String name) {
        int pos = name.lastIndexOf(".");
        if (pos > 0)
            return name.substring(0, pos);
        else
            return "";
    }
        

    /**
     * Load and describe that class described by <code>cl</code>,
     * if its signature is not <code>"null"</code>. Do nothing, if
     * <code>cl</code> itself is an instance of <b>ClassDescription</b>.
     */        
    /*private*/ ClassDescription load(FullItemDescription cl) {
        if (cl instanceof ClassDescription)
            return (ClassDescription)cl;
        
        if ((cl == null) || cl.isMethod() || cl.isConstructor() ||
            cl.isField() || cl.getSignature().trim().equals("null"))
            return null;

        try {
            return loadClass(cl.getName());
        } catch (ClassNotFoundException e) {
            throw new NoClassDefFoundError(cl.getName());
        }
    }


    /**
     * Recursively find all interfaces implemented by the given class 
     * <code>c</code>. (I.e., also find interfaces extended by some interface 
     * of the given class, and so on.) Put resulting vector of 
     * <b>FullItemDescription</b> instances into the given vector <code>h</code>.
     *
     * @see FullItemDescription
     */
    protected void getInterfaces(ClassDescription c, Vector h) {
        if (c == null)
            return;
        getInterfaces(load(c.getSuperclass()), h);
        FullItemDescription intf[] = c.getInterfaces();
        if (intf == null)
            return;
        for (int i = 0; i < intf.length; i++) {
            if (!h.contains(intf[i])) {
                h.addElement(intf[i]);
                getInterfaces(load(intf[i]), h);
            }
        }
    }
}
