/*
 * @(#)ReflClassDescrLoader.java	1.13 03/08/21
 *
 * Copyright (c) 2003 Sun Microsystems, Inc.  All rights reserved. 
 * SUN PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *
 * TDK Signature Test Tool. 
 *
 * @author Maxim Sokolnikov
 */

package com.sun.tdk.signaturetest;

import java.lang.reflect.Method;
import java.lang.reflect.InvocationTargetException;
import java.util.Enumeration;
import java.util.Vector;

/**
 * <b>ReflClassDescrLoader</b> is intended to compile information about
 * classes via reflections package. If available, the advanced method 
 * <b>Class</b>.<code>forName</code>(<b>String</b>,<code>boolean</code>,<b>ClassLoader</b>)
 * is involved, which enables to do not initialize classes being loaded. 
 * (This helps to avoid irrelevant security problems.) 
 *
 * @see ClassDescription
 *
 * @version 03/08/21
 */
public class ReflClassDescrLoader extends ClassDescrLoader {
    /**
     * Reference to advanced <code>forName()</code> method available only for
     * newer Java implementations. If only simpler <code>forName()</code> method 
     * is available, then this field is incarnated with <code>null</code>.
     *
     * @see Class#forName(String,boolean,ClassLoader)
     */
    private Method forName = null;
    /**
     * Arguments prepared to invoke advanced <code>forName()</code> method via 
     * reflection.
     *
     * @see Method#invoke(Object,Object[])
     */
    private Object[] args = null;

    /**
     * Adjust new <b>ReflClassDescLoader</b> instance. In particular, detect 
     * if advanced method <b>Class</b>.<code>forName()</code> is implemented,
     * which enables to do not initialize the class being loaded. Otherwise,
     * tune <code>this</code> instance to use simpler version of the method
     * <code>forName()</code>, which must be available anyway.
     *
     * @see Class#forName(String,boolean,ClassLoader)
     * @see Class#forName(String)
     */
    public ReflClassDescrLoader() {
        args = new Object[] {
            "",
            Boolean.FALSE,
            this.getClass().getClassLoader()
        };
        Class c = Class.class;
        Class[] param = {
            String.class,
            Boolean.TYPE,
            ClassLoader.class
        };
        try {
            forName = c.getDeclaredMethod("forName", param);
        } catch (NoSuchMethodException e) {
        } catch (SecurityException e1) {
        }
    } 

    /**
     * Return new <b>ReflClassDescription</b> for that class found by the given 
     * <code>name</code>.
     *
     * @exception ClassNotFoundException If <b>Class</b>.<code>forName(name)</code> 
     *     fails to load the required class.
     *
     * @see Class#forName(String)
     */
    public ClassDescription loadClass(String name)
        throws ClassNotFoundException {
        if (forName == null) {
            return new ReflClassDescription(Class.forName(name));
        } else {
            args[0] = name;
            try {
                return new ReflClassDescription((Class)forName.invoke(null, args));
            } catch (InvocationTargetException e) {
                Throwable t = e.getTargetException();
                if (t instanceof LinkageError)
                    throw (LinkageError)t;
                else if (t instanceof ClassNotFoundException)
                    throw (ClassNotFoundException)t;
                else
                    // if the other than Exception of the 
                    return new ReflClassDescription(Class.forName(name));
            } catch (Throwable t) {
                return new ReflClassDescription(Class.forName(name));
            }
        }
    }
}
