/*
 * @(#)SignatureClassLoader.java	1.18 03/08/28
 *
 * Copyright (c) 2003 Sun Microsystems, Inc.  All rights reserved. 
 * SUN PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *
 * TDK Signature Test Tool. 
 *
 * This program merges several signature files into a single one.
 *
 * @author Maxim Sokolnikov
 */

package com.sun.tdk.signaturetest;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.Vector;

/**
 * <b>SignatureClassLoader</b> implements input stream sequentially 
 * reading <b>ClassDescription</b> instances from signature file.
 *
 * @see ClassDescription
 *
 * @version 03/08/28
 */
public class SignatureClassLoader {
    /**
     * Reads lines from signature file.
     */
    private BufferedReader in = null;
    
    /**
     * The last line read from <code>this</code> signature file.
     */
    private String currentLine;

    /**
     * Sigfile format version found in <code>this</code> signature file.
     */
    public String signatureFileFormat = "";

    /**
     * API version found in <code>this</code> signature file.
     */
    public String apiVersion = "";

    /**
     * true if "#ThrowsNormalized" was found in this sigfile
     */
    public boolean isThrowsNormalized = false;
    
    /**
     * true if "#ThrowsRemoved" was found in this sigfile
     */
    public boolean isThrowsRemoved = false;
    

    /**
     * Open <code>fileURL</code> for input, and parse comments to initialize fields
     * <code>signatureFileFormat</code>
     * <code>apiVersion</code> 
     * <code>isThrowsNormalized</code>
     * <code>isThrowsRemoved</code>
     */
    public boolean open(URL fileURL) throws IOException {
    
	in = new BufferedReader(new InputStreamReader(fileURL.openStream(), "UTF8"));
        
	if ((currentLine = in.readLine()) == null)
            return false;

    //  Check for the required headers (first two lines)
            
        signatureFileFormat = currentLine.trim();     
        
        if (signatureFileFormat.equals("#PJava Signature file"))
	    signatureFileFormat = "#Signature file v1";
        
	if (signatureFileFormat.equals("#Signature file"))
            signatureFileFormat = "v0";
        else if (!signatureFileFormat.startsWith("#Signature file "))
            return false;
        else
            signatureFileFormat = signatureFileFormat.substring(16).trim();
            
	if ((currentLine = in.readLine()) == null)
            return false;

        currentLine += ' ';            
	if (!currentLine.startsWith("#Version "))
            return false;
        apiVersion = currentLine.substring("#Version ".length()).trim();
        
    //  Check for the v2 format tags
               
        if (signatureFileFormat.equals("v2")) {
            isThrowsNormalized = true; // 'v2' sigfiles are normalized by default
            
            while ((currentLine = in.readLine()) != null && currentLine.startsWith("#")) {
                //if (currentLine.equals("#ThrowsNormalized")) 
                //    isThrowsNormalized = true;
                
                //if (currentLine.equals("#ThrowsRemoved")) 
                if (currentLine.equals("#binary")) 
                    isThrowsRemoved = true;
            }
        }
        
        return true;
    }


    public void close() {
        try {
            if (in != null)
                in.close();
        }
        catch (IOException x) {
                if (SigTest.debug)
                    x.printStackTrace();
        }
        
        in = null;
    }

    /**
     * Return the next <b>BasedClassDescription</code> read from <code>this</code>
     * signature file.
     *
     * @see BasedClassDescription
     */
    public ClassDescription nextClass() throws IOException {
    
        String classDescr  = null;
	Vector definitions = new Vector();
        
        for (;;) {
            if (currentLine == null) 
                if ((currentLine = in.readLine()) == null) 
                    break;
                
            currentLine = currentLine.trim();
            if (currentLine.length() == 0 || currentLine.startsWith("#")) {
                currentLine = null;
                continue;
            }
                
	    if (currentLine.startsWith(ItemDescription.CLASS)) {
                if (classDescr == null) {
                    classDescr  = currentLine;
                    currentLine = null;
                }
                else
                    break;
            }
            else {
                if (classDescr == null)
                    throw new Error();
                else {
                    definitions.add(currentLine);
                    currentLine = null;
                }
            }
        }
        
        if (definitions.size() == 0)
            return null;
        else
            return new BasedClassDescription(classDescr, definitions.elements());
    }
    
}
