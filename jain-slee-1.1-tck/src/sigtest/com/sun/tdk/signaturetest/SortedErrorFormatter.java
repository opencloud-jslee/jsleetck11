/* @(#)SortedErrorFormatter.java	1.16 03/06/09
 *
 * Copyright (c) 2003 Sun Microsystems, Inc.  All rights reserved. 
 * SUN PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *
 * TDK Signature Test Tool. 
 *
 * @author Maxim Sokolnikov
 * @author Serguei Ivashin
 */

package com.sun.tdk.signaturetest;

import java.io.PrintWriter;
import java.util.Collections;
import java.util.Comparator;
import java.util.Hashtable; 
import java.util.Vector;     
import java.util.Enumeration;


/**
 * <b>SortedErrorFormatter</b> formats error messages created by <b>SignatureTest</b> 
 * and by <b>APIChangesTest</b>. This class prints messages sorted error type and by 
 * name of class affected by the error.
 *
 * @version 03/06/09
 */
public class SortedErrorFormatter extends ErrorFormatter {
    /**
     * Headers for message groups by error type.
     */
    protected String[]  headers = {
        "Missing Classes",
        "Missing Class Definitions",
        "Missing Superclasses or Superinterfaces",
        "Missing Fields",
        "Missing Constructors",
        "Missing Methods",
        "Added Classes",
        "Added Class Definitions",
        "Added Superclasses or Superinterfaces",
        "Added Fields",
        "Added Constructors",
        "Added Methods",

        "Changed Classes",
        "Changed Class Definitions",
        "Changed Superclasses or Superinterfaces",
        "Changed Fields",
        "Changed Constructors",
        "Changed Methods",
        "LinkageError",
    };


    private Hashtable /*String,String*/ testedsuper = new Hashtable();

    private boolean isVerbose = false;
    
    private int exmsgs;

    
    /**
     * Messages buffer.
     */
    protected Vector failedMessages;
    
    /**
     * Tabulator position.
     */
    protected int tabSize = 20;

    /** 
     * Assign the <b>PrintWriter</b> and reassign <code>headers</code>.
     *
     * @see #headers
     */
    public SortedErrorFormatter(PrintWriter out, String headers[], boolean isv) {
        super(out);
        this.headers = headers;
        isVerbose = isv;
    }

    /**
     * Assign the given <b>PrintWriter</b> to print error messages.
     */
    public SortedErrorFormatter(PrintWriter out, boolean isv)  {
        super(out);
        failedMessages = new Vector();
        isVerbose = isv;
    }

    public void Tested (ClassDescription tested) {
    
        if (!testedsuper.containsKey(tested.getName()))
        {
            if (tested.getSuperclass() != null)
                testedsuper.put(tested.getName(), tested.getSuperclass().getName());

            FullItemDescription[] items = tested.getInterfaces();
            if (items != null)
                for (int i = 0; i < items.length; i++)
                {
                    //System.err.println("-interface "+tested.getName()+" "+items[i].getName());
                    testedsuper.put(tested.getName(), items[i].getName());
                }
        }
    }

        
    /** 
     * Append new error message to the <code>failedMessages</code> buffer.
     *
     * @see #failedMessages
     */
    public void addError(String errorTyp, String className, String def,
                         String tail) {
                         
        Message c = createError(errorTyp, className, def, tail);
        failedMessages.addElement(c);

        if (!"Deprecated".equals(errorTyp))
            size++;
    }

    /**
     * Print all error messages collected by <code>failedMessages</code>.
     *
     * @see #addError(String,String,String,String)
     */
    public void printErrors() {
    
        exmsgs = 0;
        MsgExclude(testedsuper);
        Collections.sort(failedMessages);

        boolean hasHeader = false;
        int length = failedMessages.size();
        int lastType = -1;
        for (int i = 0; i < length; i++) {
            Message current = (Message)failedMessages.elementAt(i);
            if (current.errorType != lastType) {
                if ((headers != null) && (current.errorType < headers.length)) {
                    hasHeader = true;
                    out.println("\n" + headers[current.errorType] + "\n" +
                                space('-', headers[current.errorType].length()) + "\n");
                } else {
                    hasHeader = false;
                }
                lastType = current.errorType;
            }
            if (hasHeader) {
                if (current.definition.equals(""))
                    out.println(current.className);
                else {
                    int currentTab = (current.className.length() + 1) / tabSize;
                    if ((current.className.length() + 1) % tabSize != 0)
                        currentTab++;
                    currentTab = currentTab * tabSize;
                    out.println(current.className + ":" +
                                space(' ', currentTab - current.className.length() - 1) +
                                toString(current.definition));
                    if (isVerbose && current.tail.length() != 0)
                        out.println("--- affected "+current.tail);
                }
            } else {
                out.println(current);
            }
        }
        if (failedMessages.size() > 0)
            out.println("");
            
        if (exmsgs > 0)
            out.println("duplicate messages suppressed: " + exmsgs);
    }

    /**
     * Return string consisting of <code>len</code> copies of the symbol <code>c</code>.
     */
    protected static String space(char c, int len) {
        char buff[] = new char[len];
        for (int i = 0; i < len; i++)
            buff[i] = c;
        return new String(buff);
    }


     public void MsgExclude (Hashtable supernames) {
     
         int i, k, n;
         Vector /*Vector(Vector(Message))*/ vv = new Vector();
 
         for (i = 0; i < failedMessages.size(); i++) {
             Message msgi = (Message)failedMessages.elementAt(i);
 
             Vector /*(Message)*/ v = null;
 
             for (n = 0; n < vv.size(); n++) {
                 Vector x = (Vector)vv.elementAt(n);
                 if (MsgCompare((Message)x.firstElement(), msgi)) {
                     v = (Vector)vv.elementAt(n);
                     break;
             }   }
 
             if (v == null) {
                 for (k = i+1; k < failedMessages.size(); k++) {
                     Message msgk = (Message)failedMessages.elementAt(k);
 
                     if (MsgCompare(msgk, msgi)) {
                         if (v == null) {
                             v = new Vector();
                             vv.addElement(v);
                             v.addElement(msgi);
                         }
                         v.addElement(msgk);
         }   }   }   }   
 
         Vector exclude = new Vector();
 
         for (n = 0; n < vv.size(); n++) {
             Vector v = (Vector)vv.elementAt(n);
             //System.out.println("-Duplicate group-");
 
             for (k = 0; k < v.size(); k++) {
             rep:for (boolean flag = true; flag;) {
                     flag = false;
                     Message msgk = (Message)v.elementAt(k);
                     String  supk = (String)supernames.get(msgk.className);
                     if (supk != null) {
                         for (i = k+1; i < v.size(); i++) {
                             Message msgi = (Message)v.elementAt(i);
                             if (msgi.className.equals(supk)) {
                                 v.setElementAt(msgi, k);
                                 v.setElementAt(msgk, i);
                                 flag = true;
                                 //System.out.println("swap "+i+" "+k);
                                 continue rep;
             }   }   }   }   }
 
             for (k = v.size(); --k >= 0;) {
                 Message msgk = (Message)v.elementAt(k);
                 //System.out.println(MsgShow(msgk));
                 String  supk = (String)supernames.get(msgk.className);
                 if (supk != null) {
                     for (i = k; --i >= 0;) {
                         Message msgi = (Message)v.elementAt(i);
                         if (msgi.className.equals(supk)) {
                             if (msgi.tail.length() != 0)
                                 msgi.tail += ",";
                             msgi.tail  += msgk.className;
                             if (msgk.tail.length() != 0)
                                 msgi.tail += ","+msgk.tail;
                             exclude.addElement(v.elementAt(k));
                             exmsgs++; //size--;
                             //System.out.println(MsgShow(msgk)+"-excluded");
                             break;
         }   }   }   }   }
 
         for (i = failedMessages.size(); --i >= 0; ) {
             Message msgi = (Message)failedMessages.elementAt(i);
 
             for (k = 0; k < exclude.size(); k++) {
                     Message msgk = (Message)exclude.elementAt(k);
                     if (msgi == msgk) {
                         //System.out.println(MsgShow(msgi)+"-removed");
                         failedMessages.removeElementAt(i);
                         break;
         }   }       }
    }
 
 
     protected boolean MsgCompare (Message m1, Message m2) {
         return m1.errorType == m2.errorType 
             && m1.definition.equals(m2.definition) 
             //&& m1.tail.equals(m2.tail) 
             && m1.messages == m2.messages;
     }
 

/* 
     protected String MsgShow (Message msg) 
     {
         return "type:"+msg.errorType+
                " def:"+msg.definition+
               " tail:"+msg.tail+
              " class:"+msg.className;
     }
*/
}
