/*
 * @(#)ErrorFormatter.java	1.15 03/06/09
 *
 * Copyright (c) 2003 Sun Microsystems, Inc.  All rights reserved. 
 * SUN PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *
 * TDK Signature Test Tool. 
 *
 * @author Maxim Sokolnikov
 */

package com.sun.tdk.signaturetest;

import java.io.PrintWriter;


/**
 * This is class formats error messages reported by <b>SignatureTest</b> or 
 * <b>APIChangesTest</b>. This class prints messages using the ``plain'' format 
 * without any sorting and grouping of error messages.
 * 
 * @version 03/06/09
 */
public class ErrorFormatter {
    /**
     * Messages templates for different type of errors.
     */
    String[]  messages = {
	"Required class not found in implementation: ", // Missing Classes
        "Definition required but not found in ",       
        // Missing nested Classes or class definitions
	"Definition required but not found in ",       
	// Missing Superclasses or Superinterfaces
	"Definition required but not found in ",        // Missing Fields
	"Definition required but not found in ",        // Missing Constructors
	"Definition required but not found in ",        // Missing Methods
	"Found class not permitted in implementation: ",// Added Classes
	"Definition found but not permitted in ",      
        // Added nested Classes or class definitions
	"Definition found but not permitted in ",     
	// Added Superclasses or Superinterfaces
	"Definition found but not permitted in ",       // Added Fields
	"Definition found but not permitted in ",       // Added Constructors
	"Definition found but not permitted in ",       // Added Methods

        "Class modifiers are changed: ",// Changed Classes
	"Definition is changed in ",      
        // Changed nested Classes or class definitions
	"Definition is changed in ",      
	// Changed Superclasses or Superinterfaces
	"Definition is changed in ",       // Changed Fields
	"Definition is changed in ",       // Changed Constructors
	"Definition is changed in ",       // Changed Methods
        "LinkageError does not allow to track definition in ", // LinkageError
        "Warning: The deprecated API is not deprecated in ",
        "Warning: The API is deprecated in ",
    };
    
    /**
     * Output stream intended to print error messages.
     */
    PrintWriter out;

    /**
     * Number of errors printed.
     */
    int size;
    
    /**
     * Assign new <b>ErrorFormatter</b> to the given <b>PrintWriter</b>.
     */
    public ErrorFormatter(PrintWriter out) {
        this.out = out;
    }

    /**
     * Assign new <b>ErrorFormatter</b> to the given <b>PrintWriter</b>,
     * and reassign default <code>messages</code> templates.
     */
    public ErrorFormatter(PrintWriter out, String messages[]) {
        this.out = out;
        this.messages = messages;
    }

    /**
     * Print new error message, and increment errors counter.
     *
     * @see #createError(String,String,String,String)
     */
    public void addError(String errorType, String className, String def,
                         String tail) {
        out.println(createError(errorType, className, def, tail));
        if (!"Deprecated".equals(errorType))
            size++;
    }

    /**
     * Reassign default <code>messages</code> templates.
     */
    public void setMessages(String messages[]) {
        this.messages = messages;
    }

    /**
     * Create new error message.
     *
     * @param def Description of class of member affected by the error.
     *
     * @see Message#errorType
     * @see Message#className
     * @see Message#tail
     */        
    protected Message createError(String errorType, String className,
                                       String def, String tail) {
        int errorDeg = 0;
	if (def != null) {
	    if (def.startsWith(ItemDescription.INNER) ||
		def.startsWith(ItemDescription.CLASS))
		errorDeg = 1;
	    if (def.startsWith(ItemDescription.SUPER) || 
		def.startsWith(ItemDescription.INTERFACE))
		errorDeg = 2;
	    if (def.startsWith(ItemDescription.FIELD))
		errorDeg = 3;
	    if (def.startsWith(ItemDescription.CONSTRUCTOR))
		errorDeg = 4;
	    if (def.startsWith(ItemDescription.METHOD))
		errorDeg = 5;
	}

        
        if (errorType.equals("LinkageError"))
            errorDeg = 18;
        else if (errorType.equals("Deprecated")) {
            //creates warning about deprecated attribute changing
            if ((def != null) && (def.indexOf("<deprecated>") < 0))
                errorDeg = 19;
            else
                errorDeg = 20;
  	} else if (errorType.equals("Changed"))
	    errorDeg += 12;
	else if (!errorType.equals("Missing"))
	    errorDeg += 6;
        
        Message er = new Message(errorDeg, className, def, tail);
        er.setMessages(messages);
        return er;
    }
    
    /**
     * Print errors. This method is dummy, because no errors buffering is
     * implemented by this class. However, subclasses could overload this
     * method. 
     */
    public void printErrors() {
    }

    /**
     * Return number of errors already printed.
     */
    public int getNumErrors() {
        return size;
    }
        
    
    /**
     * Unwrap brief <b>ItemDescription</b> prefix for the given class or 
     * member description <code>label</code>. (To say: substitute <code>"CLSS"</code>
     * for <code>"class"</code>, or <code>"meth"</code> for <code>"method"</code>,
     * and so on.)
     */
    public static String toString(String label) {
        for (int i = 0; i < ItemDescription.prefixes.length; i++) {
            if (label.startsWith(ItemDescription.prefixes[i][0])) 
                return (ItemDescription.prefixes[i][1] + 
                        label.substring(ItemDescription.prefixes[i][0].length()));
        }
        return label;
    }

    /**
     * This class formats some error message reported by
     * <b>SignatureTest</b> or other similar tests.
     */
    public class Message implements Comparable {

        /**
         * Message templates for different error types.
	 *
	 * @see #errorType
         */
        public String messages[];

        /**
         * Name of the class affected by <code>this</code> error message.
         */
        public String className;

        /**
	 * Class description affected by <code>this</code> error message.
	 */
        public String definition;
        
        /**
         * The tail to append to <code>this</code> error message.
         */
        public String tail;
        
        /**
	 * Type of <code>this</code> error message.
	 */
        public int errorType;
        
        /**
         * Create new error message.
	 *
         * @see #errorType
         * @see #className
         * @see #definition
         * @see #tail
         */
        public Message(int errorType, String className, String definition,
                       String tail) {
            this.errorType = errorType;
            this.className = className;
            this.definition = (definition == null) ? "" : definition ;
            this.tail = (tail == null) ? "" : tail ;
        
        }

        /**
         * Assign <code>messages</code> templates.
	 *
	 * @see #messages
         */
        public void setMessages(String messages []) {
            this.messages = messages;
        }

        /**
         * Compare <code>this</code> <b>Message</b> to the given
         * <b>Message</b> <code>ob</code>.
	 * Messages ordering is the following: <br>
         * &nbsp; 1. Compare <code>errorType</code> fields as integers. <br>
         * &nbsp; 2. If <code>errorType</code> fields are equal, than compare 
	 *           <code>className</code> fields. <br>
         * &nbsp; 3. If <code>className</code> fields also equals, than compare 
	 *           <code>definition</code> fields.
         */
        public int compareTo(Object o) {
            Message ob = (Message)o;
            
            if (ob.errorType == this.errorType) {
                if (this.className.equals(ob.className))
                    return (getShortName(this.definition)).compareTo(
                        getShortName(ob.definition));
                else 
                    return this.className.compareTo(ob.className);
            } else {
                return this.errorType - ob.errorType;
            }
        }

        /**
         * Cut ``<code>throws</code>'' clause out off the given member 
	 * description <code>def</code>.
         */
        public String getShortName(String def) {
            String retVal;
            int pos = def.lastIndexOf(" throws ");
            if (pos >= 0)
                retVal = def.substring(0, pos);
            else
                retVal = def;
            return retVal.substring(retVal.lastIndexOf(' ') + 1);
        }

        /**
         * Return string representing <code>this</code> <b>Message</b>.
         */
        public String toString() {
            String retVal;
            if (errorType < messages.length)
                retVal = messages[errorType];
            else
                retVal = "Unknown error ";
            return retVal + className +  "\n    " + 
                   ErrorFormatter.toString(definition) + tail;
        }
    }
}
