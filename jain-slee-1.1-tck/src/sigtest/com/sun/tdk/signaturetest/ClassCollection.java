/*
 * @(#)ClassCollection.java	1.7 03/03/26
 *
 * Copyright (c) 2003 Sun Microsystems, Inc.  All rights reserved. 
 * SUN PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *
 * TDK Signature Test Tool. 
 *
 * @author Maxim Sokolnikov
 */

package com.sun.tdk.signaturetest;

import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Vector;

/**
 * <b>ClassCollection</b> implements hashtable, which maps key values to 
 * <b>Vector</b> instances. This class provides methods manipulating
 * those vectors, vector components, or whole collection of vectors.
 * It is intended to maintain collection of <b>FullItemDescription</b> 
 * instances describing set of members for some <b>ClassDescription</b>.
 *
 * @see ClassDescription
 * @see FullItemDescription
 *
 * @version 03/03/26
 */
public class ClassCollection {
    private Hashtable definitions;

    /** Incarnate with empty hashtable. */
    public ClassCollection() {
        this.definitions = new Hashtable();
    }

    /**
     * Add the given value to that vector assigned to the given <code>key</code>.
     * Create new <b>Vector</b> instance for the given <code>key</code>, if there 
     * was no such <code>key</code> known in <code>this</code> hashtable.
     *
     * @param def That value to add to the keyed vector.
     *
     * @see #addUniqueElement(Object,Object)
     */
    public void addElement(Object key, Object def) {
        Vector h = (Vector)definitions.get(key);
        if (h == null) {
            h = new Vector();
            this.definitions.put(key, h);
        }
        h.addElement(def);
    }

    /**
     * Add the given value to that vector assigned to the given <code>key</code>,
     * if there was no such value in that vector. Create new <b>Vector</b> instance 
     * for the given <code>key</code>, if there was no such <code>key</code> known 
     * in <code>this</code> hashtable.
     *
     * @param def That value to add to the keyed vector.
     *
     * @see #addElement(Object,Object)
     */
    public void addUniqueElement(Object key, Object def) {
        Vector h = (Vector)definitions.get(key);
        if (h == null){
            h = new Vector();
            this.definitions.put(key, h);
        }
        if (!h.contains(def))
            h.addElement(def);
    }

    /**
     * List all keys known for <code>this</code> hashtable.
     */
    public Enumeration keys() {
        return definitions.keys();
    }

    /**
     * Return that vectors mapped for the given <code>key</code>.
     */
    public Vector get(Object key) {
        return (Vector)definitions.get(key);
    }

    /**
     * Map the given <code>key</code> to newly created vector containing
     * the only component - reference to the given object <code>def</code>.
     * If there was other vector previously assigned to the given 
     * <code>key</code>, it is eliminated.
     */
    public void put(Object key, Object def) {
        Vector h = new Vector();
        h.addElement(def);
        this.definitions.put(key, h);
    }

    /**
     * Map the given <code>key</code> to the given vector.
     * If other vector was assigned to this <code>key</code>,
     * that vector is eliminated.
     *
     * @param member The vector to assign to the <code>key</code>.
     */
    public void putVector(Object key, Vector member) {
        definitions.put(key, member);
    }

    /**
     * Remove that vector assigned to the given <code>key</code> (if exists).
     */
    public void remove(Object key) {
        definitions.remove(key);
    }

    /**
     * Clear <code>this</code> hashtable (make it empty).
     */
    public void clear() {
        definitions.clear();
    }

    /**
     * Perform <code>addUniqueElement(key,vector)</code> for all
     * <code>(key,vector)</code> pairs in the given <b>ClassCollection</b>.
     *
     * @param values Collection of <code>(key,vector)</code> pairs.
     *
     * @see #addUniqueElement(Object,Object)
     */
    public void addUniqueCollection(ClassCollection values) {
        for (Enumeration e = values.keys(); e.hasMoreElements();) {
            Object key = e.nextElement();
            Vector h = (Vector)values.get(key);
            for (Enumeration el = h.elements(); el.hasMoreElements();
                 addUniqueElement(key, el.nextElement()));
        }
    }        
}
