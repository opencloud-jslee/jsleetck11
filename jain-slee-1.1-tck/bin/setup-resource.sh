#!/bin/sh
#
# Removes and/or installs the TCK resource adaptor in the SLEE. 
# The file config/slee-management.config must be properly configured
# for this script to work.

if [ ! -f ./bin/setup-resource.sh ]; then
  echo "This script must be run from the tck directory"; exit 1
fi

CONFIG_PATH="./config/config_variables.unix"
if [ ! -f $CONFIG_PATH ]; then
  echo "$CONFIG_PATH not found. Run the setup script (./bin/setup.sh), or create the file manually as described in ./config/config_variables.unix.template"; exit 1
fi
. $CONFIG_PATH

if [ -z "$VENDOR_LIB" ]; then
  echo "VENDOR_LIB variable must be specified in $CONFIG_PATH"; exit 1
fi

if [ -d "./lib" ]; then
 LIBS=./lib
elif [ -d "/net/java/java_libs" ]; then
 LIBS=/net/java/java_lib
elif [ -d "/mnt/java/java_libs" ]; then
 LIBS=/mnt/java/java_lib
else 
 echo "Couldn't find libraries directory"; exit 1
fi

if [ -n "$JAVA_HOME" ]; then
 JAVA=${JAVA_HOME}/bin/java
else 
 JAVA=java
fi

JARS=jars
CLASSPATH=$LIBS/slee.jar
CLASSPATH=$CLASSPATH:$LIBS/slee-ext.jar
CLASSPATH=$CLASSPATH:$LIBS/jmxri.jar
#CLASSPATH=$CLASSPATH:$LIBS/log4j.jar
CLASSPATH=$CLASSPATH:$JARS/sleetck.jar
CLASSPATH=$CLASSPATH:$JARS/sleetck-ra-common.jar
CLASSPATH=$CLASSPATH:$JARS/tcktools.jar
CLASSPATH=$CLASSPATH:$VENDOR_LIB

if [ ! -f "$JARS/tcktools.jar" ]; then
  echo "Required jar file could not be found: $JARS/tcktools.jar"
  exit 1
fi

$JAVA -classpath $CLASSPATH com.opencloud.sleetck.ext.sleemanagement.ResourceManagement $*
