@ECHO OFF
REM 
REM  Removes and/or installs the TCK resource adaptor in the SLEE. 
REM  The file config/slee-management.config must be properly configured
REM  for this script to work.

IF NOT EXIST bin\setup-resource.bat (
 ECHO ERROR: This script must be run from the tck directory
 GOTO End
)

REM  Validate current working directory
IF NOT EXIST bin\read-config-variables.bat (
 ECHO This script must be run from the root sleetck directory
 GOTO End
)
CALL bin\read-config-variables.bat
IF NOT "%ERRORLEVEL%" == "0" GOTO End

IF NOT DEFINED JAVA_HOME (
 ECHO ERROR: Set JAVA_HOME before running this script
 GOTO End
)

SET TCK_LIB=lib
SET TCK_JARS=jars

SET CLASSPATH=%TCK_LIB%\slee.jar
SET CLASSPATH=%CLASSPATH%;%TCK_JARS%\sleetck.jar
SET CLASSPATH=%CLASSPATH%;%TCK_JARS%\sleetck-ra-common.jar
SET CLASSPATH=%CLASSPATH%;%TCK_JARS%\tcktools.jar
SET CLASSPATH=%CLASSPATH%;%TCK_LIB%\jmxri.jar

REM Validate tcktools.jar
IF NOT EXIST %TCK_JARS%\tcktools.jar (
 ECHO Required jar file could not be found: $JARS/tcktools.jar
 GOTO End
)

%JAVA_HOME%\bin\java -classpath %CLASSPATH% com.opencloud.sleetck.ext.sleemanagement.ResourceManagement %1 %2 %3 %4 %5 %6 %7 %8 %9

:End
