#!/bin/sh
#
# Sets up the JAIN SLEE TCK:
# - generates configuration files from templates for TCK1.1 Release
# 
# Syntax: <script> [<VENDOR_LIB>]
#  VENDOR_LIB - the optional path of a jar file containing vendor classes.
#     If not specified, the variable VENDOR_LIB will have to be set 
#     manually in config/config_variables.unix

VENDOR_LIB=$1

TCKTOOLS_LIB=jars/tcktools.jar
CLASSPATH=$TCKTOOLS_LIB
$JAVA_HOME/bin/java -classpath $CLASSPATH com.opencloud.sleetck.ext.SetupTCKConfig $VENDOR_LIB

if [ $? -ne 0 ]; then
    exit
fi

echo "Inserting valid checksum into tck configuration interview file..."
./bin/insert-interview-checksum.sh config/sleetck.jti

if [ $? -ne 0 ]; then
    exit
fi

echo "The TCK is installed. See the User's Guide for instructions on how to configure and run it."

if [ -z "$VENDOR_LIB" ]; then
  echo "Although the install was successful, please now specify a value for the VENDOR_LIB "
  echo "property in config/config_variables.unix"
  echo "(This can be automatically set by this installer script if passed as a command line argument.)"
fi
