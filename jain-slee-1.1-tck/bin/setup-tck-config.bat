@ECHO OFF
REM
REM  Sets up the JAIN SLEE TCK:
REM  - generates configuration files from templates for TCK1.1 Release
REM
REM  Syntax: <script> [<VENDOR_LIB>]
REM  VENDOR_LIB - the optional path of a jar file containing vendor classes.
REM   If not specified, the variable VENDOR_LIB will have to be set
REM   manually in config/config_variables.bat

SET VENDOR_LIB=%1

SET TCKTOOLS_LIB=jars\tcktools.jar
SET CLASSPATH=%TCKTOOLS_LIB%
%JAVA_HOME%\bin\java -classpath %CLASSPATH% com.opencloud.sleetck.ext.SetupTCKConfig %VENDOR_LIB%
IF NOT "%ERRORLEVEL%" == "0" GOTO END

ECHO Inserting valid checksum into tck configuration interview file...
CALL bin\insert-interview-checksum.bat config\sleetck.jti
IF NOT "%ERRORLEVEL%" == "0" GOTO END

ECHO The TCK is installed. See the User's Guide for instructions on how to configure and run it.

IF "%VENDOR_LIB%" == "" (
  ECHO Although the install was successful, please now specify a value for the VENDOR_LIB
  ECHO property in config\config_variables.bat
  ECHO (This can be automatically set by this installer script if passed as a command line argument.)
)

:END
