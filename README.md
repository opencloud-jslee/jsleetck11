# The JAIN SLEE (JSLEE) v1.1 Technology Compatibility Kit #

This repository contains the JSLEE 1.1 Technology Compatibility Kit (TCK).

The JSR 240 specification and API documentation can be found here: http://www.jcp.org/en/jsr/detail?id=240

The TCK contains the various documentation, tests, source-code, and build files required in order to test an implementation of JSR 240 for compliance against the specification.

## Licensing information related to the TCK ##

The TCK is available under the license [JSLEE TCK License agreement](https://bitbucket.org/opencloud-jslee/jsleetck11/src/07ad36f31359a1bf8e38a36e4e95b0e841c29b0e/jslee-1.1-tck-license.txt?at=master&fileviewer=file-view-default)

## Getting the TCK ##

Before downloading the TCK please review its license.

The TCK can be obtained via taking a git clone of this git repository.
There are several ways to do this.

Via ssh
```
#!bash

git clone git@bitbucket.org:opencloud-jslee/jsleetck11.git
```

Via http


```
#!bash

git clone https://your-username@bitbucket.org/opencloud-jslee/jsleetck11.git
```

## Further information ##

More information is available on the
[Project's Wiki](https://bitbucket.org/opencloud-jslee/jsleetck11/wiki/Home)